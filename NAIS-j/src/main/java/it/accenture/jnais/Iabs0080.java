package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcJobExecutionDao;
import it.accenture.jnais.commons.data.to.IBtcJobExecution;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcJobExecution;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Iabs0080Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.BjeDtEnd;

/**Original name: IABS0080<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.  MAGGIO 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULI PER ACCESSO RISORSE DB               *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0080 extends Program implements IBtcJobExecution {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcJobExecutionDao btcJobExecutionDao = new BtcJobExecutionDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Iabs0080Data ws = new Iabs0080Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BTC-JOB-EXECUTION
    private BtcJobExecution btcJobExecution;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0080_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BtcJobExecution btcJobExecution) {
        this.idsv0003 = idsv0003;
        this.btcJobExecution = btcJobExecution;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                 PERFORM A200-ELABORA-PK       THRU A200-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE
        switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

            case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK       THRU A200-EX
                a200ElaboraPk();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
                break;
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0080 getInstance() {
        return ((Iabs0080)Programs.getInstance(Iabs0080.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0080'               TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0080");
        // COB_CODE: MOVE 'BTC_JOB_EXECUTION'      TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_JOB_EXECUTION");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND
        //                                        TO WS-BUFFER-WHERE-COND.
        ws.setWsBufferWhereCondFormatted(idsv0003.getBufferWhereCondFormatted());
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a210SelectPk() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BATCH
        //                ,ID_JOB
        //                ,ID_EXECUTION
        //                ,COD_ELAB_STATE
        //                ,DATA
        //                ,FLAG_WARNINGS
        //                ,DT_START
        //                ,DT_END
        //                ,USER_START
        //             INTO
        //                :BJE-ID-BATCH
        //               ,:BJE-ID-JOB
        //               ,:BJE-ID-EXECUTION
        //               ,:BJE-COD-ELAB-STATE
        //               ,:BJE-DATA-VCHAR
        //                :IND-BJE-DATA
        //               ,:BJE-FLAG-WARNINGS
        //                :IND-BJE-FLAG-WARNINGS
        //               ,:BJE-DT-START-DB
        //               ,:BJE-DT-END-DB
        //                :IND-BJE-DT-END
        //               ,:BJE-USER-START
        //             FROM BTC_JOB_EXECUTION
        //             WHERE     ID_BATCH = :BJE-ID-BATCH
        //                   AND ID_JOB = :BJE-ID-JOB
        //                   AND ID_EXECUTION = :BJE-ID-EXECUTION
        //           END-EXEC.
        btcJobExecutionDao.selectRec(btcJobExecution.getBjeIdBatch(), btcJobExecution.getBjeIdJob(), btcJobExecution.getBjeIdExecution(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z080-ESTRAI-ID-EXECUTION     THRU Z080-EX.
        z080EstraiIdExecution();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: MOVE WS-LUNG-EFF-BLOB                TO BJE-DATA-LEN
            btcJobExecution.setBjeDataLen(((short)(ws.getWsLungEffBlob())));
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO BTC_JOB_EXECUTION
            //                  (
            //                     ID_BATCH
            //                    ,ID_JOB
            //                    ,ID_EXECUTION
            //                    ,COD_ELAB_STATE
            //                    ,DATA
            //                    ,FLAG_WARNINGS
            //                    ,DT_START
            //                    ,DT_END
            //                    ,USER_START
            //                  )
            //              VALUES
            //                  (
            //                    :BJE-ID-BATCH
            //                    ,:BJE-ID-JOB
            //                    ,:BJE-ID-EXECUTION
            //                    ,:BJE-COD-ELAB-STATE
            //                    ,:BJE-DATA-VCHAR
            //                     :IND-BJE-DATA
            //                    ,:BJE-FLAG-WARNINGS
            //                     :IND-BJE-FLAG-WARNINGS
            //                    ,:BJE-DT-START-DB
            //                    ,:BJE-DT-END-DB
            //                     :IND-BJE-DT-END
            //                    ,:BJE-USER-START
            //                  )
            //           END-EXEC
            btcJobExecutionDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a230UpdatePk() {
        // COB_CODE: MOVE WS-LUNG-EFF-BLOB     TO BJE-DATA-LEN
        btcJobExecution.setBjeDataLen(((short)(ws.getWsLungEffBlob())));
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_JOB_EXECUTION SET
        //                   ID_BATCH       = :BJE-ID-BATCH
        //                  ,ID_JOB         = :BJE-ID-JOB
        //                  ,ID_EXECUTION   = :BJE-ID-EXECUTION
        //                  ,COD_ELAB_STATE = :BJE-COD-ELAB-STATE
        //                  ,DATA           = :BJE-DATA-VCHAR
        //                                    :IND-BJE-DATA
        //                  ,FLAG_WARNINGS  = :BJE-FLAG-WARNINGS
        //                                    :IND-BJE-FLAG-WARNINGS
        //                  ,DT_START       = :BJE-DT-START-DB
        //                  ,DT_END         = :BJE-DT-END-DB
        //                                    :IND-BJE-DT-END
        //                  ,USER_START     = :BJE-USER-START
        //                WHERE  ID_BATCH     = :BJE-ID-BATCH
        //                   AND ID_JOB       = :BJE-ID-JOB
        //                   AND ID_EXECUTION = :BJE-ID-EXECUTION
        //           END-EXEC.
        btcJobExecutionDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM BTC_JOB_EXECUTION
        //                WHERE  ID_BATCH     = :BJE-ID-BATCH
        //                   AND ID_JOB       = :BJE-ID-JOB
        //                   AND ID_EXECUTION = :BJE-ID-EXECUTION
        //           END-EXEC.
        btcJobExecutionDao.deleteRec(btcJobExecution.getBjeIdBatch(), btcJobExecution.getBjeIdJob(), btcJobExecution.getBjeIdExecution());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z080-ESTRAI-ID-EXECUTION<br>
	 * <pre>*****************************************************************
	 * **************************************************************
	 *  STACCO DI SEQUENCE
	 * **************************************************************</pre>*/
    private void z080EstraiIdExecution() {
        Lccs0090 lccs0090 = null;
        // COB_CODE: MOVE SPACES             TO LCCC0090
        ws.getLccc0090().initLccc0090Spaces();
        // COB_CODE: MOVE 'SEQ_ID_JOB'       TO LINK-NOME-TABELLA
        ws.getLccc0090().setNomeTabella("SEQ_ID_JOB");
        // COB_CODE: CALL PGM-LCCS0090       USING LCCC0090
        lccs0090 = Lccs0090.getInstance();
        lccs0090.run(ws.getLccc0090());
        // COB_CODE: MOVE LINK-RETURN-CODE
        //                            TO IDSV0003-RETURN-CODE
        idsv0003.getReturnCode().setReturnCode(ws.getLccc0090().getReturnCode().getReturnCode());
        // COB_CODE: MOVE LINK-SQLCODE
        //                            TO IDSV0003-SQLCODE
        idsv0003.getSqlcode().setSqlcode(ws.getLccc0090().getSqlcode().getSqlcodeSigned());
        // COB_CODE: MOVE LINK-DESCRIZ-ERR-DB2
        //                            TO IDSV0003-DESCRIZ-ERR-DB2
        idsv0003.getCampiEsito().setDescrizErrDb2(ws.getLccc0090().getDescrizErrDb2());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              MOVE LINK-SEQ-TABELLA TO BJE-ID-EXECUTION
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE LINK-SEQ-TABELLA TO BJE-ID-EXECUTION
            btcJobExecution.setBjeIdExecution(ws.getLccc0090().getSeqTabella());
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BJE-DATA = -1
        //              MOVE HIGH-VALUES TO BJE-DATA
        //           END-IF
        if (ws.getIndBtcJobExecution().getData2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJE-DATA
            btcJobExecution.setBjeData(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcJobExecution.Len.BJE_DATA));
        }
        // COB_CODE: IF IND-BJE-FLAG-WARNINGS = -1
        //              MOVE HIGH-VALUES TO BJE-FLAG-WARNINGS-NULL
        //           END-IF
        if (ws.getIndBtcJobExecution().getFlagWarnings() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJE-FLAG-WARNINGS-NULL
            btcJobExecution.setBjeFlagWarnings(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BJE-DT-END = -1
        //              MOVE HIGH-VALUES TO BJE-DT-END-NULL
        //           END-IF.
        if (ws.getIndBtcJobExecution().getDtEnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BJE-DT-END-NULL
            btcJobExecution.getBjeDtEnd().setBjeDtEndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BjeDtEnd.Len.BJE_DT_END_NULL));
        }
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BJE-DATA = HIGH-VALUES
        //              MOVE -1 TO IND-BJE-DATA
        //           ELSE
        //              MOVE 0 TO IND-BJE-DATA
        //           END-IF
        if (Characters.EQ_HIGH.test(btcJobExecution.getBjeData(), BtcJobExecution.Len.BJE_DATA)) {
            // COB_CODE: MOVE -1 TO IND-BJE-DATA
            ws.getIndBtcJobExecution().setData2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJE-DATA
            ws.getIndBtcJobExecution().setData2(((short)0));
        }
        // COB_CODE: IF BJE-FLAG-WARNINGS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJE-FLAG-WARNINGS
        //           ELSE
        //              MOVE 0 TO IND-BJE-FLAG-WARNINGS
        //           END-IF
        if (Conditions.eq(btcJobExecution.getBjeFlagWarnings(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BJE-FLAG-WARNINGS
            ws.getIndBtcJobExecution().setFlagWarnings(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJE-FLAG-WARNINGS
            ws.getIndBtcJobExecution().setFlagWarnings(((short)0));
        }
        // COB_CODE: IF BJE-DT-END-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BJE-DT-END
        //           ELSE
        //              MOVE 0 TO IND-BJE-DT-END
        //           END-IF.
        if (Characters.EQ_HIGH.test(btcJobExecution.getBjeDtEnd().getBjeDtEndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BJE-DT-END
            ws.getIndBtcJobExecution().setDtEnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BJE-DT-END
            ws.getIndBtcJobExecution().setDtEnd(((short)0));
        }
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE BJE-DT-START        TO WS-TIMESTAMP-N
        ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcJobExecution.getBjeDtStart(), 18));
        // COB_CODE: PERFORM Z701-TS-N-TO-X   THRU Z701-EX
        z701TsNToX();
        // COB_CODE: MOVE WS-TIMESTAMP-X      TO BJE-DT-START-DB
        ws.getIdbvbje3().setBjeDtStartDb(ws.getIdsv0010().getWsTimestampX());
        // COB_CODE: IF IND-BJE-DT-END = 0
        //               MOVE WS-TIMESTAMP-X     TO BJE-DT-END-DB
        //           END-IF.
        if (ws.getIndBtcJobExecution().getDtEnd() == 0) {
            // COB_CODE: MOVE BJE-DT-END         TO WS-TIMESTAMP-N
            ws.getIdsv0010().setWsTimestampN(TruncAbs.toLong(btcJobExecution.getBjeDtEnd().getBjeDtEnd(), 18));
            // COB_CODE: PERFORM Z701-TS-N-TO-X  THRU Z701-EX
            z701TsNToX();
            // COB_CODE: MOVE WS-TIMESTAMP-X     TO BJE-DT-END-DB
            ws.getIdbvbje3().setBjeDtEndDb(ws.getIdsv0010().getWsTimestampX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE BJE-DT-START-DB TO WS-TIMESTAMP-X
        ws.getIdsv0010().setWsTimestampX(ws.getIdbvbje3().getBjeDtStartDb());
        // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
        z801TsXToN();
        // COB_CODE: MOVE WS-TIMESTAMP-N      TO BJE-DT-START
        btcJobExecution.setBjeDtStart(ws.getIdsv0010().getWsTimestampN());
        // COB_CODE: IF IND-BJE-DT-END = 0
        //               MOVE WS-TIMESTAMP-N      TO BJE-DT-END
        //           END-IF.
        if (ws.getIndBtcJobExecution().getDtEnd() == 0) {
            // COB_CODE: MOVE BJE-DT-END-DB TO WS-TIMESTAMP-X
            ws.getIdsv0010().setWsTimestampX(ws.getIdbvbje3().getBjeDtEndDb());
            // COB_CODE: PERFORM Z801-TS-X-TO-N     THRU Z801-EX
            z801TsXToN();
            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BJE-DT-END
            btcJobExecution.getBjeDtEnd().setBjeDtEnd(ws.getIdsv0010().getWsTimestampN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF BJE-DATA
        //                       TO BJE-DATA-LEN.
        btcJobExecution.setBjeDataLen(((short)BtcJobExecution.Len.BJE_DATA));
    }

    /**Original name: Z701-TS-N-TO-X<br>*/
    private void z701TsNToX() {
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(1:4)
        //                TO WS-TIMESTAMP-X(1:4)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(5:2)
        //                TO WS-TIMESTAMP-X(6:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(7:2)
        //                TO WS-TIMESTAMP-X(9:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(9:2)
        //                TO WS-TIMESTAMP-X(12:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((9) - 1, 10), 12, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(11:2)
        //                TO WS-TIMESTAMP-X(15:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((11) - 1, 12), 15, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(13:2)
        //                TO WS-TIMESTAMP-X(18:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((13) - 1, 14), 18, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(15:4)
        //                TO WS-TIMESTAMP-X(21:4)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ws.getIdsv0010().getWsStrTimestampNFormatted().substring((15) - 1, 18), 21, 4));
        // COB_CODE: MOVE '00'
        //                TO WS-TIMESTAMP-X(25:2)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "00", 25, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-TIMESTAMP-X(5:1)
        //                   WS-TIMESTAMP-X(8:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "-", 5, 1));
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), "-", 8, 1));
        // COB_CODE: MOVE SPACE
        //                TO WS-TIMESTAMP-X(11:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), LiteralGenerator.create(Types.SPACE_CHAR, 1), 11, 1));
        // COB_CODE: MOVE ':'
        //                TO WS-TIMESTAMP-X(14:1)
        //                   WS-TIMESTAMP-X(17:1)
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ":", 14, 1));
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ":", 17, 1));
        // COB_CODE: MOVE '.'
        //                TO WS-TIMESTAMP-X(20:1).
        ws.getIdsv0010().setWsTimestampX(Functions.setSubstring(ws.getIdsv0010().getWsTimestampX(), ".", 20, 1));
    }

    /**Original name: Z801-TS-X-TO-N<br>*/
    private void z801TsXToN() {
        // COB_CODE: MOVE WS-TIMESTAMP-X(1:4)
        //                TO WS-STR-TIMESTAMP-N(1:4)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-TIMESTAMP-X(6:2)
        //                TO WS-STR-TIMESTAMP-N(5:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(9:2)
        //                TO WS-STR-TIMESTAMP-N(7:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((9) - 1, 10), 7, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(12:2)
        //                TO WS-STR-TIMESTAMP-N(9:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((12) - 1, 13), 9, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(15:2)
        //                TO WS-STR-TIMESTAMP-N(11:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((15) - 1, 16), 11, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(18:2)
        //                TO WS-STR-TIMESTAMP-N(13:2)
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((18) - 1, 19), 13, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(21:4)
        //                TO WS-STR-TIMESTAMP-N(15:4).
        ws.getIdsv0010().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrTimestampNFormatted(), ws.getIdsv0010().getWsTimestampXFormatted().substring((21) - 1, 24), 15, 4));
    }

    @Override
    public char getCodElabState() {
        return btcJobExecution.getBjeCodElabState();
    }

    @Override
    public void setCodElabState(char codElabState) {
        this.btcJobExecution.setBjeCodElabState(codElabState);
    }

    @Override
    public String getDataVchar() {
        return btcJobExecution.getBjeDataVcharFormatted();
    }

    @Override
    public void setDataVchar(String dataVchar) {
        this.btcJobExecution.setBjeDataVcharFormatted(dataVchar);
    }

    @Override
    public String getDataVcharObj() {
        if (ws.getIndBtcJobExecution().getData2() >= 0) {
            return getDataVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDataVcharObj(String dataVcharObj) {
        if (dataVcharObj != null) {
            setDataVchar(dataVcharObj);
            ws.getIndBtcJobExecution().setData2(((short)0));
        }
        else {
            ws.getIndBtcJobExecution().setData2(((short)-1));
        }
    }

    @Override
    public String getDtEndDb() {
        return ws.getIdbvbje3().getBjeDtEndDb();
    }

    @Override
    public void setDtEndDb(String dtEndDb) {
        this.ws.getIdbvbje3().setBjeDtEndDb(dtEndDb);
    }

    @Override
    public String getDtEndDbObj() {
        if (ws.getIndBtcJobExecution().getDtEnd() >= 0) {
            return getDtEndDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndDbObj(String dtEndDbObj) {
        if (dtEndDbObj != null) {
            setDtEndDb(dtEndDbObj);
            ws.getIndBtcJobExecution().setDtEnd(((short)0));
        }
        else {
            ws.getIndBtcJobExecution().setDtEnd(((short)-1));
        }
    }

    @Override
    public String getDtStartDb() {
        return ws.getIdbvbje3().getBjeDtStartDb();
    }

    @Override
    public void setDtStartDb(String dtStartDb) {
        this.ws.getIdbvbje3().setBjeDtStartDb(dtStartDb);
    }

    @Override
    public char getFlagWarnings() {
        return btcJobExecution.getBjeFlagWarnings();
    }

    @Override
    public void setFlagWarnings(char flagWarnings) {
        this.btcJobExecution.setBjeFlagWarnings(flagWarnings);
    }

    @Override
    public Character getFlagWarningsObj() {
        if (ws.getIndBtcJobExecution().getFlagWarnings() >= 0) {
            return ((Character)getFlagWarnings());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlagWarningsObj(Character flagWarningsObj) {
        if (flagWarningsObj != null) {
            setFlagWarnings(((char)flagWarningsObj));
            ws.getIndBtcJobExecution().setFlagWarnings(((short)0));
        }
        else {
            ws.getIndBtcJobExecution().setFlagWarnings(((short)-1));
        }
    }

    @Override
    public int getIdBatch() {
        return btcJobExecution.getBjeIdBatch();
    }

    @Override
    public void setIdBatch(int idBatch) {
        this.btcJobExecution.setBjeIdBatch(idBatch);
    }

    @Override
    public int getIdExecution() {
        return btcJobExecution.getBjeIdExecution();
    }

    @Override
    public void setIdExecution(int idExecution) {
        this.btcJobExecution.setBjeIdExecution(idExecution);
    }

    @Override
    public int getIdJob() {
        return btcJobExecution.getBjeIdJob();
    }

    @Override
    public void setIdJob(int idJob) {
        this.btcJobExecution.setBjeIdJob(idJob);
    }

    @Override
    public String getUserStart() {
        return btcJobExecution.getBjeUserStart();
    }

    @Override
    public void setUserStart(String userStart) {
        this.btcJobExecution.setBjeUserStart(userStart);
    }
}
