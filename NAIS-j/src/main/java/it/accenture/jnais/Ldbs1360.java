package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.StatOggBusTrchDiGarDao;
import it.accenture.jnais.commons.data.to.IStatOggBusTrchDiGar;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs1360Data;
import it.accenture.jnais.ws.redefines.TgaAbbAnnuUlt;
import it.accenture.jnais.ws.redefines.TgaAbbTotIni;
import it.accenture.jnais.ws.redefines.TgaAbbTotUlt;
import it.accenture.jnais.ws.redefines.TgaAcqExp;
import it.accenture.jnais.ws.redefines.TgaAlqCommisInter;
import it.accenture.jnais.ws.redefines.TgaAlqProvAcq;
import it.accenture.jnais.ws.redefines.TgaAlqProvInc;
import it.accenture.jnais.ws.redefines.TgaAlqProvRicor;
import it.accenture.jnais.ws.redefines.TgaAlqRemunAss;
import it.accenture.jnais.ws.redefines.TgaAlqScon;
import it.accenture.jnais.ws.redefines.TgaBnsGiaLiqto;
import it.accenture.jnais.ws.redefines.TgaCommisGest;
import it.accenture.jnais.ws.redefines.TgaCommisInter;
import it.accenture.jnais.ws.redefines.TgaCosRunAssva;
import it.accenture.jnais.ws.redefines.TgaCosRunAssvaIdc;
import it.accenture.jnais.ws.redefines.TgaCptInOpzRivto;
import it.accenture.jnais.ws.redefines.TgaCptMinScad;
import it.accenture.jnais.ws.redefines.TgaCptRshMor;
import it.accenture.jnais.ws.redefines.TgaDtEffStab;
import it.accenture.jnais.ws.redefines.TgaDtEmis;
import it.accenture.jnais.ws.redefines.TgaDtIniValTar;
import it.accenture.jnais.ws.redefines.TgaDtScad;
import it.accenture.jnais.ws.redefines.TgaDtUltAdegPrePr;
import it.accenture.jnais.ws.redefines.TgaDtVldtProd;
import it.accenture.jnais.ws.redefines.TgaDurAa;
import it.accenture.jnais.ws.redefines.TgaDurAbb;
import it.accenture.jnais.ws.redefines.TgaDurGg;
import it.accenture.jnais.ws.redefines.TgaDurMm;
import it.accenture.jnais.ws.redefines.TgaEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.TgaIdMoviChiu;
import it.accenture.jnais.ws.redefines.TgaImpAder;
import it.accenture.jnais.ws.redefines.TgaImpAltSopr;
import it.accenture.jnais.ws.redefines.TgaImpAz;
import it.accenture.jnais.ws.redefines.TgaImpbCommisInter;
import it.accenture.jnais.ws.redefines.TgaImpBns;
import it.accenture.jnais.ws.redefines.TgaImpBnsAntic;
import it.accenture.jnais.ws.redefines.TgaImpbProvAcq;
import it.accenture.jnais.ws.redefines.TgaImpbProvInc;
import it.accenture.jnais.ws.redefines.TgaImpbProvRicor;
import it.accenture.jnais.ws.redefines.TgaImpbRemunAss;
import it.accenture.jnais.ws.redefines.TgaImpbVisEnd2000;
import it.accenture.jnais.ws.redefines.TgaImpCarAcq;
import it.accenture.jnais.ws.redefines.TgaImpCarGest;
import it.accenture.jnais.ws.redefines.TgaImpCarInc;
import it.accenture.jnais.ws.redefines.TgaImpScon;
import it.accenture.jnais.ws.redefines.TgaImpSoprProf;
import it.accenture.jnais.ws.redefines.TgaImpSoprSan;
import it.accenture.jnais.ws.redefines.TgaImpSoprSpo;
import it.accenture.jnais.ws.redefines.TgaImpSoprTec;
import it.accenture.jnais.ws.redefines.TgaImpTfr;
import it.accenture.jnais.ws.redefines.TgaImpTfrStrc;
import it.accenture.jnais.ws.redefines.TgaImpTrasfe;
import it.accenture.jnais.ws.redefines.TgaImpVolo;
import it.accenture.jnais.ws.redefines.TgaIncrPre;
import it.accenture.jnais.ws.redefines.TgaIncrPrstz;
import it.accenture.jnais.ws.redefines.TgaIntrMora;
import it.accenture.jnais.ws.redefines.TgaManfeeAntic;
import it.accenture.jnais.ws.redefines.TgaManfeeRicor;
import it.accenture.jnais.ws.redefines.TgaMatuEnd2000;
import it.accenture.jnais.ws.redefines.TgaMinGarto;
import it.accenture.jnais.ws.redefines.TgaMinTrnut;
import it.accenture.jnais.ws.redefines.TgaNumGgRival;
import it.accenture.jnais.ws.redefines.TgaOldTsTec;
import it.accenture.jnais.ws.redefines.TgaPcCommisGest;
import it.accenture.jnais.ws.redefines.TgaPcIntrRiat;
import it.accenture.jnais.ws.redefines.TgaPcRetr;
import it.accenture.jnais.ws.redefines.TgaPcRipPre;
import it.accenture.jnais.ws.redefines.TgaPreAttDiTrch;
import it.accenture.jnais.ws.redefines.TgaPreCasoMor;
import it.accenture.jnais.ws.redefines.TgaPreIniNet;
import it.accenture.jnais.ws.redefines.TgaPreInvrioIni;
import it.accenture.jnais.ws.redefines.TgaPreInvrioUlt;
import it.accenture.jnais.ws.redefines.TgaPreLrd;
import it.accenture.jnais.ws.redefines.TgaPrePattuito;
import it.accenture.jnais.ws.redefines.TgaPrePpIni;
import it.accenture.jnais.ws.redefines.TgaPrePpUlt;
import it.accenture.jnais.ws.redefines.TgaPreRivto;
import it.accenture.jnais.ws.redefines.TgaPreStab;
import it.accenture.jnais.ws.redefines.TgaPreTariIni;
import it.accenture.jnais.ws.redefines.TgaPreTariUlt;
import it.accenture.jnais.ws.redefines.TgaPreUniRivto;
import it.accenture.jnais.ws.redefines.TgaProv1aaAcq;
import it.accenture.jnais.ws.redefines.TgaProv2aaAcq;
import it.accenture.jnais.ws.redefines.TgaProvInc;
import it.accenture.jnais.ws.redefines.TgaProvRicor;
import it.accenture.jnais.ws.redefines.TgaPrstzAggIni;
import it.accenture.jnais.ws.redefines.TgaPrstzAggUlt;
import it.accenture.jnais.ws.redefines.TgaPrstzIni;
import it.accenture.jnais.ws.redefines.TgaPrstzIniNewfis;
import it.accenture.jnais.ws.redefines.TgaPrstzIniNforz;
import it.accenture.jnais.ws.redefines.TgaPrstzIniStab;
import it.accenture.jnais.ws.redefines.TgaPrstzRidIni;
import it.accenture.jnais.ws.redefines.TgaPrstzUlt;
import it.accenture.jnais.ws.redefines.TgaRatLrd;
import it.accenture.jnais.ws.redefines.TgaRemunAss;
import it.accenture.jnais.ws.redefines.TgaRendtoLrd;
import it.accenture.jnais.ws.redefines.TgaRendtoRetr;
import it.accenture.jnais.ws.redefines.TgaRenIniTsTec0;
import it.accenture.jnais.ws.redefines.TgaRisMat;
import it.accenture.jnais.ws.redefines.TgaTsRivalFis;
import it.accenture.jnais.ws.redefines.TgaTsRivalIndiciz;
import it.accenture.jnais.ws.redefines.TgaTsRivalNet;
import it.accenture.jnais.ws.redefines.TgaVisEnd2000;
import it.accenture.jnais.ws.redefines.TgaVisEnd2000Nforz;
import it.accenture.jnais.ws.TrchDiGar;

/**Original name: LDBS1360<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.  .
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         :
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs1360 extends Program implements IStatOggBusTrchDiGar {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private StatOggBusTrchDiGarDao statOggBusTrchDiGarDao = new StatOggBusTrchDiGarDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs1360Data ws = new Ldbs1360Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: TRCH-DI-GAR
    private TrchDiGar trchDiGar;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS1360_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, TrchDiGar trchDiGar) {
        this.idsv0003 = idsv0003;
        this.trchDiGar = trchDiGar;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-IF
            //           ELSE
            //            END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: IF LDBV1361-PRODOTTO OR LDBV1361-OPZIONE
                //              END-EVALUATE
                //           ELSE
                //              END-EVALUATE
                //           END-IF
                if (ws.getLdbv1361().getLdbv1351LivelloOperazioni().isLdbv1361Prodotto() || ws.getLdbv1361().getLdbv1351LivelloOperazioni().isLdbv1361Opzione()) {
                    // COB_CODE: EVALUATE TRUE ALSO TRUE
                    //             WHEN STATO ALSO CAUSALE
                    //               PERFORM A401-ELABORA-EFF THRU A401-EX
                    //             WHEN NOT-STATO ALSO CAUSALE
                    //               PERFORM C401-ELABORA-EFF-NS THRU C401-EX
                    //             WHEN STATO ALSO NOT-CAUSALE
                    //               PERFORM E401-ELABORA-EFF-NC THRU E401-EX
                    //             WHEN NOT-STATO ALSO NOT-CAUSALE
                    //               PERFORM G401-ELABORA-EFF-NS-NC THRU G401-EX
                    //           END-EVALUATE
                    if (ws.getFlagStato().isStato() && ws.getFlagCausale().isCausale()) {
                        // COB_CODE: PERFORM A401-ELABORA-EFF THRU A401-EX
                        a401ElaboraEff();
                    }
                    else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isCausale()) {
                        // COB_CODE: PERFORM C401-ELABORA-EFF-NS THRU C401-EX
                        c401ElaboraEffNs();
                    }
                    else if (ws.getFlagStato().isStato() && ws.getFlagCausale().isNotCausale()) {
                        // COB_CODE: PERFORM E401-ELABORA-EFF-NC THRU E401-EX
                        e401ElaboraEffNc();
                    }
                    else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isNotCausale()) {
                        // COB_CODE: PERFORM G401-ELABORA-EFF-NS-NC THRU G401-EX
                        g401ElaboraEffNsNc();
                    }
                }
                else if (ws.getFlagStato().isStato() && ws.getFlagCausale().isCausale()) {
                    // COB_CODE: EVALUATE TRUE ALSO TRUE
                    //             WHEN STATO ALSO CAUSALE
                    //               PERFORM A400-ELABORA-EFF       THRU A400-EX
                    //             WHEN NOT-STATO ALSO CAUSALE
                    //               PERFORM C400-ELABORA-EFF-NS    THRU C400-EX
                    //             WHEN STATO ALSO NOT-CAUSALE
                    //               PERFORM E400-ELABORA-EFF-NC    THRU E400-EX
                    //             WHEN NOT-STATO ALSO NOT-CAUSALE
                    //               PERFORM G400-ELABORA-EFF-NS-NC THRU G400-EX
                    //           END-EVALUATE
                    // COB_CODE: PERFORM A400-ELABORA-EFF       THRU A400-EX
                    a400ElaboraEff();
                }
                else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isCausale()) {
                    // COB_CODE: PERFORM C400-ELABORA-EFF-NS    THRU C400-EX
                    c400ElaboraEffNs();
                }
                else if (ws.getFlagStato().isStato() && ws.getFlagCausale().isNotCausale()) {
                    // COB_CODE: PERFORM E400-ELABORA-EFF-NC    THRU E400-EX
                    e400ElaboraEffNc();
                }
                else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isNotCausale()) {
                    // COB_CODE: PERFORM G400-ELABORA-EFF-NS-NC THRU G400-EX
                    g400ElaboraEffNsNc();
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE:  IF IDSV0003-TRATT-X-COMPETENZA
                //               END-IF
                //            ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER       TO TRUE
                //           END-IF
                // COB_CODE: IF LDBV1361-PRODOTTO OR LDBV1361-OPZIONE
                //              END-EVALUATE
                //           ELSE
                //              END-EVALUATE
                //           END-IF
                if (ws.getLdbv1361().getLdbv1351LivelloOperazioni().isLdbv1361Prodotto() || ws.getLdbv1361().getLdbv1351LivelloOperazioni().isLdbv1361Opzione()) {
                    // COB_CODE: EVALUATE TRUE ALSO TRUE
                    //             WHEN STATO ALSO CAUSALE
                    //                 PERFORM B401-ELABORA-CPZ       THRU B401-EX
                    //             WHEN NOT-STATO ALSO CAUSALE
                    //                 PERFORM D401-ELABORA-CPZ-NS    THRU D401-EX
                    //             WHEN STATO ALSO NOT-CAUSALE
                    //                 PERFORM F401-ELABORA-CPZ-NC    THRU F401-EX
                    //             WHEN NOT-STATO ALSO NOT-CAUSALE
                    //                 PERFORM H401-ELABORA-CPZ-NS-NC THRU H401-EX
                    //           END-EVALUATE
                    if (ws.getFlagStato().isStato() && ws.getFlagCausale().isCausale()) {
                        // COB_CODE: PERFORM B401-ELABORA-CPZ       THRU B401-EX
                        b401ElaboraCpz();
                    }
                    else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isCausale()) {
                        // COB_CODE: PERFORM D401-ELABORA-CPZ-NS    THRU D401-EX
                        d401ElaboraCpzNs();
                    }
                    else if (ws.getFlagStato().isStato() && ws.getFlagCausale().isNotCausale()) {
                        // COB_CODE: PERFORM F401-ELABORA-CPZ-NC    THRU F401-EX
                        f401ElaboraCpzNc();
                    }
                    else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isNotCausale()) {
                        // COB_CODE: PERFORM H401-ELABORA-CPZ-NS-NC THRU H401-EX
                        h401ElaboraCpzNsNc();
                    }
                }
                else if (ws.getFlagStato().isStato() && ws.getFlagCausale().isCausale()) {
                    // COB_CODE: EVALUATE TRUE ALSO TRUE
                    //             WHEN STATO ALSO CAUSALE
                    //               PERFORM B400-ELABORA-CPZ       THRU B400-EX
                    //             WHEN NOT-STATO ALSO CAUSALE
                    //               PERFORM D400-ELABORA-CPZ-NS    THRU D400-EX
                    //             WHEN STATO ALSO NOT-CAUSALE
                    //               PERFORM F400-ELABORA-CPZ-NC    THRU F400-EX
                    //             WHEN NOT-STATO ALSO NOT-CAUSALE
                    //               PERFORM H400-ELABORA-CPZ-NS-NC THRU H400-EX
                    //           END-EVALUATE
                    // COB_CODE: PERFORM B400-ELABORA-CPZ       THRU B400-EX
                    b400ElaboraCpz();
                }
                else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isCausale()) {
                    // COB_CODE: PERFORM D400-ELABORA-CPZ-NS    THRU D400-EX
                    d400ElaboraCpzNs();
                }
                else if (ws.getFlagStato().isStato() && ws.getFlagCausale().isNotCausale()) {
                    // COB_CODE: PERFORM F400-ELABORA-CPZ-NC    THRU F400-EX
                    f400ElaboraCpzNc();
                }
                else if (ws.getFlagStato().isNotStato() && ws.getFlagCausale().isNotCausale()) {
                    // COB_CODE: PERFORM H400-ELABORA-CPZ-NS-NC THRU H400-EX
                    h400ElaboraCpzNsNc();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER       TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs1360 getInstance() {
        return ((Ldbs1360)Programs.getInstance(Ldbs1360.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS1360 '   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS1360 ");
        // COB_CODE: MOVE 'TGA_STB'     TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("TGA_STB");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: MOVE 000000000                TO WK-ID-ADES-DA
        //                                            WK-ID-GAR-DA
        ws.setWkIdAdesDa(0);
        ws.setWkIdGarDa(0);
        // COB_CODE: MOVE 999999999                TO WK-ID-ADES-A
        //                                            WK-ID-GAR-A
        ws.setWkIdAdesA(999999999);
        ws.setWkIdGarA(999999999);
        // COB_CODE: SET STATO                     TO TRUE
        ws.getFlagStato().setStato();
        // COB_CODE: SET CAUSALE                   TO TRUE
        ws.getFlagCausale().setCausale();
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV1361
        ws.getLdbv1361().setLdbv1351Formatted(idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM V010-VERIFICA-WHERE-COND   THRU V010-EX
        v010VerificaWhereCond();
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: V010-VERIFICA-WHERE-COND<br>*/
    private void v010VerificaWhereCond() {
        // COB_CODE: IF TGA-ID-ADES IS NUMERIC AND
        //              TGA-ID-ADES NOT = ZERO
        //                                       WK-ID-ADES-A
        //           END-IF.
        if (Functions.isNumber(trchDiGar.getTgaIdAdes()) && trchDiGar.getTgaIdAdes() != 0) {
            // COB_CODE: MOVE TGA-ID-ADES      TO WK-ID-ADES-DA
            //                                    WK-ID-ADES-A
            ws.setWkIdAdesDa(trchDiGar.getTgaIdAdes());
            ws.setWkIdAdesA(trchDiGar.getTgaIdAdes());
        }
        // COB_CODE: IF TGA-ID-GAR IS NUMERIC AND
        //              TGA-ID-GAR NOT = ZERO
        //                                       WK-ID-GAR-A
        //           END-IF.
        if (Functions.isNumber(trchDiGar.getTgaIdGar()) && trchDiGar.getTgaIdGar() != 0) {
            // COB_CODE: MOVE TGA-ID-GAR       TO WK-ID-GAR-DA
            //                                    WK-ID-GAR-A
            ws.setWkIdGarDa(trchDiGar.getTgaIdGar());
            ws.setWkIdGarA(trchDiGar.getTgaIdGar());
        }
        // COB_CODE: PERFORM V020-VERIF-OPERATORI-LOGICI THRU V020-EX.
        v020VerifOperatoriLogici();
    }

    /**Original name: V020-VERIF-OPERATORI-LOGICI<br>*/
    private void v020VerifOperatoriLogici() {
        // COB_CODE: IF LDBV1361-OPER-LOG-STAT-BUS = NEGAZIONE
        //              SET NOT-STATO               TO TRUE
        //           END-IF
        if (Conditions.eq(ws.getLdbv1361().getLdbv1351OperLogStatBus(), ws.getNegazione())) {
            // COB_CODE: SET NOT-STATO               TO TRUE
            ws.getFlagStato().setNotStato();
        }
        // COB_CODE: IF LDBV1361-OPER-LOG-CAUS = NEGAZIONE
        //              SET NOT-CAUSALE             TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getLdbv1361().getLdbv1351OperLogCaus(), ws.getNegazione())) {
            // COB_CODE: SET NOT-CAUSALE             TO TRUE
            ws.getFlagCausale().setNotCausale();
        }
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A400-ELABORA-EFF<br>*/
    private void a400ElaboraEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-EFF  THRU A460-EX
            a460OpenCursorEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-EFF THRU A470-EX
            a470CloseCursorEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-EFF  THRU A480-EX
            a480FetchFirstEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-EFF   THRU A490-EX
            a490FetchNextEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A401-ELABORA-EFF<br>*/
    private void a401ElaboraEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A461-OPEN-CURSOR-EFF  THRU A461-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A471-CLOSE-CURSOR-EFF THRU A471-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A481-FETCH-FIRST-EFF  THRU A481-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A491-FETCH-NEXT-EFF   THRU A491-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A461-OPEN-CURSOR-EFF  THRU A461-EX
            a461OpenCursorEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A471-CLOSE-CURSOR-EFF THRU A471-EX
            a471CloseCursorEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A481-FETCH-FIRST-EFF  THRU A481-EX
            a481FetchFirstEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A491-FETCH-NEXT-EFF   THRU A491-EX
            a491FetchNextEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-CPZ<br>*/
    private void b400ElaboraCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-CPZ  THRU B460-EX
            b460OpenCursorCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-CPZ THRU B470-EX
            b470CloseCursorCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-CPZ  THRU B480-EX
            b480FetchFirstCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-CPZ   THRU B490-EX
            b490FetchNextCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B401-ELABORA-CPZ<br>*/
    private void b401ElaboraCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B461-OPEN-CURSOR-CPZ  THRU B461-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B471-CLOSE-CURSOR-CPZ THRU B471-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B481-FETCH-FIRST-CPZ  THRU B481-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B491-FETCH-NEXT-CPZ   THRU B491-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B461-OPEN-CURSOR-CPZ  THRU B461-EX
            b461OpenCursorCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B471-CLOSE-CURSOR-CPZ THRU B471-EX
            b471CloseCursorCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B481-FETCH-FIRST-CPZ  THRU B481-EX
            b481FetchFirstCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B491-FETCH-NEXT-CPZ   THRU B491-EX
            b491FetchNextCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C400-ELABORA-EFF-NS<br>*/
    private void c400ElaboraEffNs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C460-OPEN-CURSOR-EFF-NS  THRU C460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C470-CLOSE-CURSOR-EFF-NS THRU C470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C480-FETCH-FIRST-EFF-NS  THRU C480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C490-FETCH-NEXT-EFF-NS   THRU C490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C460-OPEN-CURSOR-EFF-NS  THRU C460-EX
            c460OpenCursorEffNs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C470-CLOSE-CURSOR-EFF-NS THRU C470-EX
            c470CloseCursorEffNs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C480-FETCH-FIRST-EFF-NS  THRU C480-EX
            c480FetchFirstEffNs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C490-FETCH-NEXT-EFF-NS   THRU C490-EX
            c490FetchNextEffNs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C401-ELABORA-EFF-NS<br>*/
    private void c401ElaboraEffNs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C461-OPEN-CURSOR-EFF-NS  THRU C461-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C471-CLOSE-CURSOR-EFF-NS THRU C471-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C481-FETCH-FIRST-EFF-NS  THRU C481-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C491-FETCH-NEXT-EFF-NS   THRU C491-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C461-OPEN-CURSOR-EFF-NS  THRU C461-EX
            c461OpenCursorEffNs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C471-CLOSE-CURSOR-EFF-NS THRU C471-EX
            c471CloseCursorEffNs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C481-FETCH-FIRST-EFF-NS  THRU C481-EX
            c481FetchFirstEffNs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C491-FETCH-NEXT-EFF-NS   THRU C491-EX
            c491FetchNextEffNs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: D400-ELABORA-CPZ-NS<br>*/
    private void d400ElaboraCpzNs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM D460-OPEN-CURSOR-CPZ-NS  THRU D460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM D470-CLOSE-CURSOR-CPZ-NS THRU D470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM D480-FETCH-FIRST-CPZ-NS  THRU D480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM D490-FETCH-NEXT-CPZ-NS   THRU D490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM D460-OPEN-CURSOR-CPZ-NS  THRU D460-EX
            d460OpenCursorCpzNs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM D470-CLOSE-CURSOR-CPZ-NS THRU D470-EX
            d470CloseCursorCpzNs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM D480-FETCH-FIRST-CPZ-NS  THRU D480-EX
            d480FetchFirstCpzNs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM D490-FETCH-NEXT-CPZ-NS   THRU D490-EX
            d490FetchNextCpzNs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: D401-ELABORA-CPZ-NS<br>*/
    private void d401ElaboraCpzNs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM D461-OPEN-CURSOR-CPZ-NS  THRU D461-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM D471-CLOSE-CURSOR-CPZ-NS THRU D471-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM D481-FETCH-FIRST-CPZ-NS  THRU D481-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM D491-FETCH-NEXT-CPZ-NS   THRU D491-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM D461-OPEN-CURSOR-CPZ-NS  THRU D461-EX
            d461OpenCursorCpzNs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM D471-CLOSE-CURSOR-CPZ-NS THRU D471-EX
            d471CloseCursorCpzNs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM D481-FETCH-FIRST-CPZ-NS  THRU D481-EX
            d481FetchFirstCpzNs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM D491-FETCH-NEXT-CPZ-NS   THRU D491-EX
            d491FetchNextCpzNs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: E400-ELABORA-EFF-NC<br>*/
    private void e400ElaboraEffNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM E460-OPEN-CURSOR-EFF-NC  THRU E460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM E470-CLOSE-CURSOR-EFF-NC THRU E470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM E480-FETCH-FIRST-EFF-NC  THRU E480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM E490-FETCH-NEXT-EFF-NC   THRU E490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM E460-OPEN-CURSOR-EFF-NC  THRU E460-EX
            e460OpenCursorEffNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM E470-CLOSE-CURSOR-EFF-NC THRU E470-EX
            e470CloseCursorEffNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM E480-FETCH-FIRST-EFF-NC  THRU E480-EX
            e480FetchFirstEffNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM E490-FETCH-NEXT-EFF-NC   THRU E490-EX
            e490FetchNextEffNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: E401-ELABORA-EFF-NC<br>*/
    private void e401ElaboraEffNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM E461-OPEN-CURSOR-EFF-NC  THRU E461-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM E471-CLOSE-CURSOR-EFF-NC THRU E471-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM E481-FETCH-FIRST-EFF-NC  THRU E481-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM E491-FETCH-NEXT-EFF-NC   THRU E491-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM E461-OPEN-CURSOR-EFF-NC  THRU E461-EX
            e461OpenCursorEffNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM E471-CLOSE-CURSOR-EFF-NC THRU E471-EX
            e471CloseCursorEffNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM E481-FETCH-FIRST-EFF-NC  THRU E481-EX
            e481FetchFirstEffNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM E491-FETCH-NEXT-EFF-NC   THRU E491-EX
            e491FetchNextEffNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: F400-ELABORA-CPZ-NC<br>*/
    private void f400ElaboraCpzNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM F460-OPEN-CURSOR-CPZ-NC  THRU F460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM F470-CLOSE-CURSOR-CPZ-NC THRU F470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM F480-FETCH-FIRST-CPZ-NC  THRU F480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM F490-FETCH-NEXT-CPZ-NC   THRU F490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM F460-OPEN-CURSOR-CPZ-NC  THRU F460-EX
            f460OpenCursorCpzNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM F470-CLOSE-CURSOR-CPZ-NC THRU F470-EX
            f470CloseCursorCpzNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM F480-FETCH-FIRST-CPZ-NC  THRU F480-EX
            f480FetchFirstCpzNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM F490-FETCH-NEXT-CPZ-NC   THRU F490-EX
            f490FetchNextCpzNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: F401-ELABORA-CPZ-NC<br>*/
    private void f401ElaboraCpzNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM F461-OPEN-CURSOR-CPZ-NC  THRU F461-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM F471-CLOSE-CURSOR-CPZ-NC THRU F471-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM F481-FETCH-FIRST-CPZ-NC  THRU F481-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM F491-FETCH-NEXT-CPZ-NC   THRU F491-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM F461-OPEN-CURSOR-CPZ-NC  THRU F461-EX
            f461OpenCursorCpzNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM F471-CLOSE-CURSOR-CPZ-NC THRU F471-EX
            f471CloseCursorCpzNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM F481-FETCH-FIRST-CPZ-NC  THRU F481-EX
            f481FetchFirstCpzNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM F491-FETCH-NEXT-CPZ-NC   THRU F491-EX
            f491FetchNextCpzNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: G400-ELABORA-EFF-NS-NC<br>*/
    private void g400ElaboraEffNsNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM G460-OPEN-CURSOR-EFF-NS-NC  THRU G460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM G470-CLOSE-CURSOR-EFF-NS-NC THRU G470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM G480-FETCH-FIRST-EFF-NS-NC  THRU G480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM G490-FETCH-NEXT-EFF-NS-NC   THRU G490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM G460-OPEN-CURSOR-EFF-NS-NC  THRU G460-EX
            g460OpenCursorEffNsNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM G470-CLOSE-CURSOR-EFF-NS-NC THRU G470-EX
            g470CloseCursorEffNsNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM G480-FETCH-FIRST-EFF-NS-NC  THRU G480-EX
            g480FetchFirstEffNsNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM G490-FETCH-NEXT-EFF-NS-NC   THRU G490-EX
            g490FetchNextEffNsNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: G401-ELABORA-EFF-NS-NC<br>*/
    private void g401ElaboraEffNsNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM G461-OPEN-CURSOR-EFF-NS-NC  THRU G461-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM G471-CLOSE-CURSOR-EFF-NS-NC THRU G471-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM G481-FETCH-FIRST-EFF-NS-NC  THRU G481-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM G491-FETCH-NEXT-EFF-NS-NC   THRU G491-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM G461-OPEN-CURSOR-EFF-NS-NC  THRU G461-EX
            g461OpenCursorEffNsNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM G471-CLOSE-CURSOR-EFF-NS-NC THRU G471-EX
            g471CloseCursorEffNsNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM G481-FETCH-FIRST-EFF-NS-NC  THRU G481-EX
            g481FetchFirstEffNsNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM G491-FETCH-NEXT-EFF-NS-NC   THRU G491-EX
            g491FetchNextEffNsNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: H400-ELABORA-CPZ-NS-NC<br>*/
    private void h400ElaboraCpzNsNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM H460-OPEN-CURSOR-CPZ-NS-NC  THRU H460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC THRU H470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM H480-FETCH-FIRST-CPZ-NS-NC  THRU H480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM H490-FETCH-NEXT-CPZ-NS-NC   THRU H490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM H460-OPEN-CURSOR-CPZ-NS-NC  THRU H460-EX
            h460OpenCursorCpzNsNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC THRU H470-EX
            h470CloseCursorCpzNsNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM H480-FETCH-FIRST-CPZ-NS-NC  THRU H480-EX
            h480FetchFirstCpzNsNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM H490-FETCH-NEXT-CPZ-NS-NC   THRU H490-EX
            h490FetchNextCpzNsNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: H401-ELABORA-CPZ-NS-NC<br>*/
    private void h401ElaboraCpzNsNc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM H461-OPEN-CURSOR-CPZ-NS-NC  THRU H461-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM H471-CLOSE-CURSOR-CPZ-NS-NC THRU H471-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM H481-FETCH-FIRST-CPZ-NS-NC  THRU H481-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM H491-FETCH-NEXT-CPZ-NS-NC   THRU H491-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM H461-OPEN-CURSOR-CPZ-NS-NC  THRU H461-EX
            h461OpenCursorCpzNsNc();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM H471-CLOSE-CURSOR-CPZ-NS-NC THRU H471-EX
            h471CloseCursorCpzNsNc();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM H481-FETCH-FIRST-CPZ-NS-NC  THRU H481-EX
            h481FetchFirstCpzNsNc();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM H491-FETCH-NEXT-CPZ-NS-NC   THRU H491-EX
            h491FetchNextCpzNsNc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A405-DECLARE-CURSOR-EFF<br>
	 * <pre>----
	 * ----  gestione Effetto STATO / CAUSALE
	 * ----</pre>*/
    private void a405DeclareCursorEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-TGA CURSOR FOR
        //              SELECT
        //                 A.ID_TRCH_DI_GAR
        //                ,A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.IB_OGG
        //                ,A.TP_RGM_FISC
        //                ,A.DT_EMIS
        //                ,A.TP_TRCH
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.PRE_CASO_MOR
        //                ,A.PC_INTR_RIAT
        //                ,A.IMP_BNS_ANTIC
        //                ,A.PRE_INI_NET
        //                ,A.PRE_PP_INI
        //                ,A.PRE_PP_ULT
        //                ,A.PRE_TARI_INI
        //                ,A.PRE_TARI_ULT
        //                ,A.PRE_INVRIO_INI
        //                ,A.PRE_INVRIO_ULT
        //                ,A.PRE_RIVTO
        //                ,A.IMP_SOPR_PROF
        //                ,A.IMP_SOPR_SAN
        //                ,A.IMP_SOPR_SPO
        //                ,A.IMP_SOPR_TEC
        //                ,A.IMP_ALT_SOPR
        //                ,A.PRE_STAB
        //                ,A.DT_EFF_STAB
        //                ,A.TS_RIVAL_FIS
        //                ,A.TS_RIVAL_INDICIZ
        //                ,A.OLD_TS_TEC
        //                ,A.RAT_LRD
        //                ,A.PRE_LRD
        //                ,A.PRSTZ_INI
        //                ,A.PRSTZ_ULT
        //                ,A.CPT_IN_OPZ_RIVTO
        //                ,A.PRSTZ_INI_STAB
        //                ,A.CPT_RSH_MOR
        //                ,A.PRSTZ_RID_INI
        //                ,A.FL_CAR_CONT
        //                ,A.BNS_GIA_LIQTO
        //                ,A.IMP_BNS
        //                ,A.COD_DVS
        //                ,A.PRSTZ_INI_NEWFIS
        //                ,A.IMP_SCON
        //                ,A.ALQ_SCON
        //                ,A.IMP_CAR_ACQ
        //                ,A.IMP_CAR_INC
        //                ,A.IMP_CAR_GEST
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.RENDTO_LRD
        //                ,A.PC_RETR
        //                ,A.RENDTO_RETR
        //                ,A.MIN_GARTO
        //                ,A.MIN_TRNUT
        //                ,A.PRE_ATT_DI_TRCH
        //                ,A.MATU_END2000
        //                ,A.ABB_TOT_INI
        //                ,A.ABB_TOT_ULT
        //                ,A.ABB_ANNU_ULT
        //                ,A.DUR_ABB
        //                ,A.TP_ADEG_ABB
        //                ,A.MOD_CALC
        //                ,A.IMP_AZ
        //                ,A.IMP_ADER
        //                ,A.IMP_TFR
        //                ,A.IMP_VOLO
        //                ,A.VIS_END2000
        //                ,A.DT_VLDT_PROD
        //                ,A.DT_INI_VAL_TAR
        //                ,A.IMPB_VIS_END2000
        //                ,A.REN_INI_TS_TEC_0
        //                ,A.PC_RIP_PRE
        //                ,A.FL_IMPORTI_FORZ
        //                ,A.PRSTZ_INI_NFORZ
        //                ,A.VIS_END2000_NFORZ
        //                ,A.INTR_MORA
        //                ,A.MANFEE_ANTIC
        //                ,A.MANFEE_RICOR
        //                ,A.PRE_UNI_RIVTO
        //                ,A.PROV_1AA_ACQ
        //                ,A.PROV_2AA_ACQ
        //                ,A.PROV_RICOR
        //                ,A.PROV_INC
        //                ,A.ALQ_PROV_ACQ
        //                ,A.ALQ_PROV_INC
        //                ,A.ALQ_PROV_RICOR
        //                ,A.IMPB_PROV_ACQ
        //                ,A.IMPB_PROV_INC
        //                ,A.IMPB_PROV_RICOR
        //                ,A.FL_PROV_FORZ
        //                ,A.PRSTZ_AGG_INI
        //                ,A.INCR_PRE
        //                ,A.INCR_PRSTZ
        //                ,A.DT_ULT_ADEG_PRE_PR
        //                ,A.PRSTZ_AGG_ULT
        //                ,A.TS_RIVAL_NET
        //                ,A.PRE_PATTUITO
        //                ,A.TP_RIVAL
        //                ,A.RIS_MAT
        //                ,A.CPT_MIN_SCAD
        //                ,A.COMMIS_GEST
        //                ,A.TP_MANFEE_APPL
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.PC_COMMIS_GEST
        //                ,A.NUM_GG_RIVAL
        //                ,A.IMP_TRASFE
        //                ,A.IMP_TFR_STRC
        //                ,A.ACQ_EXP
        //                ,A.REMUN_ASS
        //                ,A.COMMIS_INTER
        //                ,A.ALQ_REMUN_ASS
        //                ,A.ALQ_COMMIS_INTER
        //                ,A.IMPB_REMUN_ASS
        //                ,A.IMPB_COMMIS_INTER
        //                ,A.COS_RUN_ASSVA
        //                ,A.COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_TRCH_DI_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: A460-OPEN-CURSOR-EFF<br>*/
    private void a460OpenCursorEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-EFF THRU A405-EX.
        a405DeclareCursorEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-TGA
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCEffTga(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-EFF<br>*/
    private void a470CloseCursorEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-TGA
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCEffTga();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-EFF<br>*/
    private void a480FetchFirstEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-EFF  THRU A460-EX.
        a460OpenCursorEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-EFF THRU A490-EX
            a490FetchNextEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-EFF<br>*/
    private void a490FetchNextEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-TGA
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCEffTga(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-EFF THRU A470-EX
            a470CloseCursorEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A406-DECLARE-CURSOR-EFF<br>
	 * <pre>----
	 * ----  gestione WH Effetto STATO / CAUSALE
	 * ----</pre>*/
    private void a406DeclareCursorEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-TGA-DI CURSOR FOR
        //              SELECT DISTINCT(A.DT_VLDT_PROD)
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.DT_VLDT_PROD ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: A461-OPEN-CURSOR-EFF<br>*/
    private void a461OpenCursorEff() {
        // COB_CODE: PERFORM A406-DECLARE-CURSOR-EFF THRU A406-EX.
        a406DeclareCursorEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-TGA-DI
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCEffTgaDi(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A471-CLOSE-CURSOR-EFF<br>*/
    private void a471CloseCursorEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-TGA-DI
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCEffTgaDi();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A481-FETCH-FIRST-EFF<br>*/
    private void a481FetchFirstEff() {
        // COB_CODE: PERFORM A461-OPEN-CURSOR-EFF  THRU A461-EX.
        a461OpenCursorEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A491-FETCH-NEXT-EFF THRU A491-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A491-FETCH-NEXT-EFF THRU A491-EX
            a491FetchNextEff();
        }
    }

    /**Original name: A491-FETCH-NEXT-EFF<br>*/
    private void a491FetchNextEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-TGA-DI
        //             INTO
        //                  :TGA-DT-VLDT-PROD-DB
        //                  :IND-TGA-DT-VLDT-PROD
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCEffTgaDi(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A471-CLOSE-CURSOR-EFF THRU A471-EX
            a471CloseCursorEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B405-DECLARE-CURSOR-CPZ<br>
	 * <pre>----
	 * ----  gestione FA Competenza STATO / CAUSALE
	 * ----</pre>*/
    private void b405DeclareCursorCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-TGA CURSOR FOR
        //              SELECT
        //                 A.ID_TRCH_DI_GAR
        //                ,A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.IB_OGG
        //                ,A.TP_RGM_FISC
        //                ,A.DT_EMIS
        //                ,A.TP_TRCH
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.PRE_CASO_MOR
        //                ,A.PC_INTR_RIAT
        //                ,A.IMP_BNS_ANTIC
        //                ,A.PRE_INI_NET
        //                ,A.PRE_PP_INI
        //                ,A.PRE_PP_ULT
        //                ,A.PRE_TARI_INI
        //                ,A.PRE_TARI_ULT
        //                ,A.PRE_INVRIO_INI
        //                ,A.PRE_INVRIO_ULT
        //                ,A.PRE_RIVTO
        //                ,A.IMP_SOPR_PROF
        //                ,A.IMP_SOPR_SAN
        //                ,A.IMP_SOPR_SPO
        //                ,A.IMP_SOPR_TEC
        //                ,A.IMP_ALT_SOPR
        //                ,A.PRE_STAB
        //                ,A.DT_EFF_STAB
        //                ,A.TS_RIVAL_FIS
        //                ,A.TS_RIVAL_INDICIZ
        //                ,A.OLD_TS_TEC
        //                ,A.RAT_LRD
        //                ,A.PRE_LRD
        //                ,A.PRSTZ_INI
        //                ,A.PRSTZ_ULT
        //                ,A.CPT_IN_OPZ_RIVTO
        //                ,A.PRSTZ_INI_STAB
        //                ,A.CPT_RSH_MOR
        //                ,A.PRSTZ_RID_INI
        //                ,A.FL_CAR_CONT
        //                ,A.BNS_GIA_LIQTO
        //                ,A.IMP_BNS
        //                ,A.COD_DVS
        //                ,A.PRSTZ_INI_NEWFIS
        //                ,A.IMP_SCON
        //                ,A.ALQ_SCON
        //                ,A.IMP_CAR_ACQ
        //                ,A.IMP_CAR_INC
        //                ,A.IMP_CAR_GEST
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.RENDTO_LRD
        //                ,A.PC_RETR
        //                ,A.RENDTO_RETR
        //                ,A.MIN_GARTO
        //                ,A.MIN_TRNUT
        //                ,A.PRE_ATT_DI_TRCH
        //                ,A.MATU_END2000
        //                ,A.ABB_TOT_INI
        //                ,A.ABB_TOT_ULT
        //                ,A.ABB_ANNU_ULT
        //                ,A.DUR_ABB
        //                ,A.TP_ADEG_ABB
        //                ,A.MOD_CALC
        //                ,A.IMP_AZ
        //                ,A.IMP_ADER
        //                ,A.IMP_TFR
        //                ,A.IMP_VOLO
        //                ,A.VIS_END2000
        //                ,A.DT_VLDT_PROD
        //                ,A.DT_INI_VAL_TAR
        //                ,A.IMPB_VIS_END2000
        //                ,A.REN_INI_TS_TEC_0
        //                ,A.PC_RIP_PRE
        //                ,A.FL_IMPORTI_FORZ
        //                ,A.PRSTZ_INI_NFORZ
        //                ,A.VIS_END2000_NFORZ
        //                ,A.INTR_MORA
        //                ,A.MANFEE_ANTIC
        //                ,A.MANFEE_RICOR
        //                ,A.PRE_UNI_RIVTO
        //                ,A.PROV_1AA_ACQ
        //                ,A.PROV_2AA_ACQ
        //                ,A.PROV_RICOR
        //                ,A.PROV_INC
        //                ,A.ALQ_PROV_ACQ
        //                ,A.ALQ_PROV_INC
        //                ,A.ALQ_PROV_RICOR
        //                ,A.IMPB_PROV_ACQ
        //                ,A.IMPB_PROV_INC
        //                ,A.IMPB_PROV_RICOR
        //                ,A.FL_PROV_FORZ
        //                ,A.PRSTZ_AGG_INI
        //                ,A.INCR_PRE
        //                ,A.INCR_PRSTZ
        //                ,A.DT_ULT_ADEG_PRE_PR
        //                ,A.PRSTZ_AGG_ULT
        //                ,A.TS_RIVAL_NET
        //                ,A.PRE_PATTUITO
        //                ,A.TP_RIVAL
        //                ,A.RIS_MAT
        //                ,A.CPT_MIN_SCAD
        //                ,A.COMMIS_GEST
        //                ,A.TP_MANFEE_APPL
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.PC_COMMIS_GEST
        //                ,A.NUM_GG_RIVAL
        //                ,A.IMP_TRASFE
        //                ,A.IMP_TFR_STRC
        //                ,A.ACQ_EXP
        //                ,A.REMUN_ASS
        //                ,A.COMMIS_INTER
        //                ,A.ALQ_REMUN_ASS
        //                ,A.ALQ_COMMIS_INTER
        //                ,A.IMPB_REMUN_ASS
        //                ,A.IMPB_COMMIS_INTER
        //                ,A.COS_RUN_ASSVA
        //                ,A.COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_TRCH_DI_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: B460-OPEN-CURSOR-CPZ<br>*/
    private void b460OpenCursorCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-CPZ THRU B405-EX.
        b405DeclareCursorCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-TGA
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCCpzTga(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-CPZ<br>*/
    private void b470CloseCursorCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-TGA
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCCpzTga();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-CPZ<br>*/
    private void b480FetchFirstCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-CPZ    THRU B460-EX.
        b460OpenCursorCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-CPZ THRU B490-EX
            b490FetchNextCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-CPZ<br>*/
    private void b490FetchNextCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-TGA
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCCpzTga(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-CPZ THRU B470-EX
            b470CloseCursorCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B406-DECLARE-CURSOR-CPZ<br>
	 * <pre>----
	 * ----  gestione WH Competenza STATO / CAUSALE
	 * ----</pre>*/
    private void b406DeclareCursorCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-TGA-DI CURSOR FOR
        //              SELECT DISTINCT(A.DT_VLDT_PROD)
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.DT_VLDT_PROD ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: B461-OPEN-CURSOR-CPZ<br>*/
    private void b461OpenCursorCpz() {
        // COB_CODE: PERFORM B406-DECLARE-CURSOR-CPZ THRU B406-EX.
        b406DeclareCursorCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-TGA-DI
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCCpzTgaDi(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B471-CLOSE-CURSOR-CPZ<br>*/
    private void b471CloseCursorCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-TGA-DI
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCCpzTgaDi();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B481-FETCH-FIRST-CPZ<br>*/
    private void b481FetchFirstCpz() {
        // COB_CODE: PERFORM B461-OPEN-CURSOR-CPZ    THRU B461-EX.
        b461OpenCursorCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B491-FETCH-NEXT-CPZ THRU B491-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B491-FETCH-NEXT-CPZ THRU B491-EX
            b491FetchNextCpz();
        }
    }

    /**Original name: B491-FETCH-NEXT-CPZ<br>*/
    private void b491FetchNextCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-TGA-DI
        //                INTO
        //                  :TGA-DT-VLDT-PROD-DB
        //                  :IND-TGA-DT-VLDT-PROD
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCCpzTgaDi(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B471-CLOSE-CURSOR-CPZ THRU B471-EX
            b471CloseCursorCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C405-DECLARE-CURSOR-EFF-NS<br>
	 * <pre>----
	 * ----  gestione Effetto NOT STATO / CAUSALE
	 * ----</pre>*/
    private void c405DeclareCursorEffNs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-TGA-NS CURSOR FOR
        //              SELECT
        //                 A.ID_TRCH_DI_GAR
        //                ,A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.IB_OGG
        //                ,A.TP_RGM_FISC
        //                ,A.DT_EMIS
        //                ,A.TP_TRCH
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.PRE_CASO_MOR
        //                ,A.PC_INTR_RIAT
        //                ,A.IMP_BNS_ANTIC
        //                ,A.PRE_INI_NET
        //                ,A.PRE_PP_INI
        //                ,A.PRE_PP_ULT
        //                ,A.PRE_TARI_INI
        //                ,A.PRE_TARI_ULT
        //                ,A.PRE_INVRIO_INI
        //                ,A.PRE_INVRIO_ULT
        //                ,A.PRE_RIVTO
        //                ,A.IMP_SOPR_PROF
        //                ,A.IMP_SOPR_SAN
        //                ,A.IMP_SOPR_SPO
        //                ,A.IMP_SOPR_TEC
        //                ,A.IMP_ALT_SOPR
        //                ,A.PRE_STAB
        //                ,A.DT_EFF_STAB
        //                ,A.TS_RIVAL_FIS
        //                ,A.TS_RIVAL_INDICIZ
        //                ,A.OLD_TS_TEC
        //                ,A.RAT_LRD
        //                ,A.PRE_LRD
        //                ,A.PRSTZ_INI
        //                ,A.PRSTZ_ULT
        //                ,A.CPT_IN_OPZ_RIVTO
        //                ,A.PRSTZ_INI_STAB
        //                ,A.CPT_RSH_MOR
        //                ,A.PRSTZ_RID_INI
        //                ,A.FL_CAR_CONT
        //                ,A.BNS_GIA_LIQTO
        //                ,A.IMP_BNS
        //                ,A.COD_DVS
        //                ,A.PRSTZ_INI_NEWFIS
        //                ,A.IMP_SCON
        //                ,A.ALQ_SCON
        //                ,A.IMP_CAR_ACQ
        //                ,A.IMP_CAR_INC
        //                ,A.IMP_CAR_GEST
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.RENDTO_LRD
        //                ,A.PC_RETR
        //                ,A.RENDTO_RETR
        //                ,A.MIN_GARTO
        //                ,A.MIN_TRNUT
        //                ,A.PRE_ATT_DI_TRCH
        //                ,A.MATU_END2000
        //                ,A.ABB_TOT_INI
        //                ,A.ABB_TOT_ULT
        //                ,A.ABB_ANNU_ULT
        //                ,A.DUR_ABB
        //                ,A.TP_ADEG_ABB
        //                ,A.MOD_CALC
        //                ,A.IMP_AZ
        //                ,A.IMP_ADER
        //                ,A.IMP_TFR
        //                ,A.IMP_VOLO
        //                ,A.VIS_END2000
        //                ,A.DT_VLDT_PROD
        //                ,A.DT_INI_VAL_TAR
        //                ,A.IMPB_VIS_END2000
        //                ,A.REN_INI_TS_TEC_0
        //                ,A.PC_RIP_PRE
        //                ,A.FL_IMPORTI_FORZ
        //                ,A.PRSTZ_INI_NFORZ
        //                ,A.VIS_END2000_NFORZ
        //                ,A.INTR_MORA
        //                ,A.MANFEE_ANTIC
        //                ,A.MANFEE_RICOR
        //                ,A.PRE_UNI_RIVTO
        //                ,A.PROV_1AA_ACQ
        //                ,A.PROV_2AA_ACQ
        //                ,A.PROV_RICOR
        //                ,A.PROV_INC
        //                ,A.ALQ_PROV_ACQ
        //                ,A.ALQ_PROV_INC
        //                ,A.ALQ_PROV_RICOR
        //                ,A.IMPB_PROV_ACQ
        //                ,A.IMPB_PROV_INC
        //                ,A.IMPB_PROV_RICOR
        //                ,A.FL_PROV_FORZ
        //                ,A.PRSTZ_AGG_INI
        //                ,A.INCR_PRE
        //                ,A.INCR_PRSTZ
        //                ,A.DT_ULT_ADEG_PRE_PR
        //                ,A.PRSTZ_AGG_ULT
        //                ,A.TS_RIVAL_NET
        //                ,A.PRE_PATTUITO
        //                ,A.TP_RIVAL
        //                ,A.RIS_MAT
        //                ,A.CPT_MIN_SCAD
        //                ,A.COMMIS_GEST
        //                ,A.TP_MANFEE_APPL
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.PC_COMMIS_GEST
        //                ,A.NUM_GG_RIVAL
        //                ,A.IMP_TRASFE
        //                ,A.IMP_TFR_STRC
        //                ,A.ACQ_EXP
        //                ,A.REMUN_ASS
        //                ,A.COMMIS_INTER
        //                ,A.ALQ_REMUN_ASS
        //                ,A.ALQ_COMMIS_INTER
        //                ,A.IMPB_REMUN_ASS
        //                ,A.IMPB_COMMIS_INTER
        //                ,A.COS_RUN_ASSVA
        //                ,A.COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_TRCH_DI_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: C460-OPEN-CURSOR-EFF-NS<br>*/
    private void c460OpenCursorEffNs() {
        // COB_CODE: PERFORM C405-DECLARE-CURSOR-EFF-NS THRU C405-EX.
        c405DeclareCursorEffNs();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-TGA-NS
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCEffTgaNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C470-CLOSE-CURSOR-EFF-NS<br>*/
    private void c470CloseCursorEffNs() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-TGA-NS
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCEffTgaNs();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C480-FETCH-FIRST-EFF-NS<br>*/
    private void c480FetchFirstEffNs() {
        // COB_CODE: PERFORM C460-OPEN-CURSOR-EFF-NS  THRU C460-EX.
        c460OpenCursorEffNs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C490-FETCH-NEXT-EFF-NS THRU C490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C490-FETCH-NEXT-EFF-NS THRU C490-EX
            c490FetchNextEffNs();
        }
    }

    /**Original name: C490-FETCH-NEXT-EFF-NS<br>*/
    private void c490FetchNextEffNs() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-TGA-NS
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCEffTgaNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM C470-CLOSE-CURSOR-EFF-NS THRU C470-EX
            c470CloseCursorEffNs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C406-DECLARE-CURSOR-EFF-NS<br>
	 * <pre>----
	 * ----  gestione WH Effetto NOT STATO / CAUSALE
	 * ----</pre>*/
    private void c406DeclareCursorEffNs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-TGA-DI-NS CURSOR FOR
        //              SELECT DISTINCT(A.DT_VLDT_PROD)
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.DT_VLDT_PROD ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: C461-OPEN-CURSOR-EFF-NS<br>*/
    private void c461OpenCursorEffNs() {
        // COB_CODE: PERFORM C406-DECLARE-CURSOR-EFF-NS THRU C406-EX.
        c406DeclareCursorEffNs();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-TGA-DI-NS
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCEffTgaDiNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C471-CLOSE-CURSOR-EFF-NS<br>*/
    private void c471CloseCursorEffNs() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-TGA-DI-NS
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCEffTgaDiNs();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C481-FETCH-FIRST-EFF-NS<br>*/
    private void c481FetchFirstEffNs() {
        // COB_CODE: PERFORM C461-OPEN-CURSOR-EFF-NS  THRU C461-EX.
        c461OpenCursorEffNs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C491-FETCH-NEXT-EFF-NS THRU C491-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C491-FETCH-NEXT-EFF-NS THRU C491-EX
            c491FetchNextEffNs();
        }
    }

    /**Original name: C491-FETCH-NEXT-EFF-NS<br>*/
    private void c491FetchNextEffNs() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-TGA-DI-NS
        //             INTO
        //                  :TGA-DT-VLDT-PROD-DB
        //                  :IND-TGA-DT-VLDT-PROD
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCEffTgaDiNs(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM C471-CLOSE-CURSOR-EFF-NS THRU C471-EX
            c471CloseCursorEffNs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: D405-DECLARE-CURSOR-CPZ-NS<br>
	 * <pre>----
	 * ----  gestione FA Competenza NOT STATO / CAUSALE
	 * ----</pre>*/
    private void d405DeclareCursorCpzNs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-TGA-NS CURSOR FOR
        //              SELECT
        //                 A.ID_TRCH_DI_GAR
        //                ,A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.IB_OGG
        //                ,A.TP_RGM_FISC
        //                ,A.DT_EMIS
        //                ,A.TP_TRCH
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.PRE_CASO_MOR
        //                ,A.PC_INTR_RIAT
        //                ,A.IMP_BNS_ANTIC
        //                ,A.PRE_INI_NET
        //                ,A.PRE_PP_INI
        //                ,A.PRE_PP_ULT
        //                ,A.PRE_TARI_INI
        //                ,A.PRE_TARI_ULT
        //                ,A.PRE_INVRIO_INI
        //                ,A.PRE_INVRIO_ULT
        //                ,A.PRE_RIVTO
        //                ,A.IMP_SOPR_PROF
        //                ,A.IMP_SOPR_SAN
        //                ,A.IMP_SOPR_SPO
        //                ,A.IMP_SOPR_TEC
        //                ,A.IMP_ALT_SOPR
        //                ,A.PRE_STAB
        //                ,A.DT_EFF_STAB
        //                ,A.TS_RIVAL_FIS
        //                ,A.TS_RIVAL_INDICIZ
        //                ,A.OLD_TS_TEC
        //                ,A.RAT_LRD
        //                ,A.PRE_LRD
        //                ,A.PRSTZ_INI
        //                ,A.PRSTZ_ULT
        //                ,A.CPT_IN_OPZ_RIVTO
        //                ,A.PRSTZ_INI_STAB
        //                ,A.CPT_RSH_MOR
        //                ,A.PRSTZ_RID_INI
        //                ,A.FL_CAR_CONT
        //                ,A.BNS_GIA_LIQTO
        //                ,A.IMP_BNS
        //                ,A.COD_DVS
        //                ,A.PRSTZ_INI_NEWFIS
        //                ,A.IMP_SCON
        //                ,A.ALQ_SCON
        //                ,A.IMP_CAR_ACQ
        //                ,A.IMP_CAR_INC
        //                ,A.IMP_CAR_GEST
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.RENDTO_LRD
        //                ,A.PC_RETR
        //                ,A.RENDTO_RETR
        //                ,A.MIN_GARTO
        //                ,A.MIN_TRNUT
        //                ,A.PRE_ATT_DI_TRCH
        //                ,A.MATU_END2000
        //                ,A.ABB_TOT_INI
        //                ,A.ABB_TOT_ULT
        //                ,A.ABB_ANNU_ULT
        //                ,A.DUR_ABB
        //                ,A.TP_ADEG_ABB
        //                ,A.MOD_CALC
        //                ,A.IMP_AZ
        //                ,A.IMP_ADER
        //                ,A.IMP_TFR
        //                ,A.IMP_VOLO
        //                ,A.VIS_END2000
        //                ,A.DT_VLDT_PROD
        //                ,A.DT_INI_VAL_TAR
        //                ,A.IMPB_VIS_END2000
        //                ,A.REN_INI_TS_TEC_0
        //                ,A.PC_RIP_PRE
        //                ,A.FL_IMPORTI_FORZ
        //                ,A.PRSTZ_INI_NFORZ
        //                ,A.VIS_END2000_NFORZ
        //                ,A.INTR_MORA
        //                ,A.MANFEE_ANTIC
        //                ,A.MANFEE_RICOR
        //                ,A.PRE_UNI_RIVTO
        //                ,A.PROV_1AA_ACQ
        //                ,A.PROV_2AA_ACQ
        //                ,A.PROV_RICOR
        //                ,A.PROV_INC
        //                ,A.ALQ_PROV_ACQ
        //                ,A.ALQ_PROV_INC
        //                ,A.ALQ_PROV_RICOR
        //                ,A.IMPB_PROV_ACQ
        //                ,A.IMPB_PROV_INC
        //                ,A.IMPB_PROV_RICOR
        //                ,A.FL_PROV_FORZ
        //                ,A.PRSTZ_AGG_INI
        //                ,A.INCR_PRE
        //                ,A.INCR_PRSTZ
        //                ,A.DT_ULT_ADEG_PRE_PR
        //                ,A.PRSTZ_AGG_ULT
        //                ,A.TS_RIVAL_NET
        //                ,A.PRE_PATTUITO
        //                ,A.TP_RIVAL
        //                ,A.RIS_MAT
        //                ,A.CPT_MIN_SCAD
        //                ,A.COMMIS_GEST
        //                ,A.TP_MANFEE_APPL
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.PC_COMMIS_GEST
        //                ,A.NUM_GG_RIVAL
        //                ,A.IMP_TRASFE
        //                ,A.IMP_TFR_STRC
        //                ,A.ACQ_EXP
        //                ,A.REMUN_ASS
        //                ,A.COMMIS_INTER
        //                ,A.ALQ_REMUN_ASS
        //                ,A.ALQ_COMMIS_INTER
        //                ,A.IMPB_REMUN_ASS
        //                ,A.IMPB_COMMIS_INTER
        //                ,A.COS_RUN_ASSVA
        //                ,A.COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_TRCH_DI_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: D460-OPEN-CURSOR-CPZ-NS<br>*/
    private void d460OpenCursorCpzNs() {
        // COB_CODE: PERFORM D405-DECLARE-CURSOR-CPZ-NS THRU D405-EX.
        d405DeclareCursorCpzNs();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-TGA-NS
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCCpzTgaNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: D470-CLOSE-CURSOR-CPZ-NS<br>*/
    private void d470CloseCursorCpzNs() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-TGA-NS
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCCpzTgaNs();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: D480-FETCH-FIRST-CPZ-NS<br>*/
    private void d480FetchFirstCpzNs() {
        // COB_CODE: PERFORM D460-OPEN-CURSOR-CPZ-NS    THRU D460-EX.
        d460OpenCursorCpzNs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM D490-FETCH-NEXT-CPZ-NS THRU D490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM D490-FETCH-NEXT-CPZ-NS THRU D490-EX
            d490FetchNextCpzNs();
        }
    }

    /**Original name: D490-FETCH-NEXT-CPZ-NS<br>*/
    private void d490FetchNextCpzNs() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-TGA-NS
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCCpzTgaNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM D470-CLOSE-CURSOR-CPZ-NS THRU D470-EX
            d470CloseCursorCpzNs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: D406-DECLARE-CURSOR-CPZ-NS<br>
	 * <pre>----
	 * ----  gestione WH Competenza NOT STATO / CAUSALE
	 * ----</pre>*/
    private void d406DeclareCursorCpzNs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-TGA-DI-NS CURSOR FOR
        //              SELECT DISTINCT(A.DT_VLDT_PROD)
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.DT_VLDT_PROD ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: D461-OPEN-CURSOR-CPZ-NS<br>*/
    private void d461OpenCursorCpzNs() {
        // COB_CODE: PERFORM D406-DECLARE-CURSOR-CPZ-NS THRU D406-EX.
        d406DeclareCursorCpzNs();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-TGA-DI-NS
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCCpzTgaDiNs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: D471-CLOSE-CURSOR-CPZ-NS<br>*/
    private void d471CloseCursorCpzNs() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-TGA-DI-NS
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCCpzTgaDiNs();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: D481-FETCH-FIRST-CPZ-NS<br>*/
    private void d481FetchFirstCpzNs() {
        // COB_CODE: PERFORM D461-OPEN-CURSOR-CPZ-NS    THRU D461-EX.
        d461OpenCursorCpzNs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM D491-FETCH-NEXT-CPZ-NS THRU D491-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM D491-FETCH-NEXT-CPZ-NS THRU D491-EX
            d491FetchNextCpzNs();
        }
    }

    /**Original name: D491-FETCH-NEXT-CPZ-NS<br>*/
    private void d491FetchNextCpzNs() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-TGA-DI-NS
        //                INTO
        //                  :TGA-DT-VLDT-PROD-DB
        //                  :IND-TGA-DT-VLDT-PROD
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCCpzTgaDiNs(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM D471-CLOSE-CURSOR-CPZ-NS THRU D471-EX
            d471CloseCursorCpzNs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: E405-DECLARE-CURSOR-EFF-NC<br>
	 * <pre>----
	 * ----  gestione Effetto STATO / NOT CAUSALE
	 * ----</pre>*/
    private void e405DeclareCursorEffNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-TGA-NC CURSOR FOR
        //              SELECT
        //                 A.ID_TRCH_DI_GAR
        //                ,A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.IB_OGG
        //                ,A.TP_RGM_FISC
        //                ,A.DT_EMIS
        //                ,A.TP_TRCH
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.PRE_CASO_MOR
        //                ,A.PC_INTR_RIAT
        //                ,A.IMP_BNS_ANTIC
        //                ,A.PRE_INI_NET
        //                ,A.PRE_PP_INI
        //                ,A.PRE_PP_ULT
        //                ,A.PRE_TARI_INI
        //                ,A.PRE_TARI_ULT
        //                ,A.PRE_INVRIO_INI
        //                ,A.PRE_INVRIO_ULT
        //                ,A.PRE_RIVTO
        //                ,A.IMP_SOPR_PROF
        //                ,A.IMP_SOPR_SAN
        //                ,A.IMP_SOPR_SPO
        //                ,A.IMP_SOPR_TEC
        //                ,A.IMP_ALT_SOPR
        //                ,A.PRE_STAB
        //                ,A.DT_EFF_STAB
        //                ,A.TS_RIVAL_FIS
        //                ,A.TS_RIVAL_INDICIZ
        //                ,A.OLD_TS_TEC
        //                ,A.RAT_LRD
        //                ,A.PRE_LRD
        //                ,A.PRSTZ_INI
        //                ,A.PRSTZ_ULT
        //                ,A.CPT_IN_OPZ_RIVTO
        //                ,A.PRSTZ_INI_STAB
        //                ,A.CPT_RSH_MOR
        //                ,A.PRSTZ_RID_INI
        //                ,A.FL_CAR_CONT
        //                ,A.BNS_GIA_LIQTO
        //                ,A.IMP_BNS
        //                ,A.COD_DVS
        //                ,A.PRSTZ_INI_NEWFIS
        //                ,A.IMP_SCON
        //                ,A.ALQ_SCON
        //                ,A.IMP_CAR_ACQ
        //                ,A.IMP_CAR_INC
        //                ,A.IMP_CAR_GEST
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.RENDTO_LRD
        //                ,A.PC_RETR
        //                ,A.RENDTO_RETR
        //                ,A.MIN_GARTO
        //                ,A.MIN_TRNUT
        //                ,A.PRE_ATT_DI_TRCH
        //                ,A.MATU_END2000
        //                ,A.ABB_TOT_INI
        //                ,A.ABB_TOT_ULT
        //                ,A.ABB_ANNU_ULT
        //                ,A.DUR_ABB
        //                ,A.TP_ADEG_ABB
        //                ,A.MOD_CALC
        //                ,A.IMP_AZ
        //                ,A.IMP_ADER
        //                ,A.IMP_TFR
        //                ,A.IMP_VOLO
        //                ,A.VIS_END2000
        //                ,A.DT_VLDT_PROD
        //                ,A.DT_INI_VAL_TAR
        //                ,A.IMPB_VIS_END2000
        //                ,A.REN_INI_TS_TEC_0
        //                ,A.PC_RIP_PRE
        //                ,A.FL_IMPORTI_FORZ
        //                ,A.PRSTZ_INI_NFORZ
        //                ,A.VIS_END2000_NFORZ
        //                ,A.INTR_MORA
        //                ,A.MANFEE_ANTIC
        //                ,A.MANFEE_RICOR
        //                ,A.PRE_UNI_RIVTO
        //                ,A.PROV_1AA_ACQ
        //                ,A.PROV_2AA_ACQ
        //                ,A.PROV_RICOR
        //                ,A.PROV_INC
        //                ,A.ALQ_PROV_ACQ
        //                ,A.ALQ_PROV_INC
        //                ,A.ALQ_PROV_RICOR
        //                ,A.IMPB_PROV_ACQ
        //                ,A.IMPB_PROV_INC
        //                ,A.IMPB_PROV_RICOR
        //                ,A.FL_PROV_FORZ
        //                ,A.PRSTZ_AGG_INI
        //                ,A.INCR_PRE
        //                ,A.INCR_PRSTZ
        //                ,A.DT_ULT_ADEG_PRE_PR
        //                ,A.PRSTZ_AGG_ULT
        //                ,A.TS_RIVAL_NET
        //                ,A.PRE_PATTUITO
        //                ,A.TP_RIVAL
        //                ,A.RIS_MAT
        //                ,A.CPT_MIN_SCAD
        //                ,A.COMMIS_GEST
        //                ,A.TP_MANFEE_APPL
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.PC_COMMIS_GEST
        //                ,A.NUM_GG_RIVAL
        //                ,A.IMP_TRASFE
        //                ,A.IMP_TFR_STRC
        //                ,A.ACQ_EXP
        //                ,A.REMUN_ASS
        //                ,A.COMMIS_INTER
        //                ,A.ALQ_REMUN_ASS
        //                ,A.ALQ_COMMIS_INTER
        //                ,A.IMPB_REMUN_ASS
        //                ,A.IMPB_COMMIS_INTER
        //                ,A.COS_RUN_ASSVA
        //                ,A.COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_TRCH_DI_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: E460-OPEN-CURSOR-EFF-NC<br>*/
    private void e460OpenCursorEffNc() {
        // COB_CODE: PERFORM E405-DECLARE-CURSOR-EFF-NC THRU E405-EX.
        e405DeclareCursorEffNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-TGA-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCEffTgaNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: E470-CLOSE-CURSOR-EFF-NC<br>*/
    private void e470CloseCursorEffNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-TGA-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCEffTgaNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: E480-FETCH-FIRST-EFF-NC<br>*/
    private void e480FetchFirstEffNc() {
        // COB_CODE: PERFORM E460-OPEN-CURSOR-EFF-NC  THRU E460-EX.
        e460OpenCursorEffNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM E490-FETCH-NEXT-EFF-NC THRU E490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM E490-FETCH-NEXT-EFF-NC THRU E490-EX
            e490FetchNextEffNc();
        }
    }

    /**Original name: E490-FETCH-NEXT-EFF-NC<br>*/
    private void e490FetchNextEffNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-TGA-NC
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCEffTgaNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM E470-CLOSE-CURSOR-EFF-NC THRU E470-EX
            e470CloseCursorEffNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: E406-DECLARE-CURSOR-EFF-NC<br>
	 * <pre>----
	 * ----  gestione WH Effetto STATO / NOT CAUSALE
	 * ----</pre>*/
    private void e406DeclareCursorEffNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-TGA-DI-NC CURSOR FOR
        //              SELECT DISTINCT(A.DT_VLDT_PROD)
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.DT_VLDT_PROD ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: E461-OPEN-CURSOR-EFF-NC<br>*/
    private void e461OpenCursorEffNc() {
        // COB_CODE: PERFORM E406-DECLARE-CURSOR-EFF-NC THRU E406-EX.
        e406DeclareCursorEffNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-TGA-DI-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCEffTgaDiNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: E471-CLOSE-CURSOR-EFF-NC<br>*/
    private void e471CloseCursorEffNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-TGA-DI-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCEffTgaDiNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: E481-FETCH-FIRST-EFF-NC<br>*/
    private void e481FetchFirstEffNc() {
        // COB_CODE: PERFORM E461-OPEN-CURSOR-EFF-NC  THRU E461-EX.
        e461OpenCursorEffNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM E491-FETCH-NEXT-EFF-NC THRU E491-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM E491-FETCH-NEXT-EFF-NC THRU E491-EX
            e491FetchNextEffNc();
        }
    }

    /**Original name: E491-FETCH-NEXT-EFF-NC<br>*/
    private void e491FetchNextEffNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-TGA-DI-NC
        //             INTO
        //                  :TGA-DT-VLDT-PROD-DB
        //                  :IND-TGA-DT-VLDT-PROD
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCEffTgaDiNc(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM E471-CLOSE-CURSOR-EFF-NC THRU E471-EX
            e471CloseCursorEffNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F405-DECLARE-CURSOR-CPZ-NC<br>
	 * <pre>----
	 * ----  gestione FA Competenza STATO / NOT CAUSALE
	 * ----</pre>*/
    private void f405DeclareCursorCpzNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-TGA-NC CURSOR FOR
        //              SELECT
        //                 A.ID_TRCH_DI_GAR
        //                ,A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.IB_OGG
        //                ,A.TP_RGM_FISC
        //                ,A.DT_EMIS
        //                ,A.TP_TRCH
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.PRE_CASO_MOR
        //                ,A.PC_INTR_RIAT
        //                ,A.IMP_BNS_ANTIC
        //                ,A.PRE_INI_NET
        //                ,A.PRE_PP_INI
        //                ,A.PRE_PP_ULT
        //                ,A.PRE_TARI_INI
        //                ,A.PRE_TARI_ULT
        //                ,A.PRE_INVRIO_INI
        //                ,A.PRE_INVRIO_ULT
        //                ,A.PRE_RIVTO
        //                ,A.IMP_SOPR_PROF
        //                ,A.IMP_SOPR_SAN
        //                ,A.IMP_SOPR_SPO
        //                ,A.IMP_SOPR_TEC
        //                ,A.IMP_ALT_SOPR
        //                ,A.PRE_STAB
        //                ,A.DT_EFF_STAB
        //                ,A.TS_RIVAL_FIS
        //                ,A.TS_RIVAL_INDICIZ
        //                ,A.OLD_TS_TEC
        //                ,A.RAT_LRD
        //                ,A.PRE_LRD
        //                ,A.PRSTZ_INI
        //                ,A.PRSTZ_ULT
        //                ,A.CPT_IN_OPZ_RIVTO
        //                ,A.PRSTZ_INI_STAB
        //                ,A.CPT_RSH_MOR
        //                ,A.PRSTZ_RID_INI
        //                ,A.FL_CAR_CONT
        //                ,A.BNS_GIA_LIQTO
        //                ,A.IMP_BNS
        //                ,A.COD_DVS
        //                ,A.PRSTZ_INI_NEWFIS
        //                ,A.IMP_SCON
        //                ,A.ALQ_SCON
        //                ,A.IMP_CAR_ACQ
        //                ,A.IMP_CAR_INC
        //                ,A.IMP_CAR_GEST
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.RENDTO_LRD
        //                ,A.PC_RETR
        //                ,A.RENDTO_RETR
        //                ,A.MIN_GARTO
        //                ,A.MIN_TRNUT
        //                ,A.PRE_ATT_DI_TRCH
        //                ,A.MATU_END2000
        //                ,A.ABB_TOT_INI
        //                ,A.ABB_TOT_ULT
        //                ,A.ABB_ANNU_ULT
        //                ,A.DUR_ABB
        //                ,A.TP_ADEG_ABB
        //                ,A.MOD_CALC
        //                ,A.IMP_AZ
        //                ,A.IMP_ADER
        //                ,A.IMP_TFR
        //                ,A.IMP_VOLO
        //                ,A.VIS_END2000
        //                ,A.DT_VLDT_PROD
        //                ,A.DT_INI_VAL_TAR
        //                ,A.IMPB_VIS_END2000
        //                ,A.REN_INI_TS_TEC_0
        //                ,A.PC_RIP_PRE
        //                ,A.FL_IMPORTI_FORZ
        //                ,A.PRSTZ_INI_NFORZ
        //                ,A.VIS_END2000_NFORZ
        //                ,A.INTR_MORA
        //                ,A.MANFEE_ANTIC
        //                ,A.MANFEE_RICOR
        //                ,A.PRE_UNI_RIVTO
        //                ,A.PROV_1AA_ACQ
        //                ,A.PROV_2AA_ACQ
        //                ,A.PROV_RICOR
        //                ,A.PROV_INC
        //                ,A.ALQ_PROV_ACQ
        //                ,A.ALQ_PROV_INC
        //                ,A.ALQ_PROV_RICOR
        //                ,A.IMPB_PROV_ACQ
        //                ,A.IMPB_PROV_INC
        //                ,A.IMPB_PROV_RICOR
        //                ,A.FL_PROV_FORZ
        //                ,A.PRSTZ_AGG_INI
        //                ,A.INCR_PRE
        //                ,A.INCR_PRSTZ
        //                ,A.DT_ULT_ADEG_PRE_PR
        //                ,A.PRSTZ_AGG_ULT
        //                ,A.TS_RIVAL_NET
        //                ,A.PRE_PATTUITO
        //                ,A.TP_RIVAL
        //                ,A.RIS_MAT
        //                ,A.CPT_MIN_SCAD
        //                ,A.COMMIS_GEST
        //                ,A.TP_MANFEE_APPL
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.PC_COMMIS_GEST
        //                ,A.NUM_GG_RIVAL
        //                ,A.IMP_TRASFE
        //                ,A.IMP_TFR_STRC
        //                ,A.ACQ_EXP
        //                ,A.REMUN_ASS
        //                ,A.COMMIS_INTER
        //                ,A.ALQ_REMUN_ASS
        //                ,A.ALQ_COMMIS_INTER
        //                ,A.IMPB_REMUN_ASS
        //                ,A.IMPB_COMMIS_INTER
        //                ,A.COS_RUN_ASSVA
        //                ,A.COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_TRCH_DI_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: F460-OPEN-CURSOR-CPZ-NC<br>*/
    private void f460OpenCursorCpzNc() {
        // COB_CODE: PERFORM F405-DECLARE-CURSOR-CPZ-NC THRU F405-EX.
        f405DeclareCursorCpzNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-TGA-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCCpzTgaNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: F470-CLOSE-CURSOR-CPZ-NC<br>*/
    private void f470CloseCursorCpzNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-TGA-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCCpzTgaNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: F480-FETCH-FIRST-CPZ-NC<br>*/
    private void f480FetchFirstCpzNc() {
        // COB_CODE: PERFORM F460-OPEN-CURSOR-CPZ-NC    THRU F460-EX.
        f460OpenCursorCpzNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM F490-FETCH-NEXT-CPZ-NC THRU F490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM F490-FETCH-NEXT-CPZ-NC THRU F490-EX
            f490FetchNextCpzNc();
        }
    }

    /**Original name: F490-FETCH-NEXT-CPZ-NC<br>*/
    private void f490FetchNextCpzNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-TGA-NC
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCCpzTgaNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM F470-CLOSE-CURSOR-CPZ-NC THRU F470-EX
            f470CloseCursorCpzNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: F406-DECLARE-CURSOR-CPZ-NC<br>
	 * <pre>----
	 * ----  gestione WH Competenza STATO / NOT CAUSALE
	 * ----</pre>*/
    private void f406DeclareCursorCpzNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-TGA-DI-NC CURSOR FOR
        //              SELECT DISTINCT(A.DT_VLDT_PROD)
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.DT_VLDT_PROD ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: F461-OPEN-CURSOR-CPZ-NC<br>*/
    private void f461OpenCursorCpzNc() {
        // COB_CODE: PERFORM F406-DECLARE-CURSOR-CPZ-NC THRU F406-EX.
        f406DeclareCursorCpzNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-TGA-DI-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCCpzTgaDiNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: F471-CLOSE-CURSOR-CPZ-NC<br>*/
    private void f471CloseCursorCpzNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-TGA-DI-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCCpzTgaDiNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: F481-FETCH-FIRST-CPZ-NC<br>*/
    private void f481FetchFirstCpzNc() {
        // COB_CODE: PERFORM F461-OPEN-CURSOR-CPZ-NC    THRU F461-EX.
        f461OpenCursorCpzNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM F491-FETCH-NEXT-CPZ-NC THRU F491-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM F491-FETCH-NEXT-CPZ-NC THRU F491-EX
            f491FetchNextCpzNc();
        }
    }

    /**Original name: F491-FETCH-NEXT-CPZ-NC<br>*/
    private void f491FetchNextCpzNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-TGA-DI-NC
        //                INTO
        //                  :TGA-DT-VLDT-PROD-DB
        //                  :IND-TGA-DT-VLDT-PROD
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCCpzTgaDiNc(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM F471-CLOSE-CURSOR-CPZ-NC THRU F471-EX
            f471CloseCursorCpzNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: G405-DECLARE-CURSOR-EFF-NS-NC<br>
	 * <pre>----
	 * ----  gestione Effetto NOT STATO / NOT CAUSALE
	 * ----</pre>*/
    private void g405DeclareCursorEffNsNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-TGA-NS-NC CURSOR FOR
        //              SELECT
        //                 A.ID_TRCH_DI_GAR
        //                ,A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.IB_OGG
        //                ,A.TP_RGM_FISC
        //                ,A.DT_EMIS
        //                ,A.TP_TRCH
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.PRE_CASO_MOR
        //                ,A.PC_INTR_RIAT
        //                ,A.IMP_BNS_ANTIC
        //                ,A.PRE_INI_NET
        //                ,A.PRE_PP_INI
        //                ,A.PRE_PP_ULT
        //                ,A.PRE_TARI_INI
        //                ,A.PRE_TARI_ULT
        //                ,A.PRE_INVRIO_INI
        //                ,A.PRE_INVRIO_ULT
        //                ,A.PRE_RIVTO
        //                ,A.IMP_SOPR_PROF
        //                ,A.IMP_SOPR_SAN
        //                ,A.IMP_SOPR_SPO
        //                ,A.IMP_SOPR_TEC
        //                ,A.IMP_ALT_SOPR
        //                ,A.PRE_STAB
        //                ,A.DT_EFF_STAB
        //                ,A.TS_RIVAL_FIS
        //                ,A.TS_RIVAL_INDICIZ
        //                ,A.OLD_TS_TEC
        //                ,A.RAT_LRD
        //                ,A.PRE_LRD
        //                ,A.PRSTZ_INI
        //                ,A.PRSTZ_ULT
        //                ,A.CPT_IN_OPZ_RIVTO
        //                ,A.PRSTZ_INI_STAB
        //                ,A.CPT_RSH_MOR
        //                ,A.PRSTZ_RID_INI
        //                ,A.FL_CAR_CONT
        //                ,A.BNS_GIA_LIQTO
        //                ,A.IMP_BNS
        //                ,A.COD_DVS
        //                ,A.PRSTZ_INI_NEWFIS
        //                ,A.IMP_SCON
        //                ,A.ALQ_SCON
        //                ,A.IMP_CAR_ACQ
        //                ,A.IMP_CAR_INC
        //                ,A.IMP_CAR_GEST
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.RENDTO_LRD
        //                ,A.PC_RETR
        //                ,A.RENDTO_RETR
        //                ,A.MIN_GARTO
        //                ,A.MIN_TRNUT
        //                ,A.PRE_ATT_DI_TRCH
        //                ,A.MATU_END2000
        //                ,A.ABB_TOT_INI
        //                ,A.ABB_TOT_ULT
        //                ,A.ABB_ANNU_ULT
        //                ,A.DUR_ABB
        //                ,A.TP_ADEG_ABB
        //                ,A.MOD_CALC
        //                ,A.IMP_AZ
        //                ,A.IMP_ADER
        //                ,A.IMP_TFR
        //                ,A.IMP_VOLO
        //                ,A.VIS_END2000
        //                ,A.DT_VLDT_PROD
        //                ,A.DT_INI_VAL_TAR
        //                ,A.IMPB_VIS_END2000
        //                ,A.REN_INI_TS_TEC_0
        //                ,A.PC_RIP_PRE
        //                ,A.FL_IMPORTI_FORZ
        //                ,A.PRSTZ_INI_NFORZ
        //                ,A.VIS_END2000_NFORZ
        //                ,A.INTR_MORA
        //                ,A.MANFEE_ANTIC
        //                ,A.MANFEE_RICOR
        //                ,A.PRE_UNI_RIVTO
        //                ,A.PROV_1AA_ACQ
        //                ,A.PROV_2AA_ACQ
        //                ,A.PROV_RICOR
        //                ,A.PROV_INC
        //                ,A.ALQ_PROV_ACQ
        //                ,A.ALQ_PROV_INC
        //                ,A.ALQ_PROV_RICOR
        //                ,A.IMPB_PROV_ACQ
        //                ,A.IMPB_PROV_INC
        //                ,A.IMPB_PROV_RICOR
        //                ,A.FL_PROV_FORZ
        //                ,A.PRSTZ_AGG_INI
        //                ,A.INCR_PRE
        //                ,A.INCR_PRSTZ
        //                ,A.DT_ULT_ADEG_PRE_PR
        //                ,A.PRSTZ_AGG_ULT
        //                ,A.TS_RIVAL_NET
        //                ,A.PRE_PATTUITO
        //                ,A.TP_RIVAL
        //                ,A.RIS_MAT
        //                ,A.CPT_MIN_SCAD
        //                ,A.COMMIS_GEST
        //                ,A.TP_MANFEE_APPL
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.PC_COMMIS_GEST
        //                ,A.NUM_GG_RIVAL
        //                ,A.IMP_TRASFE
        //                ,A.IMP_TFR_STRC
        //                ,A.ACQ_EXP
        //                ,A.REMUN_ASS
        //                ,A.COMMIS_INTER
        //                ,A.ALQ_REMUN_ASS
        //                ,A.ALQ_COMMIS_INTER
        //                ,A.IMPB_REMUN_ASS
        //                ,A.IMPB_COMMIS_INTER
        //                ,A.COS_RUN_ASSVA
        //                ,A.COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_TRCH_DI_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: G460-OPEN-CURSOR-EFF-NS-NC<br>*/
    private void g460OpenCursorEffNsNc() {
        // COB_CODE: PERFORM G405-DECLARE-CURSOR-EFF-NS-NC THRU G405-EX.
        g405DeclareCursorEffNsNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-TGA-NS-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCEffTgaNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: G470-CLOSE-CURSOR-EFF-NS-NC<br>*/
    private void g470CloseCursorEffNsNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-TGA-NS-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCEffTgaNsNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: G480-FETCH-FIRST-EFF-NS-NC<br>*/
    private void g480FetchFirstEffNsNc() {
        // COB_CODE: PERFORM G460-OPEN-CURSOR-EFF-NS-NC  THRU G460-EX.
        g460OpenCursorEffNsNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM G490-FETCH-NEXT-EFF-NS-NC THRU G490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM G490-FETCH-NEXT-EFF-NS-NC THRU G490-EX
            g490FetchNextEffNsNc();
        }
    }

    /**Original name: G490-FETCH-NEXT-EFF-NS-NC<br>*/
    private void g490FetchNextEffNsNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-TGA-NS-NC
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCEffTgaNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM G470-CLOSE-CURSOR-EFF-NS-NC THRU G470-EX
            g470CloseCursorEffNsNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: G406-DECLARE-CURSOR-EFF-NS-NC<br>
	 * <pre>----
	 * ----  gestione WH Effetto NOT STATO / NOT CAUSALE
	 * ----</pre>*/
    private void g406DeclareCursorEffNsNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-EFF-TGA-DI-NS-NC CURSOR FOR
        //              SELECT DISTINCT(A.DT_VLDT_PROD)
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.DT_VLDT_PROD ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: G461-OPEN-CURSOR-EFF-NS-NC<br>*/
    private void g461OpenCursorEffNsNc() {
        // COB_CODE: PERFORM G406-DECLARE-CURSOR-EFF-NS-NC THRU G406-EX.
        g406DeclareCursorEffNsNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF-TGA-DI-NS-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCEffTgaDiNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: G471-CLOSE-CURSOR-EFF-NS-NC<br>*/
    private void g471CloseCursorEffNsNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF-TGA-DI-NS-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCEffTgaDiNsNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: G481-FETCH-FIRST-EFF-NS-NC<br>*/
    private void g481FetchFirstEffNsNc() {
        // COB_CODE: PERFORM G461-OPEN-CURSOR-EFF-NS-NC  THRU G461-EX.
        g461OpenCursorEffNsNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM G491-FETCH-NEXT-EFF-NS-NC THRU G491-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM G491-FETCH-NEXT-EFF-NS-NC THRU G491-EX
            g491FetchNextEffNsNc();
        }
    }

    /**Original name: G491-FETCH-NEXT-EFF-NS-NC<br>*/
    private void g491FetchNextEffNsNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF-TGA-DI-NS-NC
        //             INTO
        //                  :TGA-DT-VLDT-PROD-DB
        //                  :IND-TGA-DT-VLDT-PROD
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCEffTgaDiNsNc(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM G471-CLOSE-CURSOR-EFF-NS-NC THRU G471-EX
            g471CloseCursorEffNsNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: H405-DECLARE-CURSOR-CPZ-NS-NC<br>
	 * <pre>----
	 * ----  gestione FA Competenza NOT STATO / NOT CAUSALE
	 * ----</pre>*/
    private void h405DeclareCursorCpzNsNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-TGA-NS-NC CURSOR FOR
        //              SELECT
        //                 A.ID_TRCH_DI_GAR
        //                ,A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.IB_OGG
        //                ,A.TP_RGM_FISC
        //                ,A.DT_EMIS
        //                ,A.TP_TRCH
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.PRE_CASO_MOR
        //                ,A.PC_INTR_RIAT
        //                ,A.IMP_BNS_ANTIC
        //                ,A.PRE_INI_NET
        //                ,A.PRE_PP_INI
        //                ,A.PRE_PP_ULT
        //                ,A.PRE_TARI_INI
        //                ,A.PRE_TARI_ULT
        //                ,A.PRE_INVRIO_INI
        //                ,A.PRE_INVRIO_ULT
        //                ,A.PRE_RIVTO
        //                ,A.IMP_SOPR_PROF
        //                ,A.IMP_SOPR_SAN
        //                ,A.IMP_SOPR_SPO
        //                ,A.IMP_SOPR_TEC
        //                ,A.IMP_ALT_SOPR
        //                ,A.PRE_STAB
        //                ,A.DT_EFF_STAB
        //                ,A.TS_RIVAL_FIS
        //                ,A.TS_RIVAL_INDICIZ
        //                ,A.OLD_TS_TEC
        //                ,A.RAT_LRD
        //                ,A.PRE_LRD
        //                ,A.PRSTZ_INI
        //                ,A.PRSTZ_ULT
        //                ,A.CPT_IN_OPZ_RIVTO
        //                ,A.PRSTZ_INI_STAB
        //                ,A.CPT_RSH_MOR
        //                ,A.PRSTZ_RID_INI
        //                ,A.FL_CAR_CONT
        //                ,A.BNS_GIA_LIQTO
        //                ,A.IMP_BNS
        //                ,A.COD_DVS
        //                ,A.PRSTZ_INI_NEWFIS
        //                ,A.IMP_SCON
        //                ,A.ALQ_SCON
        //                ,A.IMP_CAR_ACQ
        //                ,A.IMP_CAR_INC
        //                ,A.IMP_CAR_GEST
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.RENDTO_LRD
        //                ,A.PC_RETR
        //                ,A.RENDTO_RETR
        //                ,A.MIN_GARTO
        //                ,A.MIN_TRNUT
        //                ,A.PRE_ATT_DI_TRCH
        //                ,A.MATU_END2000
        //                ,A.ABB_TOT_INI
        //                ,A.ABB_TOT_ULT
        //                ,A.ABB_ANNU_ULT
        //                ,A.DUR_ABB
        //                ,A.TP_ADEG_ABB
        //                ,A.MOD_CALC
        //                ,A.IMP_AZ
        //                ,A.IMP_ADER
        //                ,A.IMP_TFR
        //                ,A.IMP_VOLO
        //                ,A.VIS_END2000
        //                ,A.DT_VLDT_PROD
        //                ,A.DT_INI_VAL_TAR
        //                ,A.IMPB_VIS_END2000
        //                ,A.REN_INI_TS_TEC_0
        //                ,A.PC_RIP_PRE
        //                ,A.FL_IMPORTI_FORZ
        //                ,A.PRSTZ_INI_NFORZ
        //                ,A.VIS_END2000_NFORZ
        //                ,A.INTR_MORA
        //                ,A.MANFEE_ANTIC
        //                ,A.MANFEE_RICOR
        //                ,A.PRE_UNI_RIVTO
        //                ,A.PROV_1AA_ACQ
        //                ,A.PROV_2AA_ACQ
        //                ,A.PROV_RICOR
        //                ,A.PROV_INC
        //                ,A.ALQ_PROV_ACQ
        //                ,A.ALQ_PROV_INC
        //                ,A.ALQ_PROV_RICOR
        //                ,A.IMPB_PROV_ACQ
        //                ,A.IMPB_PROV_INC
        //                ,A.IMPB_PROV_RICOR
        //                ,A.FL_PROV_FORZ
        //                ,A.PRSTZ_AGG_INI
        //                ,A.INCR_PRE
        //                ,A.INCR_PRSTZ
        //                ,A.DT_ULT_ADEG_PRE_PR
        //                ,A.PRSTZ_AGG_ULT
        //                ,A.TS_RIVAL_NET
        //                ,A.PRE_PATTUITO
        //                ,A.TP_RIVAL
        //                ,A.RIS_MAT
        //                ,A.CPT_MIN_SCAD
        //                ,A.COMMIS_GEST
        //                ,A.TP_MANFEE_APPL
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.PC_COMMIS_GEST
        //                ,A.NUM_GG_RIVAL
        //                ,A.IMP_TRASFE
        //                ,A.IMP_TFR_STRC
        //                ,A.ACQ_EXP
        //                ,A.REMUN_ASS
        //                ,A.COMMIS_INTER
        //                ,A.ALQ_REMUN_ASS
        //                ,A.ALQ_COMMIS_INTER
        //                ,A.IMPB_REMUN_ASS
        //                ,A.IMPB_COMMIS_INTER
        //                ,A.COS_RUN_ASSVA
        //                ,A.COS_RUN_ASSVA_IDC
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.ID_ADES BETWEEN
        //                        :WK-ID-ADES-DA AND :WK-ID-ADES-A
        //                    AND A.ID_GAR BETWEEN
        //                        :WK-ID-GAR-DA  AND :WK-ID-GAR-A
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.ID_TRCH_DI_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: H460-OPEN-CURSOR-CPZ-NS-NC<br>*/
    private void h460OpenCursorCpzNsNc() {
        // COB_CODE: PERFORM H405-DECLARE-CURSOR-CPZ-NS-NC THRU H405-EX.
        h405DeclareCursorCpzNsNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-TGA-NS-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCCpzTgaNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: H470-CLOSE-CURSOR-CPZ-NS-NC<br>*/
    private void h470CloseCursorCpzNsNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-TGA-NS-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCCpzTgaNsNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: H480-FETCH-FIRST-CPZ-NS-NC<br>*/
    private void h480FetchFirstCpzNsNc() {
        // COB_CODE: PERFORM H460-OPEN-CURSOR-CPZ-NS-NC    THRU H460-EX.
        h460OpenCursorCpzNsNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM H490-FETCH-NEXT-CPZ-NS-NC THRU H490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM H490-FETCH-NEXT-CPZ-NS-NC THRU H490-EX
            h490FetchNextCpzNsNc();
        }
    }

    /**Original name: H490-FETCH-NEXT-CPZ-NS-NC<br>*/
    private void h490FetchNextCpzNsNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-TGA-NS-NC
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCCpzTgaNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM H470-CLOSE-CURSOR-CPZ-NS-NC THRU H470-EX
            h470CloseCursorCpzNsNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: H406-DECLARE-CURSOR-CPZ-NS-NC<br>
	 * <pre>----
	 * ----  gestione WH Competenza NOT STATO / NOT CAUSALE
	 * ----</pre>*/
    private void h406DeclareCursorCpzNsNc() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-CPZ-TGA-DI-NS-NC CURSOR FOR
        //              SELECT DISTINCT(A.DT_VLDT_PROD)
        //              FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE     A.ID_POLI = :TGA-ID-POLI
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.COD_COMP_ANIA =
        //                        B.COD_COMP_ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.TP_OGG      = 'TG'
        //                    AND B.ID_OGG      = A.ID_TRCH_DI_GAR
        //                    AND
        //                    NOT B.TP_STAT_BUS IN (
        //                                         :LDBV1361-TP-STAT-BUS-01,
        //                                         :LDBV1361-TP-STAT-BUS-02,
        //                                         :LDBV1361-TP-STAT-BUS-03,
        //                                         :LDBV1361-TP-STAT-BUS-04,
        //                                         :LDBV1361-TP-STAT-BUS-05,
        //                                         :LDBV1361-TP-STAT-BUS-06,
        //                                         :LDBV1361-TP-STAT-BUS-07
        //                                         )
        //                    AND
        //                    NOT B.TP_CAUS     IN (
        //                                         :LDBV1361-TP-CAUS-01,
        //                                         :LDBV1361-TP-CAUS-02,
        //                                         :LDBV1361-TP-CAUS-03,
        //                                         :LDBV1361-TP-CAUS-04,
        //                                         :LDBV1361-TP-CAUS-05,
        //                                         :LDBV1361-TP-CAUS-06,
        //                                         :LDBV1361-TP-CAUS-07
        //                                         )
        //              ORDER BY A.DT_VLDT_PROD ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: H461-OPEN-CURSOR-CPZ-NS-NC<br>*/
    private void h461OpenCursorCpzNsNc() {
        // COB_CODE: PERFORM H406-DECLARE-CURSOR-CPZ-NS-NC THRU H406-EX.
        h406DeclareCursorCpzNsNc();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ-TGA-DI-NS-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCCpzTgaDiNsNc(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: H471-CLOSE-CURSOR-CPZ-NS-NC<br>*/
    private void h471CloseCursorCpzNsNc() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ-TGA-DI-NS-NC
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCCpzTgaDiNsNc();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: H481-FETCH-FIRST-CPZ-NS-NC<br>*/
    private void h481FetchFirstCpzNsNc() {
        // COB_CODE: PERFORM H461-OPEN-CURSOR-CPZ-NS-NC    THRU H461-EX.
        h461OpenCursorCpzNsNc();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM H491-FETCH-NEXT-CPZ-NS-NC THRU H491-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM H491-FETCH-NEXT-CPZ-NS-NC THRU H491-EX
            h491FetchNextCpzNsNc();
        }
    }

    /**Original name: H491-FETCH-NEXT-CPZ-NS-NC<br>*/
    private void h491FetchNextCpzNsNc() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ-TGA-DI-NS-NC
        //                INTO
        //                  :TGA-DT-VLDT-PROD-DB
        //                  :IND-TGA-DT-VLDT-PROD
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCCpzTgaDiNsNc(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM H471-CLOSE-CURSOR-CPZ-NS-NC THRU H471-EX
            h471CloseCursorCpzNsNc();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TGA-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TGA-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ID-MOVI-CHIU-NULL
            trchDiGar.getTgaIdMoviChiu().setTgaIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIdMoviChiu.Len.TGA_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO TGA-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-SCAD-NULL
            trchDiGar.getTgaDtScad().setTgaDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtScad.Len.TGA_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-TGA-IB-OGG = -1
        //              MOVE HIGH-VALUES TO TGA-IB-OGG-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IB-OGG-NULL
            trchDiGar.setTgaIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGar.Len.TGA_IB_OGG));
        }
        // COB_CODE: IF IND-TGA-DT-EMIS = -1
        //              MOVE HIGH-VALUES TO TGA-DT-EMIS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-EMIS-NULL
            trchDiGar.getTgaDtEmis().setTgaDtEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtEmis.Len.TGA_DT_EMIS_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-AA = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-AA-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-AA-NULL
            trchDiGar.getTgaDurAa().setTgaDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurAa.Len.TGA_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-MM = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-MM-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-MM-NULL
            trchDiGar.getTgaDurMm().setTgaDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurMm.Len.TGA_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-GG = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-GG-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-GG-NULL
            trchDiGar.getTgaDurGg().setTgaDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurGg.Len.TGA_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-CASO-MOR = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-CASO-MOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-CASO-MOR-NULL
            trchDiGar.getTgaPreCasoMor().setTgaPreCasoMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreCasoMor.Len.TGA_PRE_CASO_MOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PC-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO TGA-PC-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-INTR-RIAT-NULL
            trchDiGar.getTgaPcIntrRiat().setTgaPcIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcIntrRiat.Len.TGA_PC_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-BNS-ANTIC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-BNS-ANTIC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpBnsAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-BNS-ANTIC-NULL
            trchDiGar.getTgaImpBnsAntic().setTgaImpBnsAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpBnsAntic.Len.TGA_IMP_BNS_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-INI-NET = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-INI-NET-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreIniNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-INI-NET-NULL
            trchDiGar.getTgaPreIniNet().setTgaPreIniNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreIniNet.Len.TGA_PRE_INI_NET_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-PP-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-PP-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrePpIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-PP-INI-NULL
            trchDiGar.getTgaPrePpIni().setTgaPrePpIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrePpIni.Len.TGA_PRE_PP_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-PP-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-PP-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrePpUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-PP-ULT-NULL
            trchDiGar.getTgaPrePpUlt().setTgaPrePpUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrePpUlt.Len.TGA_PRE_PP_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-TARI-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-TARI-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreTariIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-TARI-INI-NULL
            trchDiGar.getTgaPreTariIni().setTgaPreTariIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreTariIni.Len.TGA_PRE_TARI_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-TARI-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-TARI-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreTariUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-TARI-ULT-NULL
            trchDiGar.getTgaPreTariUlt().setTgaPreTariUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreTariUlt.Len.TGA_PRE_TARI_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-INVRIO-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreInvrioIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-INVRIO-INI-NULL
            trchDiGar.getTgaPreInvrioIni().setTgaPreInvrioIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreInvrioIni.Len.TGA_PRE_INVRIO_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-INVRIO-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreInvrioUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-INVRIO-ULT-NULL
            trchDiGar.getTgaPreInvrioUlt().setTgaPreInvrioUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreInvrioUlt.Len.TGA_PRE_INVRIO_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-RIVTO = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-RIVTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreRivto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-RIVTO-NULL
            trchDiGar.getTgaPreRivto().setTgaPreRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreRivto.Len.TGA_PRE_RIVTO_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-PROF-NULL
            trchDiGar.getTgaImpSoprProf().setTgaImpSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprProf.Len.TGA_IMP_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-SAN-NULL
            trchDiGar.getTgaImpSoprSan().setTgaImpSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprSan.Len.TGA_IMP_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-SPO-NULL
            trchDiGar.getTgaImpSoprSpo().setTgaImpSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprSpo.Len.TGA_IMP_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-TEC-NULL
            trchDiGar.getTgaImpSoprTec().setTgaImpSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprTec.Len.TGA_IMP_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-ALT-SOPR = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-ALT-SOPR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpAltSopr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-ALT-SOPR-NULL
            trchDiGar.getTgaImpAltSopr().setTgaImpAltSoprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpAltSopr.Len.TGA_IMP_ALT_SOPR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-STAB = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-STAB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-STAB-NULL
            trchDiGar.getTgaPreStab().setTgaPreStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreStab.Len.TGA_PRE_STAB_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-EFF-STAB = -1
        //              MOVE HIGH-VALUES TO TGA-DT-EFF-STAB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEffStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-EFF-STAB-NULL
            trchDiGar.getTgaDtEffStab().setTgaDtEffStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtEffStab.Len.TGA_DT_EFF_STAB_NULL));
        }
        // COB_CODE: IF IND-TGA-TS-RIVAL-FIS = -1
        //              MOVE HIGH-VALUES TO TGA-TS-RIVAL-FIS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTsRivalFis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TS-RIVAL-FIS-NULL
            trchDiGar.getTgaTsRivalFis().setTgaTsRivalFisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaTsRivalFis.Len.TGA_TS_RIVAL_FIS_NULL));
        }
        // COB_CODE: IF IND-TGA-TS-RIVAL-INDICIZ = -1
        //              MOVE HIGH-VALUES TO TGA-TS-RIVAL-INDICIZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTsRivalIndiciz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TS-RIVAL-INDICIZ-NULL
            trchDiGar.getTgaTsRivalIndiciz().setTgaTsRivalIndicizNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaTsRivalIndiciz.Len.TGA_TS_RIVAL_INDICIZ_NULL));
        }
        // COB_CODE: IF IND-TGA-OLD-TS-TEC = -1
        //              MOVE HIGH-VALUES TO TGA-OLD-TS-TEC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getOldTsTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-OLD-TS-TEC-NULL
            trchDiGar.getTgaOldTsTec().setTgaOldTsTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaOldTsTec.Len.TGA_OLD_TS_TEC_NULL));
        }
        // COB_CODE: IF IND-TGA-RAT-LRD = -1
        //              MOVE HIGH-VALUES TO TGA-RAT-LRD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRatLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RAT-LRD-NULL
            trchDiGar.getTgaRatLrd().setTgaRatLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRatLrd.Len.TGA_RAT_LRD_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-LRD = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-LRD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-LRD-NULL
            trchDiGar.getTgaPreLrd().setTgaPreLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreLrd.Len.TGA_PRE_LRD_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NULL
            trchDiGar.getTgaPrstzIni().setTgaPrstzIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIni.Len.TGA_PRSTZ_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-ULT-NULL
            trchDiGar.getTgaPrstzUlt().setTgaPrstzUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzUlt.Len.TGA_PRSTZ_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-CPT-IN-OPZ-RIVTO = -1
        //              MOVE HIGH-VALUES TO TGA-CPT-IN-OPZ-RIVTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCptInOpzRivto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-CPT-IN-OPZ-RIVTO-NULL
            trchDiGar.getTgaCptInOpzRivto().setTgaCptInOpzRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCptInOpzRivto.Len.TGA_CPT_IN_OPZ_RIVTO_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI-STAB = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-STAB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIniStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-STAB-NULL
            trchDiGar.getTgaPrstzIniStab().setTgaPrstzIniStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIniStab.Len.TGA_PRSTZ_INI_STAB_NULL));
        }
        // COB_CODE: IF IND-TGA-CPT-RSH-MOR = -1
        //              MOVE HIGH-VALUES TO TGA-CPT-RSH-MOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCptRshMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-CPT-RSH-MOR-NULL
            trchDiGar.getTgaCptRshMor().setTgaCptRshMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCptRshMor.Len.TGA_CPT_RSH_MOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-RID-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-RID-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzRidIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-RID-INI-NULL
            trchDiGar.getTgaPrstzRidIni().setTgaPrstzRidIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzRidIni.Len.TGA_PRSTZ_RID_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-FL-CAR-CONT = -1
        //              MOVE HIGH-VALUES TO TGA-FL-CAR-CONT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getFlCarCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-FL-CAR-CONT-NULL
            trchDiGar.setTgaFlCarCont(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-BNS-GIA-LIQTO = -1
        //              MOVE HIGH-VALUES TO TGA-BNS-GIA-LIQTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getBnsGiaLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-BNS-GIA-LIQTO-NULL
            trchDiGar.getTgaBnsGiaLiqto().setTgaBnsGiaLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaBnsGiaLiqto.Len.TGA_BNS_GIA_LIQTO_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-BNS = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-BNS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpBns() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-BNS-NULL
            trchDiGar.getTgaImpBns().setTgaImpBnsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpBns.Len.TGA_IMP_BNS_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI-NEWFIS = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NEWFIS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIniNewfis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NEWFIS-NULL
            trchDiGar.getTgaPrstzIniNewfis().setTgaPrstzIniNewfisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIniNewfis.Len.TGA_PRSTZ_INI_NEWFIS_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SCON = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SCON-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SCON-NULL
            trchDiGar.getTgaImpScon().setTgaImpSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpScon.Len.TGA_IMP_SCON_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-SCON = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-SCON-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-SCON-NULL
            trchDiGar.getTgaAlqScon().setTgaAlqSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqScon.Len.TGA_ALQ_SCON_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-CAR-ACQ-NULL
            trchDiGar.getTgaImpCarAcq().setTgaImpCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpCarAcq.Len.TGA_IMP_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-CAR-INC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-CAR-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-CAR-INC-NULL
            trchDiGar.getTgaImpCarInc().setTgaImpCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpCarInc.Len.TGA_IMP_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-CAR-GEST-NULL
            trchDiGar.getTgaImpCarGest().setTgaImpCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpCarGest.Len.TGA_IMP_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-AA-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-AA-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaAa1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-AA-1O-ASSTO-NULL
            trchDiGar.getTgaEtaAa1oAssto().setTgaEtaAa1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaAa1oAssto.Len.TGA_ETA_AA1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-MM-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-MM-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaMm1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-MM-1O-ASSTO-NULL
            trchDiGar.getTgaEtaMm1oAssto().setTgaEtaMm1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaMm1oAssto.Len.TGA_ETA_MM1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-AA-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-AA-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaAa2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-AA-2O-ASSTO-NULL
            trchDiGar.getTgaEtaAa2oAssto().setTgaEtaAa2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaAa2oAssto.Len.TGA_ETA_AA2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-MM-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-MM-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaMm2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-MM-2O-ASSTO-NULL
            trchDiGar.getTgaEtaMm2oAssto().setTgaEtaMm2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaMm2oAssto.Len.TGA_ETA_MM2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-AA-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-AA-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaAa3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-AA-3O-ASSTO-NULL
            trchDiGar.getTgaEtaAa3oAssto().setTgaEtaAa3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaAa3oAssto.Len.TGA_ETA_AA3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-MM-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-MM-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaMm3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-MM-3O-ASSTO-NULL
            trchDiGar.getTgaEtaMm3oAssto().setTgaEtaMm3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaMm3oAssto.Len.TGA_ETA_MM3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-RENDTO-LRD = -1
        //              MOVE HIGH-VALUES TO TGA-RENDTO-LRD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRendtoLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RENDTO-LRD-NULL
            trchDiGar.getTgaRendtoLrd().setTgaRendtoLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRendtoLrd.Len.TGA_RENDTO_LRD_NULL));
        }
        // COB_CODE: IF IND-TGA-PC-RETR = -1
        //              MOVE HIGH-VALUES TO TGA-PC-RETR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcRetr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-RETR-NULL
            trchDiGar.getTgaPcRetr().setTgaPcRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcRetr.Len.TGA_PC_RETR_NULL));
        }
        // COB_CODE: IF IND-TGA-RENDTO-RETR = -1
        //              MOVE HIGH-VALUES TO TGA-RENDTO-RETR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRendtoRetr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RENDTO-RETR-NULL
            trchDiGar.getTgaRendtoRetr().setTgaRendtoRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRendtoRetr.Len.TGA_RENDTO_RETR_NULL));
        }
        // COB_CODE: IF IND-TGA-MIN-GARTO = -1
        //              MOVE HIGH-VALUES TO TGA-MIN-GARTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getMinGarto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MIN-GARTO-NULL
            trchDiGar.getTgaMinGarto().setTgaMinGartoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaMinGarto.Len.TGA_MIN_GARTO_NULL));
        }
        // COB_CODE: IF IND-TGA-MIN-TRNUT = -1
        //              MOVE HIGH-VALUES TO TGA-MIN-TRNUT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getMinTrnut() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MIN-TRNUT-NULL
            trchDiGar.getTgaMinTrnut().setTgaMinTrnutNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaMinTrnut.Len.TGA_MIN_TRNUT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-ATT-DI-TRCH = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-ATT-DI-TRCH-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreAttDiTrch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-ATT-DI-TRCH-NULL
            trchDiGar.getTgaPreAttDiTrch().setTgaPreAttDiTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreAttDiTrch.Len.TGA_PRE_ATT_DI_TRCH_NULL));
        }
        // COB_CODE: IF IND-TGA-MATU-END2000 = -1
        //              MOVE HIGH-VALUES TO TGA-MATU-END2000-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getMatuEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MATU-END2000-NULL
            trchDiGar.getTgaMatuEnd2000().setTgaMatuEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaMatuEnd2000.Len.TGA_MATU_END2000_NULL));
        }
        // COB_CODE: IF IND-TGA-ABB-TOT-INI = -1
        //              MOVE HIGH-VALUES TO TGA-ABB-TOT-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAbbTotIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ABB-TOT-INI-NULL
            trchDiGar.getTgaAbbTotIni().setTgaAbbTotIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAbbTotIni.Len.TGA_ABB_TOT_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-ABB-TOT-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-ABB-TOT-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAbbTotUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ABB-TOT-ULT-NULL
            trchDiGar.getTgaAbbTotUlt().setTgaAbbTotUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAbbTotUlt.Len.TGA_ABB_TOT_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-ABB-ANNU-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-ABB-ANNU-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAbbAnnuUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ABB-ANNU-ULT-NULL
            trchDiGar.getTgaAbbAnnuUlt().setTgaAbbAnnuUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAbbAnnuUlt.Len.TGA_ABB_ANNU_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-ABB = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-ABB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-ABB-NULL
            trchDiGar.getTgaDurAbb().setTgaDurAbbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurAbb.Len.TGA_DUR_ABB_NULL));
        }
        // COB_CODE: IF IND-TGA-TP-ADEG-ABB = -1
        //              MOVE HIGH-VALUES TO TGA-TP-ADEG-ABB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTpAdegAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TP-ADEG-ABB-NULL
            trchDiGar.setTgaTpAdegAbb(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-MOD-CALC = -1
        //              MOVE HIGH-VALUES TO TGA-MOD-CALC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getModCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MOD-CALC-NULL
            trchDiGar.setTgaModCalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGar.Len.TGA_MOD_CALC));
        }
        // COB_CODE: IF IND-TGA-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-AZ-NULL
            trchDiGar.getTgaImpAz().setTgaImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpAz.Len.TGA_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-ADER-NULL
            trchDiGar.getTgaImpAder().setTgaImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpAder.Len.TGA_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-TFR-NULL
            trchDiGar.getTgaImpTfr().setTgaImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpTfr.Len.TGA_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-VOLO-NULL
            trchDiGar.getTgaImpVolo().setTgaImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpVolo.Len.TGA_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-TGA-VIS-END2000 = -1
        //              MOVE HIGH-VALUES TO TGA-VIS-END2000-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getVisEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-VIS-END2000-NULL
            trchDiGar.getTgaVisEnd2000().setTgaVisEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaVisEnd2000.Len.TGA_VIS_END2000_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-VLDT-PROD = -1
        //              MOVE HIGH-VALUES TO TGA-DT-VLDT-PROD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtVldtProd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-VLDT-PROD-NULL
            trchDiGar.getTgaDtVldtProd().setTgaDtVldtProdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtVldtProd.Len.TGA_DT_VLDT_PROD_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-INI-VAL-TAR = -1
        //              MOVE HIGH-VALUES TO TGA-DT-INI-VAL-TAR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtIniValTar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-INI-VAL-TAR-NULL
            trchDiGar.getTgaDtIniValTar().setTgaDtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtIniValTar.Len.TGA_DT_INI_VAL_TAR_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-VIS-END2000 = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-VIS-END2000-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbVisEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-VIS-END2000-NULL
            trchDiGar.getTgaImpbVisEnd2000().setTgaImpbVisEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbVisEnd2000.Len.TGA_IMPB_VIS_END2000_NULL));
        }
        // COB_CODE: IF IND-TGA-REN-INI-TS-TEC-0 = -1
        //              MOVE HIGH-VALUES TO TGA-REN-INI-TS-TEC-0-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRenIniTsTec0() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-REN-INI-TS-TEC-0-NULL
            trchDiGar.getTgaRenIniTsTec0().setTgaRenIniTsTec0Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRenIniTsTec0.Len.TGA_REN_INI_TS_TEC0_NULL));
        }
        // COB_CODE: IF IND-TGA-PC-RIP-PRE = -1
        //              MOVE HIGH-VALUES TO TGA-PC-RIP-PRE-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcRipPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-RIP-PRE-NULL
            trchDiGar.getTgaPcRipPre().setTgaPcRipPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcRipPre.Len.TGA_PC_RIP_PRE_NULL));
        }
        // COB_CODE: IF IND-TGA-FL-IMPORTI-FORZ = -1
        //              MOVE HIGH-VALUES TO TGA-FL-IMPORTI-FORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getFlImportiForz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-FL-IMPORTI-FORZ-NULL
            trchDiGar.setTgaFlImportiForz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI-NFORZ = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NFORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIniNforz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NFORZ-NULL
            trchDiGar.getTgaPrstzIniNforz().setTgaPrstzIniNforzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIniNforz.Len.TGA_PRSTZ_INI_NFORZ_NULL));
        }
        // COB_CODE: IF IND-TGA-VIS-END2000-NFORZ = -1
        //              MOVE HIGH-VALUES TO TGA-VIS-END2000-NFORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getVisEnd2000Nforz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-VIS-END2000-NFORZ-NULL
            trchDiGar.getTgaVisEnd2000Nforz().setTgaVisEnd2000NforzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaVisEnd2000Nforz.Len.TGA_VIS_END2000_NFORZ_NULL));
        }
        // COB_CODE: IF IND-TGA-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO TGA-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-INTR-MORA-NULL
            trchDiGar.getTgaIntrMora().setTgaIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIntrMora.Len.TGA_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-TGA-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO TGA-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MANFEE-ANTIC-NULL
            trchDiGar.getTgaManfeeAntic().setTgaManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaManfeeAntic.Len.TGA_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TGA-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MANFEE-RICOR-NULL
            trchDiGar.getTgaManfeeRicor().setTgaManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaManfeeRicor.Len.TGA_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-UNI-RIVTO = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-UNI-RIVTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreUniRivto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-UNI-RIVTO-NULL
            trchDiGar.getTgaPreUniRivto().setTgaPreUniRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreUniRivto.Len.TGA_PRE_UNI_RIVTO_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-1AA-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-1AA-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProv1aaAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-1AA-ACQ-NULL
            trchDiGar.getTgaProv1aaAcq().setTgaProv1aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProv1aaAcq.Len.TGA_PROV1AA_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-2AA-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-2AA-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProv2aaAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-2AA-ACQ-NULL
            trchDiGar.getTgaProv2aaAcq().setTgaProv2aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProv2aaAcq.Len.TGA_PROV2AA_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-RICOR-NULL
            trchDiGar.getTgaProvRicor().setTgaProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProvRicor.Len.TGA_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-INC-NULL
            trchDiGar.getTgaProvInc().setTgaProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProvInc.Len.TGA_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-PROV-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-PROV-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqProvAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-PROV-ACQ-NULL
            trchDiGar.getTgaAlqProvAcq().setTgaAlqProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqProvAcq.Len.TGA_ALQ_PROV_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-PROV-INC-NULL
            trchDiGar.getTgaAlqProvInc().setTgaAlqProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqProvInc.Len.TGA_ALQ_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-PROV-RICOR-NULL
            trchDiGar.getTgaAlqProvRicor().setTgaAlqProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqProvRicor.Len.TGA_ALQ_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-PROV-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-PROV-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbProvAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-PROV-ACQ-NULL
            trchDiGar.getTgaImpbProvAcq().setTgaImpbProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbProvAcq.Len.TGA_IMPB_PROV_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-PROV-INC-NULL
            trchDiGar.getTgaImpbProvInc().setTgaImpbProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbProvInc.Len.TGA_IMPB_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-PROV-RICOR-NULL
            trchDiGar.getTgaImpbProvRicor().setTgaImpbProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbProvRicor.Len.TGA_IMPB_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-FL-PROV-FORZ = -1
        //              MOVE HIGH-VALUES TO TGA-FL-PROV-FORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getFlProvForz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-FL-PROV-FORZ-NULL
            trchDiGar.setTgaFlProvForz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-PRSTZ-AGG-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzAggIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-INI-NULL
            trchDiGar.getTgaPrstzAggIni().setTgaPrstzAggIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzAggIni.Len.TGA_PRSTZ_AGG_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-INCR-PRE = -1
        //              MOVE HIGH-VALUES TO TGA-INCR-PRE-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIncrPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-INCR-PRE-NULL
            trchDiGar.getTgaIncrPre().setTgaIncrPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIncrPre.Len.TGA_INCR_PRE_NULL));
        }
        // COB_CODE: IF IND-TGA-INCR-PRSTZ = -1
        //              MOVE HIGH-VALUES TO TGA-INCR-PRSTZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIncrPrstz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-INCR-PRSTZ-NULL
            trchDiGar.getTgaIncrPrstz().setTgaIncrPrstzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIncrPrstz.Len.TGA_INCR_PRSTZ_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-ULT-ADEG-PRE-PR = -1
        //              MOVE HIGH-VALUES TO TGA-DT-ULT-ADEG-PRE-PR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtUltAdegPrePr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-ULT-ADEG-PRE-PR-NULL
            trchDiGar.getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtUltAdegPrePr.Len.TGA_DT_ULT_ADEG_PRE_PR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-AGG-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzAggUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-ULT-NULL
            trchDiGar.getTgaPrstzAggUlt().setTgaPrstzAggUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzAggUlt.Len.TGA_PRSTZ_AGG_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-TS-RIVAL-NET = -1
        //              MOVE HIGH-VALUES TO TGA-TS-RIVAL-NET-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTsRivalNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TS-RIVAL-NET-NULL
            trchDiGar.getTgaTsRivalNet().setTgaTsRivalNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaTsRivalNet.Len.TGA_TS_RIVAL_NET_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-PATTUITO = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-PATTUITO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrePattuito() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-PATTUITO-NULL
            trchDiGar.getTgaPrePattuito().setTgaPrePattuitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrePattuito.Len.TGA_PRE_PATTUITO_NULL));
        }
        // COB_CODE: IF IND-TGA-TP-RIVAL = -1
        //              MOVE HIGH-VALUES TO TGA-TP-RIVAL-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTpRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TP-RIVAL-NULL
            trchDiGar.setTgaTpRival(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGar.Len.TGA_TP_RIVAL));
        }
        // COB_CODE: IF IND-TGA-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO TGA-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RIS-MAT-NULL
            trchDiGar.getTgaRisMat().setTgaRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRisMat.Len.TGA_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-TGA-CPT-MIN-SCAD = -1
        //              MOVE HIGH-VALUES TO TGA-CPT-MIN-SCAD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCptMinScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-CPT-MIN-SCAD-NULL
            trchDiGar.getTgaCptMinScad().setTgaCptMinScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCptMinScad.Len.TGA_CPT_MIN_SCAD_NULL));
        }
        // COB_CODE: IF IND-TGA-COMMIS-GEST = -1
        //              MOVE HIGH-VALUES TO TGA-COMMIS-GEST-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCommisGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COMMIS-GEST-NULL
            trchDiGar.getTgaCommisGest().setTgaCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCommisGest.Len.TGA_COMMIS_GEST_NULL));
        }
        // COB_CODE: IF IND-TGA-TP-MANFEE-APPL = -1
        //              MOVE HIGH-VALUES TO TGA-TP-MANFEE-APPL-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTpManfeeAppl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TP-MANFEE-APPL-NULL
            trchDiGar.setTgaTpManfeeAppl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGar.Len.TGA_TP_MANFEE_APPL));
        }
        // COB_CODE: IF IND-TGA-PC-COMMIS-GEST = -1
        //              MOVE HIGH-VALUES TO TGA-PC-COMMIS-GEST-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcCommisGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-COMMIS-GEST-NULL
            trchDiGar.getTgaPcCommisGest().setTgaPcCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcCommisGest.Len.TGA_PC_COMMIS_GEST_NULL));
        }
        // COB_CODE: IF IND-TGA-NUM-GG-RIVAL = -1
        //              MOVE HIGH-VALUES TO TGA-NUM-GG-RIVAL-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-NUM-GG-RIVAL-NULL
            trchDiGar.getTgaNumGgRival().setTgaNumGgRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaNumGgRival.Len.TGA_NUM_GG_RIVAL_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-TRASFE-NULL
            trchDiGar.getTgaImpTrasfe().setTgaImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpTrasfe.Len.TGA_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-TFR-STRC-NULL
            trchDiGar.getTgaImpTfrStrc().setTgaImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpTfrStrc.Len.TGA_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-TGA-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO TGA-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ACQ-EXP-NULL
            trchDiGar.getTgaAcqExp().setTgaAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAcqExp.Len.TGA_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-TGA-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TGA-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-REMUN-ASS-NULL
            trchDiGar.getTgaRemunAss().setTgaRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRemunAss.Len.TGA_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TGA-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TGA-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COMMIS-INTER-NULL
            trchDiGar.getTgaCommisInter().setTgaCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCommisInter.Len.TGA_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-REMUN-ASS-NULL
            trchDiGar.getTgaAlqRemunAss().setTgaAlqRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqRemunAss.Len.TGA_ALQ_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-COMMIS-INTER-NULL
            trchDiGar.getTgaAlqCommisInter().setTgaAlqCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqCommisInter.Len.TGA_ALQ_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-REMUN-ASS-NULL
            trchDiGar.getTgaImpbRemunAss().setTgaImpbRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbRemunAss.Len.TGA_IMPB_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-COMMIS-INTER-NULL
        //           END-IF.
        if (ws.getIndTrchDiGar().getImpbCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-COMMIS-INTER-NULL
            trchDiGar.getTgaImpbCommisInter().setTgaImpbCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbCommisInter.Len.TGA_IMPB_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TGA-COS-RUN-ASSVA = -1
        //              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCosRunAssva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-NULL
            trchDiGar.getTgaCosRunAssva().setTgaCosRunAssvaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCosRunAssva.Len.TGA_COS_RUN_ASSVA_NULL));
        }
        // COB_CODE: IF IND-TGA-COS-RUN-ASSVA-IDC = -1
        //              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-IDC-NULL
        //           END-IF.
        if (ws.getIndTrchDiGar().getCosRunAssvaIdc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-IDC-NULL
            trchDiGar.getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCosRunAssvaIdc.Len.TGA_COS_RUN_ASSVA_IDC_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE TGA-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-INI-EFF
        trchDiGar.setTgaDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TGA-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-END-EFF
        trchDiGar.setTgaDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TGA-DT-DECOR-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getDecorDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-DECOR
        trchDiGar.setTgaDtDecor(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-TGA-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO TGA-DT-SCAD
        //           END-IF
        if (ws.getIndTrchDiGar().getDtScad() == 0) {
            // COB_CODE: MOVE TGA-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-SCAD
            trchDiGar.getTgaDtScad().setTgaDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-EMIS = 0
        //               MOVE WS-DATE-N      TO TGA-DT-EMIS
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEmis() == 0) {
            // COB_CODE: MOVE TGA-DT-EMIS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getEmisDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-EMIS
            trchDiGar.getTgaDtEmis().setTgaDtEmis(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-EFF-STAB = 0
        //               MOVE WS-DATE-N      TO TGA-DT-EFF-STAB
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEffStab() == 0) {
            // COB_CODE: MOVE TGA-DT-EFF-STAB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getEffStabDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-EFF-STAB
            trchDiGar.getTgaDtEffStab().setTgaDtEffStab(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-VLDT-PROD = 0
        //               MOVE WS-DATE-N      TO TGA-DT-VLDT-PROD
        //           END-IF
        if (ws.getIndTrchDiGar().getDtVldtProd() == 0) {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getVldtProdDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-VLDT-PROD
            trchDiGar.getTgaDtVldtProd().setTgaDtVldtProd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-INI-VAL-TAR = 0
        //               MOVE WS-DATE-N      TO TGA-DT-INI-VAL-TAR
        //           END-IF
        if (ws.getIndTrchDiGar().getDtIniValTar() == 0) {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-INI-VAL-TAR
            trchDiGar.getTgaDtIniValTar().setTgaDtIniValTar(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-ULT-ADEG-PRE-PR = 0
        //               MOVE WS-DATE-N      TO TGA-DT-ULT-ADEG-PRE-PR
        //           END-IF.
        if (ws.getIndTrchDiGar().getDtUltAdegPrePr() == 0) {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getUltAdegPrePrDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-ULT-ADEG-PRE-PR
            trchDiGar.getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePr(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getLc601IdMoviCrz() {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public void setLc601IdMoviCrz(int lc601IdMoviCrz) {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public int getLc601IdTrchDiGar() {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public void setLc601IdTrchDiGar(int lc601IdTrchDiGar) {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public int getLdbi0731IdAdes() {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public void setLdbi0731IdAdes(int ldbi0731IdAdes) {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public int getLdbi0731IdPoli() {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public void setLdbi0731IdPoli(int ldbi0731IdPoli) {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public int getLdbi0731IdTrch() {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public void setLdbi0731IdTrch(int ldbi0731IdTrch) {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public String getLdbi0731TpStatBus() {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    @Override
    public void setLdbi0731TpStatBus(String ldbi0731TpStatBus) {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    @Override
    public String getLdbv1361TpCaus01() {
        return ws.getLdbv1361().getLdbv1351TpCausTab().getCaus01();
    }

    @Override
    public void setLdbv1361TpCaus01(String ldbv1361TpCaus01) {
        this.ws.getLdbv1361().getLdbv1351TpCausTab().setCaus01(ldbv1361TpCaus01);
    }

    @Override
    public String getLdbv1361TpCaus02() {
        return ws.getLdbv1361().getLdbv1351TpCausTab().getCaus02();
    }

    @Override
    public void setLdbv1361TpCaus02(String ldbv1361TpCaus02) {
        this.ws.getLdbv1361().getLdbv1351TpCausTab().setCaus02(ldbv1361TpCaus02);
    }

    @Override
    public String getLdbv1361TpCaus03() {
        return ws.getLdbv1361().getLdbv1351TpCausTab().getCaus03();
    }

    @Override
    public void setLdbv1361TpCaus03(String ldbv1361TpCaus03) {
        this.ws.getLdbv1361().getLdbv1351TpCausTab().setCaus03(ldbv1361TpCaus03);
    }

    @Override
    public String getLdbv1361TpCaus04() {
        return ws.getLdbv1361().getLdbv1351TpCausTab().getCaus04();
    }

    @Override
    public void setLdbv1361TpCaus04(String ldbv1361TpCaus04) {
        this.ws.getLdbv1361().getLdbv1351TpCausTab().setCaus04(ldbv1361TpCaus04);
    }

    @Override
    public String getLdbv1361TpCaus05() {
        return ws.getLdbv1361().getLdbv1351TpCausTab().getCaus05();
    }

    @Override
    public void setLdbv1361TpCaus05(String ldbv1361TpCaus05) {
        this.ws.getLdbv1361().getLdbv1351TpCausTab().setCaus05(ldbv1361TpCaus05);
    }

    @Override
    public String getLdbv1361TpCaus06() {
        return ws.getLdbv1361().getLdbv1351TpCausTab().getCaus06();
    }

    @Override
    public void setLdbv1361TpCaus06(String ldbv1361TpCaus06) {
        this.ws.getLdbv1361().getLdbv1351TpCausTab().setCaus06(ldbv1361TpCaus06);
    }

    @Override
    public String getLdbv1361TpCaus07() {
        return ws.getLdbv1361().getLdbv1351TpCausTab().getCaus07();
    }

    @Override
    public void setLdbv1361TpCaus07(String ldbv1361TpCaus07) {
        this.ws.getLdbv1361().getLdbv1351TpCausTab().setCaus07(ldbv1361TpCaus07);
    }

    @Override
    public String getLdbv1361TpStatBus01() {
        return ws.getLdbv1361().getLdbv1351StatBusTab().getTpStatBus01();
    }

    @Override
    public void setLdbv1361TpStatBus01(String ldbv1361TpStatBus01) {
        this.ws.getLdbv1361().getLdbv1351StatBusTab().setTpStatBus01(ldbv1361TpStatBus01);
    }

    @Override
    public String getLdbv1361TpStatBus02() {
        return ws.getLdbv1361().getLdbv1351StatBusTab().getTpStatBus02();
    }

    @Override
    public void setLdbv1361TpStatBus02(String ldbv1361TpStatBus02) {
        this.ws.getLdbv1361().getLdbv1351StatBusTab().setTpStatBus02(ldbv1361TpStatBus02);
    }

    @Override
    public String getLdbv1361TpStatBus03() {
        return ws.getLdbv1361().getLdbv1351StatBusTab().getTpStatBus03();
    }

    @Override
    public void setLdbv1361TpStatBus03(String ldbv1361TpStatBus03) {
        this.ws.getLdbv1361().getLdbv1351StatBusTab().setTpStatBus03(ldbv1361TpStatBus03);
    }

    @Override
    public String getLdbv1361TpStatBus04() {
        return ws.getLdbv1361().getLdbv1351StatBusTab().getTpStatBus04();
    }

    @Override
    public void setLdbv1361TpStatBus04(String ldbv1361TpStatBus04) {
        this.ws.getLdbv1361().getLdbv1351StatBusTab().setTpStatBus04(ldbv1361TpStatBus04);
    }

    @Override
    public String getLdbv1361TpStatBus05() {
        return ws.getLdbv1361().getLdbv1351StatBusTab().getTpStatBus05();
    }

    @Override
    public void setLdbv1361TpStatBus05(String ldbv1361TpStatBus05) {
        this.ws.getLdbv1361().getLdbv1351StatBusTab().setTpStatBus05(ldbv1361TpStatBus05);
    }

    @Override
    public String getLdbv1361TpStatBus06() {
        return ws.getLdbv1361().getLdbv1351StatBusTab().getTpStatBus06();
    }

    @Override
    public void setLdbv1361TpStatBus06(String ldbv1361TpStatBus06) {
        this.ws.getLdbv1361().getLdbv1351StatBusTab().setTpStatBus06(ldbv1361TpStatBus06);
    }

    @Override
    public String getLdbv1361TpStatBus07() {
        return ws.getLdbv1361().getLdbv1351StatBusTab().getTpStatBus07();
    }

    @Override
    public void setLdbv1361TpStatBus07(String ldbv1361TpStatBus07) {
        this.ws.getLdbv1361().getLdbv1351StatBusTab().setTpStatBus07(ldbv1361TpStatBus07);
    }

    @Override
    public int getLdbv2911IdAdes() {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public void setLdbv2911IdAdes(int ldbv2911IdAdes) {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public int getLdbv2911IdPoli() {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public void setLdbv2911IdPoli(int ldbv2911IdPoli) {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public AfDecimal getLdbv2911PrstzIniNewfis() {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public void setLdbv2911PrstzIniNewfis(AfDecimal ldbv2911PrstzIniNewfis) {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public int getLdbv3021IdOgg() {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public void setLdbv3021IdOgg(int ldbv3021IdOgg) {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public String getLdbv3021TpCausBus10() {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public void setLdbv3021TpCausBus10(String ldbv3021TpCausBus10) {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public String getLdbv3021TpCausBus11() {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public void setLdbv3021TpCausBus11(String ldbv3021TpCausBus11) {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public String getLdbv3021TpCausBus12() {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public void setLdbv3021TpCausBus12(String ldbv3021TpCausBus12) {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public String getLdbv3021TpCausBus13() {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public void setLdbv3021TpCausBus13(String ldbv3021TpCausBus13) {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public String getLdbv3021TpCausBus14() {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public void setLdbv3021TpCausBus14(String ldbv3021TpCausBus14) {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public String getLdbv3021TpCausBus15() {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public void setLdbv3021TpCausBus15(String ldbv3021TpCausBus15) {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public String getLdbv3021TpCausBus16() {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public void setLdbv3021TpCausBus16(String ldbv3021TpCausBus16) {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public String getLdbv3021TpCausBus17() {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public void setLdbv3021TpCausBus17(String ldbv3021TpCausBus17) {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public String getLdbv3021TpCausBus18() {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public void setLdbv3021TpCausBus18(String ldbv3021TpCausBus18) {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public String getLdbv3021TpCausBus19() {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public void setLdbv3021TpCausBus19(String ldbv3021TpCausBus19) {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public String getLdbv3021TpCausBus1() {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public void setLdbv3021TpCausBus1(String ldbv3021TpCausBus1) {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public String getLdbv3021TpCausBus20() {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public void setLdbv3021TpCausBus20(String ldbv3021TpCausBus20) {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public String getLdbv3021TpCausBus2() {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public void setLdbv3021TpCausBus2(String ldbv3021TpCausBus2) {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public String getLdbv3021TpCausBus3() {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public void setLdbv3021TpCausBus3(String ldbv3021TpCausBus3) {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public String getLdbv3021TpCausBus4() {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public void setLdbv3021TpCausBus4(String ldbv3021TpCausBus4) {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public String getLdbv3021TpCausBus5() {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public void setLdbv3021TpCausBus5(String ldbv3021TpCausBus5) {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public String getLdbv3021TpCausBus6() {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public void setLdbv3021TpCausBus6(String ldbv3021TpCausBus6) {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public String getLdbv3021TpCausBus7() {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public void setLdbv3021TpCausBus7(String ldbv3021TpCausBus7) {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public String getLdbv3021TpCausBus8() {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public void setLdbv3021TpCausBus8(String ldbv3021TpCausBus8) {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public String getLdbv3021TpCausBus9() {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public void setLdbv3021TpCausBus9(String ldbv3021TpCausBus9) {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public String getLdbv3021TpOgg() {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public void setLdbv3021TpOgg(String ldbv3021TpOgg) {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public String getLdbv3021TpStatBus() {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public void setLdbv3021TpStatBus(String ldbv3021TpStatBus) {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public int getLdbv3421IdAdes() {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public void setLdbv3421IdAdes(int ldbv3421IdAdes) {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public int getLdbv3421IdGar() {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public void setLdbv3421IdGar(int ldbv3421IdGar) {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public int getLdbv3421IdPoli() {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public void setLdbv3421IdPoli(int ldbv3421IdPoli) {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public String getLdbv3421TpOgg() {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public void setLdbv3421TpOgg(String ldbv3421TpOgg) {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public String getLdbv3421TpStatBus1() {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public void setLdbv3421TpStatBus1(String ldbv3421TpStatBus1) {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public String getLdbv3421TpStatBus2() {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public void setLdbv3421TpStatBus2(String ldbv3421TpStatBus2) {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public int getLdbvd511IdAdes() {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public void setLdbvd511IdAdes(int ldbvd511IdAdes) {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public int getLdbvd511IdPoli() {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public void setLdbvd511IdPoli(int ldbvd511IdPoli) {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public String getLdbvd511TpCaus() {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public void setLdbvd511TpCaus(String ldbvd511TpCaus) {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public String getLdbvd511TpOgg() {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public void setLdbvd511TpOgg(String ldbvd511TpOgg) {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public String getLdbvd511TpStatBus() {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public void setLdbvd511TpStatBus(String ldbvd511TpStatBus) {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public int getLdbve251IdAdes() {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public void setLdbve251IdAdes(int ldbve251IdAdes) {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public int getLdbve251IdMovi() {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public void setLdbve251IdMovi(int ldbve251IdMovi) {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public AfDecimal getLdbve251ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public void setLdbve251ImpbVisEnd2000(AfDecimal ldbve251ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public String getLdbve251TpTrch10() {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public void setLdbve251TpTrch10(String ldbve251TpTrch10) {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public String getLdbve251TpTrch11() {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public void setLdbve251TpTrch11(String ldbve251TpTrch11) {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public String getLdbve251TpTrch12() {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public void setLdbve251TpTrch12(String ldbve251TpTrch12) {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public String getLdbve251TpTrch13() {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public void setLdbve251TpTrch13(String ldbve251TpTrch13) {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public String getLdbve251TpTrch14() {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public void setLdbve251TpTrch14(String ldbve251TpTrch14) {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public String getLdbve251TpTrch15() {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public void setLdbve251TpTrch15(String ldbve251TpTrch15) {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public String getLdbve251TpTrch16() {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public void setLdbve251TpTrch16(String ldbve251TpTrch16) {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public String getLdbve251TpTrch1() {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public void setLdbve251TpTrch1(String ldbve251TpTrch1) {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public String getLdbve251TpTrch2() {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public void setLdbve251TpTrch2(String ldbve251TpTrch2) {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public String getLdbve251TpTrch3() {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public void setLdbve251TpTrch3(String ldbve251TpTrch3) {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public String getLdbve251TpTrch4() {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public void setLdbve251TpTrch4(String ldbve251TpTrch4) {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public String getLdbve251TpTrch5() {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public void setLdbve251TpTrch5(String ldbve251TpTrch5) {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public String getLdbve251TpTrch6() {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public void setLdbve251TpTrch6(String ldbve251TpTrch6) {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public String getLdbve251TpTrch7() {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public void setLdbve251TpTrch7(String ldbve251TpTrch7) {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public String getLdbve251TpTrch8() {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public void setLdbve251TpTrch8(String ldbve251TpTrch8) {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public String getLdbve251TpTrch9() {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public void setLdbve251TpTrch9(String ldbve251TpTrch9) {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public int getLdbve261IdAdes() {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public void setLdbve261IdAdes(int ldbve261IdAdes) {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public AfDecimal getLdbve261ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public void setLdbve261ImpbVisEnd2000(AfDecimal ldbve261ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public String getLdbve261TpTrch10() {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public void setLdbve261TpTrch10(String ldbve261TpTrch10) {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public String getLdbve261TpTrch11() {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public void setLdbve261TpTrch11(String ldbve261TpTrch11) {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public String getLdbve261TpTrch12() {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public void setLdbve261TpTrch12(String ldbve261TpTrch12) {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public String getLdbve261TpTrch13() {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public void setLdbve261TpTrch13(String ldbve261TpTrch13) {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public String getLdbve261TpTrch14() {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public void setLdbve261TpTrch14(String ldbve261TpTrch14) {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public String getLdbve261TpTrch15() {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public void setLdbve261TpTrch15(String ldbve261TpTrch15) {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public String getLdbve261TpTrch16() {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public void setLdbve261TpTrch16(String ldbve261TpTrch16) {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public String getLdbve261TpTrch1() {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public void setLdbve261TpTrch1(String ldbve261TpTrch1) {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public String getLdbve261TpTrch2() {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public void setLdbve261TpTrch2(String ldbve261TpTrch2) {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public String getLdbve261TpTrch3() {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public void setLdbve261TpTrch3(String ldbve261TpTrch3) {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public String getLdbve261TpTrch4() {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public void setLdbve261TpTrch4(String ldbve261TpTrch4) {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public String getLdbve261TpTrch5() {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public void setLdbve261TpTrch5(String ldbve261TpTrch5) {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public String getLdbve261TpTrch6() {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public void setLdbve261TpTrch6(String ldbve261TpTrch6) {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public String getLdbve261TpTrch7() {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public void setLdbve261TpTrch7(String ldbve261TpTrch7) {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public String getLdbve261TpTrch8() {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public void setLdbve261TpTrch8(String ldbve261TpTrch8) {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public String getLdbve261TpTrch9() {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public void setLdbve261TpTrch9(String ldbve261TpTrch9) {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public int getStbCodCompAnia() {
        throw new FieldNotMappedException("stbCodCompAnia");
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        throw new FieldNotMappedException("stbCodCompAnia");
    }

    @Override
    public char getStbDsOperSql() {
        throw new FieldNotMappedException("stbDsOperSql");
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        throw new FieldNotMappedException("stbDsOperSql");
    }

    @Override
    public long getStbDsRiga() {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public char getStbDsStatoElab() {
        throw new FieldNotMappedException("stbDsStatoElab");
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        throw new FieldNotMappedException("stbDsStatoElab");
    }

    @Override
    public long getStbDsTsEndCptz() {
        throw new FieldNotMappedException("stbDsTsEndCptz");
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        throw new FieldNotMappedException("stbDsTsEndCptz");
    }

    @Override
    public long getStbDsTsIniCptz() {
        throw new FieldNotMappedException("stbDsTsIniCptz");
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        throw new FieldNotMappedException("stbDsTsIniCptz");
    }

    @Override
    public String getStbDsUtente() {
        throw new FieldNotMappedException("stbDsUtente");
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        throw new FieldNotMappedException("stbDsUtente");
    }

    @Override
    public int getStbDsVer() {
        throw new FieldNotMappedException("stbDsVer");
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        throw new FieldNotMappedException("stbDsVer");
    }

    @Override
    public String getStbDtEndEffDb() {
        throw new FieldNotMappedException("stbDtEndEffDb");
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        throw new FieldNotMappedException("stbDtEndEffDb");
    }

    @Override
    public String getStbDtIniEffDb() {
        throw new FieldNotMappedException("stbDtIniEffDb");
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        throw new FieldNotMappedException("stbDtIniEffDb");
    }

    @Override
    public int getStbIdMoviChiu() {
        throw new FieldNotMappedException("stbIdMoviChiu");
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        throw new FieldNotMappedException("stbIdMoviChiu");
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        return ((Integer)getStbIdMoviChiu());
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        setStbIdMoviChiu(((int)stbIdMoviChiuObj));
    }

    @Override
    public int getStbIdMoviCrz() {
        throw new FieldNotMappedException("stbIdMoviCrz");
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        throw new FieldNotMappedException("stbIdMoviCrz");
    }

    @Override
    public int getStbIdOgg() {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public int getStbIdStatOggBus() {
        throw new FieldNotMappedException("stbIdStatOggBus");
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        throw new FieldNotMappedException("stbIdStatOggBus");
    }

    @Override
    public String getStbTpCaus() {
        throw new FieldNotMappedException("stbTpCaus");
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        throw new FieldNotMappedException("stbTpCaus");
    }

    @Override
    public String getStbTpOgg() {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public String getStbTpStatBus() {
        throw new FieldNotMappedException("stbTpStatBus");
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        throw new FieldNotMappedException("stbTpStatBus");
    }

    @Override
    public AfDecimal getTgaAbbAnnuUlt() {
        return trchDiGar.getTgaAbbAnnuUlt().getTgaAbbAnnuUlt();
    }

    @Override
    public void setTgaAbbAnnuUlt(AfDecimal tgaAbbAnnuUlt) {
        this.trchDiGar.getTgaAbbAnnuUlt().setTgaAbbAnnuUlt(tgaAbbAnnuUlt.copy());
    }

    @Override
    public AfDecimal getTgaAbbAnnuUltObj() {
        if (ws.getIndTrchDiGar().getAbbAnnuUlt() >= 0) {
            return getTgaAbbAnnuUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbAnnuUltObj(AfDecimal tgaAbbAnnuUltObj) {
        if (tgaAbbAnnuUltObj != null) {
            setTgaAbbAnnuUlt(new AfDecimal(tgaAbbAnnuUltObj, 15, 3));
            ws.getIndTrchDiGar().setAbbAnnuUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAbbAnnuUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAbbTotIni() {
        return trchDiGar.getTgaAbbTotIni().getTgaAbbTotIni();
    }

    @Override
    public void setTgaAbbTotIni(AfDecimal tgaAbbTotIni) {
        this.trchDiGar.getTgaAbbTotIni().setTgaAbbTotIni(tgaAbbTotIni.copy());
    }

    @Override
    public AfDecimal getTgaAbbTotIniObj() {
        if (ws.getIndTrchDiGar().getAbbTotIni() >= 0) {
            return getTgaAbbTotIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbTotIniObj(AfDecimal tgaAbbTotIniObj) {
        if (tgaAbbTotIniObj != null) {
            setTgaAbbTotIni(new AfDecimal(tgaAbbTotIniObj, 15, 3));
            ws.getIndTrchDiGar().setAbbTotIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAbbTotIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAbbTotUlt() {
        return trchDiGar.getTgaAbbTotUlt().getTgaAbbTotUlt();
    }

    @Override
    public void setTgaAbbTotUlt(AfDecimal tgaAbbTotUlt) {
        this.trchDiGar.getTgaAbbTotUlt().setTgaAbbTotUlt(tgaAbbTotUlt.copy());
    }

    @Override
    public AfDecimal getTgaAbbTotUltObj() {
        if (ws.getIndTrchDiGar().getAbbTotUlt() >= 0) {
            return getTgaAbbTotUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbTotUltObj(AfDecimal tgaAbbTotUltObj) {
        if (tgaAbbTotUltObj != null) {
            setTgaAbbTotUlt(new AfDecimal(tgaAbbTotUltObj, 15, 3));
            ws.getIndTrchDiGar().setAbbTotUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAbbTotUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAcqExp() {
        return trchDiGar.getTgaAcqExp().getTgaAcqExp();
    }

    @Override
    public void setTgaAcqExp(AfDecimal tgaAcqExp) {
        this.trchDiGar.getTgaAcqExp().setTgaAcqExp(tgaAcqExp.copy());
    }

    @Override
    public AfDecimal getTgaAcqExpObj() {
        if (ws.getIndTrchDiGar().getAcqExp() >= 0) {
            return getTgaAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAcqExpObj(AfDecimal tgaAcqExpObj) {
        if (tgaAcqExpObj != null) {
            setTgaAcqExp(new AfDecimal(tgaAcqExpObj, 15, 3));
            ws.getIndTrchDiGar().setAcqExp(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqCommisInter() {
        return trchDiGar.getTgaAlqCommisInter().getTgaAlqCommisInter();
    }

    @Override
    public void setTgaAlqCommisInter(AfDecimal tgaAlqCommisInter) {
        this.trchDiGar.getTgaAlqCommisInter().setTgaAlqCommisInter(tgaAlqCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaAlqCommisInterObj() {
        if (ws.getIndTrchDiGar().getAlqCommisInter() >= 0) {
            return getTgaAlqCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqCommisInterObj(AfDecimal tgaAlqCommisInterObj) {
        if (tgaAlqCommisInterObj != null) {
            setTgaAlqCommisInter(new AfDecimal(tgaAlqCommisInterObj, 6, 3));
            ws.getIndTrchDiGar().setAlqCommisInter(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvAcq() {
        return trchDiGar.getTgaAlqProvAcq().getTgaAlqProvAcq();
    }

    @Override
    public void setTgaAlqProvAcq(AfDecimal tgaAlqProvAcq) {
        this.trchDiGar.getTgaAlqProvAcq().setTgaAlqProvAcq(tgaAlqProvAcq.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvAcqObj() {
        if (ws.getIndTrchDiGar().getAlqProvAcq() >= 0) {
            return getTgaAlqProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvAcqObj(AfDecimal tgaAlqProvAcqObj) {
        if (tgaAlqProvAcqObj != null) {
            setTgaAlqProvAcq(new AfDecimal(tgaAlqProvAcqObj, 6, 3));
            ws.getIndTrchDiGar().setAlqProvAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvInc() {
        return trchDiGar.getTgaAlqProvInc().getTgaAlqProvInc();
    }

    @Override
    public void setTgaAlqProvInc(AfDecimal tgaAlqProvInc) {
        this.trchDiGar.getTgaAlqProvInc().setTgaAlqProvInc(tgaAlqProvInc.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvIncObj() {
        if (ws.getIndTrchDiGar().getAlqProvInc() >= 0) {
            return getTgaAlqProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvIncObj(AfDecimal tgaAlqProvIncObj) {
        if (tgaAlqProvIncObj != null) {
            setTgaAlqProvInc(new AfDecimal(tgaAlqProvIncObj, 6, 3));
            ws.getIndTrchDiGar().setAlqProvInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvRicor() {
        return trchDiGar.getTgaAlqProvRicor().getTgaAlqProvRicor();
    }

    @Override
    public void setTgaAlqProvRicor(AfDecimal tgaAlqProvRicor) {
        this.trchDiGar.getTgaAlqProvRicor().setTgaAlqProvRicor(tgaAlqProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvRicorObj() {
        if (ws.getIndTrchDiGar().getAlqProvRicor() >= 0) {
            return getTgaAlqProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvRicorObj(AfDecimal tgaAlqProvRicorObj) {
        if (tgaAlqProvRicorObj != null) {
            setTgaAlqProvRicor(new AfDecimal(tgaAlqProvRicorObj, 6, 3));
            ws.getIndTrchDiGar().setAlqProvRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqRemunAss() {
        return trchDiGar.getTgaAlqRemunAss().getTgaAlqRemunAss();
    }

    @Override
    public void setTgaAlqRemunAss(AfDecimal tgaAlqRemunAss) {
        this.trchDiGar.getTgaAlqRemunAss().setTgaAlqRemunAss(tgaAlqRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaAlqRemunAssObj() {
        if (ws.getIndTrchDiGar().getAlqRemunAss() >= 0) {
            return getTgaAlqRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqRemunAssObj(AfDecimal tgaAlqRemunAssObj) {
        if (tgaAlqRemunAssObj != null) {
            setTgaAlqRemunAss(new AfDecimal(tgaAlqRemunAssObj, 6, 3));
            ws.getIndTrchDiGar().setAlqRemunAss(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqScon() {
        return trchDiGar.getTgaAlqScon().getTgaAlqScon();
    }

    @Override
    public void setTgaAlqScon(AfDecimal tgaAlqScon) {
        this.trchDiGar.getTgaAlqScon().setTgaAlqScon(tgaAlqScon.copy());
    }

    @Override
    public AfDecimal getTgaAlqSconObj() {
        if (ws.getIndTrchDiGar().getAlqScon() >= 0) {
            return getTgaAlqScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqSconObj(AfDecimal tgaAlqSconObj) {
        if (tgaAlqSconObj != null) {
            setTgaAlqScon(new AfDecimal(tgaAlqSconObj, 6, 3));
            ws.getIndTrchDiGar().setAlqScon(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqto() {
        return trchDiGar.getTgaBnsGiaLiqto().getTgaBnsGiaLiqto();
    }

    @Override
    public void setTgaBnsGiaLiqto(AfDecimal tgaBnsGiaLiqto) {
        this.trchDiGar.getTgaBnsGiaLiqto().setTgaBnsGiaLiqto(tgaBnsGiaLiqto.copy());
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqtoObj() {
        if (ws.getIndTrchDiGar().getBnsGiaLiqto() >= 0) {
            return getTgaBnsGiaLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaBnsGiaLiqtoObj(AfDecimal tgaBnsGiaLiqtoObj) {
        if (tgaBnsGiaLiqtoObj != null) {
            setTgaBnsGiaLiqto(new AfDecimal(tgaBnsGiaLiqtoObj, 15, 3));
            ws.getIndTrchDiGar().setBnsGiaLiqto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setBnsGiaLiqto(((short)-1));
        }
    }

    @Override
    public int getTgaCodCompAnia() {
        return trchDiGar.getTgaCodCompAnia();
    }

    @Override
    public void setTgaCodCompAnia(int tgaCodCompAnia) {
        this.trchDiGar.setTgaCodCompAnia(tgaCodCompAnia);
    }

    @Override
    public String getTgaCodDvs() {
        return trchDiGar.getTgaCodDvs();
    }

    @Override
    public void setTgaCodDvs(String tgaCodDvs) {
        this.trchDiGar.setTgaCodDvs(tgaCodDvs);
    }

    @Override
    public AfDecimal getTgaCommisGest() {
        return trchDiGar.getTgaCommisGest().getTgaCommisGest();
    }

    @Override
    public void setTgaCommisGest(AfDecimal tgaCommisGest) {
        this.trchDiGar.getTgaCommisGest().setTgaCommisGest(tgaCommisGest.copy());
    }

    @Override
    public AfDecimal getTgaCommisGestObj() {
        if (ws.getIndTrchDiGar().getCommisGest() >= 0) {
            return getTgaCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCommisGestObj(AfDecimal tgaCommisGestObj) {
        if (tgaCommisGestObj != null) {
            setTgaCommisGest(new AfDecimal(tgaCommisGestObj, 15, 3));
            ws.getIndTrchDiGar().setCommisGest(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCommisInter() {
        return trchDiGar.getTgaCommisInter().getTgaCommisInter();
    }

    @Override
    public void setTgaCommisInter(AfDecimal tgaCommisInter) {
        this.trchDiGar.getTgaCommisInter().setTgaCommisInter(tgaCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaCommisInterObj() {
        if (ws.getIndTrchDiGar().getCommisInter() >= 0) {
            return getTgaCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCommisInterObj(AfDecimal tgaCommisInterObj) {
        if (tgaCommisInterObj != null) {
            setTgaCommisInter(new AfDecimal(tgaCommisInterObj, 15, 3));
            ws.getIndTrchDiGar().setCommisInter(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCosRunAssva() {
        return trchDiGar.getTgaCosRunAssva().getTgaCosRunAssva();
    }

    @Override
    public void setTgaCosRunAssva(AfDecimal tgaCosRunAssva) {
        this.trchDiGar.getTgaCosRunAssva().setTgaCosRunAssva(tgaCosRunAssva.copy());
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdc() {
        return trchDiGar.getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc();
    }

    @Override
    public void setTgaCosRunAssvaIdc(AfDecimal tgaCosRunAssvaIdc) {
        this.trchDiGar.getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdc(tgaCosRunAssvaIdc.copy());
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdcObj() {
        if (ws.getIndTrchDiGar().getCosRunAssvaIdc() >= 0) {
            return getTgaCosRunAssvaIdc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCosRunAssvaIdcObj(AfDecimal tgaCosRunAssvaIdcObj) {
        if (tgaCosRunAssvaIdcObj != null) {
            setTgaCosRunAssvaIdc(new AfDecimal(tgaCosRunAssvaIdcObj, 15, 3));
            ws.getIndTrchDiGar().setCosRunAssvaIdc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCosRunAssvaIdc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCosRunAssvaObj() {
        if (ws.getIndTrchDiGar().getCosRunAssva() >= 0) {
            return getTgaCosRunAssva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCosRunAssvaObj(AfDecimal tgaCosRunAssvaObj) {
        if (tgaCosRunAssvaObj != null) {
            setTgaCosRunAssva(new AfDecimal(tgaCosRunAssvaObj, 15, 3));
            ws.getIndTrchDiGar().setCosRunAssva(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCosRunAssva(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptInOpzRivto() {
        return trchDiGar.getTgaCptInOpzRivto().getTgaCptInOpzRivto();
    }

    @Override
    public void setTgaCptInOpzRivto(AfDecimal tgaCptInOpzRivto) {
        this.trchDiGar.getTgaCptInOpzRivto().setTgaCptInOpzRivto(tgaCptInOpzRivto.copy());
    }

    @Override
    public AfDecimal getTgaCptInOpzRivtoObj() {
        if (ws.getIndTrchDiGar().getCptInOpzRivto() >= 0) {
            return getTgaCptInOpzRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptInOpzRivtoObj(AfDecimal tgaCptInOpzRivtoObj) {
        if (tgaCptInOpzRivtoObj != null) {
            setTgaCptInOpzRivto(new AfDecimal(tgaCptInOpzRivtoObj, 15, 3));
            ws.getIndTrchDiGar().setCptInOpzRivto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCptInOpzRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptMinScad() {
        return trchDiGar.getTgaCptMinScad().getTgaCptMinScad();
    }

    @Override
    public void setTgaCptMinScad(AfDecimal tgaCptMinScad) {
        this.trchDiGar.getTgaCptMinScad().setTgaCptMinScad(tgaCptMinScad.copy());
    }

    @Override
    public AfDecimal getTgaCptMinScadObj() {
        if (ws.getIndTrchDiGar().getCptMinScad() >= 0) {
            return getTgaCptMinScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptMinScadObj(AfDecimal tgaCptMinScadObj) {
        if (tgaCptMinScadObj != null) {
            setTgaCptMinScad(new AfDecimal(tgaCptMinScadObj, 15, 3));
            ws.getIndTrchDiGar().setCptMinScad(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCptMinScad(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptRshMor() {
        return trchDiGar.getTgaCptRshMor().getTgaCptRshMor();
    }

    @Override
    public void setTgaCptRshMor(AfDecimal tgaCptRshMor) {
        this.trchDiGar.getTgaCptRshMor().setTgaCptRshMor(tgaCptRshMor.copy());
    }

    @Override
    public AfDecimal getTgaCptRshMorObj() {
        if (ws.getIndTrchDiGar().getCptRshMor() >= 0) {
            return getTgaCptRshMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptRshMorObj(AfDecimal tgaCptRshMorObj) {
        if (tgaCptRshMorObj != null) {
            setTgaCptRshMor(new AfDecimal(tgaCptRshMorObj, 15, 3));
            ws.getIndTrchDiGar().setCptRshMor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCptRshMor(((short)-1));
        }
    }

    @Override
    public char getTgaDsOperSql() {
        return trchDiGar.getTgaDsOperSql();
    }

    @Override
    public void setTgaDsOperSql(char tgaDsOperSql) {
        this.trchDiGar.setTgaDsOperSql(tgaDsOperSql);
    }

    @Override
    public long getTgaDsRiga() {
        return trchDiGar.getTgaDsRiga();
    }

    @Override
    public void setTgaDsRiga(long tgaDsRiga) {
        this.trchDiGar.setTgaDsRiga(tgaDsRiga);
    }

    @Override
    public char getTgaDsStatoElab() {
        return trchDiGar.getTgaDsStatoElab();
    }

    @Override
    public void setTgaDsStatoElab(char tgaDsStatoElab) {
        this.trchDiGar.setTgaDsStatoElab(tgaDsStatoElab);
    }

    @Override
    public long getTgaDsTsEndCptz() {
        return trchDiGar.getTgaDsTsEndCptz();
    }

    @Override
    public void setTgaDsTsEndCptz(long tgaDsTsEndCptz) {
        this.trchDiGar.setTgaDsTsEndCptz(tgaDsTsEndCptz);
    }

    @Override
    public long getTgaDsTsIniCptz() {
        return trchDiGar.getTgaDsTsIniCptz();
    }

    @Override
    public void setTgaDsTsIniCptz(long tgaDsTsIniCptz) {
        this.trchDiGar.setTgaDsTsIniCptz(tgaDsTsIniCptz);
    }

    @Override
    public String getTgaDsUtente() {
        return trchDiGar.getTgaDsUtente();
    }

    @Override
    public void setTgaDsUtente(String tgaDsUtente) {
        this.trchDiGar.setTgaDsUtente(tgaDsUtente);
    }

    @Override
    public int getTgaDsVer() {
        return trchDiGar.getTgaDsVer();
    }

    @Override
    public void setTgaDsVer(int tgaDsVer) {
        this.trchDiGar.setTgaDsVer(tgaDsVer);
    }

    @Override
    public String getTgaDtDecorDb() {
        return ws.getTrchDiGarDb().getDecorDb();
    }

    @Override
    public void setTgaDtDecorDb(String tgaDtDecorDb) {
        this.ws.getTrchDiGarDb().setDecorDb(tgaDtDecorDb);
    }

    @Override
    public String getTgaDtEffStabDb() {
        return ws.getTrchDiGarDb().getEffStabDb();
    }

    @Override
    public void setTgaDtEffStabDb(String tgaDtEffStabDb) {
        this.ws.getTrchDiGarDb().setEffStabDb(tgaDtEffStabDb);
    }

    @Override
    public String getTgaDtEffStabDbObj() {
        if (ws.getIndTrchDiGar().getDtEffStab() >= 0) {
            return getTgaDtEffStabDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtEffStabDbObj(String tgaDtEffStabDbObj) {
        if (tgaDtEffStabDbObj != null) {
            setTgaDtEffStabDb(tgaDtEffStabDbObj);
            ws.getIndTrchDiGar().setDtEffStab(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtEffStab(((short)-1));
        }
    }

    @Override
    public String getTgaDtEmisDb() {
        return ws.getTrchDiGarDb().getEmisDb();
    }

    @Override
    public void setTgaDtEmisDb(String tgaDtEmisDb) {
        this.ws.getTrchDiGarDb().setEmisDb(tgaDtEmisDb);
    }

    @Override
    public String getTgaDtEmisDbObj() {
        if (ws.getIndTrchDiGar().getDtEmis() >= 0) {
            return getTgaDtEmisDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtEmisDbObj(String tgaDtEmisDbObj) {
        if (tgaDtEmisDbObj != null) {
            setTgaDtEmisDb(tgaDtEmisDbObj);
            ws.getIndTrchDiGar().setDtEmis(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtEmis(((short)-1));
        }
    }

    @Override
    public String getTgaDtEndEffDb() {
        return ws.getTrchDiGarDb().getEndEffDb();
    }

    @Override
    public void setTgaDtEndEffDb(String tgaDtEndEffDb) {
        this.ws.getTrchDiGarDb().setEndEffDb(tgaDtEndEffDb);
    }

    @Override
    public String getTgaDtIniEffDb() {
        return ws.getTrchDiGarDb().getIniEffDb();
    }

    @Override
    public void setTgaDtIniEffDb(String tgaDtIniEffDb) {
        this.ws.getTrchDiGarDb().setIniEffDb(tgaDtIniEffDb);
    }

    @Override
    public String getTgaDtIniValTarDb() {
        return ws.getTrchDiGarDb().getIniValTarDb();
    }

    @Override
    public void setTgaDtIniValTarDb(String tgaDtIniValTarDb) {
        this.ws.getTrchDiGarDb().setIniValTarDb(tgaDtIniValTarDb);
    }

    @Override
    public String getTgaDtIniValTarDbObj() {
        if (ws.getIndTrchDiGar().getDtIniValTar() >= 0) {
            return getTgaDtIniValTarDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtIniValTarDbObj(String tgaDtIniValTarDbObj) {
        if (tgaDtIniValTarDbObj != null) {
            setTgaDtIniValTarDb(tgaDtIniValTarDbObj);
            ws.getIndTrchDiGar().setDtIniValTar(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtIniValTar(((short)-1));
        }
    }

    @Override
    public String getTgaDtScadDb() {
        return ws.getTrchDiGarDb().getScadDb();
    }

    @Override
    public void setTgaDtScadDb(String tgaDtScadDb) {
        this.ws.getTrchDiGarDb().setScadDb(tgaDtScadDb);
    }

    @Override
    public String getTgaDtScadDbObj() {
        if (ws.getIndTrchDiGar().getDtScad() >= 0) {
            return getTgaDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtScadDbObj(String tgaDtScadDbObj) {
        if (tgaDtScadDbObj != null) {
            setTgaDtScadDb(tgaDtScadDbObj);
            ws.getIndTrchDiGar().setDtScad(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtScad(((short)-1));
        }
    }

    @Override
    public String getTgaDtUltAdegPrePrDb() {
        return ws.getTrchDiGarDb().getUltAdegPrePrDb();
    }

    @Override
    public void setTgaDtUltAdegPrePrDb(String tgaDtUltAdegPrePrDb) {
        this.ws.getTrchDiGarDb().setUltAdegPrePrDb(tgaDtUltAdegPrePrDb);
    }

    @Override
    public String getTgaDtUltAdegPrePrDbObj() {
        if (ws.getIndTrchDiGar().getDtUltAdegPrePr() >= 0) {
            return getTgaDtUltAdegPrePrDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtUltAdegPrePrDbObj(String tgaDtUltAdegPrePrDbObj) {
        if (tgaDtUltAdegPrePrDbObj != null) {
            setTgaDtUltAdegPrePrDb(tgaDtUltAdegPrePrDbObj);
            ws.getIndTrchDiGar().setDtUltAdegPrePr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtUltAdegPrePr(((short)-1));
        }
    }

    @Override
    public String getTgaDtVldtProdDb() {
        return ws.getTrchDiGarDb().getVldtProdDb();
    }

    @Override
    public void setTgaDtVldtProdDb(String tgaDtVldtProdDb) {
        this.ws.getTrchDiGarDb().setVldtProdDb(tgaDtVldtProdDb);
    }

    @Override
    public String getTgaDtVldtProdDbObj() {
        if (ws.getIndTrchDiGar().getDtVldtProd() >= 0) {
            return getTgaDtVldtProdDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtVldtProdDbObj(String tgaDtVldtProdDbObj) {
        if (tgaDtVldtProdDbObj != null) {
            setTgaDtVldtProdDb(tgaDtVldtProdDbObj);
            ws.getIndTrchDiGar().setDtVldtProd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtVldtProd(((short)-1));
        }
    }

    @Override
    public int getTgaDurAa() {
        return trchDiGar.getTgaDurAa().getTgaDurAa();
    }

    @Override
    public void setTgaDurAa(int tgaDurAa) {
        this.trchDiGar.getTgaDurAa().setTgaDurAa(tgaDurAa);
    }

    @Override
    public Integer getTgaDurAaObj() {
        if (ws.getIndTrchDiGar().getDurAa() >= 0) {
            return ((Integer)getTgaDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurAaObj(Integer tgaDurAaObj) {
        if (tgaDurAaObj != null) {
            setTgaDurAa(((int)tgaDurAaObj));
            ws.getIndTrchDiGar().setDurAa(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurAa(((short)-1));
        }
    }

    @Override
    public int getTgaDurAbb() {
        return trchDiGar.getTgaDurAbb().getTgaDurAbb();
    }

    @Override
    public void setTgaDurAbb(int tgaDurAbb) {
        this.trchDiGar.getTgaDurAbb().setTgaDurAbb(tgaDurAbb);
    }

    @Override
    public Integer getTgaDurAbbObj() {
        if (ws.getIndTrchDiGar().getDurAbb() >= 0) {
            return ((Integer)getTgaDurAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurAbbObj(Integer tgaDurAbbObj) {
        if (tgaDurAbbObj != null) {
            setTgaDurAbb(((int)tgaDurAbbObj));
            ws.getIndTrchDiGar().setDurAbb(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurAbb(((short)-1));
        }
    }

    @Override
    public int getTgaDurGg() {
        return trchDiGar.getTgaDurGg().getTgaDurGg();
    }

    @Override
    public void setTgaDurGg(int tgaDurGg) {
        this.trchDiGar.getTgaDurGg().setTgaDurGg(tgaDurGg);
    }

    @Override
    public Integer getTgaDurGgObj() {
        if (ws.getIndTrchDiGar().getDurGg() >= 0) {
            return ((Integer)getTgaDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurGgObj(Integer tgaDurGgObj) {
        if (tgaDurGgObj != null) {
            setTgaDurGg(((int)tgaDurGgObj));
            ws.getIndTrchDiGar().setDurGg(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurGg(((short)-1));
        }
    }

    @Override
    public int getTgaDurMm() {
        return trchDiGar.getTgaDurMm().getTgaDurMm();
    }

    @Override
    public void setTgaDurMm(int tgaDurMm) {
        this.trchDiGar.getTgaDurMm().setTgaDurMm(tgaDurMm);
    }

    @Override
    public Integer getTgaDurMmObj() {
        if (ws.getIndTrchDiGar().getDurMm() >= 0) {
            return ((Integer)getTgaDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurMmObj(Integer tgaDurMmObj) {
        if (tgaDurMmObj != null) {
            setTgaDurMm(((int)tgaDurMmObj));
            ws.getIndTrchDiGar().setDurMm(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurMm(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa1oAssto() {
        return trchDiGar.getTgaEtaAa1oAssto().getTgaEtaAa1oAssto();
    }

    @Override
    public void setTgaEtaAa1oAssto(short tgaEtaAa1oAssto) {
        this.trchDiGar.getTgaEtaAa1oAssto().setTgaEtaAa1oAssto(tgaEtaAa1oAssto);
    }

    @Override
    public Short getTgaEtaAa1oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaAa1oAssto() >= 0) {
            return ((Short)getTgaEtaAa1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa1oAsstoObj(Short tgaEtaAa1oAsstoObj) {
        if (tgaEtaAa1oAsstoObj != null) {
            setTgaEtaAa1oAssto(((short)tgaEtaAa1oAsstoObj));
            ws.getIndTrchDiGar().setEtaAa1oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaAa1oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa2oAssto() {
        return trchDiGar.getTgaEtaAa2oAssto().getTgaEtaAa2oAssto();
    }

    @Override
    public void setTgaEtaAa2oAssto(short tgaEtaAa2oAssto) {
        this.trchDiGar.getTgaEtaAa2oAssto().setTgaEtaAa2oAssto(tgaEtaAa2oAssto);
    }

    @Override
    public Short getTgaEtaAa2oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaAa2oAssto() >= 0) {
            return ((Short)getTgaEtaAa2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa2oAsstoObj(Short tgaEtaAa2oAsstoObj) {
        if (tgaEtaAa2oAsstoObj != null) {
            setTgaEtaAa2oAssto(((short)tgaEtaAa2oAsstoObj));
            ws.getIndTrchDiGar().setEtaAa2oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaAa2oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa3oAssto() {
        return trchDiGar.getTgaEtaAa3oAssto().getTgaEtaAa3oAssto();
    }

    @Override
    public void setTgaEtaAa3oAssto(short tgaEtaAa3oAssto) {
        this.trchDiGar.getTgaEtaAa3oAssto().setTgaEtaAa3oAssto(tgaEtaAa3oAssto);
    }

    @Override
    public Short getTgaEtaAa3oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaAa3oAssto() >= 0) {
            return ((Short)getTgaEtaAa3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa3oAsstoObj(Short tgaEtaAa3oAsstoObj) {
        if (tgaEtaAa3oAsstoObj != null) {
            setTgaEtaAa3oAssto(((short)tgaEtaAa3oAsstoObj));
            ws.getIndTrchDiGar().setEtaAa3oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaAa3oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm1oAssto() {
        return trchDiGar.getTgaEtaMm1oAssto().getTgaEtaMm1oAssto();
    }

    @Override
    public void setTgaEtaMm1oAssto(short tgaEtaMm1oAssto) {
        this.trchDiGar.getTgaEtaMm1oAssto().setTgaEtaMm1oAssto(tgaEtaMm1oAssto);
    }

    @Override
    public Short getTgaEtaMm1oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaMm1oAssto() >= 0) {
            return ((Short)getTgaEtaMm1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm1oAsstoObj(Short tgaEtaMm1oAsstoObj) {
        if (tgaEtaMm1oAsstoObj != null) {
            setTgaEtaMm1oAssto(((short)tgaEtaMm1oAsstoObj));
            ws.getIndTrchDiGar().setEtaMm1oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaMm1oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm2oAssto() {
        return trchDiGar.getTgaEtaMm2oAssto().getTgaEtaMm2oAssto();
    }

    @Override
    public void setTgaEtaMm2oAssto(short tgaEtaMm2oAssto) {
        this.trchDiGar.getTgaEtaMm2oAssto().setTgaEtaMm2oAssto(tgaEtaMm2oAssto);
    }

    @Override
    public Short getTgaEtaMm2oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaMm2oAssto() >= 0) {
            return ((Short)getTgaEtaMm2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm2oAsstoObj(Short tgaEtaMm2oAsstoObj) {
        if (tgaEtaMm2oAsstoObj != null) {
            setTgaEtaMm2oAssto(((short)tgaEtaMm2oAsstoObj));
            ws.getIndTrchDiGar().setEtaMm2oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaMm2oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm3oAssto() {
        return trchDiGar.getTgaEtaMm3oAssto().getTgaEtaMm3oAssto();
    }

    @Override
    public void setTgaEtaMm3oAssto(short tgaEtaMm3oAssto) {
        this.trchDiGar.getTgaEtaMm3oAssto().setTgaEtaMm3oAssto(tgaEtaMm3oAssto);
    }

    @Override
    public Short getTgaEtaMm3oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaMm3oAssto() >= 0) {
            return ((Short)getTgaEtaMm3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm3oAsstoObj(Short tgaEtaMm3oAsstoObj) {
        if (tgaEtaMm3oAsstoObj != null) {
            setTgaEtaMm3oAssto(((short)tgaEtaMm3oAsstoObj));
            ws.getIndTrchDiGar().setEtaMm3oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaMm3oAssto(((short)-1));
        }
    }

    @Override
    public char getTgaFlCarCont() {
        return trchDiGar.getTgaFlCarCont();
    }

    @Override
    public void setTgaFlCarCont(char tgaFlCarCont) {
        this.trchDiGar.setTgaFlCarCont(tgaFlCarCont);
    }

    @Override
    public Character getTgaFlCarContObj() {
        if (ws.getIndTrchDiGar().getFlCarCont() >= 0) {
            return ((Character)getTgaFlCarCont());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlCarContObj(Character tgaFlCarContObj) {
        if (tgaFlCarContObj != null) {
            setTgaFlCarCont(((char)tgaFlCarContObj));
            ws.getIndTrchDiGar().setFlCarCont(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setFlCarCont(((short)-1));
        }
    }

    @Override
    public char getTgaFlImportiForz() {
        return trchDiGar.getTgaFlImportiForz();
    }

    @Override
    public void setTgaFlImportiForz(char tgaFlImportiForz) {
        this.trchDiGar.setTgaFlImportiForz(tgaFlImportiForz);
    }

    @Override
    public Character getTgaFlImportiForzObj() {
        if (ws.getIndTrchDiGar().getFlImportiForz() >= 0) {
            return ((Character)getTgaFlImportiForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlImportiForzObj(Character tgaFlImportiForzObj) {
        if (tgaFlImportiForzObj != null) {
            setTgaFlImportiForz(((char)tgaFlImportiForzObj));
            ws.getIndTrchDiGar().setFlImportiForz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setFlImportiForz(((short)-1));
        }
    }

    @Override
    public char getTgaFlProvForz() {
        return trchDiGar.getTgaFlProvForz();
    }

    @Override
    public void setTgaFlProvForz(char tgaFlProvForz) {
        this.trchDiGar.setTgaFlProvForz(tgaFlProvForz);
    }

    @Override
    public Character getTgaFlProvForzObj() {
        if (ws.getIndTrchDiGar().getFlProvForz() >= 0) {
            return ((Character)getTgaFlProvForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlProvForzObj(Character tgaFlProvForzObj) {
        if (tgaFlProvForzObj != null) {
            setTgaFlProvForz(((char)tgaFlProvForzObj));
            ws.getIndTrchDiGar().setFlProvForz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setFlProvForz(((short)-1));
        }
    }

    @Override
    public String getTgaIbOgg() {
        return trchDiGar.getTgaIbOgg();
    }

    @Override
    public void setTgaIbOgg(String tgaIbOgg) {
        this.trchDiGar.setTgaIbOgg(tgaIbOgg);
    }

    @Override
    public String getTgaIbOggObj() {
        if (ws.getIndTrchDiGar().getIbOgg() >= 0) {
            return getTgaIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIbOggObj(String tgaIbOggObj) {
        if (tgaIbOggObj != null) {
            setTgaIbOgg(tgaIbOggObj);
            ws.getIndTrchDiGar().setIbOgg(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getTgaIdAdes() {
        return trchDiGar.getTgaIdAdes();
    }

    @Override
    public void setTgaIdAdes(int tgaIdAdes) {
        this.trchDiGar.setTgaIdAdes(tgaIdAdes);
    }

    @Override
    public int getTgaIdGar() {
        return trchDiGar.getTgaIdGar();
    }

    @Override
    public void setTgaIdGar(int tgaIdGar) {
        this.trchDiGar.setTgaIdGar(tgaIdGar);
    }

    @Override
    public int getTgaIdMoviChiu() {
        return trchDiGar.getTgaIdMoviChiu().getTgaIdMoviChiu();
    }

    @Override
    public void setTgaIdMoviChiu(int tgaIdMoviChiu) {
        this.trchDiGar.getTgaIdMoviChiu().setTgaIdMoviChiu(tgaIdMoviChiu);
    }

    @Override
    public Integer getTgaIdMoviChiuObj() {
        if (ws.getIndTrchDiGar().getIdMoviChiu() >= 0) {
            return ((Integer)getTgaIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIdMoviChiuObj(Integer tgaIdMoviChiuObj) {
        if (tgaIdMoviChiuObj != null) {
            setTgaIdMoviChiu(((int)tgaIdMoviChiuObj));
            ws.getIndTrchDiGar().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getTgaIdMoviCrz() {
        return trchDiGar.getTgaIdMoviCrz();
    }

    @Override
    public void setTgaIdMoviCrz(int tgaIdMoviCrz) {
        this.trchDiGar.setTgaIdMoviCrz(tgaIdMoviCrz);
    }

    @Override
    public int getTgaIdPoli() {
        return trchDiGar.getTgaIdPoli();
    }

    @Override
    public void setTgaIdPoli(int tgaIdPoli) {
        this.trchDiGar.setTgaIdPoli(tgaIdPoli);
    }

    @Override
    public int getTgaIdTrchDiGar() {
        return trchDiGar.getTgaIdTrchDiGar();
    }

    @Override
    public void setTgaIdTrchDiGar(int tgaIdTrchDiGar) {
        this.trchDiGar.setTgaIdTrchDiGar(tgaIdTrchDiGar);
    }

    @Override
    public AfDecimal getTgaImpAder() {
        return trchDiGar.getTgaImpAder().getTgaImpAder();
    }

    @Override
    public void setTgaImpAder(AfDecimal tgaImpAder) {
        this.trchDiGar.getTgaImpAder().setTgaImpAder(tgaImpAder.copy());
    }

    @Override
    public AfDecimal getTgaImpAderObj() {
        if (ws.getIndTrchDiGar().getImpAder() >= 0) {
            return getTgaImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAderObj(AfDecimal tgaImpAderObj) {
        if (tgaImpAderObj != null) {
            setTgaImpAder(new AfDecimal(tgaImpAderObj, 15, 3));
            ws.getIndTrchDiGar().setImpAder(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpAltSopr() {
        return trchDiGar.getTgaImpAltSopr().getTgaImpAltSopr();
    }

    @Override
    public void setTgaImpAltSopr(AfDecimal tgaImpAltSopr) {
        this.trchDiGar.getTgaImpAltSopr().setTgaImpAltSopr(tgaImpAltSopr.copy());
    }

    @Override
    public AfDecimal getTgaImpAltSoprObj() {
        if (ws.getIndTrchDiGar().getImpAltSopr() >= 0) {
            return getTgaImpAltSopr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAltSoprObj(AfDecimal tgaImpAltSoprObj) {
        if (tgaImpAltSoprObj != null) {
            setTgaImpAltSopr(new AfDecimal(tgaImpAltSoprObj, 15, 3));
            ws.getIndTrchDiGar().setImpAltSopr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpAltSopr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpAz() {
        return trchDiGar.getTgaImpAz().getTgaImpAz();
    }

    @Override
    public void setTgaImpAz(AfDecimal tgaImpAz) {
        this.trchDiGar.getTgaImpAz().setTgaImpAz(tgaImpAz.copy());
    }

    @Override
    public AfDecimal getTgaImpAzObj() {
        if (ws.getIndTrchDiGar().getImpAz() >= 0) {
            return getTgaImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAzObj(AfDecimal tgaImpAzObj) {
        if (tgaImpAzObj != null) {
            setTgaImpAz(new AfDecimal(tgaImpAzObj, 15, 3));
            ws.getIndTrchDiGar().setImpAz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpBns() {
        return trchDiGar.getTgaImpBns().getTgaImpBns();
    }

    @Override
    public AfDecimal getTgaImpBnsAntic() {
        return trchDiGar.getTgaImpBnsAntic().getTgaImpBnsAntic();
    }

    @Override
    public void setTgaImpBnsAntic(AfDecimal tgaImpBnsAntic) {
        this.trchDiGar.getTgaImpBnsAntic().setTgaImpBnsAntic(tgaImpBnsAntic.copy());
    }

    @Override
    public AfDecimal getTgaImpBnsAnticObj() {
        if (ws.getIndTrchDiGar().getImpBnsAntic() >= 0) {
            return getTgaImpBnsAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpBnsAnticObj(AfDecimal tgaImpBnsAnticObj) {
        if (tgaImpBnsAnticObj != null) {
            setTgaImpBnsAntic(new AfDecimal(tgaImpBnsAnticObj, 15, 3));
            ws.getIndTrchDiGar().setImpBnsAntic(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpBnsAntic(((short)-1));
        }
    }

    @Override
    public void setTgaImpBns(AfDecimal tgaImpBns) {
        this.trchDiGar.getTgaImpBns().setTgaImpBns(tgaImpBns.copy());
    }

    @Override
    public AfDecimal getTgaImpBnsObj() {
        if (ws.getIndTrchDiGar().getImpBns() >= 0) {
            return getTgaImpBns();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpBnsObj(AfDecimal tgaImpBnsObj) {
        if (tgaImpBnsObj != null) {
            setTgaImpBns(new AfDecimal(tgaImpBnsObj, 15, 3));
            ws.getIndTrchDiGar().setImpBns(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpBns(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarAcq() {
        return trchDiGar.getTgaImpCarAcq().getTgaImpCarAcq();
    }

    @Override
    public void setTgaImpCarAcq(AfDecimal tgaImpCarAcq) {
        this.trchDiGar.getTgaImpCarAcq().setTgaImpCarAcq(tgaImpCarAcq.copy());
    }

    @Override
    public AfDecimal getTgaImpCarAcqObj() {
        if (ws.getIndTrchDiGar().getImpCarAcq() >= 0) {
            return getTgaImpCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarAcqObj(AfDecimal tgaImpCarAcqObj) {
        if (tgaImpCarAcqObj != null) {
            setTgaImpCarAcq(new AfDecimal(tgaImpCarAcqObj, 15, 3));
            ws.getIndTrchDiGar().setImpCarAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarGest() {
        return trchDiGar.getTgaImpCarGest().getTgaImpCarGest();
    }

    @Override
    public void setTgaImpCarGest(AfDecimal tgaImpCarGest) {
        this.trchDiGar.getTgaImpCarGest().setTgaImpCarGest(tgaImpCarGest.copy());
    }

    @Override
    public AfDecimal getTgaImpCarGestObj() {
        if (ws.getIndTrchDiGar().getImpCarGest() >= 0) {
            return getTgaImpCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarGestObj(AfDecimal tgaImpCarGestObj) {
        if (tgaImpCarGestObj != null) {
            setTgaImpCarGest(new AfDecimal(tgaImpCarGestObj, 15, 3));
            ws.getIndTrchDiGar().setImpCarGest(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarInc() {
        return trchDiGar.getTgaImpCarInc().getTgaImpCarInc();
    }

    @Override
    public void setTgaImpCarInc(AfDecimal tgaImpCarInc) {
        this.trchDiGar.getTgaImpCarInc().setTgaImpCarInc(tgaImpCarInc.copy());
    }

    @Override
    public AfDecimal getTgaImpCarIncObj() {
        if (ws.getIndTrchDiGar().getImpCarInc() >= 0) {
            return getTgaImpCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarIncObj(AfDecimal tgaImpCarIncObj) {
        if (tgaImpCarIncObj != null) {
            setTgaImpCarInc(new AfDecimal(tgaImpCarIncObj, 15, 3));
            ws.getIndTrchDiGar().setImpCarInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpScon() {
        return trchDiGar.getTgaImpScon().getTgaImpScon();
    }

    @Override
    public void setTgaImpScon(AfDecimal tgaImpScon) {
        this.trchDiGar.getTgaImpScon().setTgaImpScon(tgaImpScon.copy());
    }

    @Override
    public AfDecimal getTgaImpSconObj() {
        if (ws.getIndTrchDiGar().getImpScon() >= 0) {
            return getTgaImpScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSconObj(AfDecimal tgaImpSconObj) {
        if (tgaImpSconObj != null) {
            setTgaImpScon(new AfDecimal(tgaImpSconObj, 15, 3));
            ws.getIndTrchDiGar().setImpScon(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprProf() {
        return trchDiGar.getTgaImpSoprProf().getTgaImpSoprProf();
    }

    @Override
    public void setTgaImpSoprProf(AfDecimal tgaImpSoprProf) {
        this.trchDiGar.getTgaImpSoprProf().setTgaImpSoprProf(tgaImpSoprProf.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprProfObj() {
        if (ws.getIndTrchDiGar().getImpSoprProf() >= 0) {
            return getTgaImpSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprProfObj(AfDecimal tgaImpSoprProfObj) {
        if (tgaImpSoprProfObj != null) {
            setTgaImpSoprProf(new AfDecimal(tgaImpSoprProfObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprProf(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprSan() {
        return trchDiGar.getTgaImpSoprSan().getTgaImpSoprSan();
    }

    @Override
    public void setTgaImpSoprSan(AfDecimal tgaImpSoprSan) {
        this.trchDiGar.getTgaImpSoprSan().setTgaImpSoprSan(tgaImpSoprSan.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprSanObj() {
        if (ws.getIndTrchDiGar().getImpSoprSan() >= 0) {
            return getTgaImpSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprSanObj(AfDecimal tgaImpSoprSanObj) {
        if (tgaImpSoprSanObj != null) {
            setTgaImpSoprSan(new AfDecimal(tgaImpSoprSanObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprSan(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprSpo() {
        return trchDiGar.getTgaImpSoprSpo().getTgaImpSoprSpo();
    }

    @Override
    public void setTgaImpSoprSpo(AfDecimal tgaImpSoprSpo) {
        this.trchDiGar.getTgaImpSoprSpo().setTgaImpSoprSpo(tgaImpSoprSpo.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprSpoObj() {
        if (ws.getIndTrchDiGar().getImpSoprSpo() >= 0) {
            return getTgaImpSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprSpoObj(AfDecimal tgaImpSoprSpoObj) {
        if (tgaImpSoprSpoObj != null) {
            setTgaImpSoprSpo(new AfDecimal(tgaImpSoprSpoObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprSpo(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprTec() {
        return trchDiGar.getTgaImpSoprTec().getTgaImpSoprTec();
    }

    @Override
    public void setTgaImpSoprTec(AfDecimal tgaImpSoprTec) {
        this.trchDiGar.getTgaImpSoprTec().setTgaImpSoprTec(tgaImpSoprTec.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprTecObj() {
        if (ws.getIndTrchDiGar().getImpSoprTec() >= 0) {
            return getTgaImpSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprTecObj(AfDecimal tgaImpSoprTecObj) {
        if (tgaImpSoprTecObj != null) {
            setTgaImpSoprTec(new AfDecimal(tgaImpSoprTecObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprTec(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTfr() {
        return trchDiGar.getTgaImpTfr().getTgaImpTfr();
    }

    @Override
    public void setTgaImpTfr(AfDecimal tgaImpTfr) {
        this.trchDiGar.getTgaImpTfr().setTgaImpTfr(tgaImpTfr.copy());
    }

    @Override
    public AfDecimal getTgaImpTfrObj() {
        if (ws.getIndTrchDiGar().getImpTfr() >= 0) {
            return getTgaImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTfrObj(AfDecimal tgaImpTfrObj) {
        if (tgaImpTfrObj != null) {
            setTgaImpTfr(new AfDecimal(tgaImpTfrObj, 15, 3));
            ws.getIndTrchDiGar().setImpTfr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTfrStrc() {
        return trchDiGar.getTgaImpTfrStrc().getTgaImpTfrStrc();
    }

    @Override
    public void setTgaImpTfrStrc(AfDecimal tgaImpTfrStrc) {
        this.trchDiGar.getTgaImpTfrStrc().setTgaImpTfrStrc(tgaImpTfrStrc.copy());
    }

    @Override
    public AfDecimal getTgaImpTfrStrcObj() {
        if (ws.getIndTrchDiGar().getImpTfrStrc() >= 0) {
            return getTgaImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTfrStrcObj(AfDecimal tgaImpTfrStrcObj) {
        if (tgaImpTfrStrcObj != null) {
            setTgaImpTfrStrc(new AfDecimal(tgaImpTfrStrcObj, 15, 3));
            ws.getIndTrchDiGar().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTrasfe() {
        return trchDiGar.getTgaImpTrasfe().getTgaImpTrasfe();
    }

    @Override
    public void setTgaImpTrasfe(AfDecimal tgaImpTrasfe) {
        this.trchDiGar.getTgaImpTrasfe().setTgaImpTrasfe(tgaImpTrasfe.copy());
    }

    @Override
    public AfDecimal getTgaImpTrasfeObj() {
        if (ws.getIndTrchDiGar().getImpTrasfe() >= 0) {
            return getTgaImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTrasfeObj(AfDecimal tgaImpTrasfeObj) {
        if (tgaImpTrasfeObj != null) {
            setTgaImpTrasfe(new AfDecimal(tgaImpTrasfeObj, 15, 3));
            ws.getIndTrchDiGar().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpVolo() {
        return trchDiGar.getTgaImpVolo().getTgaImpVolo();
    }

    @Override
    public void setTgaImpVolo(AfDecimal tgaImpVolo) {
        this.trchDiGar.getTgaImpVolo().setTgaImpVolo(tgaImpVolo.copy());
    }

    @Override
    public AfDecimal getTgaImpVoloObj() {
        if (ws.getIndTrchDiGar().getImpVolo() >= 0) {
            return getTgaImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpVoloObj(AfDecimal tgaImpVoloObj) {
        if (tgaImpVoloObj != null) {
            setTgaImpVolo(new AfDecimal(tgaImpVoloObj, 15, 3));
            ws.getIndTrchDiGar().setImpVolo(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbCommisInter() {
        return trchDiGar.getTgaImpbCommisInter().getTgaImpbCommisInter();
    }

    @Override
    public void setTgaImpbCommisInter(AfDecimal tgaImpbCommisInter) {
        this.trchDiGar.getTgaImpbCommisInter().setTgaImpbCommisInter(tgaImpbCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaImpbCommisInterObj() {
        if (ws.getIndTrchDiGar().getImpbCommisInter() >= 0) {
            return getTgaImpbCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbCommisInterObj(AfDecimal tgaImpbCommisInterObj) {
        if (tgaImpbCommisInterObj != null) {
            setTgaImpbCommisInter(new AfDecimal(tgaImpbCommisInterObj, 15, 3));
            ws.getIndTrchDiGar().setImpbCommisInter(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvAcq() {
        return trchDiGar.getTgaImpbProvAcq().getTgaImpbProvAcq();
    }

    @Override
    public void setTgaImpbProvAcq(AfDecimal tgaImpbProvAcq) {
        this.trchDiGar.getTgaImpbProvAcq().setTgaImpbProvAcq(tgaImpbProvAcq.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvAcqObj() {
        if (ws.getIndTrchDiGar().getImpbProvAcq() >= 0) {
            return getTgaImpbProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvAcqObj(AfDecimal tgaImpbProvAcqObj) {
        if (tgaImpbProvAcqObj != null) {
            setTgaImpbProvAcq(new AfDecimal(tgaImpbProvAcqObj, 15, 3));
            ws.getIndTrchDiGar().setImpbProvAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvInc() {
        return trchDiGar.getTgaImpbProvInc().getTgaImpbProvInc();
    }

    @Override
    public void setTgaImpbProvInc(AfDecimal tgaImpbProvInc) {
        this.trchDiGar.getTgaImpbProvInc().setTgaImpbProvInc(tgaImpbProvInc.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvIncObj() {
        if (ws.getIndTrchDiGar().getImpbProvInc() >= 0) {
            return getTgaImpbProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvIncObj(AfDecimal tgaImpbProvIncObj) {
        if (tgaImpbProvIncObj != null) {
            setTgaImpbProvInc(new AfDecimal(tgaImpbProvIncObj, 15, 3));
            ws.getIndTrchDiGar().setImpbProvInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvRicor() {
        return trchDiGar.getTgaImpbProvRicor().getTgaImpbProvRicor();
    }

    @Override
    public void setTgaImpbProvRicor(AfDecimal tgaImpbProvRicor) {
        this.trchDiGar.getTgaImpbProvRicor().setTgaImpbProvRicor(tgaImpbProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvRicorObj() {
        if (ws.getIndTrchDiGar().getImpbProvRicor() >= 0) {
            return getTgaImpbProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvRicorObj(AfDecimal tgaImpbProvRicorObj) {
        if (tgaImpbProvRicorObj != null) {
            setTgaImpbProvRicor(new AfDecimal(tgaImpbProvRicorObj, 15, 3));
            ws.getIndTrchDiGar().setImpbProvRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbRemunAss() {
        return trchDiGar.getTgaImpbRemunAss().getTgaImpbRemunAss();
    }

    @Override
    public void setTgaImpbRemunAss(AfDecimal tgaImpbRemunAss) {
        this.trchDiGar.getTgaImpbRemunAss().setTgaImpbRemunAss(tgaImpbRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaImpbRemunAssObj() {
        if (ws.getIndTrchDiGar().getImpbRemunAss() >= 0) {
            return getTgaImpbRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbRemunAssObj(AfDecimal tgaImpbRemunAssObj) {
        if (tgaImpbRemunAssObj != null) {
            setTgaImpbRemunAss(new AfDecimal(tgaImpbRemunAssObj, 15, 3));
            ws.getIndTrchDiGar().setImpbRemunAss(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000() {
        return trchDiGar.getTgaImpbVisEnd2000().getTgaImpbVisEnd2000();
    }

    @Override
    public void setTgaImpbVisEnd2000(AfDecimal tgaImpbVisEnd2000) {
        this.trchDiGar.getTgaImpbVisEnd2000().setTgaImpbVisEnd2000(tgaImpbVisEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000Obj() {
        if (ws.getIndTrchDiGar().getImpbVisEnd2000() >= 0) {
            return getTgaImpbVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbVisEnd2000Obj(AfDecimal tgaImpbVisEnd2000Obj) {
        if (tgaImpbVisEnd2000Obj != null) {
            setTgaImpbVisEnd2000(new AfDecimal(tgaImpbVisEnd2000Obj, 15, 3));
            ws.getIndTrchDiGar().setImpbVisEnd2000(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbVisEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIncrPre() {
        return trchDiGar.getTgaIncrPre().getTgaIncrPre();
    }

    @Override
    public void setTgaIncrPre(AfDecimal tgaIncrPre) {
        this.trchDiGar.getTgaIncrPre().setTgaIncrPre(tgaIncrPre.copy());
    }

    @Override
    public AfDecimal getTgaIncrPreObj() {
        if (ws.getIndTrchDiGar().getIncrPre() >= 0) {
            return getTgaIncrPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIncrPreObj(AfDecimal tgaIncrPreObj) {
        if (tgaIncrPreObj != null) {
            setTgaIncrPre(new AfDecimal(tgaIncrPreObj, 15, 3));
            ws.getIndTrchDiGar().setIncrPre(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIncrPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIncrPrstz() {
        return trchDiGar.getTgaIncrPrstz().getTgaIncrPrstz();
    }

    @Override
    public void setTgaIncrPrstz(AfDecimal tgaIncrPrstz) {
        this.trchDiGar.getTgaIncrPrstz().setTgaIncrPrstz(tgaIncrPrstz.copy());
    }

    @Override
    public AfDecimal getTgaIncrPrstzObj() {
        if (ws.getIndTrchDiGar().getIncrPrstz() >= 0) {
            return getTgaIncrPrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIncrPrstzObj(AfDecimal tgaIncrPrstzObj) {
        if (tgaIncrPrstzObj != null) {
            setTgaIncrPrstz(new AfDecimal(tgaIncrPrstzObj, 15, 3));
            ws.getIndTrchDiGar().setIncrPrstz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIncrPrstz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIntrMora() {
        return trchDiGar.getTgaIntrMora().getTgaIntrMora();
    }

    @Override
    public void setTgaIntrMora(AfDecimal tgaIntrMora) {
        this.trchDiGar.getTgaIntrMora().setTgaIntrMora(tgaIntrMora.copy());
    }

    @Override
    public AfDecimal getTgaIntrMoraObj() {
        if (ws.getIndTrchDiGar().getIntrMora() >= 0) {
            return getTgaIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIntrMoraObj(AfDecimal tgaIntrMoraObj) {
        if (tgaIntrMoraObj != null) {
            setTgaIntrMora(new AfDecimal(tgaIntrMoraObj, 15, 3));
            ws.getIndTrchDiGar().setIntrMora(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaManfeeAntic() {
        return trchDiGar.getTgaManfeeAntic().getTgaManfeeAntic();
    }

    @Override
    public void setTgaManfeeAntic(AfDecimal tgaManfeeAntic) {
        this.trchDiGar.getTgaManfeeAntic().setTgaManfeeAntic(tgaManfeeAntic.copy());
    }

    @Override
    public AfDecimal getTgaManfeeAnticObj() {
        if (ws.getIndTrchDiGar().getManfeeAntic() >= 0) {
            return getTgaManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaManfeeAnticObj(AfDecimal tgaManfeeAnticObj) {
        if (tgaManfeeAnticObj != null) {
            setTgaManfeeAntic(new AfDecimal(tgaManfeeAnticObj, 15, 3));
            ws.getIndTrchDiGar().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaManfeeRicor() {
        return trchDiGar.getTgaManfeeRicor().getTgaManfeeRicor();
    }

    @Override
    public void setTgaManfeeRicor(AfDecimal tgaManfeeRicor) {
        this.trchDiGar.getTgaManfeeRicor().setTgaManfeeRicor(tgaManfeeRicor.copy());
    }

    @Override
    public AfDecimal getTgaManfeeRicorObj() {
        if (ws.getIndTrchDiGar().getManfeeRicor() >= 0) {
            return getTgaManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaManfeeRicorObj(AfDecimal tgaManfeeRicorObj) {
        if (tgaManfeeRicorObj != null) {
            setTgaManfeeRicor(new AfDecimal(tgaManfeeRicorObj, 15, 3));
            ws.getIndTrchDiGar().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMatuEnd2000() {
        return trchDiGar.getTgaMatuEnd2000().getTgaMatuEnd2000();
    }

    @Override
    public void setTgaMatuEnd2000(AfDecimal tgaMatuEnd2000) {
        this.trchDiGar.getTgaMatuEnd2000().setTgaMatuEnd2000(tgaMatuEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaMatuEnd2000Obj() {
        if (ws.getIndTrchDiGar().getMatuEnd2000() >= 0) {
            return getTgaMatuEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMatuEnd2000Obj(AfDecimal tgaMatuEnd2000Obj) {
        if (tgaMatuEnd2000Obj != null) {
            setTgaMatuEnd2000(new AfDecimal(tgaMatuEnd2000Obj, 15, 3));
            ws.getIndTrchDiGar().setMatuEnd2000(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setMatuEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMinGarto() {
        return trchDiGar.getTgaMinGarto().getTgaMinGarto();
    }

    @Override
    public void setTgaMinGarto(AfDecimal tgaMinGarto) {
        this.trchDiGar.getTgaMinGarto().setTgaMinGarto(tgaMinGarto.copy());
    }

    @Override
    public AfDecimal getTgaMinGartoObj() {
        if (ws.getIndTrchDiGar().getMinGarto() >= 0) {
            return getTgaMinGarto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMinGartoObj(AfDecimal tgaMinGartoObj) {
        if (tgaMinGartoObj != null) {
            setTgaMinGarto(new AfDecimal(tgaMinGartoObj, 14, 9));
            ws.getIndTrchDiGar().setMinGarto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setMinGarto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMinTrnut() {
        return trchDiGar.getTgaMinTrnut().getTgaMinTrnut();
    }

    @Override
    public void setTgaMinTrnut(AfDecimal tgaMinTrnut) {
        this.trchDiGar.getTgaMinTrnut().setTgaMinTrnut(tgaMinTrnut.copy());
    }

    @Override
    public AfDecimal getTgaMinTrnutObj() {
        if (ws.getIndTrchDiGar().getMinTrnut() >= 0) {
            return getTgaMinTrnut();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMinTrnutObj(AfDecimal tgaMinTrnutObj) {
        if (tgaMinTrnutObj != null) {
            setTgaMinTrnut(new AfDecimal(tgaMinTrnutObj, 14, 9));
            ws.getIndTrchDiGar().setMinTrnut(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setMinTrnut(((short)-1));
        }
    }

    @Override
    public String getTgaModCalc() {
        return trchDiGar.getTgaModCalc();
    }

    @Override
    public void setTgaModCalc(String tgaModCalc) {
        this.trchDiGar.setTgaModCalc(tgaModCalc);
    }

    @Override
    public String getTgaModCalcObj() {
        if (ws.getIndTrchDiGar().getModCalc() >= 0) {
            return getTgaModCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaModCalcObj(String tgaModCalcObj) {
        if (tgaModCalcObj != null) {
            setTgaModCalc(tgaModCalcObj);
            ws.getIndTrchDiGar().setModCalc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setModCalc(((short)-1));
        }
    }

    @Override
    public int getTgaNumGgRival() {
        return trchDiGar.getTgaNumGgRival().getTgaNumGgRival();
    }

    @Override
    public void setTgaNumGgRival(int tgaNumGgRival) {
        this.trchDiGar.getTgaNumGgRival().setTgaNumGgRival(tgaNumGgRival);
    }

    @Override
    public Integer getTgaNumGgRivalObj() {
        if (ws.getIndTrchDiGar().getNumGgRival() >= 0) {
            return ((Integer)getTgaNumGgRival());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaNumGgRivalObj(Integer tgaNumGgRivalObj) {
        if (tgaNumGgRivalObj != null) {
            setTgaNumGgRival(((int)tgaNumGgRivalObj));
            ws.getIndTrchDiGar().setNumGgRival(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setNumGgRival(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaOldTsTec() {
        return trchDiGar.getTgaOldTsTec().getTgaOldTsTec();
    }

    @Override
    public void setTgaOldTsTec(AfDecimal tgaOldTsTec) {
        this.trchDiGar.getTgaOldTsTec().setTgaOldTsTec(tgaOldTsTec.copy());
    }

    @Override
    public AfDecimal getTgaOldTsTecObj() {
        if (ws.getIndTrchDiGar().getOldTsTec() >= 0) {
            return getTgaOldTsTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaOldTsTecObj(AfDecimal tgaOldTsTecObj) {
        if (tgaOldTsTecObj != null) {
            setTgaOldTsTec(new AfDecimal(tgaOldTsTecObj, 14, 9));
            ws.getIndTrchDiGar().setOldTsTec(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setOldTsTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcCommisGest() {
        return trchDiGar.getTgaPcCommisGest().getTgaPcCommisGest();
    }

    @Override
    public void setTgaPcCommisGest(AfDecimal tgaPcCommisGest) {
        this.trchDiGar.getTgaPcCommisGest().setTgaPcCommisGest(tgaPcCommisGest.copy());
    }

    @Override
    public AfDecimal getTgaPcCommisGestObj() {
        if (ws.getIndTrchDiGar().getPcCommisGest() >= 0) {
            return getTgaPcCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcCommisGestObj(AfDecimal tgaPcCommisGestObj) {
        if (tgaPcCommisGestObj != null) {
            setTgaPcCommisGest(new AfDecimal(tgaPcCommisGestObj, 6, 3));
            ws.getIndTrchDiGar().setPcCommisGest(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcIntrRiat() {
        return trchDiGar.getTgaPcIntrRiat().getTgaPcIntrRiat();
    }

    @Override
    public void setTgaPcIntrRiat(AfDecimal tgaPcIntrRiat) {
        this.trchDiGar.getTgaPcIntrRiat().setTgaPcIntrRiat(tgaPcIntrRiat.copy());
    }

    @Override
    public AfDecimal getTgaPcIntrRiatObj() {
        if (ws.getIndTrchDiGar().getPcIntrRiat() >= 0) {
            return getTgaPcIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcIntrRiatObj(AfDecimal tgaPcIntrRiatObj) {
        if (tgaPcIntrRiatObj != null) {
            setTgaPcIntrRiat(new AfDecimal(tgaPcIntrRiatObj, 6, 3));
            ws.getIndTrchDiGar().setPcIntrRiat(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcRetr() {
        return trchDiGar.getTgaPcRetr().getTgaPcRetr();
    }

    @Override
    public void setTgaPcRetr(AfDecimal tgaPcRetr) {
        this.trchDiGar.getTgaPcRetr().setTgaPcRetr(tgaPcRetr.copy());
    }

    @Override
    public AfDecimal getTgaPcRetrObj() {
        if (ws.getIndTrchDiGar().getPcRetr() >= 0) {
            return getTgaPcRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcRetrObj(AfDecimal tgaPcRetrObj) {
        if (tgaPcRetrObj != null) {
            setTgaPcRetr(new AfDecimal(tgaPcRetrObj, 6, 3));
            ws.getIndTrchDiGar().setPcRetr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcRipPre() {
        return trchDiGar.getTgaPcRipPre().getTgaPcRipPre();
    }

    @Override
    public void setTgaPcRipPre(AfDecimal tgaPcRipPre) {
        this.trchDiGar.getTgaPcRipPre().setTgaPcRipPre(tgaPcRipPre.copy());
    }

    @Override
    public AfDecimal getTgaPcRipPreObj() {
        if (ws.getIndTrchDiGar().getPcRipPre() >= 0) {
            return getTgaPcRipPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcRipPreObj(AfDecimal tgaPcRipPreObj) {
        if (tgaPcRipPreObj != null) {
            setTgaPcRipPre(new AfDecimal(tgaPcRipPreObj, 6, 3));
            ws.getIndTrchDiGar().setPcRipPre(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcRipPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreAttDiTrch() {
        return trchDiGar.getTgaPreAttDiTrch().getTgaPreAttDiTrch();
    }

    @Override
    public void setTgaPreAttDiTrch(AfDecimal tgaPreAttDiTrch) {
        this.trchDiGar.getTgaPreAttDiTrch().setTgaPreAttDiTrch(tgaPreAttDiTrch.copy());
    }

    @Override
    public AfDecimal getTgaPreAttDiTrchObj() {
        if (ws.getIndTrchDiGar().getPreAttDiTrch() >= 0) {
            return getTgaPreAttDiTrch();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreAttDiTrchObj(AfDecimal tgaPreAttDiTrchObj) {
        if (tgaPreAttDiTrchObj != null) {
            setTgaPreAttDiTrch(new AfDecimal(tgaPreAttDiTrchObj, 15, 3));
            ws.getIndTrchDiGar().setPreAttDiTrch(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreAttDiTrch(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreCasoMor() {
        return trchDiGar.getTgaPreCasoMor().getTgaPreCasoMor();
    }

    @Override
    public void setTgaPreCasoMor(AfDecimal tgaPreCasoMor) {
        this.trchDiGar.getTgaPreCasoMor().setTgaPreCasoMor(tgaPreCasoMor.copy());
    }

    @Override
    public AfDecimal getTgaPreCasoMorObj() {
        if (ws.getIndTrchDiGar().getPreCasoMor() >= 0) {
            return getTgaPreCasoMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreCasoMorObj(AfDecimal tgaPreCasoMorObj) {
        if (tgaPreCasoMorObj != null) {
            setTgaPreCasoMor(new AfDecimal(tgaPreCasoMorObj, 15, 3));
            ws.getIndTrchDiGar().setPreCasoMor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreCasoMor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreIniNet() {
        return trchDiGar.getTgaPreIniNet().getTgaPreIniNet();
    }

    @Override
    public void setTgaPreIniNet(AfDecimal tgaPreIniNet) {
        this.trchDiGar.getTgaPreIniNet().setTgaPreIniNet(tgaPreIniNet.copy());
    }

    @Override
    public AfDecimal getTgaPreIniNetObj() {
        if (ws.getIndTrchDiGar().getPreIniNet() >= 0) {
            return getTgaPreIniNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreIniNetObj(AfDecimal tgaPreIniNetObj) {
        if (tgaPreIniNetObj != null) {
            setTgaPreIniNet(new AfDecimal(tgaPreIniNetObj, 15, 3));
            ws.getIndTrchDiGar().setPreIniNet(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreIniNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreInvrioIni() {
        return trchDiGar.getTgaPreInvrioIni().getTgaPreInvrioIni();
    }

    @Override
    public void setTgaPreInvrioIni(AfDecimal tgaPreInvrioIni) {
        this.trchDiGar.getTgaPreInvrioIni().setTgaPreInvrioIni(tgaPreInvrioIni.copy());
    }

    @Override
    public AfDecimal getTgaPreInvrioIniObj() {
        if (ws.getIndTrchDiGar().getPreInvrioIni() >= 0) {
            return getTgaPreInvrioIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreInvrioIniObj(AfDecimal tgaPreInvrioIniObj) {
        if (tgaPreInvrioIniObj != null) {
            setTgaPreInvrioIni(new AfDecimal(tgaPreInvrioIniObj, 15, 3));
            ws.getIndTrchDiGar().setPreInvrioIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreInvrioIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreInvrioUlt() {
        return trchDiGar.getTgaPreInvrioUlt().getTgaPreInvrioUlt();
    }

    @Override
    public void setTgaPreInvrioUlt(AfDecimal tgaPreInvrioUlt) {
        this.trchDiGar.getTgaPreInvrioUlt().setTgaPreInvrioUlt(tgaPreInvrioUlt.copy());
    }

    @Override
    public AfDecimal getTgaPreInvrioUltObj() {
        if (ws.getIndTrchDiGar().getPreInvrioUlt() >= 0) {
            return getTgaPreInvrioUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreInvrioUltObj(AfDecimal tgaPreInvrioUltObj) {
        if (tgaPreInvrioUltObj != null) {
            setTgaPreInvrioUlt(new AfDecimal(tgaPreInvrioUltObj, 15, 3));
            ws.getIndTrchDiGar().setPreInvrioUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreInvrioUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreLrd() {
        return trchDiGar.getTgaPreLrd().getTgaPreLrd();
    }

    @Override
    public void setTgaPreLrd(AfDecimal tgaPreLrd) {
        this.trchDiGar.getTgaPreLrd().setTgaPreLrd(tgaPreLrd.copy());
    }

    @Override
    public AfDecimal getTgaPreLrdObj() {
        if (ws.getIndTrchDiGar().getPreLrd() >= 0) {
            return getTgaPreLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreLrdObj(AfDecimal tgaPreLrdObj) {
        if (tgaPreLrdObj != null) {
            setTgaPreLrd(new AfDecimal(tgaPreLrdObj, 15, 3));
            ws.getIndTrchDiGar().setPreLrd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePattuito() {
        return trchDiGar.getTgaPrePattuito().getTgaPrePattuito();
    }

    @Override
    public void setTgaPrePattuito(AfDecimal tgaPrePattuito) {
        this.trchDiGar.getTgaPrePattuito().setTgaPrePattuito(tgaPrePattuito.copy());
    }

    @Override
    public AfDecimal getTgaPrePattuitoObj() {
        if (ws.getIndTrchDiGar().getPrePattuito() >= 0) {
            return getTgaPrePattuito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePattuitoObj(AfDecimal tgaPrePattuitoObj) {
        if (tgaPrePattuitoObj != null) {
            setTgaPrePattuito(new AfDecimal(tgaPrePattuitoObj, 15, 3));
            ws.getIndTrchDiGar().setPrePattuito(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrePattuito(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePpIni() {
        return trchDiGar.getTgaPrePpIni().getTgaPrePpIni();
    }

    @Override
    public void setTgaPrePpIni(AfDecimal tgaPrePpIni) {
        this.trchDiGar.getTgaPrePpIni().setTgaPrePpIni(tgaPrePpIni.copy());
    }

    @Override
    public AfDecimal getTgaPrePpIniObj() {
        if (ws.getIndTrchDiGar().getPrePpIni() >= 0) {
            return getTgaPrePpIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePpIniObj(AfDecimal tgaPrePpIniObj) {
        if (tgaPrePpIniObj != null) {
            setTgaPrePpIni(new AfDecimal(tgaPrePpIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrePpIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrePpIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePpUlt() {
        return trchDiGar.getTgaPrePpUlt().getTgaPrePpUlt();
    }

    @Override
    public void setTgaPrePpUlt(AfDecimal tgaPrePpUlt) {
        this.trchDiGar.getTgaPrePpUlt().setTgaPrePpUlt(tgaPrePpUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrePpUltObj() {
        if (ws.getIndTrchDiGar().getPrePpUlt() >= 0) {
            return getTgaPrePpUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePpUltObj(AfDecimal tgaPrePpUltObj) {
        if (tgaPrePpUltObj != null) {
            setTgaPrePpUlt(new AfDecimal(tgaPrePpUltObj, 15, 3));
            ws.getIndTrchDiGar().setPrePpUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrePpUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreRivto() {
        return trchDiGar.getTgaPreRivto().getTgaPreRivto();
    }

    @Override
    public void setTgaPreRivto(AfDecimal tgaPreRivto) {
        this.trchDiGar.getTgaPreRivto().setTgaPreRivto(tgaPreRivto.copy());
    }

    @Override
    public AfDecimal getTgaPreRivtoObj() {
        if (ws.getIndTrchDiGar().getPreRivto() >= 0) {
            return getTgaPreRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreRivtoObj(AfDecimal tgaPreRivtoObj) {
        if (tgaPreRivtoObj != null) {
            setTgaPreRivto(new AfDecimal(tgaPreRivtoObj, 15, 3));
            ws.getIndTrchDiGar().setPreRivto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreStab() {
        return trchDiGar.getTgaPreStab().getTgaPreStab();
    }

    @Override
    public void setTgaPreStab(AfDecimal tgaPreStab) {
        this.trchDiGar.getTgaPreStab().setTgaPreStab(tgaPreStab.copy());
    }

    @Override
    public AfDecimal getTgaPreStabObj() {
        if (ws.getIndTrchDiGar().getPreStab() >= 0) {
            return getTgaPreStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreStabObj(AfDecimal tgaPreStabObj) {
        if (tgaPreStabObj != null) {
            setTgaPreStab(new AfDecimal(tgaPreStabObj, 15, 3));
            ws.getIndTrchDiGar().setPreStab(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreTariIni() {
        return trchDiGar.getTgaPreTariIni().getTgaPreTariIni();
    }

    @Override
    public void setTgaPreTariIni(AfDecimal tgaPreTariIni) {
        this.trchDiGar.getTgaPreTariIni().setTgaPreTariIni(tgaPreTariIni.copy());
    }

    @Override
    public AfDecimal getTgaPreTariIniObj() {
        if (ws.getIndTrchDiGar().getPreTariIni() >= 0) {
            return getTgaPreTariIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreTariIniObj(AfDecimal tgaPreTariIniObj) {
        if (tgaPreTariIniObj != null) {
            setTgaPreTariIni(new AfDecimal(tgaPreTariIniObj, 15, 3));
            ws.getIndTrchDiGar().setPreTariIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreTariIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreTariUlt() {
        return trchDiGar.getTgaPreTariUlt().getTgaPreTariUlt();
    }

    @Override
    public void setTgaPreTariUlt(AfDecimal tgaPreTariUlt) {
        this.trchDiGar.getTgaPreTariUlt().setTgaPreTariUlt(tgaPreTariUlt.copy());
    }

    @Override
    public AfDecimal getTgaPreTariUltObj() {
        if (ws.getIndTrchDiGar().getPreTariUlt() >= 0) {
            return getTgaPreTariUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreTariUltObj(AfDecimal tgaPreTariUltObj) {
        if (tgaPreTariUltObj != null) {
            setTgaPreTariUlt(new AfDecimal(tgaPreTariUltObj, 15, 3));
            ws.getIndTrchDiGar().setPreTariUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreTariUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreUniRivto() {
        return trchDiGar.getTgaPreUniRivto().getTgaPreUniRivto();
    }

    @Override
    public void setTgaPreUniRivto(AfDecimal tgaPreUniRivto) {
        this.trchDiGar.getTgaPreUniRivto().setTgaPreUniRivto(tgaPreUniRivto.copy());
    }

    @Override
    public AfDecimal getTgaPreUniRivtoObj() {
        if (ws.getIndTrchDiGar().getPreUniRivto() >= 0) {
            return getTgaPreUniRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreUniRivtoObj(AfDecimal tgaPreUniRivtoObj) {
        if (tgaPreUniRivtoObj != null) {
            setTgaPreUniRivto(new AfDecimal(tgaPreUniRivtoObj, 15, 3));
            ws.getIndTrchDiGar().setPreUniRivto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreUniRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProv1aaAcq() {
        return trchDiGar.getTgaProv1aaAcq().getTgaProv1aaAcq();
    }

    @Override
    public void setTgaProv1aaAcq(AfDecimal tgaProv1aaAcq) {
        this.trchDiGar.getTgaProv1aaAcq().setTgaProv1aaAcq(tgaProv1aaAcq.copy());
    }

    @Override
    public AfDecimal getTgaProv1aaAcqObj() {
        if (ws.getIndTrchDiGar().getProv1aaAcq() >= 0) {
            return getTgaProv1aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProv1aaAcqObj(AfDecimal tgaProv1aaAcqObj) {
        if (tgaProv1aaAcqObj != null) {
            setTgaProv1aaAcq(new AfDecimal(tgaProv1aaAcqObj, 15, 3));
            ws.getIndTrchDiGar().setProv1aaAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProv1aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProv2aaAcq() {
        return trchDiGar.getTgaProv2aaAcq().getTgaProv2aaAcq();
    }

    @Override
    public void setTgaProv2aaAcq(AfDecimal tgaProv2aaAcq) {
        this.trchDiGar.getTgaProv2aaAcq().setTgaProv2aaAcq(tgaProv2aaAcq.copy());
    }

    @Override
    public AfDecimal getTgaProv2aaAcqObj() {
        if (ws.getIndTrchDiGar().getProv2aaAcq() >= 0) {
            return getTgaProv2aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProv2aaAcqObj(AfDecimal tgaProv2aaAcqObj) {
        if (tgaProv2aaAcqObj != null) {
            setTgaProv2aaAcq(new AfDecimal(tgaProv2aaAcqObj, 15, 3));
            ws.getIndTrchDiGar().setProv2aaAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProv2aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProvInc() {
        return trchDiGar.getTgaProvInc().getTgaProvInc();
    }

    @Override
    public void setTgaProvInc(AfDecimal tgaProvInc) {
        this.trchDiGar.getTgaProvInc().setTgaProvInc(tgaProvInc.copy());
    }

    @Override
    public AfDecimal getTgaProvIncObj() {
        if (ws.getIndTrchDiGar().getProvInc() >= 0) {
            return getTgaProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProvIncObj(AfDecimal tgaProvIncObj) {
        if (tgaProvIncObj != null) {
            setTgaProvInc(new AfDecimal(tgaProvIncObj, 15, 3));
            ws.getIndTrchDiGar().setProvInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProvRicor() {
        return trchDiGar.getTgaProvRicor().getTgaProvRicor();
    }

    @Override
    public void setTgaProvRicor(AfDecimal tgaProvRicor) {
        this.trchDiGar.getTgaProvRicor().setTgaProvRicor(tgaProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaProvRicorObj() {
        if (ws.getIndTrchDiGar().getProvRicor() >= 0) {
            return getTgaProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProvRicorObj(AfDecimal tgaProvRicorObj) {
        if (tgaProvRicorObj != null) {
            setTgaProvRicor(new AfDecimal(tgaProvRicorObj, 15, 3));
            ws.getIndTrchDiGar().setProvRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzAggIni() {
        return trchDiGar.getTgaPrstzAggIni().getTgaPrstzAggIni();
    }

    @Override
    public void setTgaPrstzAggIni(AfDecimal tgaPrstzAggIni) {
        this.trchDiGar.getTgaPrstzAggIni().setTgaPrstzAggIni(tgaPrstzAggIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzAggIniObj() {
        if (ws.getIndTrchDiGar().getPrstzAggIni() >= 0) {
            return getTgaPrstzAggIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzAggIniObj(AfDecimal tgaPrstzAggIniObj) {
        if (tgaPrstzAggIniObj != null) {
            setTgaPrstzAggIni(new AfDecimal(tgaPrstzAggIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzAggIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzAggIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzAggUlt() {
        return trchDiGar.getTgaPrstzAggUlt().getTgaPrstzAggUlt();
    }

    @Override
    public void setTgaPrstzAggUlt(AfDecimal tgaPrstzAggUlt) {
        this.trchDiGar.getTgaPrstzAggUlt().setTgaPrstzAggUlt(tgaPrstzAggUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrstzAggUltObj() {
        if (ws.getIndTrchDiGar().getPrstzAggUlt() >= 0) {
            return getTgaPrstzAggUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzAggUltObj(AfDecimal tgaPrstzAggUltObj) {
        if (tgaPrstzAggUltObj != null) {
            setTgaPrstzAggUlt(new AfDecimal(tgaPrstzAggUltObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzAggUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzAggUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIni() {
        return trchDiGar.getTgaPrstzIni().getTgaPrstzIni();
    }

    @Override
    public void setTgaPrstzIni(AfDecimal tgaPrstzIni) {
        this.trchDiGar.getTgaPrstzIni().setTgaPrstzIni(tgaPrstzIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfis() {
        return trchDiGar.getTgaPrstzIniNewfis().getTgaPrstzIniNewfis();
    }

    @Override
    public void setTgaPrstzIniNewfis(AfDecimal tgaPrstzIniNewfis) {
        this.trchDiGar.getTgaPrstzIniNewfis().setTgaPrstzIniNewfis(tgaPrstzIniNewfis.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfisObj() {
        if (ws.getIndTrchDiGar().getPrstzIniNewfis() >= 0) {
            return getTgaPrstzIniNewfis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniNewfisObj(AfDecimal tgaPrstzIniNewfisObj) {
        if (tgaPrstzIniNewfisObj != null) {
            setTgaPrstzIniNewfis(new AfDecimal(tgaPrstzIniNewfisObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIniNewfis(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIniNewfis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniNforz() {
        return trchDiGar.getTgaPrstzIniNforz().getTgaPrstzIniNforz();
    }

    @Override
    public void setTgaPrstzIniNforz(AfDecimal tgaPrstzIniNforz) {
        this.trchDiGar.getTgaPrstzIniNforz().setTgaPrstzIniNforz(tgaPrstzIniNforz.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNforzObj() {
        if (ws.getIndTrchDiGar().getPrstzIniNforz() >= 0) {
            return getTgaPrstzIniNforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniNforzObj(AfDecimal tgaPrstzIniNforzObj) {
        if (tgaPrstzIniNforzObj != null) {
            setTgaPrstzIniNforz(new AfDecimal(tgaPrstzIniNforzObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIniNforz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIniNforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniObj() {
        if (ws.getIndTrchDiGar().getPrstzIni() >= 0) {
            return getTgaPrstzIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniObj(AfDecimal tgaPrstzIniObj) {
        if (tgaPrstzIniObj != null) {
            setTgaPrstzIni(new AfDecimal(tgaPrstzIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniStab() {
        return trchDiGar.getTgaPrstzIniStab().getTgaPrstzIniStab();
    }

    @Override
    public void setTgaPrstzIniStab(AfDecimal tgaPrstzIniStab) {
        this.trchDiGar.getTgaPrstzIniStab().setTgaPrstzIniStab(tgaPrstzIniStab.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniStabObj() {
        if (ws.getIndTrchDiGar().getPrstzIniStab() >= 0) {
            return getTgaPrstzIniStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniStabObj(AfDecimal tgaPrstzIniStabObj) {
        if (tgaPrstzIniStabObj != null) {
            setTgaPrstzIniStab(new AfDecimal(tgaPrstzIniStabObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIniStab(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIniStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzRidIni() {
        return trchDiGar.getTgaPrstzRidIni().getTgaPrstzRidIni();
    }

    @Override
    public void setTgaPrstzRidIni(AfDecimal tgaPrstzRidIni) {
        this.trchDiGar.getTgaPrstzRidIni().setTgaPrstzRidIni(tgaPrstzRidIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzRidIniObj() {
        if (ws.getIndTrchDiGar().getPrstzRidIni() >= 0) {
            return getTgaPrstzRidIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzRidIniObj(AfDecimal tgaPrstzRidIniObj) {
        if (tgaPrstzRidIniObj != null) {
            setTgaPrstzRidIni(new AfDecimal(tgaPrstzRidIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzRidIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzRidIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzUlt() {
        return trchDiGar.getTgaPrstzUlt().getTgaPrstzUlt();
    }

    @Override
    public void setTgaPrstzUlt(AfDecimal tgaPrstzUlt) {
        this.trchDiGar.getTgaPrstzUlt().setTgaPrstzUlt(tgaPrstzUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrstzUltObj() {
        if (ws.getIndTrchDiGar().getPrstzUlt() >= 0) {
            return getTgaPrstzUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzUltObj(AfDecimal tgaPrstzUltObj) {
        if (tgaPrstzUltObj != null) {
            setTgaPrstzUlt(new AfDecimal(tgaPrstzUltObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRatLrd() {
        return trchDiGar.getTgaRatLrd().getTgaRatLrd();
    }

    @Override
    public void setTgaRatLrd(AfDecimal tgaRatLrd) {
        this.trchDiGar.getTgaRatLrd().setTgaRatLrd(tgaRatLrd.copy());
    }

    @Override
    public AfDecimal getTgaRatLrdObj() {
        if (ws.getIndTrchDiGar().getRatLrd() >= 0) {
            return getTgaRatLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRatLrdObj(AfDecimal tgaRatLrdObj) {
        if (tgaRatLrdObj != null) {
            setTgaRatLrd(new AfDecimal(tgaRatLrdObj, 15, 3));
            ws.getIndTrchDiGar().setRatLrd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRatLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRemunAss() {
        return trchDiGar.getTgaRemunAss().getTgaRemunAss();
    }

    @Override
    public void setTgaRemunAss(AfDecimal tgaRemunAss) {
        this.trchDiGar.getTgaRemunAss().setTgaRemunAss(tgaRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaRemunAssObj() {
        if (ws.getIndTrchDiGar().getRemunAss() >= 0) {
            return getTgaRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRemunAssObj(AfDecimal tgaRemunAssObj) {
        if (tgaRemunAssObj != null) {
            setTgaRemunAss(new AfDecimal(tgaRemunAssObj, 15, 3));
            ws.getIndTrchDiGar().setRemunAss(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0() {
        return trchDiGar.getTgaRenIniTsTec0().getTgaRenIniTsTec0();
    }

    @Override
    public void setTgaRenIniTsTec0(AfDecimal tgaRenIniTsTec0) {
        this.trchDiGar.getTgaRenIniTsTec0().setTgaRenIniTsTec0(tgaRenIniTsTec0.copy());
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0Obj() {
        if (ws.getIndTrchDiGar().getRenIniTsTec0() >= 0) {
            return getTgaRenIniTsTec0();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRenIniTsTec0Obj(AfDecimal tgaRenIniTsTec0Obj) {
        if (tgaRenIniTsTec0Obj != null) {
            setTgaRenIniTsTec0(new AfDecimal(tgaRenIniTsTec0Obj, 15, 3));
            ws.getIndTrchDiGar().setRenIniTsTec0(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRenIniTsTec0(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRendtoLrd() {
        return trchDiGar.getTgaRendtoLrd().getTgaRendtoLrd();
    }

    @Override
    public void setTgaRendtoLrd(AfDecimal tgaRendtoLrd) {
        this.trchDiGar.getTgaRendtoLrd().setTgaRendtoLrd(tgaRendtoLrd.copy());
    }

    @Override
    public AfDecimal getTgaRendtoLrdObj() {
        if (ws.getIndTrchDiGar().getRendtoLrd() >= 0) {
            return getTgaRendtoLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRendtoLrdObj(AfDecimal tgaRendtoLrdObj) {
        if (tgaRendtoLrdObj != null) {
            setTgaRendtoLrd(new AfDecimal(tgaRendtoLrdObj, 14, 9));
            ws.getIndTrchDiGar().setRendtoLrd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRendtoLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRendtoRetr() {
        return trchDiGar.getTgaRendtoRetr().getTgaRendtoRetr();
    }

    @Override
    public void setTgaRendtoRetr(AfDecimal tgaRendtoRetr) {
        this.trchDiGar.getTgaRendtoRetr().setTgaRendtoRetr(tgaRendtoRetr.copy());
    }

    @Override
    public AfDecimal getTgaRendtoRetrObj() {
        if (ws.getIndTrchDiGar().getRendtoRetr() >= 0) {
            return getTgaRendtoRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRendtoRetrObj(AfDecimal tgaRendtoRetrObj) {
        if (tgaRendtoRetrObj != null) {
            setTgaRendtoRetr(new AfDecimal(tgaRendtoRetrObj, 14, 9));
            ws.getIndTrchDiGar().setRendtoRetr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRendtoRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRisMat() {
        return trchDiGar.getTgaRisMat().getTgaRisMat();
    }

    @Override
    public void setTgaRisMat(AfDecimal tgaRisMat) {
        this.trchDiGar.getTgaRisMat().setTgaRisMat(tgaRisMat.copy());
    }

    @Override
    public AfDecimal getTgaRisMatObj() {
        if (ws.getIndTrchDiGar().getRisMat() >= 0) {
            return getTgaRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRisMatObj(AfDecimal tgaRisMatObj) {
        if (tgaRisMatObj != null) {
            setTgaRisMat(new AfDecimal(tgaRisMatObj, 15, 3));
            ws.getIndTrchDiGar().setRisMat(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRisMat(((short)-1));
        }
    }

    @Override
    public char getTgaTpAdegAbb() {
        return trchDiGar.getTgaTpAdegAbb();
    }

    @Override
    public void setTgaTpAdegAbb(char tgaTpAdegAbb) {
        this.trchDiGar.setTgaTpAdegAbb(tgaTpAdegAbb);
    }

    @Override
    public Character getTgaTpAdegAbbObj() {
        if (ws.getIndTrchDiGar().getTpAdegAbb() >= 0) {
            return ((Character)getTgaTpAdegAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpAdegAbbObj(Character tgaTpAdegAbbObj) {
        if (tgaTpAdegAbbObj != null) {
            setTgaTpAdegAbb(((char)tgaTpAdegAbbObj));
            ws.getIndTrchDiGar().setTpAdegAbb(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTpAdegAbb(((short)-1));
        }
    }

    @Override
    public String getTgaTpManfeeAppl() {
        return trchDiGar.getTgaTpManfeeAppl();
    }

    @Override
    public void setTgaTpManfeeAppl(String tgaTpManfeeAppl) {
        this.trchDiGar.setTgaTpManfeeAppl(tgaTpManfeeAppl);
    }

    @Override
    public String getTgaTpManfeeApplObj() {
        if (ws.getIndTrchDiGar().getTpManfeeAppl() >= 0) {
            return getTgaTpManfeeAppl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpManfeeApplObj(String tgaTpManfeeApplObj) {
        if (tgaTpManfeeApplObj != null) {
            setTgaTpManfeeAppl(tgaTpManfeeApplObj);
            ws.getIndTrchDiGar().setTpManfeeAppl(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTpManfeeAppl(((short)-1));
        }
    }

    @Override
    public String getTgaTpRgmFisc() {
        return trchDiGar.getTgaTpRgmFisc();
    }

    @Override
    public void setTgaTpRgmFisc(String tgaTpRgmFisc) {
        this.trchDiGar.setTgaTpRgmFisc(tgaTpRgmFisc);
    }

    @Override
    public String getTgaTpRival() {
        return trchDiGar.getTgaTpRival();
    }

    @Override
    public void setTgaTpRival(String tgaTpRival) {
        this.trchDiGar.setTgaTpRival(tgaTpRival);
    }

    @Override
    public String getTgaTpRivalObj() {
        if (ws.getIndTrchDiGar().getTpRival() >= 0) {
            return getTgaTpRival();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpRivalObj(String tgaTpRivalObj) {
        if (tgaTpRivalObj != null) {
            setTgaTpRival(tgaTpRivalObj);
            ws.getIndTrchDiGar().setTpRival(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTpRival(((short)-1));
        }
    }

    @Override
    public String getTgaTpTrch() {
        return trchDiGar.getTgaTpTrch();
    }

    @Override
    public void setTgaTpTrch(String tgaTpTrch) {
        this.trchDiGar.setTgaTpTrch(tgaTpTrch);
    }

    @Override
    public AfDecimal getTgaTsRivalFis() {
        return trchDiGar.getTgaTsRivalFis().getTgaTsRivalFis();
    }

    @Override
    public void setTgaTsRivalFis(AfDecimal tgaTsRivalFis) {
        this.trchDiGar.getTgaTsRivalFis().setTgaTsRivalFis(tgaTsRivalFis.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalFisObj() {
        if (ws.getIndTrchDiGar().getTsRivalFis() >= 0) {
            return getTgaTsRivalFis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalFisObj(AfDecimal tgaTsRivalFisObj) {
        if (tgaTsRivalFisObj != null) {
            setTgaTsRivalFis(new AfDecimal(tgaTsRivalFisObj, 14, 9));
            ws.getIndTrchDiGar().setTsRivalFis(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTsRivalFis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaTsRivalIndiciz() {
        return trchDiGar.getTgaTsRivalIndiciz().getTgaTsRivalIndiciz();
    }

    @Override
    public void setTgaTsRivalIndiciz(AfDecimal tgaTsRivalIndiciz) {
        this.trchDiGar.getTgaTsRivalIndiciz().setTgaTsRivalIndiciz(tgaTsRivalIndiciz.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalIndicizObj() {
        if (ws.getIndTrchDiGar().getTsRivalIndiciz() >= 0) {
            return getTgaTsRivalIndiciz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalIndicizObj(AfDecimal tgaTsRivalIndicizObj) {
        if (tgaTsRivalIndicizObj != null) {
            setTgaTsRivalIndiciz(new AfDecimal(tgaTsRivalIndicizObj, 14, 9));
            ws.getIndTrchDiGar().setTsRivalIndiciz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTsRivalIndiciz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaTsRivalNet() {
        return trchDiGar.getTgaTsRivalNet().getTgaTsRivalNet();
    }

    @Override
    public void setTgaTsRivalNet(AfDecimal tgaTsRivalNet) {
        this.trchDiGar.getTgaTsRivalNet().setTgaTsRivalNet(tgaTsRivalNet.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalNetObj() {
        if (ws.getIndTrchDiGar().getTsRivalNet() >= 0) {
            return getTgaTsRivalNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalNetObj(AfDecimal tgaTsRivalNetObj) {
        if (tgaTsRivalNetObj != null) {
            setTgaTsRivalNet(new AfDecimal(tgaTsRivalNetObj, 14, 9));
            ws.getIndTrchDiGar().setTsRivalNet(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTsRivalNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaVisEnd2000() {
        return trchDiGar.getTgaVisEnd2000().getTgaVisEnd2000();
    }

    @Override
    public void setTgaVisEnd2000(AfDecimal tgaVisEnd2000) {
        this.trchDiGar.getTgaVisEnd2000().setTgaVisEnd2000(tgaVisEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaVisEnd2000Nforz() {
        return trchDiGar.getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz();
    }

    @Override
    public void setTgaVisEnd2000Nforz(AfDecimal tgaVisEnd2000Nforz) {
        this.trchDiGar.getTgaVisEnd2000Nforz().setTgaVisEnd2000Nforz(tgaVisEnd2000Nforz.copy());
    }

    @Override
    public AfDecimal getTgaVisEnd2000NforzObj() {
        if (ws.getIndTrchDiGar().getVisEnd2000Nforz() >= 0) {
            return getTgaVisEnd2000Nforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaVisEnd2000NforzObj(AfDecimal tgaVisEnd2000NforzObj) {
        if (tgaVisEnd2000NforzObj != null) {
            setTgaVisEnd2000Nforz(new AfDecimal(tgaVisEnd2000NforzObj, 15, 3));
            ws.getIndTrchDiGar().setVisEnd2000Nforz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setVisEnd2000Nforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaVisEnd2000Obj() {
        if (ws.getIndTrchDiGar().getVisEnd2000() >= 0) {
            return getTgaVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaVisEnd2000Obj(AfDecimal tgaVisEnd2000Obj) {
        if (tgaVisEnd2000Obj != null) {
            setTgaVisEnd2000(new AfDecimal(tgaVisEnd2000Obj, 15, 3));
            ws.getIndTrchDiGar().setVisEnd2000(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setVisEnd2000(((short)-1));
        }
    }

    @Override
    public int getWkIdAdesA() {
        return ws.getWkIdAdesA();
    }

    @Override
    public void setWkIdAdesA(int wkIdAdesA) {
        this.ws.setWkIdAdesA(wkIdAdesA);
    }

    @Override
    public int getWkIdAdesDa() {
        return ws.getWkIdAdesDa();
    }

    @Override
    public void setWkIdAdesDa(int wkIdAdesDa) {
        this.ws.setWkIdAdesDa(wkIdAdesDa);
    }

    @Override
    public int getWkIdGarA() {
        return ws.getWkIdGarA();
    }

    @Override
    public void setWkIdGarA(int wkIdGarA) {
        this.ws.setWkIdGarA(wkIdGarA);
    }

    @Override
    public int getWkIdGarDa() {
        return ws.getWkIdGarDa();
    }

    @Override
    public void setWkIdGarDa(int wkIdGarDa) {
        this.ws.setWkIdGarDa(wkIdGarDa);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
