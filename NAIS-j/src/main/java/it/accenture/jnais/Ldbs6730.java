package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ParamInfrApplDao;
import it.accenture.jnais.commons.data.to.IParamInfrAppl;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs6730Data;
import it.accenture.jnais.ws.ParamInfrAppl;
import it.accenture.jnais.ws.redefines.D09CsocketPortNum;
import it.accenture.jnais.ws.redefines.D09MqTempoAttesa1;
import it.accenture.jnais.ws.redefines.D09MqTempoAttesa2;
import it.accenture.jnais.ws.redefines.D09MqTempoExpiry;

/**Original name: LDBS6730<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 MAG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs6730 extends Program implements IParamInfrAppl {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ParamInfrApplDao paramInfrApplDao = new ParamInfrApplDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs6730Data ws = new Ldbs6730Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PARAM-INFR-APPL
    private ParamInfrAppl paramInfrAppl;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS6730_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ParamInfrAppl paramInfrAppl) {
        this.idsv0003 = idsv0003;
        this.paramInfrAppl = paramInfrAppl;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs6730 getInstance() {
        return ((Ldbs6730)Programs.getInstance(Ldbs6730.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS6730'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS6730");
        // COB_CODE: MOVE 'PARAM-INFR-APPL' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PARAM-INFR-APPL");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT  COD_COMP_ANIA
        //                   ,AMBIENTE
        //                   ,PIATTAFORMA
        //                   ,TP_COM_COBOL_JAVA
        //                   ,MQ_TP_UTILIZZO_API
        //                   ,MQ_QUEUE_MANAGER
        //                   ,MQ_CODA_PUT
        //                   ,MQ_CODA_GET
        //                   ,MQ_OPZ_PERSISTENZA
        //                   ,MQ_OPZ_WAIT
        //                   ,MQ_OPZ_SYNCPOINT
        //                   ,MQ_ATTESA_RISPOSTA
        //                   ,MQ_TEMPO_ATTESA_1
        //                   ,MQ_TEMPO_ATTESA_2
        //                   ,MQ_TEMPO_EXPIRY
        //                   ,CSOCKET_IP_ADDRESS
        //                   ,CSOCKET_PORT_NUM
        //                   ,FL_COMPRESSORE_C
        //            INTO   :D09-COD-COMP-ANIA
        //                  ,:D09-AMBIENTE
        //                  ,:D09-PIATTAFORMA
        //                  ,:D09-TP-COM-COBOL-JAVA
        //                  ,:D09-MQ-TP-UTILIZZO-API
        //                   :IND-D09-MQ-TP-UTILIZZO-API
        //                  ,:D09-MQ-QUEUE-MANAGER
        //                   :IND-D09-MQ-QUEUE-MANAGER
        //                  ,:D09-MQ-CODA-PUT
        //                   :IND-D09-MQ-CODA-PUT
        //                  ,:D09-MQ-CODA-GET
        //                   :IND-D09-MQ-CODA-GET
        //                  ,:D09-MQ-OPZ-PERSISTENZA
        //                   :IND-D09-MQ-OPZ-PERSISTENZA
        //                  ,:D09-MQ-OPZ-WAIT
        //                   :IND-D09-MQ-OPZ-WAIT
        //                  ,:D09-MQ-OPZ-SYNCPOINT
        //                   :IND-D09-MQ-OPZ-SYNCPOINT
        //                  ,:D09-MQ-ATTESA-RISPOSTA
        //                   :IND-D09-MQ-ATTESA-RISPOSTA
        //                  ,:D09-MQ-TEMPO-ATTESA-1
        //                   :IND-D09-MQ-TEMPO-ATTESA-1
        //                  ,:D09-MQ-TEMPO-ATTESA-2
        //                   :IND-D09-MQ-TEMPO-ATTESA-2
        //                  ,:D09-MQ-TEMPO-EXPIRY
        //                   :IND-D09-MQ-TEMPO-EXPIRY
        //                  ,:D09-CSOCKET-IP-ADDRESS
        //                   :IND-D09-CSOCKET-IP-ADDRESS
        //                  ,:D09-CSOCKET-PORT-NUM
        //                   :IND-D09-CSOCKET-PORT-NUM
        //                  ,:D09-FL-COMPRESSORE-C
        //                   :IND-D09-FL-COMPRESSORE-C
        //            FROM PARAM_INFR_APPL
        //            WHERE  COD_COMP_ANIA >=
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        paramInfrApplDao.selectByIdsv0003CodiceCompagniaAnia(idsv0003.getCodiceCompagniaAnia(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-D09-MQ-TP-UTILIZZO-API = -1
        //              MOVE HIGH-VALUES TO D09-MQ-TP-UTILIZZO-API-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-TP-UTILIZZO-API-NULL
            paramInfrAppl.setD09MqTpUtilizzoApi(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_MQ_TP_UTILIZZO_API));
        }
        // COB_CODE: IF IND-D09-MQ-QUEUE-MANAGER = -1
        //              MOVE HIGH-VALUES TO D09-MQ-QUEUE-MANAGER-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-QUEUE-MANAGER-NULL
            paramInfrAppl.setD09MqQueueManager(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_MQ_QUEUE_MANAGER));
        }
        // COB_CODE: IF IND-D09-MQ-CODA-PUT = -1
        //              MOVE HIGH-VALUES TO D09-MQ-CODA-PUT-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getNumQuo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-CODA-PUT-NULL
            paramInfrAppl.setD09MqCodaPut(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_MQ_CODA_PUT));
        }
        // COB_CODE: IF IND-D09-MQ-CODA-GET = -1
        //              MOVE HIGH-VALUES TO D09-MQ-CODA-GET-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getPc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-CODA-GET-NULL
            paramInfrAppl.setD09MqCodaGet(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_MQ_CODA_GET));
        }
        // COB_CODE: IF IND-D09-MQ-OPZ-PERSISTENZA = -1
        //              MOVE HIGH-VALUES TO D09-MQ-OPZ-PERSISTENZA-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getImpMovto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-OPZ-PERSISTENZA-NULL
            paramInfrAppl.setD09MqOpzPersistenza(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-D09-MQ-OPZ-WAIT = -1
        //              MOVE HIGH-VALUES TO D09-MQ-OPZ-WAIT-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getDtInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-OPZ-WAIT-NULL
            paramInfrAppl.setD09MqOpzWait(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-D09-MQ-OPZ-SYNCPOINT = -1
        //              MOVE HIGH-VALUES TO D09-MQ-OPZ-SYNCPOINT-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-OPZ-SYNCPOINT-NULL
            paramInfrAppl.setD09MqOpzSyncpoint(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-D09-MQ-ATTESA-RISPOSTA = -1
        //              MOVE HIGH-VALUES TO D09-MQ-ATTESA-RISPOSTA-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getTpStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-ATTESA-RISPOSTA-NULL
            paramInfrAppl.setD09MqAttesaRisposta(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-D09-MQ-TEMPO-ATTESA-1 = -1
        //              MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-1-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getTpModInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-1-NULL
            paramInfrAppl.getD09MqTempoAttesa1().setD09MqTempoAttesa1Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D09MqTempoAttesa1.Len.D09_MQ_TEMPO_ATTESA1_NULL));
        }
        // COB_CODE: IF IND-D09-MQ-TEMPO-ATTESA-2 = -1
        //              MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-2-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getDtCambioVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-TEMPO-ATTESA-2-NULL
            paramInfrAppl.getD09MqTempoAttesa2().setD09MqTempoAttesa2Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D09MqTempoAttesa2.Len.D09_MQ_TEMPO_ATTESA2_NULL));
        }
        // COB_CODE: IF IND-D09-MQ-TEMPO-EXPIRY = -1
        //              MOVE HIGH-VALUES TO D09-MQ-TEMPO-EXPIRY-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getDtInvstCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-MQ-TEMPO-EXPIRY-NULL
            paramInfrAppl.getD09MqTempoExpiry().setD09MqTempoExpiryNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D09MqTempoExpiry.Len.D09_MQ_TEMPO_EXPIRY_NULL));
        }
        // COB_CODE: IF IND-D09-CSOCKET-IP-ADDRESS = -1
        //              MOVE HIGH-VALUES TO D09-CSOCKET-IP-ADDRESS-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getFlCalcInvto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-CSOCKET-IP-ADDRESS-NULL
            paramInfrAppl.setD09CsocketIpAddress(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamInfrAppl.Len.D09_CSOCKET_IP_ADDRESS));
        }
        // COB_CODE: IF IND-D09-CSOCKET-PORT-NUM = -1
        //              MOVE HIGH-VALUES TO D09-CSOCKET-PORT-NUM-NULL
        //           END-IF
        if (ws.getIndParamInfrAppl().getImpGapEvent() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-CSOCKET-PORT-NUM-NULL
            paramInfrAppl.getD09CsocketPortNum().setD09CsocketPortNumNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D09CsocketPortNum.Len.D09_CSOCKET_PORT_NUM_NULL));
        }
        // COB_CODE: IF IND-D09-FL-COMPRESSORE-C = -1
        //              MOVE HIGH-VALUES TO D09-FL-COMPRESSORE-C-NULL
        //           END-IF.
        if (ws.getIndParamInfrAppl().getFlSwmBp2s() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO D09-FL-COMPRESSORE-C-NULL
            paramInfrAppl.setD09FlCompressoreC(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    @Override
    public String getAmbiente() {
        return paramInfrAppl.getD09Ambiente();
    }

    @Override
    public void setAmbiente(String ambiente) {
        this.paramInfrAppl.setD09Ambiente(ambiente);
    }

    @Override
    public String getCsocketIpAddress() {
        return paramInfrAppl.getD09CsocketIpAddress();
    }

    @Override
    public void setCsocketIpAddress(String csocketIpAddress) {
        this.paramInfrAppl.setD09CsocketIpAddress(csocketIpAddress);
    }

    @Override
    public String getCsocketIpAddressObj() {
        if (ws.getIndParamInfrAppl().getFlCalcInvto() >= 0) {
            return getCsocketIpAddress();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCsocketIpAddressObj(String csocketIpAddressObj) {
        if (csocketIpAddressObj != null) {
            setCsocketIpAddress(csocketIpAddressObj);
            ws.getIndParamInfrAppl().setFlCalcInvto(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setFlCalcInvto(((short)-1));
        }
    }

    @Override
    public int getCsocketPortNum() {
        return paramInfrAppl.getD09CsocketPortNum().getD09CsocketPortNum();
    }

    @Override
    public void setCsocketPortNum(int csocketPortNum) {
        this.paramInfrAppl.getD09CsocketPortNum().setD09CsocketPortNum(csocketPortNum);
    }

    @Override
    public Integer getCsocketPortNumObj() {
        if (ws.getIndParamInfrAppl().getImpGapEvent() >= 0) {
            return ((Integer)getCsocketPortNum());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCsocketPortNumObj(Integer csocketPortNumObj) {
        if (csocketPortNumObj != null) {
            setCsocketPortNum(((int)csocketPortNumObj));
            ws.getIndParamInfrAppl().setImpGapEvent(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setImpGapEvent(((short)-1));
        }
    }

    @Override
    public int getD09CodCompAnia() {
        return paramInfrAppl.getD09CodCompAnia();
    }

    @Override
    public void setD09CodCompAnia(int d09CodCompAnia) {
        this.paramInfrAppl.setD09CodCompAnia(d09CodCompAnia);
    }

    @Override
    public char getFlCompressoreC() {
        return paramInfrAppl.getD09FlCompressoreC();
    }

    @Override
    public void setFlCompressoreC(char flCompressoreC) {
        this.paramInfrAppl.setD09FlCompressoreC(flCompressoreC);
    }

    @Override
    public Character getFlCompressoreCObj() {
        if (ws.getIndParamInfrAppl().getFlSwmBp2s() >= 0) {
            return ((Character)getFlCompressoreC());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCompressoreCObj(Character flCompressoreCObj) {
        if (flCompressoreCObj != null) {
            setFlCompressoreC(((char)flCompressoreCObj));
            ws.getIndParamInfrAppl().setFlSwmBp2s(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setFlSwmBp2s(((short)-1));
        }
    }

    @Override
    public char getMqAttesaRisposta() {
        return paramInfrAppl.getD09MqAttesaRisposta();
    }

    @Override
    public void setMqAttesaRisposta(char mqAttesaRisposta) {
        this.paramInfrAppl.setD09MqAttesaRisposta(mqAttesaRisposta);
    }

    @Override
    public Character getMqAttesaRispostaObj() {
        if (ws.getIndParamInfrAppl().getTpStat() >= 0) {
            return ((Character)getMqAttesaRisposta());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqAttesaRispostaObj(Character mqAttesaRispostaObj) {
        if (mqAttesaRispostaObj != null) {
            setMqAttesaRisposta(((char)mqAttesaRispostaObj));
            ws.getIndParamInfrAppl().setTpStat(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setTpStat(((short)-1));
        }
    }

    @Override
    public String getMqCodaGet() {
        return paramInfrAppl.getD09MqCodaGet();
    }

    @Override
    public void setMqCodaGet(String mqCodaGet) {
        this.paramInfrAppl.setD09MqCodaGet(mqCodaGet);
    }

    @Override
    public String getMqCodaGetObj() {
        if (ws.getIndParamInfrAppl().getPc() >= 0) {
            return getMqCodaGet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqCodaGetObj(String mqCodaGetObj) {
        if (mqCodaGetObj != null) {
            setMqCodaGet(mqCodaGetObj);
            ws.getIndParamInfrAppl().setPc(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setPc(((short)-1));
        }
    }

    @Override
    public String getMqCodaPut() {
        return paramInfrAppl.getD09MqCodaPut();
    }

    @Override
    public void setMqCodaPut(String mqCodaPut) {
        this.paramInfrAppl.setD09MqCodaPut(mqCodaPut);
    }

    @Override
    public String getMqCodaPutObj() {
        if (ws.getIndParamInfrAppl().getNumQuo() >= 0) {
            return getMqCodaPut();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqCodaPutObj(String mqCodaPutObj) {
        if (mqCodaPutObj != null) {
            setMqCodaPut(mqCodaPutObj);
            ws.getIndParamInfrAppl().setNumQuo(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setNumQuo(((short)-1));
        }
    }

    @Override
    public char getMqOpzPersistenza() {
        return paramInfrAppl.getD09MqOpzPersistenza();
    }

    @Override
    public void setMqOpzPersistenza(char mqOpzPersistenza) {
        this.paramInfrAppl.setD09MqOpzPersistenza(mqOpzPersistenza);
    }

    @Override
    public Character getMqOpzPersistenzaObj() {
        if (ws.getIndParamInfrAppl().getImpMovto() >= 0) {
            return ((Character)getMqOpzPersistenza());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqOpzPersistenzaObj(Character mqOpzPersistenzaObj) {
        if (mqOpzPersistenzaObj != null) {
            setMqOpzPersistenza(((char)mqOpzPersistenzaObj));
            ws.getIndParamInfrAppl().setImpMovto(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setImpMovto(((short)-1));
        }
    }

    @Override
    public char getMqOpzSyncpoint() {
        return paramInfrAppl.getD09MqOpzSyncpoint();
    }

    @Override
    public void setMqOpzSyncpoint(char mqOpzSyncpoint) {
        this.paramInfrAppl.setD09MqOpzSyncpoint(mqOpzSyncpoint);
    }

    @Override
    public Character getMqOpzSyncpointObj() {
        if (ws.getIndParamInfrAppl().getCodTari() >= 0) {
            return ((Character)getMqOpzSyncpoint());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqOpzSyncpointObj(Character mqOpzSyncpointObj) {
        if (mqOpzSyncpointObj != null) {
            setMqOpzSyncpoint(((char)mqOpzSyncpointObj));
            ws.getIndParamInfrAppl().setCodTari(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setCodTari(((short)-1));
        }
    }

    @Override
    public char getMqOpzWait() {
        return paramInfrAppl.getD09MqOpzWait();
    }

    @Override
    public void setMqOpzWait(char mqOpzWait) {
        this.paramInfrAppl.setD09MqOpzWait(mqOpzWait);
    }

    @Override
    public Character getMqOpzWaitObj() {
        if (ws.getIndParamInfrAppl().getDtInvst() >= 0) {
            return ((Character)getMqOpzWait());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqOpzWaitObj(Character mqOpzWaitObj) {
        if (mqOpzWaitObj != null) {
            setMqOpzWait(((char)mqOpzWaitObj));
            ws.getIndParamInfrAppl().setDtInvst(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setDtInvst(((short)-1));
        }
    }

    @Override
    public String getMqQueueManager() {
        return paramInfrAppl.getD09MqQueueManager();
    }

    @Override
    public void setMqQueueManager(String mqQueueManager) {
        this.paramInfrAppl.setD09MqQueueManager(mqQueueManager);
    }

    @Override
    public String getMqQueueManagerObj() {
        if (ws.getIndParamInfrAppl().getCodFnd() >= 0) {
            return getMqQueueManager();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqQueueManagerObj(String mqQueueManagerObj) {
        if (mqQueueManagerObj != null) {
            setMqQueueManager(mqQueueManagerObj);
            ws.getIndParamInfrAppl().setCodFnd(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setCodFnd(((short)-1));
        }
    }

    @Override
    public long getMqTempoAttesa1() {
        return paramInfrAppl.getD09MqTempoAttesa1().getD09MqTempoAttesa1();
    }

    @Override
    public void setMqTempoAttesa1(long mqTempoAttesa1) {
        this.paramInfrAppl.getD09MqTempoAttesa1().setD09MqTempoAttesa1(mqTempoAttesa1);
    }

    @Override
    public Long getMqTempoAttesa1Obj() {
        if (ws.getIndParamInfrAppl().getTpModInvst() >= 0) {
            return ((Long)getMqTempoAttesa1());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqTempoAttesa1Obj(Long mqTempoAttesa1Obj) {
        if (mqTempoAttesa1Obj != null) {
            setMqTempoAttesa1(((long)mqTempoAttesa1Obj));
            ws.getIndParamInfrAppl().setTpModInvst(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setTpModInvst(((short)-1));
        }
    }

    @Override
    public long getMqTempoAttesa2() {
        return paramInfrAppl.getD09MqTempoAttesa2().getD09MqTempoAttesa2();
    }

    @Override
    public void setMqTempoAttesa2(long mqTempoAttesa2) {
        this.paramInfrAppl.getD09MqTempoAttesa2().setD09MqTempoAttesa2(mqTempoAttesa2);
    }

    @Override
    public Long getMqTempoAttesa2Obj() {
        if (ws.getIndParamInfrAppl().getDtCambioVlt() >= 0) {
            return ((Long)getMqTempoAttesa2());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqTempoAttesa2Obj(Long mqTempoAttesa2Obj) {
        if (mqTempoAttesa2Obj != null) {
            setMqTempoAttesa2(((long)mqTempoAttesa2Obj));
            ws.getIndParamInfrAppl().setDtCambioVlt(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setDtCambioVlt(((short)-1));
        }
    }

    @Override
    public long getMqTempoExpiry() {
        return paramInfrAppl.getD09MqTempoExpiry().getD09MqTempoExpiry();
    }

    @Override
    public void setMqTempoExpiry(long mqTempoExpiry) {
        this.paramInfrAppl.getD09MqTempoExpiry().setD09MqTempoExpiry(mqTempoExpiry);
    }

    @Override
    public Long getMqTempoExpiryObj() {
        if (ws.getIndParamInfrAppl().getDtInvstCalc() >= 0) {
            return ((Long)getMqTempoExpiry());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqTempoExpiryObj(Long mqTempoExpiryObj) {
        if (mqTempoExpiryObj != null) {
            setMqTempoExpiry(((long)mqTempoExpiryObj));
            ws.getIndParamInfrAppl().setDtInvstCalc(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setDtInvstCalc(((short)-1));
        }
    }

    @Override
    public String getMqTpUtilizzoApi() {
        return paramInfrAppl.getD09MqTpUtilizzoApi();
    }

    @Override
    public void setMqTpUtilizzoApi(String mqTpUtilizzoApi) {
        this.paramInfrAppl.setD09MqTpUtilizzoApi(mqTpUtilizzoApi);
    }

    @Override
    public String getMqTpUtilizzoApiObj() {
        if (ws.getIndParamInfrAppl().getIdMoviChiu() >= 0) {
            return getMqTpUtilizzoApi();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMqTpUtilizzoApiObj(String mqTpUtilizzoApiObj) {
        if (mqTpUtilizzoApiObj != null) {
            setMqTpUtilizzoApi(mqTpUtilizzoApiObj);
            ws.getIndParamInfrAppl().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndParamInfrAppl().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public String getPiattaforma() {
        return paramInfrAppl.getD09Piattaforma();
    }

    @Override
    public void setPiattaforma(String piattaforma) {
        this.paramInfrAppl.setD09Piattaforma(piattaforma);
    }

    @Override
    public String getTpComCobolJava() {
        return paramInfrAppl.getD09TpComCobolJava();
    }

    @Override
    public void setTpComCobolJava(String tpComCobolJava) {
        this.paramInfrAppl.setD09TpComCobolJava(tpComCobolJava);
    }
}
