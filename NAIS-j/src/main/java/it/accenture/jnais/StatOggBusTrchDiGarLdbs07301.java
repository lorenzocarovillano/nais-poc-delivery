package it.accenture.jnais;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.IStatOggBusTrchDiGar;
import it.accenture.jnais.ws.Ldbs0730Data;

/**Original name: StatOggBusTrchDiGarLdbs0730<br>*/
public class StatOggBusTrchDiGarLdbs07301 implements IStatOggBusTrchDiGar {

    //==== PROPERTIES ====
    private Ldbs0730Data ws;

    //==== CONSTRUCTORS ====
    public StatOggBusTrchDiGarLdbs07301(Ldbs0730Data ws) {
        this.ws = ws;
    }

    //==== METHODS ====
    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public int getLc601IdMoviCrz() {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public void setLc601IdMoviCrz(int lc601IdMoviCrz) {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public int getLc601IdTrchDiGar() {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public void setLc601IdTrchDiGar(int lc601IdTrchDiGar) {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public int getLdbi0731IdAdes() {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public void setLdbi0731IdAdes(int ldbi0731IdAdes) {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public int getLdbi0731IdPoli() {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public void setLdbi0731IdPoli(int ldbi0731IdPoli) {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public int getLdbi0731IdTrch() {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public void setLdbi0731IdTrch(int ldbi0731IdTrch) {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public String getLdbi0731TpStatBus() {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    @Override
    public void setLdbi0731TpStatBus(String ldbi0731TpStatBus) {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    @Override
    public String getLdbv1361TpCaus01() {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public void setLdbv1361TpCaus01(String ldbv1361TpCaus01) {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public String getLdbv1361TpCaus02() {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public void setLdbv1361TpCaus02(String ldbv1361TpCaus02) {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public String getLdbv1361TpCaus03() {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public void setLdbv1361TpCaus03(String ldbv1361TpCaus03) {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public String getLdbv1361TpCaus04() {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public void setLdbv1361TpCaus04(String ldbv1361TpCaus04) {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public String getLdbv1361TpCaus05() {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public void setLdbv1361TpCaus05(String ldbv1361TpCaus05) {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public String getLdbv1361TpCaus06() {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public void setLdbv1361TpCaus06(String ldbv1361TpCaus06) {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public String getLdbv1361TpCaus07() {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public void setLdbv1361TpCaus07(String ldbv1361TpCaus07) {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public String getLdbv1361TpStatBus01() {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public void setLdbv1361TpStatBus01(String ldbv1361TpStatBus01) {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public String getLdbv1361TpStatBus02() {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public void setLdbv1361TpStatBus02(String ldbv1361TpStatBus02) {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public String getLdbv1361TpStatBus03() {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public void setLdbv1361TpStatBus03(String ldbv1361TpStatBus03) {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public String getLdbv1361TpStatBus04() {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public void setLdbv1361TpStatBus04(String ldbv1361TpStatBus04) {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public String getLdbv1361TpStatBus05() {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public void setLdbv1361TpStatBus05(String ldbv1361TpStatBus05) {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public String getLdbv1361TpStatBus06() {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public void setLdbv1361TpStatBus06(String ldbv1361TpStatBus06) {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public String getLdbv1361TpStatBus07() {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public void setLdbv1361TpStatBus07(String ldbv1361TpStatBus07) {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public int getLdbv2911IdAdes() {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public void setLdbv2911IdAdes(int ldbv2911IdAdes) {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public int getLdbv2911IdPoli() {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public void setLdbv2911IdPoli(int ldbv2911IdPoli) {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public AfDecimal getLdbv2911PrstzIniNewfis() {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public void setLdbv2911PrstzIniNewfis(AfDecimal ldbv2911PrstzIniNewfis) {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public int getLdbv3021IdOgg() {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public void setLdbv3021IdOgg(int ldbv3021IdOgg) {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public String getLdbv3021TpCausBus10() {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public void setLdbv3021TpCausBus10(String ldbv3021TpCausBus10) {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public String getLdbv3021TpCausBus11() {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public void setLdbv3021TpCausBus11(String ldbv3021TpCausBus11) {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public String getLdbv3021TpCausBus12() {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public void setLdbv3021TpCausBus12(String ldbv3021TpCausBus12) {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public String getLdbv3021TpCausBus13() {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public void setLdbv3021TpCausBus13(String ldbv3021TpCausBus13) {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public String getLdbv3021TpCausBus14() {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public void setLdbv3021TpCausBus14(String ldbv3021TpCausBus14) {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public String getLdbv3021TpCausBus15() {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public void setLdbv3021TpCausBus15(String ldbv3021TpCausBus15) {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public String getLdbv3021TpCausBus16() {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public void setLdbv3021TpCausBus16(String ldbv3021TpCausBus16) {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public String getLdbv3021TpCausBus17() {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public void setLdbv3021TpCausBus17(String ldbv3021TpCausBus17) {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public String getLdbv3021TpCausBus18() {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public void setLdbv3021TpCausBus18(String ldbv3021TpCausBus18) {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public String getLdbv3021TpCausBus19() {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public void setLdbv3021TpCausBus19(String ldbv3021TpCausBus19) {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public String getLdbv3021TpCausBus1() {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public void setLdbv3021TpCausBus1(String ldbv3021TpCausBus1) {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public String getLdbv3021TpCausBus20() {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public void setLdbv3021TpCausBus20(String ldbv3021TpCausBus20) {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public String getLdbv3021TpCausBus2() {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public void setLdbv3021TpCausBus2(String ldbv3021TpCausBus2) {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public String getLdbv3021TpCausBus3() {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public void setLdbv3021TpCausBus3(String ldbv3021TpCausBus3) {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public String getLdbv3021TpCausBus4() {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public void setLdbv3021TpCausBus4(String ldbv3021TpCausBus4) {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public String getLdbv3021TpCausBus5() {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public void setLdbv3021TpCausBus5(String ldbv3021TpCausBus5) {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public String getLdbv3021TpCausBus6() {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public void setLdbv3021TpCausBus6(String ldbv3021TpCausBus6) {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public String getLdbv3021TpCausBus7() {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public void setLdbv3021TpCausBus7(String ldbv3021TpCausBus7) {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public String getLdbv3021TpCausBus8() {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public void setLdbv3021TpCausBus8(String ldbv3021TpCausBus8) {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public String getLdbv3021TpCausBus9() {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public void setLdbv3021TpCausBus9(String ldbv3021TpCausBus9) {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public String getLdbv3021TpOgg() {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public void setLdbv3021TpOgg(String ldbv3021TpOgg) {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public String getLdbv3021TpStatBus() {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public void setLdbv3021TpStatBus(String ldbv3021TpStatBus) {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public int getLdbv3421IdAdes() {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public void setLdbv3421IdAdes(int ldbv3421IdAdes) {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public int getLdbv3421IdGar() {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public void setLdbv3421IdGar(int ldbv3421IdGar) {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public int getLdbv3421IdPoli() {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public void setLdbv3421IdPoli(int ldbv3421IdPoli) {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public String getLdbv3421TpOgg() {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public void setLdbv3421TpOgg(String ldbv3421TpOgg) {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public String getLdbv3421TpStatBus1() {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public void setLdbv3421TpStatBus1(String ldbv3421TpStatBus1) {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public String getLdbv3421TpStatBus2() {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public void setLdbv3421TpStatBus2(String ldbv3421TpStatBus2) {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public int getLdbvd511IdAdes() {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public void setLdbvd511IdAdes(int ldbvd511IdAdes) {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public int getLdbvd511IdPoli() {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public void setLdbvd511IdPoli(int ldbvd511IdPoli) {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public String getLdbvd511TpCaus() {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public void setLdbvd511TpCaus(String ldbvd511TpCaus) {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public String getLdbvd511TpOgg() {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public void setLdbvd511TpOgg(String ldbvd511TpOgg) {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public String getLdbvd511TpStatBus() {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public void setLdbvd511TpStatBus(String ldbvd511TpStatBus) {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public int getLdbve251IdAdes() {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public void setLdbve251IdAdes(int ldbve251IdAdes) {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public int getLdbve251IdMovi() {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public void setLdbve251IdMovi(int ldbve251IdMovi) {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public AfDecimal getLdbve251ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public void setLdbve251ImpbVisEnd2000(AfDecimal ldbve251ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public String getLdbve251TpTrch10() {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public void setLdbve251TpTrch10(String ldbve251TpTrch10) {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public String getLdbve251TpTrch11() {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public void setLdbve251TpTrch11(String ldbve251TpTrch11) {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public String getLdbve251TpTrch12() {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public void setLdbve251TpTrch12(String ldbve251TpTrch12) {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public String getLdbve251TpTrch13() {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public void setLdbve251TpTrch13(String ldbve251TpTrch13) {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public String getLdbve251TpTrch14() {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public void setLdbve251TpTrch14(String ldbve251TpTrch14) {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public String getLdbve251TpTrch15() {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public void setLdbve251TpTrch15(String ldbve251TpTrch15) {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public String getLdbve251TpTrch16() {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public void setLdbve251TpTrch16(String ldbve251TpTrch16) {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public String getLdbve251TpTrch1() {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public void setLdbve251TpTrch1(String ldbve251TpTrch1) {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public String getLdbve251TpTrch2() {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public void setLdbve251TpTrch2(String ldbve251TpTrch2) {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public String getLdbve251TpTrch3() {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public void setLdbve251TpTrch3(String ldbve251TpTrch3) {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public String getLdbve251TpTrch4() {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public void setLdbve251TpTrch4(String ldbve251TpTrch4) {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public String getLdbve251TpTrch5() {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public void setLdbve251TpTrch5(String ldbve251TpTrch5) {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public String getLdbve251TpTrch6() {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public void setLdbve251TpTrch6(String ldbve251TpTrch6) {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public String getLdbve251TpTrch7() {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public void setLdbve251TpTrch7(String ldbve251TpTrch7) {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public String getLdbve251TpTrch8() {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public void setLdbve251TpTrch8(String ldbve251TpTrch8) {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public String getLdbve251TpTrch9() {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public void setLdbve251TpTrch9(String ldbve251TpTrch9) {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public int getLdbve261IdAdes() {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public void setLdbve261IdAdes(int ldbve261IdAdes) {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public AfDecimal getLdbve261ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public void setLdbve261ImpbVisEnd2000(AfDecimal ldbve261ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public String getLdbve261TpTrch10() {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public void setLdbve261TpTrch10(String ldbve261TpTrch10) {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public String getLdbve261TpTrch11() {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public void setLdbve261TpTrch11(String ldbve261TpTrch11) {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public String getLdbve261TpTrch12() {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public void setLdbve261TpTrch12(String ldbve261TpTrch12) {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public String getLdbve261TpTrch13() {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public void setLdbve261TpTrch13(String ldbve261TpTrch13) {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public String getLdbve261TpTrch14() {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public void setLdbve261TpTrch14(String ldbve261TpTrch14) {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public String getLdbve261TpTrch15() {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public void setLdbve261TpTrch15(String ldbve261TpTrch15) {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public String getLdbve261TpTrch16() {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public void setLdbve261TpTrch16(String ldbve261TpTrch16) {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public String getLdbve261TpTrch1() {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public void setLdbve261TpTrch1(String ldbve261TpTrch1) {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public String getLdbve261TpTrch2() {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public void setLdbve261TpTrch2(String ldbve261TpTrch2) {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public String getLdbve261TpTrch3() {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public void setLdbve261TpTrch3(String ldbve261TpTrch3) {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public String getLdbve261TpTrch4() {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public void setLdbve261TpTrch4(String ldbve261TpTrch4) {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public String getLdbve261TpTrch5() {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public void setLdbve261TpTrch5(String ldbve261TpTrch5) {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public String getLdbve261TpTrch6() {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public void setLdbve261TpTrch6(String ldbve261TpTrch6) {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public String getLdbve261TpTrch7() {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public void setLdbve261TpTrch7(String ldbve261TpTrch7) {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public String getLdbve261TpTrch8() {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public void setLdbve261TpTrch8(String ldbve261TpTrch8) {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public String getLdbve261TpTrch9() {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public void setLdbve261TpTrch9(String ldbve261TpTrch9) {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public int getStbCodCompAnia() {
        return ws.getStatOggBus().getStbCodCompAnia();
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        ws.getStatOggBus().setStbCodCompAnia(stbCodCompAnia);
    }

    @Override
    public char getStbDsOperSql() {
        return ws.getStatOggBus().getStbDsOperSql();
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        ws.getStatOggBus().setStbDsOperSql(stbDsOperSql);
    }

    @Override
    public long getStbDsRiga() {
        return ws.getStatOggBus().getStbDsRiga();
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        ws.getStatOggBus().setStbDsRiga(stbDsRiga);
    }

    @Override
    public char getStbDsStatoElab() {
        return ws.getStatOggBus().getStbDsStatoElab();
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        ws.getStatOggBus().setStbDsStatoElab(stbDsStatoElab);
    }

    @Override
    public long getStbDsTsEndCptz() {
        return ws.getStatOggBus().getStbDsTsEndCptz();
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        ws.getStatOggBus().setStbDsTsEndCptz(stbDsTsEndCptz);
    }

    @Override
    public long getStbDsTsIniCptz() {
        return ws.getStatOggBus().getStbDsTsIniCptz();
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        ws.getStatOggBus().setStbDsTsIniCptz(stbDsTsIniCptz);
    }

    @Override
    public String getStbDsUtente() {
        return ws.getStatOggBus().getStbDsUtente();
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        ws.getStatOggBus().setStbDsUtente(stbDsUtente);
    }

    @Override
    public int getStbDsVer() {
        return ws.getStatOggBus().getStbDsVer();
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        ws.getStatOggBus().setStbDsVer(stbDsVer);
    }

    @Override
    public String getStbDtEndEffDb() {
        return ws.getIdbvstb3().getStbDtEndEffDb();
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        ws.getIdbvstb3().setStbDtEndEffDb(stbDtEndEffDb);
    }

    @Override
    public String getStbDtIniEffDb() {
        return ws.getIdbvstb3().getStbDtIniEffDb();
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        ws.getIdbvstb3().setStbDtIniEffDb(stbDtIniEffDb);
    }

    @Override
    public int getStbIdMoviChiu() {
        return ws.getStatOggBus().getStbIdMoviChiu().getStbIdMoviChiu();
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        ws.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiu(stbIdMoviChiu);
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        if (ws.getIndStbIdMoviChiu() >= 0) {
            return ((Integer)getStbIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        if (stbIdMoviChiuObj != null) {
            setStbIdMoviChiu(((int)stbIdMoviChiuObj));
            ws.setIndStbIdMoviChiu(((short)0));
        }
        else {
            ws.setIndStbIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getStbIdMoviCrz() {
        return ws.getStatOggBus().getStbIdMoviCrz();
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        ws.getStatOggBus().setStbIdMoviCrz(stbIdMoviCrz);
    }

    @Override
    public int getStbIdOgg() {
        return ws.getStatOggBus().getStbIdOgg();
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        ws.getStatOggBus().setStbIdOgg(stbIdOgg);
    }

    @Override
    public int getStbIdStatOggBus() {
        return ws.getStatOggBus().getStbIdStatOggBus();
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        ws.getStatOggBus().setStbIdStatOggBus(stbIdStatOggBus);
    }

    @Override
    public String getStbTpCaus() {
        return ws.getStatOggBus().getStbTpCaus();
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        ws.getStatOggBus().setStbTpCaus(stbTpCaus);
    }

    @Override
    public String getStbTpOgg() {
        return ws.getStatOggBus().getStbTpOgg();
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        ws.getStatOggBus().setStbTpOgg(stbTpOgg);
    }

    @Override
    public String getStbTpStatBus() {
        return ws.getStatOggBus().getStbTpStatBus();
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        ws.getStatOggBus().setStbTpStatBus(stbTpStatBus);
    }

    @Override
    public AfDecimal getTgaAbbAnnuUlt() {
        return ws.getTrchDiGar().getTgaAbbAnnuUlt().getTgaAbbAnnuUlt();
    }

    @Override
    public void setTgaAbbAnnuUlt(AfDecimal tgaAbbAnnuUlt) {
        ws.getTrchDiGar().getTgaAbbAnnuUlt().setTgaAbbAnnuUlt(tgaAbbAnnuUlt.copy());
    }

    @Override
    public AfDecimal getTgaAbbAnnuUltObj() {
        if (ws.getIndTrchDiGar().getAbbAnnuUlt() >= 0) {
            return getTgaAbbAnnuUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbAnnuUltObj(AfDecimal tgaAbbAnnuUltObj) {
        if (tgaAbbAnnuUltObj != null) {
            setTgaAbbAnnuUlt(new AfDecimal(tgaAbbAnnuUltObj, 15, 3));
            ws.getIndTrchDiGar().setAbbAnnuUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAbbAnnuUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAbbTotIni() {
        return ws.getTrchDiGar().getTgaAbbTotIni().getTgaAbbTotIni();
    }

    @Override
    public void setTgaAbbTotIni(AfDecimal tgaAbbTotIni) {
        ws.getTrchDiGar().getTgaAbbTotIni().setTgaAbbTotIni(tgaAbbTotIni.copy());
    }

    @Override
    public AfDecimal getTgaAbbTotIniObj() {
        if (ws.getIndTrchDiGar().getAbbTotIni() >= 0) {
            return getTgaAbbTotIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbTotIniObj(AfDecimal tgaAbbTotIniObj) {
        if (tgaAbbTotIniObj != null) {
            setTgaAbbTotIni(new AfDecimal(tgaAbbTotIniObj, 15, 3));
            ws.getIndTrchDiGar().setAbbTotIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAbbTotIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAbbTotUlt() {
        return ws.getTrchDiGar().getTgaAbbTotUlt().getTgaAbbTotUlt();
    }

    @Override
    public void setTgaAbbTotUlt(AfDecimal tgaAbbTotUlt) {
        ws.getTrchDiGar().getTgaAbbTotUlt().setTgaAbbTotUlt(tgaAbbTotUlt.copy());
    }

    @Override
    public AfDecimal getTgaAbbTotUltObj() {
        if (ws.getIndTrchDiGar().getAbbTotUlt() >= 0) {
            return getTgaAbbTotUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAbbTotUltObj(AfDecimal tgaAbbTotUltObj) {
        if (tgaAbbTotUltObj != null) {
            setTgaAbbTotUlt(new AfDecimal(tgaAbbTotUltObj, 15, 3));
            ws.getIndTrchDiGar().setAbbTotUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAbbTotUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAcqExp() {
        return ws.getTrchDiGar().getTgaAcqExp().getTgaAcqExp();
    }

    @Override
    public void setTgaAcqExp(AfDecimal tgaAcqExp) {
        ws.getTrchDiGar().getTgaAcqExp().setTgaAcqExp(tgaAcqExp.copy());
    }

    @Override
    public AfDecimal getTgaAcqExpObj() {
        if (ws.getIndTrchDiGar().getAcqExp() >= 0) {
            return getTgaAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAcqExpObj(AfDecimal tgaAcqExpObj) {
        if (tgaAcqExpObj != null) {
            setTgaAcqExp(new AfDecimal(tgaAcqExpObj, 15, 3));
            ws.getIndTrchDiGar().setAcqExp(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqCommisInter() {
        return ws.getTrchDiGar().getTgaAlqCommisInter().getTgaAlqCommisInter();
    }

    @Override
    public void setTgaAlqCommisInter(AfDecimal tgaAlqCommisInter) {
        ws.getTrchDiGar().getTgaAlqCommisInter().setTgaAlqCommisInter(tgaAlqCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaAlqCommisInterObj() {
        if (ws.getIndTrchDiGar().getAlqCommisInter() >= 0) {
            return getTgaAlqCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqCommisInterObj(AfDecimal tgaAlqCommisInterObj) {
        if (tgaAlqCommisInterObj != null) {
            setTgaAlqCommisInter(new AfDecimal(tgaAlqCommisInterObj, 6, 3));
            ws.getIndTrchDiGar().setAlqCommisInter(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvAcq() {
        return ws.getTrchDiGar().getTgaAlqProvAcq().getTgaAlqProvAcq();
    }

    @Override
    public void setTgaAlqProvAcq(AfDecimal tgaAlqProvAcq) {
        ws.getTrchDiGar().getTgaAlqProvAcq().setTgaAlqProvAcq(tgaAlqProvAcq.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvAcqObj() {
        if (ws.getIndTrchDiGar().getAlqProvAcq() >= 0) {
            return getTgaAlqProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvAcqObj(AfDecimal tgaAlqProvAcqObj) {
        if (tgaAlqProvAcqObj != null) {
            setTgaAlqProvAcq(new AfDecimal(tgaAlqProvAcqObj, 6, 3));
            ws.getIndTrchDiGar().setAlqProvAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvInc() {
        return ws.getTrchDiGar().getTgaAlqProvInc().getTgaAlqProvInc();
    }

    @Override
    public void setTgaAlqProvInc(AfDecimal tgaAlqProvInc) {
        ws.getTrchDiGar().getTgaAlqProvInc().setTgaAlqProvInc(tgaAlqProvInc.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvIncObj() {
        if (ws.getIndTrchDiGar().getAlqProvInc() >= 0) {
            return getTgaAlqProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvIncObj(AfDecimal tgaAlqProvIncObj) {
        if (tgaAlqProvIncObj != null) {
            setTgaAlqProvInc(new AfDecimal(tgaAlqProvIncObj, 6, 3));
            ws.getIndTrchDiGar().setAlqProvInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqProvRicor() {
        return ws.getTrchDiGar().getTgaAlqProvRicor().getTgaAlqProvRicor();
    }

    @Override
    public void setTgaAlqProvRicor(AfDecimal tgaAlqProvRicor) {
        ws.getTrchDiGar().getTgaAlqProvRicor().setTgaAlqProvRicor(tgaAlqProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaAlqProvRicorObj() {
        if (ws.getIndTrchDiGar().getAlqProvRicor() >= 0) {
            return getTgaAlqProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqProvRicorObj(AfDecimal tgaAlqProvRicorObj) {
        if (tgaAlqProvRicorObj != null) {
            setTgaAlqProvRicor(new AfDecimal(tgaAlqProvRicorObj, 6, 3));
            ws.getIndTrchDiGar().setAlqProvRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqRemunAss() {
        return ws.getTrchDiGar().getTgaAlqRemunAss().getTgaAlqRemunAss();
    }

    @Override
    public void setTgaAlqRemunAss(AfDecimal tgaAlqRemunAss) {
        ws.getTrchDiGar().getTgaAlqRemunAss().setTgaAlqRemunAss(tgaAlqRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaAlqRemunAssObj() {
        if (ws.getIndTrchDiGar().getAlqRemunAss() >= 0) {
            return getTgaAlqRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqRemunAssObj(AfDecimal tgaAlqRemunAssObj) {
        if (tgaAlqRemunAssObj != null) {
            setTgaAlqRemunAss(new AfDecimal(tgaAlqRemunAssObj, 6, 3));
            ws.getIndTrchDiGar().setAlqRemunAss(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaAlqScon() {
        return ws.getTrchDiGar().getTgaAlqScon().getTgaAlqScon();
    }

    @Override
    public void setTgaAlqScon(AfDecimal tgaAlqScon) {
        ws.getTrchDiGar().getTgaAlqScon().setTgaAlqScon(tgaAlqScon.copy());
    }

    @Override
    public AfDecimal getTgaAlqSconObj() {
        if (ws.getIndTrchDiGar().getAlqScon() >= 0) {
            return getTgaAlqScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaAlqSconObj(AfDecimal tgaAlqSconObj) {
        if (tgaAlqSconObj != null) {
            setTgaAlqScon(new AfDecimal(tgaAlqSconObj, 6, 3));
            ws.getIndTrchDiGar().setAlqScon(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setAlqScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqto() {
        return ws.getTrchDiGar().getTgaBnsGiaLiqto().getTgaBnsGiaLiqto();
    }

    @Override
    public void setTgaBnsGiaLiqto(AfDecimal tgaBnsGiaLiqto) {
        ws.getTrchDiGar().getTgaBnsGiaLiqto().setTgaBnsGiaLiqto(tgaBnsGiaLiqto.copy());
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqtoObj() {
        if (ws.getIndTrchDiGar().getBnsGiaLiqto() >= 0) {
            return getTgaBnsGiaLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaBnsGiaLiqtoObj(AfDecimal tgaBnsGiaLiqtoObj) {
        if (tgaBnsGiaLiqtoObj != null) {
            setTgaBnsGiaLiqto(new AfDecimal(tgaBnsGiaLiqtoObj, 15, 3));
            ws.getIndTrchDiGar().setBnsGiaLiqto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setBnsGiaLiqto(((short)-1));
        }
    }

    @Override
    public int getTgaCodCompAnia() {
        return ws.getTrchDiGar().getTgaCodCompAnia();
    }

    @Override
    public void setTgaCodCompAnia(int tgaCodCompAnia) {
        ws.getTrchDiGar().setTgaCodCompAnia(tgaCodCompAnia);
    }

    @Override
    public String getTgaCodDvs() {
        return ws.getTrchDiGar().getTgaCodDvs();
    }

    @Override
    public void setTgaCodDvs(String tgaCodDvs) {
        ws.getTrchDiGar().setTgaCodDvs(tgaCodDvs);
    }

    @Override
    public AfDecimal getTgaCommisGest() {
        return ws.getTrchDiGar().getTgaCommisGest().getTgaCommisGest();
    }

    @Override
    public void setTgaCommisGest(AfDecimal tgaCommisGest) {
        ws.getTrchDiGar().getTgaCommisGest().setTgaCommisGest(tgaCommisGest.copy());
    }

    @Override
    public AfDecimal getTgaCommisGestObj() {
        if (ws.getIndTrchDiGar().getCommisGest() >= 0) {
            return getTgaCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCommisGestObj(AfDecimal tgaCommisGestObj) {
        if (tgaCommisGestObj != null) {
            setTgaCommisGest(new AfDecimal(tgaCommisGestObj, 15, 3));
            ws.getIndTrchDiGar().setCommisGest(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCommisInter() {
        return ws.getTrchDiGar().getTgaCommisInter().getTgaCommisInter();
    }

    @Override
    public void setTgaCommisInter(AfDecimal tgaCommisInter) {
        ws.getTrchDiGar().getTgaCommisInter().setTgaCommisInter(tgaCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaCommisInterObj() {
        if (ws.getIndTrchDiGar().getCommisInter() >= 0) {
            return getTgaCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCommisInterObj(AfDecimal tgaCommisInterObj) {
        if (tgaCommisInterObj != null) {
            setTgaCommisInter(new AfDecimal(tgaCommisInterObj, 15, 3));
            ws.getIndTrchDiGar().setCommisInter(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCosRunAssva() {
        return ws.getTrchDiGar().getTgaCosRunAssva().getTgaCosRunAssva();
    }

    @Override
    public void setTgaCosRunAssva(AfDecimal tgaCosRunAssva) {
        ws.getTrchDiGar().getTgaCosRunAssva().setTgaCosRunAssva(tgaCosRunAssva.copy());
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdc() {
        return ws.getTrchDiGar().getTgaCosRunAssvaIdc().getTgaCosRunAssvaIdc();
    }

    @Override
    public void setTgaCosRunAssvaIdc(AfDecimal tgaCosRunAssvaIdc) {
        ws.getTrchDiGar().getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdc(tgaCosRunAssvaIdc.copy());
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdcObj() {
        if (ws.getIndTrchDiGar().getCosRunAssvaIdc() >= 0) {
            return getTgaCosRunAssvaIdc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCosRunAssvaIdcObj(AfDecimal tgaCosRunAssvaIdcObj) {
        if (tgaCosRunAssvaIdcObj != null) {
            setTgaCosRunAssvaIdc(new AfDecimal(tgaCosRunAssvaIdcObj, 15, 3));
            ws.getIndTrchDiGar().setCosRunAssvaIdc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCosRunAssvaIdc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCosRunAssvaObj() {
        if (ws.getIndTrchDiGar().getCosRunAssva() >= 0) {
            return getTgaCosRunAssva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCosRunAssvaObj(AfDecimal tgaCosRunAssvaObj) {
        if (tgaCosRunAssvaObj != null) {
            setTgaCosRunAssva(new AfDecimal(tgaCosRunAssvaObj, 15, 3));
            ws.getIndTrchDiGar().setCosRunAssva(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCosRunAssva(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptInOpzRivto() {
        return ws.getTrchDiGar().getTgaCptInOpzRivto().getTgaCptInOpzRivto();
    }

    @Override
    public void setTgaCptInOpzRivto(AfDecimal tgaCptInOpzRivto) {
        ws.getTrchDiGar().getTgaCptInOpzRivto().setTgaCptInOpzRivto(tgaCptInOpzRivto.copy());
    }

    @Override
    public AfDecimal getTgaCptInOpzRivtoObj() {
        if (ws.getIndTrchDiGar().getCptInOpzRivto() >= 0) {
            return getTgaCptInOpzRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptInOpzRivtoObj(AfDecimal tgaCptInOpzRivtoObj) {
        if (tgaCptInOpzRivtoObj != null) {
            setTgaCptInOpzRivto(new AfDecimal(tgaCptInOpzRivtoObj, 15, 3));
            ws.getIndTrchDiGar().setCptInOpzRivto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCptInOpzRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptMinScad() {
        return ws.getTrchDiGar().getTgaCptMinScad().getTgaCptMinScad();
    }

    @Override
    public void setTgaCptMinScad(AfDecimal tgaCptMinScad) {
        ws.getTrchDiGar().getTgaCptMinScad().setTgaCptMinScad(tgaCptMinScad.copy());
    }

    @Override
    public AfDecimal getTgaCptMinScadObj() {
        if (ws.getIndTrchDiGar().getCptMinScad() >= 0) {
            return getTgaCptMinScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptMinScadObj(AfDecimal tgaCptMinScadObj) {
        if (tgaCptMinScadObj != null) {
            setTgaCptMinScad(new AfDecimal(tgaCptMinScadObj, 15, 3));
            ws.getIndTrchDiGar().setCptMinScad(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCptMinScad(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaCptRshMor() {
        return ws.getTrchDiGar().getTgaCptRshMor().getTgaCptRshMor();
    }

    @Override
    public void setTgaCptRshMor(AfDecimal tgaCptRshMor) {
        ws.getTrchDiGar().getTgaCptRshMor().setTgaCptRshMor(tgaCptRshMor.copy());
    }

    @Override
    public AfDecimal getTgaCptRshMorObj() {
        if (ws.getIndTrchDiGar().getCptRshMor() >= 0) {
            return getTgaCptRshMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaCptRshMorObj(AfDecimal tgaCptRshMorObj) {
        if (tgaCptRshMorObj != null) {
            setTgaCptRshMor(new AfDecimal(tgaCptRshMorObj, 15, 3));
            ws.getIndTrchDiGar().setCptRshMor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setCptRshMor(((short)-1));
        }
    }

    @Override
    public char getTgaDsOperSql() {
        return ws.getTrchDiGar().getTgaDsOperSql();
    }

    @Override
    public void setTgaDsOperSql(char tgaDsOperSql) {
        ws.getTrchDiGar().setTgaDsOperSql(tgaDsOperSql);
    }

    @Override
    public long getTgaDsRiga() {
        return ws.getTrchDiGar().getTgaDsRiga();
    }

    @Override
    public void setTgaDsRiga(long tgaDsRiga) {
        ws.getTrchDiGar().setTgaDsRiga(tgaDsRiga);
    }

    @Override
    public char getTgaDsStatoElab() {
        return ws.getTrchDiGar().getTgaDsStatoElab();
    }

    @Override
    public void setTgaDsStatoElab(char tgaDsStatoElab) {
        ws.getTrchDiGar().setTgaDsStatoElab(tgaDsStatoElab);
    }

    @Override
    public long getTgaDsTsEndCptz() {
        return ws.getTrchDiGar().getTgaDsTsEndCptz();
    }

    @Override
    public void setTgaDsTsEndCptz(long tgaDsTsEndCptz) {
        ws.getTrchDiGar().setTgaDsTsEndCptz(tgaDsTsEndCptz);
    }

    @Override
    public long getTgaDsTsIniCptz() {
        return ws.getTrchDiGar().getTgaDsTsIniCptz();
    }

    @Override
    public void setTgaDsTsIniCptz(long tgaDsTsIniCptz) {
        ws.getTrchDiGar().setTgaDsTsIniCptz(tgaDsTsIniCptz);
    }

    @Override
    public String getTgaDsUtente() {
        return ws.getTrchDiGar().getTgaDsUtente();
    }

    @Override
    public void setTgaDsUtente(String tgaDsUtente) {
        ws.getTrchDiGar().setTgaDsUtente(tgaDsUtente);
    }

    @Override
    public int getTgaDsVer() {
        return ws.getTrchDiGar().getTgaDsVer();
    }

    @Override
    public void setTgaDsVer(int tgaDsVer) {
        ws.getTrchDiGar().setTgaDsVer(tgaDsVer);
    }

    @Override
    public String getTgaDtDecorDb() {
        return ws.getTrchDiGarDb().getDecorDb();
    }

    @Override
    public void setTgaDtDecorDb(String tgaDtDecorDb) {
        ws.getTrchDiGarDb().setDecorDb(tgaDtDecorDb);
    }

    @Override
    public String getTgaDtEffStabDb() {
        return ws.getTrchDiGarDb().getEffStabDb();
    }

    @Override
    public void setTgaDtEffStabDb(String tgaDtEffStabDb) {
        ws.getTrchDiGarDb().setEffStabDb(tgaDtEffStabDb);
    }

    @Override
    public String getTgaDtEffStabDbObj() {
        if (ws.getIndTrchDiGar().getDtEffStab() >= 0) {
            return getTgaDtEffStabDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtEffStabDbObj(String tgaDtEffStabDbObj) {
        if (tgaDtEffStabDbObj != null) {
            setTgaDtEffStabDb(tgaDtEffStabDbObj);
            ws.getIndTrchDiGar().setDtEffStab(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtEffStab(((short)-1));
        }
    }

    @Override
    public String getTgaDtEmisDb() {
        return ws.getTrchDiGarDb().getEmisDb();
    }

    @Override
    public void setTgaDtEmisDb(String tgaDtEmisDb) {
        ws.getTrchDiGarDb().setEmisDb(tgaDtEmisDb);
    }

    @Override
    public String getTgaDtEmisDbObj() {
        if (ws.getIndTrchDiGar().getDtEmis() >= 0) {
            return getTgaDtEmisDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtEmisDbObj(String tgaDtEmisDbObj) {
        if (tgaDtEmisDbObj != null) {
            setTgaDtEmisDb(tgaDtEmisDbObj);
            ws.getIndTrchDiGar().setDtEmis(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtEmis(((short)-1));
        }
    }

    @Override
    public String getTgaDtEndEffDb() {
        return ws.getTrchDiGarDb().getEndEffDb();
    }

    @Override
    public void setTgaDtEndEffDb(String tgaDtEndEffDb) {
        ws.getTrchDiGarDb().setEndEffDb(tgaDtEndEffDb);
    }

    @Override
    public String getTgaDtIniEffDb() {
        return ws.getTrchDiGarDb().getIniEffDb();
    }

    @Override
    public void setTgaDtIniEffDb(String tgaDtIniEffDb) {
        ws.getTrchDiGarDb().setIniEffDb(tgaDtIniEffDb);
    }

    @Override
    public String getTgaDtIniValTarDb() {
        return ws.getTrchDiGarDb().getIniValTarDb();
    }

    @Override
    public void setTgaDtIniValTarDb(String tgaDtIniValTarDb) {
        ws.getTrchDiGarDb().setIniValTarDb(tgaDtIniValTarDb);
    }

    @Override
    public String getTgaDtIniValTarDbObj() {
        if (ws.getIndTrchDiGar().getDtIniValTar() >= 0) {
            return getTgaDtIniValTarDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtIniValTarDbObj(String tgaDtIniValTarDbObj) {
        if (tgaDtIniValTarDbObj != null) {
            setTgaDtIniValTarDb(tgaDtIniValTarDbObj);
            ws.getIndTrchDiGar().setDtIniValTar(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtIniValTar(((short)-1));
        }
    }

    @Override
    public String getTgaDtScadDb() {
        return ws.getTrchDiGarDb().getScadDb();
    }

    @Override
    public void setTgaDtScadDb(String tgaDtScadDb) {
        ws.getTrchDiGarDb().setScadDb(tgaDtScadDb);
    }

    @Override
    public String getTgaDtScadDbObj() {
        if (ws.getIndTrchDiGar().getDtScad() >= 0) {
            return getTgaDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtScadDbObj(String tgaDtScadDbObj) {
        if (tgaDtScadDbObj != null) {
            setTgaDtScadDb(tgaDtScadDbObj);
            ws.getIndTrchDiGar().setDtScad(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtScad(((short)-1));
        }
    }

    @Override
    public String getTgaDtUltAdegPrePrDb() {
        return ws.getTrchDiGarDb().getUltAdegPrePrDb();
    }

    @Override
    public void setTgaDtUltAdegPrePrDb(String tgaDtUltAdegPrePrDb) {
        ws.getTrchDiGarDb().setUltAdegPrePrDb(tgaDtUltAdegPrePrDb);
    }

    @Override
    public String getTgaDtUltAdegPrePrDbObj() {
        if (ws.getIndTrchDiGar().getDtUltAdegPrePr() >= 0) {
            return getTgaDtUltAdegPrePrDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtUltAdegPrePrDbObj(String tgaDtUltAdegPrePrDbObj) {
        if (tgaDtUltAdegPrePrDbObj != null) {
            setTgaDtUltAdegPrePrDb(tgaDtUltAdegPrePrDbObj);
            ws.getIndTrchDiGar().setDtUltAdegPrePr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtUltAdegPrePr(((short)-1));
        }
    }

    @Override
    public String getTgaDtVldtProdDb() {
        return ws.getTrchDiGarDb().getVldtProdDb();
    }

    @Override
    public void setTgaDtVldtProdDb(String tgaDtVldtProdDb) {
        ws.getTrchDiGarDb().setVldtProdDb(tgaDtVldtProdDb);
    }

    @Override
    public String getTgaDtVldtProdDbObj() {
        if (ws.getIndTrchDiGar().getDtVldtProd() >= 0) {
            return getTgaDtVldtProdDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDtVldtProdDbObj(String tgaDtVldtProdDbObj) {
        if (tgaDtVldtProdDbObj != null) {
            setTgaDtVldtProdDb(tgaDtVldtProdDbObj);
            ws.getIndTrchDiGar().setDtVldtProd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDtVldtProd(((short)-1));
        }
    }

    @Override
    public int getTgaDurAa() {
        return ws.getTrchDiGar().getTgaDurAa().getTgaDurAa();
    }

    @Override
    public void setTgaDurAa(int tgaDurAa) {
        ws.getTrchDiGar().getTgaDurAa().setTgaDurAa(tgaDurAa);
    }

    @Override
    public Integer getTgaDurAaObj() {
        if (ws.getIndTrchDiGar().getDurAa() >= 0) {
            return ((Integer)getTgaDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurAaObj(Integer tgaDurAaObj) {
        if (tgaDurAaObj != null) {
            setTgaDurAa(((int)tgaDurAaObj));
            ws.getIndTrchDiGar().setDurAa(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurAa(((short)-1));
        }
    }

    @Override
    public int getTgaDurAbb() {
        return ws.getTrchDiGar().getTgaDurAbb().getTgaDurAbb();
    }

    @Override
    public void setTgaDurAbb(int tgaDurAbb) {
        ws.getTrchDiGar().getTgaDurAbb().setTgaDurAbb(tgaDurAbb);
    }

    @Override
    public Integer getTgaDurAbbObj() {
        if (ws.getIndTrchDiGar().getDurAbb() >= 0) {
            return ((Integer)getTgaDurAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurAbbObj(Integer tgaDurAbbObj) {
        if (tgaDurAbbObj != null) {
            setTgaDurAbb(((int)tgaDurAbbObj));
            ws.getIndTrchDiGar().setDurAbb(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurAbb(((short)-1));
        }
    }

    @Override
    public int getTgaDurGg() {
        return ws.getTrchDiGar().getTgaDurGg().getTgaDurGg();
    }

    @Override
    public void setTgaDurGg(int tgaDurGg) {
        ws.getTrchDiGar().getTgaDurGg().setTgaDurGg(tgaDurGg);
    }

    @Override
    public Integer getTgaDurGgObj() {
        if (ws.getIndTrchDiGar().getDurGg() >= 0) {
            return ((Integer)getTgaDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurGgObj(Integer tgaDurGgObj) {
        if (tgaDurGgObj != null) {
            setTgaDurGg(((int)tgaDurGgObj));
            ws.getIndTrchDiGar().setDurGg(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurGg(((short)-1));
        }
    }

    @Override
    public int getTgaDurMm() {
        return ws.getTrchDiGar().getTgaDurMm().getTgaDurMm();
    }

    @Override
    public void setTgaDurMm(int tgaDurMm) {
        ws.getTrchDiGar().getTgaDurMm().setTgaDurMm(tgaDurMm);
    }

    @Override
    public Integer getTgaDurMmObj() {
        if (ws.getIndTrchDiGar().getDurMm() >= 0) {
            return ((Integer)getTgaDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaDurMmObj(Integer tgaDurMmObj) {
        if (tgaDurMmObj != null) {
            setTgaDurMm(((int)tgaDurMmObj));
            ws.getIndTrchDiGar().setDurMm(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setDurMm(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa1oAssto() {
        return ws.getTrchDiGar().getTgaEtaAa1oAssto().getTgaEtaAa1oAssto();
    }

    @Override
    public void setTgaEtaAa1oAssto(short tgaEtaAa1oAssto) {
        ws.getTrchDiGar().getTgaEtaAa1oAssto().setTgaEtaAa1oAssto(tgaEtaAa1oAssto);
    }

    @Override
    public Short getTgaEtaAa1oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaAa1oAssto() >= 0) {
            return ((Short)getTgaEtaAa1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa1oAsstoObj(Short tgaEtaAa1oAsstoObj) {
        if (tgaEtaAa1oAsstoObj != null) {
            setTgaEtaAa1oAssto(((short)tgaEtaAa1oAsstoObj));
            ws.getIndTrchDiGar().setEtaAa1oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaAa1oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa2oAssto() {
        return ws.getTrchDiGar().getTgaEtaAa2oAssto().getTgaEtaAa2oAssto();
    }

    @Override
    public void setTgaEtaAa2oAssto(short tgaEtaAa2oAssto) {
        ws.getTrchDiGar().getTgaEtaAa2oAssto().setTgaEtaAa2oAssto(tgaEtaAa2oAssto);
    }

    @Override
    public Short getTgaEtaAa2oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaAa2oAssto() >= 0) {
            return ((Short)getTgaEtaAa2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa2oAsstoObj(Short tgaEtaAa2oAsstoObj) {
        if (tgaEtaAa2oAsstoObj != null) {
            setTgaEtaAa2oAssto(((short)tgaEtaAa2oAsstoObj));
            ws.getIndTrchDiGar().setEtaAa2oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaAa2oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaAa3oAssto() {
        return ws.getTrchDiGar().getTgaEtaAa3oAssto().getTgaEtaAa3oAssto();
    }

    @Override
    public void setTgaEtaAa3oAssto(short tgaEtaAa3oAssto) {
        ws.getTrchDiGar().getTgaEtaAa3oAssto().setTgaEtaAa3oAssto(tgaEtaAa3oAssto);
    }

    @Override
    public Short getTgaEtaAa3oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaAa3oAssto() >= 0) {
            return ((Short)getTgaEtaAa3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaAa3oAsstoObj(Short tgaEtaAa3oAsstoObj) {
        if (tgaEtaAa3oAsstoObj != null) {
            setTgaEtaAa3oAssto(((short)tgaEtaAa3oAsstoObj));
            ws.getIndTrchDiGar().setEtaAa3oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaAa3oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm1oAssto() {
        return ws.getTrchDiGar().getTgaEtaMm1oAssto().getTgaEtaMm1oAssto();
    }

    @Override
    public void setTgaEtaMm1oAssto(short tgaEtaMm1oAssto) {
        ws.getTrchDiGar().getTgaEtaMm1oAssto().setTgaEtaMm1oAssto(tgaEtaMm1oAssto);
    }

    @Override
    public Short getTgaEtaMm1oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaMm1oAssto() >= 0) {
            return ((Short)getTgaEtaMm1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm1oAsstoObj(Short tgaEtaMm1oAsstoObj) {
        if (tgaEtaMm1oAsstoObj != null) {
            setTgaEtaMm1oAssto(((short)tgaEtaMm1oAsstoObj));
            ws.getIndTrchDiGar().setEtaMm1oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaMm1oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm2oAssto() {
        return ws.getTrchDiGar().getTgaEtaMm2oAssto().getTgaEtaMm2oAssto();
    }

    @Override
    public void setTgaEtaMm2oAssto(short tgaEtaMm2oAssto) {
        ws.getTrchDiGar().getTgaEtaMm2oAssto().setTgaEtaMm2oAssto(tgaEtaMm2oAssto);
    }

    @Override
    public Short getTgaEtaMm2oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaMm2oAssto() >= 0) {
            return ((Short)getTgaEtaMm2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm2oAsstoObj(Short tgaEtaMm2oAsstoObj) {
        if (tgaEtaMm2oAsstoObj != null) {
            setTgaEtaMm2oAssto(((short)tgaEtaMm2oAsstoObj));
            ws.getIndTrchDiGar().setEtaMm2oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaMm2oAssto(((short)-1));
        }
    }

    @Override
    public short getTgaEtaMm3oAssto() {
        return ws.getTrchDiGar().getTgaEtaMm3oAssto().getTgaEtaMm3oAssto();
    }

    @Override
    public void setTgaEtaMm3oAssto(short tgaEtaMm3oAssto) {
        ws.getTrchDiGar().getTgaEtaMm3oAssto().setTgaEtaMm3oAssto(tgaEtaMm3oAssto);
    }

    @Override
    public Short getTgaEtaMm3oAsstoObj() {
        if (ws.getIndTrchDiGar().getEtaMm3oAssto() >= 0) {
            return ((Short)getTgaEtaMm3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaEtaMm3oAsstoObj(Short tgaEtaMm3oAsstoObj) {
        if (tgaEtaMm3oAsstoObj != null) {
            setTgaEtaMm3oAssto(((short)tgaEtaMm3oAsstoObj));
            ws.getIndTrchDiGar().setEtaMm3oAssto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setEtaMm3oAssto(((short)-1));
        }
    }

    @Override
    public char getTgaFlCarCont() {
        return ws.getTrchDiGar().getTgaFlCarCont();
    }

    @Override
    public void setTgaFlCarCont(char tgaFlCarCont) {
        ws.getTrchDiGar().setTgaFlCarCont(tgaFlCarCont);
    }

    @Override
    public Character getTgaFlCarContObj() {
        if (ws.getIndTrchDiGar().getFlCarCont() >= 0) {
            return ((Character)getTgaFlCarCont());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlCarContObj(Character tgaFlCarContObj) {
        if (tgaFlCarContObj != null) {
            setTgaFlCarCont(((char)tgaFlCarContObj));
            ws.getIndTrchDiGar().setFlCarCont(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setFlCarCont(((short)-1));
        }
    }

    @Override
    public char getTgaFlImportiForz() {
        return ws.getTrchDiGar().getTgaFlImportiForz();
    }

    @Override
    public void setTgaFlImportiForz(char tgaFlImportiForz) {
        ws.getTrchDiGar().setTgaFlImportiForz(tgaFlImportiForz);
    }

    @Override
    public Character getTgaFlImportiForzObj() {
        if (ws.getIndTrchDiGar().getFlImportiForz() >= 0) {
            return ((Character)getTgaFlImportiForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlImportiForzObj(Character tgaFlImportiForzObj) {
        if (tgaFlImportiForzObj != null) {
            setTgaFlImportiForz(((char)tgaFlImportiForzObj));
            ws.getIndTrchDiGar().setFlImportiForz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setFlImportiForz(((short)-1));
        }
    }

    @Override
    public char getTgaFlProvForz() {
        return ws.getTrchDiGar().getTgaFlProvForz();
    }

    @Override
    public void setTgaFlProvForz(char tgaFlProvForz) {
        ws.getTrchDiGar().setTgaFlProvForz(tgaFlProvForz);
    }

    @Override
    public Character getTgaFlProvForzObj() {
        if (ws.getIndTrchDiGar().getFlProvForz() >= 0) {
            return ((Character)getTgaFlProvForz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaFlProvForzObj(Character tgaFlProvForzObj) {
        if (tgaFlProvForzObj != null) {
            setTgaFlProvForz(((char)tgaFlProvForzObj));
            ws.getIndTrchDiGar().setFlProvForz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setFlProvForz(((short)-1));
        }
    }

    @Override
    public String getTgaIbOgg() {
        return ws.getTrchDiGar().getTgaIbOgg();
    }

    @Override
    public void setTgaIbOgg(String tgaIbOgg) {
        ws.getTrchDiGar().setTgaIbOgg(tgaIbOgg);
    }

    @Override
    public String getTgaIbOggObj() {
        if (ws.getIndTrchDiGar().getIbOgg() >= 0) {
            return getTgaIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIbOggObj(String tgaIbOggObj) {
        if (tgaIbOggObj != null) {
            setTgaIbOgg(tgaIbOggObj);
            ws.getIndTrchDiGar().setIbOgg(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getTgaIdAdes() {
        return ws.getTrchDiGar().getTgaIdAdes();
    }

    @Override
    public void setTgaIdAdes(int tgaIdAdes) {
        ws.getTrchDiGar().setTgaIdAdes(tgaIdAdes);
    }

    @Override
    public int getTgaIdGar() {
        return ws.getTrchDiGar().getTgaIdGar();
    }

    @Override
    public void setTgaIdGar(int tgaIdGar) {
        ws.getTrchDiGar().setTgaIdGar(tgaIdGar);
    }

    @Override
    public int getTgaIdMoviChiu() {
        return ws.getTrchDiGar().getTgaIdMoviChiu().getTgaIdMoviChiu();
    }

    @Override
    public void setTgaIdMoviChiu(int tgaIdMoviChiu) {
        ws.getTrchDiGar().getTgaIdMoviChiu().setTgaIdMoviChiu(tgaIdMoviChiu);
    }

    @Override
    public Integer getTgaIdMoviChiuObj() {
        if (ws.getIndTrchDiGar().getIdMoviChiu() >= 0) {
            return ((Integer)getTgaIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIdMoviChiuObj(Integer tgaIdMoviChiuObj) {
        if (tgaIdMoviChiuObj != null) {
            setTgaIdMoviChiu(((int)tgaIdMoviChiuObj));
            ws.getIndTrchDiGar().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getTgaIdMoviCrz() {
        return ws.getTrchDiGar().getTgaIdMoviCrz();
    }

    @Override
    public void setTgaIdMoviCrz(int tgaIdMoviCrz) {
        ws.getTrchDiGar().setTgaIdMoviCrz(tgaIdMoviCrz);
    }

    @Override
    public int getTgaIdPoli() {
        return ws.getTrchDiGar().getTgaIdPoli();
    }

    @Override
    public void setTgaIdPoli(int tgaIdPoli) {
        ws.getTrchDiGar().setTgaIdPoli(tgaIdPoli);
    }

    @Override
    public int getTgaIdTrchDiGar() {
        return ws.getTrchDiGar().getTgaIdTrchDiGar();
    }

    @Override
    public void setTgaIdTrchDiGar(int tgaIdTrchDiGar) {
        ws.getTrchDiGar().setTgaIdTrchDiGar(tgaIdTrchDiGar);
    }

    @Override
    public AfDecimal getTgaImpAder() {
        return ws.getTrchDiGar().getTgaImpAder().getTgaImpAder();
    }

    @Override
    public void setTgaImpAder(AfDecimal tgaImpAder) {
        ws.getTrchDiGar().getTgaImpAder().setTgaImpAder(tgaImpAder.copy());
    }

    @Override
    public AfDecimal getTgaImpAderObj() {
        if (ws.getIndTrchDiGar().getImpAder() >= 0) {
            return getTgaImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAderObj(AfDecimal tgaImpAderObj) {
        if (tgaImpAderObj != null) {
            setTgaImpAder(new AfDecimal(tgaImpAderObj, 15, 3));
            ws.getIndTrchDiGar().setImpAder(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpAltSopr() {
        return ws.getTrchDiGar().getTgaImpAltSopr().getTgaImpAltSopr();
    }

    @Override
    public void setTgaImpAltSopr(AfDecimal tgaImpAltSopr) {
        ws.getTrchDiGar().getTgaImpAltSopr().setTgaImpAltSopr(tgaImpAltSopr.copy());
    }

    @Override
    public AfDecimal getTgaImpAltSoprObj() {
        if (ws.getIndTrchDiGar().getImpAltSopr() >= 0) {
            return getTgaImpAltSopr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAltSoprObj(AfDecimal tgaImpAltSoprObj) {
        if (tgaImpAltSoprObj != null) {
            setTgaImpAltSopr(new AfDecimal(tgaImpAltSoprObj, 15, 3));
            ws.getIndTrchDiGar().setImpAltSopr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpAltSopr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpAz() {
        return ws.getTrchDiGar().getTgaImpAz().getTgaImpAz();
    }

    @Override
    public void setTgaImpAz(AfDecimal tgaImpAz) {
        ws.getTrchDiGar().getTgaImpAz().setTgaImpAz(tgaImpAz.copy());
    }

    @Override
    public AfDecimal getTgaImpAzObj() {
        if (ws.getIndTrchDiGar().getImpAz() >= 0) {
            return getTgaImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpAzObj(AfDecimal tgaImpAzObj) {
        if (tgaImpAzObj != null) {
            setTgaImpAz(new AfDecimal(tgaImpAzObj, 15, 3));
            ws.getIndTrchDiGar().setImpAz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpBns() {
        return ws.getTrchDiGar().getTgaImpBns().getTgaImpBns();
    }

    @Override
    public AfDecimal getTgaImpBnsAntic() {
        return ws.getTrchDiGar().getTgaImpBnsAntic().getTgaImpBnsAntic();
    }

    @Override
    public void setTgaImpBnsAntic(AfDecimal tgaImpBnsAntic) {
        ws.getTrchDiGar().getTgaImpBnsAntic().setTgaImpBnsAntic(tgaImpBnsAntic.copy());
    }

    @Override
    public AfDecimal getTgaImpBnsAnticObj() {
        if (ws.getIndTrchDiGar().getImpBnsAntic() >= 0) {
            return getTgaImpBnsAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpBnsAnticObj(AfDecimal tgaImpBnsAnticObj) {
        if (tgaImpBnsAnticObj != null) {
            setTgaImpBnsAntic(new AfDecimal(tgaImpBnsAnticObj, 15, 3));
            ws.getIndTrchDiGar().setImpBnsAntic(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpBnsAntic(((short)-1));
        }
    }

    @Override
    public void setTgaImpBns(AfDecimal tgaImpBns) {
        ws.getTrchDiGar().getTgaImpBns().setTgaImpBns(tgaImpBns.copy());
    }

    @Override
    public AfDecimal getTgaImpBnsObj() {
        if (ws.getIndTrchDiGar().getImpBns() >= 0) {
            return getTgaImpBns();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpBnsObj(AfDecimal tgaImpBnsObj) {
        if (tgaImpBnsObj != null) {
            setTgaImpBns(new AfDecimal(tgaImpBnsObj, 15, 3));
            ws.getIndTrchDiGar().setImpBns(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpBns(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarAcq() {
        return ws.getTrchDiGar().getTgaImpCarAcq().getTgaImpCarAcq();
    }

    @Override
    public void setTgaImpCarAcq(AfDecimal tgaImpCarAcq) {
        ws.getTrchDiGar().getTgaImpCarAcq().setTgaImpCarAcq(tgaImpCarAcq.copy());
    }

    @Override
    public AfDecimal getTgaImpCarAcqObj() {
        if (ws.getIndTrchDiGar().getImpCarAcq() >= 0) {
            return getTgaImpCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarAcqObj(AfDecimal tgaImpCarAcqObj) {
        if (tgaImpCarAcqObj != null) {
            setTgaImpCarAcq(new AfDecimal(tgaImpCarAcqObj, 15, 3));
            ws.getIndTrchDiGar().setImpCarAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarGest() {
        return ws.getTrchDiGar().getTgaImpCarGest().getTgaImpCarGest();
    }

    @Override
    public void setTgaImpCarGest(AfDecimal tgaImpCarGest) {
        ws.getTrchDiGar().getTgaImpCarGest().setTgaImpCarGest(tgaImpCarGest.copy());
    }

    @Override
    public AfDecimal getTgaImpCarGestObj() {
        if (ws.getIndTrchDiGar().getImpCarGest() >= 0) {
            return getTgaImpCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarGestObj(AfDecimal tgaImpCarGestObj) {
        if (tgaImpCarGestObj != null) {
            setTgaImpCarGest(new AfDecimal(tgaImpCarGestObj, 15, 3));
            ws.getIndTrchDiGar().setImpCarGest(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpCarInc() {
        return ws.getTrchDiGar().getTgaImpCarInc().getTgaImpCarInc();
    }

    @Override
    public void setTgaImpCarInc(AfDecimal tgaImpCarInc) {
        ws.getTrchDiGar().getTgaImpCarInc().setTgaImpCarInc(tgaImpCarInc.copy());
    }

    @Override
    public AfDecimal getTgaImpCarIncObj() {
        if (ws.getIndTrchDiGar().getImpCarInc() >= 0) {
            return getTgaImpCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpCarIncObj(AfDecimal tgaImpCarIncObj) {
        if (tgaImpCarIncObj != null) {
            setTgaImpCarInc(new AfDecimal(tgaImpCarIncObj, 15, 3));
            ws.getIndTrchDiGar().setImpCarInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpScon() {
        return ws.getTrchDiGar().getTgaImpScon().getTgaImpScon();
    }

    @Override
    public void setTgaImpScon(AfDecimal tgaImpScon) {
        ws.getTrchDiGar().getTgaImpScon().setTgaImpScon(tgaImpScon.copy());
    }

    @Override
    public AfDecimal getTgaImpSconObj() {
        if (ws.getIndTrchDiGar().getImpScon() >= 0) {
            return getTgaImpScon();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSconObj(AfDecimal tgaImpSconObj) {
        if (tgaImpSconObj != null) {
            setTgaImpScon(new AfDecimal(tgaImpSconObj, 15, 3));
            ws.getIndTrchDiGar().setImpScon(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpScon(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprProf() {
        return ws.getTrchDiGar().getTgaImpSoprProf().getTgaImpSoprProf();
    }

    @Override
    public void setTgaImpSoprProf(AfDecimal tgaImpSoprProf) {
        ws.getTrchDiGar().getTgaImpSoprProf().setTgaImpSoprProf(tgaImpSoprProf.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprProfObj() {
        if (ws.getIndTrchDiGar().getImpSoprProf() >= 0) {
            return getTgaImpSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprProfObj(AfDecimal tgaImpSoprProfObj) {
        if (tgaImpSoprProfObj != null) {
            setTgaImpSoprProf(new AfDecimal(tgaImpSoprProfObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprProf(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprSan() {
        return ws.getTrchDiGar().getTgaImpSoprSan().getTgaImpSoprSan();
    }

    @Override
    public void setTgaImpSoprSan(AfDecimal tgaImpSoprSan) {
        ws.getTrchDiGar().getTgaImpSoprSan().setTgaImpSoprSan(tgaImpSoprSan.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprSanObj() {
        if (ws.getIndTrchDiGar().getImpSoprSan() >= 0) {
            return getTgaImpSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprSanObj(AfDecimal tgaImpSoprSanObj) {
        if (tgaImpSoprSanObj != null) {
            setTgaImpSoprSan(new AfDecimal(tgaImpSoprSanObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprSan(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprSpo() {
        return ws.getTrchDiGar().getTgaImpSoprSpo().getTgaImpSoprSpo();
    }

    @Override
    public void setTgaImpSoprSpo(AfDecimal tgaImpSoprSpo) {
        ws.getTrchDiGar().getTgaImpSoprSpo().setTgaImpSoprSpo(tgaImpSoprSpo.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprSpoObj() {
        if (ws.getIndTrchDiGar().getImpSoprSpo() >= 0) {
            return getTgaImpSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprSpoObj(AfDecimal tgaImpSoprSpoObj) {
        if (tgaImpSoprSpoObj != null) {
            setTgaImpSoprSpo(new AfDecimal(tgaImpSoprSpoObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprSpo(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpSoprTec() {
        return ws.getTrchDiGar().getTgaImpSoprTec().getTgaImpSoprTec();
    }

    @Override
    public void setTgaImpSoprTec(AfDecimal tgaImpSoprTec) {
        ws.getTrchDiGar().getTgaImpSoprTec().setTgaImpSoprTec(tgaImpSoprTec.copy());
    }

    @Override
    public AfDecimal getTgaImpSoprTecObj() {
        if (ws.getIndTrchDiGar().getImpSoprTec() >= 0) {
            return getTgaImpSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpSoprTecObj(AfDecimal tgaImpSoprTecObj) {
        if (tgaImpSoprTecObj != null) {
            setTgaImpSoprTec(new AfDecimal(tgaImpSoprTecObj, 15, 3));
            ws.getIndTrchDiGar().setImpSoprTec(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTfr() {
        return ws.getTrchDiGar().getTgaImpTfr().getTgaImpTfr();
    }

    @Override
    public void setTgaImpTfr(AfDecimal tgaImpTfr) {
        ws.getTrchDiGar().getTgaImpTfr().setTgaImpTfr(tgaImpTfr.copy());
    }

    @Override
    public AfDecimal getTgaImpTfrObj() {
        if (ws.getIndTrchDiGar().getImpTfr() >= 0) {
            return getTgaImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTfrObj(AfDecimal tgaImpTfrObj) {
        if (tgaImpTfrObj != null) {
            setTgaImpTfr(new AfDecimal(tgaImpTfrObj, 15, 3));
            ws.getIndTrchDiGar().setImpTfr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTfrStrc() {
        return ws.getTrchDiGar().getTgaImpTfrStrc().getTgaImpTfrStrc();
    }

    @Override
    public void setTgaImpTfrStrc(AfDecimal tgaImpTfrStrc) {
        ws.getTrchDiGar().getTgaImpTfrStrc().setTgaImpTfrStrc(tgaImpTfrStrc.copy());
    }

    @Override
    public AfDecimal getTgaImpTfrStrcObj() {
        if (ws.getIndTrchDiGar().getImpTfrStrc() >= 0) {
            return getTgaImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTfrStrcObj(AfDecimal tgaImpTfrStrcObj) {
        if (tgaImpTfrStrcObj != null) {
            setTgaImpTfrStrc(new AfDecimal(tgaImpTfrStrcObj, 15, 3));
            ws.getIndTrchDiGar().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpTrasfe() {
        return ws.getTrchDiGar().getTgaImpTrasfe().getTgaImpTrasfe();
    }

    @Override
    public void setTgaImpTrasfe(AfDecimal tgaImpTrasfe) {
        ws.getTrchDiGar().getTgaImpTrasfe().setTgaImpTrasfe(tgaImpTrasfe.copy());
    }

    @Override
    public AfDecimal getTgaImpTrasfeObj() {
        if (ws.getIndTrchDiGar().getImpTrasfe() >= 0) {
            return getTgaImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpTrasfeObj(AfDecimal tgaImpTrasfeObj) {
        if (tgaImpTrasfeObj != null) {
            setTgaImpTrasfe(new AfDecimal(tgaImpTrasfeObj, 15, 3));
            ws.getIndTrchDiGar().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpVolo() {
        return ws.getTrchDiGar().getTgaImpVolo().getTgaImpVolo();
    }

    @Override
    public void setTgaImpVolo(AfDecimal tgaImpVolo) {
        ws.getTrchDiGar().getTgaImpVolo().setTgaImpVolo(tgaImpVolo.copy());
    }

    @Override
    public AfDecimal getTgaImpVoloObj() {
        if (ws.getIndTrchDiGar().getImpVolo() >= 0) {
            return getTgaImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpVoloObj(AfDecimal tgaImpVoloObj) {
        if (tgaImpVoloObj != null) {
            setTgaImpVolo(new AfDecimal(tgaImpVoloObj, 15, 3));
            ws.getIndTrchDiGar().setImpVolo(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbCommisInter() {
        return ws.getTrchDiGar().getTgaImpbCommisInter().getTgaImpbCommisInter();
    }

    @Override
    public void setTgaImpbCommisInter(AfDecimal tgaImpbCommisInter) {
        ws.getTrchDiGar().getTgaImpbCommisInter().setTgaImpbCommisInter(tgaImpbCommisInter.copy());
    }

    @Override
    public AfDecimal getTgaImpbCommisInterObj() {
        if (ws.getIndTrchDiGar().getImpbCommisInter() >= 0) {
            return getTgaImpbCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbCommisInterObj(AfDecimal tgaImpbCommisInterObj) {
        if (tgaImpbCommisInterObj != null) {
            setTgaImpbCommisInter(new AfDecimal(tgaImpbCommisInterObj, 15, 3));
            ws.getIndTrchDiGar().setImpbCommisInter(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvAcq() {
        return ws.getTrchDiGar().getTgaImpbProvAcq().getTgaImpbProvAcq();
    }

    @Override
    public void setTgaImpbProvAcq(AfDecimal tgaImpbProvAcq) {
        ws.getTrchDiGar().getTgaImpbProvAcq().setTgaImpbProvAcq(tgaImpbProvAcq.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvAcqObj() {
        if (ws.getIndTrchDiGar().getImpbProvAcq() >= 0) {
            return getTgaImpbProvAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvAcqObj(AfDecimal tgaImpbProvAcqObj) {
        if (tgaImpbProvAcqObj != null) {
            setTgaImpbProvAcq(new AfDecimal(tgaImpbProvAcqObj, 15, 3));
            ws.getIndTrchDiGar().setImpbProvAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbProvAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvInc() {
        return ws.getTrchDiGar().getTgaImpbProvInc().getTgaImpbProvInc();
    }

    @Override
    public void setTgaImpbProvInc(AfDecimal tgaImpbProvInc) {
        ws.getTrchDiGar().getTgaImpbProvInc().setTgaImpbProvInc(tgaImpbProvInc.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvIncObj() {
        if (ws.getIndTrchDiGar().getImpbProvInc() >= 0) {
            return getTgaImpbProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvIncObj(AfDecimal tgaImpbProvIncObj) {
        if (tgaImpbProvIncObj != null) {
            setTgaImpbProvInc(new AfDecimal(tgaImpbProvIncObj, 15, 3));
            ws.getIndTrchDiGar().setImpbProvInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbProvRicor() {
        return ws.getTrchDiGar().getTgaImpbProvRicor().getTgaImpbProvRicor();
    }

    @Override
    public void setTgaImpbProvRicor(AfDecimal tgaImpbProvRicor) {
        ws.getTrchDiGar().getTgaImpbProvRicor().setTgaImpbProvRicor(tgaImpbProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaImpbProvRicorObj() {
        if (ws.getIndTrchDiGar().getImpbProvRicor() >= 0) {
            return getTgaImpbProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbProvRicorObj(AfDecimal tgaImpbProvRicorObj) {
        if (tgaImpbProvRicorObj != null) {
            setTgaImpbProvRicor(new AfDecimal(tgaImpbProvRicorObj, 15, 3));
            ws.getIndTrchDiGar().setImpbProvRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbRemunAss() {
        return ws.getTrchDiGar().getTgaImpbRemunAss().getTgaImpbRemunAss();
    }

    @Override
    public void setTgaImpbRemunAss(AfDecimal tgaImpbRemunAss) {
        ws.getTrchDiGar().getTgaImpbRemunAss().setTgaImpbRemunAss(tgaImpbRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaImpbRemunAssObj() {
        if (ws.getIndTrchDiGar().getImpbRemunAss() >= 0) {
            return getTgaImpbRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbRemunAssObj(AfDecimal tgaImpbRemunAssObj) {
        if (tgaImpbRemunAssObj != null) {
            setTgaImpbRemunAss(new AfDecimal(tgaImpbRemunAssObj, 15, 3));
            ws.getIndTrchDiGar().setImpbRemunAss(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000() {
        return ws.getTrchDiGar().getTgaImpbVisEnd2000().getTgaImpbVisEnd2000();
    }

    @Override
    public void setTgaImpbVisEnd2000(AfDecimal tgaImpbVisEnd2000) {
        ws.getTrchDiGar().getTgaImpbVisEnd2000().setTgaImpbVisEnd2000(tgaImpbVisEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000Obj() {
        if (ws.getIndTrchDiGar().getImpbVisEnd2000() >= 0) {
            return getTgaImpbVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaImpbVisEnd2000Obj(AfDecimal tgaImpbVisEnd2000Obj) {
        if (tgaImpbVisEnd2000Obj != null) {
            setTgaImpbVisEnd2000(new AfDecimal(tgaImpbVisEnd2000Obj, 15, 3));
            ws.getIndTrchDiGar().setImpbVisEnd2000(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setImpbVisEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIncrPre() {
        return ws.getTrchDiGar().getTgaIncrPre().getTgaIncrPre();
    }

    @Override
    public void setTgaIncrPre(AfDecimal tgaIncrPre) {
        ws.getTrchDiGar().getTgaIncrPre().setTgaIncrPre(tgaIncrPre.copy());
    }

    @Override
    public AfDecimal getTgaIncrPreObj() {
        if (ws.getIndTrchDiGar().getIncrPre() >= 0) {
            return getTgaIncrPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIncrPreObj(AfDecimal tgaIncrPreObj) {
        if (tgaIncrPreObj != null) {
            setTgaIncrPre(new AfDecimal(tgaIncrPreObj, 15, 3));
            ws.getIndTrchDiGar().setIncrPre(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIncrPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIncrPrstz() {
        return ws.getTrchDiGar().getTgaIncrPrstz().getTgaIncrPrstz();
    }

    @Override
    public void setTgaIncrPrstz(AfDecimal tgaIncrPrstz) {
        ws.getTrchDiGar().getTgaIncrPrstz().setTgaIncrPrstz(tgaIncrPrstz.copy());
    }

    @Override
    public AfDecimal getTgaIncrPrstzObj() {
        if (ws.getIndTrchDiGar().getIncrPrstz() >= 0) {
            return getTgaIncrPrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIncrPrstzObj(AfDecimal tgaIncrPrstzObj) {
        if (tgaIncrPrstzObj != null) {
            setTgaIncrPrstz(new AfDecimal(tgaIncrPrstzObj, 15, 3));
            ws.getIndTrchDiGar().setIncrPrstz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIncrPrstz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaIntrMora() {
        return ws.getTrchDiGar().getTgaIntrMora().getTgaIntrMora();
    }

    @Override
    public void setTgaIntrMora(AfDecimal tgaIntrMora) {
        ws.getTrchDiGar().getTgaIntrMora().setTgaIntrMora(tgaIntrMora.copy());
    }

    @Override
    public AfDecimal getTgaIntrMoraObj() {
        if (ws.getIndTrchDiGar().getIntrMora() >= 0) {
            return getTgaIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaIntrMoraObj(AfDecimal tgaIntrMoraObj) {
        if (tgaIntrMoraObj != null) {
            setTgaIntrMora(new AfDecimal(tgaIntrMoraObj, 15, 3));
            ws.getIndTrchDiGar().setIntrMora(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaManfeeAntic() {
        return ws.getTrchDiGar().getTgaManfeeAntic().getTgaManfeeAntic();
    }

    @Override
    public void setTgaManfeeAntic(AfDecimal tgaManfeeAntic) {
        ws.getTrchDiGar().getTgaManfeeAntic().setTgaManfeeAntic(tgaManfeeAntic.copy());
    }

    @Override
    public AfDecimal getTgaManfeeAnticObj() {
        if (ws.getIndTrchDiGar().getManfeeAntic() >= 0) {
            return getTgaManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaManfeeAnticObj(AfDecimal tgaManfeeAnticObj) {
        if (tgaManfeeAnticObj != null) {
            setTgaManfeeAntic(new AfDecimal(tgaManfeeAnticObj, 15, 3));
            ws.getIndTrchDiGar().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaManfeeRicor() {
        return ws.getTrchDiGar().getTgaManfeeRicor().getTgaManfeeRicor();
    }

    @Override
    public void setTgaManfeeRicor(AfDecimal tgaManfeeRicor) {
        ws.getTrchDiGar().getTgaManfeeRicor().setTgaManfeeRicor(tgaManfeeRicor.copy());
    }

    @Override
    public AfDecimal getTgaManfeeRicorObj() {
        if (ws.getIndTrchDiGar().getManfeeRicor() >= 0) {
            return getTgaManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaManfeeRicorObj(AfDecimal tgaManfeeRicorObj) {
        if (tgaManfeeRicorObj != null) {
            setTgaManfeeRicor(new AfDecimal(tgaManfeeRicorObj, 15, 3));
            ws.getIndTrchDiGar().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMatuEnd2000() {
        return ws.getTrchDiGar().getTgaMatuEnd2000().getTgaMatuEnd2000();
    }

    @Override
    public void setTgaMatuEnd2000(AfDecimal tgaMatuEnd2000) {
        ws.getTrchDiGar().getTgaMatuEnd2000().setTgaMatuEnd2000(tgaMatuEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaMatuEnd2000Obj() {
        if (ws.getIndTrchDiGar().getMatuEnd2000() >= 0) {
            return getTgaMatuEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMatuEnd2000Obj(AfDecimal tgaMatuEnd2000Obj) {
        if (tgaMatuEnd2000Obj != null) {
            setTgaMatuEnd2000(new AfDecimal(tgaMatuEnd2000Obj, 15, 3));
            ws.getIndTrchDiGar().setMatuEnd2000(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setMatuEnd2000(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMinGarto() {
        return ws.getTrchDiGar().getTgaMinGarto().getTgaMinGarto();
    }

    @Override
    public void setTgaMinGarto(AfDecimal tgaMinGarto) {
        ws.getTrchDiGar().getTgaMinGarto().setTgaMinGarto(tgaMinGarto.copy());
    }

    @Override
    public AfDecimal getTgaMinGartoObj() {
        if (ws.getIndTrchDiGar().getMinGarto() >= 0) {
            return getTgaMinGarto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMinGartoObj(AfDecimal tgaMinGartoObj) {
        if (tgaMinGartoObj != null) {
            setTgaMinGarto(new AfDecimal(tgaMinGartoObj, 14, 9));
            ws.getIndTrchDiGar().setMinGarto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setMinGarto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaMinTrnut() {
        return ws.getTrchDiGar().getTgaMinTrnut().getTgaMinTrnut();
    }

    @Override
    public void setTgaMinTrnut(AfDecimal tgaMinTrnut) {
        ws.getTrchDiGar().getTgaMinTrnut().setTgaMinTrnut(tgaMinTrnut.copy());
    }

    @Override
    public AfDecimal getTgaMinTrnutObj() {
        if (ws.getIndTrchDiGar().getMinTrnut() >= 0) {
            return getTgaMinTrnut();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaMinTrnutObj(AfDecimal tgaMinTrnutObj) {
        if (tgaMinTrnutObj != null) {
            setTgaMinTrnut(new AfDecimal(tgaMinTrnutObj, 14, 9));
            ws.getIndTrchDiGar().setMinTrnut(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setMinTrnut(((short)-1));
        }
    }

    @Override
    public String getTgaModCalc() {
        return ws.getTrchDiGar().getTgaModCalc();
    }

    @Override
    public void setTgaModCalc(String tgaModCalc) {
        ws.getTrchDiGar().setTgaModCalc(tgaModCalc);
    }

    @Override
    public String getTgaModCalcObj() {
        if (ws.getIndTrchDiGar().getModCalc() >= 0) {
            return getTgaModCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaModCalcObj(String tgaModCalcObj) {
        if (tgaModCalcObj != null) {
            setTgaModCalc(tgaModCalcObj);
            ws.getIndTrchDiGar().setModCalc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setModCalc(((short)-1));
        }
    }

    @Override
    public int getTgaNumGgRival() {
        return ws.getTrchDiGar().getTgaNumGgRival().getTgaNumGgRival();
    }

    @Override
    public void setTgaNumGgRival(int tgaNumGgRival) {
        ws.getTrchDiGar().getTgaNumGgRival().setTgaNumGgRival(tgaNumGgRival);
    }

    @Override
    public Integer getTgaNumGgRivalObj() {
        if (ws.getIndTrchDiGar().getNumGgRival() >= 0) {
            return ((Integer)getTgaNumGgRival());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaNumGgRivalObj(Integer tgaNumGgRivalObj) {
        if (tgaNumGgRivalObj != null) {
            setTgaNumGgRival(((int)tgaNumGgRivalObj));
            ws.getIndTrchDiGar().setNumGgRival(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setNumGgRival(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaOldTsTec() {
        return ws.getTrchDiGar().getTgaOldTsTec().getTgaOldTsTec();
    }

    @Override
    public void setTgaOldTsTec(AfDecimal tgaOldTsTec) {
        ws.getTrchDiGar().getTgaOldTsTec().setTgaOldTsTec(tgaOldTsTec.copy());
    }

    @Override
    public AfDecimal getTgaOldTsTecObj() {
        if (ws.getIndTrchDiGar().getOldTsTec() >= 0) {
            return getTgaOldTsTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaOldTsTecObj(AfDecimal tgaOldTsTecObj) {
        if (tgaOldTsTecObj != null) {
            setTgaOldTsTec(new AfDecimal(tgaOldTsTecObj, 14, 9));
            ws.getIndTrchDiGar().setOldTsTec(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setOldTsTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcCommisGest() {
        return ws.getTrchDiGar().getTgaPcCommisGest().getTgaPcCommisGest();
    }

    @Override
    public void setTgaPcCommisGest(AfDecimal tgaPcCommisGest) {
        ws.getTrchDiGar().getTgaPcCommisGest().setTgaPcCommisGest(tgaPcCommisGest.copy());
    }

    @Override
    public AfDecimal getTgaPcCommisGestObj() {
        if (ws.getIndTrchDiGar().getPcCommisGest() >= 0) {
            return getTgaPcCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcCommisGestObj(AfDecimal tgaPcCommisGestObj) {
        if (tgaPcCommisGestObj != null) {
            setTgaPcCommisGest(new AfDecimal(tgaPcCommisGestObj, 6, 3));
            ws.getIndTrchDiGar().setPcCommisGest(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcCommisGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcIntrRiat() {
        return ws.getTrchDiGar().getTgaPcIntrRiat().getTgaPcIntrRiat();
    }

    @Override
    public void setTgaPcIntrRiat(AfDecimal tgaPcIntrRiat) {
        ws.getTrchDiGar().getTgaPcIntrRiat().setTgaPcIntrRiat(tgaPcIntrRiat.copy());
    }

    @Override
    public AfDecimal getTgaPcIntrRiatObj() {
        if (ws.getIndTrchDiGar().getPcIntrRiat() >= 0) {
            return getTgaPcIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcIntrRiatObj(AfDecimal tgaPcIntrRiatObj) {
        if (tgaPcIntrRiatObj != null) {
            setTgaPcIntrRiat(new AfDecimal(tgaPcIntrRiatObj, 6, 3));
            ws.getIndTrchDiGar().setPcIntrRiat(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcRetr() {
        return ws.getTrchDiGar().getTgaPcRetr().getTgaPcRetr();
    }

    @Override
    public void setTgaPcRetr(AfDecimal tgaPcRetr) {
        ws.getTrchDiGar().getTgaPcRetr().setTgaPcRetr(tgaPcRetr.copy());
    }

    @Override
    public AfDecimal getTgaPcRetrObj() {
        if (ws.getIndTrchDiGar().getPcRetr() >= 0) {
            return getTgaPcRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcRetrObj(AfDecimal tgaPcRetrObj) {
        if (tgaPcRetrObj != null) {
            setTgaPcRetr(new AfDecimal(tgaPcRetrObj, 6, 3));
            ws.getIndTrchDiGar().setPcRetr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPcRipPre() {
        return ws.getTrchDiGar().getTgaPcRipPre().getTgaPcRipPre();
    }

    @Override
    public void setTgaPcRipPre(AfDecimal tgaPcRipPre) {
        ws.getTrchDiGar().getTgaPcRipPre().setTgaPcRipPre(tgaPcRipPre.copy());
    }

    @Override
    public AfDecimal getTgaPcRipPreObj() {
        if (ws.getIndTrchDiGar().getPcRipPre() >= 0) {
            return getTgaPcRipPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPcRipPreObj(AfDecimal tgaPcRipPreObj) {
        if (tgaPcRipPreObj != null) {
            setTgaPcRipPre(new AfDecimal(tgaPcRipPreObj, 6, 3));
            ws.getIndTrchDiGar().setPcRipPre(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPcRipPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreAttDiTrch() {
        return ws.getTrchDiGar().getTgaPreAttDiTrch().getTgaPreAttDiTrch();
    }

    @Override
    public void setTgaPreAttDiTrch(AfDecimal tgaPreAttDiTrch) {
        ws.getTrchDiGar().getTgaPreAttDiTrch().setTgaPreAttDiTrch(tgaPreAttDiTrch.copy());
    }

    @Override
    public AfDecimal getTgaPreAttDiTrchObj() {
        if (ws.getIndTrchDiGar().getPreAttDiTrch() >= 0) {
            return getTgaPreAttDiTrch();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreAttDiTrchObj(AfDecimal tgaPreAttDiTrchObj) {
        if (tgaPreAttDiTrchObj != null) {
            setTgaPreAttDiTrch(new AfDecimal(tgaPreAttDiTrchObj, 15, 3));
            ws.getIndTrchDiGar().setPreAttDiTrch(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreAttDiTrch(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreCasoMor() {
        return ws.getTrchDiGar().getTgaPreCasoMor().getTgaPreCasoMor();
    }

    @Override
    public void setTgaPreCasoMor(AfDecimal tgaPreCasoMor) {
        ws.getTrchDiGar().getTgaPreCasoMor().setTgaPreCasoMor(tgaPreCasoMor.copy());
    }

    @Override
    public AfDecimal getTgaPreCasoMorObj() {
        if (ws.getIndTrchDiGar().getPreCasoMor() >= 0) {
            return getTgaPreCasoMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreCasoMorObj(AfDecimal tgaPreCasoMorObj) {
        if (tgaPreCasoMorObj != null) {
            setTgaPreCasoMor(new AfDecimal(tgaPreCasoMorObj, 15, 3));
            ws.getIndTrchDiGar().setPreCasoMor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreCasoMor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreIniNet() {
        return ws.getTrchDiGar().getTgaPreIniNet().getTgaPreIniNet();
    }

    @Override
    public void setTgaPreIniNet(AfDecimal tgaPreIniNet) {
        ws.getTrchDiGar().getTgaPreIniNet().setTgaPreIniNet(tgaPreIniNet.copy());
    }

    @Override
    public AfDecimal getTgaPreIniNetObj() {
        if (ws.getIndTrchDiGar().getPreIniNet() >= 0) {
            return getTgaPreIniNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreIniNetObj(AfDecimal tgaPreIniNetObj) {
        if (tgaPreIniNetObj != null) {
            setTgaPreIniNet(new AfDecimal(tgaPreIniNetObj, 15, 3));
            ws.getIndTrchDiGar().setPreIniNet(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreIniNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreInvrioIni() {
        return ws.getTrchDiGar().getTgaPreInvrioIni().getTgaPreInvrioIni();
    }

    @Override
    public void setTgaPreInvrioIni(AfDecimal tgaPreInvrioIni) {
        ws.getTrchDiGar().getTgaPreInvrioIni().setTgaPreInvrioIni(tgaPreInvrioIni.copy());
    }

    @Override
    public AfDecimal getTgaPreInvrioIniObj() {
        if (ws.getIndTrchDiGar().getPreInvrioIni() >= 0) {
            return getTgaPreInvrioIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreInvrioIniObj(AfDecimal tgaPreInvrioIniObj) {
        if (tgaPreInvrioIniObj != null) {
            setTgaPreInvrioIni(new AfDecimal(tgaPreInvrioIniObj, 15, 3));
            ws.getIndTrchDiGar().setPreInvrioIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreInvrioIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreInvrioUlt() {
        return ws.getTrchDiGar().getTgaPreInvrioUlt().getTgaPreInvrioUlt();
    }

    @Override
    public void setTgaPreInvrioUlt(AfDecimal tgaPreInvrioUlt) {
        ws.getTrchDiGar().getTgaPreInvrioUlt().setTgaPreInvrioUlt(tgaPreInvrioUlt.copy());
    }

    @Override
    public AfDecimal getTgaPreInvrioUltObj() {
        if (ws.getIndTrchDiGar().getPreInvrioUlt() >= 0) {
            return getTgaPreInvrioUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreInvrioUltObj(AfDecimal tgaPreInvrioUltObj) {
        if (tgaPreInvrioUltObj != null) {
            setTgaPreInvrioUlt(new AfDecimal(tgaPreInvrioUltObj, 15, 3));
            ws.getIndTrchDiGar().setPreInvrioUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreInvrioUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreLrd() {
        return ws.getTrchDiGar().getTgaPreLrd().getTgaPreLrd();
    }

    @Override
    public void setTgaPreLrd(AfDecimal tgaPreLrd) {
        ws.getTrchDiGar().getTgaPreLrd().setTgaPreLrd(tgaPreLrd.copy());
    }

    @Override
    public AfDecimal getTgaPreLrdObj() {
        if (ws.getIndTrchDiGar().getPreLrd() >= 0) {
            return getTgaPreLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreLrdObj(AfDecimal tgaPreLrdObj) {
        if (tgaPreLrdObj != null) {
            setTgaPreLrd(new AfDecimal(tgaPreLrdObj, 15, 3));
            ws.getIndTrchDiGar().setPreLrd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePattuito() {
        return ws.getTrchDiGar().getTgaPrePattuito().getTgaPrePattuito();
    }

    @Override
    public void setTgaPrePattuito(AfDecimal tgaPrePattuito) {
        ws.getTrchDiGar().getTgaPrePattuito().setTgaPrePattuito(tgaPrePattuito.copy());
    }

    @Override
    public AfDecimal getTgaPrePattuitoObj() {
        if (ws.getIndTrchDiGar().getPrePattuito() >= 0) {
            return getTgaPrePattuito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePattuitoObj(AfDecimal tgaPrePattuitoObj) {
        if (tgaPrePattuitoObj != null) {
            setTgaPrePattuito(new AfDecimal(tgaPrePattuitoObj, 15, 3));
            ws.getIndTrchDiGar().setPrePattuito(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrePattuito(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePpIni() {
        return ws.getTrchDiGar().getTgaPrePpIni().getTgaPrePpIni();
    }

    @Override
    public void setTgaPrePpIni(AfDecimal tgaPrePpIni) {
        ws.getTrchDiGar().getTgaPrePpIni().setTgaPrePpIni(tgaPrePpIni.copy());
    }

    @Override
    public AfDecimal getTgaPrePpIniObj() {
        if (ws.getIndTrchDiGar().getPrePpIni() >= 0) {
            return getTgaPrePpIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePpIniObj(AfDecimal tgaPrePpIniObj) {
        if (tgaPrePpIniObj != null) {
            setTgaPrePpIni(new AfDecimal(tgaPrePpIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrePpIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrePpIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrePpUlt() {
        return ws.getTrchDiGar().getTgaPrePpUlt().getTgaPrePpUlt();
    }

    @Override
    public void setTgaPrePpUlt(AfDecimal tgaPrePpUlt) {
        ws.getTrchDiGar().getTgaPrePpUlt().setTgaPrePpUlt(tgaPrePpUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrePpUltObj() {
        if (ws.getIndTrchDiGar().getPrePpUlt() >= 0) {
            return getTgaPrePpUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrePpUltObj(AfDecimal tgaPrePpUltObj) {
        if (tgaPrePpUltObj != null) {
            setTgaPrePpUlt(new AfDecimal(tgaPrePpUltObj, 15, 3));
            ws.getIndTrchDiGar().setPrePpUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrePpUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreRivto() {
        return ws.getTrchDiGar().getTgaPreRivto().getTgaPreRivto();
    }

    @Override
    public void setTgaPreRivto(AfDecimal tgaPreRivto) {
        ws.getTrchDiGar().getTgaPreRivto().setTgaPreRivto(tgaPreRivto.copy());
    }

    @Override
    public AfDecimal getTgaPreRivtoObj() {
        if (ws.getIndTrchDiGar().getPreRivto() >= 0) {
            return getTgaPreRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreRivtoObj(AfDecimal tgaPreRivtoObj) {
        if (tgaPreRivtoObj != null) {
            setTgaPreRivto(new AfDecimal(tgaPreRivtoObj, 15, 3));
            ws.getIndTrchDiGar().setPreRivto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreStab() {
        return ws.getTrchDiGar().getTgaPreStab().getTgaPreStab();
    }

    @Override
    public void setTgaPreStab(AfDecimal tgaPreStab) {
        ws.getTrchDiGar().getTgaPreStab().setTgaPreStab(tgaPreStab.copy());
    }

    @Override
    public AfDecimal getTgaPreStabObj() {
        if (ws.getIndTrchDiGar().getPreStab() >= 0) {
            return getTgaPreStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreStabObj(AfDecimal tgaPreStabObj) {
        if (tgaPreStabObj != null) {
            setTgaPreStab(new AfDecimal(tgaPreStabObj, 15, 3));
            ws.getIndTrchDiGar().setPreStab(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreTariIni() {
        return ws.getTrchDiGar().getTgaPreTariIni().getTgaPreTariIni();
    }

    @Override
    public void setTgaPreTariIni(AfDecimal tgaPreTariIni) {
        ws.getTrchDiGar().getTgaPreTariIni().setTgaPreTariIni(tgaPreTariIni.copy());
    }

    @Override
    public AfDecimal getTgaPreTariIniObj() {
        if (ws.getIndTrchDiGar().getPreTariIni() >= 0) {
            return getTgaPreTariIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreTariIniObj(AfDecimal tgaPreTariIniObj) {
        if (tgaPreTariIniObj != null) {
            setTgaPreTariIni(new AfDecimal(tgaPreTariIniObj, 15, 3));
            ws.getIndTrchDiGar().setPreTariIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreTariIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreTariUlt() {
        return ws.getTrchDiGar().getTgaPreTariUlt().getTgaPreTariUlt();
    }

    @Override
    public void setTgaPreTariUlt(AfDecimal tgaPreTariUlt) {
        ws.getTrchDiGar().getTgaPreTariUlt().setTgaPreTariUlt(tgaPreTariUlt.copy());
    }

    @Override
    public AfDecimal getTgaPreTariUltObj() {
        if (ws.getIndTrchDiGar().getPreTariUlt() >= 0) {
            return getTgaPreTariUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreTariUltObj(AfDecimal tgaPreTariUltObj) {
        if (tgaPreTariUltObj != null) {
            setTgaPreTariUlt(new AfDecimal(tgaPreTariUltObj, 15, 3));
            ws.getIndTrchDiGar().setPreTariUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreTariUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPreUniRivto() {
        return ws.getTrchDiGar().getTgaPreUniRivto().getTgaPreUniRivto();
    }

    @Override
    public void setTgaPreUniRivto(AfDecimal tgaPreUniRivto) {
        ws.getTrchDiGar().getTgaPreUniRivto().setTgaPreUniRivto(tgaPreUniRivto.copy());
    }

    @Override
    public AfDecimal getTgaPreUniRivtoObj() {
        if (ws.getIndTrchDiGar().getPreUniRivto() >= 0) {
            return getTgaPreUniRivto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPreUniRivtoObj(AfDecimal tgaPreUniRivtoObj) {
        if (tgaPreUniRivtoObj != null) {
            setTgaPreUniRivto(new AfDecimal(tgaPreUniRivtoObj, 15, 3));
            ws.getIndTrchDiGar().setPreUniRivto(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPreUniRivto(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProv1aaAcq() {
        return ws.getTrchDiGar().getTgaProv1aaAcq().getTgaProv1aaAcq();
    }

    @Override
    public void setTgaProv1aaAcq(AfDecimal tgaProv1aaAcq) {
        ws.getTrchDiGar().getTgaProv1aaAcq().setTgaProv1aaAcq(tgaProv1aaAcq.copy());
    }

    @Override
    public AfDecimal getTgaProv1aaAcqObj() {
        if (ws.getIndTrchDiGar().getProv1aaAcq() >= 0) {
            return getTgaProv1aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProv1aaAcqObj(AfDecimal tgaProv1aaAcqObj) {
        if (tgaProv1aaAcqObj != null) {
            setTgaProv1aaAcq(new AfDecimal(tgaProv1aaAcqObj, 15, 3));
            ws.getIndTrchDiGar().setProv1aaAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProv1aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProv2aaAcq() {
        return ws.getTrchDiGar().getTgaProv2aaAcq().getTgaProv2aaAcq();
    }

    @Override
    public void setTgaProv2aaAcq(AfDecimal tgaProv2aaAcq) {
        ws.getTrchDiGar().getTgaProv2aaAcq().setTgaProv2aaAcq(tgaProv2aaAcq.copy());
    }

    @Override
    public AfDecimal getTgaProv2aaAcqObj() {
        if (ws.getIndTrchDiGar().getProv2aaAcq() >= 0) {
            return getTgaProv2aaAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProv2aaAcqObj(AfDecimal tgaProv2aaAcqObj) {
        if (tgaProv2aaAcqObj != null) {
            setTgaProv2aaAcq(new AfDecimal(tgaProv2aaAcqObj, 15, 3));
            ws.getIndTrchDiGar().setProv2aaAcq(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProv2aaAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProvInc() {
        return ws.getTrchDiGar().getTgaProvInc().getTgaProvInc();
    }

    @Override
    public void setTgaProvInc(AfDecimal tgaProvInc) {
        ws.getTrchDiGar().getTgaProvInc().setTgaProvInc(tgaProvInc.copy());
    }

    @Override
    public AfDecimal getTgaProvIncObj() {
        if (ws.getIndTrchDiGar().getProvInc() >= 0) {
            return getTgaProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProvIncObj(AfDecimal tgaProvIncObj) {
        if (tgaProvIncObj != null) {
            setTgaProvInc(new AfDecimal(tgaProvIncObj, 15, 3));
            ws.getIndTrchDiGar().setProvInc(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaProvRicor() {
        return ws.getTrchDiGar().getTgaProvRicor().getTgaProvRicor();
    }

    @Override
    public void setTgaProvRicor(AfDecimal tgaProvRicor) {
        ws.getTrchDiGar().getTgaProvRicor().setTgaProvRicor(tgaProvRicor.copy());
    }

    @Override
    public AfDecimal getTgaProvRicorObj() {
        if (ws.getIndTrchDiGar().getProvRicor() >= 0) {
            return getTgaProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaProvRicorObj(AfDecimal tgaProvRicorObj) {
        if (tgaProvRicorObj != null) {
            setTgaProvRicor(new AfDecimal(tgaProvRicorObj, 15, 3));
            ws.getIndTrchDiGar().setProvRicor(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzAggIni() {
        return ws.getTrchDiGar().getTgaPrstzAggIni().getTgaPrstzAggIni();
    }

    @Override
    public void setTgaPrstzAggIni(AfDecimal tgaPrstzAggIni) {
        ws.getTrchDiGar().getTgaPrstzAggIni().setTgaPrstzAggIni(tgaPrstzAggIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzAggIniObj() {
        if (ws.getIndTrchDiGar().getPrstzAggIni() >= 0) {
            return getTgaPrstzAggIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzAggIniObj(AfDecimal tgaPrstzAggIniObj) {
        if (tgaPrstzAggIniObj != null) {
            setTgaPrstzAggIni(new AfDecimal(tgaPrstzAggIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzAggIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzAggIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzAggUlt() {
        return ws.getTrchDiGar().getTgaPrstzAggUlt().getTgaPrstzAggUlt();
    }

    @Override
    public void setTgaPrstzAggUlt(AfDecimal tgaPrstzAggUlt) {
        ws.getTrchDiGar().getTgaPrstzAggUlt().setTgaPrstzAggUlt(tgaPrstzAggUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrstzAggUltObj() {
        if (ws.getIndTrchDiGar().getPrstzAggUlt() >= 0) {
            return getTgaPrstzAggUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzAggUltObj(AfDecimal tgaPrstzAggUltObj) {
        if (tgaPrstzAggUltObj != null) {
            setTgaPrstzAggUlt(new AfDecimal(tgaPrstzAggUltObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzAggUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzAggUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIni() {
        return ws.getTrchDiGar().getTgaPrstzIni().getTgaPrstzIni();
    }

    @Override
    public void setTgaPrstzIni(AfDecimal tgaPrstzIni) {
        ws.getTrchDiGar().getTgaPrstzIni().setTgaPrstzIni(tgaPrstzIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfis() {
        return ws.getTrchDiGar().getTgaPrstzIniNewfis().getTgaPrstzIniNewfis();
    }

    @Override
    public void setTgaPrstzIniNewfis(AfDecimal tgaPrstzIniNewfis) {
        ws.getTrchDiGar().getTgaPrstzIniNewfis().setTgaPrstzIniNewfis(tgaPrstzIniNewfis.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfisObj() {
        if (ws.getIndTrchDiGar().getPrstzIniNewfis() >= 0) {
            return getTgaPrstzIniNewfis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniNewfisObj(AfDecimal tgaPrstzIniNewfisObj) {
        if (tgaPrstzIniNewfisObj != null) {
            setTgaPrstzIniNewfis(new AfDecimal(tgaPrstzIniNewfisObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIniNewfis(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIniNewfis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniNforz() {
        return ws.getTrchDiGar().getTgaPrstzIniNforz().getTgaPrstzIniNforz();
    }

    @Override
    public void setTgaPrstzIniNforz(AfDecimal tgaPrstzIniNforz) {
        ws.getTrchDiGar().getTgaPrstzIniNforz().setTgaPrstzIniNforz(tgaPrstzIniNforz.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniNforzObj() {
        if (ws.getIndTrchDiGar().getPrstzIniNforz() >= 0) {
            return getTgaPrstzIniNforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniNforzObj(AfDecimal tgaPrstzIniNforzObj) {
        if (tgaPrstzIniNforzObj != null) {
            setTgaPrstzIniNforz(new AfDecimal(tgaPrstzIniNforzObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIniNforz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIniNforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniObj() {
        if (ws.getIndTrchDiGar().getPrstzIni() >= 0) {
            return getTgaPrstzIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniObj(AfDecimal tgaPrstzIniObj) {
        if (tgaPrstzIniObj != null) {
            setTgaPrstzIni(new AfDecimal(tgaPrstzIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzIniStab() {
        return ws.getTrchDiGar().getTgaPrstzIniStab().getTgaPrstzIniStab();
    }

    @Override
    public void setTgaPrstzIniStab(AfDecimal tgaPrstzIniStab) {
        ws.getTrchDiGar().getTgaPrstzIniStab().setTgaPrstzIniStab(tgaPrstzIniStab.copy());
    }

    @Override
    public AfDecimal getTgaPrstzIniStabObj() {
        if (ws.getIndTrchDiGar().getPrstzIniStab() >= 0) {
            return getTgaPrstzIniStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzIniStabObj(AfDecimal tgaPrstzIniStabObj) {
        if (tgaPrstzIniStabObj != null) {
            setTgaPrstzIniStab(new AfDecimal(tgaPrstzIniStabObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzIniStab(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzIniStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzRidIni() {
        return ws.getTrchDiGar().getTgaPrstzRidIni().getTgaPrstzRidIni();
    }

    @Override
    public void setTgaPrstzRidIni(AfDecimal tgaPrstzRidIni) {
        ws.getTrchDiGar().getTgaPrstzRidIni().setTgaPrstzRidIni(tgaPrstzRidIni.copy());
    }

    @Override
    public AfDecimal getTgaPrstzRidIniObj() {
        if (ws.getIndTrchDiGar().getPrstzRidIni() >= 0) {
            return getTgaPrstzRidIni();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzRidIniObj(AfDecimal tgaPrstzRidIniObj) {
        if (tgaPrstzRidIniObj != null) {
            setTgaPrstzRidIni(new AfDecimal(tgaPrstzRidIniObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzRidIni(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzRidIni(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaPrstzUlt() {
        return ws.getTrchDiGar().getTgaPrstzUlt().getTgaPrstzUlt();
    }

    @Override
    public void setTgaPrstzUlt(AfDecimal tgaPrstzUlt) {
        ws.getTrchDiGar().getTgaPrstzUlt().setTgaPrstzUlt(tgaPrstzUlt.copy());
    }

    @Override
    public AfDecimal getTgaPrstzUltObj() {
        if (ws.getIndTrchDiGar().getPrstzUlt() >= 0) {
            return getTgaPrstzUlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaPrstzUltObj(AfDecimal tgaPrstzUltObj) {
        if (tgaPrstzUltObj != null) {
            setTgaPrstzUlt(new AfDecimal(tgaPrstzUltObj, 15, 3));
            ws.getIndTrchDiGar().setPrstzUlt(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setPrstzUlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRatLrd() {
        return ws.getTrchDiGar().getTgaRatLrd().getTgaRatLrd();
    }

    @Override
    public void setTgaRatLrd(AfDecimal tgaRatLrd) {
        ws.getTrchDiGar().getTgaRatLrd().setTgaRatLrd(tgaRatLrd.copy());
    }

    @Override
    public AfDecimal getTgaRatLrdObj() {
        if (ws.getIndTrchDiGar().getRatLrd() >= 0) {
            return getTgaRatLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRatLrdObj(AfDecimal tgaRatLrdObj) {
        if (tgaRatLrdObj != null) {
            setTgaRatLrd(new AfDecimal(tgaRatLrdObj, 15, 3));
            ws.getIndTrchDiGar().setRatLrd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRatLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRemunAss() {
        return ws.getTrchDiGar().getTgaRemunAss().getTgaRemunAss();
    }

    @Override
    public void setTgaRemunAss(AfDecimal tgaRemunAss) {
        ws.getTrchDiGar().getTgaRemunAss().setTgaRemunAss(tgaRemunAss.copy());
    }

    @Override
    public AfDecimal getTgaRemunAssObj() {
        if (ws.getIndTrchDiGar().getRemunAss() >= 0) {
            return getTgaRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRemunAssObj(AfDecimal tgaRemunAssObj) {
        if (tgaRemunAssObj != null) {
            setTgaRemunAss(new AfDecimal(tgaRemunAssObj, 15, 3));
            ws.getIndTrchDiGar().setRemunAss(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0() {
        return ws.getTrchDiGar().getTgaRenIniTsTec0().getTgaRenIniTsTec0();
    }

    @Override
    public void setTgaRenIniTsTec0(AfDecimal tgaRenIniTsTec0) {
        ws.getTrchDiGar().getTgaRenIniTsTec0().setTgaRenIniTsTec0(tgaRenIniTsTec0.copy());
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0Obj() {
        if (ws.getIndTrchDiGar().getRenIniTsTec0() >= 0) {
            return getTgaRenIniTsTec0();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRenIniTsTec0Obj(AfDecimal tgaRenIniTsTec0Obj) {
        if (tgaRenIniTsTec0Obj != null) {
            setTgaRenIniTsTec0(new AfDecimal(tgaRenIniTsTec0Obj, 15, 3));
            ws.getIndTrchDiGar().setRenIniTsTec0(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRenIniTsTec0(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRendtoLrd() {
        return ws.getTrchDiGar().getTgaRendtoLrd().getTgaRendtoLrd();
    }

    @Override
    public void setTgaRendtoLrd(AfDecimal tgaRendtoLrd) {
        ws.getTrchDiGar().getTgaRendtoLrd().setTgaRendtoLrd(tgaRendtoLrd.copy());
    }

    @Override
    public AfDecimal getTgaRendtoLrdObj() {
        if (ws.getIndTrchDiGar().getRendtoLrd() >= 0) {
            return getTgaRendtoLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRendtoLrdObj(AfDecimal tgaRendtoLrdObj) {
        if (tgaRendtoLrdObj != null) {
            setTgaRendtoLrd(new AfDecimal(tgaRendtoLrdObj, 14, 9));
            ws.getIndTrchDiGar().setRendtoLrd(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRendtoLrd(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRendtoRetr() {
        return ws.getTrchDiGar().getTgaRendtoRetr().getTgaRendtoRetr();
    }

    @Override
    public void setTgaRendtoRetr(AfDecimal tgaRendtoRetr) {
        ws.getTrchDiGar().getTgaRendtoRetr().setTgaRendtoRetr(tgaRendtoRetr.copy());
    }

    @Override
    public AfDecimal getTgaRendtoRetrObj() {
        if (ws.getIndTrchDiGar().getRendtoRetr() >= 0) {
            return getTgaRendtoRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRendtoRetrObj(AfDecimal tgaRendtoRetrObj) {
        if (tgaRendtoRetrObj != null) {
            setTgaRendtoRetr(new AfDecimal(tgaRendtoRetrObj, 14, 9));
            ws.getIndTrchDiGar().setRendtoRetr(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRendtoRetr(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaRisMat() {
        return ws.getTrchDiGar().getTgaRisMat().getTgaRisMat();
    }

    @Override
    public void setTgaRisMat(AfDecimal tgaRisMat) {
        ws.getTrchDiGar().getTgaRisMat().setTgaRisMat(tgaRisMat.copy());
    }

    @Override
    public AfDecimal getTgaRisMatObj() {
        if (ws.getIndTrchDiGar().getRisMat() >= 0) {
            return getTgaRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaRisMatObj(AfDecimal tgaRisMatObj) {
        if (tgaRisMatObj != null) {
            setTgaRisMat(new AfDecimal(tgaRisMatObj, 15, 3));
            ws.getIndTrchDiGar().setRisMat(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setRisMat(((short)-1));
        }
    }

    @Override
    public char getTgaTpAdegAbb() {
        return ws.getTrchDiGar().getTgaTpAdegAbb();
    }

    @Override
    public void setTgaTpAdegAbb(char tgaTpAdegAbb) {
        ws.getTrchDiGar().setTgaTpAdegAbb(tgaTpAdegAbb);
    }

    @Override
    public Character getTgaTpAdegAbbObj() {
        if (ws.getIndTrchDiGar().getTpAdegAbb() >= 0) {
            return ((Character)getTgaTpAdegAbb());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpAdegAbbObj(Character tgaTpAdegAbbObj) {
        if (tgaTpAdegAbbObj != null) {
            setTgaTpAdegAbb(((char)tgaTpAdegAbbObj));
            ws.getIndTrchDiGar().setTpAdegAbb(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTpAdegAbb(((short)-1));
        }
    }

    @Override
    public String getTgaTpManfeeAppl() {
        return ws.getTrchDiGar().getTgaTpManfeeAppl();
    }

    @Override
    public void setTgaTpManfeeAppl(String tgaTpManfeeAppl) {
        ws.getTrchDiGar().setTgaTpManfeeAppl(tgaTpManfeeAppl);
    }

    @Override
    public String getTgaTpManfeeApplObj() {
        if (ws.getIndTrchDiGar().getTpManfeeAppl() >= 0) {
            return getTgaTpManfeeAppl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpManfeeApplObj(String tgaTpManfeeApplObj) {
        if (tgaTpManfeeApplObj != null) {
            setTgaTpManfeeAppl(tgaTpManfeeApplObj);
            ws.getIndTrchDiGar().setTpManfeeAppl(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTpManfeeAppl(((short)-1));
        }
    }

    @Override
    public String getTgaTpRgmFisc() {
        return ws.getTrchDiGar().getTgaTpRgmFisc();
    }

    @Override
    public void setTgaTpRgmFisc(String tgaTpRgmFisc) {
        ws.getTrchDiGar().setTgaTpRgmFisc(tgaTpRgmFisc);
    }

    @Override
    public String getTgaTpRival() {
        return ws.getTrchDiGar().getTgaTpRival();
    }

    @Override
    public void setTgaTpRival(String tgaTpRival) {
        ws.getTrchDiGar().setTgaTpRival(tgaTpRival);
    }

    @Override
    public String getTgaTpRivalObj() {
        if (ws.getIndTrchDiGar().getTpRival() >= 0) {
            return getTgaTpRival();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTpRivalObj(String tgaTpRivalObj) {
        if (tgaTpRivalObj != null) {
            setTgaTpRival(tgaTpRivalObj);
            ws.getIndTrchDiGar().setTpRival(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTpRival(((short)-1));
        }
    }

    @Override
    public String getTgaTpTrch() {
        return ws.getTrchDiGar().getTgaTpTrch();
    }

    @Override
    public void setTgaTpTrch(String tgaTpTrch) {
        ws.getTrchDiGar().setTgaTpTrch(tgaTpTrch);
    }

    @Override
    public AfDecimal getTgaTsRivalFis() {
        return ws.getTrchDiGar().getTgaTsRivalFis().getTgaTsRivalFis();
    }

    @Override
    public void setTgaTsRivalFis(AfDecimal tgaTsRivalFis) {
        ws.getTrchDiGar().getTgaTsRivalFis().setTgaTsRivalFis(tgaTsRivalFis.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalFisObj() {
        if (ws.getIndTrchDiGar().getTsRivalFis() >= 0) {
            return getTgaTsRivalFis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalFisObj(AfDecimal tgaTsRivalFisObj) {
        if (tgaTsRivalFisObj != null) {
            setTgaTsRivalFis(new AfDecimal(tgaTsRivalFisObj, 14, 9));
            ws.getIndTrchDiGar().setTsRivalFis(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTsRivalFis(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaTsRivalIndiciz() {
        return ws.getTrchDiGar().getTgaTsRivalIndiciz().getTgaTsRivalIndiciz();
    }

    @Override
    public void setTgaTsRivalIndiciz(AfDecimal tgaTsRivalIndiciz) {
        ws.getTrchDiGar().getTgaTsRivalIndiciz().setTgaTsRivalIndiciz(tgaTsRivalIndiciz.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalIndicizObj() {
        if (ws.getIndTrchDiGar().getTsRivalIndiciz() >= 0) {
            return getTgaTsRivalIndiciz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalIndicizObj(AfDecimal tgaTsRivalIndicizObj) {
        if (tgaTsRivalIndicizObj != null) {
            setTgaTsRivalIndiciz(new AfDecimal(tgaTsRivalIndicizObj, 14, 9));
            ws.getIndTrchDiGar().setTsRivalIndiciz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTsRivalIndiciz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaTsRivalNet() {
        return ws.getTrchDiGar().getTgaTsRivalNet().getTgaTsRivalNet();
    }

    @Override
    public void setTgaTsRivalNet(AfDecimal tgaTsRivalNet) {
        ws.getTrchDiGar().getTgaTsRivalNet().setTgaTsRivalNet(tgaTsRivalNet.copy());
    }

    @Override
    public AfDecimal getTgaTsRivalNetObj() {
        if (ws.getIndTrchDiGar().getTsRivalNet() >= 0) {
            return getTgaTsRivalNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaTsRivalNetObj(AfDecimal tgaTsRivalNetObj) {
        if (tgaTsRivalNetObj != null) {
            setTgaTsRivalNet(new AfDecimal(tgaTsRivalNetObj, 14, 9));
            ws.getIndTrchDiGar().setTsRivalNet(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setTsRivalNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaVisEnd2000() {
        return ws.getTrchDiGar().getTgaVisEnd2000().getTgaVisEnd2000();
    }

    @Override
    public void setTgaVisEnd2000(AfDecimal tgaVisEnd2000) {
        ws.getTrchDiGar().getTgaVisEnd2000().setTgaVisEnd2000(tgaVisEnd2000.copy());
    }

    @Override
    public AfDecimal getTgaVisEnd2000Nforz() {
        return ws.getTrchDiGar().getTgaVisEnd2000Nforz().getTgaVisEnd2000Nforz();
    }

    @Override
    public void setTgaVisEnd2000Nforz(AfDecimal tgaVisEnd2000Nforz) {
        ws.getTrchDiGar().getTgaVisEnd2000Nforz().setTgaVisEnd2000Nforz(tgaVisEnd2000Nforz.copy());
    }

    @Override
    public AfDecimal getTgaVisEnd2000NforzObj() {
        if (ws.getIndTrchDiGar().getVisEnd2000Nforz() >= 0) {
            return getTgaVisEnd2000Nforz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaVisEnd2000NforzObj(AfDecimal tgaVisEnd2000NforzObj) {
        if (tgaVisEnd2000NforzObj != null) {
            setTgaVisEnd2000Nforz(new AfDecimal(tgaVisEnd2000NforzObj, 15, 3));
            ws.getIndTrchDiGar().setVisEnd2000Nforz(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setVisEnd2000Nforz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTgaVisEnd2000Obj() {
        if (ws.getIndTrchDiGar().getVisEnd2000() >= 0) {
            return getTgaVisEnd2000();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTgaVisEnd2000Obj(AfDecimal tgaVisEnd2000Obj) {
        if (tgaVisEnd2000Obj != null) {
            setTgaVisEnd2000(new AfDecimal(tgaVisEnd2000Obj, 15, 3));
            ws.getIndTrchDiGar().setVisEnd2000(((short)0));
        }
        else {
            ws.getIndTrchDiGar().setVisEnd2000(((short)-1));
        }
    }

    @Override
    public int getWkIdAdesA() {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public void setWkIdAdesA(int wkIdAdesA) {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public int getWkIdAdesDa() {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public void setWkIdAdesDa(int wkIdAdesDa) {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public int getWkIdGarA() {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public void setWkIdGarA(int wkIdGarA) {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public int getWkIdGarDa() {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public void setWkIdGarDa(int wkIdGarDa) {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }
}
