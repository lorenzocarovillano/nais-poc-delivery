package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.program.StringParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idss0010Data;
import it.accenture.jnais.ws.InIdss0010;
import it.accenture.jnais.ws.OutIdss0010;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import javax.inject.Inject;

/**Original name: IDSS0010<br>
 * <pre>**************************************************************
 *                     PGM :  IDSS0010                          *
 *                    COPY :  IDSI0011 IDSO0011                 *
 * **************************************************************
 * AUTHOR.  ATS.
 * **************************************************************
 * **************************************************************
 * **************************************************************</pre>*/
public class Idss0010 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Idss0010Data ws = new Idss0010Data();
    //Original name: IN-IDSS0010
    private InIdss0010 inIdss0010;
    //Original name: OUT-IDSS0010
    private OutIdss0010 outIdss0010;

    //==== METHODS ====
    /**Original name: MAIN_FIRST_SENTENCES<br>
	 * <pre>    SET ADDRESS OF IN-OUT-IDSS0010   TO WS-ADDRESS-DIS</pre>*/
    public long execute(InIdss0010 inIdss0010, OutIdss0010 outIdss0010) {
        this.inIdss0010 = inIdss0010;
        this.outIdss0010 = outIdss0010;
        // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
        ws.getIdsv8888().getLivelloDebug().setStressTestDbg();
        // COB_CODE: SET  IDSV8888-SONDA-S9           TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setSondaS9();
        // COB_CODE: MOVE IDSI0011-MODALITA-ESECUTIVA
        //                TO IDSV8888-MODALITA-ESECUTIVA
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(this.inIdss0010.getIdsi0011ModalitaEsecutiva().getIdsi0011ModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
        ws.getIdsv8888().getStrPerformanceDbg().setUserName(this.inIdss0010.getIdsi0011UserName());
        // COB_CODE: MOVE IDSI0011-CODICE-STR-DATO     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm(this.inIdss0010.getIdsi0011CodiceStrDato());
        // COB_CODE: MOVE 'Data Dispatcher'           TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Data Dispatcher");
        // COB_CODE: SET  IDSV8888-INIZIO             TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: PERFORM A000-INIZIALIZZA-OUTPUT
        a000InizializzaOutput();
        // COB_CODE: PERFORM A005-CONTROLLO-INPUT
        a005ControlloInput();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.outIdss0010.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM S2000-CERCA-NOME
            //              THRU EX-S2000
            s2000CercaNome();
            // COB_CODE: IF PGM-TROVATO
            //                 THRU EX-S3000
            //           END-IF
            if (ws.getSwPgmTrovato().isTrovato()) {
                // COB_CODE: PERFORM S5000-VALOR-AREA
                //              THRU EX-S5000
                s5000ValorArea();
                // COB_CODE: PERFORM S3000-CALL-PGM
                //              THRU EX-S3000
                s3000CallPgm();
            }
        }
        // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
        ws.getIdsv8888().getLivelloDebug().setStressTestDbg();
        // COB_CODE: SET  IDSV8888-SONDA-S9           TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setSondaS9();
        // COB_CODE: MOVE IDSI0011-MODALITA-ESECUTIVA
        //                TO IDSV8888-MODALITA-ESECUTIVA
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(this.inIdss0010.getIdsi0011ModalitaEsecutiva().getIdsi0011ModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0011-USER-NAME          TO IDSV8888-USER-NAME
        ws.getIdsv8888().getStrPerformanceDbg().setUserName(this.inIdss0010.getIdsi0011UserName());
        // COB_CODE: MOVE IDSI0011-CODICE-STR-DATO     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm(this.inIdss0010.getIdsi0011CodiceStrDato());
        // COB_CODE: MOVE 'Data Dispatcher'           TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Data Dispatcher");
        // COB_CODE: SET  IDSV8888-FINE               TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
        eseguiDisplay();
        // COB_CODE: PERFORM 4000-FINE.
        fine();
        return 0;
    }

    public static Idss0010 getInstance() {
        return ((Idss0010)Programs.getInstance(Idss0010.class));
    }

    /**Original name: A000-INIZIALIZZA-OUTPUT_FIRST_SENTENCES<br>*/
    private void a000InizializzaOutput() {
        // COB_CODE: INITIALIZE WS-NOME-PROG.
        ws.setWsNomeProg("");
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC  TO TRUE
        outIdss0010.getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL TO TRUE
        outIdss0010.getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: MOVE SPACES                TO IDSO0011-DESCRIZ-ERR-DB2.
        outIdss0010.getCampiEsito().setDescrizErrDb2("");
    }

    /**Original name: A005-CONTROLLO-INPUT_FIRST_SENTENCES<br>*/
    private void a005ControlloInput() {
        // COB_CODE: IF IDSI0011-CODICE-COMPAGNIA-ANIA = 0
        //              SET IDSO0011-COD-COMP-NOT-VALID TO TRUE
        //           END-IF.
        if (inIdss0010.getIdsi0011CodiceCompagniaAnia() == 0) {
            // COB_CODE: SET IDSO0011-COD-COMP-NOT-VALID TO TRUE
            outIdss0010.getReturnCode().setCodCompNotValid();
        }
        // COB_CODE: IF IDSI0011-OPERAZIONE      = SPACES
        //              SET IDSO0011-OPER-NOT-V TO TRUE
        //           END-IF.
        if (Characters.EQ_SPACE.test(inIdss0010.getIdsi0011Operazione().getOperazione())) {
            // COB_CODE: SET IDSO0011-OPER-NOT-V TO TRUE
            outIdss0010.getReturnCode().setOperNotV();
        }
        // COB_CODE: IF IDSI0011-CODICE-STR-DATO = SPACES
        //              SET IDSO0011-STR-DATO-NOT-VALID TO TRUE
        //           END-IF.
        if (Characters.EQ_SPACE.test(inIdss0010.getIdsi0011CodiceStrDato())) {
            // COB_CODE: SET IDSO0011-STR-DATO-NOT-VALID TO TRUE
            outIdss0010.getReturnCode().setStrDatoNotValid();
        }
    }

    /**Original name: S2000-CERCA-NOME_FIRST_SENTENCES<br>*/
    private void s2000CercaNome() {
        // COB_CODE: SET PGM-NO-TROVATO TO TRUE.
        ws.getSwPgmTrovato().setNoTrovato();
        // COB_CODE: IF IDSI0011-CODICE-STR-DATO(1:4) = 'LDBS' OR 'IDES'
        //               SET  PGM-TROVATO                     TO TRUE
        //           ELSE
        //               END-SEARCH
        //           END-IF.
        if (Conditions.eq(inIdss0010.getIdsi0011CodiceStrDatoFormatted().substring((1) - 1, 4), "LDBS") || Conditions.eq(inIdss0010.getIdsi0011CodiceStrDatoFormatted().substring((1) - 1, 4), "IDES")) {
            // COB_CODE: MOVE IDSI0011-CODICE-STR-DATO        TO WS-NOME-PROG
            ws.setWsNomeProg(inIdss0010.getIdsi0011CodiceStrDato());
            // COB_CODE: SET  PGM-TROVATO                     TO TRUE
            ws.getSwPgmTrovato().setTrovato();
        }
        else if (searchIabv0901EleTabCallPgmFromS2000CercaNome() != -1) {
            // COB_CODE: SEARCH ALL IABV0901-ELE-TAB-CALL-PGM
            //                 AT END
            //                       PERFORM S4000-SCRIVI-ERRORE
            //                          THRU EX-S4000
            //                 WHEN IABV0901-COD-STR-DATO(IABV0901-INDEX) =
            //                      IDSI0011-CODICE-STR-DATO
            //                      SET PGM-TROVATO           TO TRUE
            //                          THRU EX-S4000
            //                 WHEN IABV0901-COD-STR-DATO(IABV0901-INDEX) =
            //                      IDSI0011-CODICE-STR-DATO
            //                      MOVE IABV0901-NOME-PGM(IABV0901-INDEX)
            //                        TO WS-NOME-PROG
            //                      SET PGM-TROVATO           TO TRUE
            //           END-SEARCH
            // COB_CODE: MOVE IABV0901-NOME-PGM(IABV0901-INDEX)
            //             TO WS-NOME-PROG
            ws.setWsNomeProg(ws.getIabv0901TabCallPgm().getNomePgm(ws.getIabv0901Index()));
            // COB_CODE: SET PGM-TROVATO           TO TRUE
            ws.getSwPgmTrovato().setTrovato();
            //AT END GROUP;
        }
        else {
            // COB_CODE: PERFORM S4000-SCRIVI-ERRORE
            //              THRU EX-S4000
            s4000ScriviErrore();
        }
    }

    /**Original name: S3000-CALL-PGM_FIRST_SENTENCES<br>*/
    private void s3000CallPgm() {
        StringParam idsi0011BufferDati = null;
        // COB_CODE: CALL WS-NOME-PROG           USING IDSV0003
        //                                             IDSI0011-BUFFER-DATI.
        idsi0011BufferDati = new StringParam(inIdss0010.getIdsi0011BufferDati(), InIdss0010.Len.IDSI0011_BUFFER_DATI);
        DynamicCall.invoke(ws.getWsNomeProg(), ws.getIdsv0003(), idsi0011BufferDati);
        inIdss0010.setIdsi0011BufferDati(idsi0011BufferDati.getString());
        // COB_CODE: MOVE IDSV0003-SQLCODE          TO IDSO0011-SQLCODE
        //                                             IDSO0011-SQLCODE-SIGNED.
        outIdss0010.setSqlcode(ws.getIdsv0003().getSqlcode().getSqlcode());
        outIdss0010.getSqlcodeSigned().setSqlcodeSigned(ws.getIdsv0003().getSqlcode().getSqlcode());
        // COB_CODE: MOVE IDSV0003-RETURN-CODE      TO IDSO0011-RETURN-CODE.
        outIdss0010.getReturnCode().setReturnCode(ws.getIdsv0003().getReturnCode().getReturnCode());
        // COB_CODE: MOVE IDSV0003-DESCRIZ-ERR-DB2  TO IDSO0011-DESCRIZ-ERR-DB2.
        outIdss0010.getCampiEsito().setDescrizErrDb2(ws.getIdsv0003().getCampiEsito().getDescrizErrDb2());
        // COB_CODE: MOVE IDSV0003-CAMPI-ESITO      TO IDSO0011-CAMPI-ESITO.
        outIdss0010.getCampiEsito().setCampiEsitoBytes(ws.getIdsv0003().getCampiEsito().getCampiEsitoBytes());
    }

    /**Original name: S4000-SCRIVI-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ERRORE PER NON AVER TROVATO PROGRAMMA SU COPY
	 * ----------------------------------------------------------------*</pre>*/
    private void s4000ScriviErrore() {
        // COB_CODE: MOVE 'SERVIZIO NON TROVATO'     TO IDSV0003-DESCRIZ-ERR-DB2.
        ws.getIdsv0003().getCampiEsito().setDescrizErrDb2("SERVIZIO NON TROVATO");
        // COB_CODE: SET IDSV0003-SERV-NOT-F-ON-MSS  TO TRUE.
        ws.getIdsv0003().getReturnCode().setIdsv0003ServNotFOnMss();
        //    MOVE IDSV0003-SQLCODE           TO IDSO0011-SQLCODE
        //                                       IDSO0011-SQLCODE-SIGNED.
        // COB_CODE: MOVE IDSV0003-RETURN-CODE       TO IDSO0011-RETURN-CODE.
        outIdss0010.getReturnCode().setReturnCode(ws.getIdsv0003().getReturnCode().getReturnCode());
        // COB_CODE: MOVE IDSV0003-DESCRIZ-ERR-DB2   TO IDSO0011-DESCRIZ-ERR-DB2.
        outIdss0010.getCampiEsito().setDescrizErrDb2(ws.getIdsv0003().getCampiEsito().getDescrizErrDb2());
        // COB_CODE: MOVE IDSV0003-CAMPI-ESITO       TO IDSO0011-CAMPI-ESITO.
        outIdss0010.getCampiEsito().setCampiEsitoBytes(ws.getIdsv0003().getCampiEsito().getCampiEsitoBytes());
    }

    /**Original name: S5000-VALOR-AREA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA
	 * ----------------------------------------------------------------*</pre>*/
    private void s5000ValorArea() {
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC  TO TRUE
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
        ws.getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: INITIALIZE   IDSV0003-TIPOLOGIA-OPERAZIONE
        //                        IDSV0003-CAMPI-ESITO
        ws.getIdsv0003().getTipologiaOperazione().setTipologiaOperazione("");
        initCampiEsito();
        // COB_CODE: MOVE IDSI0011-MODALITA-ESECUTIVA
        //                         TO IDSV0003-MODALITA-ESECUTIVA
        ws.getIdsv0003().getModalitaEsecutiva().setModalitaEsecutiva(inIdss0010.getIdsi0011ModalitaEsecutiva().getIdsi0011ModalitaEsecutiva());
        // COB_CODE: MOVE IDSI0011-CODICE-COMPAGNIA-ANIA
        //                         TO IDSV0003-CODICE-COMPAGNIA-ANIA
        ws.getIdsv0003().setCodiceCompagniaAnia(inIdss0010.getIdsi0011CodiceCompagniaAnia());
        // COB_CODE: MOVE IDSI0011-COD-MAIN-BATCH
        //                         TO IDSV0003-COD-MAIN-BATCH
        ws.getIdsv0003().setCodMainBatch(inIdss0010.getIdsi0011CodMainBatch());
        // COB_CODE: MOVE IDSI0011-TIPO-MOVIMENTO
        //                         TO IDSV0003-TIPO-MOVIMENTO
        ws.getIdsv0003().setTipoMovimentoFormatted(inIdss0010.getIdsi0011TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSI0011-SESSIONE
        //                         TO IDSV0003-SESSIONE
        ws.getIdsv0003().setSessione(inIdss0010.getIdsi0011Sessione());
        // COB_CODE: MOVE IDSI0011-USER-NAME
        //                         TO IDSV0003-USER-NAME
        ws.getIdsv0003().setUserName(inIdss0010.getIdsi0011UserName());
        // COB_CODE: MOVE IDSI0011-DATA-INIZIO-EFFETTO
        //                         TO IDSV0003-DATA-INIZIO-EFFETTO
        ws.getIdsv0003().setDataInizioEffetto(inIdss0010.getIdsi0011DataInizioEffetto());
        // COB_CODE: MOVE IDSI0011-DATA-FINE-EFFETTO
        //                         TO IDSV0003-DATA-FINE-EFFETTO
        ws.getIdsv0003().setDataFineEffetto(inIdss0010.getIdsi0011DataFineEffetto());
        // COB_CODE: MOVE IDSI0011-DATA-COMPETENZA
        //                         TO IDSV0003-DATA-COMPETENZA
        ws.getIdsv0003().setDataCompetenza(inIdss0010.getIdsi0011DataCompetenza());
        // COB_CODE: MOVE IDSI0011-DATA-COMP-AGG-STOR
        //                         TO IDSV0003-DATA-COMP-AGG-STOR
        ws.getIdsv0003().setDataCompAggStor(inIdss0010.getIdsi0011DataCompAggStor());
        // COB_CODE: MOVE IDSI0011-TRATTAMENTO-STORICITA
        //                         TO IDSV0003-TRATTAMENTO-STORICITA
        ws.getIdsv0003().getTrattamentoStoricita().setTrattamentoStoricita(inIdss0010.getIdsi0011TrattamentoStoricita().getTrattamentoStoricita());
        // COB_CODE: MOVE IDSI0011-FORMATO-DATA-DB
        //                         TO IDSV0003-FORMATO-DATA-DB
        ws.getIdsv0003().getFormatoDataDb().setFormatoDataDb(inIdss0010.getIdsi0011FormatoDataDb().getIdsi0011FormatoDataDb());
        // COB_CODE: MOVE IDSI0011-ID-MOVI-ANNULLATO
        //                         TO IDSV0003-ID-MOVI-ANNULLATO
        ws.getIdsv0003().setIdMoviAnnullatoFormatted(inIdss0010.getIdsi0011IdMoviAnnullatoFormatted());
        // COB_CODE: MOVE IDSI0011-IDENTITA-CHIAMANTE
        //                         TO IDSV0003-IDENTITA-CHIAMANTE
        ws.getIdsv0003().getIdentitaChiamante().setIdentitaChiamante(inIdss0010.getIdsi0011IdentitaChiamante().getIdsi0011IdentitaChiamante());
        // COB_CODE: MOVE IDSI0011-LIVELLO-OPERAZIONE
        //                         TO IDSV0003-LIVELLO-OPERAZIONE
        ws.getIdsv0003().getLivelloOperazione().setLivelloOperazione(inIdss0010.getIdsi0011LivelloOperazione().getLivelloOperazione());
        // COB_CODE: MOVE IDSI0011-OPERAZIONE
        //                         TO IDSV0003-OPERAZIONE
        ws.getIdsv0003().getOperazione().setOperazione(inIdss0010.getIdsi0011Operazione().getOperazione());
        // COB_CODE: MOVE IDSI0011-FLAG-CODA-TS
        //                         TO IDSV0003-FLAG-CODA-TS
        ws.getIdsv0003().getFlagCodaTs().setFlagCodaTs(inIdss0010.getIdsi0011FlagCodaTs().getIdsi0011FlagCodaTs());
        // COB_CODE: MOVE IDSI0011-BUFFER-WHERE-COND
        //                         TO IDSV0003-BUFFER-WHERE-COND.
        ws.getIdsv0003().setBufferWhereCond(inIdss0010.getIdsi0011BufferWhereCond());
    }

    /**Original name: 4000-FINE_FIRST_SENTENCES<br>*/
    private void fine() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IDSI0011-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (inIdss0010.getIdsi0011LivelloDebug().isAnyTuningDbg() && ws.getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IDSI0011-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (inIdss0010.getIdsi0011LivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
            else if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() == inIdss0010.getIdsi0011LivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IDSI0011-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
        }
        else if (inIdss0010.getIdsi0011LivelloDebug().isAnyApplDbg() && ws.getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IDSI0011-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IDSI0011-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() <= inIdss0010.getIdsi0011LivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //               USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIdsv8888().getLivelloDebug().setNoDebug();
    }

    private int searchIabv0901EleTabCallPgmFromS2000CercaNome() {
        int last = 471, first = 1;
        while (first <= last) {
            ws.setIabv0901Index((first + last) / 2);
            if (Conditions.lt(ws.getIabv0901TabCallPgm().getCodStrDato(ws.getIabv0901Index()), inIdss0010.getIdsi0011CodiceStrDato())) {
                first = ws.getIabv0901Index() + 1;
            }
            else if (Conditions.gt(ws.getIabv0901TabCallPgm().getCodStrDato(ws.getIabv0901Index()), inIdss0010.getIdsi0011CodiceStrDato())) {
                last = ws.getIabv0901Index() - 1;
            }
            else {
                return ws.getIabv0901Index();
            }
        }
        return -1;
    }

    public void initCampiEsito() {
        ws.getIdsv0003().getCampiEsito().setDescrizErrDb2("");
        ws.getIdsv0003().getCampiEsito().setCodServizioBe("");
        ws.getIdsv0003().getCampiEsito().setNomeTabella("");
        ws.getIdsv0003().getCampiEsito().setKeyTabella("");
        ws.getIdsv0003().getCampiEsito().setIdsv0003NumRigheLetteFormatted("00");
    }

    public void initStrPerformanceDbg() {
        ws.getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIdsv8888().getStrPerformanceDbg().setUserName("");
    }
}
