package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.MatrValVarDao;
import it.accenture.jnais.commons.data.to.IMatrValVar;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs1400Data;
import it.accenture.jnais.ws.MatrValVarLdbs1400;
import it.accenture.jnais.ws.redefines.MvvIdpMatrValVar;
import it.accenture.jnais.ws.redefines.MvvTipoMovimento;

/**Original name: LDBS1400<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  NOVEMBRE 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs1400 extends Program implements IMatrValVar {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private MatrValVarDao matrValVarDao = new MatrValVarDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs1400Data ws = new Ldbs1400Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: MATR-VAL-VAR
    private MatrValVarLdbs1400 matrValVar;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS1400_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, MatrValVarLdbs1400 matrValVar) {
        this.idsv0003 = idsv0003;
        this.matrValVar = matrValVar;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A200-ELABORA                THRU A200-EX
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A200-ELABORA                THRU A200-EX
            a200Elabora();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs1400 getInstance() {
        return ((Ldbs1400)Programs.getInstance(Ldbs1400.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS1400'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS1400");
        // COB_CODE: MOVE 'MATR_VAL_VAR' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("MATR_VAL_VAR");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN -811
            //                             CONTINUE
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else if (idsv0003.getSqlcode().getSqlcode() == -811) {
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200Elabora() {
        // COB_CODE: PERFORM A210-SELECT             THRU A210-EX.
        a210Select();
    }

    /**Original name: A210-SELECT<br>
	 * <pre>----
	 * ----  gestione SELECT
	 * ----</pre>*/
    private void a210Select() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_MATR_VAL_VAR
        //                ,IDP_MATR_VAL_VAR
        //                ,COD_COMPAGNIA_ANIA
        //                ,TIPO_MOVIMENTO
        //                ,COD_DATO_EXT
        //                ,OBBLIGATORIETA
        //                ,VALORE_DEFAULT
        //                ,COD_STR_DATO_PTF
        //                ,COD_DATO_PTF
        //                ,COD_PARAMETRO
        //                ,OPERAZIONE
        //                ,LIVELLO_OPERAZIONE
        //                ,TIPO_OGGETTO
        //                ,WHERE_CONDITION
        //                ,SERVIZIO_LETTURA
        //                ,MODULO_CALCOLO
        //                ,STEP_VALORIZZATORE
        //                ,STEP_CONVERSAZIONE
        //                ,OPER_LOG_STATO_BUS
        //                ,ARRAY_STATO_BUS
        //                ,OPER_LOG_CAUSALE
        //                ,ARRAY_CAUSALE
        //             INTO
        //                :MVV-ID-MATR-VAL-VAR
        //               ,:MVV-IDP-MATR-VAL-VAR
        //                :IND-MVV-IDP-MATR-VAL-VAR
        //               ,:MVV-COD-COMPAGNIA-ANIA
        //               ,:MVV-TIPO-MOVIMENTO
        //                :IND-MVV-TIPO-MOVIMENTO
        //               ,:MVV-COD-DATO-EXT
        //                :IND-MVV-COD-DATO-EXT
        //               ,:MVV-OBBLIGATORIETA
        //                :IND-MVV-OBBLIGATORIETA
        //               ,:MVV-VALORE-DEFAULT
        //                :IND-MVV-VALORE-DEFAULT
        //               ,:MVV-COD-STR-DATO-PTF
        //                :IND-MVV-COD-STR-DATO-PTF
        //               ,:MVV-COD-DATO-PTF
        //                :IND-MVV-COD-DATO-PTF
        //               ,:MVV-COD-PARAMETRO
        //                :IND-MVV-COD-PARAMETRO
        //               ,:MVV-OPERAZIONE
        //                :IND-MVV-OPERAZIONE
        //               ,:MVV-LIVELLO-OPERAZIONE
        //                :IND-MVV-LIVELLO-OPERAZIONE
        //               ,:MVV-TIPO-OGGETTO
        //                :IND-MVV-TIPO-OGGETTO
        //               ,:MVV-WHERE-CONDITION-VCHAR
        //                :IND-MVV-WHERE-CONDITION
        //               ,:MVV-SERVIZIO-LETTURA
        //                :IND-MVV-SERVIZIO-LETTURA
        //               ,:MVV-MODULO-CALCOLO
        //                :IND-MVV-MODULO-CALCOLO
        //               ,:MVV-STEP-VALORIZZATORE
        //                :IND-MVV-STEP-VALORIZZATORE
        //               ,:MVV-STEP-CONVERSAZIONE
        //                :IND-MVV-STEP-CONVERSAZIONE
        //               ,:MVV-OPER-LOG-STATO-BUS
        //                :IND-MVV-OPER-LOG-STATO-BUS
        //               ,:MVV-ARRAY-STATO-BUS
        //                :IND-MVV-ARRAY-STATO-BUS
        //               ,:MVV-OPER-LOG-CAUSALE
        //                :IND-MVV-OPER-LOG-CAUSALE
        //               ,:MVV-ARRAY-CAUSALE
        //                :IND-MVV-ARRAY-CAUSALE
        //             FROM  MATR_VAL_VAR
        //             WHERE COD_COMPAGNIA_ANIA = :MVV-COD-COMPAGNIA-ANIA
        //             AND   TIPO_MOVIMENTO     = :MVV-TIPO-MOVIMENTO
        //             AND   STEP_VALORIZZATORE = :MVV-STEP-VALORIZZATORE
        //             AND   STEP_CONVERSAZIONE = :MVV-STEP-CONVERSAZIONE
        //           END-EXEC.
        matrValVarDao.selectRec(matrValVar.getMvvCodCompagniaAnia(), matrValVar.getMvvTipoMovimento().getMvvTipoMovimento(), matrValVar.getMvvStepValorizzatore(), matrValVar.getMvvStepConversazione(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-MVV-IDP-MATR-VAL-VAR = -1
        //              MOVE HIGH-VALUES TO MVV-IDP-MATR-VAL-VAR-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getIdpMatrValVar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-IDP-MATR-VAL-VAR-NULL
            matrValVar.getMvvIdpMatrValVar().setMvvIdpMatrValVarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MvvIdpMatrValVar.Len.MVV_IDP_MATR_VAL_VAR_NULL));
        }
        // COB_CODE: IF IND-MVV-TIPO-MOVIMENTO = -1
        //              MOVE HIGH-VALUES TO MVV-TIPO-MOVIMENTO-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getTipoMovimento() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-TIPO-MOVIMENTO-NULL
            matrValVar.getMvvTipoMovimento().setMvvTipoMovimentoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MvvTipoMovimento.Len.MVV_TIPO_MOVIMENTO_NULL));
        }
        // COB_CODE: IF IND-MVV-COD-DATO-EXT = -1
        //              MOVE HIGH-VALUES TO MVV-COD-DATO-EXT
        //           END-IF
        if (ws.getIndMatrValVar().getCodDatoExt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-COD-DATO-EXT
            matrValVar.setMvvCodDatoExt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_COD_DATO_EXT));
        }
        // COB_CODE: IF IND-MVV-OBBLIGATORIETA = -1
        //              MOVE HIGH-VALUES TO MVV-OBBLIGATORIETA-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getObbligatorieta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-OBBLIGATORIETA-NULL
            matrValVar.setMvvObbligatorieta(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-MVV-VALORE-DEFAULT = -1
        //              MOVE HIGH-VALUES TO MVV-VALORE-DEFAULT
        //           END-IF
        if (ws.getIndMatrValVar().getValoreDefault() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-VALORE-DEFAULT
            matrValVar.setMvvValoreDefault(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_VALORE_DEFAULT));
        }
        // COB_CODE: IF IND-MVV-COD-STR-DATO-PTF = -1
        //              MOVE HIGH-VALUES TO MVV-COD-STR-DATO-PTF
        //           END-IF
        if (ws.getIndMatrValVar().getCodStrDatoPtf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-COD-STR-DATO-PTF
            matrValVar.setMvvCodStrDatoPtf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_COD_STR_DATO_PTF));
        }
        // COB_CODE: IF IND-MVV-COD-DATO-PTF = -1
        //              MOVE HIGH-VALUES TO MVV-COD-DATO-PTF
        //           END-IF
        if (ws.getIndMatrValVar().getCodDatoPtf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-COD-DATO-PTF
            matrValVar.setMvvCodDatoPtf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_COD_DATO_PTF));
        }
        // COB_CODE: IF IND-MVV-COD-PARAMETRO = -1
        //              MOVE HIGH-VALUES TO MVV-COD-PARAMETRO
        //           END-IF
        if (ws.getIndMatrValVar().getCodParametro() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-COD-PARAMETRO
            matrValVar.setMvvCodParametro(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_COD_PARAMETRO));
        }
        // COB_CODE: IF IND-MVV-OPERAZIONE = -1
        //              MOVE HIGH-VALUES TO MVV-OPERAZIONE
        //           END-IF
        if (ws.getIndMatrValVar().getOperazione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-OPERAZIONE
            matrValVar.setMvvOperazione(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_OPERAZIONE));
        }
        // COB_CODE: IF IND-MVV-LIVELLO-OPERAZIONE = -1
        //              MOVE HIGH-VALUES TO MVV-LIVELLO-OPERAZIONE-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getLivelloOperazione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-LIVELLO-OPERAZIONE-NULL
            matrValVar.setMvvLivelloOperazione(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_LIVELLO_OPERAZIONE));
        }
        // COB_CODE: IF IND-MVV-TIPO-OGGETTO = -1
        //              MOVE HIGH-VALUES TO MVV-TIPO-OGGETTO-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getTipoOggetto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-TIPO-OGGETTO-NULL
            matrValVar.setMvvTipoOggetto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_TIPO_OGGETTO));
        }
        // COB_CODE: IF IND-MVV-WHERE-CONDITION = -1
        //              MOVE HIGH-VALUES TO MVV-WHERE-CONDITION
        //           END-IF
        if (ws.getIndMatrValVar().getWhereCondition() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-WHERE-CONDITION
            matrValVar.setMvvWhereCondition(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_WHERE_CONDITION));
        }
        // COB_CODE: IF IND-MVV-SERVIZIO-LETTURA = -1
        //              MOVE HIGH-VALUES TO MVV-SERVIZIO-LETTURA-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getServizioLettura() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-SERVIZIO-LETTURA-NULL
            matrValVar.setMvvServizioLettura(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_SERVIZIO_LETTURA));
        }
        // COB_CODE: IF IND-MVV-MODULO-CALCOLO = -1
        //              MOVE HIGH-VALUES TO MVV-MODULO-CALCOLO-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getModuloCalcolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-MODULO-CALCOLO-NULL
            matrValVar.setMvvModuloCalcolo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_MODULO_CALCOLO));
        }
        // COB_CODE: IF IND-MVV-STEP-VALORIZZATORE = -1
        //              MOVE HIGH-VALUES TO MVV-STEP-VALORIZZATORE-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getStepValorizzatore() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-STEP-VALORIZZATORE-NULL
            matrValVar.setMvvStepValorizzatore(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-MVV-STEP-CONVERSAZIONE = -1
        //              MOVE HIGH-VALUES TO MVV-STEP-CONVERSAZIONE-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getStepConversazione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-STEP-CONVERSAZIONE-NULL
            matrValVar.setMvvStepConversazione(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-MVV-OPER-LOG-STATO-BUS = -1
        //              MOVE HIGH-VALUES TO MVV-OPER-LOG-STATO-BUS-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getOperLogStatoBus() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-OPER-LOG-STATO-BUS-NULL
            matrValVar.setMvvOperLogStatoBus(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_OPER_LOG_STATO_BUS));
        }
        // COB_CODE: IF IND-MVV-ARRAY-STATO-BUS = -1
        //              MOVE HIGH-VALUES TO MVV-ARRAY-STATO-BUS
        //           END-IF
        if (ws.getIndMatrValVar().getArrayStatoBus() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-ARRAY-STATO-BUS
            matrValVar.setMvvArrayStatoBus(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_ARRAY_STATO_BUS));
        }
        // COB_CODE: IF IND-MVV-OPER-LOG-CAUSALE = -1
        //              MOVE HIGH-VALUES TO MVV-OPER-LOG-CAUSALE-NULL
        //           END-IF
        if (ws.getIndMatrValVar().getOperLogCausale() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-OPER-LOG-CAUSALE-NULL
            matrValVar.setMvvOperLogCausale(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_OPER_LOG_CAUSALE));
        }
        // COB_CODE: IF IND-MVV-ARRAY-CAUSALE = -1
        //              MOVE HIGH-VALUES TO MVV-ARRAY-CAUSALE
        //           END-IF.
        if (ws.getIndMatrValVar().getArrayCausale() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MVV-ARRAY-CAUSALE
            matrValVar.setMvvArrayCausale(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MatrValVarLdbs1400.Len.MVV_ARRAY_CAUSALE));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    @Override
    public String getArrayCausale() {
        return matrValVar.getMvvArrayCausale();
    }

    @Override
    public void setArrayCausale(String arrayCausale) {
        this.matrValVar.setMvvArrayCausale(arrayCausale);
    }

    @Override
    public String getArrayCausaleObj() {
        if (ws.getIndMatrValVar().getArrayCausale() >= 0) {
            return getArrayCausale();
        }
        else {
            return null;
        }
    }

    @Override
    public void setArrayCausaleObj(String arrayCausaleObj) {
        if (arrayCausaleObj != null) {
            setArrayCausale(arrayCausaleObj);
            ws.getIndMatrValVar().setArrayCausale(((short)0));
        }
        else {
            ws.getIndMatrValVar().setArrayCausale(((short)-1));
        }
    }

    @Override
    public String getArrayStatoBus() {
        return matrValVar.getMvvArrayStatoBus();
    }

    @Override
    public void setArrayStatoBus(String arrayStatoBus) {
        this.matrValVar.setMvvArrayStatoBus(arrayStatoBus);
    }

    @Override
    public String getArrayStatoBusObj() {
        if (ws.getIndMatrValVar().getArrayStatoBus() >= 0) {
            return getArrayStatoBus();
        }
        else {
            return null;
        }
    }

    @Override
    public void setArrayStatoBusObj(String arrayStatoBusObj) {
        if (arrayStatoBusObj != null) {
            setArrayStatoBus(arrayStatoBusObj);
            ws.getIndMatrValVar().setArrayStatoBus(((short)0));
        }
        else {
            ws.getIndMatrValVar().setArrayStatoBus(((short)-1));
        }
    }

    @Override
    public int getCodCompagniaAnia() {
        return matrValVar.getMvvCodCompagniaAnia();
    }

    @Override
    public void setCodCompagniaAnia(int codCompagniaAnia) {
        this.matrValVar.setMvvCodCompagniaAnia(codCompagniaAnia);
    }

    @Override
    public String getCodDatoExt() {
        return matrValVar.getMvvCodDatoExt();
    }

    @Override
    public void setCodDatoExt(String codDatoExt) {
        this.matrValVar.setMvvCodDatoExt(codDatoExt);
    }

    @Override
    public String getCodDatoExtObj() {
        if (ws.getIndMatrValVar().getCodDatoExt() >= 0) {
            return getCodDatoExt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDatoExtObj(String codDatoExtObj) {
        if (codDatoExtObj != null) {
            setCodDatoExt(codDatoExtObj);
            ws.getIndMatrValVar().setCodDatoExt(((short)0));
        }
        else {
            ws.getIndMatrValVar().setCodDatoExt(((short)-1));
        }
    }

    @Override
    public String getCodDatoPtf() {
        return matrValVar.getMvvCodDatoPtf();
    }

    @Override
    public void setCodDatoPtf(String codDatoPtf) {
        this.matrValVar.setMvvCodDatoPtf(codDatoPtf);
    }

    @Override
    public String getCodDatoPtfObj() {
        if (ws.getIndMatrValVar().getCodDatoPtf() >= 0) {
            return getCodDatoPtf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDatoPtfObj(String codDatoPtfObj) {
        if (codDatoPtfObj != null) {
            setCodDatoPtf(codDatoPtfObj);
            ws.getIndMatrValVar().setCodDatoPtf(((short)0));
        }
        else {
            ws.getIndMatrValVar().setCodDatoPtf(((short)-1));
        }
    }

    @Override
    public String getCodParametro() {
        return matrValVar.getMvvCodParametro();
    }

    @Override
    public void setCodParametro(String codParametro) {
        this.matrValVar.setMvvCodParametro(codParametro);
    }

    @Override
    public String getCodParametroObj() {
        if (ws.getIndMatrValVar().getCodParametro() >= 0) {
            return getCodParametro();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodParametroObj(String codParametroObj) {
        if (codParametroObj != null) {
            setCodParametro(codParametroObj);
            ws.getIndMatrValVar().setCodParametro(((short)0));
        }
        else {
            ws.getIndMatrValVar().setCodParametro(((short)-1));
        }
    }

    @Override
    public String getCodStrDatoPtf() {
        return matrValVar.getMvvCodStrDatoPtf();
    }

    @Override
    public void setCodStrDatoPtf(String codStrDatoPtf) {
        this.matrValVar.setMvvCodStrDatoPtf(codStrDatoPtf);
    }

    @Override
    public String getCodStrDatoPtfObj() {
        if (ws.getIndMatrValVar().getCodStrDatoPtf() >= 0) {
            return getCodStrDatoPtf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodStrDatoPtfObj(String codStrDatoPtfObj) {
        if (codStrDatoPtfObj != null) {
            setCodStrDatoPtf(codStrDatoPtfObj);
            ws.getIndMatrValVar().setCodStrDatoPtf(((short)0));
        }
        else {
            ws.getIndMatrValVar().setCodStrDatoPtf(((short)-1));
        }
    }

    @Override
    public int getIdMatrValVar() {
        return matrValVar.getMvvIdMatrValVar();
    }

    @Override
    public void setIdMatrValVar(int idMatrValVar) {
        this.matrValVar.setMvvIdMatrValVar(idMatrValVar);
    }

    @Override
    public int getIdpMatrValVar() {
        return matrValVar.getMvvIdpMatrValVar().getMvvIdpMatrValVar();
    }

    @Override
    public void setIdpMatrValVar(int idpMatrValVar) {
        this.matrValVar.getMvvIdpMatrValVar().setMvvIdpMatrValVar(idpMatrValVar);
    }

    @Override
    public Integer getIdpMatrValVarObj() {
        if (ws.getIndMatrValVar().getIdpMatrValVar() >= 0) {
            return ((Integer)getIdpMatrValVar());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdpMatrValVarObj(Integer idpMatrValVarObj) {
        if (idpMatrValVarObj != null) {
            setIdpMatrValVar(((int)idpMatrValVarObj));
            ws.getIndMatrValVar().setIdpMatrValVar(((short)0));
        }
        else {
            ws.getIndMatrValVar().setIdpMatrValVar(((short)-1));
        }
    }

    @Override
    public String getLivelloOperazione() {
        return matrValVar.getMvvLivelloOperazione();
    }

    @Override
    public void setLivelloOperazione(String livelloOperazione) {
        this.matrValVar.setMvvLivelloOperazione(livelloOperazione);
    }

    @Override
    public String getLivelloOperazioneObj() {
        if (ws.getIndMatrValVar().getLivelloOperazione() >= 0) {
            return getLivelloOperazione();
        }
        else {
            return null;
        }
    }

    @Override
    public void setLivelloOperazioneObj(String livelloOperazioneObj) {
        if (livelloOperazioneObj != null) {
            setLivelloOperazione(livelloOperazioneObj);
            ws.getIndMatrValVar().setLivelloOperazione(((short)0));
        }
        else {
            ws.getIndMatrValVar().setLivelloOperazione(((short)-1));
        }
    }

    @Override
    public String getModuloCalcolo() {
        return matrValVar.getMvvModuloCalcolo();
    }

    @Override
    public void setModuloCalcolo(String moduloCalcolo) {
        this.matrValVar.setMvvModuloCalcolo(moduloCalcolo);
    }

    @Override
    public String getModuloCalcoloObj() {
        if (ws.getIndMatrValVar().getModuloCalcolo() >= 0) {
            return getModuloCalcolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModuloCalcoloObj(String moduloCalcoloObj) {
        if (moduloCalcoloObj != null) {
            setModuloCalcolo(moduloCalcoloObj);
            ws.getIndMatrValVar().setModuloCalcolo(((short)0));
        }
        else {
            ws.getIndMatrValVar().setModuloCalcolo(((short)-1));
        }
    }

    @Override
    public char getObbligatorieta() {
        return matrValVar.getMvvObbligatorieta();
    }

    @Override
    public void setObbligatorieta(char obbligatorieta) {
        this.matrValVar.setMvvObbligatorieta(obbligatorieta);
    }

    @Override
    public Character getObbligatorietaObj() {
        if (ws.getIndMatrValVar().getObbligatorieta() >= 0) {
            return ((Character)getObbligatorieta());
        }
        else {
            return null;
        }
    }

    @Override
    public void setObbligatorietaObj(Character obbligatorietaObj) {
        if (obbligatorietaObj != null) {
            setObbligatorieta(((char)obbligatorietaObj));
            ws.getIndMatrValVar().setObbligatorieta(((short)0));
        }
        else {
            ws.getIndMatrValVar().setObbligatorieta(((short)-1));
        }
    }

    @Override
    public String getOperLogCausale() {
        return matrValVar.getMvvOperLogCausale();
    }

    @Override
    public void setOperLogCausale(String operLogCausale) {
        this.matrValVar.setMvvOperLogCausale(operLogCausale);
    }

    @Override
    public String getOperLogCausaleObj() {
        if (ws.getIndMatrValVar().getOperLogCausale() >= 0) {
            return getOperLogCausale();
        }
        else {
            return null;
        }
    }

    @Override
    public void setOperLogCausaleObj(String operLogCausaleObj) {
        if (operLogCausaleObj != null) {
            setOperLogCausale(operLogCausaleObj);
            ws.getIndMatrValVar().setOperLogCausale(((short)0));
        }
        else {
            ws.getIndMatrValVar().setOperLogCausale(((short)-1));
        }
    }

    @Override
    public String getOperLogStatoBus() {
        return matrValVar.getMvvOperLogStatoBus();
    }

    @Override
    public void setOperLogStatoBus(String operLogStatoBus) {
        this.matrValVar.setMvvOperLogStatoBus(operLogStatoBus);
    }

    @Override
    public String getOperLogStatoBusObj() {
        if (ws.getIndMatrValVar().getOperLogStatoBus() >= 0) {
            return getOperLogStatoBus();
        }
        else {
            return null;
        }
    }

    @Override
    public void setOperLogStatoBusObj(String operLogStatoBusObj) {
        if (operLogStatoBusObj != null) {
            setOperLogStatoBus(operLogStatoBusObj);
            ws.getIndMatrValVar().setOperLogStatoBus(((short)0));
        }
        else {
            ws.getIndMatrValVar().setOperLogStatoBus(((short)-1));
        }
    }

    @Override
    public String getOperazione() {
        return matrValVar.getMvvOperazione();
    }

    @Override
    public void setOperazione(String operazione) {
        this.matrValVar.setMvvOperazione(operazione);
    }

    @Override
    public String getOperazioneObj() {
        if (ws.getIndMatrValVar().getOperazione() >= 0) {
            return getOperazione();
        }
        else {
            return null;
        }
    }

    @Override
    public void setOperazioneObj(String operazioneObj) {
        if (operazioneObj != null) {
            setOperazione(operazioneObj);
            ws.getIndMatrValVar().setOperazione(((short)0));
        }
        else {
            ws.getIndMatrValVar().setOperazione(((short)-1));
        }
    }

    @Override
    public String getServizioLettura() {
        return matrValVar.getMvvServizioLettura();
    }

    @Override
    public void setServizioLettura(String servizioLettura) {
        this.matrValVar.setMvvServizioLettura(servizioLettura);
    }

    @Override
    public String getServizioLetturaObj() {
        if (ws.getIndMatrValVar().getServizioLettura() >= 0) {
            return getServizioLettura();
        }
        else {
            return null;
        }
    }

    @Override
    public void setServizioLetturaObj(String servizioLetturaObj) {
        if (servizioLetturaObj != null) {
            setServizioLettura(servizioLetturaObj);
            ws.getIndMatrValVar().setServizioLettura(((short)0));
        }
        else {
            ws.getIndMatrValVar().setServizioLettura(((short)-1));
        }
    }

    @Override
    public char getStepConversazione() {
        return matrValVar.getMvvStepConversazione();
    }

    @Override
    public void setStepConversazione(char stepConversazione) {
        this.matrValVar.setMvvStepConversazione(stepConversazione);
    }

    @Override
    public Character getStepConversazioneObj() {
        if (ws.getIndMatrValVar().getStepConversazione() >= 0) {
            return ((Character)getStepConversazione());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStepConversazioneObj(Character stepConversazioneObj) {
        if (stepConversazioneObj != null) {
            setStepConversazione(((char)stepConversazioneObj));
            ws.getIndMatrValVar().setStepConversazione(((short)0));
        }
        else {
            ws.getIndMatrValVar().setStepConversazione(((short)-1));
        }
    }

    @Override
    public char getStepValorizzatore() {
        return matrValVar.getMvvStepValorizzatore();
    }

    @Override
    public void setStepValorizzatore(char stepValorizzatore) {
        this.matrValVar.setMvvStepValorizzatore(stepValorizzatore);
    }

    @Override
    public Character getStepValorizzatoreObj() {
        if (ws.getIndMatrValVar().getStepValorizzatore() >= 0) {
            return ((Character)getStepValorizzatore());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStepValorizzatoreObj(Character stepValorizzatoreObj) {
        if (stepValorizzatoreObj != null) {
            setStepValorizzatore(((char)stepValorizzatoreObj));
            ws.getIndMatrValVar().setStepValorizzatore(((short)0));
        }
        else {
            ws.getIndMatrValVar().setStepValorizzatore(((short)-1));
        }
    }

    @Override
    public int getTipoMovimento() {
        return matrValVar.getMvvTipoMovimento().getMvvTipoMovimento();
    }

    @Override
    public void setTipoMovimento(int tipoMovimento) {
        this.matrValVar.getMvvTipoMovimento().setMvvTipoMovimento(tipoMovimento);
    }

    @Override
    public Integer getTipoMovimentoObj() {
        if (ws.getIndMatrValVar().getTipoMovimento() >= 0) {
            return ((Integer)getTipoMovimento());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTipoMovimentoObj(Integer tipoMovimentoObj) {
        if (tipoMovimentoObj != null) {
            setTipoMovimento(((int)tipoMovimentoObj));
            ws.getIndMatrValVar().setTipoMovimento(((short)0));
        }
        else {
            ws.getIndMatrValVar().setTipoMovimento(((short)-1));
        }
    }

    @Override
    public String getTipoOggetto() {
        return matrValVar.getMvvTipoOggetto();
    }

    @Override
    public void setTipoOggetto(String tipoOggetto) {
        this.matrValVar.setMvvTipoOggetto(tipoOggetto);
    }

    @Override
    public String getTipoOggettoObj() {
        if (ws.getIndMatrValVar().getTipoOggetto() >= 0) {
            return getTipoOggetto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTipoOggettoObj(String tipoOggettoObj) {
        if (tipoOggettoObj != null) {
            setTipoOggetto(tipoOggettoObj);
            ws.getIndMatrValVar().setTipoOggetto(((short)0));
        }
        else {
            ws.getIndMatrValVar().setTipoOggetto(((short)-1));
        }
    }

    @Override
    public String getValoreDefault() {
        return matrValVar.getMvvValoreDefault();
    }

    @Override
    public void setValoreDefault(String valoreDefault) {
        this.matrValVar.setMvvValoreDefault(valoreDefault);
    }

    @Override
    public String getValoreDefaultObj() {
        if (ws.getIndMatrValVar().getValoreDefault() >= 0) {
            return getValoreDefault();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValoreDefaultObj(String valoreDefaultObj) {
        if (valoreDefaultObj != null) {
            setValoreDefault(valoreDefaultObj);
            ws.getIndMatrValVar().setValoreDefault(((short)0));
        }
        else {
            ws.getIndMatrValVar().setValoreDefault(((short)-1));
        }
    }

    @Override
    public String getWhereConditionVchar() {
        return matrValVar.getMvvWhereConditionVcharFormatted();
    }

    @Override
    public void setWhereConditionVchar(String whereConditionVchar) {
        this.matrValVar.setMvvWhereConditionVcharFormatted(whereConditionVchar);
    }

    @Override
    public String getWhereConditionVcharObj() {
        if (ws.getIndMatrValVar().getWhereCondition() >= 0) {
            return getWhereConditionVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setWhereConditionVcharObj(String whereConditionVcharObj) {
        if (whereConditionVcharObj != null) {
            setWhereConditionVchar(whereConditionVcharObj);
            ws.getIndMatrValVar().setWhereCondition(((short)0));
        }
        else {
            ws.getIndMatrValVar().setWhereCondition(((short)-1));
        }
    }
}
