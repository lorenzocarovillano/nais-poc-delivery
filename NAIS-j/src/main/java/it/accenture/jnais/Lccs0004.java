package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Lccs0004Data;
import it.accenture.jnais.ws.Param;
import it.accenture.jnais.ws.redefines.XData;

/**Original name: LCCS0004<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *   'LIFE'   VERSIONE    01.00.00    DATA 12 GIUGNO 2007 ********
 * *                                                        ********
 * *****************************************************************
 * AUTHOR.            ATS.
 * DATE-WRITTEN.      12 GIUGNO 2007.
 * ****************************************************************
 *             F U N Z I O N I   D E L   P R O G R A M M A        *
 * ****************************************************************
 *              CONTROLLO E/O INVERSIONI DELLA DATA               *
 *                                                                *
 *                                                                *
 *       PARAM = 1 SOLO CONTROLLO DATA GGMMAAAA                   *
 *       PARAM = 2 CONTROLLO ED INVERSIONE DA GGMMAAAA            *
 *                                         IN AAAAMMGG            *
 *       PARAM = 3 INVERSIONE DA AAAAMMGG IN GGMMAAAA             *
 *       LA DATA PUO' ESSERE PASSATA ALLA ROUTINE O NELLA         *
 *       FORMA GGMMAAAA O VICEVERSA AAAAMMGG.                     *
 *       PARAM = 4 SOLO CONTROLLO DATA AAAAMMGG                   *
 *       PARAM = 0 IN OUTPUT IN CASO DI DATA VALIDA.              *
 * ****************************************************************
 *                                                                *
 *   INFINE,DOPO AVER AGGIORNATO L'AREA COMUNE,IL CONTROLLO       *
 *   VIENE CEDUTO AL PROGRAMMA CHIAMANTE.                         *
 *                                                                *
 * ****************************************************************
 *                                                                *
 *             R O U T I N E         B A T C H                    *
 *                                                                *
 *        C O N T R O L L O   I N V E R S I O N E   D A T A       *
 *                                                                *
 *             ( PROGRAMMA COBOL CHIAMATO CON UNA CALL)           *
 *                                                                *
 * ****************************************************************
 * DATE-COMPILED.</pre>*/
public class Lccs0004 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0004Data ws = new Lccs0004Data();
    //Original name: PARAM
    private Param param;
    //Original name: X-DATA
    private XData xData;

    //==== METHODS ====
    /**Original name: MAIN_SUBROUTINE<br>*/
    public long execute(Param param, XData xData) {
        this.param = param;
        this.xData = xData;
        String retcode = "";
        boolean gotoInversioneOpposta = false;
        retcode = inizio();
        if (!retcode.equals("INVERSIONE-OPPOSTA")) {
            controllo();
        }
        gotoInversioneOpposta = false;
        inversioneOpposta();
        controlloAaaammgg();
        return 0;
    }

    public static Lccs0004 getInstance() {
        return ((Lccs0004)Programs.getInstance(Lccs0004.class));
    }

    /**Original name: INIZIO<br>*/
    private String inizio() {
        // COB_CODE: IF X-DATANUM NOT NUMERIC OR
        //              X-AAAA EQUAL ZERO         GO TO FINE.
        if (!Functions.isNumber(xData.getxDatanumFormatted()) || Characters.EQ_ZERO.test(xData.getAaaaFormatted())) {
            // COB_CODE: X-AAAA EQUAL ZERO         GO TO FINE.
            fine();
        }
        // COB_CODE: GO TO   CONTROLLO
        //           DEPENDING ON  PARAM.
        //           DEPENDING ON  PARAM.
        if (param.getParam() == 1) {
            controllo();
        }
        else if (param.getParam() == 2) {
            controlloInversione();
        }
        else if (param.getParam() == 3) {
            return "INVERSIONE-OPPOSTA";
        }
        else if (param.getParam() == 4) {
            controlloAaaammgg();
        }
        return "";
    }

    /**Original name: CONTROLLO<br>*/
    private void controllo() {
        // COB_CODE: DIVIDE X-AAAA BY 4 GIVING RISULT REMAINDER RESTO.
        ws.setRisult(((short)(xData.getAaaa() / 4)));
        ws.setResto(((short)(xData.getAaaa() % 4)));
        // COB_CODE: IF RESTO = 0
        //              MOVE 29 TO T-GG (2)
        //           ELSE
        //              MOVE 28 TO T-GG (2).
        if (ws.getResto() == 0) {
            // COB_CODE: MOVE 29 TO T-GG (2)
            ws.getTabGiorni().settGg(2, ((short)29));
        }
        else {
            // COB_CODE: MOVE 28 TO T-GG (2).
            ws.getTabGiorni().settGg(2, ((short)28));
        }
        // COB_CODE: IF X-MM GREATER 12 OR X-MM LESS 1
        //                        OR X-GG GREATER T-GG (X-MM) OR X-GG LESS 1
        //           GO TO FINE
        //           GO TO FINE.
        if (xData.getMm() > 12 || xData.getMm() < 1 || xData.getGg() > ws.getTabGiorni().gettGg(xData.getMm()) || xData.getGg() < 1) {
            // COB_CODE: GO TO FINE
            fine();
        }
        else {
            // COB_CODE: ELSE         MOVE ZERO TO PARAM
            param.setParam(((short)0));
            // COB_CODE: GO TO FINE.
            fine();
        }
    }

    /**Original name: CONTROLLO-INVERSIONE<br>*/
    private void controlloInversione() {
        // COB_CODE: DIVIDE X-AAAA BY 4 GIVING RISULT REMAINDER RESTO.
        ws.setRisult(((short)(xData.getAaaa() / 4)));
        ws.setResto(((short)(xData.getAaaa() % 4)));
        // COB_CODE: IF RESTO = 0
        //              MOVE 29 TO T-GG (2)
        //           ELSE
        //              MOVE 28 TO T-GG (2).
        if (ws.getResto() == 0) {
            // COB_CODE: MOVE 29 TO T-GG (2)
            ws.getTabGiorni().settGg(2, ((short)29));
        }
        else {
            // COB_CODE: MOVE 28 TO T-GG (2).
            ws.getTabGiorni().settGg(2, ((short)28));
        }
        // COB_CODE: IF X-MM GREATER 12 OR X-MM LESS 1
        //                        OR X-GG GREATER T-GG (X-MM) OR X-GG LESS 1
        //               GO TO FINE
        //           ELSE
        //               MOVE ZERO     TO PARAM.
        if (xData.getMm() > 12 || xData.getMm() < 1 || xData.getGg() > ws.getTabGiorni().gettGg(xData.getMm()) || xData.getGg() < 1) {
            // COB_CODE: GO TO FINE
            fine();
        }
        else {
            // COB_CODE: MOVE X-GG     TO C-GG
            ws.getComData().setcGgFormatted(xData.getGgFormatted());
            // COB_CODE: MOVE X-MM     TO C-MM
            ws.getComData().setcMm(xData.getMm());
            // COB_CODE: MOVE X-AAAA   TO C-AAAA
            ws.getComData().getFillerComData().setcAaaaFormatted(xData.getAaaaFormatted());
            // COB_CODE: MOVE COM-DATA TO X-DATA
            xData.setxDataBytes(ws.getComData().getComDataBytes());
            // COB_CODE: MOVE ZERO     TO PARAM.
            param.setParam(((short)0));
        }
        // COB_CODE: GO TO FINE.
        fine();
    }

    /**Original name: INVERSIONE-OPPOSTA<br>*/
    private void inversioneOpposta() {
        // COB_CODE: MOVE    X-GG      TO C-MM.
        ws.getComData().setcMm(xData.getGg());
        // COB_CODE: MOVE    X-MM      TO C-GG.
        ws.getComData().setcGg(xData.getMm());
        // COB_CODE: MOVE    X-AA1     TO C-AA2.
        ws.getComData().getFillerComData().setAa2Formatted(xData.getAa1Formatted());
        // COB_CODE: MOVE    X-AA2     TO C-AA1.
        ws.getComData().getFillerComData().setAa1Formatted(xData.getAa2Formatted());
        // COB_CODE: MOVE    COM-DATA  TO X-DATA.
        xData.setxDataBytes(ws.getComData().getComDataBytes());
        // COB_CODE: MOVE    ZERO      TO PARAM.
        param.setParam(((short)0));
    }

    /**Original name: CONTROLLO-AAAAMMGG<br>*/
    private void controlloAaaammgg() {
        // COB_CODE: MOVE    X-DATA    TO COM-DATA.
        ws.getComData().setComDataBytes(xData.getxDataBytes());
        // COB_CODE: DIVIDE C-AAAA BY 4 GIVING RISULT REMAINDER RESTO.
        ws.setRisult(((short)(ws.getComData().getFillerComData().getcAaaa() / 4)));
        ws.setResto(((short)(ws.getComData().getFillerComData().getcAaaa() % 4)));
        // COB_CODE: IF RESTO = 0
        //              MOVE 29 TO T-GG (2)
        //           ELSE
        //              MOVE 28 TO T-GG (2).
        if (ws.getResto() == 0) {
            // COB_CODE: MOVE 29 TO T-GG (2)
            ws.getTabGiorni().settGg(2, ((short)29));
        }
        else {
            // COB_CODE: MOVE 28 TO T-GG (2).
            ws.getTabGiorni().settGg(2, ((short)28));
        }
        // COB_CODE: IF C-MM GREATER 12 OR C-MM LESS 1
        //                        OR C-GG GREATER T-GG (C-MM) OR C-GG LESS 1
        //           GO TO FINE
        //           GO TO FINE.
        if (ws.getComData().getcMm() > 12 || ws.getComData().getcMm() < 1 || ws.getComData().getcGg() > ws.getTabGiorni().gettGg(ws.getComData().getcMm()) || ws.getComData().getcGg() < 1) {
            // COB_CODE: GO TO FINE
            fine();
        }
        else {
            // COB_CODE: ELSE         MOVE ZERO TO PARAM
            param.setParam(((short)0));
            // COB_CODE: GO TO FINE.
            fine();
        }
    }

    /**Original name: FINE<br>*/
    private void fine() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }
}
