package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.MoviFinrioDao;
import it.accenture.jnais.commons.data.to.IMoviFinrio;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs5950Data;
import it.accenture.jnais.ws.MoviFinrioLdbs5950;
import it.accenture.jnais.ws.redefines.MfzCosOprz;
import it.accenture.jnais.ws.redefines.MfzDtEffMoviFinrio;
import it.accenture.jnais.ws.redefines.MfzDtElab;
import it.accenture.jnais.ws.redefines.MfzDtRichMovi;
import it.accenture.jnais.ws.redefines.MfzIdAdes;
import it.accenture.jnais.ws.redefines.MfzIdLiq;
import it.accenture.jnais.ws.redefines.MfzIdMoviChiu;
import it.accenture.jnais.ws.redefines.MfzIdTitCont;

/**Original name: LDBS5950<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  20 SET 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs5950 extends Program implements IMoviFinrio {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private MoviFinrioDao moviFinrioDao = new MoviFinrioDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs5950Data ws = new Ldbs5950Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: MOVI-FINRIO
    private MoviFinrioLdbs5950 moviFinrio;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS5950_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, MoviFinrioLdbs5950 moviFinrio) {
        this.idsv0003 = idsv0003;
        this.moviFinrio = moviFinrio;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs5950 getInstance() {
        return ((Ldbs5950)Programs.getInstance(Ldbs5950.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS5950'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS5950");
        // COB_CODE: MOVE 'MOVI-FINRIO' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("MOVI-FINRIO");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_MOVI_FINRIO
        //                    ,ID_ADES
        //                    ,ID_LIQ
        //                    ,ID_TIT_CONT
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI_FINRIO
        //                    ,DT_EFF_MOVI_FINRIO
        //                    ,DT_ELAB
        //                    ,DT_RICH_MOVI
        //                    ,COS_OPRZ
        //                    ,STAT_MOVI
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_CAUS_RETTIFICA
        //              FROM MOVI_FINRIO
        //              WHERE  ID_MOVI_CRZ = :MFZ-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_MOVI_FINRIO
        //                    ,ID_ADES
        //                    ,ID_LIQ
        //                    ,ID_TIT_CONT
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI_FINRIO
        //                    ,DT_EFF_MOVI_FINRIO
        //                    ,DT_ELAB
        //                    ,DT_RICH_MOVI
        //                    ,COS_OPRZ
        //                    ,STAT_MOVI
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_CAUS_RETTIFICA
        //             INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //             FROM MOVI_FINRIO
        //             WHERE  ID_MOVI_CRZ = :MFZ-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        moviFinrioDao.selectRec4(moviFinrio.getMfzIdMoviCrz(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        moviFinrioDao.openCEff32(moviFinrio.getMfzIdMoviCrz(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        moviFinrioDao.closeCEff32();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //           END-EXEC.
        moviFinrioDao.fetchCEff32(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_MOVI_FINRIO
        //                    ,ID_ADES
        //                    ,ID_LIQ
        //                    ,ID_TIT_CONT
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI_FINRIO
        //                    ,DT_EFF_MOVI_FINRIO
        //                    ,DT_ELAB
        //                    ,DT_RICH_MOVI
        //                    ,COS_OPRZ
        //                    ,STAT_MOVI
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_CAUS_RETTIFICA
        //              FROM MOVI_FINRIO
        //              WHERE  ID_MOVI_CRZ = :MFZ-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_MOVI_FINRIO
        //                    ,ID_ADES
        //                    ,ID_LIQ
        //                    ,ID_TIT_CONT
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_MOVI_FINRIO
        //                    ,DT_EFF_MOVI_FINRIO
        //                    ,DT_ELAB
        //                    ,DT_RICH_MOVI
        //                    ,COS_OPRZ
        //                    ,STAT_MOVI
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_CAUS_RETTIFICA
        //             INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //             FROM MOVI_FINRIO
        //             WHERE  ID_MOVI_CRZ = :MFZ-ID-MOVI-CRZ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        moviFinrioDao.selectRec5(moviFinrio.getMfzIdMoviCrz(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        moviFinrioDao.openCCpz32(moviFinrio.getMfzIdMoviCrz(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        moviFinrioDao.closeCCpz32();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :MFZ-ID-MOVI-FINRIO
        //               ,:MFZ-ID-ADES
        //                :IND-MFZ-ID-ADES
        //               ,:MFZ-ID-LIQ
        //                :IND-MFZ-ID-LIQ
        //               ,:MFZ-ID-TIT-CONT
        //                :IND-MFZ-ID-TIT-CONT
        //               ,:MFZ-ID-MOVI-CRZ
        //               ,:MFZ-ID-MOVI-CHIU
        //                :IND-MFZ-ID-MOVI-CHIU
        //               ,:MFZ-DT-INI-EFF-DB
        //               ,:MFZ-DT-END-EFF-DB
        //               ,:MFZ-COD-COMP-ANIA
        //               ,:MFZ-TP-MOVI-FINRIO
        //               ,:MFZ-DT-EFF-MOVI-FINRIO-DB
        //                :IND-MFZ-DT-EFF-MOVI-FINRIO
        //               ,:MFZ-DT-ELAB-DB
        //                :IND-MFZ-DT-ELAB
        //               ,:MFZ-DT-RICH-MOVI-DB
        //                :IND-MFZ-DT-RICH-MOVI
        //               ,:MFZ-COS-OPRZ
        //                :IND-MFZ-COS-OPRZ
        //               ,:MFZ-STAT-MOVI
        //               ,:MFZ-DS-RIGA
        //               ,:MFZ-DS-OPER-SQL
        //               ,:MFZ-DS-VER
        //               ,:MFZ-DS-TS-INI-CPTZ
        //               ,:MFZ-DS-TS-END-CPTZ
        //               ,:MFZ-DS-UTENTE
        //               ,:MFZ-DS-STATO-ELAB
        //               ,:MFZ-TP-CAUS-RETTIFICA
        //                :IND-MFZ-TP-CAUS-RETTIFICA
        //           END-EXEC.
        moviFinrioDao.fetchCCpz32(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-MFZ-ID-ADES = -1
        //              MOVE HIGH-VALUES TO MFZ-ID-ADES-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getIdAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-ID-ADES-NULL
            moviFinrio.getMfzIdAdes().setMfzIdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdAdes.Len.MFZ_ID_ADES_NULL));
        }
        // COB_CODE: IF IND-MFZ-ID-LIQ = -1
        //              MOVE HIGH-VALUES TO MFZ-ID-LIQ-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getIdLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-ID-LIQ-NULL
            moviFinrio.getMfzIdLiq().setMfzIdLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdLiq.Len.MFZ_ID_LIQ_NULL));
        }
        // COB_CODE: IF IND-MFZ-ID-TIT-CONT = -1
        //              MOVE HIGH-VALUES TO MFZ-ID-TIT-CONT-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getIdTitCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-ID-TIT-CONT-NULL
            moviFinrio.getMfzIdTitCont().setMfzIdTitContNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdTitCont.Len.MFZ_ID_TIT_CONT_NULL));
        }
        // COB_CODE: IF IND-MFZ-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO MFZ-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-ID-MOVI-CHIU-NULL
            moviFinrio.getMfzIdMoviChiu().setMfzIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzIdMoviChiu.Len.MFZ_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-MFZ-DT-EFF-MOVI-FINRIO = -1
        //              MOVE HIGH-VALUES TO MFZ-DT-EFF-MOVI-FINRIO-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getDtEffMoviFinrio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-DT-EFF-MOVI-FINRIO-NULL
            moviFinrio.getMfzDtEffMoviFinrio().setMfzDtEffMoviFinrioNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzDtEffMoviFinrio.Len.MFZ_DT_EFF_MOVI_FINRIO_NULL));
        }
        // COB_CODE: IF IND-MFZ-DT-ELAB = -1
        //              MOVE HIGH-VALUES TO MFZ-DT-ELAB-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getDtElab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-DT-ELAB-NULL
            moviFinrio.getMfzDtElab().setMfzDtElabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzDtElab.Len.MFZ_DT_ELAB_NULL));
        }
        // COB_CODE: IF IND-MFZ-DT-RICH-MOVI = -1
        //              MOVE HIGH-VALUES TO MFZ-DT-RICH-MOVI-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getDtRichMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-DT-RICH-MOVI-NULL
            moviFinrio.getMfzDtRichMovi().setMfzDtRichMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzDtRichMovi.Len.MFZ_DT_RICH_MOVI_NULL));
        }
        // COB_CODE: IF IND-MFZ-COS-OPRZ = -1
        //              MOVE HIGH-VALUES TO MFZ-COS-OPRZ-NULL
        //           END-IF
        if (ws.getIndMoviFinrio().getCosOprz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-COS-OPRZ-NULL
            moviFinrio.getMfzCosOprz().setMfzCosOprzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MfzCosOprz.Len.MFZ_COS_OPRZ_NULL));
        }
        // COB_CODE: IF IND-MFZ-TP-CAUS-RETTIFICA = -1
        //              MOVE HIGH-VALUES TO MFZ-TP-CAUS-RETTIFICA-NULL
        //           END-IF.
        if (ws.getIndMoviFinrio().getTpCausRettifica() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO MFZ-TP-CAUS-RETTIFICA-NULL
            moviFinrio.setMfzTpCausRettifica(LiteralGenerator.create(Types.HIGH_CHAR_VAL, MoviFinrioLdbs5950.Len.MFZ_TP_CAUS_RETTIFICA));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE MFZ-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-INI-EFF
        moviFinrio.setMfzDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE MFZ-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-END-EFF
        moviFinrio.setMfzDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-MFZ-DT-EFF-MOVI-FINRIO = 0
        //               MOVE WS-DATE-N      TO MFZ-DT-EFF-MOVI-FINRIO
        //           END-IF
        if (ws.getIndMoviFinrio().getDtEffMoviFinrio() == 0) {
            // COB_CODE: MOVE MFZ-DT-EFF-MOVI-FINRIO-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-EFF-MOVI-FINRIO
            moviFinrio.getMfzDtEffMoviFinrio().setMfzDtEffMoviFinrio(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-MFZ-DT-ELAB = 0
        //               MOVE WS-DATE-N      TO MFZ-DT-ELAB
        //           END-IF
        if (ws.getIndMoviFinrio().getDtElab() == 0) {
            // COB_CODE: MOVE MFZ-DT-ELAB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-ELAB
            moviFinrio.getMfzDtElab().setMfzDtElab(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-MFZ-DT-RICH-MOVI = 0
        //               MOVE WS-DATE-N      TO MFZ-DT-RICH-MOVI
        //           END-IF.
        if (ws.getIndMoviFinrio().getDtRichMovi() == 0) {
            // COB_CODE: MOVE MFZ-DT-RICH-MOVI-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getMoviFinrioDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO MFZ-DT-RICH-MOVI
            moviFinrio.getMfzDtRichMovi().setMfzDtRichMovi(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return moviFinrio.getMfzCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.moviFinrio.setMfzCodCompAnia(codCompAnia);
    }

    @Override
    public AfDecimal getCosOprz() {
        return moviFinrio.getMfzCosOprz().getMfzCosOprz();
    }

    @Override
    public void setCosOprz(AfDecimal cosOprz) {
        this.moviFinrio.getMfzCosOprz().setMfzCosOprz(cosOprz.copy());
    }

    @Override
    public AfDecimal getCosOprzObj() {
        if (ws.getIndMoviFinrio().getCosOprz() >= 0) {
            return getCosOprz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCosOprzObj(AfDecimal cosOprzObj) {
        if (cosOprzObj != null) {
            setCosOprz(new AfDecimal(cosOprzObj, 15, 3));
            ws.getIndMoviFinrio().setCosOprz(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setCosOprz(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return moviFinrio.getMfzDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.moviFinrio.setMfzDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return moviFinrio.getMfzDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.moviFinrio.setMfzDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return moviFinrio.getMfzDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.moviFinrio.setMfzDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return moviFinrio.getMfzDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.moviFinrio.setMfzDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return moviFinrio.getMfzDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.moviFinrio.setMfzDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return moviFinrio.getMfzDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.moviFinrio.setMfzDsVer(dsVer);
    }

    @Override
    public String getDtEffMoviFinrioDb() {
        return ws.getMoviFinrioDb().getIniCopDb();
    }

    @Override
    public void setDtEffMoviFinrioDb(String dtEffMoviFinrioDb) {
        this.ws.getMoviFinrioDb().setIniCopDb(dtEffMoviFinrioDb);
    }

    @Override
    public String getDtEffMoviFinrioDbObj() {
        if (ws.getIndMoviFinrio().getDtEffMoviFinrio() >= 0) {
            return getDtEffMoviFinrioDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEffMoviFinrioDbObj(String dtEffMoviFinrioDbObj) {
        if (dtEffMoviFinrioDbObj != null) {
            setDtEffMoviFinrioDb(dtEffMoviFinrioDbObj);
            ws.getIndMoviFinrio().setDtEffMoviFinrio(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setDtEffMoviFinrio(((short)-1));
        }
    }

    @Override
    public String getDtElabDb() {
        return ws.getMoviFinrioDb().getEndCopDb();
    }

    @Override
    public void setDtElabDb(String dtElabDb) {
        this.ws.getMoviFinrioDb().setEndCopDb(dtElabDb);
    }

    @Override
    public String getDtElabDbObj() {
        if (ws.getIndMoviFinrio().getDtElab() >= 0) {
            return getDtElabDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtElabDbObj(String dtElabDbObj) {
        if (dtElabDbObj != null) {
            setDtElabDb(dtElabDbObj);
            ws.getIndMoviFinrio().setDtElab(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setDtElab(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getMoviFinrioDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getMoviFinrioDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getMoviFinrioDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getMoviFinrioDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtRichMoviDb() {
        return ws.getMoviFinrioDb().getEsiTitDb();
    }

    @Override
    public void setDtRichMoviDb(String dtRichMoviDb) {
        this.ws.getMoviFinrioDb().setEsiTitDb(dtRichMoviDb);
    }

    @Override
    public String getDtRichMoviDbObj() {
        if (ws.getIndMoviFinrio().getDtRichMovi() >= 0) {
            return getDtRichMoviDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRichMoviDbObj(String dtRichMoviDbObj) {
        if (dtRichMoviDbObj != null) {
            setDtRichMoviDb(dtRichMoviDbObj);
            ws.getIndMoviFinrio().setDtRichMovi(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setDtRichMovi(((short)-1));
        }
    }

    @Override
    public int getIdAdes() {
        return moviFinrio.getMfzIdAdes().getMfzIdAdes();
    }

    @Override
    public void setIdAdes(int idAdes) {
        this.moviFinrio.getMfzIdAdes().setMfzIdAdes(idAdes);
    }

    @Override
    public Integer getIdAdesObj() {
        if (ws.getIndMoviFinrio().getIdAdes() >= 0) {
            return ((Integer)getIdAdes());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdAdesObj(Integer idAdesObj) {
        if (idAdesObj != null) {
            setIdAdes(((int)idAdesObj));
            ws.getIndMoviFinrio().setIdAdes(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setIdAdes(((short)-1));
        }
    }

    @Override
    public int getIdLiq() {
        return moviFinrio.getMfzIdLiq().getMfzIdLiq();
    }

    @Override
    public void setIdLiq(int idLiq) {
        this.moviFinrio.getMfzIdLiq().setMfzIdLiq(idLiq);
    }

    @Override
    public Integer getIdLiqObj() {
        if (ws.getIndMoviFinrio().getIdLiq() >= 0) {
            return ((Integer)getIdLiq());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdLiqObj(Integer idLiqObj) {
        if (idLiqObj != null) {
            setIdLiq(((int)idLiqObj));
            ws.getIndMoviFinrio().setIdLiq(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setIdLiq(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return moviFinrio.getMfzIdMoviChiu().getMfzIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.moviFinrio.getMfzIdMoviChiu().setMfzIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndMoviFinrio().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndMoviFinrio().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return moviFinrio.getMfzIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.moviFinrio.setMfzIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdMoviFinrio() {
        return moviFinrio.getMfzIdMoviFinrio();
    }

    @Override
    public void setIdMoviFinrio(int idMoviFinrio) {
        this.moviFinrio.setMfzIdMoviFinrio(idMoviFinrio);
    }

    @Override
    public int getIdTitCont() {
        return moviFinrio.getMfzIdTitCont().getMfzIdTitCont();
    }

    @Override
    public void setIdTitCont(int idTitCont) {
        this.moviFinrio.getMfzIdTitCont().setMfzIdTitCont(idTitCont);
    }

    @Override
    public Integer getIdTitContObj() {
        if (ws.getIndMoviFinrio().getIdTitCont() >= 0) {
            return ((Integer)getIdTitCont());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdTitContObj(Integer idTitContObj) {
        if (idTitContObj != null) {
            setIdTitCont(((int)idTitContObj));
            ws.getIndMoviFinrio().setIdTitCont(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setIdTitCont(((short)-1));
        }
    }

    @Override
    public long getMfzDsRiga() {
        return moviFinrio.getMfzDsRiga();
    }

    @Override
    public void setMfzDsRiga(long mfzDsRiga) {
        this.moviFinrio.setMfzDsRiga(mfzDsRiga);
    }

    @Override
    public String getStatMovi() {
        return moviFinrio.getMfzStatMovi();
    }

    @Override
    public void setStatMovi(String statMovi) {
        this.moviFinrio.setMfzStatMovi(statMovi);
    }

    @Override
    public String getTpCausRettifica() {
        return moviFinrio.getMfzTpCausRettifica();
    }

    @Override
    public void setTpCausRettifica(String tpCausRettifica) {
        this.moviFinrio.setMfzTpCausRettifica(tpCausRettifica);
    }

    @Override
    public String getTpCausRettificaObj() {
        if (ws.getIndMoviFinrio().getTpCausRettifica() >= 0) {
            return getTpCausRettifica();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausRettificaObj(String tpCausRettificaObj) {
        if (tpCausRettificaObj != null) {
            setTpCausRettifica(tpCausRettificaObj);
            ws.getIndMoviFinrio().setTpCausRettifica(((short)0));
        }
        else {
            ws.getIndMoviFinrio().setTpCausRettifica(((short)-1));
        }
    }

    @Override
    public String getTpMoviFinrio() {
        return moviFinrio.getMfzTpMoviFinrio();
    }

    @Override
    public void setTpMoviFinrio(String tpMoviFinrio) {
        this.moviFinrio.setMfzTpMoviFinrio(tpMoviFinrio);
    }
}
