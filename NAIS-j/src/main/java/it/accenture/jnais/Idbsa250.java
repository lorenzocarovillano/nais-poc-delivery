package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.PersDao;
import it.accenture.jnais.commons.data.to.IPers;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsa250Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Pers;
import it.accenture.jnais.ws.redefines.A25CodPersSecond;
import it.accenture.jnais.ws.redefines.A25Dt1aAtvt;
import it.accenture.jnais.ws.redefines.A25DtAcqsPers;
import it.accenture.jnais.ws.redefines.A25DtBlocCli;
import it.accenture.jnais.ws.redefines.A25DtDeadPers;
import it.accenture.jnais.ws.redefines.A25DtEndVldtPers;
import it.accenture.jnais.ws.redefines.A25DtNascCli;
import it.accenture.jnais.ws.redefines.A25DtSegnalPartner;
import it.accenture.jnais.ws.redefines.A25IdSegmentazCli;
import it.accenture.jnais.ws.redefines.A25TstamAggmRiga;
import it.accenture.jnais.ws.redefines.A25TstamEndVldt;

/**Original name: IDBSA250<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  13 NOV 2018.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsa250 extends Program implements IPers {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private PersDao persDao = new PersDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsa250Data ws = new Idbsa250Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PERS
    private Pers pers;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSA250_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Pers pers) {
        this.idsv0003 = idsv0003;
        this.pers = pers;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsa250 getInstance() {
        return ((Idbsa250)Programs.getInstance(Idbsa250.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSA250'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSA250");
        // COB_CODE: MOVE 'PERS' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PERS");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PERS
        //                ,TSTAM_INI_VLDT
        //                ,TSTAM_END_VLDT
        //                ,COD_PERS
        //                ,RIFTO_RETE
        //                ,COD_PRT_IVA
        //                ,IND_PVCY_PRSNL
        //                ,IND_PVCY_CMMRC
        //                ,IND_PVCY_INDST
        //                ,DT_NASC_CLI
        //                ,DT_ACQS_PERS
        //                ,IND_CLI
        //                ,COD_CMN
        //                ,COD_FRM_GIURD
        //                ,COD_ENTE_PUBB
        //                ,DEN_RGN_SOC
        //                ,DEN_SIG_RGN_SOC
        //                ,IND_ESE_FISC
        //                ,COD_STAT_CVL
        //                ,DEN_NOME
        //                ,DEN_COGN
        //                ,COD_FISC
        //                ,IND_SEX
        //                ,IND_CPCT_GIURD
        //                ,IND_PORT_HDCP
        //                ,COD_USER_INS
        //                ,TSTAM_INS_RIGA
        //                ,COD_USER_AGGM
        //                ,TSTAM_AGGM_RIGA
        //                ,DEN_CMN_NASC_STRN
        //                ,COD_RAMO_STGR
        //                ,COD_STGR_ATVT_UIC
        //                ,COD_RAMO_ATVT_UIC
        //                ,DT_END_VLDT_PERS
        //                ,DT_DEAD_PERS
        //                ,TP_STAT_CLI
        //                ,DT_BLOC_CLI
        //                ,COD_PERS_SECOND
        //                ,ID_SEGMENTAZ_CLI
        //                ,DT_1A_ATVT
        //                ,DT_SEGNAL_PARTNER
        //             INTO
        //                :A25-ID-PERS
        //               ,:A25-TSTAM-INI-VLDT
        //               ,:A25-TSTAM-END-VLDT
        //                :IND-A25-TSTAM-END-VLDT
        //               ,:A25-COD-PERS
        //               ,:A25-RIFTO-RETE
        //                :IND-A25-RIFTO-RETE
        //               ,:A25-COD-PRT-IVA
        //                :IND-A25-COD-PRT-IVA
        //               ,:A25-IND-PVCY-PRSNL
        //                :IND-A25-IND-PVCY-PRSNL
        //               ,:A25-IND-PVCY-CMMRC
        //                :IND-A25-IND-PVCY-CMMRC
        //               ,:A25-IND-PVCY-INDST
        //                :IND-A25-IND-PVCY-INDST
        //               ,:A25-DT-NASC-CLI-DB
        //                :IND-A25-DT-NASC-CLI
        //               ,:A25-DT-ACQS-PERS-DB
        //                :IND-A25-DT-ACQS-PERS
        //               ,:A25-IND-CLI
        //                :IND-A25-IND-CLI
        //               ,:A25-COD-CMN
        //                :IND-A25-COD-CMN
        //               ,:A25-COD-FRM-GIURD
        //                :IND-A25-COD-FRM-GIURD
        //               ,:A25-COD-ENTE-PUBB
        //                :IND-A25-COD-ENTE-PUBB
        //               ,:A25-DEN-RGN-SOC-VCHAR
        //                :IND-A25-DEN-RGN-SOC
        //               ,:A25-DEN-SIG-RGN-SOC-VCHAR
        //                :IND-A25-DEN-SIG-RGN-SOC
        //               ,:A25-IND-ESE-FISC
        //                :IND-A25-IND-ESE-FISC
        //               ,:A25-COD-STAT-CVL
        //                :IND-A25-COD-STAT-CVL
        //               ,:A25-DEN-NOME-VCHAR
        //                :IND-A25-DEN-NOME
        //               ,:A25-DEN-COGN-VCHAR
        //                :IND-A25-DEN-COGN
        //               ,:A25-COD-FISC
        //                :IND-A25-COD-FISC
        //               ,:A25-IND-SEX
        //                :IND-A25-IND-SEX
        //               ,:A25-IND-CPCT-GIURD
        //                :IND-A25-IND-CPCT-GIURD
        //               ,:A25-IND-PORT-HDCP
        //                :IND-A25-IND-PORT-HDCP
        //               ,:A25-COD-USER-INS
        //               ,:A25-TSTAM-INS-RIGA
        //               ,:A25-COD-USER-AGGM
        //                :IND-A25-COD-USER-AGGM
        //               ,:A25-TSTAM-AGGM-RIGA
        //                :IND-A25-TSTAM-AGGM-RIGA
        //               ,:A25-DEN-CMN-NASC-STRN-VCHAR
        //                :IND-A25-DEN-CMN-NASC-STRN
        //               ,:A25-COD-RAMO-STGR
        //                :IND-A25-COD-RAMO-STGR
        //               ,:A25-COD-STGR-ATVT-UIC
        //                :IND-A25-COD-STGR-ATVT-UIC
        //               ,:A25-COD-RAMO-ATVT-UIC
        //                :IND-A25-COD-RAMO-ATVT-UIC
        //               ,:A25-DT-END-VLDT-PERS-DB
        //                :IND-A25-DT-END-VLDT-PERS
        //               ,:A25-DT-DEAD-PERS-DB
        //                :IND-A25-DT-DEAD-PERS
        //               ,:A25-TP-STAT-CLI
        //                :IND-A25-TP-STAT-CLI
        //               ,:A25-DT-BLOC-CLI-DB
        //                :IND-A25-DT-BLOC-CLI
        //               ,:A25-COD-PERS-SECOND
        //                :IND-A25-COD-PERS-SECOND
        //               ,:A25-ID-SEGMENTAZ-CLI
        //                :IND-A25-ID-SEGMENTAZ-CLI
        //               ,:A25-DT-1A-ATVT-DB
        //                :IND-A25-DT-1A-ATVT
        //               ,:A25-DT-SEGNAL-PARTNER-DB
        //                :IND-A25-DT-SEGNAL-PARTNER
        //             FROM PERS
        //             WHERE     ID_PERS = :A25-ID-PERS
        //                   AND TSTAM_INI_VLDT = :A25-TSTAM-INI-VLDT
        //           END-EXEC.
        persDao.selectRec(pers.getA25IdPers(), pers.getA25TstamIniVldt(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO PERS
            //                  (
            //                     ID_PERS
            //                    ,TSTAM_INI_VLDT
            //                    ,TSTAM_END_VLDT
            //                    ,COD_PERS
            //                    ,RIFTO_RETE
            //                    ,COD_PRT_IVA
            //                    ,IND_PVCY_PRSNL
            //                    ,IND_PVCY_CMMRC
            //                    ,IND_PVCY_INDST
            //                    ,DT_NASC_CLI
            //                    ,DT_ACQS_PERS
            //                    ,IND_CLI
            //                    ,COD_CMN
            //                    ,COD_FRM_GIURD
            //                    ,COD_ENTE_PUBB
            //                    ,DEN_RGN_SOC
            //                    ,DEN_SIG_RGN_SOC
            //                    ,IND_ESE_FISC
            //                    ,COD_STAT_CVL
            //                    ,DEN_NOME
            //                    ,DEN_COGN
            //                    ,COD_FISC
            //                    ,IND_SEX
            //                    ,IND_CPCT_GIURD
            //                    ,IND_PORT_HDCP
            //                    ,COD_USER_INS
            //                    ,TSTAM_INS_RIGA
            //                    ,COD_USER_AGGM
            //                    ,TSTAM_AGGM_RIGA
            //                    ,DEN_CMN_NASC_STRN
            //                    ,COD_RAMO_STGR
            //                    ,COD_STGR_ATVT_UIC
            //                    ,COD_RAMO_ATVT_UIC
            //                    ,DT_END_VLDT_PERS
            //                    ,DT_DEAD_PERS
            //                    ,TP_STAT_CLI
            //                    ,DT_BLOC_CLI
            //                    ,COD_PERS_SECOND
            //                    ,ID_SEGMENTAZ_CLI
            //                    ,DT_1A_ATVT
            //                    ,DT_SEGNAL_PARTNER
            //                  )
            //              VALUES
            //                  (
            //                    :A25-ID-PERS
            //                    ,:A25-TSTAM-INI-VLDT
            //                    ,:A25-TSTAM-END-VLDT
            //                     :IND-A25-TSTAM-END-VLDT
            //                    ,:A25-COD-PERS
            //                    ,:A25-RIFTO-RETE
            //                     :IND-A25-RIFTO-RETE
            //                    ,:A25-COD-PRT-IVA
            //                     :IND-A25-COD-PRT-IVA
            //                    ,:A25-IND-PVCY-PRSNL
            //                     :IND-A25-IND-PVCY-PRSNL
            //                    ,:A25-IND-PVCY-CMMRC
            //                     :IND-A25-IND-PVCY-CMMRC
            //                    ,:A25-IND-PVCY-INDST
            //                     :IND-A25-IND-PVCY-INDST
            //                    ,:A25-DT-NASC-CLI-DB
            //                     :IND-A25-DT-NASC-CLI
            //                    ,:A25-DT-ACQS-PERS-DB
            //                     :IND-A25-DT-ACQS-PERS
            //                    ,:A25-IND-CLI
            //                     :IND-A25-IND-CLI
            //                    ,:A25-COD-CMN
            //                     :IND-A25-COD-CMN
            //                    ,:A25-COD-FRM-GIURD
            //                     :IND-A25-COD-FRM-GIURD
            //                    ,:A25-COD-ENTE-PUBB
            //                     :IND-A25-COD-ENTE-PUBB
            //                    ,:A25-DEN-RGN-SOC-VCHAR
            //                     :IND-A25-DEN-RGN-SOC
            //                    ,:A25-DEN-SIG-RGN-SOC-VCHAR
            //                     :IND-A25-DEN-SIG-RGN-SOC
            //                    ,:A25-IND-ESE-FISC
            //                     :IND-A25-IND-ESE-FISC
            //                    ,:A25-COD-STAT-CVL
            //                     :IND-A25-COD-STAT-CVL
            //                    ,:A25-DEN-NOME-VCHAR
            //                     :IND-A25-DEN-NOME
            //                    ,:A25-DEN-COGN-VCHAR
            //                     :IND-A25-DEN-COGN
            //                    ,:A25-COD-FISC
            //                     :IND-A25-COD-FISC
            //                    ,:A25-IND-SEX
            //                     :IND-A25-IND-SEX
            //                    ,:A25-IND-CPCT-GIURD
            //                     :IND-A25-IND-CPCT-GIURD
            //                    ,:A25-IND-PORT-HDCP
            //                     :IND-A25-IND-PORT-HDCP
            //                    ,:A25-COD-USER-INS
            //                    ,:A25-TSTAM-INS-RIGA
            //                    ,:A25-COD-USER-AGGM
            //                     :IND-A25-COD-USER-AGGM
            //                    ,:A25-TSTAM-AGGM-RIGA
            //                     :IND-A25-TSTAM-AGGM-RIGA
            //                    ,:A25-DEN-CMN-NASC-STRN-VCHAR
            //                     :IND-A25-DEN-CMN-NASC-STRN
            //                    ,:A25-COD-RAMO-STGR
            //                     :IND-A25-COD-RAMO-STGR
            //                    ,:A25-COD-STGR-ATVT-UIC
            //                     :IND-A25-COD-STGR-ATVT-UIC
            //                    ,:A25-COD-RAMO-ATVT-UIC
            //                     :IND-A25-COD-RAMO-ATVT-UIC
            //                    ,:A25-DT-END-VLDT-PERS-DB
            //                     :IND-A25-DT-END-VLDT-PERS
            //                    ,:A25-DT-DEAD-PERS-DB
            //                     :IND-A25-DT-DEAD-PERS
            //                    ,:A25-TP-STAT-CLI
            //                     :IND-A25-TP-STAT-CLI
            //                    ,:A25-DT-BLOC-CLI-DB
            //                     :IND-A25-DT-BLOC-CLI
            //                    ,:A25-COD-PERS-SECOND
            //                     :IND-A25-COD-PERS-SECOND
            //                    ,:A25-ID-SEGMENTAZ-CLI
            //                     :IND-A25-ID-SEGMENTAZ-CLI
            //                    ,:A25-DT-1A-ATVT-DB
            //                     :IND-A25-DT-1A-ATVT
            //                    ,:A25-DT-SEGNAL-PARTNER-DB
            //                     :IND-A25-DT-SEGNAL-PARTNER
            //                  )
            //           END-EXEC
            persDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE PERS SET
        //                   ID_PERS                =
        //                :A25-ID-PERS
        //                  ,TSTAM_INI_VLDT         =
        //                :A25-TSTAM-INI-VLDT
        //                  ,TSTAM_END_VLDT         =
        //                :A25-TSTAM-END-VLDT
        //                                       :IND-A25-TSTAM-END-VLDT
        //                  ,COD_PERS               =
        //                :A25-COD-PERS
        //                  ,RIFTO_RETE             =
        //                :A25-RIFTO-RETE
        //                                       :IND-A25-RIFTO-RETE
        //                  ,COD_PRT_IVA            =
        //                :A25-COD-PRT-IVA
        //                                       :IND-A25-COD-PRT-IVA
        //                  ,IND_PVCY_PRSNL         =
        //                :A25-IND-PVCY-PRSNL
        //                                       :IND-A25-IND-PVCY-PRSNL
        //                  ,IND_PVCY_CMMRC         =
        //                :A25-IND-PVCY-CMMRC
        //                                       :IND-A25-IND-PVCY-CMMRC
        //                  ,IND_PVCY_INDST         =
        //                :A25-IND-PVCY-INDST
        //                                       :IND-A25-IND-PVCY-INDST
        //                  ,DT_NASC_CLI            =
        //           :A25-DT-NASC-CLI-DB
        //                                       :IND-A25-DT-NASC-CLI
        //                  ,DT_ACQS_PERS           =
        //           :A25-DT-ACQS-PERS-DB
        //                                       :IND-A25-DT-ACQS-PERS
        //                  ,IND_CLI                =
        //                :A25-IND-CLI
        //                                       :IND-A25-IND-CLI
        //                  ,COD_CMN                =
        //                :A25-COD-CMN
        //                                       :IND-A25-COD-CMN
        //                  ,COD_FRM_GIURD          =
        //                :A25-COD-FRM-GIURD
        //                                       :IND-A25-COD-FRM-GIURD
        //                  ,COD_ENTE_PUBB          =
        //                :A25-COD-ENTE-PUBB
        //                                       :IND-A25-COD-ENTE-PUBB
        //                  ,DEN_RGN_SOC            =
        //                :A25-DEN-RGN-SOC-VCHAR
        //                                       :IND-A25-DEN-RGN-SOC
        //                  ,DEN_SIG_RGN_SOC        =
        //                :A25-DEN-SIG-RGN-SOC-VCHAR
        //                                       :IND-A25-DEN-SIG-RGN-SOC
        //                  ,IND_ESE_FISC           =
        //                :A25-IND-ESE-FISC
        //                                       :IND-A25-IND-ESE-FISC
        //                  ,COD_STAT_CVL           =
        //                :A25-COD-STAT-CVL
        //                                       :IND-A25-COD-STAT-CVL
        //                  ,DEN_NOME               =
        //                :A25-DEN-NOME-VCHAR
        //                                       :IND-A25-DEN-NOME
        //                  ,DEN_COGN               =
        //                :A25-DEN-COGN-VCHAR
        //                                       :IND-A25-DEN-COGN
        //                  ,COD_FISC               =
        //                :A25-COD-FISC
        //                                       :IND-A25-COD-FISC
        //                  ,IND_SEX                =
        //                :A25-IND-SEX
        //                                       :IND-A25-IND-SEX
        //                  ,IND_CPCT_GIURD         =
        //                :A25-IND-CPCT-GIURD
        //                                       :IND-A25-IND-CPCT-GIURD
        //                  ,IND_PORT_HDCP          =
        //                :A25-IND-PORT-HDCP
        //                                       :IND-A25-IND-PORT-HDCP
        //                  ,COD_USER_INS           =
        //                :A25-COD-USER-INS
        //                  ,TSTAM_INS_RIGA         =
        //                :A25-TSTAM-INS-RIGA
        //                  ,COD_USER_AGGM          =
        //                :A25-COD-USER-AGGM
        //                                       :IND-A25-COD-USER-AGGM
        //                  ,TSTAM_AGGM_RIGA        =
        //                :A25-TSTAM-AGGM-RIGA
        //                                       :IND-A25-TSTAM-AGGM-RIGA
        //                  ,DEN_CMN_NASC_STRN      =
        //                :A25-DEN-CMN-NASC-STRN-VCHAR
        //                                       :IND-A25-DEN-CMN-NASC-STRN
        //                  ,COD_RAMO_STGR          =
        //                :A25-COD-RAMO-STGR
        //                                       :IND-A25-COD-RAMO-STGR
        //                  ,COD_STGR_ATVT_UIC      =
        //                :A25-COD-STGR-ATVT-UIC
        //                                       :IND-A25-COD-STGR-ATVT-UIC
        //                  ,COD_RAMO_ATVT_UIC      =
        //                :A25-COD-RAMO-ATVT-UIC
        //                                       :IND-A25-COD-RAMO-ATVT-UIC
        //                  ,DT_END_VLDT_PERS       =
        //           :A25-DT-END-VLDT-PERS-DB
        //                                       :IND-A25-DT-END-VLDT-PERS
        //                  ,DT_DEAD_PERS           =
        //           :A25-DT-DEAD-PERS-DB
        //                                       :IND-A25-DT-DEAD-PERS
        //                  ,TP_STAT_CLI            =
        //                :A25-TP-STAT-CLI
        //                                       :IND-A25-TP-STAT-CLI
        //                  ,DT_BLOC_CLI            =
        //           :A25-DT-BLOC-CLI-DB
        //                                       :IND-A25-DT-BLOC-CLI
        //                  ,COD_PERS_SECOND        =
        //                :A25-COD-PERS-SECOND
        //                                       :IND-A25-COD-PERS-SECOND
        //                  ,ID_SEGMENTAZ_CLI       =
        //                :A25-ID-SEGMENTAZ-CLI
        //                                       :IND-A25-ID-SEGMENTAZ-CLI
        //                  ,DT_1A_ATVT             =
        //           :A25-DT-1A-ATVT-DB
        //                                       :IND-A25-DT-1A-ATVT
        //                  ,DT_SEGNAL_PARTNER      =
        //           :A25-DT-SEGNAL-PARTNER-DB
        //                                       :IND-A25-DT-SEGNAL-PARTNER
        //                WHERE     ID_PERS = :A25-ID-PERS
        //                   AND TSTAM_INI_VLDT = :A25-TSTAM-INI-VLDT
        //           END-EXEC.
        persDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM PERS
        //                WHERE     ID_PERS = :A25-ID-PERS
        //                   AND TSTAM_INI_VLDT = :A25-TSTAM-INI-VLDT
        //           END-EXEC.
        persDao.deleteRec(pers.getA25IdPers(), pers.getA25TstamIniVldt());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-A25-TSTAM-END-VLDT = -1
        //              MOVE HIGH-VALUES TO A25-TSTAM-END-VLDT-NULL
        //           END-IF
        if (ws.getIndPers().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-TSTAM-END-VLDT-NULL
            pers.getA25TstamEndVldt().setA25TstamEndVldtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25TstamEndVldt.Len.A25_TSTAM_END_VLDT_NULL));
        }
        // COB_CODE: IF IND-A25-RIFTO-RETE = -1
        //              MOVE HIGH-VALUES TO A25-RIFTO-RETE-NULL
        //           END-IF
        if (ws.getIndPers().getTpCalcRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-RIFTO-RETE-NULL
            pers.setA25RiftoRete(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_RIFTO_RETE));
        }
        // COB_CODE: IF IND-A25-COD-PRT-IVA = -1
        //              MOVE HIGH-VALUES TO A25-COD-PRT-IVA-NULL
        //           END-IF
        if (ws.getIndPers().getUltRm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-PRT-IVA-NULL
            pers.setA25CodPrtIva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_PRT_IVA));
        }
        // COB_CODE: IF IND-A25-IND-PVCY-PRSNL = -1
        //              MOVE HIGH-VALUES TO A25-IND-PVCY-PRSNL-NULL
        //           END-IF
        if (ws.getIndPers().getDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-PVCY-PRSNL-NULL
            pers.setA25IndPvcyPrsnl(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-IND-PVCY-CMMRC = -1
        //              MOVE HIGH-VALUES TO A25-IND-PVCY-CMMRC-NULL
        //           END-IF
        if (ws.getIndPers().getDtElab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-PVCY-CMMRC-NULL
            pers.setA25IndPvcyCmmrc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-IND-PVCY-INDST = -1
        //              MOVE HIGH-VALUES TO A25-IND-PVCY-INDST-NULL
        //           END-IF
        if (ws.getIndPers().getRisBila() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-PVCY-INDST-NULL
            pers.setA25IndPvcyIndst(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-DT-NASC-CLI = -1
        //              MOVE HIGH-VALUES TO A25-DT-NASC-CLI-NULL
        //           END-IF
        if (ws.getIndPers().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-NASC-CLI-NULL
            pers.getA25DtNascCli().setA25DtNascCliNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtNascCli.Len.A25_DT_NASC_CLI_NULL));
        }
        // COB_CODE: IF IND-A25-DT-ACQS-PERS = -1
        //              MOVE HIGH-VALUES TO A25-DT-ACQS-PERS-NULL
        //           END-IF
        if (ws.getIndPers().getIncrXRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-ACQS-PERS-NULL
            pers.getA25DtAcqsPers().setA25DtAcqsPersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtAcqsPers.Len.A25_DT_ACQS_PERS_NULL));
        }
        // COB_CODE: IF IND-A25-IND-CLI = -1
        //              MOVE HIGH-VALUES TO A25-IND-CLI-NULL
        //           END-IF
        if (ws.getIndPers().getRptoPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-CLI-NULL
            pers.setA25IndCli(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-COD-CMN = -1
        //              MOVE HIGH-VALUES TO A25-COD-CMN-NULL
        //           END-IF
        if (ws.getIndPers().getFrazPrePp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-CMN-NULL
            pers.setA25CodCmn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_CMN));
        }
        // COB_CODE: IF IND-A25-COD-FRM-GIURD = -1
        //              MOVE HIGH-VALUES TO A25-COD-FRM-GIURD-NULL
        //           END-IF
        if (ws.getIndPers().getRisTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-FRM-GIURD-NULL
            pers.setA25CodFrmGiurd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_FRM_GIURD));
        }
        // COB_CODE: IF IND-A25-COD-ENTE-PUBB = -1
        //              MOVE HIGH-VALUES TO A25-COD-ENTE-PUBB-NULL
        //           END-IF
        if (ws.getIndPers().getRisSpe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-ENTE-PUBB-NULL
            pers.setA25CodEntePubb(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_ENTE_PUBB));
        }
        // COB_CODE: IF IND-A25-DEN-RGN-SOC = -1
        //              MOVE HIGH-VALUES TO A25-DEN-RGN-SOC
        //           END-IF
        if (ws.getIndPers().getRisAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-RGN-SOC
            pers.setA25DenRgnSoc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_RGN_SOC));
        }
        // COB_CODE: IF IND-A25-DEN-SIG-RGN-SOC = -1
        //              MOVE HIGH-VALUES TO A25-DEN-SIG-RGN-SOC
        //           END-IF
        if (ws.getIndPers().getRisBnsfdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-SIG-RGN-SOC
            pers.setA25DenSigRgnSoc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_SIG_RGN_SOC));
        }
        // COB_CODE: IF IND-A25-IND-ESE-FISC = -1
        //              MOVE HIGH-VALUES TO A25-IND-ESE-FISC-NULL
        //           END-IF
        if (ws.getIndPers().getRisSopr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-ESE-FISC-NULL
            pers.setA25IndEseFisc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-COD-STAT-CVL = -1
        //              MOVE HIGH-VALUES TO A25-COD-STAT-CVL-NULL
        //           END-IF
        if (ws.getIndPers().getRisIntegBasTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-STAT-CVL-NULL
            pers.setA25CodStatCvl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_STAT_CVL));
        }
        // COB_CODE: IF IND-A25-DEN-NOME = -1
        //              MOVE HIGH-VALUES TO A25-DEN-NOME
        //           END-IF
        if (ws.getIndPers().getRisIntegDecrTs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-NOME
            pers.setA25DenNome(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_NOME));
        }
        // COB_CODE: IF IND-A25-DEN-COGN = -1
        //              MOVE HIGH-VALUES TO A25-DEN-COGN
        //           END-IF
        if (ws.getIndPers().getRisGarCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-COGN
            pers.setA25DenCogn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_COGN));
        }
        // COB_CODE: IF IND-A25-COD-FISC = -1
        //              MOVE HIGH-VALUES TO A25-COD-FISC-NULL
        //           END-IF
        if (ws.getIndPers().getRisZil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-FISC-NULL
            pers.setA25CodFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_FISC));
        }
        // COB_CODE: IF IND-A25-IND-SEX = -1
        //              MOVE HIGH-VALUES TO A25-IND-SEX-NULL
        //           END-IF
        if (ws.getIndPers().getRisFaivl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-SEX-NULL
            pers.setA25IndSex(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-IND-CPCT-GIURD = -1
        //              MOVE HIGH-VALUES TO A25-IND-CPCT-GIURD-NULL
        //           END-IF
        if (ws.getIndPers().getRisCosAmmtz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-CPCT-GIURD-NULL
            pers.setA25IndCpctGiurd(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-IND-PORT-HDCP = -1
        //              MOVE HIGH-VALUES TO A25-IND-PORT-HDCP-NULL
        //           END-IF
        if (ws.getIndPers().getRisSpeFaivl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-IND-PORT-HDCP-NULL
            pers.setA25IndPortHdcp(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-A25-COD-USER-AGGM = -1
        //              MOVE HIGH-VALUES TO A25-COD-USER-AGGM-NULL
        //           END-IF
        if (ws.getIndPers().getRisPrestFaivl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-USER-AGGM-NULL
            pers.setA25CodUserAggm(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_USER_AGGM));
        }
        // COB_CODE: IF IND-A25-TSTAM-AGGM-RIGA = -1
        //              MOVE HIGH-VALUES TO A25-TSTAM-AGGM-RIGA-NULL
        //           END-IF
        if (ws.getIndPers().getRisComponAssva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-TSTAM-AGGM-RIGA-NULL
            pers.getA25TstamAggmRiga().setA25TstamAggmRigaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25TstamAggmRiga.Len.A25_TSTAM_AGGM_RIGA_NULL));
        }
        // COB_CODE: IF IND-A25-DEN-CMN-NASC-STRN = -1
        //              MOVE HIGH-VALUES TO A25-DEN-CMN-NASC-STRN
        //           END-IF
        if (ws.getIndPers().getUltCoeffRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DEN-CMN-NASC-STRN
            pers.setA25DenCmnNascStrn(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_DEN_CMN_NASC_STRN));
        }
        // COB_CODE: IF IND-A25-COD-RAMO-STGR = -1
        //              MOVE HIGH-VALUES TO A25-COD-RAMO-STGR-NULL
        //           END-IF
        if (ws.getIndPers().getUltCoeffAggRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-RAMO-STGR-NULL
            pers.setA25CodRamoStgr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_RAMO_STGR));
        }
        // COB_CODE: IF IND-A25-COD-STGR-ATVT-UIC = -1
        //              MOVE HIGH-VALUES TO A25-COD-STGR-ATVT-UIC-NULL
        //           END-IF
        if (ws.getIndPers().getRisAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-STGR-ATVT-UIC-NULL
            pers.setA25CodStgrAtvtUic(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_STGR_ATVT_UIC));
        }
        // COB_CODE: IF IND-A25-COD-RAMO-ATVT-UIC = -1
        //              MOVE HIGH-VALUES TO A25-COD-RAMO-ATVT-UIC-NULL
        //           END-IF
        if (ws.getIndPers().getRisUti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-RAMO-ATVT-UIC-NULL
            pers.setA25CodRamoAtvtUic(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_COD_RAMO_ATVT_UIC));
        }
        // COB_CODE: IF IND-A25-DT-END-VLDT-PERS = -1
        //              MOVE HIGH-VALUES TO A25-DT-END-VLDT-PERS-NULL
        //           END-IF
        if (ws.getIndPers().getRisMatEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-END-VLDT-PERS-NULL
            pers.getA25DtEndVldtPers().setA25DtEndVldtPersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtEndVldtPers.Len.A25_DT_END_VLDT_PERS_NULL));
        }
        // COB_CODE: IF IND-A25-DT-DEAD-PERS = -1
        //              MOVE HIGH-VALUES TO A25-DT-DEAD-PERS-NULL
        //           END-IF
        if (ws.getIndPers().getRisRistorniCap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-DEAD-PERS-NULL
            pers.getA25DtDeadPers().setA25DtDeadPersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtDeadPers.Len.A25_DT_DEAD_PERS_NULL));
        }
        // COB_CODE: IF IND-A25-TP-STAT-CLI = -1
        //              MOVE HIGH-VALUES TO A25-TP-STAT-CLI-NULL
        //           END-IF
        if (ws.getIndPers().getRisTrmBns() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-TP-STAT-CLI-NULL
            pers.setA25TpStatCli(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Pers.Len.A25_TP_STAT_CLI));
        }
        // COB_CODE: IF IND-A25-DT-BLOC-CLI = -1
        //              MOVE HIGH-VALUES TO A25-DT-BLOC-CLI-NULL
        //           END-IF
        if (ws.getIndPers().getRisBnsric() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-BLOC-CLI-NULL
            pers.getA25DtBlocCli().setA25DtBlocCliNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtBlocCli.Len.A25_DT_BLOC_CLI_NULL));
        }
        // COB_CODE: IF IND-A25-COD-PERS-SECOND = -1
        //              MOVE HIGH-VALUES TO A25-COD-PERS-SECOND-NULL
        //           END-IF
        if (ws.getIndPers().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-COD-PERS-SECOND-NULL
            pers.getA25CodPersSecond().setA25CodPersSecondNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25CodPersSecond.Len.A25_COD_PERS_SECOND_NULL));
        }
        // COB_CODE: IF IND-A25-ID-SEGMENTAZ-CLI = -1
        //              MOVE HIGH-VALUES TO A25-ID-SEGMENTAZ-CLI-NULL
        //           END-IF
        if (ws.getIndPers().getRisMinGarto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-ID-SEGMENTAZ-CLI-NULL
            pers.getA25IdSegmentazCli().setA25IdSegmentazCliNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25IdSegmentazCli.Len.A25_ID_SEGMENTAZ_CLI_NULL));
        }
        // COB_CODE: IF IND-A25-DT-1A-ATVT = -1
        //              MOVE HIGH-VALUES TO A25-DT-1A-ATVT-NULL
        //           END-IF
        if (ws.getIndPers().getRisRshDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-1A-ATVT-NULL
            pers.getA25Dt1aAtvt().setA25Dt1aAtvtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25Dt1aAtvt.Len.A25_DT1A_ATVT_NULL));
        }
        // COB_CODE: IF IND-A25-DT-SEGNAL-PARTNER = -1
        //              MOVE HIGH-VALUES TO A25-DT-SEGNAL-PARTNER-NULL
        //           END-IF.
        if (ws.getIndPers().getRisMoviNonInves() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO A25-DT-SEGNAL-PARTNER-NULL
            pers.getA25DtSegnalPartner().setA25DtSegnalPartnerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, A25DtSegnalPartner.Len.A25_DT_SEGNAL_PARTNER_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF A25-TSTAM-END-VLDT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-TSTAM-END-VLDT
        //           ELSE
        //              MOVE 0 TO IND-A25-TSTAM-END-VLDT
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25TstamEndVldt().getA25TstamEndVldtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-TSTAM-END-VLDT
            ws.getIndPers().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-TSTAM-END-VLDT
            ws.getIndPers().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF A25-RIFTO-RETE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-RIFTO-RETE
        //           ELSE
        //              MOVE 0 TO IND-A25-RIFTO-RETE
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25RiftoReteFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-RIFTO-RETE
            ws.getIndPers().setTpCalcRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-RIFTO-RETE
            ws.getIndPers().setTpCalcRis(((short)0));
        }
        // COB_CODE: IF A25-COD-PRT-IVA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-PRT-IVA
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-PRT-IVA
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodPrtIvaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-PRT-IVA
            ws.getIndPers().setUltRm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-PRT-IVA
            ws.getIndPers().setUltRm(((short)0));
        }
        // COB_CODE: IF A25-IND-PVCY-PRSNL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-IND-PVCY-PRSNL
        //           ELSE
        //              MOVE 0 TO IND-A25-IND-PVCY-PRSNL
        //           END-IF
        if (Conditions.eq(pers.getA25IndPvcyPrsnl(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-A25-IND-PVCY-PRSNL
            ws.getIndPers().setDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-IND-PVCY-PRSNL
            ws.getIndPers().setDtCalc(((short)0));
        }
        // COB_CODE: IF A25-IND-PVCY-CMMRC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-IND-PVCY-CMMRC
        //           ELSE
        //              MOVE 0 TO IND-A25-IND-PVCY-CMMRC
        //           END-IF
        if (Conditions.eq(pers.getA25IndPvcyCmmrc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-A25-IND-PVCY-CMMRC
            ws.getIndPers().setDtElab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-IND-PVCY-CMMRC
            ws.getIndPers().setDtElab(((short)0));
        }
        // COB_CODE: IF A25-IND-PVCY-INDST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-IND-PVCY-INDST
        //           ELSE
        //              MOVE 0 TO IND-A25-IND-PVCY-INDST
        //           END-IF
        if (Conditions.eq(pers.getA25IndPvcyIndst(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-A25-IND-PVCY-INDST
            ws.getIndPers().setRisBila(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-IND-PVCY-INDST
            ws.getIndPers().setRisBila(((short)0));
        }
        // COB_CODE: IF A25-DT-NASC-CLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DT-NASC-CLI
        //           ELSE
        //              MOVE 0 TO IND-A25-DT-NASC-CLI
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DtNascCli().getA25DtNascCliNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-DT-NASC-CLI
            ws.getIndPers().setRisMat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DT-NASC-CLI
            ws.getIndPers().setRisMat(((short)0));
        }
        // COB_CODE: IF A25-DT-ACQS-PERS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DT-ACQS-PERS
        //           ELSE
        //              MOVE 0 TO IND-A25-DT-ACQS-PERS
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DtAcqsPers().getA25DtAcqsPersNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-DT-ACQS-PERS
            ws.getIndPers().setIncrXRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DT-ACQS-PERS
            ws.getIndPers().setIncrXRival(((short)0));
        }
        // COB_CODE: IF A25-IND-CLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-IND-CLI
        //           ELSE
        //              MOVE 0 TO IND-A25-IND-CLI
        //           END-IF
        if (Conditions.eq(pers.getA25IndCli(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-A25-IND-CLI
            ws.getIndPers().setRptoPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-IND-CLI
            ws.getIndPers().setRptoPre(((short)0));
        }
        // COB_CODE: IF A25-COD-CMN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-CMN
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-CMN
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodCmnFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-CMN
            ws.getIndPers().setFrazPrePp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-CMN
            ws.getIndPers().setFrazPrePp(((short)0));
        }
        // COB_CODE: IF A25-COD-FRM-GIURD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-FRM-GIURD
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-FRM-GIURD
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodFrmGiurdFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-FRM-GIURD
            ws.getIndPers().setRisTot(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-FRM-GIURD
            ws.getIndPers().setRisTot(((short)0));
        }
        // COB_CODE: IF A25-COD-ENTE-PUBB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-ENTE-PUBB
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-ENTE-PUBB
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodEntePubbFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-ENTE-PUBB
            ws.getIndPers().setRisSpe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-ENTE-PUBB
            ws.getIndPers().setRisSpe(((short)0));
        }
        // COB_CODE: IF A25-DEN-RGN-SOC = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DEN-RGN-SOC
        //           ELSE
        //              MOVE 0 TO IND-A25-DEN-RGN-SOC
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DenRgnSoc(), Pers.Len.A25_DEN_RGN_SOC)) {
            // COB_CODE: MOVE -1 TO IND-A25-DEN-RGN-SOC
            ws.getIndPers().setRisAbb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DEN-RGN-SOC
            ws.getIndPers().setRisAbb(((short)0));
        }
        // COB_CODE: IF A25-DEN-SIG-RGN-SOC = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DEN-SIG-RGN-SOC
        //           ELSE
        //              MOVE 0 TO IND-A25-DEN-SIG-RGN-SOC
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DenSigRgnSoc(), Pers.Len.A25_DEN_SIG_RGN_SOC)) {
            // COB_CODE: MOVE -1 TO IND-A25-DEN-SIG-RGN-SOC
            ws.getIndPers().setRisBnsfdt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DEN-SIG-RGN-SOC
            ws.getIndPers().setRisBnsfdt(((short)0));
        }
        // COB_CODE: IF A25-IND-ESE-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-IND-ESE-FISC
        //           ELSE
        //              MOVE 0 TO IND-A25-IND-ESE-FISC
        //           END-IF
        if (Conditions.eq(pers.getA25IndEseFisc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-A25-IND-ESE-FISC
            ws.getIndPers().setRisSopr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-IND-ESE-FISC
            ws.getIndPers().setRisSopr(((short)0));
        }
        // COB_CODE: IF A25-COD-STAT-CVL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-STAT-CVL
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-STAT-CVL
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodStatCvlFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-STAT-CVL
            ws.getIndPers().setRisIntegBasTec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-STAT-CVL
            ws.getIndPers().setRisIntegBasTec(((short)0));
        }
        // COB_CODE: IF A25-DEN-NOME = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DEN-NOME
        //           ELSE
        //              MOVE 0 TO IND-A25-DEN-NOME
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DenNome(), Pers.Len.A25_DEN_NOME)) {
            // COB_CODE: MOVE -1 TO IND-A25-DEN-NOME
            ws.getIndPers().setRisIntegDecrTs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DEN-NOME
            ws.getIndPers().setRisIntegDecrTs(((short)0));
        }
        // COB_CODE: IF A25-DEN-COGN = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DEN-COGN
        //           ELSE
        //              MOVE 0 TO IND-A25-DEN-COGN
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DenCogn(), Pers.Len.A25_DEN_COGN)) {
            // COB_CODE: MOVE -1 TO IND-A25-DEN-COGN
            ws.getIndPers().setRisGarCasoMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DEN-COGN
            ws.getIndPers().setRisGarCasoMor(((short)0));
        }
        // COB_CODE: IF A25-COD-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-FISC
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodFiscFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-FISC
            ws.getIndPers().setRisZil(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-FISC
            ws.getIndPers().setRisZil(((short)0));
        }
        // COB_CODE: IF A25-IND-SEX-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-IND-SEX
        //           ELSE
        //              MOVE 0 TO IND-A25-IND-SEX
        //           END-IF
        if (Conditions.eq(pers.getA25IndSex(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-A25-IND-SEX
            ws.getIndPers().setRisFaivl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-IND-SEX
            ws.getIndPers().setRisFaivl(((short)0));
        }
        // COB_CODE: IF A25-IND-CPCT-GIURD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-IND-CPCT-GIURD
        //           ELSE
        //              MOVE 0 TO IND-A25-IND-CPCT-GIURD
        //           END-IF
        if (Conditions.eq(pers.getA25IndCpctGiurd(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-A25-IND-CPCT-GIURD
            ws.getIndPers().setRisCosAmmtz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-IND-CPCT-GIURD
            ws.getIndPers().setRisCosAmmtz(((short)0));
        }
        // COB_CODE: IF A25-IND-PORT-HDCP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-IND-PORT-HDCP
        //           ELSE
        //              MOVE 0 TO IND-A25-IND-PORT-HDCP
        //           END-IF
        if (Conditions.eq(pers.getA25IndPortHdcp(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-A25-IND-PORT-HDCP
            ws.getIndPers().setRisSpeFaivl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-IND-PORT-HDCP
            ws.getIndPers().setRisSpeFaivl(((short)0));
        }
        // COB_CODE: IF A25-COD-USER-AGGM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-USER-AGGM
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-USER-AGGM
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodUserAggmFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-USER-AGGM
            ws.getIndPers().setRisPrestFaivl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-USER-AGGM
            ws.getIndPers().setRisPrestFaivl(((short)0));
        }
        // COB_CODE: IF A25-TSTAM-AGGM-RIGA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-TSTAM-AGGM-RIGA
        //           ELSE
        //              MOVE 0 TO IND-A25-TSTAM-AGGM-RIGA
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25TstamAggmRiga().getA25TstamAggmRigaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-TSTAM-AGGM-RIGA
            ws.getIndPers().setRisComponAssva(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-TSTAM-AGGM-RIGA
            ws.getIndPers().setRisComponAssva(((short)0));
        }
        // COB_CODE: IF A25-DEN-CMN-NASC-STRN = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DEN-CMN-NASC-STRN
        //           ELSE
        //              MOVE 0 TO IND-A25-DEN-CMN-NASC-STRN
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DenCmnNascStrn(), Pers.Len.A25_DEN_CMN_NASC_STRN)) {
            // COB_CODE: MOVE -1 TO IND-A25-DEN-CMN-NASC-STRN
            ws.getIndPers().setUltCoeffRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DEN-CMN-NASC-STRN
            ws.getIndPers().setUltCoeffRis(((short)0));
        }
        // COB_CODE: IF A25-COD-RAMO-STGR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-RAMO-STGR
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-RAMO-STGR
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodRamoStgrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-RAMO-STGR
            ws.getIndPers().setUltCoeffAggRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-RAMO-STGR
            ws.getIndPers().setUltCoeffAggRis(((short)0));
        }
        // COB_CODE: IF A25-COD-STGR-ATVT-UIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-STGR-ATVT-UIC
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-STGR-ATVT-UIC
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodStgrAtvtUicFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-STGR-ATVT-UIC
            ws.getIndPers().setRisAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-STGR-ATVT-UIC
            ws.getIndPers().setRisAcq(((short)0));
        }
        // COB_CODE: IF A25-COD-RAMO-ATVT-UIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-RAMO-ATVT-UIC
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-RAMO-ATVT-UIC
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodRamoAtvtUicFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-RAMO-ATVT-UIC
            ws.getIndPers().setRisUti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-RAMO-ATVT-UIC
            ws.getIndPers().setRisUti(((short)0));
        }
        // COB_CODE: IF A25-DT-END-VLDT-PERS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DT-END-VLDT-PERS
        //           ELSE
        //              MOVE 0 TO IND-A25-DT-END-VLDT-PERS
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DtEndVldtPers().getA25DtEndVldtPersNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-DT-END-VLDT-PERS
            ws.getIndPers().setRisMatEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DT-END-VLDT-PERS
            ws.getIndPers().setRisMatEff(((short)0));
        }
        // COB_CODE: IF A25-DT-DEAD-PERS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DT-DEAD-PERS
        //           ELSE
        //              MOVE 0 TO IND-A25-DT-DEAD-PERS
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DtDeadPers().getA25DtDeadPersNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-DT-DEAD-PERS
            ws.getIndPers().setRisRistorniCap(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DT-DEAD-PERS
            ws.getIndPers().setRisRistorniCap(((short)0));
        }
        // COB_CODE: IF A25-TP-STAT-CLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-TP-STAT-CLI
        //           ELSE
        //              MOVE 0 TO IND-A25-TP-STAT-CLI
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25TpStatCliFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-TP-STAT-CLI
            ws.getIndPers().setRisTrmBns(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-TP-STAT-CLI
            ws.getIndPers().setRisTrmBns(((short)0));
        }
        // COB_CODE: IF A25-DT-BLOC-CLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DT-BLOC-CLI
        //           ELSE
        //              MOVE 0 TO IND-A25-DT-BLOC-CLI
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25DtBlocCli().getA25DtBlocCliNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-DT-BLOC-CLI
            ws.getIndPers().setRisBnsric(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DT-BLOC-CLI
            ws.getIndPers().setRisBnsric(((short)0));
        }
        // COB_CODE: IF A25-COD-PERS-SECOND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-COD-PERS-SECOND
        //           ELSE
        //              MOVE 0 TO IND-A25-COD-PERS-SECOND
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25CodPersSecond().getA25CodPersSecondNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-COD-PERS-SECOND
            ws.getIndPers().setCodFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-COD-PERS-SECOND
            ws.getIndPers().setCodFnd(((short)0));
        }
        // COB_CODE: IF A25-ID-SEGMENTAZ-CLI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-ID-SEGMENTAZ-CLI
        //           ELSE
        //              MOVE 0 TO IND-A25-ID-SEGMENTAZ-CLI
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25IdSegmentazCli().getA25IdSegmentazCliNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-ID-SEGMENTAZ-CLI
            ws.getIndPers().setRisMinGarto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-ID-SEGMENTAZ-CLI
            ws.getIndPers().setRisMinGarto(((short)0));
        }
        // COB_CODE: IF A25-DT-1A-ATVT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DT-1A-ATVT
        //           ELSE
        //              MOVE 0 TO IND-A25-DT-1A-ATVT
        //           END-IF
        if (Characters.EQ_HIGH.test(pers.getA25Dt1aAtvt().getA25Dt1aAtvtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-DT-1A-ATVT
            ws.getIndPers().setRisRshDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DT-1A-ATVT
            ws.getIndPers().setRisRshDflt(((short)0));
        }
        // COB_CODE: IF A25-DT-SEGNAL-PARTNER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-A25-DT-SEGNAL-PARTNER
        //           ELSE
        //              MOVE 0 TO IND-A25-DT-SEGNAL-PARTNER
        //           END-IF.
        if (Characters.EQ_HIGH.test(pers.getA25DtSegnalPartner().getA25DtSegnalPartnerNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-A25-DT-SEGNAL-PARTNER
            ws.getIndPers().setRisMoviNonInves(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-A25-DT-SEGNAL-PARTNER
            ws.getIndPers().setRisMoviNonInves(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: IF IND-A25-DT-NASC-CLI = 0
        //               MOVE WS-DATE-X      TO A25-DT-NASC-CLI-DB
        //           END-IF
        if (ws.getIndPers().getRisMat() == 0) {
            // COB_CODE: MOVE A25-DT-NASC-CLI TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(pers.getA25DtNascCli().getA25DtNascCli(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO A25-DT-NASC-CLI-DB
            ws.getPersDb().setNascCliDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-A25-DT-ACQS-PERS = 0
        //               MOVE WS-DATE-X      TO A25-DT-ACQS-PERS-DB
        //           END-IF
        if (ws.getIndPers().getIncrXRival() == 0) {
            // COB_CODE: MOVE A25-DT-ACQS-PERS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(pers.getA25DtAcqsPers().getA25DtAcqsPers(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO A25-DT-ACQS-PERS-DB
            ws.getPersDb().setAcqsPersDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-A25-DT-END-VLDT-PERS = 0
        //               MOVE WS-DATE-X      TO A25-DT-END-VLDT-PERS-DB
        //           END-IF
        if (ws.getIndPers().getRisMatEff() == 0) {
            // COB_CODE: MOVE A25-DT-END-VLDT-PERS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(pers.getA25DtEndVldtPers().getA25DtEndVldtPers(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO A25-DT-END-VLDT-PERS-DB
            ws.getPersDb().setEndVldtPersDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-A25-DT-DEAD-PERS = 0
        //               MOVE WS-DATE-X      TO A25-DT-DEAD-PERS-DB
        //           END-IF
        if (ws.getIndPers().getRisRistorniCap() == 0) {
            // COB_CODE: MOVE A25-DT-DEAD-PERS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(pers.getA25DtDeadPers().getA25DtDeadPers(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO A25-DT-DEAD-PERS-DB
            ws.getPersDb().setDeadPersDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-A25-DT-BLOC-CLI = 0
        //               MOVE WS-DATE-X      TO A25-DT-BLOC-CLI-DB
        //           END-IF
        if (ws.getIndPers().getRisBnsric() == 0) {
            // COB_CODE: MOVE A25-DT-BLOC-CLI TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(pers.getA25DtBlocCli().getA25DtBlocCli(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO A25-DT-BLOC-CLI-DB
            ws.getPersDb().setBlocCliDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-A25-DT-1A-ATVT = 0
        //               MOVE WS-DATE-X      TO A25-DT-1A-ATVT-DB
        //           END-IF
        if (ws.getIndPers().getRisRshDflt() == 0) {
            // COB_CODE: MOVE A25-DT-1A-ATVT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(pers.getA25Dt1aAtvt().getA25Dt1aAtvt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO A25-DT-1A-ATVT-DB
            ws.getPersDb().setDt1aAtvtDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-A25-DT-SEGNAL-PARTNER = 0
        //               MOVE WS-DATE-X      TO A25-DT-SEGNAL-PARTNER-DB
        //           END-IF.
        if (ws.getIndPers().getRisMoviNonInves() == 0) {
            // COB_CODE: MOVE A25-DT-SEGNAL-PARTNER TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(pers.getA25DtSegnalPartner().getA25DtSegnalPartner(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO A25-DT-SEGNAL-PARTNER-DB
            ws.getPersDb().setSegnalPartnerDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: IF IND-A25-DT-NASC-CLI = 0
        //               MOVE WS-DATE-N      TO A25-DT-NASC-CLI
        //           END-IF
        if (ws.getIndPers().getRisMat() == 0) {
            // COB_CODE: MOVE A25-DT-NASC-CLI-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getNascCliDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-NASC-CLI
            pers.getA25DtNascCli().setA25DtNascCli(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-ACQS-PERS = 0
        //               MOVE WS-DATE-N      TO A25-DT-ACQS-PERS
        //           END-IF
        if (ws.getIndPers().getIncrXRival() == 0) {
            // COB_CODE: MOVE A25-DT-ACQS-PERS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getAcqsPersDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-ACQS-PERS
            pers.getA25DtAcqsPers().setA25DtAcqsPers(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-END-VLDT-PERS = 0
        //               MOVE WS-DATE-N      TO A25-DT-END-VLDT-PERS
        //           END-IF
        if (ws.getIndPers().getRisMatEff() == 0) {
            // COB_CODE: MOVE A25-DT-END-VLDT-PERS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getEndVldtPersDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-END-VLDT-PERS
            pers.getA25DtEndVldtPers().setA25DtEndVldtPers(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-DEAD-PERS = 0
        //               MOVE WS-DATE-N      TO A25-DT-DEAD-PERS
        //           END-IF
        if (ws.getIndPers().getRisRistorniCap() == 0) {
            // COB_CODE: MOVE A25-DT-DEAD-PERS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getDeadPersDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-DEAD-PERS
            pers.getA25DtDeadPers().setA25DtDeadPers(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-BLOC-CLI = 0
        //               MOVE WS-DATE-N      TO A25-DT-BLOC-CLI
        //           END-IF
        if (ws.getIndPers().getRisBnsric() == 0) {
            // COB_CODE: MOVE A25-DT-BLOC-CLI-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getBlocCliDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-BLOC-CLI
            pers.getA25DtBlocCli().setA25DtBlocCli(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-1A-ATVT = 0
        //               MOVE WS-DATE-N      TO A25-DT-1A-ATVT
        //           END-IF
        if (ws.getIndPers().getRisRshDflt() == 0) {
            // COB_CODE: MOVE A25-DT-1A-ATVT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getDt1aAtvtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-1A-ATVT
            pers.getA25Dt1aAtvt().setA25Dt1aAtvt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-A25-DT-SEGNAL-PARTNER = 0
        //               MOVE WS-DATE-N      TO A25-DT-SEGNAL-PARTNER
        //           END-IF.
        if (ws.getIndPers().getRisMoviNonInves() == 0) {
            // COB_CODE: MOVE A25-DT-SEGNAL-PARTNER-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPersDb().getSegnalPartnerDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO A25-DT-SEGNAL-PARTNER
            pers.getA25DtSegnalPartner().setA25DtSegnalPartner(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF A25-DEN-RGN-SOC
        //                       TO A25-DEN-RGN-SOC-LEN
        pers.setA25DenRgnSocLen(((short)Pers.Len.A25_DEN_RGN_SOC));
        // COB_CODE: MOVE LENGTH OF A25-DEN-SIG-RGN-SOC
        //                       TO A25-DEN-SIG-RGN-SOC-LEN
        pers.setA25DenSigRgnSocLen(((short)Pers.Len.A25_DEN_SIG_RGN_SOC));
        // COB_CODE: MOVE LENGTH OF A25-DEN-NOME
        //                       TO A25-DEN-NOME-LEN
        pers.setA25DenNomeLen(((short)Pers.Len.A25_DEN_NOME));
        // COB_CODE: MOVE LENGTH OF A25-DEN-COGN
        //                       TO A25-DEN-COGN-LEN
        pers.setA25DenCognLen(((short)Pers.Len.A25_DEN_COGN));
        // COB_CODE: MOVE LENGTH OF A25-DEN-CMN-NASC-STRN
        //                       TO A25-DEN-CMN-NASC-STRN-LEN.
        pers.setA25DenCmnNascStrnLen(((short)Pers.Len.A25_DEN_CMN_NASC_STRN));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public String getCodCmn() {
        return pers.getA25CodCmn();
    }

    @Override
    public void setCodCmn(String codCmn) {
        this.pers.setA25CodCmn(codCmn);
    }

    @Override
    public String getCodCmnObj() {
        if (ws.getIndPers().getFrazPrePp() >= 0) {
            return getCodCmn();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodCmnObj(String codCmnObj) {
        if (codCmnObj != null) {
            setCodCmn(codCmnObj);
            ws.getIndPers().setFrazPrePp(((short)0));
        }
        else {
            ws.getIndPers().setFrazPrePp(((short)-1));
        }
    }

    @Override
    public String getCodEntePubb() {
        return pers.getA25CodEntePubb();
    }

    @Override
    public void setCodEntePubb(String codEntePubb) {
        this.pers.setA25CodEntePubb(codEntePubb);
    }

    @Override
    public String getCodEntePubbObj() {
        if (ws.getIndPers().getRisSpe() >= 0) {
            return getCodEntePubb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodEntePubbObj(String codEntePubbObj) {
        if (codEntePubbObj != null) {
            setCodEntePubb(codEntePubbObj);
            ws.getIndPers().setRisSpe(((short)0));
        }
        else {
            ws.getIndPers().setRisSpe(((short)-1));
        }
    }

    @Override
    public String getCodFisc() {
        return pers.getA25CodFisc();
    }

    @Override
    public void setCodFisc(String codFisc) {
        this.pers.setA25CodFisc(codFisc);
    }

    @Override
    public String getCodFiscObj() {
        if (ws.getIndPers().getRisZil() >= 0) {
            return getCodFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFiscObj(String codFiscObj) {
        if (codFiscObj != null) {
            setCodFisc(codFiscObj);
            ws.getIndPers().setRisZil(((short)0));
        }
        else {
            ws.getIndPers().setRisZil(((short)-1));
        }
    }

    @Override
    public String getCodFrmGiurd() {
        return pers.getA25CodFrmGiurd();
    }

    @Override
    public void setCodFrmGiurd(String codFrmGiurd) {
        this.pers.setA25CodFrmGiurd(codFrmGiurd);
    }

    @Override
    public String getCodFrmGiurdObj() {
        if (ws.getIndPers().getRisTot() >= 0) {
            return getCodFrmGiurd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFrmGiurdObj(String codFrmGiurdObj) {
        if (codFrmGiurdObj != null) {
            setCodFrmGiurd(codFrmGiurdObj);
            ws.getIndPers().setRisTot(((short)0));
        }
        else {
            ws.getIndPers().setRisTot(((short)-1));
        }
    }

    @Override
    public long getCodPers() {
        return pers.getA25CodPers();
    }

    @Override
    public void setCodPers(long codPers) {
        this.pers.setA25CodPers(codPers);
    }

    @Override
    public long getCodPersSecond() {
        return pers.getA25CodPersSecond().getA25CodPersSecond();
    }

    @Override
    public void setCodPersSecond(long codPersSecond) {
        this.pers.getA25CodPersSecond().setA25CodPersSecond(codPersSecond);
    }

    @Override
    public Long getCodPersSecondObj() {
        if (ws.getIndPers().getCodFnd() >= 0) {
            return ((Long)getCodPersSecond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPersSecondObj(Long codPersSecondObj) {
        if (codPersSecondObj != null) {
            setCodPersSecond(((long)codPersSecondObj));
            ws.getIndPers().setCodFnd(((short)0));
        }
        else {
            ws.getIndPers().setCodFnd(((short)-1));
        }
    }

    @Override
    public String getCodPrtIva() {
        return pers.getA25CodPrtIva();
    }

    @Override
    public void setCodPrtIva(String codPrtIva) {
        this.pers.setA25CodPrtIva(codPrtIva);
    }

    @Override
    public String getCodPrtIvaObj() {
        if (ws.getIndPers().getUltRm() >= 0) {
            return getCodPrtIva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodPrtIvaObj(String codPrtIvaObj) {
        if (codPrtIvaObj != null) {
            setCodPrtIva(codPrtIvaObj);
            ws.getIndPers().setUltRm(((short)0));
        }
        else {
            ws.getIndPers().setUltRm(((short)-1));
        }
    }

    @Override
    public String getCodRamoAtvtUic() {
        return pers.getA25CodRamoAtvtUic();
    }

    @Override
    public void setCodRamoAtvtUic(String codRamoAtvtUic) {
        this.pers.setA25CodRamoAtvtUic(codRamoAtvtUic);
    }

    @Override
    public String getCodRamoAtvtUicObj() {
        if (ws.getIndPers().getRisUti() >= 0) {
            return getCodRamoAtvtUic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodRamoAtvtUicObj(String codRamoAtvtUicObj) {
        if (codRamoAtvtUicObj != null) {
            setCodRamoAtvtUic(codRamoAtvtUicObj);
            ws.getIndPers().setRisUti(((short)0));
        }
        else {
            ws.getIndPers().setRisUti(((short)-1));
        }
    }

    @Override
    public String getCodRamoStgr() {
        return pers.getA25CodRamoStgr();
    }

    @Override
    public void setCodRamoStgr(String codRamoStgr) {
        this.pers.setA25CodRamoStgr(codRamoStgr);
    }

    @Override
    public String getCodRamoStgrObj() {
        if (ws.getIndPers().getUltCoeffAggRis() >= 0) {
            return getCodRamoStgr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodRamoStgrObj(String codRamoStgrObj) {
        if (codRamoStgrObj != null) {
            setCodRamoStgr(codRamoStgrObj);
            ws.getIndPers().setUltCoeffAggRis(((short)0));
        }
        else {
            ws.getIndPers().setUltCoeffAggRis(((short)-1));
        }
    }

    @Override
    public String getCodStatCvl() {
        return pers.getA25CodStatCvl();
    }

    @Override
    public void setCodStatCvl(String codStatCvl) {
        this.pers.setA25CodStatCvl(codStatCvl);
    }

    @Override
    public String getCodStatCvlObj() {
        if (ws.getIndPers().getRisIntegBasTec() >= 0) {
            return getCodStatCvl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodStatCvlObj(String codStatCvlObj) {
        if (codStatCvlObj != null) {
            setCodStatCvl(codStatCvlObj);
            ws.getIndPers().setRisIntegBasTec(((short)0));
        }
        else {
            ws.getIndPers().setRisIntegBasTec(((short)-1));
        }
    }

    @Override
    public String getCodStgrAtvtUic() {
        return pers.getA25CodStgrAtvtUic();
    }

    @Override
    public void setCodStgrAtvtUic(String codStgrAtvtUic) {
        this.pers.setA25CodStgrAtvtUic(codStgrAtvtUic);
    }

    @Override
    public String getCodStgrAtvtUicObj() {
        if (ws.getIndPers().getRisAcq() >= 0) {
            return getCodStgrAtvtUic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodStgrAtvtUicObj(String codStgrAtvtUicObj) {
        if (codStgrAtvtUicObj != null) {
            setCodStgrAtvtUic(codStgrAtvtUicObj);
            ws.getIndPers().setRisAcq(((short)0));
        }
        else {
            ws.getIndPers().setRisAcq(((short)-1));
        }
    }

    @Override
    public String getCodUserAggm() {
        return pers.getA25CodUserAggm();
    }

    @Override
    public void setCodUserAggm(String codUserAggm) {
        this.pers.setA25CodUserAggm(codUserAggm);
    }

    @Override
    public String getCodUserAggmObj() {
        if (ws.getIndPers().getRisPrestFaivl() >= 0) {
            return getCodUserAggm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodUserAggmObj(String codUserAggmObj) {
        if (codUserAggmObj != null) {
            setCodUserAggm(codUserAggmObj);
            ws.getIndPers().setRisPrestFaivl(((short)0));
        }
        else {
            ws.getIndPers().setRisPrestFaivl(((short)-1));
        }
    }

    @Override
    public String getCodUserIns() {
        return pers.getA25CodUserIns();
    }

    @Override
    public void setCodUserIns(String codUserIns) {
        this.pers.setA25CodUserIns(codUserIns);
    }

    @Override
    public String getDenCmnNascStrnVchar() {
        return pers.getA25DenCmnNascStrnVcharFormatted();
    }

    @Override
    public void setDenCmnNascStrnVchar(String denCmnNascStrnVchar) {
        this.pers.setA25DenCmnNascStrnVcharFormatted(denCmnNascStrnVchar);
    }

    @Override
    public String getDenCmnNascStrnVcharObj() {
        if (ws.getIndPers().getUltCoeffRis() >= 0) {
            return getDenCmnNascStrnVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenCmnNascStrnVcharObj(String denCmnNascStrnVcharObj) {
        if (denCmnNascStrnVcharObj != null) {
            setDenCmnNascStrnVchar(denCmnNascStrnVcharObj);
            ws.getIndPers().setUltCoeffRis(((short)0));
        }
        else {
            ws.getIndPers().setUltCoeffRis(((short)-1));
        }
    }

    @Override
    public String getDenCognVchar() {
        return pers.getA25DenCognVcharFormatted();
    }

    @Override
    public void setDenCognVchar(String denCognVchar) {
        this.pers.setA25DenCognVcharFormatted(denCognVchar);
    }

    @Override
    public String getDenCognVcharObj() {
        if (ws.getIndPers().getRisGarCasoMor() >= 0) {
            return getDenCognVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenCognVcharObj(String denCognVcharObj) {
        if (denCognVcharObj != null) {
            setDenCognVchar(denCognVcharObj);
            ws.getIndPers().setRisGarCasoMor(((short)0));
        }
        else {
            ws.getIndPers().setRisGarCasoMor(((short)-1));
        }
    }

    @Override
    public String getDenNomeVchar() {
        return pers.getA25DenNomeVcharFormatted();
    }

    @Override
    public void setDenNomeVchar(String denNomeVchar) {
        this.pers.setA25DenNomeVcharFormatted(denNomeVchar);
    }

    @Override
    public String getDenNomeVcharObj() {
        if (ws.getIndPers().getRisIntegDecrTs() >= 0) {
            return getDenNomeVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenNomeVcharObj(String denNomeVcharObj) {
        if (denNomeVcharObj != null) {
            setDenNomeVchar(denNomeVcharObj);
            ws.getIndPers().setRisIntegDecrTs(((short)0));
        }
        else {
            ws.getIndPers().setRisIntegDecrTs(((short)-1));
        }
    }

    @Override
    public String getDenRgnSocVchar() {
        return pers.getA25DenRgnSocVcharFormatted();
    }

    @Override
    public void setDenRgnSocVchar(String denRgnSocVchar) {
        this.pers.setA25DenRgnSocVcharFormatted(denRgnSocVchar);
    }

    @Override
    public String getDenRgnSocVcharObj() {
        if (ws.getIndPers().getRisAbb() >= 0) {
            return getDenRgnSocVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenRgnSocVcharObj(String denRgnSocVcharObj) {
        if (denRgnSocVcharObj != null) {
            setDenRgnSocVchar(denRgnSocVcharObj);
            ws.getIndPers().setRisAbb(((short)0));
        }
        else {
            ws.getIndPers().setRisAbb(((short)-1));
        }
    }

    @Override
    public String getDenSigRgnSocVchar() {
        return pers.getA25DenSigRgnSocVcharFormatted();
    }

    @Override
    public void setDenSigRgnSocVchar(String denSigRgnSocVchar) {
        this.pers.setA25DenSigRgnSocVcharFormatted(denSigRgnSocVchar);
    }

    @Override
    public String getDenSigRgnSocVcharObj() {
        if (ws.getIndPers().getRisBnsfdt() >= 0) {
            return getDenSigRgnSocVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDenSigRgnSocVcharObj(String denSigRgnSocVcharObj) {
        if (denSigRgnSocVcharObj != null) {
            setDenSigRgnSocVchar(denSigRgnSocVcharObj);
            ws.getIndPers().setRisBnsfdt(((short)0));
        }
        else {
            ws.getIndPers().setRisBnsfdt(((short)-1));
        }
    }

    @Override
    public String getDt1aAtvtDb() {
        return ws.getPersDb().getDt1aAtvtDb();
    }

    @Override
    public void setDt1aAtvtDb(String dt1aAtvtDb) {
        this.ws.getPersDb().setDt1aAtvtDb(dt1aAtvtDb);
    }

    @Override
    public String getDt1aAtvtDbObj() {
        if (ws.getIndPers().getRisRshDflt() >= 0) {
            return getDt1aAtvtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDt1aAtvtDbObj(String dt1aAtvtDbObj) {
        if (dt1aAtvtDbObj != null) {
            setDt1aAtvtDb(dt1aAtvtDbObj);
            ws.getIndPers().setRisRshDflt(((short)0));
        }
        else {
            ws.getIndPers().setRisRshDflt(((short)-1));
        }
    }

    @Override
    public String getDtAcqsPersDb() {
        return ws.getPersDb().getAcqsPersDb();
    }

    @Override
    public void setDtAcqsPersDb(String dtAcqsPersDb) {
        this.ws.getPersDb().setAcqsPersDb(dtAcqsPersDb);
    }

    @Override
    public String getDtAcqsPersDbObj() {
        if (ws.getIndPers().getIncrXRival() >= 0) {
            return getDtAcqsPersDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtAcqsPersDbObj(String dtAcqsPersDbObj) {
        if (dtAcqsPersDbObj != null) {
            setDtAcqsPersDb(dtAcqsPersDbObj);
            ws.getIndPers().setIncrXRival(((short)0));
        }
        else {
            ws.getIndPers().setIncrXRival(((short)-1));
        }
    }

    @Override
    public String getDtBlocCliDb() {
        return ws.getPersDb().getBlocCliDb();
    }

    @Override
    public void setDtBlocCliDb(String dtBlocCliDb) {
        this.ws.getPersDb().setBlocCliDb(dtBlocCliDb);
    }

    @Override
    public String getDtBlocCliDbObj() {
        if (ws.getIndPers().getRisBnsric() >= 0) {
            return getDtBlocCliDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtBlocCliDbObj(String dtBlocCliDbObj) {
        if (dtBlocCliDbObj != null) {
            setDtBlocCliDb(dtBlocCliDbObj);
            ws.getIndPers().setRisBnsric(((short)0));
        }
        else {
            ws.getIndPers().setRisBnsric(((short)-1));
        }
    }

    @Override
    public String getDtDeadPersDb() {
        return ws.getPersDb().getDeadPersDb();
    }

    @Override
    public void setDtDeadPersDb(String dtDeadPersDb) {
        this.ws.getPersDb().setDeadPersDb(dtDeadPersDb);
    }

    @Override
    public String getDtDeadPersDbObj() {
        if (ws.getIndPers().getRisRistorniCap() >= 0) {
            return getDtDeadPersDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDeadPersDbObj(String dtDeadPersDbObj) {
        if (dtDeadPersDbObj != null) {
            setDtDeadPersDb(dtDeadPersDbObj);
            ws.getIndPers().setRisRistorniCap(((short)0));
        }
        else {
            ws.getIndPers().setRisRistorniCap(((short)-1));
        }
    }

    @Override
    public String getDtEndVldtPersDb() {
        return ws.getPersDb().getEndVldtPersDb();
    }

    @Override
    public void setDtEndVldtPersDb(String dtEndVldtPersDb) {
        this.ws.getPersDb().setEndVldtPersDb(dtEndVldtPersDb);
    }

    @Override
    public String getDtEndVldtPersDbObj() {
        if (ws.getIndPers().getRisMatEff() >= 0) {
            return getDtEndVldtPersDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndVldtPersDbObj(String dtEndVldtPersDbObj) {
        if (dtEndVldtPersDbObj != null) {
            setDtEndVldtPersDb(dtEndVldtPersDbObj);
            ws.getIndPers().setRisMatEff(((short)0));
        }
        else {
            ws.getIndPers().setRisMatEff(((short)-1));
        }
    }

    @Override
    public String getDtNascCliDb() {
        return ws.getPersDb().getNascCliDb();
    }

    @Override
    public void setDtNascCliDb(String dtNascCliDb) {
        this.ws.getPersDb().setNascCliDb(dtNascCliDb);
    }

    @Override
    public String getDtNascCliDbObj() {
        if (ws.getIndPers().getRisMat() >= 0) {
            return getDtNascCliDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtNascCliDbObj(String dtNascCliDbObj) {
        if (dtNascCliDbObj != null) {
            setDtNascCliDb(dtNascCliDbObj);
            ws.getIndPers().setRisMat(((short)0));
        }
        else {
            ws.getIndPers().setRisMat(((short)-1));
        }
    }

    @Override
    public String getDtSegnalPartnerDb() {
        return ws.getPersDb().getSegnalPartnerDb();
    }

    @Override
    public void setDtSegnalPartnerDb(String dtSegnalPartnerDb) {
        this.ws.getPersDb().setSegnalPartnerDb(dtSegnalPartnerDb);
    }

    @Override
    public String getDtSegnalPartnerDbObj() {
        if (ws.getIndPers().getRisMoviNonInves() >= 0) {
            return getDtSegnalPartnerDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtSegnalPartnerDbObj(String dtSegnalPartnerDbObj) {
        if (dtSegnalPartnerDbObj != null) {
            setDtSegnalPartnerDb(dtSegnalPartnerDbObj);
            ws.getIndPers().setRisMoviNonInves(((short)0));
        }
        else {
            ws.getIndPers().setRisMoviNonInves(((short)-1));
        }
    }

    @Override
    public int getIdPers() {
        return pers.getA25IdPers();
    }

    @Override
    public void setIdPers(int idPers) {
        this.pers.setA25IdPers(idPers);
    }

    @Override
    public int getIdSegmentazCli() {
        return pers.getA25IdSegmentazCli().getA25IdSegmentazCli();
    }

    @Override
    public void setIdSegmentazCli(int idSegmentazCli) {
        this.pers.getA25IdSegmentazCli().setA25IdSegmentazCli(idSegmentazCli);
    }

    @Override
    public Integer getIdSegmentazCliObj() {
        if (ws.getIndPers().getRisMinGarto() >= 0) {
            return ((Integer)getIdSegmentazCli());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdSegmentazCliObj(Integer idSegmentazCliObj) {
        if (idSegmentazCliObj != null) {
            setIdSegmentazCli(((int)idSegmentazCliObj));
            ws.getIndPers().setRisMinGarto(((short)0));
        }
        else {
            ws.getIndPers().setRisMinGarto(((short)-1));
        }
    }

    @Override
    public char getIndCli() {
        return pers.getA25IndCli();
    }

    @Override
    public void setIndCli(char indCli) {
        this.pers.setA25IndCli(indCli);
    }

    @Override
    public Character getIndCliObj() {
        if (ws.getIndPers().getRptoPre() >= 0) {
            return ((Character)getIndCli());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndCliObj(Character indCliObj) {
        if (indCliObj != null) {
            setIndCli(((char)indCliObj));
            ws.getIndPers().setRptoPre(((short)0));
        }
        else {
            ws.getIndPers().setRptoPre(((short)-1));
        }
    }

    @Override
    public char getIndCpctGiurd() {
        return pers.getA25IndCpctGiurd();
    }

    @Override
    public void setIndCpctGiurd(char indCpctGiurd) {
        this.pers.setA25IndCpctGiurd(indCpctGiurd);
    }

    @Override
    public Character getIndCpctGiurdObj() {
        if (ws.getIndPers().getRisCosAmmtz() >= 0) {
            return ((Character)getIndCpctGiurd());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndCpctGiurdObj(Character indCpctGiurdObj) {
        if (indCpctGiurdObj != null) {
            setIndCpctGiurd(((char)indCpctGiurdObj));
            ws.getIndPers().setRisCosAmmtz(((short)0));
        }
        else {
            ws.getIndPers().setRisCosAmmtz(((short)-1));
        }
    }

    @Override
    public char getIndEseFisc() {
        return pers.getA25IndEseFisc();
    }

    @Override
    public void setIndEseFisc(char indEseFisc) {
        this.pers.setA25IndEseFisc(indEseFisc);
    }

    @Override
    public Character getIndEseFiscObj() {
        if (ws.getIndPers().getRisSopr() >= 0) {
            return ((Character)getIndEseFisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndEseFiscObj(Character indEseFiscObj) {
        if (indEseFiscObj != null) {
            setIndEseFisc(((char)indEseFiscObj));
            ws.getIndPers().setRisSopr(((short)0));
        }
        else {
            ws.getIndPers().setRisSopr(((short)-1));
        }
    }

    @Override
    public char getIndPortHdcp() {
        return pers.getA25IndPortHdcp();
    }

    @Override
    public void setIndPortHdcp(char indPortHdcp) {
        this.pers.setA25IndPortHdcp(indPortHdcp);
    }

    @Override
    public Character getIndPortHdcpObj() {
        if (ws.getIndPers().getRisSpeFaivl() >= 0) {
            return ((Character)getIndPortHdcp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPortHdcpObj(Character indPortHdcpObj) {
        if (indPortHdcpObj != null) {
            setIndPortHdcp(((char)indPortHdcpObj));
            ws.getIndPers().setRisSpeFaivl(((short)0));
        }
        else {
            ws.getIndPers().setRisSpeFaivl(((short)-1));
        }
    }

    @Override
    public char getIndPvcyCmmrc() {
        return pers.getA25IndPvcyCmmrc();
    }

    @Override
    public void setIndPvcyCmmrc(char indPvcyCmmrc) {
        this.pers.setA25IndPvcyCmmrc(indPvcyCmmrc);
    }

    @Override
    public Character getIndPvcyCmmrcObj() {
        if (ws.getIndPers().getDtElab() >= 0) {
            return ((Character)getIndPvcyCmmrc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPvcyCmmrcObj(Character indPvcyCmmrcObj) {
        if (indPvcyCmmrcObj != null) {
            setIndPvcyCmmrc(((char)indPvcyCmmrcObj));
            ws.getIndPers().setDtElab(((short)0));
        }
        else {
            ws.getIndPers().setDtElab(((short)-1));
        }
    }

    @Override
    public char getIndPvcyIndst() {
        return pers.getA25IndPvcyIndst();
    }

    @Override
    public void setIndPvcyIndst(char indPvcyIndst) {
        this.pers.setA25IndPvcyIndst(indPvcyIndst);
    }

    @Override
    public Character getIndPvcyIndstObj() {
        if (ws.getIndPers().getRisBila() >= 0) {
            return ((Character)getIndPvcyIndst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPvcyIndstObj(Character indPvcyIndstObj) {
        if (indPvcyIndstObj != null) {
            setIndPvcyIndst(((char)indPvcyIndstObj));
            ws.getIndPers().setRisBila(((short)0));
        }
        else {
            ws.getIndPers().setRisBila(((short)-1));
        }
    }

    @Override
    public char getIndPvcyPrsnl() {
        return pers.getA25IndPvcyPrsnl();
    }

    @Override
    public void setIndPvcyPrsnl(char indPvcyPrsnl) {
        this.pers.setA25IndPvcyPrsnl(indPvcyPrsnl);
    }

    @Override
    public Character getIndPvcyPrsnlObj() {
        if (ws.getIndPers().getDtCalc() >= 0) {
            return ((Character)getIndPvcyPrsnl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPvcyPrsnlObj(Character indPvcyPrsnlObj) {
        if (indPvcyPrsnlObj != null) {
            setIndPvcyPrsnl(((char)indPvcyPrsnlObj));
            ws.getIndPers().setDtCalc(((short)0));
        }
        else {
            ws.getIndPers().setDtCalc(((short)-1));
        }
    }

    @Override
    public char getIndSex() {
        return pers.getA25IndSex();
    }

    @Override
    public void setIndSex(char indSex) {
        this.pers.setA25IndSex(indSex);
    }

    @Override
    public Character getIndSexObj() {
        if (ws.getIndPers().getRisFaivl() >= 0) {
            return ((Character)getIndSex());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndSexObj(Character indSexObj) {
        if (indSexObj != null) {
            setIndSex(((char)indSexObj));
            ws.getIndPers().setRisFaivl(((short)0));
        }
        else {
            ws.getIndPers().setRisFaivl(((short)-1));
        }
    }

    @Override
    public String getRiftoRete() {
        return pers.getA25RiftoRete();
    }

    @Override
    public void setRiftoRete(String riftoRete) {
        this.pers.setA25RiftoRete(riftoRete);
    }

    @Override
    public String getRiftoReteObj() {
        if (ws.getIndPers().getTpCalcRis() >= 0) {
            return getRiftoRete();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRiftoReteObj(String riftoReteObj) {
        if (riftoReteObj != null) {
            setRiftoRete(riftoReteObj);
            ws.getIndPers().setTpCalcRis(((short)0));
        }
        else {
            ws.getIndPers().setTpCalcRis(((short)-1));
        }
    }

    @Override
    public String getTpStatCli() {
        return pers.getA25TpStatCli();
    }

    @Override
    public void setTpStatCli(String tpStatCli) {
        this.pers.setA25TpStatCli(tpStatCli);
    }

    @Override
    public String getTpStatCliObj() {
        if (ws.getIndPers().getRisTrmBns() >= 0) {
            return getTpStatCli();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatCliObj(String tpStatCliObj) {
        if (tpStatCliObj != null) {
            setTpStatCli(tpStatCliObj);
            ws.getIndPers().setRisTrmBns(((short)0));
        }
        else {
            ws.getIndPers().setRisTrmBns(((short)-1));
        }
    }

    @Override
    public long getTstamAggmRiga() {
        return pers.getA25TstamAggmRiga().getA25TstamAggmRiga();
    }

    @Override
    public void setTstamAggmRiga(long tstamAggmRiga) {
        this.pers.getA25TstamAggmRiga().setA25TstamAggmRiga(tstamAggmRiga);
    }

    @Override
    public Long getTstamAggmRigaObj() {
        if (ws.getIndPers().getRisComponAssva() >= 0) {
            return ((Long)getTstamAggmRiga());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTstamAggmRigaObj(Long tstamAggmRigaObj) {
        if (tstamAggmRigaObj != null) {
            setTstamAggmRiga(((long)tstamAggmRigaObj));
            ws.getIndPers().setRisComponAssva(((short)0));
        }
        else {
            ws.getIndPers().setRisComponAssva(((short)-1));
        }
    }

    @Override
    public long getTstamEndVldt() {
        return pers.getA25TstamEndVldt().getA25TstamEndVldt();
    }

    @Override
    public void setTstamEndVldt(long tstamEndVldt) {
        this.pers.getA25TstamEndVldt().setA25TstamEndVldt(tstamEndVldt);
    }

    @Override
    public Long getTstamEndVldtObj() {
        if (ws.getIndPers().getIdMoviChiu() >= 0) {
            return ((Long)getTstamEndVldt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTstamEndVldtObj(Long tstamEndVldtObj) {
        if (tstamEndVldtObj != null) {
            setTstamEndVldt(((long)tstamEndVldtObj));
            ws.getIndPers().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndPers().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public long getTstamIniVldt() {
        return pers.getA25TstamIniVldt();
    }

    @Override
    public void setTstamIniVldt(long tstamIniVldt) {
        this.pers.setA25TstamIniVldt(tstamIniVldt);
    }

    @Override
    public long getTstamInsRiga() {
        return pers.getA25TstamInsRiga();
    }

    @Override
    public void setTstamInsRiga(long tstamInsRiga) {
        this.pers.setA25TstamInsRiga(tstamInsRiga);
    }
}
