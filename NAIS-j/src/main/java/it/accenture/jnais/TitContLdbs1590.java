package it.accenture.jnais;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.FieldNotMappedException;
import it.accenture.jnais.commons.data.to.ITitCont;

/**Original name: TitContLdbs1590<br>*/
public class TitContLdbs1590 implements ITitCont {

    //==== PROPERTIES ====
    private Ldbs1590 ldbs1590;

    //==== CONSTRUCTORS ====
    public TitContLdbs1590(Ldbs1590 ldbs1590) {
        this.ldbs1590 = ldbs1590;
    }

    //==== METHODS ====
    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodDvs() {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public void setCodDvs(String codDvs) {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public String getCodDvsObj() {
        return getCodDvs();
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        setCodDvs(codDvsObj);
    }

    @Override
    public String getCodIban() {
        throw new FieldNotMappedException("codIban");
    }

    @Override
    public void setCodIban(String codIban) {
        throw new FieldNotMappedException("codIban");
    }

    @Override
    public String getCodIbanObj() {
        return getCodIban();
    }

    @Override
    public void setCodIbanObj(String codIbanObj) {
        setCodIban(codIbanObj);
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsEndCptz() {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public long getDsTsIniCptz() {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtApplzMoraDb() {
        throw new FieldNotMappedException("dtApplzMoraDb");
    }

    @Override
    public void setDtApplzMoraDb(String dtApplzMoraDb) {
        throw new FieldNotMappedException("dtApplzMoraDb");
    }

    @Override
    public String getDtApplzMoraDbObj() {
        return getDtApplzMoraDb();
    }

    @Override
    public void setDtApplzMoraDbObj(String dtApplzMoraDbObj) {
        setDtApplzMoraDb(dtApplzMoraDbObj);
    }

    @Override
    public String getDtCambioVltDb() {
        throw new FieldNotMappedException("dtCambioVltDb");
    }

    @Override
    public void setDtCambioVltDb(String dtCambioVltDb) {
        throw new FieldNotMappedException("dtCambioVltDb");
    }

    @Override
    public String getDtCambioVltDbObj() {
        return getDtCambioVltDb();
    }

    @Override
    public void setDtCambioVltDbObj(String dtCambioVltDbObj) {
        setDtCambioVltDb(dtCambioVltDbObj);
    }

    @Override
    public String getDtCertFiscDb() {
        throw new FieldNotMappedException("dtCertFiscDb");
    }

    @Override
    public void setDtCertFiscDb(String dtCertFiscDb) {
        throw new FieldNotMappedException("dtCertFiscDb");
    }

    @Override
    public String getDtCertFiscDbObj() {
        return getDtCertFiscDb();
    }

    @Override
    public void setDtCertFiscDbObj(String dtCertFiscDbObj) {
        setDtCertFiscDb(dtCertFiscDbObj);
    }

    @Override
    public String getDtEmisTitDb() {
        throw new FieldNotMappedException("dtEmisTitDb");
    }

    @Override
    public void setDtEmisTitDb(String dtEmisTitDb) {
        throw new FieldNotMappedException("dtEmisTitDb");
    }

    @Override
    public String getDtEmisTitDbObj() {
        return getDtEmisTitDb();
    }

    @Override
    public void setDtEmisTitDbObj(String dtEmisTitDbObj) {
        setDtEmisTitDb(dtEmisTitDbObj);
    }

    @Override
    public String getDtEndCopDb() {
        throw new FieldNotMappedException("dtEndCopDb");
    }

    @Override
    public void setDtEndCopDb(String dtEndCopDb) {
        throw new FieldNotMappedException("dtEndCopDb");
    }

    @Override
    public String getDtEndCopDbObj() {
        return getDtEndCopDb();
    }

    @Override
    public void setDtEndCopDbObj(String dtEndCopDbObj) {
        setDtEndCopDb(dtEndCopDbObj);
    }

    @Override
    public String getDtEndEffDb() {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public String getDtEsiTitDb() {
        throw new FieldNotMappedException("dtEsiTitDb");
    }

    @Override
    public void setDtEsiTitDb(String dtEsiTitDb) {
        throw new FieldNotMappedException("dtEsiTitDb");
    }

    @Override
    public String getDtEsiTitDbObj() {
        return getDtEsiTitDb();
    }

    @Override
    public void setDtEsiTitDbObj(String dtEsiTitDbObj) {
        setDtEsiTitDb(dtEsiTitDbObj);
    }

    @Override
    public String getDtIniEffDb() {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public String getDtRichAddRidDb() {
        throw new FieldNotMappedException("dtRichAddRidDb");
    }

    @Override
    public void setDtRichAddRidDb(String dtRichAddRidDb) {
        throw new FieldNotMappedException("dtRichAddRidDb");
    }

    @Override
    public String getDtRichAddRidDbObj() {
        return getDtRichAddRidDb();
    }

    @Override
    public void setDtRichAddRidDbObj(String dtRichAddRidDbObj) {
        setDtRichAddRidDb(dtRichAddRidDbObj);
    }

    @Override
    public String getDtVltDb() {
        throw new FieldNotMappedException("dtVltDb");
    }

    @Override
    public void setDtVltDb(String dtVltDb) {
        throw new FieldNotMappedException("dtVltDb");
    }

    @Override
    public String getDtVltDbObj() {
        return getDtVltDb();
    }

    @Override
    public void setDtVltDbObj(String dtVltDbObj) {
        setDtVltDb(dtVltDbObj);
    }

    @Override
    public String getEstrCntCorrAdd() {
        throw new FieldNotMappedException("estrCntCorrAdd");
    }

    @Override
    public void setEstrCntCorrAdd(String estrCntCorrAdd) {
        throw new FieldNotMappedException("estrCntCorrAdd");
    }

    @Override
    public String getEstrCntCorrAddObj() {
        return getEstrCntCorrAdd();
    }

    @Override
    public void setEstrCntCorrAddObj(String estrCntCorrAddObj) {
        setEstrCntCorrAdd(estrCntCorrAddObj);
    }

    @Override
    public char getFlForzDtVlt() {
        throw new FieldNotMappedException("flForzDtVlt");
    }

    @Override
    public void setFlForzDtVlt(char flForzDtVlt) {
        throw new FieldNotMappedException("flForzDtVlt");
    }

    @Override
    public Character getFlForzDtVltObj() {
        return ((Character)getFlForzDtVlt());
    }

    @Override
    public void setFlForzDtVltObj(Character flForzDtVltObj) {
        setFlForzDtVlt(((char)flForzDtVltObj));
    }

    @Override
    public char getFlIncAutogen() {
        throw new FieldNotMappedException("flIncAutogen");
    }

    @Override
    public void setFlIncAutogen(char flIncAutogen) {
        throw new FieldNotMappedException("flIncAutogen");
    }

    @Override
    public Character getFlIncAutogenObj() {
        return ((Character)getFlIncAutogen());
    }

    @Override
    public void setFlIncAutogenObj(Character flIncAutogenObj) {
        setFlIncAutogen(((char)flIncAutogenObj));
    }

    @Override
    public char getFlMora() {
        throw new FieldNotMappedException("flMora");
    }

    @Override
    public void setFlMora(char flMora) {
        throw new FieldNotMappedException("flMora");
    }

    @Override
    public Character getFlMoraObj() {
        return ((Character)getFlMora());
    }

    @Override
    public void setFlMoraObj(Character flMoraObj) {
        setFlMora(((char)flMoraObj));
    }

    @Override
    public char getFlSoll() {
        throw new FieldNotMappedException("flSoll");
    }

    @Override
    public void setFlSoll(char flSoll) {
        throw new FieldNotMappedException("flSoll");
    }

    @Override
    public Character getFlSollObj() {
        return ((Character)getFlSoll());
    }

    @Override
    public void setFlSollObj(Character flSollObj) {
        setFlSoll(((char)flSollObj));
    }

    @Override
    public char getFlTitDaReinvst() {
        throw new FieldNotMappedException("flTitDaReinvst");
    }

    @Override
    public void setFlTitDaReinvst(char flTitDaReinvst) {
        throw new FieldNotMappedException("flTitDaReinvst");
    }

    @Override
    public Character getFlTitDaReinvstObj() {
        return ((Character)getFlTitDaReinvst());
    }

    @Override
    public void setFlTitDaReinvstObj(Character flTitDaReinvstObj) {
        setFlTitDaReinvst(((char)flTitDaReinvstObj));
    }

    @Override
    public int getFraz() {
        throw new FieldNotMappedException("fraz");
    }

    @Override
    public void setFraz(int fraz) {
        throw new FieldNotMappedException("fraz");
    }

    @Override
    public Integer getFrazObj() {
        return ((Integer)getFraz());
    }

    @Override
    public void setFrazObj(Integer frazObj) {
        setFraz(((int)frazObj));
    }

    @Override
    public String getIbRich() {
        throw new FieldNotMappedException("ibRich");
    }

    @Override
    public void setIbRich(String ibRich) {
        throw new FieldNotMappedException("ibRich");
    }

    @Override
    public String getIbRichObj() {
        return getIbRich();
    }

    @Override
    public void setIbRichObj(String ibRichObj) {
        setIbRich(ibRichObj);
    }

    @Override
    public int getIdMoviChiu() {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public Integer getIdMoviChiuObj() {
        return ((Integer)getIdMoviChiu());
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        setIdMoviChiu(((int)idMoviChiuObj));
    }

    @Override
    public int getIdMoviCrz() {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public int getIdRappAna() {
        throw new FieldNotMappedException("idRappAna");
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        throw new FieldNotMappedException("idRappAna");
    }

    @Override
    public Integer getIdRappAnaObj() {
        return ((Integer)getIdRappAna());
    }

    @Override
    public void setIdRappAnaObj(Integer idRappAnaObj) {
        setIdRappAna(((int)idRappAnaObj));
    }

    @Override
    public int getIdRappRete() {
        throw new FieldNotMappedException("idRappRete");
    }

    @Override
    public void setIdRappRete(int idRappRete) {
        throw new FieldNotMappedException("idRappRete");
    }

    @Override
    public Integer getIdRappReteObj() {
        return ((Integer)getIdRappRete());
    }

    @Override
    public void setIdRappReteObj(Integer idRappReteObj) {
        setIdRappRete(((int)idRappReteObj));
    }

    @Override
    public int getIdTitCont() {
        throw new FieldNotMappedException("idTitCont");
    }

    @Override
    public void setIdTitCont(int idTitCont) {
        throw new FieldNotMappedException("idTitCont");
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return ldbs1590.getIdsv0003().getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        ldbs1590.getIdsv0003().setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public AfDecimal getImpAderObj() {
        return getImpAder();
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        setImpAder(new AfDecimal(impAderObj, 15, 3));
    }

    @Override
    public AfDecimal getImpAz() {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public AfDecimal getImpAzObj() {
        return getImpAz();
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        setImpAz(new AfDecimal(impAzObj, 15, 3));
    }

    @Override
    public AfDecimal getImpPag() {
        throw new FieldNotMappedException("impPag");
    }

    @Override
    public void setImpPag(AfDecimal impPag) {
        throw new FieldNotMappedException("impPag");
    }

    @Override
    public AfDecimal getImpPagObj() {
        return getImpPag();
    }

    @Override
    public void setImpPagObj(AfDecimal impPagObj) {
        setImpPag(new AfDecimal(impPagObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTfr() {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public AfDecimal getImpTfrObj() {
        return getImpTfr();
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        setImpTfr(new AfDecimal(impTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        throw new FieldNotMappedException("impTfrStrc");
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        throw new FieldNotMappedException("impTfrStrc");
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        return getImpTfrStrc();
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTrasfe() {
        throw new FieldNotMappedException("impTrasfe");
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        throw new FieldNotMappedException("impTrasfe");
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        return getImpTrasfe();
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
    }

    @Override
    public AfDecimal getImpVolo() {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public AfDecimal getImpVoloObj() {
        return getImpVolo();
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        setImpVolo(new AfDecimal(impVoloObj, 15, 3));
    }

    @Override
    public String getIsoDtIniPerDb() {
        return ldbs1590.getWs().getImpstSostDb().getPervRichDb();
    }

    @Override
    public void setIsoDtIniPerDb(String isoDtIniPerDb) {
        ldbs1590.getWs().getImpstSostDb().setPervRichDb(isoDtIniPerDb);
    }

    @Override
    public int getLdbv1591IdOgg() {
        return ldbs1590.getLdbv1591().getIdOgg();
    }

    @Override
    public void setLdbv1591IdOgg(int ldbv1591IdOgg) {
        ldbs1590.getLdbv1591().setIdOgg(ldbv1591IdOgg);
    }

    @Override
    public AfDecimal getLdbv1591ImpTot() {
        return ldbs1590.getLdbv1591().getImpTot();
    }

    @Override
    public void setLdbv1591ImpTot(AfDecimal ldbv1591ImpTot) {
        ldbs1590.getLdbv1591().setImpTot(ldbv1591ImpTot.copy());
    }

    @Override
    public String getLdbv1591TpOgg() {
        return ldbs1590.getLdbv1591().getTpOgg();
    }

    @Override
    public void setLdbv1591TpOgg(String ldbv1591TpOgg) {
        ldbs1590.getLdbv1591().setTpOgg(ldbv1591TpOgg);
    }

    @Override
    public int getLdbv2091IdOgg() {
        throw new FieldNotMappedException("ldbv2091IdOgg");
    }

    @Override
    public void setLdbv2091IdOgg(int ldbv2091IdOgg) {
        throw new FieldNotMappedException("ldbv2091IdOgg");
    }

    @Override
    public AfDecimal getLdbv2091TotPremi() {
        throw new FieldNotMappedException("ldbv2091TotPremi");
    }

    @Override
    public void setLdbv2091TotPremi(AfDecimal ldbv2091TotPremi) {
        throw new FieldNotMappedException("ldbv2091TotPremi");
    }

    @Override
    public String getLdbv2091TpOgg() {
        throw new FieldNotMappedException("ldbv2091TpOgg");
    }

    @Override
    public void setLdbv2091TpOgg(String ldbv2091TpOgg) {
        throw new FieldNotMappedException("ldbv2091TpOgg");
    }

    @Override
    public String getLdbv2091TpStatTit01() {
        throw new FieldNotMappedException("ldbv2091TpStatTit01");
    }

    @Override
    public void setLdbv2091TpStatTit01(String ldbv2091TpStatTit01) {
        throw new FieldNotMappedException("ldbv2091TpStatTit01");
    }

    @Override
    public String getLdbv2091TpStatTit02() {
        throw new FieldNotMappedException("ldbv2091TpStatTit02");
    }

    @Override
    public void setLdbv2091TpStatTit02(String ldbv2091TpStatTit02) {
        throw new FieldNotMappedException("ldbv2091TpStatTit02");
    }

    @Override
    public String getLdbv2091TpStatTit03() {
        throw new FieldNotMappedException("ldbv2091TpStatTit03");
    }

    @Override
    public void setLdbv2091TpStatTit03(String ldbv2091TpStatTit03) {
        throw new FieldNotMappedException("ldbv2091TpStatTit03");
    }

    @Override
    public String getLdbv2091TpStatTit04() {
        throw new FieldNotMappedException("ldbv2091TpStatTit04");
    }

    @Override
    public void setLdbv2091TpStatTit04(String ldbv2091TpStatTit04) {
        throw new FieldNotMappedException("ldbv2091TpStatTit04");
    }

    @Override
    public String getLdbv2091TpStatTit05() {
        throw new FieldNotMappedException("ldbv2091TpStatTit05");
    }

    @Override
    public void setLdbv2091TpStatTit05(String ldbv2091TpStatTit05) {
        throw new FieldNotMappedException("ldbv2091TpStatTit05");
    }

    @Override
    public String getLdbv6151DtDecorPrestDb() {
        throw new FieldNotMappedException("ldbv6151DtDecorPrestDb");
    }

    @Override
    public void setLdbv6151DtDecorPrestDb(String ldbv6151DtDecorPrestDb) {
        throw new FieldNotMappedException("ldbv6151DtDecorPrestDb");
    }

    @Override
    public String getLdbv6151DtMaxDb() {
        throw new FieldNotMappedException("ldbv6151DtMaxDb");
    }

    @Override
    public void setLdbv6151DtMaxDb(String ldbv6151DtMaxDb) {
        throw new FieldNotMappedException("ldbv6151DtMaxDb");
    }

    @Override
    public String getLdbv6151DtMaxDbObj() {
        return getLdbv6151DtMaxDb();
    }

    @Override
    public void setLdbv6151DtMaxDbObj(String ldbv6151DtMaxDbObj) {
        setLdbv6151DtMaxDb(ldbv6151DtMaxDbObj);
    }

    @Override
    public int getLdbv6151IdOgg() {
        throw new FieldNotMappedException("ldbv6151IdOgg");
    }

    @Override
    public void setLdbv6151IdOgg(int ldbv6151IdOgg) {
        throw new FieldNotMappedException("ldbv6151IdOgg");
    }

    @Override
    public String getLdbv6151TpOgg() {
        throw new FieldNotMappedException("ldbv6151TpOgg");
    }

    @Override
    public void setLdbv6151TpOgg(String ldbv6151TpOgg) {
        throw new FieldNotMappedException("ldbv6151TpOgg");
    }

    @Override
    public String getLdbv6151TpStatTit() {
        throw new FieldNotMappedException("ldbv6151TpStatTit");
    }

    @Override
    public void setLdbv6151TpStatTit(String ldbv6151TpStatTit) {
        throw new FieldNotMappedException("ldbv6151TpStatTit");
    }

    @Override
    public String getLdbv6151TpTit01() {
        throw new FieldNotMappedException("ldbv6151TpTit01");
    }

    @Override
    public void setLdbv6151TpTit01(String ldbv6151TpTit01) {
        throw new FieldNotMappedException("ldbv6151TpTit01");
    }

    @Override
    public String getLdbv6151TpTit02() {
        throw new FieldNotMappedException("ldbv6151TpTit02");
    }

    @Override
    public void setLdbv6151TpTit02(String ldbv6151TpTit02) {
        throw new FieldNotMappedException("ldbv6151TpTit02");
    }

    @Override
    public String getLdbvb441DtMaxDb() {
        throw new FieldNotMappedException("ldbvb441DtMaxDb");
    }

    @Override
    public void setLdbvb441DtMaxDb(String ldbvb441DtMaxDb) {
        throw new FieldNotMappedException("ldbvb441DtMaxDb");
    }

    @Override
    public String getLdbvb441DtMaxDbObj() {
        return getLdbvb441DtMaxDb();
    }

    @Override
    public void setLdbvb441DtMaxDbObj(String ldbvb441DtMaxDbObj) {
        setLdbvb441DtMaxDb(ldbvb441DtMaxDbObj);
    }

    @Override
    public int getLdbvb441IdOgg() {
        throw new FieldNotMappedException("ldbvb441IdOgg");
    }

    @Override
    public void setLdbvb441IdOgg(int ldbvb441IdOgg) {
        throw new FieldNotMappedException("ldbvb441IdOgg");
    }

    @Override
    public String getLdbvb441TpOgg() {
        throw new FieldNotMappedException("ldbvb441TpOgg");
    }

    @Override
    public void setLdbvb441TpOgg(String ldbvb441TpOgg) {
        throw new FieldNotMappedException("ldbvb441TpOgg");
    }

    @Override
    public String getLdbvb441TpStatTit1() {
        throw new FieldNotMappedException("ldbvb441TpStatTit1");
    }

    @Override
    public void setLdbvb441TpStatTit1(String ldbvb441TpStatTit1) {
        throw new FieldNotMappedException("ldbvb441TpStatTit1");
    }

    @Override
    public String getLdbvb441TpStatTit2() {
        throw new FieldNotMappedException("ldbvb441TpStatTit2");
    }

    @Override
    public void setLdbvb441TpStatTit2(String ldbvb441TpStatTit2) {
        throw new FieldNotMappedException("ldbvb441TpStatTit2");
    }

    @Override
    public String getLdbvb441TpStatTit3() {
        throw new FieldNotMappedException("ldbvb441TpStatTit3");
    }

    @Override
    public void setLdbvb441TpStatTit3(String ldbvb441TpStatTit3) {
        throw new FieldNotMappedException("ldbvb441TpStatTit3");
    }

    @Override
    public String getLdbvb441TpTit01() {
        throw new FieldNotMappedException("ldbvb441TpTit01");
    }

    @Override
    public void setLdbvb441TpTit01(String ldbvb441TpTit01) {
        throw new FieldNotMappedException("ldbvb441TpTit01");
    }

    @Override
    public String getLdbvb441TpTit02() {
        throw new FieldNotMappedException("ldbvb441TpTit02");
    }

    @Override
    public void setLdbvb441TpTit02(String ldbvb441TpTit02) {
        throw new FieldNotMappedException("ldbvb441TpTit02");
    }

    @Override
    public String getLdbvb471DtMaxDb() {
        throw new FieldNotMappedException("ldbvb471DtMaxDb");
    }

    @Override
    public void setLdbvb471DtMaxDb(String ldbvb471DtMaxDb) {
        throw new FieldNotMappedException("ldbvb471DtMaxDb");
    }

    @Override
    public int getLdbvb471IdOgg() {
        throw new FieldNotMappedException("ldbvb471IdOgg");
    }

    @Override
    public void setLdbvb471IdOgg(int ldbvb471IdOgg) {
        throw new FieldNotMappedException("ldbvb471IdOgg");
    }

    @Override
    public String getLdbvb471TpOgg() {
        throw new FieldNotMappedException("ldbvb471TpOgg");
    }

    @Override
    public void setLdbvb471TpOgg(String ldbvb471TpOgg) {
        throw new FieldNotMappedException("ldbvb471TpOgg");
    }

    @Override
    public String getLdbvb471TpStatTit1() {
        throw new FieldNotMappedException("ldbvb471TpStatTit1");
    }

    @Override
    public void setLdbvb471TpStatTit1(String ldbvb471TpStatTit1) {
        throw new FieldNotMappedException("ldbvb471TpStatTit1");
    }

    @Override
    public String getLdbvb471TpStatTit2() {
        throw new FieldNotMappedException("ldbvb471TpStatTit2");
    }

    @Override
    public void setLdbvb471TpStatTit2(String ldbvb471TpStatTit2) {
        throw new FieldNotMappedException("ldbvb471TpStatTit2");
    }

    @Override
    public String getLdbvb471TpStatTit3() {
        throw new FieldNotMappedException("ldbvb471TpStatTit3");
    }

    @Override
    public void setLdbvb471TpStatTit3(String ldbvb471TpStatTit3) {
        throw new FieldNotMappedException("ldbvb471TpStatTit3");
    }

    @Override
    public String getLdbvb471TpTit01() {
        throw new FieldNotMappedException("ldbvb471TpTit01");
    }

    @Override
    public void setLdbvb471TpTit01(String ldbvb471TpTit01) {
        throw new FieldNotMappedException("ldbvb471TpTit01");
    }

    @Override
    public String getLdbvb471TpTit02() {
        throw new FieldNotMappedException("ldbvb471TpTit02");
    }

    @Override
    public void setLdbvb471TpTit02(String ldbvb471TpTit02) {
        throw new FieldNotMappedException("ldbvb471TpTit02");
    }

    @Override
    public AfDecimal getLdbvf111CumPreVers() {
        throw new FieldNotMappedException("ldbvf111CumPreVers");
    }

    @Override
    public void setLdbvf111CumPreVers(AfDecimal ldbvf111CumPreVers) {
        throw new FieldNotMappedException("ldbvf111CumPreVers");
    }

    @Override
    public int getLdbvf111IdOgg() {
        throw new FieldNotMappedException("ldbvf111IdOgg");
    }

    @Override
    public void setLdbvf111IdOgg(int ldbvf111IdOgg) {
        throw new FieldNotMappedException("ldbvf111IdOgg");
    }

    @Override
    public String getLdbvf111TpStatTit1() {
        throw new FieldNotMappedException("ldbvf111TpStatTit1");
    }

    @Override
    public void setLdbvf111TpStatTit1(String ldbvf111TpStatTit1) {
        throw new FieldNotMappedException("ldbvf111TpStatTit1");
    }

    @Override
    public String getLdbvf111TpStatTit2() {
        throw new FieldNotMappedException("ldbvf111TpStatTit2");
    }

    @Override
    public void setLdbvf111TpStatTit2(String ldbvf111TpStatTit2) {
        throw new FieldNotMappedException("ldbvf111TpStatTit2");
    }

    @Override
    public String getLdbvf111TpStatTit3() {
        throw new FieldNotMappedException("ldbvf111TpStatTit3");
    }

    @Override
    public void setLdbvf111TpStatTit3(String ldbvf111TpStatTit3) {
        throw new FieldNotMappedException("ldbvf111TpStatTit3");
    }

    @Override
    public String getLdbvf111TpStatTit4() {
        throw new FieldNotMappedException("ldbvf111TpStatTit4");
    }

    @Override
    public void setLdbvf111TpStatTit4(String ldbvf111TpStatTit4) {
        throw new FieldNotMappedException("ldbvf111TpStatTit4");
    }

    @Override
    public String getLdbvf111TpStatTit5() {
        throw new FieldNotMappedException("ldbvf111TpStatTit5");
    }

    @Override
    public void setLdbvf111TpStatTit5(String ldbvf111TpStatTit5) {
        throw new FieldNotMappedException("ldbvf111TpStatTit5");
    }

    @Override
    public int getNumRatAccorpate() {
        throw new FieldNotMappedException("numRatAccorpate");
    }

    @Override
    public void setNumRatAccorpate(int numRatAccorpate) {
        throw new FieldNotMappedException("numRatAccorpate");
    }

    @Override
    public Integer getNumRatAccorpateObj() {
        return ((Integer)getNumRatAccorpate());
    }

    @Override
    public void setNumRatAccorpateObj(Integer numRatAccorpateObj) {
        setNumRatAccorpate(((int)numRatAccorpateObj));
    }

    @Override
    public int getProgTit() {
        throw new FieldNotMappedException("progTit");
    }

    @Override
    public void setProgTit(int progTit) {
        throw new FieldNotMappedException("progTit");
    }

    @Override
    public Integer getProgTitObj() {
        return ((Integer)getProgTit());
    }

    @Override
    public void setProgTitObj(Integer progTitObj) {
        setProgTit(((int)progTitObj));
    }

    @Override
    public long getTitDsRiga() {
        throw new FieldNotMappedException("titDsRiga");
    }

    @Override
    public void setTitDsRiga(long titDsRiga) {
        throw new FieldNotMappedException("titDsRiga");
    }

    @Override
    public String getTitDtIniCopDb() {
        throw new FieldNotMappedException("titDtIniCopDb");
    }

    @Override
    public void setTitDtIniCopDb(String titDtIniCopDb) {
        throw new FieldNotMappedException("titDtIniCopDb");
    }

    @Override
    public String getTitDtIniCopDbObj() {
        return getTitDtIniCopDb();
    }

    @Override
    public void setTitDtIniCopDbObj(String titDtIniCopDbObj) {
        setTitDtIniCopDb(titDtIniCopDbObj);
    }

    @Override
    public int getTitIdOgg() {
        throw new FieldNotMappedException("titIdOgg");
    }

    @Override
    public void setTitIdOgg(int titIdOgg) {
        throw new FieldNotMappedException("titIdOgg");
    }

    @Override
    public String getTitTpOgg() {
        throw new FieldNotMappedException("titTpOgg");
    }

    @Override
    public void setTitTpOgg(String titTpOgg) {
        throw new FieldNotMappedException("titTpOgg");
    }

    @Override
    public String getTitTpPreTit() {
        throw new FieldNotMappedException("titTpPreTit");
    }

    @Override
    public void setTitTpPreTit(String titTpPreTit) {
        throw new FieldNotMappedException("titTpPreTit");
    }

    @Override
    public String getTitTpStatTit() {
        throw new FieldNotMappedException("titTpStatTit");
    }

    @Override
    public void setTitTpStatTit(String titTpStatTit) {
        throw new FieldNotMappedException("titTpStatTit");
    }

    @Override
    public String getTitTpTit() {
        throw new FieldNotMappedException("titTpTit");
    }

    @Override
    public void setTitTpTit(String titTpTit) {
        throw new FieldNotMappedException("titTpTit");
    }

    @Override
    public AfDecimal getTotAcqExp() {
        throw new FieldNotMappedException("totAcqExp");
    }

    @Override
    public void setTotAcqExp(AfDecimal totAcqExp) {
        throw new FieldNotMappedException("totAcqExp");
    }

    @Override
    public AfDecimal getTotAcqExpObj() {
        return getTotAcqExp();
    }

    @Override
    public void setTotAcqExpObj(AfDecimal totAcqExpObj) {
        setTotAcqExp(new AfDecimal(totAcqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCarAcq() {
        throw new FieldNotMappedException("totCarAcq");
    }

    @Override
    public void setTotCarAcq(AfDecimal totCarAcq) {
        throw new FieldNotMappedException("totCarAcq");
    }

    @Override
    public AfDecimal getTotCarAcqObj() {
        return getTotCarAcq();
    }

    @Override
    public void setTotCarAcqObj(AfDecimal totCarAcqObj) {
        setTotCarAcq(new AfDecimal(totCarAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCarGest() {
        throw new FieldNotMappedException("totCarGest");
    }

    @Override
    public void setTotCarGest(AfDecimal totCarGest) {
        throw new FieldNotMappedException("totCarGest");
    }

    @Override
    public AfDecimal getTotCarGestObj() {
        return getTotCarGest();
    }

    @Override
    public void setTotCarGestObj(AfDecimal totCarGestObj) {
        setTotCarGest(new AfDecimal(totCarGestObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCarIas() {
        throw new FieldNotMappedException("totCarIas");
    }

    @Override
    public void setTotCarIas(AfDecimal totCarIas) {
        throw new FieldNotMappedException("totCarIas");
    }

    @Override
    public AfDecimal getTotCarIasObj() {
        return getTotCarIas();
    }

    @Override
    public void setTotCarIasObj(AfDecimal totCarIasObj) {
        setTotCarIas(new AfDecimal(totCarIasObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCarInc() {
        throw new FieldNotMappedException("totCarInc");
    }

    @Override
    public void setTotCarInc(AfDecimal totCarInc) {
        throw new FieldNotMappedException("totCarInc");
    }

    @Override
    public AfDecimal getTotCarIncObj() {
        return getTotCarInc();
    }

    @Override
    public void setTotCarIncObj(AfDecimal totCarIncObj) {
        setTotCarInc(new AfDecimal(totCarIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCnbtAntirac() {
        throw new FieldNotMappedException("totCnbtAntirac");
    }

    @Override
    public void setTotCnbtAntirac(AfDecimal totCnbtAntirac) {
        throw new FieldNotMappedException("totCnbtAntirac");
    }

    @Override
    public AfDecimal getTotCnbtAntiracObj() {
        return getTotCnbtAntirac();
    }

    @Override
    public void setTotCnbtAntiracObj(AfDecimal totCnbtAntiracObj) {
        setTotCnbtAntirac(new AfDecimal(totCnbtAntiracObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCommisInter() {
        throw new FieldNotMappedException("totCommisInter");
    }

    @Override
    public void setTotCommisInter(AfDecimal totCommisInter) {
        throw new FieldNotMappedException("totCommisInter");
    }

    @Override
    public AfDecimal getTotCommisInterObj() {
        return getTotCommisInter();
    }

    @Override
    public void setTotCommisInterObj(AfDecimal totCommisInterObj) {
        setTotCommisInter(new AfDecimal(totCommisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getTotDir() {
        throw new FieldNotMappedException("totDir");
    }

    @Override
    public void setTotDir(AfDecimal totDir) {
        throw new FieldNotMappedException("totDir");
    }

    @Override
    public AfDecimal getTotDirObj() {
        return getTotDir();
    }

    @Override
    public void setTotDirObj(AfDecimal totDirObj) {
        setTotDir(new AfDecimal(totDirObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrFraz() {
        throw new FieldNotMappedException("totIntrFraz");
    }

    @Override
    public void setTotIntrFraz(AfDecimal totIntrFraz) {
        throw new FieldNotMappedException("totIntrFraz");
    }

    @Override
    public AfDecimal getTotIntrFrazObj() {
        return getTotIntrFraz();
    }

    @Override
    public void setTotIntrFrazObj(AfDecimal totIntrFrazObj) {
        setTotIntrFraz(new AfDecimal(totIntrFrazObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrMora() {
        throw new FieldNotMappedException("totIntrMora");
    }

    @Override
    public void setTotIntrMora(AfDecimal totIntrMora) {
        throw new FieldNotMappedException("totIntrMora");
    }

    @Override
    public AfDecimal getTotIntrMoraObj() {
        return getTotIntrMora();
    }

    @Override
    public void setTotIntrMoraObj(AfDecimal totIntrMoraObj) {
        setTotIntrMora(new AfDecimal(totIntrMoraObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrPrest() {
        throw new FieldNotMappedException("totIntrPrest");
    }

    @Override
    public void setTotIntrPrest(AfDecimal totIntrPrest) {
        throw new FieldNotMappedException("totIntrPrest");
    }

    @Override
    public AfDecimal getTotIntrPrestObj() {
        return getTotIntrPrest();
    }

    @Override
    public void setTotIntrPrestObj(AfDecimal totIntrPrestObj) {
        setTotIntrPrest(new AfDecimal(totIntrPrestObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrRetdt() {
        throw new FieldNotMappedException("totIntrRetdt");
    }

    @Override
    public void setTotIntrRetdt(AfDecimal totIntrRetdt) {
        throw new FieldNotMappedException("totIntrRetdt");
    }

    @Override
    public AfDecimal getTotIntrRetdtObj() {
        return getTotIntrRetdt();
    }

    @Override
    public void setTotIntrRetdtObj(AfDecimal totIntrRetdtObj) {
        setTotIntrRetdt(new AfDecimal(totIntrRetdtObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrRiat() {
        throw new FieldNotMappedException("totIntrRiat");
    }

    @Override
    public void setTotIntrRiat(AfDecimal totIntrRiat) {
        throw new FieldNotMappedException("totIntrRiat");
    }

    @Override
    public AfDecimal getTotIntrRiatObj() {
        return getTotIntrRiat();
    }

    @Override
    public void setTotIntrRiatObj(AfDecimal totIntrRiatObj) {
        setTotIntrRiat(new AfDecimal(totIntrRiatObj, 15, 3));
    }

    @Override
    public AfDecimal getTotManfeeAntic() {
        throw new FieldNotMappedException("totManfeeAntic");
    }

    @Override
    public void setTotManfeeAntic(AfDecimal totManfeeAntic) {
        throw new FieldNotMappedException("totManfeeAntic");
    }

    @Override
    public AfDecimal getTotManfeeAnticObj() {
        return getTotManfeeAntic();
    }

    @Override
    public void setTotManfeeAnticObj(AfDecimal totManfeeAnticObj) {
        setTotManfeeAntic(new AfDecimal(totManfeeAnticObj, 15, 3));
    }

    @Override
    public AfDecimal getTotManfeeRec() {
        throw new FieldNotMappedException("totManfeeRec");
    }

    @Override
    public void setTotManfeeRec(AfDecimal totManfeeRec) {
        throw new FieldNotMappedException("totManfeeRec");
    }

    @Override
    public AfDecimal getTotManfeeRecObj() {
        return getTotManfeeRec();
    }

    @Override
    public void setTotManfeeRecObj(AfDecimal totManfeeRecObj) {
        setTotManfeeRec(new AfDecimal(totManfeeRecObj, 15, 3));
    }

    @Override
    public AfDecimal getTotManfeeRicor() {
        throw new FieldNotMappedException("totManfeeRicor");
    }

    @Override
    public void setTotManfeeRicor(AfDecimal totManfeeRicor) {
        throw new FieldNotMappedException("totManfeeRicor");
    }

    @Override
    public AfDecimal getTotManfeeRicorObj() {
        return getTotManfeeRicor();
    }

    @Override
    public void setTotManfeeRicorObj(AfDecimal totManfeeRicorObj) {
        setTotManfeeRicor(new AfDecimal(totManfeeRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTotPreNet() {
        throw new FieldNotMappedException("totPreNet");
    }

    @Override
    public void setTotPreNet(AfDecimal totPreNet) {
        throw new FieldNotMappedException("totPreNet");
    }

    @Override
    public AfDecimal getTotPreNetObj() {
        return getTotPreNet();
    }

    @Override
    public void setTotPreNetObj(AfDecimal totPreNetObj) {
        setTotPreNet(new AfDecimal(totPreNetObj, 15, 3));
    }

    @Override
    public AfDecimal getTotPrePpIas() {
        throw new FieldNotMappedException("totPrePpIas");
    }

    @Override
    public void setTotPrePpIas(AfDecimal totPrePpIas) {
        throw new FieldNotMappedException("totPrePpIas");
    }

    @Override
    public AfDecimal getTotPrePpIasObj() {
        return getTotPrePpIas();
    }

    @Override
    public void setTotPrePpIasObj(AfDecimal totPrePpIasObj) {
        setTotPrePpIas(new AfDecimal(totPrePpIasObj, 15, 3));
    }

    @Override
    public AfDecimal getTotPreSoloRsh() {
        throw new FieldNotMappedException("totPreSoloRsh");
    }

    @Override
    public void setTotPreSoloRsh(AfDecimal totPreSoloRsh) {
        throw new FieldNotMappedException("totPreSoloRsh");
    }

    @Override
    public AfDecimal getTotPreSoloRshObj() {
        return getTotPreSoloRsh();
    }

    @Override
    public void setTotPreSoloRshObj(AfDecimal totPreSoloRshObj) {
        setTotPreSoloRsh(new AfDecimal(totPreSoloRshObj, 15, 3));
    }

    @Override
    public AfDecimal getTotPreTot() {
        throw new FieldNotMappedException("totPreTot");
    }

    @Override
    public void setTotPreTot(AfDecimal totPreTot) {
        throw new FieldNotMappedException("totPreTot");
    }

    @Override
    public AfDecimal getTotPreTotObj() {
        return getTotPreTot();
    }

    @Override
    public void setTotPreTotObj(AfDecimal totPreTotObj) {
        setTotPreTot(new AfDecimal(totPreTotObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvAcq1aa() {
        throw new FieldNotMappedException("totProvAcq1aa");
    }

    @Override
    public void setTotProvAcq1aa(AfDecimal totProvAcq1aa) {
        throw new FieldNotMappedException("totProvAcq1aa");
    }

    @Override
    public AfDecimal getTotProvAcq1aaObj() {
        return getTotProvAcq1aa();
    }

    @Override
    public void setTotProvAcq1aaObj(AfDecimal totProvAcq1aaObj) {
        setTotProvAcq1aa(new AfDecimal(totProvAcq1aaObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvAcq2aa() {
        throw new FieldNotMappedException("totProvAcq2aa");
    }

    @Override
    public void setTotProvAcq2aa(AfDecimal totProvAcq2aa) {
        throw new FieldNotMappedException("totProvAcq2aa");
    }

    @Override
    public AfDecimal getTotProvAcq2aaObj() {
        return getTotProvAcq2aa();
    }

    @Override
    public void setTotProvAcq2aaObj(AfDecimal totProvAcq2aaObj) {
        setTotProvAcq2aa(new AfDecimal(totProvAcq2aaObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvDaRec() {
        throw new FieldNotMappedException("totProvDaRec");
    }

    @Override
    public void setTotProvDaRec(AfDecimal totProvDaRec) {
        throw new FieldNotMappedException("totProvDaRec");
    }

    @Override
    public AfDecimal getTotProvDaRecObj() {
        return getTotProvDaRec();
    }

    @Override
    public void setTotProvDaRecObj(AfDecimal totProvDaRecObj) {
        setTotProvDaRec(new AfDecimal(totProvDaRecObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvInc() {
        throw new FieldNotMappedException("totProvInc");
    }

    @Override
    public void setTotProvInc(AfDecimal totProvInc) {
        throw new FieldNotMappedException("totProvInc");
    }

    @Override
    public AfDecimal getTotProvIncObj() {
        return getTotProvInc();
    }

    @Override
    public void setTotProvIncObj(AfDecimal totProvIncObj) {
        setTotProvInc(new AfDecimal(totProvIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvRicor() {
        throw new FieldNotMappedException("totProvRicor");
    }

    @Override
    public void setTotProvRicor(AfDecimal totProvRicor) {
        throw new FieldNotMappedException("totProvRicor");
    }

    @Override
    public AfDecimal getTotProvRicorObj() {
        return getTotProvRicor();
    }

    @Override
    public void setTotProvRicorObj(AfDecimal totProvRicorObj) {
        setTotProvRicor(new AfDecimal(totProvRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTotRemunAss() {
        throw new FieldNotMappedException("totRemunAss");
    }

    @Override
    public void setTotRemunAss(AfDecimal totRemunAss) {
        throw new FieldNotMappedException("totRemunAss");
    }

    @Override
    public AfDecimal getTotRemunAssObj() {
        return getTotRemunAss();
    }

    @Override
    public void setTotRemunAssObj(AfDecimal totRemunAssObj) {
        setTotRemunAss(new AfDecimal(totRemunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprAlt() {
        throw new FieldNotMappedException("totSoprAlt");
    }

    @Override
    public void setTotSoprAlt(AfDecimal totSoprAlt) {
        throw new FieldNotMappedException("totSoprAlt");
    }

    @Override
    public AfDecimal getTotSoprAltObj() {
        return getTotSoprAlt();
    }

    @Override
    public void setTotSoprAltObj(AfDecimal totSoprAltObj) {
        setTotSoprAlt(new AfDecimal(totSoprAltObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprProf() {
        throw new FieldNotMappedException("totSoprProf");
    }

    @Override
    public void setTotSoprProf(AfDecimal totSoprProf) {
        throw new FieldNotMappedException("totSoprProf");
    }

    @Override
    public AfDecimal getTotSoprProfObj() {
        return getTotSoprProf();
    }

    @Override
    public void setTotSoprProfObj(AfDecimal totSoprProfObj) {
        setTotSoprProf(new AfDecimal(totSoprProfObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprSan() {
        throw new FieldNotMappedException("totSoprSan");
    }

    @Override
    public void setTotSoprSan(AfDecimal totSoprSan) {
        throw new FieldNotMappedException("totSoprSan");
    }

    @Override
    public AfDecimal getTotSoprSanObj() {
        return getTotSoprSan();
    }

    @Override
    public void setTotSoprSanObj(AfDecimal totSoprSanObj) {
        setTotSoprSan(new AfDecimal(totSoprSanObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprSpo() {
        throw new FieldNotMappedException("totSoprSpo");
    }

    @Override
    public void setTotSoprSpo(AfDecimal totSoprSpo) {
        throw new FieldNotMappedException("totSoprSpo");
    }

    @Override
    public AfDecimal getTotSoprSpoObj() {
        return getTotSoprSpo();
    }

    @Override
    public void setTotSoprSpoObj(AfDecimal totSoprSpoObj) {
        setTotSoprSpo(new AfDecimal(totSoprSpoObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprTec() {
        throw new FieldNotMappedException("totSoprTec");
    }

    @Override
    public void setTotSoprTec(AfDecimal totSoprTec) {
        throw new FieldNotMappedException("totSoprTec");
    }

    @Override
    public AfDecimal getTotSoprTecObj() {
        return getTotSoprTec();
    }

    @Override
    public void setTotSoprTecObj(AfDecimal totSoprTecObj) {
        setTotSoprTec(new AfDecimal(totSoprTecObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSpeAge() {
        throw new FieldNotMappedException("totSpeAge");
    }

    @Override
    public void setTotSpeAge(AfDecimal totSpeAge) {
        throw new FieldNotMappedException("totSpeAge");
    }

    @Override
    public AfDecimal getTotSpeAgeObj() {
        return getTotSpeAge();
    }

    @Override
    public void setTotSpeAgeObj(AfDecimal totSpeAgeObj) {
        setTotSpeAge(new AfDecimal(totSpeAgeObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSpeMed() {
        throw new FieldNotMappedException("totSpeMed");
    }

    @Override
    public void setTotSpeMed(AfDecimal totSpeMed) {
        throw new FieldNotMappedException("totSpeMed");
    }

    @Override
    public AfDecimal getTotSpeMedObj() {
        return getTotSpeMed();
    }

    @Override
    public void setTotSpeMedObj(AfDecimal totSpeMedObj) {
        setTotSpeMed(new AfDecimal(totSpeMedObj, 15, 3));
    }

    @Override
    public AfDecimal getTotTax() {
        throw new FieldNotMappedException("totTax");
    }

    @Override
    public void setTotTax(AfDecimal totTax) {
        throw new FieldNotMappedException("totTax");
    }

    @Override
    public AfDecimal getTotTaxObj() {
        return getTotTax();
    }

    @Override
    public void setTotTaxObj(AfDecimal totTaxObj) {
        setTotTax(new AfDecimal(totTaxObj, 15, 3));
    }

    @Override
    public String getTpCausDispStor() {
        throw new FieldNotMappedException("tpCausDispStor");
    }

    @Override
    public void setTpCausDispStor(String tpCausDispStor) {
        throw new FieldNotMappedException("tpCausDispStor");
    }

    @Override
    public String getTpCausDispStorObj() {
        return getTpCausDispStor();
    }

    @Override
    public void setTpCausDispStorObj(String tpCausDispStorObj) {
        setTpCausDispStor(tpCausDispStorObj);
    }

    @Override
    public String getTpCausRimb() {
        throw new FieldNotMappedException("tpCausRimb");
    }

    @Override
    public void setTpCausRimb(String tpCausRimb) {
        throw new FieldNotMappedException("tpCausRimb");
    }

    @Override
    public String getTpCausRimbObj() {
        return getTpCausRimb();
    }

    @Override
    public void setTpCausRimbObj(String tpCausRimbObj) {
        setTpCausRimb(tpCausRimbObj);
    }

    @Override
    public int getTpCausStor() {
        throw new FieldNotMappedException("tpCausStor");
    }

    @Override
    public void setTpCausStor(int tpCausStor) {
        throw new FieldNotMappedException("tpCausStor");
    }

    @Override
    public Integer getTpCausStorObj() {
        return ((Integer)getTpCausStor());
    }

    @Override
    public void setTpCausStorObj(Integer tpCausStorObj) {
        setTpCausStor(((int)tpCausStorObj));
    }

    @Override
    public String getTpEsiRid() {
        throw new FieldNotMappedException("tpEsiRid");
    }

    @Override
    public void setTpEsiRid(String tpEsiRid) {
        throw new FieldNotMappedException("tpEsiRid");
    }

    @Override
    public String getTpEsiRidObj() {
        return getTpEsiRid();
    }

    @Override
    public void setTpEsiRidObj(String tpEsiRidObj) {
        setTpEsiRid(tpEsiRidObj);
    }

    @Override
    public String getTpMezPagAdd() {
        throw new FieldNotMappedException("tpMezPagAdd");
    }

    @Override
    public void setTpMezPagAdd(String tpMezPagAdd) {
        throw new FieldNotMappedException("tpMezPagAdd");
    }

    @Override
    public String getTpMezPagAddObj() {
        return getTpMezPagAdd();
    }

    @Override
    public void setTpMezPagAddObj(String tpMezPagAddObj) {
        setTpMezPagAdd(tpMezPagAddObj);
    }

    @Override
    public String getTpTitMigraz() {
        throw new FieldNotMappedException("tpTitMigraz");
    }

    @Override
    public void setTpTitMigraz(String tpTitMigraz) {
        throw new FieldNotMappedException("tpTitMigraz");
    }

    @Override
    public String getTpTitMigrazObj() {
        return getTpTitMigraz();
    }

    @Override
    public void setTpTitMigrazObj(String tpTitMigrazObj) {
        setTpTitMigraz(tpTitMigrazObj);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ldbs1590.getWs().getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        ldbs1590.getWs().getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        return ldbs1590.getWs().getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        ldbs1590.getWs().getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }

    @Override
    public long getWsTsInfinito1() {
        throw new FieldNotMappedException("wsTsInfinito1");
    }

    @Override
    public void setWsTsInfinito1(long wsTsInfinito1) {
        throw new FieldNotMappedException("wsTsInfinito1");
    }
}
