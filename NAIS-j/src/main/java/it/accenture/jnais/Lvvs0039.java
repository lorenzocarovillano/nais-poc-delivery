package it.accenture.jnais;

import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0039Data;
import it.accenture.jnais.ws.ptr.AreaIvvc0223;
import javax.inject.Inject;

/**Original name: LVVS0039<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0039
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CALCOLO VARIABILE CUMPRENETTI
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0039 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Lvvs0039Data ws = new Lvvs0039Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0039
    private Ivvc0213 ivvc0213;
    //Original name: AREA-IVVC0223
    private AreaIvvc0223 areaIvvc0223 = new AreaIvvc0223(null);

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0039_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0039 getInstance() {
        return ((Lvvs0039)Programs.getInstance(Lvvs0039.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBVF981
        //                      IMPORTO-TOTALE
        //                      DETT-TIT-CONT.
        initLdbvf981();
        ws.setImportoTotale(new AfDecimal(0, 15, 3));
        initDettTitCont();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.getIxIndici().setIndUnzip(((short)1));
        while (!(ws.getIxIndici().getIndUnzip() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.getIxIndici().setIndUnzip(Trunc.toShort(ws.getIxIndici().getIndUnzip() + 1, 4));
        }
        // COB_CODE: SET ADDRESS OF AREA-IVVC0223
        //                       TO VXG-ADDRESS
        areaIvvc0223 = (((AreaIvvc0223)pointerManager.resolve(ws.getVxgAddress(), AreaIvvc0223.class)));
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //             END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO TO WS-MOVIMENTO
            ws.getWsMovimento().setWsMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
            // COB_CODE: IF CREAZ-INDIVI
            //             MOVE ZEROES                TO IVVC0213-VAL-IMP-O
            //           ELSE
            //             END-IF
            //           END-IF
            if (ws.getWsMovimento().isCreazIndivi()) {
                // COB_CODE: MOVE ZEROES                TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
            }
            else {
                // COB_CODE: PERFORM S1240-CONTROLLA-GAR
                //              THRU EX-S1250
                s1240ControllaGar();
                // COB_CODE: IF TROVATA-VAR-NO
                //              END-IF
                //           END-IF
                if (ws.getTrovataVar().isNo()) {
                    // COB_CODE: PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
                    s1250CalcolaData1();
                    // COB_CODE: IF  NOT IDSV0003-INVALID-OPER
                    //                 THRU EX-S1260
                    //           END-IF
                    if (!idsv0003.getReturnCode().isIdsv0003InvalidOper()) {
                        // COB_CODE: PERFORM S1260-CARICA-AREA-GAR
                        //              THRU EX-S1260
                        s1260CaricaAreaGar();
                    }
                }
            }
        }
    }

    /**Original name: S1240-CONTROLLA-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 * --> CONTROLLO L ID GAR DI CONTESTO CON L ARIA DELLE GARANZIE PER
	 * --> EVITARE LETTURE MULTIPLE SULLA STESSA GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1240ControllaGar() {
        // COB_CODE: SET TROVATA-GAR-NO           TO TRUE
        ws.getTrovataGar().setNo();
        // COB_CODE: SET TROVATA-VAR-NO           TO TRUE
        ws.getTrovataVar().setNo();
        // COB_CODE: PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
        //             UNTIL IX-TAB-GRZ > IVVC0223-ELE-MAX-AREA-GAR
        //                OR TROVATA-GAR-SI
        //                   END-IF
        //           END-PERFORM.
        ws.getIxIndici().setIndEndVar(((short)1));
        while (!(ws.getIxIndici().getIndEndVar() > areaIvvc0223.getEleMaxAreaGar() || ws.getTrovataGar().isSi())) {
            // COB_CODE: IF IVVC0213-ID-GARANZIA =
            //              IVVC0223-ID-GARANZIA(IX-TAB-GRZ)
            //              END-PERFORM
            //           END-IF
            if (ivvc0213.getIdGaranzia() == areaIvvc0223.getIdGaranzia(ws.getIxIndici().getIndEndVar())) {
                // COB_CODE: SET  TROVATA-GAR-SI         TO TRUE
                ws.getTrovataGar().setSi();
                // COB_CODE: MOVE IX-TAB-GRZ TO IX-TAB-TEMP
                ws.getIxIndici().setIxTabCao(ws.getIxIndici().getIndEndVar());
                // COB_CODE: PERFORM VARYING IX-TAB-VAR FROM 1 BY 1
                //             UNTIL IX-TAB-VAR >
                //                   IVVC0223-ELE-MAX-VAR-GAR(IX-TAB-GRZ)
                //                OR TROVATA-VAR-SI
                //                   END-IF
                //           END-PERFORM
                ws.getIxIndici().setIndChar(((short)1));
                while (!(ws.getIxIndici().getIndChar() > areaIvvc0223.getEleMaxVarGar(ws.getIxIndici().getIndEndVar()) || ws.getTrovataVar().isSi())) {
                    // COB_CODE: IF IVVC0223-COD-VARIABILE
                    //              (IX-TAB-GRZ, IX-TAB-VAR) =
                    //              IVVC0213-COD-VARIABILE
                    //                TO IVVC0213-VAL-IMP-O
                    //           END-IF
                    if (Conditions.eq(areaIvvc0223.getCodVariabile(ws.getIxIndici().getIndEndVar(), ws.getIxIndici().getIndChar()), ivvc0213.getDatiLivello().getCodVariabile())) {
                        // COB_CODE: SET  TROVATA-VAR-SI    TO TRUE
                        ws.getTrovataVar().setSi();
                        // COB_CODE: MOVE IVVC0223-VAL-IMP
                        //                (IX-TAB-GRZ, IX-TAB-VAR)
                        //             TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(areaIvvc0223.getValImp(ws.getIxIndici().getIndEndVar(), ws.getIxIndici().getIndChar()), 18, 7));
                    }
                    ws.getIxIndici().setIndChar(Trunc.toShort(ws.getIxIndici().getIndChar() + 1, 4));
                }
            }
            ws.getIxIndici().setIndEndVar(Trunc.toShort(ws.getIxIndici().getIndEndVar() + 1, 4));
        }
    }

    /**Original name: S1260-CARICA-AREA-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 * --> VALORIZZAZIONE AREA DI SCAMBIO TRA VALORIZZATORE E MODULO DI
	 * --> CALCOLO PER EVITARE LETTURE MULTIPLE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1260CaricaAreaGar() {
        // COB_CODE: IF TROVATA-GAR-NO
        //                TO IVVC0213-VAL-IMP-O
        //           END-IF
        if (ws.getTrovataGar().isNo()) {
            // COB_CODE: ADD 1
            //              TO IVVC0223-ELE-MAX-AREA-GAR
            areaIvvc0223.setEleMaxAreaGar(Trunc.toShort(1 + areaIvvc0223.getEleMaxAreaGar(), 4));
            // COB_CODE: MOVE IVVC0223-ELE-MAX-AREA-GAR
            //             TO IX-TAB-TEMP
            ws.getIxIndici().setIxTabCao(areaIvvc0223.getEleMaxAreaGar());
            // COB_CODE: MOVE IVVC0213-ID-GARANZIA
            //              TO IVVC0223-ID-GARANZIA(IX-TAB-TEMP)
            areaIvvc0223.setIdGaranziaFormatted(ws.getIxIndici().getIxTabCao(), ivvc0213.getIdGaranziaFormatted());
            // COB_CODE: MOVE IVVC0213-COD-LIVELLO
            //              TO IVVC0223-COD-TARI(IX-TAB-TEMP)
            areaIvvc0223.setCodTari(ws.getIxIndici().getIxTabCao(), ivvc0213.getDatiLivello().getCodLivello());
            // COB_CODE: MOVE IMPORTO-TOTALE
            //             TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getImportoTotale(), 18, 7));
        }
        // COB_CODE: ADD 1
        //             TO IVVC0223-ELE-MAX-VAR-GAR(IX-TAB-TEMP)
        areaIvvc0223.setEleMaxVarGar(ws.getIxIndici().getIxTabCao(), Trunc.toShort(1 + areaIvvc0223.getEleMaxVarGar(ws.getIxIndici().getIxTabCao()), 4));
        // COB_CODE: MOVE IVVC0223-ELE-MAX-VAR-GAR(IX-TAB-TEMP)
        //             TO IX-TAB-TEMP2
        ws.getIxIndici().setIxAutOper(areaIvvc0223.getEleMaxVarGar(ws.getIxIndici().getIxTabCao()));
        // COB_CODE: MOVE IVVC0213-COD-VARIABILE
        //             TO IVVC0223-COD-VARIABILE(IX-TAB-TEMP, IX-TAB-TEMP2)
        areaIvvc0223.setCodVariabile(ws.getIxIndici().getIxTabCao(), ws.getIxIndici().getIxAutOper(), ivvc0213.getDatiLivello().getCodVariabile());
        // COB_CODE: MOVE 'N'
        //             TO IVVC0223-TP-DATO(IX-TAB-TEMP, IX-TAB-TEMP2)
        areaIvvc0223.setTpDatoFormatted(ws.getIxIndici().getIxTabCao(), ws.getIxIndici().getIxAutOper(), "N");
        // COB_CODE: MOVE IMPORTO-TOTALE
        //             TO IVVC0223-VAL-IMP(IX-TAB-TEMP, IX-TAB-TEMP2).
        areaIvvc0223.setValImp(ws.getIxIndici().getIxTabCao(), ws.getIxIndici().getIxAutOper(), Trunc.toDecimal(ws.getImportoTotale(), 18, 7));
        // COB_CODE: SET TROVATA-AREA-NO        TO TRUE.
        ws.getTrovataArea().setNo();
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POL
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POL
            ws.setDpolAreaPolFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getLunghezza() - 1));
        }
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-AREA-VAR-X-GAR
        //                TO AREA-POINTER
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getTabAlias(), ws.getIvvc0218().getAliasAreaVarXGar())) {
            //         TO AREA-IVVC0223
            // COB_CODE:         MOVE IVVC0213-BUFFER-DATI
            //                       (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                        IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //           *         TO AREA-IVVC0223
            //                     TO AREA-POINTER
            ws.setAreaPointerFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getIndUnzip()).getLunghezza() - 1));
        }
    }

    /**Original name: S1250-CALCOLA-DATA1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaData1() {
        Ldbsf980 ldbsf980 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-FETCH-FIRST          TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA   TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE IVVC0213-ID-GARANZIA         TO LDBVF981-ID-GAR
        ws.getLdbvf981().setIdOgg(ivvc0213.getIdGaranzia());
        // COB_CODE: MOVE 'EM'                         TO LDBVF981-TP-STAT-TIT-1
        ws.getLdbvf981().setTpStatTit1("EM");
        // COB_CODE: MOVE 'IN'                         TO LDBVF981-TP-STAT-TIT-2
        ws.getLdbvf981().setTpStatTit2("IN");
        // COB_CODE: MOVE SPACES                       TO LDBVF981-TP-STAT-TIT-3
        ws.getLdbvf981().setTpStatTit3("");
        // COB_CODE: MOVE SPACES                       TO LDBVF981-TP-STAT-TIT-4
        ws.getLdbvf981().setTpStatTit4("");
        // COB_CODE: MOVE SPACES                       TO LDBVF981-TP-STAT-TIT-5
        ws.getLdbvf981().setTpStatTit5("");
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO LDBVF981-DT-INI-COP
        ws.getLdbvf981().setDtDecorTrch(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE LDBVF981
        //             TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond(ws.getLdbvf981().getLdbvf981Formatted());
        // COB_CODE: MOVE 'LDBSF980'                   TO WK-CALL-PGM
        ws.setWkCallPgm("LDBSF980");
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC OR
        //                         NOT IDSV0003-SUCCESSFUL-SQL
        //              END-EVALUATE
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql())) {
            //
            // COB_CODE:       CALL WK-CALL-PGM  USING  IDSV0003 DETT-TIT-CONT
            //           *
            //                ON EXCEPTION
            //                     SET IDSV0003-INVALID-OPER  TO TRUE
            //                 END-CALL
            try {
                ldbsf980 = Ldbsf980.getInstance();
                ldbsf980.run(idsv0003, ws.getDettTitCont());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: MOVE 'ERRORE CHIAMATA LDBSF980 - LVVS0039'
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CHIAMATA LDBSF980 - LVVS0039");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: EVALUATE TRUE
            //             WHEN IDSV0003-SUCCESSFUL-SQL
            //             PERFORM Z000-CALCOLA       THRU Z000-EX
            //             WHEN IDSV0003-NOT-FOUND
            //                END-IF
            //             WHEN OTHER
            //                SET IDSV0003-INVALID-OPER            TO TRUE
            //             END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: SET IDSV0003-FETCH-NEXT    TO TRUE
                    idsv0003.getOperazione().setFetchNext();
                    // COB_CODE: PERFORM Z000-CALCOLA       THRU Z000-EX
                    z000Calcola();
                    break;

                case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: IF IDSV0003-FETCH-FIRST
                    //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    //           END-IF
                    if (idsv0003.getOperazione().isFetchFirst()) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                    }
                    break;

                default:// COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                    // COB_CODE:    STRING 'CHIAMATA LDBSF980 ;'
                    //                  IDSV0003-RETURN-CODE '-'
                    //                  IDSV0003-SQLCODE
                    //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBSF980 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), "-", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    break;
            }
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*
	 *     MOVE DPOL-TP-FRM-ASSVA            TO WS-TP-FRM-ASSVA</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: IF IVVC0213-ID-TRANCHE IS NUMERIC
        //              END-IF
        //           ELSE
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           END-IF.
        if (Functions.isNumber(ivvc0213.getIdTrancheFormatted())) {
            // COB_CODE: IF IVVC0213-ID-GARANZIA = 0
            //               TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ivvc0213.getIdGaranzia() == 0) {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ID-GARANZIA NON VALORIZZATO'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ID-GARANZIA NON VALORIZZATO");
            }
        }
        else {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ID-GARANZIA NON VALORIZZATO'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ID-GARANZIA NON VALORIZZATO");
        }
    }

    /**Original name: Z000-CALCOLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALCOLA IMPORTO DEFINITIVO
	 * ----------------------------------------------------------------*</pre>*/
    private void z000Calcola() {
        // COB_CODE: IF DTC-PRE-TOT-NULL NOT = HIGH-VALUE AND LOW-VALUE AND SPACE
        //                 TO IMPORTO-TOTALE
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ws.getDettTitCont().getDtcPreTot().getDtcPreTotNullFormatted()) && !Characters.EQ_LOW.test(ws.getDettTitCont().getDtcPreTot().getDtcPreTotNullFormatted()) && !Characters.EQ_SPACE.test(ws.getDettTitCont().getDtcPreTot().getDtcPreTotNull())) {
            // COB_CODE: ADD DTC-PRE-TOT
            //              TO IMPORTO-TOTALE
            ws.setImportoTotale(Trunc.toDecimal(ws.getDettTitCont().getDtcPreTot().getDtcPreTot().add(ws.getImportoTotale()), 15, 3));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.getIxIndici().setIndUnzip(((short)0));
        ws.getIxIndici().setIndStartVar(((short)0));
        ws.getIxIndici().setIndEndVar(((short)0));
        ws.getIxIndici().setIndChar(((short)0));
        ws.getIxIndici().setIxTabCao(((short)0));
        ws.getIxIndici().setIxAutOper(((short)0));
        ws.getIxIndici().setIxParam(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbvf981() {
        ws.getLdbvf981().setIdOgg(0);
        ws.getLdbvf981().setTpStatTit1("");
        ws.getLdbvf981().setTpStatTit2("");
        ws.getLdbvf981().setTpStatTit3("");
        ws.getLdbvf981().setTpStatTit4("");
        ws.getLdbvf981().setTpStatTit5("");
        ws.getLdbvf981().setDtDecorTrch(0);
    }

    public void initDettTitCont() {
        ws.getDettTitCont().setDtcIdDettTitCont(0);
        ws.getDettTitCont().setDtcIdTitCont(0);
        ws.getDettTitCont().setDtcIdOgg(0);
        ws.getDettTitCont().setDtcTpOgg("");
        ws.getDettTitCont().setDtcIdMoviCrz(0);
        ws.getDettTitCont().getDtcIdMoviChiu().setDtcIdMoviChiu(0);
        ws.getDettTitCont().setDtcDtIniEff(0);
        ws.getDettTitCont().setDtcDtEndEff(0);
        ws.getDettTitCont().setDtcCodCompAnia(0);
        ws.getDettTitCont().getDtcDtIniCop().setDtcDtIniCop(0);
        ws.getDettTitCont().getDtcDtEndCop().setDtcDtEndCop(0);
        ws.getDettTitCont().getDtcPreNet().setDtcPreNet(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrFraz().setDtcIntrFraz(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrMora().setDtcIntrMora(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrRetdt().setDtcIntrRetdt(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcIntrRiat().setDtcIntrRiat(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcDir().setDtcDir(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSpeMed().setDtcSpeMed(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcTax().setDtcTax(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprSan().setDtcSoprSan(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprSpo().setDtcSoprSpo(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprTec().setDtcSoprTec(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprProf().setDtcSoprProf(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcSoprAlt().setDtcSoprAlt(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcPreTot().setDtcPreTot(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcPrePpIas().setDtcPrePpIas(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcPreSoloRsh().setDtcPreSoloRsh(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarAcq().setDtcCarAcq(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarGest().setDtcCarGest(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarInc().setDtcCarInc(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvAcq1aa().setDtcProvAcq1aa(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvAcq2aa().setDtcProvAcq2aa(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvRicor().setDtcProvRicor(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvInc().setDtcProvInc(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcProvDaRec().setDtcProvDaRec(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().setDtcCodDvs("");
        ws.getDettTitCont().getDtcFrqMovi().setDtcFrqMovi(0);
        ws.getDettTitCont().setDtcTpRgmFisc("");
        ws.getDettTitCont().setDtcCodTari("");
        ws.getDettTitCont().setDtcTpStatTit("");
        ws.getDettTitCont().getDtcImpAz().setDtcImpAz(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpAder().setDtcImpAder(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpTfr().setDtcImpTfr(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpVolo().setDtcImpVolo(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcManfeeAntic().setDtcManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcManfeeRicor().setDtcManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcManfeeRec().setDtcManfeeRec(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcDtEsiTit().setDtcDtEsiTit(0);
        ws.getDettTitCont().getDtcSpeAge().setDtcSpeAge(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCarIas().setDtcCarIas(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcTotIntrPrest().setDtcTotIntrPrest(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().setDtcDsRiga(0);
        ws.getDettTitCont().setDtcDsOperSql(Types.SPACE_CHAR);
        ws.getDettTitCont().setDtcDsVer(0);
        ws.getDettTitCont().setDtcDsTsIniCptz(0);
        ws.getDettTitCont().setDtcDsTsEndCptz(0);
        ws.getDettTitCont().setDtcDsUtente("");
        ws.getDettTitCont().setDtcDsStatoElab(Types.SPACE_CHAR);
        ws.getDettTitCont().getDtcImpTrasfe().setDtcImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcImpTfrStrc().setDtcImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcNumGgRitardoPag().setDtcNumGgRitardoPag(0);
        ws.getDettTitCont().getDtcNumGgRival().setDtcNumGgRival(0);
        ws.getDettTitCont().getDtcAcqExp().setDtcAcqExp(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcRemunAss().setDtcRemunAss(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCommisInter().setDtcCommisInter(new AfDecimal(0, 15, 3));
        ws.getDettTitCont().getDtcCnbtAntirac().setDtcCnbtAntirac(new AfDecimal(0, 15, 3));
    }
}
