package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.CompNumOgg;
import it.accenture.jnais.copy.Idso0021;
import it.accenture.jnais.copy.Idso0021AreaErrori;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.WpagDatiInput;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WpagCodOggetto;
import it.accenture.jnais.ws.Idss0160Data;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import it.accenture.jnais.ws.redefines.D03ProgrFinale;
import it.accenture.jnais.ws.redefines.D03ProgrIniziale;
import it.accenture.jnais.ws.redefines.Idso0021StrutturaDato;
import it.accenture.jnais.ws.WpagAreaIo;
import it.accenture.jnais.ws.WsVariabiliIdss0160;
import javax.inject.Inject;
import static java.lang.Math.abs;

/**Original name: IDSS0160<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
 * *                                                        ********
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2009.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... IDSS0160
 *     TIPOLOGIA...... SERVIZIO TRASVERSALE
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... ESTRAZIONE PROPOSTA OGGETTO
 * **------------------------------------------------------------***</pre>*/
public class Idss0160 extends Program {

    //==== PROPERTIES ====
    @Inject
    private IPointerManager pointerManager;
    //Original name: WORKING-STORAGE
    private Idss0160Data ws = new Idss0160Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WPAG-AREA-IO
    private WpagAreaIo wpagAreaIo;

    //==== METHODS ====
    /**Original name: PROGRAM_IDSS0160_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, WpagAreaIo wpagAreaIo) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wpagAreaIo = wpagAreaIo;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Idss0160 getInstance() {
        return ((Idss0160)Programs.getInstance(Idss0160.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IDSS0020'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IDSS0020");
        // COB_CODE: MOVE 'Initialize area di work'      TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Initialize area di work");
        // COB_CODE: SET  IDSV8888-INIZIO                TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //                                    AREA-TAB-APPOGGIO
        // COB_CODE:      INITIALIZE                      WS-VARIABILI
        //                                                IX-INDICI
        //           *                                    AREA-TAB-APPOGGIO
        //                                                WKS-ELE-MAX-TABB
        //                                                WKS-NOME-TABB(1)
        //                                                WKS-ELE-MAX-DATO(1)
        //                                                PROGR-NUM-OGG.
        initWsVariabili();
        initIxIndici();
        ws.setWksEleMaxTabb(((short)0));
        ws.getWksTabStrDato().setNomeTabb(1, "");
        ws.getWksTabStrDato().setEleMaxDato(1, ((short)0));
        initProgrNumOgg();
        // COB_CODE: PERFORM VARYING IX-WKS-TAB FROM 1 BY 1
        //                   UNTIL   IX-WKS-TAB  >  1000
        //                   INITIALIZE   WKS-ELEMENTS-STR-DATO(1,IX-WKS-TAB)
        //           END-PERFORM.
        ws.getIxIndici().setIxWksTab(((short)1));
        while (!(ws.getIxIndici().getIxWksTab() > 1000)) {
            // COB_CODE: INITIALIZE   WKS-ELEMENTS-STR-DATO(1,IX-WKS-TAB)
            initElementsStrDato();
            ws.getIxIndici().setIxWksTab(Trunc.toShort(ws.getIxIndici().getIxWksTab() + 1, 4));
        }
        // COB_CODE: MOVE  WKS-TAB-STR-DATO  TO  WKS-RESTO-TAB-STR-DATO.
        ws.getWksTabStrDato().setRestoTabStrDato(ws.getWksTabStrDato().getWksTabStrDatoFormatted());
        //--> VALORIZZA TAB INPUT
        // COB_CODE: PERFORM A0010-VALORIZZA-DCLGEN  THRU A0010-EX
        //                   VARYING IND-STR FROM 1 BY 1
        //                     UNTIL IND-STR > WPAG-ELE-INFO-MAX OR
        //                           WPAG-TAB-ALIAS(IND-STR) =
        //                      SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.getIxIndici().setIndStr(((short)1));
        while (!(ws.getIxIndici().getIndStr() > wpagAreaIo.getDatiInput().getEleInfoMax() || Characters.EQ_SPACE.test(wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getTabAlias()) || Characters.EQ_LOW.test(wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getTabAliasFormatted()) || Characters.EQ_HIGH.test(wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getTabAliasFormatted()))) {
            a0010ValorizzaDclgen();
            ws.getIxIndici().setIndStr(Trunc.toShort(ws.getIxIndici().getIndStr() + 1, 4));
        }
        //
        // COB_CODE: PERFORM INIZIA-TOT-D03
        //              THRU INIZIA-TOT-D03-EX.
        iniziaTotD03();
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IDSS0020'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IDSS0020");
        // COB_CODE: MOVE 'Initialize area di work'      TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Initialize area di work");
        // COB_CODE: SET  IDSV8888-FINE                  TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT
        //              THRU EX-S0005.
        s0005CtrlDatiInput();
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT
	 * ----------------------------------------------------------------*
	 * --> CODICE COMPAGNIA OBBLIGATORIO</pre>*/
    private void s0005CtrlDatiInput() {
        // COB_CODE: IF IDSV0001-COD-COMPAGNIA-ANIA = ZEROES
        //                 THRU EX-S0300
        //           END-IF.
        if (Characters.EQ_ZERO.test(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted())) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
            // COB_CODE: MOVE '005026'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005026");
            // COB_CODE: MOVE 'COD-COMP-ANIA'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("COD-COMP-ANIA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        //--> TIPO FORMA ASSICURATIVA OBBLIGATORIO
        // COB_CODE: IF WPOL-TP-FRM-ASSVA = SPACES OR HIGH-VALUES
        //                 THRU EX-S0300
        //           END-IF.
        if (Characters.EQ_SPACE.test(ws.getLccvpol1().getDati().getWpolTpFrmAssva()) || Characters.EQ_HIGH.test(ws.getLccvpol1().getDati().getWpolTpFrmAssvaFormatted())) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
            // COB_CODE: MOVE '005026'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005026");
            // COB_CODE: MOVE 'TP-FRM-ASSVA'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("TP-FRM-ASSVA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        //--> CODICE OGGETTO OBBLIGATORIO
        // COB_CODE: IF WPAG-COD-OGGETTO = SPACES OR HIGH-VALUES
        //                 THRU EX-S0300
        //           END-IF.
        if (Characters.EQ_SPACE.test(wpagAreaIo.getDatiInput().getCodOggetto().getCodOggetto()) || Characters.EQ_HIGH.test(wpagAreaIo.getDatiInput().getCodOggetto().getCodOggetto(), WpagCodOggetto.Len.COD_OGGETTO)) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
            // COB_CODE: MOVE '005026'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005026");
            // COB_CODE: MOVE 'COD-OGGETTO'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("COD-OGGETTO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        //--> NEL CASO DI NUMERAZIONE MANUALE DEVE ESSERE VALORIZZATO IL
        //--> CAMPO IB-OGGETTO-I
        // COB_CODE: IF WPAG-MANUALE
        //           END-IF.
        if (wpagAreaIo.getDatiInput().getTipoNumerazione().isManuale()) {
            // COB_CODE:    IF WPAG-IB-OGGETTO-I = SPACES OR HIGH-VALUE
            //                 THRU EX-S0300
            //           END-IF.
            if (Characters.EQ_SPACE.test(wpagAreaIo.getDatiInput().getIbOggettoI()) || Characters.EQ_HIGH.test(wpagAreaIo.getDatiInput().getIbOggettoI(), WpagDatiInput.Len.IB_OGGETTO_I)) {
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005026'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005026");
                // COB_CODE: MOVE 'IB-OGGETTO-I'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("IB-OGGETTO-I");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        //--> NEL CASO DI NUMERAZIONE AUTOMATICA IL CAMPO IB-OGGETTO-I
        //--> DEVE ESSERE VALORIZZATO A SPACES
        // COB_CODE: IF WPAG-AUTOMATICA
        //           END-IF.
        if (wpagAreaIo.getDatiInput().getTipoNumerazione().isAutomatica()) {
            // COB_CODE:    IF WPAG-IB-OGGETTO-I NOT = SPACES
            //                 THRU EX-S0300
            //           END-IF.
            if (!Characters.EQ_SPACE.test(wpagAreaIo.getDatiInput().getIbOggettoI())) {
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005008'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005008");
                // COB_CODE: MOVE 'IB-OGGETTO-I'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("IB-OGGETTO-I");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: MOVE 1                          TO WS-POSIZIONE-IB.
        ws.getWsVariabili().setWsPosizioneIb(1);
        // COB_CODE: SET WS-PRIMA-LETTURA            TO TRUE.
        ws.getWsLettura().setPrimaLettura();
        //
        //--> LETTURA DELLA TABELLA COMP-NUM-OGG PER COMPORRE L'IB-OGGETTO
        //
        // COB_CODE: PERFORM S1100-LEGGI-COMP-NUM-OGG
        //              THRU EX-S1100.
        s1100LeggiCompNumOgg();
        //
        // COB_CODE: IF WS-SECONDA-LETTURA
        //           AND IDSV0001-ESITO-OK
        //                 THRU EX-S1100
        //           END-IF.
        if (ws.getWsLettura().isSecondaLettura() && areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1100-LEGGI-COMP-NUM-OGG
            //              THRU EX-S1100
            s1100LeggiCompNumOgg();
        }
    }

    /**Original name: S1100-LEGGI-COMP-NUM-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DELLA TABELLA COMP-NUM-OGG PER COMPORRE L'IB-OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100LeggiCompNumOgg() {
        // COB_CODE: PERFORM S1150-PREPARA-CNO            THRU EX-S1150.
        s1150PreparaCno();
        //
        // COB_CODE:      PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC  OR
        //                              NOT IDSO0011-SUCCESSFUL-SQL
        //           *
        //                  END-IF
        //                END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql())) {
            //
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:        IF IDSO0011-SUCCESSFUL-RC
            //                     END-EVALUATE
            //                  ELSE
            //           *-->      ERRORE DISPATCHER
            //                     PERFORM S9001-ERRORE-COMUNE      THRU EX-S9001
            //                  END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:           EVALUATE TRUE
                //                         WHEN IDSO0011-SUCCESSFUL-SQL
                //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                 THRU EX-S1101
                //           *
                //                         WHEN IDSO0011-NOT-FOUND
                //           *--->         CHIAVE NON TROVATA SU TABELLA $
                //                                 THRU EX-S1102
                //                         WHEN OTHER
                //           *-->          ERRORE DI ACCESSO AL DB
                //                                 THRU EX-S9001
                //                     END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: PERFORM S1101-LETTURA-CNO-OK
                        //              THRU EX-S1101
                        s1101LetturaCnoOk();
                        //
                        break;

                    case Idso0011SqlcodeSigned.NOT_FOUND://--->         CHIAVE NON TROVATA SU TABELLA $
                        // COB_CODE: PERFORM S1102-LETTURA-CNO-100
                        //              THRU EX-S1102
                        s1102LetturaCno100();
                        break;

                    default://-->          ERRORE DI ACCESSO AL DB
                        // COB_CODE: PERFORM S9001-ERRORE-COMUNE
                        //              THRU EX-S9001
                        s9001ErroreComune();
                        break;
                }
            }
            else {
                //-->      ERRORE DISPATCHER
                // COB_CODE: PERFORM S9001-ERRORE-COMUNE      THRU EX-S9001
                s9001ErroreComune();
            }
        }
    }

    /**Original name: S1150-PREPARA-CNO<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE DELL'OUTPUT DELLA TABELLA CON LETTURA PROGR-OGG
	 * ----------------------------------------------------------------*</pre>*/
    private void s1150PreparaCno() {
        // COB_CODE: MOVE 'COMP-NUM-OGG'           TO WS-TABELLA.
        ws.getWsVariabili().setWsTabella("COMP-NUM-OGG");
        //--> DATA EFFETTO
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //--> INIZIALIZZAZIONE FLAG ESITO
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //--> TABELLA STORICA
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'LDBS4870'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS4870");
        //--> VALORIZZA DCLGEN TABELLA
        // COB_CODE: INITIALIZE                       COMP-NUM-OGG.
        initCompNumOgg();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO CNO-COD-COMPAGNIA-ANIA.
        ws.getCompNumOgg().setCnoCodCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: IF WS-SECONDA-LETTURA
        //              MOVE 'EN'                  TO CNO-FORMA-ASSICURATIVA
        //           ELSE
        //              MOVE WPOL-TP-FRM-ASSVA     TO CNO-FORMA-ASSICURATIVA
        //           END-IF.
        if (ws.getWsLettura().isSecondaLettura()) {
            // COB_CODE: MOVE 'EN'                  TO CNO-FORMA-ASSICURATIVA
            ws.getCompNumOgg().setCnoFormaAssicurativa("EN");
        }
        else {
            // COB_CODE: MOVE WPOL-TP-FRM-ASSVA     TO CNO-FORMA-ASSICURATIVA
            ws.getCompNumOgg().setCnoFormaAssicurativa(ws.getLccvpol1().getDati().getWpolTpFrmAssva());
        }
        // COB_CODE: MOVE WPAG-COD-OGGETTO         TO CNO-COD-OGGETTO.
        ws.getCompNumOgg().setCnoCodOggetto(wpagAreaIo.getDatiInput().getCodOggetto().getCodOggetto());
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE COMP-NUM-OGG             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getCompNumOgg().getCompNumOggFormatted());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
    }

    /**Original name: S1101-LETTURA-CNO-OK<br>
	 * <pre>----------------------------------------------------------------*
	 *    GESTIONE LETTURA OK
	 * ----------------------------------------------------------------*</pre>*/
    private void s1101LetturaCnoOk() {
        // COB_CODE: MOVE IDSO0011-BUFFER-DATI       TO COMP-NUM-OGG.
        ws.getCompNumOgg().setCompNumOggFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
        // COB_CODE: MOVE IDSI0011-AREA              TO WKS-APPO-IDSI0011.
        ws.setWksAppoIdsi0011(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011AreaFormatted());
        // COB_CODE: ADD 1                           TO IX-TAB-CNO.
        ws.getIxIndici().setIxTabCno(Trunc.toShort(1 + ws.getIxIndici().getIxTabCno(), 4));
        // COB_CODE: IF CNO-FLAG-KEY-ULT-PROGR = 'P'
        //                 THRU EX-S110B
        //           ELSE
        //                THRU EX-S110A
        //           END-IF.
        if (ws.getCompNumOgg().getCnoFlagKeyUltProgr() == 'P') {
            // COB_CODE: PERFORM S110B-GEST-OUTPUT-CNO-P
            //              THRU EX-S110B
            s110bGestOutputCnoP();
        }
        else {
            // COB_CODE: PERFORM S110A-GEST-OUTPUT-CNO
            //             THRU EX-S110A
            s110aGestOutputCno();
        }
        //
        // COB_CODE: MOVE WKS-APPO-IDSI0011          TO IDSI0011-AREA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011AreaFormatted(ws.getWksAppoIdsi0011Formatted());
        // COB_CODE: SET IDSI0011-FETCH-NEXT         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
    }

    /**Original name: S1102-LETTURA-CNO-100<br>
	 * <pre>----------------------------------------------------------------*
	 *    GESTIONE LETTURA NOT FOUND
	 * ----------------------------------------------------------------*</pre>*/
    private void s1102LetturaCno100() {
        // COB_CODE: IF IDSI0011-FETCH-FIRST
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
            // COB_CODE: IF WS-PRIMA-LETTURA
            //              SET WS-SECONDA-LETTURA             TO TRUE
            //           ELSE
            //                 THRU EX-S0300
            //           END-IF
            if (ws.getWsLettura().isPrimaLettura()) {
                // COB_CODE: SET WS-SECONDA-LETTURA             TO TRUE
                ws.getWsLettura().setSecondaLettura();
            }
            else {
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S1100-LEGGI-COMP-NUM-OGG'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S1100-LEGGI-COMP-NUM-OGG");
                // COB_CODE: MOVE '005056'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005056");
                // COB_CODE: MOVE 'COMP-NUM-OGG'
                //             TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("COMP-NUM-OGG");
                //
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        else {
            // COB_CODE: IF IX-TAB-CNO = 1
            //           OR WS-QTA-FLAG-P GREATER 1
            //                 THRU EX-S0300
            //           END-IF
            if (ws.getIxIndici().getIxTabCno() == 1 || ws.getWsVariabili().getWsQtaFlagP() > 1) {
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S1100-LEGGI-COMP-NUM-OGG'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S1100-LEGGI-COMP-NUM-OGG");
                // COB_CODE: IF IX-TAB-CNO = 1
                //                TO IEAI9901-PARAMETRI-ERR
                //           END-IF
                if (ws.getIxIndici().getIxTabCno() == 1) {
                    // COB_CODE: MOVE '001114'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("001114");
                    // COB_CODE: MOVE 'UNA SOLA OCCORR TROVATA SU COMP-NUM-OGG'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("UNA SOLA OCCORR TROVATA SU COMP-NUM-OGG");
                }
                // COB_CODE: IF WS-QTA-FLAG-P GREATER 1
                //                TO IEAI9901-PARAMETRI-ERR
                //           END-IF
                if (ws.getWsVariabili().getWsQtaFlagP() > 1) {
                    // COB_CODE: MOVE '001114'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("001114");
                    // COB_CODE: MOVE 'PIU OCCORR TROV SU COMP-NUM-OGG FLAG = P'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("PIU OCCORR TROV SU COMP-NUM-OGG FLAG = P");
                }
                //
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF WS-QTA-FLAG-K GREATER ZERO OR
                //              WS-QTA-FLAG-J GREATER ZERO
                //                 THRU EX-S1116
                //           ELSE
                //                 THRU EX-S1115
                //           END-IF
                if (ws.getWsVariabili().getWsQtaFlagK() > 0 || ws.getWsVariabili().getWsQtaFlagJ() > 0) {
                    // COB_CODE: PERFORM S1116-GEST-PROGR-NUM-OGG
                    //              THRU EX-S1116
                    s1116GestProgrNumOgg();
                }
                else {
                    // COB_CODE: PERFORM S1115-GEST-NUM-OGG
                    //              THRU EX-S1115
                    s1115GestNumOgg();
                }
            }
        }
        //
        // COB_CODE: SET IDSO0011-NOT-FOUND           TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setNotFound();
    }

    /**Original name: S110A-GEST-OUTPUT-CNO<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE DELL'OUTPUT DELLA TABELLA CON FLAG = K OPPURE J
	 * ----------------------------------------------------------------*</pre>*/
    private void s110aGestOutputCno() {
        // COB_CODE: IF CNO-FLAG-KEY-ULT-PROGR = 'K'
        //              ADD 1                      TO WS-QTA-FLAG-K
        //           END-IF.
        if (ws.getCompNumOgg().getCnoFlagKeyUltProgr() == 'K') {
            // COB_CODE: ADD 1                      TO WS-QTA-FLAG-K
            ws.getWsVariabili().setWsQtaFlagK(Trunc.toShort(1 + ws.getWsVariabili().getWsQtaFlagK(), 4));
        }
        //
        // COB_CODE: IF CNO-FLAG-KEY-ULT-PROGR = 'J'
        //              ADD 1                      TO WS-QTA-FLAG-J
        //           END-IF.
        if (ws.getCompNumOgg().getCnoFlagKeyUltProgr() == 'J') {
            // COB_CODE: ADD 1                      TO WS-QTA-FLAG-J
            ws.getWsVariabili().setWsQtaFlagJ(Trunc.toShort(1 + ws.getWsVariabili().getWsQtaFlagJ(), 4));
        }
        // COB_CODE:      IF CNO-VALORE-DEFAULT-NULL = HIGH-VALUE
        //           *-->
        //                   END-IF
        //                ELSE
        //                   END-IF
        //                END-IF.
        if (Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoValoreDefault(), CompNumOgg.Len.CNO_VALORE_DEFAULT)) {
            //-->
            // COB_CODE: IF CNO-COD-DATO-NULL NOT = HIGH-VALUE AND
            //              CNO-COD-STR-DATO-NULL = HIGH-VALUE
            //                 THRU EX-S110C
            //           ELSE
            //              END-IF
            //           END-IF
            if (!Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoCodDato(), CompNumOgg.Len.CNO_COD_DATO) && Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoCodStrDato(), CompNumOgg.Len.CNO_COD_STR_DATO)) {
                // COB_CODE: PERFORM S110C-VALORIZZA-DATO-STATICO
                //              THRU EX-S110C
                s110cValorizzaDatoStatico();
            }
            else {
                // COB_CODE: PERFORM S2000-VERIFICA-DATO        THRU EX-S2000
                s2000VerificaDato();
                //-->
                // COB_CODE: IF IDSV0001-ESITO-OK
                //              PERFORM S3000-VALORIZZA-DATO    THRU EX-S3000
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM S3000-VALORIZZA-DATO    THRU EX-S3000
                    s3000ValorizzaDato();
                }
            }
        }
        else {
            // COB_CODE: MOVE CNO-VALORE-DEFAULT             TO WS-VALORE
            ws.getWsVariabili().setWsValore(ws.getCompNumOgg().getCnoValoreDefault());
            //-->
            // COB_CODE:         IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
            //           *-->       CALCOLA LUNGHEZZA
            //                      PERFORM S4000-CALCOLA-LUNGHEZZA  THRU EX-S4000
            //                   ELSE
            //                      MOVE CNO-LUNGHEZZA-DATO      TO WS-LUNGHEZZA-VALORE
            //                   END-IF
            if (Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDatoNullFormatted())) {
                //-->       CALCOLA LUNGHEZZA
                // COB_CODE: PERFORM S4000-CALCOLA-LUNGHEZZA  THRU EX-S4000
                s4000CalcolaLunghezza();
            }
            else {
                // COB_CODE: MOVE CNO-LUNGHEZZA-DATO      TO WS-LUNGHEZZA-VALORE
                ws.getWsVariabili().setWsLunghezzaValore(TruncAbs.toInt(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDato(), 5));
            }
        }
        //--> CONCATENA IB OGGETTO
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                                        WS-LUNGHEZZA-VALORE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF CNO-FLAG-KEY-ULT-PROGR NOT = 'J'
            //              PERFORM S5000-CONCATENA-IB-OGG   THRU EX-S5000
            //           END-IF
            if (ws.getCompNumOgg().getCnoFlagKeyUltProgr() != 'J') {
                // COB_CODE: PERFORM S5000-CONCATENA-IB-OGG   THRU EX-S5000
                s5000ConcatenaIbOgg();
            }
            // COB_CODE: IF CNO-FLAG-KEY-ULT-PROGR = 'K' OR 'J'
            //              PERFORM S5001-CONCATENA-KEY      THRU EX-S5001
            //           END-IF
            if (ws.getCompNumOgg().getCnoFlagKeyUltProgr() == 'K' || ws.getCompNumOgg().getCnoFlagKeyUltProgr() == 'J') {
                // COB_CODE: PERFORM S5001-CONCATENA-KEY      THRU EX-S5001
                s5001ConcatenaKey();
            }
            // COB_CODE: COMPUTE WS-POSIZIONE-IB = WS-POSIZIONE-IB +
            //                                     WS-LUNGHEZZA-VALORE
            ws.getWsVariabili().setWsPosizioneIb(Trunc.toInt(ws.getWsVariabili().getWsPosizioneIb() + ws.getWsVariabili().getWsLunghezzaValore(), 5));
        }
        //
        // COB_CODE: IF CNO-FLAG-KEY-ULT-PROGR NOT = 'J'
        //              ADD WS-LUNGHEZZA-VALORE TO WS-LUNGHEZZA-VALORE-PR
        //           END-IF.
        if (ws.getCompNumOgg().getCnoFlagKeyUltProgr() != 'J') {
            // COB_CODE: ADD WS-LUNGHEZZA-VALORE TO WS-LUNGHEZZA-VALORE-PR
            ws.getWsVariabili().setWsLunghezzaValorePr(Trunc.toInt(ws.getWsVariabili().getWsLunghezzaValore() + ws.getWsVariabili().getWsLunghezzaValorePr(), 5));
        }
    }

    /**Original name: S110B-GEST-OUTPUT-CNO-P<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE DELL'OUTPUT DELLA TABELLA CON FLAG = P
	 * ----------------------------------------------------------------*</pre>*/
    private void s110bGestOutputCnoP() {
        // COB_CODE: ADD 1                           TO WS-QTA-FLAG-P.
        ws.getWsVariabili().setWsQtaFlagP(Trunc.toShort(1 + ws.getWsVariabili().getWsQtaFlagP(), 4));
        //
        // COB_CODE:  IF CNO-VALORE-DEFAULT-NULL = HIGH-VALUE
        //               END-IF
        //           ELSE
        //               END-IF
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoValoreDefault(), CompNumOgg.Len.CNO_VALORE_DEFAULT)) {
            // COB_CODE: IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
            //              MOVE ZERO                 TO WS-LUNGHEZZA-P
            //           ELSE
            //              MOVE CNO-LUNGHEZZA-DATO   TO WS-LUNGHEZZA-P
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDatoNullFormatted())) {
                // COB_CODE: MOVE ZERO                 TO WS-LUNGHEZZA-P
                ws.getWsVariabili().setWsLunghezzaP(0);
            }
            else {
                // COB_CODE: MOVE CNO-LUNGHEZZA-DATO   TO WS-LUNGHEZZA-P
                ws.getWsVariabili().setWsLunghezzaP(TruncAbs.toInt(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDato(), 5));
            }
        }
        else {
            // COB_CODE: MOVE CNO-VALORE-DEFAULT      TO WS-VALORE-P
            ws.getWsVariabili().setWsValoreP(ws.getCompNumOgg().getCnoValoreDefault());
            //-->
            // COB_CODE:          IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
            //           *-->        CALCOLA LUNGHEZZA
            //                       MOVE WS-LUNGHEZZA-VALORE     TO WS-LUNGHEZZA-P
            //                    ELSE
            //                       MOVE CNO-LUNGHEZZA-DATO      TO WS-LUNGHEZZA-P
            //                    END-IF
            if (Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDatoNullFormatted())) {
                //-->        CALCOLA LUNGHEZZA
                // COB_CODE: PERFORM S4000-CALCOLA-LUNGHEZZA  THRU EX-S4000
                s4000CalcolaLunghezza();
                // COB_CODE: MOVE WS-LUNGHEZZA-VALORE     TO WS-LUNGHEZZA-P
                ws.getWsVariabili().setWsLunghezzaPFormatted(ws.getWsVariabili().getWsLunghezzaValoreFormatted());
            }
            else {
                // COB_CODE: MOVE CNO-LUNGHEZZA-DATO      TO WS-LUNGHEZZA-P
                ws.getWsVariabili().setWsLunghezzaP(TruncAbs.toInt(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDato(), 5));
            }
        }
        //
        // COB_CODE: IF WPAG-IB-OGGETTO = SPACES OR HIGH-VALUE
        //              MOVE 1                           TO WS-POSIZIONE-P
        //           ELSE
        //              MOVE WS-POSIZIONE-IB             TO WS-POSIZIONE-P
        //           END-IF.
        if (Characters.EQ_SPACE.test(wpagAreaIo.getIbOggetto()) || Characters.EQ_HIGH.test(wpagAreaIo.getIbOggetto(), WpagAreaIo.Len.IB_OGGETTO)) {
            // COB_CODE: MOVE 1                           TO WS-POSIZIONE-P
            ws.getWsVariabili().setWsPosizioneP(1);
        }
        else {
            // COB_CODE: MOVE WS-POSIZIONE-IB             TO WS-POSIZIONE-P
            ws.getWsVariabili().setWsPosizionePFormatted(ws.getWsVariabili().getWsPosizioneIbFormatted());
        }
        //
        // COB_CODE: COMPUTE WS-POSIZIONE-IB = WS-POSIZIONE-IB + WS-LUNGHEZZA-P.
        ws.getWsVariabili().setWsPosizioneIb(Trunc.toInt(ws.getWsVariabili().getWsPosizioneIb() + ws.getWsVariabili().getWsLunghezzaP(), 5));
    }

    /**Original name: S110C-VALORIZZA-DATO-STATICO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA IL DATO IN MANIERA STATICA PRENDENDOLO DAL
	 *     CONTESTO E/O DALL' INTERFACCIA
	 * ----------------------------------------------------------------*
	 *  TIPO NUMERAZIONE</pre>*/
    private void s110cValorizzaDatoStatico() {
        // COB_CODE: IF CNO-COD-DATO = 'TIPO-NUMERAZIONE'
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getCompNumOgg().getCnoCodDato(), "TIPO-NUMERAZIONE")) {
            // COB_CODE: MOVE WPAG-TIPO-NUMERAZIONE TO WS-VALORE
            ws.getWsVariabili().setWsValore(String.valueOf(wpagAreaIo.getDatiInput().getTipoNumerazione().getTipoNumerazione()));
            // COB_CODE: IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
            //                 THRU EX-S4000
            //           ELSE
            //                TO WS-LUNGHEZZA-VALORE
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDatoNullFormatted())) {
                // COB_CODE: PERFORM S4000-CALCOLA-LUNGHEZZA
                //              THRU EX-S4000
                s4000CalcolaLunghezza();
            }
            else {
                // COB_CODE: MOVE CNO-LUNGHEZZA-DATO
                //             TO WS-LUNGHEZZA-VALORE
                ws.getWsVariabili().setWsLunghezzaValore(TruncAbs.toInt(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDato(), 5));
            }
        }
    }

    /**Original name: S1115-GEST-NUM-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE DELL'OUTPUT DELLA TABELLA CON LETTURA NUM-OGG
	 * ----------------------------------------------------------------*
	 * --> AGGIORNA IB OGGETTO</pre>*/
    private void s1115GestNumOgg() {
        // COB_CODE:      IF WS-VALORE-P NOT = SPACES
        //                      THRU EX-S6000
        //                ELSE
        //           *--> LETTURA DELLA TABELLA NUM-OGG PER ESTRARRE IL SEQUENCE
        //                   END-IF
        //           *
        //                END-IF.
        if (!Characters.EQ_SPACE.test(ws.getWsVariabili().getWsValoreP())) {
            // COB_CODE: MOVE WS-VALORE-P              TO WS-VALORE-APP
            ws.getWsValoreApp().setWsValoreAppFormatted(ws.getWsVariabili().getWsValorePFormatted());
            // COB_CODE: PERFORM S6000-AGGIORNA-IB-OGG
            //              THRU EX-S6000
            s6000AggiornaIbOgg();
        }
        else {
            //--> LETTURA DELLA TABELLA NUM-OGG PER ESTRARRE IL SEQUENCE
            // COB_CODE: PERFORM S115A-LEGGI-NUM-OGG   THRU EX-S115A
            s115aLeggiNumOgg();
            //
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU EX-S6000
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S6000-AGGIORNA-IB-OGG
                //              THRU EX-S6000
                s6000AggiornaIbOgg();
            }
            //
        }
    }

    /**Original name: S115A-LEGGI-NUM-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DELLA TABELLA NUM-OGG PER ESTRARRE IL SEQUENCE
	 * ----------------------------------------------------------------*</pre>*/
    private void s115aLeggiNumOgg() {
        // COB_CODE: PERFORM S115B-PREPARA-NOG          THRU EX-S115B.
        s115bPreparaNog();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                               THRU EX-S115C
            //                       WHEN IDSO0011-NOT-FOUND
            //           *--->       CHIAVE NON TROVATA SU TABELLA $
            //                               THRU EX-S0300
            //                       WHEN OTHER
            //           *-->        ERRORE DI ACCESSO AL DB
            //                               THRU EX-S9001
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO NUM-OGG
                    ws.getNumOgg().setNumOggFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //-->             GESTIONE DELL'OUTPUT DELLA TABELLA
                    // COB_CODE: PERFORM S115C-GEST-OUTPUT-NOG
                    //              THRU EX-S115C
                    s115cGestOutputNog();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->       CHIAVE NON TROVATA SU TABELLA $
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1111-LEGGI-NUM-OGG'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1111-LEGGI-NUM-OGG");
                    // COB_CODE: MOVE '005056'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005056");
                    // COB_CODE: MOVE 'NUM-OGG'
                    //             TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("NUM-OGG");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default://-->        ERRORE DI ACCESSO AL DB
                    // COB_CODE: PERFORM S9001-ERRORE-COMUNE
                    //              THRU EX-S9001
                    s9001ErroreComune();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
            s9001ErroreComune();
        }
    }

    /**Original name: S115B-PREPARA-NOG<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE DELL'OUTPUT DELLA TABELLA
	 * ----------------------------------------------------------------*</pre>*/
    private void s115bPreparaNog() {
        // COB_CODE: MOVE 'NUM-OGG'                TO WS-TABELLA.
        ws.getWsVariabili().setWsTabella("NUM-OGG");
        //--> DATA EFFETTO
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--> INIZIALIZZAZIONE FLAG ESITO
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //--> TABELLA STORICA
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'LDBS4880'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS4880");
        //--> VALORIZZA DCLGEN TABELLA
        // COB_CODE: INITIALIZE                       NUM-OGG.
        initNumOgg();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO NOG-COD-COMPAGNIA-ANIA.
        ws.getNumOgg().setNogCodCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: IF WS-SECONDA-LETTURA
        //              MOVE 'EN'                  TO NOG-FORMA-ASSICURATIVA
        //           ELSE
        //              MOVE WPOL-TP-FRM-ASSVA     TO NOG-FORMA-ASSICURATIVA
        //           END-IF
        if (ws.getWsLettura().isSecondaLettura()) {
            // COB_CODE: MOVE 'EN'                  TO NOG-FORMA-ASSICURATIVA
            ws.getNumOgg().setNogFormaAssicurativa("EN");
        }
        else {
            // COB_CODE: MOVE WPOL-TP-FRM-ASSVA     TO NOG-FORMA-ASSICURATIVA
            ws.getNumOgg().setNogFormaAssicurativa(ws.getLccvpol1().getDati().getWpolTpFrmAssva());
        }
        //
        // COB_CODE: MOVE WPAG-COD-OGGETTO         TO NOG-COD-OGGETTO.
        ws.getNumOgg().setNogCodOggetto(wpagAreaIo.getDatiInput().getCodOggetto().getCodOggetto());
        // COB_CODE: MOVE 'PO'                     TO NOG-TIPO-OGGETTO.
        ws.getNumOgg().setNogTipoOggetto("PO");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE NUM-OGG                  TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getNumOgg().getNumOggFormatted());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
    }

    /**Original name: S115C-GEST-OUTPUT-NOG<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE DELL'OUTPUT DELLA TABELLA
	 * ----------------------------------------------------------------*</pre>*/
    private void s115cGestOutputNog() {
        // COB_CODE: MOVE NOG-ULT-PROGR                   TO WS-VALORE-APP.
        ws.getWsValoreApp().setWsValoreApp(TruncAbs.toLong(ws.getNumOgg().getNogUltProgr().getNogUltProgr(), 18));
    }

    /**Original name: S1116-GEST-PROGR-NUM-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE PROGR OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1116GestProgrNumOgg() {
        // COB_CODE:      IF WS-VALORE-P NOT = SPACES
        //                      THRU EX-S6000
        //                ELSE
        //           *--> LETTURA PREVENTIVA DELLA TABELLA PROGR-NUM-OGG PER
        //           *--> CONTROLLARE IL RANGE DI PROGRESSIVI PRESENTI
        //                   END-IF
        //                END-IF.
        if (!Characters.EQ_SPACE.test(ws.getWsVariabili().getWsValoreP())) {
            // COB_CODE: MOVE WS-VALORE-P               TO WS-VALORE-APP
            ws.getWsValoreApp().setWsValoreAppFormatted(ws.getWsVariabili().getWsValorePFormatted());
            // COB_CODE: PERFORM S6000-AGGIORNA-IB-OGG
            //              THRU EX-S6000
            s6000AggiornaIbOgg();
        }
        else {
            //--> LETTURA PREVENTIVA DELLA TABELLA PROGR-NUM-OGG PER
            //--> CONTROLLARE IL RANGE DI PROGRESSIVI PRESENTI
            // COB_CODE: PERFORM S116E-LEGGI-PROGR-OGG
            //              THRU EX-S116E
            s116eLeggiProgrOgg();
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: IF WPAG-MANUALE
                //              END-IF
                //           ELSE
                //              END-IF
                //           END-IF
                if (wpagAreaIo.getDatiInput().getTipoNumerazione().isManuale()) {
                    // COB_CODE: PERFORM S1117-CONVERTI-CHAR
                    //              THRU EX-S1117
                    s1117ConvertiChar();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE IWFO0051-CAMPO-OUTPUT-DEFI  TO WS-ID-NUM
                        ws.getWsVariabili().setWsIdNum(TruncAbs.toInt(ws.getAreaCallIwfs0050().getIwfo0051().getCampoOutputDefi(), 9));
                        // COB_CODE: IF D03-PROGR-INIZIALE-NULL = HIGH-VALUE AND
                        //              D03-PROGR-FINALE-NULL = HIGH-VALUE
                        //                 THRU EX-S0300
                        //           ELSE
                        //              END-IF
                        //           END-IF
                        if (Characters.EQ_HIGH.test(ws.getProgrNumOgg().getD03ProgrIniziale().getD03ProgrInizialeNullFormatted()) && Characters.EQ_HIGH.test(ws.getProgrNumOgg().getD03ProgrFinale().getD03ProgrFinaleNullFormatted())) {
                            // COB_CODE: MOVE WK-PGM
                            //             TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE 'S1116-GEST-PROGR-NUM-OGG'
                            //             TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr("S1116-GEST-PROGR-NUM-OGG");
                            // COB_CODE: MOVE '005166'
                            //             TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005166");
                            // COB_CODE: MOVE 'RANGE DEI PROGRESSIVI NON VALORIZZATO'
                            //             TO IEAI9901-PARAMETRI-ERR
                            ws.getIeai9901Area().setParametriErr("RANGE DEI PROGRESSIVI NON VALORIZZATO");
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                        }
                        else if (ws.getWsVariabili().getWsIdNum() >= ws.getProgrNumOgg().getD03ProgrIniziale().getD03ProgrIniziale() && ws.getWsVariabili().getWsIdNum() <= ws.getProgrNumOgg().getD03ProgrFinale().getD03ProgrFinale()) {
                            // COB_CODE: IF WS-ID-NUM >= D03-PROGR-INIZIALE AND
                            //              WS-ID-NUM <= D03-PROGR-FINALE
                            //                 THRU EX-S6000
                            //           ELSE
                            //                 THRU EX-S0300
                            //           END-IF
                            // COB_CODE: MOVE WS-ID-NUM             TO WS-VALORE-APP
                            ws.getWsValoreApp().setWsValoreAppFormatted(ws.getWsVariabili().getWsIdNumFormatted());
                            // COB_CODE: PERFORM S6000-AGGIORNA-IB-OGG
                            //              THRU EX-S6000
                            s6000AggiornaIbOgg();
                        }
                        else {
                            // COB_CODE: MOVE WK-PGM
                            //             TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE 'S1116-GEST-PROGR-NUM-OGG'
                            //             TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr("S1116-GEST-PROGR-NUM-OGG");
                            // COB_CODE: MOVE '005166'
                            //             TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005166");
                            // COB_CODE: MOVE 'PROGRESSIVO NON COMPRESO NEL RANGE'
                            //             TO IEAI9901-PARAMETRI-ERR
                            ws.getIeai9901Area().setParametriErr("PROGRESSIVO NON COMPRESO NEL RANGE");
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                        }
                    }
                }
                else {
                    // COB_CODE: IF D03-PROGR-INIZIALE-NULL = HIGH-VALUE AND
                    //              D03-PROGR-FINALE-NULL = HIGH-VALUE
                    //              CONTINUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    if (Characters.EQ_HIGH.test(ws.getProgrNumOgg().getD03ProgrIniziale().getD03ProgrInizialeNullFormatted()) && Characters.EQ_HIGH.test(ws.getProgrNumOgg().getD03ProgrFinale().getD03ProgrFinaleNullFormatted())) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: ADD 1 TO D03-ULT-PROGR
                        ws.getProgrNumOgg().setD03UltProgr(Trunc.toLong(1 + ws.getProgrNumOgg().getD03UltProgr(), 18));
                        // COB_CODE: IF D03-ULT-PROGR >= D03-PROGR-INIZIALE AND
                        //              D03-ULT-PROGR <= D03-PROGR-FINALE
                        //              CONTINUE
                        //           ELSE
                        //                 THRU EX-S0300
                        //           END-IF
                        if (ws.getProgrNumOgg().getD03UltProgr() >= ws.getProgrNumOgg().getD03ProgrIniziale().getD03ProgrIniziale() && ws.getProgrNumOgg().getD03UltProgr() <= ws.getProgrNumOgg().getD03ProgrFinale().getD03ProgrFinale()) {
                        // COB_CODE: CONTINUE
                        //continue
                        }
                        else {
                            // COB_CODE: MOVE WK-PGM
                            //             TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                            // COB_CODE: MOVE 'S1116-GEST-PROGR-NUM-OGG'
                            //             TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr("S1116-GEST-PROGR-NUM-OGG");
                            // COB_CODE: MOVE '005166'
                            //             TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005166");
                            // COB_CODE: MOVE 'PROGRESSIVO NON COMPRESO NEL RANGE'
                            //             TO IEAI9901-PARAMETRI-ERR
                            ws.getIeai9901Area().setParametriErr("PROGRESSIVO NON COMPRESO NEL RANGE");
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                        }
                    }
                    // COB_CODE:               IF IDSV0001-ESITO-OK
                    //           *--> LETTURA DELLA TABELLA PROGR-NUM-OGG PER ESTRARRE IL SEQUENCE
                    //                            END-IF
                    //                         END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        //--> LETTURA DELLA TABELLA PROGR-NUM-OGG PER ESTRARRE IL SEQUENCE
                        // COB_CODE: PERFORM S116A-LEGGI-PROGR-OGG-UPD
                        //              THRU EX-S116A
                        s116aLeggiProgrOggUpd();
                        // COB_CODE: IF IDSV0001-ESITO-OK
                        //                 THRU EX-S6000
                        //           END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                            // COB_CODE: PERFORM S6000-AGGIORNA-IB-OGG
                            //              THRU EX-S6000
                            s6000AggiornaIbOgg();
                        }
                    }
                }
            }
        }
    }

    /**Original name: S1117-CONVERTI-CHAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONVERTI VALORE DA STRINGA IN NUMERICO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1117ConvertiChar() {
        Iwfs0050 iwfs0050 = null;
        // COB_CODE: MOVE HIGH-VALUE          TO AREA-CALL-IWFS0050.
        ws.getAreaCallIwfs0050().initAreaCallIwfs0050HighValues();
        // COB_CODE: ADD 1                    TO WS-LUNGHEZZA-VALORE-PR.
        ws.getWsVariabili().setWsLunghezzaValorePr(Trunc.toInt(1 + ws.getWsVariabili().getWsLunghezzaValorePr(), 5));
        // COB_CODE: MOVE WPAG-IB-OGGETTO-I(WS-LUNGHEZZA-VALORE-PR:WS-LUNGHEZZA-P)
        //             TO IWFI0051-ARRAY-STRINGA-INPUT.
        ws.getAreaCallIwfs0050().setIwfi0051ArrayStringaInputFormatted(wpagAreaIo.getDatiInput().getIbOggettoIFormatted().substring((ws.getWsVariabili().getWsLunghezzaValorePr()) - 1, ws.getWsVariabili().getWsLunghezzaValorePr() + ws.getWsVariabili().getWsLunghezzaP() - 1));
        // COB_CODE: CALL IWFS0050            USING AREA-CALL-IWFS0050.
        iwfs0050 = Iwfs0050.getInstance();
        iwfs0050.run(ws.getAreaCallIwfs0050());
        // COB_CODE: MOVE IWFO0051-ESITO      TO IDSV0001-ESITO.
        areaIdsv0001.getEsito().setEsito(ws.getAreaCallIwfs0050().getIwfo0051().getEsito().getEsito());
        // COB_CODE: MOVE IWFO0051-LOG-ERRORE TO IDSV0001-LOG-ERRORE.
        areaIdsv0001.getLogErrore().setLogErroreBytes(ws.getAreaCallIwfs0050().getIwfo0051().getLogErrore().getIwfo0051LogErroreBytes());
    }

    /**Original name: S116A-LEGGI-PROGR-OGG-UPD<br>
	 * <pre>----------------------------------------------------------------*
	 *     LEGGE PROGR OGGETTO E POI AGGIORNA L' ULTIMO PROGRESSIVO
	 * ----------------------------------------------------------------*</pre>*/
    private void s116aLeggiProgrOggUpd() {
        // COB_CODE: PERFORM S116B-PREPARA-D03          THRU EX-S116B.
        s116bPreparaD03();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                               THRU EX-S116C
            //                       WHEN IDSO0011-NOT-FOUND
            //           *--->       CHIAVE NON TROVATA SU TABELLA $
            //                               THRU EX-S116D
            //           *
            //                       WHEN OTHER
            //           *-->        ERRORE DI ACCESSO AL DB
            //                               THRU EX-S9001
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO PROGR-NUM-OGG
                    ws.getProgrNumOgg().setProgrNumOggFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    //-->             GESTIONE DELL'OUTPUT DELLA TABELLA
                    // COB_CODE: PERFORM S116C-GEST-OUTPUT-D03
                    //              THRU EX-S116C
                    s116cGestOutputD03();
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->       CHIAVE NON TROVATA SU TABELLA $
                    // COB_CODE: MOVE '1'                 TO WS-VALORE
                    ws.getWsVariabili().setWsValore("1");
                    // COB_CODE: MOVE 1                   TO D03-ULT-PROGR
                    //                                       WS-VALORE-APP
                    ws.getProgrNumOgg().setD03UltProgr(1);
                    ws.getWsValoreApp().setWsValoreApp(1);
                    // COB_CODE: PERFORM S116D-INSERT-D03
                    //              THRU EX-S116D
                    s116dInsertD03();
                    //
                    break;

                default://-->        ERRORE DI ACCESSO AL DB
                    // COB_CODE: PERFORM S9001-ERRORE-COMUNE
                    //              THRU EX-S9001
                    s9001ErroreComune();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
            s9001ErroreComune();
        }
    }

    /**Original name: S116B-PREPARA-D03<br>
	 * <pre>----------------------------------------------------------------*
	 *    PEREPARA AREA LDBS
	 * ----------------------------------------------------------------*</pre>*/
    private void s116bPreparaD03() {
        // COB_CODE: MOVE 'PROGR-NUM-OGG'          TO WS-TABELLA.
        ws.getWsVariabili().setWsTabella("PROGR-NUM-OGG");
        //--> DATA EFFETTO
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //--> INIZIALIZZAZIONE FLAG ESITO
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //--> TABELLA STORICA
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'LDBS4900'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS4900");
        //--> VALORIZZA DCLGEN TABELLA
        // COB_CODE: INITIALIZE                       COMP-NUM-OGG.
        initCompNumOgg();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO D03-COD-COMPAGNIA-ANIA.
        ws.getProgrNumOgg().setD03CodCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: IF WS-SECONDA-LETTURA
        //              MOVE 'EN'                  TO D03-FORMA-ASSICURATIVA
        //           ELSE
        //              MOVE WPOL-TP-FRM-ASSVA     TO D03-FORMA-ASSICURATIVA
        //           END-IF.
        if (ws.getWsLettura().isSecondaLettura()) {
            // COB_CODE: MOVE 'EN'                  TO D03-FORMA-ASSICURATIVA
            ws.getProgrNumOgg().setD03FormaAssicurativa("EN");
        }
        else {
            // COB_CODE: MOVE WPOL-TP-FRM-ASSVA     TO D03-FORMA-ASSICURATIVA
            ws.getProgrNumOgg().setD03FormaAssicurativa(ws.getLccvpol1().getDati().getWpolTpFrmAssva());
        }
        //
        // COB_CODE: MOVE WPAG-COD-OGGETTO         TO D03-COD-OGGETTO.
        ws.getProgrNumOgg().setD03CodOggetto(wpagAreaIo.getDatiInput().getCodOggetto().getCodOggetto());
        // COB_CODE: MOVE WS-KEY-OGGETTO           TO D03-KEY-BUSINESS.
        ws.getProgrNumOgg().setD03KeyBusiness(ws.getWsVariabili().getWsKeyOggetto());
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE PROGR-NUM-OGG            TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getProgrNumOgg().getProgrNumOggFormatted());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
    }

    /**Original name: S116C-GEST-OUTPUT-D03<br>
	 * <pre>----------------------------------------------------------------*
	 *    GESTIONE PROGR-NUM-OGG
	 * ----------------------------------------------------------------*</pre>*/
    private void s116cGestOutputD03() {
        // COB_CODE: MOVE D03-ULT-PROGR          TO WS-VALORE-APP.
        ws.getWsValoreApp().setWsValoreApp(TruncAbs.toLong(ws.getProgrNumOgg().getD03UltProgr(), 18));
    }

    /**Original name: S116D-INSERT-D03<br>
	 * <pre>----------------------------------------------------------------*
	 *    GESTIONE INSERT PROGR-NUM-OGG
	 * ----------------------------------------------------------------*</pre>*/
    private void s116dInsertD03() {
        // COB_CODE: MOVE 'PROGR-NUM-OGG'          TO WS-TABELLA.
        ws.getWsVariabili().setWsTabella("PROGR-NUM-OGG");
        //--> DATA EFFETTO
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-INSERT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
        //--> INIZIALIZZAZIONE FLAG ESITO
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //--> TABELLA STORICA
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-PRIMARY-KEY      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'PROGR-NUM-OGG'          TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("PROGR-NUM-OGG");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE PROGR-NUM-OGG            TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getProgrNumOgg().getProgrNumOggFormatted());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                       WHEN IDSO0011-NOT-FOUND
            //                          CONTINUE
            //                       WHEN OTHER
            //           *-->        ERRORE DI ACCESSO AL DB
            //                               THRU EX-S9001
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:
                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: CONTINUE
                //continue
                    break;

                default://-->        ERRORE DI ACCESSO AL DB
                    // COB_CODE: PERFORM S9001-ERRORE-COMUNE
                    //              THRU EX-S9001
                    s9001ErroreComune();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
            s9001ErroreComune();
        }
    }

    /**Original name: S116E-LEGGI-PROGR-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     LEGGURA PROGR OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s116eLeggiProgrOgg() {
        // COB_CODE: MOVE 'PROGR-NUM-OGG'          TO WS-TABELLA.
        ws.getWsVariabili().setWsTabella("PROGR-NUM-OGG");
        //--> DATA EFFETTO
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //--> INIZIALIZZAZIONE FLAG ESITO
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        //--> TABELLA STORICA
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-PRIMARY-KEY      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'PROGR-NUM-OGG'          TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("PROGR-NUM-OGG");
        //--> VALORIZZA DCLGEN TABELLA
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO D03-COD-COMPAGNIA-ANIA.
        ws.getProgrNumOgg().setD03CodCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: IF WS-SECONDA-LETTURA
        //              MOVE 'EN'                  TO D03-FORMA-ASSICURATIVA
        //           ELSE
        //              MOVE WPOL-TP-FRM-ASSVA     TO D03-FORMA-ASSICURATIVA
        //           END-IF.
        if (ws.getWsLettura().isSecondaLettura()) {
            // COB_CODE: MOVE 'EN'                  TO D03-FORMA-ASSICURATIVA
            ws.getProgrNumOgg().setD03FormaAssicurativa("EN");
        }
        else {
            // COB_CODE: MOVE WPOL-TP-FRM-ASSVA     TO D03-FORMA-ASSICURATIVA
            ws.getProgrNumOgg().setD03FormaAssicurativa(ws.getLccvpol1().getDati().getWpolTpFrmAssva());
        }
        //
        // COB_CODE: MOVE WPAG-COD-OGGETTO         TO D03-COD-OGGETTO.
        ws.getProgrNumOgg().setD03CodOggetto(wpagAreaIo.getDatiInput().getCodOggetto().getCodOggetto());
        // COB_CODE: MOVE WS-KEY-OGGETTO           TO D03-KEY-BUSINESS.
        ws.getProgrNumOgg().setD03KeyBusiness(ws.getWsVariabili().getWsKeyOggetto());
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE PROGR-NUM-OGG            TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getProgrNumOgg().getProgrNumOggFormatted());
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                   PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                              TO PROGR-NUM-OGG
            //                       WHEN IDSO0011-NOT-FOUND
            //           *--->       CHIAVE NON TROVATA SU TABELLA $
            //                            CONTINUE
            //           *
            //                       WHEN OTHER
            //           *-->        ERRORE DI ACCESSO AL DB
            //                               THRU EX-S9001
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO PROGR-NUM-OGG
                    ws.getProgrNumOgg().setProgrNumOggFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                case Idso0011SqlcodeSigned.NOT_FOUND://--->       CHIAVE NON TROVATA SU TABELLA $
                // COB_CODE: CONTINUE
                //continue
                //
                    break;

                default://-->        ERRORE DI ACCESSO AL DB
                    // COB_CODE: PERFORM S9001-ERRORE-COMUNE
                    //              THRU EX-S9001
                    s9001ErroreComune();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: PERFORM S9001-ERRORE-COMUNE        THRU EX-S9001
            s9001ErroreComune();
        }
    }

    /**Original name: S2000-VERIFICA-DATO<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALCOLO STRUTTURA TABELLA SE NON E' GIA' STATA
	 *     PRECEDENTEMENTE CARICATA
	 * ----------------------------------------------------------------*</pre>*/
    private void s2000VerificaDato() {
        // COB_CODE: SET WKS-TROVATO-TABB-NO           TO TRUE.
        ws.getWksTrovatoTabb().setNo();
        // COB_CODE: SET WK-CAMPO-FIND-NO              TO TRUE.
        ws.getWkFindCampo().setNo();
        // COB_CODE: MOVE 1                            TO IX-AP-TABB.
        ws.getIxIndici().setIxApTabb(((short)1));
        // COB_CODE: PERFORM VARYING IX-TABB FROM 1 BY 1
        //             UNTIL IX-TABB > WKS-ELE-MAX-TABB
        //               OR WKS-TROVATO-TABB-SI
        //               END-IF
        //           END-PERFORM.
        ws.getIxIndici().setIxTabb(((short)1));
        while (!(ws.getIxIndici().getIxTabb() > ws.getWksEleMaxTabb() || ws.getWksTrovatoTabb().isSi())) {
            // COB_CODE: IF CNO-COD-STR-DATO = WKS-NOME-TABB(IX-TABB)
            //              MOVE IX-TABB               TO IX-AP-TABB
            //           END-IF
            if (Conditions.eq(ws.getCompNumOgg().getCnoCodStrDato(), ws.getWksTabStrDato().getNomeTabb(ws.getIxIndici().getIxTabb()))) {
                // COB_CODE: SET WKS-TROVATO-TABB-SI    TO TRUE
                ws.getWksTrovatoTabb().setSi();
                //-->        MI SALVO L'INDICE PER CAPIRE QUAL'E' LA TABELLA DA
                //-->        UTILIZZARE PER REPERIRE IL DATO
                // COB_CODE: MOVE IX-TABB               TO IX-AP-TABB
                ws.getIxIndici().setIxApTabb(ws.getIxIndici().getIxTabb());
            }
            ws.getIxIndici().setIxTabb(Trunc.toShort(ws.getIxIndici().getIxTabb() + 1, 4));
        }
        // COB_CODE: IF WKS-TROVATO-TABB-SI
        //              END-PERFORM
        //           END-IF.
        if (ws.getWksTrovatoTabb().isSi()) {
            // COB_CODE: PERFORM VARYING IX-DATO FROM 1 BY 1
            //            UNTIL IX-DATO > WKS-ELE-MAX-DATO(IX-AP-TABB)
            //               OR WK-CAMPO-FIND-SI
            //               END-IF
            //           END-PERFORM
            ws.getIxIndici().setIxDato(((short)1));
            while (!(ws.getIxIndici().getIxDato() > ws.getWksTabStrDato().getEleMaxDato(ws.getIxIndici().getIxApTabb()) || ws.getWkFindCampo().isSi())) {
                // COB_CODE: IF WKS-CODICE-DATO(IX-AP-TABB, IX-DATO) =
                //              CNO-COD-DATO
                //              MOVE IX-DATO                 TO IX-AP-DATO
                //           END-IF
                if (Conditions.eq(ws.getWksTabStrDato().getCodiceDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxDato()), ws.getCompNumOgg().getCnoCodDato())) {
                    // COB_CODE: SET WK-CAMPO-FIND-SI         TO TRUE
                    ws.getWkFindCampo().setSi();
                    // COB_CODE: MOVE IX-DATO                 TO IX-AP-DATO
                    ws.getIxIndici().setIxApDato(ws.getIxIndici().getIxDato());
                }
                ws.getIxIndici().setIxDato(Trunc.toShort(ws.getIxIndici().getIxDato() + 1, 4));
            }
        }
        //--> SE LA STRUTTURA NON E' GIA' SALVATA IN CACHE LA RECUPERO
        //--> INVOCANDO IL SERVIZIO IDSS0020
        // COB_CODE:      IF WKS-TROVATO-TABB-NO
        //                OR WK-CAMPO-FIND-NO
        //           *-->    CHIAMATA RECUPERA STRUTTURA TABELLA IDSS0020
        //                      THRU EX-S2100
        //                END-IF.
        if (ws.getWksTrovatoTabb().isNo() || ws.getWkFindCampo().isNo()) {
            //-->    CHIAMATA RECUPERA STRUTTURA TABELLA IDSS0020
            // COB_CODE: PERFORM S2100-CALL-IDSS0020
            //              THRU EX-S2100
            s2100CallIdss0020();
        }
    }

    /**Original name: S2100-CALL-IDSS0020<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIAMATA RECUPERA STRUTTURA TABELLA IDSS0020
	 * ----------------------------------------------------------------*</pre>*/
    private void s2100CallIdss0020() {
        Idss0020 idss0020 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE SPACES                TO INPUT-IDSS0020.
        ws.initInputIdss0020Spaces();
        // COB_CODE: SET IDSI0021-INITIALIZE-SI TO TRUE.
        ws.getIdsi0021Area().getInitialize().setIdsi0021InitializeSi();
        // COB_CODE: SET IDSI0021-ON-LINE       TO TRUE.
        ws.getIdsi0021Area().getModalitaEsecutiva().setIdsi0021OnLine();
        // COB_CODE: SET IDSI0021-PTF-NEWLIFE   TO TRUE.
        ws.getIdsi0021Area().getIdentitaChiamante().setIdsi0021PtfNewlife();
        // COB_CODE: MOVE 9999                  TO IDSI0021-TIPO-MOVIMENTO.
        ws.getIdsi0021Area().setTipoMovimento(9999);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                      TO IDSI0021-CODICE-COMPAGNIA-ANIA.
        ws.getIdsi0021Area().setCodiceCompagniaAniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: MOVE CNO-COD-STR-DATO      TO IDSI0021-CODICE-STR-DATO.
        ws.getIdsi0021Area().setCodiceStrDato(ws.getCompNumOgg().getCnoCodStrDato());
        // COB_CODE: MOVE SPACES                TO AREA-CALL.
        ws.getAreaCall().initAreaCallSpaces();
        // COB_CODE: MOVE INPUT-IDSS0020        TO AREA-CALL.
        ws.getAreaCall().setAreaCallBytes(ws.getInputIdss0020Bytes());
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IDSS0020'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IDSS0020");
        // COB_CODE: MOVE 'Serv recup strutt tab. '      TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv recup strutt tab. ");
        // COB_CODE: SET  IDSV8888-INIZIO                TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setInizio();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: CALL IDSS0020              USING AREA-CALL
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            idss0020 = Idss0020.getInstance();
            idss0020.run(ws.getAreaCall());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'STRUTTURA TABELLA'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("STRUTTURA TABELLA");
            // COB_CODE: MOVE 'S2100-CALL-IDSS0020'
            //            TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S2100-CALL-IDSS0020");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
        ws.getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
        // COB_CODE: MOVE 'IDSS0020'                     TO IDSV8888-NOME-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("IDSS0020");
        // COB_CODE: MOVE 'Serv recup strutt tab. '      TO IDSV8888-DESC-PGM
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv recup strutt tab. ");
        // COB_CODE: SET  IDSV8888-FINE                  TO TRUE
        ws.getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //     MOVE SPACES                TO OUTPUT-IDSS0020.
        // COB_CODE: INITIALIZE  IDSO0021-LIMITE-STR-MAX
        //                       IDSO0021-NUM-ELEM
        //                       IDSO0021-AREA-ERRORI
        //                       IDSO0021-ELEMENTS-STR-DATO(1).
        ws.getIdso0021().setIdso0021LimiteStrMaxFormatted("00000");
        ws.getIdso0021().setIdso0021NumElemFormatted("00000");
        initIdso0021AreaErrori();
        initIdso0021ElementsStrDato();
        // COB_CODE: MOVE IDSO0021-STRUTTURA-DATO  TO IDSO0021-RESTO-TAB.
        ws.getIdso0021().getIdso0021StrutturaDato().setIdso0021RestoTab(ws.getIdso0021().getIdso0021StrutturaDato().getIdso0021StrutturaDatoFormatted());
        // COB_CODE: MOVE AREA-CALL             TO OUTPUT-IDSS0020.
        ws.setOutputIdss0020Bytes(ws.getAreaCall().getAreaCallBytes());
        // COB_CODE:      IF IDSO0021-SUCCESSFUL-RC
        //           *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
        //                      THRU EX-S2110
        //                ELSE
        //                      THRU EX-S0290
        //                END-IF.
        if (ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().isSuccessfulRc()) {
            //-->    OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE: PERFORM S2110-VALORIZZA-AREAT
            //              THRU EX-S2110
            s2110ValorizzaAreat();
        }
        else {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'CALL IDSS0020 (' CNO-COD-STR-DATO ') - '
            //           'RC: ' IDSO0021-RETURN-CODE ' - '
            //           'SQLCODE: ' IDSO0021-SQLCODE
            //           'DESC: ' IDSO0021-DESCRIZ-ERR-DB2
            //           DELIMITED BY SIZE
            //           INTO CALL-DESC
            concatUtil = ConcatUtil.buildString(Idsv0002.Len.CALL_DESC, new String[] {"CALL IDSS0020 (", ws.getCompNumOgg().getCnoCodStrDatoFormatted(), ") - ", "RC: ", ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().getIdso0021ReturnCodeFormatted(), " - ", "SQLCODE: ", ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().getIdso0021SqlcodeAsString(), "DESC: ", ws.getIdso0021().getIdso0021AreaErrori().getDescrizErrDb2Formatted()});
            ws.getIdsv0002().setCallDesc(concatUtil.replaceInString(ws.getIdsv0002().getCallDescFormatted()));
            // COB_CODE: MOVE 'S2100-CALL-IDSS0020'
            //            TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S2100-CALL-IDSS0020");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: S2110-VALORIZZA-AREAT<br>
	 * <pre>----------------------------------------------------------------*
	 *   VALORIZZAZIONI AREA TABELLA                                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void s2110ValorizzaAreat() {
        // COB_CODE: SET WK-CAMPO-FIND-NO        TO TRUE.
        ws.getWkFindCampo().setNo();
        // COB_CODE: ADD 1                       TO WKS-ELE-MAX-TABB.
        ws.setWksEleMaxTabb(Trunc.toShort(1 + ws.getWksEleMaxTabb(), 4));
        // COB_CODE: MOVE WKS-ELE-MAX-TABB       TO IX-TABB
        //                                          IX-AP-TABB.
        ws.getIxIndici().setIxTabb(ws.getWksEleMaxTabb());
        ws.getIxIndici().setIxApTabb(ws.getWksEleMaxTabb());
        // COB_CODE: MOVE 0                      TO IX-DATO
        //                                          IX-AP-DATO.
        ws.getIxIndici().setIxDato(((short)0));
        ws.getIxIndici().setIxApDato(((short)0));
        // COB_CODE: MOVE CNO-COD-STR-DATO       TO WKS-NOME-TABB(IX-AP-TABB).
        ws.getWksTabStrDato().setNomeTabb(ws.getIxIndici().getIxApTabb(), ws.getCompNumOgg().getCnoCodStrDato());
        // COB_CODE: PERFORM VARYING IX-IDSS0020 FROM 1 BY 1
        //              UNTIL IX-IDSS0020 > 1000
        //                 OR IDSO0021-CODICE-DATO(IX-IDSS0020)
        //                 = HIGH-VALUES OR SPACES
        //                   TO WKS-PRECISIONE-DATO(IX-AP-TABB, IX-DATO)
        //           END-PERFORM.
        ws.getIxIndici().setIxIdss0020(((short)1));
        while (!(ws.getIxIndici().getIxIdss0020() > 1000 || Characters.EQ_HIGH.test(ws.getIdso0021().getIdso0021StrutturaDato().getCodiceDato(ws.getIxIndici().getIxIdss0020()), Idso0021StrutturaDato.Len.CODICE_DATO) || Characters.EQ_SPACE.test(ws.getIdso0021().getIdso0021StrutturaDato().getCodiceDato(ws.getIxIndici().getIxIdss0020())))) {
            // COB_CODE: ADD 1                 TO WKS-ELE-MAX-DATO(IX-AP-TABB)
            ws.getWksTabStrDato().setEleMaxDato(ws.getIxIndici().getIxApTabb(), Trunc.toShort(1 + ws.getWksTabStrDato().getEleMaxDato(ws.getIxIndici().getIxApTabb()), 4));
            // COB_CODE: ADD 1                 TO IX-DATO
            ws.getIxIndici().setIxDato(Trunc.toShort(1 + ws.getIxIndici().getIxDato(), 4));
            // COB_CODE: MOVE IDSO0021-CODICE-DATO(IX-IDSS0020)
            //             TO WKS-CODICE-DATO(IX-AP-TABB, IX-DATO)
            ws.getWksTabStrDato().setCodiceDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxDato(), ws.getIdso0021().getIdso0021StrutturaDato().getCodiceDato(ws.getIxIndici().getIxIdss0020()));
            // COB_CODE: MOVE IDSO0021-TIPO-DATO(IX-IDSS0020)
            //             TO WKS-TIPO-DATO(IX-AP-TABB, IX-DATO)
            ws.getWksTabStrDato().setTipoDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxDato(), ws.getIdso0021().getIdso0021StrutturaDato().getTipoDato(ws.getIxIndici().getIxIdss0020()));
            // COB_CODE: MOVE IDSO0021-INIZIO-POSIZIONE(IX-IDSS0020)
            //             TO WKS-INI-POSI(IX-AP-TABB, IX-DATO)
            ws.getWksTabStrDato().setIniPosi(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxDato(), TruncAbs.toLong(ws.getIdso0021().getIdso0021StrutturaDato().getInizioPosizione(ws.getIxIndici().getIxIdss0020()), 5));
            // COB_CODE: MOVE IDSO0021-LUNGHEZZA-DATO(IX-IDSS0020)
            //             TO WKS-LUN-DATO(IX-AP-TABB, IX-DATO)
            ws.getWksTabStrDato().setLunDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxDato(), TruncAbs.toLong(ws.getIdso0021().getIdso0021StrutturaDato().getLunghezzaDato(ws.getIxIndici().getIxIdss0020()), 5));
            // COB_CODE: MOVE IDSO0021-LUNGHEZZA-DATO-NOM(IX-IDSS0020)
            //             TO WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-DATO)
            ws.getWksTabStrDato().setLunghezzaDatoNom(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxDato(), TruncAbs.toLong(ws.getIdso0021().getIdso0021StrutturaDato().getLunghezzaDatoNom(ws.getIxIndici().getIxIdss0020()), 5));
            // COB_CODE: MOVE IDSO0021-PRECISIONE-DATO(IX-IDSS0020)
            //             TO WKS-PRECISIONE-DATO(IX-AP-TABB, IX-DATO)
            ws.getWksTabStrDato().setPrecisioneDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxDato(), TruncAbs.toInt(ws.getIdso0021().getIdso0021StrutturaDato().getPrecisioneDato(ws.getIxIndici().getIxIdss0020()), 2));
            ws.getIxIndici().setIxIdss0020(Trunc.toShort(ws.getIxIndici().getIxIdss0020() + 1, 4));
        }
        // COB_CODE: PERFORM VARYING IX-DATO FROM 1 BY 1
        //             UNTIL IX-DATO > WKS-ELE-MAX-DATO(IX-AP-TABB)
        //                OR WK-CAMPO-FIND-SI
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setIxDato(((short)1));
        while (!(ws.getIxIndici().getIxDato() > ws.getWksTabStrDato().getEleMaxDato(ws.getIxIndici().getIxApTabb()) || ws.getWkFindCampo().isSi())) {
            // COB_CODE: IF WKS-CODICE-DATO(IX-AP-TABB, IX-DATO) =
            //              CNO-COD-DATO
            //              MOVE IX-DATO                 TO IX-AP-DATO
            //           END-IF
            if (Conditions.eq(ws.getWksTabStrDato().getCodiceDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxDato()), ws.getCompNumOgg().getCnoCodDato())) {
                // COB_CODE: SET WK-CAMPO-FIND-SI         TO TRUE
                ws.getWkFindCampo().setSi();
                // COB_CODE: MOVE IX-DATO                 TO IX-AP-DATO
                ws.getIxIndici().setIxApDato(ws.getIxIndici().getIxDato());
            }
            ws.getIxIndici().setIxDato(Trunc.toShort(ws.getIxIndici().getIxDato() + 1, 4));
        }
    }

    /**Original name: S3000-VALORIZZA-DATO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s3000ValorizzaDato() {
        // COB_CODE: PERFORM S3100-SWITCH-DCLGEN-AREA-APPO
        //              THRU EX-S3100.
        s3100SwitchDclgenAreaAppo();
        // COB_CODE:      IF WKS-TIPO-DATO(IX-AP-TABB, IX-AP-DATO) = 'C3'
        //           *-->    TRASCODIFICA CAMPO COMP-3
        //                   END-IF
        //           *
        //                ELSE
        //           *
        //                     TO WS-VALORE
        //                END-IF.
        if (Conditions.eq(ws.getWksTabStrDato().getTipoDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato()), "C3")) {
            //-->    TRASCODIFICA CAMPO COMP-3
            // COB_CODE: SET IDSV0141-MITT-COMP-3  TO TRUE
            ws.getAreaIdsv0141().getTipoDatoMitt().setIdsv0141MittComp3();
            // COB_CODE: MOVE WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO)
            //             TO IDSV0141-LUNGHEZZA-DATO-MITT
            ws.getAreaIdsv0141().setLunghezzaDatoMitt(((int)(ws.getWksTabStrDato().getLunghezzaDatoNom(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato()))));
            // COB_CODE: MOVE WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO)
            //             TO IDSV0141-LUNGHEZZA-DATO-STR
            ws.getAreaIdsv0141().setLunghezzaDatoStr(((int)(ws.getWksTabStrDato().getLunghezzaDatoNom(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato()))));
            // COB_CODE: MOVE WKS-PRECISIONE-DATO(IX-AP-TABB, IX-DATO)
            //             TO IDSV0141-PRECISIONE-DATO-MITT
            ws.getAreaIdsv0141().setPrecisioneDatoMitt(((short)(ws.getWksTabStrDato().getPrecisioneDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxDato()))));
            // COB_CODE: MOVE WKS-AREA-TABB-APPO(WKS-INI-POSI(IX-AP-TABB,
            //                                                IX-AP-DATO):
            //                                   WKS-LUN-DATO(IX-AP-TABB,
            //                                                IX-AP-DATO))
            //             TO IDSV0141-CAMPO-MITT
            ws.getAreaIdsv0141().setCampoMitt(ws.getWsVariabili().getWksAreaTabbAppoFormatted().substring((((int)(ws.getWksTabStrDato().getIniPosi(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato())))) - 1, (((int)(ws.getWksTabStrDato().getIniPosi(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato())))) + (((int)(ws.getWksTabStrDato().getLunDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato())))) - 1));
            // COB_CODE: PERFORM Y9999-CHIAMA-IDSS0100   THRU Y9999-EX
            y9999ChiamaIdss0100();
            //
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                TO WS-VALORE
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: MOVE IDSV0141-CAMPO-DEST     TO WS-VALORE-APP
                ws.getWsValoreApp().setWsValoreApp(TruncAbs.toLong(ws.getAreaIdsv0141().getCampoDest(), 18));
                // COB_CODE: COMPUTE WS-POSIZIONE-APP = (18 -
                //           WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO)) + 1
                ws.getWsVariabili().setWsPosizioneApp(Trunc.toInt(abs(18 - ws.getWksTabStrDato().getLunghezzaDatoNom(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato()) + 1), 5));
                // COB_CODE: MOVE WS-VALORE-STR(WS-POSIZIONE-APP:
                //                  WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO))
                //             TO WS-VALORE
                ws.getWsVariabili().setWsValore(ws.getWsValoreApp().getWsValoreStrFormatted().substring((ws.getWsVariabili().getWsPosizioneApp()) - 1, ws.getWsVariabili().getWsPosizioneApp() + (((int)(ws.getWksTabStrDato().getLunghezzaDatoNom(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato())))) - 1));
            }
            //
        }
        else {
            //
            // COB_CODE: MOVE WKS-AREA-TABB-APPO
            //               (WKS-INI-POSI(IX-AP-TABB, IX-AP-DATO):
            //                WKS-LUN-DATO(IX-AP-TABB, IX-AP-DATO))
            //             TO WS-VALORE
            ws.getWsVariabili().setWsValore(ws.getWsVariabili().getWksAreaTabbAppoFormatted().substring((((int)(ws.getWksTabStrDato().getIniPosi(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato())))) - 1, (((int)(ws.getWksTabStrDato().getIniPosi(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato())))) + (((int)(ws.getWksTabStrDato().getLunDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato())))) - 1));
        }
        // COB_CODE: IF CNO-LUNGHEZZA-DATO-NULL = HIGH-VALUE
        //                TO WS-LUNGHEZZA-VALORE
        //           ELSE
        //                TO WS-LUNGHEZZA-VALORE
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDatoNullFormatted())) {
            // COB_CODE: MOVE WKS-LUN-DATO(IX-AP-TABB, IX-AP-DATO)
            //             TO WS-LUNGHEZZA-VALORE
            ws.getWsVariabili().setWsLunghezzaValore(((int)(ws.getWksTabStrDato().getLunDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato()))));
        }
        else {
            // COB_CODE: MOVE CNO-LUNGHEZZA-DATO
            //             TO WS-LUNGHEZZA-VALORE
            ws.getWsVariabili().setWsLunghezzaValore(TruncAbs.toInt(ws.getCompNumOgg().getCnoLunghezzaDato().getCnoLunghezzaDato(), 5));
        }
    }

    /**Original name: S3100-SWITCH-DCLGEN-AREA-APPO<br>
	 * <pre>----------------------------------------------------------------*
	 *     SWITCH DCLGEN/AREA APPOGGIO
	 * ----------------------------------------------------------------*</pre>*/
    private void s3100SwitchDclgenAreaAppo() {
        // COB_CODE: MOVE SPACES                     TO WKS-AREA-TABB-APPO.
        ws.getWsVariabili().setWksAreaTabbAppo("");
        //-->     POLIZZA
        // COB_CODE:      EVALUATE CNO-COD-STR-DATO
        //           *-->     POLIZZA
        //                    WHEN 'POLI'
        //                    WHEN 'POLI-COLL'
        //                           TO WKS-AREA-TABB-APPO
        //           *-->     ADESIONE
        //                    WHEN 'ADES'
        //                    WHEN 'ADES-COLL'
        //                           TO WKS-AREA-TABB-APPO
        //           *-->     RAPPORTO RETE
        //                    WHEN 'RAPP-RETE'
        //                            THRU EX-S7000
        //                END-EVALUATE.
        switch (ws.getCompNumOgg().getCnoCodStrDato()) {

            case "POLI":
            case "POLI-COLL":// COB_CODE: MOVE WPOL-DATI
                //             TO WKS-AREA-TABB-APPO
                ws.getWsVariabili().setWksAreaTabbAppo(ws.getLccvpol1().getDati().getDatiFormatted());
                //-->     ADESIONE
                break;

            case "ADES":
            case "ADES-COLL":// COB_CODE: MOVE WADE-DATI
                //             TO WKS-AREA-TABB-APPO
                ws.getWsVariabili().setWksAreaTabbAppo(ws.getLccvade1().getDati().getDatiFormatted());
                //-->     RAPPORTO RETE
                break;

            case "RAPP-RETE":// COB_CODE: PERFORM S7000-GESTIONE-RAPP-RETE
                //              THRU EX-S7000
                s7000GestioneRappRete();
                break;

            default:break;
        }
    }

    /**Original name: S4000-CALCOLA-LUNGHEZZA<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALCOLA LUNGHEZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void s4000CalcolaLunghezza() {
        // COB_CODE: IF WS-VALORE-P NOT = SPACES
        //              MOVE LENGTH OF WS-VALORE-P      TO WS-LUNGHEZZA-CAMPO
        //           ELSE
        //              MOVE LENGTH OF WS-VALORE        TO WS-LUNGHEZZA-CAMPO
        //           END-IF.
        if (!Characters.EQ_SPACE.test(ws.getWsVariabili().getWsValoreP())) {
            // COB_CODE: MOVE LENGTH OF WS-VALORE-P      TO WS-LUNGHEZZA-CAMPO
            ws.getWsVariabili().setWsLunghezzaCampo(WsVariabiliIdss0160.Len.WS_VALORE_P);
        }
        else {
            // COB_CODE: MOVE LENGTH OF WS-VALORE        TO WS-LUNGHEZZA-CAMPO
            ws.getWsVariabili().setWsLunghezzaCampo(WsVariabiliIdss0160.Len.WS_VALORE);
        }
        // COB_CODE: SET  WS-NO-FINE-VALORE          TO TRUE.
        ws.getWsFlagFineValore().setNoFineValore();
        // COB_CODE: PERFORM VARYING WS-POSIZIONE-VAL FROM 1 BY 1
        //             UNTIL WS-POSIZIONE-VAL > WS-LUNGHEZZA-CAMPO
        //                OR WS-SI-FINE-VALORE
        //               END-IF
        //           END-PERFORM.
        ws.getWsVariabili().setWsPosizioneVal(1);
        while (!(ws.getWsVariabili().getWsPosizioneVal() > ws.getWsVariabili().getWsLunghezzaCampo() || ws.getWsFlagFineValore().isSiFineValore())) {
            // COB_CODE: IF WS-VALORE(WS-POSIZIONE-VAL:1) = SPACES OR HIGH-VALUE
            //                                              OR LOW-VALUE
            //                      WS-POSIZIONE-VAL - 1
            //           END-IF
            if (Conditions.eq(ws.getWsVariabili().getWsValoreFormatted().substring((ws.getWsVariabili().getWsPosizioneVal()) - 1, ws.getWsVariabili().getWsPosizioneVal()), "") || Conditions.eq(ws.getWsVariabili().getWsValoreFormatted().substring((ws.getWsVariabili().getWsPosizioneVal()) - 1, ws.getWsVariabili().getWsPosizioneVal()), LiteralGenerator.create(Types.HIGH_CHAR_VAL, 1)) || Conditions.eq(ws.getWsVariabili().getWsValoreFormatted().substring((ws.getWsVariabili().getWsPosizioneVal()) - 1, ws.getWsVariabili().getWsPosizioneVal()), LiteralGenerator.create(Types.LOW_CHAR_VAL, 1))) {
                // COB_CODE: SET  WS-SI-FINE-VALORE          TO TRUE
                ws.getWsFlagFineValore().setSiFineValore();
                // COB_CODE: COMPUTE WS-LUNGHEZZA-VALORE =
                //                   WS-POSIZIONE-VAL - 1
                ws.getWsVariabili().setWsLunghezzaValore(Trunc.toInt(abs(ws.getWsVariabili().getWsPosizioneVal() - 1), 5));
            }
            ws.getWsVariabili().setWsPosizioneVal(Trunc.toInt(ws.getWsVariabili().getWsPosizioneVal() + 1, 5));
        }
    }

    /**Original name: S5000-CONCATENA-IB-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONCATENA IB OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s5000ConcatenaIbOgg() {
        // COB_CODE: IF (CNO-VALORE-DEFAULT-NULL    = HIGH-VALUE)
        //           AND NOT (CNO-COD-DATO-NULL NOT = HIGH-VALUE AND
        //                    CNO-COD-STR-DATO-NULL = HIGH-VALUE)
        //           AND (WKS-TIPO-DATO(IX-AP-TABB, IX-AP-DATO) = 'C3')
        //                TO WPAG-IB-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
        //           ELSE
        //                TO WPAG-IB-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoValoreDefault(), CompNumOgg.Len.CNO_VALORE_DEFAULT) && !(!Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoCodDato(), CompNumOgg.Len.CNO_COD_DATO) && Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoCodStrDato(), CompNumOgg.Len.CNO_COD_STR_DATO)) && Conditions.eq(ws.getWksTabStrDato().getTipoDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato()), "C3")) {
            // COB_CODE: COMPUTE WS-POSIZ-INI =
            //              WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO) -
            //              WS-LUNGHEZZA-VALORE + 1
            ws.getWsVariabili().setWsPosizIni(Trunc.toInt(abs(ws.getWksTabStrDato().getLunghezzaDatoNom(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato()) - ws.getWsVariabili().getWsLunghezzaValore() + 1), 5));
            // COB_CODE: MOVE WS-VALORE(WS-POSIZ-INI:WS-LUNGHEZZA-VALORE)
            //             TO WPAG-IB-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
            wpagAreaIo.setIbOggetto(Functions.setSubstring(wpagAreaIo.getIbOggetto(), ws.getWsVariabili().getWsValoreFormatted().substring((ws.getWsVariabili().getWsPosizIni()) - 1, ws.getWsVariabili().getWsPosizIni() + ws.getWsVariabili().getWsLunghezzaValore() - 1), ws.getWsVariabili().getWsPosizioneIb(), ws.getWsVariabili().getWsLunghezzaValore()));
        }
        else {
            // COB_CODE: MOVE WS-VALORE(1:WS-LUNGHEZZA-VALORE)
            //             TO WPAG-IB-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
            wpagAreaIo.setIbOggetto(Functions.setSubstring(wpagAreaIo.getIbOggetto(), ws.getWsVariabili().getWsValoreFormatted().substring((1) - 1, ws.getWsVariabili().getWsLunghezzaValore()), ws.getWsVariabili().getWsPosizioneIb(), ws.getWsVariabili().getWsLunghezzaValore()));
        }
    }

    /**Original name: S5001-CONCATENA-KEY<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONCATENA IB OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s5001ConcatenaKey() {
        // COB_CODE: IF (CNO-VALORE-DEFAULT-NULL    = HIGH-VALUE)
        //           AND NOT (CNO-COD-DATO-NULL NOT = HIGH-VALUE AND
        //                    CNO-COD-STR-DATO-NULL = HIGH-VALUE)
        //           AND (WKS-TIPO-DATO(IX-AP-TABB, IX-AP-DATO) = 'C3')
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoValoreDefault(), CompNumOgg.Len.CNO_VALORE_DEFAULT) && !(!Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoCodDato(), CompNumOgg.Len.CNO_COD_DATO) && Characters.EQ_HIGH.test(ws.getCompNumOgg().getCnoCodStrDato(), CompNumOgg.Len.CNO_COD_STR_DATO)) && Conditions.eq(ws.getWksTabStrDato().getTipoDato(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato()), "C3")) {
            // COB_CODE: COMPUTE WS-POSIZ-INI =
            //              WKS-LUNGHEZZA-DATO-NOM(IX-AP-TABB, IX-AP-DATO) -
            //              WS-LUNGHEZZA-VALORE + 1
            ws.getWsVariabili().setWsPosizIni(Trunc.toInt(abs(ws.getWksTabStrDato().getLunghezzaDatoNom(ws.getIxIndici().getIxApTabb(), ws.getIxIndici().getIxApDato()) - ws.getWsVariabili().getWsLunghezzaValore() + 1), 5));
            // COB_CODE: IF WS-QTA-FLAG-P = 0
            //              TO WS-KEY-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
            //           ELSE
            //              TO WS-KEY-OGGETTO(WS-POSIZIONE-P:WS-LUNGHEZZA-VALORE)
            //           END-IF
            if (ws.getWsVariabili().getWsQtaFlagP() == 0) {
                // COB_CODE: MOVE WS-VALORE(WS-POSIZ-INI:WS-LUNGHEZZA-VALORE)
                //           TO WS-KEY-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
                ws.getWsVariabili().setWsKeyOggetto(Functions.setSubstring(ws.getWsVariabili().getWsKeyOggetto(), ws.getWsVariabili().getWsValoreFormatted().substring((ws.getWsVariabili().getWsPosizIni()) - 1, ws.getWsVariabili().getWsPosizIni() + ws.getWsVariabili().getWsLunghezzaValore() - 1), ws.getWsVariabili().getWsPosizioneIb(), ws.getWsVariabili().getWsLunghezzaValore()));
            }
            else {
                // COB_CODE: MOVE WS-VALORE(WS-POSIZ-INI:WS-LUNGHEZZA-VALORE)
                //           TO WS-KEY-OGGETTO(WS-POSIZIONE-P:WS-LUNGHEZZA-VALORE)
                ws.getWsVariabili().setWsKeyOggetto(Functions.setSubstring(ws.getWsVariabili().getWsKeyOggetto(), ws.getWsVariabili().getWsValoreFormatted().substring((ws.getWsVariabili().getWsPosizIni()) - 1, ws.getWsVariabili().getWsPosizIni() + ws.getWsVariabili().getWsLunghezzaValore() - 1), ws.getWsVariabili().getWsPosizioneP(), ws.getWsVariabili().getWsLunghezzaValore()));
            }
        }
        else if (ws.getWsVariabili().getWsQtaFlagP() == 0) {
            // COB_CODE: IF WS-QTA-FLAG-P = 0
            //              TO WS-KEY-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
            //           ELSE
            //              TO WS-KEY-OGGETTO(WS-POSIZIONE-P:WS-LUNGHEZZA-VALORE)
            //           END-IF
            // COB_CODE: MOVE WS-VALORE(1:WS-LUNGHEZZA-VALORE)
            //           TO WS-KEY-OGGETTO(WS-POSIZIONE-IB:WS-LUNGHEZZA-VALORE)
            ws.getWsVariabili().setWsKeyOggetto(Functions.setSubstring(ws.getWsVariabili().getWsKeyOggetto(), ws.getWsVariabili().getWsValoreFormatted().substring((1) - 1, ws.getWsVariabili().getWsLunghezzaValore()), ws.getWsVariabili().getWsPosizioneIb(), ws.getWsVariabili().getWsLunghezzaValore()));
        }
        else {
            // COB_CODE: MOVE WS-VALORE(1:WS-LUNGHEZZA-VALORE)
            //           TO WS-KEY-OGGETTO(WS-POSIZIONE-P:WS-LUNGHEZZA-VALORE)
            ws.getWsVariabili().setWsKeyOggetto(Functions.setSubstring(ws.getWsVariabili().getWsKeyOggetto(), ws.getWsVariabili().getWsValoreFormatted().substring((1) - 1, ws.getWsVariabili().getWsLunghezzaValore()), ws.getWsVariabili().getWsPosizioneP(), ws.getWsVariabili().getWsLunghezzaValore()));
        }
    }

    /**Original name: S6000-AGGIORNA-IB-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     AGGIORNA IB OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s6000AggiornaIbOgg() {
        // COB_CODE: COMPUTE WS-POSIZIONE-APP = (18 - WS-LUNGHEZZA-P) + 1.
        ws.getWsVariabili().setWsPosizioneApp(Trunc.toInt(abs(18 - ws.getWsVariabili().getWsLunghezzaP() + 1), 5));
        //
        // COB_CODE: MOVE WS-VALORE-STR(WS-POSIZIONE-APP:WS-LUNGHEZZA-P)
        //             TO WPAG-IB-OGGETTO(WS-POSIZIONE-P:WS-LUNGHEZZA-P).
        wpagAreaIo.setIbOggetto(Functions.setSubstring(wpagAreaIo.getIbOggetto(), ws.getWsValoreApp().getWsValoreStrFormatted().substring((ws.getWsVariabili().getWsPosizioneApp()) - 1, ws.getWsVariabili().getWsPosizioneApp() + ws.getWsVariabili().getWsLunghezzaP() - 1), ws.getWsVariabili().getWsPosizioneP(), ws.getWsVariabili().getWsLunghezzaP()));
    }

    /**Original name: S7000-GESTIONE-RAPP-RETE<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE RAPPORTO RETE
	 * ----------------------------------------------------------------*
	 *     SE L'AREA E' PRESENTE IN CONTESTO</pre>*/
    private void s7000GestioneRappRete() {
        // COB_CODE:      IF WRRE-ELE-RAPP-RETE-MAX > ZEROES
        //           *       PRENDO I DATI DA CONTESTO
        //                   END-PERFORM
        //           *    SE NON E' PRESENTE IN CONTESTO LA LEGGO DA DB
        //                ELSE
        //                   END-IF
        //                END-IF.
        if (ws.getWrreEleRappReteMax() > 0) {
            //       PRENDO I DATI DA CONTESTO
            // COB_CODE: PERFORM VARYING IX-TAB-RRE FROM 1 BY 1
            //                     UNTIL IX-TAB-RRE > WRRE-ELE-RAPP-RETE-MAX
            //               END-IF
            //           END-PERFORM
            ws.getIxIndici().setIxTabRre(((short)1));
            while (!(ws.getIxIndici().getIxTabRre() > ws.getWrreEleRappReteMax())) {
                // COB_CODE: IF  (NOT WRRE-ST-DEL(IX-TAB-RRE))
                //           AND WRRE-ID-OGG(IX-TAB-RRE)  = WPOL-ID-POLI
                //           AND WRRE-TP-OGG(IX-TAB-RRE)  = 'PO'
                //           AND WRRE-TP-RETE(IX-TAB-RRE) = 'GE'
                //                 TO WKS-AREA-TABB-APPO
                //           END-IF
                if (!ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getStatus().isDel() && ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreIdOgg().getWrreIdOgg() == ws.getLccvpol1().getDati().getWpolIdPoli() && Conditions.eq(ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreTpOgg(), "PO") && Conditions.eq(ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreTpRete(), "GE")) {
                    // COB_CODE: MOVE WRRE-DATI(IX-TAB-RRE)
                    //             TO WKS-AREA-TABB-APPO
                    ws.getWsVariabili().setWksAreaTabbAppo(ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getDatiFormatted());
                }
                ws.getIxIndici().setIxTabRre(Trunc.toShort(ws.getIxIndici().getIxTabRre() + 1, 4));
            }
            //    SE NON E' PRESENTE IN CONTESTO LA LEGGO DA DB
        }
        else {
            // COB_CODE: PERFORM S7100-LETTURA-RAPP-RETE
            //              THRU EX-S7100
            s7100LetturaRappRete();
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                TO WKS-AREA-TABB-APPO
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: MOVE WRRE-DATI(IX-TAB-RRE)
                //             TO WKS-AREA-TABB-APPO
                ws.getWsVariabili().setWksAreaTabbAppo(ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getDatiFormatted());
            }
        }
    }

    /**Original name: S7100-LETTURA-RAPP-RETE<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA RAPPORTO RETE
	 * ----------------------------------------------------------------*
	 *     LETTURA PER ID-OGGETTO E TIPO-OGGETTO = POLIZZA
	 *     E TIPO-RETE = GESTIONE</pre>*/
    private void s7100LetturaRappRete() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WPOL-ID-POLI             TO RRE-ID-OGG
        ws.getRappRete().getRreIdOgg().setRreIdOgg(ws.getLccvpol1().getDati().getWpolIdPoli());
        // COB_CODE: MOVE 'PO'                     TO RRE-TP-OGG
        ws.getRappRete().setRreTpOgg("PO");
        // COB_CODE: MOVE 'GE'                     TO RRE-TP-RETE
        ws.getRappRete().setRreTpRete("GE");
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA
        //            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT            TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: MOVE SPACES                    TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: MOVE RAPP-RETE                 TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRappRete().getRappReteFormatted());
        // COB_CODE: MOVE 'LDBS4240'                TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS4240");
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE:         EVALUATE TRUE
            //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                              TO WRRE-ELE-RAPP-RETE-MAX
            //           *--->       ERRORE DI ACCESSO AL DB
            //                       WHEN OTHER
            //                               THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO RAPP-RETE
                    ws.getRappRete().setRappReteFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE 1
                    //             TO IX-TAB-RRE
                    ws.getIxIndici().setIxTabRre(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-RRE
                    //              THRU VALORIZZA-OUTPUT-RRE-EX
                    valorizzaOutputRre();
                    // COB_CODE: MOVE IX-TAB-RRE
                    //             TO WRRE-ELE-RAPP-RETE-MAX
                    ws.setWrreEleRappReteMax(ws.getIxIndici().getIxTabRre());
                    //--->       ERRORE DI ACCESSO AL DB
                    break;

                default:// COB_CODE: MOVE WK-PGM        TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S7100-LETTURA-RAPP-RETE'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S7100-LETTURA-RAPP-RETE");
                    // COB_CODE: MOVE '005016'      TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING 'LDBS4240;'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS4240;", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S7100-LETTURA-RAPP-RETE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S7100-LETTURA-RAPP-RETE");
            // COB_CODE: MOVE '005016'           TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBS4240;'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //                  DELIMITED BY SIZE
            //                  INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS4240;", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: A0010-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DELLA COPY IVVC0214
	 * ----------------------------------------------------------------*</pre>*/
    private void a0010ValorizzaDclgen() {
        // COB_CODE: IF WPAG-TAB-ALIAS(IND-STR) =
        //              C161-ALIAS-ADES
        //                TO WADE-AREA-ADESIONE
        //           END-IF.
        if (Conditions.eq(wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getTabAlias(), ws.getIvvc0218().getAliasAdes())) {
            // COB_CODE: MOVE WPAG-BUFFER-DATI
            //               (WPAG-POSIZ-INI(IND-STR) :
            //                WPAG-LUNGHEZZA(IND-STR))
            //             TO WADE-AREA-ADESIONE
            ws.setWadeAreaAdesioneFormatted(wpagAreaIo.getDatiInput().getBufferDatiFormatted().substring((wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getPosizIni()) - 1, wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getPosizIni() + wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getLunghezza() - 1));
        }
        // COB_CODE: IF WPAG-TAB-ALIAS(IND-STR) =
        //              C161-ALIAS-POLI
        //                TO WPOL-AREA-POLIZZA
        //           END-IF.
        if (Conditions.eq(wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE WPAG-BUFFER-DATI
            //               (WPAG-POSIZ-INI(IND-STR) :
            //                WPAG-LUNGHEZZA(IND-STR))
            //             TO WPOL-AREA-POLIZZA
            ws.setWpolAreaPolizzaFormatted(wpagAreaIo.getDatiInput().getBufferDatiFormatted().substring((wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getPosizIni()) - 1, wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getPosizIni() + wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getLunghezza() - 1));
        }
        // COB_CODE: IF WPAG-TAB-ALIAS(IND-STR) =
        //              C161-ALIAS-RAPP-RETE
        //                TO WRRE-AREA-RAPP-RETE
        //           END-IF.
        if (Conditions.eq(wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getTabAlias(), ws.getIvvc0218().getAliasRappRete())) {
            // COB_CODE: MOVE WPAG-BUFFER-DATI
            //               (WPAG-POSIZ-INI(IND-STR) :
            //                WPAG-LUNGHEZZA(IND-STR))
            //             TO WRRE-AREA-RAPP-RETE
            ws.setWrreAreaRappReteFormatted(wpagAreaIo.getDatiInput().getBufferDatiFormatted().substring((wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getPosizIni()) - 1, wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getPosizIni() + wpagAreaIo.getDatiInput().getTabInfo(ws.getIxIndici().getIndStr()).getLunghezza() - 1));
        }
    }

    /**Original name: Y9999-CHIAMA-IDSS0100<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIAMATA MODULO PER FORMATTAZIONE CAMPI COMP
	 * ----------------------------------------------------------------*</pre>*/
    private void y9999ChiamaIdss0100() {
        Idss0140 idss0140 = null;
        // COB_CODE: CALL IDSS0140              USING AREA-IDSV0141
        //             ON EXCEPTION
        //                   THRU EX-S0300
        //           END-CALL.
        try {
            idss0140 = Idss0140.getInstance();
            idss0140.run(ws.getAreaIdsv0141());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'Y9999-CHIAMA-IDSS0100'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("Y9999-CHIAMA-IDSS0100");
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'ERRORE CALL IDSS0140'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE CALL IDSS0140");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        //
        // COB_CODE: IF IDSV0141-UNSUCCESSFUL-RC
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getAreaIdsv0141().getReturnCode().isIdsv0141UnsuccessfulRc()) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'Y9999-CHIAMA-IDSS0100'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("Y9999-CHIAMA-IDSS0100");
            // COB_CODE: MOVE '001114'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("001114");
            // COB_CODE: MOVE 'ERRORE CHIAMATA MODULO IDSS0140'
            //             TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ERRORE CHIAMATA MODULO IDSS0140");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S9001-ERRORE-COMUNE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ERRORE COMUNE
	 * ----------------------------------------------------------------*</pre>*/
    private void s9001ErroreComune() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-PGM
        //             TO IEAI9901-COD-SERVIZIO-BE.
        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
        // COB_CODE: MOVE 'S1111-LEGGI-NUM-OGG'
        //             TO IEAI9901-LABEL-ERR.
        ws.getIeai9901Area().setLabelErr("S1111-LEGGI-NUM-OGG");
        // COB_CODE: MOVE '005016'
        //             TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005016");
        //
        // COB_CODE: STRING WS-TABELLA            ';'
        //                  IDSO0011-RETURN-CODE ';'
        //                  IDSO0011-SQLCODE
        //                  DELIMITED BY SIZE
        //                  INTO IEAI9901-PARAMETRI-ERR
        //           END-STRING.
        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWsVariabili().getWsTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
        //
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VALORIZZA-OUTPUT-RRE<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY VALORIZZA OUTPUT
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVRRE3
	 *    ULTIMO AGG. 01 OTT 2014
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputRre() {
        // COB_CODE: MOVE RRE-ID-RAPP-RETE
        //             TO (SF)-ID-PTF(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().setIdPtf(ws.getRappRete().getRreIdRappRete());
        // COB_CODE: MOVE RRE-ID-RAPP-RETE
        //             TO (SF)-ID-RAPP-RETE(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreIdRappRete(ws.getRappRete().getRreIdRappRete());
        // COB_CODE: IF RRE-ID-OGG-NULL = HIGH-VALUES
        //                TO (SF)-ID-OGG-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-ID-OGG(IX-TAB-RRE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getRappRete().getRreIdOgg().getRreIdOggNullFormatted())) {
            // COB_CODE: MOVE RRE-ID-OGG-NULL
            //             TO (SF)-ID-OGG-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreIdOgg().setWrreIdOggNull(ws.getRappRete().getRreIdOgg().getRreIdOggNull());
        }
        else {
            // COB_CODE: MOVE RRE-ID-OGG
            //             TO (SF)-ID-OGG(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreIdOgg().setWrreIdOgg(ws.getRappRete().getRreIdOgg().getRreIdOgg());
        }
        // COB_CODE: MOVE RRE-TP-OGG
        //             TO (SF)-TP-OGG(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreTpOgg(ws.getRappRete().getRreTpOgg());
        // COB_CODE: MOVE RRE-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreIdMoviCrz(ws.getRappRete().getRreIdMoviCrz());
        // COB_CODE: IF RRE-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-RRE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getRappRete().getRreIdMoviChiu().getRreIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE RRE-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreIdMoviChiu().setWrreIdMoviChiuNull(ws.getRappRete().getRreIdMoviChiu().getRreIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE RRE-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreIdMoviChiu().setWrreIdMoviChiu(ws.getRappRete().getRreIdMoviChiu().getRreIdMoviChiu());
        }
        // COB_CODE: MOVE RRE-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreDtIniEff(ws.getRappRete().getRreDtIniEff());
        // COB_CODE: MOVE RRE-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreDtEndEff(ws.getRappRete().getRreDtEndEff());
        // COB_CODE: MOVE RRE-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreCodCompAnia(ws.getRappRete().getRreCodCompAnia());
        // COB_CODE: MOVE RRE-TP-RETE
        //             TO (SF)-TP-RETE(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreTpRete(ws.getRappRete().getRreTpRete());
        // COB_CODE: IF RRE-TP-ACQS-CNTRT-NULL = HIGH-VALUES
        //                TO (SF)-TP-ACQS-CNTRT-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-TP-ACQS-CNTRT(IX-TAB-RRE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getRappRete().getRreTpAcqsCntrt().getRreTpAcqsCntrtNullFormatted())) {
            // COB_CODE: MOVE RRE-TP-ACQS-CNTRT-NULL
            //             TO (SF)-TP-ACQS-CNTRT-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreTpAcqsCntrt().setWrreTpAcqsCntrtNull(ws.getRappRete().getRreTpAcqsCntrt().getRreTpAcqsCntrtNull());
        }
        else {
            // COB_CODE: MOVE RRE-TP-ACQS-CNTRT
            //             TO (SF)-TP-ACQS-CNTRT(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreTpAcqsCntrt().setWrreTpAcqsCntrt(ws.getRappRete().getRreTpAcqsCntrt().getRreTpAcqsCntrt());
        }
        // COB_CODE: IF RRE-COD-ACQS-CNTRT-NULL = HIGH-VALUES
        //                TO (SF)-COD-ACQS-CNTRT-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-COD-ACQS-CNTRT(IX-TAB-RRE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getRappRete().getRreCodAcqsCntrt().getRreCodAcqsCntrtNullFormatted())) {
            // COB_CODE: MOVE RRE-COD-ACQS-CNTRT-NULL
            //             TO (SF)-COD-ACQS-CNTRT-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodAcqsCntrt().setWrreCodAcqsCntrtNull(ws.getRappRete().getRreCodAcqsCntrt().getRreCodAcqsCntrtNull());
        }
        else {
            // COB_CODE: MOVE RRE-COD-ACQS-CNTRT
            //             TO (SF)-COD-ACQS-CNTRT(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodAcqsCntrt().setWrreCodAcqsCntrt(ws.getRappRete().getRreCodAcqsCntrt().getRreCodAcqsCntrt());
        }
        // COB_CODE: IF RRE-COD-PNT-RETE-INI-NULL = HIGH-VALUES
        //                TO (SF)-COD-PNT-RETE-INI-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-COD-PNT-RETE-INI(IX-TAB-RRE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getRappRete().getRreCodPntReteIni().getRreCodPntReteIniNullFormatted())) {
            // COB_CODE: MOVE RRE-COD-PNT-RETE-INI-NULL
            //             TO (SF)-COD-PNT-RETE-INI-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodPntReteIni().setWrreCodPntReteIniNull(ws.getRappRete().getRreCodPntReteIni().getRreCodPntReteIniNull());
        }
        else {
            // COB_CODE: MOVE RRE-COD-PNT-RETE-INI
            //             TO (SF)-COD-PNT-RETE-INI(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodPntReteIni().setWrreCodPntReteIni(ws.getRappRete().getRreCodPntReteIni().getRreCodPntReteIni());
        }
        // COB_CODE: IF RRE-COD-PNT-RETE-END-NULL = HIGH-VALUES
        //                TO (SF)-COD-PNT-RETE-END-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-COD-PNT-RETE-END(IX-TAB-RRE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getRappRete().getRreCodPntReteEnd().getRreCodPntReteEndNullFormatted())) {
            // COB_CODE: MOVE RRE-COD-PNT-RETE-END-NULL
            //             TO (SF)-COD-PNT-RETE-END-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodPntReteEnd().setWrreCodPntReteEndNull(ws.getRappRete().getRreCodPntReteEnd().getRreCodPntReteEndNull());
        }
        else {
            // COB_CODE: MOVE RRE-COD-PNT-RETE-END
            //             TO (SF)-COD-PNT-RETE-END(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodPntReteEnd().setWrreCodPntReteEnd(ws.getRappRete().getRreCodPntReteEnd().getRreCodPntReteEnd());
        }
        // COB_CODE: IF RRE-FL-PNT-RETE-1RIO-NULL = HIGH-VALUES
        //                TO (SF)-FL-PNT-RETE-1RIO-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-FL-PNT-RETE-1RIO(IX-TAB-RRE)
        //           END-IF
        if (Conditions.eq(ws.getRappRete().getRreFlPntRete1rio(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE RRE-FL-PNT-RETE-1RIO-NULL
            //             TO (SF)-FL-PNT-RETE-1RIO-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreFlPntRete1rio(ws.getRappRete().getRreFlPntRete1rio());
        }
        else {
            // COB_CODE: MOVE RRE-FL-PNT-RETE-1RIO
            //             TO (SF)-FL-PNT-RETE-1RIO(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreFlPntRete1rio(ws.getRappRete().getRreFlPntRete1rio());
        }
        // COB_CODE: IF RRE-COD-CAN-NULL = HIGH-VALUES
        //                TO (SF)-COD-CAN-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-COD-CAN(IX-TAB-RRE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getRappRete().getRreCodCan().getRreCodCanNullFormatted())) {
            // COB_CODE: MOVE RRE-COD-CAN-NULL
            //             TO (SF)-COD-CAN-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodCan().setWrreCodCanNull(ws.getRappRete().getRreCodCan().getRreCodCanNull());
        }
        else {
            // COB_CODE: MOVE RRE-COD-CAN
            //             TO (SF)-COD-CAN(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodCan().setWrreCodCan(ws.getRappRete().getRreCodCan().getRreCodCan());
        }
        // COB_CODE: MOVE RRE-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreDsRiga(ws.getRappRete().getRreDsRiga());
        // COB_CODE: MOVE RRE-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreDsOperSql(ws.getRappRete().getRreDsOperSql());
        // COB_CODE: MOVE RRE-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreDsVer(ws.getRappRete().getRreDsVer());
        // COB_CODE: MOVE RRE-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreDsTsIniCptz(ws.getRappRete().getRreDsTsIniCptz());
        // COB_CODE: MOVE RRE-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreDsTsEndCptz(ws.getRappRete().getRreDsTsEndCptz());
        // COB_CODE: MOVE RRE-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreDsUtente(ws.getRappRete().getRreDsUtente());
        // COB_CODE: MOVE RRE-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-RRE)
        ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreDsStatoElab(ws.getRappRete().getRreDsStatoElab());
        // COB_CODE: IF RRE-COD-PNT-RETE-INI-C-NULL = HIGH-VALUES
        //                TO (SF)-COD-PNT-RETE-INI-C-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-COD-PNT-RETE-INI-C(IX-TAB-RRE)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getRappRete().getRreCodPntReteIniC().getRreCodPntReteIniCNullFormatted())) {
            // COB_CODE: MOVE RRE-COD-PNT-RETE-INI-C-NULL
            //             TO (SF)-COD-PNT-RETE-INI-C-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodPntReteIniC().setWrreCodPntReteIniCNull(ws.getRappRete().getRreCodPntReteIniC().getRreCodPntReteIniCNull());
        }
        else {
            // COB_CODE: MOVE RRE-COD-PNT-RETE-INI-C
            //             TO (SF)-COD-PNT-RETE-INI-C(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().getWrreCodPntReteIniC().setWrreCodPntReteIniC(ws.getRappRete().getRreCodPntReteIniC().getRreCodPntReteIniC());
        }
        // COB_CODE: IF RRE-MATR-OPRT-NULL = HIGH-VALUES
        //                TO (SF)-MATR-OPRT-NULL(IX-TAB-RRE)
        //           ELSE
        //                TO (SF)-MATR-OPRT(IX-TAB-RRE)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getRappRete().getRreMatrOprtFormatted())) {
            // COB_CODE: MOVE RRE-MATR-OPRT-NULL
            //             TO (SF)-MATR-OPRT-NULL(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreMatrOprt(ws.getRappRete().getRreMatrOprt());
        }
        else {
            // COB_CODE: MOVE RRE-MATR-OPRT
            //             TO (SF)-MATR-OPRT(IX-TAB-RRE)
            ws.getWrreTabRappRete(ws.getIxIndici().getIxTabRre()).getLccvrre1().getDati().setWrreMatrOprt(ws.getRappRete().getRreMatrOprt());
        }
    }

    /**Original name: INIZIA-TOT-D03<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE INIZIALIZZAZIONE AREE
	 * ----------------------------------------------------------------*
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVD034
	 *    ULTIMO AGG. 16 MAR 2009
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotD03() {
        // COB_CODE: PERFORM INIZIA-ZEROES-D03 THRU INIZIA-ZEROES-D03-EX
        iniziaZeroesD03();
        // COB_CODE: PERFORM INIZIA-SPACES-D03 THRU INIZIA-SPACES-D03-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVD034:line=12, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-NULL-D03 THRU INIZIA-NULL-D03-EX.
        iniziaNullD03();
    }

    /**Original name: INIZIA-NULL-D03<br>*/
    private void iniziaNullD03() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROGR-INIZIALE-NULL
        ws.getProgrNumOgg().getD03ProgrIniziale().setD03ProgrInizialeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D03ProgrIniziale.Len.D03_PROGR_INIZIALE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROGR-FINALE-NULL.
        ws.getProgrNumOgg().getD03ProgrFinale().setD03ProgrFinaleNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, D03ProgrFinale.Len.D03_PROGR_FINALE_NULL));
    }

    /**Original name: INIZIA-ZEROES-D03<br>*/
    private void iniziaZeroesD03() {
        // COB_CODE: MOVE 0 TO (SF)-COD-COMPAGNIA-ANIA
        ws.getProgrNumOgg().setD03CodCompagniaAnia(0);
        // COB_CODE: MOVE 0 TO (SF)-ULT-PROGR.
        ws.getProgrNumOgg().setD03UltProgr(0);
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORE
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IDSV0001-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyTuningDbg() && ws.getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IDSV0001-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //                TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
            else if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() == areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IDSV0001-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initStrPerformanceDbg();
            }
        }
        else if (areaIdsv0001.getAreaComune().getLivelloDebug().isAnyApplDbg() && ws.getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IDSV0001-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IDSV0001-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIdsv8888().getLivelloDebug().getLivelloDebug() <= areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIdsv8888().getLivelloDebug().setNoDebug();
    }

    public void initWsVariabili() {
        ws.getWsVariabili().setWsTabella("");
        ws.getWsVariabili().setWsValore("");
        ws.getWsVariabili().setWsValoreAll("");
        ws.getWsVariabili().setWsValoreP("");
        ws.getWsVariabili().setWsKeyOggetto("");
        ws.getWsVariabili().setWsValoreNum(0);
        ws.getWsVariabili().setWsLunghezzaValoreFormatted("00000");
        ws.getWsVariabili().setWsLunghezzaCampoFormatted("00000");
        ws.getWsVariabili().setWsPosizioneVal(0);
        ws.getWsVariabili().setWsPosizioneIbFormatted("00000");
        ws.getWsVariabili().setWsPosizionePFormatted("00000");
        ws.getWsVariabili().setWsPosizioneAppFormatted("00000");
        ws.getWsVariabili().setWsLunghezzaPFormatted("00000");
        ws.getWsVariabili().setWsQtaFlagK(((short)0));
        ws.getWsVariabili().setWsQtaFlagP(((short)0));
        ws.getWsVariabili().setWsQtaFlagJ(((short)0));
        ws.getWsVariabili().setWksAreaTabbAppo("");
        ws.getWsVariabili().setWsIdNumFormatted("000000000");
        ws.getWsVariabili().setWsLunghezzaValorePrFormatted("00000");
        ws.getWsVariabili().setWsPosizIniFormatted("00000");
    }

    public void initIxIndici() {
        ws.getIxIndici().setIxTabb(((short)0));
        ws.getIxIndici().setIxApTabb(((short)0));
        ws.getIxIndici().setIxDato(((short)0));
        ws.getIxIndici().setIxApDato(((short)0));
        ws.getIxIndici().setIxIdss0020(((short)0));
        ws.getIxIndici().setIndStr(((short)0));
        ws.getIxIndici().setIxTabCno(((short)0));
        ws.getIxIndici().setIxTabRre(((short)0));
        ws.getIxIndici().setIxTabVal(((short)0));
        ws.getIxIndici().setIxWksTab(((short)0));
    }

    public void initProgrNumOgg() {
        ws.getProgrNumOgg().setD03CodCompagniaAnia(0);
        ws.getProgrNumOgg().setD03FormaAssicurativa("");
        ws.getProgrNumOgg().setD03CodOggetto("");
        ws.getProgrNumOgg().setD03KeyBusiness("");
        ws.getProgrNumOgg().setD03UltProgr(0);
        ws.getProgrNumOgg().getD03ProgrIniziale().setD03ProgrIniziale(0);
        ws.getProgrNumOgg().getD03ProgrFinale().setD03ProgrFinale(0);
    }

    public void initElementsStrDato() {
        ws.getWksTabStrDato().setCodiceDato(1, ws.getIxIndici().getIxWksTab(), "");
        ws.getWksTabStrDato().setTipoDato(1, ws.getIxIndici().getIxWksTab(), "");
        ws.getWksTabStrDato().setIniPosi(1, ws.getIxIndici().getIxWksTab(), 0);
        ws.getWksTabStrDato().setLunDato(1, ws.getIxIndici().getIxWksTab(), 0);
        ws.getWksTabStrDato().setLunghezzaDatoNom(1, ws.getIxIndici().getIxWksTab(), 0);
        ws.getWksTabStrDato().setPrecisioneDato(1, ws.getIxIndici().getIxWksTab(), 0);
    }

    public void initCompNumOgg() {
        ws.getCompNumOgg().setCnoCodCompagniaAnia(0);
        ws.getCompNumOgg().setCnoFormaAssicurativa("");
        ws.getCompNumOgg().setCnoCodOggetto("");
        ws.getCompNumOgg().setCnoPosizione(0);
        ws.getCompNumOgg().setCnoCodStrDato("");
        ws.getCompNumOgg().setCnoCodDato("");
        ws.getCompNumOgg().setCnoValoreDefault("");
        ws.getCompNumOgg().getCnoLunghezzaDato().setCnoLunghezzaDato(0);
        ws.getCompNumOgg().setCnoFlagKeyUltProgr(Types.SPACE_CHAR);
    }

    public void initNumOgg() {
        ws.getNumOgg().setNogCodCompagniaAnia(0);
        ws.getNumOgg().setNogFormaAssicurativa("");
        ws.getNumOgg().setNogCodOggetto("");
        ws.getNumOgg().setNogTipoOggetto("");
        ws.getNumOgg().getNogUltProgr().setNogUltProgr(0);
        ws.getNumOgg().setNogDescOggettoLen(((short)0));
        ws.getNumOgg().setNogDescOggetto("");
    }

    public void initIdso0021AreaErrori() {
        ws.getIdso0021().getIdso0021AreaErrori().getReturnCode().setReturnCode("");
        ws.getIdso0021().getIdso0021AreaErrori().getSqlcode().setSqlcodeSigned(0);
        ws.getIdso0021().getIdso0021AreaErrori().setDescrizErrDb2("");
        ws.getIdso0021().getIdso0021AreaErrori().setCodServizioBe("");
        ws.getIdso0021().getIdso0021AreaErrori().setNomeTabella("");
        ws.getIdso0021().getIdso0021AreaErrori().setKeyTabella("");
        ws.getIdso0021().getIdso0021AreaErrori().setIdso0021NumRigheLetteFormatted("00");
    }

    public void initIdso0021ElementsStrDato() {
        ws.getIdso0021().getIdso0021StrutturaDato().setCodiceDato(1, "");
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagKey(1, Types.SPACE_CHAR);
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagReturnCode(1, Types.SPACE_CHAR);
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagCallUsing(1, Types.SPACE_CHAR);
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagWhereCond(1, Types.SPACE_CHAR);
        ws.getIdso0021().getIdso0021StrutturaDato().setFlagRedefines(1, Types.SPACE_CHAR);
        ws.getIdso0021().getIdso0021StrutturaDato().setTipoDato(1, "");
        ws.getIdso0021().getIdso0021StrutturaDato().setInizioPosizione(1, 0);
        ws.getIdso0021().getIdso0021StrutturaDato().setLunghezzaDato(1, 0);
        ws.getIdso0021().getIdso0021StrutturaDato().setLunghezzaDatoNom(1, 0);
        ws.getIdso0021().getIdso0021StrutturaDato().setPrecisioneDato(1, ((short)0));
        ws.getIdso0021().getIdso0021StrutturaDato().setFormattazioneDato(1, "");
        ws.getIdso0021().getIdso0021StrutturaDato().setServConversione(1, "");
        ws.getIdso0021().getIdso0021StrutturaDato().setAreaConvStandard(1, Types.SPACE_CHAR);
        ws.getIdso0021().getIdso0021StrutturaDato().setCodiceDominio(1, "");
        ws.getIdso0021().getIdso0021StrutturaDato().setIdso0021ServizioValidazione(1, "");
        ws.getIdso0021().getIdso0021StrutturaDato().setRicorrenza(1, 0);
        ws.getIdso0021().getIdso0021StrutturaDato().setClone(1, Types.SPACE_CHAR);
        ws.getIdso0021().getIdso0021StrutturaDato().setObbligatorieta(1, Types.SPACE_CHAR);
        ws.getIdso0021().getIdso0021StrutturaDato().setValoreDefault(1, "");
    }

    public void initStrPerformanceDbg() {
        ws.getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIdsv8888().getStrPerformanceDbg().setUserName("");
    }
}
