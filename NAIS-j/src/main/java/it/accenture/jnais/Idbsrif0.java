package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.RichInvstFndDao;
import it.accenture.jnais.commons.data.to.IRichInvstFnd;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsrif0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.RifDtCambioVlt;
import it.accenture.jnais.ws.redefines.RifDtInvst;
import it.accenture.jnais.ws.redefines.RifDtInvstCalc;
import it.accenture.jnais.ws.redefines.RifIdMoviChiu;
import it.accenture.jnais.ws.redefines.RifImpGapEvent;
import it.accenture.jnais.ws.redefines.RifImpMovto;
import it.accenture.jnais.ws.redefines.RifNumQuo;
import it.accenture.jnais.ws.redefines.RifPc;
import it.accenture.jnais.ws.RichInvstFndIdbsrif0;

/**Original name: IDBSRIF0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  25 NOV 2019.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsrif0 extends Program implements IRichInvstFnd {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private RichInvstFndDao richInvstFndDao = new RichInvstFndDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsrif0Data ws = new Idbsrif0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: RICH-INVST-FND
    private RichInvstFndIdbsrif0 richInvstFnd;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSRIF0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, RichInvstFndIdbsrif0 richInvstFnd) {
        this.idsv0003 = idsv0003;
        this.richInvstFnd = richInvstFnd;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsrif0 getInstance() {
        return ((Idbsrif0)Programs.getInstance(Idbsrif0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSRIF0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSRIF0");
        // COB_CODE: MOVE 'RICH_INVST_FND' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("RICH_INVST_FND");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_INVST_FND
        //                ,ID_MOVI_FINRIO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_FND
        //                ,NUM_QUO
        //                ,PC
        //                ,IMP_MOVTO
        //                ,DT_INVST
        //                ,COD_TARI
        //                ,TP_STAT
        //                ,TP_MOD_INVST
        //                ,COD_DIV
        //                ,DT_CAMBIO_VLT
        //                ,TP_FND
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_INVST_CALC
        //                ,FL_CALC_INVTO
        //                ,IMP_GAP_EVENT
        //                ,FL_SWM_BP2S
        //             INTO
        //                :RIF-ID-RICH-INVST-FND
        //               ,:RIF-ID-MOVI-FINRIO
        //               ,:RIF-ID-MOVI-CRZ
        //               ,:RIF-ID-MOVI-CHIU
        //                :IND-RIF-ID-MOVI-CHIU
        //               ,:RIF-DT-INI-EFF-DB
        //               ,:RIF-DT-END-EFF-DB
        //               ,:RIF-COD-COMP-ANIA
        //               ,:RIF-COD-FND
        //                :IND-RIF-COD-FND
        //               ,:RIF-NUM-QUO
        //                :IND-RIF-NUM-QUO
        //               ,:RIF-PC
        //                :IND-RIF-PC
        //               ,:RIF-IMP-MOVTO
        //                :IND-RIF-IMP-MOVTO
        //               ,:RIF-DT-INVST-DB
        //                :IND-RIF-DT-INVST
        //               ,:RIF-COD-TARI
        //                :IND-RIF-COD-TARI
        //               ,:RIF-TP-STAT
        //                :IND-RIF-TP-STAT
        //               ,:RIF-TP-MOD-INVST
        //                :IND-RIF-TP-MOD-INVST
        //               ,:RIF-COD-DIV
        //               ,:RIF-DT-CAMBIO-VLT-DB
        //                :IND-RIF-DT-CAMBIO-VLT
        //               ,:RIF-TP-FND
        //               ,:RIF-DS-RIGA
        //               ,:RIF-DS-OPER-SQL
        //               ,:RIF-DS-VER
        //               ,:RIF-DS-TS-INI-CPTZ
        //               ,:RIF-DS-TS-END-CPTZ
        //               ,:RIF-DS-UTENTE
        //               ,:RIF-DS-STATO-ELAB
        //               ,:RIF-DT-INVST-CALC-DB
        //                :IND-RIF-DT-INVST-CALC
        //               ,:RIF-FL-CALC-INVTO
        //                :IND-RIF-FL-CALC-INVTO
        //               ,:RIF-IMP-GAP-EVENT
        //                :IND-RIF-IMP-GAP-EVENT
        //               ,:RIF-FL-SWM-BP2S
        //                :IND-RIF-FL-SWM-BP2S
        //             FROM RICH_INVST_FND
        //             WHERE     DS_RIGA = :RIF-DS-RIGA
        //           END-EXEC.
        richInvstFndDao.selectByRifDsRiga(richInvstFnd.getRifDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO RICH_INVST_FND
            //                  (
            //                     ID_RICH_INVST_FND
            //                    ,ID_MOVI_FINRIO
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,COD_FND
            //                    ,NUM_QUO
            //                    ,PC
            //                    ,IMP_MOVTO
            //                    ,DT_INVST
            //                    ,COD_TARI
            //                    ,TP_STAT
            //                    ,TP_MOD_INVST
            //                    ,COD_DIV
            //                    ,DT_CAMBIO_VLT
            //                    ,TP_FND
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,DT_INVST_CALC
            //                    ,FL_CALC_INVTO
            //                    ,IMP_GAP_EVENT
            //                    ,FL_SWM_BP2S
            //                  )
            //              VALUES
            //                  (
            //                    :RIF-ID-RICH-INVST-FND
            //                    ,:RIF-ID-MOVI-FINRIO
            //                    ,:RIF-ID-MOVI-CRZ
            //                    ,:RIF-ID-MOVI-CHIU
            //                     :IND-RIF-ID-MOVI-CHIU
            //                    ,:RIF-DT-INI-EFF-DB
            //                    ,:RIF-DT-END-EFF-DB
            //                    ,:RIF-COD-COMP-ANIA
            //                    ,:RIF-COD-FND
            //                     :IND-RIF-COD-FND
            //                    ,:RIF-NUM-QUO
            //                     :IND-RIF-NUM-QUO
            //                    ,:RIF-PC
            //                     :IND-RIF-PC
            //                    ,:RIF-IMP-MOVTO
            //                     :IND-RIF-IMP-MOVTO
            //                    ,:RIF-DT-INVST-DB
            //                     :IND-RIF-DT-INVST
            //                    ,:RIF-COD-TARI
            //                     :IND-RIF-COD-TARI
            //                    ,:RIF-TP-STAT
            //                     :IND-RIF-TP-STAT
            //                    ,:RIF-TP-MOD-INVST
            //                     :IND-RIF-TP-MOD-INVST
            //                    ,:RIF-COD-DIV
            //                    ,:RIF-DT-CAMBIO-VLT-DB
            //                     :IND-RIF-DT-CAMBIO-VLT
            //                    ,:RIF-TP-FND
            //                    ,:RIF-DS-RIGA
            //                    ,:RIF-DS-OPER-SQL
            //                    ,:RIF-DS-VER
            //                    ,:RIF-DS-TS-INI-CPTZ
            //                    ,:RIF-DS-TS-END-CPTZ
            //                    ,:RIF-DS-UTENTE
            //                    ,:RIF-DS-STATO-ELAB
            //                    ,:RIF-DT-INVST-CALC-DB
            //                     :IND-RIF-DT-INVST-CALC
            //                    ,:RIF-FL-CALC-INVTO
            //                     :IND-RIF-FL-CALC-INVTO
            //                    ,:RIF-IMP-GAP-EVENT
            //                     :IND-RIF-IMP-GAP-EVENT
            //                    ,:RIF-FL-SWM-BP2S
            //                     :IND-RIF-FL-SWM-BP2S
            //                  )
            //           END-EXEC
            richInvstFndDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH_INVST_FND SET
        //                   ID_RICH_INVST_FND      =
        //                :RIF-ID-RICH-INVST-FND
        //                  ,ID_MOVI_FINRIO         =
        //                :RIF-ID-MOVI-FINRIO
        //                  ,ID_MOVI_CRZ            =
        //                :RIF-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :RIF-ID-MOVI-CHIU
        //                                       :IND-RIF-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :RIF-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :RIF-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :RIF-COD-COMP-ANIA
        //                  ,COD_FND                =
        //                :RIF-COD-FND
        //                                       :IND-RIF-COD-FND
        //                  ,NUM_QUO                =
        //                :RIF-NUM-QUO
        //                                       :IND-RIF-NUM-QUO
        //                  ,PC                     =
        //                :RIF-PC
        //                                       :IND-RIF-PC
        //                  ,IMP_MOVTO              =
        //                :RIF-IMP-MOVTO
        //                                       :IND-RIF-IMP-MOVTO
        //                  ,DT_INVST               =
        //           :RIF-DT-INVST-DB
        //                                       :IND-RIF-DT-INVST
        //                  ,COD_TARI               =
        //                :RIF-COD-TARI
        //                                       :IND-RIF-COD-TARI
        //                  ,TP_STAT                =
        //                :RIF-TP-STAT
        //                                       :IND-RIF-TP-STAT
        //                  ,TP_MOD_INVST           =
        //                :RIF-TP-MOD-INVST
        //                                       :IND-RIF-TP-MOD-INVST
        //                  ,COD_DIV                =
        //                :RIF-COD-DIV
        //                  ,DT_CAMBIO_VLT          =
        //           :RIF-DT-CAMBIO-VLT-DB
        //                                       :IND-RIF-DT-CAMBIO-VLT
        //                  ,TP_FND                 =
        //                :RIF-TP-FND
        //                  ,DS_RIGA                =
        //                :RIF-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :RIF-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :RIF-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :RIF-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :RIF-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :RIF-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :RIF-DS-STATO-ELAB
        //                  ,DT_INVST_CALC          =
        //           :RIF-DT-INVST-CALC-DB
        //                                       :IND-RIF-DT-INVST-CALC
        //                  ,FL_CALC_INVTO          =
        //                :RIF-FL-CALC-INVTO
        //                                       :IND-RIF-FL-CALC-INVTO
        //                  ,IMP_GAP_EVENT          =
        //                :RIF-IMP-GAP-EVENT
        //                                       :IND-RIF-IMP-GAP-EVENT
        //                  ,FL_SWM_BP2S            =
        //                :RIF-FL-SWM-BP2S
        //                                       :IND-RIF-FL-SWM-BP2S
        //                WHERE     DS_RIGA = :RIF-DS-RIGA
        //           END-EXEC.
        richInvstFndDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM RICH_INVST_FND
        //                WHERE     DS_RIGA = :RIF-DS-RIGA
        //           END-EXEC.
        richInvstFndDao.deleteByRifDsRiga(richInvstFnd.getRifDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-RIF CURSOR FOR
        //              SELECT
        //                     ID_RICH_INVST_FND
        //                    ,ID_MOVI_FINRIO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_FND
        //                    ,NUM_QUO
        //                    ,PC
        //                    ,IMP_MOVTO
        //                    ,DT_INVST
        //                    ,COD_TARI
        //                    ,TP_STAT
        //                    ,TP_MOD_INVST
        //                    ,COD_DIV
        //                    ,DT_CAMBIO_VLT
        //                    ,TP_FND
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,DT_INVST_CALC
        //                    ,FL_CALC_INVTO
        //                    ,IMP_GAP_EVENT
        //                    ,FL_SWM_BP2S
        //              FROM RICH_INVST_FND
        //              WHERE     ID_RICH_INVST_FND = :RIF-ID-RICH-INVST-FND
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_INVST_FND
        //                ,ID_MOVI_FINRIO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_FND
        //                ,NUM_QUO
        //                ,PC
        //                ,IMP_MOVTO
        //                ,DT_INVST
        //                ,COD_TARI
        //                ,TP_STAT
        //                ,TP_MOD_INVST
        //                ,COD_DIV
        //                ,DT_CAMBIO_VLT
        //                ,TP_FND
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_INVST_CALC
        //                ,FL_CALC_INVTO
        //                ,IMP_GAP_EVENT
        //                ,FL_SWM_BP2S
        //             INTO
        //                :RIF-ID-RICH-INVST-FND
        //               ,:RIF-ID-MOVI-FINRIO
        //               ,:RIF-ID-MOVI-CRZ
        //               ,:RIF-ID-MOVI-CHIU
        //                :IND-RIF-ID-MOVI-CHIU
        //               ,:RIF-DT-INI-EFF-DB
        //               ,:RIF-DT-END-EFF-DB
        //               ,:RIF-COD-COMP-ANIA
        //               ,:RIF-COD-FND
        //                :IND-RIF-COD-FND
        //               ,:RIF-NUM-QUO
        //                :IND-RIF-NUM-QUO
        //               ,:RIF-PC
        //                :IND-RIF-PC
        //               ,:RIF-IMP-MOVTO
        //                :IND-RIF-IMP-MOVTO
        //               ,:RIF-DT-INVST-DB
        //                :IND-RIF-DT-INVST
        //               ,:RIF-COD-TARI
        //                :IND-RIF-COD-TARI
        //               ,:RIF-TP-STAT
        //                :IND-RIF-TP-STAT
        //               ,:RIF-TP-MOD-INVST
        //                :IND-RIF-TP-MOD-INVST
        //               ,:RIF-COD-DIV
        //               ,:RIF-DT-CAMBIO-VLT-DB
        //                :IND-RIF-DT-CAMBIO-VLT
        //               ,:RIF-TP-FND
        //               ,:RIF-DS-RIGA
        //               ,:RIF-DS-OPER-SQL
        //               ,:RIF-DS-VER
        //               ,:RIF-DS-TS-INI-CPTZ
        //               ,:RIF-DS-TS-END-CPTZ
        //               ,:RIF-DS-UTENTE
        //               ,:RIF-DS-STATO-ELAB
        //               ,:RIF-DT-INVST-CALC-DB
        //                :IND-RIF-DT-INVST-CALC
        //               ,:RIF-FL-CALC-INVTO
        //                :IND-RIF-FL-CALC-INVTO
        //               ,:RIF-IMP-GAP-EVENT
        //                :IND-RIF-IMP-GAP-EVENT
        //               ,:RIF-FL-SWM-BP2S
        //                :IND-RIF-FL-SWM-BP2S
        //             FROM RICH_INVST_FND
        //             WHERE     ID_RICH_INVST_FND = :RIF-ID-RICH-INVST-FND
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        richInvstFndDao.selectRec(richInvstFnd.getRifIdRichInvstFnd(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE RICH_INVST_FND SET
        //                   ID_RICH_INVST_FND      =
        //                :RIF-ID-RICH-INVST-FND
        //                  ,ID_MOVI_FINRIO         =
        //                :RIF-ID-MOVI-FINRIO
        //                  ,ID_MOVI_CRZ            =
        //                :RIF-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :RIF-ID-MOVI-CHIU
        //                                       :IND-RIF-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :RIF-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :RIF-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :RIF-COD-COMP-ANIA
        //                  ,COD_FND                =
        //                :RIF-COD-FND
        //                                       :IND-RIF-COD-FND
        //                  ,NUM_QUO                =
        //                :RIF-NUM-QUO
        //                                       :IND-RIF-NUM-QUO
        //                  ,PC                     =
        //                :RIF-PC
        //                                       :IND-RIF-PC
        //                  ,IMP_MOVTO              =
        //                :RIF-IMP-MOVTO
        //                                       :IND-RIF-IMP-MOVTO
        //                  ,DT_INVST               =
        //           :RIF-DT-INVST-DB
        //                                       :IND-RIF-DT-INVST
        //                  ,COD_TARI               =
        //                :RIF-COD-TARI
        //                                       :IND-RIF-COD-TARI
        //                  ,TP_STAT                =
        //                :RIF-TP-STAT
        //                                       :IND-RIF-TP-STAT
        //                  ,TP_MOD_INVST           =
        //                :RIF-TP-MOD-INVST
        //                                       :IND-RIF-TP-MOD-INVST
        //                  ,COD_DIV                =
        //                :RIF-COD-DIV
        //                  ,DT_CAMBIO_VLT          =
        //           :RIF-DT-CAMBIO-VLT-DB
        //                                       :IND-RIF-DT-CAMBIO-VLT
        //                  ,TP_FND                 =
        //                :RIF-TP-FND
        //                  ,DS_RIGA                =
        //                :RIF-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :RIF-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :RIF-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :RIF-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :RIF-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :RIF-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :RIF-DS-STATO-ELAB
        //                  ,DT_INVST_CALC          =
        //           :RIF-DT-INVST-CALC-DB
        //                                       :IND-RIF-DT-INVST-CALC
        //                  ,FL_CALC_INVTO          =
        //                :RIF-FL-CALC-INVTO
        //                                       :IND-RIF-FL-CALC-INVTO
        //                  ,IMP_GAP_EVENT          =
        //                :RIF-IMP-GAP-EVENT
        //                                       :IND-RIF-IMP-GAP-EVENT
        //                  ,FL_SWM_BP2S            =
        //                :RIF-FL-SWM-BP2S
        //                                       :IND-RIF-FL-SWM-BP2S
        //                WHERE     DS_RIGA = :RIF-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        richInvstFndDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-RIF
        //           END-EXEC.
        richInvstFndDao.openCIdUpdEffRif(richInvstFnd.getRifIdRichInvstFnd(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-RIF
        //           END-EXEC.
        richInvstFndDao.closeCIdUpdEffRif();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-RIF
        //           INTO
        //                :RIF-ID-RICH-INVST-FND
        //               ,:RIF-ID-MOVI-FINRIO
        //               ,:RIF-ID-MOVI-CRZ
        //               ,:RIF-ID-MOVI-CHIU
        //                :IND-RIF-ID-MOVI-CHIU
        //               ,:RIF-DT-INI-EFF-DB
        //               ,:RIF-DT-END-EFF-DB
        //               ,:RIF-COD-COMP-ANIA
        //               ,:RIF-COD-FND
        //                :IND-RIF-COD-FND
        //               ,:RIF-NUM-QUO
        //                :IND-RIF-NUM-QUO
        //               ,:RIF-PC
        //                :IND-RIF-PC
        //               ,:RIF-IMP-MOVTO
        //                :IND-RIF-IMP-MOVTO
        //               ,:RIF-DT-INVST-DB
        //                :IND-RIF-DT-INVST
        //               ,:RIF-COD-TARI
        //                :IND-RIF-COD-TARI
        //               ,:RIF-TP-STAT
        //                :IND-RIF-TP-STAT
        //               ,:RIF-TP-MOD-INVST
        //                :IND-RIF-TP-MOD-INVST
        //               ,:RIF-COD-DIV
        //               ,:RIF-DT-CAMBIO-VLT-DB
        //                :IND-RIF-DT-CAMBIO-VLT
        //               ,:RIF-TP-FND
        //               ,:RIF-DS-RIGA
        //               ,:RIF-DS-OPER-SQL
        //               ,:RIF-DS-VER
        //               ,:RIF-DS-TS-INI-CPTZ
        //               ,:RIF-DS-TS-END-CPTZ
        //               ,:RIF-DS-UTENTE
        //               ,:RIF-DS-STATO-ELAB
        //               ,:RIF-DT-INVST-CALC-DB
        //                :IND-RIF-DT-INVST-CALC
        //               ,:RIF-FL-CALC-INVTO
        //                :IND-RIF-FL-CALC-INVTO
        //               ,:RIF-IMP-GAP-EVENT
        //                :IND-RIF-IMP-GAP-EVENT
        //               ,:RIF-FL-SWM-BP2S
        //                :IND-RIF-FL-SWM-BP2S
        //           END-EXEC.
        richInvstFndDao.fetchCIdUpdEffRif(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-RIF CURSOR FOR
        //              SELECT
        //                     ID_RICH_INVST_FND
        //                    ,ID_MOVI_FINRIO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_FND
        //                    ,NUM_QUO
        //                    ,PC
        //                    ,IMP_MOVTO
        //                    ,DT_INVST
        //                    ,COD_TARI
        //                    ,TP_STAT
        //                    ,TP_MOD_INVST
        //                    ,COD_DIV
        //                    ,DT_CAMBIO_VLT
        //                    ,TP_FND
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,DT_INVST_CALC
        //                    ,FL_CALC_INVTO
        //                    ,IMP_GAP_EVENT
        //                    ,FL_SWM_BP2S
        //              FROM RICH_INVST_FND
        //              WHERE     ID_MOVI_FINRIO = :RIF-ID-MOVI-FINRIO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_RICH_INVST_FND ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_INVST_FND
        //                ,ID_MOVI_FINRIO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_FND
        //                ,NUM_QUO
        //                ,PC
        //                ,IMP_MOVTO
        //                ,DT_INVST
        //                ,COD_TARI
        //                ,TP_STAT
        //                ,TP_MOD_INVST
        //                ,COD_DIV
        //                ,DT_CAMBIO_VLT
        //                ,TP_FND
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_INVST_CALC
        //                ,FL_CALC_INVTO
        //                ,IMP_GAP_EVENT
        //                ,FL_SWM_BP2S
        //             INTO
        //                :RIF-ID-RICH-INVST-FND
        //               ,:RIF-ID-MOVI-FINRIO
        //               ,:RIF-ID-MOVI-CRZ
        //               ,:RIF-ID-MOVI-CHIU
        //                :IND-RIF-ID-MOVI-CHIU
        //               ,:RIF-DT-INI-EFF-DB
        //               ,:RIF-DT-END-EFF-DB
        //               ,:RIF-COD-COMP-ANIA
        //               ,:RIF-COD-FND
        //                :IND-RIF-COD-FND
        //               ,:RIF-NUM-QUO
        //                :IND-RIF-NUM-QUO
        //               ,:RIF-PC
        //                :IND-RIF-PC
        //               ,:RIF-IMP-MOVTO
        //                :IND-RIF-IMP-MOVTO
        //               ,:RIF-DT-INVST-DB
        //                :IND-RIF-DT-INVST
        //               ,:RIF-COD-TARI
        //                :IND-RIF-COD-TARI
        //               ,:RIF-TP-STAT
        //                :IND-RIF-TP-STAT
        //               ,:RIF-TP-MOD-INVST
        //                :IND-RIF-TP-MOD-INVST
        //               ,:RIF-COD-DIV
        //               ,:RIF-DT-CAMBIO-VLT-DB
        //                :IND-RIF-DT-CAMBIO-VLT
        //               ,:RIF-TP-FND
        //               ,:RIF-DS-RIGA
        //               ,:RIF-DS-OPER-SQL
        //               ,:RIF-DS-VER
        //               ,:RIF-DS-TS-INI-CPTZ
        //               ,:RIF-DS-TS-END-CPTZ
        //               ,:RIF-DS-UTENTE
        //               ,:RIF-DS-STATO-ELAB
        //               ,:RIF-DT-INVST-CALC-DB
        //                :IND-RIF-DT-INVST-CALC
        //               ,:RIF-FL-CALC-INVTO
        //                :IND-RIF-FL-CALC-INVTO
        //               ,:RIF-IMP-GAP-EVENT
        //                :IND-RIF-IMP-GAP-EVENT
        //               ,:RIF-FL-SWM-BP2S
        //                :IND-RIF-FL-SWM-BP2S
        //             FROM RICH_INVST_FND
        //             WHERE     ID_MOVI_FINRIO = :RIF-ID-MOVI-FINRIO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        richInvstFndDao.selectRec1(richInvstFnd.getRifIdMoviFinrio(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-RIF
        //           END-EXEC.
        richInvstFndDao.openCIdpEffRif(richInvstFnd.getRifIdMoviFinrio(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-RIF
        //           END-EXEC.
        richInvstFndDao.closeCIdpEffRif();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-RIF
        //           INTO
        //                :RIF-ID-RICH-INVST-FND
        //               ,:RIF-ID-MOVI-FINRIO
        //               ,:RIF-ID-MOVI-CRZ
        //               ,:RIF-ID-MOVI-CHIU
        //                :IND-RIF-ID-MOVI-CHIU
        //               ,:RIF-DT-INI-EFF-DB
        //               ,:RIF-DT-END-EFF-DB
        //               ,:RIF-COD-COMP-ANIA
        //               ,:RIF-COD-FND
        //                :IND-RIF-COD-FND
        //               ,:RIF-NUM-QUO
        //                :IND-RIF-NUM-QUO
        //               ,:RIF-PC
        //                :IND-RIF-PC
        //               ,:RIF-IMP-MOVTO
        //                :IND-RIF-IMP-MOVTO
        //               ,:RIF-DT-INVST-DB
        //                :IND-RIF-DT-INVST
        //               ,:RIF-COD-TARI
        //                :IND-RIF-COD-TARI
        //               ,:RIF-TP-STAT
        //                :IND-RIF-TP-STAT
        //               ,:RIF-TP-MOD-INVST
        //                :IND-RIF-TP-MOD-INVST
        //               ,:RIF-COD-DIV
        //               ,:RIF-DT-CAMBIO-VLT-DB
        //                :IND-RIF-DT-CAMBIO-VLT
        //               ,:RIF-TP-FND
        //               ,:RIF-DS-RIGA
        //               ,:RIF-DS-OPER-SQL
        //               ,:RIF-DS-VER
        //               ,:RIF-DS-TS-INI-CPTZ
        //               ,:RIF-DS-TS-END-CPTZ
        //               ,:RIF-DS-UTENTE
        //               ,:RIF-DS-STATO-ELAB
        //               ,:RIF-DT-INVST-CALC-DB
        //                :IND-RIF-DT-INVST-CALC
        //               ,:RIF-FL-CALC-INVTO
        //                :IND-RIF-FL-CALC-INVTO
        //               ,:RIF-IMP-GAP-EVENT
        //                :IND-RIF-IMP-GAP-EVENT
        //               ,:RIF-FL-SWM-BP2S
        //                :IND-RIF-FL-SWM-BP2S
        //           END-EXEC.
        richInvstFndDao.fetchCIdpEffRif(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_INVST_FND
        //                ,ID_MOVI_FINRIO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_FND
        //                ,NUM_QUO
        //                ,PC
        //                ,IMP_MOVTO
        //                ,DT_INVST
        //                ,COD_TARI
        //                ,TP_STAT
        //                ,TP_MOD_INVST
        //                ,COD_DIV
        //                ,DT_CAMBIO_VLT
        //                ,TP_FND
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_INVST_CALC
        //                ,FL_CALC_INVTO
        //                ,IMP_GAP_EVENT
        //                ,FL_SWM_BP2S
        //             INTO
        //                :RIF-ID-RICH-INVST-FND
        //               ,:RIF-ID-MOVI-FINRIO
        //               ,:RIF-ID-MOVI-CRZ
        //               ,:RIF-ID-MOVI-CHIU
        //                :IND-RIF-ID-MOVI-CHIU
        //               ,:RIF-DT-INI-EFF-DB
        //               ,:RIF-DT-END-EFF-DB
        //               ,:RIF-COD-COMP-ANIA
        //               ,:RIF-COD-FND
        //                :IND-RIF-COD-FND
        //               ,:RIF-NUM-QUO
        //                :IND-RIF-NUM-QUO
        //               ,:RIF-PC
        //                :IND-RIF-PC
        //               ,:RIF-IMP-MOVTO
        //                :IND-RIF-IMP-MOVTO
        //               ,:RIF-DT-INVST-DB
        //                :IND-RIF-DT-INVST
        //               ,:RIF-COD-TARI
        //                :IND-RIF-COD-TARI
        //               ,:RIF-TP-STAT
        //                :IND-RIF-TP-STAT
        //               ,:RIF-TP-MOD-INVST
        //                :IND-RIF-TP-MOD-INVST
        //               ,:RIF-COD-DIV
        //               ,:RIF-DT-CAMBIO-VLT-DB
        //                :IND-RIF-DT-CAMBIO-VLT
        //               ,:RIF-TP-FND
        //               ,:RIF-DS-RIGA
        //               ,:RIF-DS-OPER-SQL
        //               ,:RIF-DS-VER
        //               ,:RIF-DS-TS-INI-CPTZ
        //               ,:RIF-DS-TS-END-CPTZ
        //               ,:RIF-DS-UTENTE
        //               ,:RIF-DS-STATO-ELAB
        //               ,:RIF-DT-INVST-CALC-DB
        //                :IND-RIF-DT-INVST-CALC
        //               ,:RIF-FL-CALC-INVTO
        //                :IND-RIF-FL-CALC-INVTO
        //               ,:RIF-IMP-GAP-EVENT
        //                :IND-RIF-IMP-GAP-EVENT
        //               ,:RIF-FL-SWM-BP2S
        //                :IND-RIF-FL-SWM-BP2S
        //             FROM RICH_INVST_FND
        //             WHERE     ID_RICH_INVST_FND = :RIF-ID-RICH-INVST-FND
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        richInvstFndDao.selectRec2(richInvstFnd.getRifIdRichInvstFnd(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-RIF CURSOR FOR
        //              SELECT
        //                     ID_RICH_INVST_FND
        //                    ,ID_MOVI_FINRIO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_FND
        //                    ,NUM_QUO
        //                    ,PC
        //                    ,IMP_MOVTO
        //                    ,DT_INVST
        //                    ,COD_TARI
        //                    ,TP_STAT
        //                    ,TP_MOD_INVST
        //                    ,COD_DIV
        //                    ,DT_CAMBIO_VLT
        //                    ,TP_FND
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,DT_INVST_CALC
        //                    ,FL_CALC_INVTO
        //                    ,IMP_GAP_EVENT
        //                    ,FL_SWM_BP2S
        //              FROM RICH_INVST_FND
        //              WHERE     ID_MOVI_FINRIO = :RIF-ID-MOVI-FINRIO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_RICH_INVST_FND ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RICH_INVST_FND
        //                ,ID_MOVI_FINRIO
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_FND
        //                ,NUM_QUO
        //                ,PC
        //                ,IMP_MOVTO
        //                ,DT_INVST
        //                ,COD_TARI
        //                ,TP_STAT
        //                ,TP_MOD_INVST
        //                ,COD_DIV
        //                ,DT_CAMBIO_VLT
        //                ,TP_FND
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_INVST_CALC
        //                ,FL_CALC_INVTO
        //                ,IMP_GAP_EVENT
        //                ,FL_SWM_BP2S
        //             INTO
        //                :RIF-ID-RICH-INVST-FND
        //               ,:RIF-ID-MOVI-FINRIO
        //               ,:RIF-ID-MOVI-CRZ
        //               ,:RIF-ID-MOVI-CHIU
        //                :IND-RIF-ID-MOVI-CHIU
        //               ,:RIF-DT-INI-EFF-DB
        //               ,:RIF-DT-END-EFF-DB
        //               ,:RIF-COD-COMP-ANIA
        //               ,:RIF-COD-FND
        //                :IND-RIF-COD-FND
        //               ,:RIF-NUM-QUO
        //                :IND-RIF-NUM-QUO
        //               ,:RIF-PC
        //                :IND-RIF-PC
        //               ,:RIF-IMP-MOVTO
        //                :IND-RIF-IMP-MOVTO
        //               ,:RIF-DT-INVST-DB
        //                :IND-RIF-DT-INVST
        //               ,:RIF-COD-TARI
        //                :IND-RIF-COD-TARI
        //               ,:RIF-TP-STAT
        //                :IND-RIF-TP-STAT
        //               ,:RIF-TP-MOD-INVST
        //                :IND-RIF-TP-MOD-INVST
        //               ,:RIF-COD-DIV
        //               ,:RIF-DT-CAMBIO-VLT-DB
        //                :IND-RIF-DT-CAMBIO-VLT
        //               ,:RIF-TP-FND
        //               ,:RIF-DS-RIGA
        //               ,:RIF-DS-OPER-SQL
        //               ,:RIF-DS-VER
        //               ,:RIF-DS-TS-INI-CPTZ
        //               ,:RIF-DS-TS-END-CPTZ
        //               ,:RIF-DS-UTENTE
        //               ,:RIF-DS-STATO-ELAB
        //               ,:RIF-DT-INVST-CALC-DB
        //                :IND-RIF-DT-INVST-CALC
        //               ,:RIF-FL-CALC-INVTO
        //                :IND-RIF-FL-CALC-INVTO
        //               ,:RIF-IMP-GAP-EVENT
        //                :IND-RIF-IMP-GAP-EVENT
        //               ,:RIF-FL-SWM-BP2S
        //                :IND-RIF-FL-SWM-BP2S
        //             FROM RICH_INVST_FND
        //             WHERE     ID_MOVI_FINRIO = :RIF-ID-MOVI-FINRIO
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        richInvstFndDao.selectRec3(richInvstFnd.getRifIdMoviFinrio(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-RIF
        //           END-EXEC.
        richInvstFndDao.openCIdpCpzRif(richInvstFnd.getRifIdMoviFinrio(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-RIF
        //           END-EXEC.
        richInvstFndDao.closeCIdpCpzRif();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-RIF
        //           INTO
        //                :RIF-ID-RICH-INVST-FND
        //               ,:RIF-ID-MOVI-FINRIO
        //               ,:RIF-ID-MOVI-CRZ
        //               ,:RIF-ID-MOVI-CHIU
        //                :IND-RIF-ID-MOVI-CHIU
        //               ,:RIF-DT-INI-EFF-DB
        //               ,:RIF-DT-END-EFF-DB
        //               ,:RIF-COD-COMP-ANIA
        //               ,:RIF-COD-FND
        //                :IND-RIF-COD-FND
        //               ,:RIF-NUM-QUO
        //                :IND-RIF-NUM-QUO
        //               ,:RIF-PC
        //                :IND-RIF-PC
        //               ,:RIF-IMP-MOVTO
        //                :IND-RIF-IMP-MOVTO
        //               ,:RIF-DT-INVST-DB
        //                :IND-RIF-DT-INVST
        //               ,:RIF-COD-TARI
        //                :IND-RIF-COD-TARI
        //               ,:RIF-TP-STAT
        //                :IND-RIF-TP-STAT
        //               ,:RIF-TP-MOD-INVST
        //                :IND-RIF-TP-MOD-INVST
        //               ,:RIF-COD-DIV
        //               ,:RIF-DT-CAMBIO-VLT-DB
        //                :IND-RIF-DT-CAMBIO-VLT
        //               ,:RIF-TP-FND
        //               ,:RIF-DS-RIGA
        //               ,:RIF-DS-OPER-SQL
        //               ,:RIF-DS-VER
        //               ,:RIF-DS-TS-INI-CPTZ
        //               ,:RIF-DS-TS-END-CPTZ
        //               ,:RIF-DS-UTENTE
        //               ,:RIF-DS-STATO-ELAB
        //               ,:RIF-DT-INVST-CALC-DB
        //                :IND-RIF-DT-INVST-CALC
        //               ,:RIF-FL-CALC-INVTO
        //                :IND-RIF-FL-CALC-INVTO
        //               ,:RIF-IMP-GAP-EVENT
        //                :IND-RIF-IMP-GAP-EVENT
        //               ,:RIF-FL-SWM-BP2S
        //                :IND-RIF-FL-SWM-BP2S
        //           END-EXEC.
        richInvstFndDao.fetchCIdpCpzRif(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-RIF-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO RIF-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-ID-MOVI-CHIU-NULL
            richInvstFnd.getRifIdMoviChiu().setRifIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifIdMoviChiu.Len.RIF_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-RIF-COD-FND = -1
        //              MOVE HIGH-VALUES TO RIF-COD-FND-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-COD-FND-NULL
            richInvstFnd.setRifCodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichInvstFndIdbsrif0.Len.RIF_COD_FND));
        }
        // COB_CODE: IF IND-RIF-NUM-QUO = -1
        //              MOVE HIGH-VALUES TO RIF-NUM-QUO-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getNumQuo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-NUM-QUO-NULL
            richInvstFnd.getRifNumQuo().setRifNumQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifNumQuo.Len.RIF_NUM_QUO_NULL));
        }
        // COB_CODE: IF IND-RIF-PC = -1
        //              MOVE HIGH-VALUES TO RIF-PC-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getPc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-PC-NULL
            richInvstFnd.getRifPc().setRifPcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifPc.Len.RIF_PC_NULL));
        }
        // COB_CODE: IF IND-RIF-IMP-MOVTO = -1
        //              MOVE HIGH-VALUES TO RIF-IMP-MOVTO-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getImpMovto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-IMP-MOVTO-NULL
            richInvstFnd.getRifImpMovto().setRifImpMovtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifImpMovto.Len.RIF_IMP_MOVTO_NULL));
        }
        // COB_CODE: IF IND-RIF-DT-INVST = -1
        //              MOVE HIGH-VALUES TO RIF-DT-INVST-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getDtInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-DT-INVST-NULL
            richInvstFnd.getRifDtInvst().setRifDtInvstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifDtInvst.Len.RIF_DT_INVST_NULL));
        }
        // COB_CODE: IF IND-RIF-COD-TARI = -1
        //              MOVE HIGH-VALUES TO RIF-COD-TARI-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-COD-TARI-NULL
            richInvstFnd.setRifCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichInvstFndIdbsrif0.Len.RIF_COD_TARI));
        }
        // COB_CODE: IF IND-RIF-TP-STAT = -1
        //              MOVE HIGH-VALUES TO RIF-TP-STAT-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getTpStat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-TP-STAT-NULL
            richInvstFnd.setRifTpStat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichInvstFndIdbsrif0.Len.RIF_TP_STAT));
        }
        // COB_CODE: IF IND-RIF-TP-MOD-INVST = -1
        //              MOVE HIGH-VALUES TO RIF-TP-MOD-INVST-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getTpModInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-TP-MOD-INVST-NULL
            richInvstFnd.setRifTpModInvst(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichInvstFndIdbsrif0.Len.RIF_TP_MOD_INVST));
        }
        // COB_CODE: IF IND-RIF-DT-CAMBIO-VLT = -1
        //              MOVE HIGH-VALUES TO RIF-DT-CAMBIO-VLT-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getDtCambioVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-DT-CAMBIO-VLT-NULL
            richInvstFnd.getRifDtCambioVlt().setRifDtCambioVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifDtCambioVlt.Len.RIF_DT_CAMBIO_VLT_NULL));
        }
        // COB_CODE: IF IND-RIF-DT-INVST-CALC = -1
        //              MOVE HIGH-VALUES TO RIF-DT-INVST-CALC-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getDtInvstCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-DT-INVST-CALC-NULL
            richInvstFnd.getRifDtInvstCalc().setRifDtInvstCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifDtInvstCalc.Len.RIF_DT_INVST_CALC_NULL));
        }
        // COB_CODE: IF IND-RIF-FL-CALC-INVTO = -1
        //              MOVE HIGH-VALUES TO RIF-FL-CALC-INVTO-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getFlCalcInvto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-FL-CALC-INVTO-NULL
            richInvstFnd.setRifFlCalcInvto(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RIF-IMP-GAP-EVENT = -1
        //              MOVE HIGH-VALUES TO RIF-IMP-GAP-EVENT-NULL
        //           END-IF
        if (ws.getIndRichInvstFnd().getImpGapEvent() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-IMP-GAP-EVENT-NULL
            richInvstFnd.getRifImpGapEvent().setRifImpGapEventNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifImpGapEvent.Len.RIF_IMP_GAP_EVENT_NULL));
        }
        // COB_CODE: IF IND-RIF-FL-SWM-BP2S = -1
        //              MOVE HIGH-VALUES TO RIF-FL-SWM-BP2S-NULL
        //           END-IF.
        if (ws.getIndRichInvstFnd().getFlSwmBp2s() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIF-FL-SWM-BP2S-NULL
            richInvstFnd.setRifFlSwmBp2s(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO RIF-DS-OPER-SQL
        richInvstFnd.setRifDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO RIF-DS-VER
        richInvstFnd.setRifDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO RIF-DS-UTENTE
        richInvstFnd.setRifDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO RIF-DS-STATO-ELAB.
        richInvstFnd.setRifDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO RIF-DS-OPER-SQL
        richInvstFnd.setRifDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO RIF-DS-UTENTE.
        richInvstFnd.setRifDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF RIF-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-RIF-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifIdMoviChiu().getRifIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-ID-MOVI-CHIU
            ws.getIndRichInvstFnd().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-ID-MOVI-CHIU
            ws.getIndRichInvstFnd().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF RIF-COD-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-COD-FND
        //           ELSE
        //              MOVE 0 TO IND-RIF-COD-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifCodFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-COD-FND
            ws.getIndRichInvstFnd().setCodFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-COD-FND
            ws.getIndRichInvstFnd().setCodFnd(((short)0));
        }
        // COB_CODE: IF RIF-NUM-QUO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-NUM-QUO
        //           ELSE
        //              MOVE 0 TO IND-RIF-NUM-QUO
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifNumQuo().getRifNumQuoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-NUM-QUO
            ws.getIndRichInvstFnd().setNumQuo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-NUM-QUO
            ws.getIndRichInvstFnd().setNumQuo(((short)0));
        }
        // COB_CODE: IF RIF-PC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-PC
        //           ELSE
        //              MOVE 0 TO IND-RIF-PC
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifPc().getRifPcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-PC
            ws.getIndRichInvstFnd().setPc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-PC
            ws.getIndRichInvstFnd().setPc(((short)0));
        }
        // COB_CODE: IF RIF-IMP-MOVTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-IMP-MOVTO
        //           ELSE
        //              MOVE 0 TO IND-RIF-IMP-MOVTO
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifImpMovto().getRifImpMovtoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-IMP-MOVTO
            ws.getIndRichInvstFnd().setImpMovto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-IMP-MOVTO
            ws.getIndRichInvstFnd().setImpMovto(((short)0));
        }
        // COB_CODE: IF RIF-DT-INVST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-DT-INVST
        //           ELSE
        //              MOVE 0 TO IND-RIF-DT-INVST
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifDtInvst().getRifDtInvstNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-DT-INVST
            ws.getIndRichInvstFnd().setDtInvst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-DT-INVST
            ws.getIndRichInvstFnd().setDtInvst(((short)0));
        }
        // COB_CODE: IF RIF-COD-TARI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-COD-TARI
        //           ELSE
        //              MOVE 0 TO IND-RIF-COD-TARI
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifCodTariFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-COD-TARI
            ws.getIndRichInvstFnd().setCodTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-COD-TARI
            ws.getIndRichInvstFnd().setCodTari(((short)0));
        }
        // COB_CODE: IF RIF-TP-STAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-TP-STAT
        //           ELSE
        //              MOVE 0 TO IND-RIF-TP-STAT
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifTpStatFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-TP-STAT
            ws.getIndRichInvstFnd().setTpStat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-TP-STAT
            ws.getIndRichInvstFnd().setTpStat(((short)0));
        }
        // COB_CODE: IF RIF-TP-MOD-INVST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-TP-MOD-INVST
        //           ELSE
        //              MOVE 0 TO IND-RIF-TP-MOD-INVST
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifTpModInvstFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-TP-MOD-INVST
            ws.getIndRichInvstFnd().setTpModInvst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-TP-MOD-INVST
            ws.getIndRichInvstFnd().setTpModInvst(((short)0));
        }
        // COB_CODE: IF RIF-DT-CAMBIO-VLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-DT-CAMBIO-VLT
        //           ELSE
        //              MOVE 0 TO IND-RIF-DT-CAMBIO-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifDtCambioVlt().getRifDtCambioVltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-DT-CAMBIO-VLT
            ws.getIndRichInvstFnd().setDtCambioVlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-DT-CAMBIO-VLT
            ws.getIndRichInvstFnd().setDtCambioVlt(((short)0));
        }
        // COB_CODE: IF RIF-DT-INVST-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-DT-INVST-CALC
        //           ELSE
        //              MOVE 0 TO IND-RIF-DT-INVST-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifDtInvstCalc().getRifDtInvstCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-DT-INVST-CALC
            ws.getIndRichInvstFnd().setDtInvstCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-DT-INVST-CALC
            ws.getIndRichInvstFnd().setDtInvstCalc(((short)0));
        }
        // COB_CODE: IF RIF-FL-CALC-INVTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-FL-CALC-INVTO
        //           ELSE
        //              MOVE 0 TO IND-RIF-FL-CALC-INVTO
        //           END-IF
        if (Conditions.eq(richInvstFnd.getRifFlCalcInvto(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RIF-FL-CALC-INVTO
            ws.getIndRichInvstFnd().setFlCalcInvto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-FL-CALC-INVTO
            ws.getIndRichInvstFnd().setFlCalcInvto(((short)0));
        }
        // COB_CODE: IF RIF-IMP-GAP-EVENT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-IMP-GAP-EVENT
        //           ELSE
        //              MOVE 0 TO IND-RIF-IMP-GAP-EVENT
        //           END-IF
        if (Characters.EQ_HIGH.test(richInvstFnd.getRifImpGapEvent().getRifImpGapEventNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RIF-IMP-GAP-EVENT
            ws.getIndRichInvstFnd().setImpGapEvent(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-IMP-GAP-EVENT
            ws.getIndRichInvstFnd().setImpGapEvent(((short)0));
        }
        // COB_CODE: IF RIF-FL-SWM-BP2S-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RIF-FL-SWM-BP2S
        //           ELSE
        //              MOVE 0 TO IND-RIF-FL-SWM-BP2S
        //           END-IF.
        if (Conditions.eq(richInvstFnd.getRifFlSwmBp2s(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RIF-FL-SWM-BP2S
            ws.getIndRichInvstFnd().setFlSwmBp2s(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RIF-FL-SWM-BP2S
            ws.getIndRichInvstFnd().setFlSwmBp2s(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : RIF-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE RICH-INVST-FND TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(richInvstFnd.getRichInvstFndFormatted());
        // COB_CODE: MOVE RIF-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(richInvstFnd.getRifIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO RIF-ID-MOVI-CHIU
                richInvstFnd.getRifIdMoviChiu().setRifIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO RIF-DS-TS-END-CPTZ
                richInvstFnd.setRifDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO RIF-ID-MOVI-CRZ
                    richInvstFnd.setRifIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO RIF-ID-MOVI-CHIU-NULL
                    richInvstFnd.getRifIdMoviChiu().setRifIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifIdMoviChiu.Len.RIF_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO RIF-DT-END-EFF
                    richInvstFnd.setRifDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO RIF-DS-TS-INI-CPTZ
                    richInvstFnd.setRifDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO RIF-DS-TS-END-CPTZ
                    richInvstFnd.setRifDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE RICH-INVST-FND TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(richInvstFnd.getRichInvstFndFormatted());
        // COB_CODE: MOVE RIF-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(richInvstFnd.getRifIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO RICH-INVST-FND.
        richInvstFnd.setRichInvstFndFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO RIF-ID-MOVI-CRZ.
        richInvstFnd.setRifIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO RIF-ID-MOVI-CHIU-NULL.
        richInvstFnd.getRifIdMoviChiu().setRifIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RifIdMoviChiu.Len.RIF_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO RIF-DT-INI-EFF.
        richInvstFnd.setRifDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO RIF-DT-END-EFF.
        richInvstFnd.setRifDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO RIF-DS-TS-INI-CPTZ.
        richInvstFnd.setRifDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO RIF-DS-TS-END-CPTZ.
        richInvstFnd.setRifDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO RIF-COD-COMP-ANIA.
        richInvstFnd.setRifCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE RIF-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richInvstFnd.getRifDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RIF-DT-INI-EFF-DB
        ws.getRichInvstFndDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE RIF-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richInvstFnd.getRifDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RIF-DT-END-EFF-DB
        ws.getRichInvstFndDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-RIF-DT-INVST = 0
        //               MOVE WS-DATE-X      TO RIF-DT-INVST-DB
        //           END-IF
        if (ws.getIndRichInvstFnd().getDtInvst() == 0) {
            // COB_CODE: MOVE RIF-DT-INVST TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richInvstFnd.getRifDtInvst().getRifDtInvst(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO RIF-DT-INVST-DB
            ws.getRichInvstFndDb().setIniCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-RIF-DT-CAMBIO-VLT = 0
        //               MOVE WS-DATE-X      TO RIF-DT-CAMBIO-VLT-DB
        //           END-IF
        if (ws.getIndRichInvstFnd().getDtCambioVlt() == 0) {
            // COB_CODE: MOVE RIF-DT-CAMBIO-VLT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richInvstFnd.getRifDtCambioVlt().getRifDtCambioVlt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO RIF-DT-CAMBIO-VLT-DB
            ws.getRichInvstFndDb().setEndCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-RIF-DT-INVST-CALC = 0
        //               MOVE WS-DATE-X      TO RIF-DT-INVST-CALC-DB
        //           END-IF.
        if (ws.getIndRichInvstFnd().getDtInvstCalc() == 0) {
            // COB_CODE: MOVE RIF-DT-INVST-CALC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(richInvstFnd.getRifDtInvstCalc().getRifDtInvstCalc(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO RIF-DT-INVST-CALC-DB
            ws.getRichInvstFndDb().setEsiTitDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE RIF-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichInvstFndDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIF-DT-INI-EFF
        richInvstFnd.setRifDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RIF-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichInvstFndDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIF-DT-END-EFF
        richInvstFnd.setRifDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-RIF-DT-INVST = 0
        //               MOVE WS-DATE-N      TO RIF-DT-INVST
        //           END-IF
        if (ws.getIndRichInvstFnd().getDtInvst() == 0) {
            // COB_CODE: MOVE RIF-DT-INVST-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichInvstFndDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RIF-DT-INVST
            richInvstFnd.getRifDtInvst().setRifDtInvst(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-RIF-DT-CAMBIO-VLT = 0
        //               MOVE WS-DATE-N      TO RIF-DT-CAMBIO-VLT
        //           END-IF
        if (ws.getIndRichInvstFnd().getDtCambioVlt() == 0) {
            // COB_CODE: MOVE RIF-DT-CAMBIO-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichInvstFndDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RIF-DT-CAMBIO-VLT
            richInvstFnd.getRifDtCambioVlt().setRifDtCambioVlt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-RIF-DT-INVST-CALC = 0
        //               MOVE WS-DATE-N      TO RIF-DT-INVST-CALC
        //           END-IF.
        if (ws.getIndRichInvstFnd().getDtInvstCalc() == 0) {
            // COB_CODE: MOVE RIF-DT-INVST-CALC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRichInvstFndDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RIF-DT-INVST-CALC
            richInvstFnd.getRifDtInvstCalc().setRifDtInvstCalc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return richInvstFnd.getRifCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.richInvstFnd.setRifCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDiv() {
        return richInvstFnd.getRifCodDiv();
    }

    @Override
    public void setCodDiv(String codDiv) {
        this.richInvstFnd.setRifCodDiv(codDiv);
    }

    @Override
    public String getCodFnd() {
        return richInvstFnd.getRifCodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.richInvstFnd.setRifCodFnd(codFnd);
    }

    @Override
    public String getCodFndObj() {
        if (ws.getIndRichInvstFnd().getCodFnd() >= 0) {
            return getCodFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        if (codFndObj != null) {
            setCodFnd(codFndObj);
            ws.getIndRichInvstFnd().setCodFnd(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setCodFnd(((short)-1));
        }
    }

    @Override
    public String getCodTari() {
        return richInvstFnd.getRifCodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.richInvstFnd.setRifCodTari(codTari);
    }

    @Override
    public String getCodTariObj() {
        if (ws.getIndRichInvstFnd().getCodTari() >= 0) {
            return getCodTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTariObj(String codTariObj) {
        if (codTariObj != null) {
            setCodTari(codTariObj);
            ws.getIndRichInvstFnd().setCodTari(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setCodTari(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return richInvstFnd.getRifDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.richInvstFnd.setRifDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return richInvstFnd.getRifDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.richInvstFnd.setRifDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return richInvstFnd.getRifDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.richInvstFnd.setRifDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return richInvstFnd.getRifDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.richInvstFnd.setRifDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return richInvstFnd.getRifDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.richInvstFnd.setRifDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return richInvstFnd.getRifDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.richInvstFnd.setRifDsVer(dsVer);
    }

    @Override
    public String getDtCambioVltDb() {
        return ws.getRichInvstFndDb().getEndCopDb();
    }

    @Override
    public void setDtCambioVltDb(String dtCambioVltDb) {
        this.ws.getRichInvstFndDb().setEndCopDb(dtCambioVltDb);
    }

    @Override
    public String getDtCambioVltDbObj() {
        if (ws.getIndRichInvstFnd().getDtCambioVlt() >= 0) {
            return getDtCambioVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtCambioVltDbObj(String dtCambioVltDbObj) {
        if (dtCambioVltDbObj != null) {
            setDtCambioVltDb(dtCambioVltDbObj);
            ws.getIndRichInvstFnd().setDtCambioVlt(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setDtCambioVlt(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getRichInvstFndDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getRichInvstFndDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getRichInvstFndDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getRichInvstFndDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtInvstCalcDb() {
        return ws.getRichInvstFndDb().getEsiTitDb();
    }

    @Override
    public void setDtInvstCalcDb(String dtInvstCalcDb) {
        this.ws.getRichInvstFndDb().setEsiTitDb(dtInvstCalcDb);
    }

    @Override
    public String getDtInvstCalcDbObj() {
        if (ws.getIndRichInvstFnd().getDtInvstCalc() >= 0) {
            return getDtInvstCalcDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtInvstCalcDbObj(String dtInvstCalcDbObj) {
        if (dtInvstCalcDbObj != null) {
            setDtInvstCalcDb(dtInvstCalcDbObj);
            ws.getIndRichInvstFnd().setDtInvstCalc(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setDtInvstCalc(((short)-1));
        }
    }

    @Override
    public String getDtInvstDb() {
        return ws.getRichInvstFndDb().getIniCopDb();
    }

    @Override
    public void setDtInvstDb(String dtInvstDb) {
        this.ws.getRichInvstFndDb().setIniCopDb(dtInvstDb);
    }

    @Override
    public String getDtInvstDbObj() {
        if (ws.getIndRichInvstFnd().getDtInvst() >= 0) {
            return getDtInvstDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtInvstDbObj(String dtInvstDbObj) {
        if (dtInvstDbObj != null) {
            setDtInvstDb(dtInvstDbObj);
            ws.getIndRichInvstFnd().setDtInvst(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setDtInvst(((short)-1));
        }
    }

    @Override
    public char getFlCalcInvto() {
        return richInvstFnd.getRifFlCalcInvto();
    }

    @Override
    public void setFlCalcInvto(char flCalcInvto) {
        this.richInvstFnd.setRifFlCalcInvto(flCalcInvto);
    }

    @Override
    public Character getFlCalcInvtoObj() {
        if (ws.getIndRichInvstFnd().getFlCalcInvto() >= 0) {
            return ((Character)getFlCalcInvto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCalcInvtoObj(Character flCalcInvtoObj) {
        if (flCalcInvtoObj != null) {
            setFlCalcInvto(((char)flCalcInvtoObj));
            ws.getIndRichInvstFnd().setFlCalcInvto(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setFlCalcInvto(((short)-1));
        }
    }

    @Override
    public char getFlSwmBp2s() {
        return richInvstFnd.getRifFlSwmBp2s();
    }

    @Override
    public void setFlSwmBp2s(char flSwmBp2s) {
        this.richInvstFnd.setRifFlSwmBp2s(flSwmBp2s);
    }

    @Override
    public Character getFlSwmBp2sObj() {
        if (ws.getIndRichInvstFnd().getFlSwmBp2s() >= 0) {
            return ((Character)getFlSwmBp2s());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlSwmBp2sObj(Character flSwmBp2sObj) {
        if (flSwmBp2sObj != null) {
            setFlSwmBp2s(((char)flSwmBp2sObj));
            ws.getIndRichInvstFnd().setFlSwmBp2s(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setFlSwmBp2s(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return richInvstFnd.getRifIdMoviChiu().getRifIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.richInvstFnd.getRifIdMoviChiu().setRifIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndRichInvstFnd().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndRichInvstFnd().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return richInvstFnd.getRifIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.richInvstFnd.setRifIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdMoviFinrio() {
        return richInvstFnd.getRifIdMoviFinrio();
    }

    @Override
    public void setIdMoviFinrio(int idMoviFinrio) {
        this.richInvstFnd.setRifIdMoviFinrio(idMoviFinrio);
    }

    @Override
    public int getIdRichInvstFnd() {
        return richInvstFnd.getRifIdRichInvstFnd();
    }

    @Override
    public void setIdRichInvstFnd(int idRichInvstFnd) {
        this.richInvstFnd.setRifIdRichInvstFnd(idRichInvstFnd);
    }

    @Override
    public AfDecimal getImpGapEvent() {
        return richInvstFnd.getRifImpGapEvent().getRifImpGapEvent();
    }

    @Override
    public void setImpGapEvent(AfDecimal impGapEvent) {
        this.richInvstFnd.getRifImpGapEvent().setRifImpGapEvent(impGapEvent.copy());
    }

    @Override
    public AfDecimal getImpGapEventObj() {
        if (ws.getIndRichInvstFnd().getImpGapEvent() >= 0) {
            return getImpGapEvent();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpGapEventObj(AfDecimal impGapEventObj) {
        if (impGapEventObj != null) {
            setImpGapEvent(new AfDecimal(impGapEventObj, 15, 3));
            ws.getIndRichInvstFnd().setImpGapEvent(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setImpGapEvent(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpMovto() {
        return richInvstFnd.getRifImpMovto().getRifImpMovto();
    }

    @Override
    public void setImpMovto(AfDecimal impMovto) {
        this.richInvstFnd.getRifImpMovto().setRifImpMovto(impMovto.copy());
    }

    @Override
    public AfDecimal getImpMovtoObj() {
        if (ws.getIndRichInvstFnd().getImpMovto() >= 0) {
            return getImpMovto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpMovtoObj(AfDecimal impMovtoObj) {
        if (impMovtoObj != null) {
            setImpMovto(new AfDecimal(impMovtoObj, 15, 3));
            ws.getIndRichInvstFnd().setImpMovto(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setImpMovto(((short)-1));
        }
    }

    @Override
    public AfDecimal getNumQuo() {
        return richInvstFnd.getRifNumQuo().getRifNumQuo();
    }

    @Override
    public void setNumQuo(AfDecimal numQuo) {
        this.richInvstFnd.getRifNumQuo().setRifNumQuo(numQuo.copy());
    }

    @Override
    public AfDecimal getNumQuoObj() {
        if (ws.getIndRichInvstFnd().getNumQuo() >= 0) {
            return getNumQuo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumQuoObj(AfDecimal numQuoObj) {
        if (numQuoObj != null) {
            setNumQuo(new AfDecimal(numQuoObj, 12, 5));
            ws.getIndRichInvstFnd().setNumQuo(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setNumQuo(((short)-1));
        }
    }

    @Override
    public AfDecimal getPc() {
        return richInvstFnd.getRifPc().getRifPc();
    }

    @Override
    public void setPc(AfDecimal pc) {
        this.richInvstFnd.getRifPc().setRifPc(pc.copy());
    }

    @Override
    public AfDecimal getPcObj() {
        if (ws.getIndRichInvstFnd().getPc() >= 0) {
            return getPc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcObj(AfDecimal pcObj) {
        if (pcObj != null) {
            setPc(new AfDecimal(pcObj, 6, 3));
            ws.getIndRichInvstFnd().setPc(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setPc(((short)-1));
        }
    }

    @Override
    public long getRifDsRiga() {
        return richInvstFnd.getRifDsRiga();
    }

    @Override
    public void setRifDsRiga(long rifDsRiga) {
        this.richInvstFnd.setRifDsRiga(rifDsRiga);
    }

    @Override
    public char getTpFnd() {
        return richInvstFnd.getRifTpFnd();
    }

    @Override
    public void setTpFnd(char tpFnd) {
        this.richInvstFnd.setRifTpFnd(tpFnd);
    }

    @Override
    public String getTpModInvst() {
        return richInvstFnd.getRifTpModInvst();
    }

    @Override
    public void setTpModInvst(String tpModInvst) {
        this.richInvstFnd.setRifTpModInvst(tpModInvst);
    }

    @Override
    public String getTpModInvstObj() {
        if (ws.getIndRichInvstFnd().getTpModInvst() >= 0) {
            return getTpModInvst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpModInvstObj(String tpModInvstObj) {
        if (tpModInvstObj != null) {
            setTpModInvst(tpModInvstObj);
            ws.getIndRichInvstFnd().setTpModInvst(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setTpModInvst(((short)-1));
        }
    }

    @Override
    public String getTpStat() {
        return richInvstFnd.getRifTpStat();
    }

    @Override
    public void setTpStat(String tpStat) {
        this.richInvstFnd.setRifTpStat(tpStat);
    }

    @Override
    public String getTpStatObj() {
        if (ws.getIndRichInvstFnd().getTpStat() >= 0) {
            return getTpStat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatObj(String tpStatObj) {
        if (tpStatObj != null) {
            setTpStat(tpStatObj);
            ws.getIndRichInvstFnd().setTpStat(((short)0));
        }
        else {
            ws.getIndRichInvstFnd().setTpStat(((short)-1));
        }
    }
}
