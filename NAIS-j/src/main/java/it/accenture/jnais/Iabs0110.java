package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.DefaultValues;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcRecScheduleDao;
import it.accenture.jnais.commons.data.to.IBtcRecSchedule;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcRecSchedule;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Idsv0003;

/**Original name: IABS0110<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
 *                    DA TABELLE GUIDA RICHIAMATO DAL BATCH EXEC. *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0110 extends Program implements IBtcRecSchedule {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcRecScheduleDao btcRecScheduleDao = new BtcRecScheduleDao(dbAccessStatus);
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-BRS-TYPE-RECORD
    private short indBrsTypeRecord = DefaultValues.BIN_SHORT_VAL;
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IABV0002
    private Iabv0002 iabv0002;
    //Original name: BTC-REC-SCHEDULE
    private BtcRecSchedule btcRecSchedule;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0110_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Iabv0002 iabv0002, BtcRecSchedule btcRecSchedule) {
        this.idsv0003 = idsv0003;
        this.iabv0002 = iabv0002;
        this.btcRecSchedule = btcRecSchedule;
        // COB_CODE: PERFORM A000-INIZIO                 THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A300-ELABORA                THRU A300-EX.
        a300Elabora();
        // COB_CODE: PERFORM A400-FINE                   THRU A400-EX.
        a400Fine();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0110 getInstance() {
        return ((Iabs0110)Programs.getInstance(Iabs0110.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0110'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0110");
        // COB_CODE: MOVE 'BTC_REC_SCHEDULE'      TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_REC_SCHEDULE");
        // COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
        //                                             IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                             IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(descrizErrDb2);
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A310-DELETE                 THRU A310-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A320-INSERT                 THRU A320-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR           THRU A370-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST            THRU A380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT             THRU A390-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A310-DELETE                 THRU A310-EX
            a310Delete();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A320-INSERT                 THRU A320-EX
            a320Insert();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR           THRU A370-EX
            a370CloseCursor();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST            THRU A380-EX
            a380FetchFirst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT             THRU A390-EX
            a390FetchNext();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-FINE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a400Fine() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A305-DECLARE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursor() {
    // COB_CODE: EXEC SQL
    //              DECLARE CUR-BRS CURSOR WITH HOLD FOR
    //              SELECT
    //                ID_BATCH
    //                ,ID_JOB
    //                ,ID_RECORD
    //                ,TYPE_RECORD
    //                ,DATA_RECORD
    //              FROM BTC_REC_SCHEDULE
    //             WHERE     ID_BATCH = :BRS-ID-BATCH
    //                   AND ID_JOB   = :BRS-ID-JOB
    //              ORDER BY ID_RECORD
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A320-INSERT<br>*/
    private void a320Insert() {
        // COB_CODE: PERFORM Z400-SEQ                       THRU Z400-EX.
        z400Seq();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX
            z150ValorizzaDataServices();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO BTC_REC_SCHEDULE
            //                  (
            //                     ID_BATCH
            //                    ,ID_JOB
            //                    ,ID_RECORD
            //                    ,TYPE_RECORD
            //                    ,DATA_RECORD
            //                  )
            //              VALUES
            //                  (
            //                    :BRS-ID-BATCH
            //                    ,:BRS-ID-JOB
            //                    ,:BRS-ID-RECORD
            //                    ,:BRS-TYPE-RECORD
            //                     :IND-BRS-TYPE-RECORD
            //                    ,:BRS-DATA-RECORD-VCHAR
            //                  )
            //           END-EXEC
            btcRecScheduleDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A360-OPEN-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursor() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR THRU A305-EX.
        a305DeclareCursor();
        // COB_CODE: EXEC SQL
        //                OPEN CUR-BRS
        //           END-EXEC.
        btcRecScheduleDao.openCurBrs(btcRecSchedule.getIdBatch(), btcRecSchedule.getIdJob());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-BRS
        //           END-EXEC.
        btcRecScheduleDao.closeCurBrs();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirst() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR    THRU A360-EX.
        a360OpenCursor();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT THRU A390-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT THRU A390-EX
            a390FetchNext();
        }
    }

    /**Original name: A390-FETCH-NEXT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNext() {
        // COB_CODE: INITIALIZE BTC-REC-SCHEDULE.
        initBtcRecSchedule();
        // COB_CODE: EXEC SQL
        //                FETCH CUR-BRS
        //           INTO
        //                 :BRS-ID-BATCH
        //                 ,:BRS-ID-JOB
        //                 ,:BRS-ID-RECORD
        //                 ,:BRS-TYPE-RECORD
        //                  :IND-BRS-TYPE-RECORD
        //                 ,:BRS-DATA-RECORD-VCHAR
        //           END-EXEC.
        btcRecScheduleDao.fetchCurBrs(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR THRU A370-EX
            a370CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A310-DELETE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310Delete() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM BTC_REC_SCHEDULE
        //                WHERE  ID_BATCH = :BRS-ID-BATCH
        //                   AND ID_JOB   = :BRS-ID-JOB
        //           END-EXEC.
        btcRecScheduleDao.deleteRec(btcRecSchedule.getIdBatch(), btcRecSchedule.getIdJob());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BRS-TYPE-RECORD = -1
        //              MOVE HIGH-VALUES TO BRS-TYPE-RECORD-NULL
        //           END-IF.
        if (indBrsTypeRecord == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BRS-TYPE-RECORD-NULL
            btcRecSchedule.setTypeRecord(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcRecSchedule.Len.TYPE_RECORD));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES<br>
	 * <pre>*****************************************************************</pre>*/
    private void z150ValorizzaDataServices() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BRS-TYPE-RECORD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BRS-TYPE-RECORD
        //           ELSE
        //              MOVE 0 TO IND-BRS-TYPE-RECORD
        //           END-IF.
        if (Characters.EQ_HIGH.test(btcRecSchedule.getTypeRecordFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BRS-TYPE-RECORD
            indBrsTypeRecord = ((short)-1);
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BRS-TYPE-RECORD
            indBrsTypeRecord = ((short)0);
        }
    }

    /**Original name: Z400-SEQ<br>*/
    private void z400Seq() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_ID_RECORD
        //              INTO :BRS-ID-RECORD
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: IF BRS-DATA-RECORD-LEN NOT > ZEROES
        //                          TO BRS-DATA-RECORD-LEN
        //           END-IF.
        if (btcRecSchedule.getDataRecordLen() <= 0) {
            // COB_CODE: MOVE LENGTH OF BRS-DATA-RECORD
            //                       TO BRS-DATA-RECORD-LEN
            btcRecSchedule.setDataRecordLen(((short)BtcRecSchedule.Len.DATA_RECORD));
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            idsv0010.setWsDataInizioEffettoDb(idsv0010.getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                idsv0010.setWsDataFineEffettoDb(idsv0010.getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            idsv0010.setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            idsv0010.setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    public void initBtcRecSchedule() {
        btcRecSchedule.setIdBatch(0);
        btcRecSchedule.setIdJob(0);
        btcRecSchedule.setIdRecord(0);
        btcRecSchedule.setTypeRecord("");
        btcRecSchedule.setDataRecordLen(((short)0));
        btcRecSchedule.setDataRecord("");
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    public void setIndBrsTypeRecord(short indBrsTypeRecord) {
        this.indBrsTypeRecord = indBrsTypeRecord;
    }

    public short getIndBrsTypeRecord() {
        return this.indBrsTypeRecord;
    }

    @Override
    public String getDataRecordVchar() {
        return btcRecSchedule.getDataRecordVcharFormatted();
    }

    @Override
    public void setDataRecordVchar(String dataRecordVchar) {
        this.btcRecSchedule.setDataRecordVcharFormatted(dataRecordVchar);
    }

    @Override
    public int getIdBatch() {
        return btcRecSchedule.getIdBatch();
    }

    @Override
    public void setIdBatch(int idBatch) {
        this.btcRecSchedule.setIdBatch(idBatch);
    }

    @Override
    public int getIdJob() {
        return btcRecSchedule.getIdJob();
    }

    @Override
    public void setIdJob(int idJob) {
        this.btcRecSchedule.setIdJob(idJob);
    }

    @Override
    public int getIdRecord() {
        return btcRecSchedule.getIdRecord();
    }

    @Override
    public void setIdRecord(int idRecord) {
        this.btcRecSchedule.setIdRecord(idRecord);
    }

    @Override
    public String getTypeRecord() {
        return btcRecSchedule.getTypeRecord();
    }

    @Override
    public void setTypeRecord(String typeRecord) {
        this.btcRecSchedule.setTypeRecord(typeRecord);
    }

    @Override
    public String getTypeRecordObj() {
        if (getIndBrsTypeRecord() >= 0) {
            return getTypeRecord();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTypeRecordObj(String typeRecordObj) {
        if (typeRecordObj != null) {
            setTypeRecord(typeRecordObj);
            setIndBrsTypeRecord(((short)0));
        }
        else {
            setIndBrsTypeRecord(((short)-1));
        }
    }
}
