package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettTitDiRatDao;
import it.accenture.jnais.commons.data.to.IDettTitDiRat;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DettTitDiRatIdbsdtr0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs3460Data;
import it.accenture.jnais.ws.redefines.DtrAcqExp;
import it.accenture.jnais.ws.redefines.DtrCarAcq;
import it.accenture.jnais.ws.redefines.DtrCarGest;
import it.accenture.jnais.ws.redefines.DtrCarIas;
import it.accenture.jnais.ws.redefines.DtrCarInc;
import it.accenture.jnais.ws.redefines.DtrCnbtAntirac;
import it.accenture.jnais.ws.redefines.DtrCommisInter;
import it.accenture.jnais.ws.redefines.DtrDir;
import it.accenture.jnais.ws.redefines.DtrDtEndCop;
import it.accenture.jnais.ws.redefines.DtrDtIniCop;
import it.accenture.jnais.ws.redefines.DtrFrqMovi;
import it.accenture.jnais.ws.redefines.DtrIdMoviChiu;
import it.accenture.jnais.ws.redefines.DtrImpAder;
import it.accenture.jnais.ws.redefines.DtrImpAz;
import it.accenture.jnais.ws.redefines.DtrImpTfr;
import it.accenture.jnais.ws.redefines.DtrImpTfrStrc;
import it.accenture.jnais.ws.redefines.DtrImpTrasfe;
import it.accenture.jnais.ws.redefines.DtrImpVolo;
import it.accenture.jnais.ws.redefines.DtrIntrFraz;
import it.accenture.jnais.ws.redefines.DtrIntrMora;
import it.accenture.jnais.ws.redefines.DtrIntrRetdt;
import it.accenture.jnais.ws.redefines.DtrIntrRiat;
import it.accenture.jnais.ws.redefines.DtrManfeeAntic;
import it.accenture.jnais.ws.redefines.DtrManfeeRicor;
import it.accenture.jnais.ws.redefines.DtrPreNet;
import it.accenture.jnais.ws.redefines.DtrPrePpIas;
import it.accenture.jnais.ws.redefines.DtrPreSoloRsh;
import it.accenture.jnais.ws.redefines.DtrPreTot;
import it.accenture.jnais.ws.redefines.DtrProvAcq1aa;
import it.accenture.jnais.ws.redefines.DtrProvAcq2aa;
import it.accenture.jnais.ws.redefines.DtrProvDaRec;
import it.accenture.jnais.ws.redefines.DtrProvInc;
import it.accenture.jnais.ws.redefines.DtrProvRicor;
import it.accenture.jnais.ws.redefines.DtrRemunAss;
import it.accenture.jnais.ws.redefines.DtrSoprAlt;
import it.accenture.jnais.ws.redefines.DtrSoprProf;
import it.accenture.jnais.ws.redefines.DtrSoprSan;
import it.accenture.jnais.ws.redefines.DtrSoprSpo;
import it.accenture.jnais.ws.redefines.DtrSoprTec;
import it.accenture.jnais.ws.redefines.DtrSpeAge;
import it.accenture.jnais.ws.redefines.DtrSpeMed;
import it.accenture.jnais.ws.redefines.DtrTax;
import it.accenture.jnais.ws.redefines.DtrTotIntrPrest;

/**Original name: LDBS3460<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs3460 extends Program implements IDettTitDiRat {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DettTitDiRatDao dettTitDiRatDao = new DettTitDiRatDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs3460Data ws = new Ldbs3460Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: DETT-TIT-DI-RAT
    private DettTitDiRatIdbsdtr0 dettTitDiRat;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS3460_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DettTitDiRatIdbsdtr0 dettTitDiRat) {
        this.idsv0003 = idsv0003;
        this.dettTitDiRat = dettTitDiRat;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs3460 getInstance() {
        return ((Ldbs3460)Programs.getInstance(Ldbs3460.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS3460'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS3460");
        // COB_CODE: MOVE 'DETT-TIT-DI-RAT' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("DETT-TIT-DI-RAT");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_DETT_TIT_DI_RAT
        //                    ,ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,SPE_AGE
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_IAS
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_DI_RAT
        //              WHERE  ID_TIT_RAT      = :DTR-ID-TIT-RAT
        //                    AND FL_VLDT_TIT = :DTR-FL-VLDT-TIT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //                    UNION
        //              SELECT
        //                     ID_DETT_TIT_DI_RAT
        //                    ,ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,SPE_AGE
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_IAS
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_DI_RAT
        //              WHERE  ID_TIT_RAT      = :DTR-ID-TIT-RAT
        //                    AND FL_VLDT_TIT IS NULL
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        dettTitDiRatDao.openCEff21(dettTitDiRat.getDtrIdTitRat(), dettTitDiRat.getDtrFlVldtTit(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        dettTitDiRatDao.closeCEff21();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //           END-EXEC.
        dettTitDiRatDao.fetchCEff21(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_DETT_TIT_DI_RAT
        //                    ,ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,SPE_AGE
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_IAS
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_DI_RAT
        //              WHERE  ID_TIT_RAT      = :DTR-ID-TIT-RAT
        //                        AND FL_VLDT_TIT = :DTR-FL-VLDT-TIT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                        UNION
        //              SELECT
        //                     ID_DETT_TIT_DI_RAT
        //                    ,ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,SPE_AGE
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_IAS
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_DI_RAT
        //              WHERE  ID_TIT_RAT      = :DTR-ID-TIT-RAT
        //                        AND FL_VLDT_TIT IS NULL
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        dettTitDiRatDao.openCCpz21(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        dettTitDiRatDao.closeCCpz21();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //           END-EXEC.
        dettTitDiRatDao.fetchCCpz21(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DTR-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DTR-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-ID-MOVI-CHIU-NULL
            dettTitDiRat.getDtrIdMoviChiu().setDtrIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIdMoviChiu.Len.DTR_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DTR-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO DTR-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-DT-INI-COP-NULL
            dettTitDiRat.getDtrDtIniCop().setDtrDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrDtIniCop.Len.DTR_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-DTR-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO DTR-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-DT-END-COP-NULL
            dettTitDiRat.getDtrDtEndCop().setDtrDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrDtEndCop.Len.DTR_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-DTR-PRE-NET = -1
        //              MOVE HIGH-VALUES TO DTR-PRE-NET-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PRE-NET-NULL
            dettTitDiRat.getDtrPreNet().setDtrPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrPreNet.Len.DTR_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-DTR-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO DTR-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-INTR-FRAZ-NULL
            dettTitDiRat.getDtrIntrFraz().setDtrIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIntrFraz.Len.DTR_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-DTR-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO DTR-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-INTR-MORA-NULL
            dettTitDiRat.getDtrIntrMora().setDtrIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIntrMora.Len.DTR_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-DTR-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO DTR-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-INTR-RETDT-NULL
            dettTitDiRat.getDtrIntrRetdt().setDtrIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIntrRetdt.Len.DTR_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-DTR-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO DTR-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-INTR-RIAT-NULL
            dettTitDiRat.getDtrIntrRiat().setDtrIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIntrRiat.Len.DTR_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-DTR-DIR = -1
        //              MOVE HIGH-VALUES TO DTR-DIR-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-DIR-NULL
            dettTitDiRat.getDtrDir().setDtrDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrDir.Len.DTR_DIR_NULL));
        }
        // COB_CODE: IF IND-DTR-SPE-MED = -1
        //              MOVE HIGH-VALUES TO DTR-SPE-MED-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SPE-MED-NULL
            dettTitDiRat.getDtrSpeMed().setDtrSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSpeMed.Len.DTR_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-DTR-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO DTR-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SPE-AGE-NULL
            dettTitDiRat.getDtrSpeAge().setDtrSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSpeAge.Len.DTR_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-DTR-TAX = -1
        //              MOVE HIGH-VALUES TO DTR-TAX-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-TAX-NULL
            dettTitDiRat.getDtrTax().setDtrTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrTax.Len.DTR_TAX_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-SAN-NULL
            dettTitDiRat.getDtrSoprSan().setDtrSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprSan.Len.DTR_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-SPO-NULL
            dettTitDiRat.getDtrSoprSpo().setDtrSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprSpo.Len.DTR_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-TEC-NULL
            dettTitDiRat.getDtrSoprTec().setDtrSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprTec.Len.DTR_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-PROF-NULL
            dettTitDiRat.getDtrSoprProf().setDtrSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprProf.Len.DTR_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-ALT-NULL
            dettTitDiRat.getDtrSoprAlt().setDtrSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprAlt.Len.DTR_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-DTR-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO DTR-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PRE-TOT-NULL
            dettTitDiRat.getDtrPreTot().setDtrPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrPreTot.Len.DTR_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-DTR-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO DTR-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PRE-PP-IAS-NULL
            dettTitDiRat.getDtrPrePpIas().setDtrPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrPrePpIas.Len.DTR_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-DTR-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO DTR-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PRE-SOLO-RSH-NULL
            dettTitDiRat.getDtrPreSoloRsh().setDtrPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrPreSoloRsh.Len.DTR_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-DTR-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO DTR-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CAR-IAS-NULL
            dettTitDiRat.getDtrCarIas().setDtrCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCarIas.Len.DTR_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-ACQ-1AA-NULL
            dettTitDiRat.getDtrProvAcq1aa().setDtrProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvAcq1aa.Len.DTR_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-ACQ-2AA-NULL
            dettTitDiRat.getDtrProvAcq2aa().setDtrProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvAcq2aa.Len.DTR_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-RICOR-NULL
            dettTitDiRat.getDtrProvRicor().setDtrProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvRicor.Len.DTR_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-INC = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-INC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-INC-NULL
            dettTitDiRat.getDtrProvInc().setDtrProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvInc.Len.DTR_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-DA-REC-NULL
            dettTitDiRat.getDtrProvDaRec().setDtrProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvDaRec.Len.DTR_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-DTR-COD-DVS = -1
        //              MOVE HIGH-VALUES TO DTR-COD-DVS-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-COD-DVS-NULL
            dettTitDiRat.setDtrCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitDiRatIdbsdtr0.Len.DTR_COD_DVS));
        }
        // COB_CODE: IF IND-DTR-FRQ-MOVI = -1
        //              MOVE HIGH-VALUES TO DTR-FRQ-MOVI-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-FRQ-MOVI-NULL
            dettTitDiRat.getDtrFrqMovi().setDtrFrqMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrFrqMovi.Len.DTR_FRQ_MOVI_NULL));
        }
        // COB_CODE: IF IND-DTR-COD-TARI = -1
        //              MOVE HIGH-VALUES TO DTR-COD-TARI-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-COD-TARI-NULL
            dettTitDiRat.setDtrCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitDiRatIdbsdtr0.Len.DTR_COD_TARI));
        }
        // COB_CODE: IF IND-DTR-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-AZ-NULL
            dettTitDiRat.getDtrImpAz().setDtrImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpAz.Len.DTR_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-ADER-NULL
            dettTitDiRat.getDtrImpAder().setDtrImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpAder.Len.DTR_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-TFR-NULL
            dettTitDiRat.getDtrImpTfr().setDtrImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpTfr.Len.DTR_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-VOLO-NULL
            dettTitDiRat.getDtrImpVolo().setDtrImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpVolo.Len.DTR_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-DTR-FL-VLDT-TIT = -1
        //              MOVE HIGH-VALUES TO DTR-FL-VLDT-TIT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getFlVldtTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-FL-VLDT-TIT-NULL
            dettTitDiRat.setDtrFlVldtTit(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DTR-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO DTR-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CAR-ACQ-NULL
            dettTitDiRat.getDtrCarAcq().setDtrCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCarAcq.Len.DTR_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-DTR-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO DTR-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CAR-GEST-NULL
            dettTitDiRat.getDtrCarGest().setDtrCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCarGest.Len.DTR_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-DTR-CAR-INC = -1
        //              MOVE HIGH-VALUES TO DTR-CAR-INC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CAR-INC-NULL
            dettTitDiRat.getDtrCarInc().setDtrCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCarInc.Len.DTR_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-DTR-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO DTR-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-MANFEE-ANTIC-NULL
            dettTitDiRat.getDtrManfeeAntic().setDtrManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrManfeeAntic.Len.DTR_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DTR-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO DTR-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-MANFEE-RICOR-NULL
            dettTitDiRat.getDtrManfeeRicor().setDtrManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrManfeeRicor.Len.DTR_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTR-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO DTR-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-MANFEE-REC-NULL
            dettTitDiRat.setDtrManfeeRec(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitDiRatIdbsdtr0.Len.DTR_MANFEE_REC));
        }
        // COB_CODE: IF IND-DTR-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO DTR-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-TOT-INTR-PREST-NULL
            dettTitDiRat.getDtrTotIntrPrest().setDtrTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrTotIntrPrest.Len.DTR_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-TRASFE-NULL
            dettTitDiRat.getDtrImpTrasfe().setDtrImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpTrasfe.Len.DTR_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-TFR-STRC-NULL
            dettTitDiRat.getDtrImpTfrStrc().setDtrImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpTfrStrc.Len.DTR_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-DTR-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO DTR-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-ACQ-EXP-NULL
            dettTitDiRat.getDtrAcqExp().setDtrAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrAcqExp.Len.DTR_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-DTR-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO DTR-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-REMUN-ASS-NULL
            dettTitDiRat.getDtrRemunAss().setDtrRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrRemunAss.Len.DTR_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-DTR-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO DTR-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-COMMIS-INTER-NULL
            dettTitDiRat.getDtrCommisInter().setDtrCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCommisInter.Len.DTR_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-DTR-CNBT-ANTIRAC = -1
        //              MOVE HIGH-VALUES TO DTR-CNBT-ANTIRAC-NULL
        //           END-IF.
        if (ws.getIndDettTitDiRat().getCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CNBT-ANTIRAC-NULL
            dettTitDiRat.getDtrCnbtAntirac().setDtrCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCnbtAntirac.Len.DTR_CNBT_ANTIRAC_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE DTR-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettTitDiRatDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DTR-DT-INI-EFF
        dettTitDiRat.setDtrDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE DTR-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettTitDiRatDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DTR-DT-END-EFF
        dettTitDiRat.setDtrDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-DTR-DT-INI-COP = 0
        //               MOVE WS-DATE-N      TO DTR-DT-INI-COP
        //           END-IF
        if (ws.getIndDettTitDiRat().getDtIniCop() == 0) {
            // COB_CODE: MOVE DTR-DT-INI-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitDiRatDb().getPervRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTR-DT-INI-COP
            dettTitDiRat.getDtrDtIniCop().setDtrDtIniCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DTR-DT-END-COP = 0
        //               MOVE WS-DATE-N      TO DTR-DT-END-COP
        //           END-IF.
        if (ws.getIndDettTitDiRat().getDtEndCop() == 0) {
            // COB_CODE: MOVE DTR-DT-END-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitDiRatDb().getEsecRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTR-DT-END-COP
            dettTitDiRat.getDtrDtEndCop().setDtrDtEndCop(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getAcqExp() {
        return dettTitDiRat.getDtrAcqExp().getDtrAcqExp();
    }

    @Override
    public void setAcqExp(AfDecimal acqExp) {
        this.dettTitDiRat.getDtrAcqExp().setDtrAcqExp(acqExp.copy());
    }

    @Override
    public AfDecimal getAcqExpObj() {
        if (ws.getIndDettTitDiRat().getAcqExp() >= 0) {
            return getAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAcqExpObj(AfDecimal acqExpObj) {
        if (acqExpObj != null) {
            setAcqExp(new AfDecimal(acqExpObj, 15, 3));
            ws.getIndDettTitDiRat().setAcqExp(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarAcq() {
        return dettTitDiRat.getDtrCarAcq().getDtrCarAcq();
    }

    @Override
    public void setCarAcq(AfDecimal carAcq) {
        this.dettTitDiRat.getDtrCarAcq().setDtrCarAcq(carAcq.copy());
    }

    @Override
    public AfDecimal getCarAcqObj() {
        if (ws.getIndDettTitDiRat().getCarAcq() >= 0) {
            return getCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarAcqObj(AfDecimal carAcqObj) {
        if (carAcqObj != null) {
            setCarAcq(new AfDecimal(carAcqObj, 15, 3));
            ws.getIndDettTitDiRat().setCarAcq(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarGest() {
        return dettTitDiRat.getDtrCarGest().getDtrCarGest();
    }

    @Override
    public void setCarGest(AfDecimal carGest) {
        this.dettTitDiRat.getDtrCarGest().setDtrCarGest(carGest.copy());
    }

    @Override
    public AfDecimal getCarGestObj() {
        if (ws.getIndDettTitDiRat().getCarGest() >= 0) {
            return getCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarGestObj(AfDecimal carGestObj) {
        if (carGestObj != null) {
            setCarGest(new AfDecimal(carGestObj, 15, 3));
            ws.getIndDettTitDiRat().setCarGest(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarIas() {
        return dettTitDiRat.getDtrCarIas().getDtrCarIas();
    }

    @Override
    public void setCarIas(AfDecimal carIas) {
        this.dettTitDiRat.getDtrCarIas().setDtrCarIas(carIas.copy());
    }

    @Override
    public AfDecimal getCarIasObj() {
        if (ws.getIndDettTitDiRat().getCarIas() >= 0) {
            return getCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarIasObj(AfDecimal carIasObj) {
        if (carIasObj != null) {
            setCarIas(new AfDecimal(carIasObj, 15, 3));
            ws.getIndDettTitDiRat().setCarIas(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarInc() {
        return dettTitDiRat.getDtrCarInc().getDtrCarInc();
    }

    @Override
    public void setCarInc(AfDecimal carInc) {
        this.dettTitDiRat.getDtrCarInc().setDtrCarInc(carInc.copy());
    }

    @Override
    public AfDecimal getCarIncObj() {
        if (ws.getIndDettTitDiRat().getCarInc() >= 0) {
            return getCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarIncObj(AfDecimal carIncObj) {
        if (carIncObj != null) {
            setCarInc(new AfDecimal(carIncObj, 15, 3));
            ws.getIndDettTitDiRat().setCarInc(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getCnbtAntirac() {
        return dettTitDiRat.getDtrCnbtAntirac().getDtrCnbtAntirac();
    }

    @Override
    public void setCnbtAntirac(AfDecimal cnbtAntirac) {
        this.dettTitDiRat.getDtrCnbtAntirac().setDtrCnbtAntirac(cnbtAntirac.copy());
    }

    @Override
    public AfDecimal getCnbtAntiracObj() {
        if (ws.getIndDettTitDiRat().getCnbtAntirac() >= 0) {
            return getCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCnbtAntiracObj(AfDecimal cnbtAntiracObj) {
        if (cnbtAntiracObj != null) {
            setCnbtAntirac(new AfDecimal(cnbtAntiracObj, 15, 3));
            ws.getIndDettTitDiRat().setCnbtAntirac(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return dettTitDiRat.getDtrCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dettTitDiRat.setDtrCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return dettTitDiRat.getDtrCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.dettTitDiRat.setDtrCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndDettTitDiRat().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndDettTitDiRat().setCodDvs(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCodDvs(((short)-1));
        }
    }

    @Override
    public String getCodTari() {
        return dettTitDiRat.getDtrCodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.dettTitDiRat.setDtrCodTari(codTari);
    }

    @Override
    public String getCodTariObj() {
        if (ws.getIndDettTitDiRat().getCodTari() >= 0) {
            return getCodTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTariObj(String codTariObj) {
        if (codTariObj != null) {
            setCodTari(codTariObj);
            ws.getIndDettTitDiRat().setCodTari(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getCommisInter() {
        return dettTitDiRat.getDtrCommisInter().getDtrCommisInter();
    }

    @Override
    public void setCommisInter(AfDecimal commisInter) {
        this.dettTitDiRat.getDtrCommisInter().setDtrCommisInter(commisInter.copy());
    }

    @Override
    public AfDecimal getCommisInterObj() {
        if (ws.getIndDettTitDiRat().getCommisInter() >= 0) {
            return getCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommisInterObj(AfDecimal commisInterObj) {
        if (commisInterObj != null) {
            setCommisInter(new AfDecimal(commisInterObj, 15, 3));
            ws.getIndDettTitDiRat().setCommisInter(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getDir() {
        return dettTitDiRat.getDtrDir().getDtrDir();
    }

    @Override
    public void setDir(AfDecimal dir) {
        this.dettTitDiRat.getDtrDir().setDtrDir(dir.copy());
    }

    @Override
    public AfDecimal getDirObj() {
        if (ws.getIndDettTitDiRat().getDir() >= 0) {
            return getDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirObj(AfDecimal dirObj) {
        if (dirObj != null) {
            setDir(new AfDecimal(dirObj, 15, 3));
            ws.getIndDettTitDiRat().setDir(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setDir(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return dettTitDiRat.getDtrDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dettTitDiRat.setDtrDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return dettTitDiRat.getDtrDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dettTitDiRat.setDtrDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return dettTitDiRat.getDtrDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dettTitDiRat.setDtrDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return dettTitDiRat.getDtrDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dettTitDiRat.setDtrDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return dettTitDiRat.getDtrDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dettTitDiRat.setDtrDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dettTitDiRat.getDtrDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dettTitDiRat.setDtrDsVer(dsVer);
    }

    @Override
    public String getDtEndCopDb() {
        return ws.getDettTitDiRatDb().getEsecRichDb();
    }

    @Override
    public void setDtEndCopDb(String dtEndCopDb) {
        this.ws.getDettTitDiRatDb().setEsecRichDb(dtEndCopDb);
    }

    @Override
    public String getDtEndCopDbObj() {
        if (ws.getIndDettTitDiRat().getDtEndCop() >= 0) {
            return getDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndCopDbObj(String dtEndCopDbObj) {
        if (dtEndCopDbObj != null) {
            setDtEndCopDb(dtEndCopDbObj);
            ws.getIndDettTitDiRat().setDtEndCop(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getDettTitDiRatDb().getRgstrzRichDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getDettTitDiRatDb().setRgstrzRichDb(dtEndEffDb);
    }

    @Override
    public String getDtIniCopDb() {
        return ws.getDettTitDiRatDb().getPervRichDb();
    }

    @Override
    public void setDtIniCopDb(String dtIniCopDb) {
        this.ws.getDettTitDiRatDb().setPervRichDb(dtIniCopDb);
    }

    @Override
    public String getDtIniCopDbObj() {
        if (ws.getIndDettTitDiRat().getDtIniCop() >= 0) {
            return getDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniCopDbObj(String dtIniCopDbObj) {
        if (dtIniCopDbObj != null) {
            setDtIniCopDb(dtIniCopDbObj);
            ws.getIndDettTitDiRat().setDtIniCop(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getDettTitDiRatDb().getEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getDettTitDiRatDb().setEffDb(dtIniEffDb);
    }

    @Override
    public long getDtrDsRiga() {
        return dettTitDiRat.getDtrDsRiga();
    }

    @Override
    public void setDtrDsRiga(long dtrDsRiga) {
        this.dettTitDiRat.setDtrDsRiga(dtrDsRiga);
    }

    @Override
    public char getDtrFlVldtTit() {
        return dettTitDiRat.getDtrFlVldtTit();
    }

    @Override
    public void setDtrFlVldtTit(char dtrFlVldtTit) {
        this.dettTitDiRat.setDtrFlVldtTit(dtrFlVldtTit);
    }

    @Override
    public Character getDtrFlVldtTitObj() {
        if (ws.getIndDettTitDiRat().getFlVldtTit() >= 0) {
            return ((Character)getDtrFlVldtTit());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtrFlVldtTitObj(Character dtrFlVldtTitObj) {
        if (dtrFlVldtTitObj != null) {
            setDtrFlVldtTit(((char)dtrFlVldtTitObj));
            ws.getIndDettTitDiRat().setFlVldtTit(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setFlVldtTit(((short)-1));
        }
    }

    @Override
    public int getDtrIdOgg() {
        return dettTitDiRat.getDtrIdOgg();
    }

    @Override
    public void setDtrIdOgg(int dtrIdOgg) {
        this.dettTitDiRat.setDtrIdOgg(dtrIdOgg);
    }

    @Override
    public int getDtrIdTitRat() {
        return dettTitDiRat.getDtrIdTitRat();
    }

    @Override
    public void setDtrIdTitRat(int dtrIdTitRat) {
        this.dettTitDiRat.setDtrIdTitRat(dtrIdTitRat);
    }

    @Override
    public String getDtrTpOgg() {
        return dettTitDiRat.getDtrTpOgg();
    }

    @Override
    public void setDtrTpOgg(String dtrTpOgg) {
        this.dettTitDiRat.setDtrTpOgg(dtrTpOgg);
    }

    @Override
    public int getFrqMovi() {
        return dettTitDiRat.getDtrFrqMovi().getDtrFrqMovi();
    }

    @Override
    public void setFrqMovi(int frqMovi) {
        this.dettTitDiRat.getDtrFrqMovi().setDtrFrqMovi(frqMovi);
    }

    @Override
    public Integer getFrqMoviObj() {
        if (ws.getIndDettTitDiRat().getFrqMovi() >= 0) {
            return ((Integer)getFrqMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrqMoviObj(Integer frqMoviObj) {
        if (frqMoviObj != null) {
            setFrqMovi(((int)frqMoviObj));
            ws.getIndDettTitDiRat().setFrqMovi(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getIdDettTitDiRat() {
        return dettTitDiRat.getDtrIdDettTitDiRat();
    }

    @Override
    public void setIdDettTitDiRat(int idDettTitDiRat) {
        this.dettTitDiRat.setDtrIdDettTitDiRat(idDettTitDiRat);
    }

    @Override
    public int getIdMoviChiu() {
        return dettTitDiRat.getDtrIdMoviChiu().getDtrIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.dettTitDiRat.getDtrIdMoviChiu().setDtrIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndDettTitDiRat().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndDettTitDiRat().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return dettTitDiRat.getDtrIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.dettTitDiRat.setDtrIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        return dettTitDiRat.getDtrImpAder().getDtrImpAder();
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        this.dettTitDiRat.getDtrImpAder().setDtrImpAder(impAder.copy());
    }

    @Override
    public AfDecimal getImpAderObj() {
        if (ws.getIndDettTitDiRat().getImpAder() >= 0) {
            return getImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        if (impAderObj != null) {
            setImpAder(new AfDecimal(impAderObj, 15, 3));
            ws.getIndDettTitDiRat().setImpAder(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAz() {
        return dettTitDiRat.getDtrImpAz().getDtrImpAz();
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        this.dettTitDiRat.getDtrImpAz().setDtrImpAz(impAz.copy());
    }

    @Override
    public AfDecimal getImpAzObj() {
        if (ws.getIndDettTitDiRat().getImpAz() >= 0) {
            return getImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        if (impAzObj != null) {
            setImpAz(new AfDecimal(impAzObj, 15, 3));
            ws.getIndDettTitDiRat().setImpAz(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfr() {
        return dettTitDiRat.getDtrImpTfr().getDtrImpTfr();
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        this.dettTitDiRat.getDtrImpTfr().setDtrImpTfr(impTfr.copy());
    }

    @Override
    public AfDecimal getImpTfrObj() {
        if (ws.getIndDettTitDiRat().getImpTfr() >= 0) {
            return getImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        if (impTfrObj != null) {
            setImpTfr(new AfDecimal(impTfrObj, 15, 3));
            ws.getIndDettTitDiRat().setImpTfr(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        return dettTitDiRat.getDtrImpTfrStrc().getDtrImpTfrStrc();
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        this.dettTitDiRat.getDtrImpTfrStrc().setDtrImpTfrStrc(impTfrStrc.copy());
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        if (ws.getIndDettTitDiRat().getImpTfrStrc() >= 0) {
            return getImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        if (impTfrStrcObj != null) {
            setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
            ws.getIndDettTitDiRat().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasfe() {
        return dettTitDiRat.getDtrImpTrasfe().getDtrImpTrasfe();
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        this.dettTitDiRat.getDtrImpTrasfe().setDtrImpTrasfe(impTrasfe.copy());
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        if (ws.getIndDettTitDiRat().getImpTrasfe() >= 0) {
            return getImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        if (impTrasfeObj != null) {
            setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
            ws.getIndDettTitDiRat().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpVolo() {
        return dettTitDiRat.getDtrImpVolo().getDtrImpVolo();
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        this.dettTitDiRat.getDtrImpVolo().setDtrImpVolo(impVolo.copy());
    }

    @Override
    public AfDecimal getImpVoloObj() {
        if (ws.getIndDettTitDiRat().getImpVolo() >= 0) {
            return getImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        if (impVoloObj != null) {
            setImpVolo(new AfDecimal(impVoloObj, 15, 3));
            ws.getIndDettTitDiRat().setImpVolo(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrFraz() {
        return dettTitDiRat.getDtrIntrFraz().getDtrIntrFraz();
    }

    @Override
    public void setIntrFraz(AfDecimal intrFraz) {
        this.dettTitDiRat.getDtrIntrFraz().setDtrIntrFraz(intrFraz.copy());
    }

    @Override
    public AfDecimal getIntrFrazObj() {
        if (ws.getIndDettTitDiRat().getIntrFraz() >= 0) {
            return getIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrFrazObj(AfDecimal intrFrazObj) {
        if (intrFrazObj != null) {
            setIntrFraz(new AfDecimal(intrFrazObj, 15, 3));
            ws.getIndDettTitDiRat().setIntrFraz(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrMora() {
        return dettTitDiRat.getDtrIntrMora().getDtrIntrMora();
    }

    @Override
    public void setIntrMora(AfDecimal intrMora) {
        this.dettTitDiRat.getDtrIntrMora().setDtrIntrMora(intrMora.copy());
    }

    @Override
    public AfDecimal getIntrMoraObj() {
        if (ws.getIndDettTitDiRat().getIntrMora() >= 0) {
            return getIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrMoraObj(AfDecimal intrMoraObj) {
        if (intrMoraObj != null) {
            setIntrMora(new AfDecimal(intrMoraObj, 15, 3));
            ws.getIndDettTitDiRat().setIntrMora(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrRetdt() {
        return dettTitDiRat.getDtrIntrRetdt().getDtrIntrRetdt();
    }

    @Override
    public void setIntrRetdt(AfDecimal intrRetdt) {
        this.dettTitDiRat.getDtrIntrRetdt().setDtrIntrRetdt(intrRetdt.copy());
    }

    @Override
    public AfDecimal getIntrRetdtObj() {
        if (ws.getIndDettTitDiRat().getIntrRetdt() >= 0) {
            return getIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrRetdtObj(AfDecimal intrRetdtObj) {
        if (intrRetdtObj != null) {
            setIntrRetdt(new AfDecimal(intrRetdtObj, 15, 3));
            ws.getIndDettTitDiRat().setIntrRetdt(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrRiat() {
        return dettTitDiRat.getDtrIntrRiat().getDtrIntrRiat();
    }

    @Override
    public void setIntrRiat(AfDecimal intrRiat) {
        this.dettTitDiRat.getDtrIntrRiat().setDtrIntrRiat(intrRiat.copy());
    }

    @Override
    public AfDecimal getIntrRiatObj() {
        if (ws.getIndDettTitDiRat().getIntrRiat() >= 0) {
            return getIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrRiatObj(AfDecimal intrRiatObj) {
        if (intrRiatObj != null) {
            setIntrRiat(new AfDecimal(intrRiatObj, 15, 3));
            ws.getIndDettTitDiRat().setIntrRiat(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getManfeeAntic() {
        return dettTitDiRat.getDtrManfeeAntic().getDtrManfeeAntic();
    }

    @Override
    public void setManfeeAntic(AfDecimal manfeeAntic) {
        this.dettTitDiRat.getDtrManfeeAntic().setDtrManfeeAntic(manfeeAntic.copy());
    }

    @Override
    public AfDecimal getManfeeAnticObj() {
        if (ws.getIndDettTitDiRat().getManfeeAntic() >= 0) {
            return getManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeAnticObj(AfDecimal manfeeAnticObj) {
        if (manfeeAnticObj != null) {
            setManfeeAntic(new AfDecimal(manfeeAnticObj, 15, 3));
            ws.getIndDettTitDiRat().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public String getManfeeRec() {
        return dettTitDiRat.getDtrManfeeRec();
    }

    @Override
    public void setManfeeRec(String manfeeRec) {
        this.dettTitDiRat.setDtrManfeeRec(manfeeRec);
    }

    @Override
    public String getManfeeRecObj() {
        if (ws.getIndDettTitDiRat().getManfeeRec() >= 0) {
            return getManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeRecObj(String manfeeRecObj) {
        if (manfeeRecObj != null) {
            setManfeeRec(manfeeRecObj);
            ws.getIndDettTitDiRat().setManfeeRec(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getManfeeRicor() {
        return dettTitDiRat.getDtrManfeeRicor().getDtrManfeeRicor();
    }

    @Override
    public void setManfeeRicor(AfDecimal manfeeRicor) {
        this.dettTitDiRat.getDtrManfeeRicor().setDtrManfeeRicor(manfeeRicor.copy());
    }

    @Override
    public AfDecimal getManfeeRicorObj() {
        if (ws.getIndDettTitDiRat().getManfeeRicor() >= 0) {
            return getManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeRicorObj(AfDecimal manfeeRicorObj) {
        if (manfeeRicorObj != null) {
            setManfeeRicor(new AfDecimal(manfeeRicorObj, 15, 3));
            ws.getIndDettTitDiRat().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreNet() {
        return dettTitDiRat.getDtrPreNet().getDtrPreNet();
    }

    @Override
    public void setPreNet(AfDecimal preNet) {
        this.dettTitDiRat.getDtrPreNet().setDtrPreNet(preNet.copy());
    }

    @Override
    public AfDecimal getPreNetObj() {
        if (ws.getIndDettTitDiRat().getPreNet() >= 0) {
            return getPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreNetObj(AfDecimal preNetObj) {
        if (preNetObj != null) {
            setPreNet(new AfDecimal(preNetObj, 15, 3));
            ws.getIndDettTitDiRat().setPreNet(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePpIas() {
        return dettTitDiRat.getDtrPrePpIas().getDtrPrePpIas();
    }

    @Override
    public void setPrePpIas(AfDecimal prePpIas) {
        this.dettTitDiRat.getDtrPrePpIas().setDtrPrePpIas(prePpIas.copy());
    }

    @Override
    public AfDecimal getPrePpIasObj() {
        if (ws.getIndDettTitDiRat().getPrePpIas() >= 0) {
            return getPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePpIasObj(AfDecimal prePpIasObj) {
        if (prePpIasObj != null) {
            setPrePpIas(new AfDecimal(prePpIasObj, 15, 3));
            ws.getIndDettTitDiRat().setPrePpIas(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreSoloRsh() {
        return dettTitDiRat.getDtrPreSoloRsh().getDtrPreSoloRsh();
    }

    @Override
    public void setPreSoloRsh(AfDecimal preSoloRsh) {
        this.dettTitDiRat.getDtrPreSoloRsh().setDtrPreSoloRsh(preSoloRsh.copy());
    }

    @Override
    public AfDecimal getPreSoloRshObj() {
        if (ws.getIndDettTitDiRat().getPreSoloRsh() >= 0) {
            return getPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreSoloRshObj(AfDecimal preSoloRshObj) {
        if (preSoloRshObj != null) {
            setPreSoloRsh(new AfDecimal(preSoloRshObj, 15, 3));
            ws.getIndDettTitDiRat().setPreSoloRsh(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreTot() {
        return dettTitDiRat.getDtrPreTot().getDtrPreTot();
    }

    @Override
    public void setPreTot(AfDecimal preTot) {
        this.dettTitDiRat.getDtrPreTot().setDtrPreTot(preTot.copy());
    }

    @Override
    public AfDecimal getPreTotObj() {
        if (ws.getIndDettTitDiRat().getPreTot() >= 0) {
            return getPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreTotObj(AfDecimal preTotObj) {
        if (preTotObj != null) {
            setPreTot(new AfDecimal(preTotObj, 15, 3));
            ws.getIndDettTitDiRat().setPreTot(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvAcq1aa() {
        return dettTitDiRat.getDtrProvAcq1aa().getDtrProvAcq1aa();
    }

    @Override
    public void setProvAcq1aa(AfDecimal provAcq1aa) {
        this.dettTitDiRat.getDtrProvAcq1aa().setDtrProvAcq1aa(provAcq1aa.copy());
    }

    @Override
    public AfDecimal getProvAcq1aaObj() {
        if (ws.getIndDettTitDiRat().getProvAcq1aa() >= 0) {
            return getProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvAcq1aaObj(AfDecimal provAcq1aaObj) {
        if (provAcq1aaObj != null) {
            setProvAcq1aa(new AfDecimal(provAcq1aaObj, 15, 3));
            ws.getIndDettTitDiRat().setProvAcq1aa(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvAcq2aa() {
        return dettTitDiRat.getDtrProvAcq2aa().getDtrProvAcq2aa();
    }

    @Override
    public void setProvAcq2aa(AfDecimal provAcq2aa) {
        this.dettTitDiRat.getDtrProvAcq2aa().setDtrProvAcq2aa(provAcq2aa.copy());
    }

    @Override
    public AfDecimal getProvAcq2aaObj() {
        if (ws.getIndDettTitDiRat().getProvAcq2aa() >= 0) {
            return getProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvAcq2aaObj(AfDecimal provAcq2aaObj) {
        if (provAcq2aaObj != null) {
            setProvAcq2aa(new AfDecimal(provAcq2aaObj, 15, 3));
            ws.getIndDettTitDiRat().setProvAcq2aa(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvDaRec() {
        return dettTitDiRat.getDtrProvDaRec().getDtrProvDaRec();
    }

    @Override
    public void setProvDaRec(AfDecimal provDaRec) {
        this.dettTitDiRat.getDtrProvDaRec().setDtrProvDaRec(provDaRec.copy());
    }

    @Override
    public AfDecimal getProvDaRecObj() {
        if (ws.getIndDettTitDiRat().getProvDaRec() >= 0) {
            return getProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvDaRecObj(AfDecimal provDaRecObj) {
        if (provDaRecObj != null) {
            setProvDaRec(new AfDecimal(provDaRecObj, 15, 3));
            ws.getIndDettTitDiRat().setProvDaRec(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvInc() {
        return dettTitDiRat.getDtrProvInc().getDtrProvInc();
    }

    @Override
    public void setProvInc(AfDecimal provInc) {
        this.dettTitDiRat.getDtrProvInc().setDtrProvInc(provInc.copy());
    }

    @Override
    public AfDecimal getProvIncObj() {
        if (ws.getIndDettTitDiRat().getProvInc() >= 0) {
            return getProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvIncObj(AfDecimal provIncObj) {
        if (provIncObj != null) {
            setProvInc(new AfDecimal(provIncObj, 15, 3));
            ws.getIndDettTitDiRat().setProvInc(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvRicor() {
        return dettTitDiRat.getDtrProvRicor().getDtrProvRicor();
    }

    @Override
    public void setProvRicor(AfDecimal provRicor) {
        this.dettTitDiRat.getDtrProvRicor().setDtrProvRicor(provRicor.copy());
    }

    @Override
    public AfDecimal getProvRicorObj() {
        if (ws.getIndDettTitDiRat().getProvRicor() >= 0) {
            return getProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvRicorObj(AfDecimal provRicorObj) {
        if (provRicorObj != null) {
            setProvRicor(new AfDecimal(provRicorObj, 15, 3));
            ws.getIndDettTitDiRat().setProvRicor(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getRemunAss() {
        return dettTitDiRat.getDtrRemunAss().getDtrRemunAss();
    }

    @Override
    public void setRemunAss(AfDecimal remunAss) {
        this.dettTitDiRat.getDtrRemunAss().setDtrRemunAss(remunAss.copy());
    }

    @Override
    public AfDecimal getRemunAssObj() {
        if (ws.getIndDettTitDiRat().getRemunAss() >= 0) {
            return getRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRemunAssObj(AfDecimal remunAssObj) {
        if (remunAssObj != null) {
            setRemunAss(new AfDecimal(remunAssObj, 15, 3));
            ws.getIndDettTitDiRat().setRemunAss(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprAlt() {
        return dettTitDiRat.getDtrSoprAlt().getDtrSoprAlt();
    }

    @Override
    public void setSoprAlt(AfDecimal soprAlt) {
        this.dettTitDiRat.getDtrSoprAlt().setDtrSoprAlt(soprAlt.copy());
    }

    @Override
    public AfDecimal getSoprAltObj() {
        if (ws.getIndDettTitDiRat().getSoprAlt() >= 0) {
            return getSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprAltObj(AfDecimal soprAltObj) {
        if (soprAltObj != null) {
            setSoprAlt(new AfDecimal(soprAltObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprAlt(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprProf() {
        return dettTitDiRat.getDtrSoprProf().getDtrSoprProf();
    }

    @Override
    public void setSoprProf(AfDecimal soprProf) {
        this.dettTitDiRat.getDtrSoprProf().setDtrSoprProf(soprProf.copy());
    }

    @Override
    public AfDecimal getSoprProfObj() {
        if (ws.getIndDettTitDiRat().getSoprProf() >= 0) {
            return getSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprProfObj(AfDecimal soprProfObj) {
        if (soprProfObj != null) {
            setSoprProf(new AfDecimal(soprProfObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprProf(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprSan() {
        return dettTitDiRat.getDtrSoprSan().getDtrSoprSan();
    }

    @Override
    public void setSoprSan(AfDecimal soprSan) {
        this.dettTitDiRat.getDtrSoprSan().setDtrSoprSan(soprSan.copy());
    }

    @Override
    public AfDecimal getSoprSanObj() {
        if (ws.getIndDettTitDiRat().getSoprSan() >= 0) {
            return getSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprSanObj(AfDecimal soprSanObj) {
        if (soprSanObj != null) {
            setSoprSan(new AfDecimal(soprSanObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprSan(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprSpo() {
        return dettTitDiRat.getDtrSoprSpo().getDtrSoprSpo();
    }

    @Override
    public void setSoprSpo(AfDecimal soprSpo) {
        this.dettTitDiRat.getDtrSoprSpo().setDtrSoprSpo(soprSpo.copy());
    }

    @Override
    public AfDecimal getSoprSpoObj() {
        if (ws.getIndDettTitDiRat().getSoprSpo() >= 0) {
            return getSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprSpoObj(AfDecimal soprSpoObj) {
        if (soprSpoObj != null) {
            setSoprSpo(new AfDecimal(soprSpoObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprSpo(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprTec() {
        return dettTitDiRat.getDtrSoprTec().getDtrSoprTec();
    }

    @Override
    public void setSoprTec(AfDecimal soprTec) {
        this.dettTitDiRat.getDtrSoprTec().setDtrSoprTec(soprTec.copy());
    }

    @Override
    public AfDecimal getSoprTecObj() {
        if (ws.getIndDettTitDiRat().getSoprTec() >= 0) {
            return getSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprTecObj(AfDecimal soprTecObj) {
        if (soprTecObj != null) {
            setSoprTec(new AfDecimal(soprTecObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprTec(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpeAge() {
        return dettTitDiRat.getDtrSpeAge().getDtrSpeAge();
    }

    @Override
    public void setSpeAge(AfDecimal speAge) {
        this.dettTitDiRat.getDtrSpeAge().setDtrSpeAge(speAge.copy());
    }

    @Override
    public AfDecimal getSpeAgeObj() {
        if (ws.getIndDettTitDiRat().getSpeAge() >= 0) {
            return getSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeAgeObj(AfDecimal speAgeObj) {
        if (speAgeObj != null) {
            setSpeAge(new AfDecimal(speAgeObj, 15, 3));
            ws.getIndDettTitDiRat().setSpeAge(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpeMed() {
        return dettTitDiRat.getDtrSpeMed().getDtrSpeMed();
    }

    @Override
    public void setSpeMed(AfDecimal speMed) {
        this.dettTitDiRat.getDtrSpeMed().setDtrSpeMed(speMed.copy());
    }

    @Override
    public AfDecimal getSpeMedObj() {
        if (ws.getIndDettTitDiRat().getSpeMed() >= 0) {
            return getSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeMedObj(AfDecimal speMedObj) {
        if (speMedObj != null) {
            setSpeMed(new AfDecimal(speMedObj, 15, 3));
            ws.getIndDettTitDiRat().setSpeMed(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getTax() {
        return dettTitDiRat.getDtrTax().getDtrTax();
    }

    @Override
    public void setTax(AfDecimal tax) {
        this.dettTitDiRat.getDtrTax().setDtrTax(tax.copy());
    }

    @Override
    public AfDecimal getTaxObj() {
        if (ws.getIndDettTitDiRat().getTax() >= 0) {
            return getTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTaxObj(AfDecimal taxObj) {
        if (taxObj != null) {
            setTax(new AfDecimal(taxObj, 15, 3));
            ws.getIndDettTitDiRat().setTax(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setTax(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrPrest() {
        return dettTitDiRat.getDtrTotIntrPrest().getDtrTotIntrPrest();
    }

    @Override
    public void setTotIntrPrest(AfDecimal totIntrPrest) {
        this.dettTitDiRat.getDtrTotIntrPrest().setDtrTotIntrPrest(totIntrPrest.copy());
    }

    @Override
    public AfDecimal getTotIntrPrestObj() {
        if (ws.getIndDettTitDiRat().getTotIntrPrest() >= 0) {
            return getTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrPrestObj(AfDecimal totIntrPrestObj) {
        if (totIntrPrestObj != null) {
            setTotIntrPrest(new AfDecimal(totIntrPrestObj, 15, 3));
            ws.getIndDettTitDiRat().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public String getTpRgmFisc() {
        return dettTitDiRat.getDtrTpRgmFisc();
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        this.dettTitDiRat.setDtrTpRgmFisc(tpRgmFisc);
    }

    @Override
    public String getTpStatTit() {
        return dettTitDiRat.getDtrTpStatTit();
    }

    @Override
    public void setTpStatTit(String tpStatTit) {
        this.dettTitDiRat.setDtrTpStatTit(tpStatTit);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
