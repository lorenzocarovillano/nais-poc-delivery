package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs1890Data;

/**Original name: LVVS1890<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2011.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 * **         CALCOLO DELLA VARIABILE FR - Frazion. Rendita      ***
 * **------------------------------------------------------------***</pre>*/
public class Lvvs1890 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs1890Data ws = new Lvvs1890Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS1450
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS1890_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs1890 getInstance() {
        return ((Lvvs1890)Programs.getInstance(Lvvs1890.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE AREA-IO-GAR
        //                      AREA-IO-PMO.
        initAreaIoGar();
        initAreaIoPmo();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG
        //             TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES TO IVVC0213-VAL-IMP-O.
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //             END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CERCA-FRAZ-GAR
            //              THRU S1200-EX
            s1200CercaFrazGar();
            // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
            //           AND IDSV0003-SUCCESSFUL-SQL
            //           AND WK-FRZ-GAR-TROVATO
            //                 THRU S1300-EX
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql() && ws.getWkIdCodLivFlagGrz().isTrovato()) {
                // COB_CODE: PERFORM S1300-CERCA-PARAM-MOVI
                //              THRU S1300-EX
                s1300CercaParamMovi();
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-GARANZIA
        //                TO DGRZ-AREA-GRA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasGaranzia())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DGRZ-AREA-GRA
            ws.setDgrzAreaGraFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-PARAM-MOV
        //                TO DPMO-AREA-PMO
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasParamMov())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPMO-AREA-PMO
            ws.setDpmoAreaPmoFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CERCA-FRAZ-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200CercaFrazGar() {
        // COB_CODE: SET WK-FRZ-GAR-NON-TROVATO              TO TRUE.
        ws.getWkIdCodLivFlagGrz().setNonTrovato();
        //
        // COB_CODE: IF DGRZ-ELE-GAR-MAX > 0
        //              END-IF
        //           END-IF.
        if (ws.getDgrzEleGarMax() > 0) {
            // COB_CODE: IF DGRZ-FRAZ-INI-EROG-REN(IVVC0213-IX-TABB) IS NUMERIC
            //              SET WK-FRZ-GAR-TROVATO    TO TRUE
            //           END-IF
            if (Functions.isNumber(ws.getDgrzTabGar(ivvc0213.getIxTabb()).getLccvgrz1().getDati().getWgrzFrazIniErogRen().getWgrzFrazIniErogRen())) {
                // COB_CODE: MOVE DGRZ-FRAZ-INI-EROG-REN(IVVC0213-IX-TABB)
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getDgrzTabGar(ivvc0213.getIxTabb()).getLccvgrz1().getDati().getWgrzFrazIniErogRen().getWgrzFrazIniErogRen(), 18, 7));
                // COB_CODE: SET WK-FRZ-GAR-TROVATO    TO TRUE
                ws.getWkIdCodLivFlagGrz().setTrovato();
            }
        }
    }

    /**Original name: S1300-CERCA-PARAM-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300CercaParamMovi() {
        // COB_CODE: IF DPMO-ELE-PMO-MAX > 0
        //              END-PERFORM
        //           END-IF.
        if (ws.getDpmoElePmoMax() > 0) {
            // COB_CODE: PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
            //             UNTIL IX-TAB-PMO > DPMO-ELE-PMO-MAX
            //              END-IF
            //           END-PERFORM
            ws.getIxIndici().setGuidaGrz(((short)1));
            while (!(ws.getIxIndici().getGuidaGrz() > ws.getDpmoElePmoMax())) {
                // COB_CODE: IF   DPMO-TP-MOVI(IX-TAB-PMO) IS NUMERIC
                //            AND DPMO-TP-MOVI(IX-TAB-PMO) = WK-PMO-SCADENZA
                //            AND DPMO-FRQ-MOVI(IX-TAB-PMO) IS NUMERIC
                //                   TO IVVC0213-VAL-IMP-O
                //           END-IF
                if (Functions.isNumber(ws.getDpmoTabParamMov(ws.getIxIndici().getGuidaGrz()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi()) && ws.getDpmoTabParamMov(ws.getIxIndici().getGuidaGrz()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi() == ws.getWkPmoScadenza() && Functions.isNumber(ws.getDpmoTabParamMov(ws.getIxIndici().getGuidaGrz()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMovi())) {
                    // COB_CODE: MOVE DPMO-FRQ-MOVI(IX-TAB-PMO)
                    //             TO IVVC0213-VAL-IMP-O
                    ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getDpmoTabParamMov(ws.getIxIndici().getGuidaGrz()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMovi(), 18, 7));
                }
                ws.getIxIndici().setGuidaGrz(Trunc.toShort(ws.getIxIndici().getGuidaGrz() + 1, 4));
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.getIxIndici().setDclgen(((short)0));
        ws.getIxIndici().setTabGrz(((short)0));
        ws.getIxIndici().setTabL19(((short)0));
        ws.getIxIndici().setTabTga(((short)0));
        ws.getIxIndici().setTabVas(((short)0));
        ws.getIxIndici().setGuidaGrz(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoGar() {
        ws.setDgrzEleGarMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs1890Data.DGRZ_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getDgrzTabGar(idx0).getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().setIdPtf(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdGar(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdPoli(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtIniEff(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtEndEff(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIbOgg("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodSez("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTari("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRamoBila("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRsh("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzModPagGarcol("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPerPre("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDur("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodFnd("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzAaRenCer("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPcRip("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpIas("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpStab("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTratRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCessRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsRiga(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsVer(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsUtente("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRamoBila("");
        }
    }

    public void initAreaIoPmo() {
        ws.setDpmoElePmoMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs1890Data.DPMO_TAB_PARAM_MOV_MAXOCCURS; idx0++) {
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().setIdPtf(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdParamMovi(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdOgg(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOgg("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdMoviCrz(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdMoviChiu().setWpmoIdMoviChiu(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtIniEff(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDtEndEff(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodCompAnia(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTpMovi().setWpmoTpMovi(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoFrqMovi().setWpmoFrqMovi(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurAa().setWpmoDurAa(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurMm().setWpmoDurMm(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDurGg().setWpmoDurGg(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorPrec().setWpmoDtRicorPrec(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtRicorSucc().setWpmoDtRicorSucc(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcIntrFraz().setWpmoPcIntrFraz(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaScoTot().setWpmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpBnsDaSco().setWpmoImpBnsDaSco(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcAnticBns().setWpmoPcAnticBns(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRinnColl("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPre("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpRivalPrstz("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlEvidRival(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoUltPcPerd().setWpmoUltPcPerd(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoTotAaGiaPror().setWpmoTotAaGiaPror(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOpz("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoAaRenCer().setWpmoAaRenCer(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcRevrsb().setWpmoPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRiscParzPrgt().setWpmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpLrdDiRat().setWpmoImpLrdDiRat(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIbOgg("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoCosOner().setWpmoCosOner(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSpePc().setWpmoSpePc(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoFlAttivGar(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCambioVerProd(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoMmDiff().setWpmoMmDiff(((short)0));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoImpRatManfee().setWpmoImpRatManfee(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoDtUltErogManfee().setWpmoDtUltErogManfee(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpOggRival("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoSomAsstaGarac().setWpmoSomAsstaGarac(new AfDecimal(0, 15, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcApplzOpz().setWpmoPcApplzOpz(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoIdAdes().setWpmoIdAdes(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoIdPoli(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpFrmAssva("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsRiga(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsOperSql(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsVer(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsIniCptz(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsTsEndCptz(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsUtente("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoDsStatoElab(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoTpEstrCnt("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodRamo("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoGenDaSin(Types.SPACE_CHAR);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().setWpmoCodTari("");
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoNumRatPagPre().setWpmoNumRatPagPre(0);
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoPcServVal().setWpmoPcServVal(new AfDecimal(0, 6, 3));
            ws.getDpmoTabParamMov(idx0).getLccvpmo1().getDati().getWpmoEtaAaSoglBnficr().setWpmoEtaAaSoglBnficr(((short)0));
        }
    }
}
