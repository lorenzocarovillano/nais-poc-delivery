package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lccs0062Data;
import it.accenture.jnais.ws.WcomAreaPagina;

/**Original name: LCCS0062<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
 * *                                                        ********
 * *****************************************************************
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       02/2008.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... LCCS0062
 *     TIPOLOGIA...... SERVIZIO TRASVERSALE
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... CALCOLO RATE DI POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lccs0062 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0062Data ws = new Lccs0062Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: WCOM-AREA-PAGINA
    private WcomAreaPagina wcomAreaPagina;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0062_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, WcomAreaPagina wcomAreaPagina) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.wcomAreaPagina = wcomAreaPagina;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0062 getInstance() {
        return ((Lccs0062)Programs.getInstance(Lccs0062.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONI INIZIALI                                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT
        //              THRU EX-S0005.
        s0005CtrlDatiInput();
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void s0005CtrlDatiInput() {
        // COB_CODE: INITIALIZE WK-VARIABILI
        //                      WK-INDICI.
        initWkVariabili();
        initWkIndici();
        // COB_CODE: IF  LCCC0062-DT-ULT-QTZO IS NUMERIC
        //           AND LCCC0062-DT-ULT-QTZO > ZEROES
        //               MOVE LCCC0062-DT-ULT-QTZO      TO WK-DATA-ULT-PCO
        //           END-IF
        if (Functions.isNumber(wcomAreaPagina.getDtUltQtzo()) && wcomAreaPagina.getDtUltQtzo() > 0) {
            // COB_CODE: MOVE LCCC0062-DT-ULT-QTZO      TO WK-DATA-ULT-PCO
            ws.getWkVariabili().setDataUltPco(wcomAreaPagina.getDtUltQtzo());
        }
        // COB_CODE: IF  LCCC0062-DT-ULTGZ-TRCH IS NUMERIC
        //           AND LCCC0062-DT-ULTGZ-TRCH > ZEROES
        //               MOVE LCCC0062-DT-ULTGZ-TRCH    TO WK-DATA-ULT-PCO
        //           END-IF.
        if (Functions.isNumber(wcomAreaPagina.getDtUltgzTrch()) && wcomAreaPagina.getDtUltgzTrch() > 0) {
            // COB_CODE: MOVE LCCC0062-DT-ULTGZ-TRCH    TO WK-DATA-ULT-PCO
            ws.getWkVariabili().setDataUltPco(wcomAreaPagina.getDtUltgzTrch());
        }
        // COB_CODE: IF LCCC0062-FRAZIONAMENTO IS NUMERIC
        //           AND LCCC0062-FRAZIONAMENTO > ZEROES
        //              CONTINUE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (Functions.isNumber(wcomAreaPagina.getFrazionamentoFormatted()) && Characters.GT_ZERO.test(wcomAreaPagina.getFrazionamentoFormatted())) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
            // COB_CODE: MOVE '005018'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005018");
            // COB_CODE: MOVE 'CODICE FRAZIONAMENTO'  TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("CODICE FRAZIONAMENTO");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *   ELABORAZIONE                                                  *
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: IF LCCC0062-DT-DECORRENZA > WK-DATA-ULT-PCO
        //               MOVE ZEROES   TO LCCC0062-TOT-NUM-RATE
        //           ELSE
        //                 THRU CALCOLA-NUM-RATE-EX
        //           END-IF.
        if (wcomAreaPagina.getDtDecorrenza() > ws.getWkVariabili().getDataUltPco()) {
            // COB_CODE: MOVE ZEROES   TO LCCC0062-TOT-NUM-RATE
            wcomAreaPagina.setTotNumRate(0);
        }
        else {
            // COB_CODE: PERFORM CALCOLA-NUM-RATE
            //              THRU CALCOLA-NUM-RATE-EX
            calcolaNumRate();
        }
    }

    /**Original name: CALCOLA-NUM-RATE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES PER IL CALCOLO SULLE DATE
	 * ----------------------------------------------------------------*</pre>*/
    private void calcolaNumRate() {
        // COB_CODE: SET  FINE-CICLO-NO            TO TRUE
        ws.setWkCiclo(false);
        // COB_CODE:      PERFORM UNTIL FINE-CICLO-SI
        //                           OR IDSV0001-ESITO-KO
        //           *--     Calcola il limite inferiore dell'intervallo di rata
        //                   END-IF
        //                END-PERFORM.
        while (!(ws.isWkCiclo() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //--     Calcola il limite inferiore dell'intervallo di rata
            // COB_CODE: PERFORM CALCOLA-DT-INF
            //              THRU CALCOLA-DT-INF-EX
            calcolaDtInf();
            //--     Calcola il limite superiore dell'intervallo di rata
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU CALCOLA-DT-SUP-EX
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM CALCOLA-DT-SUP
                //              THRU CALCOLA-DT-SUP-EX
                calcolaDtSup();
            }
            // COB_CODE: IF  IDSV0001-ESITO-OK
            //           AND WK-DT-INFERIORE >  WK-DATA-ULT-PCO
            //              SET FINE-CICLO-SI    TO TRUE
            //           ELSE
            //              END-IF
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.getWkVariabili().getDtInferiore() > ws.getWkVariabili().getDataUltPco()) {
                // COB_CODE: SET FINE-CICLO-SI    TO TRUE
                ws.setWkCiclo(true);
            }
            else if (wcomAreaPagina.getTotNumRate() < ws.getWkEleMaxRate()) {
                // COB_CODE: IF LCCC0062-TOT-NUM-RATE < WK-ELE-MAX-RATE
                //              ADD 1               TO WK-COUNT-RATE
                //           ELSE
                //              SET FINE-CICLO-SI    TO TRUE
                //           END-IF
                // COB_CODE: ADD 1               TO IX-TAB-RATE
                ws.setIxTabRate(Trunc.toInt(1 + ws.getIxTabRate(), 4));
                // COB_CODE: MOVE WK-DT-INFERIORE
                //             TO LCCC0062-DT-INF(IX-TAB-RATE)
                wcomAreaPagina.getTabRate(ws.getIxTabRate()).setDtInf(ws.getWkVariabili().getDtInferiore());
                // COB_CODE: MOVE WK-DT-SUPERIORE
                //             TO LCCC0062-DT-SUP(IX-TAB-RATE)
                wcomAreaPagina.getTabRate(ws.getIxTabRate()).setDtSup(ws.getWkVariabili().getDtSuperiore());
                // COB_CODE: ADD 1               TO LCCC0062-TOT-NUM-RATE
                wcomAreaPagina.setTotNumRate(Trunc.toLong(1 + wcomAreaPagina.getTotNumRate(), 14));
                // COB_CODE: ADD 1               TO WK-COUNT-RATE
                ws.getWkVariabili().setCountRate(Trunc.toInt(1 + ws.getWkVariabili().getCountRate(), 4));
            }
            else {
                // COB_CODE: SET FINE-CICLO-SI    TO TRUE
                ws.setWkCiclo(true);
            }
        }
    }

    /**Original name: CALCOLA-DT-INF<br>
	 * <pre>----------------------------------------------------------------*
	 *     Calcola la data inferiore dell'intervallo di rata della
	 *     polizza
	 * ----------------------------------------------------------------*</pre>*/
    private void calcolaDtInf() {
        // COB_CODE: MOVE ZERO TO WK-DT-INFERIORE.
        ws.getWkVariabili().setDtInferiore(0);
        // COB_CODE: COMPUTE WK-MESI = (LCCC0062-FRAZIONAMENTO * WK-COUNT-RATE).
        ws.getWkVariabili().setMesi(Trunc.toInt(wcomAreaPagina.getFrazionamento() * ws.getWkVariabili().getCountRate(), 4));
        // COB_CODE: IF WK-MESI > ZERO
        //              END-IF
        //           ELSE
        //              MOVE LCCC0062-DT-DECORRENZA  TO WK-DT-INFERIORE
        //           END-IF.
        if (ws.getWkVariabili().getMesi() > 0) {
            // COB_CODE: MOVE 'M'                     TO A2K-TDELTA
            ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("M");
            // COB_CODE: MOVE WK-MESI                 TO A2K-DELTA
            ws.getIoA2kLccc0003().getInput().setA2kDelta(Trunc.toShort(ws.getWkVariabili().getMesi(), 3));
            // COB_CODE: MOVE LCCC0062-DT-DECORRENZA  TO A2K-INDATA
            ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(wcomAreaPagina.getDtDecorrenzaFormatted());
            // COB_CODE: MOVE '02'                    TO A2K-FUNZ
            ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
            // COB_CODE: MOVE '03'                    TO A2K-INFO
            ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
            // COB_CODE: MOVE '0'                     TO A2K-FISLAV
            ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
            // COB_CODE: MOVE '0'                     TO A2K-INICON
            ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
            // COB_CODE: PERFORM CALL-LCCS0003
            //              THRU CALL-LCCS0003-EX
            callLccs0003();
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              MOVE A2K-OUAMG            TO WK-DT-INFERIORE
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: MOVE A2K-OUAMG            TO WK-DT-INFERIORE
                ws.getWkVariabili().setDtInferiore(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamg());
            }
        }
        else {
            // COB_CODE: MOVE LCCC0062-DT-DECORRENZA  TO WK-DT-INFERIORE
            ws.getWkVariabili().setDtInferiore(wcomAreaPagina.getDtDecorrenza());
        }
    }

    /**Original name: CALCOLA-DT-SUP<br>
	 * <pre>----------------------------------------------------------------*
	 *     Calcola la data inferiore dell'intervallo di rata della
	 *     polizza
	 * ----------------------------------------------------------------*</pre>*/
    private void calcolaDtSup() {
        // COB_CODE: MOVE ZERO                    TO WK-DT-SUPERIORE
        ws.getWkVariabili().setDtSuperiore(0);
        // COB_CODE: MOVE 'M'                     TO A2K-TDELTA
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("M");
        // COB_CODE: MOVE LCCC0062-FRAZIONAMENTO  TO A2K-DELTA
        ws.getIoA2kLccc0003().getInput().setA2kDeltaFormatted(wcomAreaPagina.getFrazionamentoFormatted());
        // COB_CODE: MOVE WK-DT-INFERIORE         TO A2K-INDATA
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(ws.getWkVariabili().getDtInferioreFormatted());
        // COB_CODE: MOVE '02'                    TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        // COB_CODE: MOVE '03'                    TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        // COB_CODE: MOVE '0'                     TO A2K-FISLAV
        //                                           A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        // COB_CODE: PERFORM CALL-LCCS0003
        //              THRU CALL-LCCS0003-EX
        callLccs0003();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE A2K-OUAMG               TO WK-DT-SUPERIORE
            ws.getWkVariabili().setDtSuperiore(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamg());
            // COB_CODE: MOVE SPACES                  TO A2K-TDELTA
            ws.getIoA2kLccc0003().getInput().setA2kTdelta(Types.SPACE_CHAR);
            // COB_CODE: MOVE 1                       TO A2K-DELTA
            ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
            // COB_CODE: MOVE WK-DT-SUPERIORE         TO A2K-INDATA
            ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(ws.getWkVariabili().getDtSuperioreFormatted());
            // COB_CODE: MOVE '03'                    TO A2K-FUNZ
            ws.getIoA2kLccc0003().getInput().setA2kFunz("03");
            // COB_CODE: MOVE '03'                    TO A2K-INFO
            ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
            // COB_CODE: MOVE '0'                     TO A2K-FISLAV
            ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
            // COB_CODE: MOVE '0'                     TO A2K-INICON
            ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
            // COB_CODE: PERFORM CALL-LCCS0003
            //              THRU CALL-LCCS0003-EX
            callLccs0003();
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              MOVE A2K-OUAMG               TO WK-DT-SUPERIORE
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: MOVE A2K-OUAMG               TO WK-DT-SUPERIORE
                ws.getWkVariabili().setDtSuperiore(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamg());
            }
        }
    }

    /**Original name: CALL-LCCS0003<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES PER IL CALCOLO SULLE DATE
	 * ----------------------------------------------------------------*</pre>*/
    private void callLccs0003() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        // COB_CODE: CALL LCCS0003 USING IO-A2K-LCCC0003 IN-RCODE
        //           ON EXCEPTION
        //                  THRU EX-S0290
        //           END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Lccs0062Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO OPERAZIONI SULLE DATE'
            //                                          TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO OPERAZIONI SULLE DATE");
            // COB_CODE: MOVE 'CALL-LCCS0003'          TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-LCCS0003");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IN-RCODE  = '00'
        //              CONTINUE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getInRcodeFormatted().equals("00")) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALL-LCCS0003'        TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-LCCS0003");
            // COB_CODE: MOVE '005016'               TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES GESTIONE ERRORE
	 * ----------------------------------------------------------------*
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    public void initWkVariabili() {
        ws.getWkVariabili().setDtInferiore(0);
        ws.getWkVariabili().setDtSuperiore(0);
        ws.getWkVariabili().setDataUltPco(0);
        ws.getWkVariabili().setCountRate(0);
        ws.getWkVariabili().setMesi(0);
    }

    public void initWkIndici() {
        ws.setIxTabRate(0);
    }
}
