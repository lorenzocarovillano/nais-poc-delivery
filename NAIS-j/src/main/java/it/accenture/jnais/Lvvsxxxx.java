package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.LvvsxxxxData;

/**Original name: LVVSXXXX<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2012.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVSXXXX
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CALCOLO VARIABILE IMPPRESTNR
 * **------------------------------------------------------------***</pre>*/
public class Lvvsxxxx extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private LvvsxxxxData ws = new LvvsxxxxData();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS1000
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVSXXXX_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvsxxxx getInstance() {
        return ((Lvvsxxxx)Programs.getInstance(Lvvsxxxx.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             WS-SOM-IMP-PRE.
        initIxIndici();
        initTabOutput();
        ws.setWsSomImpPre(new AfDecimal(0, 15, 3));
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformVaryingStmtImpl @source=LVVS1980.cbl:line=110, because the code is unreachable.
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                 THRU S1200-EX
        //           END-IF
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CALCOLO-VAR
            //              THRU S1200-EX
            s1200CalcoloVar();
        }
        // COB_CODE: MOVE WS-SOM-IMP-PRE
        //             TO IVVC0213-VAL-IMP-O.
        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWsSomImpPre(), 18, 7));
    }

    /**Original name: S1200-CALCOLO-VAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     SOMMATORIA IMP-PRE TABELLA PRESTITI
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200CalcoloVar() {
        Idbspre0 idbspre0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE PREST
        initPrest();
        // COB_CODE: SET IDSV0003-ID-OGGETTO             TO TRUE
        idsv0003.getLivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: SET IDSV0003-FETCH-FIRST            TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC          TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL         TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE  TO PRE-ID-OGG
        ws.getPrest().setPreIdOgg(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE 'AD'                  TO PRE-TP-OGG
        ws.getPrest().setPreTpOgg("AD");
        // COB_CODE:      PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                           OR NOT IDSV0003-SUCCESSFUL-SQL
        //           *
        //                   END-EVALUATE
        //           *
        //                END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql())) {
            //
            // COB_CODE: CALL IDBSPRE0  USING  IDSV0003 PREST
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                idbspre0 = Idbspre0.getInstance();
                idbspre0.run(idsv0003, ws.getPrest());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE IDBSPRE0
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getIdbspre0());
                // COB_CODE: MOVE 'ERRORE CALL IDBSPRE0'
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL IDBSPRE0");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSV0003-NOT-FOUND
            //           *-->          CHIAVE NON TROVATA
            //                           CONTINUE
            //                      WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
            //                         SET IDSV0003-FETCH-NEXT TO TRUE
            //           *
            //                      WHEN OTHER
            //                         END-STRING
            //                   END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.NOT_FOUND://-->          CHIAVE NON TROVATA
                // COB_CODE: CONTINUE
                //continue
                    break;

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: IF PRE-IMP-PREST-NULL = HIGH-VALUE
                    //                          OR LOW-VALUE OR SPACES
                    //              CONTINUE
                    //           ELSE
                    //                     PRE-IMP-PREST
                    //            END-IF
                    if (Characters.EQ_HIGH.test(ws.getPrest().getPreImpPrest().getPreImpPrestNullFormatted()) || Characters.EQ_LOW.test(ws.getPrest().getPreImpPrest().getPreImpPrestNullFormatted()) || Characters.EQ_SPACE.test(ws.getPrest().getPreImpPrest().getPreImpPrestNull())) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: COMPUTE WS-SOM-IMP-PRE =
                        //                   WS-SOM-IMP-PRE +
                        //                   PRE-IMP-PREST
                        ws.setWsSomImpPre(Trunc.toDecimal(ws.getWsSomImpPre().add(ws.getPrest().getPreImpPrest().getPreImpPrest()), 15, 3));
                    }
                    // COB_CODE: SET IDSV0003-FETCH-NEXT TO TRUE
                    idsv0003.getOperazione().setFetchNext();
                    //
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'ERRORE LETTURA TABELLA PRESTITI ;'
                    //                 IDSV0003-RETURN-CODE ';'
                    //                 IDSV0003-SQLCODE
                    //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA PRESTITI ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
            //
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initPrest() {
        ws.getPrest().setPreIdPrest(0);
        ws.getPrest().setPreIdOgg(0);
        ws.getPrest().setPreTpOgg("");
        ws.getPrest().setPreIdMoviCrz(0);
        ws.getPrest().getPreIdMoviChiu().setPreIdMoviChiu(0);
        ws.getPrest().setPreDtIniEff(0);
        ws.getPrest().setPreDtEndEff(0);
        ws.getPrest().setPreCodCompAnia(0);
        ws.getPrest().getPreDtConcsPrest().setPreDtConcsPrest(0);
        ws.getPrest().getPreDtDecorPrest().setPreDtDecorPrest(0);
        ws.getPrest().getPreImpPrest().setPreImpPrest(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreIntrPrest().setPreIntrPrest(new AfDecimal(0, 6, 3));
        ws.getPrest().setPreTpPrest("");
        ws.getPrest().getPreFrazPagIntr().setPreFrazPagIntr(0);
        ws.getPrest().getPreDtRimb().setPreDtRimb(0);
        ws.getPrest().getPreImpRimb().setPreImpRimb(new AfDecimal(0, 15, 3));
        ws.getPrest().setPreCodDvs("");
        ws.getPrest().setPreDtRichPrest(0);
        ws.getPrest().setPreModIntrPrest("");
        ws.getPrest().getPreSpePrest().setPreSpePrest(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreImpPrestLiqto().setPreImpPrestLiqto(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreSdoIntr().setPreSdoIntr(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreRimbEff().setPreRimbEff(new AfDecimal(0, 15, 3));
        ws.getPrest().getPrePrestResEff().setPrePrestResEff(new AfDecimal(0, 15, 3));
        ws.getPrest().setPreDsRiga(0);
        ws.getPrest().setPreDsOperSql(Types.SPACE_CHAR);
        ws.getPrest().setPreDsVer(0);
        ws.getPrest().setPreDsTsIniCptz(0);
        ws.getPrest().setPreDsTsEndCptz(0);
        ws.getPrest().setPreDsUtente("");
        ws.getPrest().setPreDsStatoElab(Types.SPACE_CHAR);
    }
}
