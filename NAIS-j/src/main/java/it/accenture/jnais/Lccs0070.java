package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaIoLccs0070;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.Lccc0070Funzione;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccs0070Data;

/**Original name: LCCS0070<br>
 * <pre>*****************************************************************
 *                                                                 *
 *     PORTAFOGLIO VITA ITALIA  VER 1.0                            *
 *                                                                 *
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2009.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... LCCS0070
 *     TIPOLOGIA...... SERVIZIO TRASVERSALE
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... CALCOLA IB OGGETTO PER ADESIONE,GARANZIA
 *                     E TRANCHE
 * **------------------------------------------------------------***</pre>*/
public class Lccs0070 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0070Data ws = new Lccs0070Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: AREA-IO-LCCS0070
    private AreaIoLccs0070 areaIoLccs0070;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0070_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, AreaIoLccs0070 areaIoLccs0070) {
        this.areaIdsv0001 = areaIdsv0001;
        this.areaIoLccs0070 = areaIoLccs0070;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0070 getInstance() {
        return ((Lccs0070)Programs.getInstance(Lccs0070.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                      WS-VARIABILI
        initWsVariabili();
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT
        //              THRU EX-S0005.
        s0005CtrlDatiInput();
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void s0005CtrlDatiInput() {
        // COB_CODE: EVALUATE TRUE
        //             WHEN LCCC0070-CALC-IB-ADE
        //                END-IF
        //             WHEN LCCC0070-CALC-IB-GAR
        //                END-IF
        //             WHEN LCCC0070-CALC-IB-TGA
        //                END-IF
        //             WHEN OTHER
        //                   THRU EX-S0300
        //           END-EVALUATE.
        switch (areaIoLccs0070.getFunzione().getFunzione()) {

            case Lccc0070Funzione.ADE:// COB_CODE: IF LCCC0070-TP-OGGETTO NOT = 'PO'
                //                 THRU EX-S0300
                //           END-IF
                if (!Conditions.eq(areaIoLccs0070.getTpOggetto(), "PO")) {
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                    // COB_CODE: MOVE '005020'               TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005020");
                    // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
                break;

            case Lccc0070Funzione.GAR:// COB_CODE: IF LCCC0070-TP-OGGETTO NOT = 'AD'
                //                 THRU EX-S0300
                //           END-IF
                if (!Conditions.eq(areaIoLccs0070.getTpOggetto(), "AD")) {
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                    // COB_CODE: MOVE '005020'               TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005020");
                    // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
                break;

            case Lccc0070Funzione.TGA:// COB_CODE: IF LCCC0070-TP-OGGETTO NOT = 'GA'
                //                 THRU EX-S0300
                //           END-IF
                if (!Conditions.eq(areaIoLccs0070.getTpOggetto(), "GA")) {
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                    // COB_CODE: MOVE '005020'               TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005020");
                    // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
                break;

            default:// COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0005-CTRL-DATI-INPUT'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0005-CTRL-DATI-INPUT");
                // COB_CODE: MOVE '005020'               TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005020");
                // COB_CODE: MOVE SPACES                 TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
                break;
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1010-LETT-ADE-GAR-TRA
        //              THRU S1010-EX
        s1010LettAdeGarTra();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF OGGETTO-IN-PTF-SI
            //           *--        Aggiornamento occorrenza in tabella
            //                      END-IF
            //                   ELSE
            //           *--        Inserimento occorrenza in tabella
            //                      END-IF
            //                   END-IF
            if (ws.getWsOggettoInPtf().isSi()) {
                //--        Aggiornamento occorrenza in tabella
                // COB_CODE: PERFORM S1020-UPDATE-ADE-GAR-TRA
                //              THRU S1020-EX
                s1020UpdateAdeGarTra();
                // COB_CODE: IF IDSV0001-ESITO-OK
                //              MOVE WS-IB-OGGETTO TO LCCC0070-IB-OGGETTO
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: MOVE WS-IB-OGGETTO TO LCCC0070-IB-OGGETTO
                    areaIoLccs0070.setIbOggetto(ws.getWsIbOggettoFormatted());
                }
            }
            else {
                //--        Inserimento occorrenza in tabella
                // COB_CODE: PERFORM S1030-INSERT-ADE-GAR-TRA
                //              THRU S1030-EX
                s1030InsertAdeGarTra();
                // COB_CODE: IF IDSV0001-ESITO-OK
                //              MOVE WS-IB-OGGETTO TO LCCC0070-IB-OGGETTO
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: MOVE WS-IB-OGGETTO TO LCCC0070-IB-OGGETTO
                    areaIoLccs0070.setIbOggetto(ws.getWsIbOggettoFormatted());
                }
            }
        }
    }

    /**Original name: S1010-LETT-ADE-GAR-TRA<br>
	 * <pre>----------------------------------------------------------------*
	 *     Lettura della tabella numerazione ade/gar/tga
	 * ----------------------------------------------------------------*</pre>*/
    private void s1010LettAdeGarTra() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET OGGETTO-IN-PTF-NO TO TRUE
        ws.getWsOggettoInPtf().setNo();
        // COB_CODE: INITIALIZE NUM-ADE-GAR-TRA
        initNumAdeGarTra();
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        // COB_CODE: SET IDSI0011-SELECT              TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: MOVE 'NUM-ADE-GAR-TRA'           TO WK-TABELLA
        ws.setWkTabella("NUM-ADE-GAR-TRA");
        // COB_CODE: SET IDSI0011-PRIMARY-KEY         TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO NUM-COD-COMP-ANIA
        ws.getNumAdeGarTra().setNumCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE LCCC0070-ID-OGGETTO         TO NUM-ID-OGG
        ws.getNumAdeGarTra().setNumIdOgg(areaIoLccs0070.getIdOggetto());
        // COB_CODE: MOVE LCCC0070-TP-OGGETTO         TO NUM-TP-OGG
        ws.getNumAdeGarTra().setNumTpOgg(areaIoLccs0070.getTpOggetto());
        // COB_CODE: MOVE WK-TABELLA                  TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: MOVE NUM-ADE-GAR-TRA             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getNumAdeGarTra().getNumAdeGarTraFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-NOT-FOUND
            //                  CONTINUE
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                  MOVE IDSO0011-BUFFER-DATI TO NUM-ADE-GAR-TRA
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: CONTINUE
                //continue
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: SET OGGETTO-IN-PTF-SI       TO TRUE
                    ws.getWsOggettoInPtf().setSi();
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO NUM-ADE-GAR-TRA
                    ws.getNumAdeGarTra().setNumAdeGarTraFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SELECT-DISPATCHER'      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("SELECT-DISPATCHER");
            // COB_CODE: MOVE '005016'                 TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1020-UPDATE-ADE-GAR-TRA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1020UpdateAdeGarTra() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IDSV0001-DATA-EFFETTO    TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
        // COB_CODE: MOVE ZEROES                   TO IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA TO IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        // COB_CODE: MOVE 'NUM-ADE-GAR-TRA'           TO WK-TABELLA
        ws.setWkTabella("NUM-ADE-GAR-TRA");
        // COB_CODE: ADD  1                           TO NUM-ULT-NUM-LIN
        ws.getNumAdeGarTra().getNumUltNumLin().setNumUltNumLin(Trunc.toInt(1 + ws.getNumAdeGarTra().getNumUltNumLin().getNumUltNumLin(), 5));
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO NUM-COD-COMP-ANIA
        ws.getNumAdeGarTra().setNumCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE LCCC0070-ID-OGGETTO         TO NUM-ID-OGG
        ws.getNumAdeGarTra().setNumIdOgg(areaIoLccs0070.getIdOggetto());
        // COB_CODE: MOVE LCCC0070-TP-OGGETTO         TO NUM-TP-OGG
        ws.getNumAdeGarTra().setNumTpOgg(areaIoLccs0070.getTpOggetto());
        // COB_CODE: SET IDSI0011-UPDATE              TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Update();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY         TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE WK-TABELLA                  TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: MOVE NUM-ADE-GAR-TRA             TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getNumAdeGarTra().getNumAdeGarTraFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                MOVE NUM-ULT-NUM-LIN      TO WS-NUM-APPO
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: INITIALIZE WS-IB-OGGETTO
                    initWsIbOggetto();
                    // COB_CODE: MOVE NUM-ULT-NUM-LIN      TO WS-NUM-APPO
                    ws.setWsNumAppo(TruncAbs.toInt(ws.getNumAdeGarTra().getNumUltNumLin().getNumUltNumLin(), 9));
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1020-UPDATE-ADE-GAR-TRA'
            //                                         TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1020-UPDATE-ADE-GAR-TRA");
            // COB_CODE: MOVE '005016'                 TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1030-INSERT-ADE-GAR-TRA<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1030InsertAdeGarTra() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE NUM-ADE-GAR-TRA
        initNumAdeGarTra();
        // COB_CODE: MOVE 'NUM-ADE-GAR-TRA'           TO WK-TABELLA
        ws.setWkTabella("NUM-ADE-GAR-TRA");
        // COB_CODE: ADD  1                           TO NUM-ULT-NUM-LIN
        ws.getNumAdeGarTra().getNumUltNumLin().setNumUltNumLin(Trunc.toInt(1 + ws.getNumAdeGarTra().getNumUltNumLin().getNumUltNumLin(), 5));
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO NUM-COD-COMP-ANIA
        ws.getNumAdeGarTra().setNumCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE LCCC0070-ID-OGGETTO         TO NUM-ID-OGG
        ws.getNumAdeGarTra().setNumIdOgg(areaIoLccs0070.getIdOggetto());
        // COB_CODE: MOVE LCCC0070-TP-OGGETTO         TO NUM-TP-OGG
        ws.getNumAdeGarTra().setNumTpOgg(areaIoLccs0070.getTpOggetto());
        // COB_CODE: SET IDSI0011-INSERT              TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY         TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR    TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE WK-TABELLA                  TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: MOVE NUM-ADE-GAR-TRA             TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getNumAdeGarTra().getNumAdeGarTraFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        // COB_CODE: IF IDSO0011-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                 THRU EX-S0300
        //           END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSO0011-SUCCESSFUL-SQL
            //                MOVE NUM-ULT-NUM-LIN      TO WS-NUM-APPO
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: INITIALIZE WS-IB-OGGETTO
                    initWsIbOggetto();
                    // COB_CODE: MOVE NUM-ULT-NUM-LIN      TO WS-NUM-APPO
                    ws.setWsNumAppo(TruncAbs.toInt(ws.getNumAdeGarTra().getNumUltNumLin().getNumUltNumLin(), 9));
                    break;

                default:break;
            }
        }
        else {
            // COB_CODE: MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1030-INSERT-ADE-GAR-TRA'
            //                                         TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1030-INSERT-ADE-GAR-TRA");
            // COB_CODE: MOVE '005016'                 TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    public void initWsVariabili() {
        ws.setWsNumAppoFormatted("000000000");
        ws.setWsNumerFiller("");
    }

    public void initNumAdeGarTra() {
        ws.getNumAdeGarTra().setNumCodCompAnia(0);
        ws.getNumAdeGarTra().setNumIdOgg(0);
        ws.getNumAdeGarTra().setNumTpOgg("");
        ws.getNumAdeGarTra().getNumUltNumLin().setNumUltNumLin(0);
        ws.getNumAdeGarTra().setNumDsOperSql(Types.SPACE_CHAR);
        ws.getNumAdeGarTra().setNumDsVer(0);
        ws.getNumAdeGarTra().setNumDsTsCptz(0);
        ws.getNumAdeGarTra().setNumDsUtente("");
        ws.getNumAdeGarTra().setNumDsStatoElab(Types.SPACE_CHAR);
    }

    public void initWsIbOggetto() {
        ws.setWsNumAppoFormatted("000000000");
        ws.setWsNumerFiller("");
    }
}
