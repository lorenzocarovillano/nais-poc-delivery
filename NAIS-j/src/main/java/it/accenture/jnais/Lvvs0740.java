package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.C214TabOutput;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.WtitDati;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0740Data;
import it.accenture.jnais.ws.redefines.WtitDtApplzMora;
import it.accenture.jnais.ws.redefines.WtitDtCambioVlt;
import it.accenture.jnais.ws.redefines.WtitDtCertFisc;
import it.accenture.jnais.ws.redefines.WtitDtEmisTit;
import it.accenture.jnais.ws.redefines.WtitDtEndCop;
import it.accenture.jnais.ws.redefines.WtitDtEsiTit;
import it.accenture.jnais.ws.redefines.WtitDtIniCop;
import it.accenture.jnais.ws.redefines.WtitDtRichAddRid;
import it.accenture.jnais.ws.redefines.WtitDtVlt;
import it.accenture.jnais.ws.redefines.WtitFraz;
import it.accenture.jnais.ws.redefines.WtitIdMoviChiu;
import it.accenture.jnais.ws.redefines.WtitIdRappAna;
import it.accenture.jnais.ws.redefines.WtitIdRappRete;
import it.accenture.jnais.ws.redefines.WtitImpAder;
import it.accenture.jnais.ws.redefines.WtitImpAz;
import it.accenture.jnais.ws.redefines.WtitImpPag;
import it.accenture.jnais.ws.redefines.WtitImpTfr;
import it.accenture.jnais.ws.redefines.WtitImpTfrStrc;
import it.accenture.jnais.ws.redefines.WtitImpTrasfe;
import it.accenture.jnais.ws.redefines.WtitImpVolo;
import it.accenture.jnais.ws.redefines.WtitNumRatAccorpate;
import it.accenture.jnais.ws.redefines.WtitProgTit;
import it.accenture.jnais.ws.redefines.WtitTotAcqExp;
import it.accenture.jnais.ws.redefines.WtitTotCarAcq;
import it.accenture.jnais.ws.redefines.WtitTotCarGest;
import it.accenture.jnais.ws.redefines.WtitTotCarIas;
import it.accenture.jnais.ws.redefines.WtitTotCarInc;
import it.accenture.jnais.ws.redefines.WtitTotCnbtAntirac;
import it.accenture.jnais.ws.redefines.WtitTotCommisInter;
import it.accenture.jnais.ws.redefines.WtitTotDir;
import it.accenture.jnais.ws.redefines.WtitTotIntrFraz;
import it.accenture.jnais.ws.redefines.WtitTotIntrMora;
import it.accenture.jnais.ws.redefines.WtitTotIntrPrest;
import it.accenture.jnais.ws.redefines.WtitTotIntrRetdt;
import it.accenture.jnais.ws.redefines.WtitTotIntrRiat;
import it.accenture.jnais.ws.redefines.WtitTotManfeeAntic;
import it.accenture.jnais.ws.redefines.WtitTotManfeeRec;
import it.accenture.jnais.ws.redefines.WtitTotManfeeRicor;
import it.accenture.jnais.ws.redefines.WtitTotPreNet;
import it.accenture.jnais.ws.redefines.WtitTotPrePpIas;
import it.accenture.jnais.ws.redefines.WtitTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.WtitTotPreTot;
import it.accenture.jnais.ws.redefines.WtitTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.WtitTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.WtitTotProvDaRec;
import it.accenture.jnais.ws.redefines.WtitTotProvInc;
import it.accenture.jnais.ws.redefines.WtitTotProvRicor;
import it.accenture.jnais.ws.redefines.WtitTotRemunAss;
import it.accenture.jnais.ws.redefines.WtitTotSoprAlt;
import it.accenture.jnais.ws.redefines.WtitTotSoprProf;
import it.accenture.jnais.ws.redefines.WtitTotSoprSan;
import it.accenture.jnais.ws.redefines.WtitTotSoprSpo;
import it.accenture.jnais.ws.redefines.WtitTotSoprTec;
import it.accenture.jnais.ws.redefines.WtitTotSpeAge;
import it.accenture.jnais.ws.redefines.WtitTotSpeMed;
import it.accenture.jnais.ws.redefines.WtitTotTax;
import it.accenture.jnais.ws.redefines.WtitTpCausStor;
import it.accenture.jnais.ws.TitContIdbstit0;

/**Original name: LVVS0740<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2010.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0740
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA FINE COPERTURA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0740 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0740Data ws = new Lvvs0740Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0740_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Lvvs0740 getInstance() {
        return ((Lvvs0740)Programs.getInstance(Lvvs0740.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE 1    TO IX-TAB-TIT
        ws.getIxIndici().setTabTit(((short)1));
        // COB_CODE: PERFORM INIZIA-TOT-TIT
        //              THRU INIZIA-TOT-TIT-EX
        iniziaTotTit();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-ADE.
        initAreaIoAde();
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1110-LEGGI-PRESTITO  THRU  S1110-EX
            s1110LeggiPrestito();
            // COB_CODE: IF  PREST-ATTIVO
            //               END-IF
            //           ELSE
            //               MOVE  ZERO               TO IVVC0213-VAL-STR-O
            //           END-IF
            if (ws.getWkPrestito().isAttivo()) {
                // COB_CODE: MOVE  PRE-DT-DECOR-PREST
                //                    TO WK-DT-MAX
                ws.setWkDtMax(TruncAbs.toInt(ws.getPrest().getPreDtDecorPrest().getPreDtDecorPrest(), 8));
                // COB_CODE: MOVE  PRE-MOD-INTR-PREST
                //                    TO WK-TP-PRESTITO
                ws.getWkTpPrestito().setWkTpPrestito(ws.getPrest().getPreModIntrPrest());
                // COB_CODE: PERFORM S1255-LEGGI-TIT  THRU S1255-EX
                s1255LeggiTit();
                // COB_CODE:              IF DTIT-DT-END-COP(1) IS NUMERIC
                //           *            AND DTIT-DT-END-COP-NULL(1) NOT = HIGH-VALUE
                //           *                                     OR LOW-VALUE OR SPACES
                //                           END-IF
                //                        ELSE
                //                           MOVE PRE-DT-RICH-PREST  TO IVVC0213-VAL-STR-O
                //                        END-IF
                if (Functions.isNumber(ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getWtitDtEndCop())) {
                    //            AND DTIT-DT-END-COP-NULL(1) NOT = HIGH-VALUE
                    //                                     OR LOW-VALUE OR SPACES
                    // COB_CODE: IF  PRE-DT-CONCS-PREST IS NUMERIC
                    //              END-IF
                    //           END-IF
                    if (Functions.isNumber(ws.getPrest().getPreDtConcsPrest().getPreDtConcsPrest())) {
                        // COB_CODE: IF  DTIT-DT-END-COP(1) > PRE-DT-CONCS-PREST
                        //             MOVE DTIT-DT-END-COP(1) TO IVVC0213-VAL-STR-O
                        //           ELSE
                        //             MOVE PRE-DT-RICH-PREST  TO IVVC0213-VAL-STR-O
                        //           END-IF
                        if (ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getWtitDtEndCop() > ws.getPrest().getPreDtConcsPrest().getPreDtConcsPrest()) {
                            // COB_CODE: MOVE DTIT-DT-END-COP(1) TO IVVC0213-VAL-STR-O
                            ivvc0213.getTabOutput().setValStrO(ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getDtitDtEndCopFormatted());
                        }
                        else {
                            // COB_CODE: MOVE PRE-DT-RICH-PREST  TO IVVC0213-VAL-STR-O
                            ivvc0213.getTabOutput().setValStrO(ws.getPrest().getPreDtRichPrestFormatted());
                        }
                    }
                }
                else {
                    // COB_CODE: MOVE PRE-DT-RICH-PREST  TO IVVC0213-VAL-STR-O
                    ivvc0213.getTabOutput().setValStrO(ws.getPrest().getPreDtRichPrestFormatted());
                }
            }
            else {
                // COB_CODE: MOVE  ZERO               TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO(LiteralGenerator.create('0', C214TabOutput.Len.VAL_STR_O));
            }
        }
    }

    /**Original name: S1110-LEGGI-PRESTITO<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DELLA TABELLA PRESTITI CON DATA DECORRENZA PRESTITO
	 *     MINORE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1110LeggiPrestito() {
        Ldbs6160 ldbs6160 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET PREST-NON-ATTIVO          TO TRUE
        ws.getWkPrestito().setNonAttivo();
        // COB_CODE: INITIALIZE                       PREST.
        initPrest();
        // COB_CODE: SET  IDSV0003-TRATT-X-EFFETTO TO TRUE.
        idsv0003.getTrattamentoStoricita().setIdsi0011TrattXEffetto();
        // COB_CODE: SET  IDSV0003-WHERE-CONDITION TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET  IDSV0003-SELECT          TO TRUE.
        idsv0003.getOperazione().setSelect();
        //    SET  IDSV0003-FETCH-FIRST     TO TRUE.
        // COB_CODE: MOVE 'AD'                     TO PRE-TP-OGG
        ws.getPrest().setPreTpOgg("AD");
        // COB_CODE: MOVE DADE-ID-ADES             TO PRE-ID-OGG
        ws.getPrest().setPreIdOgg(ws.getLccvade1().getDati().getWadeIdAdes());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                         TO PRE-COD-COMP-ANIA
        ws.getPrest().setPreCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-APPO-DATA
        ws.setWkAppoData(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
        // COB_CODE: MOVE 99991230                 TO IDSV0003-DATA-INIZIO-EFFETTO
        idsv0003.setDataInizioEffetto(99991230);
        //    MOVE 999912314023595998       TO IDSV0003-DATA-COMPETENZA
        //
        // COB_CODE: MOVE 'LDBS6160'               TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS6160");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 PREST
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs6160 = Ldbs6160.getInstance();
            ldbs6160.run(idsv0003, ws.getPrest());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS6160 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6160 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: MOVE  WK-APPO-DATA          TO IDSV0003-DATA-INIZIO-EFFETTO
        idsv0003.setDataInizioEffetto(ws.getWkAppoData());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN IDSV0003-SUCCESSFUL-SQL
            //                SET PREST-ATTIVO      TO  TRUE
            //             WHEN IDSV0003-NOT-FOUND
            //                CONTINUE
            //             WHEN OTHER
            //                END-STRING
            //           END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: SET PREST-ATTIVO      TO  TRUE
                    ws.getWkPrestito().setAttivo();
                    break;

                case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-CALL-PGM        TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                    // COB_CODE: STRING 'CHIAMATA LDBS6160 ;'
                    //              IDSV0003-RETURN-CODE ';'
                    //              IDSV0003-SQLCODE
                    //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6160 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'ERRORE CHIAMATA LDBS6160'   ';'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LDBS6160", ";", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-ADES
        //                TO DADE-AREA-ADES
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasAdes())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DADE-AREA-ADES
            ws.setDadeAreaAdesFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1255-LEGGI-TIT<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1255LeggiTit() {
        Ldbs8570 ldbs8570 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                          TIT-CONT.
        initTitCont();
        //
        // COB_CODE: SET  IDSV0003-TRATT-X-EFFETTO       TO TRUE.
        idsv0003.getTrattamentoStoricita().setIdsi0011TrattXEffetto();
        // COB_CODE: SET  IDSV0003-FETCH-FIRST           TO TRUE.
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET  IDSV0003-WHERE-CONDITION       TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: IF WK-SEPARATO
        //              MOVE 'IP'                        TO TIT-TP-TIT
        //           ELSE
        //              MOVE 'PR'                        TO TIT-TP-TIT
        //           END-IF
        if (ws.getWkTpPrestito().isSeparato()) {
            // COB_CODE: MOVE 'IP'                        TO TIT-TP-TIT
            ws.getTitCont().setTitTpTit("IP");
        }
        else {
            // COB_CODE: MOVE 'PR'                        TO TIT-TP-TIT
            ws.getTitCont().setTitTpTit("PR");
        }
        // COB_CODE: MOVE 'IN'                           TO TIT-TP-STAT-TIT.
        ws.getTitCont().setTitTpStatTit("IN");
        // COB_CODE: MOVE 'PO'                           TO TIT-TP-OGG.
        ws.getTitCont().setTitTpOgg("PO");
        // COB_CODE: MOVE DADE-ID-POLI                   TO TIT-ID-OGG.
        ws.getTitCont().setTitIdOgg(ws.getLccvade1().getDati().getWadeIdPoli());
        //
        // COB_CODE: MOVE 'LDBS8570'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS8570");
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql())) {
            //
            // COB_CODE:         CALL WK-CALL-PGM  USING  IDSV0003 TIT-CONT
            //           *
            //                   ON EXCEPTION
            //                        SET IDSV0003-INVALID-OPER  TO TRUE
            //                   END-CALL
            try {
                ldbs8570 = Ldbs8570.getInstance();
                ldbs8570.run(idsv0003, ws.getTitCont());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: MOVE 'CALL-LDBS8570 ERRORE CHIAMATA'
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS8570 ERRORE CHIAMATA");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //            END-IF
            //           ELSE
            //              END-STRING
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 THRU S1260-EX
                //           ELSE
                //              END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: SET IDSV0003-FETCH-NEXT TO TRUE
                    idsv0003.getOperazione().setFetchNext();
                    // COB_CODE: PERFORM S1260-VALORIZZA
                    //              THRU S1260-EX
                    s1260Valorizza();
                }
                else if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //              CONTINUE
                //           ELSE
                //            END-STRING
                //           END-IF
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: IF IDSV0003-SQLCODE = -305
                    //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    //           ELSE
                    //              SET IDSV0003-INVALID-OPER            TO TRUE
                    //           END-IF
                    if (idsv0003.getSqlcode().getSqlcode() == -305) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                    }
                    else {
                        // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                    }
                    //
                    // COB_CODE: MOVE WK-CALL-PGM         TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                    // COB_CODE: STRING 'CHIAMATA LDBS8570 ;'
                    //             IDSV0003-RETURN-CODE ';'
                    //             IDSV0003-SQLCODE
                    //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS8570 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'ERRORE CHIAMATA LDBS8570'   ';'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //                DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LDBS8570", ";", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            }
        }
    }

    /**Original name: S1260-VALORIZZA<br>*/
    private void s1260Valorizza() {
        // COB_CODE: IF WK-SEPARATO
        //              END-IF
        //           END-IF
        if (ws.getWkTpPrestito().isSeparato()) {
            // COB_CODE: IF DTIT-DT-END-COP-NULL(1) NOT = HIGH-VALUE
            //                              OR LOW-VALUE OR SPACES
            //              END-IF
            //           ELSE
            //                 THRU VALORIZZA-OUTPUT-TIT-EX
            //           END-IF
            if (!Characters.EQ_HIGH.test(ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getDtitDtEndCopNullFormatted()) || !Characters.EQ_LOW.test(ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getDtitDtEndCopNullFormatted()) || !Characters.EQ_SPACE.test(ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getWtitDtEndCopNull())) {
                // COB_CODE: IF TIT-DT-END-COP-NULL NOT = HIGH-VALUE
                //                                     OR LOW-VALUE OR SPACES
                //              END-IF
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNullFormatted()) || !Characters.EQ_LOW.test(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNullFormatted()) || !Characters.EQ_SPACE.test(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNull())) {
                    // COB_CODE: IF TIT-DT-END-COP > DTIT-DT-END-COP(1)
                    //                 THRU VALORIZZA-OUTPUT-TIT-EX
                    //           END-IF
                    if (ws.getTitCont().getTitDtEndCop().getTitDtEndCop() > ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getWtitDtEndCop()) {
                        // COB_CODE: MOVE 1 TO IX-TAB-TIT
                        ws.getIxIndici().setTabTit(((short)1));
                        // COB_CODE: PERFORM VALORIZZA-OUTPUT-TIT
                        //              THRU VALORIZZA-OUTPUT-TIT-EX
                        valorizzaOutputTit();
                    }
                }
            }
            else {
                // COB_CODE: MOVE 1 TO IX-TAB-TIT
                ws.getIxIndici().setTabTit(((short)1));
                // COB_CODE: PERFORM VALORIZZA-OUTPUT-TIT
                //              THRU VALORIZZA-OUTPUT-TIT-EX
                valorizzaOutputTit();
            }
        }
        // COB_CODE: IF WK-UNICO
        //              END-IF
        //           END-IF.
        if (ws.getWkTpPrestito().isUnico()) {
            // COB_CODE: IF TIT-TP-PRE-TIT = 'PE'
            //           OR TIT-TP-PRE-TIT = 'QU'
            //           OR TIT-TP-PRE-TIT = 'SE'
            //              END-IF
            //           END-IF
            if (Conditions.eq(ws.getTitCont().getTitTpPreTit(), "PE") || Conditions.eq(ws.getTitCont().getTitTpPreTit(), "QU") || Conditions.eq(ws.getTitCont().getTitTpPreTit(), "SE")) {
                // COB_CODE: IF DTIT-DT-END-COP-NULL(1) NOT = HIGH-VALUE
                //                              OR LOW-VALUE OR SPACES
                //              END-IF
                //           ELSE
                //                 THRU VALORIZZA-OUTPUT-TIT-EX
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getDtitDtEndCopNullFormatted()) || !Characters.EQ_LOW.test(ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getDtitDtEndCopNullFormatted()) || !Characters.EQ_SPACE.test(ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getWtitDtEndCopNull())) {
                    // COB_CODE: IF TIT-DT-END-COP-NULL NOT = HIGH-VALUE
                    //                                     OR LOW-VALUE OR SPACES
                    //              END-IF
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNullFormatted()) || !Characters.EQ_LOW.test(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNullFormatted()) || !Characters.EQ_SPACE.test(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNull())) {
                        // COB_CODE: IF TIT-DT-END-COP > DTIT-DT-END-COP(1)
                        //                 THRU VALORIZZA-OUTPUT-TIT-EX
                        //           END-IF
                        if (ws.getTitCont().getTitDtEndCop().getTitDtEndCop() > ws.getDtitTabTitCont(1).getLccvtit1().getDati().getWtitDtEndCop().getWtitDtEndCop()) {
                            // COB_CODE: MOVE 1 TO IX-TAB-TIT
                            ws.getIxIndici().setTabTit(((short)1));
                            // COB_CODE: PERFORM VALORIZZA-OUTPUT-TIT
                            //              THRU VALORIZZA-OUTPUT-TIT-EX
                            valorizzaOutputTit();
                        }
                    }
                }
                else {
                    // COB_CODE: MOVE 1 TO IX-TAB-TIT
                    ws.getIxIndici().setTabTit(((short)1));
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-TIT
                    //              THRU VALORIZZA-OUTPUT-TIT-EX
                    valorizzaOutputTit();
                }
            }
        }
    }

    /**Original name: VALORIZZA-OUTPUT-TIT<br>
	 * <pre>*****************************************************************
	 *  COPY UTILI INIZIALIZZA E VALORIZZA TIT-CONT
	 * *****************************************************************
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVTIT3
	 *    ULTIMO AGG. 05 DIC 2014
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputTit() {
        // COB_CODE: MOVE TIT-ID-TIT-CONT
        //             TO (SF)-ID-PTF(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().setIdPtf(ws.getTitCont().getTitIdTitCont());
        // COB_CODE: MOVE TIT-ID-TIT-CONT
        //             TO (SF)-ID-TIT-CONT(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitIdTitCont(ws.getTitCont().getTitIdTitCont());
        // COB_CODE: MOVE TIT-ID-OGG
        //             TO (SF)-ID-OGG(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitIdOgg(ws.getTitCont().getTitIdOgg());
        // COB_CODE: MOVE TIT-TP-OGG
        //             TO (SF)-TP-OGG(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpOgg(ws.getTitCont().getTitTpOgg());
        // COB_CODE: IF TIT-IB-RICH-NULL = HIGH-VALUES
        //                TO (SF)-IB-RICH-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IB-RICH(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitIbRich(), TitContIdbstit0.Len.TIT_IB_RICH)) {
            // COB_CODE: MOVE TIT-IB-RICH-NULL
            //             TO (SF)-IB-RICH-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitIbRich(ws.getTitCont().getTitIbRich());
        }
        else {
            // COB_CODE: MOVE TIT-IB-RICH
            //             TO (SF)-IB-RICH(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitIbRich(ws.getTitCont().getTitIbRich());
        }
        // COB_CODE: MOVE TIT-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitIdMoviCrz(ws.getTitCont().getTitIdMoviCrz());
        // COB_CODE: IF TIT-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitIdMoviChiu().getTitIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE TIT-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitIdMoviChiu().setWtitIdMoviChiuNull(ws.getTitCont().getTitIdMoviChiu().getTitIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE TIT-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitIdMoviChiu().setWtitIdMoviChiu(ws.getTitCont().getTitIdMoviChiu().getTitIdMoviChiu());
        }
        // COB_CODE: MOVE TIT-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitDtIniEff(ws.getTitCont().getTitDtIniEff());
        // COB_CODE: MOVE TIT-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitDtEndEff(ws.getTitCont().getTitDtEndEff());
        // COB_CODE: MOVE TIT-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitCodCompAnia(ws.getTitCont().getTitCodCompAnia());
        // COB_CODE: MOVE TIT-TP-TIT
        //             TO (SF)-TP-TIT(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpTit(ws.getTitCont().getTitTpTit());
        // COB_CODE: IF TIT-PROG-TIT-NULL = HIGH-VALUES
        //                TO (SF)-PROG-TIT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-PROG-TIT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitProgTit().getTitProgTitNullFormatted())) {
            // COB_CODE: MOVE TIT-PROG-TIT-NULL
            //             TO (SF)-PROG-TIT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitProgTit().setWtitProgTitNull(ws.getTitCont().getTitProgTit().getTitProgTitNull());
        }
        else {
            // COB_CODE: MOVE TIT-PROG-TIT
            //             TO (SF)-PROG-TIT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitProgTit().setWtitProgTit(ws.getTitCont().getTitProgTit().getTitProgTit());
        }
        // COB_CODE: MOVE TIT-TP-PRE-TIT
        //             TO (SF)-TP-PRE-TIT(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpPreTit(ws.getTitCont().getTitTpPreTit());
        // COB_CODE: MOVE TIT-TP-STAT-TIT
        //             TO (SF)-TP-STAT-TIT(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpStatTit(ws.getTitCont().getTitTpStatTit());
        // COB_CODE: IF TIT-DT-INI-COP-NULL = HIGH-VALUES
        //                TO (SF)-DT-INI-COP-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-INI-COP(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtIniCop().getTitDtIniCopNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-INI-COP-NULL
            //             TO (SF)-DT-INI-COP-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtIniCop().setWtitDtIniCopNull(ws.getTitCont().getTitDtIniCop().getTitDtIniCopNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-INI-COP
            //             TO (SF)-DT-INI-COP(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtIniCop().setWtitDtIniCop(ws.getTitCont().getTitDtIniCop().getTitDtIniCop());
        }
        // COB_CODE: IF TIT-DT-END-COP-NULL = HIGH-VALUES
        //                TO (SF)-DT-END-COP-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-END-COP(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-END-COP-NULL
            //             TO (SF)-DT-END-COP-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtEndCop().setWtitDtEndCopNull(ws.getTitCont().getTitDtEndCop().getTitDtEndCopNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-END-COP
            //             TO (SF)-DT-END-COP(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtEndCop().setWtitDtEndCop(ws.getTitCont().getTitDtEndCop().getTitDtEndCop());
        }
        // COB_CODE: IF TIT-IMP-PAG-NULL = HIGH-VALUES
        //                TO (SF)-IMP-PAG-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-PAG(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpPag().getTitImpPagNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-PAG-NULL
            //             TO (SF)-IMP-PAG-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpPag().setWtitImpPagNull(ws.getTitCont().getTitImpPag().getTitImpPagNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-PAG
            //             TO (SF)-IMP-PAG(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpPag().setWtitImpPag(Trunc.toDecimal(ws.getTitCont().getTitImpPag().getTitImpPag(), 15, 3));
        }
        // COB_CODE: IF TIT-FL-SOLL-NULL = HIGH-VALUES
        //                TO (SF)-FL-SOLL-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-SOLL(IX-TAB-TIT)
        //           END-IF
        if (Conditions.eq(ws.getTitCont().getTitFlSoll(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-SOLL-NULL
            //             TO (SF)-FL-SOLL-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlSoll(ws.getTitCont().getTitFlSoll());
        }
        else {
            // COB_CODE: MOVE TIT-FL-SOLL
            //             TO (SF)-FL-SOLL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlSoll(ws.getTitCont().getTitFlSoll());
        }
        // COB_CODE: IF TIT-FRAZ-NULL = HIGH-VALUES
        //                TO (SF)-FRAZ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FRAZ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitFraz().getTitFrazNullFormatted())) {
            // COB_CODE: MOVE TIT-FRAZ-NULL
            //             TO (SF)-FRAZ-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitFraz().setWtitFrazNull(ws.getTitCont().getTitFraz().getTitFrazNull());
        }
        else {
            // COB_CODE: MOVE TIT-FRAZ
            //             TO (SF)-FRAZ(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitFraz().setWtitFraz(ws.getTitCont().getTitFraz().getTitFraz());
        }
        // COB_CODE: IF TIT-DT-APPLZ-MORA-NULL = HIGH-VALUES
        //                TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-APPLZ-MORA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtApplzMora().getTitDtApplzMoraNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-APPLZ-MORA-NULL
            //             TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtApplzMora().setWtitDtApplzMoraNull(ws.getTitCont().getTitDtApplzMora().getTitDtApplzMoraNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-APPLZ-MORA
            //             TO (SF)-DT-APPLZ-MORA(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtApplzMora().setWtitDtApplzMora(ws.getTitCont().getTitDtApplzMora().getTitDtApplzMora());
        }
        // COB_CODE: IF TIT-FL-MORA-NULL = HIGH-VALUES
        //                TO (SF)-FL-MORA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-MORA(IX-TAB-TIT)
        //           END-IF
        if (Conditions.eq(ws.getTitCont().getTitFlMora(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-MORA-NULL
            //             TO (SF)-FL-MORA-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlMora(ws.getTitCont().getTitFlMora());
        }
        else {
            // COB_CODE: MOVE TIT-FL-MORA
            //             TO (SF)-FL-MORA(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlMora(ws.getTitCont().getTitFlMora());
        }
        // COB_CODE: IF TIT-ID-RAPP-RETE-NULL = HIGH-VALUES
        //                TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-ID-RAPP-RETE(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitIdRappRete().getTitIdRappReteNullFormatted())) {
            // COB_CODE: MOVE TIT-ID-RAPP-RETE-NULL
            //             TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitIdRappRete().setWtitIdRappReteNull(ws.getTitCont().getTitIdRappRete().getTitIdRappReteNull());
        }
        else {
            // COB_CODE: MOVE TIT-ID-RAPP-RETE
            //             TO (SF)-ID-RAPP-RETE(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitIdRappRete().setWtitIdRappRete(ws.getTitCont().getTitIdRappRete().getTitIdRappRete());
        }
        // COB_CODE: IF TIT-ID-RAPP-ANA-NULL = HIGH-VALUES
        //                TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-ID-RAPP-ANA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitIdRappAna().getTitIdRappAnaNullFormatted())) {
            // COB_CODE: MOVE TIT-ID-RAPP-ANA-NULL
            //             TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitIdRappAna().setWtitIdRappAnaNull(ws.getTitCont().getTitIdRappAna().getTitIdRappAnaNull());
        }
        else {
            // COB_CODE: MOVE TIT-ID-RAPP-ANA
            //             TO (SF)-ID-RAPP-ANA(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitIdRappAna().setWtitIdRappAna(ws.getTitCont().getTitIdRappAna().getTitIdRappAna());
        }
        // COB_CODE: IF TIT-COD-DVS-NULL = HIGH-VALUES
        //                TO (SF)-COD-DVS-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-COD-DVS(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitCodDvsFormatted())) {
            // COB_CODE: MOVE TIT-COD-DVS-NULL
            //             TO (SF)-COD-DVS-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitCodDvs(ws.getTitCont().getTitCodDvs());
        }
        else {
            // COB_CODE: MOVE TIT-COD-DVS
            //             TO (SF)-COD-DVS(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitCodDvs(ws.getTitCont().getTitCodDvs());
        }
        // COB_CODE: IF TIT-DT-EMIS-TIT-NULL = HIGH-VALUES
        //                TO (SF)-DT-EMIS-TIT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-EMIS-TIT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtEmisTit().getTitDtEmisTitNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-EMIS-TIT-NULL
            //             TO (SF)-DT-EMIS-TIT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtEmisTit().setWtitDtEmisTitNull(ws.getTitCont().getTitDtEmisTit().getTitDtEmisTitNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-EMIS-TIT
            //             TO (SF)-DT-EMIS-TIT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtEmisTit().setWtitDtEmisTit(ws.getTitCont().getTitDtEmisTit().getTitDtEmisTit());
        }
        // COB_CODE: IF TIT-DT-ESI-TIT-NULL = HIGH-VALUES
        //                TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-ESI-TIT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtEsiTit().getTitDtEsiTitNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-ESI-TIT-NULL
            //             TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtEsiTit().setWtitDtEsiTitNull(ws.getTitCont().getTitDtEsiTit().getTitDtEsiTitNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-ESI-TIT
            //             TO (SF)-DT-ESI-TIT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtEsiTit().setWtitDtEsiTit(ws.getTitCont().getTitDtEsiTit().getTitDtEsiTit());
        }
        // COB_CODE: IF TIT-TOT-PRE-NET-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PRE-NET(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPreNet().getTitTotPreNetNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PRE-NET-NULL
            //             TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPreNet().setWtitTotPreNetNull(ws.getTitCont().getTitTotPreNet().getTitTotPreNetNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PRE-NET
            //             TO (SF)-TOT-PRE-NET(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPreNet().setWtitTotPreNet(Trunc.toDecimal(ws.getTitCont().getTitTotPreNet().getTitTotPreNet(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-FRAZ-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-FRAZ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrFraz().getTitTotIntrFrazNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-FRAZ-NULL
            //             TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrFraz().setWtitTotIntrFrazNull(ws.getTitCont().getTitTotIntrFraz().getTitTotIntrFrazNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-FRAZ
            //             TO (SF)-TOT-INTR-FRAZ(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrFraz().setWtitTotIntrFraz(Trunc.toDecimal(ws.getTitCont().getTitTotIntrFraz().getTitTotIntrFraz(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-MORA-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-MORA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrMora().getTitTotIntrMoraNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-MORA-NULL
            //             TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrMora().setWtitTotIntrMoraNull(ws.getTitCont().getTitTotIntrMora().getTitTotIntrMoraNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-MORA
            //             TO (SF)-TOT-INTR-MORA(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrMora().setWtitTotIntrMora(Trunc.toDecimal(ws.getTitCont().getTitTotIntrMora().getTitTotIntrMora(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-PREST-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-PREST(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrPrest().getTitTotIntrPrestNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-PREST-NULL
            //             TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrPrest().setWtitTotIntrPrestNull(ws.getTitCont().getTitTotIntrPrest().getTitTotIntrPrestNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-PREST
            //             TO (SF)-TOT-INTR-PREST(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrPrest().setWtitTotIntrPrest(Trunc.toDecimal(ws.getTitCont().getTitTotIntrPrest().getTitTotIntrPrest(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-RETDT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-RETDT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrRetdt().getTitTotIntrRetdtNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-RETDT-NULL
            //             TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrRetdt().setWtitTotIntrRetdtNull(ws.getTitCont().getTitTotIntrRetdt().getTitTotIntrRetdtNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-RETDT
            //             TO (SF)-TOT-INTR-RETDT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrRetdt().setWtitTotIntrRetdt(Trunc.toDecimal(ws.getTitCont().getTitTotIntrRetdt().getTitTotIntrRetdt(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-INTR-RIAT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-INTR-RIAT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotIntrRiat().getTitTotIntrRiatNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-INTR-RIAT-NULL
            //             TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrRiat().setWtitTotIntrRiatNull(ws.getTitCont().getTitTotIntrRiat().getTitTotIntrRiatNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-RIAT
            //             TO (SF)-TOT-INTR-RIAT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrRiat().setWtitTotIntrRiat(Trunc.toDecimal(ws.getTitCont().getTitTotIntrRiat().getTitTotIntrRiat(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-DIR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-DIR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-DIR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotDir().getTitTotDirNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-DIR-NULL
            //             TO (SF)-TOT-DIR-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotDir().setWtitTotDirNull(ws.getTitCont().getTitTotDir().getTitTotDirNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-DIR
            //             TO (SF)-TOT-DIR(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotDir().setWtitTotDir(Trunc.toDecimal(ws.getTitCont().getTitTotDir().getTitTotDir(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SPE-MED-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SPE-MED(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSpeMed().getTitTotSpeMedNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SPE-MED-NULL
            //             TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSpeMed().setWtitTotSpeMedNull(ws.getTitCont().getTitTotSpeMed().getTitTotSpeMedNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SPE-MED
            //             TO (SF)-TOT-SPE-MED(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSpeMed().setWtitTotSpeMed(Trunc.toDecimal(ws.getTitCont().getTitTotSpeMed().getTitTotSpeMed(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-TAX-NULL = HIGH-VALUES
        //                TO (SF)-TOT-TAX-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-TAX(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotTax().getTitTotTaxNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-TAX-NULL
            //             TO (SF)-TOT-TAX-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotTax().setWtitTotTaxNull(ws.getTitCont().getTitTotTax().getTitTotTaxNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-TAX
            //             TO (SF)-TOT-TAX(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotTax().setWtitTotTax(Trunc.toDecimal(ws.getTitCont().getTitTotTax().getTitTotTax(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-SAN-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-SAN(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprSan().getTitTotSoprSanNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-SAN-NULL
            //             TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprSan().setWtitTotSoprSanNull(ws.getTitCont().getTitTotSoprSan().getTitTotSoprSanNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-SAN
            //             TO (SF)-TOT-SOPR-SAN(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprSan().setWtitTotSoprSan(Trunc.toDecimal(ws.getTitCont().getTitTotSoprSan().getTitTotSoprSan(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-TEC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-TEC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprTec().getTitTotSoprTecNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-TEC-NULL
            //             TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprTec().setWtitTotSoprTecNull(ws.getTitCont().getTitTotSoprTec().getTitTotSoprTecNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-TEC
            //             TO (SF)-TOT-SOPR-TEC(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprTec().setWtitTotSoprTec(Trunc.toDecimal(ws.getTitCont().getTitTotSoprTec().getTitTotSoprTec(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-SPO-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-SPO(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprSpo().getTitTotSoprSpoNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-SPO-NULL
            //             TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprSpo().setWtitTotSoprSpoNull(ws.getTitCont().getTitTotSoprSpo().getTitTotSoprSpoNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-SPO
            //             TO (SF)-TOT-SOPR-SPO(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprSpo().setWtitTotSoprSpo(Trunc.toDecimal(ws.getTitCont().getTitTotSoprSpo().getTitTotSoprSpo(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-PROF-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-PROF(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprProf().getTitTotSoprProfNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-PROF-NULL
            //             TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprProf().setWtitTotSoprProfNull(ws.getTitCont().getTitTotSoprProf().getTitTotSoprProfNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-PROF
            //             TO (SF)-TOT-SOPR-PROF(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprProf().setWtitTotSoprProf(Trunc.toDecimal(ws.getTitCont().getTitTotSoprProf().getTitTotSoprProf(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-SOPR-ALT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SOPR-ALT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSoprAlt().getTitTotSoprAltNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SOPR-ALT-NULL
            //             TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprAlt().setWtitTotSoprAltNull(ws.getTitCont().getTitTotSoprAlt().getTitTotSoprAltNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SOPR-ALT
            //             TO (SF)-TOT-SOPR-ALT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprAlt().setWtitTotSoprAlt(Trunc.toDecimal(ws.getTitCont().getTitTotSoprAlt().getTitTotSoprAlt(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PRE-TOT-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PRE-TOT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPreTot().getTitTotPreTotNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PRE-TOT-NULL
            //             TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPreTot().setWtitTotPreTotNull(ws.getTitCont().getTitTotPreTot().getTitTotPreTotNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PRE-TOT
            //             TO (SF)-TOT-PRE-TOT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPreTot().setWtitTotPreTot(Trunc.toDecimal(ws.getTitCont().getTitTotPreTot().getTitTotPreTot(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PRE-PP-IAS-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PRE-PP-IAS(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPrePpIas().getTitTotPrePpIasNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PRE-PP-IAS-NULL
            //             TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPrePpIas().setWtitTotPrePpIasNull(ws.getTitCont().getTitTotPrePpIas().getTitTotPrePpIasNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PRE-PP-IAS
            //             TO (SF)-TOT-PRE-PP-IAS(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPrePpIas().setWtitTotPrePpIas(Trunc.toDecimal(ws.getTitCont().getTitTotPrePpIas().getTitTotPrePpIas(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-CAR-ACQ-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CAR-ACQ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCarAcq().getTitTotCarAcqNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CAR-ACQ-NULL
            //             TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarAcq().setWtitTotCarAcqNull(ws.getTitCont().getTitTotCarAcq().getTitTotCarAcqNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CAR-ACQ
            //             TO (SF)-TOT-CAR-ACQ(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarAcq().setWtitTotCarAcq(Trunc.toDecimal(ws.getTitCont().getTitTotCarAcq().getTitTotCarAcq(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-CAR-GEST-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CAR-GEST(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCarGest().getTitTotCarGestNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CAR-GEST-NULL
            //             TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarGest().setWtitTotCarGestNull(ws.getTitCont().getTitTotCarGest().getTitTotCarGestNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CAR-GEST
            //             TO (SF)-TOT-CAR-GEST(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarGest().setWtitTotCarGest(Trunc.toDecimal(ws.getTitCont().getTitTotCarGest().getTitTotCarGest(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-CAR-INC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CAR-INC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCarInc().getTitTotCarIncNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CAR-INC-NULL
            //             TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarInc().setWtitTotCarIncNull(ws.getTitCont().getTitTotCarInc().getTitTotCarIncNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CAR-INC
            //             TO (SF)-TOT-CAR-INC(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarInc().setWtitTotCarInc(Trunc.toDecimal(ws.getTitCont().getTitTotCarInc().getTitTotCarInc(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PRE-SOLO-RSH-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotPreSoloRsh().getTitTotPreSoloRshNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PRE-SOLO-RSH-NULL
            //             TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPreSoloRsh().setWtitTotPreSoloRshNull(ws.getTitCont().getTitTotPreSoloRsh().getTitTotPreSoloRshNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PRE-SOLO-RSH
            //             TO (SF)-TOT-PRE-SOLO-RSH(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPreSoloRsh().setWtitTotPreSoloRsh(Trunc.toDecimal(ws.getTitCont().getTitTotPreSoloRsh().getTitTotPreSoloRsh(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-ACQ-1AA-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvAcq1aa().getTitTotProvAcq1aaNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-ACQ-1AA-NULL
            //             TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvAcq1aa().setWtitTotProvAcq1aaNull(ws.getTitCont().getTitTotProvAcq1aa().getTitTotProvAcq1aaNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-ACQ-1AA
            //             TO (SF)-TOT-PROV-ACQ-1AA(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvAcq1aa().setWtitTotProvAcq1aa(Trunc.toDecimal(ws.getTitCont().getTitTotProvAcq1aa().getTitTotProvAcq1aa(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-ACQ-2AA-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvAcq2aa().getTitTotProvAcq2aaNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-ACQ-2AA-NULL
            //             TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvAcq2aa().setWtitTotProvAcq2aaNull(ws.getTitCont().getTitTotProvAcq2aa().getTitTotProvAcq2aaNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-ACQ-2AA
            //             TO (SF)-TOT-PROV-ACQ-2AA(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvAcq2aa().setWtitTotProvAcq2aa(Trunc.toDecimal(ws.getTitCont().getTitTotProvAcq2aa().getTitTotProvAcq2aa(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-RICOR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvRicor().getTitTotProvRicorNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-RICOR-NULL
            //             TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvRicor().setWtitTotProvRicorNull(ws.getTitCont().getTitTotProvRicor().getTitTotProvRicorNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-RICOR
            //             TO (SF)-TOT-PROV-RICOR(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvRicor().setWtitTotProvRicor(Trunc.toDecimal(ws.getTitCont().getTitTotProvRicor().getTitTotProvRicor(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-INC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-INC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvInc().getTitTotProvIncNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-INC-NULL
            //             TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvInc().setWtitTotProvIncNull(ws.getTitCont().getTitTotProvInc().getTitTotProvIncNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-INC
            //             TO (SF)-TOT-PROV-INC(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvInc().setWtitTotProvInc(Trunc.toDecimal(ws.getTitCont().getTitTotProvInc().getTitTotProvInc(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-PROV-DA-REC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-PROV-DA-REC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotProvDaRec().getTitTotProvDaRecNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-PROV-DA-REC-NULL
            //             TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvDaRec().setWtitTotProvDaRecNull(ws.getTitCont().getTitTotProvDaRec().getTitTotProvDaRecNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-PROV-DA-REC
            //             TO (SF)-TOT-PROV-DA-REC(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvDaRec().setWtitTotProvDaRec(Trunc.toDecimal(ws.getTitCont().getTitTotProvDaRec().getTitTotProvDaRec(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-AZ-NULL = HIGH-VALUES
        //                TO (SF)-IMP-AZ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-AZ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpAz().getTitImpAzNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-AZ-NULL
            //             TO (SF)-IMP-AZ-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpAz().setWtitImpAzNull(ws.getTitCont().getTitImpAz().getTitImpAzNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-AZ
            //             TO (SF)-IMP-AZ(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpAz().setWtitImpAz(Trunc.toDecimal(ws.getTitCont().getTitImpAz().getTitImpAz(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-ADER-NULL = HIGH-VALUES
        //                TO (SF)-IMP-ADER-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-ADER(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpAder().getTitImpAderNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-ADER-NULL
            //             TO (SF)-IMP-ADER-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpAder().setWtitImpAderNull(ws.getTitCont().getTitImpAder().getTitImpAderNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-ADER
            //             TO (SF)-IMP-ADER(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpAder().setWtitImpAder(Trunc.toDecimal(ws.getTitCont().getTitImpAder().getTitImpAder(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-TFR-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-TFR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpTfr().getTitImpTfrNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-TFR-NULL
            //             TO (SF)-IMP-TFR-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpTfr().setWtitImpTfrNull(ws.getTitCont().getTitImpTfr().getTitImpTfrNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-TFR
            //             TO (SF)-IMP-TFR(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpTfr().setWtitImpTfr(Trunc.toDecimal(ws.getTitCont().getTitImpTfr().getTitImpTfr(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-VOLO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-VOLO-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-VOLO(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpVolo().getTitImpVoloNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-VOLO-NULL
            //             TO (SF)-IMP-VOLO-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpVolo().setWtitImpVoloNull(ws.getTitCont().getTitImpVolo().getTitImpVoloNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-VOLO
            //             TO (SF)-IMP-VOLO(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpVolo().setWtitImpVolo(Trunc.toDecimal(ws.getTitCont().getTitImpVolo().getTitImpVolo(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-MANFEE-ANTIC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-MANFEE-ANTIC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotManfeeAntic().getTitTotManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-MANFEE-ANTIC-NULL
            //             TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotManfeeAntic().setWtitTotManfeeAnticNull(ws.getTitCont().getTitTotManfeeAntic().getTitTotManfeeAnticNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-MANFEE-ANTIC
            //             TO (SF)-TOT-MANFEE-ANTIC(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotManfeeAntic().setWtitTotManfeeAntic(Trunc.toDecimal(ws.getTitCont().getTitTotManfeeAntic().getTitTotManfeeAntic(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-MANFEE-RICOR-NULL = HIGH-VALUES
        //                TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-MANFEE-RICOR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotManfeeRicor().getTitTotManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-MANFEE-RICOR-NULL
            //             TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotManfeeRicor().setWtitTotManfeeRicorNull(ws.getTitCont().getTitTotManfeeRicor().getTitTotManfeeRicorNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-MANFEE-RICOR
            //             TO (SF)-TOT-MANFEE-RICOR(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotManfeeRicor().setWtitTotManfeeRicor(Trunc.toDecimal(ws.getTitCont().getTitTotManfeeRicor().getTitTotManfeeRicor(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-MANFEE-REC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-MANFEE-REC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotManfeeRec().getTitTotManfeeRecNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-MANFEE-REC-NULL
            //             TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotManfeeRec().setWtitTotManfeeRecNull(ws.getTitCont().getTitTotManfeeRec().getTitTotManfeeRecNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-MANFEE-REC
            //             TO (SF)-TOT-MANFEE-REC(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotManfeeRec().setWtitTotManfeeRec(Trunc.toDecimal(ws.getTitCont().getTitTotManfeeRec().getTitTotManfeeRec(), 15, 3));
        }
        // COB_CODE: IF TIT-TP-MEZ-PAG-ADD-NULL = HIGH-VALUES
        //                TO (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-MEZ-PAG-ADD(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpMezPagAddFormatted())) {
            // COB_CODE: MOVE TIT-TP-MEZ-PAG-ADD-NULL
            //             TO (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpMezPagAdd(ws.getTitCont().getTitTpMezPagAdd());
        }
        else {
            // COB_CODE: MOVE TIT-TP-MEZ-PAG-ADD
            //             TO (SF)-TP-MEZ-PAG-ADD(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpMezPagAdd(ws.getTitCont().getTitTpMezPagAdd());
        }
        // COB_CODE: IF TIT-ESTR-CNT-CORR-ADD-NULL = HIGH-VALUES
        //                TO (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-ESTR-CNT-CORR-ADD(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitEstrCntCorrAddFormatted())) {
            // COB_CODE: MOVE TIT-ESTR-CNT-CORR-ADD-NULL
            //             TO (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitEstrCntCorrAdd(ws.getTitCont().getTitEstrCntCorrAdd());
        }
        else {
            // COB_CODE: MOVE TIT-ESTR-CNT-CORR-ADD
            //             TO (SF)-ESTR-CNT-CORR-ADD(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitEstrCntCorrAdd(ws.getTitCont().getTitEstrCntCorrAdd());
        }
        // COB_CODE: IF TIT-DT-VLT-NULL = HIGH-VALUES
        //                TO (SF)-DT-VLT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-VLT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtVlt().getTitDtVltNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-VLT-NULL
            //             TO (SF)-DT-VLT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtVlt().setWtitDtVltNull(ws.getTitCont().getTitDtVlt().getTitDtVltNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-VLT
            //             TO (SF)-DT-VLT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtVlt().setWtitDtVlt(ws.getTitCont().getTitDtVlt().getTitDtVlt());
        }
        // COB_CODE: IF TIT-FL-FORZ-DT-VLT-NULL = HIGH-VALUES
        //                TO (SF)-FL-FORZ-DT-VLT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-FORZ-DT-VLT(IX-TAB-TIT)
        //           END-IF
        if (Conditions.eq(ws.getTitCont().getTitFlForzDtVlt(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-FORZ-DT-VLT-NULL
            //             TO (SF)-FL-FORZ-DT-VLT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlForzDtVlt(ws.getTitCont().getTitFlForzDtVlt());
        }
        else {
            // COB_CODE: MOVE TIT-FL-FORZ-DT-VLT
            //             TO (SF)-FL-FORZ-DT-VLT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlForzDtVlt(ws.getTitCont().getTitFlForzDtVlt());
        }
        // COB_CODE: IF TIT-DT-CAMBIO-VLT-NULL = HIGH-VALUES
        //                TO (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-CAMBIO-VLT(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtCambioVlt().getTitDtCambioVltNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-CAMBIO-VLT-NULL
            //             TO (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtCambioVlt().setWtitDtCambioVltNull(ws.getTitCont().getTitDtCambioVlt().getTitDtCambioVltNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-CAMBIO-VLT
            //             TO (SF)-DT-CAMBIO-VLT(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtCambioVlt().setWtitDtCambioVlt(ws.getTitCont().getTitDtCambioVlt().getTitDtCambioVlt());
        }
        // COB_CODE: IF TIT-TOT-SPE-AGE-NULL = HIGH-VALUES
        //                TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-SPE-AGE(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotSpeAge().getTitTotSpeAgeNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-SPE-AGE-NULL
            //             TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSpeAge().setWtitTotSpeAgeNull(ws.getTitCont().getTitTotSpeAge().getTitTotSpeAgeNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-SPE-AGE
            //             TO (SF)-TOT-SPE-AGE(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSpeAge().setWtitTotSpeAge(Trunc.toDecimal(ws.getTitCont().getTitTotSpeAge().getTitTotSpeAge(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-CAR-IAS-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CAR-IAS(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCarIas().getTitTotCarIasNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CAR-IAS-NULL
            //             TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarIas().setWtitTotCarIasNull(ws.getTitCont().getTitTotCarIas().getTitTotCarIasNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CAR-IAS
            //             TO (SF)-TOT-CAR-IAS(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarIas().setWtitTotCarIas(Trunc.toDecimal(ws.getTitCont().getTitTotCarIas().getTitTotCarIas(), 15, 3));
        }
        // COB_CODE: IF TIT-NUM-RAT-ACCORPATE-NULL = HIGH-VALUES
        //                TO (SF)-NUM-RAT-ACCORPATE-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-NUM-RAT-ACCORPATE(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitNumRatAccorpate().getTitNumRatAccorpateNullFormatted())) {
            // COB_CODE: MOVE TIT-NUM-RAT-ACCORPATE-NULL
            //             TO (SF)-NUM-RAT-ACCORPATE-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitNumRatAccorpate().setWtitNumRatAccorpateNull(ws.getTitCont().getTitNumRatAccorpate().getTitNumRatAccorpateNull());
        }
        else {
            // COB_CODE: MOVE TIT-NUM-RAT-ACCORPATE
            //             TO (SF)-NUM-RAT-ACCORPATE(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitNumRatAccorpate().setWtitNumRatAccorpate(ws.getTitCont().getTitNumRatAccorpate().getTitNumRatAccorpate());
        }
        // COB_CODE: MOVE TIT-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitDsRiga(ws.getTitCont().getTitDsRiga());
        // COB_CODE: MOVE TIT-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitDsOperSql(ws.getTitCont().getTitDsOperSql());
        // COB_CODE: MOVE TIT-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitDsVer(ws.getTitCont().getTitDsVer());
        // COB_CODE: MOVE TIT-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitDsTsIniCptz(ws.getTitCont().getTitDsTsIniCptz());
        // COB_CODE: MOVE TIT-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitDsTsEndCptz(ws.getTitCont().getTitDsTsEndCptz());
        // COB_CODE: MOVE TIT-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitDsUtente(ws.getTitCont().getTitDsUtente());
        // COB_CODE: MOVE TIT-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitDsStatoElab(ws.getTitCont().getTitDsStatoElab());
        // COB_CODE: IF TIT-FL-TIT-DA-REINVST-NULL = HIGH-VALUES
        //                TO (SF)-FL-TIT-DA-REINVST-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-TIT-DA-REINVST(IX-TAB-TIT)
        //           END-IF
        if (Conditions.eq(ws.getTitCont().getTitFlTitDaReinvst(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-TIT-DA-REINVST-NULL
            //             TO (SF)-FL-TIT-DA-REINVST-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlTitDaReinvst(ws.getTitCont().getTitFlTitDaReinvst());
        }
        else {
            // COB_CODE: MOVE TIT-FL-TIT-DA-REINVST
            //             TO (SF)-FL-TIT-DA-REINVST(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlTitDaReinvst(ws.getTitCont().getTitFlTitDaReinvst());
        }
        // COB_CODE: IF TIT-DT-RICH-ADD-RID-NULL = HIGH-VALUES
        //                TO (SF)-DT-RICH-ADD-RID-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-RICH-ADD-RID(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtRichAddRid().getTitDtRichAddRidNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-RICH-ADD-RID-NULL
            //             TO (SF)-DT-RICH-ADD-RID-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtRichAddRid().setWtitDtRichAddRidNull(ws.getTitCont().getTitDtRichAddRid().getTitDtRichAddRidNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-RICH-ADD-RID
            //             TO (SF)-DT-RICH-ADD-RID(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtRichAddRid().setWtitDtRichAddRid(ws.getTitCont().getTitDtRichAddRid().getTitDtRichAddRid());
        }
        // COB_CODE: IF TIT-TP-ESI-RID-NULL = HIGH-VALUES
        //                TO (SF)-TP-ESI-RID-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-ESI-RID(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpEsiRidFormatted())) {
            // COB_CODE: MOVE TIT-TP-ESI-RID-NULL
            //             TO (SF)-TP-ESI-RID-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpEsiRid(ws.getTitCont().getTitTpEsiRid());
        }
        else {
            // COB_CODE: MOVE TIT-TP-ESI-RID
            //             TO (SF)-TP-ESI-RID(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpEsiRid(ws.getTitCont().getTitTpEsiRid());
        }
        // COB_CODE: IF TIT-COD-IBAN-NULL = HIGH-VALUES
        //                TO (SF)-COD-IBAN-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-COD-IBAN(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitCodIban(), TitContIdbstit0.Len.TIT_COD_IBAN)) {
            // COB_CODE: MOVE TIT-COD-IBAN-NULL
            //             TO (SF)-COD-IBAN-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitCodIban(ws.getTitCont().getTitCodIban());
        }
        else {
            // COB_CODE: MOVE TIT-COD-IBAN
            //             TO (SF)-COD-IBAN(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitCodIban(ws.getTitCont().getTitCodIban());
        }
        // COB_CODE: IF TIT-IMP-TRASFE-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TRASFE-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-TRASFE(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpTrasfe().getTitImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-TRASFE-NULL
            //             TO (SF)-IMP-TRASFE-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpTrasfe().setWtitImpTrasfeNull(ws.getTitCont().getTitImpTrasfe().getTitImpTrasfeNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-TRASFE
            //             TO (SF)-IMP-TRASFE(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpTrasfe().setWtitImpTrasfe(Trunc.toDecimal(ws.getTitCont().getTitImpTrasfe().getTitImpTrasfe(), 15, 3));
        }
        // COB_CODE: IF TIT-IMP-TFR-STRC-NULL = HIGH-VALUES
        //                TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-IMP-TFR-STRC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitImpTfrStrc().getTitImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE TIT-IMP-TFR-STRC-NULL
            //             TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpTfrStrc().setWtitImpTfrStrcNull(ws.getTitCont().getTitImpTfrStrc().getTitImpTfrStrcNull());
        }
        else {
            // COB_CODE: MOVE TIT-IMP-TFR-STRC
            //             TO (SF)-IMP-TFR-STRC(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpTfrStrc().setWtitImpTfrStrc(Trunc.toDecimal(ws.getTitCont().getTitImpTfrStrc().getTitImpTfrStrc(), 15, 3));
        }
        // COB_CODE: IF TIT-DT-CERT-FISC-NULL = HIGH-VALUES
        //                TO (SF)-DT-CERT-FISC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-DT-CERT-FISC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtCertFisc().getTitDtCertFiscNullFormatted())) {
            // COB_CODE: MOVE TIT-DT-CERT-FISC-NULL
            //             TO (SF)-DT-CERT-FISC-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtCertFisc().setWtitDtCertFiscNull(ws.getTitCont().getTitDtCertFisc().getTitDtCertFiscNull());
        }
        else {
            // COB_CODE: MOVE TIT-DT-CERT-FISC
            //             TO (SF)-DT-CERT-FISC(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtCertFisc().setWtitDtCertFisc(ws.getTitCont().getTitDtCertFisc().getTitDtCertFisc());
        }
        // COB_CODE: IF TIT-TP-CAUS-STOR-NULL = HIGH-VALUES
        //                TO (SF)-TP-CAUS-STOR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-CAUS-STOR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpCausStor().getTitTpCausStorNullFormatted())) {
            // COB_CODE: MOVE TIT-TP-CAUS-STOR-NULL
            //             TO (SF)-TP-CAUS-STOR-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTpCausStor().setWtitTpCausStorNull(ws.getTitCont().getTitTpCausStor().getTitTpCausStorNull());
        }
        else {
            // COB_CODE: MOVE TIT-TP-CAUS-STOR
            //             TO (SF)-TP-CAUS-STOR(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTpCausStor().setWtitTpCausStor(ws.getTitCont().getTitTpCausStor().getTitTpCausStor());
        }
        // COB_CODE: IF TIT-TP-CAUS-DISP-STOR-NULL = HIGH-VALUES
        //                TO (SF)-TP-CAUS-DISP-STOR-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-CAUS-DISP-STOR(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpCausDispStorFormatted())) {
            // COB_CODE: MOVE TIT-TP-CAUS-DISP-STOR-NULL
            //             TO (SF)-TP-CAUS-DISP-STOR-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpCausDispStor(ws.getTitCont().getTitTpCausDispStor());
        }
        else {
            // COB_CODE: MOVE TIT-TP-CAUS-DISP-STOR
            //             TO (SF)-TP-CAUS-DISP-STOR(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpCausDispStor(ws.getTitCont().getTitTpCausDispStor());
        }
        // COB_CODE: IF TIT-TP-TIT-MIGRAZ-NULL = HIGH-VALUES
        //                TO (SF)-TP-TIT-MIGRAZ-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-TIT-MIGRAZ(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpTitMigrazFormatted())) {
            // COB_CODE: MOVE TIT-TP-TIT-MIGRAZ-NULL
            //             TO (SF)-TP-TIT-MIGRAZ-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpTitMigraz(ws.getTitCont().getTitTpTitMigraz());
        }
        else {
            // COB_CODE: MOVE TIT-TP-TIT-MIGRAZ
            //             TO (SF)-TP-TIT-MIGRAZ(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpTitMigraz(ws.getTitCont().getTitTpTitMigraz());
        }
        // COB_CODE: IF TIT-TOT-ACQ-EXP-NULL = HIGH-VALUES
        //                TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-ACQ-EXP(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotAcqExp().getTitTotAcqExpNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-ACQ-EXP-NULL
            //             TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotAcqExp().setWtitTotAcqExpNull(ws.getTitCont().getTitTotAcqExp().getTitTotAcqExpNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-ACQ-EXP
            //             TO (SF)-TOT-ACQ-EXP(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotAcqExp().setWtitTotAcqExp(Trunc.toDecimal(ws.getTitCont().getTitTotAcqExp().getTitTotAcqExp(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-REMUN-ASS-NULL = HIGH-VALUES
        //                TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-REMUN-ASS(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotRemunAss().getTitTotRemunAssNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-REMUN-ASS-NULL
            //             TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotRemunAss().setWtitTotRemunAssNull(ws.getTitCont().getTitTotRemunAss().getTitTotRemunAssNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-REMUN-ASS
            //             TO (SF)-TOT-REMUN-ASS(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotRemunAss().setWtitTotRemunAss(Trunc.toDecimal(ws.getTitCont().getTitTotRemunAss().getTitTotRemunAss(), 15, 3));
        }
        // COB_CODE: IF TIT-TOT-COMMIS-INTER-NULL = HIGH-VALUES
        //                TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-COMMIS-INTER(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCommisInter().getTitTotCommisInterNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-COMMIS-INTER-NULL
            //             TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCommisInter().setWtitTotCommisInterNull(ws.getTitCont().getTitTotCommisInter().getTitTotCommisInterNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-COMMIS-INTER
            //             TO (SF)-TOT-COMMIS-INTER(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCommisInter().setWtitTotCommisInter(Trunc.toDecimal(ws.getTitCont().getTitTotCommisInter().getTitTotCommisInter(), 15, 3));
        }
        // COB_CODE: IF TIT-TP-CAUS-RIMB-NULL = HIGH-VALUES
        //                TO (SF)-TP-CAUS-RIMB-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TP-CAUS-RIMB(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTpCausRimbFormatted())) {
            // COB_CODE: MOVE TIT-TP-CAUS-RIMB-NULL
            //             TO (SF)-TP-CAUS-RIMB-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpCausRimb(ws.getTitCont().getTitTpCausRimb());
        }
        else {
            // COB_CODE: MOVE TIT-TP-CAUS-RIMB
            //             TO (SF)-TP-CAUS-RIMB(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpCausRimb(ws.getTitCont().getTitTpCausRimb());
        }
        // COB_CODE: IF TIT-TOT-CNBT-ANTIRAC-NULL = HIGH-VALUES
        //                TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TIT)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitTotCnbtAntirac().getTitTotCnbtAntiracNullFormatted())) {
            // COB_CODE: MOVE TIT-TOT-CNBT-ANTIRAC-NULL
            //             TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCnbtAntirac().setWtitTotCnbtAntiracNull(ws.getTitCont().getTitTotCnbtAntirac().getTitTotCnbtAntiracNull());
        }
        else {
            // COB_CODE: MOVE TIT-TOT-CNBT-ANTIRAC
            //             TO (SF)-TOT-CNBT-ANTIRAC(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCnbtAntirac().setWtitTotCnbtAntirac(Trunc.toDecimal(ws.getTitCont().getTitTotCnbtAntirac().getTitTotCnbtAntirac(), 15, 3));
        }
        // COB_CODE: IF TIT-FL-INC-AUTOGEN-NULL = HIGH-VALUES
        //                TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TIT)
        //           ELSE
        //                TO (SF)-FL-INC-AUTOGEN(IX-TAB-TIT)
        //           END-IF.
        if (Conditions.eq(ws.getTitCont().getTitFlIncAutogen(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE TIT-FL-INC-AUTOGEN-NULL
            //             TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlIncAutogen(ws.getTitCont().getTitFlIncAutogen());
        }
        else {
            // COB_CODE: MOVE TIT-FL-INC-AUTOGEN
            //             TO (SF)-FL-INC-AUTOGEN(IX-TAB-TIT)
            ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlIncAutogen(ws.getTitCont().getTitFlIncAutogen());
        }
    }

    /**Original name: INIZIA-TOT-TIT<br>
	 * <pre>------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVTIT4
	 *    ULTIMO AGG. 05 DIC 2014
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotTit() {
        // COB_CODE: PERFORM INIZIA-ZEROES-TIT THRU INIZIA-ZEROES-TIT-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVTIT4:line=10, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-SPACES-TIT THRU INIZIA-SPACES-TIT-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVTIT4:line=12, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-NULL-TIT THRU INIZIA-NULL-TIT-EX.
        iniziaNullTit();
    }

    /**Original name: INIZIA-NULL-TIT<br>*/
    private void iniziaNullTit() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IB-RICH-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitIbRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDati.Len.WTIT_IB_RICH));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitIdMoviChiu().setWtitIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitIdMoviChiu.Len.WTIT_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PROG-TIT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitProgTit().setWtitProgTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitProgTit.Len.WTIT_PROG_TIT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-INI-COP-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtIniCop().setWtitDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDtIniCop.Len.WTIT_DT_INI_COP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-END-COP-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtEndCop().setWtitDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDtEndCop.Len.WTIT_DT_END_COP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-PAG-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpPag().setWtitImpPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitImpPag.Len.WTIT_IMP_PAG_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-SOLL-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlSoll(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FRAZ-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitFraz().setWtitFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitFraz.Len.WTIT_FRAZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-APPLZ-MORA-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtApplzMora().setWtitDtApplzMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDtApplzMora.Len.WTIT_DT_APPLZ_MORA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-MORA-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlMora(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-RAPP-RETE-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitIdRappRete().setWtitIdRappReteNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitIdRappRete.Len.WTIT_ID_RAPP_RETE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-RAPP-ANA-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitIdRappAna().setWtitIdRappAnaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitIdRappAna.Len.WTIT_ID_RAPP_ANA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-DVS-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDati.Len.WTIT_COD_DVS));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-EMIS-TIT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtEmisTit().setWtitDtEmisTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDtEmisTit.Len.WTIT_DT_EMIS_TIT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-ESI-TIT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtEsiTit().setWtitDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDtEsiTit.Len.WTIT_DT_ESI_TIT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PRE-NET-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPreNet().setWtitTotPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotPreNet.Len.WTIT_TOT_PRE_NET_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-FRAZ-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrFraz().setWtitTotIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotIntrFraz.Len.WTIT_TOT_INTR_FRAZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-MORA-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrMora().setWtitTotIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotIntrMora.Len.WTIT_TOT_INTR_MORA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-PREST-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrPrest().setWtitTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotIntrPrest.Len.WTIT_TOT_INTR_PREST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-RETDT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrRetdt().setWtitTotIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotIntrRetdt.Len.WTIT_TOT_INTR_RETDT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-INTR-RIAT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotIntrRiat().setWtitTotIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotIntrRiat.Len.WTIT_TOT_INTR_RIAT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-DIR-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotDir().setWtitTotDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotDir.Len.WTIT_TOT_DIR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SPE-MED-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSpeMed().setWtitTotSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotSpeMed.Len.WTIT_TOT_SPE_MED_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-TAX-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotTax().setWtitTotTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotTax.Len.WTIT_TOT_TAX_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-SAN-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprSan().setWtitTotSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotSoprSan.Len.WTIT_TOT_SOPR_SAN_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-TEC-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprTec().setWtitTotSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotSoprTec.Len.WTIT_TOT_SOPR_TEC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-SPO-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprSpo().setWtitTotSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotSoprSpo.Len.WTIT_TOT_SOPR_SPO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-PROF-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprProf().setWtitTotSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotSoprProf.Len.WTIT_TOT_SOPR_PROF_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SOPR-ALT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSoprAlt().setWtitTotSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotSoprAlt.Len.WTIT_TOT_SOPR_ALT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PRE-TOT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPreTot().setWtitTotPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotPreTot.Len.WTIT_TOT_PRE_TOT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PRE-PP-IAS-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPrePpIas().setWtitTotPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotPrePpIas.Len.WTIT_TOT_PRE_PP_IAS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CAR-ACQ-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarAcq().setWtitTotCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotCarAcq.Len.WTIT_TOT_CAR_ACQ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CAR-GEST-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarGest().setWtitTotCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotCarGest.Len.WTIT_TOT_CAR_GEST_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CAR-INC-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarInc().setWtitTotCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotCarInc.Len.WTIT_TOT_CAR_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PRE-SOLO-RSH-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotPreSoloRsh().setWtitTotPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotPreSoloRsh.Len.WTIT_TOT_PRE_SOLO_RSH_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-ACQ-1AA-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvAcq1aa().setWtitTotProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotProvAcq1aa.Len.WTIT_TOT_PROV_ACQ1AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-ACQ-2AA-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvAcq2aa().setWtitTotProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotProvAcq2aa.Len.WTIT_TOT_PROV_ACQ2AA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-RICOR-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvRicor().setWtitTotProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotProvRicor.Len.WTIT_TOT_PROV_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-INC-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvInc().setWtitTotProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotProvInc.Len.WTIT_TOT_PROV_INC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-PROV-DA-REC-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotProvDaRec().setWtitTotProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotProvDaRec.Len.WTIT_TOT_PROV_DA_REC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-AZ-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpAz().setWtitImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitImpAz.Len.WTIT_IMP_AZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-ADER-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpAder().setWtitImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitImpAder.Len.WTIT_IMP_ADER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TFR-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpTfr().setWtitImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitImpTfr.Len.WTIT_IMP_TFR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-VOLO-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpVolo().setWtitImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitImpVolo.Len.WTIT_IMP_VOLO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-MANFEE-ANTIC-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotManfeeAntic().setWtitTotManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotManfeeAntic.Len.WTIT_TOT_MANFEE_ANTIC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-MANFEE-RICOR-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotManfeeRicor().setWtitTotManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotManfeeRicor.Len.WTIT_TOT_MANFEE_RICOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-MANFEE-REC-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotManfeeRec().setWtitTotManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotManfeeRec.Len.WTIT_TOT_MANFEE_REC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-MEZ-PAG-ADD-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpMezPagAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDati.Len.WTIT_TP_MEZ_PAG_ADD));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ESTR-CNT-CORR-ADD-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitEstrCntCorrAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDati.Len.WTIT_ESTR_CNT_CORR_ADD));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-VLT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtVlt().setWtitDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDtVlt.Len.WTIT_DT_VLT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-FORZ-DT-VLT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlForzDtVlt(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-CAMBIO-VLT-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtCambioVlt().setWtitDtCambioVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDtCambioVlt.Len.WTIT_DT_CAMBIO_VLT_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-SPE-AGE-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotSpeAge().setWtitTotSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotSpeAge.Len.WTIT_TOT_SPE_AGE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CAR-IAS-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCarIas().setWtitTotCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotCarIas.Len.WTIT_TOT_CAR_IAS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-NUM-RAT-ACCORPATE-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitNumRatAccorpate().setWtitNumRatAccorpateNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitNumRatAccorpate.Len.WTIT_NUM_RAT_ACCORPATE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-TIT-DA-REINVST-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlTitDaReinvst(Types.HIGH_CHAR_VAL);
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-RICH-ADD-RID-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtRichAddRid().setWtitDtRichAddRidNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDtRichAddRid.Len.WTIT_DT_RICH_ADD_RID_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-ESI-RID-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpEsiRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDati.Len.WTIT_TP_ESI_RID));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-IBAN-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitCodIban(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDati.Len.WTIT_COD_IBAN));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TRASFE-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpTrasfe().setWtitImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitImpTrasfe.Len.WTIT_IMP_TRASFE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-TFR-STRC-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitImpTfrStrc().setWtitImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitImpTfrStrc.Len.WTIT_IMP_TFR_STRC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-CERT-FISC-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitDtCertFisc().setWtitDtCertFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDtCertFisc.Len.WTIT_DT_CERT_FISC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-CAUS-STOR-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTpCausStor().setWtitTpCausStorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTpCausStor.Len.WTIT_TP_CAUS_STOR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-CAUS-DISP-STOR-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpCausDispStor(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDati.Len.WTIT_TP_CAUS_DISP_STOR));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-TIT-MIGRAZ-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpTitMigraz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDati.Len.WTIT_TP_TIT_MIGRAZ));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-ACQ-EXP-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotAcqExp().setWtitTotAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotAcqExp.Len.WTIT_TOT_ACQ_EXP_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-REMUN-ASS-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotRemunAss().setWtitTotRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotRemunAss.Len.WTIT_TOT_REMUN_ASS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-COMMIS-INTER-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCommisInter().setWtitTotCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotCommisInter.Len.WTIT_TOT_COMMIS_INTER_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TP-CAUS-RIMB-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitTpCausRimb(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitDati.Len.WTIT_TP_CAUS_RIMB));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-TOT-CNBT-ANTIRAC-NULL(IX-TAB-TIT)
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().getWtitTotCnbtAntirac().setWtitTotCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WtitTotCnbtAntirac.Len.WTIT_TOT_CNBT_ANTIRAC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-FL-INC-AUTOGEN-NULL(IX-TAB-TIT).
        ws.getDtitTabTitCont(ws.getIxIndici().getTabTit()).getLccvtit1().getDati().setWtitFlIncAutogen(Types.HIGH_CHAR_VAL);
    }

    public void initIxIndici() {
        ws.getIxIndici().setDclgen(((short)0));
        ws.getIxIndici().setTabAde(((short)0));
        ws.getIxIndici().setTabTit(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoAde() {
        ws.setDadeEleAdesMax(((short)0));
        ws.getLccvade1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvade1().setIdPtf(0);
        ws.getLccvade1().getDati().setWadeIdAdes(0);
        ws.getLccvade1().getDati().setWadeIdPoli(0);
        ws.getLccvade1().getDati().setWadeIdMoviCrz(0);
        ws.getLccvade1().getDati().getWadeIdMoviChiu().setWadeIdMoviChiu(0);
        ws.getLccvade1().getDati().setWadeDtIniEff(0);
        ws.getLccvade1().getDati().setWadeDtEndEff(0);
        ws.getLccvade1().getDati().setWadeIbPrev("");
        ws.getLccvade1().getDati().setWadeIbOgg("");
        ws.getLccvade1().getDati().setWadeCodCompAnia(0);
        ws.getLccvade1().getDati().getWadeDtDecor().setWadeDtDecor(0);
        ws.getLccvade1().getDati().getWadeDtScad().setWadeDtScad(0);
        ws.getLccvade1().getDati().getWadeEtaAScad().setWadeEtaAScad(0);
        ws.getLccvade1().getDati().getWadeDurAa().setWadeDurAa(0);
        ws.getLccvade1().getDati().getWadeDurMm().setWadeDurMm(0);
        ws.getLccvade1().getDati().getWadeDurGg().setWadeDurGg(0);
        ws.getLccvade1().getDati().setWadeTpRgmFisc("");
        ws.getLccvade1().getDati().setWadeTpRiat("");
        ws.getLccvade1().getDati().setWadeTpModPagTit("");
        ws.getLccvade1().getDati().setWadeTpIas("");
        ws.getLccvade1().getDati().getWadeDtVarzTpIas().setWadeDtVarzTpIas(0);
        ws.getLccvade1().getDati().getWadePreNetInd().setWadePreNetInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePreLrdInd().setWadePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeRatLrdInd().setWadeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePrstzIniInd().setWadePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().setWadeFlCoincAssto(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeIbDflt("");
        ws.getLccvade1().getDati().setWadeModCalc("");
        ws.getLccvade1().getDati().setWadeTpFntCnbtva("");
        ws.getLccvade1().getDati().getWadeImpAz().setWadeImpAz(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpAder().setWadeImpAder(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpTfr().setWadeImpTfr(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpVolo().setWadeImpVolo(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePcAz().setWadePcAz(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcAder().setWadePcAder(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcTfr().setWadePcTfr(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcVolo().setWadePcVolo(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadeDtNovaRgmFisc().setWadeDtNovaRgmFisc(0);
        ws.getLccvade1().getDati().setWadeFlAttiv(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeImpRecRitVis().setWadeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpRecRitAcc().setWadeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().setWadeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeImpbVisDaRec().setWadeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeDtDecorPrestBan().setWadeDtDecorPrestBan(0);
        ws.getLccvade1().getDati().getWadeDtEffVarzStatT().setWadeDtEffVarzStatT(0);
        ws.getLccvade1().getDati().setWadeDsRiga(0);
        ws.getLccvade1().getDati().setWadeDsOperSql(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeDsVer(0);
        ws.getLccvade1().getDati().setWadeDsTsIniCptz(0);
        ws.getLccvade1().getDati().setWadeDsTsEndCptz(0);
        ws.getLccvade1().getDati().setWadeDsUtente("");
        ws.getLccvade1().getDati().setWadeDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeCumCnbtCap().setWadeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpGarCnbt().setWadeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeDtUltConsCnbt().setWadeDtUltConsCnbt(0);
        ws.getLccvade1().getDati().setWadeIdenIscFnd("");
        ws.getLccvade1().getDati().getWadeNumRatPian().setWadeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getLccvade1().getDati().getWadeDtPresc().setWadeDtPresc(0);
        ws.getLccvade1().getDati().setWadeConcsPrest(Types.SPACE_CHAR);
    }

    public void initPrest() {
        ws.getPrest().setPreIdPrest(0);
        ws.getPrest().setPreIdOgg(0);
        ws.getPrest().setPreTpOgg("");
        ws.getPrest().setPreIdMoviCrz(0);
        ws.getPrest().getPreIdMoviChiu().setPreIdMoviChiu(0);
        ws.getPrest().setPreDtIniEff(0);
        ws.getPrest().setPreDtEndEff(0);
        ws.getPrest().setPreCodCompAnia(0);
        ws.getPrest().getPreDtConcsPrest().setPreDtConcsPrest(0);
        ws.getPrest().getPreDtDecorPrest().setPreDtDecorPrest(0);
        ws.getPrest().getPreImpPrest().setPreImpPrest(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreIntrPrest().setPreIntrPrest(new AfDecimal(0, 6, 3));
        ws.getPrest().setPreTpPrest("");
        ws.getPrest().getPreFrazPagIntr().setPreFrazPagIntr(0);
        ws.getPrest().getPreDtRimb().setPreDtRimb(0);
        ws.getPrest().getPreImpRimb().setPreImpRimb(new AfDecimal(0, 15, 3));
        ws.getPrest().setPreCodDvs("");
        ws.getPrest().setPreDtRichPrest(0);
        ws.getPrest().setPreModIntrPrest("");
        ws.getPrest().getPreSpePrest().setPreSpePrest(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreImpPrestLiqto().setPreImpPrestLiqto(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreSdoIntr().setPreSdoIntr(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreRimbEff().setPreRimbEff(new AfDecimal(0, 15, 3));
        ws.getPrest().getPrePrestResEff().setPrePrestResEff(new AfDecimal(0, 15, 3));
        ws.getPrest().setPreDsRiga(0);
        ws.getPrest().setPreDsOperSql(Types.SPACE_CHAR);
        ws.getPrest().setPreDsVer(0);
        ws.getPrest().setPreDsTsIniCptz(0);
        ws.getPrest().setPreDsTsEndCptz(0);
        ws.getPrest().setPreDsUtente("");
        ws.getPrest().setPreDsStatoElab(Types.SPACE_CHAR);
    }

    public void initTitCont() {
        ws.getTitCont().setTitIdTitCont(0);
        ws.getTitCont().setTitIdOgg(0);
        ws.getTitCont().setTitTpOgg("");
        ws.getTitCont().setTitIbRich("");
        ws.getTitCont().setTitIdMoviCrz(0);
        ws.getTitCont().getTitIdMoviChiu().setTitIdMoviChiu(0);
        ws.getTitCont().setTitDtIniEff(0);
        ws.getTitCont().setTitDtEndEff(0);
        ws.getTitCont().setTitCodCompAnia(0);
        ws.getTitCont().setTitTpTit("");
        ws.getTitCont().getTitProgTit().setTitProgTit(0);
        ws.getTitCont().setTitTpPreTit("");
        ws.getTitCont().setTitTpStatTit("");
        ws.getTitCont().getTitDtIniCop().setTitDtIniCop(0);
        ws.getTitCont().getTitDtEndCop().setTitDtEndCop(0);
        ws.getTitCont().getTitImpPag().setTitImpPag(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitFlSoll(Types.SPACE_CHAR);
        ws.getTitCont().getTitFraz().setTitFraz(0);
        ws.getTitCont().getTitDtApplzMora().setTitDtApplzMora(0);
        ws.getTitCont().setTitFlMora(Types.SPACE_CHAR);
        ws.getTitCont().getTitIdRappRete().setTitIdRappRete(0);
        ws.getTitCont().getTitIdRappAna().setTitIdRappAna(0);
        ws.getTitCont().setTitCodDvs("");
        ws.getTitCont().getTitDtEmisTit().setTitDtEmisTit(0);
        ws.getTitCont().getTitDtEsiTit().setTitDtEsiTit(0);
        ws.getTitCont().getTitTotPreNet().setTitTotPreNet(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrFraz().setTitTotIntrFraz(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrMora().setTitTotIntrMora(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrPrest().setTitTotIntrPrest(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrRetdt().setTitTotIntrRetdt(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotIntrRiat().setTitTotIntrRiat(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotDir().setTitTotDir(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSpeMed().setTitTotSpeMed(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotTax().setTitTotTax(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprSan().setTitTotSoprSan(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprTec().setTitTotSoprTec(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprSpo().setTitTotSoprSpo(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprProf().setTitTotSoprProf(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotSoprAlt().setTitTotSoprAlt(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotPreTot().setTitTotPreTot(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotPrePpIas().setTitTotPrePpIas(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarAcq().setTitTotCarAcq(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarGest().setTitTotCarGest(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarInc().setTitTotCarInc(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotPreSoloRsh().setTitTotPreSoloRsh(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvAcq1aa().setTitTotProvAcq1aa(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvAcq2aa().setTitTotProvAcq2aa(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvRicor().setTitTotProvRicor(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvInc().setTitTotProvInc(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotProvDaRec().setTitTotProvDaRec(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpAz().setTitImpAz(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpAder().setTitImpAder(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpTfr().setTitImpTfr(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpVolo().setTitImpVolo(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotManfeeAntic().setTitTotManfeeAntic(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotManfeeRicor().setTitTotManfeeRicor(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotManfeeRec().setTitTotManfeeRec(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitTpMezPagAdd("");
        ws.getTitCont().setTitEstrCntCorrAdd("");
        ws.getTitCont().getTitDtVlt().setTitDtVlt(0);
        ws.getTitCont().setTitFlForzDtVlt(Types.SPACE_CHAR);
        ws.getTitCont().getTitDtCambioVlt().setTitDtCambioVlt(0);
        ws.getTitCont().getTitTotSpeAge().setTitTotSpeAge(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCarIas().setTitTotCarIas(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitNumRatAccorpate().setTitNumRatAccorpate(0);
        ws.getTitCont().setTitDsRiga(0);
        ws.getTitCont().setTitDsOperSql(Types.SPACE_CHAR);
        ws.getTitCont().setTitDsVer(0);
        ws.getTitCont().setTitDsTsIniCptz(0);
        ws.getTitCont().setTitDsTsEndCptz(0);
        ws.getTitCont().setTitDsUtente("");
        ws.getTitCont().setTitDsStatoElab(Types.SPACE_CHAR);
        ws.getTitCont().setTitFlTitDaReinvst(Types.SPACE_CHAR);
        ws.getTitCont().getTitDtRichAddRid().setTitDtRichAddRid(0);
        ws.getTitCont().setTitTpEsiRid("");
        ws.getTitCont().setTitCodIban("");
        ws.getTitCont().getTitImpTrasfe().setTitImpTrasfe(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitImpTfrStrc().setTitImpTfrStrc(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitDtCertFisc().setTitDtCertFisc(0);
        ws.getTitCont().getTitTpCausStor().setTitTpCausStor(0);
        ws.getTitCont().setTitTpCausDispStor("");
        ws.getTitCont().setTitTpTitMigraz("");
        ws.getTitCont().getTitTotAcqExp().setTitTotAcqExp(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotRemunAss().setTitTotRemunAss(new AfDecimal(0, 15, 3));
        ws.getTitCont().getTitTotCommisInter().setTitTotCommisInter(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitTpCausRimb("");
        ws.getTitCont().getTitTotCnbtAntirac().setTitTotCnbtAntirac(new AfDecimal(0, 15, 3));
        ws.getTitCont().setTitFlIncAutogen(Types.SPACE_CHAR);
    }
}
