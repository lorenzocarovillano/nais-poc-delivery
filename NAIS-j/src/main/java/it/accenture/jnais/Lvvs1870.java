package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs1870Data;

/**Original name: LVVS1870<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS1870
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... IMPOSTA SOSTITUTIVA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs1870 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs1870Data ws = new Lvvs1870Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS1870
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS1870_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs1870 getInstance() {
        return ((Lvvs1870)Programs.getInstance(Lvvs1870.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-ISO.
        initAreaIoIso();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformVaryingStmtImpl @source=LVVS1870.cbl:line=131, because the code is unreachable.
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
        //--> DCLGEN DI WORKING
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1250-CALCOLA-ULTRISMATNET       THRU S1250-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1250-CALCOLA-ULTRISMATNET       THRU S1250-EX
            s1250CalcolaUltrismatnet();
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: S1250-CALCOLA-ULTRISMATNET<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaUltrismatnet() {
        Ldbs3990 ldbs3990 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'LDBS3990'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS3990");
        //
        // COB_CODE: INITIALIZE IMPST-SOST.
        initImpstSost();
        // COB_CODE: MOVE IVVC0213-ID-LIVELLO TO ISO-ID-OGG
        ws.getImpstSost().getIsoIdOgg().setIsoIdOgg(ivvc0213.getDatiLivello().getIdLivello());
        // COB_CODE: MOVE 'TG'                TO ISO-TP-OGG
        ws.getImpstSost().setIsoTpOgg("TG");
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 IMPST-SOST
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs3990 = Ldbs3990.getInstance();
            ldbs3990.run(idsv0003, ws.getImpstSost());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS3990 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS3990 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //                 MOVE ISO-RIS-MAT-POST-TAX TO IVVC0213-VAL-IMP-O
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE ISO-RIS-MAT-POST-TAX TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getImpstSost().getIsoRisMatPostTax().getIsoRisMatPostTax(), 18, 7));
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS3990 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS3990 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabIso(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoIso() {
        ws.setDisoEleIsoMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs1870Data.DISO_TAB_ISO_MAXOCCURS; idx0++) {
            ws.getDisoTabIso(idx0).getLccviso1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDisoTabIso(idx0).getLccviso1().setIdPtf(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoIdImpstSost(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoIdOgg().setWisoIdOgg(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoTpOgg("");
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoIdMoviCrz(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoIdMoviChiu().setWisoIdMoviChiu(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDtIniEff(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDtEndEff(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoDtIniPer().setWisoDtIniPer(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoDtEndPer().setWisoDtEndPer(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoCodCompAnia(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoImpstSost().setWisoImpstSost(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoImpbIs().setWisoImpbIs(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoAlqIs().setWisoAlqIs(new AfDecimal(0, 6, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoCodTrb("");
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoPrstzLrdAnteIs().setWisoPrstzLrdAnteIs(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoRisMatNetPrec().setWisoRisMatNetPrec(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoRisMatAnteTax().setWisoRisMatAnteTax(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoRisMatPostTax().setWisoRisMatPostTax(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoPrstzNet().setWisoPrstzNet(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoPrstzPrec().setWisoPrstzPrec(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().getWisoCumPreVers().setWisoCumPreVers(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoTpCalcImpst("");
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoImpGiaTassato(new AfDecimal(0, 15, 3));
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsRiga(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsOperSql(Types.SPACE_CHAR);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsVer(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsTsIniCptz(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsTsEndCptz(0);
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsUtente("");
            ws.getDisoTabIso(idx0).getLccviso1().getDati().setWisoDsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initImpstSost() {
        ws.getImpstSost().setIsoIdImpstSost(0);
        ws.getImpstSost().getIsoIdOgg().setIsoIdOgg(0);
        ws.getImpstSost().setIsoTpOgg("");
        ws.getImpstSost().setIsoIdMoviCrz(0);
        ws.getImpstSost().getIsoIdMoviChiu().setIsoIdMoviChiu(0);
        ws.getImpstSost().setIsoDtIniEff(0);
        ws.getImpstSost().setIsoDtEndEff(0);
        ws.getImpstSost().getIsoDtIniPer().setIsoDtIniPer(0);
        ws.getImpstSost().getIsoDtEndPer().setIsoDtEndPer(0);
        ws.getImpstSost().setIsoCodCompAnia(0);
        ws.getImpstSost().getIsoImpstSost().setIsoImpstSost(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoImpbIs().setIsoImpbIs(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoAlqIs().setIsoAlqIs(new AfDecimal(0, 6, 3));
        ws.getImpstSost().setIsoCodTrb("");
        ws.getImpstSost().getIsoPrstzLrdAnteIs().setIsoPrstzLrdAnteIs(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoRisMatNetPrec().setIsoRisMatNetPrec(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoRisMatAnteTax().setIsoRisMatAnteTax(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoRisMatPostTax().setIsoRisMatPostTax(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoPrstzNet().setIsoPrstzNet(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoPrstzPrec().setIsoPrstzPrec(new AfDecimal(0, 15, 3));
        ws.getImpstSost().getIsoCumPreVers().setIsoCumPreVers(new AfDecimal(0, 15, 3));
        ws.getImpstSost().setIsoTpCalcImpst("");
        ws.getImpstSost().setIsoImpGiaTassato(new AfDecimal(0, 15, 3));
        ws.getImpstSost().setIsoDsRiga(0);
        ws.getImpstSost().setIsoDsOperSql(Types.SPACE_CHAR);
        ws.getImpstSost().setIsoDsVer(0);
        ws.getImpstSost().setIsoDsTsIniCptz(0);
        ws.getImpstSost().setIsoDsTsEndCptz(0);
        ws.getImpstSost().setIsoDsUtente("");
        ws.getImpstSost().setIsoDsStatoElab(Types.SPACE_CHAR);
    }
}
