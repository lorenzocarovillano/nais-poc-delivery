package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.VarFunzDiCalcRDao;
import it.accenture.jnais.commons.data.to.IVarFunzDiCalcR;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs8800Data;
import it.accenture.jnais.ws.redefines.Ac5Isprecalc;
import it.accenture.jnais.ws.VarFunzDiCalcR;

/**Original name: LDBS8800<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 OTT 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs8800 extends Program implements IVarFunzDiCalcR {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private VarFunzDiCalcRDao varFunzDiCalcRDao = new VarFunzDiCalcRDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs8800Data ws = new Ldbs8800Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: VAR-FUNZ-DI-CALC-R
    private VarFunzDiCalcR varFunzDiCalcR;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS8800_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, VarFunzDiCalcR varFunzDiCalcR) {
        this.idsv0003 = idsv0003;
        this.varFunzDiCalcR = varFunzDiCalcR;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs8800 getInstance() {
        return ((Ldbs8800)Programs.getInstance(Ldbs8800.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS8800'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS8800");
        // COB_CODE: MOVE 'VAR-FUNZ-DI-CALC-R' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("VAR-FUNZ-DI-CALC-R");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-NST CURSOR FOR
        //              SELECT
        //                     IDCOMP
        //                    ,CODPROD
        //                    ,CODTARI
        //                    ,DINIZ
        //                    ,NOMEFUNZ
        //                    ,DEND
        //                    ,ISPRECALC
        //                    ,NOMEPROGR
        //                    ,MODCALC
        //                    ,GLOVARLIST
        //                    ,STEP_ELAB
        //              FROM VAR_FUNZ_DI_CALC_R
        //              WHERE      IDCOMP    = :AC5-IDCOMP
        //                        AND CODPROD   = :AC5-CODPROD-VCHAR
        //                        AND CODTARI   = :AC5-CODTARI-VCHAR
        //                        AND DINIZ    <= :AC5-DINIZ
        //                        AND DEND     >= :AC5-DINIZ
        //                        AND NOMEFUNZ  = :AC5-NOMEFUNZ-VCHAR
        //                        AND STEP_ELAB = :AC5-STEP-ELAB-VCHAR
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     IDCOMP
        //                    ,CODPROD
        //                    ,CODTARI
        //                    ,DINIZ
        //                    ,NOMEFUNZ
        //                    ,DEND
        //                    ,ISPRECALC
        //                    ,NOMEPROGR
        //                    ,MODCALC
        //                    ,GLOVARLIST
        //                    ,STEP_ELAB
        //             INTO
        //                :AC5-IDCOMP
        //               ,:AC5-CODPROD-VCHAR
        //               ,:AC5-CODTARI-VCHAR
        //               ,:AC5-DINIZ
        //               ,:AC5-NOMEFUNZ-VCHAR
        //               ,:AC5-DEND
        //               ,:AC5-ISPRECALC
        //                :IND-AC5-ISPRECALC
        //               ,:AC5-NOMEPROGR-VCHAR
        //                :IND-AC5-NOMEPROGR
        //               ,:AC5-MODCALC-VCHAR
        //                :IND-AC5-MODCALC
        //               ,:AC5-GLOVARLIST-VCHAR
        //                :IND-AC5-GLOVARLIST
        //               ,:AC5-STEP-ELAB-VCHAR
        //             FROM VAR_FUNZ_DI_CALC_R
        //             WHERE      IDCOMP    = :AC5-IDCOMP
        //                    AND CODPROD   = :AC5-CODPROD-VCHAR
        //                    AND CODTARI   = :AC5-CODTARI-VCHAR
        //                    AND DINIZ    <= :AC5-DINIZ
        //                    AND DEND     >= :AC5-DINIZ
        //                    AND NOMEFUNZ  = :AC5-NOMEFUNZ-VCHAR
        //                    AND STEP_ELAB = :AC5-STEP-ELAB-VCHAR
        //           END-EXEC.
        varFunzDiCalcRDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: EXEC SQL
        //                OPEN C-NST
        //           END-EXEC.
        varFunzDiCalcRDao.openCNst20(varFunzDiCalcR);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-NST
        //           END-EXEC.
        varFunzDiCalcRDao.closeCNst20();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: EXEC SQL
        //                FETCH C-NST
        //           INTO
        //                :AC5-IDCOMP
        //               ,:AC5-CODPROD-VCHAR
        //               ,:AC5-CODTARI-VCHAR
        //               ,:AC5-DINIZ
        //               ,:AC5-NOMEFUNZ-VCHAR
        //               ,:AC5-DEND
        //               ,:AC5-ISPRECALC
        //                :IND-AC5-ISPRECALC
        //               ,:AC5-NOMEPROGR-VCHAR
        //                :IND-AC5-NOMEPROGR
        //               ,:AC5-MODCALC-VCHAR
        //                :IND-AC5-MODCALC
        //               ,:AC5-GLOVARLIST-VCHAR
        //                :IND-AC5-GLOVARLIST
        //               ,:AC5-STEP-ELAB-VCHAR
        //           END-EXEC.
        varFunzDiCalcRDao.fetchCNst20(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST THRU C270-EX
            c270CloseCursorWcNst();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-AC5-ISPRECALC = -1
        //              MOVE HIGH-VALUES TO AC5-ISPRECALC-NULL
        //           END-IF
        if (ws.getIndVarFunzDiCalcR().getCodBlocco() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO AC5-ISPRECALC-NULL
            varFunzDiCalcR.getAc5Isprecalc().setAc5IsprecalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ac5Isprecalc.Len.AC5_ISPRECALC_NULL));
        }
        // COB_CODE: IF IND-AC5-NOMEPROGR = -1
        //              MOVE HIGH-VALUES TO AC5-NOMEPROGR
        //           END-IF
        if (ws.getIndVarFunzDiCalcR().getSrvzVerAnn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO AC5-NOMEPROGR
            varFunzDiCalcR.setAc5Nomeprogr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VarFunzDiCalcR.Len.AC5_NOMEPROGR));
        }
        // COB_CODE: IF IND-AC5-MODCALC = -1
        //              MOVE HIGH-VALUES TO AC5-MODCALC
        //           END-IF
        if (ws.getIndVarFunzDiCalcR().getWhereCondition() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO AC5-MODCALC
            varFunzDiCalcR.setAc5Modcalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VarFunzDiCalcR.Len.AC5_MODCALC));
        }
        // COB_CODE: IF IND-AC5-GLOVARLIST = -1
        //              MOVE HIGH-VALUES TO AC5-GLOVARLIST
        //           END-IF.
        if (ws.getIndVarFunzDiCalcR().getFlPoliIfp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO AC5-GLOVARLIST
            varFunzDiCalcR.setAc5Glovarlist(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VarFunzDiCalcR.Len.AC5_GLOVARLIST));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF AC5-CODPROD
        //                       TO AC5-CODPROD-LEN
        varFunzDiCalcR.setAc5CodprodLen(((short)VarFunzDiCalcR.Len.AC5_CODPROD));
        // COB_CODE: MOVE LENGTH OF AC5-CODTARI
        //                       TO AC5-CODTARI-LEN
        varFunzDiCalcR.setAc5CodtariLen(((short)VarFunzDiCalcR.Len.AC5_CODTARI));
        // COB_CODE: MOVE LENGTH OF AC5-NOMEFUNZ
        //                       TO AC5-NOMEFUNZ-LEN
        varFunzDiCalcR.setAc5NomefunzLen(((short)VarFunzDiCalcR.Len.AC5_NOMEFUNZ));
        // COB_CODE: MOVE LENGTH OF AC5-NOMEPROGR
        //                       TO AC5-NOMEPROGR-LEN
        varFunzDiCalcR.setAc5NomeprogrLen(((short)VarFunzDiCalcR.Len.AC5_NOMEPROGR));
        // COB_CODE: MOVE LENGTH OF AC5-MODCALC
        //                       TO AC5-MODCALC-LEN
        varFunzDiCalcR.setAc5ModcalcLen(((short)VarFunzDiCalcR.Len.AC5_MODCALC));
        // COB_CODE: MOVE LENGTH OF AC5-GLOVARLIST
        //                       TO AC5-GLOVARLIST-LEN
        varFunzDiCalcR.setAc5GlovarlistLen(((short)VarFunzDiCalcR.Len.AC5_GLOVARLIST));
        // COB_CODE: MOVE LENGTH OF AC5-STEP-ELAB
        //                       TO AC5-STEP-ELAB-LEN.
        varFunzDiCalcR.setAc5StepElabLen(((short)VarFunzDiCalcR.Len.AC5_STEP_ELAB));
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    @Override
    public String getCodprodVchar() {
        return varFunzDiCalcR.getAc5CodprodVcharFormatted();
    }

    @Override
    public void setCodprodVchar(String codprodVchar) {
        this.varFunzDiCalcR.setAc5CodprodVcharFormatted(codprodVchar);
    }

    @Override
    public String getCodtariVchar() {
        return varFunzDiCalcR.getAc5CodtariVcharFormatted();
    }

    @Override
    public void setCodtariVchar(String codtariVchar) {
        this.varFunzDiCalcR.setAc5CodtariVcharFormatted(codtariVchar);
    }

    @Override
    public int getDend() {
        return varFunzDiCalcR.getAc5Dend();
    }

    @Override
    public void setDend(int dend) {
        this.varFunzDiCalcR.setAc5Dend(dend);
    }

    @Override
    public int getDiniz() {
        return varFunzDiCalcR.getAc5Diniz();
    }

    @Override
    public void setDiniz(int diniz) {
        this.varFunzDiCalcR.setAc5Diniz(diniz);
    }

    @Override
    public String getGlovarlistVchar() {
        return varFunzDiCalcR.getAc5GlovarlistVcharFormatted();
    }

    @Override
    public void setGlovarlistVchar(String glovarlistVchar) {
        this.varFunzDiCalcR.setAc5GlovarlistVcharFormatted(glovarlistVchar);
    }

    @Override
    public String getGlovarlistVcharObj() {
        if (ws.getIndVarFunzDiCalcR().getFlPoliIfp() >= 0) {
            return getGlovarlistVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setGlovarlistVcharObj(String glovarlistVcharObj) {
        if (glovarlistVcharObj != null) {
            setGlovarlistVchar(glovarlistVcharObj);
            ws.getIndVarFunzDiCalcR().setFlPoliIfp(((short)0));
        }
        else {
            ws.getIndVarFunzDiCalcR().setFlPoliIfp(((short)-1));
        }
    }

    @Override
    public short getIdcomp() {
        return varFunzDiCalcR.getAc5Idcomp();
    }

    @Override
    public void setIdcomp(short idcomp) {
        this.varFunzDiCalcR.setAc5Idcomp(idcomp);
    }

    @Override
    public short getIsprecalc() {
        return varFunzDiCalcR.getAc5Isprecalc().getAc5Isprecalc();
    }

    @Override
    public void setIsprecalc(short isprecalc) {
        this.varFunzDiCalcR.getAc5Isprecalc().setAc5Isprecalc(isprecalc);
    }

    @Override
    public Short getIsprecalcObj() {
        if (ws.getIndVarFunzDiCalcR().getCodBlocco() >= 0) {
            return ((Short)getIsprecalc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIsprecalcObj(Short isprecalcObj) {
        if (isprecalcObj != null) {
            setIsprecalc(((short)isprecalcObj));
            ws.getIndVarFunzDiCalcR().setCodBlocco(((short)0));
        }
        else {
            ws.getIndVarFunzDiCalcR().setCodBlocco(((short)-1));
        }
    }

    @Override
    public String getModcalcVchar() {
        return varFunzDiCalcR.getAc5ModcalcVcharFormatted();
    }

    @Override
    public void setModcalcVchar(String modcalcVchar) {
        this.varFunzDiCalcR.setAc5ModcalcVcharFormatted(modcalcVchar);
    }

    @Override
    public String getModcalcVcharObj() {
        if (ws.getIndVarFunzDiCalcR().getWhereCondition() >= 0) {
            return getModcalcVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModcalcVcharObj(String modcalcVcharObj) {
        if (modcalcVcharObj != null) {
            setModcalcVchar(modcalcVcharObj);
            ws.getIndVarFunzDiCalcR().setWhereCondition(((short)0));
        }
        else {
            ws.getIndVarFunzDiCalcR().setWhereCondition(((short)-1));
        }
    }

    @Override
    public String getNomefunzVchar() {
        return varFunzDiCalcR.getAc5NomefunzVcharFormatted();
    }

    @Override
    public void setNomefunzVchar(String nomefunzVchar) {
        this.varFunzDiCalcR.setAc5NomefunzVcharFormatted(nomefunzVchar);
    }

    @Override
    public String getNomeprogrVchar() {
        return varFunzDiCalcR.getAc5NomeprogrVcharFormatted();
    }

    @Override
    public void setNomeprogrVchar(String nomeprogrVchar) {
        this.varFunzDiCalcR.setAc5NomeprogrVcharFormatted(nomeprogrVchar);
    }

    @Override
    public String getNomeprogrVcharObj() {
        if (ws.getIndVarFunzDiCalcR().getSrvzVerAnn() >= 0) {
            return getNomeprogrVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNomeprogrVcharObj(String nomeprogrVcharObj) {
        if (nomeprogrVcharObj != null) {
            setNomeprogrVchar(nomeprogrVcharObj);
            ws.getIndVarFunzDiCalcR().setSrvzVerAnn(((short)0));
        }
        else {
            ws.getIndVarFunzDiCalcR().setSrvzVerAnn(((short)-1));
        }
    }

    @Override
    public String getStepElabVchar() {
        return varFunzDiCalcR.getAc5StepElabVcharFormatted();
    }

    @Override
    public void setStepElabVchar(String stepElabVchar) {
        this.varFunzDiCalcR.setAc5StepElabVcharFormatted(stepElabVchar);
    }
}
