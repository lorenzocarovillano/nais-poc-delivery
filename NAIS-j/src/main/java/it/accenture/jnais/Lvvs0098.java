package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.C214TabOutput;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Ldbv6151;
import it.accenture.jnais.ws.Lvvs0098Data;

/**Original name: LVVS0098<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0098
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0098 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0098Data ws = new Lvvs0098Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0098_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Lvvs0098 getInstance() {
        return ((Lvvs0098)Programs.getInstance(Lvvs0098.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBV6151.
        initLdbv6151();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-ADE.
        initAreaIoAde();
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1110-LEGGI-PRESTITO  THRU  S1110-EX
            s1110LeggiPrestito();
            // COB_CODE: IF  PREST-ATTIVO
            //               END-IF
            //           ELSE
            //               MOVE  ZERO               TO IVVC0213-VAL-STR-O
            //           END-IF
            if (ws.getWkPrestito().isAttivo()) {
                // COB_CODE: MOVE  PRE-DT-DECOR-PREST
                //                    TO WK-DT-MAX
                ws.setWkDtMax(TruncAbs.toInt(ws.getPrest().getPreDtDecorPrest().getPreDtDecorPrest(), 8));
                // COB_CODE: PERFORM S1255-CALCOLA-IMPORTO  THRU S1255-EX
                s1255CalcolaImporto();
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //                    TO IVVC0213-VAL-STR-O
                //           ELSE
                //              CONTINUE
                //           END-IF
                if (idsv0003.getSqlcode().isNotFound()) {
                    // COB_CODE: MOVE  WK-DT-MAX
                    //                 TO IVVC0213-VAL-STR-O
                    ivvc0213.getTabOutput().setValStrO(ws.getWkDtMaxFormatted());
                }
                else {
                // COB_CODE: CONTINUE
                //continue
                }
            }
            else {
                // COB_CODE: MOVE  ZERO               TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO(LiteralGenerator.create('0', C214TabOutput.Len.VAL_STR_O));
            }
        }
    }

    /**Original name: S1110-LEGGI-PRESTITO<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DELLA TABELLA PRESTITI CON DATA DECORRENZA PRESTITO
	 *     MINORE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1110LeggiPrestito() {
        Ldbs6160 ldbs6160 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET PREST-NON-ATTIVO       TO TRUE
        ws.getWkPrestito().setNonAttivo();
        // COB_CODE: INITIALIZE                     PREST.
        initPrest();
        // COB_CODE: SET  IDSV0003-TRATT-X-EFFETTO TO TRUE.
        idsv0003.getTrattamentoStoricita().setIdsi0011TrattXEffetto();
        // COB_CODE: SET  IDSV0003-WHERE-CONDITION  TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET  IDSV0003-SELECT        TO TRUE
        idsv0003.getOperazione().setSelect();
        //    SET  IDSV0003-FETCH-FIRST   TO TRUE.
        // COB_CODE: MOVE 'AD'                   TO PRE-TP-OGG
        ws.getPrest().setPreTpOgg("AD");
        // COB_CODE: MOVE DADE-ID-ADES           TO PRE-ID-OGG
        ws.getPrest().setPreIdOgg(ws.getLccvade1().getDati().getWadeIdAdes());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                       TO PRE-COD-COMP-ANIA
        ws.getPrest().setPreCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        //    MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO LDBV6151-DT-DECOR-PREST
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-APPO-DATA
        ws.setWkAppoData(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
        // COB_CODE: MOVE 99991230               TO IDSV0003-DATA-INIZIO-EFFETTO
        idsv0003.setDataInizioEffetto(99991230);
        //    MOVE 999912314023595998     TO IDSV0003-DATA-COMPETENZA
        //
        // COB_CODE: MOVE 'LDBS6160'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS6160");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 PREST
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs6160 = Ldbs6160.getInstance();
            ldbs6160.run(idsv0003, ws.getPrest());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS6160 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6160 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: MOVE  WK-APPO-DATA          TO IDSV0003-DATA-INIZIO-EFFETTO
        idsv0003.setDataInizioEffetto(ws.getWkAppoData());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //             WHEN IDSV0003-SUCCESSFUL-SQL
            //                SET PREST-ATTIVO      TO  TRUE
            //             WHEN IDSV0003-NOT-FOUND
            //                CONTINUE
            //             WHEN OTHER
            //                END-STRING
            //           END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: SET PREST-ATTIVO      TO  TRUE
                    ws.getWkPrestito().setAttivo();
                    break;

                case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-CALL-PGM        TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                    // COB_CODE: STRING 'CHIAMATA LDBS6160 ;'
                    //              IDSV0003-RETURN-CODE ';'
                    //              IDSV0003-SQLCODE
                    //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6160 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'ERRORE CHIAMATA LDBS6160'   ';'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LDBS6160", ";", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-ADES
        //                TO DADE-AREA-ADES
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasAdes())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DADE-AREA-ADES
            ws.setDadeAreaAdesFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1255-CALCOLA-IMPORTO<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1255CalcolaImporto() {
        Ldbs6150 ldbs6150 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE LDBV6151.
        initLdbv6151();
        //
        // COB_CODE: SET  IDSV0003-TRATT-X-EFFETTO TO TRUE.
        idsv0003.getTrattamentoStoricita().setIdsi0011TrattXEffetto();
        // COB_CODE: SET  IDSV0003-SELECT           TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET  IDSV0003-WHERE-CONDITION  TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: MOVE 'PR'                           TO LDBV6151-TP-TIT-01.
        ws.getLdbv6151().setTpTit01("PR");
        // COB_CODE: MOVE 'IP'                           TO LDBV6151-TP-TIT-02.
        ws.getLdbv6151().setTpTit02("IP");
        // COB_CODE: MOVE 'IN'                           TO LDBV6151-TP-STAT-TIT.
        ws.getLdbv6151().setTpStatTit("IN");
        // COB_CODE: MOVE 'PO'                           TO LDBV6151-TP-OGG.
        ws.getLdbv6151().setTpOgg("PO");
        // COB_CODE: MOVE DADE-ID-POLI                   TO LDBV6151-ID-OGG.
        ws.getLdbv6151().setIdOgg(ws.getLccvade1().getDati().getWadeIdPoli());
        // COB_CODE: MOVE PRE-DT-DECOR-PREST
        //                                          TO LDBV6151-DT-DECOR-PREST.
        ws.getLdbv6151().setDtDecorPrest(ws.getPrest().getPreDtDecorPrest().getPreDtDecorPrest());
        //
        // COB_CODE: MOVE 'LDBS6150'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS6150");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV6151
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs6150 = Ldbs6150.getInstance();
            ldbs6150.run(idsv0003, ws.getLdbv6151());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS6150 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6150 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //            END-IF
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              MOVE LDBV6151-DT-MAX               TO IVVC0213-VAL-STR-O
            //           ELSE
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE LDBV6151-DT-MAX               TO IVVC0213-VAL-STR-O
                ivvc0213.getTabOutput().setValStrO(ws.getLdbv6151().getDtMaxFormatted());
            }
            else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              CONTINUE
            //           ELSE
            //            END-STRING
            //           END-IF
            // COB_CODE: CONTINUE
            //continue
            }
            else {
                // COB_CODE: IF IDSV0003-SQLCODE = -305
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              SET IDSV0003-INVALID-OPER            TO TRUE
                //           END-IF
                if (idsv0003.getSqlcode().getSqlcode() == -305) {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
                //
                // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CHIAMATA LDBS6150 ;'
                //             IDSV0003-RETURN-CODE ';'
                //             IDSV0003-SQLCODE
                //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6150 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            }
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'ERRORE CHIAMATA LDBS6150'   ';'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LDBS6150", ";", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabAde(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv6151() {
        ws.getLdbv6151().setTpTit01("");
        ws.getLdbv6151().setTpTit02("");
        ws.getLdbv6151().setTpStatTit("");
        ws.getLdbv6151().setDtMaxFormatted("00000000");
        ws.getLdbv6151().setDtMaxDb("");
        ws.getLdbv6151().setDtMaxInd(((short)0));
        ws.getLdbv6151().setDtDecorPrest(0);
        ws.getLdbv6151().setDtDecorPrestDb("");
        ws.getLdbv6151().setTpOgg("");
        ws.getLdbv6151().setIdOgg(0);
    }

    public void initAreaIoAde() {
        ws.setDadeEleAdesMax(((short)0));
        ws.getLccvade1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvade1().setIdPtf(0);
        ws.getLccvade1().getDati().setWadeIdAdes(0);
        ws.getLccvade1().getDati().setWadeIdPoli(0);
        ws.getLccvade1().getDati().setWadeIdMoviCrz(0);
        ws.getLccvade1().getDati().getWadeIdMoviChiu().setWadeIdMoviChiu(0);
        ws.getLccvade1().getDati().setWadeDtIniEff(0);
        ws.getLccvade1().getDati().setWadeDtEndEff(0);
        ws.getLccvade1().getDati().setWadeIbPrev("");
        ws.getLccvade1().getDati().setWadeIbOgg("");
        ws.getLccvade1().getDati().setWadeCodCompAnia(0);
        ws.getLccvade1().getDati().getWadeDtDecor().setWadeDtDecor(0);
        ws.getLccvade1().getDati().getWadeDtScad().setWadeDtScad(0);
        ws.getLccvade1().getDati().getWadeEtaAScad().setWadeEtaAScad(0);
        ws.getLccvade1().getDati().getWadeDurAa().setWadeDurAa(0);
        ws.getLccvade1().getDati().getWadeDurMm().setWadeDurMm(0);
        ws.getLccvade1().getDati().getWadeDurGg().setWadeDurGg(0);
        ws.getLccvade1().getDati().setWadeTpRgmFisc("");
        ws.getLccvade1().getDati().setWadeTpRiat("");
        ws.getLccvade1().getDati().setWadeTpModPagTit("");
        ws.getLccvade1().getDati().setWadeTpIas("");
        ws.getLccvade1().getDati().getWadeDtVarzTpIas().setWadeDtVarzTpIas(0);
        ws.getLccvade1().getDati().getWadePreNetInd().setWadePreNetInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePreLrdInd().setWadePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeRatLrdInd().setWadeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePrstzIniInd().setWadePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().setWadeFlCoincAssto(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeIbDflt("");
        ws.getLccvade1().getDati().setWadeModCalc("");
        ws.getLccvade1().getDati().setWadeTpFntCnbtva("");
        ws.getLccvade1().getDati().getWadeImpAz().setWadeImpAz(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpAder().setWadeImpAder(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpTfr().setWadeImpTfr(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpVolo().setWadeImpVolo(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadePcAz().setWadePcAz(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcAder().setWadePcAder(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcTfr().setWadePcTfr(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadePcVolo().setWadePcVolo(new AfDecimal(0, 6, 3));
        ws.getLccvade1().getDati().getWadeDtNovaRgmFisc().setWadeDtNovaRgmFisc(0);
        ws.getLccvade1().getDati().setWadeFlAttiv(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeImpRecRitVis().setWadeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpRecRitAcc().setWadeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().setWadeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeImpbVisDaRec().setWadeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeDtDecorPrestBan().setWadeDtDecorPrestBan(0);
        ws.getLccvade1().getDati().getWadeDtEffVarzStatT().setWadeDtEffVarzStatT(0);
        ws.getLccvade1().getDati().setWadeDsRiga(0);
        ws.getLccvade1().getDati().setWadeDsOperSql(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().setWadeDsVer(0);
        ws.getLccvade1().getDati().setWadeDsTsIniCptz(0);
        ws.getLccvade1().getDati().setWadeDsTsEndCptz(0);
        ws.getLccvade1().getDati().setWadeDsUtente("");
        ws.getLccvade1().getDati().setWadeDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvade1().getDati().getWadeCumCnbtCap().setWadeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeImpGarCnbt().setWadeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getLccvade1().getDati().getWadeDtUltConsCnbt().setWadeDtUltConsCnbt(0);
        ws.getLccvade1().getDati().setWadeIdenIscFnd("");
        ws.getLccvade1().getDati().getWadeNumRatPian().setWadeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getLccvade1().getDati().getWadeDtPresc().setWadeDtPresc(0);
        ws.getLccvade1().getDati().setWadeConcsPrest(Types.SPACE_CHAR);
    }

    public void initPrest() {
        ws.getPrest().setPreIdPrest(0);
        ws.getPrest().setPreIdOgg(0);
        ws.getPrest().setPreTpOgg("");
        ws.getPrest().setPreIdMoviCrz(0);
        ws.getPrest().getPreIdMoviChiu().setPreIdMoviChiu(0);
        ws.getPrest().setPreDtIniEff(0);
        ws.getPrest().setPreDtEndEff(0);
        ws.getPrest().setPreCodCompAnia(0);
        ws.getPrest().getPreDtConcsPrest().setPreDtConcsPrest(0);
        ws.getPrest().getPreDtDecorPrest().setPreDtDecorPrest(0);
        ws.getPrest().getPreImpPrest().setPreImpPrest(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreIntrPrest().setPreIntrPrest(new AfDecimal(0, 6, 3));
        ws.getPrest().setPreTpPrest("");
        ws.getPrest().getPreFrazPagIntr().setPreFrazPagIntr(0);
        ws.getPrest().getPreDtRimb().setPreDtRimb(0);
        ws.getPrest().getPreImpRimb().setPreImpRimb(new AfDecimal(0, 15, 3));
        ws.getPrest().setPreCodDvs("");
        ws.getPrest().setPreDtRichPrest(0);
        ws.getPrest().setPreModIntrPrest("");
        ws.getPrest().getPreSpePrest().setPreSpePrest(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreImpPrestLiqto().setPreImpPrestLiqto(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreSdoIntr().setPreSdoIntr(new AfDecimal(0, 15, 3));
        ws.getPrest().getPreRimbEff().setPreRimbEff(new AfDecimal(0, 15, 3));
        ws.getPrest().getPrePrestResEff().setPrePrestResEff(new AfDecimal(0, 15, 3));
        ws.getPrest().setPreDsRiga(0);
        ws.getPrest().setPreDsOperSql(Types.SPACE_CHAR);
        ws.getPrest().setPreDsVer(0);
        ws.getPrest().setPreDsTsIniCptz(0);
        ws.getPrest().setPreDsTsEndCptz(0);
        ws.getPrest().setPreDsUtente("");
        ws.getPrest().setPreDsStatoElab(Types.SPACE_CHAR);
    }
}
