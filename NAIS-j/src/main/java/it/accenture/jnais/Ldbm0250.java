package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.AdesDao;
import it.accenture.jnais.commons.data.dao.AdesPoliDao;
import it.accenture.jnais.commons.data.to.IAdes;
import it.accenture.jnais.commons.data.to.IAdesPoli;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.Ades;
import it.accenture.jnais.ws.enums.Idsv0003TipologiaOperazione;
import it.accenture.jnais.ws.Iabv0002;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbm0250Data;
import it.accenture.jnais.ws.Llbv0269;
import it.accenture.jnais.ws.PoliIdbspol0;
import it.accenture.jnais.ws.redefines.AdeCumCnbtCap;
import it.accenture.jnais.ws.redefines.AdeDtDecor;
import it.accenture.jnais.ws.redefines.AdeDtDecorPrestBan;
import it.accenture.jnais.ws.redefines.AdeDtEffVarzStatT;
import it.accenture.jnais.ws.redefines.AdeDtNovaRgmFisc;
import it.accenture.jnais.ws.redefines.AdeDtPresc;
import it.accenture.jnais.ws.redefines.AdeDtScad;
import it.accenture.jnais.ws.redefines.AdeDtUltConsCnbt;
import it.accenture.jnais.ws.redefines.AdeDtVarzTpIas;
import it.accenture.jnais.ws.redefines.AdeDurAa;
import it.accenture.jnais.ws.redefines.AdeDurGg;
import it.accenture.jnais.ws.redefines.AdeDurMm;
import it.accenture.jnais.ws.redefines.AdeEtaAScad;
import it.accenture.jnais.ws.redefines.AdeIdMoviChiu;
import it.accenture.jnais.ws.redefines.AdeImpAder;
import it.accenture.jnais.ws.redefines.AdeImpAz;
import it.accenture.jnais.ws.redefines.AdeImpbVisDaRec;
import it.accenture.jnais.ws.redefines.AdeImpGarCnbt;
import it.accenture.jnais.ws.redefines.AdeImpRecRitAcc;
import it.accenture.jnais.ws.redefines.AdeImpRecRitVis;
import it.accenture.jnais.ws.redefines.AdeImpTfr;
import it.accenture.jnais.ws.redefines.AdeImpVolo;
import it.accenture.jnais.ws.redefines.AdeNumRatPian;
import it.accenture.jnais.ws.redefines.AdePcAder;
import it.accenture.jnais.ws.redefines.AdePcAz;
import it.accenture.jnais.ws.redefines.AdePcTfr;
import it.accenture.jnais.ws.redefines.AdePcVolo;
import it.accenture.jnais.ws.redefines.AdePreLrdInd;
import it.accenture.jnais.ws.redefines.AdePreNetInd;
import it.accenture.jnais.ws.redefines.AdePrstzIniInd;
import it.accenture.jnais.ws.redefines.AdeRatLrdInd;
import it.accenture.jnais.ws.redefines.PolAaDiffProrDflt;
import it.accenture.jnais.ws.redefines.PolDir1oVers;
import it.accenture.jnais.ws.redefines.PolDirEmis;
import it.accenture.jnais.ws.redefines.PolDirQuiet;
import it.accenture.jnais.ws.redefines.PolDirVersAgg;
import it.accenture.jnais.ws.redefines.PolDtApplzConv;
import it.accenture.jnais.ws.redefines.PolDtIniVldtConv;
import it.accenture.jnais.ws.redefines.PolDtPresc;
import it.accenture.jnais.ws.redefines.PolDtProp;
import it.accenture.jnais.ws.redefines.PolDtScad;
import it.accenture.jnais.ws.redefines.PolDurAa;
import it.accenture.jnais.ws.redefines.PolDurGg;
import it.accenture.jnais.ws.redefines.PolDurMm;
import it.accenture.jnais.ws.redefines.PolIdAccComm;
import it.accenture.jnais.ws.redefines.PolIdMoviChiu;
import it.accenture.jnais.ws.redefines.PolSpeMed;
import it.accenture.jnais.ws.redefines.StbIdMoviChiu;
import it.accenture.jnais.ws.StatOggBusIdbsstb0;

/**Original name: LDBM0250<br>
 * <pre>AUTHOR.        A.I.I.S
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : SERVIZIO ESTRAZIONE OCCORRENZE              *
 *                    DA TABELLE GUIDA RICHIAMATO DAL BATCH EXEC. *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbm0250 extends Program implements IAdes, IAdesPoli {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private AdesPoliLdbm0250 adesPoliLdbm0250 = new AdesPoliLdbm0250(this);
    private AdesPoliLdbm02501 adesPoliLdbm02501 = new AdesPoliLdbm02501(this);
    private AdesPoliLdbm02502 adesPoliLdbm02502 = new AdesPoliLdbm02502(this);
    private AdesPoliDao adesPoliDao = new AdesPoliDao(dbAccessStatus);
    private AdesDao adesDao = new AdesDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbm0250Data ws = new Ldbm0250Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IABV0002
    private Iabv0002 iabv0002;
    //Original name: ADES
    private Ades ades;
    //Original name: POLI
    private PoliIdbspol0 poli;
    //Original name: STAT-OGG-BUS
    private StatOggBusIdbsstb0 statOggBus;
    //Original name: LLBV0269
    private Llbv0269 llbv0269;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBM0250_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Iabv0002 iabv0002, Ades ades, PoliIdbspol0 poli, StatOggBusIdbsstb0 statOggBus, Llbv0269 llbv0269) {
        this.idsv0003 = idsv0003;
        this.iabv0002 = iabv0002;
        this.ades = ades;
        this.poli = poli;
        this.statOggBus = statOggBus;
        this.llbv0269 = llbv0269;
        // COB_CODE: PERFORM A000-INIZIO                  THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A025-CNTL-INPUT              THRU A025-EX.
        a025CntlInput();
        // COB_CODE: PERFORM A040-CARICA-WHERE-CONDITION  THRU A040-EX.
        a040CaricaWhereCondition();
        // COB_CODE: PERFORM A300-ELABORA                 THRU A300-EX
        a300Elabora();
        // COB_CODE: PERFORM A350-CTRL-COMMIT             THRU A350-EX.
        a350CtrlCommit();
        // COB_CODE: PERFORM A400-FINE                    THRU A400-EX.
        a400Fine();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbm0250 getInstance() {
        return ((Ldbm0250)Programs.getInstance(Ldbm0250.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBM0250'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBM0250");
        // COB_CODE: MOVE 'ADES'                  TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("ADES");
        // COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
        //                                             IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                             IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: SET ACCESSO-X-RANGE-NO       TO   TRUE.
        ws.getFlagAccessoXRange().setNo();
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A025-CNTL-INPUT<br>
	 * <pre>*****************************************************************</pre>*/
    private void a025CntlInput() {
        // COB_CODE: IF IABV0009-ID-OGG-DA IS NUMERIC AND
        //              IABV0009-ID-OGG-DA NOT = ZEROES
        //              END-IF
        //           END-IF.
        if (Functions.isNumber(iabv0002.getIabv0009GestGuideService().getIdOggDa()) && iabv0002.getIabv0009GestGuideService().getIdOggDa() != 0) {
            // COB_CODE: IF IABV0009-ID-OGG-A  IS NUMERIC AND
            //              IABV0009-ID-OGG-A  NOT = ZEROES
            //              SET ACCESSO-X-RANGE-SI    TO TRUE
            //           END-IF
            if (Functions.isNumber(iabv0002.getIabv0009GestGuideService().getIdOggA()) && iabv0002.getIabv0009GestGuideService().getIdOggA() != 0) {
                // COB_CODE: SET ACCESSO-X-RANGE-SI    TO TRUE
                ws.getFlagAccessoXRange().setSi();
            }
        }
    }

    /**Original name: A040-CARICA-WHERE-CONDITION<br>
	 * <pre>*****************************************************************</pre>*/
    private void a040CaricaWhereCondition() {
        // COB_CODE: MOVE IABV0009-BLOB-DATA-REC        TO WLB-REC-PREN
        ws.getWlbRecPren().setWlbRecPrenFormatted(iabv0002.getIabv0009GestGuideService().getIabv0009BlobDataRecFormatted());
        // COB_CODE: INITIALIZE WS-DT-PTF-X
        //                      WS-DT-TS-PTF
        //                      WS-DT-TS-PTF-PRE.
        ws.setWsDtPtfX("");
        ws.setWsDtTsPtf(0);
        ws.setWsDtTsPtfPre(0);
        //    LA DATA EFFETTO VIENE VALORIZZATA CON LA DATA PRODUZIONE
        // COB_CODE: MOVE WLB-DT-PRODUZIONE
        //             TO WS-DT-N.
        ws.getWsDtN().setWsDtNFormatted(ws.getWlbRecPren().getDtProduzioneFormatted());
        // COB_CODE: MOVE WS-DT-N-AA
        //             TO WS-DT-X-AA.
        ws.getWsDtX().setAaFormatted(ws.getWsDtN().getAaFormatted());
        // COB_CODE: MOVE WS-DT-N-MM
        //             TO WS-DT-X-MM.
        ws.getWsDtX().setMmFormatted(ws.getWsDtN().getMmFormatted());
        // COB_CODE: MOVE WS-DT-N-GG
        //             TO WS-DT-X-GG.
        ws.getWsDtX().setGgFormatted(ws.getWsDtN().getGgFormatted());
        // COB_CODE: MOVE WS-DT-X
        //             TO WS-DT-PTF-X.
        ws.setWsDtPtfX(ws.getWsDtX().getWsDtXFormatted());
        // COB_CODE: MOVE WLB-TS-COMPETENZA
        //             TO WS-DT-TS-PTF.
        ws.setWsDtTsPtf(ws.getWlbRecPren().getTsCompetenza());
        // COB_CODE: MOVE LLBV0269-TS-PRECED
        //             TO WS-DT-TS-PTF-PRE.
        ws.setWsDtTsPtfPre(llbv0269.getTsPreced());
        //    DISPLAY PER VERIFICA CORRETTEZZA DATE
        //    DISPLAY 'DATA RISERVA =                   ' WLB-DT-RISERVA
        //    DISPLAY 'DATA PRODUZIONE =                ' WLB-DT-PRODUZIONE
        //    DISPLAY 'DATA EFFETTO (DATA PRODUZIONE)=  ' WS-DT-PTF-X
        //    DISPLAY 'TIMESTAMP COMPETENZA =           ' WS-DT-TS-PTF
        //    DISPLAY 'TIMESTAMP COMPETENZA PRECEDENTE = '
        //                                                WS-DT-TS-PTF-PRE
        // COB_CODE: IF  WLB-TP-FRM-ASSVA EQUAL 'EN'
        //               MOVE 'CO'                      TO WS-TP-FRM-ASSVA2
        //           ELSE
        //               MOVE SPACES                    TO WS-TP-FRM-ASSVA2
        //           END-IF.
        if (Conditions.eq(ws.getWlbRecPren().getTpFrmAssva(), "EN")) {
            // COB_CODE: MOVE 'IN'                      TO WS-TP-FRM-ASSVA1
            ws.setWsTpFrmAssva1("IN");
            // COB_CODE: MOVE 'CO'                      TO WS-TP-FRM-ASSVA2
            ws.setWsTpFrmAssva2("CO");
        }
        else {
            // COB_CODE: MOVE WLB-TP-FRM-ASSVA          TO WS-TP-FRM-ASSVA1
            ws.setWsTpFrmAssva1(ws.getWlbRecPren().getTpFrmAssva());
            // COB_CODE: MOVE SPACES                    TO WS-TP-FRM-ASSVA2
            ws.setWsTpFrmAssva2("");
        }
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE:      EVALUATE TRUE
        //                   WHEN IDSV0003-WHERE-CONDITION-01
        //           * Estrazione massiva (parametrizzata per tp_forma_ass)
        //                        PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX
        //           * Estrazione per Prodotto
        //                   WHEN IDSV0003-WHERE-CONDITION-02
        //                        PERFORM SC02-SELECTION-CURSOR-02 THRU SC02-EX
        //           * Estrazione per Range di polizze
        //                   WHEN IDSV0003-WHERE-CONDITION-03
        //                        PERFORM SC03-SELECTION-CURSOR-03 THRU SC03-EX
        //           * Estrazione per collettiva\range di adesioni
        //                   WHEN IDSV0003-WHERE-CONDITION-04
        //                        PERFORM SC04-SELECTION-CURSOR-04 THRU SC04-EX
        //           * Aggiornamento estrazione massiva
        //                   WHEN IDSV0003-WHERE-CONDITION-05
        //                        PERFORM SC05-SELECTION-CURSOR-05 THRU SC05-EX
        //           * Aggiornamento estrazione massiva per Prodotto
        //                   WHEN IDSV0003-WHERE-CONDITION-06
        //                        PERFORM SC06-SELECTION-CURSOR-06 THRU SC06-EX
        //           * Aggiornamento estrazione massiva per Range di polizze
        //                   WHEN IDSV0003-WHERE-CONDITION-07
        //                        PERFORM SC07-SELECTION-CURSOR-07 THRU SC07-EX
        //           * Aggiornamento estrazione massiva per Range di polizze\adesioni
        //                   WHEN IDSV0003-WHERE-CONDITION-08
        //                        PERFORM SC08-SELECTION-CURSOR-08 THRU SC08-EX
        //           *
        //                   WHEN IDSV0003-WHERE-CONDITION-09
        //                        SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
        //           *
        //                   WHEN IDSV0003-WHERE-CONDITION-10
        //                        SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
        //                   WHEN OTHER
        //                        SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
        //                END-EVALUATE.
        switch (idsv0003.getTipologiaOperazione().getTipologiaOperazione()) {

            case Idsv0003TipologiaOperazione.CONDITION01:// Estrazione massiva (parametrizzata per tp_forma_ass)
                // COB_CODE: PERFORM SC01-SELECTION-CURSOR-01 THRU SC01-EX
                sc01SelectionCursor01();
                // Estrazione per Prodotto
                break;

            case Idsv0003TipologiaOperazione.CONDITION02:// COB_CODE: PERFORM SC02-SELECTION-CURSOR-02 THRU SC02-EX
                sc02SelectionCursor02();
                // Estrazione per Range di polizze
                break;

            case Idsv0003TipologiaOperazione.CONDITION03:// COB_CODE: PERFORM SC03-SELECTION-CURSOR-03 THRU SC03-EX
                sc03SelectionCursor03();
                // Estrazione per collettiva\\range di adesioni
                break;

            case Idsv0003TipologiaOperazione.CONDITION04:// COB_CODE: PERFORM SC04-SELECTION-CURSOR-04 THRU SC04-EX
                sc04SelectionCursor04();
                // Aggiornamento estrazione massiva
                break;

            case Idsv0003TipologiaOperazione.CONDITION05:// COB_CODE: PERFORM SC05-SELECTION-CURSOR-05 THRU SC05-EX
                sc05SelectionCursor05();
                // Aggiornamento estrazione massiva per Prodotto
                break;

            case Idsv0003TipologiaOperazione.CONDITION06:// COB_CODE: PERFORM SC06-SELECTION-CURSOR-06 THRU SC06-EX
                sc06SelectionCursor06();
                // Aggiornamento estrazione massiva per Range di polizze
                break;

            case Idsv0003TipologiaOperazione.CONDITION07:// COB_CODE: PERFORM SC07-SELECTION-CURSOR-07 THRU SC07-EX
                sc07SelectionCursor07();
                // Aggiornamento estrazione massiva per Range di polizze\\adesioni
                break;

            case Idsv0003TipologiaOperazione.CONDITION08:// COB_CODE: PERFORM SC08-SELECTION-CURSOR-08 THRU SC08-EX
                sc08SelectionCursor08();
                //
                break;

            case Idsv0003TipologiaOperazione.CONDITION09:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidLevelOper();
                //
                break;

            case Idsv0003TipologiaOperazione.CONDITION10:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidLevelOper();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidLevelOper();
                break;
        }
    }

    /**Original name: SC01-SELECTION-CURSOR-01<br>
	 * <pre>*****************************************************************</pre>*/
    private void sc01SelectionCursor01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC01           THRU A310-SC01-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-SC01      THRU A360-SC01-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-SC01     THRU A370-SC01-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-SC01      THRU A380-SC01-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-SC01       THRU A390-SC01-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE-SC01           THRU A320-SC01-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER          TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC01           THRU A310-SC01-EX
            a310SelectSc01();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-SC01      THRU A360-SC01-EX
            a360OpenCursorSc01();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC01     THRU A370-SC01-EX
            a370CloseCursorSc01();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-SC01      THRU A380-SC01-EX
            a380FetchFirstSc01();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC01       THRU A390-SC01-EX
            a390FetchNextSc01();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE-SC01           THRU A320-SC01-EX
            a320UpdateSc01();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursorSc01() {
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              CONTINUE
        //           ELSE
        //              CONTINUE
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
        // COB_CODE:         EXEC SQL
        //                        DECLARE CUR-SC01-RANGE CURSOR WITH HOLD FOR
        //                     SELECT
        //                         A.ID_ADES
        //                        ,A.ID_POLI
        //                        ,A.ID_MOVI_CRZ
        //                        ,A.ID_MOVI_CHIU
        //                        ,A.DT_INI_EFF
        //                        ,A.DT_END_EFF
        //                        ,A.IB_PREV
        //                        ,A.IB_OGG
        //                        ,A.COD_COMP_ANIA
        //                        ,A.DT_DECOR
        //                        ,A.DT_SCAD
        //                        ,A.ETA_A_SCAD
        //                        ,A.DUR_AA
        //                        ,A.DUR_MM
        //                        ,A.DUR_GG
        //                        ,A.TP_RGM_FISC
        //                        ,A.TP_RIAT
        //                        ,A.TP_MOD_PAG_TIT
        //                        ,A.TP_IAS
        //                        ,A.DT_VARZ_TP_IAS
        //                        ,A.PRE_NET_IND
        //                        ,A.PRE_LRD_IND
        //                        ,A.RAT_LRD_IND
        //                        ,A.PRSTZ_INI_IND
        //                        ,A.FL_COINC_ASSTO
        //                        ,A.IB_DFLT
        //                        ,A.MOD_CALC
        //                        ,A.TP_FNT_CNBTVA
        //                        ,A.IMP_AZ
        //                        ,A.IMP_ADER
        //                        ,A.IMP_TFR
        //                        ,A.IMP_VOLO
        //                        ,A.PC_AZ
        //                        ,A.PC_ADER
        //                        ,A.PC_TFR
        //                        ,A.PC_VOLO
        //                        ,A.DT_NOVA_RGM_FISC
        //                        ,A.FL_ATTIV
        //                        ,A.IMP_REC_RIT_VIS
        //                        ,A.IMP_REC_RIT_ACC
        //                        ,A.FL_VARZ_STAT_TBGC
        //                        ,A.FL_PROVZA_MIGRAZ
        //                        ,A.IMPB_VIS_DA_REC
        //                        ,A.DT_DECOR_PREST_BAN
        //                        ,A.DT_EFF_VARZ_STAT_T
        //                        ,A.DS_RIGA
        //                        ,A.DS_OPER_SQL
        //                        ,A.DS_VER
        //                        ,A.DS_TS_INI_CPTZ
        //                        ,A.DS_TS_END_CPTZ
        //                        ,A.DS_UTENTE
        //                        ,A.DS_STATO_ELAB
        //                        ,A.CUM_CNBT_CAP
        //                        ,A.IMP_GAR_CNBT
        //                        ,A.DT_ULT_CONS_CNBT
        //                        ,A.IDEN_ISC_FND
        //                        ,A.NUM_RAT_PIAN
        //                        ,A.DT_PRESC
        //                        ,A.CONCS_PREST
        //                        ,B.ID_POLI
        //                        ,B.ID_MOVI_CRZ
        //                        ,B.ID_MOVI_CHIU
        //                        ,B.IB_OGG
        //                        ,B.IB_PROP
        //                        ,B.DT_PROP
        //                        ,B.DT_INI_EFF
        //                        ,B.DT_END_EFF
        //                        ,B.COD_COMP_ANIA
        //                        ,B.DT_DECOR
        //                        ,B.DT_EMIS
        //                        ,B.TP_POLI
        //                        ,B.DUR_AA
        //                        ,B.DUR_MM
        //                        ,B.DT_SCAD
        //                        ,B.COD_PROD
        //                        ,B.DT_INI_VLDT_PROD
        //                        ,B.COD_CONV
        //                        ,B.COD_RAMO
        //                        ,B.DT_INI_VLDT_CONV
        //                        ,B.DT_APPLZ_CONV
        //                        ,B.TP_FRM_ASSVA
        //                        ,B.TP_RGM_FISC
        //                        ,B.FL_ESTAS
        //                        ,B.FL_RSH_COMUN
        //                        ,B.FL_RSH_COMUN_COND
        //                        ,B.TP_LIV_GENZ_TIT
        //                        ,B.FL_COP_FINANZ
        //                        ,B.TP_APPLZ_DIR
        //                        ,B.SPE_MED
        //                        ,B.DIR_EMIS
        //                        ,B.DIR_1O_VERS
        //                        ,B.DIR_VERS_AGG
        //                        ,B.COD_DVS
        //                        ,B.FL_FNT_AZ
        //                        ,B.FL_FNT_ADER
        //                        ,B.FL_FNT_TFR
        //                        ,B.FL_FNT_VOLO
        //                        ,B.TP_OPZ_A_SCAD
        //                        ,B.AA_DIFF_PROR_DFLT
        //                        ,B.FL_VER_PROD
        //                        ,B.DUR_GG
        //                        ,B.DIR_QUIET
        //                        ,B.TP_PTF_ESTNO
        //                        ,B.FL_CUM_PRE_CNTR
        //                        ,B.FL_AMMB_MOVI
        //                        ,B.CONV_GECO
        //                        ,B.DS_RIGA
        //                        ,B.DS_OPER_SQL
        //                        ,B.DS_VER
        //                        ,B.DS_TS_INI_CPTZ
        //                        ,B.DS_TS_END_CPTZ
        //                        ,B.DS_UTENTE
        //                        ,B.DS_STATO_ELAB
        //                        ,B.FL_SCUDO_FISC
        //                        ,B.FL_TRASFE
        //                        ,B.FL_TFR_STRC
        //                        ,B.DT_PRESC
        //                        ,B.COD_CONV_AGG
        //                        ,B.SUBCAT_PROD
        //                        ,B.FL_QUEST_ADEGZ_ASS
        //                        ,B.COD_TPA
        //                        ,B.ID_ACC_COMM
        //                        ,B.FL_POLI_CPI_PR
        //                        ,B.FL_POLI_BUNDLING
        //                        ,B.IND_POLI_PRIN_COLL
        //                        ,B.FL_VND_BUNDLE
        //                        ,B.IB_BS
        //                        ,B.FL_POLI_IFP
        //                        ,C.ID_STAT_OGG_BUS
        //                        ,C.ID_OGG
        //                        ,C.TP_OGG
        //                        ,C.ID_MOVI_CRZ
        //                        ,C.ID_MOVI_CHIU
        //                        ,C.DT_INI_EFF
        //                        ,C.DT_END_EFF
        //                        ,C.COD_COMP_ANIA
        //                        ,C.TP_STAT_BUS
        //                        ,C.TP_CAUS
        //                        ,C.DS_RIGA
        //                        ,C.DS_OPER_SQL
        //                        ,C.DS_VER
        //                        ,C.DS_TS_INI_CPTZ
        //                        ,C.DS_TS_END_CPTZ
        //                        ,C.DS_UTENTE
        //                        ,C.DS_STATO_ELAB
        //                     FROM ADES         A,
        //                          POLI         B,
        //                          STAT_OGG_BUS C
        //                     WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
        //                                                :WS-TP-FRM-ASSVA2)
        //                       AND A.ID_POLI         =   B.ID_POLI
        //                       AND A.ID_ADES         =   C.ID_OGG
        //                       AND C.TP_OGG          =  'AD'
        //                       AND C.TP_STAT_BUS    =  'VI'
        //           *           OR
        //           *               (C.TP_STAT_BUS    =  'ST'  AND
        //           *               EXISTS (SELECT
        //           *                     D.ID_MOVI_FINRIO
        //           *
        //           *                  FROM  MOVI_FINRIO D
        //           *                  WHERE A.ID_ADES = D.ID_ADES
        //           *                    AND D.STAT_MOVI IN ('AL','IE')
        //           *                    AND D.TP_MOVI_FINRIO = 'DI'
        //           *                    AND D.COD_COMP_ANIA   =
        //           *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
        //           *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
        //           *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //           *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
        //                     AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                     AND A.ID_POLI        BETWEEN   :IABV0009-ID-OGG-DA AND
        //                                                    :IABV0009-ID-OGG-A
        //                     AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                     AND A.DT_END_EFF     >   :WS-DT-PTF-X
        //                     AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                     AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                     AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
        //                     AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                     AND B.DT_END_EFF     >   :WS-DT-PTF-X
        //                     AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                     AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                     AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
        //                     AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                     AND C.DT_END_EFF     >   :WS-DT-PTF-X
        //                     AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                     AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //           *         AND A.DS_STATO_ELAB IN (
        //           *                                 :IABV0002-STATE-01,
        //           *                                 :IABV0002-STATE-02,
        //           *                                 :IABV0002-STATE-03,
        //           *                                 :IABV0002-STATE-04,
        //           *                                 :IABV0002-STATE-05,
        //           *                                 :IABV0002-STATE-06,
        //           *                                 :IABV0002-STATE-07,
        //           *                                 :IABV0002-STATE-08,
        //           *                                 :IABV0002-STATE-09,
        //           *                                 :IABV0002-STATE-10
        //           *                                     )
        //           *           AND NOT A.DS_VER     = :IABV0009-VERSIONING
        //                       ORDER BY A.ID_POLI, A.ID_ADES
        //                   END-EXEC
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE
        //continue
        }
        else {
        // COB_CODE:         EXEC SQL
        //                        DECLARE CUR-SC01 CURSOR WITH HOLD FOR
        //                     SELECT
        //                         A.ID_ADES
        //                        ,A.ID_POLI
        //                        ,A.ID_MOVI_CRZ
        //                        ,A.ID_MOVI_CHIU
        //                        ,A.DT_INI_EFF
        //                        ,A.DT_END_EFF
        //                        ,A.IB_PREV
        //                        ,A.IB_OGG
        //                        ,A.COD_COMP_ANIA
        //                        ,A.DT_DECOR
        //                        ,A.DT_SCAD
        //                        ,A.ETA_A_SCAD
        //                        ,A.DUR_AA
        //                        ,A.DUR_MM
        //                        ,A.DUR_GG
        //                        ,A.TP_RGM_FISC
        //                        ,A.TP_RIAT
        //                        ,A.TP_MOD_PAG_TIT
        //                        ,A.TP_IAS
        //                        ,A.DT_VARZ_TP_IAS
        //                        ,A.PRE_NET_IND
        //                        ,A.PRE_LRD_IND
        //                        ,A.RAT_LRD_IND
        //                        ,A.PRSTZ_INI_IND
        //                        ,A.FL_COINC_ASSTO
        //                        ,A.IB_DFLT
        //                        ,A.MOD_CALC
        //                        ,A.TP_FNT_CNBTVA
        //                        ,A.IMP_AZ
        //                        ,A.IMP_ADER
        //                        ,A.IMP_TFR
        //                        ,A.IMP_VOLO
        //                        ,A.PC_AZ
        //                        ,A.PC_ADER
        //                        ,A.PC_TFR
        //                        ,A.PC_VOLO
        //                        ,A.DT_NOVA_RGM_FISC
        //                        ,A.FL_ATTIV
        //                        ,A.IMP_REC_RIT_VIS
        //                        ,A.IMP_REC_RIT_ACC
        //                        ,A.FL_VARZ_STAT_TBGC
        //                        ,A.FL_PROVZA_MIGRAZ
        //                        ,A.IMPB_VIS_DA_REC
        //                        ,A.DT_DECOR_PREST_BAN
        //                        ,A.DT_EFF_VARZ_STAT_T
        //                        ,A.DS_RIGA
        //                        ,A.DS_OPER_SQL
        //                        ,A.DS_VER
        //                        ,A.DS_TS_INI_CPTZ
        //                        ,A.DS_TS_END_CPTZ
        //                        ,A.DS_UTENTE
        //                        ,A.DS_STATO_ELAB
        //                        ,A.CUM_CNBT_CAP
        //                        ,A.IMP_GAR_CNBT
        //                        ,A.DT_ULT_CONS_CNBT
        //                        ,A.IDEN_ISC_FND
        //                        ,A.NUM_RAT_PIAN
        //                        ,A.DT_PRESC
        //                        ,A.CONCS_PREST
        //                        ,B.ID_POLI
        //                        ,B.ID_MOVI_CRZ
        //                        ,B.ID_MOVI_CHIU
        //                        ,B.IB_OGG
        //                        ,B.IB_PROP
        //                        ,B.DT_PROP
        //                        ,B.DT_INI_EFF
        //                        ,B.DT_END_EFF
        //                        ,B.COD_COMP_ANIA
        //                        ,B.DT_DECOR
        //                        ,B.DT_EMIS
        //                        ,B.TP_POLI
        //                        ,B.DUR_AA
        //                        ,B.DUR_MM
        //                        ,B.DT_SCAD
        //                        ,B.COD_PROD
        //                        ,B.DT_INI_VLDT_PROD
        //                        ,B.COD_CONV
        //                        ,B.COD_RAMO
        //                        ,B.DT_INI_VLDT_CONV
        //                        ,B.DT_APPLZ_CONV
        //                        ,B.TP_FRM_ASSVA
        //                        ,B.TP_RGM_FISC
        //                        ,B.FL_ESTAS
        //                        ,B.FL_RSH_COMUN
        //                        ,B.FL_RSH_COMUN_COND
        //                        ,B.TP_LIV_GENZ_TIT
        //                        ,B.FL_COP_FINANZ
        //                        ,B.TP_APPLZ_DIR
        //                        ,B.SPE_MED
        //                        ,B.DIR_EMIS
        //                        ,B.DIR_1O_VERS
        //                        ,B.DIR_VERS_AGG
        //                        ,B.COD_DVS
        //                        ,B.FL_FNT_AZ
        //                        ,B.FL_FNT_ADER
        //                        ,B.FL_FNT_TFR
        //                        ,B.FL_FNT_VOLO
        //                        ,B.TP_OPZ_A_SCAD
        //                        ,B.AA_DIFF_PROR_DFLT
        //                        ,B.FL_VER_PROD
        //                        ,B.DUR_GG
        //                        ,B.DIR_QUIET
        //                        ,B.TP_PTF_ESTNO
        //                        ,B.FL_CUM_PRE_CNTR
        //                        ,B.FL_AMMB_MOVI
        //                        ,B.CONV_GECO
        //                        ,B.DS_RIGA
        //                        ,B.DS_OPER_SQL
        //                        ,B.DS_VER
        //                        ,B.DS_TS_INI_CPTZ
        //                        ,B.DS_TS_END_CPTZ
        //                        ,B.DS_UTENTE
        //                        ,B.DS_STATO_ELAB
        //                        ,B.FL_SCUDO_FISC
        //                        ,B.FL_TRASFE
        //                        ,B.FL_TFR_STRC
        //                        ,B.DT_PRESC
        //                        ,B.COD_CONV_AGG
        //                        ,B.SUBCAT_PROD
        //                        ,B.FL_QUEST_ADEGZ_ASS
        //                        ,B.COD_TPA
        //                        ,B.ID_ACC_COMM
        //                        ,B.FL_POLI_CPI_PR
        //                        ,B.FL_POLI_BUNDLING
        //                        ,B.IND_POLI_PRIN_COLL
        //                        ,B.FL_VND_BUNDLE
        //                        ,B.IB_BS
        //                        ,B.FL_POLI_IFP
        //                        ,C.ID_STAT_OGG_BUS
        //                        ,C.ID_OGG
        //                        ,C.TP_OGG
        //                        ,C.ID_MOVI_CRZ
        //                        ,C.ID_MOVI_CHIU
        //                        ,C.DT_INI_EFF
        //                        ,C.DT_END_EFF
        //                        ,C.COD_COMP_ANIA
        //                        ,C.TP_STAT_BUS
        //                        ,C.TP_CAUS
        //                        ,C.DS_RIGA
        //                        ,C.DS_OPER_SQL
        //                        ,C.DS_VER
        //                        ,C.DS_TS_INI_CPTZ
        //                        ,C.DS_TS_END_CPTZ
        //                        ,C.DS_UTENTE
        //                        ,C.DS_STATO_ELAB
        //                     FROM ADES         A,
        //                          POLI         B,
        //                          STAT_OGG_BUS C
        //                     WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
        //                                                :WS-TP-FRM-ASSVA2)
        //                       AND A.ID_POLI         =   B.ID_POLI
        //                       AND A.ID_ADES         =   C.ID_OGG
        //                       AND C.TP_OGG          =  'AD'
        //                       AND C.TP_STAT_BUS    =  'VI'
        //           *           OR
        //           *               (C.TP_STAT_BUS    =  'ST'  AND
        //           *               EXISTS (SELECT
        //           *                     D.ID_MOVI_FINRIO
        //           *
        //           *                  FROM  MOVI_FINRIO D
        //           *                  WHERE A.ID_ADES = D.ID_ADES
        //           *                    AND D.STAT_MOVI IN ('AL','IE')
        //           *                    AND D.TP_MOVI_FINRIO = 'DI'
        //           *                    AND D.COD_COMP_ANIA   =
        //           *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
        //           *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
        //           *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //           *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
        //                     AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                     AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                     AND A.DT_END_EFF     >   :WS-DT-PTF-X
        //                     AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                     AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                     AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
        //                     AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                     AND B.DT_END_EFF     >   :WS-DT-PTF-X
        //                     AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                     AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                     AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
        //                     AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                     AND C.DT_END_EFF     >   :WS-DT-PTF-X
        //                     AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                     AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //           *         AND A.DS_STATO_ELAB IN (
        //           *                                 :IABV0002-STATE-01,
        //           *                                 :IABV0002-STATE-02,
        //           *                                 :IABV0002-STATE-03,
        //           *                                 :IABV0002-STATE-04,
        //           *                                 :IABV0002-STATE-05,
        //           *                                 :IABV0002-STATE-06,
        //           *                                 :IABV0002-STATE-07,
        //           *                                 :IABV0002-STATE-08,
        //           *                                 :IABV0002-STATE-09,
        //           *                                 :IABV0002-STATE-10
        //           *                                     )
        //           *           AND NOT A.DS_VER     = :IABV0009-VERSIONING
        //                       ORDER BY A.ID_POLI, A.ID_ADES
        //                   END-EXEC
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE
        //continue
        }
    }

    /**Original name: A310-SELECT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310SelectSc01() {
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              END-EXEC
        //           ELSE
        //              END-EXEC
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
            //         AND A.DS_STATO_ELAB IN   (
            //                                   :IABV0002-STATE-01,
            //                                   :IABV0002-STATE-02,
            //                                   :IABV0002-STATE-03,
            //                                   :IABV0002-STATE-04,
            //                                   :IABV0002-STATE-05,
            //                                   :IABV0002-STATE-06,
            //                                   :IABV0002-STATE-07,
            //                                   :IABV0002-STATE-08,
            //                                   :IABV0002-STATE-09,
            //                                   :IABV0002-STATE-10
            //                                    )
            //           AND NOT A.DS_VER     = :IABV0009-VERSIONING
            // COB_CODE:         EXEC SQL
            //                     SELECT
            //                         A.ID_ADES
            //                        ,A.ID_POLI
            //                        ,A.ID_MOVI_CRZ
            //                        ,A.ID_MOVI_CHIU
            //                        ,A.DT_INI_EFF
            //                        ,A.DT_END_EFF
            //                        ,A.IB_PREV
            //                        ,A.IB_OGG
            //                        ,A.COD_COMP_ANIA
            //                        ,A.DT_DECOR
            //                        ,A.DT_SCAD
            //                        ,A.ETA_A_SCAD
            //                        ,A.DUR_AA
            //                        ,A.DUR_MM
            //                        ,A.DUR_GG
            //                        ,A.TP_RGM_FISC
            //                        ,A.TP_RIAT
            //                        ,A.TP_MOD_PAG_TIT
            //                        ,A.TP_IAS
            //                        ,A.DT_VARZ_TP_IAS
            //                        ,A.PRE_NET_IND
            //                        ,A.PRE_LRD_IND
            //                        ,A.RAT_LRD_IND
            //                        ,A.PRSTZ_INI_IND
            //                        ,A.FL_COINC_ASSTO
            //                        ,A.IB_DFLT
            //                        ,A.MOD_CALC
            //                        ,A.TP_FNT_CNBTVA
            //                        ,A.IMP_AZ
            //                        ,A.IMP_ADER
            //                        ,A.IMP_TFR
            //                        ,A.IMP_VOLO
            //                        ,A.PC_AZ
            //                        ,A.PC_ADER
            //                        ,A.PC_TFR
            //                        ,A.PC_VOLO
            //                        ,A.DT_NOVA_RGM_FISC
            //                        ,A.FL_ATTIV
            //                        ,A.IMP_REC_RIT_VIS
            //                        ,A.IMP_REC_RIT_ACC
            //                        ,A.FL_VARZ_STAT_TBGC
            //                        ,A.FL_PROVZA_MIGRAZ
            //                        ,A.IMPB_VIS_DA_REC
            //                        ,A.DT_DECOR_PREST_BAN
            //                        ,A.DT_EFF_VARZ_STAT_T
            //                        ,A.DS_RIGA
            //                        ,A.DS_OPER_SQL
            //                        ,A.DS_VER
            //                        ,A.DS_TS_INI_CPTZ
            //                        ,A.DS_TS_END_CPTZ
            //                        ,A.DS_UTENTE
            //                        ,A.DS_STATO_ELAB
            //                        ,A.CUM_CNBT_CAP
            //                        ,A.IMP_GAR_CNBT
            //                        ,A.DT_ULT_CONS_CNBT
            //                        ,A.IDEN_ISC_FND
            //                        ,A.NUM_RAT_PIAN
            //                        ,A.DT_PRESC
            //                        ,A.CONCS_PREST
            //                        ,B.ID_POLI
            //                        ,B.ID_MOVI_CRZ
            //                        ,B.ID_MOVI_CHIU
            //                        ,B.IB_OGG
            //                        ,B.IB_PROP
            //                        ,B.DT_PROP
            //                        ,B.DT_INI_EFF
            //                        ,B.DT_END_EFF
            //                        ,B.COD_COMP_ANIA
            //                        ,B.DT_DECOR
            //                        ,B.DT_EMIS
            //                        ,B.TP_POLI
            //                        ,B.DUR_AA
            //                        ,B.DUR_MM
            //                        ,B.DT_SCAD
            //                        ,B.COD_PROD
            //                        ,B.DT_INI_VLDT_PROD
            //                        ,B.COD_CONV
            //                        ,B.COD_RAMO
            //                        ,B.DT_INI_VLDT_CONV
            //                        ,B.DT_APPLZ_CONV
            //                        ,B.TP_FRM_ASSVA
            //                        ,B.TP_RGM_FISC
            //                        ,B.FL_ESTAS
            //                        ,B.FL_RSH_COMUN
            //                        ,B.FL_RSH_COMUN_COND
            //                        ,B.TP_LIV_GENZ_TIT
            //                        ,B.FL_COP_FINANZ
            //                        ,B.TP_APPLZ_DIR
            //                        ,B.SPE_MED
            //                        ,B.DIR_EMIS
            //                        ,B.DIR_1O_VERS
            //                        ,B.DIR_VERS_AGG
            //                        ,B.COD_DVS
            //                        ,B.FL_FNT_AZ
            //                        ,B.FL_FNT_ADER
            //                        ,B.FL_FNT_TFR
            //                        ,B.FL_FNT_VOLO
            //                        ,B.TP_OPZ_A_SCAD
            //                        ,B.AA_DIFF_PROR_DFLT
            //                        ,B.FL_VER_PROD
            //                        ,B.DUR_GG
            //                        ,B.DIR_QUIET
            //                        ,B.TP_PTF_ESTNO
            //                        ,B.FL_CUM_PRE_CNTR
            //                        ,B.FL_AMMB_MOVI
            //                        ,B.CONV_GECO
            //                        ,B.DS_RIGA
            //                        ,B.DS_OPER_SQL
            //                        ,B.DS_VER
            //                        ,B.DS_TS_INI_CPTZ
            //                        ,B.DS_TS_END_CPTZ
            //                        ,B.DS_UTENTE
            //                        ,B.DS_STATO_ELAB
            //                        ,B.FL_SCUDO_FISC
            //                        ,B.FL_TRASFE
            //                        ,B.FL_TFR_STRC
            //                        ,B.DT_PRESC
            //                        ,B.COD_CONV_AGG
            //                        ,B.SUBCAT_PROD
            //                        ,B.FL_QUEST_ADEGZ_ASS
            //                        ,B.COD_TPA
            //                        ,B.ID_ACC_COMM
            //                        ,B.FL_POLI_CPI_PR
            //                        ,B.FL_POLI_BUNDLING
            //                        ,B.IND_POLI_PRIN_COLL
            //                        ,B.FL_VND_BUNDLE
            //                        ,B.IB_BS
            //                        ,B.FL_POLI_IFP
            //                        ,C.ID_STAT_OGG_BUS
            //                        ,C.ID_OGG
            //                        ,C.TP_OGG
            //                        ,C.ID_MOVI_CRZ
            //                        ,C.ID_MOVI_CHIU
            //                        ,C.DT_INI_EFF
            //                        ,C.DT_END_EFF
            //                        ,C.COD_COMP_ANIA
            //                        ,C.TP_STAT_BUS
            //                        ,C.TP_CAUS
            //                        ,C.DS_RIGA
            //                        ,C.DS_OPER_SQL
            //                        ,C.DS_VER
            //                        ,C.DS_TS_INI_CPTZ
            //                        ,C.DS_TS_END_CPTZ
            //                        ,C.DS_UTENTE
            //                        ,C.DS_STATO_ELAB
            //                     INTO
            //                        :ADE-ID-ADES
            //                       ,:ADE-ID-POLI
            //                       ,:ADE-ID-MOVI-CRZ
            //                       ,:ADE-ID-MOVI-CHIU
            //                        :IND-ADE-ID-MOVI-CHIU
            //                       ,:ADE-DT-INI-EFF-DB
            //                       ,:ADE-DT-END-EFF-DB
            //                       ,:ADE-IB-PREV
            //                        :IND-ADE-IB-PREV
            //                       ,:ADE-IB-OGG
            //                        :IND-ADE-IB-OGG
            //                       ,:ADE-COD-COMP-ANIA
            //                       ,:ADE-DT-DECOR-DB
            //                        :IND-ADE-DT-DECOR
            //                       ,:ADE-DT-SCAD-DB
            //                        :IND-ADE-DT-SCAD
            //                       ,:ADE-ETA-A-SCAD
            //                        :IND-ADE-ETA-A-SCAD
            //                       ,:ADE-DUR-AA
            //                        :IND-ADE-DUR-AA
            //                       ,:ADE-DUR-MM
            //                        :IND-ADE-DUR-MM
            //                       ,:ADE-DUR-GG
            //                        :IND-ADE-DUR-GG
            //                       ,:ADE-TP-RGM-FISC
            //                       ,:ADE-TP-RIAT
            //                        :IND-ADE-TP-RIAT
            //                       ,:ADE-TP-MOD-PAG-TIT
            //                       ,:ADE-TP-IAS
            //                        :IND-ADE-TP-IAS
            //                       ,:ADE-DT-VARZ-TP-IAS-DB
            //                        :IND-ADE-DT-VARZ-TP-IAS
            //                       ,:ADE-PRE-NET-IND
            //                        :IND-ADE-PRE-NET-IND
            //                       ,:ADE-PRE-LRD-IND
            //                        :IND-ADE-PRE-LRD-IND
            //                       ,:ADE-RAT-LRD-IND
            //                        :IND-ADE-RAT-LRD-IND
            //                       ,:ADE-PRSTZ-INI-IND
            //                        :IND-ADE-PRSTZ-INI-IND
            //                       ,:ADE-FL-COINC-ASSTO
            //                        :IND-ADE-FL-COINC-ASSTO
            //                       ,:ADE-IB-DFLT
            //                        :IND-ADE-IB-DFLT
            //                       ,:ADE-MOD-CALC
            //                        :IND-ADE-MOD-CALC
            //                       ,:ADE-TP-FNT-CNBTVA
            //                        :IND-ADE-TP-FNT-CNBTVA
            //                       ,:ADE-IMP-AZ
            //                        :IND-ADE-IMP-AZ
            //                       ,:ADE-IMP-ADER
            //                        :IND-ADE-IMP-ADER
            //                       ,:ADE-IMP-TFR
            //                        :IND-ADE-IMP-TFR
            //                       ,:ADE-IMP-VOLO
            //                        :IND-ADE-IMP-VOLO
            //                       ,:ADE-PC-AZ
            //                        :IND-ADE-PC-AZ
            //                       ,:ADE-PC-ADER
            //                        :IND-ADE-PC-ADER
            //                       ,:ADE-PC-TFR
            //                        :IND-ADE-PC-TFR
            //                       ,:ADE-PC-VOLO
            //                        :IND-ADE-PC-VOLO
            //                       ,:ADE-DT-NOVA-RGM-FISC-DB
            //                        :IND-ADE-DT-NOVA-RGM-FISC
            //                       ,:ADE-FL-ATTIV
            //                        :IND-ADE-FL-ATTIV
            //                       ,:ADE-IMP-REC-RIT-VIS
            //                        :IND-ADE-IMP-REC-RIT-VIS
            //                       ,:ADE-IMP-REC-RIT-ACC
            //                        :IND-ADE-IMP-REC-RIT-ACC
            //                       ,:ADE-FL-VARZ-STAT-TBGC
            //                        :IND-ADE-FL-VARZ-STAT-TBGC
            //                       ,:ADE-FL-PROVZA-MIGRAZ
            //                        :IND-ADE-FL-PROVZA-MIGRAZ
            //                       ,:ADE-IMPB-VIS-DA-REC
            //                        :IND-ADE-IMPB-VIS-DA-REC
            //                       ,:ADE-DT-DECOR-PREST-BAN-DB
            //                        :IND-ADE-DT-DECOR-PREST-BAN
            //                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
            //                        :IND-ADE-DT-EFF-VARZ-STAT-T
            //                       ,:ADE-DS-RIGA
            //                       ,:ADE-DS-OPER-SQL
            //                       ,:ADE-DS-VER
            //                       ,:ADE-DS-TS-INI-CPTZ
            //                       ,:ADE-DS-TS-END-CPTZ
            //                       ,:ADE-DS-UTENTE
            //                       ,:ADE-DS-STATO-ELAB
            //                       ,:ADE-CUM-CNBT-CAP
            //                        :IND-ADE-CUM-CNBT-CAP
            //                       ,:ADE-IMP-GAR-CNBT
            //                        :IND-ADE-IMP-GAR-CNBT
            //                       ,:ADE-DT-ULT-CONS-CNBT-DB
            //                        :IND-ADE-DT-ULT-CONS-CNBT
            //                       ,:ADE-IDEN-ISC-FND
            //                        :IND-ADE-IDEN-ISC-FND
            //                       ,:ADE-NUM-RAT-PIAN
            //                        :IND-ADE-NUM-RAT-PIAN
            //                       ,:ADE-DT-PRESC-DB
            //                        :IND-ADE-DT-PRESC
            //                       ,:ADE-CONCS-PREST
            //                        :IND-ADE-CONCS-PREST
            //                       ,:POL-ID-POLI
            //                       ,:POL-ID-MOVI-CRZ
            //                       ,:POL-ID-MOVI-CHIU
            //                        :IND-POL-ID-MOVI-CHIU
            //                       ,:POL-IB-OGG
            //                        :IND-POL-IB-OGG
            //                       ,:POL-IB-PROP
            //                       ,:POL-DT-PROP-DB
            //                        :IND-POL-DT-PROP
            //                       ,:POL-DT-INI-EFF-DB
            //                       ,:POL-DT-END-EFF-DB
            //                       ,:POL-COD-COMP-ANIA
            //                       ,:POL-DT-DECOR-DB
            //                       ,:POL-DT-EMIS-DB
            //                       ,:POL-TP-POLI
            //                       ,:POL-DUR-AA
            //                        :IND-POL-DUR-AA
            //                       ,:POL-DUR-MM
            //                        :IND-POL-DUR-MM
            //                       ,:POL-DT-SCAD-DB
            //                        :IND-POL-DT-SCAD
            //                       ,:POL-COD-PROD
            //                       ,:POL-DT-INI-VLDT-PROD-DB
            //                       ,:POL-COD-CONV
            //                        :IND-POL-COD-CONV
            //                       ,:POL-COD-RAMO
            //                        :IND-POL-COD-RAMO
            //                       ,:POL-DT-INI-VLDT-CONV-DB
            //                        :IND-POL-DT-INI-VLDT-CONV
            //                       ,:POL-DT-APPLZ-CONV-DB
            //                        :IND-POL-DT-APPLZ-CONV
            //                       ,:POL-TP-FRM-ASSVA
            //                       ,:POL-TP-RGM-FISC
            //                        :IND-POL-TP-RGM-FISC
            //                       ,:POL-FL-ESTAS
            //                        :IND-POL-FL-ESTAS
            //                       ,:POL-FL-RSH-COMUN
            //                        :IND-POL-FL-RSH-COMUN
            //                       ,:POL-FL-RSH-COMUN-COND
            //                        :IND-POL-FL-RSH-COMUN-COND
            //                       ,:POL-TP-LIV-GENZ-TIT
            //                       ,:POL-FL-COP-FINANZ
            //                        :IND-POL-FL-COP-FINANZ
            //                       ,:POL-TP-APPLZ-DIR
            //                        :IND-POL-TP-APPLZ-DIR
            //                       ,:POL-SPE-MED
            //                        :IND-POL-SPE-MED
            //                       ,:POL-DIR-EMIS
            //                        :IND-POL-DIR-EMIS
            //                       ,:POL-DIR-1O-VERS
            //                        :IND-POL-DIR-1O-VERS
            //                       ,:POL-DIR-VERS-AGG
            //                        :IND-POL-DIR-VERS-AGG
            //                       ,:POL-COD-DVS
            //                        :IND-POL-COD-DVS
            //                       ,:POL-FL-FNT-AZ
            //                        :IND-POL-FL-FNT-AZ
            //                       ,:POL-FL-FNT-ADER
            //                        :IND-POL-FL-FNT-ADER
            //                       ,:POL-FL-FNT-TFR
            //                        :IND-POL-FL-FNT-TFR
            //                       ,:POL-FL-FNT-VOLO
            //                        :IND-POL-FL-FNT-VOLO
            //                       ,:POL-TP-OPZ-A-SCAD
            //                        :IND-POL-TP-OPZ-A-SCAD
            //                       ,:POL-AA-DIFF-PROR-DFLT
            //                        :IND-POL-AA-DIFF-PROR-DFLT
            //                       ,:POL-FL-VER-PROD
            //                        :IND-POL-FL-VER-PROD
            //                       ,:POL-DUR-GG
            //                        :IND-POL-DUR-GG
            //                       ,:POL-DIR-QUIET
            //                        :IND-POL-DIR-QUIET
            //                       ,:POL-TP-PTF-ESTNO
            //                        :IND-POL-TP-PTF-ESTNO
            //                       ,:POL-FL-CUM-PRE-CNTR
            //                        :IND-POL-FL-CUM-PRE-CNTR
            //                       ,:POL-FL-AMMB-MOVI
            //                        :IND-POL-FL-AMMB-MOVI
            //                       ,:POL-CONV-GECO
            //                        :IND-POL-CONV-GECO
            //                       ,:POL-DS-RIGA
            //                       ,:POL-DS-OPER-SQL
            //                       ,:POL-DS-VER
            //                       ,:POL-DS-TS-INI-CPTZ
            //                       ,:POL-DS-TS-END-CPTZ
            //                       ,:POL-DS-UTENTE
            //                       ,:POL-DS-STATO-ELAB
            //                       ,:POL-FL-SCUDO-FISC
            //                        :IND-POL-FL-SCUDO-FISC
            //                       ,:POL-FL-TRASFE
            //                        :IND-POL-FL-TRASFE
            //                       ,:POL-FL-TFR-STRC
            //                        :IND-POL-FL-TFR-STRC
            //                       ,:POL-DT-PRESC-DB
            //                        :IND-POL-DT-PRESC
            //                       ,:POL-COD-CONV-AGG
            //                        :IND-POL-COD-CONV-AGG
            //                       ,:POL-SUBCAT-PROD
            //                        :IND-POL-SUBCAT-PROD
            //                       ,:POL-FL-QUEST-ADEGZ-ASS
            //                        :IND-POL-FL-QUEST-ADEGZ-ASS
            //                       ,:POL-COD-TPA
            //                        :IND-POL-COD-TPA
            //                       ,:POL-ID-ACC-COMM
            //                        :IND-POL-ID-ACC-COMM
            //                       ,:POL-FL-POLI-CPI-PR
            //                        :IND-POL-FL-POLI-CPI-PR
            //                       ,:POL-FL-POLI-BUNDLING
            //                        :IND-POL-FL-POLI-BUNDLING
            //                       ,:POL-IND-POLI-PRIN-COLL
            //                        :IND-POL-IND-POLI-PRIN-COLL
            //                       ,:POL-FL-VND-BUNDLE
            //                        :IND-POL-FL-VND-BUNDLE
            //                       ,:POL-IB-BS
            //                        :IND-POL-IB-BS
            //                       ,:POL-FL-POLI-IFP
            //                        :IND-POL-FL-POLI-IFP
            //                       ,:STB-ID-STAT-OGG-BUS
            //                       ,:STB-ID-OGG
            //                       ,:STB-TP-OGG
            //                       ,:STB-ID-MOVI-CRZ
            //                       ,:STB-ID-MOVI-CHIU
            //                        :IND-STB-ID-MOVI-CHIU
            //                       ,:STB-DT-INI-EFF-DB
            //                       ,:STB-DT-END-EFF-DB
            //                       ,:STB-COD-COMP-ANIA
            //                       ,:STB-TP-STAT-BUS
            //                       ,:STB-TP-CAUS
            //                       ,:STB-DS-RIGA
            //                       ,:STB-DS-OPER-SQL
            //                       ,:STB-DS-VER
            //                       ,:STB-DS-TS-INI-CPTZ
            //                       ,:STB-DS-TS-END-CPTZ
            //                       ,:STB-DS-UTENTE
            //                       ,:STB-DS-STATO-ELAB
            //                     FROM ADES         A,
            //                          POLI         B,
            //                          STAT_OGG_BUS C
            //                     WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
            //                                                :WS-TP-FRM-ASSVA2)
            //                       AND A.ID_POLI         =   B.ID_POLI
            //                       AND A.ID_ADES         =   C.ID_OGG
            //                       AND C.TP_OGG          =  'AD'
            //                       AND C.TP_STAT_BUS     =  'VI'
            //           *           OR
            //           *               (C.TP_STAT_BUS    =  'ST'  AND
            //           *               EXISTS(SELECT
            //           *                     D.ID_MOVI_FINRIO
            //           *
            //           *                  FROM  MOVI_FINRIO D
            //           *                  WHERE A.ID_ADES = D.ID_ADES
            //           *                    AND D.STAT_MOVI IN ('AL','IE')
            //           *                    AND D.TP_MOVI_FINRIO = 'DI'
            //           *                    AND D.COD_COMP_ANIA   =
            //           *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
            //           *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
            //           *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
            //           *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
            //           *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
            //                     AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
            //                     AND A.ID_POLI        BETWEEN   :IABV0009-ID-OGG-DA AND
            //                                                    :IABV0009-ID-OGG-A
            //                     AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
            //                     AND A.DT_END_EFF     >   :WS-DT-PTF-X
            //                     AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
            //                     AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
            //                     AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
            //                     AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
            //                     AND B.DT_END_EFF     >   :WS-DT-PTF-X
            //                     AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
            //                     AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
            //                     AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
            //                     AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
            //                     AND C.DT_END_EFF     >   :WS-DT-PTF-X
            //                     AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
            //                     AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
            //           *         AND A.DS_STATO_ELAB IN   (
            //           *                                   :IABV0002-STATE-01,
            //           *                                   :IABV0002-STATE-02,
            //           *                                   :IABV0002-STATE-03,
            //           *                                   :IABV0002-STATE-04,
            //           *                                   :IABV0002-STATE-05,
            //           *                                   :IABV0002-STATE-06,
            //           *                                   :IABV0002-STATE-07,
            //           *                                   :IABV0002-STATE-08,
            //           *                                   :IABV0002-STATE-09,
            //           *                                   :IABV0002-STATE-10
            //           *                                    )
            //           *           AND NOT A.DS_VER     = :IABV0009-VERSIONING
            //                      FETCH FIRST ROW ONLY
            //                   END-EXEC
            adesPoliDao.selectRec(this.getAdesPoliLdbm0250());
        }
        else {
            //         AND A.DS_STATO_ELAB IN   (
            //                                   :IABV0002-STATE-01,
            //                                   :IABV0002-STATE-02,
            //                                   :IABV0002-STATE-03,
            //                                   :IABV0002-STATE-04,
            //                                   :IABV0002-STATE-05,
            //                                   :IABV0002-STATE-06,
            //                                   :IABV0002-STATE-07,
            //                                   :IABV0002-STATE-08,
            //                                   :IABV0002-STATE-09,
            //                                   :IABV0002-STATE-10
            //                                    )
            //           AND NOT A.DS_VER     = :IABV0009-VERSIONING
            // COB_CODE:         EXEC SQL
            //                     SELECT
            //                         A.ID_ADES
            //                        ,A.ID_POLI
            //                        ,A.ID_MOVI_CRZ
            //                        ,A.ID_MOVI_CHIU
            //                        ,A.DT_INI_EFF
            //                        ,A.DT_END_EFF
            //                        ,A.IB_PREV
            //                        ,A.IB_OGG
            //                        ,A.COD_COMP_ANIA
            //                        ,A.DT_DECOR
            //                        ,A.DT_SCAD
            //                        ,A.ETA_A_SCAD
            //                        ,A.DUR_AA
            //                        ,A.DUR_MM
            //                        ,A.DUR_GG
            //                        ,A.TP_RGM_FISC
            //                        ,A.TP_RIAT
            //                        ,A.TP_MOD_PAG_TIT
            //                        ,A.TP_IAS
            //                        ,A.DT_VARZ_TP_IAS
            //                        ,A.PRE_NET_IND
            //                        ,A.PRE_LRD_IND
            //                        ,A.RAT_LRD_IND
            //                        ,A.PRSTZ_INI_IND
            //                        ,A.FL_COINC_ASSTO
            //                        ,A.IB_DFLT
            //                        ,A.MOD_CALC
            //                        ,A.TP_FNT_CNBTVA
            //                        ,A.IMP_AZ
            //                        ,A.IMP_ADER
            //                        ,A.IMP_TFR
            //                        ,A.IMP_VOLO
            //                        ,A.PC_AZ
            //                        ,A.PC_ADER
            //                        ,A.PC_TFR
            //                        ,A.PC_VOLO
            //                        ,A.DT_NOVA_RGM_FISC
            //                        ,A.FL_ATTIV
            //                        ,A.IMP_REC_RIT_VIS
            //                        ,A.IMP_REC_RIT_ACC
            //                        ,A.FL_VARZ_STAT_TBGC
            //                        ,A.FL_PROVZA_MIGRAZ
            //                        ,A.IMPB_VIS_DA_REC
            //                        ,A.DT_DECOR_PREST_BAN
            //                        ,A.DT_EFF_VARZ_STAT_T
            //                        ,A.DS_RIGA
            //                        ,A.DS_OPER_SQL
            //                        ,A.DS_VER
            //                        ,A.DS_TS_INI_CPTZ
            //                        ,A.DS_TS_END_CPTZ
            //                        ,A.DS_UTENTE
            //                        ,A.DS_STATO_ELAB
            //                        ,A.CUM_CNBT_CAP
            //                        ,A.IMP_GAR_CNBT
            //                        ,A.DT_ULT_CONS_CNBT
            //                        ,A.IDEN_ISC_FND
            //                        ,A.NUM_RAT_PIAN
            //                        ,A.DT_PRESC
            //                        ,A.CONCS_PREST
            //                        ,B.ID_POLI
            //                        ,B.ID_MOVI_CRZ
            //                        ,B.ID_MOVI_CHIU
            //                        ,B.IB_OGG
            //                        ,B.IB_PROP
            //                        ,B.DT_PROP
            //                        ,B.DT_INI_EFF
            //                        ,B.DT_END_EFF
            //                        ,B.COD_COMP_ANIA
            //                        ,B.DT_DECOR
            //                        ,B.DT_EMIS
            //                        ,B.TP_POLI
            //                        ,B.DUR_AA
            //                        ,B.DUR_MM
            //                        ,B.DT_SCAD
            //                        ,B.COD_PROD
            //                        ,B.DT_INI_VLDT_PROD
            //                        ,B.COD_CONV
            //                        ,B.COD_RAMO
            //                        ,B.DT_INI_VLDT_CONV
            //                        ,B.DT_APPLZ_CONV
            //                        ,B.TP_FRM_ASSVA
            //                        ,B.TP_RGM_FISC
            //                        ,B.FL_ESTAS
            //                        ,B.FL_RSH_COMUN
            //                        ,B.FL_RSH_COMUN_COND
            //                        ,B.TP_LIV_GENZ_TIT
            //                        ,B.FL_COP_FINANZ
            //                        ,B.TP_APPLZ_DIR
            //                        ,B.SPE_MED
            //                        ,B.DIR_EMIS
            //                        ,B.DIR_1O_VERS
            //                        ,B.DIR_VERS_AGG
            //                        ,B.COD_DVS
            //                        ,B.FL_FNT_AZ
            //                        ,B.FL_FNT_ADER
            //                        ,B.FL_FNT_TFR
            //                        ,B.FL_FNT_VOLO
            //                        ,B.TP_OPZ_A_SCAD
            //                        ,B.AA_DIFF_PROR_DFLT
            //                        ,B.FL_VER_PROD
            //                        ,B.DUR_GG
            //                        ,B.DIR_QUIET
            //                        ,B.TP_PTF_ESTNO
            //                        ,B.FL_CUM_PRE_CNTR
            //                        ,B.FL_AMMB_MOVI
            //                        ,B.CONV_GECO
            //                        ,B.DS_RIGA
            //                        ,B.DS_OPER_SQL
            //                        ,B.DS_VER
            //                        ,B.DS_TS_INI_CPTZ
            //                        ,B.DS_TS_END_CPTZ
            //                        ,B.DS_UTENTE
            //                        ,B.DS_STATO_ELAB
            //                        ,B.FL_SCUDO_FISC
            //                        ,B.FL_TRASFE
            //                        ,B.FL_TFR_STRC
            //                        ,B.DT_PRESC
            //                        ,B.COD_CONV_AGG
            //                        ,B.SUBCAT_PROD
            //                        ,B.FL_QUEST_ADEGZ_ASS
            //                        ,B.COD_TPA
            //                        ,B.ID_ACC_COMM
            //                        ,B.FL_POLI_CPI_PR
            //                        ,B.FL_POLI_BUNDLING
            //                        ,B.IND_POLI_PRIN_COLL
            //                        ,B.FL_VND_BUNDLE
            //                        ,B.IB_BS
            //                        ,B.FL_POLI_IFP
            //                        ,C.ID_STAT_OGG_BUS
            //                        ,C.ID_OGG
            //                        ,C.TP_OGG
            //                        ,C.ID_MOVI_CRZ
            //                        ,C.ID_MOVI_CHIU
            //                        ,C.DT_INI_EFF
            //                        ,C.DT_END_EFF
            //                        ,C.COD_COMP_ANIA
            //                        ,C.TP_STAT_BUS
            //                        ,C.TP_CAUS
            //                        ,C.DS_RIGA
            //                        ,C.DS_OPER_SQL
            //                        ,C.DS_VER
            //                        ,C.DS_TS_INI_CPTZ
            //                        ,C.DS_TS_END_CPTZ
            //                        ,C.DS_UTENTE
            //                        ,C.DS_STATO_ELAB
            //                     INTO
            //                        :ADE-ID-ADES
            //                       ,:ADE-ID-POLI
            //                       ,:ADE-ID-MOVI-CRZ
            //                       ,:ADE-ID-MOVI-CHIU
            //                        :IND-ADE-ID-MOVI-CHIU
            //                       ,:ADE-DT-INI-EFF-DB
            //                       ,:ADE-DT-END-EFF-DB
            //                       ,:ADE-IB-PREV
            //                        :IND-ADE-IB-PREV
            //                       ,:ADE-IB-OGG
            //                        :IND-ADE-IB-OGG
            //                       ,:ADE-COD-COMP-ANIA
            //                       ,:ADE-DT-DECOR-DB
            //                        :IND-ADE-DT-DECOR
            //                       ,:ADE-DT-SCAD-DB
            //                        :IND-ADE-DT-SCAD
            //                       ,:ADE-ETA-A-SCAD
            //                        :IND-ADE-ETA-A-SCAD
            //                       ,:ADE-DUR-AA
            //                        :IND-ADE-DUR-AA
            //                       ,:ADE-DUR-MM
            //                        :IND-ADE-DUR-MM
            //                       ,:ADE-DUR-GG
            //                        :IND-ADE-DUR-GG
            //                       ,:ADE-TP-RGM-FISC
            //                       ,:ADE-TP-RIAT
            //                        :IND-ADE-TP-RIAT
            //                       ,:ADE-TP-MOD-PAG-TIT
            //                       ,:ADE-TP-IAS
            //                        :IND-ADE-TP-IAS
            //                       ,:ADE-DT-VARZ-TP-IAS-DB
            //                        :IND-ADE-DT-VARZ-TP-IAS
            //                       ,:ADE-PRE-NET-IND
            //                        :IND-ADE-PRE-NET-IND
            //                       ,:ADE-PRE-LRD-IND
            //                        :IND-ADE-PRE-LRD-IND
            //                       ,:ADE-RAT-LRD-IND
            //                        :IND-ADE-RAT-LRD-IND
            //                       ,:ADE-PRSTZ-INI-IND
            //                        :IND-ADE-PRSTZ-INI-IND
            //                       ,:ADE-FL-COINC-ASSTO
            //                        :IND-ADE-FL-COINC-ASSTO
            //                       ,:ADE-IB-DFLT
            //                        :IND-ADE-IB-DFLT
            //                       ,:ADE-MOD-CALC
            //                        :IND-ADE-MOD-CALC
            //                       ,:ADE-TP-FNT-CNBTVA
            //                        :IND-ADE-TP-FNT-CNBTVA
            //                       ,:ADE-IMP-AZ
            //                        :IND-ADE-IMP-AZ
            //                       ,:ADE-IMP-ADER
            //                        :IND-ADE-IMP-ADER
            //                       ,:ADE-IMP-TFR
            //                        :IND-ADE-IMP-TFR
            //                       ,:ADE-IMP-VOLO
            //                        :IND-ADE-IMP-VOLO
            //                       ,:ADE-PC-AZ
            //                        :IND-ADE-PC-AZ
            //                       ,:ADE-PC-ADER
            //                        :IND-ADE-PC-ADER
            //                       ,:ADE-PC-TFR
            //                        :IND-ADE-PC-TFR
            //                       ,:ADE-PC-VOLO
            //                        :IND-ADE-PC-VOLO
            //                       ,:ADE-DT-NOVA-RGM-FISC-DB
            //                        :IND-ADE-DT-NOVA-RGM-FISC
            //                       ,:ADE-FL-ATTIV
            //                        :IND-ADE-FL-ATTIV
            //                       ,:ADE-IMP-REC-RIT-VIS
            //                        :IND-ADE-IMP-REC-RIT-VIS
            //                       ,:ADE-IMP-REC-RIT-ACC
            //                        :IND-ADE-IMP-REC-RIT-ACC
            //                       ,:ADE-FL-VARZ-STAT-TBGC
            //                        :IND-ADE-FL-VARZ-STAT-TBGC
            //                       ,:ADE-FL-PROVZA-MIGRAZ
            //                        :IND-ADE-FL-PROVZA-MIGRAZ
            //                       ,:ADE-IMPB-VIS-DA-REC
            //                        :IND-ADE-IMPB-VIS-DA-REC
            //                       ,:ADE-DT-DECOR-PREST-BAN-DB
            //                        :IND-ADE-DT-DECOR-PREST-BAN
            //                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
            //                        :IND-ADE-DT-EFF-VARZ-STAT-T
            //                       ,:ADE-DS-RIGA
            //                       ,:ADE-DS-OPER-SQL
            //                       ,:ADE-DS-VER
            //                       ,:ADE-DS-TS-INI-CPTZ
            //                       ,:ADE-DS-TS-END-CPTZ
            //                       ,:ADE-DS-UTENTE
            //                       ,:ADE-DS-STATO-ELAB
            //                       ,:ADE-CUM-CNBT-CAP
            //                        :IND-ADE-CUM-CNBT-CAP
            //                       ,:ADE-IMP-GAR-CNBT
            //                        :IND-ADE-IMP-GAR-CNBT
            //                       ,:ADE-DT-ULT-CONS-CNBT-DB
            //                        :IND-ADE-DT-ULT-CONS-CNBT
            //                       ,:ADE-IDEN-ISC-FND
            //                        :IND-ADE-IDEN-ISC-FND
            //                       ,:ADE-NUM-RAT-PIAN
            //                        :IND-ADE-NUM-RAT-PIAN
            //                       ,:ADE-DT-PRESC-DB
            //                        :IND-ADE-DT-PRESC
            //                       ,:ADE-CONCS-PREST
            //                        :IND-ADE-CONCS-PREST
            //                       ,:POL-ID-POLI
            //                       ,:POL-ID-MOVI-CRZ
            //                       ,:POL-ID-MOVI-CHIU
            //                        :IND-POL-ID-MOVI-CHIU
            //                       ,:POL-IB-OGG
            //                        :IND-POL-IB-OGG
            //                       ,:POL-IB-PROP
            //                       ,:POL-DT-PROP-DB
            //                        :IND-POL-DT-PROP
            //                       ,:POL-DT-INI-EFF-DB
            //                       ,:POL-DT-END-EFF-DB
            //                       ,:POL-COD-COMP-ANIA
            //                       ,:POL-DT-DECOR-DB
            //                       ,:POL-DT-EMIS-DB
            //                       ,:POL-TP-POLI
            //                       ,:POL-DUR-AA
            //                        :IND-POL-DUR-AA
            //                       ,:POL-DUR-MM
            //                        :IND-POL-DUR-MM
            //                       ,:POL-DT-SCAD-DB
            //                        :IND-POL-DT-SCAD
            //                       ,:POL-COD-PROD
            //                       ,:POL-DT-INI-VLDT-PROD-DB
            //                       ,:POL-COD-CONV
            //                        :IND-POL-COD-CONV
            //                       ,:POL-COD-RAMO
            //                        :IND-POL-COD-RAMO
            //                       ,:POL-DT-INI-VLDT-CONV-DB
            //                        :IND-POL-DT-INI-VLDT-CONV
            //                       ,:POL-DT-APPLZ-CONV-DB
            //                        :IND-POL-DT-APPLZ-CONV
            //                       ,:POL-TP-FRM-ASSVA
            //                       ,:POL-TP-RGM-FISC
            //                        :IND-POL-TP-RGM-FISC
            //                       ,:POL-FL-ESTAS
            //                        :IND-POL-FL-ESTAS
            //                       ,:POL-FL-RSH-COMUN
            //                        :IND-POL-FL-RSH-COMUN
            //                       ,:POL-FL-RSH-COMUN-COND
            //                        :IND-POL-FL-RSH-COMUN-COND
            //                       ,:POL-TP-LIV-GENZ-TIT
            //                       ,:POL-FL-COP-FINANZ
            //                        :IND-POL-FL-COP-FINANZ
            //                       ,:POL-TP-APPLZ-DIR
            //                        :IND-POL-TP-APPLZ-DIR
            //                       ,:POL-SPE-MED
            //                        :IND-POL-SPE-MED
            //                       ,:POL-DIR-EMIS
            //                        :IND-POL-DIR-EMIS
            //                       ,:POL-DIR-1O-VERS
            //                        :IND-POL-DIR-1O-VERS
            //                       ,:POL-DIR-VERS-AGG
            //                        :IND-POL-DIR-VERS-AGG
            //                       ,:POL-COD-DVS
            //                        :IND-POL-COD-DVS
            //                       ,:POL-FL-FNT-AZ
            //                        :IND-POL-FL-FNT-AZ
            //                       ,:POL-FL-FNT-ADER
            //                        :IND-POL-FL-FNT-ADER
            //                       ,:POL-FL-FNT-TFR
            //                        :IND-POL-FL-FNT-TFR
            //                       ,:POL-FL-FNT-VOLO
            //                        :IND-POL-FL-FNT-VOLO
            //                       ,:POL-TP-OPZ-A-SCAD
            //                        :IND-POL-TP-OPZ-A-SCAD
            //                       ,:POL-AA-DIFF-PROR-DFLT
            //                        :IND-POL-AA-DIFF-PROR-DFLT
            //                       ,:POL-FL-VER-PROD
            //                        :IND-POL-FL-VER-PROD
            //                       ,:POL-DUR-GG
            //                        :IND-POL-DUR-GG
            //                       ,:POL-DIR-QUIET
            //                        :IND-POL-DIR-QUIET
            //                       ,:POL-TP-PTF-ESTNO
            //                        :IND-POL-TP-PTF-ESTNO
            //                       ,:POL-FL-CUM-PRE-CNTR
            //                        :IND-POL-FL-CUM-PRE-CNTR
            //                       ,:POL-FL-AMMB-MOVI
            //                        :IND-POL-FL-AMMB-MOVI
            //                       ,:POL-CONV-GECO
            //                        :IND-POL-CONV-GECO
            //                       ,:POL-DS-RIGA
            //                       ,:POL-DS-OPER-SQL
            //                       ,:POL-DS-VER
            //                       ,:POL-DS-TS-INI-CPTZ
            //                       ,:POL-DS-TS-END-CPTZ
            //                       ,:POL-DS-UTENTE
            //                       ,:POL-DS-STATO-ELAB
            //                       ,:POL-FL-SCUDO-FISC
            //                        :IND-POL-FL-SCUDO-FISC
            //                       ,:POL-FL-TRASFE
            //                        :IND-POL-FL-TRASFE
            //                       ,:POL-FL-TFR-STRC
            //                        :IND-POL-FL-TFR-STRC
            //                       ,:POL-DT-PRESC-DB
            //                        :IND-POL-DT-PRESC
            //                       ,:POL-COD-CONV-AGG
            //                        :IND-POL-COD-CONV-AGG
            //                       ,:POL-SUBCAT-PROD
            //                        :IND-POL-SUBCAT-PROD
            //                       ,:POL-FL-QUEST-ADEGZ-ASS
            //                        :IND-POL-FL-QUEST-ADEGZ-ASS
            //                       ,:POL-COD-TPA
            //                        :IND-POL-COD-TPA
            //                       ,:POL-ID-ACC-COMM
            //                        :IND-POL-ID-ACC-COMM
            //                       ,:POL-FL-POLI-CPI-PR
            //                        :IND-POL-FL-POLI-CPI-PR
            //                       ,:POL-FL-POLI-BUNDLING
            //                        :IND-POL-FL-POLI-BUNDLING
            //                       ,:POL-IND-POLI-PRIN-COLL
            //                        :IND-POL-IND-POLI-PRIN-COLL
            //                       ,:POL-FL-VND-BUNDLE
            //                        :IND-POL-FL-VND-BUNDLE
            //                       ,:POL-IB-BS
            //                        :IND-POL-IB-BS
            //                       ,:POL-FL-POLI-IFP
            //                        :IND-POL-FL-POLI-IFP
            //                       ,:STB-ID-STAT-OGG-BUS
            //                       ,:STB-ID-OGG
            //                       ,:STB-TP-OGG
            //                       ,:STB-ID-MOVI-CRZ
            //                       ,:STB-ID-MOVI-CHIU
            //                        :IND-STB-ID-MOVI-CHIU
            //                       ,:STB-DT-INI-EFF-DB
            //                       ,:STB-DT-END-EFF-DB
            //                       ,:STB-COD-COMP-ANIA
            //                       ,:STB-TP-STAT-BUS
            //                       ,:STB-TP-CAUS
            //                       ,:STB-DS-RIGA
            //                       ,:STB-DS-OPER-SQL
            //                       ,:STB-DS-VER
            //                       ,:STB-DS-TS-INI-CPTZ
            //                       ,:STB-DS-TS-END-CPTZ
            //                       ,:STB-DS-UTENTE
            //                       ,:STB-DS-STATO-ELAB
            //                     FROM ADES         A,
            //                          POLI         B,
            //                          STAT_OGG_BUS C
            //                     WHERE B.TP_FRM_ASSVA   IN (:WS-TP-FRM-ASSVA1,
            //                                                :WS-TP-FRM-ASSVA2)
            //                       AND A.ID_POLI         =   B.ID_POLI
            //                       AND A.ID_ADES         =   C.ID_OGG
            //                       AND C.TP_OGG          =  'AD'
            //                       AND C.TP_STAT_BUS     =  'VI'
            //           *           OR
            //           *               (C.TP_STAT_BUS    =  'ST'  AND
            //           *               EXISTS(SELECT
            //           *                     D.ID_MOVI_FINRIO
            //           *
            //           *                  FROM  MOVI_FINRIO D
            //           *                  WHERE A.ID_ADES = D.ID_ADES
            //           *                    AND D.STAT_MOVI IN ('AL','IE')
            //           *                    AND D.TP_MOVI_FINRIO = 'DI'
            //           *                    AND D.COD_COMP_ANIA   =
            //           *                        :IDSV0003-CODICE-COMPAGNIA-ANIA
            //           *                    AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
            //           *                    AND D.DT_END_EFF     >   :WS-DT-PTF-X
            //           *                    AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
            //           *                    AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
            //                     AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
            //                     AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
            //                     AND A.DT_END_EFF     >   :WS-DT-PTF-X
            //                     AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
            //                     AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
            //                     AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
            //                     AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
            //                     AND B.DT_END_EFF     >   :WS-DT-PTF-X
            //                     AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
            //                     AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
            //                     AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
            //                     AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
            //                     AND C.DT_END_EFF     >   :WS-DT-PTF-X
            //                     AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
            //                     AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
            //           *         AND A.DS_STATO_ELAB IN   (
            //           *                                   :IABV0002-STATE-01,
            //           *                                   :IABV0002-STATE-02,
            //           *                                   :IABV0002-STATE-03,
            //           *                                   :IABV0002-STATE-04,
            //           *                                   :IABV0002-STATE-05,
            //           *                                   :IABV0002-STATE-06,
            //           *                                   :IABV0002-STATE-07,
            //           *                                   :IABV0002-STATE-08,
            //           *                                   :IABV0002-STATE-09,
            //           *                                   :IABV0002-STATE-10
            //           *                                    )
            //           *           AND NOT A.DS_VER     = :IABV0009-VERSIONING
            //                      FETCH FIRST ROW ONLY
            //                   END-EXEC
            adesPoliDao.selectRec1(this.getAdesPoliLdbm0250());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A320-UPDATE-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320UpdateSc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK-SC01         THRU A330-SC01-EX
        //              WHEN IDSV0003-WHERE-CONDITION-01
        //                   PERFORM A340-UPDATE-WHERE-COND-SC01 THRU A340-SC01-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                       THRU A345-SC01-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
            // COB_CODE: PERFORM A330-UPDATE-PK-SC01         THRU A330-SC01-EX
            a330UpdatePkSc01();
        }
        else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition01()) {
            // COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC01 THRU A340-SC01-EX
            a340UpdateWhereCondSc01();
        }
        else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
            // COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC01
            //                                               THRU A345-SC01-EX
            a345UpdateFirstActionSc01();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidLevelOper();
        }
    }

    /**Original name: A330-UPDATE-PK-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePkSc01() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=1698, because the code is unreachable.
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
        z960LengthVchar();
        //          ,DS_VER                 = :IABV0009-VERSIONING
        // COB_CODE:      EXEC SQL
        //                    UPDATE ADES
        //                    SET
        //                       DS_OPER_SQL            = :ADE-DS-OPER-SQL
        //           *          ,DS_VER                 = :IABV0009-VERSIONING
        //                      ,DS_UTENTE              = :ADE-DS-UTENTE
        //                      ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
        //                    WHERE             DS_RIGA = :ADE-DS-RIGA
        //                END-EXEC.
        adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A340-UPDATE-WHERE-COND-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateWhereCondSc01() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A345-UPDATE-FIRST-ACTION-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a345UpdateFirstActionSc01() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A350-CTRL-COMMIT<br>
	 * <pre>*****************************************************************
	 * --
	 * --    si effettuano i controlli per comandare
	 * --    un'eventuale COMMIT al BATCH EXECUTOR
	 * --    tramite il settaggio del campo IABV0002-FLAG-COMMIT
	 * --</pre>*/
    private void a350CtrlCommit() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A360-OPEN-CURSOR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursorSc01() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-SC01 THRU A305-SC01-EX.
        a305DeclareCursorSc01();
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              END-EXEC
        //           ELSE
        //              END-EXEC
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
            // COB_CODE: EXEC SQL
            //                OPEN CUR-SC01-RANGE
            //           END-EXEC
            adesPoliDao.openCurSc01Range(this.getAdesPoliLdbm0250());
        }
        else {
            // COB_CODE: EXEC SQL
            //                OPEN CUR-SC01
            //           END-EXEC
            adesPoliDao.openCurSc01(this.getAdesPoliLdbm0250());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursorSc01() {
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              END-EXEC
        //           ELSE
        //              END-EXEC
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
            // COB_CODE: EXEC SQL
            //                CLOSE CUR-SC01-RANGE
            //           END-EXEC
            adesPoliDao.closeCurSc01Range();
        }
        else {
            // COB_CODE: EXEC SQL
            //                CLOSE CUR-SC01
            //           END-EXEC
            adesPoliDao.closeCurSc01();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirstSc01() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-SC01    THRU A360-SC01-EX.
        a360OpenCursorSc01();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT-SC01  THRU A390-SC01-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC01  THRU A390-SC01-EX
            a390FetchNextSc01();
        }
    }

    /**Original name: A390-FETCH-NEXT-SC01<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNextSc01() {
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              END-EXEC
        //           ELSE
        //              END-EXEC
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
            // COB_CODE: EXEC SQL
            //             FETCH CUR-SC01-RANGE
            //           INTO
            //             :ADE-ID-ADES
            //            ,:ADE-ID-POLI
            //            ,:ADE-ID-MOVI-CRZ
            //            ,:ADE-ID-MOVI-CHIU
            //             :IND-ADE-ID-MOVI-CHIU
            //            ,:ADE-DT-INI-EFF-DB
            //            ,:ADE-DT-END-EFF-DB
            //            ,:ADE-IB-PREV
            //             :IND-ADE-IB-PREV
            //            ,:ADE-IB-OGG
            //             :IND-ADE-IB-OGG
            //            ,:ADE-COD-COMP-ANIA
            //            ,:ADE-DT-DECOR-DB
            //             :IND-ADE-DT-DECOR
            //            ,:ADE-DT-SCAD-DB
            //             :IND-ADE-DT-SCAD
            //            ,:ADE-ETA-A-SCAD
            //             :IND-ADE-ETA-A-SCAD
            //            ,:ADE-DUR-AA
            //             :IND-ADE-DUR-AA
            //            ,:ADE-DUR-MM
            //             :IND-ADE-DUR-MM
            //            ,:ADE-DUR-GG
            //             :IND-ADE-DUR-GG
            //            ,:ADE-TP-RGM-FISC
            //            ,:ADE-TP-RIAT
            //             :IND-ADE-TP-RIAT
            //            ,:ADE-TP-MOD-PAG-TIT
            //            ,:ADE-TP-IAS
            //             :IND-ADE-TP-IAS
            //            ,:ADE-DT-VARZ-TP-IAS-DB
            //             :IND-ADE-DT-VARZ-TP-IAS
            //            ,:ADE-PRE-NET-IND
            //             :IND-ADE-PRE-NET-IND
            //            ,:ADE-PRE-LRD-IND
            //             :IND-ADE-PRE-LRD-IND
            //            ,:ADE-RAT-LRD-IND
            //             :IND-ADE-RAT-LRD-IND
            //            ,:ADE-PRSTZ-INI-IND
            //             :IND-ADE-PRSTZ-INI-IND
            //            ,:ADE-FL-COINC-ASSTO
            //             :IND-ADE-FL-COINC-ASSTO
            //            ,:ADE-IB-DFLT
            //             :IND-ADE-IB-DFLT
            //            ,:ADE-MOD-CALC
            //             :IND-ADE-MOD-CALC
            //            ,:ADE-TP-FNT-CNBTVA
            //             :IND-ADE-TP-FNT-CNBTVA
            //            ,:ADE-IMP-AZ
            //             :IND-ADE-IMP-AZ
            //            ,:ADE-IMP-ADER
            //             :IND-ADE-IMP-ADER
            //            ,:ADE-IMP-TFR
            //             :IND-ADE-IMP-TFR
            //            ,:ADE-IMP-VOLO
            //             :IND-ADE-IMP-VOLO
            //            ,:ADE-PC-AZ
            //             :IND-ADE-PC-AZ
            //            ,:ADE-PC-ADER
            //             :IND-ADE-PC-ADER
            //            ,:ADE-PC-TFR
            //             :IND-ADE-PC-TFR
            //            ,:ADE-PC-VOLO
            //             :IND-ADE-PC-VOLO
            //            ,:ADE-DT-NOVA-RGM-FISC-DB
            //             :IND-ADE-DT-NOVA-RGM-FISC
            //            ,:ADE-FL-ATTIV
            //             :IND-ADE-FL-ATTIV
            //            ,:ADE-IMP-REC-RIT-VIS
            //             :IND-ADE-IMP-REC-RIT-VIS
            //            ,:ADE-IMP-REC-RIT-ACC
            //             :IND-ADE-IMP-REC-RIT-ACC
            //            ,:ADE-FL-VARZ-STAT-TBGC
            //             :IND-ADE-FL-VARZ-STAT-TBGC
            //            ,:ADE-FL-PROVZA-MIGRAZ
            //             :IND-ADE-FL-PROVZA-MIGRAZ
            //            ,:ADE-IMPB-VIS-DA-REC
            //             :IND-ADE-IMPB-VIS-DA-REC
            //            ,:ADE-DT-DECOR-PREST-BAN-DB
            //             :IND-ADE-DT-DECOR-PREST-BAN
            //            ,:ADE-DT-EFF-VARZ-STAT-T-DB
            //             :IND-ADE-DT-EFF-VARZ-STAT-T
            //            ,:ADE-DS-RIGA
            //            ,:ADE-DS-OPER-SQL
            //            ,:ADE-DS-VER
            //            ,:ADE-DS-TS-INI-CPTZ
            //            ,:ADE-DS-TS-END-CPTZ
            //            ,:ADE-DS-UTENTE
            //            ,:ADE-DS-STATO-ELAB
            //            ,:ADE-CUM-CNBT-CAP
            //             :IND-ADE-CUM-CNBT-CAP
            //            ,:ADE-IMP-GAR-CNBT
            //             :IND-ADE-IMP-GAR-CNBT
            //            ,:ADE-DT-ULT-CONS-CNBT-DB
            //             :IND-ADE-DT-ULT-CONS-CNBT
            //            ,:ADE-IDEN-ISC-FND
            //             :IND-ADE-IDEN-ISC-FND
            //            ,:ADE-NUM-RAT-PIAN
            //             :IND-ADE-NUM-RAT-PIAN
            //            ,:ADE-DT-PRESC-DB
            //             :IND-ADE-DT-PRESC
            //            ,:ADE-CONCS-PREST
            //             :IND-ADE-CONCS-PREST
            //            ,:POL-ID-POLI
            //            ,:POL-ID-MOVI-CRZ
            //            ,:POL-ID-MOVI-CHIU
            //             :IND-POL-ID-MOVI-CHIU
            //            ,:POL-IB-OGG
            //             :IND-POL-IB-OGG
            //            ,:POL-IB-PROP
            //            ,:POL-DT-PROP-DB
            //             :IND-POL-DT-PROP
            //            ,:POL-DT-INI-EFF-DB
            //            ,:POL-DT-END-EFF-DB
            //            ,:POL-COD-COMP-ANIA
            //            ,:POL-DT-DECOR-DB
            //            ,:POL-DT-EMIS-DB
            //            ,:POL-TP-POLI
            //            ,:POL-DUR-AA
            //             :IND-POL-DUR-AA
            //            ,:POL-DUR-MM
            //             :IND-POL-DUR-MM
            //            ,:POL-DT-SCAD-DB
            //             :IND-POL-DT-SCAD
            //            ,:POL-COD-PROD
            //            ,:POL-DT-INI-VLDT-PROD-DB
            //            ,:POL-COD-CONV
            //             :IND-POL-COD-CONV
            //            ,:POL-COD-RAMO
            //             :IND-POL-COD-RAMO
            //            ,:POL-DT-INI-VLDT-CONV-DB
            //             :IND-POL-DT-INI-VLDT-CONV
            //            ,:POL-DT-APPLZ-CONV-DB
            //             :IND-POL-DT-APPLZ-CONV
            //            ,:POL-TP-FRM-ASSVA
            //            ,:POL-TP-RGM-FISC
            //             :IND-POL-TP-RGM-FISC
            //            ,:POL-FL-ESTAS
            //             :IND-POL-FL-ESTAS
            //            ,:POL-FL-RSH-COMUN
            //             :IND-POL-FL-RSH-COMUN
            //            ,:POL-FL-RSH-COMUN-COND
            //             :IND-POL-FL-RSH-COMUN-COND
            //            ,:POL-TP-LIV-GENZ-TIT
            //            ,:POL-FL-COP-FINANZ
            //             :IND-POL-FL-COP-FINANZ
            //            ,:POL-TP-APPLZ-DIR
            //             :IND-POL-TP-APPLZ-DIR
            //            ,:POL-SPE-MED
            //             :IND-POL-SPE-MED
            //            ,:POL-DIR-EMIS
            //             :IND-POL-DIR-EMIS
            //            ,:POL-DIR-1O-VERS
            //             :IND-POL-DIR-1O-VERS
            //            ,:POL-DIR-VERS-AGG
            //             :IND-POL-DIR-VERS-AGG
            //            ,:POL-COD-DVS
            //             :IND-POL-COD-DVS
            //            ,:POL-FL-FNT-AZ
            //             :IND-POL-FL-FNT-AZ
            //            ,:POL-FL-FNT-ADER
            //             :IND-POL-FL-FNT-ADER
            //            ,:POL-FL-FNT-TFR
            //             :IND-POL-FL-FNT-TFR
            //            ,:POL-FL-FNT-VOLO
            //             :IND-POL-FL-FNT-VOLO
            //            ,:POL-TP-OPZ-A-SCAD
            //             :IND-POL-TP-OPZ-A-SCAD
            //            ,:POL-AA-DIFF-PROR-DFLT
            //             :IND-POL-AA-DIFF-PROR-DFLT
            //            ,:POL-FL-VER-PROD
            //             :IND-POL-FL-VER-PROD
            //            ,:POL-DUR-GG
            //             :IND-POL-DUR-GG
            //            ,:POL-DIR-QUIET
            //             :IND-POL-DIR-QUIET
            //            ,:POL-TP-PTF-ESTNO
            //             :IND-POL-TP-PTF-ESTNO
            //            ,:POL-FL-CUM-PRE-CNTR
            //             :IND-POL-FL-CUM-PRE-CNTR
            //            ,:POL-FL-AMMB-MOVI
            //             :IND-POL-FL-AMMB-MOVI
            //            ,:POL-CONV-GECO
            //             :IND-POL-CONV-GECO
            //            ,:POL-DS-RIGA
            //            ,:POL-DS-OPER-SQL
            //            ,:POL-DS-VER
            //            ,:POL-DS-TS-INI-CPTZ
            //            ,:POL-DS-TS-END-CPTZ
            //            ,:POL-DS-UTENTE
            //            ,:POL-DS-STATO-ELAB
            //            ,:POL-FL-SCUDO-FISC
            //             :IND-POL-FL-SCUDO-FISC
            //            ,:POL-FL-TRASFE
            //             :IND-POL-FL-TRASFE
            //            ,:POL-FL-TFR-STRC
            //             :IND-POL-FL-TFR-STRC
            //            ,:POL-DT-PRESC-DB
            //             :IND-POL-DT-PRESC
            //            ,:POL-COD-CONV-AGG
            //             :IND-POL-COD-CONV-AGG
            //            ,:POL-SUBCAT-PROD
            //             :IND-POL-SUBCAT-PROD
            //            ,:POL-FL-QUEST-ADEGZ-ASS
            //             :IND-POL-FL-QUEST-ADEGZ-ASS
            //            ,:POL-COD-TPA
            //             :IND-POL-COD-TPA
            //            ,:POL-ID-ACC-COMM
            //             :IND-POL-ID-ACC-COMM
            //            ,:POL-FL-POLI-CPI-PR
            //             :IND-POL-FL-POLI-CPI-PR
            //            ,:POL-FL-POLI-BUNDLING
            //             :IND-POL-FL-POLI-BUNDLING
            //            ,:POL-IND-POLI-PRIN-COLL
            //             :IND-POL-IND-POLI-PRIN-COLL
            //            ,:POL-FL-VND-BUNDLE
            //             :IND-POL-FL-VND-BUNDLE
            //            ,:POL-IB-BS
            //             :IND-POL-IB-BS
            //            ,:POL-FL-POLI-IFP
            //             :IND-POL-FL-POLI-IFP
            //            ,:STB-ID-STAT-OGG-BUS
            //            ,:STB-ID-OGG
            //            ,:STB-TP-OGG
            //            ,:STB-ID-MOVI-CRZ
            //            ,:STB-ID-MOVI-CHIU
            //             :IND-STB-ID-MOVI-CHIU
            //            ,:STB-DT-INI-EFF-DB
            //            ,:STB-DT-END-EFF-DB
            //            ,:STB-COD-COMP-ANIA
            //            ,:STB-TP-STAT-BUS
            //            ,:STB-TP-CAUS
            //            ,:STB-DS-RIGA
            //            ,:STB-DS-OPER-SQL
            //            ,:STB-DS-VER
            //            ,:STB-DS-TS-INI-CPTZ
            //            ,:STB-DS-TS-END-CPTZ
            //            ,:STB-DS-UTENTE
            //            ,:STB-DS-STATO-ELAB
            //           END-EXEC
            adesPoliDao.fetchCurSc01Range(this.getAdesPoliLdbm0250());
        }
        else {
            // COB_CODE: EXEC SQL
            //             FETCH CUR-SC01
            //           INTO
            //                :ADE-ID-ADES
            //               ,:ADE-ID-POLI
            //               ,:ADE-ID-MOVI-CRZ
            //               ,:ADE-ID-MOVI-CHIU
            //                :IND-ADE-ID-MOVI-CHIU
            //               ,:ADE-DT-INI-EFF-DB
            //               ,:ADE-DT-END-EFF-DB
            //               ,:ADE-IB-PREV
            //                :IND-ADE-IB-PREV
            //               ,:ADE-IB-OGG
            //                :IND-ADE-IB-OGG
            //               ,:ADE-COD-COMP-ANIA
            //               ,:ADE-DT-DECOR-DB
            //                :IND-ADE-DT-DECOR
            //               ,:ADE-DT-SCAD-DB
            //                :IND-ADE-DT-SCAD
            //               ,:ADE-ETA-A-SCAD
            //                :IND-ADE-ETA-A-SCAD
            //               ,:ADE-DUR-AA
            //                :IND-ADE-DUR-AA
            //               ,:ADE-DUR-MM
            //                :IND-ADE-DUR-MM
            //               ,:ADE-DUR-GG
            //                :IND-ADE-DUR-GG
            //               ,:ADE-TP-RGM-FISC
            //               ,:ADE-TP-RIAT
            //                :IND-ADE-TP-RIAT
            //               ,:ADE-TP-MOD-PAG-TIT
            //               ,:ADE-TP-IAS
            //                :IND-ADE-TP-IAS
            //               ,:ADE-DT-VARZ-TP-IAS-DB
            //                :IND-ADE-DT-VARZ-TP-IAS
            //               ,:ADE-PRE-NET-IND
            //                :IND-ADE-PRE-NET-IND
            //               ,:ADE-PRE-LRD-IND
            //                :IND-ADE-PRE-LRD-IND
            //               ,:ADE-RAT-LRD-IND
            //                :IND-ADE-RAT-LRD-IND
            //               ,:ADE-PRSTZ-INI-IND
            //                :IND-ADE-PRSTZ-INI-IND
            //               ,:ADE-FL-COINC-ASSTO
            //                :IND-ADE-FL-COINC-ASSTO
            //               ,:ADE-IB-DFLT
            //                :IND-ADE-IB-DFLT
            //               ,:ADE-MOD-CALC
            //                :IND-ADE-MOD-CALC
            //               ,:ADE-TP-FNT-CNBTVA
            //                :IND-ADE-TP-FNT-CNBTVA
            //               ,:ADE-IMP-AZ
            //                :IND-ADE-IMP-AZ
            //               ,:ADE-IMP-ADER
            //                :IND-ADE-IMP-ADER
            //               ,:ADE-IMP-TFR
            //                :IND-ADE-IMP-TFR
            //               ,:ADE-IMP-VOLO
            //                :IND-ADE-IMP-VOLO
            //               ,:ADE-PC-AZ
            //                :IND-ADE-PC-AZ
            //               ,:ADE-PC-ADER
            //                :IND-ADE-PC-ADER
            //               ,:ADE-PC-TFR
            //                :IND-ADE-PC-TFR
            //               ,:ADE-PC-VOLO
            //                :IND-ADE-PC-VOLO
            //               ,:ADE-DT-NOVA-RGM-FISC-DB
            //                :IND-ADE-DT-NOVA-RGM-FISC
            //               ,:ADE-FL-ATTIV
            //                :IND-ADE-FL-ATTIV
            //               ,:ADE-IMP-REC-RIT-VIS
            //                :IND-ADE-IMP-REC-RIT-VIS
            //               ,:ADE-IMP-REC-RIT-ACC
            //                :IND-ADE-IMP-REC-RIT-ACC
            //               ,:ADE-FL-VARZ-STAT-TBGC
            //                :IND-ADE-FL-VARZ-STAT-TBGC
            //               ,:ADE-FL-PROVZA-MIGRAZ
            //                :IND-ADE-FL-PROVZA-MIGRAZ
            //               ,:ADE-IMPB-VIS-DA-REC
            //                :IND-ADE-IMPB-VIS-DA-REC
            //               ,:ADE-DT-DECOR-PREST-BAN-DB
            //                :IND-ADE-DT-DECOR-PREST-BAN
            //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
            //                :IND-ADE-DT-EFF-VARZ-STAT-T
            //               ,:ADE-DS-RIGA
            //               ,:ADE-DS-OPER-SQL
            //               ,:ADE-DS-VER
            //               ,:ADE-DS-TS-INI-CPTZ
            //               ,:ADE-DS-TS-END-CPTZ
            //               ,:ADE-DS-UTENTE
            //               ,:ADE-DS-STATO-ELAB
            //               ,:ADE-CUM-CNBT-CAP
            //                :IND-ADE-CUM-CNBT-CAP
            //               ,:ADE-IMP-GAR-CNBT
            //                :IND-ADE-IMP-GAR-CNBT
            //               ,:ADE-DT-ULT-CONS-CNBT-DB
            //                :IND-ADE-DT-ULT-CONS-CNBT
            //               ,:ADE-IDEN-ISC-FND
            //                :IND-ADE-IDEN-ISC-FND
            //               ,:ADE-NUM-RAT-PIAN
            //                :IND-ADE-NUM-RAT-PIAN
            //               ,:ADE-DT-PRESC-DB
            //                :IND-ADE-DT-PRESC
            //               ,:ADE-CONCS-PREST
            //                :IND-ADE-CONCS-PREST
            //               ,:POL-ID-POLI
            //               ,:POL-ID-MOVI-CRZ
            //               ,:POL-ID-MOVI-CHIU
            //                :IND-POL-ID-MOVI-CHIU
            //               ,:POL-IB-OGG
            //                :IND-POL-IB-OGG
            //               ,:POL-IB-PROP
            //               ,:POL-DT-PROP-DB
            //                :IND-POL-DT-PROP
            //               ,:POL-DT-INI-EFF-DB
            //               ,:POL-DT-END-EFF-DB
            //               ,:POL-COD-COMP-ANIA
            //               ,:POL-DT-DECOR-DB
            //               ,:POL-DT-EMIS-DB
            //               ,:POL-TP-POLI
            //               ,:POL-DUR-AA
            //                :IND-POL-DUR-AA
            //               ,:POL-DUR-MM
            //                :IND-POL-DUR-MM
            //               ,:POL-DT-SCAD-DB
            //                :IND-POL-DT-SCAD
            //               ,:POL-COD-PROD
            //               ,:POL-DT-INI-VLDT-PROD-DB
            //               ,:POL-COD-CONV
            //                :IND-POL-COD-CONV
            //               ,:POL-COD-RAMO
            //                :IND-POL-COD-RAMO
            //               ,:POL-DT-INI-VLDT-CONV-DB
            //                :IND-POL-DT-INI-VLDT-CONV
            //               ,:POL-DT-APPLZ-CONV-DB
            //                :IND-POL-DT-APPLZ-CONV
            //               ,:POL-TP-FRM-ASSVA
            //               ,:POL-TP-RGM-FISC
            //                :IND-POL-TP-RGM-FISC
            //               ,:POL-FL-ESTAS
            //                :IND-POL-FL-ESTAS
            //               ,:POL-FL-RSH-COMUN
            //                :IND-POL-FL-RSH-COMUN
            //               ,:POL-FL-RSH-COMUN-COND
            //                :IND-POL-FL-RSH-COMUN-COND
            //               ,:POL-TP-LIV-GENZ-TIT
            //               ,:POL-FL-COP-FINANZ
            //                :IND-POL-FL-COP-FINANZ
            //               ,:POL-TP-APPLZ-DIR
            //                :IND-POL-TP-APPLZ-DIR
            //               ,:POL-SPE-MED
            //                :IND-POL-SPE-MED
            //               ,:POL-DIR-EMIS
            //                :IND-POL-DIR-EMIS
            //               ,:POL-DIR-1O-VERS
            //                :IND-POL-DIR-1O-VERS
            //               ,:POL-DIR-VERS-AGG
            //                :IND-POL-DIR-VERS-AGG
            //               ,:POL-COD-DVS
            //                :IND-POL-COD-DVS
            //               ,:POL-FL-FNT-AZ
            //                :IND-POL-FL-FNT-AZ
            //               ,:POL-FL-FNT-ADER
            //                :IND-POL-FL-FNT-ADER
            //               ,:POL-FL-FNT-TFR
            //                :IND-POL-FL-FNT-TFR
            //               ,:POL-FL-FNT-VOLO
            //                :IND-POL-FL-FNT-VOLO
            //               ,:POL-TP-OPZ-A-SCAD
            //                :IND-POL-TP-OPZ-A-SCAD
            //               ,:POL-AA-DIFF-PROR-DFLT
            //                :IND-POL-AA-DIFF-PROR-DFLT
            //               ,:POL-FL-VER-PROD
            //                :IND-POL-FL-VER-PROD
            //               ,:POL-DUR-GG
            //                :IND-POL-DUR-GG
            //               ,:POL-DIR-QUIET
            //                :IND-POL-DIR-QUIET
            //               ,:POL-TP-PTF-ESTNO
            //                :IND-POL-TP-PTF-ESTNO
            //               ,:POL-FL-CUM-PRE-CNTR
            //                :IND-POL-FL-CUM-PRE-CNTR
            //               ,:POL-FL-AMMB-MOVI
            //                :IND-POL-FL-AMMB-MOVI
            //               ,:POL-CONV-GECO
            //                :IND-POL-CONV-GECO
            //               ,:POL-DS-RIGA
            //               ,:POL-DS-OPER-SQL
            //               ,:POL-DS-VER
            //               ,:POL-DS-TS-INI-CPTZ
            //               ,:POL-DS-TS-END-CPTZ
            //               ,:POL-DS-UTENTE
            //               ,:POL-DS-STATO-ELAB
            //               ,:POL-FL-SCUDO-FISC
            //                :IND-POL-FL-SCUDO-FISC
            //               ,:POL-FL-TRASFE
            //                :IND-POL-FL-TRASFE
            //               ,:POL-FL-TFR-STRC
            //                :IND-POL-FL-TFR-STRC
            //               ,:POL-DT-PRESC-DB
            //                :IND-POL-DT-PRESC
            //               ,:POL-COD-CONV-AGG
            //                :IND-POL-COD-CONV-AGG
            //               ,:POL-SUBCAT-PROD
            //                :IND-POL-SUBCAT-PROD
            //               ,:POL-FL-QUEST-ADEGZ-ASS
            //                :IND-POL-FL-QUEST-ADEGZ-ASS
            //               ,:POL-COD-TPA
            //                :IND-POL-COD-TPA
            //               ,:POL-ID-ACC-COMM
            //                :IND-POL-ID-ACC-COMM
            //               ,:POL-FL-POLI-CPI-PR
            //                :IND-POL-FL-POLI-CPI-PR
            //               ,:POL-FL-POLI-BUNDLING
            //                :IND-POL-FL-POLI-BUNDLING
            //               ,:POL-IND-POLI-PRIN-COLL
            //                :IND-POL-IND-POLI-PRIN-COLL
            //               ,:POL-FL-VND-BUNDLE
            //                :IND-POL-FL-VND-BUNDLE
            //               ,:POL-IB-BS
            //                :IND-POL-IB-BS
            //               ,:POL-FL-POLI-IFP
            //                :IND-POL-FL-POLI-IFP
            //               ,:STB-ID-STAT-OGG-BUS
            //               ,:STB-ID-OGG
            //               ,:STB-TP-OGG
            //               ,:STB-ID-MOVI-CRZ
            //               ,:STB-ID-MOVI-CHIU
            //                :IND-STB-ID-MOVI-CHIU
            //               ,:STB-DT-INI-EFF-DB
            //               ,:STB-DT-END-EFF-DB
            //               ,:STB-COD-COMP-ANIA
            //               ,:STB-TP-STAT-BUS
            //               ,:STB-TP-CAUS
            //               ,:STB-DS-RIGA
            //               ,:STB-DS-OPER-SQL
            //               ,:STB-DS-VER
            //               ,:STB-DS-TS-INI-CPTZ
            //               ,:STB-DS-TS-END-CPTZ
            //               ,:STB-DS-UTENTE
            //               ,:STB-DS-STATO-ELAB
            //           END-EXEC
            adesPoliDao.fetchCurSc01(this.getAdesPoliLdbm0250());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC01 THRU A370-SC01-EX
            a370CloseCursorSc01();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A400-FINE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a400Fine() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: SC02-SELECTION-CURSOR-02<br>
	 * <pre>*****************************************************************</pre>*/
    private void sc02SelectionCursor02() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC02           THRU A310-SC02-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-SC02      THRU A360-SC02-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-SC02     THRU A370-SC02-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-SC02      THRU A380-SC02-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-SC02       THRU A390-SC02-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE-SC02           THRU A320-SC02-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER          TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC02           THRU A310-SC02-EX
            a310SelectSc02();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-SC02      THRU A360-SC02-EX
            a360OpenCursorSc02();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC02     THRU A370-SC02-EX
            a370CloseCursorSc02();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-SC02      THRU A380-SC02-EX
            a380FetchFirstSc02();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC02       THRU A390-SC02-EX
            a390FetchNextSc02();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE-SC02           THRU A320-SC02-EX
            a320UpdateSc02();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursorSc02() {
    // COB_CODE:      EXEC SQL
    //                     DECLARE CUR-SC02 CURSOR WITH HOLD FOR
    //                  SELECT
    //                      A.ID_ADES
    //                     ,A.ID_POLI
    //                     ,A.ID_MOVI_CRZ
    //                     ,A.ID_MOVI_CHIU
    //                     ,A.DT_INI_EFF
    //                     ,A.DT_END_EFF
    //                     ,A.IB_PREV
    //                     ,A.IB_OGG
    //                     ,A.COD_COMP_ANIA
    //                     ,A.DT_DECOR
    //                     ,A.DT_SCAD
    //                     ,A.ETA_A_SCAD
    //                     ,A.DUR_AA
    //                     ,A.DUR_MM
    //                     ,A.DUR_GG
    //                     ,A.TP_RGM_FISC
    //                     ,A.TP_RIAT
    //                     ,A.TP_MOD_PAG_TIT
    //                     ,A.TP_IAS
    //                     ,A.DT_VARZ_TP_IAS
    //                     ,A.PRE_NET_IND
    //                     ,A.PRE_LRD_IND
    //                     ,A.RAT_LRD_IND
    //                     ,A.PRSTZ_INI_IND
    //                     ,A.FL_COINC_ASSTO
    //                     ,A.IB_DFLT
    //                     ,A.MOD_CALC
    //                     ,A.TP_FNT_CNBTVA
    //                     ,A.IMP_AZ
    //                     ,A.IMP_ADER
    //                     ,A.IMP_TFR
    //                     ,A.IMP_VOLO
    //                     ,A.PC_AZ
    //                     ,A.PC_ADER
    //                     ,A.PC_TFR
    //                     ,A.PC_VOLO
    //                     ,A.DT_NOVA_RGM_FISC
    //                     ,A.FL_ATTIV
    //                     ,A.IMP_REC_RIT_VIS
    //                     ,A.IMP_REC_RIT_ACC
    //                     ,A.FL_VARZ_STAT_TBGC
    //                     ,A.FL_PROVZA_MIGRAZ
    //                     ,A.IMPB_VIS_DA_REC
    //                     ,A.DT_DECOR_PREST_BAN
    //                     ,A.DT_EFF_VARZ_STAT_T
    //                     ,A.DS_RIGA
    //                     ,A.DS_OPER_SQL
    //                     ,A.DS_VER
    //                     ,A.DS_TS_INI_CPTZ
    //                     ,A.DS_TS_END_CPTZ
    //                     ,A.DS_UTENTE
    //                     ,A.DS_STATO_ELAB
    //                     ,A.CUM_CNBT_CAP
    //                     ,A.IMP_GAR_CNBT
    //                     ,A.DT_ULT_CONS_CNBT
    //                     ,A.IDEN_ISC_FND
    //                     ,A.NUM_RAT_PIAN
    //                     ,A.DT_PRESC
    //                     ,A.CONCS_PREST
    //                     ,B.ID_POLI
    //                     ,B.ID_MOVI_CRZ
    //                     ,B.ID_MOVI_CHIU
    //                     ,B.IB_OGG
    //                     ,B.IB_PROP
    //                     ,B.DT_PROP
    //                     ,B.DT_INI_EFF
    //                     ,B.DT_END_EFF
    //                     ,B.COD_COMP_ANIA
    //                     ,B.DT_DECOR
    //                     ,B.DT_EMIS
    //                     ,B.TP_POLI
    //                     ,B.DUR_AA
    //                     ,B.DUR_MM
    //                     ,B.DT_SCAD
    //                     ,B.COD_PROD
    //                     ,B.DT_INI_VLDT_PROD
    //                     ,B.COD_CONV
    //                     ,B.COD_RAMO
    //                     ,B.DT_INI_VLDT_CONV
    //                     ,B.DT_APPLZ_CONV
    //                     ,B.TP_FRM_ASSVA
    //                     ,B.TP_RGM_FISC
    //                     ,B.FL_ESTAS
    //                     ,B.FL_RSH_COMUN
    //                     ,B.FL_RSH_COMUN_COND
    //                     ,B.TP_LIV_GENZ_TIT
    //                     ,B.FL_COP_FINANZ
    //                     ,B.TP_APPLZ_DIR
    //                     ,B.SPE_MED
    //                     ,B.DIR_EMIS
    //                     ,B.DIR_1O_VERS
    //                     ,B.DIR_VERS_AGG
    //                     ,B.COD_DVS
    //                     ,B.FL_FNT_AZ
    //                     ,B.FL_FNT_ADER
    //                     ,B.FL_FNT_TFR
    //                     ,B.FL_FNT_VOLO
    //                     ,B.TP_OPZ_A_SCAD
    //                     ,B.AA_DIFF_PROR_DFLT
    //                     ,B.FL_VER_PROD
    //                     ,B.DUR_GG
    //                     ,B.DIR_QUIET
    //                     ,B.TP_PTF_ESTNO
    //                     ,B.FL_CUM_PRE_CNTR
    //                     ,B.FL_AMMB_MOVI
    //                     ,B.CONV_GECO
    //                     ,B.DS_RIGA
    //                     ,B.DS_OPER_SQL
    //                     ,B.DS_VER
    //                     ,B.DS_TS_INI_CPTZ
    //                     ,B.DS_TS_END_CPTZ
    //                     ,B.DS_UTENTE
    //                     ,B.DS_STATO_ELAB
    //                     ,B.FL_SCUDO_FISC
    //                     ,B.FL_TRASFE
    //                     ,B.FL_TFR_STRC
    //                     ,B.DT_PRESC
    //                     ,B.COD_CONV_AGG
    //                     ,B.SUBCAT_PROD
    //                     ,B.FL_QUEST_ADEGZ_ASS
    //                     ,B.COD_TPA
    //                     ,B.ID_ACC_COMM
    //                     ,B.FL_POLI_CPI_PR
    //                     ,B.FL_POLI_BUNDLING
    //                     ,B.IND_POLI_PRIN_COLL
    //                     ,B.FL_VND_BUNDLE
    //                     ,B.IB_BS
    //                     ,B.FL_POLI_IFP
    //                     ,C.ID_STAT_OGG_BUS
    //                     ,C.ID_OGG
    //                     ,C.TP_OGG
    //                     ,C.ID_MOVI_CRZ
    //                     ,C.ID_MOVI_CHIU
    //                     ,C.DT_INI_EFF
    //                     ,C.DT_END_EFF
    //                     ,C.COD_COMP_ANIA
    //                     ,C.TP_STAT_BUS
    //                     ,C.TP_CAUS
    //                     ,C.DS_RIGA
    //                     ,C.DS_OPER_SQL
    //                     ,C.DS_VER
    //                     ,C.DS_TS_INI_CPTZ
    //                     ,C.DS_TS_END_CPTZ
    //                     ,C.DS_UTENTE
    //                     ,C.DS_STATO_ELAB
    //                  FROM ADES         A,
    //                       POLI         B,
    //                       STAT_OGG_BUS C
    //                  WHERE B.COD_PROD        =  :WLB-COD-PROD
    //                    AND A.ID_POLI         =   B.ID_POLI
    //                    AND A.ID_ADES         =   C.ID_OGG
    //                    AND C.TP_OGG          =  'AD'
    //                    AND C.TP_STAT_BUS     =  'VI'
    //           *        OR
    //           *            (C.TP_STAT_BUS    =  'ST'  AND
    //           *            EXISTS (SELECT
    //           *                  D.ID_MOVI_FINRIO
    //           *
    //           *               FROM  MOVI_FINRIO D
    //           *               WHERE A.ID_ADES = D.ID_ADES
    //           *                 AND D.STAT_MOVI IN ('AL','IE')
    //           *                 AND D.TP_MOVI_FINRIO = 'DI'
    //           *                 AND D.COD_COMP_ANIA   =
    //           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
    //           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
    //           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
    //           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
    //                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
    //                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
    //                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
    //                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
    //                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
    //                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
    //                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
    //                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
    //                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
    //                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
    //                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
    //           *      AND A.DS_STATO_ELAB IN (
    //           *                              :IABV0002-STATE-01,
    //           *                              :IABV0002-STATE-02,
    //           *                              :IABV0002-STATE-03,
    //           *                              :IABV0002-STATE-04,
    //           *                              :IABV0002-STATE-05,
    //           *                              :IABV0002-STATE-06,
    //           *                              :IABV0002-STATE-07,
    //           *                              :IABV0002-STATE-08,
    //           *                              :IABV0002-STATE-09,
    //           *                              :IABV0002-STATE-10
    //           *                                  )
    //           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
    //                    ORDER BY A.ID_POLI, A.ID_ADES
    //                END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-SC02<br>
	 * <pre>*****************************************************************
	 *       AND A.DS_STATO_ELAB IN (
	 *                               :IABV0002-STATE-01,
	 *                               :IABV0002-STATE-02,
	 *                               :IABV0002-STATE-03,
	 *                               :IABV0002-STATE-04,
	 *                               :IABV0002-STATE-05,
	 *                               :IABV0002-STATE-06,
	 *                               :IABV0002-STATE-07,
	 *                               :IABV0002-STATE-08,
	 *                               :IABV0002-STATE-09,
	 *                               :IABV0002-STATE-10
	 *                                   )
	 *         AND NOT A.DS_VER     = :IABV0009-VERSIONING</pre>*/
    private void a310SelectSc02() {
        // COB_CODE:      EXEC SQL
        //                  SELECT
        //                      A.ID_ADES
        //                     ,A.ID_POLI
        //                     ,A.ID_MOVI_CRZ
        //                     ,A.ID_MOVI_CHIU
        //                     ,A.DT_INI_EFF
        //                     ,A.DT_END_EFF
        //                     ,A.IB_PREV
        //                     ,A.IB_OGG
        //                     ,A.COD_COMP_ANIA
        //                     ,A.DT_DECOR
        //                     ,A.DT_SCAD
        //                     ,A.ETA_A_SCAD
        //                     ,A.DUR_AA
        //                     ,A.DUR_MM
        //                     ,A.DUR_GG
        //                     ,A.TP_RGM_FISC
        //                     ,A.TP_RIAT
        //                     ,A.TP_MOD_PAG_TIT
        //                     ,A.TP_IAS
        //                     ,A.DT_VARZ_TP_IAS
        //                     ,A.PRE_NET_IND
        //                     ,A.PRE_LRD_IND
        //                     ,A.RAT_LRD_IND
        //                     ,A.PRSTZ_INI_IND
        //                     ,A.FL_COINC_ASSTO
        //                     ,A.IB_DFLT
        //                     ,A.MOD_CALC
        //                     ,A.TP_FNT_CNBTVA
        //                     ,A.IMP_AZ
        //                     ,A.IMP_ADER
        //                     ,A.IMP_TFR
        //                     ,A.IMP_VOLO
        //                     ,A.PC_AZ
        //                     ,A.PC_ADER
        //                     ,A.PC_TFR
        //                     ,A.PC_VOLO
        //                     ,A.DT_NOVA_RGM_FISC
        //                     ,A.FL_ATTIV
        //                     ,A.IMP_REC_RIT_VIS
        //                     ,A.IMP_REC_RIT_ACC
        //                     ,A.FL_VARZ_STAT_TBGC
        //                     ,A.FL_PROVZA_MIGRAZ
        //                     ,A.IMPB_VIS_DA_REC
        //                     ,A.DT_DECOR_PREST_BAN
        //                     ,A.DT_EFF_VARZ_STAT_T
        //                     ,A.DS_RIGA
        //                     ,A.DS_OPER_SQL
        //                     ,A.DS_VER
        //                     ,A.DS_TS_INI_CPTZ
        //                     ,A.DS_TS_END_CPTZ
        //                     ,A.DS_UTENTE
        //                     ,A.DS_STATO_ELAB
        //                     ,A.CUM_CNBT_CAP
        //                     ,A.IMP_GAR_CNBT
        //                     ,A.DT_ULT_CONS_CNBT
        //                     ,A.IDEN_ISC_FND
        //                     ,A.NUM_RAT_PIAN
        //                     ,A.DT_PRESC
        //                     ,A.CONCS_PREST
        //                     ,B.ID_POLI
        //                     ,B.ID_MOVI_CRZ
        //                     ,B.ID_MOVI_CHIU
        //                     ,B.IB_OGG
        //                     ,B.IB_PROP
        //                     ,B.DT_PROP
        //                     ,B.DT_INI_EFF
        //                     ,B.DT_END_EFF
        //                     ,B.COD_COMP_ANIA
        //                     ,B.DT_DECOR
        //                     ,B.DT_EMIS
        //                     ,B.TP_POLI
        //                     ,B.DUR_AA
        //                     ,B.DUR_MM
        //                     ,B.DT_SCAD
        //                     ,B.COD_PROD
        //                     ,B.DT_INI_VLDT_PROD
        //                     ,B.COD_CONV
        //                     ,B.COD_RAMO
        //                     ,B.DT_INI_VLDT_CONV
        //                     ,B.DT_APPLZ_CONV
        //                     ,B.TP_FRM_ASSVA
        //                     ,B.TP_RGM_FISC
        //                     ,B.FL_ESTAS
        //                     ,B.FL_RSH_COMUN
        //                     ,B.FL_RSH_COMUN_COND
        //                     ,B.TP_LIV_GENZ_TIT
        //                     ,B.FL_COP_FINANZ
        //                     ,B.TP_APPLZ_DIR
        //                     ,B.SPE_MED
        //                     ,B.DIR_EMIS
        //                     ,B.DIR_1O_VERS
        //                     ,B.DIR_VERS_AGG
        //                     ,B.COD_DVS
        //                     ,B.FL_FNT_AZ
        //                     ,B.FL_FNT_ADER
        //                     ,B.FL_FNT_TFR
        //                     ,B.FL_FNT_VOLO
        //                     ,B.TP_OPZ_A_SCAD
        //                     ,B.AA_DIFF_PROR_DFLT
        //                     ,B.FL_VER_PROD
        //                     ,B.DUR_GG
        //                     ,B.DIR_QUIET
        //                     ,B.TP_PTF_ESTNO
        //                     ,B.FL_CUM_PRE_CNTR
        //                     ,B.FL_AMMB_MOVI
        //                     ,B.CONV_GECO
        //                     ,B.DS_RIGA
        //                     ,B.DS_OPER_SQL
        //                     ,B.DS_VER
        //                     ,B.DS_TS_INI_CPTZ
        //                     ,B.DS_TS_END_CPTZ
        //                     ,B.DS_UTENTE
        //                     ,B.DS_STATO_ELAB
        //                     ,B.FL_SCUDO_FISC
        //                     ,B.FL_TRASFE
        //                     ,B.FL_TFR_STRC
        //                     ,B.DT_PRESC
        //                     ,B.COD_CONV_AGG
        //                     ,B.SUBCAT_PROD
        //                     ,B.FL_QUEST_ADEGZ_ASS
        //                     ,B.COD_TPA
        //                     ,B.ID_ACC_COMM
        //                     ,B.FL_POLI_CPI_PR
        //                     ,B.FL_POLI_BUNDLING
        //                     ,B.IND_POLI_PRIN_COLL
        //                     ,B.FL_VND_BUNDLE
        //                     ,B.IB_BS
        //                     ,B.FL_POLI_IFP
        //                     ,C.ID_STAT_OGG_BUS
        //                     ,C.ID_OGG
        //                     ,C.TP_OGG
        //                     ,C.ID_MOVI_CRZ
        //                     ,C.ID_MOVI_CHIU
        //                     ,C.DT_INI_EFF
        //                     ,C.DT_END_EFF
        //                     ,C.COD_COMP_ANIA
        //                     ,C.TP_STAT_BUS
        //                     ,C.TP_CAUS
        //                     ,C.DS_RIGA
        //                     ,C.DS_OPER_SQL
        //                     ,C.DS_VER
        //                     ,C.DS_TS_INI_CPTZ
        //                     ,C.DS_TS_END_CPTZ
        //                     ,C.DS_UTENTE
        //                     ,C.DS_STATO_ELAB
        //                  INTO
        //                     :ADE-ID-ADES
        //                    ,:ADE-ID-POLI
        //                    ,:ADE-ID-MOVI-CRZ
        //                    ,:ADE-ID-MOVI-CHIU
        //                     :IND-ADE-ID-MOVI-CHIU
        //                    ,:ADE-DT-INI-EFF-DB
        //                    ,:ADE-DT-END-EFF-DB
        //                    ,:ADE-IB-PREV
        //                     :IND-ADE-IB-PREV
        //                    ,:ADE-IB-OGG
        //                     :IND-ADE-IB-OGG
        //                    ,:ADE-COD-COMP-ANIA
        //                    ,:ADE-DT-DECOR-DB
        //                     :IND-ADE-DT-DECOR
        //                    ,:ADE-DT-SCAD-DB
        //                     :IND-ADE-DT-SCAD
        //                    ,:ADE-ETA-A-SCAD
        //                     :IND-ADE-ETA-A-SCAD
        //                    ,:ADE-DUR-AA
        //                     :IND-ADE-DUR-AA
        //                    ,:ADE-DUR-MM
        //                     :IND-ADE-DUR-MM
        //                    ,:ADE-DUR-GG
        //                     :IND-ADE-DUR-GG
        //                    ,:ADE-TP-RGM-FISC
        //                    ,:ADE-TP-RIAT
        //                     :IND-ADE-TP-RIAT
        //                    ,:ADE-TP-MOD-PAG-TIT
        //                    ,:ADE-TP-IAS
        //                     :IND-ADE-TP-IAS
        //                    ,:ADE-DT-VARZ-TP-IAS-DB
        //                     :IND-ADE-DT-VARZ-TP-IAS
        //                    ,:ADE-PRE-NET-IND
        //                     :IND-ADE-PRE-NET-IND
        //                    ,:ADE-PRE-LRD-IND
        //                     :IND-ADE-PRE-LRD-IND
        //                    ,:ADE-RAT-LRD-IND
        //                     :IND-ADE-RAT-LRD-IND
        //                    ,:ADE-PRSTZ-INI-IND
        //                     :IND-ADE-PRSTZ-INI-IND
        //                    ,:ADE-FL-COINC-ASSTO
        //                     :IND-ADE-FL-COINC-ASSTO
        //                    ,:ADE-IB-DFLT
        //                     :IND-ADE-IB-DFLT
        //                    ,:ADE-MOD-CALC
        //                     :IND-ADE-MOD-CALC
        //                    ,:ADE-TP-FNT-CNBTVA
        //                     :IND-ADE-TP-FNT-CNBTVA
        //                    ,:ADE-IMP-AZ
        //                     :IND-ADE-IMP-AZ
        //                    ,:ADE-IMP-ADER
        //                     :IND-ADE-IMP-ADER
        //                    ,:ADE-IMP-TFR
        //                     :IND-ADE-IMP-TFR
        //                    ,:ADE-IMP-VOLO
        //                     :IND-ADE-IMP-VOLO
        //                    ,:ADE-PC-AZ
        //                     :IND-ADE-PC-AZ
        //                    ,:ADE-PC-ADER
        //                     :IND-ADE-PC-ADER
        //                    ,:ADE-PC-TFR
        //                     :IND-ADE-PC-TFR
        //                    ,:ADE-PC-VOLO
        //                     :IND-ADE-PC-VOLO
        //                    ,:ADE-DT-NOVA-RGM-FISC-DB
        //                     :IND-ADE-DT-NOVA-RGM-FISC
        //                    ,:ADE-FL-ATTIV
        //                     :IND-ADE-FL-ATTIV
        //                    ,:ADE-IMP-REC-RIT-VIS
        //                     :IND-ADE-IMP-REC-RIT-VIS
        //                    ,:ADE-IMP-REC-RIT-ACC
        //                     :IND-ADE-IMP-REC-RIT-ACC
        //                    ,:ADE-FL-VARZ-STAT-TBGC
        //                     :IND-ADE-FL-VARZ-STAT-TBGC
        //                    ,:ADE-FL-PROVZA-MIGRAZ
        //                     :IND-ADE-FL-PROVZA-MIGRAZ
        //                    ,:ADE-IMPB-VIS-DA-REC
        //                     :IND-ADE-IMPB-VIS-DA-REC
        //                    ,:ADE-DT-DECOR-PREST-BAN-DB
        //                     :IND-ADE-DT-DECOR-PREST-BAN
        //                    ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                     :IND-ADE-DT-EFF-VARZ-STAT-T
        //                    ,:ADE-DS-RIGA
        //                    ,:ADE-DS-OPER-SQL
        //                    ,:ADE-DS-VER
        //                    ,:ADE-DS-TS-INI-CPTZ
        //                    ,:ADE-DS-TS-END-CPTZ
        //                    ,:ADE-DS-UTENTE
        //                    ,:ADE-DS-STATO-ELAB
        //                    ,:ADE-CUM-CNBT-CAP
        //                     :IND-ADE-CUM-CNBT-CAP
        //                    ,:ADE-IMP-GAR-CNBT
        //                     :IND-ADE-IMP-GAR-CNBT
        //                    ,:ADE-DT-ULT-CONS-CNBT-DB
        //                     :IND-ADE-DT-ULT-CONS-CNBT
        //                    ,:ADE-IDEN-ISC-FND
        //                     :IND-ADE-IDEN-ISC-FND
        //                    ,:ADE-NUM-RAT-PIAN
        //                     :IND-ADE-NUM-RAT-PIAN
        //                    ,:ADE-DT-PRESC-DB
        //                     :IND-ADE-DT-PRESC
        //                    ,:ADE-CONCS-PREST
        //                     :IND-ADE-CONCS-PREST
        //                    ,:POL-ID-POLI
        //                    ,:POL-ID-MOVI-CRZ
        //                    ,:POL-ID-MOVI-CHIU
        //                     :IND-POL-ID-MOVI-CHIU
        //                    ,:POL-IB-OGG
        //                     :IND-POL-IB-OGG
        //                    ,:POL-IB-PROP
        //                    ,:POL-DT-PROP-DB
        //                     :IND-POL-DT-PROP
        //                    ,:POL-DT-INI-EFF-DB
        //                    ,:POL-DT-END-EFF-DB
        //                    ,:POL-COD-COMP-ANIA
        //                    ,:POL-DT-DECOR-DB
        //                    ,:POL-DT-EMIS-DB
        //                    ,:POL-TP-POLI
        //                    ,:POL-DUR-AA
        //                     :IND-POL-DUR-AA
        //                    ,:POL-DUR-MM
        //                     :IND-POL-DUR-MM
        //                    ,:POL-DT-SCAD-DB
        //                     :IND-POL-DT-SCAD
        //                    ,:POL-COD-PROD
        //                    ,:POL-DT-INI-VLDT-PROD-DB
        //                    ,:POL-COD-CONV
        //                     :IND-POL-COD-CONV
        //                    ,:POL-COD-RAMO
        //                     :IND-POL-COD-RAMO
        //                    ,:POL-DT-INI-VLDT-CONV-DB
        //                     :IND-POL-DT-INI-VLDT-CONV
        //                    ,:POL-DT-APPLZ-CONV-DB
        //                     :IND-POL-DT-APPLZ-CONV
        //                    ,:POL-TP-FRM-ASSVA
        //                    ,:POL-TP-RGM-FISC
        //                     :IND-POL-TP-RGM-FISC
        //                    ,:POL-FL-ESTAS
        //                     :IND-POL-FL-ESTAS
        //                    ,:POL-FL-RSH-COMUN
        //                     :IND-POL-FL-RSH-COMUN
        //                    ,:POL-FL-RSH-COMUN-COND
        //                     :IND-POL-FL-RSH-COMUN-COND
        //                    ,:POL-TP-LIV-GENZ-TIT
        //                    ,:POL-FL-COP-FINANZ
        //                     :IND-POL-FL-COP-FINANZ
        //                    ,:POL-TP-APPLZ-DIR
        //                     :IND-POL-TP-APPLZ-DIR
        //                    ,:POL-SPE-MED
        //                     :IND-POL-SPE-MED
        //                    ,:POL-DIR-EMIS
        //                     :IND-POL-DIR-EMIS
        //                    ,:POL-DIR-1O-VERS
        //                     :IND-POL-DIR-1O-VERS
        //                    ,:POL-DIR-VERS-AGG
        //                     :IND-POL-DIR-VERS-AGG
        //                    ,:POL-COD-DVS
        //                     :IND-POL-COD-DVS
        //                    ,:POL-FL-FNT-AZ
        //                     :IND-POL-FL-FNT-AZ
        //                    ,:POL-FL-FNT-ADER
        //                     :IND-POL-FL-FNT-ADER
        //                    ,:POL-FL-FNT-TFR
        //                     :IND-POL-FL-FNT-TFR
        //                    ,:POL-FL-FNT-VOLO
        //                     :IND-POL-FL-FNT-VOLO
        //                    ,:POL-TP-OPZ-A-SCAD
        //                     :IND-POL-TP-OPZ-A-SCAD
        //                    ,:POL-AA-DIFF-PROR-DFLT
        //                     :IND-POL-AA-DIFF-PROR-DFLT
        //                    ,:POL-FL-VER-PROD
        //                     :IND-POL-FL-VER-PROD
        //                    ,:POL-DUR-GG
        //                     :IND-POL-DUR-GG
        //                    ,:POL-DIR-QUIET
        //                     :IND-POL-DIR-QUIET
        //                    ,:POL-TP-PTF-ESTNO
        //                     :IND-POL-TP-PTF-ESTNO
        //                    ,:POL-FL-CUM-PRE-CNTR
        //                     :IND-POL-FL-CUM-PRE-CNTR
        //                    ,:POL-FL-AMMB-MOVI
        //                     :IND-POL-FL-AMMB-MOVI
        //                    ,:POL-CONV-GECO
        //                     :IND-POL-CONV-GECO
        //                    ,:POL-DS-RIGA
        //                    ,:POL-DS-OPER-SQL
        //                    ,:POL-DS-VER
        //                    ,:POL-DS-TS-INI-CPTZ
        //                    ,:POL-DS-TS-END-CPTZ
        //                    ,:POL-DS-UTENTE
        //                    ,:POL-DS-STATO-ELAB
        //                    ,:POL-FL-SCUDO-FISC
        //                     :IND-POL-FL-SCUDO-FISC
        //                    ,:POL-FL-TRASFE
        //                     :IND-POL-FL-TRASFE
        //                    ,:POL-FL-TFR-STRC
        //                     :IND-POL-FL-TFR-STRC
        //                    ,:POL-DT-PRESC-DB
        //                     :IND-POL-DT-PRESC
        //                    ,:POL-COD-CONV-AGG
        //                     :IND-POL-COD-CONV-AGG
        //                    ,:POL-SUBCAT-PROD
        //                     :IND-POL-SUBCAT-PROD
        //                    ,:POL-FL-QUEST-ADEGZ-ASS
        //                     :IND-POL-FL-QUEST-ADEGZ-ASS
        //                    ,:POL-COD-TPA
        //                     :IND-POL-COD-TPA
        //                    ,:POL-ID-ACC-COMM
        //                     :IND-POL-ID-ACC-COMM
        //                    ,:POL-FL-POLI-CPI-PR
        //                     :IND-POL-FL-POLI-CPI-PR
        //                    ,:POL-FL-POLI-BUNDLING
        //                     :IND-POL-FL-POLI-BUNDLING
        //                    ,:POL-IND-POLI-PRIN-COLL
        //                     :IND-POL-IND-POLI-PRIN-COLL
        //                    ,:POL-FL-VND-BUNDLE
        //                     :IND-POL-FL-VND-BUNDLE
        //                    ,:POL-IB-BS
        //                     :IND-POL-IB-BS
        //                    ,:POL-FL-POLI-IFP
        //                     :IND-POL-FL-POLI-IFP
        //                    ,:STB-ID-STAT-OGG-BUS
        //                    ,:STB-ID-OGG
        //                    ,:STB-TP-OGG
        //                    ,:STB-ID-MOVI-CRZ
        //                    ,:STB-ID-MOVI-CHIU
        //                     :IND-STB-ID-MOVI-CHIU
        //                    ,:STB-DT-INI-EFF-DB
        //                    ,:STB-DT-END-EFF-DB
        //                    ,:STB-COD-COMP-ANIA
        //                    ,:STB-TP-STAT-BUS
        //                    ,:STB-TP-CAUS
        //                    ,:STB-DS-RIGA
        //                    ,:STB-DS-OPER-SQL
        //                    ,:STB-DS-VER
        //                    ,:STB-DS-TS-INI-CPTZ
        //                    ,:STB-DS-TS-END-CPTZ
        //                    ,:STB-DS-UTENTE
        //                    ,:STB-DS-STATO-ELAB
        //                  FROM ADES         A,
        //                       POLI         B,
        //                       STAT_OGG_BUS C
        //                  WHERE B.COD_PROD        =  :WLB-COD-PROD
        //                    AND A.ID_POLI         =   B.ID_POLI
        //                    AND A.ID_ADES         =   C.ID_OGG
        //                    AND C.TP_OGG          =  'AD'
        //                    AND C.TP_STAT_BUS     =  'VI'
        //           *        OR
        //           *            (C.TP_STAT_BUS    =  'ST'  AND
        //           *            EXISTS (SELECT
        //           *                  D.ID_MOVI_FINRIO
        //           *
        //           *               FROM  MOVI_FINRIO D
        //           *               WHERE A.ID_ADES = D.ID_ADES
        //           *                 AND D.STAT_MOVI IN ('AL','IE')
        //           *                 AND D.TP_MOVI_FINRIO = 'DI'
        //           *                 AND D.COD_COMP_ANIA   =
        //           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
        //           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
        //           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
        //                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
        //                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
        //                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
        //                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
        //                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
        //                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //           *      AND A.DS_STATO_ELAB IN (
        //           *                              :IABV0002-STATE-01,
        //           *                              :IABV0002-STATE-02,
        //           *                              :IABV0002-STATE-03,
        //           *                              :IABV0002-STATE-04,
        //           *                              :IABV0002-STATE-05,
        //           *                              :IABV0002-STATE-06,
        //           *                              :IABV0002-STATE-07,
        //           *                              :IABV0002-STATE-08,
        //           *                              :IABV0002-STATE-09,
        //           *                              :IABV0002-STATE-10
        //           *                                  )
        //           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
        //                    ORDER BY A.ID_POLI, A.ID_ADES
        //                   FETCH FIRST ROW ONLY
        //                END-EXEC.
        adesPoliDao.selectRec2(ws.getWlbRecPren().getCodProd(), idsv0003.getCodiceCompagniaAnia(), ws.getWsDtPtfX(), ws.getWsDtTsPtf(), this.getAdesPoliLdbm0250());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A320-UPDATE-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320UpdateSc02() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK-SC02         THRU A330-SC02-EX
        //              WHEN IDSV0003-WHERE-CONDITION-02
        //                   PERFORM A340-UPDATE-WHERE-COND-SC02 THRU A340-SC02-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                       THRU A345-SC02-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
            // COB_CODE: PERFORM A330-UPDATE-PK-SC02         THRU A330-SC02-EX
            a330UpdatePkSc02();
        }
        else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition02()) {
            // COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC02 THRU A340-SC02-EX
            a340UpdateWhereCondSc02();
        }
        else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
            // COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC02
            //                                               THRU A345-SC02-EX
            a345UpdateFirstActionSc02();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidLevelOper();
        }
    }

    /**Original name: A330-UPDATE-PK-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePkSc02() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=3039, because the code is unreachable.
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
        z960LengthVchar();
        //           ,DS_VER                 = :IABV0009-VERSIONING
        // COB_CODE:      EXEC SQL
        //                     UPDATE ADES
        //                     SET
        //                        DS_OPER_SQL            = :ADE-DS-OPER-SQL
        //           *           ,DS_VER                 = :IABV0009-VERSIONING
        //                       ,DS_UTENTE              = :ADE-DS-UTENTE
        //                       ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
        //                     WHERE
        //                        DS_RIGA                = :ADE-DS-RIGA
        //                END-EXEC.
        adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A340-UPDATE-WHERE-COND-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateWhereCondSc02() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A345-UPDATE-FIRST-ACTION-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a345UpdateFirstActionSc02() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A360-OPEN-CURSOR-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursorSc02() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-SC02 THRU A305-SC02-EX.
        a305DeclareCursorSc02();
        // COB_CODE: EXEC SQL
        //                OPEN CUR-SC02
        //           END-EXEC.
        adesPoliDao.openCurSc02(ws.getWlbRecPren().getCodProd(), idsv0003.getCodiceCompagniaAnia(), ws.getWsDtPtfX(), ws.getWsDtTsPtf());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursorSc02() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-SC02
        //           END-EXEC.
        adesPoliDao.closeCurSc02();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirstSc02() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-SC02    THRU A360-SC02-EX.
        a360OpenCursorSc02();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT-SC02  THRU A390-SC02-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC02  THRU A390-SC02-EX
            a390FetchNextSc02();
        }
    }

    /**Original name: A390-FETCH-NEXT-SC02<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNextSc02() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-SC02
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //               ,:POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //               ,:STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        adesPoliDao.fetchCurSc02(this.getAdesPoliLdbm0250());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC02 THRU A370-SC02-EX
            a370CloseCursorSc02();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: SC03-SELECTION-CURSOR-03<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
    private void sc03SelectionCursor03() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC03           THRU A310-SC03-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-SC03      THRU A360-SC03-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-SC03     THRU A370-SC03-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-SC03      THRU A380-SC03-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-SC03       THRU A390-SC03-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE-SC03           THRU A320-SC03-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER          TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC03           THRU A310-SC03-EX
            a310SelectSc03();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-SC03      THRU A360-SC03-EX
            a360OpenCursorSc03();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC03     THRU A370-SC03-EX
            a370CloseCursorSc03();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-SC03      THRU A380-SC03-EX
            a380FetchFirstSc03();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC03       THRU A390-SC03-EX
            a390FetchNextSc03();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE-SC03           THRU A320-SC03-EX
            a320UpdateSc03();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR-SC03<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursorSc03() {
    // COB_CODE:      EXEC SQL
    //                     DECLARE CUR-SC03 CURSOR WITH HOLD FOR
    //                     SELECT
    //                      A.ID_ADES
    //                     ,A.ID_POLI
    //                     ,A.ID_MOVI_CRZ
    //                     ,A.ID_MOVI_CHIU
    //                     ,A.DT_INI_EFF
    //                     ,A.DT_END_EFF
    //                     ,A.IB_PREV
    //                     ,A.IB_OGG
    //                     ,A.COD_COMP_ANIA
    //                     ,A.DT_DECOR
    //                     ,A.DT_SCAD
    //                     ,A.ETA_A_SCAD
    //                     ,A.DUR_AA
    //                     ,A.DUR_MM
    //                     ,A.DUR_GG
    //                     ,A.TP_RGM_FISC
    //                     ,A.TP_RIAT
    //                     ,A.TP_MOD_PAG_TIT
    //                     ,A.TP_IAS
    //                     ,A.DT_VARZ_TP_IAS
    //                     ,A.PRE_NET_IND
    //                     ,A.PRE_LRD_IND
    //                     ,A.RAT_LRD_IND
    //                     ,A.PRSTZ_INI_IND
    //                     ,A.FL_COINC_ASSTO
    //                     ,A.IB_DFLT
    //                     ,A.MOD_CALC
    //                     ,A.TP_FNT_CNBTVA
    //                     ,A.IMP_AZ
    //                     ,A.IMP_ADER
    //                     ,A.IMP_TFR
    //                     ,A.IMP_VOLO
    //                     ,A.PC_AZ
    //                     ,A.PC_ADER
    //                     ,A.PC_TFR
    //                     ,A.PC_VOLO
    //                     ,A.DT_NOVA_RGM_FISC
    //                     ,A.FL_ATTIV
    //                     ,A.IMP_REC_RIT_VIS
    //                     ,A.IMP_REC_RIT_ACC
    //                     ,A.FL_VARZ_STAT_TBGC
    //                     ,A.FL_PROVZA_MIGRAZ
    //                     ,A.IMPB_VIS_DA_REC
    //                     ,A.DT_DECOR_PREST_BAN
    //                     ,A.DT_EFF_VARZ_STAT_T
    //                     ,A.DS_RIGA
    //                     ,A.DS_OPER_SQL
    //                     ,A.DS_VER
    //                     ,A.DS_TS_INI_CPTZ
    //                     ,A.DS_TS_END_CPTZ
    //                     ,A.DS_UTENTE
    //                     ,A.DS_STATO_ELAB
    //                     ,A.CUM_CNBT_CAP
    //                     ,A.IMP_GAR_CNBT
    //                     ,A.DT_ULT_CONS_CNBT
    //                     ,A.IDEN_ISC_FND
    //                     ,A.NUM_RAT_PIAN
    //                     ,A.DT_PRESC
    //                     ,A.CONCS_PREST
    //                     ,B.ID_POLI
    //                     ,B.ID_MOVI_CRZ
    //                     ,B.ID_MOVI_CHIU
    //                     ,B.IB_OGG
    //                     ,B.IB_PROP
    //                     ,B.DT_PROP
    //                     ,B.DT_INI_EFF
    //                     ,B.DT_END_EFF
    //                     ,B.COD_COMP_ANIA
    //                     ,B.DT_DECOR
    //                     ,B.DT_EMIS
    //                     ,B.TP_POLI
    //                     ,B.DUR_AA
    //                     ,B.DUR_MM
    //                     ,B.DT_SCAD
    //                     ,B.COD_PROD
    //                     ,B.DT_INI_VLDT_PROD
    //                     ,B.COD_CONV
    //                     ,B.COD_RAMO
    //                     ,B.DT_INI_VLDT_CONV
    //                     ,B.DT_APPLZ_CONV
    //                     ,B.TP_FRM_ASSVA
    //                     ,B.TP_RGM_FISC
    //                     ,B.FL_ESTAS
    //                     ,B.FL_RSH_COMUN
    //                     ,B.FL_RSH_COMUN_COND
    //                     ,B.TP_LIV_GENZ_TIT
    //                     ,B.FL_COP_FINANZ
    //                     ,B.TP_APPLZ_DIR
    //                     ,B.SPE_MED
    //                     ,B.DIR_EMIS
    //                     ,B.DIR_1O_VERS
    //                     ,B.DIR_VERS_AGG
    //                     ,B.COD_DVS
    //                     ,B.FL_FNT_AZ
    //                     ,B.FL_FNT_ADER
    //                     ,B.FL_FNT_TFR
    //                     ,B.FL_FNT_VOLO
    //                     ,B.TP_OPZ_A_SCAD
    //                     ,B.AA_DIFF_PROR_DFLT
    //                     ,B.FL_VER_PROD
    //                     ,B.DUR_GG
    //                     ,B.DIR_QUIET
    //                     ,B.TP_PTF_ESTNO
    //                     ,B.FL_CUM_PRE_CNTR
    //                     ,B.FL_AMMB_MOVI
    //                     ,B.CONV_GECO
    //                     ,B.DS_RIGA
    //                     ,B.DS_OPER_SQL
    //                     ,B.DS_VER
    //                     ,B.DS_TS_INI_CPTZ
    //                     ,B.DS_TS_END_CPTZ
    //                     ,B.DS_UTENTE
    //                     ,B.DS_STATO_ELAB
    //                     ,B.FL_SCUDO_FISC
    //                     ,B.FL_TRASFE
    //                     ,B.FL_TFR_STRC
    //                     ,B.DT_PRESC
    //                     ,B.COD_CONV_AGG
    //                     ,B.SUBCAT_PROD
    //                     ,B.FL_QUEST_ADEGZ_ASS
    //                     ,B.COD_TPA
    //                     ,B.ID_ACC_COMM
    //                     ,B.FL_POLI_CPI_PR
    //                     ,B.FL_POLI_BUNDLING
    //                     ,B.IND_POLI_PRIN_COLL
    //                     ,B.FL_VND_BUNDLE
    //                     ,B.IB_BS
    //                     ,B.FL_POLI_IFP
    //                     ,C.ID_STAT_OGG_BUS
    //                     ,C.ID_OGG
    //                     ,C.TP_OGG
    //                     ,C.ID_MOVI_CRZ
    //                     ,C.ID_MOVI_CHIU
    //                     ,C.DT_INI_EFF
    //                     ,C.DT_END_EFF
    //                     ,C.COD_COMP_ANIA
    //                     ,C.TP_STAT_BUS
    //                     ,C.TP_CAUS
    //                     ,C.DS_RIGA
    //                     ,C.DS_OPER_SQL
    //                     ,C.DS_VER
    //                     ,C.DS_TS_INI_CPTZ
    //                     ,C.DS_TS_END_CPTZ
    //                     ,C.DS_UTENTE
    //                     ,C.DS_STATO_ELAB
    //                  FROM ADES         A,
    //                       POLI         B,
    //                       STAT_OGG_BUS C
    //                  WHERE B.IB_OGG   BETWEEN   :WLB-IB-POLI-FIRST AND
    //                                             :WLB-IB-POLI-LAST
    //                    AND A.ID_POLI         =   B.ID_POLI
    //                    AND A.ID_ADES         =   C.ID_OGG
    //                    AND C.TP_OGG          =  'AD'
    //                    AND C.TP_STAT_BUS     =  'VI'
    //           *        OR
    //           *            (C.TP_STAT_BUS    =  'ST'  AND
    //           *            EXISTS (SELECT
    //           *                  D.ID_MOVI_FINRIO
    //           *
    //           *               FROM  MOVI_FINRIO D
    //           *               WHERE A.ID_ADES = D.ID_ADES
    //           *                 AND D.STAT_MOVI IN ('AL','IE')
    //           *                 AND D.TP_MOVI_FINRIO = 'DI'
    //           *                 AND D.COD_COMP_ANIA   =
    //           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
    //           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
    //           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
    //           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
    //                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
    //                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
    //                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
    //                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
    //                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
    //                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
    //                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
    //                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
    //                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
    //                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
    //                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
    //           *      AND A.DS_STATO_ELAB IN (
    //           *                              :IABV0002-STATE-01,
    //           *                              :IABV0002-STATE-02,
    //           *                              :IABV0002-STATE-03,
    //           *                              :IABV0002-STATE-04,
    //           *                              :IABV0002-STATE-05,
    //           *                              :IABV0002-STATE-06,
    //           *                              :IABV0002-STATE-07,
    //           *                              :IABV0002-STATE-08,
    //           *                              :IABV0002-STATE-09,
    //           *                              :IABV0002-STATE-10
    //           *                                  )
    //           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
    //                    ORDER BY A.ID_POLI, A.ID_ADES
    //                END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-SC03<br>
	 * <pre>*****************************************************************
	 *       AND A.DS_STATO_ELAB IN (
	 *                               :IABV0002-STATE-01,
	 *                               :IABV0002-STATE-02,
	 *                               :IABV0002-STATE-03,
	 *                               :IABV0002-STATE-04,
	 *                               :IABV0002-STATE-05,
	 *                               :IABV0002-STATE-06,
	 *                               :IABV0002-STATE-07,
	 *                               :IABV0002-STATE-08,
	 *                               :IABV0002-STATE-09,
	 *                               :IABV0002-STATE-10
	 *                                   )
	 *         AND NOT A.DS_VER     = :IABV0009-VERSIONING</pre>*/
    private void a310SelectSc03() {
        // COB_CODE:      EXEC SQL
        //                  SELECT
        //                      A.ID_ADES
        //                     ,A.ID_POLI
        //                     ,A.ID_MOVI_CRZ
        //                     ,A.ID_MOVI_CHIU
        //                     ,A.DT_INI_EFF
        //                     ,A.DT_END_EFF
        //                     ,A.IB_PREV
        //                     ,A.IB_OGG
        //                     ,A.COD_COMP_ANIA
        //                     ,A.DT_DECOR
        //                     ,A.DT_SCAD
        //                     ,A.ETA_A_SCAD
        //                     ,A.DUR_AA
        //                     ,A.DUR_MM
        //                     ,A.DUR_GG
        //                     ,A.TP_RGM_FISC
        //                     ,A.TP_RIAT
        //                     ,A.TP_MOD_PAG_TIT
        //                     ,A.TP_IAS
        //                     ,A.DT_VARZ_TP_IAS
        //                     ,A.PRE_NET_IND
        //                     ,A.PRE_LRD_IND
        //                     ,A.RAT_LRD_IND
        //                     ,A.PRSTZ_INI_IND
        //                     ,A.FL_COINC_ASSTO
        //                     ,A.IB_DFLT
        //                     ,A.MOD_CALC
        //                     ,A.TP_FNT_CNBTVA
        //                     ,A.IMP_AZ
        //                     ,A.IMP_ADER
        //                     ,A.IMP_TFR
        //                     ,A.IMP_VOLO
        //                     ,A.PC_AZ
        //                     ,A.PC_ADER
        //                     ,A.PC_TFR
        //                     ,A.PC_VOLO
        //                     ,A.DT_NOVA_RGM_FISC
        //                     ,A.FL_ATTIV
        //                     ,A.IMP_REC_RIT_VIS
        //                     ,A.IMP_REC_RIT_ACC
        //                     ,A.FL_VARZ_STAT_TBGC
        //                     ,A.FL_PROVZA_MIGRAZ
        //                     ,A.IMPB_VIS_DA_REC
        //                     ,A.DT_DECOR_PREST_BAN
        //                     ,A.DT_EFF_VARZ_STAT_T
        //                     ,A.DS_RIGA
        //                     ,A.DS_OPER_SQL
        //                     ,A.DS_VER
        //                     ,A.DS_TS_INI_CPTZ
        //                     ,A.DS_TS_END_CPTZ
        //                     ,A.DS_UTENTE
        //                     ,A.DS_STATO_ELAB
        //                     ,A.CUM_CNBT_CAP
        //                     ,A.IMP_GAR_CNBT
        //                     ,A.DT_ULT_CONS_CNBT
        //                     ,A.IDEN_ISC_FND
        //                     ,A.NUM_RAT_PIAN
        //                     ,A.DT_PRESC
        //                     ,A.CONCS_PREST
        //                     ,B.ID_POLI
        //                     ,B.ID_MOVI_CRZ
        //                     ,B.ID_MOVI_CHIU
        //                     ,B.IB_OGG
        //                     ,B.IB_PROP
        //                     ,B.DT_PROP
        //                     ,B.DT_INI_EFF
        //                     ,B.DT_END_EFF
        //                     ,B.COD_COMP_ANIA
        //                     ,B.DT_DECOR
        //                     ,B.DT_EMIS
        //                     ,B.TP_POLI
        //                     ,B.DUR_AA
        //                     ,B.DUR_MM
        //                     ,B.DT_SCAD
        //                     ,B.COD_PROD
        //                     ,B.DT_INI_VLDT_PROD
        //                     ,B.COD_CONV
        //                     ,B.COD_RAMO
        //                     ,B.DT_INI_VLDT_CONV
        //                     ,B.DT_APPLZ_CONV
        //                     ,B.TP_FRM_ASSVA
        //                     ,B.TP_RGM_FISC
        //                     ,B.FL_ESTAS
        //                     ,B.FL_RSH_COMUN
        //                     ,B.FL_RSH_COMUN_COND
        //                     ,B.TP_LIV_GENZ_TIT
        //                     ,B.FL_COP_FINANZ
        //                     ,B.TP_APPLZ_DIR
        //                     ,B.SPE_MED
        //                     ,B.DIR_EMIS
        //                     ,B.DIR_1O_VERS
        //                     ,B.DIR_VERS_AGG
        //                     ,B.COD_DVS
        //                     ,B.FL_FNT_AZ
        //                     ,B.FL_FNT_ADER
        //                     ,B.FL_FNT_TFR
        //                     ,B.FL_FNT_VOLO
        //                     ,B.TP_OPZ_A_SCAD
        //                     ,B.AA_DIFF_PROR_DFLT
        //                     ,B.FL_VER_PROD
        //                     ,B.DUR_GG
        //                     ,B.DIR_QUIET
        //                     ,B.TP_PTF_ESTNO
        //                     ,B.FL_CUM_PRE_CNTR
        //                     ,B.FL_AMMB_MOVI
        //                     ,B.CONV_GECO
        //                     ,B.DS_RIGA
        //                     ,B.DS_OPER_SQL
        //                     ,B.DS_VER
        //                     ,B.DS_TS_INI_CPTZ
        //                     ,B.DS_TS_END_CPTZ
        //                     ,B.DS_UTENTE
        //                     ,B.DS_STATO_ELAB
        //                     ,B.FL_SCUDO_FISC
        //                     ,B.FL_TRASFE
        //                     ,B.FL_TFR_STRC
        //                     ,B.DT_PRESC
        //                     ,B.COD_CONV_AGG
        //                     ,B.SUBCAT_PROD
        //                     ,B.FL_QUEST_ADEGZ_ASS
        //                     ,B.COD_TPA
        //                     ,B.ID_ACC_COMM
        //                     ,B.FL_POLI_CPI_PR
        //                     ,B.FL_POLI_BUNDLING
        //                     ,B.IND_POLI_PRIN_COLL
        //                     ,B.FL_VND_BUNDLE
        //                     ,B.IB_BS
        //                     ,B.FL_POLI_IFP
        //                     ,C.ID_STAT_OGG_BUS
        //                     ,C.ID_OGG
        //                     ,C.TP_OGG
        //                     ,C.ID_MOVI_CRZ
        //                     ,C.ID_MOVI_CHIU
        //                     ,C.DT_INI_EFF
        //                     ,C.DT_END_EFF
        //                     ,C.COD_COMP_ANIA
        //                     ,C.TP_STAT_BUS
        //                     ,C.TP_CAUS
        //                     ,C.DS_RIGA
        //                     ,C.DS_OPER_SQL
        //                     ,C.DS_VER
        //                     ,C.DS_TS_INI_CPTZ
        //                     ,C.DS_TS_END_CPTZ
        //                     ,C.DS_UTENTE
        //                     ,C.DS_STATO_ELAB
        //                  INTO
        //                     :ADE-ID-ADES
        //                    ,:ADE-ID-POLI
        //                    ,:ADE-ID-MOVI-CRZ
        //                    ,:ADE-ID-MOVI-CHIU
        //                     :IND-ADE-ID-MOVI-CHIU
        //                    ,:ADE-DT-INI-EFF-DB
        //                    ,:ADE-DT-END-EFF-DB
        //                    ,:ADE-IB-PREV
        //                     :IND-ADE-IB-PREV
        //                    ,:ADE-IB-OGG
        //                     :IND-ADE-IB-OGG
        //                    ,:ADE-COD-COMP-ANIA
        //                    ,:ADE-DT-DECOR-DB
        //                     :IND-ADE-DT-DECOR
        //                    ,:ADE-DT-SCAD-DB
        //                     :IND-ADE-DT-SCAD
        //                    ,:ADE-ETA-A-SCAD
        //                     :IND-ADE-ETA-A-SCAD
        //                    ,:ADE-DUR-AA
        //                     :IND-ADE-DUR-AA
        //                    ,:ADE-DUR-MM
        //                     :IND-ADE-DUR-MM
        //                    ,:ADE-DUR-GG
        //                     :IND-ADE-DUR-GG
        //                    ,:ADE-TP-RGM-FISC
        //                    ,:ADE-TP-RIAT
        //                     :IND-ADE-TP-RIAT
        //                    ,:ADE-TP-MOD-PAG-TIT
        //                    ,:ADE-TP-IAS
        //                     :IND-ADE-TP-IAS
        //                    ,:ADE-DT-VARZ-TP-IAS-DB
        //                     :IND-ADE-DT-VARZ-TP-IAS
        //                    ,:ADE-PRE-NET-IND
        //                     :IND-ADE-PRE-NET-IND
        //                    ,:ADE-PRE-LRD-IND
        //                     :IND-ADE-PRE-LRD-IND
        //                    ,:ADE-RAT-LRD-IND
        //                     :IND-ADE-RAT-LRD-IND
        //                    ,:ADE-PRSTZ-INI-IND
        //                     :IND-ADE-PRSTZ-INI-IND
        //                    ,:ADE-FL-COINC-ASSTO
        //                     :IND-ADE-FL-COINC-ASSTO
        //                    ,:ADE-IB-DFLT
        //                     :IND-ADE-IB-DFLT
        //                    ,:ADE-MOD-CALC
        //                     :IND-ADE-MOD-CALC
        //                    ,:ADE-TP-FNT-CNBTVA
        //                     :IND-ADE-TP-FNT-CNBTVA
        //                    ,:ADE-IMP-AZ
        //                     :IND-ADE-IMP-AZ
        //                    ,:ADE-IMP-ADER
        //                     :IND-ADE-IMP-ADER
        //                    ,:ADE-IMP-TFR
        //                     :IND-ADE-IMP-TFR
        //                    ,:ADE-IMP-VOLO
        //                     :IND-ADE-IMP-VOLO
        //                    ,:ADE-PC-AZ
        //                     :IND-ADE-PC-AZ
        //                    ,:ADE-PC-ADER
        //                     :IND-ADE-PC-ADER
        //                    ,:ADE-PC-TFR
        //                     :IND-ADE-PC-TFR
        //                    ,:ADE-PC-VOLO
        //                     :IND-ADE-PC-VOLO
        //                    ,:ADE-DT-NOVA-RGM-FISC-DB
        //                     :IND-ADE-DT-NOVA-RGM-FISC
        //                    ,:ADE-FL-ATTIV
        //                     :IND-ADE-FL-ATTIV
        //                    ,:ADE-IMP-REC-RIT-VIS
        //                     :IND-ADE-IMP-REC-RIT-VIS
        //                    ,:ADE-IMP-REC-RIT-ACC
        //                     :IND-ADE-IMP-REC-RIT-ACC
        //                    ,:ADE-FL-VARZ-STAT-TBGC
        //                     :IND-ADE-FL-VARZ-STAT-TBGC
        //                    ,:ADE-FL-PROVZA-MIGRAZ
        //                     :IND-ADE-FL-PROVZA-MIGRAZ
        //                    ,:ADE-IMPB-VIS-DA-REC
        //                     :IND-ADE-IMPB-VIS-DA-REC
        //                    ,:ADE-DT-DECOR-PREST-BAN-DB
        //                     :IND-ADE-DT-DECOR-PREST-BAN
        //                    ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                     :IND-ADE-DT-EFF-VARZ-STAT-T
        //                    ,:ADE-DS-RIGA
        //                    ,:ADE-DS-OPER-SQL
        //                    ,:ADE-DS-VER
        //                    ,:ADE-DS-TS-INI-CPTZ
        //                    ,:ADE-DS-TS-END-CPTZ
        //                    ,:ADE-DS-UTENTE
        //                    ,:ADE-DS-STATO-ELAB
        //                    ,:ADE-CUM-CNBT-CAP
        //                     :IND-ADE-CUM-CNBT-CAP
        //                    ,:ADE-IMP-GAR-CNBT
        //                     :IND-ADE-IMP-GAR-CNBT
        //                    ,:ADE-DT-ULT-CONS-CNBT-DB
        //                     :IND-ADE-DT-ULT-CONS-CNBT
        //                    ,:ADE-IDEN-ISC-FND
        //                     :IND-ADE-IDEN-ISC-FND
        //                    ,:ADE-NUM-RAT-PIAN
        //                     :IND-ADE-NUM-RAT-PIAN
        //                    ,:ADE-DT-PRESC-DB
        //                     :IND-ADE-DT-PRESC
        //                    ,:ADE-CONCS-PREST
        //                     :IND-ADE-CONCS-PREST
        //                    ,:POL-ID-POLI
        //                    ,:POL-ID-MOVI-CRZ
        //                    ,:POL-ID-MOVI-CHIU
        //                     :IND-POL-ID-MOVI-CHIU
        //                    ,:POL-IB-OGG
        //                     :IND-POL-IB-OGG
        //                    ,:POL-IB-PROP
        //                    ,:POL-DT-PROP-DB
        //                     :IND-POL-DT-PROP
        //                    ,:POL-DT-INI-EFF-DB
        //                    ,:POL-DT-END-EFF-DB
        //                    ,:POL-COD-COMP-ANIA
        //                    ,:POL-DT-DECOR-DB
        //                    ,:POL-DT-EMIS-DB
        //                    ,:POL-TP-POLI
        //                    ,:POL-DUR-AA
        //                     :IND-POL-DUR-AA
        //                    ,:POL-DUR-MM
        //                     :IND-POL-DUR-MM
        //                    ,:POL-DT-SCAD-DB
        //                     :IND-POL-DT-SCAD
        //                    ,:POL-COD-PROD
        //                    ,:POL-DT-INI-VLDT-PROD-DB
        //                    ,:POL-COD-CONV
        //                     :IND-POL-COD-CONV
        //                    ,:POL-COD-RAMO
        //                     :IND-POL-COD-RAMO
        //                    ,:POL-DT-INI-VLDT-CONV-DB
        //                     :IND-POL-DT-INI-VLDT-CONV
        //                    ,:POL-DT-APPLZ-CONV-DB
        //                     :IND-POL-DT-APPLZ-CONV
        //                    ,:POL-TP-FRM-ASSVA
        //                    ,:POL-TP-RGM-FISC
        //                     :IND-POL-TP-RGM-FISC
        //                    ,:POL-FL-ESTAS
        //                     :IND-POL-FL-ESTAS
        //                    ,:POL-FL-RSH-COMUN
        //                     :IND-POL-FL-RSH-COMUN
        //                    ,:POL-FL-RSH-COMUN-COND
        //                     :IND-POL-FL-RSH-COMUN-COND
        //                    ,:POL-TP-LIV-GENZ-TIT
        //                    ,:POL-FL-COP-FINANZ
        //                     :IND-POL-FL-COP-FINANZ
        //                    ,:POL-TP-APPLZ-DIR
        //                     :IND-POL-TP-APPLZ-DIR
        //                    ,:POL-SPE-MED
        //                     :IND-POL-SPE-MED
        //                    ,:POL-DIR-EMIS
        //                     :IND-POL-DIR-EMIS
        //                    ,:POL-DIR-1O-VERS
        //                     :IND-POL-DIR-1O-VERS
        //                    ,:POL-DIR-VERS-AGG
        //                     :IND-POL-DIR-VERS-AGG
        //                    ,:POL-COD-DVS
        //                     :IND-POL-COD-DVS
        //                    ,:POL-FL-FNT-AZ
        //                     :IND-POL-FL-FNT-AZ
        //                    ,:POL-FL-FNT-ADER
        //                     :IND-POL-FL-FNT-ADER
        //                    ,:POL-FL-FNT-TFR
        //                     :IND-POL-FL-FNT-TFR
        //                    ,:POL-FL-FNT-VOLO
        //                     :IND-POL-FL-FNT-VOLO
        //                    ,:POL-TP-OPZ-A-SCAD
        //                     :IND-POL-TP-OPZ-A-SCAD
        //                    ,:POL-AA-DIFF-PROR-DFLT
        //                     :IND-POL-AA-DIFF-PROR-DFLT
        //                    ,:POL-FL-VER-PROD
        //                     :IND-POL-FL-VER-PROD
        //                    ,:POL-DUR-GG
        //                     :IND-POL-DUR-GG
        //                    ,:POL-DIR-QUIET
        //                     :IND-POL-DIR-QUIET
        //                    ,:POL-TP-PTF-ESTNO
        //                     :IND-POL-TP-PTF-ESTNO
        //                    ,:POL-FL-CUM-PRE-CNTR
        //                     :IND-POL-FL-CUM-PRE-CNTR
        //                    ,:POL-FL-AMMB-MOVI
        //                     :IND-POL-FL-AMMB-MOVI
        //                    ,:POL-CONV-GECO
        //                     :IND-POL-CONV-GECO
        //                    ,:POL-DS-RIGA
        //                    ,:POL-DS-OPER-SQL
        //                    ,:POL-DS-VER
        //                    ,:POL-DS-TS-INI-CPTZ
        //                    ,:POL-DS-TS-END-CPTZ
        //                    ,:POL-DS-UTENTE
        //                    ,:POL-DS-STATO-ELAB
        //                    ,:POL-FL-SCUDO-FISC
        //                     :IND-POL-FL-SCUDO-FISC
        //                    ,:POL-FL-TRASFE
        //                     :IND-POL-FL-TRASFE
        //                    ,:POL-FL-TFR-STRC
        //                     :IND-POL-FL-TFR-STRC
        //                    ,:POL-DT-PRESC-DB
        //                     :IND-POL-DT-PRESC
        //                    ,:POL-COD-CONV-AGG
        //                     :IND-POL-COD-CONV-AGG
        //                    ,:POL-SUBCAT-PROD
        //                     :IND-POL-SUBCAT-PROD
        //                    ,:POL-FL-QUEST-ADEGZ-ASS
        //                     :IND-POL-FL-QUEST-ADEGZ-ASS
        //                    ,:POL-COD-TPA
        //                     :IND-POL-COD-TPA
        //                    ,:POL-ID-ACC-COMM
        //                     :IND-POL-ID-ACC-COMM
        //                    ,:POL-FL-POLI-CPI-PR
        //                     :IND-POL-FL-POLI-CPI-PR
        //                    ,:POL-FL-POLI-BUNDLING
        //                     :IND-POL-FL-POLI-BUNDLING
        //                    ,:POL-IND-POLI-PRIN-COLL
        //                     :IND-POL-IND-POLI-PRIN-COLL
        //                    ,:POL-FL-VND-BUNDLE
        //                     :IND-POL-FL-VND-BUNDLE
        //                    ,:POL-IB-BS
        //                     :IND-POL-IB-BS
        //                    ,:POL-FL-POLI-IFP
        //                     :IND-POL-FL-POLI-IFP
        //                    ,:STB-ID-STAT-OGG-BUS
        //                    ,:STB-ID-OGG
        //                    ,:STB-TP-OGG
        //                    ,:STB-ID-MOVI-CRZ
        //                    ,:STB-ID-MOVI-CHIU
        //                     :IND-STB-ID-MOVI-CHIU
        //                    ,:STB-DT-INI-EFF-DB
        //                    ,:STB-DT-END-EFF-DB
        //                    ,:STB-COD-COMP-ANIA
        //                    ,:STB-TP-STAT-BUS
        //                    ,:STB-TP-CAUS
        //                    ,:STB-DS-RIGA
        //                    ,:STB-DS-OPER-SQL
        //                    ,:STB-DS-VER
        //                    ,:STB-DS-TS-INI-CPTZ
        //                    ,:STB-DS-TS-END-CPTZ
        //                    ,:STB-DS-UTENTE
        //                    ,:STB-DS-STATO-ELAB
        //                  FROM ADES         A,
        //                       POLI         B,
        //                       STAT_OGG_BUS C
        //                  WHERE B.IB_OGG   BETWEEN   :WLB-IB-POLI-FIRST AND
        //                                             :WLB-IB-POLI-LAST
        //                    AND A.ID_POLI         =   B.ID_POLI
        //                    AND A.ID_ADES         =   C.ID_OGG
        //                    AND C.TP_OGG          =  'AD'
        //                    AND C.TP_STAT_BUS     =  'VI'
        //           *        OR
        //           *            (C.TP_STAT_BUS    =  'ST'  AND
        //           *            EXISTS (SELECT
        //           *                  D.ID_MOVI_FINRIO
        //           *
        //           *               FROM  MOVI_FINRIO D
        //           *               WHERE A.ID_ADES = D.ID_ADES
        //           *                 AND D.STAT_MOVI IN ('AL','IE')
        //           *                 AND D.TP_MOVI_FINRIO = 'DI'
        //           *                 AND D.COD_COMP_ANIA   =
        //           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
        //           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
        //           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
        //                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
        //                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
        //                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
        //                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
        //                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
        //                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //           *      AND A.DS_STATO_ELAB IN (
        //           *                              :IABV0002-STATE-01,
        //           *                              :IABV0002-STATE-02,
        //           *                              :IABV0002-STATE-03,
        //           *                              :IABV0002-STATE-04,
        //           *                              :IABV0002-STATE-05,
        //           *                              :IABV0002-STATE-06,
        //           *                              :IABV0002-STATE-07,
        //           *                              :IABV0002-STATE-08,
        //           *                              :IABV0002-STATE-09,
        //           *                              :IABV0002-STATE-10
        //           *                                  )
        //           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
        //                    ORDER BY A.ID_POLI, A.ID_ADES
        //                   FETCH FIRST ROW ONLY
        //                END-EXEC.
        adesPoliDao.selectRec3(this.getAdesPoliLdbm02501());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A320-UPDATE-SC03<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320UpdateSc03() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK-SC03         THRU A330-SC03-EX
        //              WHEN IDSV0003-WHERE-CONDITION-03
        //                   PERFORM A340-UPDATE-WHERE-COND-SC03 THRU A340-SC03-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                       THRU A345-SC03-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
            // COB_CODE: PERFORM A330-UPDATE-PK-SC03         THRU A330-SC03-EX
            a330UpdatePkSc03();
        }
        else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition03()) {
            // COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC03 THRU A340-SC03-EX
            a340UpdateWhereCondSc03();
        }
        else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
            // COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC03
            //                                               THRU A345-SC03-EX
            a345UpdateFirstActionSc03();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidLevelOper();
        }
    }

    /**Original name: A330-UPDATE-PK-SC03<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePkSc03() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=4105, because the code is unreachable.
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
        z960LengthVchar();
        //           ,DS_VER                 = :IABV0009-VERSIONING
        // COB_CODE:      EXEC SQL
        //                     UPDATE ADES
        //                     SET
        //                        DS_OPER_SQL            = :ADE-DS-OPER-SQL
        //           *           ,DS_VER                 = :IABV0009-VERSIONING
        //                       ,DS_UTENTE              = :ADE-DS-UTENTE
        //                       ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
        //                     WHERE             DS_RIGA = :ADE-DS-RIGA
        //                END-EXEC.
        adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A340-UPDATE-WHERE-COND-SC03<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateWhereCondSc03() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A345-UPDATE-FIRST-ACTION-SC03<br>
	 * <pre>*****************************************************************</pre>*/
    private void a345UpdateFirstActionSc03() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A360-OPEN-CURSOR-SC03<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursorSc03() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-SC03 THRU A305-SC03-EX.
        a305DeclareCursorSc03();
        // COB_CODE: EXEC SQL
        //                OPEN CUR-SC03
        //           END-EXEC.
        adesPoliDao.openCurSc03(this.getAdesPoliLdbm02501());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-SC03<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursorSc03() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-SC03
        //           END-EXEC.
        adesPoliDao.closeCurSc03();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST-SC03<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirstSc03() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-SC03    THRU A360-SC03-EX.
        a360OpenCursorSc03();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT-SC03  THRU A390-SC03-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC03  THRU A390-SC03-EX
            a390FetchNextSc03();
        }
    }

    /**Original name: A390-FETCH-NEXT-SC03<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNextSc03() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-SC03
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //               ,:POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //               ,:STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        adesPoliDao.fetchCurSc03(this.getAdesPoliLdbm0250());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC03 THRU A370-SC03-EX
            a370CloseCursorSc03();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: SC04-SELECTION-CURSOR-04<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
    private void sc04SelectionCursor04() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC04           THRU A310-SC04-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-SC04      THRU A360-SC04-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-SC04     THRU A370-SC04-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-SC04      THRU A380-SC04-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-SC04       THRU A390-SC04-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE-SC04           THRU A320-SC04-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER          TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC04           THRU A310-SC04-EX
            a310SelectSc04();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-SC04      THRU A360-SC04-EX
            a360OpenCursorSc04();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC04     THRU A370-SC04-EX
            a370CloseCursorSc04();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-SC04      THRU A380-SC04-EX
            a380FetchFirstSc04();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC04       THRU A390-SC04-EX
            a390FetchNextSc04();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE-SC04           THRU A320-SC04-EX
            a320UpdateSc04();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR-SC04<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursorSc04() {
    // COB_CODE:      EXEC SQL
    //                     DECLARE CUR-SC04 CURSOR WITH HOLD FOR
    //                  SELECT
    //                      A.ID_ADES
    //                     ,A.ID_POLI
    //                     ,A.ID_MOVI_CRZ
    //                     ,A.ID_MOVI_CHIU
    //                     ,A.DT_INI_EFF
    //                     ,A.DT_END_EFF
    //                     ,A.IB_PREV
    //                     ,A.IB_OGG
    //                     ,A.COD_COMP_ANIA
    //                     ,A.DT_DECOR
    //                     ,A.DT_SCAD
    //                     ,A.ETA_A_SCAD
    //                     ,A.DUR_AA
    //                     ,A.DUR_MM
    //                     ,A.DUR_GG
    //                     ,A.TP_RGM_FISC
    //                     ,A.TP_RIAT
    //                     ,A.TP_MOD_PAG_TIT
    //                     ,A.TP_IAS
    //                     ,A.DT_VARZ_TP_IAS
    //                     ,A.PRE_NET_IND
    //                     ,A.PRE_LRD_IND
    //                     ,A.RAT_LRD_IND
    //                     ,A.PRSTZ_INI_IND
    //                     ,A.FL_COINC_ASSTO
    //                     ,A.IB_DFLT
    //                     ,A.MOD_CALC
    //                     ,A.TP_FNT_CNBTVA
    //                     ,A.IMP_AZ
    //                     ,A.IMP_ADER
    //                     ,A.IMP_TFR
    //                     ,A.IMP_VOLO
    //                     ,A.PC_AZ
    //                     ,A.PC_ADER
    //                     ,A.PC_TFR
    //                     ,A.PC_VOLO
    //                     ,A.DT_NOVA_RGM_FISC
    //                     ,A.FL_ATTIV
    //                     ,A.IMP_REC_RIT_VIS
    //                     ,A.IMP_REC_RIT_ACC
    //                     ,A.FL_VARZ_STAT_TBGC
    //                     ,A.FL_PROVZA_MIGRAZ
    //                     ,A.IMPB_VIS_DA_REC
    //                     ,A.DT_DECOR_PREST_BAN
    //                     ,A.DT_EFF_VARZ_STAT_T
    //                     ,A.DS_RIGA
    //                     ,A.DS_OPER_SQL
    //                     ,A.DS_VER
    //                     ,A.DS_TS_INI_CPTZ
    //                     ,A.DS_TS_END_CPTZ
    //                     ,A.DS_UTENTE
    //                     ,A.DS_STATO_ELAB
    //                     ,A.CUM_CNBT_CAP
    //                     ,A.IMP_GAR_CNBT
    //                     ,A.DT_ULT_CONS_CNBT
    //                     ,A.IDEN_ISC_FND
    //                     ,A.NUM_RAT_PIAN
    //                     ,A.DT_PRESC
    //                     ,A.CONCS_PREST
    //                     ,B.ID_POLI
    //                     ,B.ID_MOVI_CRZ
    //                     ,B.ID_MOVI_CHIU
    //                     ,B.IB_OGG
    //                     ,B.IB_PROP
    //                     ,B.DT_PROP
    //                     ,B.DT_INI_EFF
    //                     ,B.DT_END_EFF
    //                     ,B.COD_COMP_ANIA
    //                     ,B.DT_DECOR
    //                     ,B.DT_EMIS
    //                     ,B.TP_POLI
    //                     ,B.DUR_AA
    //                     ,B.DUR_MM
    //                     ,B.DT_SCAD
    //                     ,B.COD_PROD
    //                     ,B.DT_INI_VLDT_PROD
    //                     ,B.COD_CONV
    //                     ,B.COD_RAMO
    //                     ,B.DT_INI_VLDT_CONV
    //                     ,B.DT_APPLZ_CONV
    //                     ,B.TP_FRM_ASSVA
    //                     ,B.TP_RGM_FISC
    //                     ,B.FL_ESTAS
    //                     ,B.FL_RSH_COMUN
    //                     ,B.FL_RSH_COMUN_COND
    //                     ,B.TP_LIV_GENZ_TIT
    //                     ,B.FL_COP_FINANZ
    //                     ,B.TP_APPLZ_DIR
    //                     ,B.SPE_MED
    //                     ,B.DIR_EMIS
    //                     ,B.DIR_1O_VERS
    //                     ,B.DIR_VERS_AGG
    //                     ,B.COD_DVS
    //                     ,B.FL_FNT_AZ
    //                     ,B.FL_FNT_ADER
    //                     ,B.FL_FNT_TFR
    //                     ,B.FL_FNT_VOLO
    //                     ,B.TP_OPZ_A_SCAD
    //                     ,B.AA_DIFF_PROR_DFLT
    //                     ,B.FL_VER_PROD
    //                     ,B.DUR_GG
    //                     ,B.DIR_QUIET
    //                     ,B.TP_PTF_ESTNO
    //                     ,B.FL_CUM_PRE_CNTR
    //                     ,B.FL_AMMB_MOVI
    //                     ,B.CONV_GECO
    //                     ,B.DS_RIGA
    //                     ,B.DS_OPER_SQL
    //                     ,B.DS_VER
    //                     ,B.DS_TS_INI_CPTZ
    //                     ,B.DS_TS_END_CPTZ
    //                     ,B.DS_UTENTE
    //                     ,B.DS_STATO_ELAB
    //                     ,B.FL_SCUDO_FISC
    //                     ,B.FL_TRASFE
    //                     ,B.FL_TFR_STRC
    //                     ,B.DT_PRESC
    //                     ,B.COD_CONV_AGG
    //                     ,B.SUBCAT_PROD
    //                     ,B.FL_QUEST_ADEGZ_ASS
    //                     ,B.COD_TPA
    //                     ,B.ID_ACC_COMM
    //                     ,B.FL_POLI_CPI_PR
    //                     ,B.FL_POLI_BUNDLING
    //                     ,B.IND_POLI_PRIN_COLL
    //                     ,B.FL_VND_BUNDLE
    //                     ,B.IB_BS
    //                     ,B.FL_POLI_IFP
    //                     ,C.ID_STAT_OGG_BUS
    //                     ,C.ID_OGG
    //                     ,C.TP_OGG
    //                     ,C.ID_MOVI_CRZ
    //                     ,C.ID_MOVI_CHIU
    //                     ,C.DT_INI_EFF
    //                     ,C.DT_END_EFF
    //                     ,C.COD_COMP_ANIA
    //                     ,C.TP_STAT_BUS
    //                     ,C.TP_CAUS
    //                     ,C.DS_RIGA
    //                     ,C.DS_OPER_SQL
    //                     ,C.DS_VER
    //                     ,C.DS_TS_INI_CPTZ
    //                     ,C.DS_TS_END_CPTZ
    //                     ,C.DS_UTENTE
    //                     ,C.DS_STATO_ELAB
    //                  FROM ADES         A,
    //                       POLI         B,
    //                       STAT_OGG_BUS C
    //                  WHERE A.IB_OGG    BETWEEN  :WLB-IB-ADE-FIRST AND
    //                                             :WLB-IB-ADE-LAST
    //                    AND B.IB_OGG          =  :WLB-IB-POLI-FIRST
    //                    AND A.ID_POLI         =   B.ID_POLI
    //                    AND A.ID_ADES         =   C.ID_OGG
    //                    AND C.TP_OGG          =  'AD'
    //                    AND C.TP_STAT_BUS     =  'VI'
    //           *        OR
    //           *            (C.TP_STAT_BUS    =  'ST'  AND
    //           *            EXISTS (SELECT
    //           *                  D.ID_MOVI_FINRIO
    //           *
    //           *               FROM  MOVI_FINRIO D
    //           *               WHERE A.ID_ADES = D.ID_ADES
    //           *                 AND D.STAT_MOVI IN ('AL','IE')
    //           *                 AND D.TP_MOVI_FINRIO = 'DI'
    //           *                 AND D.COD_COMP_ANIA   =
    //           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
    //           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
    //           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
    //           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
    //                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
    //                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
    //                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
    //                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
    //                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
    //                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
    //                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
    //                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
    //                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
    //                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
    //                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
    //                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
    //           *      AND A.DS_STATO_ELAB IN (
    //           *                              :IABV0002-STATE-01,
    //           *                              :IABV0002-STATE-02,
    //           *                              :IABV0002-STATE-03,
    //           *                              :IABV0002-STATE-04,
    //           *                              :IABV0002-STATE-05,
    //           *                              :IABV0002-STATE-06,
    //           *                              :IABV0002-STATE-07,
    //           *                              :IABV0002-STATE-08,
    //           *                              :IABV0002-STATE-09,
    //           *                              :IABV0002-STATE-10
    //           *                                  )
    //           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
    //                    ORDER BY A.ID_POLI, A.ID_ADES
    //                END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-SC04<br>
	 * <pre>*****************************************************************
	 *       AND A.DS_STATO_ELAB IN (
	 *                               :IABV0002-STATE-01,
	 *                               :IABV0002-STATE-02,
	 *                               :IABV0002-STATE-03,
	 *                               :IABV0002-STATE-04,
	 *                               :IABV0002-STATE-05,
	 *                               :IABV0002-STATE-06,
	 *                               :IABV0002-STATE-07,
	 *                               :IABV0002-STATE-08,
	 *                               :IABV0002-STATE-09,
	 *                               :IABV0002-STATE-10
	 *                                   )
	 *         AND NOT A.DS_VER     = :IABV0009-VERSIONING</pre>*/
    private void a310SelectSc04() {
        // COB_CODE:      EXEC SQL
        //                  SELECT
        //                      A.ID_ADES
        //                     ,A.ID_POLI
        //                     ,A.ID_MOVI_CRZ
        //                     ,A.ID_MOVI_CHIU
        //                     ,A.DT_INI_EFF
        //                     ,A.DT_END_EFF
        //                     ,A.IB_PREV
        //                     ,A.IB_OGG
        //                     ,A.COD_COMP_ANIA
        //                     ,A.DT_DECOR
        //                     ,A.DT_SCAD
        //                     ,A.ETA_A_SCAD
        //                     ,A.DUR_AA
        //                     ,A.DUR_MM
        //                     ,A.DUR_GG
        //                     ,A.TP_RGM_FISC
        //                     ,A.TP_RIAT
        //                     ,A.TP_MOD_PAG_TIT
        //                     ,A.TP_IAS
        //                     ,A.DT_VARZ_TP_IAS
        //                     ,A.PRE_NET_IND
        //                     ,A.PRE_LRD_IND
        //                     ,A.RAT_LRD_IND
        //                     ,A.PRSTZ_INI_IND
        //                     ,A.FL_COINC_ASSTO
        //                     ,A.IB_DFLT
        //                     ,A.MOD_CALC
        //                     ,A.TP_FNT_CNBTVA
        //                     ,A.IMP_AZ
        //                     ,A.IMP_ADER
        //                     ,A.IMP_TFR
        //                     ,A.IMP_VOLO
        //                     ,A.PC_AZ
        //                     ,A.PC_ADER
        //                     ,A.PC_TFR
        //                     ,A.PC_VOLO
        //                     ,A.DT_NOVA_RGM_FISC
        //                     ,A.FL_ATTIV
        //                     ,A.IMP_REC_RIT_VIS
        //                     ,A.IMP_REC_RIT_ACC
        //                     ,A.FL_VARZ_STAT_TBGC
        //                     ,A.FL_PROVZA_MIGRAZ
        //                     ,A.IMPB_VIS_DA_REC
        //                     ,A.DT_DECOR_PREST_BAN
        //                     ,A.DT_EFF_VARZ_STAT_T
        //                     ,A.DS_RIGA
        //                     ,A.DS_OPER_SQL
        //                     ,A.DS_VER
        //                     ,A.DS_TS_INI_CPTZ
        //                     ,A.DS_TS_END_CPTZ
        //                     ,A.DS_UTENTE
        //                     ,A.DS_STATO_ELAB
        //                     ,A.CUM_CNBT_CAP
        //                     ,A.IMP_GAR_CNBT
        //                     ,A.DT_ULT_CONS_CNBT
        //                     ,A.IDEN_ISC_FND
        //                     ,A.NUM_RAT_PIAN
        //                     ,A.DT_PRESC
        //                     ,A.CONCS_PREST
        //                     ,B.ID_POLI
        //                     ,B.ID_MOVI_CRZ
        //                     ,B.ID_MOVI_CHIU
        //                     ,B.IB_OGG
        //                     ,B.IB_PROP
        //                     ,B.DT_PROP
        //                     ,B.DT_INI_EFF
        //                     ,B.DT_END_EFF
        //                     ,B.COD_COMP_ANIA
        //                     ,B.DT_DECOR
        //                     ,B.DT_EMIS
        //                     ,B.TP_POLI
        //                     ,B.DUR_AA
        //                     ,B.DUR_MM
        //                     ,B.DT_SCAD
        //                     ,B.COD_PROD
        //                     ,B.DT_INI_VLDT_PROD
        //                     ,B.COD_CONV
        //                     ,B.COD_RAMO
        //                     ,B.DT_INI_VLDT_CONV
        //                     ,B.DT_APPLZ_CONV
        //                     ,B.TP_FRM_ASSVA
        //                     ,B.TP_RGM_FISC
        //                     ,B.FL_ESTAS
        //                     ,B.FL_RSH_COMUN
        //                     ,B.FL_RSH_COMUN_COND
        //                     ,B.TP_LIV_GENZ_TIT
        //                     ,B.FL_COP_FINANZ
        //                     ,B.TP_APPLZ_DIR
        //                     ,B.SPE_MED
        //                     ,B.DIR_EMIS
        //                     ,B.DIR_1O_VERS
        //                     ,B.DIR_VERS_AGG
        //                     ,B.COD_DVS
        //                     ,B.FL_FNT_AZ
        //                     ,B.FL_FNT_ADER
        //                     ,B.FL_FNT_TFR
        //                     ,B.FL_FNT_VOLO
        //                     ,B.TP_OPZ_A_SCAD
        //                     ,B.AA_DIFF_PROR_DFLT
        //                     ,B.FL_VER_PROD
        //                     ,B.DUR_GG
        //                     ,B.DIR_QUIET
        //                     ,B.TP_PTF_ESTNO
        //                     ,B.FL_CUM_PRE_CNTR
        //                     ,B.FL_AMMB_MOVI
        //                     ,B.CONV_GECO
        //                     ,B.DS_RIGA
        //                     ,B.DS_OPER_SQL
        //                     ,B.DS_VER
        //                     ,B.DS_TS_INI_CPTZ
        //                     ,B.DS_TS_END_CPTZ
        //                     ,B.DS_UTENTE
        //                     ,B.DS_STATO_ELAB
        //                     ,B.FL_SCUDO_FISC
        //                     ,B.FL_TRASFE
        //                     ,B.FL_TFR_STRC
        //                     ,B.DT_PRESC
        //                     ,B.COD_CONV_AGG
        //                     ,B.SUBCAT_PROD
        //                     ,B.FL_QUEST_ADEGZ_ASS
        //                     ,B.COD_TPA
        //                     ,B.ID_ACC_COMM
        //                     ,B.FL_POLI_CPI_PR
        //                     ,B.FL_POLI_BUNDLING
        //                     ,B.IND_POLI_PRIN_COLL
        //                     ,B.FL_VND_BUNDLE
        //                     ,B.IB_BS
        //                     ,B.FL_POLI_IFP
        //                     ,C.ID_STAT_OGG_BUS
        //                     ,C.ID_OGG
        //                     ,C.TP_OGG
        //                     ,C.ID_MOVI_CRZ
        //                     ,C.ID_MOVI_CHIU
        //                     ,C.DT_INI_EFF
        //                     ,C.DT_END_EFF
        //                     ,C.COD_COMP_ANIA
        //                     ,C.TP_STAT_BUS
        //                     ,C.TP_CAUS
        //                     ,C.DS_RIGA
        //                     ,C.DS_OPER_SQL
        //                     ,C.DS_VER
        //                     ,C.DS_TS_INI_CPTZ
        //                     ,C.DS_TS_END_CPTZ
        //                     ,C.DS_UTENTE
        //                     ,C.DS_STATO_ELAB
        //                  INTO
        //                     :ADE-ID-ADES
        //                    ,:ADE-ID-POLI
        //                    ,:ADE-ID-MOVI-CRZ
        //                    ,:ADE-ID-MOVI-CHIU
        //                     :IND-ADE-ID-MOVI-CHIU
        //                    ,:ADE-DT-INI-EFF-DB
        //                    ,:ADE-DT-END-EFF-DB
        //                    ,:ADE-IB-PREV
        //                     :IND-ADE-IB-PREV
        //                    ,:ADE-IB-OGG
        //                     :IND-ADE-IB-OGG
        //                    ,:ADE-COD-COMP-ANIA
        //                    ,:ADE-DT-DECOR-DB
        //                     :IND-ADE-DT-DECOR
        //                    ,:ADE-DT-SCAD-DB
        //                     :IND-ADE-DT-SCAD
        //                    ,:ADE-ETA-A-SCAD
        //                     :IND-ADE-ETA-A-SCAD
        //                    ,:ADE-DUR-AA
        //                     :IND-ADE-DUR-AA
        //                    ,:ADE-DUR-MM
        //                     :IND-ADE-DUR-MM
        //                    ,:ADE-DUR-GG
        //                     :IND-ADE-DUR-GG
        //                    ,:ADE-TP-RGM-FISC
        //                    ,:ADE-TP-RIAT
        //                     :IND-ADE-TP-RIAT
        //                    ,:ADE-TP-MOD-PAG-TIT
        //                    ,:ADE-TP-IAS
        //                     :IND-ADE-TP-IAS
        //                    ,:ADE-DT-VARZ-TP-IAS-DB
        //                     :IND-ADE-DT-VARZ-TP-IAS
        //                    ,:ADE-PRE-NET-IND
        //                     :IND-ADE-PRE-NET-IND
        //                    ,:ADE-PRE-LRD-IND
        //                     :IND-ADE-PRE-LRD-IND
        //                    ,:ADE-RAT-LRD-IND
        //                     :IND-ADE-RAT-LRD-IND
        //                    ,:ADE-PRSTZ-INI-IND
        //                     :IND-ADE-PRSTZ-INI-IND
        //                    ,:ADE-FL-COINC-ASSTO
        //                     :IND-ADE-FL-COINC-ASSTO
        //                    ,:ADE-IB-DFLT
        //                     :IND-ADE-IB-DFLT
        //                    ,:ADE-MOD-CALC
        //                     :IND-ADE-MOD-CALC
        //                    ,:ADE-TP-FNT-CNBTVA
        //                     :IND-ADE-TP-FNT-CNBTVA
        //                    ,:ADE-IMP-AZ
        //                     :IND-ADE-IMP-AZ
        //                    ,:ADE-IMP-ADER
        //                     :IND-ADE-IMP-ADER
        //                    ,:ADE-IMP-TFR
        //                     :IND-ADE-IMP-TFR
        //                    ,:ADE-IMP-VOLO
        //                     :IND-ADE-IMP-VOLO
        //                    ,:ADE-PC-AZ
        //                     :IND-ADE-PC-AZ
        //                    ,:ADE-PC-ADER
        //                     :IND-ADE-PC-ADER
        //                    ,:ADE-PC-TFR
        //                     :IND-ADE-PC-TFR
        //                    ,:ADE-PC-VOLO
        //                     :IND-ADE-PC-VOLO
        //                    ,:ADE-DT-NOVA-RGM-FISC-DB
        //                     :IND-ADE-DT-NOVA-RGM-FISC
        //                    ,:ADE-FL-ATTIV
        //                     :IND-ADE-FL-ATTIV
        //                    ,:ADE-IMP-REC-RIT-VIS
        //                     :IND-ADE-IMP-REC-RIT-VIS
        //                    ,:ADE-IMP-REC-RIT-ACC
        //                     :IND-ADE-IMP-REC-RIT-ACC
        //                    ,:ADE-FL-VARZ-STAT-TBGC
        //                     :IND-ADE-FL-VARZ-STAT-TBGC
        //                    ,:ADE-FL-PROVZA-MIGRAZ
        //                     :IND-ADE-FL-PROVZA-MIGRAZ
        //                    ,:ADE-IMPB-VIS-DA-REC
        //                     :IND-ADE-IMPB-VIS-DA-REC
        //                    ,:ADE-DT-DECOR-PREST-BAN-DB
        //                     :IND-ADE-DT-DECOR-PREST-BAN
        //                    ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                     :IND-ADE-DT-EFF-VARZ-STAT-T
        //                    ,:ADE-DS-RIGA
        //                    ,:ADE-DS-OPER-SQL
        //                    ,:ADE-DS-VER
        //                    ,:ADE-DS-TS-INI-CPTZ
        //                    ,:ADE-DS-TS-END-CPTZ
        //                    ,:ADE-DS-UTENTE
        //                    ,:ADE-DS-STATO-ELAB
        //                    ,:ADE-CUM-CNBT-CAP
        //                     :IND-ADE-CUM-CNBT-CAP
        //                    ,:ADE-IMP-GAR-CNBT
        //                     :IND-ADE-IMP-GAR-CNBT
        //                    ,:ADE-DT-ULT-CONS-CNBT-DB
        //                     :IND-ADE-DT-ULT-CONS-CNBT
        //                    ,:ADE-IDEN-ISC-FND
        //                     :IND-ADE-IDEN-ISC-FND
        //                    ,:ADE-NUM-RAT-PIAN
        //                     :IND-ADE-NUM-RAT-PIAN
        //                    ,:ADE-DT-PRESC-DB
        //                     :IND-ADE-DT-PRESC
        //                    ,:ADE-CONCS-PREST
        //                     :IND-ADE-CONCS-PREST
        //                    ,:POL-ID-POLI
        //                    ,:POL-ID-MOVI-CRZ
        //                    ,:POL-ID-MOVI-CHIU
        //                     :IND-POL-ID-MOVI-CHIU
        //                    ,:POL-IB-OGG
        //                     :IND-POL-IB-OGG
        //                    ,:POL-IB-PROP
        //                    ,:POL-DT-PROP-DB
        //                     :IND-POL-DT-PROP
        //                    ,:POL-DT-INI-EFF-DB
        //                    ,:POL-DT-END-EFF-DB
        //                    ,:POL-COD-COMP-ANIA
        //                    ,:POL-DT-DECOR-DB
        //                    ,:POL-DT-EMIS-DB
        //                    ,:POL-TP-POLI
        //                    ,:POL-DUR-AA
        //                     :IND-POL-DUR-AA
        //                    ,:POL-DUR-MM
        //                     :IND-POL-DUR-MM
        //                    ,:POL-DT-SCAD-DB
        //                     :IND-POL-DT-SCAD
        //                    ,:POL-COD-PROD
        //                    ,:POL-DT-INI-VLDT-PROD-DB
        //                    ,:POL-COD-CONV
        //                     :IND-POL-COD-CONV
        //                    ,:POL-COD-RAMO
        //                     :IND-POL-COD-RAMO
        //                    ,:POL-DT-INI-VLDT-CONV-DB
        //                     :IND-POL-DT-INI-VLDT-CONV
        //                    ,:POL-DT-APPLZ-CONV-DB
        //                     :IND-POL-DT-APPLZ-CONV
        //                    ,:POL-TP-FRM-ASSVA
        //                    ,:POL-TP-RGM-FISC
        //                     :IND-POL-TP-RGM-FISC
        //                    ,:POL-FL-ESTAS
        //                     :IND-POL-FL-ESTAS
        //                    ,:POL-FL-RSH-COMUN
        //                     :IND-POL-FL-RSH-COMUN
        //                    ,:POL-FL-RSH-COMUN-COND
        //                     :IND-POL-FL-RSH-COMUN-COND
        //                    ,:POL-TP-LIV-GENZ-TIT
        //                    ,:POL-FL-COP-FINANZ
        //                     :IND-POL-FL-COP-FINANZ
        //                    ,:POL-TP-APPLZ-DIR
        //                     :IND-POL-TP-APPLZ-DIR
        //                    ,:POL-SPE-MED
        //                     :IND-POL-SPE-MED
        //                    ,:POL-DIR-EMIS
        //                     :IND-POL-DIR-EMIS
        //                    ,:POL-DIR-1O-VERS
        //                     :IND-POL-DIR-1O-VERS
        //                    ,:POL-DIR-VERS-AGG
        //                     :IND-POL-DIR-VERS-AGG
        //                    ,:POL-COD-DVS
        //                     :IND-POL-COD-DVS
        //                    ,:POL-FL-FNT-AZ
        //                     :IND-POL-FL-FNT-AZ
        //                    ,:POL-FL-FNT-ADER
        //                     :IND-POL-FL-FNT-ADER
        //                    ,:POL-FL-FNT-TFR
        //                     :IND-POL-FL-FNT-TFR
        //                    ,:POL-FL-FNT-VOLO
        //                     :IND-POL-FL-FNT-VOLO
        //                    ,:POL-TP-OPZ-A-SCAD
        //                     :IND-POL-TP-OPZ-A-SCAD
        //                    ,:POL-AA-DIFF-PROR-DFLT
        //                     :IND-POL-AA-DIFF-PROR-DFLT
        //                    ,:POL-FL-VER-PROD
        //                     :IND-POL-FL-VER-PROD
        //                    ,:POL-DUR-GG
        //                     :IND-POL-DUR-GG
        //                    ,:POL-DIR-QUIET
        //                     :IND-POL-DIR-QUIET
        //                    ,:POL-TP-PTF-ESTNO
        //                     :IND-POL-TP-PTF-ESTNO
        //                    ,:POL-FL-CUM-PRE-CNTR
        //                     :IND-POL-FL-CUM-PRE-CNTR
        //                    ,:POL-FL-AMMB-MOVI
        //                     :IND-POL-FL-AMMB-MOVI
        //                    ,:POL-CONV-GECO
        //                     :IND-POL-CONV-GECO
        //                    ,:POL-DS-RIGA
        //                    ,:POL-DS-OPER-SQL
        //                    ,:POL-DS-VER
        //                    ,:POL-DS-TS-INI-CPTZ
        //                    ,:POL-DS-TS-END-CPTZ
        //                    ,:POL-DS-UTENTE
        //                    ,:POL-DS-STATO-ELAB
        //                    ,:POL-FL-SCUDO-FISC
        //                     :IND-POL-FL-SCUDO-FISC
        //                    ,:POL-FL-TRASFE
        //                     :IND-POL-FL-TRASFE
        //                    ,:POL-FL-TFR-STRC
        //                     :IND-POL-FL-TFR-STRC
        //                    ,:POL-DT-PRESC-DB
        //                     :IND-POL-DT-PRESC
        //                    ,:POL-COD-CONV-AGG
        //                     :IND-POL-COD-CONV-AGG
        //                    ,:POL-SUBCAT-PROD
        //                     :IND-POL-SUBCAT-PROD
        //                    ,:POL-FL-QUEST-ADEGZ-ASS
        //                     :IND-POL-FL-QUEST-ADEGZ-ASS
        //                    ,:POL-COD-TPA
        //                     :IND-POL-COD-TPA
        //                    ,:POL-ID-ACC-COMM
        //                     :IND-POL-ID-ACC-COMM
        //                    ,:POL-FL-POLI-CPI-PR
        //                     :IND-POL-FL-POLI-CPI-PR
        //                    ,:POL-FL-POLI-BUNDLING
        //                     :IND-POL-FL-POLI-BUNDLING
        //                    ,:POL-IND-POLI-PRIN-COLL
        //                     :IND-POL-IND-POLI-PRIN-COLL
        //                    ,:POL-FL-VND-BUNDLE
        //                     :IND-POL-FL-VND-BUNDLE
        //                    ,:POL-IB-BS
        //                     :IND-POL-IB-BS
        //                    ,:POL-FL-POLI-IFP
        //                     :IND-POL-FL-POLI-IFP
        //                    ,:STB-ID-STAT-OGG-BUS
        //                    ,:STB-ID-OGG
        //                    ,:STB-TP-OGG
        //                    ,:STB-ID-MOVI-CRZ
        //                    ,:STB-ID-MOVI-CHIU
        //                     :IND-STB-ID-MOVI-CHIU
        //                    ,:STB-DT-INI-EFF-DB
        //                    ,:STB-DT-END-EFF-DB
        //                    ,:STB-COD-COMP-ANIA
        //                    ,:STB-TP-STAT-BUS
        //                    ,:STB-TP-CAUS
        //                    ,:STB-DS-RIGA
        //                    ,:STB-DS-OPER-SQL
        //                    ,:STB-DS-VER
        //                    ,:STB-DS-TS-INI-CPTZ
        //                    ,:STB-DS-TS-END-CPTZ
        //                    ,:STB-DS-UTENTE
        //                    ,:STB-DS-STATO-ELAB
        //                  FROM ADES         A,
        //                       POLI         B,
        //                       STAT_OGG_BUS C
        //                  WHERE  A.IB_OGG    BETWEEN  :WLB-IB-ADE-FIRST AND
        //                                             :WLB-IB-ADE-LAST
        //                    AND B.IB_OGG          =  :WLB-IB-POLI-FIRST
        //                    AND A.ID_POLI         =   B.ID_POLI
        //                    AND A.ID_ADES         =   C.ID_OGG
        //                    AND C.TP_OGG          =  'AD'
        //                    AND C.TP_STAT_BUS     =  'VI'
        //           *        OR
        //           *            (C.TP_STAT_BUS    =  'ST'  AND
        //           *            EXISTS (SELECT
        //           *                  D.ID_MOVI_FINRIO
        //           *
        //           *               FROM  MOVI_FINRIO D
        //           *               WHERE A.ID_ADES = D.ID_ADES
        //           *                 AND D.STAT_MOVI IN ('AL','IE')
        //           *                 AND D.TP_MOVI_FINRIO = 'DI'
        //           *                 AND D.COD_COMP_ANIA   =
        //           *                     :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           *                 AND D.DT_INI_EFF     <=  :WS-DT-PTF-X
        //           *                 AND D.DT_END_EFF     >   :WS-DT-PTF-X
        //           *                 AND D.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //           *                 AND D.DS_TS_END_CPTZ >   :WS-DT-TS-PTF )))
        //                  AND A.COD_COMP_ANIA   =  :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                  AND A.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                  AND A.DT_END_EFF     >   :WS-DT-PTF-X
        //                  AND A.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                  AND A.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                  AND A.COD_COMP_ANIA   =   B.COD_COMP_ANIA
        //                  AND B.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                  AND B.DT_END_EFF     >   :WS-DT-PTF-X
        //                  AND B.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                  AND B.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //                  AND B.COD_COMP_ANIA   =   C.COD_COMP_ANIA
        //                  AND C.DT_INI_EFF     <=  :WS-DT-PTF-X
        //                  AND C.DT_END_EFF     >   :WS-DT-PTF-X
        //                  AND C.DS_TS_INI_CPTZ <=  :WS-DT-TS-PTF
        //                  AND C.DS_TS_END_CPTZ >   :WS-DT-TS-PTF
        //           *      AND A.DS_STATO_ELAB IN (
        //           *                              :IABV0002-STATE-01,
        //           *                              :IABV0002-STATE-02,
        //           *                              :IABV0002-STATE-03,
        //           *                              :IABV0002-STATE-04,
        //           *                              :IABV0002-STATE-05,
        //           *                              :IABV0002-STATE-06,
        //           *                              :IABV0002-STATE-07,
        //           *                              :IABV0002-STATE-08,
        //           *                              :IABV0002-STATE-09,
        //           *                              :IABV0002-STATE-10
        //           *                                  )
        //           *        AND NOT A.DS_VER     = :IABV0009-VERSIONING
        //                    ORDER BY A.ID_POLI, A.ID_ADES
        //                   FETCH FIRST ROW ONLY
        //                END-EXEC.
        adesPoliDao.selectRec4(this.getAdesPoliLdbm02502());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A320-UPDATE-SC04<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320UpdateSc04() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK-SC04         THRU A330-SC04-EX
        //              WHEN IDSV0003-WHERE-CONDITION-04
        //                   PERFORM A340-UPDATE-WHERE-COND-SC04 THRU A340-SC04-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                       THRU A345-SC04-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
            // COB_CODE: PERFORM A330-UPDATE-PK-SC04         THRU A330-SC04-EX
            a330UpdatePkSc04();
        }
        else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition04()) {
            // COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC04 THRU A340-SC04-EX
            a340UpdateWhereCondSc04();
        }
        else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
            // COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC04
            //                                               THRU A345-SC04-EX
            a345UpdateFirstActionSc04();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidLevelOper();
        }
    }

    /**Original name: A330-UPDATE-PK-SC04<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePkSc04() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=5171, because the code is unreachable.
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
        z960LengthVchar();
        //           ,DS_VER                 = :IABV0009-VERSIONING
        // COB_CODE:      EXEC SQL
        //                     UPDATE ADES
        //                     SET
        //                        DS_OPER_SQL            = :ADE-DS-OPER-SQL
        //           *           ,DS_VER                 = :IABV0009-VERSIONING
        //                       ,DS_UTENTE              = :ADE-DS-UTENTE
        //                       ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
        //                     WHERE             DS_RIGA = :ADE-DS-RIGA
        //                END-EXEC.
        adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A340-UPDATE-WHERE-COND-SC04<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateWhereCondSc04() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A345-UPDATE-FIRST-ACTION-SC04<br>
	 * <pre>*****************************************************************</pre>*/
    private void a345UpdateFirstActionSc04() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A360-OPEN-CURSOR-SC04<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursorSc04() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-SC04 THRU A305-SC04-EX.
        a305DeclareCursorSc04();
        // COB_CODE: EXEC SQL
        //                OPEN CUR-SC04
        //           END-EXEC.
        adesPoliDao.openCurSc04(this.getAdesPoliLdbm02502());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-SC04<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursorSc04() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-SC04
        //           END-EXEC.
        adesPoliDao.closeCurSc04();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST-SC04<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirstSc04() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-SC04    THRU A360-SC04-EX.
        a360OpenCursorSc04();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT-SC04  THRU A390-SC04-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC04  THRU A390-SC04-EX
            a390FetchNextSc04();
        }
    }

    /**Original name: A390-FETCH-NEXT-SC04<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNextSc04() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-SC04
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //               ,:POL-ID-POLI
        //               ,:POL-ID-MOVI-CRZ
        //               ,:POL-ID-MOVI-CHIU
        //                :IND-POL-ID-MOVI-CHIU
        //               ,:POL-IB-OGG
        //                :IND-POL-IB-OGG
        //               ,:POL-IB-PROP
        //               ,:POL-DT-PROP-DB
        //                :IND-POL-DT-PROP
        //               ,:POL-DT-INI-EFF-DB
        //               ,:POL-DT-END-EFF-DB
        //               ,:POL-COD-COMP-ANIA
        //               ,:POL-DT-DECOR-DB
        //               ,:POL-DT-EMIS-DB
        //               ,:POL-TP-POLI
        //               ,:POL-DUR-AA
        //                :IND-POL-DUR-AA
        //               ,:POL-DUR-MM
        //                :IND-POL-DUR-MM
        //               ,:POL-DT-SCAD-DB
        //                :IND-POL-DT-SCAD
        //               ,:POL-COD-PROD
        //               ,:POL-DT-INI-VLDT-PROD-DB
        //               ,:POL-COD-CONV
        //                :IND-POL-COD-CONV
        //               ,:POL-COD-RAMO
        //                :IND-POL-COD-RAMO
        //               ,:POL-DT-INI-VLDT-CONV-DB
        //                :IND-POL-DT-INI-VLDT-CONV
        //               ,:POL-DT-APPLZ-CONV-DB
        //                :IND-POL-DT-APPLZ-CONV
        //               ,:POL-TP-FRM-ASSVA
        //               ,:POL-TP-RGM-FISC
        //                :IND-POL-TP-RGM-FISC
        //               ,:POL-FL-ESTAS
        //                :IND-POL-FL-ESTAS
        //               ,:POL-FL-RSH-COMUN
        //                :IND-POL-FL-RSH-COMUN
        //               ,:POL-FL-RSH-COMUN-COND
        //                :IND-POL-FL-RSH-COMUN-COND
        //               ,:POL-TP-LIV-GENZ-TIT
        //               ,:POL-FL-COP-FINANZ
        //                :IND-POL-FL-COP-FINANZ
        //               ,:POL-TP-APPLZ-DIR
        //                :IND-POL-TP-APPLZ-DIR
        //               ,:POL-SPE-MED
        //                :IND-POL-SPE-MED
        //               ,:POL-DIR-EMIS
        //                :IND-POL-DIR-EMIS
        //               ,:POL-DIR-1O-VERS
        //                :IND-POL-DIR-1O-VERS
        //               ,:POL-DIR-VERS-AGG
        //                :IND-POL-DIR-VERS-AGG
        //               ,:POL-COD-DVS
        //                :IND-POL-COD-DVS
        //               ,:POL-FL-FNT-AZ
        //                :IND-POL-FL-FNT-AZ
        //               ,:POL-FL-FNT-ADER
        //                :IND-POL-FL-FNT-ADER
        //               ,:POL-FL-FNT-TFR
        //                :IND-POL-FL-FNT-TFR
        //               ,:POL-FL-FNT-VOLO
        //                :IND-POL-FL-FNT-VOLO
        //               ,:POL-TP-OPZ-A-SCAD
        //                :IND-POL-TP-OPZ-A-SCAD
        //               ,:POL-AA-DIFF-PROR-DFLT
        //                :IND-POL-AA-DIFF-PROR-DFLT
        //               ,:POL-FL-VER-PROD
        //                :IND-POL-FL-VER-PROD
        //               ,:POL-DUR-GG
        //                :IND-POL-DUR-GG
        //               ,:POL-DIR-QUIET
        //                :IND-POL-DIR-QUIET
        //               ,:POL-TP-PTF-ESTNO
        //                :IND-POL-TP-PTF-ESTNO
        //               ,:POL-FL-CUM-PRE-CNTR
        //                :IND-POL-FL-CUM-PRE-CNTR
        //               ,:POL-FL-AMMB-MOVI
        //                :IND-POL-FL-AMMB-MOVI
        //               ,:POL-CONV-GECO
        //                :IND-POL-CONV-GECO
        //               ,:POL-DS-RIGA
        //               ,:POL-DS-OPER-SQL
        //               ,:POL-DS-VER
        //               ,:POL-DS-TS-INI-CPTZ
        //               ,:POL-DS-TS-END-CPTZ
        //               ,:POL-DS-UTENTE
        //               ,:POL-DS-STATO-ELAB
        //               ,:POL-FL-SCUDO-FISC
        //                :IND-POL-FL-SCUDO-FISC
        //               ,:POL-FL-TRASFE
        //                :IND-POL-FL-TRASFE
        //               ,:POL-FL-TFR-STRC
        //                :IND-POL-FL-TFR-STRC
        //               ,:POL-DT-PRESC-DB
        //                :IND-POL-DT-PRESC
        //               ,:POL-COD-CONV-AGG
        //                :IND-POL-COD-CONV-AGG
        //               ,:POL-SUBCAT-PROD
        //                :IND-POL-SUBCAT-PROD
        //               ,:POL-FL-QUEST-ADEGZ-ASS
        //                :IND-POL-FL-QUEST-ADEGZ-ASS
        //               ,:POL-COD-TPA
        //                :IND-POL-COD-TPA
        //               ,:POL-ID-ACC-COMM
        //                :IND-POL-ID-ACC-COMM
        //               ,:POL-FL-POLI-CPI-PR
        //                :IND-POL-FL-POLI-CPI-PR
        //               ,:POL-FL-POLI-BUNDLING
        //                :IND-POL-FL-POLI-BUNDLING
        //               ,:POL-IND-POLI-PRIN-COLL
        //                :IND-POL-IND-POLI-PRIN-COLL
        //               ,:POL-FL-VND-BUNDLE
        //                :IND-POL-FL-VND-BUNDLE
        //               ,:POL-IB-BS
        //                :IND-POL-IB-BS
        //               ,:POL-FL-POLI-IFP
        //                :IND-POL-FL-POLI-IFP
        //               ,:STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        adesPoliDao.fetchCurSc04(this.getAdesPoliLdbm0250());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC04 THRU A370-SC04-EX
            a370CloseCursorSc04();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: SC05-SELECTION-CURSOR-05<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
    private void sc05SelectionCursor05() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC05           THRU A310-SC05-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-SC05      THRU A360-SC05-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-SC05     THRU A370-SC05-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-SC05      THRU A380-SC05-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-SC05       THRU A390-SC05-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE-SC05           THRU A320-SC05-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER          TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC05           THRU A310-SC05-EX
            a310SelectSc05();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-SC05      THRU A360-SC05-EX
            a360OpenCursorSc05();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC05     THRU A370-SC05-EX
            a370CloseCursorSc05();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-SC05      THRU A380-SC05-EX
            a380FetchFirstSc05();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC05       THRU A390-SC05-EX
            a390FetchNextSc05();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE-SC05           THRU A320-SC05-EX
            a320UpdateSc05();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursorSc05() {
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              CONTINUE
        //           ELSE
        //              CONTINUE
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
        // COB_CODE:         EXEC SQL
        //                        DECLARE CUR-SC05-RANGE CURSOR WITH HOLD FOR
        //                     SELECT
        //                            A.ID_ADES
        //                           ,A.ID_POLI
        //                           ,A.ID_MOVI_CRZ
        //                           ,A.ID_MOVI_CHIU
        //                           ,A.DT_INI_EFF
        //                           ,A.DT_END_EFF
        //                           ,A.IB_PREV
        //                           ,A.IB_OGG
        //                           ,A.COD_COMP_ANIA
        //                           ,A.DT_DECOR
        //                           ,A.DT_SCAD
        //                           ,A.ETA_A_SCAD
        //                           ,A.DUR_AA
        //                           ,A.DUR_MM
        //                           ,A.DUR_GG
        //                           ,A.TP_RGM_FISC
        //                           ,A.TP_RIAT
        //                           ,A.TP_MOD_PAG_TIT
        //                           ,A.TP_IAS
        //                           ,A.DT_VARZ_TP_IAS
        //                           ,A.PRE_NET_IND
        //                           ,A.PRE_LRD_IND
        //                           ,A.RAT_LRD_IND
        //                           ,A.PRSTZ_INI_IND
        //                           ,A.FL_COINC_ASSTO
        //                           ,A.IB_DFLT
        //                           ,A.MOD_CALC
        //                           ,A.TP_FNT_CNBTVA
        //                           ,A.IMP_AZ
        //                           ,A.IMP_ADER
        //                           ,A.IMP_TFR
        //                           ,A.IMP_VOLO
        //                           ,A.PC_AZ
        //                           ,A.PC_ADER
        //                           ,A.PC_TFR
        //                           ,A.PC_VOLO
        //                           ,A.DT_NOVA_RGM_FISC
        //                           ,A.FL_ATTIV
        //                           ,A.IMP_REC_RIT_VIS
        //                           ,A.IMP_REC_RIT_ACC
        //                           ,A.FL_VARZ_STAT_TBGC
        //                           ,A.FL_PROVZA_MIGRAZ
        //                           ,A.IMPB_VIS_DA_REC
        //                           ,A.DT_DECOR_PREST_BAN
        //                           ,A.DT_EFF_VARZ_STAT_T
        //                           ,A.DS_RIGA
        //                           ,A.DS_OPER_SQL
        //                           ,A.DS_VER
        //                           ,A.DS_TS_INI_CPTZ
        //                           ,A.DS_TS_END_CPTZ
        //                           ,A.DS_UTENTE
        //                           ,A.DS_STATO_ELAB
        //                           ,A.CUM_CNBT_CAP
        //                           ,A.IMP_GAR_CNBT
        //                           ,A.DT_ULT_CONS_CNBT
        //                           ,A.IDEN_ISC_FND
        //                           ,A.NUM_RAT_PIAN
        //                           ,A.DT_PRESC
        //                           ,A.CONCS_PREST
        //                           ,B.ID_POLI
        //                           ,B.ID_MOVI_CRZ
        //                           ,B.ID_MOVI_CHIU
        //                           ,B.IB_OGG
        //                           ,B.IB_PROP
        //                           ,B.DT_PROP
        //                           ,B.DT_INI_EFF
        //                           ,B.DT_END_EFF
        //                           ,B.COD_COMP_ANIA
        //                           ,B.DT_DECOR
        //                           ,B.DT_EMIS
        //                           ,B.TP_POLI
        //                           ,B.DUR_AA
        //                           ,B.DUR_MM
        //                           ,B.DT_SCAD
        //                           ,B.COD_PROD
        //                           ,B.DT_INI_VLDT_PROD
        //                           ,B.COD_CONV
        //                           ,B.COD_RAMO
        //                           ,B.DT_INI_VLDT_CONV
        //                           ,B.DT_APPLZ_CONV
        //                           ,B.TP_FRM_ASSVA
        //                           ,B.TP_RGM_FISC
        //                           ,B.FL_ESTAS
        //                           ,B.FL_RSH_COMUN
        //                           ,B.FL_RSH_COMUN_COND
        //                           ,B.TP_LIV_GENZ_TIT
        //                           ,B.FL_COP_FINANZ
        //                           ,B.TP_APPLZ_DIR
        //                           ,B.SPE_MED
        //                           ,B.DIR_EMIS
        //                           ,B.DIR_1O_VERS
        //                           ,B.DIR_VERS_AGG
        //                           ,B.COD_DVS
        //                           ,B.FL_FNT_AZ
        //                           ,B.FL_FNT_ADER
        //                           ,B.FL_FNT_TFR
        //                           ,B.FL_FNT_VOLO
        //                           ,B.TP_OPZ_A_SCAD
        //                           ,B.AA_DIFF_PROR_DFLT
        //                           ,B.FL_VER_PROD
        //                           ,B.DUR_GG
        //                           ,B.DIR_QUIET
        //                           ,B.TP_PTF_ESTNO
        //                           ,B.FL_CUM_PRE_CNTR
        //                           ,B.FL_AMMB_MOVI
        //                           ,B.CONV_GECO
        //                           ,B.DS_RIGA
        //                           ,B.DS_OPER_SQL
        //                           ,B.DS_VER
        //                           ,B.DS_TS_INI_CPTZ
        //                           ,B.DS_TS_END_CPTZ
        //                           ,B.DS_UTENTE
        //                           ,B.DS_STATO_ELAB
        //                           ,B.FL_SCUDO_FISC
        //                           ,B.FL_TRASFE
        //                           ,B.FL_TFR_STRC
        //                           ,B.DT_PRESC
        //                           ,B.COD_CONV_AGG
        //                           ,B.SUBCAT_PROD
        //                           ,B.FL_QUEST_ADEGZ_ASS
        //                           ,B.COD_TPA
        //                           ,B.ID_ACC_COMM
        //                           ,B.FL_POLI_CPI_PR
        //                           ,B.FL_POLI_BUNDLING
        //                           ,B.IND_POLI_PRIN_COLL
        //                           ,B.FL_VND_BUNDLE
        //                           ,B.IB_BS
        //                           ,B.FL_POLI_IFP
        //                           ,C.ID_STAT_OGG_BUS
        //                           ,C.ID_OGG
        //                           ,C.TP_OGG
        //                           ,C.ID_MOVI_CRZ
        //                           ,C.ID_MOVI_CHIU
        //                           ,C.DT_INI_EFF
        //                           ,C.DT_END_EFF
        //                           ,C.COD_COMP_ANIA
        //                           ,C.TP_STAT_BUS
        //                           ,C.TP_CAUS
        //                           ,C.DS_RIGA
        //                           ,C.DS_OPER_SQL
        //                           ,C.DS_VER
        //                           ,C.DS_TS_INI_CPTZ
        //                           ,C.DS_TS_END_CPTZ
        //                           ,C.DS_UTENTE
        //                           ,C.DS_STATO_ELAB
        //                     FROM ADES      A,
        //                          POLI      B,
        //                          STAT_OGG_BUS C
        //                     WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
        //                                                :WS-TP-FRM-ASSVA2 )
        //                       AND A.ID_POLI      =   B.ID_POLI
        //                       AND A.ID_ADES      =   C.ID_OGG
        //                       AND A.ID_POLI     BETWEEN   :IABV0009-ID-OGG-DA AND
        //                                                   :IABV0009-ID-OGG-A
        //                       AND C.TP_OGG       =  'AD'
        //                       AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                       AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                       AND A.DT_END_EFF  >   :WS-DT-PTF-X
        //                       AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                       AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                       AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
        //                       AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                       AND B.DT_END_EFF  >   :WS-DT-PTF-X
        //                       AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                       AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                       AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
        //                       AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                       AND C.DT_END_EFF  >   :WS-DT-PTF-X
        //                       AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                       AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                       AND (A.ID_ADES IN
        //                          (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D,
        //                                                           STAT_OGG_BUS E
        //                           WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
        //                           AND E.TP_OGG = 'TG'
        //           *                AND E.TP_STAT_BUS IN ('VI','ST')
        //                           AND E.TP_STAT_BUS  = 'VI'
        //                           AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
        //                           AND D.COD_COMP_ANIA =
        //                              :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                           AND D.DT_INI_EFF <=   :WS-DT-PTF-X
        //                           AND D.DT_END_EFF >    :WS-DT-PTF-X
        //                           AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                           AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                           AND E.DT_INI_EFF <=   :WS-DT-PTF-X
        //                           AND E.DT_END_EFF >    :WS-DT-PTF-X
        //                           AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                           AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                           AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
        //                                OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
        //                           ))
        //           *               OR A.ID_ADES IN (
        //           *                 SELECT DISTINCT(D.ID_ADES)
        //           *                 FROM MOVI_FINRIO D
        //           *                 WHERE A.ID_ADES = D.ID_ADES
        //           *                 AND D.TP_MOVI_FINRIO = 'DI'
        //           *                 AND D.COD_COMP_ANIA =
        //           *                 :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           *                 AND D.DT_INI_EFF  <=  :WS-DT-PTF-X
        //           *                 AND D.DT_END_EFF  >   :WS-DT-PTF-X
        //           *                 AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF-PRE
        //           *                 AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF-PRE
        //           *                 AND C.TP_STAT_BUS =  'ST'
        //           *               ))
        //           *           AND A.DS_STATO_ELAB IN (
        //           *                                   :IABV0002-STATE-01,
        //           *                                   :IABV0002-STATE-02,
        //           *                                   :IABV0002-STATE-03,
        //           *                                   :IABV0002-STATE-04,
        //           *                                   :IABV0002-STATE-05,
        //           *                                   :IABV0002-STATE-06,
        //           *                                   :IABV0002-STATE-07,
        //           *                                   :IABV0002-STATE-08,
        //           *                                   :IABV0002-STATE-09,
        //           *                                   :IABV0002-STATE-10
        //           *                                     )
        //           *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
        //                       ORDER BY A.ID_POLI, A.ID_ADES
        //                   END-EXEC
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE
        //continue
        }
        else {
        // COB_CODE:         EXEC SQL
        //                        DECLARE CUR-SC05 CURSOR WITH HOLD FOR
        //                     SELECT
        //                            A.ID_ADES
        //                           ,A.ID_POLI
        //                           ,A.ID_MOVI_CRZ
        //                           ,A.ID_MOVI_CHIU
        //                           ,A.DT_INI_EFF
        //                           ,A.DT_END_EFF
        //                           ,A.IB_PREV
        //                           ,A.IB_OGG
        //                           ,A.COD_COMP_ANIA
        //                           ,A.DT_DECOR
        //                           ,A.DT_SCAD
        //                           ,A.ETA_A_SCAD
        //                           ,A.DUR_AA
        //                           ,A.DUR_MM
        //                           ,A.DUR_GG
        //                           ,A.TP_RGM_FISC
        //                           ,A.TP_RIAT
        //                           ,A.TP_MOD_PAG_TIT
        //                           ,A.TP_IAS
        //                           ,A.DT_VARZ_TP_IAS
        //                           ,A.PRE_NET_IND
        //                           ,A.PRE_LRD_IND
        //                           ,A.RAT_LRD_IND
        //                           ,A.PRSTZ_INI_IND
        //                           ,A.FL_COINC_ASSTO
        //                           ,A.IB_DFLT
        //                           ,A.MOD_CALC
        //                           ,A.TP_FNT_CNBTVA
        //                           ,A.IMP_AZ
        //                           ,A.IMP_ADER
        //                           ,A.IMP_TFR
        //                           ,A.IMP_VOLO
        //                           ,A.PC_AZ
        //                           ,A.PC_ADER
        //                           ,A.PC_TFR
        //                           ,A.PC_VOLO
        //                           ,A.DT_NOVA_RGM_FISC
        //                           ,A.FL_ATTIV
        //                           ,A.IMP_REC_RIT_VIS
        //                           ,A.IMP_REC_RIT_ACC
        //                           ,A.FL_VARZ_STAT_TBGC
        //                           ,A.FL_PROVZA_MIGRAZ
        //                           ,A.IMPB_VIS_DA_REC
        //                           ,A.DT_DECOR_PREST_BAN
        //                           ,A.DT_EFF_VARZ_STAT_T
        //                           ,A.DS_RIGA
        //                           ,A.DS_OPER_SQL
        //                           ,A.DS_VER
        //                           ,A.DS_TS_INI_CPTZ
        //                           ,A.DS_TS_END_CPTZ
        //                           ,A.DS_UTENTE
        //                           ,A.DS_STATO_ELAB
        //                           ,A.CUM_CNBT_CAP
        //                           ,A.IMP_GAR_CNBT
        //                           ,A.DT_ULT_CONS_CNBT
        //                           ,A.IDEN_ISC_FND
        //                           ,A.NUM_RAT_PIAN
        //                           ,A.DT_PRESC
        //                           ,A.CONCS_PREST
        //                           ,B.ID_POLI
        //                           ,B.ID_MOVI_CRZ
        //                           ,B.ID_MOVI_CHIU
        //                           ,B.IB_OGG
        //                           ,B.IB_PROP
        //                           ,B.DT_PROP
        //                           ,B.DT_INI_EFF
        //                           ,B.DT_END_EFF
        //                           ,B.COD_COMP_ANIA
        //                           ,B.DT_DECOR
        //                           ,B.DT_EMIS
        //                           ,B.TP_POLI
        //                           ,B.DUR_AA
        //                           ,B.DUR_MM
        //                           ,B.DT_SCAD
        //                           ,B.COD_PROD
        //                           ,B.DT_INI_VLDT_PROD
        //                           ,B.COD_CONV
        //                           ,B.COD_RAMO
        //                           ,B.DT_INI_VLDT_CONV
        //                           ,B.DT_APPLZ_CONV
        //                           ,B.TP_FRM_ASSVA
        //                           ,B.TP_RGM_FISC
        //                           ,B.FL_ESTAS
        //                           ,B.FL_RSH_COMUN
        //                           ,B.FL_RSH_COMUN_COND
        //                           ,B.TP_LIV_GENZ_TIT
        //                           ,B.FL_COP_FINANZ
        //                           ,B.TP_APPLZ_DIR
        //                           ,B.SPE_MED
        //                           ,B.DIR_EMIS
        //                           ,B.DIR_1O_VERS
        //                           ,B.DIR_VERS_AGG
        //                           ,B.COD_DVS
        //                           ,B.FL_FNT_AZ
        //                           ,B.FL_FNT_ADER
        //                           ,B.FL_FNT_TFR
        //                           ,B.FL_FNT_VOLO
        //                           ,B.TP_OPZ_A_SCAD
        //                           ,B.AA_DIFF_PROR_DFLT
        //                           ,B.FL_VER_PROD
        //                           ,B.DUR_GG
        //                           ,B.DIR_QUIET
        //                           ,B.TP_PTF_ESTNO
        //                           ,B.FL_CUM_PRE_CNTR
        //                           ,B.FL_AMMB_MOVI
        //                           ,B.CONV_GECO
        //                           ,B.DS_RIGA
        //                           ,B.DS_OPER_SQL
        //                           ,B.DS_VER
        //                           ,B.DS_TS_INI_CPTZ
        //                           ,B.DS_TS_END_CPTZ
        //                           ,B.DS_UTENTE
        //                           ,B.DS_STATO_ELAB
        //                           ,B.FL_SCUDO_FISC
        //                           ,B.FL_TRASFE
        //                           ,B.FL_TFR_STRC
        //                           ,B.DT_PRESC
        //                           ,B.COD_CONV_AGG
        //                           ,B.SUBCAT_PROD
        //                           ,B.FL_QUEST_ADEGZ_ASS
        //                           ,B.COD_TPA
        //                           ,B.ID_ACC_COMM
        //                           ,B.FL_POLI_CPI_PR
        //                           ,B.FL_POLI_BUNDLING
        //                           ,B.IND_POLI_PRIN_COLL
        //                           ,B.FL_VND_BUNDLE
        //                           ,B.IB_BS
        //                           ,B.FL_POLI_IFP
        //                           ,C.ID_STAT_OGG_BUS
        //                           ,C.ID_OGG
        //                           ,C.TP_OGG
        //                           ,C.ID_MOVI_CRZ
        //                           ,C.ID_MOVI_CHIU
        //                           ,C.DT_INI_EFF
        //                           ,C.DT_END_EFF
        //                           ,C.COD_COMP_ANIA
        //                           ,C.TP_STAT_BUS
        //                           ,C.TP_CAUS
        //                           ,C.DS_RIGA
        //                           ,C.DS_OPER_SQL
        //                           ,C.DS_VER
        //                           ,C.DS_TS_INI_CPTZ
        //                           ,C.DS_TS_END_CPTZ
        //                           ,C.DS_UTENTE
        //                           ,C.DS_STATO_ELAB
        //                     FROM ADES      A,
        //                          POLI      B,
        //                          STAT_OGG_BUS C
        //                     WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
        //                                                :WS-TP-FRM-ASSVA2 )
        //                       AND A.ID_POLI      =   B.ID_POLI
        //                       AND A.ID_ADES      =   C.ID_OGG
        //                       AND C.TP_OGG       =  'AD'
        //                       AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                       AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                       AND A.DT_END_EFF  >   :WS-DT-PTF-X
        //                       AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                       AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                       AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
        //                       AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                       AND B.DT_END_EFF  >   :WS-DT-PTF-X
        //                       AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                       AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                       AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
        //                       AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                       AND C.DT_END_EFF  >   :WS-DT-PTF-X
        //                       AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                       AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                       AND (A.ID_ADES IN
        //                          (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D,
        //                                                           STAT_OGG_BUS E
        //                           WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
        //                           AND E.TP_OGG = 'TG'
        //           *                AND E.TP_STAT_BUS IN ('VI','ST')
        //                           AND E.TP_STAT_BUS = 'VI'
        //                           AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
        //                           AND D.COD_COMP_ANIA =
        //                               :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                           AND D.DT_INI_EFF <=   :WS-DT-PTF-X
        //                           AND D.DT_END_EFF >    :WS-DT-PTF-X
        //                           AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                           AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                           AND E.DT_INI_EFF <=   :WS-DT-PTF-X
        //                           AND E.DT_END_EFF >    :WS-DT-PTF-X
        //                           AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                           AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                           AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
        //                                OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
        //                           ))
        //           *               OR A.ID_ADES IN (
        //           *                 SELECT DISTINCT(D.ID_ADES)
        //           *                 FROM MOVI_FINRIO D
        //           *                 WHERE A.ID_ADES = D.ID_ADES
        //           *                 AND D.TP_MOVI_FINRIO = 'DI'
        //           *                 AND D.COD_COMP_ANIA =
        //           *                 :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           *                 AND D.DT_INI_EFF  <=  :WS-DT-PTF-X
        //           *                 AND D.DT_END_EFF  >   :WS-DT-PTF-X
        //           *                 AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF-PRE
        //           *                 AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF-PRE
        //           *                 AND C.TP_STAT_BUS =  'ST'
        //           *               ))
        //           *           AND A.DS_STATO_ELAB IN (
        //           *                                   :IABV0002-STATE-01,
        //           *                                   :IABV0002-STATE-02,
        //           *                                   :IABV0002-STATE-03,
        //           *                                   :IABV0002-STATE-04,
        //           *                                   :IABV0002-STATE-05,
        //           *                                   :IABV0002-STATE-06,
        //           *                                   :IABV0002-STATE-07,
        //           *                                   :IABV0002-STATE-08,
        //           *                                   :IABV0002-STATE-09,
        //           *                                   :IABV0002-STATE-10
        //           *                                     )
        //           *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
        //                       ORDER BY A.ID_POLI, A.ID_ADES
        //                   END-EXEC
        // DECLARE CURSOR doesn't need a translation;
        // COB_CODE: CONTINUE
        //continue
        }
    }

    /**Original name: A310-SELECT-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310SelectSc05() {
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              END-EXEC
        //           ELSE
        //              END-EXEC
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
            //           AND A.DS_STATO_ELAB IN (
            //                                   :IABV0002-STATE-01,
            //                                   :IABV0002-STATE-02,
            //                                   :IABV0002-STATE-03,
            //                                   :IABV0002-STATE-04,
            //                                   :IABV0002-STATE-05,
            //                                   :IABV0002-STATE-06,
            //                                   :IABV0002-STATE-07,
            //                                   :IABV0002-STATE-08,
            //                                   :IABV0002-STATE-09,
            //                                   :IABV0002-STATE-10
            //                                     )
            //           AND NOT A.DS_VER  = :IABV0009-VERSIONING
            // COB_CODE:         EXEC SQL
            //                     SELECT
            //                            A.ID_ADES
            //                           ,A.ID_POLI
            //                           ,A.ID_MOVI_CRZ
            //                           ,A.ID_MOVI_CHIU
            //                           ,A.DT_INI_EFF
            //                           ,A.DT_END_EFF
            //                           ,A.IB_PREV
            //                           ,A.IB_OGG
            //                           ,A.COD_COMP_ANIA
            //                           ,A.DT_DECOR
            //                           ,A.DT_SCAD
            //                           ,A.ETA_A_SCAD
            //                           ,A.DUR_AA
            //                           ,A.DUR_MM
            //                           ,A.DUR_GG
            //                           ,A.TP_RGM_FISC
            //                           ,A.TP_RIAT
            //                           ,A.TP_MOD_PAG_TIT
            //                           ,A.TP_IAS
            //                           ,A.DT_VARZ_TP_IAS
            //                           ,A.PRE_NET_IND
            //                           ,A.PRE_LRD_IND
            //                           ,A.RAT_LRD_IND
            //                           ,A.PRSTZ_INI_IND
            //                           ,A.FL_COINC_ASSTO
            //                           ,A.IB_DFLT
            //                           ,A.MOD_CALC
            //                           ,A.TP_FNT_CNBTVA
            //                           ,A.IMP_AZ
            //                           ,A.IMP_ADER
            //                           ,A.IMP_TFR
            //                           ,A.IMP_VOLO
            //                           ,A.PC_AZ
            //                           ,A.PC_ADER
            //                           ,A.PC_TFR
            //                           ,A.PC_VOLO
            //                           ,A.DT_NOVA_RGM_FISC
            //                           ,A.FL_ATTIV
            //                           ,A.IMP_REC_RIT_VIS
            //                           ,A.IMP_REC_RIT_ACC
            //                           ,A.FL_VARZ_STAT_TBGC
            //                           ,A.FL_PROVZA_MIGRAZ
            //                           ,A.IMPB_VIS_DA_REC
            //                           ,A.DT_DECOR_PREST_BAN
            //                           ,A.DT_EFF_VARZ_STAT_T
            //                           ,A.DS_RIGA
            //                           ,A.DS_OPER_SQL
            //                           ,A.DS_VER
            //                           ,A.DS_TS_INI_CPTZ
            //                           ,A.DS_TS_END_CPTZ
            //                           ,A.DS_UTENTE
            //                           ,A.DS_STATO_ELAB
            //                           ,A.CUM_CNBT_CAP
            //                           ,A.IMP_GAR_CNBT
            //                           ,A.DT_ULT_CONS_CNBT
            //                           ,A.IDEN_ISC_FND
            //                           ,A.NUM_RAT_PIAN
            //                           ,A.DT_PRESC
            //                           ,A.CONCS_PREST
            //                           ,B.ID_POLI
            //                           ,B.ID_MOVI_CRZ
            //                           ,B.ID_MOVI_CHIU
            //                           ,B.IB_OGG
            //                           ,B.IB_PROP
            //                           ,B.DT_PROP
            //                           ,B.DT_INI_EFF
            //                           ,B.DT_END_EFF
            //                           ,B.COD_COMP_ANIA
            //                           ,B.DT_DECOR
            //                           ,B.DT_EMIS
            //                           ,B.TP_POLI
            //                           ,B.DUR_AA
            //                           ,B.DUR_MM
            //                           ,B.DT_SCAD
            //                           ,B.COD_PROD
            //                           ,B.DT_INI_VLDT_PROD
            //                           ,B.COD_CONV
            //                           ,B.COD_RAMO
            //                           ,B.DT_INI_VLDT_CONV
            //                           ,B.DT_APPLZ_CONV
            //                           ,B.TP_FRM_ASSVA
            //                           ,B.TP_RGM_FISC
            //                           ,B.FL_ESTAS
            //                           ,B.FL_RSH_COMUN
            //                           ,B.FL_RSH_COMUN_COND
            //                           ,B.TP_LIV_GENZ_TIT
            //                           ,B.FL_COP_FINANZ
            //                           ,B.TP_APPLZ_DIR
            //                           ,B.SPE_MED
            //                           ,B.DIR_EMIS
            //                           ,B.DIR_1O_VERS
            //                           ,B.DIR_VERS_AGG
            //                           ,B.COD_DVS
            //                           ,B.FL_FNT_AZ
            //                           ,B.FL_FNT_ADER
            //                           ,B.FL_FNT_TFR
            //                           ,B.FL_FNT_VOLO
            //                           ,B.TP_OPZ_A_SCAD
            //                           ,B.AA_DIFF_PROR_DFLT
            //                           ,B.FL_VER_PROD
            //                           ,B.DUR_GG
            //                           ,B.DIR_QUIET
            //                           ,B.TP_PTF_ESTNO
            //                           ,B.FL_CUM_PRE_CNTR
            //                           ,B.FL_AMMB_MOVI
            //                           ,B.CONV_GECO
            //                           ,B.DS_RIGA
            //                           ,B.DS_OPER_SQL
            //                           ,B.DS_VER
            //                           ,B.DS_TS_INI_CPTZ
            //                           ,B.DS_TS_END_CPTZ
            //                           ,B.DS_UTENTE
            //                           ,B.DS_STATO_ELAB
            //                           ,B.FL_SCUDO_FISC
            //                           ,B.FL_TRASFE
            //                           ,B.FL_TFR_STRC
            //                           ,B.DT_PRESC
            //                           ,B.COD_CONV_AGG
            //                           ,B.SUBCAT_PROD
            //                           ,B.FL_QUEST_ADEGZ_ASS
            //                           ,B.COD_TPA
            //                           ,B.ID_ACC_COMM
            //                           ,B.FL_POLI_CPI_PR
            //                           ,B.FL_POLI_BUNDLING
            //                           ,B.IND_POLI_PRIN_COLL
            //                           ,B.FL_VND_BUNDLE
            //                           ,B.IB_BS
            //                           ,B.FL_POLI_IFP
            //                           ,C.ID_STAT_OGG_BUS
            //                           ,C.ID_OGG
            //                           ,C.TP_OGG
            //                           ,C.ID_MOVI_CRZ
            //                           ,C.ID_MOVI_CHIU
            //                           ,C.DT_INI_EFF
            //                           ,C.DT_END_EFF
            //                           ,C.COD_COMP_ANIA
            //                           ,C.TP_STAT_BUS
            //                           ,C.TP_CAUS
            //                           ,C.DS_RIGA
            //                           ,C.DS_OPER_SQL
            //                           ,C.DS_VER
            //                           ,C.DS_TS_INI_CPTZ
            //                           ,C.DS_TS_END_CPTZ
            //                           ,C.DS_UTENTE
            //                           ,C.DS_STATO_ELAB
            //                     INTO
            //                           :ADE-ID-ADES
            //                          ,:ADE-ID-POLI
            //                          ,:ADE-ID-MOVI-CRZ
            //                          ,:ADE-ID-MOVI-CHIU
            //                           :IND-ADE-ID-MOVI-CHIU
            //                          ,:ADE-DT-INI-EFF-DB
            //                          ,:ADE-DT-END-EFF-DB
            //                          ,:ADE-IB-PREV
            //                           :IND-ADE-IB-PREV
            //                          ,:ADE-IB-OGG
            //                           :IND-ADE-IB-OGG
            //                          ,:ADE-COD-COMP-ANIA
            //                          ,:ADE-DT-DECOR-DB
            //                           :IND-ADE-DT-DECOR
            //                          ,:ADE-DT-SCAD-DB
            //                           :IND-ADE-DT-SCAD
            //                          ,:ADE-ETA-A-SCAD
            //                           :IND-ADE-ETA-A-SCAD
            //                          ,:ADE-DUR-AA
            //                           :IND-ADE-DUR-AA
            //                          ,:ADE-DUR-MM
            //                           :IND-ADE-DUR-MM
            //                          ,:ADE-DUR-GG
            //                           :IND-ADE-DUR-GG
            //                          ,:ADE-TP-RGM-FISC
            //                          ,:ADE-TP-RIAT
            //                           :IND-ADE-TP-RIAT
            //                          ,:ADE-TP-MOD-PAG-TIT
            //                          ,:ADE-TP-IAS
            //                           :IND-ADE-TP-IAS
            //                          ,:ADE-DT-VARZ-TP-IAS-DB
            //                           :IND-ADE-DT-VARZ-TP-IAS
            //                          ,:ADE-PRE-NET-IND
            //                           :IND-ADE-PRE-NET-IND
            //                          ,:ADE-PRE-LRD-IND
            //                           :IND-ADE-PRE-LRD-IND
            //                          ,:ADE-RAT-LRD-IND
            //                           :IND-ADE-RAT-LRD-IND
            //                          ,:ADE-PRSTZ-INI-IND
            //                           :IND-ADE-PRSTZ-INI-IND
            //                          ,:ADE-FL-COINC-ASSTO
            //                           :IND-ADE-FL-COINC-ASSTO
            //                          ,:ADE-IB-DFLT
            //                           :IND-ADE-IB-DFLT
            //                          ,:ADE-MOD-CALC
            //                           :IND-ADE-MOD-CALC
            //                          ,:ADE-TP-FNT-CNBTVA
            //                           :IND-ADE-TP-FNT-CNBTVA
            //                          ,:ADE-IMP-AZ
            //                           :IND-ADE-IMP-AZ
            //                          ,:ADE-IMP-ADER
            //                           :IND-ADE-IMP-ADER
            //                          ,:ADE-IMP-TFR
            //                           :IND-ADE-IMP-TFR
            //                          ,:ADE-IMP-VOLO
            //                           :IND-ADE-IMP-VOLO
            //                          ,:ADE-PC-AZ
            //                           :IND-ADE-PC-AZ
            //                          ,:ADE-PC-ADER
            //                           :IND-ADE-PC-ADER
            //                          ,:ADE-PC-TFR
            //                           :IND-ADE-PC-TFR
            //                          ,:ADE-PC-VOLO
            //                           :IND-ADE-PC-VOLO
            //                          ,:ADE-DT-NOVA-RGM-FISC-DB
            //                           :IND-ADE-DT-NOVA-RGM-FISC
            //                          ,:ADE-FL-ATTIV
            //                           :IND-ADE-FL-ATTIV
            //                          ,:ADE-IMP-REC-RIT-VIS
            //                           :IND-ADE-IMP-REC-RIT-VIS
            //                          ,:ADE-IMP-REC-RIT-ACC
            //                           :IND-ADE-IMP-REC-RIT-ACC
            //                          ,:ADE-FL-VARZ-STAT-TBGC
            //                           :IND-ADE-FL-VARZ-STAT-TBGC
            //                          ,:ADE-FL-PROVZA-MIGRAZ
            //                           :IND-ADE-FL-PROVZA-MIGRAZ
            //                          ,:ADE-IMPB-VIS-DA-REC
            //                           :IND-ADE-IMPB-VIS-DA-REC
            //                          ,:ADE-DT-DECOR-PREST-BAN-DB
            //                           :IND-ADE-DT-DECOR-PREST-BAN
            //                          ,:ADE-DT-EFF-VARZ-STAT-T-DB
            //                           :IND-ADE-DT-EFF-VARZ-STAT-T
            //                          ,:ADE-DS-RIGA
            //                          ,:ADE-DS-OPER-SQL
            //                          ,:ADE-DS-VER
            //                          ,:ADE-DS-TS-INI-CPTZ
            //                          ,:ADE-DS-TS-END-CPTZ
            //                          ,:ADE-DS-UTENTE
            //                          ,:ADE-DS-STATO-ELAB
            //                          ,:ADE-CUM-CNBT-CAP
            //                           :IND-ADE-CUM-CNBT-CAP
            //                          ,:ADE-IMP-GAR-CNBT
            //                           :IND-ADE-IMP-GAR-CNBT
            //                          ,:ADE-DT-ULT-CONS-CNBT-DB
            //                           :IND-ADE-DT-ULT-CONS-CNBT
            //                          ,:ADE-IDEN-ISC-FND
            //                           :IND-ADE-IDEN-ISC-FND
            //                          ,:ADE-NUM-RAT-PIAN
            //                           :IND-ADE-NUM-RAT-PIAN
            //                          ,:ADE-DT-PRESC-DB
            //                           :IND-ADE-DT-PRESC
            //                          ,:ADE-CONCS-PREST
            //                           :IND-ADE-CONCS-PREST
            //                          ,:POL-ID-POLI
            //                          ,:POL-ID-MOVI-CRZ
            //                          ,:POL-ID-MOVI-CHIU
            //                           :IND-POL-ID-MOVI-CHIU
            //                          ,:POL-IB-OGG
            //                           :IND-POL-IB-OGG
            //                          ,:POL-IB-PROP
            //                          ,:POL-DT-PROP-DB
            //                           :IND-POL-DT-PROP
            //                          ,:POL-DT-INI-EFF-DB
            //                          ,:POL-DT-END-EFF-DB
            //                          ,:POL-COD-COMP-ANIA
            //                          ,:POL-DT-DECOR-DB
            //                          ,:POL-DT-EMIS-DB
            //                          ,:POL-TP-POLI
            //                          ,:POL-DUR-AA
            //                           :IND-POL-DUR-AA
            //                          ,:POL-DUR-MM
            //                           :IND-POL-DUR-MM
            //                          ,:POL-DT-SCAD-DB
            //                           :IND-POL-DT-SCAD
            //                          ,:POL-COD-PROD
            //                          ,:POL-DT-INI-VLDT-PROD-DB
            //                          ,:POL-COD-CONV
            //                           :IND-POL-COD-CONV
            //                          ,:POL-COD-RAMO
            //                           :IND-POL-COD-RAMO
            //                          ,:POL-DT-INI-VLDT-CONV-DB
            //                           :IND-POL-DT-INI-VLDT-CONV
            //                          ,:POL-DT-APPLZ-CONV-DB
            //                           :IND-POL-DT-APPLZ-CONV
            //                          ,:POL-TP-FRM-ASSVA
            //                          ,:POL-TP-RGM-FISC
            //                           :IND-POL-TP-RGM-FISC
            //                          ,:POL-FL-ESTAS
            //                           :IND-POL-FL-ESTAS
            //                          ,:POL-FL-RSH-COMUN
            //                           :IND-POL-FL-RSH-COMUN
            //                          ,:POL-FL-RSH-COMUN-COND
            //                           :IND-POL-FL-RSH-COMUN-COND
            //                          ,:POL-TP-LIV-GENZ-TIT
            //                          ,:POL-FL-COP-FINANZ
            //                           :IND-POL-FL-COP-FINANZ
            //                          ,:POL-TP-APPLZ-DIR
            //                           :IND-POL-TP-APPLZ-DIR
            //                          ,:POL-SPE-MED
            //                           :IND-POL-SPE-MED
            //                          ,:POL-DIR-EMIS
            //                           :IND-POL-DIR-EMIS
            //                          ,:POL-DIR-1O-VERS
            //                           :IND-POL-DIR-1O-VERS
            //                          ,:POL-DIR-VERS-AGG
            //                           :IND-POL-DIR-VERS-AGG
            //                          ,:POL-COD-DVS
            //                           :IND-POL-COD-DVS
            //                          ,:POL-FL-FNT-AZ
            //                           :IND-POL-FL-FNT-AZ
            //                          ,:POL-FL-FNT-ADER
            //                           :IND-POL-FL-FNT-ADER
            //                          ,:POL-FL-FNT-TFR
            //                           :IND-POL-FL-FNT-TFR
            //                          ,:POL-FL-FNT-VOLO
            //                           :IND-POL-FL-FNT-VOLO
            //                          ,:POL-TP-OPZ-A-SCAD
            //                           :IND-POL-TP-OPZ-A-SCAD
            //                          ,:POL-AA-DIFF-PROR-DFLT
            //                           :IND-POL-AA-DIFF-PROR-DFLT
            //                          ,:POL-FL-VER-PROD
            //                           :IND-POL-FL-VER-PROD
            //                          ,:POL-DUR-GG
            //                           :IND-POL-DUR-GG
            //                          ,:POL-DIR-QUIET
            //                           :IND-POL-DIR-QUIET
            //                          ,:POL-TP-PTF-ESTNO
            //                           :IND-POL-TP-PTF-ESTNO
            //                          ,:POL-FL-CUM-PRE-CNTR
            //                           :IND-POL-FL-CUM-PRE-CNTR
            //                          ,:POL-FL-AMMB-MOVI
            //                           :IND-POL-FL-AMMB-MOVI
            //                          ,:POL-CONV-GECO
            //                           :IND-POL-CONV-GECO
            //                          ,:POL-DS-RIGA
            //                          ,:POL-DS-OPER-SQL
            //                          ,:POL-DS-VER
            //                          ,:POL-DS-TS-INI-CPTZ
            //                          ,:POL-DS-TS-END-CPTZ
            //                          ,:POL-DS-UTENTE
            //                          ,:POL-DS-STATO-ELAB
            //                          ,:POL-FL-SCUDO-FISC
            //                           :IND-POL-FL-SCUDO-FISC
            //                          ,:POL-FL-TRASFE
            //                           :IND-POL-FL-TRASFE
            //                          ,:POL-FL-TFR-STRC
            //                           :IND-POL-FL-TFR-STRC
            //                          ,:POL-DT-PRESC-DB
            //                           :IND-POL-DT-PRESC
            //                          ,:POL-COD-CONV-AGG
            //                           :IND-POL-COD-CONV-AGG
            //                          ,:POL-SUBCAT-PROD
            //                           :IND-POL-SUBCAT-PROD
            //                          ,:POL-FL-QUEST-ADEGZ-ASS
            //                           :IND-POL-FL-QUEST-ADEGZ-ASS
            //                          ,:POL-COD-TPA
            //                           :IND-POL-COD-TPA
            //                          ,:POL-ID-ACC-COMM
            //                           :IND-POL-ID-ACC-COMM
            //                          ,:POL-FL-POLI-CPI-PR
            //                           :IND-POL-FL-POLI-CPI-PR
            //                          ,:POL-FL-POLI-BUNDLING
            //                           :IND-POL-FL-POLI-BUNDLING
            //                          ,:POL-IND-POLI-PRIN-COLL
            //                           :IND-POL-IND-POLI-PRIN-COLL
            //                          ,:POL-FL-VND-BUNDLE
            //                           :IND-POL-FL-VND-BUNDLE
            //                          ,:POL-IB-BS
            //                           :IND-POL-IB-BS
            //                          ,:POL-FL-POLI-IFP
            //                           :IND-POL-FL-POLI-IFP
            //                          ,:STB-ID-STAT-OGG-BUS
            //                          ,:STB-ID-OGG
            //                          ,:STB-TP-OGG
            //                          ,:STB-ID-MOVI-CRZ
            //                          ,:STB-ID-MOVI-CHIU
            //                           :IND-STB-ID-MOVI-CHIU
            //                          ,:STB-DT-INI-EFF-DB
            //                          ,:STB-DT-END-EFF-DB
            //                          ,:STB-COD-COMP-ANIA
            //                          ,:STB-TP-STAT-BUS
            //                          ,:STB-TP-CAUS
            //                          ,:STB-DS-RIGA
            //                          ,:STB-DS-OPER-SQL
            //                          ,:STB-DS-VER
            //                          ,:STB-DS-TS-INI-CPTZ
            //                          ,:STB-DS-TS-END-CPTZ
            //                          ,:STB-DS-UTENTE
            //                          ,:STB-DS-STATO-ELAB
            //                     FROM ADES      A,
            //                          POLI      B,
            //                          STAT_OGG_BUS C
            //                     WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
            //                                                :WS-TP-FRM-ASSVA2 )
            //                       AND A.ID_POLI      =   B.ID_POLI
            //                       AND A.ID_ADES      =   C.ID_OGG
            //                       AND A.ID_POLI        BETWEEN   :IABV0009-ID-OGG-DA AND
            //                                                      :IABV0009-ID-OGG-A
            //                       AND C.TP_OGG       =  'AD'
            //           *--      AND C.TP_STAT_BUS     =  'VI'
            //                       AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
            //                       AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
            //                       AND A.DT_END_EFF  >   :WS-DT-PTF-X
            //                       AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                       AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                       AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
            //                       AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
            //                       AND B.DT_END_EFF  >   :WS-DT-PTF-X
            //                       AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                       AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                       AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
            //                       AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
            //                       AND C.DT_END_EFF  >   :WS-DT-PTF-X
            //                       AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                       AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                       AND A.ID_ADES IN
            //                          (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
            //                                                           STAT_OGG_BUS E
            //                           WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
            //                           AND E.TP_OGG = 'TG'
            //                           AND E.TP_STAT_BUS IN ('VI','ST')
            //                           AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
            //                           AND D.COD_COMP_ANIA =
            //                               :IDSV0003-CODICE-COMPAGNIA-ANIA
            //                           AND D.DT_INI_EFF <=   :WS-DT-PTF-X
            //                           AND D.DT_END_EFF >    :WS-DT-PTF-X
            //                           AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                           AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                           AND E.DT_INI_EFF <=   :WS-DT-PTF-X
            //                           AND E.DT_END_EFF >    :WS-DT-PTF-X
            //                           AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                           AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                           AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
            //                                OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
            //                           )
            //           *           AND A.DS_STATO_ELAB IN (
            //           *                                   :IABV0002-STATE-01,
            //           *                                   :IABV0002-STATE-02,
            //           *                                   :IABV0002-STATE-03,
            //           *                                   :IABV0002-STATE-04,
            //           *                                   :IABV0002-STATE-05,
            //           *                                   :IABV0002-STATE-06,
            //           *                                   :IABV0002-STATE-07,
            //           *                                   :IABV0002-STATE-08,
            //           *                                   :IABV0002-STATE-09,
            //           *                                   :IABV0002-STATE-10
            //           *                                     )
            //           *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
            //                       ORDER BY A.ID_POLI, A.ID_ADES
            //                      FETCH FIRST ROW ONLY
            //                   END-EXEC
            adesPoliDao.selectRec5(this.getAdesPoliLdbm0250());
        }
        else {
            //           AND A.DS_STATO_ELAB IN (
            //                                   :IABV0002-STATE-01,
            //                                   :IABV0002-STATE-02,
            //                                   :IABV0002-STATE-03,
            //                                   :IABV0002-STATE-04,
            //                                   :IABV0002-STATE-05,
            //                                   :IABV0002-STATE-06,
            //                                   :IABV0002-STATE-07,
            //                                   :IABV0002-STATE-08,
            //                                   :IABV0002-STATE-09,
            //                                   :IABV0002-STATE-10
            //                                     )
            //           AND NOT A.DS_VER  = :IABV0009-VERSIONING
            // COB_CODE:         EXEC SQL
            //                     SELECT
            //                            A.ID_ADES
            //                           ,A.ID_POLI
            //                           ,A.ID_MOVI_CRZ
            //                           ,A.ID_MOVI_CHIU
            //                           ,A.DT_INI_EFF
            //                           ,A.DT_END_EFF
            //                           ,A.IB_PREV
            //                           ,A.IB_OGG
            //                           ,A.COD_COMP_ANIA
            //                           ,A.DT_DECOR
            //                           ,A.DT_SCAD
            //                           ,A.ETA_A_SCAD
            //                           ,A.DUR_AA
            //                           ,A.DUR_MM
            //                           ,A.DUR_GG
            //                           ,A.TP_RGM_FISC
            //                           ,A.TP_RIAT
            //                           ,A.TP_MOD_PAG_TIT
            //                           ,A.TP_IAS
            //                           ,A.DT_VARZ_TP_IAS
            //                           ,A.PRE_NET_IND
            //                           ,A.PRE_LRD_IND
            //                           ,A.RAT_LRD_IND
            //                           ,A.PRSTZ_INI_IND
            //                           ,A.FL_COINC_ASSTO
            //                           ,A.IB_DFLT
            //                           ,A.MOD_CALC
            //                           ,A.TP_FNT_CNBTVA
            //                           ,A.IMP_AZ
            //                           ,A.IMP_ADER
            //                           ,A.IMP_TFR
            //                           ,A.IMP_VOLO
            //                           ,A.PC_AZ
            //                           ,A.PC_ADER
            //                           ,A.PC_TFR
            //                           ,A.PC_VOLO
            //                           ,A.DT_NOVA_RGM_FISC
            //                           ,A.FL_ATTIV
            //                           ,A.IMP_REC_RIT_VIS
            //                           ,A.IMP_REC_RIT_ACC
            //                           ,A.FL_VARZ_STAT_TBGC
            //                           ,A.FL_PROVZA_MIGRAZ
            //                           ,A.IMPB_VIS_DA_REC
            //                           ,A.DT_DECOR_PREST_BAN
            //                           ,A.DT_EFF_VARZ_STAT_T
            //                           ,A.DS_RIGA
            //                           ,A.DS_OPER_SQL
            //                           ,A.DS_VER
            //                           ,A.DS_TS_INI_CPTZ
            //                           ,A.DS_TS_END_CPTZ
            //                           ,A.DS_UTENTE
            //                           ,A.DS_STATO_ELAB
            //                           ,A.CUM_CNBT_CAP
            //                           ,A.IMP_GAR_CNBT
            //                           ,A.DT_ULT_CONS_CNBT
            //                           ,A.IDEN_ISC_FND
            //                           ,A.NUM_RAT_PIAN
            //                           ,A.DT_PRESC
            //                           ,A.CONCS_PREST
            //                           ,B.ID_POLI
            //                           ,B.ID_MOVI_CRZ
            //                           ,B.ID_MOVI_CHIU
            //                           ,B.IB_OGG
            //                           ,B.IB_PROP
            //                           ,B.DT_PROP
            //                           ,B.DT_INI_EFF
            //                           ,B.DT_END_EFF
            //                           ,B.COD_COMP_ANIA
            //                           ,B.DT_DECOR
            //                           ,B.DT_EMIS
            //                           ,B.TP_POLI
            //                           ,B.DUR_AA
            //                           ,B.DUR_MM
            //                           ,B.DT_SCAD
            //                           ,B.COD_PROD
            //                           ,B.DT_INI_VLDT_PROD
            //                           ,B.COD_CONV
            //                           ,B.COD_RAMO
            //                           ,B.DT_INI_VLDT_CONV
            //                           ,B.DT_APPLZ_CONV
            //                           ,B.TP_FRM_ASSVA
            //                           ,B.TP_RGM_FISC
            //                           ,B.FL_ESTAS
            //                           ,B.FL_RSH_COMUN
            //                           ,B.FL_RSH_COMUN_COND
            //                           ,B.TP_LIV_GENZ_TIT
            //                           ,B.FL_COP_FINANZ
            //                           ,B.TP_APPLZ_DIR
            //                           ,B.SPE_MED
            //                           ,B.DIR_EMIS
            //                           ,B.DIR_1O_VERS
            //                           ,B.DIR_VERS_AGG
            //                           ,B.COD_DVS
            //                           ,B.FL_FNT_AZ
            //                           ,B.FL_FNT_ADER
            //                           ,B.FL_FNT_TFR
            //                           ,B.FL_FNT_VOLO
            //                           ,B.TP_OPZ_A_SCAD
            //                           ,B.AA_DIFF_PROR_DFLT
            //                           ,B.FL_VER_PROD
            //                           ,B.DUR_GG
            //                           ,B.DIR_QUIET
            //                           ,B.TP_PTF_ESTNO
            //                           ,B.FL_CUM_PRE_CNTR
            //                           ,B.FL_AMMB_MOVI
            //                           ,B.CONV_GECO
            //                           ,B.DS_RIGA
            //                           ,B.DS_OPER_SQL
            //                           ,B.DS_VER
            //                           ,B.DS_TS_INI_CPTZ
            //                           ,B.DS_TS_END_CPTZ
            //                           ,B.DS_UTENTE
            //                           ,B.DS_STATO_ELAB
            //                           ,B.FL_SCUDO_FISC
            //                           ,B.FL_TRASFE
            //                           ,B.FL_TFR_STRC
            //                           ,B.DT_PRESC
            //                           ,B.COD_CONV_AGG
            //                           ,B.SUBCAT_PROD
            //                           ,B.FL_QUEST_ADEGZ_ASS
            //                           ,B.COD_TPA
            //                           ,B.ID_ACC_COMM
            //                           ,B.FL_POLI_CPI_PR
            //                           ,B.FL_POLI_BUNDLING
            //                           ,B.IND_POLI_PRIN_COLL
            //                           ,B.FL_VND_BUNDLE
            //                           ,B.IB_BS
            //                           ,B.FL_POLI_IFP
            //                           ,C.ID_STAT_OGG_BUS
            //                           ,C.ID_OGG
            //                           ,C.TP_OGG
            //                           ,C.ID_MOVI_CRZ
            //                           ,C.ID_MOVI_CHIU
            //                           ,C.DT_INI_EFF
            //                           ,C.DT_END_EFF
            //                           ,C.COD_COMP_ANIA
            //                           ,C.TP_STAT_BUS
            //                           ,C.TP_CAUS
            //                           ,C.DS_RIGA
            //                           ,C.DS_OPER_SQL
            //                           ,C.DS_VER
            //                           ,C.DS_TS_INI_CPTZ
            //                           ,C.DS_TS_END_CPTZ
            //                           ,C.DS_UTENTE
            //                           ,C.DS_STATO_ELAB
            //                     INTO
            //                           :ADE-ID-ADES
            //                          ,:ADE-ID-POLI
            //                          ,:ADE-ID-MOVI-CRZ
            //                          ,:ADE-ID-MOVI-CHIU
            //                           :IND-ADE-ID-MOVI-CHIU
            //                          ,:ADE-DT-INI-EFF-DB
            //                          ,:ADE-DT-END-EFF-DB
            //                          ,:ADE-IB-PREV
            //                           :IND-ADE-IB-PREV
            //                          ,:ADE-IB-OGG
            //                           :IND-ADE-IB-OGG
            //                          ,:ADE-COD-COMP-ANIA
            //                          ,:ADE-DT-DECOR-DB
            //                           :IND-ADE-DT-DECOR
            //                          ,:ADE-DT-SCAD-DB
            //                           :IND-ADE-DT-SCAD
            //                          ,:ADE-ETA-A-SCAD
            //                           :IND-ADE-ETA-A-SCAD
            //                          ,:ADE-DUR-AA
            //                           :IND-ADE-DUR-AA
            //                          ,:ADE-DUR-MM
            //                           :IND-ADE-DUR-MM
            //                          ,:ADE-DUR-GG
            //                           :IND-ADE-DUR-GG
            //                          ,:ADE-TP-RGM-FISC
            //                          ,:ADE-TP-RIAT
            //                           :IND-ADE-TP-RIAT
            //                          ,:ADE-TP-MOD-PAG-TIT
            //                          ,:ADE-TP-IAS
            //                           :IND-ADE-TP-IAS
            //                          ,:ADE-DT-VARZ-TP-IAS-DB
            //                           :IND-ADE-DT-VARZ-TP-IAS
            //                          ,:ADE-PRE-NET-IND
            //                           :IND-ADE-PRE-NET-IND
            //                          ,:ADE-PRE-LRD-IND
            //                           :IND-ADE-PRE-LRD-IND
            //                          ,:ADE-RAT-LRD-IND
            //                           :IND-ADE-RAT-LRD-IND
            //                          ,:ADE-PRSTZ-INI-IND
            //                           :IND-ADE-PRSTZ-INI-IND
            //                          ,:ADE-FL-COINC-ASSTO
            //                           :IND-ADE-FL-COINC-ASSTO
            //                          ,:ADE-IB-DFLT
            //                           :IND-ADE-IB-DFLT
            //                          ,:ADE-MOD-CALC
            //                           :IND-ADE-MOD-CALC
            //                          ,:ADE-TP-FNT-CNBTVA
            //                           :IND-ADE-TP-FNT-CNBTVA
            //                          ,:ADE-IMP-AZ
            //                           :IND-ADE-IMP-AZ
            //                          ,:ADE-IMP-ADER
            //                           :IND-ADE-IMP-ADER
            //                          ,:ADE-IMP-TFR
            //                           :IND-ADE-IMP-TFR
            //                          ,:ADE-IMP-VOLO
            //                           :IND-ADE-IMP-VOLO
            //                          ,:ADE-PC-AZ
            //                           :IND-ADE-PC-AZ
            //                          ,:ADE-PC-ADER
            //                           :IND-ADE-PC-ADER
            //                          ,:ADE-PC-TFR
            //                           :IND-ADE-PC-TFR
            //                          ,:ADE-PC-VOLO
            //                           :IND-ADE-PC-VOLO
            //                          ,:ADE-DT-NOVA-RGM-FISC-DB
            //                           :IND-ADE-DT-NOVA-RGM-FISC
            //                          ,:ADE-FL-ATTIV
            //                           :IND-ADE-FL-ATTIV
            //                          ,:ADE-IMP-REC-RIT-VIS
            //                           :IND-ADE-IMP-REC-RIT-VIS
            //                          ,:ADE-IMP-REC-RIT-ACC
            //                           :IND-ADE-IMP-REC-RIT-ACC
            //                          ,:ADE-FL-VARZ-STAT-TBGC
            //                           :IND-ADE-FL-VARZ-STAT-TBGC
            //                          ,:ADE-FL-PROVZA-MIGRAZ
            //                           :IND-ADE-FL-PROVZA-MIGRAZ
            //                          ,:ADE-IMPB-VIS-DA-REC
            //                           :IND-ADE-IMPB-VIS-DA-REC
            //                          ,:ADE-DT-DECOR-PREST-BAN-DB
            //                           :IND-ADE-DT-DECOR-PREST-BAN
            //                          ,:ADE-DT-EFF-VARZ-STAT-T-DB
            //                           :IND-ADE-DT-EFF-VARZ-STAT-T
            //                          ,:ADE-DS-RIGA
            //                          ,:ADE-DS-OPER-SQL
            //                          ,:ADE-DS-VER
            //                          ,:ADE-DS-TS-INI-CPTZ
            //                          ,:ADE-DS-TS-END-CPTZ
            //                          ,:ADE-DS-UTENTE
            //                          ,:ADE-DS-STATO-ELAB
            //                          ,:ADE-CUM-CNBT-CAP
            //                           :IND-ADE-CUM-CNBT-CAP
            //                          ,:ADE-IMP-GAR-CNBT
            //                           :IND-ADE-IMP-GAR-CNBT
            //                          ,:ADE-DT-ULT-CONS-CNBT-DB
            //                           :IND-ADE-DT-ULT-CONS-CNBT
            //                          ,:ADE-IDEN-ISC-FND
            //                           :IND-ADE-IDEN-ISC-FND
            //                          ,:ADE-NUM-RAT-PIAN
            //                           :IND-ADE-NUM-RAT-PIAN
            //                          ,:ADE-DT-PRESC-DB
            //                           :IND-ADE-DT-PRESC
            //                          ,:ADE-CONCS-PREST
            //                           :IND-ADE-CONCS-PREST
            //                          ,:POL-ID-POLI
            //                          ,:POL-ID-MOVI-CRZ
            //                          ,:POL-ID-MOVI-CHIU
            //                           :IND-POL-ID-MOVI-CHIU
            //                          ,:POL-IB-OGG
            //                           :IND-POL-IB-OGG
            //                          ,:POL-IB-PROP
            //                          ,:POL-DT-PROP-DB
            //                           :IND-POL-DT-PROP
            //                          ,:POL-DT-INI-EFF-DB
            //                          ,:POL-DT-END-EFF-DB
            //                          ,:POL-COD-COMP-ANIA
            //                          ,:POL-DT-DECOR-DB
            //                          ,:POL-DT-EMIS-DB
            //                          ,:POL-TP-POLI
            //                          ,:POL-DUR-AA
            //                           :IND-POL-DUR-AA
            //                          ,:POL-DUR-MM
            //                           :IND-POL-DUR-MM
            //                          ,:POL-DT-SCAD-DB
            //                           :IND-POL-DT-SCAD
            //                          ,:POL-COD-PROD
            //                          ,:POL-DT-INI-VLDT-PROD-DB
            //                          ,:POL-COD-CONV
            //                           :IND-POL-COD-CONV
            //                          ,:POL-COD-RAMO
            //                           :IND-POL-COD-RAMO
            //                          ,:POL-DT-INI-VLDT-CONV-DB
            //                           :IND-POL-DT-INI-VLDT-CONV
            //                          ,:POL-DT-APPLZ-CONV-DB
            //                           :IND-POL-DT-APPLZ-CONV
            //                          ,:POL-TP-FRM-ASSVA
            //                          ,:POL-TP-RGM-FISC
            //                           :IND-POL-TP-RGM-FISC
            //                          ,:POL-FL-ESTAS
            //                           :IND-POL-FL-ESTAS
            //                          ,:POL-FL-RSH-COMUN
            //                           :IND-POL-FL-RSH-COMUN
            //                          ,:POL-FL-RSH-COMUN-COND
            //                           :IND-POL-FL-RSH-COMUN-COND
            //                          ,:POL-TP-LIV-GENZ-TIT
            //                          ,:POL-FL-COP-FINANZ
            //                           :IND-POL-FL-COP-FINANZ
            //                          ,:POL-TP-APPLZ-DIR
            //                           :IND-POL-TP-APPLZ-DIR
            //                          ,:POL-SPE-MED
            //                           :IND-POL-SPE-MED
            //                          ,:POL-DIR-EMIS
            //                           :IND-POL-DIR-EMIS
            //                          ,:POL-DIR-1O-VERS
            //                           :IND-POL-DIR-1O-VERS
            //                          ,:POL-DIR-VERS-AGG
            //                           :IND-POL-DIR-VERS-AGG
            //                          ,:POL-COD-DVS
            //                           :IND-POL-COD-DVS
            //                          ,:POL-FL-FNT-AZ
            //                           :IND-POL-FL-FNT-AZ
            //                          ,:POL-FL-FNT-ADER
            //                           :IND-POL-FL-FNT-ADER
            //                          ,:POL-FL-FNT-TFR
            //                           :IND-POL-FL-FNT-TFR
            //                          ,:POL-FL-FNT-VOLO
            //                           :IND-POL-FL-FNT-VOLO
            //                          ,:POL-TP-OPZ-A-SCAD
            //                           :IND-POL-TP-OPZ-A-SCAD
            //                          ,:POL-AA-DIFF-PROR-DFLT
            //                           :IND-POL-AA-DIFF-PROR-DFLT
            //                          ,:POL-FL-VER-PROD
            //                           :IND-POL-FL-VER-PROD
            //                          ,:POL-DUR-GG
            //                           :IND-POL-DUR-GG
            //                          ,:POL-DIR-QUIET
            //                           :IND-POL-DIR-QUIET
            //                          ,:POL-TP-PTF-ESTNO
            //                           :IND-POL-TP-PTF-ESTNO
            //                          ,:POL-FL-CUM-PRE-CNTR
            //                           :IND-POL-FL-CUM-PRE-CNTR
            //                          ,:POL-FL-AMMB-MOVI
            //                           :IND-POL-FL-AMMB-MOVI
            //                          ,:POL-CONV-GECO
            //                           :IND-POL-CONV-GECO
            //                          ,:POL-DS-RIGA
            //                          ,:POL-DS-OPER-SQL
            //                          ,:POL-DS-VER
            //                          ,:POL-DS-TS-INI-CPTZ
            //                          ,:POL-DS-TS-END-CPTZ
            //                          ,:POL-DS-UTENTE
            //                          ,:POL-DS-STATO-ELAB
            //                          ,:POL-FL-SCUDO-FISC
            //                           :IND-POL-FL-SCUDO-FISC
            //                          ,:POL-FL-TRASFE
            //                           :IND-POL-FL-TRASFE
            //                          ,:POL-FL-TFR-STRC
            //                           :IND-POL-FL-TFR-STRC
            //                          ,:POL-DT-PRESC-DB
            //                           :IND-POL-DT-PRESC
            //                          ,:POL-COD-CONV-AGG
            //                           :IND-POL-COD-CONV-AGG
            //                          ,:POL-SUBCAT-PROD
            //                           :IND-POL-SUBCAT-PROD
            //                          ,:POL-FL-QUEST-ADEGZ-ASS
            //                           :IND-POL-FL-QUEST-ADEGZ-ASS
            //                          ,:POL-COD-TPA
            //                           :IND-POL-COD-TPA
            //                          ,:POL-ID-ACC-COMM
            //                           :IND-POL-ID-ACC-COMM
            //                          ,:POL-FL-POLI-CPI-PR
            //                           :IND-POL-FL-POLI-CPI-PR
            //                          ,:POL-FL-POLI-BUNDLING
            //                           :IND-POL-FL-POLI-BUNDLING
            //                          ,:POL-IND-POLI-PRIN-COLL
            //                           :IND-POL-IND-POLI-PRIN-COLL
            //                          ,:POL-FL-VND-BUNDLE
            //                           :IND-POL-FL-VND-BUNDLE
            //                          ,:POL-IB-BS
            //                           :IND-POL-IB-BS
            //                          ,:POL-FL-POLI-IFP
            //                           :IND-POL-FL-POLI-IFP
            //                          ,:STB-ID-STAT-OGG-BUS
            //                          ,:STB-ID-OGG
            //                          ,:STB-TP-OGG
            //                          ,:STB-ID-MOVI-CRZ
            //                          ,:STB-ID-MOVI-CHIU
            //                           :IND-STB-ID-MOVI-CHIU
            //                          ,:STB-DT-INI-EFF-DB
            //                          ,:STB-DT-END-EFF-DB
            //                          ,:STB-COD-COMP-ANIA
            //                          ,:STB-TP-STAT-BUS
            //                          ,:STB-TP-CAUS
            //                          ,:STB-DS-RIGA
            //                          ,:STB-DS-OPER-SQL
            //                          ,:STB-DS-VER
            //                          ,:STB-DS-TS-INI-CPTZ
            //                          ,:STB-DS-TS-END-CPTZ
            //                          ,:STB-DS-UTENTE
            //                          ,:STB-DS-STATO-ELAB
            //                     FROM ADES      A,
            //                          POLI      B,
            //                          STAT_OGG_BUS C
            //                     WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
            //                                                :WS-TP-FRM-ASSVA2 )
            //                       AND A.ID_POLI      =   B.ID_POLI
            //                       AND A.ID_ADES      =   C.ID_OGG
            //                       AND C.TP_OGG       =  'AD'
            //           *--      AND C.TP_STAT_BUS     =  'VI'
            //                       AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
            //                       AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
            //                       AND A.DT_END_EFF  >   :WS-DT-PTF-X
            //                       AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                       AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                       AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
            //                       AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
            //                       AND B.DT_END_EFF  >   :WS-DT-PTF-X
            //                       AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                       AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                       AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
            //                       AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
            //                       AND C.DT_END_EFF  >   :WS-DT-PTF-X
            //                       AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                       AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                       AND A.ID_ADES IN
            //                          (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
            //                                                           STAT_OGG_BUS E
            //                           WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
            //                           AND E.TP_OGG = 'TG'
            //                           AND E.TP_STAT_BUS IN ('VI','ST')
            //                           AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
            //                           AND D.COD_COMP_ANIA =
            //                               :IDSV0003-CODICE-COMPAGNIA-ANIA
            //                           AND D.DT_INI_EFF <=   :WS-DT-PTF-X
            //                           AND D.DT_END_EFF >    :WS-DT-PTF-X
            //                           AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                           AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                           AND E.DT_INI_EFF <=   :WS-DT-PTF-X
            //                           AND E.DT_END_EFF >    :WS-DT-PTF-X
            //                           AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
            //                           AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
            //                           AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
            //                                OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
            //                           )
            //           *           AND A.DS_STATO_ELAB IN (
            //           *                                   :IABV0002-STATE-01,
            //           *                                   :IABV0002-STATE-02,
            //           *                                   :IABV0002-STATE-03,
            //           *                                   :IABV0002-STATE-04,
            //           *                                   :IABV0002-STATE-05,
            //           *                                   :IABV0002-STATE-06,
            //           *                                   :IABV0002-STATE-07,
            //           *                                   :IABV0002-STATE-08,
            //           *                                   :IABV0002-STATE-09,
            //           *                                   :IABV0002-STATE-10
            //           *                                     )
            //           *           AND NOT A.DS_VER  = :IABV0009-VERSIONING
            //                       ORDER BY A.ID_POLI, A.ID_ADES
            //                      FETCH FIRST ROW ONLY
            //                   END-EXEC
            adesPoliDao.selectRec6(this.getAdesPoliLdbm0250());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A320-UPDATE-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320UpdateSc05() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK-SC05         THRU A330-SC05-EX
        //              WHEN IDSV0003-WHERE-CONDITION-05
        //                   PERFORM A340-UPDATE-WHERE-COND-SC05 THRU A340-SC05-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                       THRU A345-SC05-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
            // COB_CODE: PERFORM A330-UPDATE-PK-SC05         THRU A330-SC05-EX
            a330UpdatePkSc05();
        }
        else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition05()) {
            // COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC05 THRU A340-SC05-EX
            a340UpdateWhereCondSc05();
        }
        else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
            // COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC05
            //                                               THRU A345-SC05-EX
            a345UpdateFirstActionSc05();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidLevelOper();
        }
    }

    /**Original name: A330-UPDATE-PK-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePkSc05() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=6945, because the code is unreachable.
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
        z960LengthVchar();
        //           ,DS_VER                 = :IABV0009-VERSIONING
        // COB_CODE:      EXEC SQL
        //                     UPDATE ADES
        //                     SET
        //                        DS_OPER_SQL            = :ADE-DS-OPER-SQL
        //           *           ,DS_VER                 = :IABV0009-VERSIONING
        //                       ,DS_UTENTE              = :ADE-DS-UTENTE
        //                       ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
        //                     WHERE             DS_RIGA = :ADE-DS-RIGA
        //                END-EXEC.
        adesDao.updateRec2(ades.getAdeDsOperSql(), ades.getAdeDsUtente(), iabv0002.getIabv0002StateCurrent(), ades.getAdeDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A340-UPDATE-WHERE-COND-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateWhereCondSc05() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A345-UPDATE-FIRST-ACTION-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a345UpdateFirstActionSc05() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A360-OPEN-CURSOR-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursorSc05() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-SC05 THRU A305-SC05-EX.
        a305DeclareCursorSc05();
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              END-EXEC
        //           ELSE
        //              END-EXEC
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
            // COB_CODE: EXEC SQL
            //                OPEN CUR-SC05-RANGE
            //           END-EXEC
            adesPoliDao.openCurSc05Range(this);
        }
        else {
            // COB_CODE: EXEC SQL
            //                OPEN CUR-SC05
            //           END-EXEC
            adesPoliDao.openCurSc05(this);
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursorSc05() {
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              END-EXEC
        //           ELSE
        //              END-EXEC
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
            // COB_CODE: EXEC SQL
            //                CLOSE CUR-SC05-RANGE
            //           END-EXEC
            adesPoliDao.closeCurSc05Range();
        }
        else {
            // COB_CODE: EXEC SQL
            //                CLOSE CUR-SC05
            //           END-EXEC
            adesPoliDao.closeCurSc05();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirstSc05() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-SC05    THRU A360-SC05-EX.
        a360OpenCursorSc05();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT-SC05  THRU A390-SC05-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC05  THRU A390-SC05-EX
            a390FetchNextSc05();
        }
    }

    /**Original name: A390-FETCH-NEXT-SC05<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNextSc05() {
        // COB_CODE: IF ACCESSO-X-RANGE-SI
        //              END-EXEC
        //           ELSE
        //              END-EXEC
        //           END-IF.
        if (ws.getFlagAccessoXRange().isSi()) {
            // COB_CODE: EXEC SQL
            //                FETCH CUR-SC05-RANGE
            //           INTO
            //                   :ADE-ID-ADES
            //                  ,:ADE-ID-POLI
            //                  ,:ADE-ID-MOVI-CRZ
            //                  ,:ADE-ID-MOVI-CHIU
            //                   :IND-ADE-ID-MOVI-CHIU
            //                  ,:ADE-DT-INI-EFF-DB
            //                  ,:ADE-DT-END-EFF-DB
            //                  ,:ADE-IB-PREV
            //                   :IND-ADE-IB-PREV
            //                  ,:ADE-IB-OGG
            //                   :IND-ADE-IB-OGG
            //                  ,:ADE-COD-COMP-ANIA
            //                  ,:ADE-DT-DECOR-DB
            //                   :IND-ADE-DT-DECOR
            //                  ,:ADE-DT-SCAD-DB
            //                   :IND-ADE-DT-SCAD
            //                  ,:ADE-ETA-A-SCAD
            //                   :IND-ADE-ETA-A-SCAD
            //                  ,:ADE-DUR-AA
            //                   :IND-ADE-DUR-AA
            //                  ,:ADE-DUR-MM
            //                   :IND-ADE-DUR-MM
            //                  ,:ADE-DUR-GG
            //                   :IND-ADE-DUR-GG
            //                  ,:ADE-TP-RGM-FISC
            //                  ,:ADE-TP-RIAT
            //                   :IND-ADE-TP-RIAT
            //                  ,:ADE-TP-MOD-PAG-TIT
            //                  ,:ADE-TP-IAS
            //                   :IND-ADE-TP-IAS
            //                  ,:ADE-DT-VARZ-TP-IAS-DB
            //                   :IND-ADE-DT-VARZ-TP-IAS
            //                  ,:ADE-PRE-NET-IND
            //                   :IND-ADE-PRE-NET-IND
            //                  ,:ADE-PRE-LRD-IND
            //                   :IND-ADE-PRE-LRD-IND
            //                  ,:ADE-RAT-LRD-IND
            //                   :IND-ADE-RAT-LRD-IND
            //                  ,:ADE-PRSTZ-INI-IND
            //                   :IND-ADE-PRSTZ-INI-IND
            //                  ,:ADE-FL-COINC-ASSTO
            //                   :IND-ADE-FL-COINC-ASSTO
            //                  ,:ADE-IB-DFLT
            //                   :IND-ADE-IB-DFLT
            //                  ,:ADE-MOD-CALC
            //                   :IND-ADE-MOD-CALC
            //                  ,:ADE-TP-FNT-CNBTVA
            //                   :IND-ADE-TP-FNT-CNBTVA
            //                  ,:ADE-IMP-AZ
            //                   :IND-ADE-IMP-AZ
            //                  ,:ADE-IMP-ADER
            //                   :IND-ADE-IMP-ADER
            //                  ,:ADE-IMP-TFR
            //                   :IND-ADE-IMP-TFR
            //                  ,:ADE-IMP-VOLO
            //                   :IND-ADE-IMP-VOLO
            //                  ,:ADE-PC-AZ
            //                   :IND-ADE-PC-AZ
            //                  ,:ADE-PC-ADER
            //                   :IND-ADE-PC-ADER
            //                  ,:ADE-PC-TFR
            //                   :IND-ADE-PC-TFR
            //                  ,:ADE-PC-VOLO
            //                   :IND-ADE-PC-VOLO
            //                  ,:ADE-DT-NOVA-RGM-FISC-DB
            //                   :IND-ADE-DT-NOVA-RGM-FISC
            //                  ,:ADE-FL-ATTIV
            //                   :IND-ADE-FL-ATTIV
            //                  ,:ADE-IMP-REC-RIT-VIS
            //                   :IND-ADE-IMP-REC-RIT-VIS
            //                  ,:ADE-IMP-REC-RIT-ACC
            //                   :IND-ADE-IMP-REC-RIT-ACC
            //                  ,:ADE-FL-VARZ-STAT-TBGC
            //                   :IND-ADE-FL-VARZ-STAT-TBGC
            //                  ,:ADE-FL-PROVZA-MIGRAZ
            //                   :IND-ADE-FL-PROVZA-MIGRAZ
            //                  ,:ADE-IMPB-VIS-DA-REC
            //                   :IND-ADE-IMPB-VIS-DA-REC
            //                  ,:ADE-DT-DECOR-PREST-BAN-DB
            //                   :IND-ADE-DT-DECOR-PREST-BAN
            //                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
            //                   :IND-ADE-DT-EFF-VARZ-STAT-T
            //                  ,:ADE-DS-RIGA
            //                  ,:ADE-DS-OPER-SQL
            //                  ,:ADE-DS-VER
            //                  ,:ADE-DS-TS-INI-CPTZ
            //                  ,:ADE-DS-TS-END-CPTZ
            //                  ,:ADE-DS-UTENTE
            //                  ,:ADE-DS-STATO-ELAB
            //                  ,:ADE-CUM-CNBT-CAP
            //                   :IND-ADE-CUM-CNBT-CAP
            //                  ,:ADE-IMP-GAR-CNBT
            //                   :IND-ADE-IMP-GAR-CNBT
            //                  ,:ADE-DT-ULT-CONS-CNBT-DB
            //                   :IND-ADE-DT-ULT-CONS-CNBT
            //                  ,:ADE-IDEN-ISC-FND
            //                   :IND-ADE-IDEN-ISC-FND
            //                  ,:ADE-NUM-RAT-PIAN
            //                   :IND-ADE-NUM-RAT-PIAN
            //                  ,:ADE-DT-PRESC-DB
            //                   :IND-ADE-DT-PRESC
            //                  ,:ADE-CONCS-PREST
            //                   :IND-ADE-CONCS-PREST
            //                  ,:POL-ID-POLI
            //                  ,:POL-ID-MOVI-CRZ
            //                  ,:POL-ID-MOVI-CHIU
            //                   :IND-POL-ID-MOVI-CHIU
            //                  ,:POL-IB-OGG
            //                   :IND-POL-IB-OGG
            //                  ,:POL-IB-PROP
            //                  ,:POL-DT-PROP-DB
            //                   :IND-POL-DT-PROP
            //                  ,:POL-DT-INI-EFF-DB
            //                  ,:POL-DT-END-EFF-DB
            //                  ,:POL-COD-COMP-ANIA
            //                  ,:POL-DT-DECOR-DB
            //                  ,:POL-DT-EMIS-DB
            //                  ,:POL-TP-POLI
            //                  ,:POL-DUR-AA
            //                   :IND-POL-DUR-AA
            //                  ,:POL-DUR-MM
            //                   :IND-POL-DUR-MM
            //                  ,:POL-DT-SCAD-DB
            //                   :IND-POL-DT-SCAD
            //                  ,:POL-COD-PROD
            //                  ,:POL-DT-INI-VLDT-PROD-DB
            //                  ,:POL-COD-CONV
            //                   :IND-POL-COD-CONV
            //                  ,:POL-COD-RAMO
            //                   :IND-POL-COD-RAMO
            //                  ,:POL-DT-INI-VLDT-CONV-DB
            //                   :IND-POL-DT-INI-VLDT-CONV
            //                  ,:POL-DT-APPLZ-CONV-DB
            //                   :IND-POL-DT-APPLZ-CONV
            //                  ,:POL-TP-FRM-ASSVA
            //                  ,:POL-TP-RGM-FISC
            //                   :IND-POL-TP-RGM-FISC
            //                  ,:POL-FL-ESTAS
            //                   :IND-POL-FL-ESTAS
            //                  ,:POL-FL-RSH-COMUN
            //                   :IND-POL-FL-RSH-COMUN
            //                  ,:POL-FL-RSH-COMUN-COND
            //                   :IND-POL-FL-RSH-COMUN-COND
            //                  ,:POL-TP-LIV-GENZ-TIT
            //                  ,:POL-FL-COP-FINANZ
            //                   :IND-POL-FL-COP-FINANZ
            //                  ,:POL-TP-APPLZ-DIR
            //                   :IND-POL-TP-APPLZ-DIR
            //                  ,:POL-SPE-MED
            //                   :IND-POL-SPE-MED
            //                  ,:POL-DIR-EMIS
            //                   :IND-POL-DIR-EMIS
            //                  ,:POL-DIR-1O-VERS
            //                   :IND-POL-DIR-1O-VERS
            //                  ,:POL-DIR-VERS-AGG
            //                   :IND-POL-DIR-VERS-AGG
            //                  ,:POL-COD-DVS
            //                   :IND-POL-COD-DVS
            //                  ,:POL-FL-FNT-AZ
            //                   :IND-POL-FL-FNT-AZ
            //                  ,:POL-FL-FNT-ADER
            //                   :IND-POL-FL-FNT-ADER
            //                  ,:POL-FL-FNT-TFR
            //                   :IND-POL-FL-FNT-TFR
            //                  ,:POL-FL-FNT-VOLO
            //                   :IND-POL-FL-FNT-VOLO
            //                  ,:POL-TP-OPZ-A-SCAD
            //                   :IND-POL-TP-OPZ-A-SCAD
            //                  ,:POL-AA-DIFF-PROR-DFLT
            //                   :IND-POL-AA-DIFF-PROR-DFLT
            //                  ,:POL-FL-VER-PROD
            //                   :IND-POL-FL-VER-PROD
            //                  ,:POL-DUR-GG
            //                   :IND-POL-DUR-GG
            //                  ,:POL-DIR-QUIET
            //                   :IND-POL-DIR-QUIET
            //                  ,:POL-TP-PTF-ESTNO
            //                   :IND-POL-TP-PTF-ESTNO
            //                  ,:POL-FL-CUM-PRE-CNTR
            //                   :IND-POL-FL-CUM-PRE-CNTR
            //                  ,:POL-FL-AMMB-MOVI
            //                   :IND-POL-FL-AMMB-MOVI
            //                  ,:POL-CONV-GECO
            //                   :IND-POL-CONV-GECO
            //                  ,:POL-DS-RIGA
            //                  ,:POL-DS-OPER-SQL
            //                  ,:POL-DS-VER
            //                  ,:POL-DS-TS-INI-CPTZ
            //                  ,:POL-DS-TS-END-CPTZ
            //                  ,:POL-DS-UTENTE
            //                  ,:POL-DS-STATO-ELAB
            //                  ,:POL-FL-SCUDO-FISC
            //                   :IND-POL-FL-SCUDO-FISC
            //                  ,:POL-FL-TRASFE
            //                   :IND-POL-FL-TRASFE
            //                  ,:POL-FL-TFR-STRC
            //                   :IND-POL-FL-TFR-STRC
            //                  ,:POL-DT-PRESC-DB
            //                   :IND-POL-DT-PRESC
            //                  ,:POL-COD-CONV-AGG
            //                   :IND-POL-COD-CONV-AGG
            //                  ,:POL-SUBCAT-PROD
            //                   :IND-POL-SUBCAT-PROD
            //                  ,:POL-FL-QUEST-ADEGZ-ASS
            //                   :IND-POL-FL-QUEST-ADEGZ-ASS
            //                  ,:POL-COD-TPA
            //                   :IND-POL-COD-TPA
            //                  ,:POL-ID-ACC-COMM
            //                   :IND-POL-ID-ACC-COMM
            //                  ,:POL-FL-POLI-CPI-PR
            //                   :IND-POL-FL-POLI-CPI-PR
            //                  ,:POL-FL-POLI-BUNDLING
            //                   :IND-POL-FL-POLI-BUNDLING
            //                  ,:POL-IND-POLI-PRIN-COLL
            //                   :IND-POL-IND-POLI-PRIN-COLL
            //                  ,:POL-FL-VND-BUNDLE
            //                   :IND-POL-FL-VND-BUNDLE
            //                  ,:POL-IB-BS
            //                   :IND-POL-IB-BS
            //                  ,:POL-FL-POLI-IFP
            //                   :IND-POL-FL-POLI-IFP
            //                  ,:STB-ID-STAT-OGG-BUS
            //                  ,:STB-ID-OGG
            //                  ,:STB-TP-OGG
            //                  ,:STB-ID-MOVI-CRZ
            //                  ,:STB-ID-MOVI-CHIU
            //                   :IND-STB-ID-MOVI-CHIU
            //                  ,:STB-DT-INI-EFF-DB
            //                  ,:STB-DT-END-EFF-DB
            //                  ,:STB-COD-COMP-ANIA
            //                  ,:STB-TP-STAT-BUS
            //                  ,:STB-TP-CAUS
            //                  ,:STB-DS-RIGA
            //                  ,:STB-DS-OPER-SQL
            //                  ,:STB-DS-VER
            //                  ,:STB-DS-TS-INI-CPTZ
            //                  ,:STB-DS-TS-END-CPTZ
            //                  ,:STB-DS-UTENTE
            //                  ,:STB-DS-STATO-ELAB
            //           END-EXEC
            adesPoliDao.fetchCurSc05Range(this.getAdesPoliLdbm0250());
        }
        else {
            // COB_CODE: EXEC SQL
            //                FETCH CUR-SC05
            //           INTO
            //                   :ADE-ID-ADES
            //                  ,:ADE-ID-POLI
            //                  ,:ADE-ID-MOVI-CRZ
            //                  ,:ADE-ID-MOVI-CHIU
            //                   :IND-ADE-ID-MOVI-CHIU
            //                  ,:ADE-DT-INI-EFF-DB
            //                  ,:ADE-DT-END-EFF-DB
            //                  ,:ADE-IB-PREV
            //                   :IND-ADE-IB-PREV
            //                  ,:ADE-IB-OGG
            //                   :IND-ADE-IB-OGG
            //                  ,:ADE-COD-COMP-ANIA
            //                  ,:ADE-DT-DECOR-DB
            //                   :IND-ADE-DT-DECOR
            //                  ,:ADE-DT-SCAD-DB
            //                   :IND-ADE-DT-SCAD
            //                  ,:ADE-ETA-A-SCAD
            //                   :IND-ADE-ETA-A-SCAD
            //                  ,:ADE-DUR-AA
            //                   :IND-ADE-DUR-AA
            //                  ,:ADE-DUR-MM
            //                   :IND-ADE-DUR-MM
            //                  ,:ADE-DUR-GG
            //                   :IND-ADE-DUR-GG
            //                  ,:ADE-TP-RGM-FISC
            //                  ,:ADE-TP-RIAT
            //                   :IND-ADE-TP-RIAT
            //                  ,:ADE-TP-MOD-PAG-TIT
            //                  ,:ADE-TP-IAS
            //                   :IND-ADE-TP-IAS
            //                  ,:ADE-DT-VARZ-TP-IAS-DB
            //                   :IND-ADE-DT-VARZ-TP-IAS
            //                  ,:ADE-PRE-NET-IND
            //                   :IND-ADE-PRE-NET-IND
            //                  ,:ADE-PRE-LRD-IND
            //                   :IND-ADE-PRE-LRD-IND
            //                  ,:ADE-RAT-LRD-IND
            //                   :IND-ADE-RAT-LRD-IND
            //                  ,:ADE-PRSTZ-INI-IND
            //                   :IND-ADE-PRSTZ-INI-IND
            //                  ,:ADE-FL-COINC-ASSTO
            //                   :IND-ADE-FL-COINC-ASSTO
            //                  ,:ADE-IB-DFLT
            //                   :IND-ADE-IB-DFLT
            //                  ,:ADE-MOD-CALC
            //                   :IND-ADE-MOD-CALC
            //                  ,:ADE-TP-FNT-CNBTVA
            //                   :IND-ADE-TP-FNT-CNBTVA
            //                  ,:ADE-IMP-AZ
            //                   :IND-ADE-IMP-AZ
            //                  ,:ADE-IMP-ADER
            //                   :IND-ADE-IMP-ADER
            //                  ,:ADE-IMP-TFR
            //                   :IND-ADE-IMP-TFR
            //                  ,:ADE-IMP-VOLO
            //                   :IND-ADE-IMP-VOLO
            //                  ,:ADE-PC-AZ
            //                   :IND-ADE-PC-AZ
            //                  ,:ADE-PC-ADER
            //                   :IND-ADE-PC-ADER
            //                  ,:ADE-PC-TFR
            //                   :IND-ADE-PC-TFR
            //                  ,:ADE-PC-VOLO
            //                   :IND-ADE-PC-VOLO
            //                  ,:ADE-DT-NOVA-RGM-FISC-DB
            //                   :IND-ADE-DT-NOVA-RGM-FISC
            //                  ,:ADE-FL-ATTIV
            //                   :IND-ADE-FL-ATTIV
            //                  ,:ADE-IMP-REC-RIT-VIS
            //                   :IND-ADE-IMP-REC-RIT-VIS
            //                  ,:ADE-IMP-REC-RIT-ACC
            //                   :IND-ADE-IMP-REC-RIT-ACC
            //                  ,:ADE-FL-VARZ-STAT-TBGC
            //                   :IND-ADE-FL-VARZ-STAT-TBGC
            //                  ,:ADE-FL-PROVZA-MIGRAZ
            //                   :IND-ADE-FL-PROVZA-MIGRAZ
            //                  ,:ADE-IMPB-VIS-DA-REC
            //                   :IND-ADE-IMPB-VIS-DA-REC
            //                  ,:ADE-DT-DECOR-PREST-BAN-DB
            //                   :IND-ADE-DT-DECOR-PREST-BAN
            //                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
            //                   :IND-ADE-DT-EFF-VARZ-STAT-T
            //                  ,:ADE-DS-RIGA
            //                  ,:ADE-DS-OPER-SQL
            //                  ,:ADE-DS-VER
            //                  ,:ADE-DS-TS-INI-CPTZ
            //                  ,:ADE-DS-TS-END-CPTZ
            //                  ,:ADE-DS-UTENTE
            //                  ,:ADE-DS-STATO-ELAB
            //                  ,:ADE-CUM-CNBT-CAP
            //                   :IND-ADE-CUM-CNBT-CAP
            //                  ,:ADE-IMP-GAR-CNBT
            //                   :IND-ADE-IMP-GAR-CNBT
            //                  ,:ADE-DT-ULT-CONS-CNBT-DB
            //                   :IND-ADE-DT-ULT-CONS-CNBT
            //                  ,:ADE-IDEN-ISC-FND
            //                   :IND-ADE-IDEN-ISC-FND
            //                  ,:ADE-NUM-RAT-PIAN
            //                   :IND-ADE-NUM-RAT-PIAN
            //                  ,:ADE-DT-PRESC-DB
            //                   :IND-ADE-DT-PRESC
            //                  ,:ADE-CONCS-PREST
            //                   :IND-ADE-CONCS-PREST
            //                  ,:POL-ID-POLI
            //                  ,:POL-ID-MOVI-CRZ
            //                  ,:POL-ID-MOVI-CHIU
            //                   :IND-POL-ID-MOVI-CHIU
            //                  ,:POL-IB-OGG
            //                   :IND-POL-IB-OGG
            //                  ,:POL-IB-PROP
            //                  ,:POL-DT-PROP-DB
            //                   :IND-POL-DT-PROP
            //                  ,:POL-DT-INI-EFF-DB
            //                  ,:POL-DT-END-EFF-DB
            //                  ,:POL-COD-COMP-ANIA
            //                  ,:POL-DT-DECOR-DB
            //                  ,:POL-DT-EMIS-DB
            //                  ,:POL-TP-POLI
            //                  ,:POL-DUR-AA
            //                   :IND-POL-DUR-AA
            //                  ,:POL-DUR-MM
            //                   :IND-POL-DUR-MM
            //                  ,:POL-DT-SCAD-DB
            //                   :IND-POL-DT-SCAD
            //                  ,:POL-COD-PROD
            //                  ,:POL-DT-INI-VLDT-PROD-DB
            //                  ,:POL-COD-CONV
            //                   :IND-POL-COD-CONV
            //                  ,:POL-COD-RAMO
            //                   :IND-POL-COD-RAMO
            //                  ,:POL-DT-INI-VLDT-CONV-DB
            //                   :IND-POL-DT-INI-VLDT-CONV
            //                  ,:POL-DT-APPLZ-CONV-DB
            //                   :IND-POL-DT-APPLZ-CONV
            //                  ,:POL-TP-FRM-ASSVA
            //                  ,:POL-TP-RGM-FISC
            //                   :IND-POL-TP-RGM-FISC
            //                  ,:POL-FL-ESTAS
            //                   :IND-POL-FL-ESTAS
            //                  ,:POL-FL-RSH-COMUN
            //                   :IND-POL-FL-RSH-COMUN
            //                  ,:POL-FL-RSH-COMUN-COND
            //                   :IND-POL-FL-RSH-COMUN-COND
            //                  ,:POL-TP-LIV-GENZ-TIT
            //                  ,:POL-FL-COP-FINANZ
            //                   :IND-POL-FL-COP-FINANZ
            //                  ,:POL-TP-APPLZ-DIR
            //                   :IND-POL-TP-APPLZ-DIR
            //                  ,:POL-SPE-MED
            //                   :IND-POL-SPE-MED
            //                  ,:POL-DIR-EMIS
            //                   :IND-POL-DIR-EMIS
            //                  ,:POL-DIR-1O-VERS
            //                   :IND-POL-DIR-1O-VERS
            //                  ,:POL-DIR-VERS-AGG
            //                   :IND-POL-DIR-VERS-AGG
            //                  ,:POL-COD-DVS
            //                   :IND-POL-COD-DVS
            //                  ,:POL-FL-FNT-AZ
            //                   :IND-POL-FL-FNT-AZ
            //                  ,:POL-FL-FNT-ADER
            //                   :IND-POL-FL-FNT-ADER
            //                  ,:POL-FL-FNT-TFR
            //                   :IND-POL-FL-FNT-TFR
            //                  ,:POL-FL-FNT-VOLO
            //                   :IND-POL-FL-FNT-VOLO
            //                  ,:POL-TP-OPZ-A-SCAD
            //                   :IND-POL-TP-OPZ-A-SCAD
            //                  ,:POL-AA-DIFF-PROR-DFLT
            //                   :IND-POL-AA-DIFF-PROR-DFLT
            //                  ,:POL-FL-VER-PROD
            //                   :IND-POL-FL-VER-PROD
            //                  ,:POL-DUR-GG
            //                   :IND-POL-DUR-GG
            //                  ,:POL-DIR-QUIET
            //                   :IND-POL-DIR-QUIET
            //                  ,:POL-TP-PTF-ESTNO
            //                   :IND-POL-TP-PTF-ESTNO
            //                  ,:POL-FL-CUM-PRE-CNTR
            //                   :IND-POL-FL-CUM-PRE-CNTR
            //                  ,:POL-FL-AMMB-MOVI
            //                   :IND-POL-FL-AMMB-MOVI
            //                  ,:POL-CONV-GECO
            //                   :IND-POL-CONV-GECO
            //                  ,:POL-DS-RIGA
            //                  ,:POL-DS-OPER-SQL
            //                  ,:POL-DS-VER
            //                  ,:POL-DS-TS-INI-CPTZ
            //                  ,:POL-DS-TS-END-CPTZ
            //                  ,:POL-DS-UTENTE
            //                  ,:POL-DS-STATO-ELAB
            //                  ,:POL-FL-SCUDO-FISC
            //                   :IND-POL-FL-SCUDO-FISC
            //                  ,:POL-FL-TRASFE
            //                   :IND-POL-FL-TRASFE
            //                  ,:POL-FL-TFR-STRC
            //                   :IND-POL-FL-TFR-STRC
            //                  ,:POL-DT-PRESC-DB
            //                   :IND-POL-DT-PRESC
            //                  ,:POL-COD-CONV-AGG
            //                   :IND-POL-COD-CONV-AGG
            //                  ,:POL-SUBCAT-PROD
            //                   :IND-POL-SUBCAT-PROD
            //                  ,:POL-FL-QUEST-ADEGZ-ASS
            //                   :IND-POL-FL-QUEST-ADEGZ-ASS
            //                  ,:POL-COD-TPA
            //                   :IND-POL-COD-TPA
            //                  ,:POL-ID-ACC-COMM
            //                   :IND-POL-ID-ACC-COMM
            //                  ,:POL-FL-POLI-CPI-PR
            //                   :IND-POL-FL-POLI-CPI-PR
            //                  ,:POL-FL-POLI-BUNDLING
            //                   :IND-POL-FL-POLI-BUNDLING
            //                  ,:POL-IND-POLI-PRIN-COLL
            //                   :IND-POL-IND-POLI-PRIN-COLL
            //                  ,:POL-FL-VND-BUNDLE
            //                   :IND-POL-FL-VND-BUNDLE
            //                  ,:POL-IB-BS
            //                   :IND-POL-IB-BS
            //                  ,:POL-FL-POLI-IFP
            //                   :IND-POL-FL-POLI-IFP
            //                  ,:STB-ID-STAT-OGG-BUS
            //                  ,:STB-ID-OGG
            //                  ,:STB-TP-OGG
            //                  ,:STB-ID-MOVI-CRZ
            //                  ,:STB-ID-MOVI-CHIU
            //                   :IND-STB-ID-MOVI-CHIU
            //                  ,:STB-DT-INI-EFF-DB
            //                  ,:STB-DT-END-EFF-DB
            //                  ,:STB-COD-COMP-ANIA
            //                  ,:STB-TP-STAT-BUS
            //                  ,:STB-TP-CAUS
            //                  ,:STB-DS-RIGA
            //                  ,:STB-DS-OPER-SQL
            //                  ,:STB-DS-VER
            //                  ,:STB-DS-TS-INI-CPTZ
            //                  ,:STB-DS-TS-END-CPTZ
            //                  ,:STB-DS-UTENTE
            //                  ,:STB-DS-STATO-ELAB
            //           END-EXEC
            adesPoliDao.fetchCurSc05(this.getAdesPoliLdbm0250());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC05 THRU A370-SC05-EX
            a370CloseCursorSc05();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: SC06-SELECTION-CURSOR-06<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
    private void sc06SelectionCursor06() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC06           THRU A310-SC06-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-SC06      THRU A360-SC06-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-SC06     THRU A370-SC06-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-SC06      THRU A380-SC06-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-SC06       THRU A390-SC06-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE-SC06           THRU A320-SC06-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER          TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC06           THRU A310-SC06-EX
            a310SelectSc06();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-SC06      THRU A360-SC06-EX
            a360OpenCursorSc06();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC06     THRU A370-SC06-EX
            a370CloseCursorSc06();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-SC06      THRU A380-SC06-EX
            a380FetchFirstSc06();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC06       THRU A390-SC06-EX
            a390FetchNextSc06();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE-SC06           THRU A320-SC06-EX
            a320UpdateSc06();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursorSc06() {
    // COB_CODE:      EXEC SQL
    //                     DECLARE CUR-SC06 CURSOR WITH HOLD FOR
    //                  SELECT
    //                         A.ID_ADES
    //                        ,A.ID_POLI
    //                        ,A.ID_MOVI_CRZ
    //                        ,A.ID_MOVI_CHIU
    //                        ,A.DT_INI_EFF
    //                        ,A.DT_END_EFF
    //                        ,A.IB_PREV
    //                        ,A.IB_OGG
    //                        ,A.COD_COMP_ANIA
    //                        ,A.DT_DECOR
    //                        ,A.DT_SCAD
    //                        ,A.ETA_A_SCAD
    //                        ,A.DUR_AA
    //                        ,A.DUR_MM
    //                        ,A.DUR_GG
    //                        ,A.TP_RGM_FISC
    //                        ,A.TP_RIAT
    //                        ,A.TP_MOD_PAG_TIT
    //                        ,A.TP_IAS
    //                        ,A.DT_VARZ_TP_IAS
    //                        ,A.PRE_NET_IND
    //                        ,A.PRE_LRD_IND
    //                        ,A.RAT_LRD_IND
    //                        ,A.PRSTZ_INI_IND
    //                        ,A.FL_COINC_ASSTO
    //                        ,A.IB_DFLT
    //                        ,A.MOD_CALC
    //                        ,A.TP_FNT_CNBTVA
    //                        ,A.IMP_AZ
    //                        ,A.IMP_ADER
    //                        ,A.IMP_TFR
    //                        ,A.IMP_VOLO
    //                        ,A.PC_AZ
    //                        ,A.PC_ADER
    //                        ,A.PC_TFR
    //                        ,A.PC_VOLO
    //                        ,A.DT_NOVA_RGM_FISC
    //                        ,A.FL_ATTIV
    //                        ,A.IMP_REC_RIT_VIS
    //                        ,A.IMP_REC_RIT_ACC
    //                        ,A.FL_VARZ_STAT_TBGC
    //                        ,A.FL_PROVZA_MIGRAZ
    //                        ,A.IMPB_VIS_DA_REC
    //                        ,A.DT_DECOR_PREST_BAN
    //                        ,A.DT_EFF_VARZ_STAT_T
    //                        ,A.DS_RIGA
    //                        ,A.DS_OPER_SQL
    //                        ,A.DS_VER
    //                        ,A.DS_TS_INI_CPTZ
    //                        ,A.DS_TS_END_CPTZ
    //                        ,A.DS_UTENTE
    //                        ,A.DS_STATO_ELAB
    //                        ,A.CUM_CNBT_CAP
    //                        ,A.IMP_GAR_CNBT
    //                        ,A.DT_ULT_CONS_CNBT
    //                        ,A.IDEN_ISC_FND
    //                        ,A.NUM_RAT_PIAN
    //                        ,A.DT_PRESC
    //                        ,A.CONCS_PREST
    //                        ,B.ID_POLI
    //                        ,B.ID_MOVI_CRZ
    //                        ,B.ID_MOVI_CHIU
    //                        ,B.IB_OGG
    //                        ,B.IB_PROP
    //                        ,B.DT_PROP
    //                        ,B.DT_INI_EFF
    //                        ,B.DT_END_EFF
    //                        ,B.COD_COMP_ANIA
    //                        ,B.DT_DECOR
    //                        ,B.DT_EMIS
    //                        ,B.TP_POLI
    //                        ,B.DUR_AA
    //                        ,B.DUR_MM
    //                        ,B.DT_SCAD
    //                        ,B.COD_PROD
    //                        ,B.DT_INI_VLDT_PROD
    //                        ,B.COD_CONV
    //                        ,B.COD_RAMO
    //                        ,B.DT_INI_VLDT_CONV
    //                        ,B.DT_APPLZ_CONV
    //                        ,B.TP_FRM_ASSVA
    //                        ,B.TP_RGM_FISC
    //                        ,B.FL_ESTAS
    //                        ,B.FL_RSH_COMUN
    //                        ,B.FL_RSH_COMUN_COND
    //                        ,B.TP_LIV_GENZ_TIT
    //                        ,B.FL_COP_FINANZ
    //                        ,B.TP_APPLZ_DIR
    //                        ,B.SPE_MED
    //                        ,B.DIR_EMIS
    //                        ,B.DIR_1O_VERS
    //                        ,B.DIR_VERS_AGG
    //                        ,B.COD_DVS
    //                        ,B.FL_FNT_AZ
    //                        ,B.FL_FNT_ADER
    //                        ,B.FL_FNT_TFR
    //                        ,B.FL_FNT_VOLO
    //                        ,B.TP_OPZ_A_SCAD
    //                        ,B.AA_DIFF_PROR_DFLT
    //                        ,B.FL_VER_PROD
    //                        ,B.DUR_GG
    //                        ,B.DIR_QUIET
    //                        ,B.TP_PTF_ESTNO
    //                        ,B.FL_CUM_PRE_CNTR
    //                        ,B.FL_AMMB_MOVI
    //                        ,B.CONV_GECO
    //                        ,B.DS_RIGA
    //                        ,B.DS_OPER_SQL
    //                        ,B.DS_VER
    //                        ,B.DS_TS_INI_CPTZ
    //                        ,B.DS_TS_END_CPTZ
    //                        ,B.DS_UTENTE
    //                        ,B.DS_STATO_ELAB
    //                        ,B.FL_SCUDO_FISC
    //                        ,B.FL_TRASFE
    //                        ,B.FL_TFR_STRC
    //                        ,B.DT_PRESC
    //                        ,B.COD_CONV_AGG
    //                        ,B.SUBCAT_PROD
    //                        ,B.FL_QUEST_ADEGZ_ASS
    //                        ,B.COD_TPA
    //                        ,B.ID_ACC_COMM
    //                        ,B.FL_POLI_CPI_PR
    //                        ,B.FL_POLI_BUNDLING
    //                        ,B.IND_POLI_PRIN_COLL
    //                        ,B.FL_VND_BUNDLE
    //                        ,B.IB_BS
    //                        ,B.FL_POLI_IFP
    //                        ,C.ID_STAT_OGG_BUS
    //                        ,C.ID_OGG
    //                        ,C.TP_OGG
    //                        ,C.ID_MOVI_CRZ
    //                        ,C.ID_MOVI_CHIU
    //                        ,C.DT_INI_EFF
    //                        ,C.DT_END_EFF
    //                        ,C.COD_COMP_ANIA
    //                        ,C.TP_STAT_BUS
    //                        ,C.TP_CAUS
    //                        ,C.DS_RIGA
    //                        ,C.DS_OPER_SQL
    //                        ,C.DS_VER
    //                        ,C.DS_TS_INI_CPTZ
    //                        ,C.DS_TS_END_CPTZ
    //                        ,C.DS_UTENTE
    //                        ,C.DS_STATO_ELAB
    //                  FROM ADES      A,
    //                       POLI      B,
    //                       STAT_OGG_BUS C
    //                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
    //                                             :WS-TP-FRM-ASSVA2 )
    //                    AND B.COD_PROD     =  :WLB-COD-PROD
    //                    AND A.ID_POLI      =   B.ID_POLI
    //                    AND A.ID_ADES      =   C.ID_OGG
    //                    AND C.TP_OGG       =  'AD'
    //           *--  D C.TP_STAT_BUS     =  'VI'
    //                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
    //                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
    //                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
    //                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
    //                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
    //                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
    //                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
    //                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
    //                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                    AND A.ID_ADES IN
    //                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
    //                                                        STAT_OGG_BUS E
    //                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
    //                        AND E.TP_OGG = 'TG'
    //                        AND E.TP_STAT_BUS IN ('VI','ST')
    //                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
    //                        AND D.COD_COMP_ANIA =
    //                            :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
    //                        AND D.DT_END_EFF >    :WS-DT-PTF-X
    //                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
    //                        AND E.DT_END_EFF >    :WS-DT-PTF-X
    //                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
    //                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
    //                        )
    //                    AND A.DS_STATO_ELAB IN (
    //                                            :IABV0002-STATE-01,
    //                                            :IABV0002-STATE-02,
    //                                            :IABV0002-STATE-03,
    //                                            :IABV0002-STATE-04,
    //                                            :IABV0002-STATE-05,
    //                                            :IABV0002-STATE-06,
    //                                            :IABV0002-STATE-07,
    //                                            :IABV0002-STATE-08,
    //                                            :IABV0002-STATE-09,
    //                                            :IABV0002-STATE-10
    //                                              )
    //                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
    //                    ORDER BY A.ID_POLI, A.ID_ADES
    //                END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310SelectSc06() {
        // COB_CODE:      EXEC SQL
        //                  SELECT
        //                         A.ID_ADES
        //                        ,A.ID_POLI
        //                        ,A.ID_MOVI_CRZ
        //                        ,A.ID_MOVI_CHIU
        //                        ,A.DT_INI_EFF
        //                        ,A.DT_END_EFF
        //                        ,A.IB_PREV
        //                        ,A.IB_OGG
        //                        ,A.COD_COMP_ANIA
        //                        ,A.DT_DECOR
        //                        ,A.DT_SCAD
        //                        ,A.ETA_A_SCAD
        //                        ,A.DUR_AA
        //                        ,A.DUR_MM
        //                        ,A.DUR_GG
        //                        ,A.TP_RGM_FISC
        //                        ,A.TP_RIAT
        //                        ,A.TP_MOD_PAG_TIT
        //                        ,A.TP_IAS
        //                        ,A.DT_VARZ_TP_IAS
        //                        ,A.PRE_NET_IND
        //                        ,A.PRE_LRD_IND
        //                        ,A.RAT_LRD_IND
        //                        ,A.PRSTZ_INI_IND
        //                        ,A.FL_COINC_ASSTO
        //                        ,A.IB_DFLT
        //                        ,A.MOD_CALC
        //                        ,A.TP_FNT_CNBTVA
        //                        ,A.IMP_AZ
        //                        ,A.IMP_ADER
        //                        ,A.IMP_TFR
        //                        ,A.IMP_VOLO
        //                        ,A.PC_AZ
        //                        ,A.PC_ADER
        //                        ,A.PC_TFR
        //                        ,A.PC_VOLO
        //                        ,A.DT_NOVA_RGM_FISC
        //                        ,A.FL_ATTIV
        //                        ,A.IMP_REC_RIT_VIS
        //                        ,A.IMP_REC_RIT_ACC
        //                        ,A.FL_VARZ_STAT_TBGC
        //                        ,A.FL_PROVZA_MIGRAZ
        //                        ,A.IMPB_VIS_DA_REC
        //                        ,A.DT_DECOR_PREST_BAN
        //                        ,A.DT_EFF_VARZ_STAT_T
        //                        ,A.DS_RIGA
        //                        ,A.DS_OPER_SQL
        //                        ,A.DS_VER
        //                        ,A.DS_TS_INI_CPTZ
        //                        ,A.DS_TS_END_CPTZ
        //                        ,A.DS_UTENTE
        //                        ,A.DS_STATO_ELAB
        //                        ,A.CUM_CNBT_CAP
        //                        ,A.IMP_GAR_CNBT
        //                        ,A.DT_ULT_CONS_CNBT
        //                        ,A.IDEN_ISC_FND
        //                        ,A.NUM_RAT_PIAN
        //                        ,A.DT_PRESC
        //                        ,A.CONCS_PREST
        //                        ,B.ID_POLI
        //                        ,B.ID_MOVI_CRZ
        //                        ,B.ID_MOVI_CHIU
        //                        ,B.IB_OGG
        //                        ,B.IB_PROP
        //                        ,B.DT_PROP
        //                        ,B.DT_INI_EFF
        //                        ,B.DT_END_EFF
        //                        ,B.COD_COMP_ANIA
        //                        ,B.DT_DECOR
        //                        ,B.DT_EMIS
        //                        ,B.TP_POLI
        //                        ,B.DUR_AA
        //                        ,B.DUR_MM
        //                        ,B.DT_SCAD
        //                        ,B.COD_PROD
        //                        ,B.DT_INI_VLDT_PROD
        //                        ,B.COD_CONV
        //                        ,B.COD_RAMO
        //                        ,B.DT_INI_VLDT_CONV
        //                        ,B.DT_APPLZ_CONV
        //                        ,B.TP_FRM_ASSVA
        //                        ,B.TP_RGM_FISC
        //                        ,B.FL_ESTAS
        //                        ,B.FL_RSH_COMUN
        //                        ,B.FL_RSH_COMUN_COND
        //                        ,B.TP_LIV_GENZ_TIT
        //                        ,B.FL_COP_FINANZ
        //                        ,B.TP_APPLZ_DIR
        //                        ,B.SPE_MED
        //                        ,B.DIR_EMIS
        //                        ,B.DIR_1O_VERS
        //                        ,B.DIR_VERS_AGG
        //                        ,B.COD_DVS
        //                        ,B.FL_FNT_AZ
        //                        ,B.FL_FNT_ADER
        //                        ,B.FL_FNT_TFR
        //                        ,B.FL_FNT_VOLO
        //                        ,B.TP_OPZ_A_SCAD
        //                        ,B.AA_DIFF_PROR_DFLT
        //                        ,B.FL_VER_PROD
        //                        ,B.DUR_GG
        //                        ,B.DIR_QUIET
        //                        ,B.TP_PTF_ESTNO
        //                        ,B.FL_CUM_PRE_CNTR
        //                        ,B.FL_AMMB_MOVI
        //                        ,B.CONV_GECO
        //                        ,B.DS_RIGA
        //                        ,B.DS_OPER_SQL
        //                        ,B.DS_VER
        //                        ,B.DS_TS_INI_CPTZ
        //                        ,B.DS_TS_END_CPTZ
        //                        ,B.DS_UTENTE
        //                        ,B.DS_STATO_ELAB
        //                        ,B.FL_SCUDO_FISC
        //                        ,B.FL_TRASFE
        //                        ,B.FL_TFR_STRC
        //                        ,B.DT_PRESC
        //                        ,B.COD_CONV_AGG
        //                        ,B.SUBCAT_PROD
        //                        ,B.FL_QUEST_ADEGZ_ASS
        //                        ,B.COD_TPA
        //                        ,B.ID_ACC_COMM
        //                        ,B.FL_POLI_CPI_PR
        //                        ,B.FL_POLI_BUNDLING
        //                        ,B.IND_POLI_PRIN_COLL
        //                        ,B.FL_VND_BUNDLE
        //                        ,B.IB_BS
        //                        ,B.FL_POLI_IFP
        //                        ,C.ID_STAT_OGG_BUS
        //                        ,C.ID_OGG
        //                        ,C.TP_OGG
        //                        ,C.ID_MOVI_CRZ
        //                        ,C.ID_MOVI_CHIU
        //                        ,C.DT_INI_EFF
        //                        ,C.DT_END_EFF
        //                        ,C.COD_COMP_ANIA
        //                        ,C.TP_STAT_BUS
        //                        ,C.TP_CAUS
        //                        ,C.DS_RIGA
        //                        ,C.DS_OPER_SQL
        //                        ,C.DS_VER
        //                        ,C.DS_TS_INI_CPTZ
        //                        ,C.DS_TS_END_CPTZ
        //                        ,C.DS_UTENTE
        //                        ,C.DS_STATO_ELAB
        //                  INTO
        //                        :ADE-ID-ADES
        //                       ,:ADE-ID-POLI
        //                       ,:ADE-ID-MOVI-CRZ
        //                       ,:ADE-ID-MOVI-CHIU
        //                        :IND-ADE-ID-MOVI-CHIU
        //                       ,:ADE-DT-INI-EFF-DB
        //                       ,:ADE-DT-END-EFF-DB
        //                       ,:ADE-IB-PREV
        //                        :IND-ADE-IB-PREV
        //                       ,:ADE-IB-OGG
        //                        :IND-ADE-IB-OGG
        //                       ,:ADE-COD-COMP-ANIA
        //                       ,:ADE-DT-DECOR-DB
        //                        :IND-ADE-DT-DECOR
        //                       ,:ADE-DT-SCAD-DB
        //                        :IND-ADE-DT-SCAD
        //                       ,:ADE-ETA-A-SCAD
        //                        :IND-ADE-ETA-A-SCAD
        //                       ,:ADE-DUR-AA
        //                        :IND-ADE-DUR-AA
        //                       ,:ADE-DUR-MM
        //                        :IND-ADE-DUR-MM
        //                       ,:ADE-DUR-GG
        //                        :IND-ADE-DUR-GG
        //                       ,:ADE-TP-RGM-FISC
        //                       ,:ADE-TP-RIAT
        //                        :IND-ADE-TP-RIAT
        //                       ,:ADE-TP-MOD-PAG-TIT
        //                       ,:ADE-TP-IAS
        //                        :IND-ADE-TP-IAS
        //                       ,:ADE-DT-VARZ-TP-IAS-DB
        //                        :IND-ADE-DT-VARZ-TP-IAS
        //                       ,:ADE-PRE-NET-IND
        //                        :IND-ADE-PRE-NET-IND
        //                       ,:ADE-PRE-LRD-IND
        //                        :IND-ADE-PRE-LRD-IND
        //                       ,:ADE-RAT-LRD-IND
        //                        :IND-ADE-RAT-LRD-IND
        //                       ,:ADE-PRSTZ-INI-IND
        //                        :IND-ADE-PRSTZ-INI-IND
        //                       ,:ADE-FL-COINC-ASSTO
        //                        :IND-ADE-FL-COINC-ASSTO
        //                       ,:ADE-IB-DFLT
        //                        :IND-ADE-IB-DFLT
        //                       ,:ADE-MOD-CALC
        //                        :IND-ADE-MOD-CALC
        //                       ,:ADE-TP-FNT-CNBTVA
        //                        :IND-ADE-TP-FNT-CNBTVA
        //                       ,:ADE-IMP-AZ
        //                        :IND-ADE-IMP-AZ
        //                       ,:ADE-IMP-ADER
        //                        :IND-ADE-IMP-ADER
        //                       ,:ADE-IMP-TFR
        //                        :IND-ADE-IMP-TFR
        //                       ,:ADE-IMP-VOLO
        //                        :IND-ADE-IMP-VOLO
        //                       ,:ADE-PC-AZ
        //                        :IND-ADE-PC-AZ
        //                       ,:ADE-PC-ADER
        //                        :IND-ADE-PC-ADER
        //                       ,:ADE-PC-TFR
        //                        :IND-ADE-PC-TFR
        //                       ,:ADE-PC-VOLO
        //                        :IND-ADE-PC-VOLO
        //                       ,:ADE-DT-NOVA-RGM-FISC-DB
        //                        :IND-ADE-DT-NOVA-RGM-FISC
        //                       ,:ADE-FL-ATTIV
        //                        :IND-ADE-FL-ATTIV
        //                       ,:ADE-IMP-REC-RIT-VIS
        //                        :IND-ADE-IMP-REC-RIT-VIS
        //                       ,:ADE-IMP-REC-RIT-ACC
        //                        :IND-ADE-IMP-REC-RIT-ACC
        //                       ,:ADE-FL-VARZ-STAT-TBGC
        //                        :IND-ADE-FL-VARZ-STAT-TBGC
        //                       ,:ADE-FL-PROVZA-MIGRAZ
        //                        :IND-ADE-FL-PROVZA-MIGRAZ
        //                       ,:ADE-IMPB-VIS-DA-REC
        //                        :IND-ADE-IMPB-VIS-DA-REC
        //                       ,:ADE-DT-DECOR-PREST-BAN-DB
        //                        :IND-ADE-DT-DECOR-PREST-BAN
        //                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                        :IND-ADE-DT-EFF-VARZ-STAT-T
        //                       ,:ADE-DS-RIGA
        //                       ,:ADE-DS-OPER-SQL
        //                       ,:ADE-DS-VER
        //                       ,:ADE-DS-TS-INI-CPTZ
        //                       ,:ADE-DS-TS-END-CPTZ
        //                       ,:ADE-DS-UTENTE
        //                       ,:ADE-DS-STATO-ELAB
        //                       ,:ADE-CUM-CNBT-CAP
        //                        :IND-ADE-CUM-CNBT-CAP
        //                       ,:ADE-IMP-GAR-CNBT
        //                        :IND-ADE-IMP-GAR-CNBT
        //                       ,:ADE-DT-ULT-CONS-CNBT-DB
        //                        :IND-ADE-DT-ULT-CONS-CNBT
        //                       ,:ADE-IDEN-ISC-FND
        //                        :IND-ADE-IDEN-ISC-FND
        //                       ,:ADE-NUM-RAT-PIAN
        //                        :IND-ADE-NUM-RAT-PIAN
        //                       ,:ADE-DT-PRESC-DB
        //                        :IND-ADE-DT-PRESC
        //                       ,:ADE-CONCS-PREST
        //                        :IND-ADE-CONCS-PREST
        //                       ,:POL-ID-POLI
        //                       ,:POL-ID-MOVI-CRZ
        //                       ,:POL-ID-MOVI-CHIU
        //                        :IND-POL-ID-MOVI-CHIU
        //                       ,:POL-IB-OGG
        //                        :IND-POL-IB-OGG
        //                       ,:POL-IB-PROP
        //                       ,:POL-DT-PROP-DB
        //                        :IND-POL-DT-PROP
        //                       ,:POL-DT-INI-EFF-DB
        //                       ,:POL-DT-END-EFF-DB
        //                       ,:POL-COD-COMP-ANIA
        //                       ,:POL-DT-DECOR-DB
        //                       ,:POL-DT-EMIS-DB
        //                       ,:POL-TP-POLI
        //                       ,:POL-DUR-AA
        //                        :IND-POL-DUR-AA
        //                       ,:POL-DUR-MM
        //                        :IND-POL-DUR-MM
        //                       ,:POL-DT-SCAD-DB
        //                        :IND-POL-DT-SCAD
        //                       ,:POL-COD-PROD
        //                       ,:POL-DT-INI-VLDT-PROD-DB
        //                       ,:POL-COD-CONV
        //                        :IND-POL-COD-CONV
        //                       ,:POL-COD-RAMO
        //                        :IND-POL-COD-RAMO
        //                       ,:POL-DT-INI-VLDT-CONV-DB
        //                        :IND-POL-DT-INI-VLDT-CONV
        //                       ,:POL-DT-APPLZ-CONV-DB
        //                        :IND-POL-DT-APPLZ-CONV
        //                       ,:POL-TP-FRM-ASSVA
        //                       ,:POL-TP-RGM-FISC
        //                        :IND-POL-TP-RGM-FISC
        //                       ,:POL-FL-ESTAS
        //                        :IND-POL-FL-ESTAS
        //                       ,:POL-FL-RSH-COMUN
        //                        :IND-POL-FL-RSH-COMUN
        //                       ,:POL-FL-RSH-COMUN-COND
        //                        :IND-POL-FL-RSH-COMUN-COND
        //                       ,:POL-TP-LIV-GENZ-TIT
        //                       ,:POL-FL-COP-FINANZ
        //                        :IND-POL-FL-COP-FINANZ
        //                       ,:POL-TP-APPLZ-DIR
        //                        :IND-POL-TP-APPLZ-DIR
        //                       ,:POL-SPE-MED
        //                        :IND-POL-SPE-MED
        //                       ,:POL-DIR-EMIS
        //                        :IND-POL-DIR-EMIS
        //                       ,:POL-DIR-1O-VERS
        //                        :IND-POL-DIR-1O-VERS
        //                       ,:POL-DIR-VERS-AGG
        //                        :IND-POL-DIR-VERS-AGG
        //                       ,:POL-COD-DVS
        //                        :IND-POL-COD-DVS
        //                       ,:POL-FL-FNT-AZ
        //                        :IND-POL-FL-FNT-AZ
        //                       ,:POL-FL-FNT-ADER
        //                        :IND-POL-FL-FNT-ADER
        //                       ,:POL-FL-FNT-TFR
        //                        :IND-POL-FL-FNT-TFR
        //                       ,:POL-FL-FNT-VOLO
        //                        :IND-POL-FL-FNT-VOLO
        //                       ,:POL-TP-OPZ-A-SCAD
        //                        :IND-POL-TP-OPZ-A-SCAD
        //                       ,:POL-AA-DIFF-PROR-DFLT
        //                        :IND-POL-AA-DIFF-PROR-DFLT
        //                       ,:POL-FL-VER-PROD
        //                        :IND-POL-FL-VER-PROD
        //                       ,:POL-DUR-GG
        //                        :IND-POL-DUR-GG
        //                       ,:POL-DIR-QUIET
        //                        :IND-POL-DIR-QUIET
        //                       ,:POL-TP-PTF-ESTNO
        //                        :IND-POL-TP-PTF-ESTNO
        //                       ,:POL-FL-CUM-PRE-CNTR
        //                        :IND-POL-FL-CUM-PRE-CNTR
        //                       ,:POL-FL-AMMB-MOVI
        //                        :IND-POL-FL-AMMB-MOVI
        //                       ,:POL-CONV-GECO
        //                        :IND-POL-CONV-GECO
        //                       ,:POL-DS-RIGA
        //                       ,:POL-DS-OPER-SQL
        //                       ,:POL-DS-VER
        //                       ,:POL-DS-TS-INI-CPTZ
        //                       ,:POL-DS-TS-END-CPTZ
        //                       ,:POL-DS-UTENTE
        //                       ,:POL-DS-STATO-ELAB
        //                       ,:POL-FL-SCUDO-FISC
        //                        :IND-POL-FL-SCUDO-FISC
        //                       ,:POL-FL-TRASFE
        //                        :IND-POL-FL-TRASFE
        //                       ,:POL-FL-TFR-STRC
        //                        :IND-POL-FL-TFR-STRC
        //                       ,:POL-DT-PRESC-DB
        //                        :IND-POL-DT-PRESC
        //                       ,:POL-COD-CONV-AGG
        //                        :IND-POL-COD-CONV-AGG
        //                       ,:POL-SUBCAT-PROD
        //                        :IND-POL-SUBCAT-PROD
        //                       ,:POL-FL-QUEST-ADEGZ-ASS
        //                        :IND-POL-FL-QUEST-ADEGZ-ASS
        //                       ,:POL-COD-TPA
        //                        :IND-POL-COD-TPA
        //                       ,:POL-ID-ACC-COMM
        //                        :IND-POL-ID-ACC-COMM
        //                       ,:POL-FL-POLI-CPI-PR
        //                        :IND-POL-FL-POLI-CPI-PR
        //                       ,:POL-FL-POLI-BUNDLING
        //                        :IND-POL-FL-POLI-BUNDLING
        //                       ,:POL-IND-POLI-PRIN-COLL
        //                        :IND-POL-IND-POLI-PRIN-COLL
        //                       ,:POL-FL-VND-BUNDLE
        //                        :IND-POL-FL-VND-BUNDLE
        //                       ,:POL-IB-BS
        //                        :IND-POL-IB-BS
        //                       ,:POL-FL-POLI-IFP
        //                        :IND-POL-FL-POLI-IFP
        //                       ,:STB-ID-STAT-OGG-BUS
        //                       ,:STB-ID-OGG
        //                       ,:STB-TP-OGG
        //                       ,:STB-ID-MOVI-CRZ
        //                       ,:STB-ID-MOVI-CHIU
        //                        :IND-STB-ID-MOVI-CHIU
        //                       ,:STB-DT-INI-EFF-DB
        //                       ,:STB-DT-END-EFF-DB
        //                       ,:STB-COD-COMP-ANIA
        //                       ,:STB-TP-STAT-BUS
        //                       ,:STB-TP-CAUS
        //                       ,:STB-DS-RIGA
        //                       ,:STB-DS-OPER-SQL
        //                       ,:STB-DS-VER
        //                       ,:STB-DS-TS-INI-CPTZ
        //                       ,:STB-DS-TS-END-CPTZ
        //                       ,:STB-DS-UTENTE
        //                       ,:STB-DS-STATO-ELAB
        //                  FROM ADES      A,
        //                       POLI      B,
        //                       STAT_OGG_BUS C
        //                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
        //                                             :WS-TP-FRM-ASSVA2 )
        //                    AND B.COD_PROD     =  :WLB-COD-PROD
        //                    AND A.ID_POLI      =   B.ID_POLI
        //                    AND A.ID_ADES      =   C.ID_OGG
        //                    AND C.TP_OGG       =  'AD'
        //           *--  D C.TP_STAT_BUS     =  'VI'
        //                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
        //                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
        //                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
        //                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
        //                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
        //                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                    AND A.ID_ADES IN
        //                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
        //                                                        STAT_OGG_BUS E
        //                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
        //                        AND E.TP_OGG = 'TG'
        //                        AND E.TP_STAT_BUS IN ('VI','ST')
        //                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
        //                        AND D.COD_COMP_ANIA =
        //                            :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
        //                        AND D.DT_END_EFF >    :WS-DT-PTF-X
        //                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
        //                        AND E.DT_END_EFF >    :WS-DT-PTF-X
        //                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
        //                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
        //                        )
        //                    AND A.DS_STATO_ELAB IN (
        //                                            :IABV0002-STATE-01,
        //                                            :IABV0002-STATE-02,
        //                                            :IABV0002-STATE-03,
        //                                            :IABV0002-STATE-04,
        //                                            :IABV0002-STATE-05,
        //                                            :IABV0002-STATE-06,
        //                                            :IABV0002-STATE-07,
        //                                            :IABV0002-STATE-08,
        //                                            :IABV0002-STATE-09,
        //                                            :IABV0002-STATE-10
        //                                              )
        //                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
        //                    ORDER BY A.ID_POLI, A.ID_ADES
        //                   FETCH FIRST ROW ONLY
        //                END-EXEC.
        adesPoliDao.selectRec7(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A320-UPDATE-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320UpdateSc06() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK-SC06         THRU A330-SC06-EX
        //              WHEN IDSV0003-WHERE-CONDITION-06
        //                   PERFORM A340-UPDATE-WHERE-COND-SC06 THRU A340-SC06-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                       THRU A345-SC06-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
            // COB_CODE: PERFORM A330-UPDATE-PK-SC06         THRU A330-SC06-EX
            a330UpdatePkSc06();
        }
        else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition06()) {
            // COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC06 THRU A340-SC06-EX
            a340UpdateWhereCondSc06();
        }
        else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
            // COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC06
            //                                               THRU A345-SC06-EX
            a345UpdateFirstActionSc06();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidLevelOper();
        }
    }

    /**Original name: A330-UPDATE-PK-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePkSc06() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=8285, because the code is unreachable.
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE ADES
        //                SET
        //                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
        //                  ,DS_VER                 = :IABV0009-VERSIONING
        //                  ,DS_UTENTE              = :ADE-DS-UTENTE
        //                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
        //                WHERE             DS_RIGA = :ADE-DS-RIGA
        //           END-EXEC.
        adesDao.updateRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A340-UPDATE-WHERE-COND-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateWhereCondSc06() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A345-UPDATE-FIRST-ACTION-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a345UpdateFirstActionSc06() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A360-OPEN-CURSOR-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursorSc06() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-SC06 THRU A305-SC06-EX.
        a305DeclareCursorSc06();
        // COB_CODE: EXEC SQL
        //                OPEN CUR-SC06
        //           END-EXEC.
        adesPoliDao.openCurSc06(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursorSc06() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-SC06
        //           END-EXEC.
        adesPoliDao.closeCurSc06();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirstSc06() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-SC06    THRU A360-SC06-EX.
        a360OpenCursorSc06();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT-SC06  THRU A390-SC06-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC06  THRU A390-SC06-EX
            a390FetchNextSc06();
        }
    }

    /**Original name: A390-FETCH-NEXT-SC06<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNextSc06() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-SC06
        //           INTO
        //                   :ADE-ID-ADES
        //                  ,:ADE-ID-POLI
        //                  ,:ADE-ID-MOVI-CRZ
        //                  ,:ADE-ID-MOVI-CHIU
        //                   :IND-ADE-ID-MOVI-CHIU
        //                  ,:ADE-DT-INI-EFF-DB
        //                  ,:ADE-DT-END-EFF-DB
        //                  ,:ADE-IB-PREV
        //                   :IND-ADE-IB-PREV
        //                  ,:ADE-IB-OGG
        //                   :IND-ADE-IB-OGG
        //                  ,:ADE-COD-COMP-ANIA
        //                  ,:ADE-DT-DECOR-DB
        //                   :IND-ADE-DT-DECOR
        //                  ,:ADE-DT-SCAD-DB
        //                   :IND-ADE-DT-SCAD
        //                  ,:ADE-ETA-A-SCAD
        //                   :IND-ADE-ETA-A-SCAD
        //                  ,:ADE-DUR-AA
        //                   :IND-ADE-DUR-AA
        //                  ,:ADE-DUR-MM
        //                   :IND-ADE-DUR-MM
        //                  ,:ADE-DUR-GG
        //                   :IND-ADE-DUR-GG
        //                  ,:ADE-TP-RGM-FISC
        //                  ,:ADE-TP-RIAT
        //                   :IND-ADE-TP-RIAT
        //                  ,:ADE-TP-MOD-PAG-TIT
        //                  ,:ADE-TP-IAS
        //                   :IND-ADE-TP-IAS
        //                  ,:ADE-DT-VARZ-TP-IAS-DB
        //                   :IND-ADE-DT-VARZ-TP-IAS
        //                  ,:ADE-PRE-NET-IND
        //                   :IND-ADE-PRE-NET-IND
        //                  ,:ADE-PRE-LRD-IND
        //                   :IND-ADE-PRE-LRD-IND
        //                  ,:ADE-RAT-LRD-IND
        //                   :IND-ADE-RAT-LRD-IND
        //                  ,:ADE-PRSTZ-INI-IND
        //                   :IND-ADE-PRSTZ-INI-IND
        //                  ,:ADE-FL-COINC-ASSTO
        //                   :IND-ADE-FL-COINC-ASSTO
        //                  ,:ADE-IB-DFLT
        //                   :IND-ADE-IB-DFLT
        //                  ,:ADE-MOD-CALC
        //                   :IND-ADE-MOD-CALC
        //                  ,:ADE-TP-FNT-CNBTVA
        //                   :IND-ADE-TP-FNT-CNBTVA
        //                  ,:ADE-IMP-AZ
        //                   :IND-ADE-IMP-AZ
        //                  ,:ADE-IMP-ADER
        //                   :IND-ADE-IMP-ADER
        //                  ,:ADE-IMP-TFR
        //                   :IND-ADE-IMP-TFR
        //                  ,:ADE-IMP-VOLO
        //                   :IND-ADE-IMP-VOLO
        //                  ,:ADE-PC-AZ
        //                   :IND-ADE-PC-AZ
        //                  ,:ADE-PC-ADER
        //                   :IND-ADE-PC-ADER
        //                  ,:ADE-PC-TFR
        //                   :IND-ADE-PC-TFR
        //                  ,:ADE-PC-VOLO
        //                   :IND-ADE-PC-VOLO
        //                  ,:ADE-DT-NOVA-RGM-FISC-DB
        //                   :IND-ADE-DT-NOVA-RGM-FISC
        //                  ,:ADE-FL-ATTIV
        //                   :IND-ADE-FL-ATTIV
        //                  ,:ADE-IMP-REC-RIT-VIS
        //                   :IND-ADE-IMP-REC-RIT-VIS
        //                  ,:ADE-IMP-REC-RIT-ACC
        //                   :IND-ADE-IMP-REC-RIT-ACC
        //                  ,:ADE-FL-VARZ-STAT-TBGC
        //                   :IND-ADE-FL-VARZ-STAT-TBGC
        //                  ,:ADE-FL-PROVZA-MIGRAZ
        //                   :IND-ADE-FL-PROVZA-MIGRAZ
        //                  ,:ADE-IMPB-VIS-DA-REC
        //                   :IND-ADE-IMPB-VIS-DA-REC
        //                  ,:ADE-DT-DECOR-PREST-BAN-DB
        //                   :IND-ADE-DT-DECOR-PREST-BAN
        //                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                   :IND-ADE-DT-EFF-VARZ-STAT-T
        //                  ,:ADE-DS-RIGA
        //                  ,:ADE-DS-OPER-SQL
        //                  ,:ADE-DS-VER
        //                  ,:ADE-DS-TS-INI-CPTZ
        //                  ,:ADE-DS-TS-END-CPTZ
        //                  ,:ADE-DS-UTENTE
        //                  ,:ADE-DS-STATO-ELAB
        //                  ,:ADE-CUM-CNBT-CAP
        //                   :IND-ADE-CUM-CNBT-CAP
        //                  ,:ADE-IMP-GAR-CNBT
        //                   :IND-ADE-IMP-GAR-CNBT
        //                  ,:ADE-DT-ULT-CONS-CNBT-DB
        //                   :IND-ADE-DT-ULT-CONS-CNBT
        //                  ,:ADE-IDEN-ISC-FND
        //                   :IND-ADE-IDEN-ISC-FND
        //                  ,:ADE-NUM-RAT-PIAN
        //                   :IND-ADE-NUM-RAT-PIAN
        //                  ,:ADE-DT-PRESC-DB
        //                   :IND-ADE-DT-PRESC
        //                  ,:ADE-CONCS-PREST
        //                   :IND-ADE-CONCS-PREST
        //                  ,:POL-ID-POLI
        //                  ,:POL-ID-MOVI-CRZ
        //                  ,:POL-ID-MOVI-CHIU
        //                   :IND-POL-ID-MOVI-CHIU
        //                  ,:POL-IB-OGG
        //                   :IND-POL-IB-OGG
        //                  ,:POL-IB-PROP
        //                  ,:POL-DT-PROP-DB
        //                   :IND-POL-DT-PROP
        //                  ,:POL-DT-INI-EFF-DB
        //                  ,:POL-DT-END-EFF-DB
        //                  ,:POL-COD-COMP-ANIA
        //                  ,:POL-DT-DECOR-DB
        //                  ,:POL-DT-EMIS-DB
        //                  ,:POL-TP-POLI
        //                  ,:POL-DUR-AA
        //                   :IND-POL-DUR-AA
        //                  ,:POL-DUR-MM
        //                   :IND-POL-DUR-MM
        //                  ,:POL-DT-SCAD-DB
        //                   :IND-POL-DT-SCAD
        //                  ,:POL-COD-PROD
        //                  ,:POL-DT-INI-VLDT-PROD-DB
        //                  ,:POL-COD-CONV
        //                   :IND-POL-COD-CONV
        //                  ,:POL-COD-RAMO
        //                   :IND-POL-COD-RAMO
        //                  ,:POL-DT-INI-VLDT-CONV-DB
        //                   :IND-POL-DT-INI-VLDT-CONV
        //                  ,:POL-DT-APPLZ-CONV-DB
        //                   :IND-POL-DT-APPLZ-CONV
        //                  ,:POL-TP-FRM-ASSVA
        //                  ,:POL-TP-RGM-FISC
        //                   :IND-POL-TP-RGM-FISC
        //                  ,:POL-FL-ESTAS
        //                   :IND-POL-FL-ESTAS
        //                  ,:POL-FL-RSH-COMUN
        //                   :IND-POL-FL-RSH-COMUN
        //                  ,:POL-FL-RSH-COMUN-COND
        //                   :IND-POL-FL-RSH-COMUN-COND
        //                  ,:POL-TP-LIV-GENZ-TIT
        //                  ,:POL-FL-COP-FINANZ
        //                   :IND-POL-FL-COP-FINANZ
        //                  ,:POL-TP-APPLZ-DIR
        //                   :IND-POL-TP-APPLZ-DIR
        //                  ,:POL-SPE-MED
        //                   :IND-POL-SPE-MED
        //                  ,:POL-DIR-EMIS
        //                   :IND-POL-DIR-EMIS
        //                  ,:POL-DIR-1O-VERS
        //                   :IND-POL-DIR-1O-VERS
        //                  ,:POL-DIR-VERS-AGG
        //                   :IND-POL-DIR-VERS-AGG
        //                  ,:POL-COD-DVS
        //                   :IND-POL-COD-DVS
        //                  ,:POL-FL-FNT-AZ
        //                   :IND-POL-FL-FNT-AZ
        //                  ,:POL-FL-FNT-ADER
        //                   :IND-POL-FL-FNT-ADER
        //                  ,:POL-FL-FNT-TFR
        //                   :IND-POL-FL-FNT-TFR
        //                  ,:POL-FL-FNT-VOLO
        //                   :IND-POL-FL-FNT-VOLO
        //                  ,:POL-TP-OPZ-A-SCAD
        //                   :IND-POL-TP-OPZ-A-SCAD
        //                  ,:POL-AA-DIFF-PROR-DFLT
        //                   :IND-POL-AA-DIFF-PROR-DFLT
        //                  ,:POL-FL-VER-PROD
        //                   :IND-POL-FL-VER-PROD
        //                  ,:POL-DUR-GG
        //                   :IND-POL-DUR-GG
        //                  ,:POL-DIR-QUIET
        //                   :IND-POL-DIR-QUIET
        //                  ,:POL-TP-PTF-ESTNO
        //                   :IND-POL-TP-PTF-ESTNO
        //                  ,:POL-FL-CUM-PRE-CNTR
        //                   :IND-POL-FL-CUM-PRE-CNTR
        //                  ,:POL-FL-AMMB-MOVI
        //                   :IND-POL-FL-AMMB-MOVI
        //                  ,:POL-CONV-GECO
        //                   :IND-POL-CONV-GECO
        //                  ,:POL-DS-RIGA
        //                  ,:POL-DS-OPER-SQL
        //                  ,:POL-DS-VER
        //                  ,:POL-DS-TS-INI-CPTZ
        //                  ,:POL-DS-TS-END-CPTZ
        //                  ,:POL-DS-UTENTE
        //                  ,:POL-DS-STATO-ELAB
        //                  ,:POL-FL-SCUDO-FISC
        //                   :IND-POL-FL-SCUDO-FISC
        //                  ,:POL-FL-TRASFE
        //                   :IND-POL-FL-TRASFE
        //                  ,:POL-FL-TFR-STRC
        //                   :IND-POL-FL-TFR-STRC
        //                  ,:POL-DT-PRESC-DB
        //                   :IND-POL-DT-PRESC
        //                  ,:POL-COD-CONV-AGG
        //                   :IND-POL-COD-CONV-AGG
        //                  ,:POL-SUBCAT-PROD
        //                   :IND-POL-SUBCAT-PROD
        //                  ,:POL-FL-QUEST-ADEGZ-ASS
        //                   :IND-POL-FL-QUEST-ADEGZ-ASS
        //                  ,:POL-COD-TPA
        //                   :IND-POL-COD-TPA
        //                  ,:POL-ID-ACC-COMM
        //                   :IND-POL-ID-ACC-COMM
        //                  ,:POL-FL-POLI-CPI-PR
        //                   :IND-POL-FL-POLI-CPI-PR
        //                  ,:POL-FL-POLI-BUNDLING
        //                   :IND-POL-FL-POLI-BUNDLING
        //                  ,:POL-IND-POLI-PRIN-COLL
        //                   :IND-POL-IND-POLI-PRIN-COLL
        //                  ,:POL-FL-VND-BUNDLE
        //                   :IND-POL-FL-VND-BUNDLE
        //                  ,:POL-IB-BS
        //                   :IND-POL-IB-BS
        //                  ,:POL-FL-POLI-IFP
        //                   :IND-POL-FL-POLI-IFP
        //                  ,:STB-ID-STAT-OGG-BUS
        //                  ,:STB-ID-OGG
        //                  ,:STB-TP-OGG
        //                  ,:STB-ID-MOVI-CRZ
        //                  ,:STB-ID-MOVI-CHIU
        //                   :IND-STB-ID-MOVI-CHIU
        //                  ,:STB-DT-INI-EFF-DB
        //                  ,:STB-DT-END-EFF-DB
        //                  ,:STB-COD-COMP-ANIA
        //                  ,:STB-TP-STAT-BUS
        //                  ,:STB-TP-CAUS
        //                  ,:STB-DS-RIGA
        //                  ,:STB-DS-OPER-SQL
        //                  ,:STB-DS-VER
        //                  ,:STB-DS-TS-INI-CPTZ
        //                  ,:STB-DS-TS-END-CPTZ
        //                  ,:STB-DS-UTENTE
        //                  ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        adesPoliDao.fetchCurSc06(this.getAdesPoliLdbm0250());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC06 THRU A370-SC06-EX
            a370CloseCursorSc06();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: SC07-SELECTION-CURSOR-07<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
    private void sc07SelectionCursor07() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC07           THRU A310-SC07-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-SC07      THRU A360-SC07-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-SC07     THRU A370-SC07-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-SC07      THRU A380-SC07-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-SC07       THRU A390-SC07-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE-SC07           THRU A320-SC07-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER          TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC07           THRU A310-SC07-EX
            a310SelectSc07();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-SC07      THRU A360-SC07-EX
            a360OpenCursorSc07();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC07     THRU A370-SC07-EX
            a370CloseCursorSc07();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-SC07      THRU A380-SC07-EX
            a380FetchFirstSc07();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC07       THRU A390-SC07-EX
            a390FetchNextSc07();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE-SC07           THRU A320-SC07-EX
            a320UpdateSc07();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursorSc07() {
    // COB_CODE:      EXEC SQL
    //                     DECLARE CUR-SC07 CURSOR WITH HOLD FOR
    //                  SELECT
    //                         A.ID_ADES
    //                        ,A.ID_POLI
    //                        ,A.ID_MOVI_CRZ
    //                        ,A.ID_MOVI_CHIU
    //                        ,A.DT_INI_EFF
    //                        ,A.DT_END_EFF
    //                        ,A.IB_PREV
    //                        ,A.IB_OGG
    //                        ,A.COD_COMP_ANIA
    //                        ,A.DT_DECOR
    //                        ,A.DT_SCAD
    //                        ,A.ETA_A_SCAD
    //                        ,A.DUR_AA
    //                        ,A.DUR_MM
    //                        ,A.DUR_GG
    //                        ,A.TP_RGM_FISC
    //                        ,A.TP_RIAT
    //                        ,A.TP_MOD_PAG_TIT
    //                        ,A.TP_IAS
    //                        ,A.DT_VARZ_TP_IAS
    //                        ,A.PRE_NET_IND
    //                        ,A.PRE_LRD_IND
    //                        ,A.RAT_LRD_IND
    //                        ,A.PRSTZ_INI_IND
    //                        ,A.FL_COINC_ASSTO
    //                        ,A.IB_DFLT
    //                        ,A.MOD_CALC
    //                        ,A.TP_FNT_CNBTVA
    //                        ,A.IMP_AZ
    //                        ,A.IMP_ADER
    //                        ,A.IMP_TFR
    //                        ,A.IMP_VOLO
    //                        ,A.PC_AZ
    //                        ,A.PC_ADER
    //                        ,A.PC_TFR
    //                        ,A.PC_VOLO
    //                        ,A.DT_NOVA_RGM_FISC
    //                        ,A.FL_ATTIV
    //                        ,A.IMP_REC_RIT_VIS
    //                        ,A.IMP_REC_RIT_ACC
    //                        ,A.FL_VARZ_STAT_TBGC
    //                        ,A.FL_PROVZA_MIGRAZ
    //                        ,A.IMPB_VIS_DA_REC
    //                        ,A.DT_DECOR_PREST_BAN
    //                        ,A.DT_EFF_VARZ_STAT_T
    //                        ,A.DS_RIGA
    //                        ,A.DS_OPER_SQL
    //                        ,A.DS_VER
    //                        ,A.DS_TS_INI_CPTZ
    //                        ,A.DS_TS_END_CPTZ
    //                        ,A.DS_UTENTE
    //                        ,A.DS_STATO_ELAB
    //                        ,A.CUM_CNBT_CAP
    //                        ,A.IMP_GAR_CNBT
    //                        ,A.DT_ULT_CONS_CNBT
    //                        ,A.IDEN_ISC_FND
    //                        ,A.NUM_RAT_PIAN
    //                        ,A.DT_PRESC
    //                        ,A.CONCS_PREST
    //                        ,B.ID_POLI
    //                        ,B.ID_MOVI_CRZ
    //                        ,B.ID_MOVI_CHIU
    //                        ,B.IB_OGG
    //                        ,B.IB_PROP
    //                        ,B.DT_PROP
    //                        ,B.DT_INI_EFF
    //                        ,B.DT_END_EFF
    //                        ,B.COD_COMP_ANIA
    //                        ,B.DT_DECOR
    //                        ,B.DT_EMIS
    //                        ,B.TP_POLI
    //                        ,B.DUR_AA
    //                        ,B.DUR_MM
    //                        ,B.DT_SCAD
    //                        ,B.COD_PROD
    //                        ,B.DT_INI_VLDT_PROD
    //                        ,B.COD_CONV
    //                        ,B.COD_RAMO
    //                        ,B.DT_INI_VLDT_CONV
    //                        ,B.DT_APPLZ_CONV
    //                        ,B.TP_FRM_ASSVA
    //                        ,B.TP_RGM_FISC
    //                        ,B.FL_ESTAS
    //                        ,B.FL_RSH_COMUN
    //                        ,B.FL_RSH_COMUN_COND
    //                        ,B.TP_LIV_GENZ_TIT
    //                        ,B.FL_COP_FINANZ
    //                        ,B.TP_APPLZ_DIR
    //                        ,B.SPE_MED
    //                        ,B.DIR_EMIS
    //                        ,B.DIR_1O_VERS
    //                        ,B.DIR_VERS_AGG
    //                        ,B.COD_DVS
    //                        ,B.FL_FNT_AZ
    //                        ,B.FL_FNT_ADER
    //                        ,B.FL_FNT_TFR
    //                        ,B.FL_FNT_VOLO
    //                        ,B.TP_OPZ_A_SCAD
    //                        ,B.AA_DIFF_PROR_DFLT
    //                        ,B.FL_VER_PROD
    //                        ,B.DUR_GG
    //                        ,B.DIR_QUIET
    //                        ,B.TP_PTF_ESTNO
    //                        ,B.FL_CUM_PRE_CNTR
    //                        ,B.FL_AMMB_MOVI
    //                        ,B.CONV_GECO
    //                        ,B.DS_RIGA
    //                        ,B.DS_OPER_SQL
    //                        ,B.DS_VER
    //                        ,B.DS_TS_INI_CPTZ
    //                        ,B.DS_TS_END_CPTZ
    //                        ,B.DS_UTENTE
    //                        ,B.DS_STATO_ELAB
    //                        ,B.FL_SCUDO_FISC
    //                        ,B.FL_TRASFE
    //                        ,B.FL_TFR_STRC
    //                        ,B.DT_PRESC
    //                        ,B.COD_CONV_AGG
    //                        ,B.SUBCAT_PROD
    //                        ,B.FL_QUEST_ADEGZ_ASS
    //                        ,B.COD_TPA
    //                        ,B.ID_ACC_COMM
    //                        ,B.FL_POLI_CPI_PR
    //                        ,B.FL_POLI_BUNDLING
    //                        ,B.IND_POLI_PRIN_COLL
    //                        ,B.FL_VND_BUNDLE
    //                        ,B.IB_BS
    //                        ,B.FL_POLI_IFP
    //                        ,C.ID_STAT_OGG_BUS
    //                        ,C.ID_OGG
    //                        ,C.TP_OGG
    //                        ,C.ID_MOVI_CRZ
    //                        ,C.ID_MOVI_CHIU
    //                        ,C.DT_INI_EFF
    //                        ,C.DT_END_EFF
    //                        ,C.COD_COMP_ANIA
    //                        ,C.TP_STAT_BUS
    //                        ,C.TP_CAUS
    //                        ,C.DS_RIGA
    //                        ,C.DS_OPER_SQL
    //                        ,C.DS_VER
    //                        ,C.DS_TS_INI_CPTZ
    //                        ,C.DS_TS_END_CPTZ
    //                        ,C.DS_UTENTE
    //                        ,C.DS_STATO_ELAB
    //                  FROM ADES      A,
    //                       POLI      B,
    //                       STAT_OGG_BUS C
    //                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
    //                                             :WS-TP-FRM-ASSVA2 )
    //                    AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
    //                                             :WLB-IB-POLI-LAST
    //                    AND A.ID_POLI      =   B.ID_POLI
    //                    AND A.ID_ADES      =   C.ID_OGG
    //                    AND C.TP_OGG       =  'AD'
    //           *--  D C.TP_STAT_BUS     =  'VI'
    //                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
    //                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
    //                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
    //                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
    //                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
    //                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
    //                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
    //                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
    //                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                    AND A.ID_ADES IN
    //                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
    //                                                        STAT_OGG_BUS E
    //                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
    //                        AND E.TP_OGG = 'TG'
    //                        AND E.TP_STAT_BUS IN ('VI','ST')
    //                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
    //                        AND D.COD_COMP_ANIA =
    //                            :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
    //                        AND D.DT_END_EFF >    :WS-DT-PTF-X
    //                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
    //                        AND E.DT_END_EFF >    :WS-DT-PTF-X
    //                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
    //                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
    //                        )
    //                    AND A.DS_STATO_ELAB IN (
    //                                            :IABV0002-STATE-01,
    //                                            :IABV0002-STATE-02,
    //                                            :IABV0002-STATE-03,
    //                                            :IABV0002-STATE-04,
    //                                            :IABV0002-STATE-05,
    //                                            :IABV0002-STATE-06,
    //                                            :IABV0002-STATE-07,
    //                                            :IABV0002-STATE-08,
    //                                            :IABV0002-STATE-09,
    //                                            :IABV0002-STATE-10
    //                                              )
    //                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
    //                    ORDER BY A.ID_POLI, A.ID_ADES
    //                END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310SelectSc07() {
        // COB_CODE:      EXEC SQL
        //                  SELECT
        //                         A.ID_ADES
        //                        ,A.ID_POLI
        //                        ,A.ID_MOVI_CRZ
        //                        ,A.ID_MOVI_CHIU
        //                        ,A.DT_INI_EFF
        //                        ,A.DT_END_EFF
        //                        ,A.IB_PREV
        //                        ,A.IB_OGG
        //                        ,A.COD_COMP_ANIA
        //                        ,A.DT_DECOR
        //                        ,A.DT_SCAD
        //                        ,A.ETA_A_SCAD
        //                        ,A.DUR_AA
        //                        ,A.DUR_MM
        //                        ,A.DUR_GG
        //                        ,A.TP_RGM_FISC
        //                        ,A.TP_RIAT
        //                        ,A.TP_MOD_PAG_TIT
        //                        ,A.TP_IAS
        //                        ,A.DT_VARZ_TP_IAS
        //                        ,A.PRE_NET_IND
        //                        ,A.PRE_LRD_IND
        //                        ,A.RAT_LRD_IND
        //                        ,A.PRSTZ_INI_IND
        //                        ,A.FL_COINC_ASSTO
        //                        ,A.IB_DFLT
        //                        ,A.MOD_CALC
        //                        ,A.TP_FNT_CNBTVA
        //                        ,A.IMP_AZ
        //                        ,A.IMP_ADER
        //                        ,A.IMP_TFR
        //                        ,A.IMP_VOLO
        //                        ,A.PC_AZ
        //                        ,A.PC_ADER
        //                        ,A.PC_TFR
        //                        ,A.PC_VOLO
        //                        ,A.DT_NOVA_RGM_FISC
        //                        ,A.FL_ATTIV
        //                        ,A.IMP_REC_RIT_VIS
        //                        ,A.IMP_REC_RIT_ACC
        //                        ,A.FL_VARZ_STAT_TBGC
        //                        ,A.FL_PROVZA_MIGRAZ
        //                        ,A.IMPB_VIS_DA_REC
        //                        ,A.DT_DECOR_PREST_BAN
        //                        ,A.DT_EFF_VARZ_STAT_T
        //                        ,A.DS_RIGA
        //                        ,A.DS_OPER_SQL
        //                        ,A.DS_VER
        //                        ,A.DS_TS_INI_CPTZ
        //                        ,A.DS_TS_END_CPTZ
        //                        ,A.DS_UTENTE
        //                        ,A.DS_STATO_ELAB
        //                        ,A.CUM_CNBT_CAP
        //                        ,A.IMP_GAR_CNBT
        //                        ,A.DT_ULT_CONS_CNBT
        //                        ,A.IDEN_ISC_FND
        //                        ,A.NUM_RAT_PIAN
        //                        ,A.DT_PRESC
        //                        ,A.CONCS_PREST
        //                        ,B.ID_POLI
        //                        ,B.ID_MOVI_CRZ
        //                        ,B.ID_MOVI_CHIU
        //                        ,B.IB_OGG
        //                        ,B.IB_PROP
        //                        ,B.DT_PROP
        //                        ,B.DT_INI_EFF
        //                        ,B.DT_END_EFF
        //                        ,B.COD_COMP_ANIA
        //                        ,B.DT_DECOR
        //                        ,B.DT_EMIS
        //                        ,B.TP_POLI
        //                        ,B.DUR_AA
        //                        ,B.DUR_MM
        //                        ,B.DT_SCAD
        //                        ,B.COD_PROD
        //                        ,B.DT_INI_VLDT_PROD
        //                        ,B.COD_CONV
        //                        ,B.COD_RAMO
        //                        ,B.DT_INI_VLDT_CONV
        //                        ,B.DT_APPLZ_CONV
        //                        ,B.TP_FRM_ASSVA
        //                        ,B.TP_RGM_FISC
        //                        ,B.FL_ESTAS
        //                        ,B.FL_RSH_COMUN
        //                        ,B.FL_RSH_COMUN_COND
        //                        ,B.TP_LIV_GENZ_TIT
        //                        ,B.FL_COP_FINANZ
        //                        ,B.TP_APPLZ_DIR
        //                        ,B.SPE_MED
        //                        ,B.DIR_EMIS
        //                        ,B.DIR_1O_VERS
        //                        ,B.DIR_VERS_AGG
        //                        ,B.COD_DVS
        //                        ,B.FL_FNT_AZ
        //                        ,B.FL_FNT_ADER
        //                        ,B.FL_FNT_TFR
        //                        ,B.FL_FNT_VOLO
        //                        ,B.TP_OPZ_A_SCAD
        //                        ,B.AA_DIFF_PROR_DFLT
        //                        ,B.FL_VER_PROD
        //                        ,B.DUR_GG
        //                        ,B.DIR_QUIET
        //                        ,B.TP_PTF_ESTNO
        //                        ,B.FL_CUM_PRE_CNTR
        //                        ,B.FL_AMMB_MOVI
        //                        ,B.CONV_GECO
        //                        ,B.DS_RIGA
        //                        ,B.DS_OPER_SQL
        //                        ,B.DS_VER
        //                        ,B.DS_TS_INI_CPTZ
        //                        ,B.DS_TS_END_CPTZ
        //                        ,B.DS_UTENTE
        //                        ,B.DS_STATO_ELAB
        //                        ,B.FL_SCUDO_FISC
        //                        ,B.FL_TRASFE
        //                        ,B.FL_TFR_STRC
        //                        ,B.DT_PRESC
        //                        ,B.COD_CONV_AGG
        //                        ,B.SUBCAT_PROD
        //                        ,B.FL_QUEST_ADEGZ_ASS
        //                        ,B.COD_TPA
        //                        ,B.ID_ACC_COMM
        //                        ,B.FL_POLI_CPI_PR
        //                        ,B.FL_POLI_BUNDLING
        //                        ,B.IND_POLI_PRIN_COLL
        //                        ,B.FL_VND_BUNDLE
        //                        ,B.IB_BS
        //                        ,B.FL_POLI_IFP
        //                        ,C.ID_STAT_OGG_BUS
        //                        ,C.ID_OGG
        //                        ,C.TP_OGG
        //                        ,C.ID_MOVI_CRZ
        //                        ,C.ID_MOVI_CHIU
        //                        ,C.DT_INI_EFF
        //                        ,C.DT_END_EFF
        //                        ,C.COD_COMP_ANIA
        //                        ,C.TP_STAT_BUS
        //                        ,C.TP_CAUS
        //                        ,C.DS_RIGA
        //                        ,C.DS_OPER_SQL
        //                        ,C.DS_VER
        //                        ,C.DS_TS_INI_CPTZ
        //                        ,C.DS_TS_END_CPTZ
        //                        ,C.DS_UTENTE
        //                        ,C.DS_STATO_ELAB
        //                  INTO
        //                        :ADE-ID-ADES
        //                       ,:ADE-ID-POLI
        //                       ,:ADE-ID-MOVI-CRZ
        //                       ,:ADE-ID-MOVI-CHIU
        //                        :IND-ADE-ID-MOVI-CHIU
        //                       ,:ADE-DT-INI-EFF-DB
        //                       ,:ADE-DT-END-EFF-DB
        //                       ,:ADE-IB-PREV
        //                        :IND-ADE-IB-PREV
        //                       ,:ADE-IB-OGG
        //                        :IND-ADE-IB-OGG
        //                       ,:ADE-COD-COMP-ANIA
        //                       ,:ADE-DT-DECOR-DB
        //                        :IND-ADE-DT-DECOR
        //                       ,:ADE-DT-SCAD-DB
        //                        :IND-ADE-DT-SCAD
        //                       ,:ADE-ETA-A-SCAD
        //                        :IND-ADE-ETA-A-SCAD
        //                       ,:ADE-DUR-AA
        //                        :IND-ADE-DUR-AA
        //                       ,:ADE-DUR-MM
        //                        :IND-ADE-DUR-MM
        //                       ,:ADE-DUR-GG
        //                        :IND-ADE-DUR-GG
        //                       ,:ADE-TP-RGM-FISC
        //                       ,:ADE-TP-RIAT
        //                        :IND-ADE-TP-RIAT
        //                       ,:ADE-TP-MOD-PAG-TIT
        //                       ,:ADE-TP-IAS
        //                        :IND-ADE-TP-IAS
        //                       ,:ADE-DT-VARZ-TP-IAS-DB
        //                        :IND-ADE-DT-VARZ-TP-IAS
        //                       ,:ADE-PRE-NET-IND
        //                        :IND-ADE-PRE-NET-IND
        //                       ,:ADE-PRE-LRD-IND
        //                        :IND-ADE-PRE-LRD-IND
        //                       ,:ADE-RAT-LRD-IND
        //                        :IND-ADE-RAT-LRD-IND
        //                       ,:ADE-PRSTZ-INI-IND
        //                        :IND-ADE-PRSTZ-INI-IND
        //                       ,:ADE-FL-COINC-ASSTO
        //                        :IND-ADE-FL-COINC-ASSTO
        //                       ,:ADE-IB-DFLT
        //                        :IND-ADE-IB-DFLT
        //                       ,:ADE-MOD-CALC
        //                        :IND-ADE-MOD-CALC
        //                       ,:ADE-TP-FNT-CNBTVA
        //                        :IND-ADE-TP-FNT-CNBTVA
        //                       ,:ADE-IMP-AZ
        //                        :IND-ADE-IMP-AZ
        //                       ,:ADE-IMP-ADER
        //                        :IND-ADE-IMP-ADER
        //                       ,:ADE-IMP-TFR
        //                        :IND-ADE-IMP-TFR
        //                       ,:ADE-IMP-VOLO
        //                        :IND-ADE-IMP-VOLO
        //                       ,:ADE-PC-AZ
        //                        :IND-ADE-PC-AZ
        //                       ,:ADE-PC-ADER
        //                        :IND-ADE-PC-ADER
        //                       ,:ADE-PC-TFR
        //                        :IND-ADE-PC-TFR
        //                       ,:ADE-PC-VOLO
        //                        :IND-ADE-PC-VOLO
        //                       ,:ADE-DT-NOVA-RGM-FISC-DB
        //                        :IND-ADE-DT-NOVA-RGM-FISC
        //                       ,:ADE-FL-ATTIV
        //                        :IND-ADE-FL-ATTIV
        //                       ,:ADE-IMP-REC-RIT-VIS
        //                        :IND-ADE-IMP-REC-RIT-VIS
        //                       ,:ADE-IMP-REC-RIT-ACC
        //                        :IND-ADE-IMP-REC-RIT-ACC
        //                       ,:ADE-FL-VARZ-STAT-TBGC
        //                        :IND-ADE-FL-VARZ-STAT-TBGC
        //                       ,:ADE-FL-PROVZA-MIGRAZ
        //                        :IND-ADE-FL-PROVZA-MIGRAZ
        //                       ,:ADE-IMPB-VIS-DA-REC
        //                        :IND-ADE-IMPB-VIS-DA-REC
        //                       ,:ADE-DT-DECOR-PREST-BAN-DB
        //                        :IND-ADE-DT-DECOR-PREST-BAN
        //                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                        :IND-ADE-DT-EFF-VARZ-STAT-T
        //                       ,:ADE-DS-RIGA
        //                       ,:ADE-DS-OPER-SQL
        //                       ,:ADE-DS-VER
        //                       ,:ADE-DS-TS-INI-CPTZ
        //                       ,:ADE-DS-TS-END-CPTZ
        //                       ,:ADE-DS-UTENTE
        //                       ,:ADE-DS-STATO-ELAB
        //                       ,:ADE-CUM-CNBT-CAP
        //                        :IND-ADE-CUM-CNBT-CAP
        //                       ,:ADE-IMP-GAR-CNBT
        //                        :IND-ADE-IMP-GAR-CNBT
        //                       ,:ADE-DT-ULT-CONS-CNBT-DB
        //                        :IND-ADE-DT-ULT-CONS-CNBT
        //                       ,:ADE-IDEN-ISC-FND
        //                        :IND-ADE-IDEN-ISC-FND
        //                       ,:ADE-NUM-RAT-PIAN
        //                        :IND-ADE-NUM-RAT-PIAN
        //                       ,:ADE-DT-PRESC-DB
        //                        :IND-ADE-DT-PRESC
        //                       ,:ADE-CONCS-PREST
        //                        :IND-ADE-CONCS-PREST
        //                       ,:POL-ID-POLI
        //                       ,:POL-ID-MOVI-CRZ
        //                       ,:POL-ID-MOVI-CHIU
        //                        :IND-POL-ID-MOVI-CHIU
        //                       ,:POL-IB-OGG
        //                        :IND-POL-IB-OGG
        //                       ,:POL-IB-PROP
        //                       ,:POL-DT-PROP-DB
        //                        :IND-POL-DT-PROP
        //                       ,:POL-DT-INI-EFF-DB
        //                       ,:POL-DT-END-EFF-DB
        //                       ,:POL-COD-COMP-ANIA
        //                       ,:POL-DT-DECOR-DB
        //                       ,:POL-DT-EMIS-DB
        //                       ,:POL-TP-POLI
        //                       ,:POL-DUR-AA
        //                        :IND-POL-DUR-AA
        //                       ,:POL-DUR-MM
        //                        :IND-POL-DUR-MM
        //                       ,:POL-DT-SCAD-DB
        //                        :IND-POL-DT-SCAD
        //                       ,:POL-COD-PROD
        //                       ,:POL-DT-INI-VLDT-PROD-DB
        //                       ,:POL-COD-CONV
        //                        :IND-POL-COD-CONV
        //                       ,:POL-COD-RAMO
        //                        :IND-POL-COD-RAMO
        //                       ,:POL-DT-INI-VLDT-CONV-DB
        //                        :IND-POL-DT-INI-VLDT-CONV
        //                       ,:POL-DT-APPLZ-CONV-DB
        //                        :IND-POL-DT-APPLZ-CONV
        //                       ,:POL-TP-FRM-ASSVA
        //                       ,:POL-TP-RGM-FISC
        //                        :IND-POL-TP-RGM-FISC
        //                       ,:POL-FL-ESTAS
        //                        :IND-POL-FL-ESTAS
        //                       ,:POL-FL-RSH-COMUN
        //                        :IND-POL-FL-RSH-COMUN
        //                       ,:POL-FL-RSH-COMUN-COND
        //                        :IND-POL-FL-RSH-COMUN-COND
        //                       ,:POL-TP-LIV-GENZ-TIT
        //                       ,:POL-FL-COP-FINANZ
        //                        :IND-POL-FL-COP-FINANZ
        //                       ,:POL-TP-APPLZ-DIR
        //                        :IND-POL-TP-APPLZ-DIR
        //                       ,:POL-SPE-MED
        //                        :IND-POL-SPE-MED
        //                       ,:POL-DIR-EMIS
        //                        :IND-POL-DIR-EMIS
        //                       ,:POL-DIR-1O-VERS
        //                        :IND-POL-DIR-1O-VERS
        //                       ,:POL-DIR-VERS-AGG
        //                        :IND-POL-DIR-VERS-AGG
        //                       ,:POL-COD-DVS
        //                        :IND-POL-COD-DVS
        //                       ,:POL-FL-FNT-AZ
        //                        :IND-POL-FL-FNT-AZ
        //                       ,:POL-FL-FNT-ADER
        //                        :IND-POL-FL-FNT-ADER
        //                       ,:POL-FL-FNT-TFR
        //                        :IND-POL-FL-FNT-TFR
        //                       ,:POL-FL-FNT-VOLO
        //                        :IND-POL-FL-FNT-VOLO
        //                       ,:POL-TP-OPZ-A-SCAD
        //                        :IND-POL-TP-OPZ-A-SCAD
        //                       ,:POL-AA-DIFF-PROR-DFLT
        //                        :IND-POL-AA-DIFF-PROR-DFLT
        //                       ,:POL-FL-VER-PROD
        //                        :IND-POL-FL-VER-PROD
        //                       ,:POL-DUR-GG
        //                        :IND-POL-DUR-GG
        //                       ,:POL-DIR-QUIET
        //                        :IND-POL-DIR-QUIET
        //                       ,:POL-TP-PTF-ESTNO
        //                        :IND-POL-TP-PTF-ESTNO
        //                       ,:POL-FL-CUM-PRE-CNTR
        //                        :IND-POL-FL-CUM-PRE-CNTR
        //                       ,:POL-FL-AMMB-MOVI
        //                        :IND-POL-FL-AMMB-MOVI
        //                       ,:POL-CONV-GECO
        //                        :IND-POL-CONV-GECO
        //                       ,:POL-DS-RIGA
        //                       ,:POL-DS-OPER-SQL
        //                       ,:POL-DS-VER
        //                       ,:POL-DS-TS-INI-CPTZ
        //                       ,:POL-DS-TS-END-CPTZ
        //                       ,:POL-DS-UTENTE
        //                       ,:POL-DS-STATO-ELAB
        //                       ,:POL-FL-SCUDO-FISC
        //                        :IND-POL-FL-SCUDO-FISC
        //                       ,:POL-FL-TRASFE
        //                        :IND-POL-FL-TRASFE
        //                       ,:POL-FL-TFR-STRC
        //                        :IND-POL-FL-TFR-STRC
        //                       ,:POL-DT-PRESC-DB
        //                        :IND-POL-DT-PRESC
        //                       ,:POL-COD-CONV-AGG
        //                        :IND-POL-COD-CONV-AGG
        //                       ,:POL-SUBCAT-PROD
        //                        :IND-POL-SUBCAT-PROD
        //                       ,:POL-FL-QUEST-ADEGZ-ASS
        //                        :IND-POL-FL-QUEST-ADEGZ-ASS
        //                       ,:POL-COD-TPA
        //                        :IND-POL-COD-TPA
        //                       ,:POL-ID-ACC-COMM
        //                        :IND-POL-ID-ACC-COMM
        //                       ,:POL-FL-POLI-CPI-PR
        //                        :IND-POL-FL-POLI-CPI-PR
        //                       ,:POL-FL-POLI-BUNDLING
        //                        :IND-POL-FL-POLI-BUNDLING
        //                       ,:POL-IND-POLI-PRIN-COLL
        //                        :IND-POL-IND-POLI-PRIN-COLL
        //                       ,:POL-FL-VND-BUNDLE
        //                        :IND-POL-FL-VND-BUNDLE
        //                       ,:POL-IB-BS
        //                        :IND-POL-IB-BS
        //                       ,:POL-FL-POLI-IFP
        //                        :IND-POL-FL-POLI-IFP
        //                       ,:STB-ID-STAT-OGG-BUS
        //                       ,:STB-ID-OGG
        //                       ,:STB-TP-OGG
        //                       ,:STB-ID-MOVI-CRZ
        //                       ,:STB-ID-MOVI-CHIU
        //                        :IND-STB-ID-MOVI-CHIU
        //                       ,:STB-DT-INI-EFF-DB
        //                       ,:STB-DT-END-EFF-DB
        //                       ,:STB-COD-COMP-ANIA
        //                       ,:STB-TP-STAT-BUS
        //                       ,:STB-TP-CAUS
        //                       ,:STB-DS-RIGA
        //                       ,:STB-DS-OPER-SQL
        //                       ,:STB-DS-VER
        //                       ,:STB-DS-TS-INI-CPTZ
        //                       ,:STB-DS-TS-END-CPTZ
        //                       ,:STB-DS-UTENTE
        //                       ,:STB-DS-STATO-ELAB
        //                  FROM ADES      A,
        //                       POLI      B,
        //                       STAT_OGG_BUS C
        //                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
        //                                             :WS-TP-FRM-ASSVA2 )
        //                    AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
        //                                             :WLB-IB-POLI-LAST
        //                    AND A.ID_POLI      =   B.ID_POLI
        //                    AND A.ID_ADES      =   C.ID_OGG
        //                    AND C.TP_OGG       =  'AD'
        //           *--  D C.TP_STAT_BUS     =  'VI'
        //                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
        //                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
        //                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
        //                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
        //                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
        //                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                    AND A.ID_ADES IN
        //                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
        //                                                        STAT_OGG_BUS E
        //                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
        //                        AND E.TP_OGG = 'TG'
        //                        AND E.TP_STAT_BUS IN ('VI','ST')
        //                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
        //                        AND D.COD_COMP_ANIA =
        //                            :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
        //                        AND D.DT_END_EFF >    :WS-DT-PTF-X
        //                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
        //                        AND E.DT_END_EFF >    :WS-DT-PTF-X
        //                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
        //                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
        //                        )
        //                    AND A.DS_STATO_ELAB IN (
        //                                            :IABV0002-STATE-01,
        //                                            :IABV0002-STATE-02,
        //                                            :IABV0002-STATE-03,
        //                                            :IABV0002-STATE-04,
        //                                            :IABV0002-STATE-05,
        //                                            :IABV0002-STATE-06,
        //                                            :IABV0002-STATE-07,
        //                                            :IABV0002-STATE-08,
        //                                            :IABV0002-STATE-09,
        //                                            :IABV0002-STATE-10
        //                                              )
        //                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
        //                    ORDER BY A.ID_POLI, A.ID_ADES
        //                   FETCH FIRST ROW ONLY
        //                END-EXEC.
        adesPoliDao.selectRec8(this.getAdesPoliLdbm02501());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A320-UPDATE-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320UpdateSc07() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK-SC07         THRU A330-SC07-EX
        //              WHEN IDSV0003-WHERE-CONDITION-07
        //                   PERFORM A340-UPDATE-WHERE-COND-SC07 THRU A340-SC07-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                       THRU A345-SC07-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
            // COB_CODE: PERFORM A330-UPDATE-PK-SC07         THRU A330-SC07-EX
            a330UpdatePkSc07();
        }
        else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition07()) {
            // COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC07 THRU A340-SC07-EX
            a340UpdateWhereCondSc07();
        }
        else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
            // COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC07
            //                                               THRU A345-SC07-EX
            a345UpdateFirstActionSc07();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidLevelOper();
        }
    }

    /**Original name: A330-UPDATE-PK-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePkSc07() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=9369, because the code is unreachable.
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE ADES
        //                SET
        //                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
        //                  ,DS_VER                 = :IABV0009-VERSIONING
        //                  ,DS_UTENTE              = :ADE-DS-UTENTE
        //                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
        //                WHERE             DS_RIGA = :ADE-DS-RIGA
        //           END-EXEC.
        adesDao.updateRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A340-UPDATE-WHERE-COND-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateWhereCondSc07() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A345-UPDATE-FIRST-ACTION-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a345UpdateFirstActionSc07() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A360-OPEN-CURSOR-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursorSc07() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-SC07 THRU A305-SC07-EX.
        a305DeclareCursorSc07();
        // COB_CODE: EXEC SQL
        //               OPEN CUR-SC07
        //           END-EXEC.
        adesPoliDao.openCurSc07(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursorSc07() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-SC07
        //           END-EXEC.
        adesPoliDao.closeCurSc07();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirstSc07() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-SC07    THRU A360-SC07-EX.
        a360OpenCursorSc07();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT-SC07  THRU A390-SC07-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC07  THRU A390-SC07-EX
            a390FetchNextSc07();
        }
    }

    /**Original name: A390-FETCH-NEXT-SC07<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNextSc07() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-SC07
        //           INTO
        //                   :ADE-ID-ADES
        //                  ,:ADE-ID-POLI
        //                  ,:ADE-ID-MOVI-CRZ
        //                  ,:ADE-ID-MOVI-CHIU
        //                   :IND-ADE-ID-MOVI-CHIU
        //                  ,:ADE-DT-INI-EFF-DB
        //                  ,:ADE-DT-END-EFF-DB
        //                  ,:ADE-IB-PREV
        //                   :IND-ADE-IB-PREV
        //                  ,:ADE-IB-OGG
        //                   :IND-ADE-IB-OGG
        //                  ,:ADE-COD-COMP-ANIA
        //                  ,:ADE-DT-DECOR-DB
        //                   :IND-ADE-DT-DECOR
        //                  ,:ADE-DT-SCAD-DB
        //                   :IND-ADE-DT-SCAD
        //                  ,:ADE-ETA-A-SCAD
        //                   :IND-ADE-ETA-A-SCAD
        //                  ,:ADE-DUR-AA
        //                   :IND-ADE-DUR-AA
        //                  ,:ADE-DUR-MM
        //                   :IND-ADE-DUR-MM
        //                  ,:ADE-DUR-GG
        //                   :IND-ADE-DUR-GG
        //                  ,:ADE-TP-RGM-FISC
        //                  ,:ADE-TP-RIAT
        //                   :IND-ADE-TP-RIAT
        //                  ,:ADE-TP-MOD-PAG-TIT
        //                  ,:ADE-TP-IAS
        //                   :IND-ADE-TP-IAS
        //                  ,:ADE-DT-VARZ-TP-IAS-DB
        //                   :IND-ADE-DT-VARZ-TP-IAS
        //                  ,:ADE-PRE-NET-IND
        //                   :IND-ADE-PRE-NET-IND
        //                  ,:ADE-PRE-LRD-IND
        //                   :IND-ADE-PRE-LRD-IND
        //                  ,:ADE-RAT-LRD-IND
        //                   :IND-ADE-RAT-LRD-IND
        //                  ,:ADE-PRSTZ-INI-IND
        //                   :IND-ADE-PRSTZ-INI-IND
        //                  ,:ADE-FL-COINC-ASSTO
        //                   :IND-ADE-FL-COINC-ASSTO
        //                  ,:ADE-IB-DFLT
        //                   :IND-ADE-IB-DFLT
        //                  ,:ADE-MOD-CALC
        //                   :IND-ADE-MOD-CALC
        //                  ,:ADE-TP-FNT-CNBTVA
        //                   :IND-ADE-TP-FNT-CNBTVA
        //                  ,:ADE-IMP-AZ
        //                   :IND-ADE-IMP-AZ
        //                  ,:ADE-IMP-ADER
        //                   :IND-ADE-IMP-ADER
        //                  ,:ADE-IMP-TFR
        //                   :IND-ADE-IMP-TFR
        //                  ,:ADE-IMP-VOLO
        //                   :IND-ADE-IMP-VOLO
        //                  ,:ADE-PC-AZ
        //                   :IND-ADE-PC-AZ
        //                  ,:ADE-PC-ADER
        //                   :IND-ADE-PC-ADER
        //                  ,:ADE-PC-TFR
        //                   :IND-ADE-PC-TFR
        //                  ,:ADE-PC-VOLO
        //                   :IND-ADE-PC-VOLO
        //                  ,:ADE-DT-NOVA-RGM-FISC-DB
        //                   :IND-ADE-DT-NOVA-RGM-FISC
        //                  ,:ADE-FL-ATTIV
        //                   :IND-ADE-FL-ATTIV
        //                  ,:ADE-IMP-REC-RIT-VIS
        //                   :IND-ADE-IMP-REC-RIT-VIS
        //                  ,:ADE-IMP-REC-RIT-ACC
        //                   :IND-ADE-IMP-REC-RIT-ACC
        //                  ,:ADE-FL-VARZ-STAT-TBGC
        //                   :IND-ADE-FL-VARZ-STAT-TBGC
        //                  ,:ADE-FL-PROVZA-MIGRAZ
        //                   :IND-ADE-FL-PROVZA-MIGRAZ
        //                  ,:ADE-IMPB-VIS-DA-REC
        //                   :IND-ADE-IMPB-VIS-DA-REC
        //                  ,:ADE-DT-DECOR-PREST-BAN-DB
        //                   :IND-ADE-DT-DECOR-PREST-BAN
        //                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                   :IND-ADE-DT-EFF-VARZ-STAT-T
        //                  ,:ADE-DS-RIGA
        //                  ,:ADE-DS-OPER-SQL
        //                  ,:ADE-DS-VER
        //                  ,:ADE-DS-TS-INI-CPTZ
        //                  ,:ADE-DS-TS-END-CPTZ
        //                  ,:ADE-DS-UTENTE
        //                  ,:ADE-DS-STATO-ELAB
        //                  ,:ADE-CUM-CNBT-CAP
        //                   :IND-ADE-CUM-CNBT-CAP
        //                  ,:ADE-IMP-GAR-CNBT
        //                   :IND-ADE-IMP-GAR-CNBT
        //                  ,:ADE-DT-ULT-CONS-CNBT-DB
        //                   :IND-ADE-DT-ULT-CONS-CNBT
        //                  ,:ADE-IDEN-ISC-FND
        //                   :IND-ADE-IDEN-ISC-FND
        //                  ,:ADE-NUM-RAT-PIAN
        //                   :IND-ADE-NUM-RAT-PIAN
        //                  ,:ADE-DT-PRESC-DB
        //                   :IND-ADE-DT-PRESC
        //                  ,:ADE-CONCS-PREST
        //                   :IND-ADE-CONCS-PREST
        //                  ,:POL-ID-POLI
        //                  ,:POL-ID-MOVI-CRZ
        //                  ,:POL-ID-MOVI-CHIU
        //                   :IND-POL-ID-MOVI-CHIU
        //                  ,:POL-IB-OGG
        //                   :IND-POL-IB-OGG
        //                  ,:POL-IB-PROP
        //                  ,:POL-DT-PROP-DB
        //                   :IND-POL-DT-PROP
        //                  ,:POL-DT-INI-EFF-DB
        //                  ,:POL-DT-END-EFF-DB
        //                  ,:POL-COD-COMP-ANIA
        //                  ,:POL-DT-DECOR-DB
        //                  ,:POL-DT-EMIS-DB
        //                  ,:POL-TP-POLI
        //                  ,:POL-DUR-AA
        //                   :IND-POL-DUR-AA
        //                  ,:POL-DUR-MM
        //                   :IND-POL-DUR-MM
        //                  ,:POL-DT-SCAD-DB
        //                   :IND-POL-DT-SCAD
        //                  ,:POL-COD-PROD
        //                  ,:POL-DT-INI-VLDT-PROD-DB
        //                  ,:POL-COD-CONV
        //                   :IND-POL-COD-CONV
        //                  ,:POL-COD-RAMO
        //                   :IND-POL-COD-RAMO
        //                  ,:POL-DT-INI-VLDT-CONV-DB
        //                   :IND-POL-DT-INI-VLDT-CONV
        //                  ,:POL-DT-APPLZ-CONV-DB
        //                   :IND-POL-DT-APPLZ-CONV
        //                  ,:POL-TP-FRM-ASSVA
        //                  ,:POL-TP-RGM-FISC
        //                   :IND-POL-TP-RGM-FISC
        //                  ,:POL-FL-ESTAS
        //                   :IND-POL-FL-ESTAS
        //                  ,:POL-FL-RSH-COMUN
        //                   :IND-POL-FL-RSH-COMUN
        //                  ,:POL-FL-RSH-COMUN-COND
        //                   :IND-POL-FL-RSH-COMUN-COND
        //                  ,:POL-TP-LIV-GENZ-TIT
        //                  ,:POL-FL-COP-FINANZ
        //                   :IND-POL-FL-COP-FINANZ
        //                  ,:POL-TP-APPLZ-DIR
        //                   :IND-POL-TP-APPLZ-DIR
        //                  ,:POL-SPE-MED
        //                   :IND-POL-SPE-MED
        //                  ,:POL-DIR-EMIS
        //                   :IND-POL-DIR-EMIS
        //                  ,:POL-DIR-1O-VERS
        //                   :IND-POL-DIR-1O-VERS
        //                  ,:POL-DIR-VERS-AGG
        //                   :IND-POL-DIR-VERS-AGG
        //                  ,:POL-COD-DVS
        //                   :IND-POL-COD-DVS
        //                  ,:POL-FL-FNT-AZ
        //                   :IND-POL-FL-FNT-AZ
        //                  ,:POL-FL-FNT-ADER
        //                   :IND-POL-FL-FNT-ADER
        //                  ,:POL-FL-FNT-TFR
        //                   :IND-POL-FL-FNT-TFR
        //                  ,:POL-FL-FNT-VOLO
        //                   :IND-POL-FL-FNT-VOLO
        //                  ,:POL-TP-OPZ-A-SCAD
        //                   :IND-POL-TP-OPZ-A-SCAD
        //                  ,:POL-AA-DIFF-PROR-DFLT
        //                   :IND-POL-AA-DIFF-PROR-DFLT
        //                  ,:POL-FL-VER-PROD
        //                   :IND-POL-FL-VER-PROD
        //                  ,:POL-DUR-GG
        //                   :IND-POL-DUR-GG
        //                  ,:POL-DIR-QUIET
        //                   :IND-POL-DIR-QUIET
        //                  ,:POL-TP-PTF-ESTNO
        //                   :IND-POL-TP-PTF-ESTNO
        //                  ,:POL-FL-CUM-PRE-CNTR
        //                   :IND-POL-FL-CUM-PRE-CNTR
        //                  ,:POL-FL-AMMB-MOVI
        //                   :IND-POL-FL-AMMB-MOVI
        //                  ,:POL-CONV-GECO
        //                   :IND-POL-CONV-GECO
        //                  ,:POL-DS-RIGA
        //                  ,:POL-DS-OPER-SQL
        //                  ,:POL-DS-VER
        //                  ,:POL-DS-TS-INI-CPTZ
        //                  ,:POL-DS-TS-END-CPTZ
        //                  ,:POL-DS-UTENTE
        //                  ,:POL-DS-STATO-ELAB
        //                  ,:POL-FL-SCUDO-FISC
        //                   :IND-POL-FL-SCUDO-FISC
        //                  ,:POL-FL-TRASFE
        //                   :IND-POL-FL-TRASFE
        //                  ,:POL-FL-TFR-STRC
        //                   :IND-POL-FL-TFR-STRC
        //                  ,:POL-DT-PRESC-DB
        //                   :IND-POL-DT-PRESC
        //                  ,:POL-COD-CONV-AGG
        //                   :IND-POL-COD-CONV-AGG
        //                  ,:POL-SUBCAT-PROD
        //                   :IND-POL-SUBCAT-PROD
        //                  ,:POL-FL-QUEST-ADEGZ-ASS
        //                   :IND-POL-FL-QUEST-ADEGZ-ASS
        //                  ,:POL-COD-TPA
        //                   :IND-POL-COD-TPA
        //                  ,:POL-ID-ACC-COMM
        //                   :IND-POL-ID-ACC-COMM
        //                  ,:POL-FL-POLI-CPI-PR
        //                   :IND-POL-FL-POLI-CPI-PR
        //                  ,:POL-FL-POLI-BUNDLING
        //                   :IND-POL-FL-POLI-BUNDLING
        //                  ,:POL-IND-POLI-PRIN-COLL
        //                   :IND-POL-IND-POLI-PRIN-COLL
        //                  ,:POL-FL-VND-BUNDLE
        //                   :IND-POL-FL-VND-BUNDLE
        //                  ,:POL-IB-BS
        //                   :IND-POL-IB-BS
        //                  ,:POL-FL-POLI-IFP
        //                   :IND-POL-FL-POLI-IFP
        //                  ,:STB-ID-STAT-OGG-BUS
        //                  ,:STB-ID-OGG
        //                  ,:STB-TP-OGG
        //                  ,:STB-ID-MOVI-CRZ
        //                  ,:STB-ID-MOVI-CHIU
        //                   :IND-STB-ID-MOVI-CHIU
        //                  ,:STB-DT-INI-EFF-DB
        //                  ,:STB-DT-END-EFF-DB
        //                  ,:STB-COD-COMP-ANIA
        //                  ,:STB-TP-STAT-BUS
        //                  ,:STB-TP-CAUS
        //                  ,:STB-DS-RIGA
        //                  ,:STB-DS-OPER-SQL
        //                  ,:STB-DS-VER
        //                  ,:STB-DS-TS-INI-CPTZ
        //                  ,:STB-DS-TS-END-CPTZ
        //                  ,:STB-DS-UTENTE
        //                  ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        adesPoliDao.fetchCurSc07(this.getAdesPoliLdbm0250());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC07 THRU A370-SC07-EX
            a370CloseCursorSc07();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: SC08-SELECTION-CURSOR-08<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
    private void sc08SelectionCursor08() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-SC08           THRU A310-SC08-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A360-OPEN-CURSOR-SC08      THRU A360-SC08-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A370-CLOSE-CURSOR-SC08     THRU A370-SC08-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A380-FETCH-FIRST-SC08      THRU A380-SC08-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A390-FETCH-NEXT-SC08       THRU A390-SC08-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A320-UPDATE-SC08           THRU A320-SC08-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER          TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-SC08           THRU A310-SC08-EX
            a310SelectSc08();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A360-OPEN-CURSOR-SC08      THRU A360-SC08-EX
            a360OpenCursorSc08();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC08     THRU A370-SC08-EX
            a370CloseCursorSc08();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A380-FETCH-FIRST-SC08      THRU A380-SC08-EX
            a380FetchFirstSc08();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC08       THRU A390-SC08-EX
            a390FetchNextSc08();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A320-UPDATE-SC08           THRU A320-SC08-EX
            a320UpdateSc08();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER          TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A305-DECLARE-CURSOR-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a305DeclareCursorSc08() {
    // COB_CODE:      EXEC SQL
    //                     DECLARE CUR-SC08 CURSOR WITH HOLD FOR
    //                  SELECT
    //                         A.ID_ADES
    //                        ,A.ID_POLI
    //                        ,A.ID_MOVI_CRZ
    //                        ,A.ID_MOVI_CHIU
    //                        ,A.DT_INI_EFF
    //                        ,A.DT_END_EFF
    //                        ,A.IB_PREV
    //                        ,A.IB_OGG
    //                        ,A.COD_COMP_ANIA
    //                        ,A.DT_DECOR
    //                        ,A.DT_SCAD
    //                        ,A.ETA_A_SCAD
    //                        ,A.DUR_AA
    //                        ,A.DUR_MM
    //                        ,A.DUR_GG
    //                        ,A.TP_RGM_FISC
    //                        ,A.TP_RIAT
    //                        ,A.TP_MOD_PAG_TIT
    //                        ,A.TP_IAS
    //                        ,A.DT_VARZ_TP_IAS
    //                        ,A.PRE_NET_IND
    //                        ,A.PRE_LRD_IND
    //                        ,A.RAT_LRD_IND
    //                        ,A.PRSTZ_INI_IND
    //                        ,A.FL_COINC_ASSTO
    //                        ,A.IB_DFLT
    //                        ,A.MOD_CALC
    //                        ,A.TP_FNT_CNBTVA
    //                        ,A.IMP_AZ
    //                        ,A.IMP_ADER
    //                        ,A.IMP_TFR
    //                        ,A.IMP_VOLO
    //                        ,A.PC_AZ
    //                        ,A.PC_ADER
    //                        ,A.PC_TFR
    //                        ,A.PC_VOLO
    //                        ,A.DT_NOVA_RGM_FISC
    //                        ,A.FL_ATTIV
    //                        ,A.IMP_REC_RIT_VIS
    //                        ,A.IMP_REC_RIT_ACC
    //                        ,A.FL_VARZ_STAT_TBGC
    //                        ,A.FL_PROVZA_MIGRAZ
    //                        ,A.IMPB_VIS_DA_REC
    //                        ,A.DT_DECOR_PREST_BAN
    //                        ,A.DT_EFF_VARZ_STAT_T
    //                        ,A.DS_RIGA
    //                        ,A.DS_OPER_SQL
    //                        ,A.DS_VER
    //                        ,A.DS_TS_INI_CPTZ
    //                        ,A.DS_TS_END_CPTZ
    //                        ,A.DS_UTENTE
    //                        ,A.DS_STATO_ELAB
    //                        ,A.CUM_CNBT_CAP
    //                        ,A.IMP_GAR_CNBT
    //                        ,A.DT_ULT_CONS_CNBT
    //                        ,A.IDEN_ISC_FND
    //                        ,A.NUM_RAT_PIAN
    //                        ,A.DT_PRESC
    //                        ,A.CONCS_PREST
    //                        ,B.ID_POLI
    //                        ,B.ID_MOVI_CRZ
    //                        ,B.ID_MOVI_CHIU
    //                        ,B.IB_OGG
    //                        ,B.IB_PROP
    //                        ,B.DT_PROP
    //                        ,B.DT_INI_EFF
    //                        ,B.DT_END_EFF
    //                        ,B.COD_COMP_ANIA
    //                        ,B.DT_DECOR
    //                        ,B.DT_EMIS
    //                        ,B.TP_POLI
    //                        ,B.DUR_AA
    //                        ,B.DUR_MM
    //                        ,B.DT_SCAD
    //                        ,B.COD_PROD
    //                        ,B.DT_INI_VLDT_PROD
    //                        ,B.COD_CONV
    //                        ,B.COD_RAMO
    //                        ,B.DT_INI_VLDT_CONV
    //                        ,B.DT_APPLZ_CONV
    //                        ,B.TP_FRM_ASSVA
    //                        ,B.TP_RGM_FISC
    //                        ,B.FL_ESTAS
    //                        ,B.FL_RSH_COMUN
    //                        ,B.FL_RSH_COMUN_COND
    //                        ,B.TP_LIV_GENZ_TIT
    //                        ,B.FL_COP_FINANZ
    //                        ,B.TP_APPLZ_DIR
    //                        ,B.SPE_MED
    //                        ,B.DIR_EMIS
    //                        ,B.DIR_1O_VERS
    //                        ,B.DIR_VERS_AGG
    //                        ,B.COD_DVS
    //                        ,B.FL_FNT_AZ
    //                        ,B.FL_FNT_ADER
    //                        ,B.FL_FNT_TFR
    //                        ,B.FL_FNT_VOLO
    //                        ,B.TP_OPZ_A_SCAD
    //                        ,B.AA_DIFF_PROR_DFLT
    //                        ,B.FL_VER_PROD
    //                        ,B.DUR_GG
    //                        ,B.DIR_QUIET
    //                        ,B.TP_PTF_ESTNO
    //                        ,B.FL_CUM_PRE_CNTR
    //                        ,B.FL_AMMB_MOVI
    //                        ,B.CONV_GECO
    //                        ,B.DS_RIGA
    //                        ,B.DS_OPER_SQL
    //                        ,B.DS_VER
    //                        ,B.DS_TS_INI_CPTZ
    //                        ,B.DS_TS_END_CPTZ
    //                        ,B.DS_UTENTE
    //                        ,B.DS_STATO_ELAB
    //                        ,B.FL_SCUDO_FISC
    //                        ,B.FL_TRASFE
    //                        ,B.FL_TFR_STRC
    //                        ,B.DT_PRESC
    //                        ,B.COD_CONV_AGG
    //                        ,B.SUBCAT_PROD
    //                        ,B.FL_QUEST_ADEGZ_ASS
    //                        ,B.COD_TPA
    //                        ,B.ID_ACC_COMM
    //                        ,B.FL_POLI_CPI_PR
    //                        ,B.FL_POLI_BUNDLING
    //                        ,B.IND_POLI_PRIN_COLL
    //                        ,B.FL_VND_BUNDLE
    //                        ,B.IB_BS
    //                        ,B.FL_POLI_IFP
    //                        ,C.ID_STAT_OGG_BUS
    //                        ,C.ID_OGG
    //                        ,C.TP_OGG
    //                        ,C.ID_MOVI_CRZ
    //                        ,C.ID_MOVI_CHIU
    //                        ,C.DT_INI_EFF
    //                        ,C.DT_END_EFF
    //                        ,C.COD_COMP_ANIA
    //                        ,C.TP_STAT_BUS
    //                        ,C.TP_CAUS
    //                        ,C.DS_RIGA
    //                        ,C.DS_OPER_SQL
    //                        ,C.DS_VER
    //                        ,C.DS_TS_INI_CPTZ
    //                        ,C.DS_TS_END_CPTZ
    //                        ,C.DS_UTENTE
    //                        ,C.DS_STATO_ELAB
    //                  FROM ADES      A,
    //                       POLI      B,
    //                       STAT_OGG_BUS C
    //                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
    //                                             :WS-TP-FRM-ASSVA2 )
    //                    AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
    //                                             :WLB-IB-POLI-LAST
    //                    AND A.IB_OGG BETWEEN  :WLB-IB-ADE-FIRST AND
    //                                             :WLB-IB-ADE-LAST
    //                    AND A.ID_POLI      =   B.ID_POLI
    //                    AND A.ID_ADES      =   C.ID_OGG
    //                    AND C.TP_OGG       =  'AD'
    //           *--  D C.TP_STAT_BUS     =  'VI'
    //                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
    //                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
    //                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
    //                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
    //                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
    //                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
    //                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
    //                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
    //                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                    AND A.ID_ADES IN
    //                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
    //                                                        STAT_OGG_BUS E
    //                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
    //                        AND E.TP_OGG = 'TG'
    //                        AND E.TP_STAT_BUS IN ('VI','ST')
    //                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
    //                        AND D.COD_COMP_ANIA =
    //                            :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
    //                        AND D.DT_END_EFF >    :WS-DT-PTF-X
    //                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
    //                        AND E.DT_END_EFF >    :WS-DT-PTF-X
    //                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
    //                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
    //                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
    //                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
    //                        )
    //                    AND A.DS_STATO_ELAB IN (
    //                                            :IABV0002-STATE-01,
    //                                            :IABV0002-STATE-02,
    //                                            :IABV0002-STATE-03,
    //                                            :IABV0002-STATE-04,
    //                                            :IABV0002-STATE-05,
    //                                            :IABV0002-STATE-06,
    //                                            :IABV0002-STATE-07,
    //                                            :IABV0002-STATE-08,
    //                                            :IABV0002-STATE-09,
    //                                            :IABV0002-STATE-10
    //                                              )
    //                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
    //                    ORDER BY A.ID_POLI, A.ID_ADES
    //                END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310SelectSc08() {
        // COB_CODE:      EXEC SQL
        //                  SELECT
        //                         A.ID_ADES
        //                        ,A.ID_POLI
        //                        ,A.ID_MOVI_CRZ
        //                        ,A.ID_MOVI_CHIU
        //                        ,A.DT_INI_EFF
        //                        ,A.DT_END_EFF
        //                        ,A.IB_PREV
        //                        ,A.IB_OGG
        //                        ,A.COD_COMP_ANIA
        //                        ,A.DT_DECOR
        //                        ,A.DT_SCAD
        //                        ,A.ETA_A_SCAD
        //                        ,A.DUR_AA
        //                        ,A.DUR_MM
        //                        ,A.DUR_GG
        //                        ,A.TP_RGM_FISC
        //                        ,A.TP_RIAT
        //                        ,A.TP_MOD_PAG_TIT
        //                        ,A.TP_IAS
        //                        ,A.DT_VARZ_TP_IAS
        //                        ,A.PRE_NET_IND
        //                        ,A.PRE_LRD_IND
        //                        ,A.RAT_LRD_IND
        //                        ,A.PRSTZ_INI_IND
        //                        ,A.FL_COINC_ASSTO
        //                        ,A.IB_DFLT
        //                        ,A.MOD_CALC
        //                        ,A.TP_FNT_CNBTVA
        //                        ,A.IMP_AZ
        //                        ,A.IMP_ADER
        //                        ,A.IMP_TFR
        //                        ,A.IMP_VOLO
        //                        ,A.PC_AZ
        //                        ,A.PC_ADER
        //                        ,A.PC_TFR
        //                        ,A.PC_VOLO
        //                        ,A.DT_NOVA_RGM_FISC
        //                        ,A.FL_ATTIV
        //                        ,A.IMP_REC_RIT_VIS
        //                        ,A.IMP_REC_RIT_ACC
        //                        ,A.FL_VARZ_STAT_TBGC
        //                        ,A.FL_PROVZA_MIGRAZ
        //                        ,A.IMPB_VIS_DA_REC
        //                        ,A.DT_DECOR_PREST_BAN
        //                        ,A.DT_EFF_VARZ_STAT_T
        //                        ,A.DS_RIGA
        //                        ,A.DS_OPER_SQL
        //                        ,A.DS_VER
        //                        ,A.DS_TS_INI_CPTZ
        //                        ,A.DS_TS_END_CPTZ
        //                        ,A.DS_UTENTE
        //                        ,A.DS_STATO_ELAB
        //                        ,A.CUM_CNBT_CAP
        //                        ,A.IMP_GAR_CNBT
        //                        ,A.DT_ULT_CONS_CNBT
        //                        ,A.IDEN_ISC_FND
        //                        ,A.NUM_RAT_PIAN
        //                        ,A.DT_PRESC
        //                        ,A.CONCS_PREST
        //                        ,B.ID_POLI
        //                        ,B.ID_MOVI_CRZ
        //                        ,B.ID_MOVI_CHIU
        //                        ,B.IB_OGG
        //                        ,B.IB_PROP
        //                        ,B.DT_PROP
        //                        ,B.DT_INI_EFF
        //                        ,B.DT_END_EFF
        //                        ,B.COD_COMP_ANIA
        //                        ,B.DT_DECOR
        //                        ,B.DT_EMIS
        //                        ,B.TP_POLI
        //                        ,B.DUR_AA
        //                        ,B.DUR_MM
        //                        ,B.DT_SCAD
        //                        ,B.COD_PROD
        //                        ,B.DT_INI_VLDT_PROD
        //                        ,B.COD_CONV
        //                        ,B.COD_RAMO
        //                        ,B.DT_INI_VLDT_CONV
        //                        ,B.DT_APPLZ_CONV
        //                        ,B.TP_FRM_ASSVA
        //                        ,B.TP_RGM_FISC
        //                        ,B.FL_ESTAS
        //                        ,B.FL_RSH_COMUN
        //                        ,B.FL_RSH_COMUN_COND
        //                        ,B.TP_LIV_GENZ_TIT
        //                        ,B.FL_COP_FINANZ
        //                        ,B.TP_APPLZ_DIR
        //                        ,B.SPE_MED
        //                        ,B.DIR_EMIS
        //                        ,B.DIR_1O_VERS
        //                        ,B.DIR_VERS_AGG
        //                        ,B.COD_DVS
        //                        ,B.FL_FNT_AZ
        //                        ,B.FL_FNT_ADER
        //                        ,B.FL_FNT_TFR
        //                        ,B.FL_FNT_VOLO
        //                        ,B.TP_OPZ_A_SCAD
        //                        ,B.AA_DIFF_PROR_DFLT
        //                        ,B.FL_VER_PROD
        //                        ,B.DUR_GG
        //                        ,B.DIR_QUIET
        //                        ,B.TP_PTF_ESTNO
        //                        ,B.FL_CUM_PRE_CNTR
        //                        ,B.FL_AMMB_MOVI
        //                        ,B.CONV_GECO
        //                        ,B.DS_RIGA
        //                        ,B.DS_OPER_SQL
        //                        ,B.DS_VER
        //                        ,B.DS_TS_INI_CPTZ
        //                        ,B.DS_TS_END_CPTZ
        //                        ,B.DS_UTENTE
        //                        ,B.DS_STATO_ELAB
        //                        ,B.FL_SCUDO_FISC
        //                        ,B.FL_TRASFE
        //                        ,B.FL_TFR_STRC
        //                        ,B.DT_PRESC
        //                        ,B.COD_CONV_AGG
        //                        ,B.SUBCAT_PROD
        //                        ,B.FL_QUEST_ADEGZ_ASS
        //                        ,B.COD_TPA
        //                        ,B.ID_ACC_COMM
        //                        ,B.FL_POLI_CPI_PR
        //                        ,B.FL_POLI_BUNDLING
        //                        ,B.IND_POLI_PRIN_COLL
        //                        ,B.FL_VND_BUNDLE
        //                        ,B.IB_BS
        //                        ,B.FL_POLI_IFP
        //                        ,C.ID_STAT_OGG_BUS
        //                        ,C.ID_OGG
        //                        ,C.TP_OGG
        //                        ,C.ID_MOVI_CRZ
        //                        ,C.ID_MOVI_CHIU
        //                        ,C.DT_INI_EFF
        //                        ,C.DT_END_EFF
        //                        ,C.COD_COMP_ANIA
        //                        ,C.TP_STAT_BUS
        //                        ,C.TP_CAUS
        //                        ,C.DS_RIGA
        //                        ,C.DS_OPER_SQL
        //                        ,C.DS_VER
        //                        ,C.DS_TS_INI_CPTZ
        //                        ,C.DS_TS_END_CPTZ
        //                        ,C.DS_UTENTE
        //                        ,C.DS_STATO_ELAB
        //                  INTO
        //                        :ADE-ID-ADES
        //                       ,:ADE-ID-POLI
        //                       ,:ADE-ID-MOVI-CRZ
        //                       ,:ADE-ID-MOVI-CHIU
        //                        :IND-ADE-ID-MOVI-CHIU
        //                       ,:ADE-DT-INI-EFF-DB
        //                       ,:ADE-DT-END-EFF-DB
        //                       ,:ADE-IB-PREV
        //                        :IND-ADE-IB-PREV
        //                       ,:ADE-IB-OGG
        //                        :IND-ADE-IB-OGG
        //                       ,:ADE-COD-COMP-ANIA
        //                       ,:ADE-DT-DECOR-DB
        //                        :IND-ADE-DT-DECOR
        //                       ,:ADE-DT-SCAD-DB
        //                        :IND-ADE-DT-SCAD
        //                       ,:ADE-ETA-A-SCAD
        //                        :IND-ADE-ETA-A-SCAD
        //                       ,:ADE-DUR-AA
        //                        :IND-ADE-DUR-AA
        //                       ,:ADE-DUR-MM
        //                        :IND-ADE-DUR-MM
        //                       ,:ADE-DUR-GG
        //                        :IND-ADE-DUR-GG
        //                       ,:ADE-TP-RGM-FISC
        //                       ,:ADE-TP-RIAT
        //                        :IND-ADE-TP-RIAT
        //                       ,:ADE-TP-MOD-PAG-TIT
        //                       ,:ADE-TP-IAS
        //                        :IND-ADE-TP-IAS
        //                       ,:ADE-DT-VARZ-TP-IAS-DB
        //                        :IND-ADE-DT-VARZ-TP-IAS
        //                       ,:ADE-PRE-NET-IND
        //                        :IND-ADE-PRE-NET-IND
        //                       ,:ADE-PRE-LRD-IND
        //                        :IND-ADE-PRE-LRD-IND
        //                       ,:ADE-RAT-LRD-IND
        //                        :IND-ADE-RAT-LRD-IND
        //                       ,:ADE-PRSTZ-INI-IND
        //                        :IND-ADE-PRSTZ-INI-IND
        //                       ,:ADE-FL-COINC-ASSTO
        //                        :IND-ADE-FL-COINC-ASSTO
        //                       ,:ADE-IB-DFLT
        //                        :IND-ADE-IB-DFLT
        //                       ,:ADE-MOD-CALC
        //                        :IND-ADE-MOD-CALC
        //                       ,:ADE-TP-FNT-CNBTVA
        //                        :IND-ADE-TP-FNT-CNBTVA
        //                       ,:ADE-IMP-AZ
        //                        :IND-ADE-IMP-AZ
        //                       ,:ADE-IMP-ADER
        //                        :IND-ADE-IMP-ADER
        //                       ,:ADE-IMP-TFR
        //                        :IND-ADE-IMP-TFR
        //                       ,:ADE-IMP-VOLO
        //                        :IND-ADE-IMP-VOLO
        //                       ,:ADE-PC-AZ
        //                        :IND-ADE-PC-AZ
        //                       ,:ADE-PC-ADER
        //                        :IND-ADE-PC-ADER
        //                       ,:ADE-PC-TFR
        //                        :IND-ADE-PC-TFR
        //                       ,:ADE-PC-VOLO
        //                        :IND-ADE-PC-VOLO
        //                       ,:ADE-DT-NOVA-RGM-FISC-DB
        //                        :IND-ADE-DT-NOVA-RGM-FISC
        //                       ,:ADE-FL-ATTIV
        //                        :IND-ADE-FL-ATTIV
        //                       ,:ADE-IMP-REC-RIT-VIS
        //                        :IND-ADE-IMP-REC-RIT-VIS
        //                       ,:ADE-IMP-REC-RIT-ACC
        //                        :IND-ADE-IMP-REC-RIT-ACC
        //                       ,:ADE-FL-VARZ-STAT-TBGC
        //                        :IND-ADE-FL-VARZ-STAT-TBGC
        //                       ,:ADE-FL-PROVZA-MIGRAZ
        //                        :IND-ADE-FL-PROVZA-MIGRAZ
        //                       ,:ADE-IMPB-VIS-DA-REC
        //                        :IND-ADE-IMPB-VIS-DA-REC
        //                       ,:ADE-DT-DECOR-PREST-BAN-DB
        //                        :IND-ADE-DT-DECOR-PREST-BAN
        //                       ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                        :IND-ADE-DT-EFF-VARZ-STAT-T
        //                       ,:ADE-DS-RIGA
        //                       ,:ADE-DS-OPER-SQL
        //                       ,:ADE-DS-VER
        //                       ,:ADE-DS-TS-INI-CPTZ
        //                       ,:ADE-DS-TS-END-CPTZ
        //                       ,:ADE-DS-UTENTE
        //                       ,:ADE-DS-STATO-ELAB
        //                       ,:ADE-CUM-CNBT-CAP
        //                        :IND-ADE-CUM-CNBT-CAP
        //                       ,:ADE-IMP-GAR-CNBT
        //                        :IND-ADE-IMP-GAR-CNBT
        //                       ,:ADE-DT-ULT-CONS-CNBT-DB
        //                        :IND-ADE-DT-ULT-CONS-CNBT
        //                       ,:ADE-IDEN-ISC-FND
        //                        :IND-ADE-IDEN-ISC-FND
        //                       ,:ADE-NUM-RAT-PIAN
        //                        :IND-ADE-NUM-RAT-PIAN
        //                       ,:ADE-DT-PRESC-DB
        //                        :IND-ADE-DT-PRESC
        //                       ,:ADE-CONCS-PREST
        //                        :IND-ADE-CONCS-PREST
        //                       ,:POL-ID-POLI
        //                       ,:POL-ID-MOVI-CRZ
        //                       ,:POL-ID-MOVI-CHIU
        //                        :IND-POL-ID-MOVI-CHIU
        //                       ,:POL-IB-OGG
        //                        :IND-POL-IB-OGG
        //                       ,:POL-IB-PROP
        //                       ,:POL-DT-PROP-DB
        //                        :IND-POL-DT-PROP
        //                       ,:POL-DT-INI-EFF-DB
        //                       ,:POL-DT-END-EFF-DB
        //                       ,:POL-COD-COMP-ANIA
        //                       ,:POL-DT-DECOR-DB
        //                       ,:POL-DT-EMIS-DB
        //                       ,:POL-TP-POLI
        //                       ,:POL-DUR-AA
        //                        :IND-POL-DUR-AA
        //                       ,:POL-DUR-MM
        //                        :IND-POL-DUR-MM
        //                       ,:POL-DT-SCAD-DB
        //                        :IND-POL-DT-SCAD
        //                       ,:POL-COD-PROD
        //                       ,:POL-DT-INI-VLDT-PROD-DB
        //                       ,:POL-COD-CONV
        //                        :IND-POL-COD-CONV
        //                       ,:POL-COD-RAMO
        //                        :IND-POL-COD-RAMO
        //                       ,:POL-DT-INI-VLDT-CONV-DB
        //                        :IND-POL-DT-INI-VLDT-CONV
        //                       ,:POL-DT-APPLZ-CONV-DB
        //                        :IND-POL-DT-APPLZ-CONV
        //                       ,:POL-TP-FRM-ASSVA
        //                       ,:POL-TP-RGM-FISC
        //                        :IND-POL-TP-RGM-FISC
        //                       ,:POL-FL-ESTAS
        //                        :IND-POL-FL-ESTAS
        //                       ,:POL-FL-RSH-COMUN
        //                        :IND-POL-FL-RSH-COMUN
        //                       ,:POL-FL-RSH-COMUN-COND
        //                        :IND-POL-FL-RSH-COMUN-COND
        //                       ,:POL-TP-LIV-GENZ-TIT
        //                       ,:POL-FL-COP-FINANZ
        //                        :IND-POL-FL-COP-FINANZ
        //                       ,:POL-TP-APPLZ-DIR
        //                        :IND-POL-TP-APPLZ-DIR
        //                       ,:POL-SPE-MED
        //                        :IND-POL-SPE-MED
        //                       ,:POL-DIR-EMIS
        //                        :IND-POL-DIR-EMIS
        //                       ,:POL-DIR-1O-VERS
        //                        :IND-POL-DIR-1O-VERS
        //                       ,:POL-DIR-VERS-AGG
        //                        :IND-POL-DIR-VERS-AGG
        //                       ,:POL-COD-DVS
        //                        :IND-POL-COD-DVS
        //                       ,:POL-FL-FNT-AZ
        //                        :IND-POL-FL-FNT-AZ
        //                       ,:POL-FL-FNT-ADER
        //                        :IND-POL-FL-FNT-ADER
        //                       ,:POL-FL-FNT-TFR
        //                        :IND-POL-FL-FNT-TFR
        //                       ,:POL-FL-FNT-VOLO
        //                        :IND-POL-FL-FNT-VOLO
        //                       ,:POL-TP-OPZ-A-SCAD
        //                        :IND-POL-TP-OPZ-A-SCAD
        //                       ,:POL-AA-DIFF-PROR-DFLT
        //                        :IND-POL-AA-DIFF-PROR-DFLT
        //                       ,:POL-FL-VER-PROD
        //                        :IND-POL-FL-VER-PROD
        //                       ,:POL-DUR-GG
        //                        :IND-POL-DUR-GG
        //                       ,:POL-DIR-QUIET
        //                        :IND-POL-DIR-QUIET
        //                       ,:POL-TP-PTF-ESTNO
        //                        :IND-POL-TP-PTF-ESTNO
        //                       ,:POL-FL-CUM-PRE-CNTR
        //                        :IND-POL-FL-CUM-PRE-CNTR
        //                       ,:POL-FL-AMMB-MOVI
        //                        :IND-POL-FL-AMMB-MOVI
        //                       ,:POL-CONV-GECO
        //                        :IND-POL-CONV-GECO
        //                       ,:POL-DS-RIGA
        //                       ,:POL-DS-OPER-SQL
        //                       ,:POL-DS-VER
        //                       ,:POL-DS-TS-INI-CPTZ
        //                       ,:POL-DS-TS-END-CPTZ
        //                       ,:POL-DS-UTENTE
        //                       ,:POL-DS-STATO-ELAB
        //                       ,:POL-FL-SCUDO-FISC
        //                        :IND-POL-FL-SCUDO-FISC
        //                       ,:POL-FL-TRASFE
        //                        :IND-POL-FL-TRASFE
        //                       ,:POL-FL-TFR-STRC
        //                        :IND-POL-FL-TFR-STRC
        //                       ,:POL-DT-PRESC-DB
        //                        :IND-POL-DT-PRESC
        //                       ,:POL-COD-CONV-AGG
        //                        :IND-POL-COD-CONV-AGG
        //                       ,:POL-SUBCAT-PROD
        //                        :IND-POL-SUBCAT-PROD
        //                       ,:POL-FL-QUEST-ADEGZ-ASS
        //                        :IND-POL-FL-QUEST-ADEGZ-ASS
        //                       ,:POL-COD-TPA
        //                        :IND-POL-COD-TPA
        //                       ,:POL-ID-ACC-COMM
        //                        :IND-POL-ID-ACC-COMM
        //                       ,:POL-FL-POLI-CPI-PR
        //                        :IND-POL-FL-POLI-CPI-PR
        //                       ,:POL-FL-POLI-BUNDLING
        //                        :IND-POL-FL-POLI-BUNDLING
        //                       ,:POL-IND-POLI-PRIN-COLL
        //                        :IND-POL-IND-POLI-PRIN-COLL
        //                       ,:POL-FL-VND-BUNDLE
        //                        :IND-POL-FL-VND-BUNDLE
        //                       ,:POL-IB-BS
        //                        :IND-POL-IB-BS
        //                       ,:POL-FL-POLI-IFP
        //                        :IND-POL-FL-POLI-IFP
        //                       ,:STB-ID-STAT-OGG-BUS
        //                       ,:STB-ID-OGG
        //                       ,:STB-TP-OGG
        //                       ,:STB-ID-MOVI-CRZ
        //                       ,:STB-ID-MOVI-CHIU
        //                        :IND-STB-ID-MOVI-CHIU
        //                       ,:STB-DT-INI-EFF-DB
        //                       ,:STB-DT-END-EFF-DB
        //                       ,:STB-COD-COMP-ANIA
        //                       ,:STB-TP-STAT-BUS
        //                       ,:STB-TP-CAUS
        //                       ,:STB-DS-RIGA
        //                       ,:STB-DS-OPER-SQL
        //                       ,:STB-DS-VER
        //                       ,:STB-DS-TS-INI-CPTZ
        //                       ,:STB-DS-TS-END-CPTZ
        //                       ,:STB-DS-UTENTE
        //                       ,:STB-DS-STATO-ELAB
        //                  FROM ADES      A,
        //                       POLI      B,
        //                       STAT_OGG_BUS C
        //                  WHERE B.TP_FRM_ASSVA IN (:WS-TP-FRM-ASSVA1,
        //                                             :WS-TP-FRM-ASSVA2 )
        //                    AND B.IB_OGG BETWEEN  :WLB-IB-POLI-FIRST AND
        //                                             :WLB-IB-POLI-LAST
        //                    AND A.IB_OGG BETWEEN  :WLB-IB-ADE-FIRST AND
        //                                             :WLB-IB-ADE-LAST
        //                    AND A.ID_POLI      =   B.ID_POLI
        //                    AND A.ID_ADES      =   C.ID_OGG
        //                    AND C.TP_OGG       =  'AD'
        //           *--  D C.TP_STAT_BUS     =  'VI'
        //                    AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                    AND A.DT_END_EFF  >   :WS-DT-PTF-X
        //                    AND A.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                    AND A.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                    AND A.COD_COMP_ANIA =  B.COD_COMP_ANIA
        //                    AND B.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                    AND B.DT_END_EFF  >   :WS-DT-PTF-X
        //                    AND B.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                    AND B.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                    AND B.COD_COMP_ANIA =  C.COD_COMP_ANIA
        //                    AND C.DT_INI_EFF  <=  :WS-DT-PTF-X
        //                    AND C.DT_END_EFF  >   :WS-DT-PTF-X
        //                    AND C.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                    AND C.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                    AND A.ID_ADES IN
        //                       (SELECT DISTINCT(D.ID_ADES) FROM TRCH_DI_GAR D ,
        //                                                        STAT_OGG_BUS E
        //                        WHERE D.ID_TRCH_DI_GAR = E.ID_OGG
        //                        AND E.TP_OGG = 'TG'
        //                        AND E.TP_STAT_BUS IN ('VI','ST')
        //                        AND D.COD_COMP_ANIA = E.COD_COMP_ANIA
        //                        AND D.COD_COMP_ANIA =
        //                            :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                        AND D.DT_INI_EFF <=   :WS-DT-PTF-X
        //                        AND D.DT_END_EFF >    :WS-DT-PTF-X
        //                        AND D.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                        AND D.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                        AND E.DT_INI_EFF <=   :WS-DT-PTF-X
        //                        AND E.DT_END_EFF >    :WS-DT-PTF-X
        //                        AND E.DS_TS_INI_CPTZ <= :WS-DT-TS-PTF
        //                        AND E.DS_TS_END_CPTZ > :WS-DT-TS-PTF
        //                        AND (D.DS_TS_INI_CPTZ >   :WS-DT-TS-PTF-PRE
        //                             OR E.DS_TS_INI_CPTZ > :WS-DT-TS-PTF-PRE)
        //                        )
        //                    AND A.DS_STATO_ELAB IN (
        //                                            :IABV0002-STATE-01,
        //                                            :IABV0002-STATE-02,
        //                                            :IABV0002-STATE-03,
        //                                            :IABV0002-STATE-04,
        //                                            :IABV0002-STATE-05,
        //                                            :IABV0002-STATE-06,
        //                                            :IABV0002-STATE-07,
        //                                            :IABV0002-STATE-08,
        //                                            :IABV0002-STATE-09,
        //                                            :IABV0002-STATE-10
        //                                              )
        //                    AND NOT A.DS_VER  = :IABV0009-VERSIONING
        //                    ORDER BY A.ID_POLI, A.ID_ADES
        //                   FETCH FIRST ROW ONLY
        //                END-EXEC.
        adesPoliDao.selectRec9(this.getAdesPoliLdbm02501());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A320-UPDATE-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a320UpdateSc08() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                   PERFORM A330-UPDATE-PK-SC08         THRU A330-SC08-EX
        //              WHEN IDSV0003-WHERE-CONDITION-08
        //                   PERFORM A340-UPDATE-WHERE-COND-SC08 THRU A340-SC08-EX
        //              WHEN IDSV0003-FIRST-ACTION
        //                                                       THRU A345-SC08-EX
        //              WHEN OTHER
        //                   SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getLivelloOperazione().isIdsv0003PrimaryKey()) {
            // COB_CODE: PERFORM A330-UPDATE-PK-SC08         THRU A330-SC08-EX
            a330UpdatePkSc08();
        }
        else if (idsv0003.getTipologiaOperazione().isIdsv0003WhereCondition08()) {
            // COB_CODE: PERFORM A340-UPDATE-WHERE-COND-SC08 THRU A340-SC08-EX
            a340UpdateWhereCondSc08();
        }
        else if (idsv0003.getLivelloOperazione().isIdsv0003FirstAction()) {
            // COB_CODE: PERFORM A345-UPDATE-FIRST-ACTION-SC08
            //                                               THRU A345-SC08-EX
            a345UpdateFirstActionSc08();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidLevelOper();
        }
    }

    /**Original name: A330-UPDATE-PK-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a330UpdatePkSc08() {
        // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES THRU Z150-EX.
        z150ValorizzaDataServices();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBM0250.cbl:line=10457, because the code is unreachable.
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE ADES
        //                SET
        //                   DS_OPER_SQL            = :ADE-DS-OPER-SQL
        //                  ,DS_VER                 = :IABV0009-VERSIONING
        //                  ,DS_UTENTE              = :ADE-DS-UTENTE
        //                  ,DS_STATO_ELAB          = :IABV0002-STATE-CURRENT
        //                WHERE             DS_RIGA = :ADE-DS-RIGA
        //           END-EXEC.
        adesDao.updateRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A340-UPDATE-WHERE-COND-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a340UpdateWhereCondSc08() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A345-UPDATE-FIRST-ACTION-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a345UpdateFirstActionSc08() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A360-OPEN-CURSOR-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a360OpenCursorSc08() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-SC08 THRU A305-SC08-EX.
        a305DeclareCursorSc08();
        // COB_CODE: EXEC SQL
        //                OPEN CUR-SC08
        //           END-EXEC.
        adesPoliDao.openCurSc08(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a370CloseCursorSc08() {
        // COB_CODE: EXEC SQL
        //               CLOSE CUR-SC08
        //           END-EXEC.
        adesPoliDao.closeCurSc08();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A380-FETCH-FIRST-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a380FetchFirstSc08() {
        // COB_CODE: PERFORM A360-OPEN-CURSOR-SC08    THRU A360-SC08-EX.
        a360OpenCursorSc08();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A390-FETCH-NEXT-SC08  THRU A390-SC08-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-SC08  THRU A390-SC08-EX
            a390FetchNextSc08();
        }
    }

    /**Original name: A390-FETCH-NEXT-SC08<br>
	 * <pre>*****************************************************************</pre>*/
    private void a390FetchNextSc08() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-SC08
        //           INTO
        //                   :ADE-ID-ADES
        //                  ,:ADE-ID-POLI
        //                  ,:ADE-ID-MOVI-CRZ
        //                  ,:ADE-ID-MOVI-CHIU
        //                   :IND-ADE-ID-MOVI-CHIU
        //                  ,:ADE-DT-INI-EFF-DB
        //                  ,:ADE-DT-END-EFF-DB
        //                  ,:ADE-IB-PREV
        //                   :IND-ADE-IB-PREV
        //                  ,:ADE-IB-OGG
        //                   :IND-ADE-IB-OGG
        //                  ,:ADE-COD-COMP-ANIA
        //                  ,:ADE-DT-DECOR-DB
        //                   :IND-ADE-DT-DECOR
        //                  ,:ADE-DT-SCAD-DB
        //                   :IND-ADE-DT-SCAD
        //                  ,:ADE-ETA-A-SCAD
        //                   :IND-ADE-ETA-A-SCAD
        //                  ,:ADE-DUR-AA
        //                   :IND-ADE-DUR-AA
        //                  ,:ADE-DUR-MM
        //                   :IND-ADE-DUR-MM
        //                  ,:ADE-DUR-GG
        //                   :IND-ADE-DUR-GG
        //                  ,:ADE-TP-RGM-FISC
        //                  ,:ADE-TP-RIAT
        //                   :IND-ADE-TP-RIAT
        //                  ,:ADE-TP-MOD-PAG-TIT
        //                  ,:ADE-TP-IAS
        //                   :IND-ADE-TP-IAS
        //                  ,:ADE-DT-VARZ-TP-IAS-DB
        //                   :IND-ADE-DT-VARZ-TP-IAS
        //                  ,:ADE-PRE-NET-IND
        //                   :IND-ADE-PRE-NET-IND
        //                  ,:ADE-PRE-LRD-IND
        //                   :IND-ADE-PRE-LRD-IND
        //                  ,:ADE-RAT-LRD-IND
        //                   :IND-ADE-RAT-LRD-IND
        //                  ,:ADE-PRSTZ-INI-IND
        //                   :IND-ADE-PRSTZ-INI-IND
        //                  ,:ADE-FL-COINC-ASSTO
        //                   :IND-ADE-FL-COINC-ASSTO
        //                  ,:ADE-IB-DFLT
        //                   :IND-ADE-IB-DFLT
        //                  ,:ADE-MOD-CALC
        //                   :IND-ADE-MOD-CALC
        //                  ,:ADE-TP-FNT-CNBTVA
        //                   :IND-ADE-TP-FNT-CNBTVA
        //                  ,:ADE-IMP-AZ
        //                   :IND-ADE-IMP-AZ
        //                  ,:ADE-IMP-ADER
        //                   :IND-ADE-IMP-ADER
        //                  ,:ADE-IMP-TFR
        //                   :IND-ADE-IMP-TFR
        //                  ,:ADE-IMP-VOLO
        //                   :IND-ADE-IMP-VOLO
        //                  ,:ADE-PC-AZ
        //                   :IND-ADE-PC-AZ
        //                  ,:ADE-PC-ADER
        //                   :IND-ADE-PC-ADER
        //                  ,:ADE-PC-TFR
        //                   :IND-ADE-PC-TFR
        //                  ,:ADE-PC-VOLO
        //                   :IND-ADE-PC-VOLO
        //                  ,:ADE-DT-NOVA-RGM-FISC-DB
        //                   :IND-ADE-DT-NOVA-RGM-FISC
        //                  ,:ADE-FL-ATTIV
        //                   :IND-ADE-FL-ATTIV
        //                  ,:ADE-IMP-REC-RIT-VIS
        //                   :IND-ADE-IMP-REC-RIT-VIS
        //                  ,:ADE-IMP-REC-RIT-ACC
        //                   :IND-ADE-IMP-REC-RIT-ACC
        //                  ,:ADE-FL-VARZ-STAT-TBGC
        //                   :IND-ADE-FL-VARZ-STAT-TBGC
        //                  ,:ADE-FL-PROVZA-MIGRAZ
        //                   :IND-ADE-FL-PROVZA-MIGRAZ
        //                  ,:ADE-IMPB-VIS-DA-REC
        //                   :IND-ADE-IMPB-VIS-DA-REC
        //                  ,:ADE-DT-DECOR-PREST-BAN-DB
        //                   :IND-ADE-DT-DECOR-PREST-BAN
        //                  ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                   :IND-ADE-DT-EFF-VARZ-STAT-T
        //                  ,:ADE-DS-RIGA
        //                  ,:ADE-DS-OPER-SQL
        //                  ,:ADE-DS-VER
        //                  ,:ADE-DS-TS-INI-CPTZ
        //                  ,:ADE-DS-TS-END-CPTZ
        //                  ,:ADE-DS-UTENTE
        //                  ,:ADE-DS-STATO-ELAB
        //                  ,:ADE-CUM-CNBT-CAP
        //                   :IND-ADE-CUM-CNBT-CAP
        //                  ,:ADE-IMP-GAR-CNBT
        //                   :IND-ADE-IMP-GAR-CNBT
        //                  ,:ADE-DT-ULT-CONS-CNBT-DB
        //                   :IND-ADE-DT-ULT-CONS-CNBT
        //                  ,:ADE-IDEN-ISC-FND
        //                   :IND-ADE-IDEN-ISC-FND
        //                  ,:ADE-NUM-RAT-PIAN
        //                   :IND-ADE-NUM-RAT-PIAN
        //                  ,:ADE-DT-PRESC-DB
        //                   :IND-ADE-DT-PRESC
        //                  ,:ADE-CONCS-PREST
        //                   :IND-ADE-CONCS-PREST
        //                  ,:POL-ID-POLI
        //                  ,:POL-ID-MOVI-CRZ
        //                  ,:POL-ID-MOVI-CHIU
        //                   :IND-POL-ID-MOVI-CHIU
        //                  ,:POL-IB-OGG
        //                   :IND-POL-IB-OGG
        //                  ,:POL-IB-PROP
        //                  ,:POL-DT-PROP-DB
        //                   :IND-POL-DT-PROP
        //                  ,:POL-DT-INI-EFF-DB
        //                  ,:POL-DT-END-EFF-DB
        //                  ,:POL-COD-COMP-ANIA
        //                  ,:POL-DT-DECOR-DB
        //                  ,:POL-DT-EMIS-DB
        //                  ,:POL-TP-POLI
        //                  ,:POL-DUR-AA
        //                   :IND-POL-DUR-AA
        //                  ,:POL-DUR-MM
        //                   :IND-POL-DUR-MM
        //                  ,:POL-DT-SCAD-DB
        //                   :IND-POL-DT-SCAD
        //                  ,:POL-COD-PROD
        //                  ,:POL-DT-INI-VLDT-PROD-DB
        //                  ,:POL-COD-CONV
        //                   :IND-POL-COD-CONV
        //                  ,:POL-COD-RAMO
        //                   :IND-POL-COD-RAMO
        //                  ,:POL-DT-INI-VLDT-CONV-DB
        //                   :IND-POL-DT-INI-VLDT-CONV
        //                  ,:POL-DT-APPLZ-CONV-DB
        //                   :IND-POL-DT-APPLZ-CONV
        //                  ,:POL-TP-FRM-ASSVA
        //                  ,:POL-TP-RGM-FISC
        //                   :IND-POL-TP-RGM-FISC
        //                  ,:POL-FL-ESTAS
        //                   :IND-POL-FL-ESTAS
        //                  ,:POL-FL-RSH-COMUN
        //                   :IND-POL-FL-RSH-COMUN
        //                  ,:POL-FL-RSH-COMUN-COND
        //                   :IND-POL-FL-RSH-COMUN-COND
        //                  ,:POL-TP-LIV-GENZ-TIT
        //                  ,:POL-FL-COP-FINANZ
        //                   :IND-POL-FL-COP-FINANZ
        //                  ,:POL-TP-APPLZ-DIR
        //                   :IND-POL-TP-APPLZ-DIR
        //                  ,:POL-SPE-MED
        //                   :IND-POL-SPE-MED
        //                  ,:POL-DIR-EMIS
        //                   :IND-POL-DIR-EMIS
        //                  ,:POL-DIR-1O-VERS
        //                   :IND-POL-DIR-1O-VERS
        //                  ,:POL-DIR-VERS-AGG
        //                   :IND-POL-DIR-VERS-AGG
        //                  ,:POL-COD-DVS
        //                   :IND-POL-COD-DVS
        //                  ,:POL-FL-FNT-AZ
        //                   :IND-POL-FL-FNT-AZ
        //                  ,:POL-FL-FNT-ADER
        //                   :IND-POL-FL-FNT-ADER
        //                  ,:POL-FL-FNT-TFR
        //                   :IND-POL-FL-FNT-TFR
        //                  ,:POL-FL-FNT-VOLO
        //                   :IND-POL-FL-FNT-VOLO
        //                  ,:POL-TP-OPZ-A-SCAD
        //                   :IND-POL-TP-OPZ-A-SCAD
        //                  ,:POL-AA-DIFF-PROR-DFLT
        //                   :IND-POL-AA-DIFF-PROR-DFLT
        //                  ,:POL-FL-VER-PROD
        //                   :IND-POL-FL-VER-PROD
        //                  ,:POL-DUR-GG
        //                   :IND-POL-DUR-GG
        //                  ,:POL-DIR-QUIET
        //                   :IND-POL-DIR-QUIET
        //                  ,:POL-TP-PTF-ESTNO
        //                   :IND-POL-TP-PTF-ESTNO
        //                  ,:POL-FL-CUM-PRE-CNTR
        //                   :IND-POL-FL-CUM-PRE-CNTR
        //                  ,:POL-FL-AMMB-MOVI
        //                   :IND-POL-FL-AMMB-MOVI
        //                  ,:POL-CONV-GECO
        //                   :IND-POL-CONV-GECO
        //                  ,:POL-DS-RIGA
        //                  ,:POL-DS-OPER-SQL
        //                  ,:POL-DS-VER
        //                  ,:POL-DS-TS-INI-CPTZ
        //                  ,:POL-DS-TS-END-CPTZ
        //                  ,:POL-DS-UTENTE
        //                  ,:POL-DS-STATO-ELAB
        //                  ,:POL-FL-SCUDO-FISC
        //                   :IND-POL-FL-SCUDO-FISC
        //                  ,:POL-FL-TRASFE
        //                   :IND-POL-FL-TRASFE
        //                  ,:POL-FL-TFR-STRC
        //                   :IND-POL-FL-TFR-STRC
        //                  ,:POL-DT-PRESC-DB
        //                   :IND-POL-DT-PRESC
        //                  ,:POL-COD-CONV-AGG
        //                   :IND-POL-COD-CONV-AGG
        //                  ,:POL-SUBCAT-PROD
        //                   :IND-POL-SUBCAT-PROD
        //                  ,:POL-FL-QUEST-ADEGZ-ASS
        //                   :IND-POL-FL-QUEST-ADEGZ-ASS
        //                  ,:POL-COD-TPA
        //                   :IND-POL-COD-TPA
        //                  ,:POL-ID-ACC-COMM
        //                   :IND-POL-ID-ACC-COMM
        //                  ,:POL-FL-POLI-CPI-PR
        //                   :IND-POL-FL-POLI-CPI-PR
        //                  ,:POL-FL-POLI-BUNDLING
        //                   :IND-POL-FL-POLI-BUNDLING
        //                  ,:POL-IND-POLI-PRIN-COLL
        //                   :IND-POL-IND-POLI-PRIN-COLL
        //                  ,:POL-FL-VND-BUNDLE
        //                   :IND-POL-FL-VND-BUNDLE
        //                  ,:POL-IB-BS
        //                   :IND-POL-IB-BS
        //                  ,:POL-FL-POLI-IFP
        //                   :IND-POL-FL-POLI-IFP
        //                  ,:STB-ID-STAT-OGG-BUS
        //                  ,:STB-ID-OGG
        //                  ,:STB-TP-OGG
        //                  ,:STB-ID-MOVI-CRZ
        //                  ,:STB-ID-MOVI-CHIU
        //                   :IND-STB-ID-MOVI-CHIU
        //                  ,:STB-DT-INI-EFF-DB
        //                  ,:STB-DT-END-EFF-DB
        //                  ,:STB-COD-COMP-ANIA
        //                  ,:STB-TP-STAT-BUS
        //                  ,:STB-TP-CAUS
        //                  ,:STB-DS-RIGA
        //                  ,:STB-DS-OPER-SQL
        //                  ,:STB-DS-VER
        //                  ,:STB-DS-TS-INI-CPTZ
        //                  ,:STB-DS-TS-END-CPTZ
        //                  ,:STB-DS-UTENTE
        //                  ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        adesPoliDao.fetchCurSc08(this.getAdesPoliLdbm0250());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-SC08 THRU A370-SC08-EX
            a370CloseCursorSc08();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-ADE-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndAdes().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
            ades.getAdeIdMoviChiu().setAdeIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeIdMoviChiu.Len.ADE_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-ADE-IB-PREV = -1
        //              MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
        //           END-IF
        if (ws.getIndAdes().getIbPrev() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
            ades.setAdeIbPrev(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IB_PREV));
        }
        // COB_CODE: IF IND-ADE-IB-OGG = -1
        //              MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
        //           END-IF
        if (ws.getIndAdes().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
            ades.setAdeIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IB_OGG));
        }
        // COB_CODE: IF IND-ADE-DT-DECOR = -1
        //              MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
        //           END-IF
        if (ws.getIndAdes().getDtDecor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
            ades.getAdeDtDecor().setAdeDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtDecor.Len.ADE_DT_DECOR_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndAdes().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
            ades.getAdeDtScad().setAdeDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtScad.Len.ADE_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-ADE-ETA-A-SCAD = -1
        //              MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
        //           END-IF
        if (ws.getIndAdes().getEtaAScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
            ades.getAdeEtaAScad().setAdeEtaAScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeEtaAScad.Len.ADE_ETA_A_SCAD_NULL));
        }
        // COB_CODE: IF IND-ADE-DUR-AA = -1
        //              MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
        //           END-IF
        if (ws.getIndAdes().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
            ades.getAdeDurAa().setAdeDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurAa.Len.ADE_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-ADE-DUR-MM = -1
        //              MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
        //           END-IF
        if (ws.getIndAdes().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
            ades.getAdeDurMm().setAdeDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurMm.Len.ADE_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-ADE-DUR-GG = -1
        //              MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
        //           END-IF
        if (ws.getIndAdes().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
            ades.getAdeDurGg().setAdeDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurGg.Len.ADE_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-ADE-TP-RIAT = -1
        //              MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
        //           END-IF
        if (ws.getIndAdes().getTpRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
            ades.setAdeTpRiat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_TP_RIAT));
        }
        // COB_CODE: IF IND-ADE-TP-IAS = -1
        //              MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
        //           END-IF
        if (ws.getIndAdes().getTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
            ades.setAdeTpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_TP_IAS));
        }
        // COB_CODE: IF IND-ADE-DT-VARZ-TP-IAS = -1
        //              MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
        //           END-IF
        if (ws.getIndAdes().getDtVarzTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
            ades.getAdeDtVarzTpIas().setAdeDtVarzTpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtVarzTpIas.Len.ADE_DT_VARZ_TP_IAS_NULL));
        }
        // COB_CODE: IF IND-ADE-PRE-NET-IND = -1
        //              MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getPreNetInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
            ades.getAdePreNetInd().setAdePreNetIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePreNetInd.Len.ADE_PRE_NET_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-PRE-LRD-IND = -1
        //              MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getPreLrdInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
            ades.getAdePreLrdInd().setAdePreLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePreLrdInd.Len.ADE_PRE_LRD_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-RAT-LRD-IND = -1
        //              MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getRatLrdInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
            ades.getAdeRatLrdInd().setAdeRatLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeRatLrdInd.Len.ADE_RAT_LRD_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-PRSTZ-INI-IND = -1
        //              MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getPrstzIniInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
            ades.getAdePrstzIniInd().setAdePrstzIniIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePrstzIniInd.Len.ADE_PRSTZ_INI_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-FL-COINC-ASSTO = -1
        //              MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
        //           END-IF
        if (ws.getIndAdes().getFlCoincAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
            ades.setAdeFlCoincAssto(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-IB-DFLT = -1
        //              MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
        //           END-IF
        if (ws.getIndAdes().getIbDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
            ades.setAdeIbDflt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IB_DFLT));
        }
        // COB_CODE: IF IND-ADE-MOD-CALC = -1
        //              MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
        //           END-IF
        if (ws.getIndAdes().getModCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
            ades.setAdeModCalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_MOD_CALC));
        }
        // COB_CODE: IF IND-ADE-TP-FNT-CNBTVA = -1
        //              MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
        //           END-IF
        if (ws.getIndAdes().getTpFntCnbtva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
            ades.setAdeTpFntCnbtva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_TP_FNT_CNBTVA));
        }
        // COB_CODE: IF IND-ADE-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndAdes().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
            ades.getAdeImpAz().setAdeImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpAz.Len.ADE_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndAdes().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
            ades.getAdeImpAder().setAdeImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpAder.Len.ADE_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndAdes().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
            ades.getAdeImpTfr().setAdeImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpTfr.Len.ADE_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndAdes().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
            ades.getAdeImpVolo().setAdeImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpVolo.Len.ADE_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-AZ = -1
        //              MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
        //           END-IF
        if (ws.getIndAdes().getPcAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
            ades.getAdePcAz().setAdePcAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcAz.Len.ADE_PC_AZ_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-ADER = -1
        //              MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
        //           END-IF
        if (ws.getIndAdes().getPcAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
            ades.getAdePcAder().setAdePcAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcAder.Len.ADE_PC_ADER_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-TFR = -1
        //              MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
        //           END-IF
        if (ws.getIndAdes().getPcTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
            ades.getAdePcTfr().setAdePcTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcTfr.Len.ADE_PC_TFR_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-VOLO = -1
        //              MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
        //           END-IF
        if (ws.getIndAdes().getPcVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
            ades.getAdePcVolo().setAdePcVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcVolo.Len.ADE_PC_VOLO_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-NOVA-RGM-FISC = -1
        //              MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
        //           END-IF
        if (ws.getIndAdes().getDtNovaRgmFisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
            ades.getAdeDtNovaRgmFisc().setAdeDtNovaRgmFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtNovaRgmFisc.Len.ADE_DT_NOVA_RGM_FISC_NULL));
        }
        // COB_CODE: IF IND-ADE-FL-ATTIV = -1
        //              MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
        //           END-IF
        if (ws.getIndAdes().getFlAttiv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
            ades.setAdeFlAttiv(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-IMP-REC-RIT-VIS = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
        //           END-IF
        if (ws.getIndAdes().getImpRecRitVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
            ades.getAdeImpRecRitVis().setAdeImpRecRitVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpRecRitVis.Len.ADE_IMP_REC_RIT_VIS_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-REC-RIT-ACC = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
        //           END-IF
        if (ws.getIndAdes().getImpRecRitAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
            ades.getAdeImpRecRitAcc().setAdeImpRecRitAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpRecRitAcc.Len.ADE_IMP_REC_RIT_ACC_NULL));
        }
        // COB_CODE: IF IND-ADE-FL-VARZ-STAT-TBGC = -1
        //              MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
        //           END-IF
        if (ws.getIndAdes().getFlVarzStatTbgc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
            ades.setAdeFlVarzStatTbgc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-FL-PROVZA-MIGRAZ = -1
        //              MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
        //           END-IF
        if (ws.getIndAdes().getFlProvzaMigraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
            ades.setAdeFlProvzaMigraz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-IMPB-VIS-DA-REC = -1
        //              MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
        //           END-IF
        if (ws.getIndAdes().getImpbVisDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
            ades.getAdeImpbVisDaRec().setAdeImpbVisDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpbVisDaRec.Len.ADE_IMPB_VIS_DA_REC_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-DECOR-PREST-BAN = -1
        //              MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
        //           END-IF
        if (ws.getIndAdes().getDtDecorPrestBan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
            ades.getAdeDtDecorPrestBan().setAdeDtDecorPrestBanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtDecorPrestBan.Len.ADE_DT_DECOR_PREST_BAN_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-EFF-VARZ-STAT-T = -1
        //              MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
        //           END-IF
        if (ws.getIndAdes().getDtEffVarzStatT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
            ades.getAdeDtEffVarzStatT().setAdeDtEffVarzStatTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtEffVarzStatT.Len.ADE_DT_EFF_VARZ_STAT_T_NULL));
        }
        // COB_CODE: IF IND-ADE-CUM-CNBT-CAP = -1
        //              MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
        //           END-IF
        if (ws.getIndAdes().getCumCnbtCap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
            ades.getAdeCumCnbtCap().setAdeCumCnbtCapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeCumCnbtCap.Len.ADE_CUM_CNBT_CAP_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-GAR-CNBT = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
        //           END-IF
        if (ws.getIndAdes().getImpGarCnbt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
            ades.getAdeImpGarCnbt().setAdeImpGarCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpGarCnbt.Len.ADE_IMP_GAR_CNBT_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-ULT-CONS-CNBT = -1
        //              MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
        //           END-IF
        if (ws.getIndAdes().getDtUltConsCnbt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
            ades.getAdeDtUltConsCnbt().setAdeDtUltConsCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtUltConsCnbt.Len.ADE_DT_ULT_CONS_CNBT_NULL));
        }
        // COB_CODE: IF IND-ADE-IDEN-ISC-FND = -1
        //              MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
        //           END-IF
        if (ws.getIndAdes().getIdenIscFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
            ades.setAdeIdenIscFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IDEN_ISC_FND));
        }
        // COB_CODE: IF IND-ADE-NUM-RAT-PIAN = -1
        //              MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
        //           END-IF
        if (ws.getIndAdes().getNumRatPian() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
            ades.getAdeNumRatPian().setAdeNumRatPianNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeNumRatPian.Len.ADE_NUM_RAT_PIAN_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-PRESC = -1
        //              MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
        //           END-IF.
        if (ws.getIndAdes().getDtPresc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
            ades.getAdeDtPresc().setAdeDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtPresc.Len.ADE_DT_PRESC_NULL));
        }
        // COB_CODE: IF IND-ADE-CONCS-PREST = -1
        //              MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
        //           END-IF.
        if (ws.getIndAdes().getConcsPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
            ades.setAdeConcsPrest(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndPoli().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-ID-MOVI-CHIU-NULL
            poli.getPolIdMoviChiu().setPolIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdMoviChiu.Len.POL_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-POL-IB-OGG = -1
        //              MOVE HIGH-VALUES TO POL-IB-OGG-NULL
        //           END-IF
        if (ws.getIndPoli().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IB-OGG-NULL
            poli.setPolIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_IB_OGG));
        }
        // COB_CODE: IF IND-POL-DT-PROP = -1
        //              MOVE HIGH-VALUES TO POL-DT-PROP-NULL
        //           END-IF
        if (ws.getIndPoli().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-PROP-NULL
            poli.getPolDtProp().setPolDtPropNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtProp.Len.POL_DT_PROP_NULL));
        }
        // COB_CODE: IF IND-POL-DUR-AA = -1
        //              MOVE HIGH-VALUES TO POL-DUR-AA-NULL
        //           END-IF
        if (ws.getIndPoli().getPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-AA-NULL
            poli.getPolDurAa().setPolDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurAa.Len.POL_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-POL-DUR-MM = -1
        //              MOVE HIGH-VALUES TO POL-DUR-MM-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-MM-NULL
            poli.getPolDurMm().setPolDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurMm.Len.POL_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-POL-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-SCAD-NULL
            poli.getPolDtScad().setPolDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtScad.Len.POL_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-POL-COD-CONV = -1
        //              MOVE HIGH-VALUES TO POL-COD-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-NULL
            poli.setPolCodConv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_CONV));
        }
        // COB_CODE: IF IND-POL-COD-RAMO = -1
        //              MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
        //           END-IF
        if (ws.getIndPoli().getIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-RAMO-NULL
            poli.setPolCodRamo(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_RAMO));
        }
        // COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = -1
        //              MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-INI-VLDT-CONV-NULL
            poli.getPolDtIniVldtConv().setPolDtIniVldtConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtIniVldtConv.Len.POL_DT_INI_VLDT_CONV_NULL));
        }
        // COB_CODE: IF IND-POL-DT-APPLZ-CONV = -1
        //              MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
        //           END-IF
        if (ws.getIndPoli().getSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-APPLZ-CONV-NULL
            poli.getPolDtApplzConv().setPolDtApplzConvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtApplzConv.Len.POL_DT_APPLZ_CONV_NULL));
        }
        // COB_CODE: IF IND-POL-TP-RGM-FISC = -1
        //              MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
        //           END-IF
        if (ws.getIndPoli().getTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-RGM-FISC-NULL
            poli.setPolTpRgmFisc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_RGM_FISC));
        }
        // COB_CODE: IF IND-POL-FL-ESTAS = -1
        //              MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-ESTAS-NULL
            poli.setPolFlEstas(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-RSH-COMUN = -1
        //              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-NULL
            poli.setPolFlRshComun(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-RSH-COMUN-COND = -1
        //              MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-RSH-COMUN-COND-NULL
            poli.setPolFlRshComunCond(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-COP-FINANZ = -1
        //              MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-COP-FINANZ-NULL
            poli.setPolFlCopFinanz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-TP-APPLZ-DIR = -1
        //              MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
        //           END-IF
        if (ws.getIndPoli().getSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-APPLZ-DIR-NULL
            poli.setPolTpApplzDir(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_APPLZ_DIR));
        }
        // COB_CODE: IF IND-POL-SPE-MED = -1
        //              MOVE HIGH-VALUES TO POL-SPE-MED-NULL
        //           END-IF
        if (ws.getIndPoli().getPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-SPE-MED-NULL
            poli.getPolSpeMed().setPolSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolSpeMed.Len.POL_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-EMIS = -1
        //              MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
        //           END-IF
        if (ws.getIndPoli().getPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-EMIS-NULL
            poli.getPolDirEmis().setPolDirEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirEmis.Len.POL_DIR_EMIS_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-1O-VERS = -1
        //              MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
        //           END-IF
        if (ws.getIndPoli().getPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-1O-VERS-NULL
            poli.getPolDir1oVers().setPolDir1oVersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDir1oVers.Len.POL_DIR1O_VERS_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-VERS-AGG = -1
        //              MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
        //           END-IF
        if (ws.getIndPoli().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-VERS-AGG-NULL
            poli.getPolDirVersAgg().setPolDirVersAggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirVersAgg.Len.POL_DIR_VERS_AGG_NULL));
        }
        // COB_CODE: IF IND-POL-COD-DVS = -1
        //              MOVE HIGH-VALUES TO POL-COD-DVS-NULL
        //           END-IF
        if (ws.getIndPoli().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-DVS-NULL
            poli.setPolCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_DVS));
        }
        // COB_CODE: IF IND-POL-FL-FNT-AZ = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
        //           END-IF
        if (ws.getIndPoli().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-AZ-NULL
            poli.setPolFlFntAz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-ADER = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
        //           END-IF
        if (ws.getIndPoli().getProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-ADER-NULL
            poli.setPolFlFntAder(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-TFR = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
        //           END-IF
        if (ws.getIndPoli().getProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-TFR-NULL
            poli.setPolFlFntTfr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-FNT-VOLO = -1
        //              MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
        //           END-IF
        if (ws.getIndPoli().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-FNT-VOLO-NULL
            poli.setPolFlFntVolo(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-TP-OPZ-A-SCAD = -1
        //              MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
        //           END-IF
        if (ws.getIndPoli().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-OPZ-A-SCAD-NULL
            poli.setPolTpOpzAScad(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_OPZ_A_SCAD));
        }
        // COB_CODE: IF IND-POL-AA-DIFF-PROR-DFLT = -1
        //              MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
        //           END-IF
        if (ws.getIndPoli().getProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-AA-DIFF-PROR-DFLT-NULL
            poli.getPolAaDiffProrDflt().setPolAaDiffProrDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolAaDiffProrDflt.Len.POL_AA_DIFF_PROR_DFLT_NULL));
        }
        // COB_CODE: IF IND-POL-FL-VER-PROD = -1
        //              MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
        //           END-IF
        if (ws.getIndPoli().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-VER-PROD-NULL
            poli.setPolFlVerProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_FL_VER_PROD));
        }
        // COB_CODE: IF IND-POL-DUR-GG = -1
        //              MOVE HIGH-VALUES TO POL-DUR-GG-NULL
        //           END-IF
        if (ws.getIndPoli().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DUR-GG-NULL
            poli.getPolDurGg().setPolDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDurGg.Len.POL_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-POL-DIR-QUIET = -1
        //              MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
        //           END-IF
        if (ws.getIndPoli().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DIR-QUIET-NULL
            poli.getPolDirQuiet().setPolDirQuietNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDirQuiet.Len.POL_DIR_QUIET_NULL));
        }
        // COB_CODE: IF IND-POL-TP-PTF-ESTNO = -1
        //              MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
        //           END-IF
        if (ws.getIndPoli().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-TP-PTF-ESTNO-NULL
            poli.setPolTpPtfEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_TP_PTF_ESTNO));
        }
        // COB_CODE: IF IND-POL-FL-CUM-PRE-CNTR = -1
        //              MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
        //           END-IF
        if (ws.getIndPoli().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-CUM-PRE-CNTR-NULL
            poli.setPolFlCumPreCntr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-AMMB-MOVI = -1
        //              MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-AMMB-MOVI-NULL
            poli.setPolFlAmmbMovi(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-CONV-GECO = -1
        //              MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
        //           END-IF
        if (ws.getIndPoli().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-CONV-GECO-NULL
            poli.setPolConvGeco(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_CONV_GECO));
        }
        // COB_CODE: IF IND-POL-FL-SCUDO-FISC = -1
        //              MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-SCUDO-FISC-NULL
            poli.setPolFlScudoFisc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-TRASFE = -1
        //              MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-TRASFE-NULL
            poli.setPolFlTrasfe(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndPoli().getManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-TFR-STRC-NULL
            poli.setPolFlTfrStrc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-DT-PRESC = -1
        //              MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
        //           END-IF
        if (ws.getIndPoli().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-DT-PRESC-NULL
            poli.getPolDtPresc().setPolDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolDtPresc.Len.POL_DT_PRESC_NULL));
        }
        // COB_CODE: IF IND-POL-COD-CONV-AGG = -1
        //              MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
        //           END-IF
        if (ws.getIndPoli().getSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-CONV-AGG-NULL
            poli.setPolCodConvAgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_CONV_AGG));
        }
        // COB_CODE: IF IND-POL-SUBCAT-PROD = -1
        //              MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
        //           END-IF.
        if (ws.getIndPoli().getCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-SUBCAT-PROD-NULL
            poli.setPolSubcatProd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_SUBCAT_PROD));
        }
        // COB_CODE: IF IND-POL-FL-QUEST-ADEGZ-ASS = -1
        //              MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
        //           END-IF
        if (ws.getIndPoli().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-QUEST-ADEGZ-ASS-NULL
            poli.setPolFlQuestAdegzAss(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-COD-TPA = -1
        //              MOVE HIGH-VALUES TO POL-COD-TPA-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-COD-TPA-NULL
            poli.setPolCodTpa(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_COD_TPA));
        }
        // COB_CODE: IF IND-POL-ID-ACC-COMM = -1
        //              MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
        //           END-IF
        if (ws.getIndPoli().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-ID-ACC-COMM-NULL
            poli.getPolIdAccComm().setPolIdAccCommNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PolIdAccComm.Len.POL_ID_ACC_COMM_NULL));
        }
        // COB_CODE: IF IND-POL-FL-POLI-CPI-PR = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
        //           END-IF
        if (ws.getIndPoli().getNumGgRitardoPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-CPI-PR-NULL
            poli.setPolFlPoliCpiPr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-POLI-BUNDLING = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
        //           END-IF
        if (ws.getIndPoli().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-BUNDLING-NULL
            poli.setPolFlPoliBundling(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-IND-POLI-PRIN-COLL = -1
        //              MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
        //           END-IF
        if (ws.getIndPoli().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IND-POLI-PRIN-COLL-NULL
            poli.setPolIndPoliPrinColl(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-FL-VND-BUNDLE = -1
        //              MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
        //           END-IF.
        if (ws.getIndPoli().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-VND-BUNDLE-NULL
            poli.setPolFlVndBundle(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POL-IB-BS = -1
        //              MOVE HIGH-VALUES TO POL-IB-BS-NULL
        //           END-IF.
        if (ws.getIndPoli().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-IB-BS-NULL
            poli.setPolIbBs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PoliIdbspol0.Len.POL_IB_BS));
        }
        // COB_CODE: IF IND-POL-FL-POLI-IFP = -1
        //              MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
        //           END-IF.
        if (ws.getIndPoli().getCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POL-FL-POLI-IFP-NULL
            poli.setPolFlPoliIfp(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-STB-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
        //           END-IF.
        if (ws.getIndStbIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
            statOggBus.getStbIdMoviChiu().setStbIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StbIdMoviChiu.Len.STB_ID_MOVI_CHIU_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES<br>
	 * <pre>*****************************************************************</pre>*/
    private void z150ValorizzaDataServices() {
        // COB_CODE: MOVE IDSV0003-OPERAZIONE     TO POL-DS-OPER-SQL.
        poli.setPolDsOperSqlFormatted(idsv0003.getOperazione().getOperazioneFormatted());
        // COB_CODE: MOVE IDSV0003-USER-NAME      TO POL-DS-UTENTE.
        poli.setPolDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF ADE-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-ADE-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIdMoviChiu().getAdeIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-ID-MOVI-CHIU
            ws.getIndAdes().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-ID-MOVI-CHIU
            ws.getIndAdes().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF ADE-IB-PREV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IB-PREV
        //           ELSE
        //              MOVE 0 TO IND-ADE-IB-PREV
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: MOVE -1 TO IND-ADE-IB-PREV
            ws.getIndAdes().setIbPrev(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IB-PREV
            ws.getIndAdes().setIbPrev(((short)0));
        }
        // COB_CODE: IF ADE-IB-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IB-OGG
        //           ELSE
        //              MOVE 0 TO IND-ADE-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIbOgg(), Ades.Len.ADE_IB_OGG)) {
            // COB_CODE: MOVE -1 TO IND-ADE-IB-OGG
            ws.getIndAdes().setIbOgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IB-OGG
            ws.getIndAdes().setIbOgg(((short)0));
        }
        // COB_CODE: IF ADE-DT-DECOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-DECOR
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-DECOR
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtDecor().getAdeDtDecorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-DECOR
            ws.getIndAdes().setDtDecor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-DECOR
            ws.getIndAdes().setDtDecor(((short)0));
        }
        // COB_CODE: IF ADE-DT-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-SCAD
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtScad().getAdeDtScadNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-SCAD
            ws.getIndAdes().setDtScad(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-SCAD
            ws.getIndAdes().setDtScad(((short)0));
        }
        // COB_CODE: IF ADE-ETA-A-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-ETA-A-SCAD
        //           ELSE
        //              MOVE 0 TO IND-ADE-ETA-A-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeEtaAScad().getAdeEtaAScadNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-ETA-A-SCAD
            ws.getIndAdes().setEtaAScad(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-ETA-A-SCAD
            ws.getIndAdes().setEtaAScad(((short)0));
        }
        // COB_CODE: IF ADE-DUR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DUR-AA
        //           ELSE
        //              MOVE 0 TO IND-ADE-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDurAa().getAdeDurAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DUR-AA
            ws.getIndAdes().setDurAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DUR-AA
            ws.getIndAdes().setDurAa(((short)0));
        }
        // COB_CODE: IF ADE-DUR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DUR-MM
        //           ELSE
        //              MOVE 0 TO IND-ADE-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDurMm().getAdeDurMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DUR-MM
            ws.getIndAdes().setDurMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DUR-MM
            ws.getIndAdes().setDurMm(((short)0));
        }
        // COB_CODE: IF ADE-DUR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DUR-GG
        //           ELSE
        //              MOVE 0 TO IND-ADE-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDurGg().getAdeDurGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DUR-GG
            ws.getIndAdes().setDurGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DUR-GG
            ws.getIndAdes().setDurGg(((short)0));
        }
        // COB_CODE: IF ADE-TP-RIAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-TP-RIAT
        //           ELSE
        //              MOVE 0 TO IND-ADE-TP-RIAT
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeTpRiatFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-TP-RIAT
            ws.getIndAdes().setTpRiat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-TP-RIAT
            ws.getIndAdes().setTpRiat(((short)0));
        }
        // COB_CODE: IF ADE-TP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-TP-IAS
        //           ELSE
        //              MOVE 0 TO IND-ADE-TP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeTpIasFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-TP-IAS
            ws.getIndAdes().setTpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-TP-IAS
            ws.getIndAdes().setTpIas(((short)0));
        }
        // COB_CODE: IF ADE-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-VARZ-TP-IAS
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-VARZ-TP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtVarzTpIas().getAdeDtVarzTpIasNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-VARZ-TP-IAS
            ws.getIndAdes().setDtVarzTpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-VARZ-TP-IAS
            ws.getIndAdes().setDtVarzTpIas(((short)0));
        }
        // COB_CODE: IF ADE-PRE-NET-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PRE-NET-IND
        //           ELSE
        //              MOVE 0 TO IND-ADE-PRE-NET-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePreNetInd().getAdePreNetIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PRE-NET-IND
            ws.getIndAdes().setPreNetInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PRE-NET-IND
            ws.getIndAdes().setPreNetInd(((short)0));
        }
        // COB_CODE: IF ADE-PRE-LRD-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PRE-LRD-IND
        //           ELSE
        //              MOVE 0 TO IND-ADE-PRE-LRD-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePreLrdInd().getAdePreLrdIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PRE-LRD-IND
            ws.getIndAdes().setPreLrdInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PRE-LRD-IND
            ws.getIndAdes().setPreLrdInd(((short)0));
        }
        // COB_CODE: IF ADE-RAT-LRD-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-RAT-LRD-IND
        //           ELSE
        //              MOVE 0 TO IND-ADE-RAT-LRD-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeRatLrdInd().getAdeRatLrdIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-RAT-LRD-IND
            ws.getIndAdes().setRatLrdInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-RAT-LRD-IND
            ws.getIndAdes().setRatLrdInd(((short)0));
        }
        // COB_CODE: IF ADE-PRSTZ-INI-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PRSTZ-INI-IND
        //           ELSE
        //              MOVE 0 TO IND-ADE-PRSTZ-INI-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePrstzIniInd().getAdePrstzIniIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PRSTZ-INI-IND
            ws.getIndAdes().setPrstzIniInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PRSTZ-INI-IND
            ws.getIndAdes().setPrstzIniInd(((short)0));
        }
        // COB_CODE: IF ADE-FL-COINC-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-FL-COINC-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-ADE-FL-COINC-ASSTO
        //           END-IF
        if (Conditions.eq(ades.getAdeFlCoincAssto(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-FL-COINC-ASSTO
            ws.getIndAdes().setFlCoincAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-FL-COINC-ASSTO
            ws.getIndAdes().setFlCoincAssto(((short)0));
        }
        // COB_CODE: IF ADE-IB-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IB-DFLT
        //           ELSE
        //              MOVE 0 TO IND-ADE-IB-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: MOVE -1 TO IND-ADE-IB-DFLT
            ws.getIndAdes().setIbDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IB-DFLT
            ws.getIndAdes().setIbDflt(((short)0));
        }
        // COB_CODE: IF ADE-MOD-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-MOD-CALC
        //           ELSE
        //              MOVE 0 TO IND-ADE-MOD-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeModCalcFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-MOD-CALC
            ws.getIndAdes().setModCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-MOD-CALC
            ws.getIndAdes().setModCalc(((short)0));
        }
        // COB_CODE: IF ADE-TP-FNT-CNBTVA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-TP-FNT-CNBTVA
        //           ELSE
        //              MOVE 0 TO IND-ADE-TP-FNT-CNBTVA
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeTpFntCnbtvaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-TP-FNT-CNBTVA
            ws.getIndAdes().setTpFntCnbtva(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-TP-FNT-CNBTVA
            ws.getIndAdes().setTpFntCnbtva(((short)0));
        }
        // COB_CODE: IF ADE-IMP-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-AZ
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpAz().getAdeImpAzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-AZ
            ws.getIndAdes().setImpAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-AZ
            ws.getIndAdes().setImpAz(((short)0));
        }
        // COB_CODE: IF ADE-IMP-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-ADER
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpAder().getAdeImpAderNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-ADER
            ws.getIndAdes().setImpAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-ADER
            ws.getIndAdes().setImpAder(((short)0));
        }
        // COB_CODE: IF ADE-IMP-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-TFR
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpTfr().getAdeImpTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-TFR
            ws.getIndAdes().setImpTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-TFR
            ws.getIndAdes().setImpTfr(((short)0));
        }
        // COB_CODE: IF ADE-IMP-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-VOLO
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpVolo().getAdeImpVoloNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-VOLO
            ws.getIndAdes().setImpVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-VOLO
            ws.getIndAdes().setImpVolo(((short)0));
        }
        // COB_CODE: IF ADE-PC-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PC-AZ
        //           ELSE
        //              MOVE 0 TO IND-ADE-PC-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePcAz().getAdePcAzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PC-AZ
            ws.getIndAdes().setPcAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PC-AZ
            ws.getIndAdes().setPcAz(((short)0));
        }
        // COB_CODE: IF ADE-PC-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PC-ADER
        //           ELSE
        //              MOVE 0 TO IND-ADE-PC-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePcAder().getAdePcAderNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PC-ADER
            ws.getIndAdes().setPcAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PC-ADER
            ws.getIndAdes().setPcAder(((short)0));
        }
        // COB_CODE: IF ADE-PC-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PC-TFR
        //           ELSE
        //              MOVE 0 TO IND-ADE-PC-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePcTfr().getAdePcTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PC-TFR
            ws.getIndAdes().setPcTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PC-TFR
            ws.getIndAdes().setPcTfr(((short)0));
        }
        // COB_CODE: IF ADE-PC-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PC-VOLO
        //           ELSE
        //              MOVE 0 TO IND-ADE-PC-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePcVolo().getAdePcVoloNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PC-VOLO
            ws.getIndAdes().setPcVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PC-VOLO
            ws.getIndAdes().setPcVolo(((short)0));
        }
        // COB_CODE: IF ADE-DT-NOVA-RGM-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-NOVA-RGM-FISC
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-NOVA-RGM-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtNovaRgmFisc().getAdeDtNovaRgmFiscNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-NOVA-RGM-FISC
            ws.getIndAdes().setDtNovaRgmFisc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-NOVA-RGM-FISC
            ws.getIndAdes().setDtNovaRgmFisc(((short)0));
        }
        // COB_CODE: IF ADE-FL-ATTIV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-FL-ATTIV
        //           ELSE
        //              MOVE 0 TO IND-ADE-FL-ATTIV
        //           END-IF
        if (Conditions.eq(ades.getAdeFlAttiv(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-FL-ATTIV
            ws.getIndAdes().setFlAttiv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-FL-ATTIV
            ws.getIndAdes().setFlAttiv(((short)0));
        }
        // COB_CODE: IF ADE-IMP-REC-RIT-VIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-REC-RIT-VIS
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-REC-RIT-VIS
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpRecRitVis().getAdeImpRecRitVisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-REC-RIT-VIS
            ws.getIndAdes().setImpRecRitVis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-REC-RIT-VIS
            ws.getIndAdes().setImpRecRitVis(((short)0));
        }
        // COB_CODE: IF ADE-IMP-REC-RIT-ACC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-REC-RIT-ACC
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-REC-RIT-ACC
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpRecRitAcc().getAdeImpRecRitAccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-REC-RIT-ACC
            ws.getIndAdes().setImpRecRitAcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-REC-RIT-ACC
            ws.getIndAdes().setImpRecRitAcc(((short)0));
        }
        // COB_CODE: IF ADE-FL-VARZ-STAT-TBGC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-FL-VARZ-STAT-TBGC
        //           ELSE
        //              MOVE 0 TO IND-ADE-FL-VARZ-STAT-TBGC
        //           END-IF
        if (Conditions.eq(ades.getAdeFlVarzStatTbgc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-FL-VARZ-STAT-TBGC
            ws.getIndAdes().setFlVarzStatTbgc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-FL-VARZ-STAT-TBGC
            ws.getIndAdes().setFlVarzStatTbgc(((short)0));
        }
        // COB_CODE: IF ADE-FL-PROVZA-MIGRAZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-FL-PROVZA-MIGRAZ
        //           ELSE
        //              MOVE 0 TO IND-ADE-FL-PROVZA-MIGRAZ
        //           END-IF
        if (Conditions.eq(ades.getAdeFlProvzaMigraz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-FL-PROVZA-MIGRAZ
            ws.getIndAdes().setFlProvzaMigraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-FL-PROVZA-MIGRAZ
            ws.getIndAdes().setFlProvzaMigraz(((short)0));
        }
        // COB_CODE: IF ADE-IMPB-VIS-DA-REC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMPB-VIS-DA-REC
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMPB-VIS-DA-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpbVisDaRec().getAdeImpbVisDaRecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMPB-VIS-DA-REC
            ws.getIndAdes().setImpbVisDaRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMPB-VIS-DA-REC
            ws.getIndAdes().setImpbVisDaRec(((short)0));
        }
        // COB_CODE: IF ADE-DT-DECOR-PREST-BAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-DECOR-PREST-BAN
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-DECOR-PREST-BAN
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtDecorPrestBan().getAdeDtDecorPrestBanNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-DECOR-PREST-BAN
            ws.getIndAdes().setDtDecorPrestBan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-DECOR-PREST-BAN
            ws.getIndAdes().setDtDecorPrestBan(((short)0));
        }
        // COB_CODE: IF ADE-DT-EFF-VARZ-STAT-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-EFF-VARZ-STAT-T
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-EFF-VARZ-STAT-T
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtEffVarzStatT().getAdeDtEffVarzStatTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-EFF-VARZ-STAT-T
            ws.getIndAdes().setDtEffVarzStatT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-EFF-VARZ-STAT-T
            ws.getIndAdes().setDtEffVarzStatT(((short)0));
        }
        // COB_CODE: IF ADE-CUM-CNBT-CAP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-CUM-CNBT-CAP
        //           ELSE
        //              MOVE 0 TO IND-ADE-CUM-CNBT-CAP
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeCumCnbtCap().getAdeCumCnbtCapNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-CUM-CNBT-CAP
            ws.getIndAdes().setCumCnbtCap(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-CUM-CNBT-CAP
            ws.getIndAdes().setCumCnbtCap(((short)0));
        }
        // COB_CODE: IF ADE-IMP-GAR-CNBT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-GAR-CNBT
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-GAR-CNBT
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpGarCnbt().getAdeImpGarCnbtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-GAR-CNBT
            ws.getIndAdes().setImpGarCnbt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-GAR-CNBT
            ws.getIndAdes().setImpGarCnbt(((short)0));
        }
        // COB_CODE: IF ADE-DT-ULT-CONS-CNBT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-ULT-CONS-CNBT
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-ULT-CONS-CNBT
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtUltConsCnbt().getAdeDtUltConsCnbtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-ULT-CONS-CNBT
            ws.getIndAdes().setDtUltConsCnbt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-ULT-CONS-CNBT
            ws.getIndAdes().setDtUltConsCnbt(((short)0));
        }
        // COB_CODE: IF ADE-IDEN-ISC-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IDEN-ISC-FND
        //           ELSE
        //              MOVE 0 TO IND-ADE-IDEN-ISC-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIdenIscFnd(), Ades.Len.ADE_IDEN_ISC_FND)) {
            // COB_CODE: MOVE -1 TO IND-ADE-IDEN-ISC-FND
            ws.getIndAdes().setIdenIscFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IDEN-ISC-FND
            ws.getIndAdes().setIdenIscFnd(((short)0));
        }
        // COB_CODE: IF ADE-NUM-RAT-PIAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-NUM-RAT-PIAN
        //           ELSE
        //              MOVE 0 TO IND-ADE-NUM-RAT-PIAN
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeNumRatPian().getAdeNumRatPianNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-NUM-RAT-PIAN
            ws.getIndAdes().setNumRatPian(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-NUM-RAT-PIAN
            ws.getIndAdes().setNumRatPian(((short)0));
        }
        // COB_CODE: IF ADE-DT-PRESC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-PRESC
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-PRESC
        //           END-IF.
        if (Characters.EQ_HIGH.test(ades.getAdeDtPresc().getAdeDtPrescNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-PRESC
            ws.getIndAdes().setDtPresc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-PRESC
            ws.getIndAdes().setDtPresc(((short)0));
        }
        // COB_CODE: IF ADE-CONCS-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-CONCS-PREST
        //           ELSE
        //              MOVE 0 TO IND-ADE-CONCS-PREST
        //           END-IF.
        if (Conditions.eq(ades.getAdeConcsPrest(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-CONCS-PREST
            ws.getIndAdes().setConcsPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-CONCS-PREST
            ws.getIndAdes().setConcsPrest(((short)0));
        }
        // COB_CODE: IF POL-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-POL-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolIdMoviChiu().getPolIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-ID-MOVI-CHIU
            ws.getIndPoli().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-ID-MOVI-CHIU
            ws.getIndPoli().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF POL-IB-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-IB-OGG
        //           ELSE
        //              MOVE 0 TO IND-POL-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolIbOgg(), PoliIdbspol0.Len.POL_IB_OGG)) {
            // COB_CODE: MOVE -1 TO IND-POL-IB-OGG
            ws.getIndPoli().setDtIniCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-IB-OGG
            ws.getIndPoli().setDtIniCop(((short)0));
        }
        // COB_CODE: IF POL-DT-PROP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-PROP
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-PROP
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtProp().getPolDtPropNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-PROP
            ws.getIndPoli().setDtEndCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-PROP
            ws.getIndPoli().setDtEndCop(((short)0));
        }
        // COB_CODE: IF POL-DUR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DUR-AA
        //           ELSE
        //              MOVE 0 TO IND-POL-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDurAa().getPolDurAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DUR-AA
            ws.getIndPoli().setPreNet(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DUR-AA
            ws.getIndPoli().setPreNet(((short)0));
        }
        // COB_CODE: IF POL-DUR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DUR-MM
        //           ELSE
        //              MOVE 0 TO IND-POL-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDurMm().getPolDurMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DUR-MM
            ws.getIndPoli().setIntrFraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DUR-MM
            ws.getIndPoli().setIntrFraz(((short)0));
        }
        // COB_CODE: IF POL-DT-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-SCAD
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtScad().getPolDtScadNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-SCAD
            ws.getIndPoli().setIntrMora(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-SCAD
            ws.getIndPoli().setIntrMora(((short)0));
        }
        // COB_CODE: IF POL-COD-CONV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-CONV
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodConvFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-CONV
            ws.getIndPoli().setIntrRetdt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-CONV
            ws.getIndPoli().setIntrRetdt(((short)0));
        }
        // COB_CODE: IF POL-COD-RAMO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-RAMO
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-RAMO
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodRamoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-RAMO
            ws.getIndPoli().setIntrRiat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-RAMO
            ws.getIndPoli().setIntrRiat(((short)0));
        }
        // COB_CODE: IF POL-DT-INI-VLDT-CONV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-INI-VLDT-CONV
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-INI-VLDT-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtIniVldtConv().getPolDtIniVldtConvNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-INI-VLDT-CONV
            ws.getIndPoli().setDir(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-INI-VLDT-CONV
            ws.getIndPoli().setDir(((short)0));
        }
        // COB_CODE: IF POL-DT-APPLZ-CONV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-APPLZ-CONV
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-APPLZ-CONV
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtApplzConv().getPolDtApplzConvNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-APPLZ-CONV
            ws.getIndPoli().setSpeMed(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-APPLZ-CONV
            ws.getIndPoli().setSpeMed(((short)0));
        }
        // COB_CODE: IF POL-TP-RGM-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-TP-RGM-FISC
        //           ELSE
        //              MOVE 0 TO IND-POL-TP-RGM-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolTpRgmFiscFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-TP-RGM-FISC
            ws.getIndPoli().setTax(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-TP-RGM-FISC
            ws.getIndPoli().setTax(((short)0));
        }
        // COB_CODE: IF POL-FL-ESTAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-ESTAS
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-ESTAS
        //           END-IF
        if (Conditions.eq(poli.getPolFlEstas(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-ESTAS
            ws.getIndPoli().setSoprSan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-ESTAS
            ws.getIndPoli().setSoprSan(((short)0));
        }
        // COB_CODE: IF POL-FL-RSH-COMUN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-RSH-COMUN
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-RSH-COMUN
        //           END-IF
        if (Conditions.eq(poli.getPolFlRshComun(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-RSH-COMUN
            ws.getIndPoli().setSoprSpo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-RSH-COMUN
            ws.getIndPoli().setSoprSpo(((short)0));
        }
        // COB_CODE: IF POL-FL-RSH-COMUN-COND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-RSH-COMUN-COND
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-RSH-COMUN-COND
        //           END-IF
        if (Conditions.eq(poli.getPolFlRshComunCond(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-RSH-COMUN-COND
            ws.getIndPoli().setSoprTec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-RSH-COMUN-COND
            ws.getIndPoli().setSoprTec(((short)0));
        }
        // COB_CODE: IF POL-FL-COP-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-COP-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-COP-FINANZ
        //           END-IF
        if (Conditions.eq(poli.getPolFlCopFinanz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-COP-FINANZ
            ws.getIndPoli().setSoprProf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-COP-FINANZ
            ws.getIndPoli().setSoprProf(((short)0));
        }
        // COB_CODE: IF POL-TP-APPLZ-DIR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-TP-APPLZ-DIR
        //           ELSE
        //              MOVE 0 TO IND-POL-TP-APPLZ-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolTpApplzDirFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-TP-APPLZ-DIR
            ws.getIndPoli().setSoprAlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-TP-APPLZ-DIR
            ws.getIndPoli().setSoprAlt(((short)0));
        }
        // COB_CODE: IF POL-SPE-MED-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-SPE-MED
        //           ELSE
        //              MOVE 0 TO IND-POL-SPE-MED
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolSpeMed().getPolSpeMedNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-SPE-MED
            ws.getIndPoli().setPreTot(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-SPE-MED
            ws.getIndPoli().setPreTot(((short)0));
        }
        // COB_CODE: IF POL-DIR-EMIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DIR-EMIS
        //           ELSE
        //              MOVE 0 TO IND-POL-DIR-EMIS
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDirEmis().getPolDirEmisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DIR-EMIS
            ws.getIndPoli().setPrePpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DIR-EMIS
            ws.getIndPoli().setPrePpIas(((short)0));
        }
        // COB_CODE: IF POL-DIR-1O-VERS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DIR-1O-VERS
        //           ELSE
        //              MOVE 0 TO IND-POL-DIR-1O-VERS
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDir1oVers().getPolDir1oVersNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DIR-1O-VERS
            ws.getIndPoli().setPreSoloRsh(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DIR-1O-VERS
            ws.getIndPoli().setPreSoloRsh(((short)0));
        }
        // COB_CODE: IF POL-DIR-VERS-AGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DIR-VERS-AGG
        //           ELSE
        //              MOVE 0 TO IND-POL-DIR-VERS-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDirVersAgg().getPolDirVersAggNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DIR-VERS-AGG
            ws.getIndPoli().setCarAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DIR-VERS-AGG
            ws.getIndPoli().setCarAcq(((short)0));
        }
        // COB_CODE: IF POL-COD-DVS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-DVS
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodDvsFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-DVS
            ws.getIndPoli().setCarGest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-DVS
            ws.getIndPoli().setCarGest(((short)0));
        }
        // COB_CODE: IF POL-FL-FNT-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-FNT-AZ
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-FNT-AZ
        //           END-IF
        if (Conditions.eq(poli.getPolFlFntAz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-FNT-AZ
            ws.getIndPoli().setCarInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-FNT-AZ
            ws.getIndPoli().setCarInc(((short)0));
        }
        // COB_CODE: IF POL-FL-FNT-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-FNT-ADER
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-FNT-ADER
        //           END-IF
        if (Conditions.eq(poli.getPolFlFntAder(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-FNT-ADER
            ws.getIndPoli().setProvAcq1aa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-FNT-ADER
            ws.getIndPoli().setProvAcq1aa(((short)0));
        }
        // COB_CODE: IF POL-FL-FNT-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-FNT-TFR
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-FNT-TFR
        //           END-IF
        if (Conditions.eq(poli.getPolFlFntTfr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-FNT-TFR
            ws.getIndPoli().setProvAcq2aa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-FNT-TFR
            ws.getIndPoli().setProvAcq2aa(((short)0));
        }
        // COB_CODE: IF POL-FL-FNT-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-FNT-VOLO
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-FNT-VOLO
        //           END-IF
        if (Conditions.eq(poli.getPolFlFntVolo(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-FNT-VOLO
            ws.getIndPoli().setProvRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-FNT-VOLO
            ws.getIndPoli().setProvRicor(((short)0));
        }
        // COB_CODE: IF POL-TP-OPZ-A-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-TP-OPZ-A-SCAD
        //           ELSE
        //              MOVE 0 TO IND-POL-TP-OPZ-A-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolTpOpzAScadFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-TP-OPZ-A-SCAD
            ws.getIndPoli().setProvInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-TP-OPZ-A-SCAD
            ws.getIndPoli().setProvInc(((short)0));
        }
        // COB_CODE: IF POL-AA-DIFF-PROR-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-AA-DIFF-PROR-DFLT
        //           ELSE
        //              MOVE 0 TO IND-POL-AA-DIFF-PROR-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolAaDiffProrDflt().getPolAaDiffProrDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-AA-DIFF-PROR-DFLT
            ws.getIndPoli().setProvDaRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-AA-DIFF-PROR-DFLT
            ws.getIndPoli().setProvDaRec(((short)0));
        }
        // COB_CODE: IF POL-FL-VER-PROD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-VER-PROD
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-VER-PROD
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolFlVerProdFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-VER-PROD
            ws.getIndPoli().setCodDvs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-VER-PROD
            ws.getIndPoli().setCodDvs(((short)0));
        }
        // COB_CODE: IF POL-DUR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DUR-GG
        //           ELSE
        //              MOVE 0 TO IND-POL-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDurGg().getPolDurGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DUR-GG
            ws.getIndPoli().setFrqMovi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DUR-GG
            ws.getIndPoli().setFrqMovi(((short)0));
        }
        // COB_CODE: IF POL-DIR-QUIET-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DIR-QUIET
        //           ELSE
        //              MOVE 0 TO IND-POL-DIR-QUIET
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDirQuiet().getPolDirQuietNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DIR-QUIET
            ws.getIndPoli().setCodTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DIR-QUIET
            ws.getIndPoli().setCodTari(((short)0));
        }
        // COB_CODE: IF POL-TP-PTF-ESTNO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-TP-PTF-ESTNO
        //           ELSE
        //              MOVE 0 TO IND-POL-TP-PTF-ESTNO
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolTpPtfEstnoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-TP-PTF-ESTNO
            ws.getIndPoli().setImpAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-TP-PTF-ESTNO
            ws.getIndPoli().setImpAz(((short)0));
        }
        // COB_CODE: IF POL-FL-CUM-PRE-CNTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-CUM-PRE-CNTR
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-CUM-PRE-CNTR
        //           END-IF
        if (Conditions.eq(poli.getPolFlCumPreCntr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-CUM-PRE-CNTR
            ws.getIndPoli().setImpAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-CUM-PRE-CNTR
            ws.getIndPoli().setImpAder(((short)0));
        }
        // COB_CODE: IF POL-FL-AMMB-MOVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-AMMB-MOVI
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-AMMB-MOVI
        //           END-IF
        if (Conditions.eq(poli.getPolFlAmmbMovi(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-AMMB-MOVI
            ws.getIndPoli().setImpTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-AMMB-MOVI
            ws.getIndPoli().setImpTfr(((short)0));
        }
        // COB_CODE: IF POL-CONV-GECO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-CONV-GECO
        //           ELSE
        //              MOVE 0 TO IND-POL-CONV-GECO
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolConvGecoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-CONV-GECO
            ws.getIndPoli().setImpVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-CONV-GECO
            ws.getIndPoli().setImpVolo(((short)0));
        }
        // COB_CODE: IF POL-FL-SCUDO-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-SCUDO-FISC
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-SCUDO-FISC
        //           END-IF
        if (Conditions.eq(poli.getPolFlScudoFisc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-SCUDO-FISC
            ws.getIndPoli().setManfeeAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-SCUDO-FISC
            ws.getIndPoli().setManfeeAntic(((short)0));
        }
        // COB_CODE: IF POL-FL-TRASFE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-TRASFE
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-TRASFE
        //           END-IF
        if (Conditions.eq(poli.getPolFlTrasfe(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-TRASFE
            ws.getIndPoli().setManfeeRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-TRASFE
            ws.getIndPoli().setManfeeRicor(((short)0));
        }
        // COB_CODE: IF POL-FL-TFR-STRC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-TFR-STRC
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-TFR-STRC
        //           END-IF
        if (Conditions.eq(poli.getPolFlTfrStrc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-TFR-STRC
            ws.getIndPoli().setManfeeRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-TFR-STRC
            ws.getIndPoli().setManfeeRec(((short)0));
        }
        // COB_CODE: IF POL-DT-PRESC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-DT-PRESC
        //           ELSE
        //              MOVE 0 TO IND-POL-DT-PRESC
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolDtPresc().getPolDtPrescNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-DT-PRESC
            ws.getIndPoli().setDtEsiTit(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-DT-PRESC
            ws.getIndPoli().setDtEsiTit(((short)0));
        }
        // COB_CODE: IF POL-COD-CONV-AGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-CONV-AGG
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-CONV-AGG
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodConvAggFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-CONV-AGG
            ws.getIndPoli().setSpeAge(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-CONV-AGG
            ws.getIndPoli().setSpeAge(((short)0));
        }
        // COB_CODE: IF POL-SUBCAT-PROD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-SUBCAT-PROD
        //           ELSE
        //              MOVE 0 TO IND-POL-SUBCAT-PROD
        //           END-IF.
        if (Characters.EQ_HIGH.test(poli.getPolSubcatProdFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-SUBCAT-PROD
            ws.getIndPoli().setCarIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-SUBCAT-PROD
            ws.getIndPoli().setCarIas(((short)0));
        }
        // COB_CODE: IF POL-FL-QUEST-ADEGZ-ASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-QUEST-ADEGZ-ASS
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-QUEST-ADEGZ-ASS
        //           END-IF
        if (Conditions.eq(poli.getPolFlQuestAdegzAss(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-QUEST-ADEGZ-ASS
            ws.getIndPoli().setTotIntrPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-QUEST-ADEGZ-ASS
            ws.getIndPoli().setTotIntrPrest(((short)0));
        }
        // COB_CODE: IF POL-COD-TPA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-COD-TPA
        //           ELSE
        //              MOVE 0 TO IND-POL-COD-TPA
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolCodTpaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-COD-TPA
            ws.getIndPoli().setImpTrasfe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-COD-TPA
            ws.getIndPoli().setImpTrasfe(((short)0));
        }
        // COB_CODE: IF POL-ID-ACC-COMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-ID-ACC-COMM
        //           ELSE
        //              MOVE 0 TO IND-POL-ID-ACC-COMM
        //           END-IF
        if (Characters.EQ_HIGH.test(poli.getPolIdAccComm().getPolIdAccCommNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-POL-ID-ACC-COMM
            ws.getIndPoli().setImpTfrStrc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-ID-ACC-COMM
            ws.getIndPoli().setImpTfrStrc(((short)0));
        }
        // COB_CODE: IF POL-FL-POLI-CPI-PR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-POLI-CPI-PR
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-POLI-CPI-PR
        //           END-IF
        if (Conditions.eq(poli.getPolFlPoliCpiPr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-POLI-CPI-PR
            ws.getIndPoli().setNumGgRitardoPag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-POLI-CPI-PR
            ws.getIndPoli().setNumGgRitardoPag(((short)0));
        }
        // COB_CODE: IF POL-FL-POLI-BUNDLING-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-POLI-BUNDLING
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-POLI-BUNDLING
        //           END-IF
        if (Conditions.eq(poli.getPolFlPoliBundling(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-POLI-BUNDLING
            ws.getIndPoli().setNumGgRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-POLI-BUNDLING
            ws.getIndPoli().setNumGgRival(((short)0));
        }
        // COB_CODE: IF POL-IND-POLI-PRIN-COLL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-IND-POLI-PRIN-COLL
        //           ELSE
        //              MOVE 0 TO IND-POL-IND-POLI-PRIN-COLL
        //           END-IF
        if (Conditions.eq(poli.getPolIndPoliPrinColl(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-IND-POLI-PRIN-COLL
            ws.getIndPoli().setAcqExp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-IND-POLI-PRIN-COLL
            ws.getIndPoli().setAcqExp(((short)0));
        }
        // COB_CODE: IF POL-FL-VND-BUNDLE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-VND-BUNDLE
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-VND-BUNDLE
        //           END-IF.
        if (Conditions.eq(poli.getPolFlVndBundle(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-VND-BUNDLE
            ws.getIndPoli().setRemunAss(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-VND-BUNDLE
            ws.getIndPoli().setRemunAss(((short)0));
        }
        // COB_CODE: IF POL-IB-BS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-IB-BS
        //           ELSE
        //              MOVE 0 TO IND-POL-IB-BS
        //           END-IF.
        if (Characters.EQ_HIGH.test(poli.getPolIbBs(), PoliIdbspol0.Len.POL_IB_BS)) {
            // COB_CODE: MOVE -1 TO IND-POL-IB-BS
            ws.getIndPoli().setCommisInter(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-IB-BS
            ws.getIndPoli().setCommisInter(((short)0));
        }
        // COB_CODE: IF POL-FL-POLI-IFP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-POL-FL-POLI-IFP
        //           ELSE
        //              MOVE 0 TO IND-POL-FL-POLI-IFP
        //           END-IF.
        if (Conditions.eq(poli.getPolFlPoliIfp(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-POL-FL-POLI-IFP
            ws.getIndPoli().setCnbtAntirac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-POL-FL-POLI-IFP
            ws.getIndPoli().setCnbtAntirac(((short)0));
        }
        // COB_CODE: IF STB-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-STB-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-STB-ID-MOVI-CHIU
        //           END-IF.
        if (Characters.EQ_HIGH.test(statOggBus.getStbIdMoviChiu().getStbIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-STB-ID-MOVI-CHIU
            ws.setIndStbIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-STB-ID-MOVI-CHIU
            ws.setIndStbIdMoviChiu(((short)0));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE ADE-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getAdesDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-INI-EFF
        ades.setAdeDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE ADE-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getAdesDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-END-EFF
        ades.setAdeDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-ADE-DT-DECOR = 0
        //               MOVE WS-DATE-N      TO ADE-DT-DECOR
        //           END-IF
        if (ws.getIndAdes().getDtDecor() == 0) {
            // COB_CODE: MOVE ADE-DT-DECOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getDecorDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-DECOR
            ades.getAdeDtDecor().setAdeDtDecor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO ADE-DT-SCAD
        //           END-IF
        if (ws.getIndAdes().getDtScad() == 0) {
            // COB_CODE: MOVE ADE-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-SCAD
            ades.getAdeDtScad().setAdeDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-VARZ-TP-IAS = 0
        //               MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
        //           END-IF
        if (ws.getIndAdes().getDtVarzTpIas() == 0) {
            // COB_CODE: MOVE ADE-DT-VARZ-TP-IAS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getVarzTpIasDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
            ades.getAdeDtVarzTpIas().setAdeDtVarzTpIas(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-NOVA-RGM-FISC = 0
        //               MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
        //           END-IF
        if (ws.getIndAdes().getDtNovaRgmFisc() == 0) {
            // COB_CODE: MOVE ADE-DT-NOVA-RGM-FISC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getNovaRgmFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
            ades.getAdeDtNovaRgmFisc().setAdeDtNovaRgmFisc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-DECOR-PREST-BAN = 0
        //               MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
        //           END-IF
        if (ws.getIndAdes().getDtDecorPrestBan() == 0) {
            // COB_CODE: MOVE ADE-DT-DECOR-PREST-BAN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getDecorPrestBanDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
            ades.getAdeDtDecorPrestBan().setAdeDtDecorPrestBan(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
        //               MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
        //           END-IF
        if (ws.getIndAdes().getDtEffVarzStatT() == 0) {
            // COB_CODE: MOVE ADE-DT-EFF-VARZ-STAT-T-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getEffVarzStatTDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
            ades.getAdeDtEffVarzStatT().setAdeDtEffVarzStatT(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-ULT-CONS-CNBT = 0
        //               MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
        //           END-IF
        if (ws.getIndAdes().getDtUltConsCnbt() == 0) {
            // COB_CODE: MOVE ADE-DT-ULT-CONS-CNBT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getUltConsCnbtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
            ades.getAdeDtUltConsCnbt().setAdeDtUltConsCnbt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-PRESC = 0
        //               MOVE WS-DATE-N      TO ADE-DT-PRESC
        //           END-IF.
        if (ws.getIndAdes().getDtPresc() == 0) {
            // COB_CODE: MOVE ADE-DT-PRESC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-PRESC
            ades.getAdeDtPresc().setAdeDtPresc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-PROP = 0
        //               MOVE WS-DATE-N      TO POL-DT-PROP
        //           END-IF
        if (ws.getIndPoli().getDtEndCop() == 0) {
            // COB_CODE: MOVE POL-DT-PROP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getIniEffDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-PROP
            poli.getPolDtProp().setPolDtProp(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE POL-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-EFF
        poli.setPolDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-END-EFF
        poli.setPolDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-DECOR-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getScadDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-DECOR
        poli.setPolDtDecor(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POL-DT-EMIS-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getVarzTpIasDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-EMIS
        poli.setPolDtEmis(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-POL-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO POL-DT-SCAD
        //           END-IF
        if (ws.getIndPoli().getIntrMora() == 0) {
            // COB_CODE: MOVE POL-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getNovaRgmFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-SCAD
            poli.getPolDtScad().setPolDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE POL-DT-INI-VLDT-PROD-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getPoliDb().getDecorPrestBanDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-PROD
        poli.setPolDtIniVldtProd(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-POL-DT-INI-VLDT-CONV = 0
        //               MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
        //           END-IF
        if (ws.getIndPoli().getDir() == 0) {
            // COB_CODE: MOVE POL-DT-INI-VLDT-CONV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getEffVarzStatTDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-INI-VLDT-CONV
            poli.getPolDtIniVldtConv().setPolDtIniVldtConv(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-APPLZ-CONV = 0
        //               MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
        //           END-IF
        if (ws.getIndPoli().getSpeMed() == 0) {
            // COB_CODE: MOVE POL-DT-APPLZ-CONV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getUltConsCnbtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-APPLZ-CONV
            poli.getPolDtApplzConv().setPolDtApplzConv(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-POL-DT-PRESC = 0
        //               MOVE WS-DATE-N      TO POL-DT-PRESC
        //           END-IF.
        if (ws.getIndPoli().getDtEsiTit() == 0) {
            // COB_CODE: MOVE POL-DT-PRESC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getPoliDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POL-DT-PRESC
            poli.getPolDtPresc().setPolDtPresc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: MOVE STB-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvstb3().getStbDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO STB-DT-INI-EFF
        statOggBus.setStbDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE STB-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvstb3().getStbDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO STB-DT-END-EFF.
        statOggBus.setStbDtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAdeCodCompAnia() {
        return ades.getAdeCodCompAnia();
    }

    @Override
    public void setAdeCodCompAnia(int adeCodCompAnia) {
        this.ades.setAdeCodCompAnia(adeCodCompAnia);
    }

    @Override
    public char getAdeConcsPrest() {
        return ades.getAdeConcsPrest();
    }

    @Override
    public void setAdeConcsPrest(char adeConcsPrest) {
        this.ades.setAdeConcsPrest(adeConcsPrest);
    }

    @Override
    public Character getAdeConcsPrestObj() {
        if (ws.getIndAdes().getConcsPrest() >= 0) {
            return ((Character)getAdeConcsPrest());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeConcsPrestObj(Character adeConcsPrestObj) {
        if (adeConcsPrestObj != null) {
            setAdeConcsPrest(((char)adeConcsPrestObj));
            ws.getIndAdes().setConcsPrest(((short)0));
        }
        else {
            ws.getIndAdes().setConcsPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeCumCnbtCap() {
        return ades.getAdeCumCnbtCap().getAdeCumCnbtCap();
    }

    @Override
    public void setAdeCumCnbtCap(AfDecimal adeCumCnbtCap) {
        this.ades.getAdeCumCnbtCap().setAdeCumCnbtCap(adeCumCnbtCap.copy());
    }

    @Override
    public AfDecimal getAdeCumCnbtCapObj() {
        if (ws.getIndAdes().getCumCnbtCap() >= 0) {
            return getAdeCumCnbtCap();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeCumCnbtCapObj(AfDecimal adeCumCnbtCapObj) {
        if (adeCumCnbtCapObj != null) {
            setAdeCumCnbtCap(new AfDecimal(adeCumCnbtCapObj, 15, 3));
            ws.getIndAdes().setCumCnbtCap(((short)0));
        }
        else {
            ws.getIndAdes().setCumCnbtCap(((short)-1));
        }
    }

    @Override
    public char getAdeDsOperSql() {
        return ades.getAdeDsOperSql();
    }

    @Override
    public void setAdeDsOperSql(char adeDsOperSql) {
        this.ades.setAdeDsOperSql(adeDsOperSql);
    }

    @Override
    public long getAdeDsRiga() {
        return ades.getAdeDsRiga();
    }

    @Override
    public void setAdeDsRiga(long adeDsRiga) {
        this.ades.setAdeDsRiga(adeDsRiga);
    }

    @Override
    public char getAdeDsStatoElab() {
        return ades.getAdeDsStatoElab();
    }

    @Override
    public void setAdeDsStatoElab(char adeDsStatoElab) {
        this.ades.setAdeDsStatoElab(adeDsStatoElab);
    }

    @Override
    public long getAdeDsTsEndCptz() {
        return ades.getAdeDsTsEndCptz();
    }

    @Override
    public void setAdeDsTsEndCptz(long adeDsTsEndCptz) {
        this.ades.setAdeDsTsEndCptz(adeDsTsEndCptz);
    }

    @Override
    public long getAdeDsTsIniCptz() {
        return ades.getAdeDsTsIniCptz();
    }

    @Override
    public void setAdeDsTsIniCptz(long adeDsTsIniCptz) {
        this.ades.setAdeDsTsIniCptz(adeDsTsIniCptz);
    }

    @Override
    public String getAdeDsUtente() {
        return ades.getAdeDsUtente();
    }

    @Override
    public void setAdeDsUtente(String adeDsUtente) {
        this.ades.setAdeDsUtente(adeDsUtente);
    }

    @Override
    public int getAdeDsVer() {
        return ades.getAdeDsVer();
    }

    @Override
    public void setAdeDsVer(int adeDsVer) {
        this.ades.setAdeDsVer(adeDsVer);
    }

    @Override
    public String getAdeDtDecorDb() {
        return ws.getAdesDb().getDecorDb();
    }

    @Override
    public void setAdeDtDecorDb(String adeDtDecorDb) {
        this.ws.getAdesDb().setDecorDb(adeDtDecorDb);
    }

    @Override
    public String getAdeDtDecorDbObj() {
        if (ws.getIndAdes().getDtDecor() >= 0) {
            return getAdeDtDecorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtDecorDbObj(String adeDtDecorDbObj) {
        if (adeDtDecorDbObj != null) {
            setAdeDtDecorDb(adeDtDecorDbObj);
            ws.getIndAdes().setDtDecor(((short)0));
        }
        else {
            ws.getIndAdes().setDtDecor(((short)-1));
        }
    }

    @Override
    public String getAdeDtDecorPrestBanDb() {
        return ws.getAdesDb().getDecorPrestBanDb();
    }

    @Override
    public void setAdeDtDecorPrestBanDb(String adeDtDecorPrestBanDb) {
        this.ws.getAdesDb().setDecorPrestBanDb(adeDtDecorPrestBanDb);
    }

    @Override
    public String getAdeDtDecorPrestBanDbObj() {
        if (ws.getIndAdes().getDtDecorPrestBan() >= 0) {
            return getAdeDtDecorPrestBanDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtDecorPrestBanDbObj(String adeDtDecorPrestBanDbObj) {
        if (adeDtDecorPrestBanDbObj != null) {
            setAdeDtDecorPrestBanDb(adeDtDecorPrestBanDbObj);
            ws.getIndAdes().setDtDecorPrestBan(((short)0));
        }
        else {
            ws.getIndAdes().setDtDecorPrestBan(((short)-1));
        }
    }

    @Override
    public String getAdeDtEffVarzStatTDb() {
        return ws.getAdesDb().getEffVarzStatTDb();
    }

    @Override
    public void setAdeDtEffVarzStatTDb(String adeDtEffVarzStatTDb) {
        this.ws.getAdesDb().setEffVarzStatTDb(adeDtEffVarzStatTDb);
    }

    @Override
    public String getAdeDtEffVarzStatTDbObj() {
        if (ws.getIndAdes().getDtEffVarzStatT() >= 0) {
            return getAdeDtEffVarzStatTDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtEffVarzStatTDbObj(String adeDtEffVarzStatTDbObj) {
        if (adeDtEffVarzStatTDbObj != null) {
            setAdeDtEffVarzStatTDb(adeDtEffVarzStatTDbObj);
            ws.getIndAdes().setDtEffVarzStatT(((short)0));
        }
        else {
            ws.getIndAdes().setDtEffVarzStatT(((short)-1));
        }
    }

    @Override
    public String getAdeDtEndEffDb() {
        return ws.getAdesDb().getEndEffDb();
    }

    @Override
    public void setAdeDtEndEffDb(String adeDtEndEffDb) {
        this.ws.getAdesDb().setEndEffDb(adeDtEndEffDb);
    }

    @Override
    public String getAdeDtIniEffDb() {
        return ws.getAdesDb().getIniEffDb();
    }

    @Override
    public void setAdeDtIniEffDb(String adeDtIniEffDb) {
        this.ws.getAdesDb().setIniEffDb(adeDtIniEffDb);
    }

    @Override
    public String getAdeDtNovaRgmFiscDb() {
        return ws.getAdesDb().getNovaRgmFiscDb();
    }

    @Override
    public void setAdeDtNovaRgmFiscDb(String adeDtNovaRgmFiscDb) {
        this.ws.getAdesDb().setNovaRgmFiscDb(adeDtNovaRgmFiscDb);
    }

    @Override
    public String getAdeDtNovaRgmFiscDbObj() {
        if (ws.getIndAdes().getDtNovaRgmFisc() >= 0) {
            return getAdeDtNovaRgmFiscDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtNovaRgmFiscDbObj(String adeDtNovaRgmFiscDbObj) {
        if (adeDtNovaRgmFiscDbObj != null) {
            setAdeDtNovaRgmFiscDb(adeDtNovaRgmFiscDbObj);
            ws.getIndAdes().setDtNovaRgmFisc(((short)0));
        }
        else {
            ws.getIndAdes().setDtNovaRgmFisc(((short)-1));
        }
    }

    @Override
    public String getAdeDtPrescDb() {
        return ws.getAdesDb().getPrescDb();
    }

    @Override
    public void setAdeDtPrescDb(String adeDtPrescDb) {
        this.ws.getAdesDb().setPrescDb(adeDtPrescDb);
    }

    @Override
    public String getAdeDtPrescDbObj() {
        if (ws.getIndAdes().getDtPresc() >= 0) {
            return getAdeDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtPrescDbObj(String adeDtPrescDbObj) {
        if (adeDtPrescDbObj != null) {
            setAdeDtPrescDb(adeDtPrescDbObj);
            ws.getIndAdes().setDtPresc(((short)0));
        }
        else {
            ws.getIndAdes().setDtPresc(((short)-1));
        }
    }

    @Override
    public String getAdeDtScadDb() {
        return ws.getAdesDb().getScadDb();
    }

    @Override
    public void setAdeDtScadDb(String adeDtScadDb) {
        this.ws.getAdesDb().setScadDb(adeDtScadDb);
    }

    @Override
    public String getAdeDtScadDbObj() {
        if (ws.getIndAdes().getDtScad() >= 0) {
            return getAdeDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtScadDbObj(String adeDtScadDbObj) {
        if (adeDtScadDbObj != null) {
            setAdeDtScadDb(adeDtScadDbObj);
            ws.getIndAdes().setDtScad(((short)0));
        }
        else {
            ws.getIndAdes().setDtScad(((short)-1));
        }
    }

    @Override
    public String getAdeDtUltConsCnbtDb() {
        return ws.getAdesDb().getUltConsCnbtDb();
    }

    @Override
    public void setAdeDtUltConsCnbtDb(String adeDtUltConsCnbtDb) {
        this.ws.getAdesDb().setUltConsCnbtDb(adeDtUltConsCnbtDb);
    }

    @Override
    public String getAdeDtUltConsCnbtDbObj() {
        if (ws.getIndAdes().getDtUltConsCnbt() >= 0) {
            return getAdeDtUltConsCnbtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtUltConsCnbtDbObj(String adeDtUltConsCnbtDbObj) {
        if (adeDtUltConsCnbtDbObj != null) {
            setAdeDtUltConsCnbtDb(adeDtUltConsCnbtDbObj);
            ws.getIndAdes().setDtUltConsCnbt(((short)0));
        }
        else {
            ws.getIndAdes().setDtUltConsCnbt(((short)-1));
        }
    }

    @Override
    public String getAdeDtVarzTpIasDb() {
        return ws.getAdesDb().getVarzTpIasDb();
    }

    @Override
    public void setAdeDtVarzTpIasDb(String adeDtVarzTpIasDb) {
        this.ws.getAdesDb().setVarzTpIasDb(adeDtVarzTpIasDb);
    }

    @Override
    public String getAdeDtVarzTpIasDbObj() {
        if (ws.getIndAdes().getDtVarzTpIas() >= 0) {
            return getAdeDtVarzTpIasDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDtVarzTpIasDbObj(String adeDtVarzTpIasDbObj) {
        if (adeDtVarzTpIasDbObj != null) {
            setAdeDtVarzTpIasDb(adeDtVarzTpIasDbObj);
            ws.getIndAdes().setDtVarzTpIas(((short)0));
        }
        else {
            ws.getIndAdes().setDtVarzTpIas(((short)-1));
        }
    }

    @Override
    public int getAdeDurAa() {
        return ades.getAdeDurAa().getAdeDurAa();
    }

    @Override
    public void setAdeDurAa(int adeDurAa) {
        this.ades.getAdeDurAa().setAdeDurAa(adeDurAa);
    }

    @Override
    public Integer getAdeDurAaObj() {
        if (ws.getIndAdes().getDurAa() >= 0) {
            return ((Integer)getAdeDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDurAaObj(Integer adeDurAaObj) {
        if (adeDurAaObj != null) {
            setAdeDurAa(((int)adeDurAaObj));
            ws.getIndAdes().setDurAa(((short)0));
        }
        else {
            ws.getIndAdes().setDurAa(((short)-1));
        }
    }

    @Override
    public int getAdeDurGg() {
        return ades.getAdeDurGg().getAdeDurGg();
    }

    @Override
    public void setAdeDurGg(int adeDurGg) {
        this.ades.getAdeDurGg().setAdeDurGg(adeDurGg);
    }

    @Override
    public Integer getAdeDurGgObj() {
        if (ws.getIndAdes().getDurGg() >= 0) {
            return ((Integer)getAdeDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDurGgObj(Integer adeDurGgObj) {
        if (adeDurGgObj != null) {
            setAdeDurGg(((int)adeDurGgObj));
            ws.getIndAdes().setDurGg(((short)0));
        }
        else {
            ws.getIndAdes().setDurGg(((short)-1));
        }
    }

    @Override
    public int getAdeDurMm() {
        return ades.getAdeDurMm().getAdeDurMm();
    }

    @Override
    public void setAdeDurMm(int adeDurMm) {
        this.ades.getAdeDurMm().setAdeDurMm(adeDurMm);
    }

    @Override
    public Integer getAdeDurMmObj() {
        if (ws.getIndAdes().getDurMm() >= 0) {
            return ((Integer)getAdeDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeDurMmObj(Integer adeDurMmObj) {
        if (adeDurMmObj != null) {
            setAdeDurMm(((int)adeDurMmObj));
            ws.getIndAdes().setDurMm(((short)0));
        }
        else {
            ws.getIndAdes().setDurMm(((short)-1));
        }
    }

    @Override
    public int getAdeEtaAScad() {
        return ades.getAdeEtaAScad().getAdeEtaAScad();
    }

    @Override
    public void setAdeEtaAScad(int adeEtaAScad) {
        this.ades.getAdeEtaAScad().setAdeEtaAScad(adeEtaAScad);
    }

    @Override
    public Integer getAdeEtaAScadObj() {
        if (ws.getIndAdes().getEtaAScad() >= 0) {
            return ((Integer)getAdeEtaAScad());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeEtaAScadObj(Integer adeEtaAScadObj) {
        if (adeEtaAScadObj != null) {
            setAdeEtaAScad(((int)adeEtaAScadObj));
            ws.getIndAdes().setEtaAScad(((short)0));
        }
        else {
            ws.getIndAdes().setEtaAScad(((short)-1));
        }
    }

    @Override
    public char getAdeFlAttiv() {
        return ades.getAdeFlAttiv();
    }

    @Override
    public void setAdeFlAttiv(char adeFlAttiv) {
        this.ades.setAdeFlAttiv(adeFlAttiv);
    }

    @Override
    public Character getAdeFlAttivObj() {
        if (ws.getIndAdes().getFlAttiv() >= 0) {
            return ((Character)getAdeFlAttiv());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlAttivObj(Character adeFlAttivObj) {
        if (adeFlAttivObj != null) {
            setAdeFlAttiv(((char)adeFlAttivObj));
            ws.getIndAdes().setFlAttiv(((short)0));
        }
        else {
            ws.getIndAdes().setFlAttiv(((short)-1));
        }
    }

    @Override
    public char getAdeFlCoincAssto() {
        return ades.getAdeFlCoincAssto();
    }

    @Override
    public void setAdeFlCoincAssto(char adeFlCoincAssto) {
        this.ades.setAdeFlCoincAssto(adeFlCoincAssto);
    }

    @Override
    public Character getAdeFlCoincAsstoObj() {
        if (ws.getIndAdes().getFlCoincAssto() >= 0) {
            return ((Character)getAdeFlCoincAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlCoincAsstoObj(Character adeFlCoincAsstoObj) {
        if (adeFlCoincAsstoObj != null) {
            setAdeFlCoincAssto(((char)adeFlCoincAsstoObj));
            ws.getIndAdes().setFlCoincAssto(((short)0));
        }
        else {
            ws.getIndAdes().setFlCoincAssto(((short)-1));
        }
    }

    @Override
    public char getAdeFlProvzaMigraz() {
        return ades.getAdeFlProvzaMigraz();
    }

    @Override
    public void setAdeFlProvzaMigraz(char adeFlProvzaMigraz) {
        this.ades.setAdeFlProvzaMigraz(adeFlProvzaMigraz);
    }

    @Override
    public Character getAdeFlProvzaMigrazObj() {
        if (ws.getIndAdes().getFlProvzaMigraz() >= 0) {
            return ((Character)getAdeFlProvzaMigraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlProvzaMigrazObj(Character adeFlProvzaMigrazObj) {
        if (adeFlProvzaMigrazObj != null) {
            setAdeFlProvzaMigraz(((char)adeFlProvzaMigrazObj));
            ws.getIndAdes().setFlProvzaMigraz(((short)0));
        }
        else {
            ws.getIndAdes().setFlProvzaMigraz(((short)-1));
        }
    }

    @Override
    public char getAdeFlVarzStatTbgc() {
        return ades.getAdeFlVarzStatTbgc();
    }

    @Override
    public void setAdeFlVarzStatTbgc(char adeFlVarzStatTbgc) {
        this.ades.setAdeFlVarzStatTbgc(adeFlVarzStatTbgc);
    }

    @Override
    public Character getAdeFlVarzStatTbgcObj() {
        if (ws.getIndAdes().getFlVarzStatTbgc() >= 0) {
            return ((Character)getAdeFlVarzStatTbgc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeFlVarzStatTbgcObj(Character adeFlVarzStatTbgcObj) {
        if (adeFlVarzStatTbgcObj != null) {
            setAdeFlVarzStatTbgc(((char)adeFlVarzStatTbgcObj));
            ws.getIndAdes().setFlVarzStatTbgc(((short)0));
        }
        else {
            ws.getIndAdes().setFlVarzStatTbgc(((short)-1));
        }
    }

    @Override
    public String getAdeIbDflt() {
        return ades.getAdeIbDflt();
    }

    @Override
    public void setAdeIbDflt(String adeIbDflt) {
        this.ades.setAdeIbDflt(adeIbDflt);
    }

    @Override
    public String getAdeIbDfltObj() {
        if (ws.getIndAdes().getIbDflt() >= 0) {
            return getAdeIbDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIbDfltObj(String adeIbDfltObj) {
        if (adeIbDfltObj != null) {
            setAdeIbDflt(adeIbDfltObj);
            ws.getIndAdes().setIbDflt(((short)0));
        }
        else {
            ws.getIndAdes().setIbDflt(((short)-1));
        }
    }

    @Override
    public String getAdeIbOgg() {
        return ades.getAdeIbOgg();
    }

    @Override
    public void setAdeIbOgg(String adeIbOgg) {
        this.ades.setAdeIbOgg(adeIbOgg);
    }

    @Override
    public String getAdeIbOggObj() {
        if (ws.getIndAdes().getIbOgg() >= 0) {
            return getAdeIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIbOggObj(String adeIbOggObj) {
        if (adeIbOggObj != null) {
            setAdeIbOgg(adeIbOggObj);
            ws.getIndAdes().setIbOgg(((short)0));
        }
        else {
            ws.getIndAdes().setIbOgg(((short)-1));
        }
    }

    @Override
    public String getAdeIbPrev() {
        return ades.getAdeIbPrev();
    }

    @Override
    public void setAdeIbPrev(String adeIbPrev) {
        this.ades.setAdeIbPrev(adeIbPrev);
    }

    @Override
    public String getAdeIbPrevObj() {
        if (ws.getIndAdes().getIbPrev() >= 0) {
            return getAdeIbPrev();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIbPrevObj(String adeIbPrevObj) {
        if (adeIbPrevObj != null) {
            setAdeIbPrev(adeIbPrevObj);
            ws.getIndAdes().setIbPrev(((short)0));
        }
        else {
            ws.getIndAdes().setIbPrev(((short)-1));
        }
    }

    @Override
    public int getAdeIdAdes() {
        return ades.getAdeIdAdes();
    }

    @Override
    public void setAdeIdAdes(int adeIdAdes) {
        this.ades.setAdeIdAdes(adeIdAdes);
    }

    @Override
    public int getAdeIdMoviChiu() {
        return ades.getAdeIdMoviChiu().getAdeIdMoviChiu();
    }

    @Override
    public void setAdeIdMoviChiu(int adeIdMoviChiu) {
        this.ades.getAdeIdMoviChiu().setAdeIdMoviChiu(adeIdMoviChiu);
    }

    @Override
    public Integer getAdeIdMoviChiuObj() {
        if (ws.getIndAdes().getIdMoviChiu() >= 0) {
            return ((Integer)getAdeIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIdMoviChiuObj(Integer adeIdMoviChiuObj) {
        if (adeIdMoviChiuObj != null) {
            setAdeIdMoviChiu(((int)adeIdMoviChiuObj));
            ws.getIndAdes().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndAdes().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getAdeIdMoviCrz() {
        return ades.getAdeIdMoviCrz();
    }

    @Override
    public void setAdeIdMoviCrz(int adeIdMoviCrz) {
        this.ades.setAdeIdMoviCrz(adeIdMoviCrz);
    }

    @Override
    public int getAdeIdPoli() {
        return ades.getAdeIdPoli();
    }

    @Override
    public void setAdeIdPoli(int adeIdPoli) {
        this.ades.setAdeIdPoli(adeIdPoli);
    }

    @Override
    public String getAdeIdenIscFnd() {
        return ades.getAdeIdenIscFnd();
    }

    @Override
    public void setAdeIdenIscFnd(String adeIdenIscFnd) {
        this.ades.setAdeIdenIscFnd(adeIdenIscFnd);
    }

    @Override
    public String getAdeIdenIscFndObj() {
        if (ws.getIndAdes().getIdenIscFnd() >= 0) {
            return getAdeIdenIscFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeIdenIscFndObj(String adeIdenIscFndObj) {
        if (adeIdenIscFndObj != null) {
            setAdeIdenIscFnd(adeIdenIscFndObj);
            ws.getIndAdes().setIdenIscFnd(((short)0));
        }
        else {
            ws.getIndAdes().setIdenIscFnd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpAder() {
        return ades.getAdeImpAder().getAdeImpAder();
    }

    @Override
    public void setAdeImpAder(AfDecimal adeImpAder) {
        this.ades.getAdeImpAder().setAdeImpAder(adeImpAder.copy());
    }

    @Override
    public AfDecimal getAdeImpAderObj() {
        if (ws.getIndAdes().getImpAder() >= 0) {
            return getAdeImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpAderObj(AfDecimal adeImpAderObj) {
        if (adeImpAderObj != null) {
            setAdeImpAder(new AfDecimal(adeImpAderObj, 15, 3));
            ws.getIndAdes().setImpAder(((short)0));
        }
        else {
            ws.getIndAdes().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpAz() {
        return ades.getAdeImpAz().getAdeImpAz();
    }

    @Override
    public void setAdeImpAz(AfDecimal adeImpAz) {
        this.ades.getAdeImpAz().setAdeImpAz(adeImpAz.copy());
    }

    @Override
    public AfDecimal getAdeImpAzObj() {
        if (ws.getIndAdes().getImpAz() >= 0) {
            return getAdeImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpAzObj(AfDecimal adeImpAzObj) {
        if (adeImpAzObj != null) {
            setAdeImpAz(new AfDecimal(adeImpAzObj, 15, 3));
            ws.getIndAdes().setImpAz(((short)0));
        }
        else {
            ws.getIndAdes().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpGarCnbt() {
        return ades.getAdeImpGarCnbt().getAdeImpGarCnbt();
    }

    @Override
    public void setAdeImpGarCnbt(AfDecimal adeImpGarCnbt) {
        this.ades.getAdeImpGarCnbt().setAdeImpGarCnbt(adeImpGarCnbt.copy());
    }

    @Override
    public AfDecimal getAdeImpGarCnbtObj() {
        if (ws.getIndAdes().getImpGarCnbt() >= 0) {
            return getAdeImpGarCnbt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpGarCnbtObj(AfDecimal adeImpGarCnbtObj) {
        if (adeImpGarCnbtObj != null) {
            setAdeImpGarCnbt(new AfDecimal(adeImpGarCnbtObj, 15, 3));
            ws.getIndAdes().setImpGarCnbt(((short)0));
        }
        else {
            ws.getIndAdes().setImpGarCnbt(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpRecRitAcc() {
        return ades.getAdeImpRecRitAcc().getAdeImpRecRitAcc();
    }

    @Override
    public void setAdeImpRecRitAcc(AfDecimal adeImpRecRitAcc) {
        this.ades.getAdeImpRecRitAcc().setAdeImpRecRitAcc(adeImpRecRitAcc.copy());
    }

    @Override
    public AfDecimal getAdeImpRecRitAccObj() {
        if (ws.getIndAdes().getImpRecRitAcc() >= 0) {
            return getAdeImpRecRitAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpRecRitAccObj(AfDecimal adeImpRecRitAccObj) {
        if (adeImpRecRitAccObj != null) {
            setAdeImpRecRitAcc(new AfDecimal(adeImpRecRitAccObj, 15, 3));
            ws.getIndAdes().setImpRecRitAcc(((short)0));
        }
        else {
            ws.getIndAdes().setImpRecRitAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpRecRitVis() {
        return ades.getAdeImpRecRitVis().getAdeImpRecRitVis();
    }

    @Override
    public void setAdeImpRecRitVis(AfDecimal adeImpRecRitVis) {
        this.ades.getAdeImpRecRitVis().setAdeImpRecRitVis(adeImpRecRitVis.copy());
    }

    @Override
    public AfDecimal getAdeImpRecRitVisObj() {
        if (ws.getIndAdes().getImpRecRitVis() >= 0) {
            return getAdeImpRecRitVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpRecRitVisObj(AfDecimal adeImpRecRitVisObj) {
        if (adeImpRecRitVisObj != null) {
            setAdeImpRecRitVis(new AfDecimal(adeImpRecRitVisObj, 15, 3));
            ws.getIndAdes().setImpRecRitVis(((short)0));
        }
        else {
            ws.getIndAdes().setImpRecRitVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpTfr() {
        return ades.getAdeImpTfr().getAdeImpTfr();
    }

    @Override
    public void setAdeImpTfr(AfDecimal adeImpTfr) {
        this.ades.getAdeImpTfr().setAdeImpTfr(adeImpTfr.copy());
    }

    @Override
    public AfDecimal getAdeImpTfrObj() {
        if (ws.getIndAdes().getImpTfr() >= 0) {
            return getAdeImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpTfrObj(AfDecimal adeImpTfrObj) {
        if (adeImpTfrObj != null) {
            setAdeImpTfr(new AfDecimal(adeImpTfrObj, 15, 3));
            ws.getIndAdes().setImpTfr(((short)0));
        }
        else {
            ws.getIndAdes().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpVolo() {
        return ades.getAdeImpVolo().getAdeImpVolo();
    }

    @Override
    public void setAdeImpVolo(AfDecimal adeImpVolo) {
        this.ades.getAdeImpVolo().setAdeImpVolo(adeImpVolo.copy());
    }

    @Override
    public AfDecimal getAdeImpVoloObj() {
        if (ws.getIndAdes().getImpVolo() >= 0) {
            return getAdeImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpVoloObj(AfDecimal adeImpVoloObj) {
        if (adeImpVoloObj != null) {
            setAdeImpVolo(new AfDecimal(adeImpVoloObj, 15, 3));
            ws.getIndAdes().setImpVolo(((short)0));
        }
        else {
            ws.getIndAdes().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeImpbVisDaRec() {
        return ades.getAdeImpbVisDaRec().getAdeImpbVisDaRec();
    }

    @Override
    public void setAdeImpbVisDaRec(AfDecimal adeImpbVisDaRec) {
        this.ades.getAdeImpbVisDaRec().setAdeImpbVisDaRec(adeImpbVisDaRec.copy());
    }

    @Override
    public AfDecimal getAdeImpbVisDaRecObj() {
        if (ws.getIndAdes().getImpbVisDaRec() >= 0) {
            return getAdeImpbVisDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeImpbVisDaRecObj(AfDecimal adeImpbVisDaRecObj) {
        if (adeImpbVisDaRecObj != null) {
            setAdeImpbVisDaRec(new AfDecimal(adeImpbVisDaRecObj, 15, 3));
            ws.getIndAdes().setImpbVisDaRec(((short)0));
        }
        else {
            ws.getIndAdes().setImpbVisDaRec(((short)-1));
        }
    }

    @Override
    public String getAdeModCalc() {
        return ades.getAdeModCalc();
    }

    @Override
    public void setAdeModCalc(String adeModCalc) {
        this.ades.setAdeModCalc(adeModCalc);
    }

    @Override
    public String getAdeModCalcObj() {
        if (ws.getIndAdes().getModCalc() >= 0) {
            return getAdeModCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeModCalcObj(String adeModCalcObj) {
        if (adeModCalcObj != null) {
            setAdeModCalc(adeModCalcObj);
            ws.getIndAdes().setModCalc(((short)0));
        }
        else {
            ws.getIndAdes().setModCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeNumRatPian() {
        return ades.getAdeNumRatPian().getAdeNumRatPian();
    }

    @Override
    public void setAdeNumRatPian(AfDecimal adeNumRatPian) {
        this.ades.getAdeNumRatPian().setAdeNumRatPian(adeNumRatPian.copy());
    }

    @Override
    public AfDecimal getAdeNumRatPianObj() {
        if (ws.getIndAdes().getNumRatPian() >= 0) {
            return getAdeNumRatPian();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeNumRatPianObj(AfDecimal adeNumRatPianObj) {
        if (adeNumRatPianObj != null) {
            setAdeNumRatPian(new AfDecimal(adeNumRatPianObj, 12, 5));
            ws.getIndAdes().setNumRatPian(((short)0));
        }
        else {
            ws.getIndAdes().setNumRatPian(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcAder() {
        return ades.getAdePcAder().getAdePcAder();
    }

    @Override
    public void setAdePcAder(AfDecimal adePcAder) {
        this.ades.getAdePcAder().setAdePcAder(adePcAder.copy());
    }

    @Override
    public AfDecimal getAdePcAderObj() {
        if (ws.getIndAdes().getPcAder() >= 0) {
            return getAdePcAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcAderObj(AfDecimal adePcAderObj) {
        if (adePcAderObj != null) {
            setAdePcAder(new AfDecimal(adePcAderObj, 6, 3));
            ws.getIndAdes().setPcAder(((short)0));
        }
        else {
            ws.getIndAdes().setPcAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcAz() {
        return ades.getAdePcAz().getAdePcAz();
    }

    @Override
    public void setAdePcAz(AfDecimal adePcAz) {
        this.ades.getAdePcAz().setAdePcAz(adePcAz.copy());
    }

    @Override
    public AfDecimal getAdePcAzObj() {
        if (ws.getIndAdes().getPcAz() >= 0) {
            return getAdePcAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcAzObj(AfDecimal adePcAzObj) {
        if (adePcAzObj != null) {
            setAdePcAz(new AfDecimal(adePcAzObj, 6, 3));
            ws.getIndAdes().setPcAz(((short)0));
        }
        else {
            ws.getIndAdes().setPcAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcTfr() {
        return ades.getAdePcTfr().getAdePcTfr();
    }

    @Override
    public void setAdePcTfr(AfDecimal adePcTfr) {
        this.ades.getAdePcTfr().setAdePcTfr(adePcTfr.copy());
    }

    @Override
    public AfDecimal getAdePcTfrObj() {
        if (ws.getIndAdes().getPcTfr() >= 0) {
            return getAdePcTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcTfrObj(AfDecimal adePcTfrObj) {
        if (adePcTfrObj != null) {
            setAdePcTfr(new AfDecimal(adePcTfrObj, 6, 3));
            ws.getIndAdes().setPcTfr(((short)0));
        }
        else {
            ws.getIndAdes().setPcTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePcVolo() {
        return ades.getAdePcVolo().getAdePcVolo();
    }

    @Override
    public void setAdePcVolo(AfDecimal adePcVolo) {
        this.ades.getAdePcVolo().setAdePcVolo(adePcVolo.copy());
    }

    @Override
    public AfDecimal getAdePcVoloObj() {
        if (ws.getIndAdes().getPcVolo() >= 0) {
            return getAdePcVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePcVoloObj(AfDecimal adePcVoloObj) {
        if (adePcVoloObj != null) {
            setAdePcVolo(new AfDecimal(adePcVoloObj, 6, 3));
            ws.getIndAdes().setPcVolo(((short)0));
        }
        else {
            ws.getIndAdes().setPcVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePreLrdInd() {
        return ades.getAdePreLrdInd().getAdePreLrdInd();
    }

    @Override
    public void setAdePreLrdInd(AfDecimal adePreLrdInd) {
        this.ades.getAdePreLrdInd().setAdePreLrdInd(adePreLrdInd.copy());
    }

    @Override
    public AfDecimal getAdePreLrdIndObj() {
        if (ws.getIndAdes().getPreLrdInd() >= 0) {
            return getAdePreLrdInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePreLrdIndObj(AfDecimal adePreLrdIndObj) {
        if (adePreLrdIndObj != null) {
            setAdePreLrdInd(new AfDecimal(adePreLrdIndObj, 15, 3));
            ws.getIndAdes().setPreLrdInd(((short)0));
        }
        else {
            ws.getIndAdes().setPreLrdInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePreNetInd() {
        return ades.getAdePreNetInd().getAdePreNetInd();
    }

    @Override
    public void setAdePreNetInd(AfDecimal adePreNetInd) {
        this.ades.getAdePreNetInd().setAdePreNetInd(adePreNetInd.copy());
    }

    @Override
    public AfDecimal getAdePreNetIndObj() {
        if (ws.getIndAdes().getPreNetInd() >= 0) {
            return getAdePreNetInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePreNetIndObj(AfDecimal adePreNetIndObj) {
        if (adePreNetIndObj != null) {
            setAdePreNetInd(new AfDecimal(adePreNetIndObj, 15, 3));
            ws.getIndAdes().setPreNetInd(((short)0));
        }
        else {
            ws.getIndAdes().setPreNetInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdePrstzIniInd() {
        return ades.getAdePrstzIniInd().getAdePrstzIniInd();
    }

    @Override
    public void setAdePrstzIniInd(AfDecimal adePrstzIniInd) {
        this.ades.getAdePrstzIniInd().setAdePrstzIniInd(adePrstzIniInd.copy());
    }

    @Override
    public AfDecimal getAdePrstzIniIndObj() {
        if (ws.getIndAdes().getPrstzIniInd() >= 0) {
            return getAdePrstzIniInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdePrstzIniIndObj(AfDecimal adePrstzIniIndObj) {
        if (adePrstzIniIndObj != null) {
            setAdePrstzIniInd(new AfDecimal(adePrstzIniIndObj, 15, 3));
            ws.getIndAdes().setPrstzIniInd(((short)0));
        }
        else {
            ws.getIndAdes().setPrstzIniInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getAdeRatLrdInd() {
        return ades.getAdeRatLrdInd().getAdeRatLrdInd();
    }

    @Override
    public void setAdeRatLrdInd(AfDecimal adeRatLrdInd) {
        this.ades.getAdeRatLrdInd().setAdeRatLrdInd(adeRatLrdInd.copy());
    }

    @Override
    public AfDecimal getAdeRatLrdIndObj() {
        if (ws.getIndAdes().getRatLrdInd() >= 0) {
            return getAdeRatLrdInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeRatLrdIndObj(AfDecimal adeRatLrdIndObj) {
        if (adeRatLrdIndObj != null) {
            setAdeRatLrdInd(new AfDecimal(adeRatLrdIndObj, 15, 3));
            ws.getIndAdes().setRatLrdInd(((short)0));
        }
        else {
            ws.getIndAdes().setRatLrdInd(((short)-1));
        }
    }

    @Override
    public String getAdeTpFntCnbtva() {
        return ades.getAdeTpFntCnbtva();
    }

    @Override
    public void setAdeTpFntCnbtva(String adeTpFntCnbtva) {
        this.ades.setAdeTpFntCnbtva(adeTpFntCnbtva);
    }

    @Override
    public String getAdeTpFntCnbtvaObj() {
        if (ws.getIndAdes().getTpFntCnbtva() >= 0) {
            return getAdeTpFntCnbtva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeTpFntCnbtvaObj(String adeTpFntCnbtvaObj) {
        if (adeTpFntCnbtvaObj != null) {
            setAdeTpFntCnbtva(adeTpFntCnbtvaObj);
            ws.getIndAdes().setTpFntCnbtva(((short)0));
        }
        else {
            ws.getIndAdes().setTpFntCnbtva(((short)-1));
        }
    }

    @Override
    public String getAdeTpIas() {
        return ades.getAdeTpIas();
    }

    @Override
    public void setAdeTpIas(String adeTpIas) {
        this.ades.setAdeTpIas(adeTpIas);
    }

    @Override
    public String getAdeTpIasObj() {
        if (ws.getIndAdes().getTpIas() >= 0) {
            return getAdeTpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeTpIasObj(String adeTpIasObj) {
        if (adeTpIasObj != null) {
            setAdeTpIas(adeTpIasObj);
            ws.getIndAdes().setTpIas(((short)0));
        }
        else {
            ws.getIndAdes().setTpIas(((short)-1));
        }
    }

    @Override
    public String getAdeTpModPagTit() {
        return ades.getAdeTpModPagTit();
    }

    @Override
    public void setAdeTpModPagTit(String adeTpModPagTit) {
        this.ades.setAdeTpModPagTit(adeTpModPagTit);
    }

    @Override
    public String getAdeTpRgmFisc() {
        return ades.getAdeTpRgmFisc();
    }

    @Override
    public void setAdeTpRgmFisc(String adeTpRgmFisc) {
        this.ades.setAdeTpRgmFisc(adeTpRgmFisc);
    }

    @Override
    public String getAdeTpRiat() {
        return ades.getAdeTpRiat();
    }

    @Override
    public void setAdeTpRiat(String adeTpRiat) {
        this.ades.setAdeTpRiat(adeTpRiat);
    }

    @Override
    public String getAdeTpRiatObj() {
        if (ws.getIndAdes().getTpRiat() >= 0) {
            return getAdeTpRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAdeTpRiatObj(String adeTpRiatObj) {
        if (adeTpRiatObj != null) {
            setAdeTpRiat(adeTpRiatObj);
            ws.getIndAdes().setTpRiat(((short)0));
        }
        else {
            ws.getIndAdes().setTpRiat(((short)-1));
        }
    }

    public Ades getAdes() {
        return ades;
    }

    public AdesPoliLdbm02501 getAdesPoliLdbm02501() {
        return adesPoliLdbm02501;
    }

    public AdesPoliLdbm02502 getAdesPoliLdbm02502() {
        return adesPoliLdbm02502;
    }

    public AdesPoliLdbm0250 getAdesPoliLdbm0250() {
        return adesPoliLdbm0250;
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public char getConcsPrest() {
        throw new FieldNotMappedException("concsPrest");
    }

    @Override
    public void setConcsPrest(char concsPrest) {
        throw new FieldNotMappedException("concsPrest");
    }

    @Override
    public Character getConcsPrestObj() {
        return ((Character)getConcsPrest());
    }

    @Override
    public void setConcsPrestObj(Character concsPrestObj) {
        setConcsPrest(((char)concsPrestObj));
    }

    @Override
    public AfDecimal getCumCnbtCap() {
        throw new FieldNotMappedException("cumCnbtCap");
    }

    @Override
    public void setCumCnbtCap(AfDecimal cumCnbtCap) {
        throw new FieldNotMappedException("cumCnbtCap");
    }

    @Override
    public AfDecimal getCumCnbtCapObj() {
        return getCumCnbtCap();
    }

    @Override
    public void setCumCnbtCapObj(AfDecimal cumCnbtCapObj) {
        setCumCnbtCap(new AfDecimal(cumCnbtCapObj, 15, 3));
    }

    @Override
    public char getDsOperSql() {
        return ades.getAdeDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.ades.setAdeDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsEndCptz() {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public long getDsTsIniCptz() {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public String getDsUtente() {
        return ades.getAdeDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.ades.setAdeDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtDecorDb() {
        throw new FieldNotMappedException("dtDecorDb");
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        throw new FieldNotMappedException("dtDecorDb");
    }

    @Override
    public String getDtDecorDbObj() {
        return getDtDecorDb();
    }

    @Override
    public void setDtDecorDbObj(String dtDecorDbObj) {
        setDtDecorDb(dtDecorDbObj);
    }

    @Override
    public String getDtDecorPrestBanDb() {
        throw new FieldNotMappedException("dtDecorPrestBanDb");
    }

    @Override
    public void setDtDecorPrestBanDb(String dtDecorPrestBanDb) {
        throw new FieldNotMappedException("dtDecorPrestBanDb");
    }

    @Override
    public String getDtDecorPrestBanDbObj() {
        return getDtDecorPrestBanDb();
    }

    @Override
    public void setDtDecorPrestBanDbObj(String dtDecorPrestBanDbObj) {
        setDtDecorPrestBanDb(dtDecorPrestBanDbObj);
    }

    @Override
    public String getDtEffVarzStatTDb() {
        throw new FieldNotMappedException("dtEffVarzStatTDb");
    }

    @Override
    public void setDtEffVarzStatTDb(String dtEffVarzStatTDb) {
        throw new FieldNotMappedException("dtEffVarzStatTDb");
    }

    @Override
    public String getDtEffVarzStatTDbObj() {
        return getDtEffVarzStatTDb();
    }

    @Override
    public void setDtEffVarzStatTDbObj(String dtEffVarzStatTDbObj) {
        setDtEffVarzStatTDb(dtEffVarzStatTDbObj);
    }

    @Override
    public String getDtEndEffDb() {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public String getDtIniEffDb() {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        throw new FieldNotMappedException("dtIniEffDb");
    }

    @Override
    public String getDtNovaRgmFiscDb() {
        throw new FieldNotMappedException("dtNovaRgmFiscDb");
    }

    @Override
    public void setDtNovaRgmFiscDb(String dtNovaRgmFiscDb) {
        throw new FieldNotMappedException("dtNovaRgmFiscDb");
    }

    @Override
    public String getDtNovaRgmFiscDbObj() {
        return getDtNovaRgmFiscDb();
    }

    @Override
    public void setDtNovaRgmFiscDbObj(String dtNovaRgmFiscDbObj) {
        setDtNovaRgmFiscDb(dtNovaRgmFiscDbObj);
    }

    @Override
    public String getDtPrescDb() {
        throw new FieldNotMappedException("dtPrescDb");
    }

    @Override
    public void setDtPrescDb(String dtPrescDb) {
        throw new FieldNotMappedException("dtPrescDb");
    }

    @Override
    public String getDtPrescDbObj() {
        return getDtPrescDb();
    }

    @Override
    public void setDtPrescDbObj(String dtPrescDbObj) {
        setDtPrescDb(dtPrescDbObj);
    }

    @Override
    public String getDtScadDb() {
        throw new FieldNotMappedException("dtScadDb");
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        throw new FieldNotMappedException("dtScadDb");
    }

    @Override
    public String getDtScadDbObj() {
        return getDtScadDb();
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        setDtScadDb(dtScadDbObj);
    }

    @Override
    public String getDtUltConsCnbtDb() {
        throw new FieldNotMappedException("dtUltConsCnbtDb");
    }

    @Override
    public void setDtUltConsCnbtDb(String dtUltConsCnbtDb) {
        throw new FieldNotMappedException("dtUltConsCnbtDb");
    }

    @Override
    public String getDtUltConsCnbtDbObj() {
        return getDtUltConsCnbtDb();
    }

    @Override
    public void setDtUltConsCnbtDbObj(String dtUltConsCnbtDbObj) {
        setDtUltConsCnbtDb(dtUltConsCnbtDbObj);
    }

    @Override
    public String getDtVarzTpIasDb() {
        throw new FieldNotMappedException("dtVarzTpIasDb");
    }

    @Override
    public void setDtVarzTpIasDb(String dtVarzTpIasDb) {
        throw new FieldNotMappedException("dtVarzTpIasDb");
    }

    @Override
    public String getDtVarzTpIasDbObj() {
        return getDtVarzTpIasDb();
    }

    @Override
    public void setDtVarzTpIasDbObj(String dtVarzTpIasDbObj) {
        setDtVarzTpIasDb(dtVarzTpIasDbObj);
    }

    @Override
    public int getDurAa() {
        throw new FieldNotMappedException("durAa");
    }

    @Override
    public void setDurAa(int durAa) {
        throw new FieldNotMappedException("durAa");
    }

    @Override
    public Integer getDurAaObj() {
        return ((Integer)getDurAa());
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        setDurAa(((int)durAaObj));
    }

    @Override
    public int getDurGg() {
        throw new FieldNotMappedException("durGg");
    }

    @Override
    public void setDurGg(int durGg) {
        throw new FieldNotMappedException("durGg");
    }

    @Override
    public Integer getDurGgObj() {
        return ((Integer)getDurGg());
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        setDurGg(((int)durGgObj));
    }

    @Override
    public int getDurMm() {
        throw new FieldNotMappedException("durMm");
    }

    @Override
    public void setDurMm(int durMm) {
        throw new FieldNotMappedException("durMm");
    }

    @Override
    public Integer getDurMmObj() {
        return ((Integer)getDurMm());
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        setDurMm(((int)durMmObj));
    }

    @Override
    public int getEtaAScad() {
        throw new FieldNotMappedException("etaAScad");
    }

    @Override
    public void setEtaAScad(int etaAScad) {
        throw new FieldNotMappedException("etaAScad");
    }

    @Override
    public Integer getEtaAScadObj() {
        return ((Integer)getEtaAScad());
    }

    @Override
    public void setEtaAScadObj(Integer etaAScadObj) {
        setEtaAScad(((int)etaAScadObj));
    }

    @Override
    public char getFlAttiv() {
        throw new FieldNotMappedException("flAttiv");
    }

    @Override
    public void setFlAttiv(char flAttiv) {
        throw new FieldNotMappedException("flAttiv");
    }

    @Override
    public Character getFlAttivObj() {
        return ((Character)getFlAttiv());
    }

    @Override
    public void setFlAttivObj(Character flAttivObj) {
        setFlAttiv(((char)flAttivObj));
    }

    @Override
    public char getFlCoincAssto() {
        throw new FieldNotMappedException("flCoincAssto");
    }

    @Override
    public void setFlCoincAssto(char flCoincAssto) {
        throw new FieldNotMappedException("flCoincAssto");
    }

    @Override
    public Character getFlCoincAsstoObj() {
        return ((Character)getFlCoincAssto());
    }

    @Override
    public void setFlCoincAsstoObj(Character flCoincAsstoObj) {
        setFlCoincAssto(((char)flCoincAsstoObj));
    }

    @Override
    public char getFlProvzaMigraz() {
        throw new FieldNotMappedException("flProvzaMigraz");
    }

    @Override
    public void setFlProvzaMigraz(char flProvzaMigraz) {
        throw new FieldNotMappedException("flProvzaMigraz");
    }

    @Override
    public Character getFlProvzaMigrazObj() {
        return ((Character)getFlProvzaMigraz());
    }

    @Override
    public void setFlProvzaMigrazObj(Character flProvzaMigrazObj) {
        setFlProvzaMigraz(((char)flProvzaMigrazObj));
    }

    @Override
    public char getFlVarzStatTbgc() {
        throw new FieldNotMappedException("flVarzStatTbgc");
    }

    @Override
    public void setFlVarzStatTbgc(char flVarzStatTbgc) {
        throw new FieldNotMappedException("flVarzStatTbgc");
    }

    @Override
    public Character getFlVarzStatTbgcObj() {
        return ((Character)getFlVarzStatTbgc());
    }

    @Override
    public void setFlVarzStatTbgcObj(Character flVarzStatTbgcObj) {
        setFlVarzStatTbgc(((char)flVarzStatTbgcObj));
    }

    public Iabv0002 getIabv0002() {
        return iabv0002;
    }

    @Override
    public char getIabv0002State01() {
        return iabv0002.getIabv0002StateGlobal().getState01();
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        this.iabv0002.getIabv0002StateGlobal().setState01(iabv0002State01);
    }

    @Override
    public char getIabv0002State02() {
        return iabv0002.getIabv0002StateGlobal().getState02();
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        this.iabv0002.getIabv0002StateGlobal().setState02(iabv0002State02);
    }

    @Override
    public char getIabv0002State03() {
        return iabv0002.getIabv0002StateGlobal().getState03();
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        this.iabv0002.getIabv0002StateGlobal().setState03(iabv0002State03);
    }

    @Override
    public char getIabv0002State04() {
        return iabv0002.getIabv0002StateGlobal().getState04();
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        this.iabv0002.getIabv0002StateGlobal().setState04(iabv0002State04);
    }

    @Override
    public char getIabv0002State05() {
        return iabv0002.getIabv0002StateGlobal().getState05();
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        this.iabv0002.getIabv0002StateGlobal().setState05(iabv0002State05);
    }

    @Override
    public char getIabv0002State06() {
        return iabv0002.getIabv0002StateGlobal().getState06();
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        this.iabv0002.getIabv0002StateGlobal().setState06(iabv0002State06);
    }

    @Override
    public char getIabv0002State07() {
        return iabv0002.getIabv0002StateGlobal().getState07();
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        this.iabv0002.getIabv0002StateGlobal().setState07(iabv0002State07);
    }

    @Override
    public char getIabv0002State08() {
        return iabv0002.getIabv0002StateGlobal().getState08();
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        this.iabv0002.getIabv0002StateGlobal().setState08(iabv0002State08);
    }

    @Override
    public char getIabv0002State09() {
        return iabv0002.getIabv0002StateGlobal().getState09();
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        this.iabv0002.getIabv0002StateGlobal().setState09(iabv0002State09);
    }

    @Override
    public char getIabv0002State10() {
        return iabv0002.getIabv0002StateGlobal().getState10();
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        this.iabv0002.getIabv0002StateGlobal().setState10(iabv0002State10);
    }

    @Override
    public char getIabv0002StateCurrent() {
        return iabv0002.getIabv0002StateCurrent();
    }

    @Override
    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        this.iabv0002.setIabv0002StateCurrent(iabv0002StateCurrent);
    }

    @Override
    public int getIabv0009IdOggA() {
        return iabv0002.getIabv0009GestGuideService().getIdOggA();
    }

    @Override
    public void setIabv0009IdOggA(int iabv0009IdOggA) {
        this.iabv0002.getIabv0009GestGuideService().setIdOggA(iabv0009IdOggA);
    }

    @Override
    public int getIabv0009IdOggDa() {
        return iabv0002.getIabv0009GestGuideService().getIdOggDa();
    }

    @Override
    public void setIabv0009IdOggDa(int iabv0009IdOggDa) {
        this.iabv0002.getIabv0009GestGuideService().setIdOggDa(iabv0009IdOggDa);
    }

    @Override
    public int getIabv0009Versioning() {
        return iabv0002.getIabv0009GestGuideService().getVersioning();
    }

    @Override
    public void setIabv0009Versioning(int iabv0009Versioning) {
        this.iabv0002.getIabv0009GestGuideService().setVersioning(iabv0009Versioning);
    }

    @Override
    public String getIbDflt() {
        throw new FieldNotMappedException("ibDflt");
    }

    @Override
    public void setIbDflt(String ibDflt) {
        throw new FieldNotMappedException("ibDflt");
    }

    @Override
    public String getIbDfltObj() {
        return getIbDflt();
    }

    @Override
    public void setIbDfltObj(String ibDfltObj) {
        setIbDflt(ibDfltObj);
    }

    @Override
    public String getIbOgg() {
        throw new FieldNotMappedException("ibOgg");
    }

    @Override
    public void setIbOgg(String ibOgg) {
        throw new FieldNotMappedException("ibOgg");
    }

    @Override
    public String getIbOggObj() {
        return getIbOgg();
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        setIbOgg(ibOggObj);
    }

    @Override
    public String getIbPrev() {
        throw new FieldNotMappedException("ibPrev");
    }

    @Override
    public void setIbPrev(String ibPrev) {
        throw new FieldNotMappedException("ibPrev");
    }

    @Override
    public String getIbPrevObj() {
        return getIbPrev();
    }

    @Override
    public void setIbPrevObj(String ibPrevObj) {
        setIbPrev(ibPrevObj);
    }

    @Override
    public int getIdAdes() {
        throw new FieldNotMappedException("idAdes");
    }

    @Override
    public void setIdAdes(int idAdes) {
        throw new FieldNotMappedException("idAdes");
    }

    @Override
    public int getIdMoviChiu() {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public Integer getIdMoviChiuObj() {
        return ((Integer)getIdMoviChiu());
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        setIdMoviChiu(((int)idMoviChiuObj));
    }

    @Override
    public int getIdMoviCrz() {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public int getIdPoli() {
        throw new FieldNotMappedException("idPoli");
    }

    @Override
    public void setIdPoli(int idPoli) {
        throw new FieldNotMappedException("idPoli");
    }

    @Override
    public String getIdenIscFnd() {
        throw new FieldNotMappedException("idenIscFnd");
    }

    @Override
    public void setIdenIscFnd(String idenIscFnd) {
        throw new FieldNotMappedException("idenIscFnd");
    }

    @Override
    public String getIdenIscFndObj() {
        return getIdenIscFnd();
    }

    @Override
    public void setIdenIscFndObj(String idenIscFndObj) {
        setIdenIscFnd(idenIscFndObj);
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public AfDecimal getImpAderObj() {
        return getImpAder();
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        setImpAder(new AfDecimal(impAderObj, 15, 3));
    }

    @Override
    public AfDecimal getImpAz() {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public AfDecimal getImpAzObj() {
        return getImpAz();
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        setImpAz(new AfDecimal(impAzObj, 15, 3));
    }

    @Override
    public AfDecimal getImpGarCnbt() {
        throw new FieldNotMappedException("impGarCnbt");
    }

    @Override
    public void setImpGarCnbt(AfDecimal impGarCnbt) {
        throw new FieldNotMappedException("impGarCnbt");
    }

    @Override
    public AfDecimal getImpGarCnbtObj() {
        return getImpGarCnbt();
    }

    @Override
    public void setImpGarCnbtObj(AfDecimal impGarCnbtObj) {
        setImpGarCnbt(new AfDecimal(impGarCnbtObj, 15, 3));
    }

    @Override
    public AfDecimal getImpRecRitAcc() {
        throw new FieldNotMappedException("impRecRitAcc");
    }

    @Override
    public void setImpRecRitAcc(AfDecimal impRecRitAcc) {
        throw new FieldNotMappedException("impRecRitAcc");
    }

    @Override
    public AfDecimal getImpRecRitAccObj() {
        return getImpRecRitAcc();
    }

    @Override
    public void setImpRecRitAccObj(AfDecimal impRecRitAccObj) {
        setImpRecRitAcc(new AfDecimal(impRecRitAccObj, 15, 3));
    }

    @Override
    public AfDecimal getImpRecRitVis() {
        throw new FieldNotMappedException("impRecRitVis");
    }

    @Override
    public void setImpRecRitVis(AfDecimal impRecRitVis) {
        throw new FieldNotMappedException("impRecRitVis");
    }

    @Override
    public AfDecimal getImpRecRitVisObj() {
        return getImpRecRitVis();
    }

    @Override
    public void setImpRecRitVisObj(AfDecimal impRecRitVisObj) {
        setImpRecRitVis(new AfDecimal(impRecRitVisObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTfr() {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public AfDecimal getImpTfrObj() {
        return getImpTfr();
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        setImpTfr(new AfDecimal(impTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getImpVolo() {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public AfDecimal getImpVoloObj() {
        return getImpVolo();
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        setImpVolo(new AfDecimal(impVoloObj, 15, 3));
    }

    @Override
    public AfDecimal getImpbVisDaRec() {
        throw new FieldNotMappedException("impbVisDaRec");
    }

    @Override
    public void setImpbVisDaRec(AfDecimal impbVisDaRec) {
        throw new FieldNotMappedException("impbVisDaRec");
    }

    @Override
    public AfDecimal getImpbVisDaRecObj() {
        return getImpbVisDaRec();
    }

    @Override
    public void setImpbVisDaRecObj(AfDecimal impbVisDaRecObj) {
        setImpbVisDaRec(new AfDecimal(impbVisDaRecObj, 15, 3));
    }

    @Override
    public String getModCalc() {
        throw new FieldNotMappedException("modCalc");
    }

    @Override
    public void setModCalc(String modCalc) {
        throw new FieldNotMappedException("modCalc");
    }

    @Override
    public String getModCalcObj() {
        return getModCalc();
    }

    @Override
    public void setModCalcObj(String modCalcObj) {
        setModCalc(modCalcObj);
    }

    @Override
    public AfDecimal getNumRatPian() {
        throw new FieldNotMappedException("numRatPian");
    }

    @Override
    public void setNumRatPian(AfDecimal numRatPian) {
        throw new FieldNotMappedException("numRatPian");
    }

    @Override
    public AfDecimal getNumRatPianObj() {
        return getNumRatPian();
    }

    @Override
    public void setNumRatPianObj(AfDecimal numRatPianObj) {
        setNumRatPian(new AfDecimal(numRatPianObj, 12, 5));
    }

    @Override
    public AfDecimal getPcAder() {
        throw new FieldNotMappedException("pcAder");
    }

    @Override
    public void setPcAder(AfDecimal pcAder) {
        throw new FieldNotMappedException("pcAder");
    }

    @Override
    public AfDecimal getPcAderObj() {
        return getPcAder();
    }

    @Override
    public void setPcAderObj(AfDecimal pcAderObj) {
        setPcAder(new AfDecimal(pcAderObj, 6, 3));
    }

    @Override
    public AfDecimal getPcAz() {
        throw new FieldNotMappedException("pcAz");
    }

    @Override
    public void setPcAz(AfDecimal pcAz) {
        throw new FieldNotMappedException("pcAz");
    }

    @Override
    public AfDecimal getPcAzObj() {
        return getPcAz();
    }

    @Override
    public void setPcAzObj(AfDecimal pcAzObj) {
        setPcAz(new AfDecimal(pcAzObj, 6, 3));
    }

    @Override
    public AfDecimal getPcTfr() {
        throw new FieldNotMappedException("pcTfr");
    }

    @Override
    public void setPcTfr(AfDecimal pcTfr) {
        throw new FieldNotMappedException("pcTfr");
    }

    @Override
    public AfDecimal getPcTfrObj() {
        return getPcTfr();
    }

    @Override
    public void setPcTfrObj(AfDecimal pcTfrObj) {
        setPcTfr(new AfDecimal(pcTfrObj, 6, 3));
    }

    @Override
    public AfDecimal getPcVolo() {
        throw new FieldNotMappedException("pcVolo");
    }

    @Override
    public void setPcVolo(AfDecimal pcVolo) {
        throw new FieldNotMappedException("pcVolo");
    }

    @Override
    public AfDecimal getPcVoloObj() {
        return getPcVolo();
    }

    @Override
    public void setPcVoloObj(AfDecimal pcVoloObj) {
        setPcVolo(new AfDecimal(pcVoloObj, 6, 3));
    }

    @Override
    public int getPolAaDiffProrDflt() {
        return poli.getPolAaDiffProrDflt().getPolAaDiffProrDflt();
    }

    @Override
    public void setPolAaDiffProrDflt(int polAaDiffProrDflt) {
        this.poli.getPolAaDiffProrDflt().setPolAaDiffProrDflt(polAaDiffProrDflt);
    }

    @Override
    public Integer getPolAaDiffProrDfltObj() {
        if (ws.getIndPoli().getProvDaRec() >= 0) {
            return ((Integer)getPolAaDiffProrDflt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolAaDiffProrDfltObj(Integer polAaDiffProrDfltObj) {
        if (polAaDiffProrDfltObj != null) {
            setPolAaDiffProrDflt(((int)polAaDiffProrDfltObj));
            ws.getIndPoli().setProvDaRec(((short)0));
        }
        else {
            ws.getIndPoli().setProvDaRec(((short)-1));
        }
    }

    @Override
    public int getPolCodCompAnia() {
        return poli.getPolCodCompAnia();
    }

    @Override
    public void setPolCodCompAnia(int polCodCompAnia) {
        this.poli.setPolCodCompAnia(polCodCompAnia);
    }

    @Override
    public String getPolCodConv() {
        return poli.getPolCodConv();
    }

    @Override
    public String getPolCodConvAgg() {
        return poli.getPolCodConvAgg();
    }

    @Override
    public void setPolCodConvAgg(String polCodConvAgg) {
        this.poli.setPolCodConvAgg(polCodConvAgg);
    }

    @Override
    public String getPolCodConvAggObj() {
        if (ws.getIndPoli().getSpeAge() >= 0) {
            return getPolCodConvAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodConvAggObj(String polCodConvAggObj) {
        if (polCodConvAggObj != null) {
            setPolCodConvAgg(polCodConvAggObj);
            ws.getIndPoli().setSpeAge(((short)0));
        }
        else {
            ws.getIndPoli().setSpeAge(((short)-1));
        }
    }

    @Override
    public void setPolCodConv(String polCodConv) {
        this.poli.setPolCodConv(polCodConv);
    }

    @Override
    public String getPolCodConvObj() {
        if (ws.getIndPoli().getIntrRetdt() >= 0) {
            return getPolCodConv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodConvObj(String polCodConvObj) {
        if (polCodConvObj != null) {
            setPolCodConv(polCodConvObj);
            ws.getIndPoli().setIntrRetdt(((short)0));
        }
        else {
            ws.getIndPoli().setIntrRetdt(((short)-1));
        }
    }

    @Override
    public String getPolCodDvs() {
        return poli.getPolCodDvs();
    }

    @Override
    public void setPolCodDvs(String polCodDvs) {
        this.poli.setPolCodDvs(polCodDvs);
    }

    @Override
    public String getPolCodDvsObj() {
        if (ws.getIndPoli().getCarGest() >= 0) {
            return getPolCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodDvsObj(String polCodDvsObj) {
        if (polCodDvsObj != null) {
            setPolCodDvs(polCodDvsObj);
            ws.getIndPoli().setCarGest(((short)0));
        }
        else {
            ws.getIndPoli().setCarGest(((short)-1));
        }
    }

    @Override
    public String getPolCodProd() {
        return poli.getPolCodProd();
    }

    @Override
    public void setPolCodProd(String polCodProd) {
        this.poli.setPolCodProd(polCodProd);
    }

    @Override
    public String getPolCodRamo() {
        return poli.getPolCodRamo();
    }

    @Override
    public void setPolCodRamo(String polCodRamo) {
        this.poli.setPolCodRamo(polCodRamo);
    }

    @Override
    public String getPolCodRamoObj() {
        if (ws.getIndPoli().getIntrRiat() >= 0) {
            return getPolCodRamo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodRamoObj(String polCodRamoObj) {
        if (polCodRamoObj != null) {
            setPolCodRamo(polCodRamoObj);
            ws.getIndPoli().setIntrRiat(((short)0));
        }
        else {
            ws.getIndPoli().setIntrRiat(((short)-1));
        }
    }

    @Override
    public String getPolCodTpa() {
        return poli.getPolCodTpa();
    }

    @Override
    public void setPolCodTpa(String polCodTpa) {
        this.poli.setPolCodTpa(polCodTpa);
    }

    @Override
    public String getPolCodTpaObj() {
        if (ws.getIndPoli().getImpTrasfe() >= 0) {
            return getPolCodTpa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolCodTpaObj(String polCodTpaObj) {
        if (polCodTpaObj != null) {
            setPolCodTpa(polCodTpaObj);
            ws.getIndPoli().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndPoli().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public String getPolConvGeco() {
        return poli.getPolConvGeco();
    }

    @Override
    public void setPolConvGeco(String polConvGeco) {
        this.poli.setPolConvGeco(polConvGeco);
    }

    @Override
    public String getPolConvGecoObj() {
        if (ws.getIndPoli().getImpVolo() >= 0) {
            return getPolConvGeco();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolConvGecoObj(String polConvGecoObj) {
        if (polConvGecoObj != null) {
            setPolConvGeco(polConvGecoObj);
            ws.getIndPoli().setImpVolo(((short)0));
        }
        else {
            ws.getIndPoli().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDir1oVers() {
        return poli.getPolDir1oVers().getPolDir1oVers();
    }

    @Override
    public void setPolDir1oVers(AfDecimal polDir1oVers) {
        this.poli.getPolDir1oVers().setPolDir1oVers(polDir1oVers.copy());
    }

    @Override
    public AfDecimal getPolDir1oVersObj() {
        if (ws.getIndPoli().getPreSoloRsh() >= 0) {
            return getPolDir1oVers();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDir1oVersObj(AfDecimal polDir1oVersObj) {
        if (polDir1oVersObj != null) {
            setPolDir1oVers(new AfDecimal(polDir1oVersObj, 15, 3));
            ws.getIndPoli().setPreSoloRsh(((short)0));
        }
        else {
            ws.getIndPoli().setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDirEmis() {
        return poli.getPolDirEmis().getPolDirEmis();
    }

    @Override
    public void setPolDirEmis(AfDecimal polDirEmis) {
        this.poli.getPolDirEmis().setPolDirEmis(polDirEmis.copy());
    }

    @Override
    public AfDecimal getPolDirEmisObj() {
        if (ws.getIndPoli().getPrePpIas() >= 0) {
            return getPolDirEmis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDirEmisObj(AfDecimal polDirEmisObj) {
        if (polDirEmisObj != null) {
            setPolDirEmis(new AfDecimal(polDirEmisObj, 15, 3));
            ws.getIndPoli().setPrePpIas(((short)0));
        }
        else {
            ws.getIndPoli().setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDirQuiet() {
        return poli.getPolDirQuiet().getPolDirQuiet();
    }

    @Override
    public void setPolDirQuiet(AfDecimal polDirQuiet) {
        this.poli.getPolDirQuiet().setPolDirQuiet(polDirQuiet.copy());
    }

    @Override
    public AfDecimal getPolDirQuietObj() {
        if (ws.getIndPoli().getCodTari() >= 0) {
            return getPolDirQuiet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDirQuietObj(AfDecimal polDirQuietObj) {
        if (polDirQuietObj != null) {
            setPolDirQuiet(new AfDecimal(polDirQuietObj, 15, 3));
            ws.getIndPoli().setCodTari(((short)0));
        }
        else {
            ws.getIndPoli().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolDirVersAgg() {
        return poli.getPolDirVersAgg().getPolDirVersAgg();
    }

    @Override
    public void setPolDirVersAgg(AfDecimal polDirVersAgg) {
        this.poli.getPolDirVersAgg().setPolDirVersAgg(polDirVersAgg.copy());
    }

    @Override
    public AfDecimal getPolDirVersAggObj() {
        if (ws.getIndPoli().getCarAcq() >= 0) {
            return getPolDirVersAgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDirVersAggObj(AfDecimal polDirVersAggObj) {
        if (polDirVersAggObj != null) {
            setPolDirVersAgg(new AfDecimal(polDirVersAggObj, 15, 3));
            ws.getIndPoli().setCarAcq(((short)0));
        }
        else {
            ws.getIndPoli().setCarAcq(((short)-1));
        }
    }

    @Override
    public char getPolDsOperSql() {
        return poli.getPolDsOperSql();
    }

    @Override
    public void setPolDsOperSql(char polDsOperSql) {
        this.poli.setPolDsOperSql(polDsOperSql);
    }

    @Override
    public long getPolDsRiga() {
        return poli.getPolDsRiga();
    }

    @Override
    public void setPolDsRiga(long polDsRiga) {
        this.poli.setPolDsRiga(polDsRiga);
    }

    @Override
    public char getPolDsStatoElab() {
        return poli.getPolDsStatoElab();
    }

    @Override
    public void setPolDsStatoElab(char polDsStatoElab) {
        this.poli.setPolDsStatoElab(polDsStatoElab);
    }

    @Override
    public long getPolDsTsEndCptz() {
        return poli.getPolDsTsEndCptz();
    }

    @Override
    public void setPolDsTsEndCptz(long polDsTsEndCptz) {
        this.poli.setPolDsTsEndCptz(polDsTsEndCptz);
    }

    @Override
    public long getPolDsTsIniCptz() {
        return poli.getPolDsTsIniCptz();
    }

    @Override
    public void setPolDsTsIniCptz(long polDsTsIniCptz) {
        this.poli.setPolDsTsIniCptz(polDsTsIniCptz);
    }

    @Override
    public String getPolDsUtente() {
        return poli.getPolDsUtente();
    }

    @Override
    public void setPolDsUtente(String polDsUtente) {
        this.poli.setPolDsUtente(polDsUtente);
    }

    @Override
    public int getPolDsVer() {
        return poli.getPolDsVer();
    }

    @Override
    public void setPolDsVer(int polDsVer) {
        this.poli.setPolDsVer(polDsVer);
    }

    @Override
    public String getPolDtApplzConvDb() {
        return ws.getPoliDb().getUltConsCnbtDb();
    }

    @Override
    public void setPolDtApplzConvDb(String polDtApplzConvDb) {
        this.ws.getPoliDb().setUltConsCnbtDb(polDtApplzConvDb);
    }

    @Override
    public String getPolDtApplzConvDbObj() {
        if (ws.getIndPoli().getSpeMed() >= 0) {
            return getPolDtApplzConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtApplzConvDbObj(String polDtApplzConvDbObj) {
        if (polDtApplzConvDbObj != null) {
            setPolDtApplzConvDb(polDtApplzConvDbObj);
            ws.getIndPoli().setSpeMed(((short)0));
        }
        else {
            ws.getIndPoli().setSpeMed(((short)-1));
        }
    }

    @Override
    public String getPolDtDecorDb() {
        return ws.getPoliDb().getScadDb();
    }

    @Override
    public void setPolDtDecorDb(String polDtDecorDb) {
        this.ws.getPoliDb().setScadDb(polDtDecorDb);
    }

    @Override
    public String getPolDtEmisDb() {
        return ws.getPoliDb().getVarzTpIasDb();
    }

    @Override
    public void setPolDtEmisDb(String polDtEmisDb) {
        this.ws.getPoliDb().setVarzTpIasDb(polDtEmisDb);
    }

    @Override
    public String getPolDtEndEffDb() {
        return ws.getPoliDb().getDecorDb();
    }

    @Override
    public void setPolDtEndEffDb(String polDtEndEffDb) {
        this.ws.getPoliDb().setDecorDb(polDtEndEffDb);
    }

    @Override
    public String getPolDtIniEffDb() {
        return ws.getPoliDb().getEndEffDb();
    }

    @Override
    public void setPolDtIniEffDb(String polDtIniEffDb) {
        this.ws.getPoliDb().setEndEffDb(polDtIniEffDb);
    }

    @Override
    public String getPolDtIniVldtConvDb() {
        return ws.getPoliDb().getEffVarzStatTDb();
    }

    @Override
    public void setPolDtIniVldtConvDb(String polDtIniVldtConvDb) {
        this.ws.getPoliDb().setEffVarzStatTDb(polDtIniVldtConvDb);
    }

    @Override
    public String getPolDtIniVldtConvDbObj() {
        if (ws.getIndPoli().getDir() >= 0) {
            return getPolDtIniVldtConvDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtIniVldtConvDbObj(String polDtIniVldtConvDbObj) {
        if (polDtIniVldtConvDbObj != null) {
            setPolDtIniVldtConvDb(polDtIniVldtConvDbObj);
            ws.getIndPoli().setDir(((short)0));
        }
        else {
            ws.getIndPoli().setDir(((short)-1));
        }
    }

    @Override
    public String getPolDtIniVldtProdDb() {
        return ws.getPoliDb().getDecorPrestBanDb();
    }

    @Override
    public void setPolDtIniVldtProdDb(String polDtIniVldtProdDb) {
        this.ws.getPoliDb().setDecorPrestBanDb(polDtIniVldtProdDb);
    }

    @Override
    public String getPolDtPrescDb() {
        return ws.getPoliDb().getPrescDb();
    }

    @Override
    public void setPolDtPrescDb(String polDtPrescDb) {
        this.ws.getPoliDb().setPrescDb(polDtPrescDb);
    }

    @Override
    public String getPolDtPrescDbObj() {
        if (ws.getIndPoli().getDtEsiTit() >= 0) {
            return getPolDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtPrescDbObj(String polDtPrescDbObj) {
        if (polDtPrescDbObj != null) {
            setPolDtPrescDb(polDtPrescDbObj);
            ws.getIndPoli().setDtEsiTit(((short)0));
        }
        else {
            ws.getIndPoli().setDtEsiTit(((short)-1));
        }
    }

    @Override
    public String getPolDtPropDb() {
        return ws.getPoliDb().getIniEffDb();
    }

    @Override
    public void setPolDtPropDb(String polDtPropDb) {
        this.ws.getPoliDb().setIniEffDb(polDtPropDb);
    }

    @Override
    public String getPolDtPropDbObj() {
        if (ws.getIndPoli().getDtEndCop() >= 0) {
            return getPolDtPropDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtPropDbObj(String polDtPropDbObj) {
        if (polDtPropDbObj != null) {
            setPolDtPropDb(polDtPropDbObj);
            ws.getIndPoli().setDtEndCop(((short)0));
        }
        else {
            ws.getIndPoli().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getPolDtScadDb() {
        return ws.getPoliDb().getNovaRgmFiscDb();
    }

    @Override
    public void setPolDtScadDb(String polDtScadDb) {
        this.ws.getPoliDb().setNovaRgmFiscDb(polDtScadDb);
    }

    @Override
    public String getPolDtScadDbObj() {
        if (ws.getIndPoli().getIntrMora() >= 0) {
            return getPolDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDtScadDbObj(String polDtScadDbObj) {
        if (polDtScadDbObj != null) {
            setPolDtScadDb(polDtScadDbObj);
            ws.getIndPoli().setIntrMora(((short)0));
        }
        else {
            ws.getIndPoli().setIntrMora(((short)-1));
        }
    }

    @Override
    public int getPolDurAa() {
        return poli.getPolDurAa().getPolDurAa();
    }

    @Override
    public void setPolDurAa(int polDurAa) {
        this.poli.getPolDurAa().setPolDurAa(polDurAa);
    }

    @Override
    public Integer getPolDurAaObj() {
        if (ws.getIndPoli().getPreNet() >= 0) {
            return ((Integer)getPolDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDurAaObj(Integer polDurAaObj) {
        if (polDurAaObj != null) {
            setPolDurAa(((int)polDurAaObj));
            ws.getIndPoli().setPreNet(((short)0));
        }
        else {
            ws.getIndPoli().setPreNet(((short)-1));
        }
    }

    @Override
    public int getPolDurGg() {
        return poli.getPolDurGg().getPolDurGg();
    }

    @Override
    public void setPolDurGg(int polDurGg) {
        this.poli.getPolDurGg().setPolDurGg(polDurGg);
    }

    @Override
    public Integer getPolDurGgObj() {
        if (ws.getIndPoli().getFrqMovi() >= 0) {
            return ((Integer)getPolDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDurGgObj(Integer polDurGgObj) {
        if (polDurGgObj != null) {
            setPolDurGg(((int)polDurGgObj));
            ws.getIndPoli().setFrqMovi(((short)0));
        }
        else {
            ws.getIndPoli().setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getPolDurMm() {
        return poli.getPolDurMm().getPolDurMm();
    }

    @Override
    public void setPolDurMm(int polDurMm) {
        this.poli.getPolDurMm().setPolDurMm(polDurMm);
    }

    @Override
    public Integer getPolDurMmObj() {
        if (ws.getIndPoli().getIntrFraz() >= 0) {
            return ((Integer)getPolDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolDurMmObj(Integer polDurMmObj) {
        if (polDurMmObj != null) {
            setPolDurMm(((int)polDurMmObj));
            ws.getIndPoli().setIntrFraz(((short)0));
        }
        else {
            ws.getIndPoli().setIntrFraz(((short)-1));
        }
    }

    @Override
    public char getPolFlAmmbMovi() {
        return poli.getPolFlAmmbMovi();
    }

    @Override
    public void setPolFlAmmbMovi(char polFlAmmbMovi) {
        this.poli.setPolFlAmmbMovi(polFlAmmbMovi);
    }

    @Override
    public Character getPolFlAmmbMoviObj() {
        if (ws.getIndPoli().getImpTfr() >= 0) {
            return ((Character)getPolFlAmmbMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlAmmbMoviObj(Character polFlAmmbMoviObj) {
        if (polFlAmmbMoviObj != null) {
            setPolFlAmmbMovi(((char)polFlAmmbMoviObj));
            ws.getIndPoli().setImpTfr(((short)0));
        }
        else {
            ws.getIndPoli().setImpTfr(((short)-1));
        }
    }

    @Override
    public char getPolFlCopFinanz() {
        return poli.getPolFlCopFinanz();
    }

    @Override
    public void setPolFlCopFinanz(char polFlCopFinanz) {
        this.poli.setPolFlCopFinanz(polFlCopFinanz);
    }

    @Override
    public Character getPolFlCopFinanzObj() {
        if (ws.getIndPoli().getSoprProf() >= 0) {
            return ((Character)getPolFlCopFinanz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlCopFinanzObj(Character polFlCopFinanzObj) {
        if (polFlCopFinanzObj != null) {
            setPolFlCopFinanz(((char)polFlCopFinanzObj));
            ws.getIndPoli().setSoprProf(((short)0));
        }
        else {
            ws.getIndPoli().setSoprProf(((short)-1));
        }
    }

    @Override
    public char getPolFlCumPreCntr() {
        return poli.getPolFlCumPreCntr();
    }

    @Override
    public void setPolFlCumPreCntr(char polFlCumPreCntr) {
        this.poli.setPolFlCumPreCntr(polFlCumPreCntr);
    }

    @Override
    public Character getPolFlCumPreCntrObj() {
        if (ws.getIndPoli().getImpAder() >= 0) {
            return ((Character)getPolFlCumPreCntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlCumPreCntrObj(Character polFlCumPreCntrObj) {
        if (polFlCumPreCntrObj != null) {
            setPolFlCumPreCntr(((char)polFlCumPreCntrObj));
            ws.getIndPoli().setImpAder(((short)0));
        }
        else {
            ws.getIndPoli().setImpAder(((short)-1));
        }
    }

    @Override
    public char getPolFlEstas() {
        return poli.getPolFlEstas();
    }

    @Override
    public void setPolFlEstas(char polFlEstas) {
        this.poli.setPolFlEstas(polFlEstas);
    }

    @Override
    public Character getPolFlEstasObj() {
        if (ws.getIndPoli().getSoprSan() >= 0) {
            return ((Character)getPolFlEstas());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlEstasObj(Character polFlEstasObj) {
        if (polFlEstasObj != null) {
            setPolFlEstas(((char)polFlEstasObj));
            ws.getIndPoli().setSoprSan(((short)0));
        }
        else {
            ws.getIndPoli().setSoprSan(((short)-1));
        }
    }

    @Override
    public char getPolFlFntAder() {
        return poli.getPolFlFntAder();
    }

    @Override
    public void setPolFlFntAder(char polFlFntAder) {
        this.poli.setPolFlFntAder(polFlFntAder);
    }

    @Override
    public Character getPolFlFntAderObj() {
        if (ws.getIndPoli().getProvAcq1aa() >= 0) {
            return ((Character)getPolFlFntAder());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntAderObj(Character polFlFntAderObj) {
        if (polFlFntAderObj != null) {
            setPolFlFntAder(((char)polFlFntAderObj));
            ws.getIndPoli().setProvAcq1aa(((short)0));
        }
        else {
            ws.getIndPoli().setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public char getPolFlFntAz() {
        return poli.getPolFlFntAz();
    }

    @Override
    public void setPolFlFntAz(char polFlFntAz) {
        this.poli.setPolFlFntAz(polFlFntAz);
    }

    @Override
    public Character getPolFlFntAzObj() {
        if (ws.getIndPoli().getCarInc() >= 0) {
            return ((Character)getPolFlFntAz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntAzObj(Character polFlFntAzObj) {
        if (polFlFntAzObj != null) {
            setPolFlFntAz(((char)polFlFntAzObj));
            ws.getIndPoli().setCarInc(((short)0));
        }
        else {
            ws.getIndPoli().setCarInc(((short)-1));
        }
    }

    @Override
    public char getPolFlFntTfr() {
        return poli.getPolFlFntTfr();
    }

    @Override
    public void setPolFlFntTfr(char polFlFntTfr) {
        this.poli.setPolFlFntTfr(polFlFntTfr);
    }

    @Override
    public Character getPolFlFntTfrObj() {
        if (ws.getIndPoli().getProvAcq2aa() >= 0) {
            return ((Character)getPolFlFntTfr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntTfrObj(Character polFlFntTfrObj) {
        if (polFlFntTfrObj != null) {
            setPolFlFntTfr(((char)polFlFntTfrObj));
            ws.getIndPoli().setProvAcq2aa(((short)0));
        }
        else {
            ws.getIndPoli().setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public char getPolFlFntVolo() {
        return poli.getPolFlFntVolo();
    }

    @Override
    public void setPolFlFntVolo(char polFlFntVolo) {
        this.poli.setPolFlFntVolo(polFlFntVolo);
    }

    @Override
    public Character getPolFlFntVoloObj() {
        if (ws.getIndPoli().getProvRicor() >= 0) {
            return ((Character)getPolFlFntVolo());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlFntVoloObj(Character polFlFntVoloObj) {
        if (polFlFntVoloObj != null) {
            setPolFlFntVolo(((char)polFlFntVoloObj));
            ws.getIndPoli().setProvRicor(((short)0));
        }
        else {
            ws.getIndPoli().setProvRicor(((short)-1));
        }
    }

    @Override
    public char getPolFlPoliBundling() {
        return poli.getPolFlPoliBundling();
    }

    @Override
    public void setPolFlPoliBundling(char polFlPoliBundling) {
        this.poli.setPolFlPoliBundling(polFlPoliBundling);
    }

    @Override
    public Character getPolFlPoliBundlingObj() {
        if (ws.getIndPoli().getNumGgRival() >= 0) {
            return ((Character)getPolFlPoliBundling());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlPoliBundlingObj(Character polFlPoliBundlingObj) {
        if (polFlPoliBundlingObj != null) {
            setPolFlPoliBundling(((char)polFlPoliBundlingObj));
            ws.getIndPoli().setNumGgRival(((short)0));
        }
        else {
            ws.getIndPoli().setNumGgRival(((short)-1));
        }
    }

    @Override
    public char getPolFlPoliCpiPr() {
        return poli.getPolFlPoliCpiPr();
    }

    @Override
    public void setPolFlPoliCpiPr(char polFlPoliCpiPr) {
        this.poli.setPolFlPoliCpiPr(polFlPoliCpiPr);
    }

    @Override
    public Character getPolFlPoliCpiPrObj() {
        if (ws.getIndPoli().getNumGgRitardoPag() >= 0) {
            return ((Character)getPolFlPoliCpiPr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlPoliCpiPrObj(Character polFlPoliCpiPrObj) {
        if (polFlPoliCpiPrObj != null) {
            setPolFlPoliCpiPr(((char)polFlPoliCpiPrObj));
            ws.getIndPoli().setNumGgRitardoPag(((short)0));
        }
        else {
            ws.getIndPoli().setNumGgRitardoPag(((short)-1));
        }
    }

    @Override
    public char getPolFlPoliIfp() {
        return poli.getPolFlPoliIfp();
    }

    @Override
    public void setPolFlPoliIfp(char polFlPoliIfp) {
        this.poli.setPolFlPoliIfp(polFlPoliIfp);
    }

    @Override
    public Character getPolFlPoliIfpObj() {
        if (ws.getIndPoli().getCnbtAntirac() >= 0) {
            return ((Character)getPolFlPoliIfp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlPoliIfpObj(Character polFlPoliIfpObj) {
        if (polFlPoliIfpObj != null) {
            setPolFlPoliIfp(((char)polFlPoliIfpObj));
            ws.getIndPoli().setCnbtAntirac(((short)0));
        }
        else {
            ws.getIndPoli().setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public char getPolFlQuestAdegzAss() {
        return poli.getPolFlQuestAdegzAss();
    }

    @Override
    public void setPolFlQuestAdegzAss(char polFlQuestAdegzAss) {
        this.poli.setPolFlQuestAdegzAss(polFlQuestAdegzAss);
    }

    @Override
    public Character getPolFlQuestAdegzAssObj() {
        if (ws.getIndPoli().getTotIntrPrest() >= 0) {
            return ((Character)getPolFlQuestAdegzAss());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlQuestAdegzAssObj(Character polFlQuestAdegzAssObj) {
        if (polFlQuestAdegzAssObj != null) {
            setPolFlQuestAdegzAss(((char)polFlQuestAdegzAssObj));
            ws.getIndPoli().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndPoli().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public char getPolFlRshComun() {
        return poli.getPolFlRshComun();
    }

    @Override
    public void setPolFlRshComun(char polFlRshComun) {
        this.poli.setPolFlRshComun(polFlRshComun);
    }

    @Override
    public char getPolFlRshComunCond() {
        return poli.getPolFlRshComunCond();
    }

    @Override
    public void setPolFlRshComunCond(char polFlRshComunCond) {
        this.poli.setPolFlRshComunCond(polFlRshComunCond);
    }

    @Override
    public Character getPolFlRshComunCondObj() {
        if (ws.getIndPoli().getSoprTec() >= 0) {
            return ((Character)getPolFlRshComunCond());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlRshComunCondObj(Character polFlRshComunCondObj) {
        if (polFlRshComunCondObj != null) {
            setPolFlRshComunCond(((char)polFlRshComunCondObj));
            ws.getIndPoli().setSoprTec(((short)0));
        }
        else {
            ws.getIndPoli().setSoprTec(((short)-1));
        }
    }

    @Override
    public Character getPolFlRshComunObj() {
        if (ws.getIndPoli().getSoprSpo() >= 0) {
            return ((Character)getPolFlRshComun());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlRshComunObj(Character polFlRshComunObj) {
        if (polFlRshComunObj != null) {
            setPolFlRshComun(((char)polFlRshComunObj));
            ws.getIndPoli().setSoprSpo(((short)0));
        }
        else {
            ws.getIndPoli().setSoprSpo(((short)-1));
        }
    }

    @Override
    public char getPolFlScudoFisc() {
        return poli.getPolFlScudoFisc();
    }

    @Override
    public void setPolFlScudoFisc(char polFlScudoFisc) {
        this.poli.setPolFlScudoFisc(polFlScudoFisc);
    }

    @Override
    public Character getPolFlScudoFiscObj() {
        if (ws.getIndPoli().getManfeeAntic() >= 0) {
            return ((Character)getPolFlScudoFisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlScudoFiscObj(Character polFlScudoFiscObj) {
        if (polFlScudoFiscObj != null) {
            setPolFlScudoFisc(((char)polFlScudoFiscObj));
            ws.getIndPoli().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public char getPolFlTfrStrc() {
        return poli.getPolFlTfrStrc();
    }

    @Override
    public void setPolFlTfrStrc(char polFlTfrStrc) {
        this.poli.setPolFlTfrStrc(polFlTfrStrc);
    }

    @Override
    public Character getPolFlTfrStrcObj() {
        if (ws.getIndPoli().getManfeeRec() >= 0) {
            return ((Character)getPolFlTfrStrc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlTfrStrcObj(Character polFlTfrStrcObj) {
        if (polFlTfrStrcObj != null) {
            setPolFlTfrStrc(((char)polFlTfrStrcObj));
            ws.getIndPoli().setManfeeRec(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeRec(((short)-1));
        }
    }

    @Override
    public char getPolFlTrasfe() {
        return poli.getPolFlTrasfe();
    }

    @Override
    public void setPolFlTrasfe(char polFlTrasfe) {
        this.poli.setPolFlTrasfe(polFlTrasfe);
    }

    @Override
    public Character getPolFlTrasfeObj() {
        if (ws.getIndPoli().getManfeeRicor() >= 0) {
            return ((Character)getPolFlTrasfe());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlTrasfeObj(Character polFlTrasfeObj) {
        if (polFlTrasfeObj != null) {
            setPolFlTrasfe(((char)polFlTrasfeObj));
            ws.getIndPoli().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndPoli().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public String getPolFlVerProd() {
        return poli.getPolFlVerProd();
    }

    @Override
    public void setPolFlVerProd(String polFlVerProd) {
        this.poli.setPolFlVerProd(polFlVerProd);
    }

    @Override
    public String getPolFlVerProdObj() {
        if (ws.getIndPoli().getCodDvs() >= 0) {
            return getPolFlVerProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlVerProdObj(String polFlVerProdObj) {
        if (polFlVerProdObj != null) {
            setPolFlVerProd(polFlVerProdObj);
            ws.getIndPoli().setCodDvs(((short)0));
        }
        else {
            ws.getIndPoli().setCodDvs(((short)-1));
        }
    }

    @Override
    public char getPolFlVndBundle() {
        return poli.getPolFlVndBundle();
    }

    @Override
    public void setPolFlVndBundle(char polFlVndBundle) {
        this.poli.setPolFlVndBundle(polFlVndBundle);
    }

    @Override
    public Character getPolFlVndBundleObj() {
        if (ws.getIndPoli().getRemunAss() >= 0) {
            return ((Character)getPolFlVndBundle());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolFlVndBundleObj(Character polFlVndBundleObj) {
        if (polFlVndBundleObj != null) {
            setPolFlVndBundle(((char)polFlVndBundleObj));
            ws.getIndPoli().setRemunAss(((short)0));
        }
        else {
            ws.getIndPoli().setRemunAss(((short)-1));
        }
    }

    @Override
    public String getPolIbBs() {
        return poli.getPolIbBs();
    }

    @Override
    public void setPolIbBs(String polIbBs) {
        this.poli.setPolIbBs(polIbBs);
    }

    @Override
    public String getPolIbBsObj() {
        if (ws.getIndPoli().getCommisInter() >= 0) {
            return getPolIbBs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIbBsObj(String polIbBsObj) {
        if (polIbBsObj != null) {
            setPolIbBs(polIbBsObj);
            ws.getIndPoli().setCommisInter(((short)0));
        }
        else {
            ws.getIndPoli().setCommisInter(((short)-1));
        }
    }

    @Override
    public String getPolIbOgg() {
        return poli.getPolIbOgg();
    }

    @Override
    public void setPolIbOgg(String polIbOgg) {
        this.poli.setPolIbOgg(polIbOgg);
    }

    @Override
    public String getPolIbOggObj() {
        if (ws.getIndPoli().getDtIniCop() >= 0) {
            return getPolIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIbOggObj(String polIbOggObj) {
        if (polIbOggObj != null) {
            setPolIbOgg(polIbOggObj);
            ws.getIndPoli().setDtIniCop(((short)0));
        }
        else {
            ws.getIndPoli().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getPolIbProp() {
        return poli.getPolIbProp();
    }

    @Override
    public void setPolIbProp(String polIbProp) {
        this.poli.setPolIbProp(polIbProp);
    }

    @Override
    public int getPolIdAccComm() {
        return poli.getPolIdAccComm().getPolIdAccComm();
    }

    @Override
    public void setPolIdAccComm(int polIdAccComm) {
        this.poli.getPolIdAccComm().setPolIdAccComm(polIdAccComm);
    }

    @Override
    public Integer getPolIdAccCommObj() {
        if (ws.getIndPoli().getImpTfrStrc() >= 0) {
            return ((Integer)getPolIdAccComm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIdAccCommObj(Integer polIdAccCommObj) {
        if (polIdAccCommObj != null) {
            setPolIdAccComm(((int)polIdAccCommObj));
            ws.getIndPoli().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndPoli().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public int getPolIdMoviChiu() {
        return poli.getPolIdMoviChiu().getPolIdMoviChiu();
    }

    @Override
    public void setPolIdMoviChiu(int polIdMoviChiu) {
        this.poli.getPolIdMoviChiu().setPolIdMoviChiu(polIdMoviChiu);
    }

    @Override
    public Integer getPolIdMoviChiuObj() {
        if (ws.getIndPoli().getIdMoviChiu() >= 0) {
            return ((Integer)getPolIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIdMoviChiuObj(Integer polIdMoviChiuObj) {
        if (polIdMoviChiuObj != null) {
            setPolIdMoviChiu(((int)polIdMoviChiuObj));
            ws.getIndPoli().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndPoli().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getPolIdMoviCrz() {
        return poli.getPolIdMoviCrz();
    }

    @Override
    public void setPolIdMoviCrz(int polIdMoviCrz) {
        this.poli.setPolIdMoviCrz(polIdMoviCrz);
    }

    @Override
    public int getPolIdPoli() {
        return poli.getPolIdPoli();
    }

    @Override
    public void setPolIdPoli(int polIdPoli) {
        this.poli.setPolIdPoli(polIdPoli);
    }

    @Override
    public char getPolIndPoliPrinColl() {
        return poli.getPolIndPoliPrinColl();
    }

    @Override
    public void setPolIndPoliPrinColl(char polIndPoliPrinColl) {
        this.poli.setPolIndPoliPrinColl(polIndPoliPrinColl);
    }

    @Override
    public Character getPolIndPoliPrinCollObj() {
        if (ws.getIndPoli().getAcqExp() >= 0) {
            return ((Character)getPolIndPoliPrinColl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolIndPoliPrinCollObj(Character polIndPoliPrinCollObj) {
        if (polIndPoliPrinCollObj != null) {
            setPolIndPoliPrinColl(((char)polIndPoliPrinCollObj));
            ws.getIndPoli().setAcqExp(((short)0));
        }
        else {
            ws.getIndPoli().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getPolSpeMed() {
        return poli.getPolSpeMed().getPolSpeMed();
    }

    @Override
    public void setPolSpeMed(AfDecimal polSpeMed) {
        this.poli.getPolSpeMed().setPolSpeMed(polSpeMed.copy());
    }

    @Override
    public AfDecimal getPolSpeMedObj() {
        if (ws.getIndPoli().getPreTot() >= 0) {
            return getPolSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolSpeMedObj(AfDecimal polSpeMedObj) {
        if (polSpeMedObj != null) {
            setPolSpeMed(new AfDecimal(polSpeMedObj, 15, 3));
            ws.getIndPoli().setPreTot(((short)0));
        }
        else {
            ws.getIndPoli().setPreTot(((short)-1));
        }
    }

    @Override
    public String getPolSubcatProd() {
        return poli.getPolSubcatProd();
    }

    @Override
    public void setPolSubcatProd(String polSubcatProd) {
        this.poli.setPolSubcatProd(polSubcatProd);
    }

    @Override
    public String getPolSubcatProdObj() {
        if (ws.getIndPoli().getCarIas() >= 0) {
            return getPolSubcatProd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolSubcatProdObj(String polSubcatProdObj) {
        if (polSubcatProdObj != null) {
            setPolSubcatProd(polSubcatProdObj);
            ws.getIndPoli().setCarIas(((short)0));
        }
        else {
            ws.getIndPoli().setCarIas(((short)-1));
        }
    }

    @Override
    public String getPolTpApplzDir() {
        return poli.getPolTpApplzDir();
    }

    @Override
    public void setPolTpApplzDir(String polTpApplzDir) {
        this.poli.setPolTpApplzDir(polTpApplzDir);
    }

    @Override
    public String getPolTpApplzDirObj() {
        if (ws.getIndPoli().getSoprAlt() >= 0) {
            return getPolTpApplzDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpApplzDirObj(String polTpApplzDirObj) {
        if (polTpApplzDirObj != null) {
            setPolTpApplzDir(polTpApplzDirObj);
            ws.getIndPoli().setSoprAlt(((short)0));
        }
        else {
            ws.getIndPoli().setSoprAlt(((short)-1));
        }
    }

    @Override
    public String getPolTpFrmAssva() {
        return poli.getPolTpFrmAssva();
    }

    @Override
    public void setPolTpFrmAssva(String polTpFrmAssva) {
        this.poli.setPolTpFrmAssva(polTpFrmAssva);
    }

    @Override
    public String getPolTpLivGenzTit() {
        return poli.getPolTpLivGenzTit();
    }

    @Override
    public void setPolTpLivGenzTit(String polTpLivGenzTit) {
        this.poli.setPolTpLivGenzTit(polTpLivGenzTit);
    }

    @Override
    public String getPolTpOpzAScad() {
        return poli.getPolTpOpzAScad();
    }

    @Override
    public void setPolTpOpzAScad(String polTpOpzAScad) {
        this.poli.setPolTpOpzAScad(polTpOpzAScad);
    }

    @Override
    public String getPolTpOpzAScadObj() {
        if (ws.getIndPoli().getProvInc() >= 0) {
            return getPolTpOpzAScad();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpOpzAScadObj(String polTpOpzAScadObj) {
        if (polTpOpzAScadObj != null) {
            setPolTpOpzAScad(polTpOpzAScadObj);
            ws.getIndPoli().setProvInc(((short)0));
        }
        else {
            ws.getIndPoli().setProvInc(((short)-1));
        }
    }

    @Override
    public String getPolTpPoli() {
        return poli.getPolTpPoli();
    }

    @Override
    public void setPolTpPoli(String polTpPoli) {
        this.poli.setPolTpPoli(polTpPoli);
    }

    @Override
    public String getPolTpPtfEstno() {
        return poli.getPolTpPtfEstno();
    }

    @Override
    public void setPolTpPtfEstno(String polTpPtfEstno) {
        this.poli.setPolTpPtfEstno(polTpPtfEstno);
    }

    @Override
    public String getPolTpPtfEstnoObj() {
        if (ws.getIndPoli().getImpAz() >= 0) {
            return getPolTpPtfEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpPtfEstnoObj(String polTpPtfEstnoObj) {
        if (polTpPtfEstnoObj != null) {
            setPolTpPtfEstno(polTpPtfEstnoObj);
            ws.getIndPoli().setImpAz(((short)0));
        }
        else {
            ws.getIndPoli().setImpAz(((short)-1));
        }
    }

    @Override
    public String getPolTpRgmFisc() {
        return poli.getPolTpRgmFisc();
    }

    @Override
    public void setPolTpRgmFisc(String polTpRgmFisc) {
        this.poli.setPolTpRgmFisc(polTpRgmFisc);
    }

    @Override
    public String getPolTpRgmFiscObj() {
        if (ws.getIndPoli().getTax() >= 0) {
            return getPolTpRgmFisc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPolTpRgmFiscObj(String polTpRgmFiscObj) {
        if (polTpRgmFiscObj != null) {
            setPolTpRgmFisc(polTpRgmFiscObj);
            ws.getIndPoli().setTax(((short)0));
        }
        else {
            ws.getIndPoli().setTax(((short)-1));
        }
    }

    public PoliIdbspol0 getPoli() {
        return poli;
    }

    @Override
    public AfDecimal getPreLrdInd() {
        throw new FieldNotMappedException("preLrdInd");
    }

    @Override
    public void setPreLrdInd(AfDecimal preLrdInd) {
        throw new FieldNotMappedException("preLrdInd");
    }

    @Override
    public AfDecimal getPreLrdIndObj() {
        return getPreLrdInd();
    }

    @Override
    public void setPreLrdIndObj(AfDecimal preLrdIndObj) {
        setPreLrdInd(new AfDecimal(preLrdIndObj, 15, 3));
    }

    @Override
    public AfDecimal getPreNetInd() {
        throw new FieldNotMappedException("preNetInd");
    }

    @Override
    public void setPreNetInd(AfDecimal preNetInd) {
        throw new FieldNotMappedException("preNetInd");
    }

    @Override
    public AfDecimal getPreNetIndObj() {
        return getPreNetInd();
    }

    @Override
    public void setPreNetIndObj(AfDecimal preNetIndObj) {
        setPreNetInd(new AfDecimal(preNetIndObj, 15, 3));
    }

    @Override
    public AfDecimal getPrstzIniInd() {
        throw new FieldNotMappedException("prstzIniInd");
    }

    @Override
    public void setPrstzIniInd(AfDecimal prstzIniInd) {
        throw new FieldNotMappedException("prstzIniInd");
    }

    @Override
    public AfDecimal getPrstzIniIndObj() {
        return getPrstzIniInd();
    }

    @Override
    public void setPrstzIniIndObj(AfDecimal prstzIniIndObj) {
        setPrstzIniInd(new AfDecimal(prstzIniIndObj, 15, 3));
    }

    @Override
    public AfDecimal getRatLrdInd() {
        throw new FieldNotMappedException("ratLrdInd");
    }

    @Override
    public void setRatLrdInd(AfDecimal ratLrdInd) {
        throw new FieldNotMappedException("ratLrdInd");
    }

    @Override
    public AfDecimal getRatLrdIndObj() {
        return getRatLrdInd();
    }

    @Override
    public void setRatLrdIndObj(AfDecimal ratLrdIndObj) {
        setRatLrdInd(new AfDecimal(ratLrdIndObj, 15, 3));
    }

    public StatOggBusIdbsstb0 getStatOggBus() {
        return statOggBus;
    }

    @Override
    public int getStbCodCompAnia() {
        return statOggBus.getStbCodCompAnia();
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        this.statOggBus.setStbCodCompAnia(stbCodCompAnia);
    }

    @Override
    public char getStbDsOperSql() {
        return statOggBus.getStbDsOperSql();
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        this.statOggBus.setStbDsOperSql(stbDsOperSql);
    }

    @Override
    public long getStbDsRiga() {
        return statOggBus.getStbDsRiga();
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        this.statOggBus.setStbDsRiga(stbDsRiga);
    }

    @Override
    public char getStbDsStatoElab() {
        return statOggBus.getStbDsStatoElab();
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        this.statOggBus.setStbDsStatoElab(stbDsStatoElab);
    }

    @Override
    public long getStbDsTsEndCptz() {
        return statOggBus.getStbDsTsEndCptz();
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        this.statOggBus.setStbDsTsEndCptz(stbDsTsEndCptz);
    }

    @Override
    public long getStbDsTsIniCptz() {
        return statOggBus.getStbDsTsIniCptz();
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        this.statOggBus.setStbDsTsIniCptz(stbDsTsIniCptz);
    }

    @Override
    public String getStbDsUtente() {
        return statOggBus.getStbDsUtente();
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        this.statOggBus.setStbDsUtente(stbDsUtente);
    }

    @Override
    public int getStbDsVer() {
        return statOggBus.getStbDsVer();
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        this.statOggBus.setStbDsVer(stbDsVer);
    }

    @Override
    public String getStbDtEndEffDb() {
        return ws.getIdbvstb3().getStbDtEndEffDb();
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        this.ws.getIdbvstb3().setStbDtEndEffDb(stbDtEndEffDb);
    }

    @Override
    public String getStbDtIniEffDb() {
        return ws.getIdbvstb3().getStbDtIniEffDb();
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        this.ws.getIdbvstb3().setStbDtIniEffDb(stbDtIniEffDb);
    }

    @Override
    public int getStbIdMoviChiu() {
        return statOggBus.getStbIdMoviChiu().getStbIdMoviChiu();
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        this.statOggBus.getStbIdMoviChiu().setStbIdMoviChiu(stbIdMoviChiu);
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        if (ws.getIndStbIdMoviChiu() >= 0) {
            return ((Integer)getStbIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        if (stbIdMoviChiuObj != null) {
            setStbIdMoviChiu(((int)stbIdMoviChiuObj));
            ws.setIndStbIdMoviChiu(((short)0));
        }
        else {
            ws.setIndStbIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getStbIdMoviCrz() {
        return statOggBus.getStbIdMoviCrz();
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        this.statOggBus.setStbIdMoviCrz(stbIdMoviCrz);
    }

    @Override
    public int getStbIdOgg() {
        return statOggBus.getStbIdOgg();
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        this.statOggBus.setStbIdOgg(stbIdOgg);
    }

    @Override
    public int getStbIdStatOggBus() {
        return statOggBus.getStbIdStatOggBus();
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        this.statOggBus.setStbIdStatOggBus(stbIdStatOggBus);
    }

    @Override
    public String getStbTpCaus() {
        return statOggBus.getStbTpCaus();
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        this.statOggBus.setStbTpCaus(stbTpCaus);
    }

    @Override
    public String getStbTpOgg() {
        return statOggBus.getStbTpOgg();
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        this.statOggBus.setStbTpOgg(stbTpOgg);
    }

    @Override
    public String getStbTpStatBus() {
        return statOggBus.getStbTpStatBus();
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        this.statOggBus.setStbTpStatBus(stbTpStatBus);
    }

    @Override
    public String getTpFntCnbtva() {
        throw new FieldNotMappedException("tpFntCnbtva");
    }

    @Override
    public void setTpFntCnbtva(String tpFntCnbtva) {
        throw new FieldNotMappedException("tpFntCnbtva");
    }

    @Override
    public String getTpFntCnbtvaObj() {
        return getTpFntCnbtva();
    }

    @Override
    public void setTpFntCnbtvaObj(String tpFntCnbtvaObj) {
        setTpFntCnbtva(tpFntCnbtvaObj);
    }

    @Override
    public String getTpIas() {
        throw new FieldNotMappedException("tpIas");
    }

    @Override
    public void setTpIas(String tpIas) {
        throw new FieldNotMappedException("tpIas");
    }

    @Override
    public String getTpIasObj() {
        return getTpIas();
    }

    @Override
    public void setTpIasObj(String tpIasObj) {
        setTpIas(tpIasObj);
    }

    @Override
    public String getTpModPagTit() {
        throw new FieldNotMappedException("tpModPagTit");
    }

    @Override
    public void setTpModPagTit(String tpModPagTit) {
        throw new FieldNotMappedException("tpModPagTit");
    }

    @Override
    public String getTpRgmFisc() {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        throw new FieldNotMappedException("tpRgmFisc");
    }

    @Override
    public String getTpRiat() {
        throw new FieldNotMappedException("tpRiat");
    }

    @Override
    public void setTpRiat(String tpRiat) {
        throw new FieldNotMappedException("tpRiat");
    }

    @Override
    public String getTpRiatObj() {
        return getTpRiat();
    }

    @Override
    public void setTpRiatObj(String tpRiatObj) {
        setTpRiat(tpRiatObj);
    }

    @Override
    public String getWlbCodProd() {
        return ws.getWlbRecPren().getCodProd();
    }

    @Override
    public void setWlbCodProd(String wlbCodProd) {
        this.ws.getWlbRecPren().setCodProd(wlbCodProd);
    }

    @Override
    public String getWlbIbAdeFirst() {
        return ws.getWlbRecPren().getIbAdeFirst();
    }

    @Override
    public void setWlbIbAdeFirst(String wlbIbAdeFirst) {
        this.ws.getWlbRecPren().setIbAdeFirst(wlbIbAdeFirst);
    }

    @Override
    public String getWlbIbAdeLast() {
        return ws.getWlbRecPren().getIbAdeLast();
    }

    @Override
    public void setWlbIbAdeLast(String wlbIbAdeLast) {
        this.ws.getWlbRecPren().setIbAdeLast(wlbIbAdeLast);
    }

    @Override
    public String getWlbIbPoliFirst() {
        return ws.getWlbRecPren().getIbPoliFirst();
    }

    @Override
    public void setWlbIbPoliFirst(String wlbIbPoliFirst) {
        this.ws.getWlbRecPren().setIbPoliFirst(wlbIbPoliFirst);
    }

    @Override
    public String getWlbIbPoliLast() {
        return ws.getWlbRecPren().getIbPoliLast();
    }

    @Override
    public void setWlbIbPoliLast(String wlbIbPoliLast) {
        this.ws.getWlbRecPren().setIbPoliLast(wlbIbPoliLast);
    }

    public Ldbm0250Data getWs() {
        return ws;
    }

    @Override
    public String getWsDtPtfX() {
        return ws.getWsDtPtfX();
    }

    @Override
    public void setWsDtPtfX(String wsDtPtfX) {
        this.ws.setWsDtPtfX(wsDtPtfX);
    }

    @Override
    public long getWsDtTsPtf() {
        return ws.getWsDtTsPtf();
    }

    @Override
    public void setWsDtTsPtf(long wsDtTsPtf) {
        this.ws.setWsDtTsPtf(wsDtTsPtf);
    }

    @Override
    public long getWsDtTsPtfPre() {
        return ws.getWsDtTsPtfPre();
    }

    @Override
    public void setWsDtTsPtfPre(long wsDtTsPtfPre) {
        this.ws.setWsDtTsPtfPre(wsDtTsPtfPre);
    }

    @Override
    public String getWsTpFrmAssva1() {
        return ws.getWsTpFrmAssva1();
    }

    @Override
    public void setWsTpFrmAssva1(String wsTpFrmAssva1) {
        this.ws.setWsTpFrmAssva1(wsTpFrmAssva1);
    }

    @Override
    public String getWsTpFrmAssva2() {
        return ws.getWsTpFrmAssva2();
    }

    @Override
    public void setWsTpFrmAssva2(String wsTpFrmAssva2) {
        this.ws.setWsTpFrmAssva2(wsTpFrmAssva2);
    }
}
