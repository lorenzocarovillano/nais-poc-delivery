package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ReinvstPoliLqDao;
import it.accenture.jnais.commons.data.to.IReinvstPoliLq;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsl300Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.L30CodSubAge;
import it.accenture.jnais.ws.redefines.L30DtDecorPoliLq;
import it.accenture.jnais.ws.redefines.L30IdMoviChiu;
import it.accenture.jnais.ws.redefines.L30TpInvstLiq;
import it.accenture.jnais.ws.ReinvstPoliLqIdbsl300;

/**Original name: IDBSL300<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  09 LUG 2009.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsl300 extends Program implements IReinvstPoliLq {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ReinvstPoliLqDao reinvstPoliLqDao = new ReinvstPoliLqDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsl300Data ws = new Idbsl300Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: REINVST-POLI-LQ
    private ReinvstPoliLqIdbsl300 reinvstPoliLq;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSL300_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ReinvstPoliLqIdbsl300 reinvstPoliLq) {
        this.idsv0003 = idsv0003;
        this.reinvstPoliLq = reinvstPoliLq;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsl300 getInstance() {
        return ((Idbsl300)Programs.getInstance(Idbsl300.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSL300'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSL300");
        // COB_CODE: MOVE 'REINVST_POLI_LQ' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("REINVST_POLI_LQ");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_REINVST_POLI_LQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_RAMO
        //                ,IB_POLI
        //                ,PR_KEY_SIST_ESTNO
        //                ,SEC_KEY_SIST_ESTNO
        //                ,COD_CAN
        //                ,COD_AGE
        //                ,COD_SUB_AGE
        //                ,IMP_RES
        //                ,TP_LIQ
        //                ,TP_SIST_ESTNO
        //                ,COD_FISC_PART_IVA
        //                ,COD_MOVI_LIQ
        //                ,TP_INVST_LIQ
        //                ,IMP_TOT_LIQ
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_DECOR_POLI_LQ
        //             INTO
        //                :L30-ID-REINVST-POLI-LQ
        //               ,:L30-ID-MOVI-CRZ
        //               ,:L30-ID-MOVI-CHIU
        //                :IND-L30-ID-MOVI-CHIU
        //               ,:L30-DT-INI-EFF-DB
        //               ,:L30-DT-END-EFF-DB
        //               ,:L30-COD-COMP-ANIA
        //               ,:L30-COD-RAMO
        //               ,:L30-IB-POLI
        //               ,:L30-PR-KEY-SIST-ESTNO
        //                :IND-L30-PR-KEY-SIST-ESTNO
        //               ,:L30-SEC-KEY-SIST-ESTNO
        //                :IND-L30-SEC-KEY-SIST-ESTNO
        //               ,:L30-COD-CAN
        //               ,:L30-COD-AGE
        //               ,:L30-COD-SUB-AGE
        //                :IND-L30-COD-SUB-AGE
        //               ,:L30-IMP-RES
        //               ,:L30-TP-LIQ
        //               ,:L30-TP-SIST-ESTNO
        //               ,:L30-COD-FISC-PART-IVA
        //               ,:L30-COD-MOVI-LIQ
        //               ,:L30-TP-INVST-LIQ
        //                :IND-L30-TP-INVST-LIQ
        //               ,:L30-IMP-TOT-LIQ
        //               ,:L30-DS-RIGA
        //               ,:L30-DS-OPER-SQL
        //               ,:L30-DS-VER
        //               ,:L30-DS-TS-INI-CPTZ
        //               ,:L30-DS-TS-END-CPTZ
        //               ,:L30-DS-UTENTE
        //               ,:L30-DS-STATO-ELAB
        //               ,:L30-DT-DECOR-POLI-LQ-DB
        //                :IND-L30-DT-DECOR-POLI-LQ
        //             FROM REINVST_POLI_LQ
        //             WHERE     DS_RIGA = :L30-DS-RIGA
        //           END-EXEC.
        reinvstPoliLqDao.selectByL30DsRiga(reinvstPoliLq.getL30DsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO REINVST_POLI_LQ
            //                  (
            //                     ID_REINVST_POLI_LQ
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,COD_RAMO
            //                    ,IB_POLI
            //                    ,PR_KEY_SIST_ESTNO
            //                    ,SEC_KEY_SIST_ESTNO
            //                    ,COD_CAN
            //                    ,COD_AGE
            //                    ,COD_SUB_AGE
            //                    ,IMP_RES
            //                    ,TP_LIQ
            //                    ,TP_SIST_ESTNO
            //                    ,COD_FISC_PART_IVA
            //                    ,COD_MOVI_LIQ
            //                    ,TP_INVST_LIQ
            //                    ,IMP_TOT_LIQ
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,DT_DECOR_POLI_LQ
            //                  )
            //              VALUES
            //                  (
            //                    :L30-ID-REINVST-POLI-LQ
            //                    ,:L30-ID-MOVI-CRZ
            //                    ,:L30-ID-MOVI-CHIU
            //                     :IND-L30-ID-MOVI-CHIU
            //                    ,:L30-DT-INI-EFF-DB
            //                    ,:L30-DT-END-EFF-DB
            //                    ,:L30-COD-COMP-ANIA
            //                    ,:L30-COD-RAMO
            //                    ,:L30-IB-POLI
            //                    ,:L30-PR-KEY-SIST-ESTNO
            //                     :IND-L30-PR-KEY-SIST-ESTNO
            //                    ,:L30-SEC-KEY-SIST-ESTNO
            //                     :IND-L30-SEC-KEY-SIST-ESTNO
            //                    ,:L30-COD-CAN
            //                    ,:L30-COD-AGE
            //                    ,:L30-COD-SUB-AGE
            //                     :IND-L30-COD-SUB-AGE
            //                    ,:L30-IMP-RES
            //                    ,:L30-TP-LIQ
            //                    ,:L30-TP-SIST-ESTNO
            //                    ,:L30-COD-FISC-PART-IVA
            //                    ,:L30-COD-MOVI-LIQ
            //                    ,:L30-TP-INVST-LIQ
            //                     :IND-L30-TP-INVST-LIQ
            //                    ,:L30-IMP-TOT-LIQ
            //                    ,:L30-DS-RIGA
            //                    ,:L30-DS-OPER-SQL
            //                    ,:L30-DS-VER
            //                    ,:L30-DS-TS-INI-CPTZ
            //                    ,:L30-DS-TS-END-CPTZ
            //                    ,:L30-DS-UTENTE
            //                    ,:L30-DS-STATO-ELAB
            //                    ,:L30-DT-DECOR-POLI-LQ-DB
            //                     :IND-L30-DT-DECOR-POLI-LQ
            //                  )
            //           END-EXEC
            reinvstPoliLqDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE REINVST_POLI_LQ SET
        //                   ID_REINVST_POLI_LQ     =
        //                :L30-ID-REINVST-POLI-LQ
        //                  ,ID_MOVI_CRZ            =
        //                :L30-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :L30-ID-MOVI-CHIU
        //                                       :IND-L30-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :L30-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :L30-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :L30-COD-COMP-ANIA
        //                  ,COD_RAMO               =
        //                :L30-COD-RAMO
        //                  ,IB_POLI                =
        //                :L30-IB-POLI
        //                  ,PR_KEY_SIST_ESTNO      =
        //                :L30-PR-KEY-SIST-ESTNO
        //                                       :IND-L30-PR-KEY-SIST-ESTNO
        //                  ,SEC_KEY_SIST_ESTNO     =
        //                :L30-SEC-KEY-SIST-ESTNO
        //                                       :IND-L30-SEC-KEY-SIST-ESTNO
        //                  ,COD_CAN                =
        //                :L30-COD-CAN
        //                  ,COD_AGE                =
        //                :L30-COD-AGE
        //                  ,COD_SUB_AGE            =
        //                :L30-COD-SUB-AGE
        //                                       :IND-L30-COD-SUB-AGE
        //                  ,IMP_RES                =
        //                :L30-IMP-RES
        //                  ,TP_LIQ                 =
        //                :L30-TP-LIQ
        //                  ,TP_SIST_ESTNO          =
        //                :L30-TP-SIST-ESTNO
        //                  ,COD_FISC_PART_IVA      =
        //                :L30-COD-FISC-PART-IVA
        //                  ,COD_MOVI_LIQ           =
        //                :L30-COD-MOVI-LIQ
        //                  ,TP_INVST_LIQ           =
        //                :L30-TP-INVST-LIQ
        //                                       :IND-L30-TP-INVST-LIQ
        //                  ,IMP_TOT_LIQ            =
        //                :L30-IMP-TOT-LIQ
        //                  ,DS_RIGA                =
        //                :L30-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :L30-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :L30-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :L30-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :L30-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :L30-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :L30-DS-STATO-ELAB
        //                  ,DT_DECOR_POLI_LQ       =
        //           :L30-DT-DECOR-POLI-LQ-DB
        //                                       :IND-L30-DT-DECOR-POLI-LQ
        //                WHERE     DS_RIGA = :L30-DS-RIGA
        //           END-EXEC.
        reinvstPoliLqDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM REINVST_POLI_LQ
        //                WHERE     DS_RIGA = :L30-DS-RIGA
        //           END-EXEC.
        reinvstPoliLqDao.deleteByL30DsRiga(reinvstPoliLq.getL30DsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-L30 CURSOR FOR
        //              SELECT
        //                     ID_REINVST_POLI_LQ
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_RAMO
        //                    ,IB_POLI
        //                    ,PR_KEY_SIST_ESTNO
        //                    ,SEC_KEY_SIST_ESTNO
        //                    ,COD_CAN
        //                    ,COD_AGE
        //                    ,COD_SUB_AGE
        //                    ,IMP_RES
        //                    ,TP_LIQ
        //                    ,TP_SIST_ESTNO
        //                    ,COD_FISC_PART_IVA
        //                    ,COD_MOVI_LIQ
        //                    ,TP_INVST_LIQ
        //                    ,IMP_TOT_LIQ
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,DT_DECOR_POLI_LQ
        //              FROM REINVST_POLI_LQ
        //              WHERE     ID_REINVST_POLI_LQ = :L30-ID-REINVST-POLI-LQ
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_REINVST_POLI_LQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_RAMO
        //                ,IB_POLI
        //                ,PR_KEY_SIST_ESTNO
        //                ,SEC_KEY_SIST_ESTNO
        //                ,COD_CAN
        //                ,COD_AGE
        //                ,COD_SUB_AGE
        //                ,IMP_RES
        //                ,TP_LIQ
        //                ,TP_SIST_ESTNO
        //                ,COD_FISC_PART_IVA
        //                ,COD_MOVI_LIQ
        //                ,TP_INVST_LIQ
        //                ,IMP_TOT_LIQ
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_DECOR_POLI_LQ
        //             INTO
        //                :L30-ID-REINVST-POLI-LQ
        //               ,:L30-ID-MOVI-CRZ
        //               ,:L30-ID-MOVI-CHIU
        //                :IND-L30-ID-MOVI-CHIU
        //               ,:L30-DT-INI-EFF-DB
        //               ,:L30-DT-END-EFF-DB
        //               ,:L30-COD-COMP-ANIA
        //               ,:L30-COD-RAMO
        //               ,:L30-IB-POLI
        //               ,:L30-PR-KEY-SIST-ESTNO
        //                :IND-L30-PR-KEY-SIST-ESTNO
        //               ,:L30-SEC-KEY-SIST-ESTNO
        //                :IND-L30-SEC-KEY-SIST-ESTNO
        //               ,:L30-COD-CAN
        //               ,:L30-COD-AGE
        //               ,:L30-COD-SUB-AGE
        //                :IND-L30-COD-SUB-AGE
        //               ,:L30-IMP-RES
        //               ,:L30-TP-LIQ
        //               ,:L30-TP-SIST-ESTNO
        //               ,:L30-COD-FISC-PART-IVA
        //               ,:L30-COD-MOVI-LIQ
        //               ,:L30-TP-INVST-LIQ
        //                :IND-L30-TP-INVST-LIQ
        //               ,:L30-IMP-TOT-LIQ
        //               ,:L30-DS-RIGA
        //               ,:L30-DS-OPER-SQL
        //               ,:L30-DS-VER
        //               ,:L30-DS-TS-INI-CPTZ
        //               ,:L30-DS-TS-END-CPTZ
        //               ,:L30-DS-UTENTE
        //               ,:L30-DS-STATO-ELAB
        //               ,:L30-DT-DECOR-POLI-LQ-DB
        //                :IND-L30-DT-DECOR-POLI-LQ
        //             FROM REINVST_POLI_LQ
        //             WHERE     ID_REINVST_POLI_LQ = :L30-ID-REINVST-POLI-LQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        reinvstPoliLqDao.selectRec(reinvstPoliLq.getL30IdReinvstPoliLq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE REINVST_POLI_LQ SET
        //                   ID_REINVST_POLI_LQ     =
        //                :L30-ID-REINVST-POLI-LQ
        //                  ,ID_MOVI_CRZ            =
        //                :L30-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :L30-ID-MOVI-CHIU
        //                                       :IND-L30-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :L30-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :L30-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :L30-COD-COMP-ANIA
        //                  ,COD_RAMO               =
        //                :L30-COD-RAMO
        //                  ,IB_POLI                =
        //                :L30-IB-POLI
        //                  ,PR_KEY_SIST_ESTNO      =
        //                :L30-PR-KEY-SIST-ESTNO
        //                                       :IND-L30-PR-KEY-SIST-ESTNO
        //                  ,SEC_KEY_SIST_ESTNO     =
        //                :L30-SEC-KEY-SIST-ESTNO
        //                                       :IND-L30-SEC-KEY-SIST-ESTNO
        //                  ,COD_CAN                =
        //                :L30-COD-CAN
        //                  ,COD_AGE                =
        //                :L30-COD-AGE
        //                  ,COD_SUB_AGE            =
        //                :L30-COD-SUB-AGE
        //                                       :IND-L30-COD-SUB-AGE
        //                  ,IMP_RES                =
        //                :L30-IMP-RES
        //                  ,TP_LIQ                 =
        //                :L30-TP-LIQ
        //                  ,TP_SIST_ESTNO          =
        //                :L30-TP-SIST-ESTNO
        //                  ,COD_FISC_PART_IVA      =
        //                :L30-COD-FISC-PART-IVA
        //                  ,COD_MOVI_LIQ           =
        //                :L30-COD-MOVI-LIQ
        //                  ,TP_INVST_LIQ           =
        //                :L30-TP-INVST-LIQ
        //                                       :IND-L30-TP-INVST-LIQ
        //                  ,IMP_TOT_LIQ            =
        //                :L30-IMP-TOT-LIQ
        //                  ,DS_RIGA                =
        //                :L30-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :L30-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :L30-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :L30-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :L30-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :L30-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :L30-DS-STATO-ELAB
        //                  ,DT_DECOR_POLI_LQ       =
        //           :L30-DT-DECOR-POLI-LQ-DB
        //                                       :IND-L30-DT-DECOR-POLI-LQ
        //                WHERE     DS_RIGA = :L30-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        reinvstPoliLqDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-L30
        //           END-EXEC.
        reinvstPoliLqDao.openCIdUpdEffL30(reinvstPoliLq.getL30IdReinvstPoliLq(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-L30
        //           END-EXEC.
        reinvstPoliLqDao.closeCIdUpdEffL30();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-L30
        //           INTO
        //                :L30-ID-REINVST-POLI-LQ
        //               ,:L30-ID-MOVI-CRZ
        //               ,:L30-ID-MOVI-CHIU
        //                :IND-L30-ID-MOVI-CHIU
        //               ,:L30-DT-INI-EFF-DB
        //               ,:L30-DT-END-EFF-DB
        //               ,:L30-COD-COMP-ANIA
        //               ,:L30-COD-RAMO
        //               ,:L30-IB-POLI
        //               ,:L30-PR-KEY-SIST-ESTNO
        //                :IND-L30-PR-KEY-SIST-ESTNO
        //               ,:L30-SEC-KEY-SIST-ESTNO
        //                :IND-L30-SEC-KEY-SIST-ESTNO
        //               ,:L30-COD-CAN
        //               ,:L30-COD-AGE
        //               ,:L30-COD-SUB-AGE
        //                :IND-L30-COD-SUB-AGE
        //               ,:L30-IMP-RES
        //               ,:L30-TP-LIQ
        //               ,:L30-TP-SIST-ESTNO
        //               ,:L30-COD-FISC-PART-IVA
        //               ,:L30-COD-MOVI-LIQ
        //               ,:L30-TP-INVST-LIQ
        //                :IND-L30-TP-INVST-LIQ
        //               ,:L30-IMP-TOT-LIQ
        //               ,:L30-DS-RIGA
        //               ,:L30-DS-OPER-SQL
        //               ,:L30-DS-VER
        //               ,:L30-DS-TS-INI-CPTZ
        //               ,:L30-DS-TS-END-CPTZ
        //               ,:L30-DS-UTENTE
        //               ,:L30-DS-STATO-ELAB
        //               ,:L30-DT-DECOR-POLI-LQ-DB
        //                :IND-L30-DT-DECOR-POLI-LQ
        //           END-EXEC.
        reinvstPoliLqDao.fetchCIdUpdEffL30(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DCL-CUR-IBS-POLI<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DclCurIbsPoli() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-L30-0 CURSOR FOR
    //              SELECT
    //                     ID_REINVST_POLI_LQ
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,COD_RAMO
    //                    ,IB_POLI
    //                    ,PR_KEY_SIST_ESTNO
    //                    ,SEC_KEY_SIST_ESTNO
    //                    ,COD_CAN
    //                    ,COD_AGE
    //                    ,COD_SUB_AGE
    //                    ,IMP_RES
    //                    ,TP_LIQ
    //                    ,TP_SIST_ESTNO
    //                    ,COD_FISC_PART_IVA
    //                    ,COD_MOVI_LIQ
    //                    ,TP_INVST_LIQ
    //                    ,IMP_TOT_LIQ
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,DT_DECOR_POLI_LQ
    //              FROM REINVST_POLI_LQ
    //              WHERE     IB_POLI = :L30-IB-POLI
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_REINVST_POLI_LQ ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //                  THRU A605-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-POLI
            //              THRU A605-POLI-EX
            a605DclCurIbsPoli();
        }
    }

    /**Original name: A610-SELECT-IBS-POLI<br>*/
    private void a610SelectIbsPoli() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_REINVST_POLI_LQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_RAMO
        //                ,IB_POLI
        //                ,PR_KEY_SIST_ESTNO
        //                ,SEC_KEY_SIST_ESTNO
        //                ,COD_CAN
        //                ,COD_AGE
        //                ,COD_SUB_AGE
        //                ,IMP_RES
        //                ,TP_LIQ
        //                ,TP_SIST_ESTNO
        //                ,COD_FISC_PART_IVA
        //                ,COD_MOVI_LIQ
        //                ,TP_INVST_LIQ
        //                ,IMP_TOT_LIQ
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_DECOR_POLI_LQ
        //             INTO
        //                :L30-ID-REINVST-POLI-LQ
        //               ,:L30-ID-MOVI-CRZ
        //               ,:L30-ID-MOVI-CHIU
        //                :IND-L30-ID-MOVI-CHIU
        //               ,:L30-DT-INI-EFF-DB
        //               ,:L30-DT-END-EFF-DB
        //               ,:L30-COD-COMP-ANIA
        //               ,:L30-COD-RAMO
        //               ,:L30-IB-POLI
        //               ,:L30-PR-KEY-SIST-ESTNO
        //                :IND-L30-PR-KEY-SIST-ESTNO
        //               ,:L30-SEC-KEY-SIST-ESTNO
        //                :IND-L30-SEC-KEY-SIST-ESTNO
        //               ,:L30-COD-CAN
        //               ,:L30-COD-AGE
        //               ,:L30-COD-SUB-AGE
        //                :IND-L30-COD-SUB-AGE
        //               ,:L30-IMP-RES
        //               ,:L30-TP-LIQ
        //               ,:L30-TP-SIST-ESTNO
        //               ,:L30-COD-FISC-PART-IVA
        //               ,:L30-COD-MOVI-LIQ
        //               ,:L30-TP-INVST-LIQ
        //                :IND-L30-TP-INVST-LIQ
        //               ,:L30-IMP-TOT-LIQ
        //               ,:L30-DS-RIGA
        //               ,:L30-DS-OPER-SQL
        //               ,:L30-DS-VER
        //               ,:L30-DS-TS-INI-CPTZ
        //               ,:L30-DS-TS-END-CPTZ
        //               ,:L30-DS-UTENTE
        //               ,:L30-DS-STATO-ELAB
        //               ,:L30-DT-DECOR-POLI-LQ-DB
        //                :IND-L30-DT-DECOR-POLI-LQ
        //             FROM REINVST_POLI_LQ
        //             WHERE     IB_POLI = :L30-IB-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        reinvstPoliLqDao.selectRec1(reinvstPoliLq.getL30IbPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //                  THRU A610-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: PERFORM A610-SELECT-IBS-POLI
            //              THRU A610-POLI-EX
            a610SelectIbsPoli();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-L30-0
            //           END-EXEC
            reinvstPoliLqDao.openCIbsEffL300(reinvstPoliLq.getL30IbPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-L30-0
            //           END-EXEC
            reinvstPoliLqDao.closeCIbsEffL300();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FN-IBS-POLI<br>*/
    private void a690FnIbsPoli() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-L30-0
        //           INTO
        //                :L30-ID-REINVST-POLI-LQ
        //               ,:L30-ID-MOVI-CRZ
        //               ,:L30-ID-MOVI-CHIU
        //                :IND-L30-ID-MOVI-CHIU
        //               ,:L30-DT-INI-EFF-DB
        //               ,:L30-DT-END-EFF-DB
        //               ,:L30-COD-COMP-ANIA
        //               ,:L30-COD-RAMO
        //               ,:L30-IB-POLI
        //               ,:L30-PR-KEY-SIST-ESTNO
        //                :IND-L30-PR-KEY-SIST-ESTNO
        //               ,:L30-SEC-KEY-SIST-ESTNO
        //                :IND-L30-SEC-KEY-SIST-ESTNO
        //               ,:L30-COD-CAN
        //               ,:L30-COD-AGE
        //               ,:L30-COD-SUB-AGE
        //                :IND-L30-COD-SUB-AGE
        //               ,:L30-IMP-RES
        //               ,:L30-TP-LIQ
        //               ,:L30-TP-SIST-ESTNO
        //               ,:L30-COD-FISC-PART-IVA
        //               ,:L30-COD-MOVI-LIQ
        //               ,:L30-TP-INVST-LIQ
        //                :IND-L30-TP-INVST-LIQ
        //               ,:L30-IMP-TOT-LIQ
        //               ,:L30-DS-RIGA
        //               ,:L30-DS-OPER-SQL
        //               ,:L30-DS-VER
        //               ,:L30-DS-TS-INI-CPTZ
        //               ,:L30-DS-TS-END-CPTZ
        //               ,:L30-DS-UTENTE
        //               ,:L30-DS-STATO-ELAB
        //               ,:L30-DT-DECOR-POLI-LQ-DB
        //                :IND-L30-DT-DECOR-POLI-LQ
        //           END-EXEC.
        reinvstPoliLqDao.fetchCIbsEffL300(this);
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //                  THRU A690-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: PERFORM A690-FN-IBS-POLI
            //              THRU A690-POLI-EX
            a690FnIbsPoli();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX
            a670CloseCursorIbs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_REINVST_POLI_LQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_RAMO
        //                ,IB_POLI
        //                ,PR_KEY_SIST_ESTNO
        //                ,SEC_KEY_SIST_ESTNO
        //                ,COD_CAN
        //                ,COD_AGE
        //                ,COD_SUB_AGE
        //                ,IMP_RES
        //                ,TP_LIQ
        //                ,TP_SIST_ESTNO
        //                ,COD_FISC_PART_IVA
        //                ,COD_MOVI_LIQ
        //                ,TP_INVST_LIQ
        //                ,IMP_TOT_LIQ
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_DECOR_POLI_LQ
        //             INTO
        //                :L30-ID-REINVST-POLI-LQ
        //               ,:L30-ID-MOVI-CRZ
        //               ,:L30-ID-MOVI-CHIU
        //                :IND-L30-ID-MOVI-CHIU
        //               ,:L30-DT-INI-EFF-DB
        //               ,:L30-DT-END-EFF-DB
        //               ,:L30-COD-COMP-ANIA
        //               ,:L30-COD-RAMO
        //               ,:L30-IB-POLI
        //               ,:L30-PR-KEY-SIST-ESTNO
        //                :IND-L30-PR-KEY-SIST-ESTNO
        //               ,:L30-SEC-KEY-SIST-ESTNO
        //                :IND-L30-SEC-KEY-SIST-ESTNO
        //               ,:L30-COD-CAN
        //               ,:L30-COD-AGE
        //               ,:L30-COD-SUB-AGE
        //                :IND-L30-COD-SUB-AGE
        //               ,:L30-IMP-RES
        //               ,:L30-TP-LIQ
        //               ,:L30-TP-SIST-ESTNO
        //               ,:L30-COD-FISC-PART-IVA
        //               ,:L30-COD-MOVI-LIQ
        //               ,:L30-TP-INVST-LIQ
        //                :IND-L30-TP-INVST-LIQ
        //               ,:L30-IMP-TOT-LIQ
        //               ,:L30-DS-RIGA
        //               ,:L30-DS-OPER-SQL
        //               ,:L30-DS-VER
        //               ,:L30-DS-TS-INI-CPTZ
        //               ,:L30-DS-TS-END-CPTZ
        //               ,:L30-DS-UTENTE
        //               ,:L30-DS-STATO-ELAB
        //               ,:L30-DT-DECOR-POLI-LQ-DB
        //                :IND-L30-DT-DECOR-POLI-LQ
        //             FROM REINVST_POLI_LQ
        //             WHERE     ID_REINVST_POLI_LQ = :L30-ID-REINVST-POLI-LQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        reinvstPoliLqDao.selectRec2(reinvstPoliLq.getL30IdReinvstPoliLq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DCL-CUR-IBS-POLI<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DclCurIbsPoli() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-L30-0 CURSOR FOR
    //              SELECT
    //                     ID_REINVST_POLI_LQ
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,COD_RAMO
    //                    ,IB_POLI
    //                    ,PR_KEY_SIST_ESTNO
    //                    ,SEC_KEY_SIST_ESTNO
    //                    ,COD_CAN
    //                    ,COD_AGE
    //                    ,COD_SUB_AGE
    //                    ,IMP_RES
    //                    ,TP_LIQ
    //                    ,TP_SIST_ESTNO
    //                    ,COD_FISC_PART_IVA
    //                    ,COD_MOVI_LIQ
    //                    ,TP_INVST_LIQ
    //                    ,IMP_TOT_LIQ
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,DT_DECOR_POLI_LQ
    //              FROM REINVST_POLI_LQ
    //              WHERE     IB_POLI = :L30-IB-POLI
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_REINVST_POLI_LQ ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //                  THRU B605-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: PERFORM B605-DCL-CUR-IBS-POLI
            //              THRU B605-POLI-EX
            b605DclCurIbsPoli();
        }
    }

    /**Original name: B610-SELECT-IBS-POLI<br>*/
    private void b610SelectIbsPoli() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_REINVST_POLI_LQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_RAMO
        //                ,IB_POLI
        //                ,PR_KEY_SIST_ESTNO
        //                ,SEC_KEY_SIST_ESTNO
        //                ,COD_CAN
        //                ,COD_AGE
        //                ,COD_SUB_AGE
        //                ,IMP_RES
        //                ,TP_LIQ
        //                ,TP_SIST_ESTNO
        //                ,COD_FISC_PART_IVA
        //                ,COD_MOVI_LIQ
        //                ,TP_INVST_LIQ
        //                ,IMP_TOT_LIQ
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_DECOR_POLI_LQ
        //             INTO
        //                :L30-ID-REINVST-POLI-LQ
        //               ,:L30-ID-MOVI-CRZ
        //               ,:L30-ID-MOVI-CHIU
        //                :IND-L30-ID-MOVI-CHIU
        //               ,:L30-DT-INI-EFF-DB
        //               ,:L30-DT-END-EFF-DB
        //               ,:L30-COD-COMP-ANIA
        //               ,:L30-COD-RAMO
        //               ,:L30-IB-POLI
        //               ,:L30-PR-KEY-SIST-ESTNO
        //                :IND-L30-PR-KEY-SIST-ESTNO
        //               ,:L30-SEC-KEY-SIST-ESTNO
        //                :IND-L30-SEC-KEY-SIST-ESTNO
        //               ,:L30-COD-CAN
        //               ,:L30-COD-AGE
        //               ,:L30-COD-SUB-AGE
        //                :IND-L30-COD-SUB-AGE
        //               ,:L30-IMP-RES
        //               ,:L30-TP-LIQ
        //               ,:L30-TP-SIST-ESTNO
        //               ,:L30-COD-FISC-PART-IVA
        //               ,:L30-COD-MOVI-LIQ
        //               ,:L30-TP-INVST-LIQ
        //                :IND-L30-TP-INVST-LIQ
        //               ,:L30-IMP-TOT-LIQ
        //               ,:L30-DS-RIGA
        //               ,:L30-DS-OPER-SQL
        //               ,:L30-DS-VER
        //               ,:L30-DS-TS-INI-CPTZ
        //               ,:L30-DS-TS-END-CPTZ
        //               ,:L30-DS-UTENTE
        //               ,:L30-DS-STATO-ELAB
        //               ,:L30-DT-DECOR-POLI-LQ-DB
        //                :IND-L30-DT-DECOR-POLI-LQ
        //             FROM REINVST_POLI_LQ
        //             WHERE     IB_POLI = :L30-IB-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        reinvstPoliLqDao.selectRec3(reinvstPoliLq.getL30IbPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //                  THRU B610-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: PERFORM B610-SELECT-IBS-POLI
            //              THRU B610-POLI-EX
            b610SelectIbsPoli();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-L30-0
            //           END-EXEC
            reinvstPoliLqDao.openCIbsCpzL300(reinvstPoliLq.getL30IbPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-L30-0
            //           END-EXEC
            reinvstPoliLqDao.closeCIbsCpzL300();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FN-IBS-POLI<br>*/
    private void b690FnIbsPoli() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-L30-0
        //           INTO
        //                :L30-ID-REINVST-POLI-LQ
        //               ,:L30-ID-MOVI-CRZ
        //               ,:L30-ID-MOVI-CHIU
        //                :IND-L30-ID-MOVI-CHIU
        //               ,:L30-DT-INI-EFF-DB
        //               ,:L30-DT-END-EFF-DB
        //               ,:L30-COD-COMP-ANIA
        //               ,:L30-COD-RAMO
        //               ,:L30-IB-POLI
        //               ,:L30-PR-KEY-SIST-ESTNO
        //                :IND-L30-PR-KEY-SIST-ESTNO
        //               ,:L30-SEC-KEY-SIST-ESTNO
        //                :IND-L30-SEC-KEY-SIST-ESTNO
        //               ,:L30-COD-CAN
        //               ,:L30-COD-AGE
        //               ,:L30-COD-SUB-AGE
        //                :IND-L30-COD-SUB-AGE
        //               ,:L30-IMP-RES
        //               ,:L30-TP-LIQ
        //               ,:L30-TP-SIST-ESTNO
        //               ,:L30-COD-FISC-PART-IVA
        //               ,:L30-COD-MOVI-LIQ
        //               ,:L30-TP-INVST-LIQ
        //                :IND-L30-TP-INVST-LIQ
        //               ,:L30-IMP-TOT-LIQ
        //               ,:L30-DS-RIGA
        //               ,:L30-DS-OPER-SQL
        //               ,:L30-DS-VER
        //               ,:L30-DS-TS-INI-CPTZ
        //               ,:L30-DS-TS-END-CPTZ
        //               ,:L30-DS-UTENTE
        //               ,:L30-DS-STATO-ELAB
        //               ,:L30-DT-DECOR-POLI-LQ-DB
        //                :IND-L30-DT-DECOR-POLI-LQ
        //           END-EXEC.
        reinvstPoliLqDao.fetchCIbsCpzL300(this);
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: IF L30-IB-POLI NOT = HIGH-VALUES
        //                  THRU B690-POLI-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(reinvstPoliLq.getL30IbPoli(), ReinvstPoliLqIdbsl300.Len.L30_IB_POLI)) {
            // COB_CODE: PERFORM B690-FN-IBS-POLI
            //              THRU B690-POLI-EX
            b690FnIbsPoli();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX
            b670CloseCursorIbsCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-L30-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO L30-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndReinvstPoliLq().getDescDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L30-ID-MOVI-CHIU-NULL
            reinvstPoliLq.getL30IdMoviChiu().setL30IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L30IdMoviChiu.Len.L30_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-L30-PR-KEY-SIST-ESTNO = -1
        //              MOVE HIGH-VALUES TO L30-PR-KEY-SIST-ESTNO-NULL
        //           END-IF
        if (ws.getIndReinvstPoliLq().getTipoDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L30-PR-KEY-SIST-ESTNO-NULL
            reinvstPoliLq.setL30PrKeySistEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ReinvstPoliLqIdbsl300.Len.L30_PR_KEY_SIST_ESTNO));
        }
        // COB_CODE: IF IND-L30-SEC-KEY-SIST-ESTNO = -1
        //              MOVE HIGH-VALUES TO L30-SEC-KEY-SIST-ESTNO-NULL
        //           END-IF
        if (ws.getIndReinvstPoliLq().getLunghezzaDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L30-SEC-KEY-SIST-ESTNO-NULL
            reinvstPoliLq.setL30SecKeySistEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ReinvstPoliLqIdbsl300.Len.L30_SEC_KEY_SIST_ESTNO));
        }
        // COB_CODE: IF IND-L30-COD-SUB-AGE = -1
        //              MOVE HIGH-VALUES TO L30-COD-SUB-AGE-NULL
        //           END-IF
        if (ws.getIndReinvstPoliLq().getPrecisioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L30-COD-SUB-AGE-NULL
            reinvstPoliLq.getL30CodSubAge().setL30CodSubAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L30CodSubAge.Len.L30_COD_SUB_AGE_NULL));
        }
        // COB_CODE: IF IND-L30-TP-INVST-LIQ = -1
        //              MOVE HIGH-VALUES TO L30-TP-INVST-LIQ-NULL
        //           END-IF
        if (ws.getIndReinvstPoliLq().getCodDominio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L30-TP-INVST-LIQ-NULL
            reinvstPoliLq.getL30TpInvstLiq().setL30TpInvstLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L30TpInvstLiq.Len.L30_TP_INVST_LIQ_NULL));
        }
        // COB_CODE: IF IND-L30-DT-DECOR-POLI-LQ = -1
        //              MOVE HIGH-VALUES TO L30-DT-DECOR-POLI-LQ-NULL
        //           END-IF.
        if (ws.getIndReinvstPoliLq().getFormattazioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO L30-DT-DECOR-POLI-LQ-NULL
            reinvstPoliLq.getL30DtDecorPoliLq().setL30DtDecorPoliLqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L30DtDecorPoliLq.Len.L30_DT_DECOR_POLI_LQ_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO L30-DS-OPER-SQL
        reinvstPoliLq.setL30DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO L30-DS-VER
        reinvstPoliLq.setL30DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO L30-DS-UTENTE
        reinvstPoliLq.setL30DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO L30-DS-STATO-ELAB.
        reinvstPoliLq.setL30DsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO L30-DS-OPER-SQL
        reinvstPoliLq.setL30DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO L30-DS-UTENTE.
        reinvstPoliLq.setL30DsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF L30-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-L30-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-L30-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(reinvstPoliLq.getL30IdMoviChiu().getL30IdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-L30-ID-MOVI-CHIU
            ws.getIndReinvstPoliLq().setFlContiguousData(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-L30-ID-MOVI-CHIU
            ws.getIndReinvstPoliLq().setFlContiguousData(((short)0));
        }
        // COB_CODE: IF L30-PR-KEY-SIST-ESTNO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-L30-PR-KEY-SIST-ESTNO
        //           ELSE
        //              MOVE 0 TO IND-L30-PR-KEY-SIST-ESTNO
        //           END-IF
        if (Characters.EQ_HIGH.test(reinvstPoliLq.getL30PrKeySistEstno(), ReinvstPoliLqIdbsl300.Len.L30_PR_KEY_SIST_ESTNO)) {
            // COB_CODE: MOVE -1 TO IND-L30-PR-KEY-SIST-ESTNO
            ws.getIndReinvstPoliLq().setTipoDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-L30-PR-KEY-SIST-ESTNO
            ws.getIndReinvstPoliLq().setTipoDato(((short)0));
        }
        // COB_CODE: IF L30-SEC-KEY-SIST-ESTNO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-L30-SEC-KEY-SIST-ESTNO
        //           ELSE
        //              MOVE 0 TO IND-L30-SEC-KEY-SIST-ESTNO
        //           END-IF
        if (Characters.EQ_HIGH.test(reinvstPoliLq.getL30SecKeySistEstno(), ReinvstPoliLqIdbsl300.Len.L30_SEC_KEY_SIST_ESTNO)) {
            // COB_CODE: MOVE -1 TO IND-L30-SEC-KEY-SIST-ESTNO
            ws.getIndReinvstPoliLq().setLunghezzaDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-L30-SEC-KEY-SIST-ESTNO
            ws.getIndReinvstPoliLq().setLunghezzaDato(((short)0));
        }
        // COB_CODE: IF L30-COD-SUB-AGE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-L30-COD-SUB-AGE
        //           ELSE
        //              MOVE 0 TO IND-L30-COD-SUB-AGE
        //           END-IF
        if (Characters.EQ_HIGH.test(reinvstPoliLq.getL30CodSubAge().getL30CodSubAgeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-L30-COD-SUB-AGE
            ws.getIndReinvstPoliLq().setPrecisioneDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-L30-COD-SUB-AGE
            ws.getIndReinvstPoliLq().setPrecisioneDato(((short)0));
        }
        // COB_CODE: IF L30-TP-INVST-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-L30-TP-INVST-LIQ
        //           ELSE
        //              MOVE 0 TO IND-L30-TP-INVST-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(reinvstPoliLq.getL30TpInvstLiq().getL30TpInvstLiqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-L30-TP-INVST-LIQ
            ws.getIndReinvstPoliLq().setTypeRecord(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-L30-TP-INVST-LIQ
            ws.getIndReinvstPoliLq().setTypeRecord(((short)0));
        }
        // COB_CODE: IF L30-DT-DECOR-POLI-LQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-L30-DT-DECOR-POLI-LQ
        //           ELSE
        //              MOVE 0 TO IND-L30-DT-DECOR-POLI-LQ
        //           END-IF.
        if (Characters.EQ_HIGH.test(reinvstPoliLq.getL30DtDecorPoliLq().getL30DtDecorPoliLqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-L30-DT-DECOR-POLI-LQ
            ws.getIndReinvstPoliLq().setFormattazioneDato(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-L30-DT-DECOR-POLI-LQ
            ws.getIndReinvstPoliLq().setFormattazioneDato(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : L30-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE REINVST-POLI-LQ TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(reinvstPoliLq.getReinvstPoliLqFormatted());
        // COB_CODE: MOVE L30-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(reinvstPoliLq.getL30IdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO L30-ID-MOVI-CHIU
                reinvstPoliLq.getL30IdMoviChiu().setL30IdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO L30-DS-TS-END-CPTZ
                reinvstPoliLq.setL30DsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO L30-ID-MOVI-CRZ
                    reinvstPoliLq.setL30IdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO L30-ID-MOVI-CHIU-NULL
                    reinvstPoliLq.getL30IdMoviChiu().setL30IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L30IdMoviChiu.Len.L30_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO L30-DT-END-EFF
                    reinvstPoliLq.setL30DtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO L30-DS-TS-INI-CPTZ
                    reinvstPoliLq.setL30DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO L30-DS-TS-END-CPTZ
                    reinvstPoliLq.setL30DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO REINVST-POLI-LQ.
        reinvstPoliLq.setReinvstPoliLqFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO L30-ID-MOVI-CRZ.
        reinvstPoliLq.setL30IdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO L30-ID-MOVI-CHIU-NULL.
        reinvstPoliLq.getL30IdMoviChiu().setL30IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, L30IdMoviChiu.Len.L30_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO L30-DT-INI-EFF.
        reinvstPoliLq.setL30DtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO L30-DT-END-EFF.
        reinvstPoliLq.setL30DtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO L30-DS-TS-INI-CPTZ.
        reinvstPoliLq.setL30DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO L30-DS-TS-END-CPTZ.
        reinvstPoliLq.setL30DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO L30-COD-COMP-ANIA.
        reinvstPoliLq.setL30CodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE L30-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(reinvstPoliLq.getL30DtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO L30-DT-INI-EFF-DB
        ws.getReinvstPoliLqDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE L30-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(reinvstPoliLq.getL30DtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO L30-DT-END-EFF-DB
        ws.getReinvstPoliLqDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-L30-DT-DECOR-POLI-LQ = 0
        //               MOVE WS-DATE-X      TO L30-DT-DECOR-POLI-LQ-DB
        //           END-IF.
        if (ws.getIndReinvstPoliLq().getFormattazioneDato() == 0) {
            // COB_CODE: MOVE L30-DT-DECOR-POLI-LQ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(reinvstPoliLq.getL30DtDecorPoliLq().getL30DtDecorPoliLq(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO L30-DT-DECOR-POLI-LQ-DB
            ws.getReinvstPoliLqDb().setDecorPoliLqDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE L30-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getReinvstPoliLqDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO L30-DT-INI-EFF
        reinvstPoliLq.setL30DtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE L30-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getReinvstPoliLqDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO L30-DT-END-EFF
        reinvstPoliLq.setL30DtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-L30-DT-DECOR-POLI-LQ = 0
        //               MOVE WS-DATE-N      TO L30-DT-DECOR-POLI-LQ
        //           END-IF.
        if (ws.getIndReinvstPoliLq().getFormattazioneDato() == 0) {
            // COB_CODE: MOVE L30-DT-DECOR-POLI-LQ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getReinvstPoliLqDb().getDecorPoliLqDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO L30-DT-DECOR-POLI-LQ
            reinvstPoliLq.getL30DtDecorPoliLq().setL30DtDecorPoliLq(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodAge() {
        return reinvstPoliLq.getL30CodAge();
    }

    @Override
    public void setCodAge(int codAge) {
        this.reinvstPoliLq.setL30CodAge(codAge);
    }

    @Override
    public int getCodCan() {
        return reinvstPoliLq.getL30CodCan();
    }

    @Override
    public void setCodCan(int codCan) {
        this.reinvstPoliLq.setL30CodCan(codCan);
    }

    @Override
    public int getCodCompAnia() {
        return reinvstPoliLq.getL30CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.reinvstPoliLq.setL30CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFiscPartIva() {
        return reinvstPoliLq.getL30CodFiscPartIva();
    }

    @Override
    public void setCodFiscPartIva(String codFiscPartIva) {
        this.reinvstPoliLq.setL30CodFiscPartIva(codFiscPartIva);
    }

    @Override
    public int getCodMoviLiq() {
        return reinvstPoliLq.getL30CodMoviLiq();
    }

    @Override
    public void setCodMoviLiq(int codMoviLiq) {
        this.reinvstPoliLq.setL30CodMoviLiq(codMoviLiq);
    }

    @Override
    public String getCodRamo() {
        return reinvstPoliLq.getL30CodRamo();
    }

    @Override
    public void setCodRamo(String codRamo) {
        this.reinvstPoliLq.setL30CodRamo(codRamo);
    }

    @Override
    public int getCodSubAge() {
        return reinvstPoliLq.getL30CodSubAge().getL30CodSubAge();
    }

    @Override
    public void setCodSubAge(int codSubAge) {
        this.reinvstPoliLq.getL30CodSubAge().setL30CodSubAge(codSubAge);
    }

    @Override
    public Integer getCodSubAgeObj() {
        if (ws.getIndReinvstPoliLq().getPrecisioneDato() >= 0) {
            return ((Integer)getCodSubAge());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSubAgeObj(Integer codSubAgeObj) {
        if (codSubAgeObj != null) {
            setCodSubAge(((int)codSubAgeObj));
            ws.getIndReinvstPoliLq().setPrecisioneDato(((short)0));
        }
        else {
            ws.getIndReinvstPoliLq().setPrecisioneDato(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return reinvstPoliLq.getL30DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.reinvstPoliLq.setL30DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return reinvstPoliLq.getL30DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.reinvstPoliLq.setL30DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return reinvstPoliLq.getL30DsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.reinvstPoliLq.setL30DsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return reinvstPoliLq.getL30DsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.reinvstPoliLq.setL30DsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return reinvstPoliLq.getL30DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.reinvstPoliLq.setL30DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return reinvstPoliLq.getL30DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.reinvstPoliLq.setL30DsVer(dsVer);
    }

    @Override
    public String getDtDecorPoliLqDb() {
        return ws.getReinvstPoliLqDb().getDecorPoliLqDb();
    }

    @Override
    public void setDtDecorPoliLqDb(String dtDecorPoliLqDb) {
        this.ws.getReinvstPoliLqDb().setDecorPoliLqDb(dtDecorPoliLqDb);
    }

    @Override
    public String getDtDecorPoliLqDbObj() {
        if (ws.getIndReinvstPoliLq().getFormattazioneDato() >= 0) {
            return getDtDecorPoliLqDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorPoliLqDbObj(String dtDecorPoliLqDbObj) {
        if (dtDecorPoliLqDbObj != null) {
            setDtDecorPoliLqDb(dtDecorPoliLqDbObj);
            ws.getIndReinvstPoliLq().setFormattazioneDato(((short)0));
        }
        else {
            ws.getIndReinvstPoliLq().setFormattazioneDato(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getReinvstPoliLqDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getReinvstPoliLqDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getReinvstPoliLqDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getReinvstPoliLqDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getIbPoli() {
        return reinvstPoliLq.getL30IbPoli();
    }

    @Override
    public void setIbPoli(String ibPoli) {
        this.reinvstPoliLq.setL30IbPoli(ibPoli);
    }

    @Override
    public int getIdMoviChiu() {
        return reinvstPoliLq.getL30IdMoviChiu().getL30IdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.reinvstPoliLq.getL30IdMoviChiu().setL30IdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndReinvstPoliLq().getDescDato() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndReinvstPoliLq().setFlContiguousData(((short)0));
        }
        else {
            ws.getIndReinvstPoliLq().setFlContiguousData(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return reinvstPoliLq.getL30IdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.reinvstPoliLq.setL30IdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdReinvstPoliLq() {
        return reinvstPoliLq.getL30IdReinvstPoliLq();
    }

    @Override
    public void setIdReinvstPoliLq(int idReinvstPoliLq) {
        this.reinvstPoliLq.setL30IdReinvstPoliLq(idReinvstPoliLq);
    }

    @Override
    public AfDecimal getImpRes() {
        return reinvstPoliLq.getL30ImpRes();
    }

    @Override
    public void setImpRes(AfDecimal impRes) {
        this.reinvstPoliLq.setL30ImpRes(impRes.copy());
    }

    @Override
    public AfDecimal getImpTotLiq() {
        return reinvstPoliLq.getL30ImpTotLiq();
    }

    @Override
    public void setImpTotLiq(AfDecimal impTotLiq) {
        this.reinvstPoliLq.setL30ImpTotLiq(impTotLiq.copy());
    }

    @Override
    public long getL30DsRiga() {
        return reinvstPoliLq.getL30DsRiga();
    }

    @Override
    public void setL30DsRiga(long l30DsRiga) {
        this.reinvstPoliLq.setL30DsRiga(l30DsRiga);
    }

    @Override
    public String getPrKeySistEstno() {
        return reinvstPoliLq.getL30PrKeySistEstno();
    }

    @Override
    public void setPrKeySistEstno(String prKeySistEstno) {
        this.reinvstPoliLq.setL30PrKeySistEstno(prKeySistEstno);
    }

    @Override
    public String getPrKeySistEstnoObj() {
        if (ws.getIndReinvstPoliLq().getTipoDato() >= 0) {
            return getPrKeySistEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrKeySistEstnoObj(String prKeySistEstnoObj) {
        if (prKeySistEstnoObj != null) {
            setPrKeySistEstno(prKeySistEstnoObj);
            ws.getIndReinvstPoliLq().setTipoDato(((short)0));
        }
        else {
            ws.getIndReinvstPoliLq().setTipoDato(((short)-1));
        }
    }

    @Override
    public String getSecKeySistEstno() {
        return reinvstPoliLq.getL30SecKeySistEstno();
    }

    @Override
    public void setSecKeySistEstno(String secKeySistEstno) {
        this.reinvstPoliLq.setL30SecKeySistEstno(secKeySistEstno);
    }

    @Override
    public String getSecKeySistEstnoObj() {
        if (ws.getIndReinvstPoliLq().getLunghezzaDato() >= 0) {
            return getSecKeySistEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSecKeySistEstnoObj(String secKeySistEstnoObj) {
        if (secKeySistEstnoObj != null) {
            setSecKeySistEstno(secKeySistEstnoObj);
            ws.getIndReinvstPoliLq().setLunghezzaDato(((short)0));
        }
        else {
            ws.getIndReinvstPoliLq().setLunghezzaDato(((short)-1));
        }
    }

    @Override
    public short getTpInvstLiq() {
        return reinvstPoliLq.getL30TpInvstLiq().getL30TpInvstLiq();
    }

    @Override
    public void setTpInvstLiq(short tpInvstLiq) {
        this.reinvstPoliLq.getL30TpInvstLiq().setL30TpInvstLiq(tpInvstLiq);
    }

    @Override
    public Short getTpInvstLiqObj() {
        if (ws.getIndReinvstPoliLq().getCodDominio() >= 0) {
            return ((Short)getTpInvstLiq());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpInvstLiqObj(Short tpInvstLiqObj) {
        if (tpInvstLiqObj != null) {
            setTpInvstLiq(((short)tpInvstLiqObj));
            ws.getIndReinvstPoliLq().setTypeRecord(((short)0));
        }
        else {
            ws.getIndReinvstPoliLq().setTypeRecord(((short)-1));
        }
    }

    @Override
    public String getTpLiq() {
        return reinvstPoliLq.getL30TpLiq();
    }

    @Override
    public void setTpLiq(String tpLiq) {
        this.reinvstPoliLq.setL30TpLiq(tpLiq);
    }

    @Override
    public String getTpSistEstno() {
        return reinvstPoliLq.getL30TpSistEstno();
    }

    @Override
    public void setTpSistEstno(String tpSistEstno) {
        this.reinvstPoliLq.setL30TpSistEstno(tpSistEstno);
    }
}
