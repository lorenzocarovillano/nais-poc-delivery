package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs1010Data;

/**Original name: LVVS1010<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2011.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS1010
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... ESTRAZIONE CUMULI PREMI ATTIVI
 * **------------------------------------------------------------***</pre>*/
public class Lvvs1010 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs1010Data ws = new Lvvs1010Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS1010
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS1010_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU EX-S1000
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc() && this.idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs1010 getInstance() {
        return ((Lvvs1010)Programs.getInstance(Lvvs1010.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI.
        initIxIndici();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: IF IVVC0213-ID-ADE-FITTZIO
        //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
        //           END-IF.
        if (ivvc0213.getModalitaIdAde().isIvvc0213IdAdeFittzio()) {
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> RECUPERO LE DATA PRIMA DEL MOVIMENTO DI COMUNICAZIONE</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM RECUP-MOVI-COMUN
        //              THRU RECUP-MOVI-COMUN-EX
        recupMoviComun();
        // COB_CODE: MOVE IVVC0213-TIPO-MOVI-ORIG      TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(ivvc0213.getTipoMoviOrigFormatted());
        //--> SE SONO IN FASE DI LIQUIDAZIONE DEVO ESCLUDERE DAL CALCOLO
        //--> IL VALORE CALCOLATO IN FASE DI COMUNICAZIONE
        // COB_CODE: IF (LIQUI-RISPAR-POLIND
        //           OR LIQUI-RISPAR-ADE
        //           OR LIQUI-RPP-REDDITO-PROGR
        //           OR LIQUI-RPP-TAKE-PROFIT) AND COMUN-TROV-SI
        //               MOVE WK-DATA-CPTZ-PREC    TO IDSV0003-DATA-COMPETENZA
        //           END-IF
        if ((ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiRisparAde() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppTakeProfit()) && ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-DT-INI-EFF-TEMP
            ws.setWkDtIniEffTemp(idsv0003.getDataInizioEffetto());
            // COB_CODE: IF IDSV0003-DATA-COMPETENZA IS NUMERIC
            //              MOVE IDSV0003-DATA-COMPETENZA     TO WK-DT-CPTZ-TEMP
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataCompetenza())) {
                // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA     TO WK-DT-CPTZ-TEMP
                ws.setWkDtCptzTemp(idsv0003.getDataCompetenza());
            }
            // COB_CODE: MOVE WK-DATA-EFF-PREC     TO IDSV0003-DATA-INIZIO-EFFETTO
            //                                        IDSV0003-DATA-FINE-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkDataEffPrec());
            idsv0003.setDataFineEffetto(ws.getWkDataEffPrec());
            // COB_CODE: MOVE WK-DATA-CPTZ-PREC    TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkDataCptzPrec());
        }
        // COB_CODE:  PERFORM RECUP-CUM-PRE-ATT-TRA
        //           THRU RECUP-CUM-PRE-ATT-TRA-EX.
        recupCumPreAttTra();
        // COB_CODE: IF (LIQUI-RISPAR-POLIND
        //           OR LIQUI-RISPAR-ADE
        //           OR LIQUI-RPP-REDDITO-PROGR
        //           OR LIQUI-RPP-TAKE-PROFIT) AND COMUN-TROV-SI
        //               MOVE WK-DT-CPTZ-TEMP     TO IDSV0003-DATA-COMPETENZA
        //           END-IF.
        if ((ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiRisparAde() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppTakeProfit()) && ws.getFlagComunTrov().isSi()) {
            // COB_CODE: MOVE WK-DT-INI-EFF-TEMP  TO IDSV0003-DATA-INIZIO-EFFETTO
            idsv0003.setDataInizioEffetto(ws.getWkDtIniEffTemp());
            // COB_CODE: MOVE WK-DT-CPTZ-TEMP     TO IDSV0003-DATA-COMPETENZA
            idsv0003.setDataCompetenza(ws.getWkDtCptzTemp());
        }
    }

    /**Original name: RECUP-MOVI-COMUN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Recupero il movimento di comunicazione
	 * ----------------------------------------------------------------*</pre>*/
    private void recupMoviComun() {
        Ldbs6040 ldbs6040 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-FETCH-FIRST           TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET NO-ULTIMA-LETTURA              TO TRUE
        ws.getFlagUltimaLettura().setNoUltimaLettura();
        // COB_CODE: SET INIT-CUR-MOV                   TO TRUE
        ws.getFlagCurMov().setInitCurMov();
        // COB_CODE: SET COMUN-TROV-NO                  TO TRUE
        ws.getFlagComunTrov().setNo();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR FINE-CUR-MOV
        //                      OR COMUN-TROV-SI
        //              END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || ws.getFlagCurMov().isFineCurMov() || ws.getFlagComunTrov().isSi())) {
            // COB_CODE: INITIALIZE MOVI
            initMovi();
            // COB_CODE: MOVE IVVC0213-ID-POLIZZA            TO MOV-ID-OGG
            ws.getMovi().getMovIdOgg().setMovIdOgg(ivvc0213.getIdPolizza());
            // COB_CODE: MOVE 'PO'                           TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg("PO");
            // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR       TO TRUE
            idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION       TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
            idsv0003.getSqlcode().setSuccessfulSql();
            // COB_CODE: MOVE 'LDBS6040'            TO WK-CALL-PGM
            ws.setWkCallPgm("LDBS6040");
            // COB_CODE: CALL WK-CALL-PGM   USING  IDSV0003 MOVI
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs6040 = Ldbs6040.getInstance();
                ldbs6040.run(idsv0003, ws.getMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE 'LDBS6040'
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
                // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA ");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //             END-EVALUATE
            //           ELSE
            //             END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE: EVALUATE TRUE
                //               WHEN IDSV0003-NOT-FOUND
                //                 END-IF
                //               WHEN IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //               WHEN OTHER
                //                   END-STRING
                //           END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: SET FINE-CUR-MOV TO TRUE
                        ws.getFlagCurMov().setFineCurMov();
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //              END-STRING
                        //           ELSE
                        //              SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
                        //           END-IF
                        if (idsv0003.getOperazione().isFetchFirst()) {
                            // COB_CODE: MOVE 'LDBS6040'     TO IDSV0003-COD-SERVIZIO-BE
                            idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
                            // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
                            //                  IDSV0003-RETURN-CODE ';'
                            //                  IDSV0003-SQLCODE
                            //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        }
                        else {
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
                            idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
                            // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
                            idsv0003.getSqlcode().setSuccessfulSql();
                        }
                        break;

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: IF SI-ULTIMA-LETTURA
                        //              SET FINE-CUR-MOV   TO TRUE
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getFlagUltimaLettura().isSiUltimaLettura()) {
                            // COB_CODE: SET COMUN-TROV-SI        TO TRUE
                            ws.getFlagComunTrov().setSi();
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        else {
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            idsv0003.getOperazione().setFetchNext();
                        }
                        // COB_CODE: MOVE MOV-TP-MOVI      TO WS-MOVIMENTO
                        ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(ws.getMovi().getMovTpMovi().getMovTpMovi(), 5));
                        // COB_CODE: IF COMUN-RISPAR-IND
                        //           OR COMUN-RISPAR-ADE
                        //           OR RPP-TAKE-PROFIT
                        //           OR RPP-REDDITO-PROGRAMMATO
                        //                                        - 1
                        //           ELSE
                        //              END-IF
                        //           END-IF
                        if (ws.getWsMovimento().isComunRisparInd() || ws.getWsMovimento().isComunRisparAde() || ws.getWsMovimento().isRppTakeProfit() || ws.getWsMovimento().isRppRedditoProgrammato()) {
                            // COB_CODE: SET SI-ULTIMA-LETTURA  TO TRUE
                            ws.getFlagUltimaLettura().setSiUltimaLettura();
                            // COB_CODE: MOVE MOV-DT-EFF     TO WK-DATA-EFF-PREC
                            ws.setWkDataEffPrec(ws.getMovi().getMovDtEff());
                            // COB_CODE: COMPUTE WK-DATA-CPTZ-PREC =  MOV-DS-TS-CPTZ
                            //                                     - 1
                            ws.setWkDataCptzPrec(Trunc.toLong(ws.getMovi().getMovDsTsCptz() - 1, 18));
                        }
                        else if (ws.getWsMovimento().isLiquiRisparPolind() || ws.getWsMovimento().isLiquiRisparAde() || ws.getWsMovimento().isLiquiRppRedditoProgr() || ws.getWsMovimento().isLiquiRppTakeProfit()) {
                            // COB_CODE: IF (LIQUI-RISPAR-POLIND
                            //           OR  LIQUI-RISPAR-ADE
                            //           OR LIQUI-RPP-REDDITO-PROGR
                            //           OR  LIQUI-RPP-TAKE-PROFIT)
                            //           SET FINE-CUR-MOV   TO TRUE
                            //           END-IF
                            // COB_CODE: PERFORM CLOSE-MOVI
                            //              THRU CLOSE-MOVI-EX
                            closeMovi();
                            // COB_CODE: SET FINE-CUR-MOV   TO TRUE
                            ws.getFlagCurMov().setFineCurMov();
                        }
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                        // COB_CODE: MOVE WK-CALL-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                        // COB_CODE: STRING 'ERRORE RECUP MOVI COMUN ;'
                        //                  IDSV0003-RETURN-CODE ';'
                        //                  IDSV0003-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE RECUP MOVI COMUN ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
            else {
                // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CHIAMATA LDBS6040 ;'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS6040 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //           OR IDSV0003-SQLCODE = -305
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              SET IDSV0003-INVALID-OPER            TO TRUE
                //           END-IF
                if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
        }
    }

    /**Original name: CLOSE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIUDO IL CURSORE SULLA TABELLA MOVIMENTO LDBS6040
	 * ----------------------------------------------------------------*</pre>*/
    private void closeMovi() {
        Ldbs6040 ldbs6040 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR   TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE SPACES                     TO IDSV0003-BUFFER-WHERE-COND
        idsv0003.setBufferWhereCond("");
        //
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR       TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        //
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'LDBS6040'            TO WK-CALL-PGM
        ws.setWkCallPgm("LDBS6040");
        // COB_CODE: CALL WK-CALL-PGM    USING  IDSV0003 MOVI
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs6040 = Ldbs6040.getInstance();
            ldbs6040.run(idsv0003, ws.getMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE 'LDBS6040'
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
            // COB_CODE: MOVE 'CALL-LDBS6040 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS6040 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSV0003-SUCCESSFUL-SQL
            //                   CONTINUE
            //               WHEN OTHER
            //                   END-IF
            //           END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: MOVE 'LDBS6040'       TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
                    // COB_CODE: STRING 'CHIAMATA LDBS5140 ;'
                    //                  IDSV0003-RETURN-CODE ';'
                    //                  IDSV0003-SQLCODE
                    //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS5140 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    // COB_CODE: IF IDSV0003-NOT-FOUND
                    //           OR IDSV0003-SQLCODE = -305
                    //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    //           ELSE
                    //              SET IDSV0003-INVALID-OPER            TO TRUE
                    //           END-IF
                    if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                    }
                    else {
                        // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                    }
                    break;
            }
        }
        else {
            // COB_CODE: MOVE 'LDBS6040'             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("LDBS6040");
            // COB_CODE: STRING 'CHIAMATA LDBS5140 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS5140 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: RECUP-CUM-PRE-ATT-TRA<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLO LA VARIABILE CUMPREATTRA PER LA FASE DI LIQUIDAZIONE
	 *   RISCATTO PARZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void recupCumPreAttTra() {
        Idbse120 idbse120 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE                   EST-TRCH-DI-GAR.
        initEstTrchDiGar();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC             TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL            TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA        TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-SELECT                    TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-ID-PADRE                  TO TRUE
        idsv0003.getLivelloOperazione().setIdsi0011IdPadre();
        // COB_CODE: MOVE IVVC0213-ID-TRANCHE         TO E12-ID-TRCH-DI-GAR
        ws.getEstTrchDiGar().setE12IdTrchDiGar(ivvc0213.getIdTranche());
        // COB_CODE: MOVE 'IDBSE120'          TO WK-CALL-PGM
        ws.setWkCallPgm("IDBSE120");
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 EST-TRCH-DI-GAR
        //           *
        //                ON EXCEPTION
        //                      SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL
        try {
            idbse120 = Idbse120.getInstance();
            idbse120.run(idsv0003, ws.getEstTrchDiGar());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'RECUP-CUM-PRE-ATT-TRA'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("RECUP-CUM-PRE-ATT-TRA");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //                  END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                         END-IF
            //                       WHEN OTHER
            //                           END-IF
            //                   END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: IF E12-CUM-PRE-ATT-NULL = HIGH-VALUES
                    //               MOVE ZEROES             TO IVVC0213-VAL-IMP-O
                    //           ELSE
                    //               MOVE E12-CUM-PRE-ATT    TO IVVC0213-VAL-IMP-O
                    //           END-IF
                    if (Characters.EQ_HIGH.test(ws.getEstTrchDiGar().getE12CumPreAtt().getE12CumPreAttNullFormatted())) {
                        // COB_CODE: MOVE ZEROES             TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
                    }
                    else {
                        // COB_CODE: MOVE E12-CUM-PRE-ATT    TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getEstTrchDiGar().getE12CumPreAtt().getE12CumPreAtt(), 18, 7));
                    }
                    break;

                default:// COB_CODE: MOVE 'IDBSE120'       TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe("IDBSE120");
                    // COB_CODE: STRING 'LETTURA EST-TRCH-DI-GAR;'
                    //                  IDSV0003-RETURN-CODE ';'
                    //                  IDSV0003-SQLCODE
                    //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "LETTURA EST-TRCH-DI-GAR;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    // COB_CODE: IF IDSV0003-NOT-FOUND
                    //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    //           ELSE
                    //              SET IDSV0003-INVALID-OPER            TO TRUE
                    //           END-IF
                    if (idsv0003.getSqlcode().isNotFound()) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                    }
                    else {
                        // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                        idsv0003.getReturnCode().setInvalidOper();
                    }
                    break;
            }
        }
        else {
            // COB_CODE: MOVE 'IDBSE120'       TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("IDBSE120");
            // COB_CODE: STRING 'LETTURA EST-TRCH-DI-GAR;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "LETTURA EST-TRCH-DI-GAR;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initEstTrchDiGar() {
        ws.getEstTrchDiGar().setE12IdEstTrchDiGar(0);
        ws.getEstTrchDiGar().setE12IdTrchDiGar(0);
        ws.getEstTrchDiGar().setE12IdGar(0);
        ws.getEstTrchDiGar().setE12IdAdes(0);
        ws.getEstTrchDiGar().setE12IdPoli(0);
        ws.getEstTrchDiGar().setE12IdMoviCrz(0);
        ws.getEstTrchDiGar().getE12IdMoviChiu().setE12IdMoviChiu(0);
        ws.getEstTrchDiGar().setE12DtIniEff(0);
        ws.getEstTrchDiGar().setE12DtEndEff(0);
        ws.getEstTrchDiGar().setE12CodCompAnia(0);
        ws.getEstTrchDiGar().getE12DtEmis().setE12DtEmis(0);
        ws.getEstTrchDiGar().getE12CumPreAtt().setE12CumPreAtt(new AfDecimal(0, 15, 3));
        ws.getEstTrchDiGar().getE12AccprePag().setE12AccprePag(new AfDecimal(0, 15, 3));
        ws.getEstTrchDiGar().getE12CumPrstz().setE12CumPrstz(new AfDecimal(0, 15, 3));
        ws.getEstTrchDiGar().setE12DsRiga(0);
        ws.getEstTrchDiGar().setE12DsOperSql(Types.SPACE_CHAR);
        ws.getEstTrchDiGar().setE12DsVer(0);
        ws.getEstTrchDiGar().setE12DsTsIniCptz(0);
        ws.getEstTrchDiGar().setE12DsTsEndCptz(0);
        ws.getEstTrchDiGar().setE12DsUtente("");
        ws.getEstTrchDiGar().setE12DsStatoElab(Types.SPACE_CHAR);
        ws.getEstTrchDiGar().setE12TpTrch("");
        ws.getEstTrchDiGar().setE12CausScon("");
    }
}
