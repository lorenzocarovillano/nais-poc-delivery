package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs3500Data;

/**Original name: LVVS3500<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS3500
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... VARIABILE RISLRDK2TRA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs3500 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3500Data ws = new Lvvs3500Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS3500
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3500_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3500 getInstance() {
        return ((Lvvs3500)Programs.getInstance(Lvvs3500.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformVaryingStmtImpl @source=LVVS3500.cbl:line=108, because the code is unreachable.
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: PERFORM LETTURA-D-CRIST
        //              THRU LETTURA-D-CRIST-EX.
        letturaDCrist();
    }

    /**Original name: LETTURA-D-CRIST<br>
	 * <pre>----------------------------------------------------------------*
	 * --> LETTURA TABELLA DATI CRISTALLIZZATI PER ID TRANCHE
	 * --> PER RECUPERARE IL CAMPO
	 * ----------------------------------------------------------------*</pre>*/
    private void letturaDCrist() {
        Ldbsh580 ldbsh580 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET IDSV0003-SELECT                TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WK-APP-DATA-EFFETTO
        ws.setWkAppDataEffetto(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE 99991230  TO IDSV0003-DATA-INIZIO-EFFETTO
        idsv0003.setDataInizioEffetto(99991230);
        // COB_CODE: INITIALIZE D-CRIST.
        initDCrist();
        // COB_CODE: MOVE IVVC0213-ID-TRANCHE            TO P61-ID-TRCH-DI-GAR
        ws.getdCrist().getP61IdTrchDiGar().setP61IdTrchDiGar(ivvc0213.getIdTranche());
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA     TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-WHERE-CONDITION       TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC         TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL        TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'LDBSH580'            TO WK-PGM-CALL
        ws.setWkPgmCall("LDBSH580");
        // COB_CODE: CALL WK-PGM-CALL   USING  IDSV0003 D-CRIST
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbsh580 = Ldbsh580.getInstance();
            ldbsh580.run(idsv0003, ws.getdCrist());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE 'LDBSH580'
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe("LDBSH580");
            // COB_CODE: MOVE 'CALL-LDBSH580 ERRORE CHIAMATA '
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBSH580 ERRORE CHIAMATA ");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //             END-EVALUATE
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //               WHEN IDSV0003-NOT-FOUND
            //                   TO  IVVC0213-VAL-IMP-O
            //               WHEN IDSV0003-SUCCESSFUL-SQL
            //                 END-IF
            //               WHEN OTHER
            //                   END-STRING
            //           END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: MOVE 0
                    //             TO  IVVC0213-VAL-IMP-O
                    ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
                    break;

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: IF P61-MONT-LRD-END2006-NULL NOT = HIGH-VALUE
                    //                                             AND LOW-VALUE
                    //                                             AND SPACES
                    //                 TO IVVC0213-VAL-IMP-O
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getdCrist().getP61MontLrdEnd2006().getP61MontLrdEnd2006NullFormatted()) && !Characters.EQ_LOW.test(ws.getdCrist().getP61MontLrdEnd2006().getP61MontLrdEnd2006NullFormatted()) && !Characters.EQ_SPACE.test(ws.getdCrist().getP61MontLrdEnd2006().getP61MontLrdEnd2006Null())) {
                        // COB_CODE: MOVE P61-MONT-LRD-END2006
                        //             TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getdCrist().getP61MontLrdEnd2006().getP61MontLrdEnd2006(), 18, 7));
                    }
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM-CALL
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgmCall());
                    // COB_CODE: STRING 'ERRORE RECUP D CRIST ;'
                    //                  IDSV0003-RETURN-CODE ';'
                    //                  IDSV0003-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE RECUP D CRIST ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBSH580 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBSH580 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //           OR IDSV0003-SQLCODE = -305
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
        // COB_CODE: MOVE WK-APP-DATA-EFFETTO
        //             TO IDSV0003-DATA-INIZIO-EFFETTO.
        idsv0003.setDataInizioEffetto(ws.getWkAppDataEffetto());
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initDCrist() {
        ws.getdCrist().setP61IdDCrist(0);
        ws.getdCrist().setP61IdPoli(0);
        ws.getdCrist().setP61CodCompAnia(0);
        ws.getdCrist().setP61IdMoviCrz(0);
        ws.getdCrist().getP61IdMoviChiu().setP61IdMoviChiu(0);
        ws.getdCrist().setP61DtIniEff(0);
        ws.getdCrist().setP61DtEndEff(0);
        ws.getdCrist().setP61CodProd("");
        ws.getdCrist().setP61DtDecor(0);
        ws.getdCrist().setP61DsRiga(0);
        ws.getdCrist().setP61DsOperSql(Types.SPACE_CHAR);
        ws.getdCrist().setP61DsVer(0);
        ws.getdCrist().setP61DsTsIniCptz(0);
        ws.getdCrist().setP61DsTsEndCptz(0);
        ws.getdCrist().setP61DsUtente("");
        ws.getdCrist().setP61DsStatoElab(Types.SPACE_CHAR);
        ws.getdCrist().getP61RisMat31122011().setP61RisMat31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreV31122011().setP61PreV31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreRshV31122011().setP61PreRshV31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61CptRivto31122011().setP61CptRivto31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbVis31122011().setP61ImpbVis31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbIs31122011().setP61ImpbIs31122011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbVisRpP2011().setP61ImpbVisRpP2011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbIsRpP2011().setP61ImpbIsRpP2011(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreV30062014().setP61PreV30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreRshV30062014().setP61PreRshV30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61CptIni30062014().setP61CptIni30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbVis30062014().setP61ImpbVis30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbIs30062014().setP61ImpbIs30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbVisRpP62014().setP61ImpbVisRpP62014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61ImpbIsRpP62014().setP61ImpbIsRpP62014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61RisMat30062014().setP61RisMat30062014(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61IdAdes().setP61IdAdes(0);
        ws.getdCrist().getP61MontLrdEnd2000().setP61MontLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreLrdEnd2000().setP61PreLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61RendtoLrdEnd2000().setP61RendtoLrdEnd2000(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61MontLrdEnd2006().setP61MontLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreLrdEnd2006().setP61PreLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61RendtoLrdEnd2006().setP61RendtoLrdEnd2006(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61MontLrdDal2007().setP61MontLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61PreLrdDal2007().setP61PreLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61RendtoLrdDal2007().setP61RendtoLrdDal2007(new AfDecimal(0, 15, 3));
        ws.getdCrist().getP61IdTrchDiGar().setP61IdTrchDiGar(0);
    }
}
