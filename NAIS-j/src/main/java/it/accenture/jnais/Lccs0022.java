package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lccc0022AreaComunicaz;
import it.accenture.jnais.ws.Lccs0022Data;

/**Original name: LCCS0022<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA  VER 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2008.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... LCCS0022
 *     TIPOLOGIA...... CONTROLLI
 *     PROCESSO....... BLOCCHI
 *     FUNZIONE....... BLOCCHI
 *     DESCRIZIONE.... VERIFICA PRESENZA BLOCCHI
 *     PAGINA WEB..... N/A
 * **------------------------------------------------------------***
 *     DESCRIZIONE SERVIZIO: IL SERVIZIO RICEVE IN INPUT
 *     IL TIPO OGGETTO (POLIZZA O ADESIONE), L'ID POLIZZA (SEMPRE)
 *     ED EVENTUALMENTE L'ID ADESIONE (SE IL TP-OGGETTO E' ADESIONE)
 *     ED IL TIPO STATO BLOCCO (ATTIVO/NON ATTIVO).
 *     IN OUTPUT RESTITUISCE I DATI DELLA TABELLA OGGETTO BLOCCO
 *     (SOLO QUELLI CHE HANNO LA GRAVITA' PRESENTE E CHE HANNO
 *     IL TIPO STATO BLOCCO UGUALE A QUELLO DI INPUT)
 *     E IL FLAG (FL-AMMIS-BLOC) SARA' VALORIZZATO CON 'S' SE
 *     ESISTE ALMENO 1 BLOCCO CON GRAVITA' BLOCCANTE, ALTRIMENTI 'N'
 * **------------------------------------------------------------***</pre>*/
public class Lccs0022 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0022Data ws = new Lccs0022Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: LCCC0022-AREA-COMUNICAZ
    private Lccc0022AreaComunicaz lccc0022AreaComunicaz;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0022_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, Lccc0022AreaComunicaz lccc0022AreaComunicaz) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.lccc0022AreaComunicaz = lccc0022AreaComunicaz;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0022 getInstance() {
        return ((Lccs0022)Programs.getInstance(Lccs0022.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI                                         *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: MOVE ZEROES TO IX-TAB-L11.
        ws.setIxTabL11(((short)0));
        // COB_CODE: MOVE ZEROES TO WK-CONT-ERR-BLOCC.
        ws.setWkContErrBlocc(((short)0));
        //--> CONTROLLI FORMALI
        // COB_CODE: PERFORM S0050-CONTROLLI            THRU EX-S0050.
        s0050Controlli();
    }

    /**Original name: S0050-CONTROLLI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLI FORMALLI                                          *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0050Controlli() {
        // COB_CODE: MOVE LCCC0022-TP-STATO-BLOCCO     TO WK-CTRL-TP-STAT-BLOC.
        ws.setWkCtrlTpStatBloc(lccc0022AreaComunicaz.getTpStatoBlocco().getTpStatoBlocco());
        // COB_CODE: MOVE LCCC0022-TP-OGG              TO WK-CTRL-TP-OGG
        //                                                WS-TP-OGG.
        ws.setWkCtrlTpOgg(lccc0022AreaComunicaz.getTpOgg());
        ws.getWsTpOgg().setWsTpOgg(lccc0022AreaComunicaz.getTpOgg());
        //--> ID POLIZZA
        // COB_CODE:      IF LCCC0022-ID-POLI = ZEROES
        //           *       IL CAMPO $ DEVE ESSERE VALORIZZATO
        //                      THRU EX-S0300
        //                END-IF.
        if (lccc0022AreaComunicaz.getIdPoli() == 0) {
            //       IL CAMPO $ DEVE ESSERE VALORIZZATO
            // COB_CODE: MOVE WK-PGM                    TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S0050-CONTROLLI'         TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S0050-CONTROLLI");
            // COB_CODE: MOVE '005007'                  TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005007");
            // COB_CODE: MOVE 'ID POLIZZA'              TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ID POLIZZA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
        //--> ID ADESIONE
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF ADESIONE
            //              END-IF
            //           END-IF
            if (ws.getWsTpOgg().isAdesione()) {
                // COB_CODE:            IF LCCC0022-ID-ADES = ZEROES
                //           *             IL CAMPO $ DEVE ESSERE VALORIZZATO
                //                            THRU EX-S0300
                //                      END-IF
                if (lccc0022AreaComunicaz.getIdAdes() == 0) {
                    //             IL CAMPO $ DEVE ESSERE VALORIZZATO
                    // COB_CODE: MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S0050-CONTROLLI'   TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S0050-CONTROLLI");
                    // COB_CODE: MOVE '005007'            TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005007");
                    // COB_CODE: MOVE 'ID ADESIONE'       TO IEAI9901-PARAMETRI-ERR
                    ws.getIeai9901Area().setParametriErr("ID ADESIONE");
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
        }
        //--> TIPO OGGETTO (VALORIZZATO)
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF LCCC0022-TP-OGG  = SPACES OR HIGH-VALUE
            //           *          IL CAMPO $ DEVE ESSERE VALORIZZATO
            //                         THRU EX-S0300
            //                   END-IF
            if (Characters.EQ_SPACE.test(lccc0022AreaComunicaz.getTpOgg()) || Characters.EQ_HIGH.test(lccc0022AreaComunicaz.getTpOggFormatted())) {
                //          IL CAMPO $ DEVE ESSERE VALORIZZATO
                // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0050-CONTROLLI");
                // COB_CODE: MOVE '005007'               TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005007");
                // COB_CODE: MOVE 'TIPO OGGETTO'         TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("TIPO OGGETTO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        //--> TIPO OGGETTO (VALIDO)
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF NOT WK-TP-OGG-VALID
            //           *          IL CAMPO $ DEVE ESSERE VALORIZZATO
            //                         THRU EX-S0300
            //                   END-IF
            if (!ws.isWkTpOggValid()) {
                //          IL CAMPO $ DEVE ESSERE VALORIZZATO
                // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0050-CONTROLLI");
                // COB_CODE: MOVE '005018'               TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005018");
                // COB_CODE: MOVE 'TIPO OGGETTO'         TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("TIPO OGGETTO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        //--> TIPO STATO BLOCCO (VALORIZZATO)
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF LCCC0022-TP-STATO-BLOCCO = SPACES OR HIGH-VALUE
            //           *          IL CAMPO $ DEVE ESSERE VALORIZZATO
            //                         THRU EX-S0300
            //                   END-IF
            if (Characters.EQ_SPACE.test(lccc0022AreaComunicaz.getTpStatoBlocco().getTpStatoBlocco()) || Characters.EQ_HIGH.test(lccc0022AreaComunicaz.getTpStatoBlocco().getTpStatoBloccoFormatted())) {
                //          IL CAMPO $ DEVE ESSERE VALORIZZATO
                // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0050-CONTROLLI");
                // COB_CODE: MOVE '005007'               TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005007");
                // COB_CODE: MOVE 'TIPO STATO BLOCCO'    TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("TIPO STATO BLOCCO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        //--> TIPO STATO BLOCCO (VALIDO)
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF NOT WK-TP-STAT-BLOC-VALID
            //           *          IL CAMPO $ NON E' VALIDO
            //                         THRU EX-S0300
            //                   END-IF
            if (!ws.isWkTpStatBlocValid()) {
                //          IL CAMPO $ NON E' VALIDO
                // COB_CODE: MOVE WK-PGM                 TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S0050-CONTROLLI'      TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S0050-CONTROLLI");
                // COB_CODE: MOVE '005018'               TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005018");
                // COB_CODE: MOVE 'TIPO STATO BLOCCO'    TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("TIPO STATO BLOCCO");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> SE IL TP-OGG E' ADESIONE</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF ADESIONE
            //           *-->       LETTURA OGGETTO BLOCCO PER ADESIONE
            //                         THRU EX-S1100
            //                   END-IF
            if (ws.getWsTpOgg().isAdesione()) {
                //-->       LETTURA OGGETTO BLOCCO PER ADESIONE
                // COB_CODE: PERFORM S1100-LETTURA-OGG-BLOCCO-ADE
                //              THRU EX-S1100
                s1100LetturaOggBloccoAde();
            }
        }
        //--> LETTURA OGGETTO BLOCCO PER POLIZZA
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1105
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1105-LETTURA-OGG-BLOCCO-POL
            //              THRU EX-S1105
            s1105LetturaOggBloccoPol();
        }
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *-->    SE CI SONO ERRORI BLOCCANTI
        //                   END-IF
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //-->    SE CI SONO ERRORI BLOCCANTI
            // COB_CODE: IF WK-CONT-ERR-BLOCC > 0
            //              MOVE 'S' TO LCCC0022-FL-PRES-GRAV-BLOC
            //           ELSE
            //              MOVE 'N' TO LCCC0022-FL-PRES-GRAV-BLOC
            //           END-IF
            if (ws.getWkContErrBlocc() > 0) {
                // COB_CODE: MOVE 'S' TO LCCC0022-FL-PRES-GRAV-BLOC
                lccc0022AreaComunicaz.setFlPresGravBlocFormatted("S");
            }
            else {
                // COB_CODE: MOVE 'N' TO LCCC0022-FL-PRES-GRAV-BLOC
                lccc0022AreaComunicaz.setFlPresGravBlocFormatted("N");
            }
        }
    }

    /**Original name: S1100-LETTURA-OGG-BLOCCO-ADE<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TABELLA OGGETTO BLOCCO (ADESIONE)
	 * ----------------------------------------------------------------*
	 * --> VALORIZZAZIONE ID-OGG CON ID ADESIONE</pre>*/
    private void s1100LetturaOggBloccoAde() {
        // COB_CODE: MOVE LCCC0022-ID-ADES      TO L11-ID-OGG.
        ws.getOggBlocco().setL11IdOgg(lccc0022AreaComunicaz.getIdAdes());
        // COB_CODE: MOVE LCCC0022-TP-OGG       TO L11-TP-OGG.
        ws.getOggBlocco().setL11TpOgg(lccc0022AreaComunicaz.getTpOgg());
        //--> PREPARA DATI PER CALL LDBS2410
        // COB_CODE: PERFORM S1110-PREPARA-AREA-LDBS2410
        //              THRU EX-S1110.
        s1110PreparaAreaLdbs2410();
        //--> CALL LDBS2410
        // COB_CODE: PERFORM S1120-CALL-LDBS2410
        //              THRU EX-S1120.
        s1120CallLdbs2410();
        // COB_CODE: MOVE IX-TAB-L11    TO LCCC0022-ELE-MAX-OGG-BLOC.
        lccc0022AreaComunicaz.setEleMaxOggBloc(ws.getIxTabL11());
    }

    /**Original name: S1105-LETTURA-OGG-BLOCCO-POL<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TABELLA OGGETTO BLOCCO (POLIZZA)
	 * ----------------------------------------------------------------*
	 * --> VALORIZZAZIONE ID-OGG CON ID POLIZZA</pre>*/
    private void s1105LetturaOggBloccoPol() {
        // COB_CODE: MOVE LCCC0022-ID-POLI      TO L11-ID-OGG.
        ws.getOggBlocco().setL11IdOgg(lccc0022AreaComunicaz.getIdPoli());
        // COB_CODE: SET  POLIZZA               TO TRUE.
        ws.getWsTpOgg().setPolizza();
        // COB_CODE: MOVE WS-TP-OGG             TO L11-TP-OGG.
        ws.getOggBlocco().setL11TpOgg(ws.getWsTpOgg().getWsTpOgg());
        //--> PREPARA DATI PER CALL LDBS2410
        // COB_CODE: PERFORM S1110-PREPARA-AREA-LDBS2410
        //              THRU EX-S1110.
        s1110PreparaAreaLdbs2410();
        //--> CALL LDBS2410
        // COB_CODE: PERFORM S1120-CALL-LDBS2410
        //              THRU EX-S1120.
        s1120CallLdbs2410();
        // COB_CODE: MOVE IX-TAB-L11    TO LCCC0022-ELE-MAX-OGG-BLOC.
        lccc0022AreaComunicaz.setEleMaxOggBloc(ws.getIxTabL11());
    }

    /**Original name: S1110-PREPARA-AREA-LDBS2410<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA OGGETTO BLOCCO (LDBS2410)
	 * ----------------------------------------------------------------*</pre>*/
    private void s1110PreparaAreaLdbs2410() {
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA     TO L11-COD-COMP-ANIA.
        ws.getOggBlocco().setL11CodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE LCCC0022-TP-STATO-BLOCCO        TO L11-TP-STAT-BLOCCO.
        ws.getOggBlocco().setL11TpStatBlocco(lccc0022AreaComunicaz.getTpStatoBlocco().getTpStatoBlocco());
        // COB_CODE: MOVE 'LDBS2410'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS2410");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE OGG-BLOCCO               TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getOggBlocco().getOggBloccoFormatted());
        // COB_CODE: MOVE OGG-BLOCCO               TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getOggBlocco().getOggBloccoFormatted());
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-FETCH-FIRST      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S1120-CALL-LDBS2410<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA OGGETTO BLOCCO (LDBS2410)
	 * ----------------------------------------------------------------*
	 * --> CICLO PER LA LETTURA SUL DB</pre>*/
    private void s1120CallLdbs2410() {
        ConcatUtil concatUtil = null;
        // COB_CODE:      PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                           OR NOT IDSO0011-SUCCESSFUL-SQL
        //                           OR IDSV0001-ESITO-KO
        //           *-->     EFFETTUO LA LETTURA SUL DB
        //                    END-IF
        //                END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || !ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().isSuccessfulSql() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //-->     EFFETTUO LA LETTURA SUL DB
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            //-->     SE IL RETURN CODE E' TRUE
            // COB_CODE:          IF IDSO0011-SUCCESSFUL-RC
            //                       END-EVALUATE
            //                    ELSE
            //           *-->        ERRORE DISPATCHER
            //                          THRU EX-S0300
            //                    END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                //-->            SE NON HA TROVATO NESSUNA OCCORRENZA
                // COB_CODE:             EVALUATE TRUE
                //           *-->            SE NON HA TROVATO NESSUNA OCCORRENZA
                //                           WHEN IDSO0011-NOT-FOUND
                //                                CONTINUE
                //           *-->            SE CI SONO DELLE OCCORRENZE
                //                           WHEN IDSO0011-SUCCESSFUL-SQL
                //                                SET  IDSI0011-FETCH-NEXT  TO TRUE
                //                       END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: CONTINUE
                    //continue
                    //-->            SE CI SONO DELLE OCCORRENZE
                        break;

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                        //             TO OGG-BLOCCO
                        ws.getOggBlocco().setOggBloccoFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        //-->                 LETTURA SU AMMISSIBILITA' FUNZIONE BLOCCO
                        //                    PER RICERCARE LA GRAVITA'
                        // COB_CODE: PERFORM S1300-LETTURA-AMM-BLOCCO
                        //              THRU EX-S1300
                        s1300LetturaAmmBlocco();
                        //-->                 SE E' PRESENTE LA GRAVITA' PER IL BLOCCO
                        // COB_CODE: IF WK-FL-GRAV-PRESENTE-SI
                        //                 THRU EX-S1400
                        //           END-IF
                        if (ws.getWkFlGravPresente().isSi()) {
                            // COB_CODE: ADD 1 TO IX-TAB-L11
                            ws.setIxTabL11(Trunc.toShort(1 + ws.getIxTabL11(), 4));
                            //-->                    VALORIZZA OUTPUT AREA COMUNICAZIONE
                            // COB_CODE: PERFORM VALORIZZA-OUTPUT-L11
                            //              THRU VALORIZZA-OUTPUT-L11-EX
                            valorizzaOutputL11();
                            // COB_CODE: MOVE L16-GRAV-FUNZ-BLOCCO
                            //             TO LCCC0022-OGB-GRAVITA(IX-TAB-L11)
                            lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setGravita(ws.getAmmbFunzBlocco().getL16GravFunzBlocco());
                            //-->                    LETTURA SU ANA-BLOCCO PER RECUPERARE LA
                            //                       DESCRIZIONE
                            // COB_CODE: PERFORM S1400-LETTURA-ANA-BLOCCO
                            //              THRU EX-S1400
                            s1400LetturaAnaBlocco();
                        }
                        // COB_CODE: PERFORM S1110-PREPARA-AREA-LDBS2410
                        //              THRU EX-S1110
                        s1110PreparaAreaLdbs2410();
                        // COB_CODE: SET  IDSI0011-FETCH-NEXT  TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                        break;

                    default:break;
                }
            }
            else {
                //-->        ERRORE DISPATCHER
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S1120-CALL-LDBS2410'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S1120-CALL-LDBS2410");
                // COB_CODE: MOVE '005016'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING 'LDBS2410'           ';'
                //                  IDSO0011-RETURN-CODE ';'
                //                  IDSO0011-SQLCODE
                //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS2410", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S1300-LETTURA-AMM-BLOCCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TABELLA AMMISSIBILITA' FUNZIONE BLOCCO
	 * ----------------------------------------------------------------*
	 * --> LA LETTURA DELLA TABELLA AMMB_FUNZ_BLOCCO VIENE EFFETTUATA
	 * --> CON 5 MODALITA' DI ACCESSO DIFFERENTI, PARTENDO DA UN
	 * --> ACCESSO CON DETTAGLIO MAGGIORE A QUELLO CON DETTAGLIO MINORE
	 * --> 1) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
	 * -->    TP_MOVI DI CONTESTO CODICE-CANALE LEGATO ALL'UTENZA
	 * -->    D'ACCESSO E TIPO MOVIMENTO RIFERIMENTO UGUALE A TIPO
	 * -->    MOVIMENTO PRESENTE SULLA TABELLA OGG_BLOCCO
	 * --> 2) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
	 * -->    TP_MOVI DI CONTESTO E CODICE-CANALE LEGATO ALL'UTENZA
	 * -->    D'ACCESSO
	 * --> 3) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
	 * -->    TP_MOVI DI CONTESTO E CODICE-CANALE ZERO, OVVERO PER
	 * -->    UTENZA DI DEFAULT
	 * --> 4) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
	 * -->    TP_MOVI ZERO OVVERO TUTTI GENERALIZZATO PER TUTTI I
	 * -->    TIPO MOVIMENTO E CODICE-CANALE ZERO, LEGATO ALL'UTENZA
	 * -->    D'ACCESSO
	 * --> 5) ACCESSO CON COD_BLOCCO LETTO DALLA TABELLA OGG_BLOCCO
	 * -->    TP_MOVI ZERO OVVERO TUTTI GENERALIZZATO PER TUTTI I
	 * -->    TIPO MOVIMENTO E CODICE-CANALE ZERO, OVVERO PER
	 * -->    UTENZA DI DEFAULT</pre>*/
    private void s1300LetturaAmmBlocco() {
        // COB_CODE: SET  WK-FL-GRAV-PRESENTE-NO    TO TRUE.
        ws.getWkFlGravPresente().setNo();
        //--> PREPARA DATI PER CALL LDBS2420
        // COB_CODE: PERFORM S1310-PREPARA-AREA-LDBSF920
        //              THRU EX-S1310.
        s1310PreparaAreaLdbsf920();
        //--> CALL LDBS2420
        // COB_CODE: PERFORM S1320-CALL-LDBSF920
        //              THRU EX-S1320.
        s1320CallLdbsf920();
    }

    /**Original name: S1400-LETTURA-ANA-BLOCCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA TABELLA ANAGRAFICA BLOCCO
	 * ----------------------------------------------------------------*
	 * --> PREPARA DATI PER CALL ANA-BLOCCO</pre>*/
    private void s1400LetturaAnaBlocco() {
        // COB_CODE: PERFORM S1410-PREPARA-AREA-ANA-BLOCCO
        //              THRU EX-S1410.
        s1410PreparaAreaAnaBlocco();
        //--> CALL ANA-BLOCCO
        // COB_CODE: PERFORM S1420-CALL-ANA-BLOCCO
        //              THRU EX-S1420.
        s1420CallAnaBlocco();
    }

    /**Original name: S1410-PREPARA-AREA-ANA-BLOCCO<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA TABELLA ANAGRAFICA BLOCCO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1410PreparaAreaAnaBlocco() {
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA     TO XAB-COD-COMP-ANIA.
        ws.getAnaBlocco().setCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE L11-COD-BLOCCO                  TO XAB-COD-BLOCCO.
        ws.getAnaBlocco().setCodBlocco(ws.getOggBlocco().getL11CodBlocco());
        // COB_CODE: MOVE 'ANA-BLOCCO'             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("ANA-BLOCCO");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE ANA-BLOCCO               TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAnaBlocco().getAnaBloccoFormatted());
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S1420-CALL-ANA-BLOCCO<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA ANAGRAFICA BLOCCO
	 * ----------------------------------------------------------------*
	 * --> EFFETTUO LA LETTURA SUL DB</pre>*/
    private void s1420CallAnaBlocco() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //--> SE IL RETURN CODE E' TRUE
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            // COB_CODE:         EVALUATE TRUE
            //           *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            //                       WHEN IDSO0011-NOT-FOUND
            //                              TO LCCC0022-OGB-DESCRIZIONE(IX-TAB-L11)
            //           *-->        SE CI SONO DELLE OCCORRENZE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                              TO LCCC0022-OGB-DESCRIZIONE(IX-TAB-L11)
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: MOVE SPACES
                    //             TO LCCC0022-OGB-DESCRIZIONE(IX-TAB-L11)
                    lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setDescrizione("");
                    //-->        SE CI SONO DELLE OCCORRENZE
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO ANA-BLOCCO
                    ws.getAnaBlocco().setAnaBloccoFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE XAB-DESC
                    //             TO LCCC0022-OGB-DESCRIZIONE(IX-TAB-L11)
                    lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setDescrizione(ws.getAnaBlocco().getDesc());
                    break;

                default:break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1420-CALL-ANA-BLOCCO'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1420-CALL-ANA-BLOCCO");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'ANA-BLOCCO'         ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "ANA-BLOCCO", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: VALORIZZA-OUTPUT-L11<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZA OUTPUT OGGETTO BLOCCO
	 * ----------------------------------------------------------------*</pre>*/
    private void valorizzaOutputL11() {
        // COB_CODE: MOVE L11-ID-OGG-BLOCCO
        //             TO LCCC0022-OGB-ID-OGG-BLOCCO(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setIdOggBlocco(ws.getOggBlocco().getL11IdOggBlocco());
        // COB_CODE: MOVE L11-ID-OGG-1RIO
        //             TO LCCC0022-OGB-ID-OGG-1RIO(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setIdOgg1rio(ws.getOggBlocco().getL11IdOgg1rio().getL11IdOgg1rio());
        // COB_CODE: MOVE L11-TP-OGG-1RIO
        //             TO LCCC0022-OGB-TP-OGG-1RIO(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setTpOgg1rio(ws.getOggBlocco().getL11TpOgg1rio());
        // COB_CODE: MOVE L11-COD-COMP-ANIA
        //             TO LCCC0022-OGB-COD-COMP-ANIA(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setCodCompAnia(ws.getOggBlocco().getL11CodCompAnia());
        // COB_CODE: MOVE L11-TP-MOVI
        //             TO LCCC0022-OGB-TP-MOVI(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setTpMovi(ws.getOggBlocco().getL11TpMovi());
        // COB_CODE: MOVE L11-TP-OGG
        //             TO LCCC0022-OGB-TP-OGG(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setTpOgg(ws.getOggBlocco().getL11TpOgg());
        // COB_CODE: MOVE L11-ID-OGG
        //             TO LCCC0022-OGB-ID-OGG(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setIdOgg(ws.getOggBlocco().getL11IdOgg());
        // COB_CODE: MOVE L11-DT-EFF
        //             TO LCCC0022-OGB-DT-EFFETTO(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setDtEffetto(ws.getOggBlocco().getL11DtEff());
        // COB_CODE: MOVE L11-TP-STAT-BLOCCO
        //             TO LCCC0022-OGB-TP-STAT-BLOCCO(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setTpStatBlocco(ws.getOggBlocco().getL11TpStatBlocco());
        // COB_CODE: MOVE L11-COD-BLOCCO
        //             TO LCCC0022-OGB-COD-BLOCCO(IX-TAB-L11).
        lccc0022AreaComunicaz.getTabOggBlocco(ws.getIxTabL11()).setCodBlocco(ws.getOggBlocco().getL11CodBlocco());
    }

    /**Original name: S1310-PREPARA-AREA-LDBSF920<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSF920)
	 * ----------------------------------------------------------------*</pre>*/
    private void s1310PreparaAreaLdbsf920() {
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
        ws.getAmmbFunzBlocco().setL16CodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
        ws.getAmmbFunzBlocco().setL16CodBlocco(ws.getOggBlocco().getL11CodBlocco());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO      TO L16-TP-MOVI.
        ws.getAmmbFunzBlocco().setL16TpMovi(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        // COB_CODE: MOVE WCOM-CANALE-VENDITA          TO L16-COD-CAN.
        ws.getAmmbFunzBlocco().setL16CodCan(lccc0001.getDatiDeroghe().getCanaleVendita());
        // COB_CODE: MOVE L11-TP-MOVI                  TO L16-TP-MOVI-RIFTO.
        ws.getAmmbFunzBlocco().getL16TpMoviRifto().setL16TpMoviRifto(ws.getOggBlocco().getL11TpMovi());
        // COB_CODE: MOVE 'LDBSF920'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSF920");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S1320-CALL-LDBSF920<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSF920)
	 * ----------------------------------------------------------------*
	 * --> EFFETTUO LA LETTURA SUL DB</pre>*/
    private void s1320CallLdbsf920() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //--> SE IL RETURN CODE E' TRUE
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            // COB_CODE:         EVALUATE TRUE
            //           *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->            PREPARA DATI PER CALL LDBS2420
            //                              THRU EX-S1160
            //           *-->        SE CI SONO DELLE OCCORRENZE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                            END-IF
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://-->            PREPARA DATI PER CALL LDBS2420
                    // COB_CODE: PERFORM S1150-PREPARA-AREA-LDBSE200-1
                    //              THRU EX-S1150
                    s1150PreparaAreaLdbse2001();
                    //-->            CALL LDBS2420
                    // COB_CODE: PERFORM S1160-CALL-LDBSE200-1
                    //              THRU EX-S1160
                    s1160CallLdbse2001();
                    //-->        SE CI SONO DELLE OCCORRENZE
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO AMMB-FUNZ-BLOCCO
                    ws.getAmmbFunzBlocco().setAmmbFunzBloccoFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE L16-GRAV-FUNZ-BLOCCO
                    //             TO WK-GRAV-FUNZ-BLOCCO
                    ws.getWkGravFunzBlocco().setWkGravFunzBlocco(ws.getAmmbFunzBlocco().getL16GravFunzBlocco());
                    // COB_CODE: IF WK-GRAVITA-AMMESSO
                    //              CONTINUE
                    //           ELSE
                    //              SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isAmmesso()) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                        ws.getWkFlGravPresente().setSi();
                    }
                    // COB_CODE: IF WK-GRAVITA-BLOCCANTE
                    //              ADD 1 TO WK-CONT-ERR-BLOCC
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isBloccante()) {
                        // COB_CODE: ADD 1 TO WK-CONT-ERR-BLOCC
                        ws.setWkContErrBlocc(Trunc.toShort(1 + ws.getWkContErrBlocc(), 4));
                    }
                    break;

                default:break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1320-CALL-LDBSF920'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1320-CALL-LDBSF920");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBSF920'           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBSF920", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1150-PREPARA-AREA-LDBSE200-1<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSE200)
	 * ----------------------------------------------------------------*</pre>*/
    private void s1150PreparaAreaLdbse2001() {
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
        ws.getAmmbFunzBlocco().setL16CodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
        ws.getAmmbFunzBlocco().setL16CodBlocco(ws.getOggBlocco().getL11CodBlocco());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO      TO L16-TP-MOVI.
        ws.getAmmbFunzBlocco().setL16TpMovi(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        // COB_CODE: MOVE WCOM-CANALE-VENDITA          TO L16-COD-CAN.
        ws.getAmmbFunzBlocco().setL16CodCan(lccc0001.getDatiDeroghe().getCanaleVendita());
        // COB_CODE: MOVE 'LDBSE200'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSE200");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S1160-CALL-LDBSE200-1<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSE200) PER CANALE
	 * ----------------------------------------------------------------*
	 * --> EFFETTUO LA LETTURA SUL DB</pre>*/
    private void s1160CallLdbse2001() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //--> SE IL RETURN CODE E' TRUE
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            // COB_CODE:         EVALUATE TRUE
            //           *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->             PREPARA DATI PER CALL LDBSE200
            //                               THRU EX-S1140
            //           *-->        SE CI SONO DELLE OCCORRENZE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                            END-IF
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://-->             PREPARA DATI PER CALL LDBSE200
                    // COB_CODE: PERFORM S1130-PREPARA-AREA-LDBSE200-2
                    //              THRU EX-S1130
                    s1130PreparaAreaLdbse2002();
                    //-->             CALL LDBS2420
                    // COB_CODE: PERFORM S1140-CALL-LDBSE200-2
                    //              THRU EX-S1140
                    s1140CallLdbse2002();
                    //-->        SE CI SONO DELLE OCCORRENZE
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO AMMB-FUNZ-BLOCCO
                    ws.getAmmbFunzBlocco().setAmmbFunzBloccoFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE L16-GRAV-FUNZ-BLOCCO
                    //             TO WK-GRAV-FUNZ-BLOCCO
                    ws.getWkGravFunzBlocco().setWkGravFunzBlocco(ws.getAmmbFunzBlocco().getL16GravFunzBlocco());
                    // COB_CODE: IF WK-GRAVITA-AMMESSO
                    //              CONTINUE
                    //           ELSE
                    //              SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isAmmesso()) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                        ws.getWkFlGravPresente().setSi();
                    }
                    // COB_CODE: IF WK-GRAVITA-BLOCCANTE
                    //              ADD 1 TO WK-CONT-ERR-BLOCC
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isBloccante()) {
                        // COB_CODE: ADD 1 TO WK-CONT-ERR-BLOCC
                        ws.setWkContErrBlocc(Trunc.toShort(1 + ws.getWkContErrBlocc(), 4));
                    }
                    break;

                default:break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1160-CALL-LDBSE200-1'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1160-CALL-LDBSE200-1");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBSE200'           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBSE200", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1170-PREPARA-AREA-LDBSE200-3<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSE200) PER CANALE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1170PreparaAreaLdbse2003() {
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
        ws.getAmmbFunzBlocco().setL16CodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
        ws.getAmmbFunzBlocco().setL16CodBlocco(ws.getOggBlocco().getL11CodBlocco());
        // COB_CODE: MOVE ZEROES                       TO L16-TP-MOVI.
        ws.getAmmbFunzBlocco().setL16TpMovi(0);
        // COB_CODE: MOVE WCOM-CANALE-VENDITA          TO L16-COD-CAN.
        ws.getAmmbFunzBlocco().setL16CodCan(lccc0001.getDatiDeroghe().getCanaleVendita());
        // COB_CODE: MOVE 'LDBSE200'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSE200");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S1180-CALL-LDBSE200-3<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSE200) PER CANALE
	 * ----------------------------------------------------------------*
	 * --> EFFETTUO LA LETTURA SUL DB</pre>*/
    private void s1180CallLdbse2003() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //--> SE IL RETURN CODE E' TRUE
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            // COB_CODE:         EVALUATE TRUE
            //           *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            //                       WHEN IDSO0011-NOT-FOUND
            //                               THRU EX-S1141
            //           *-->        SE CI SONO DELLE OCCORRENZE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                            END-IF
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: PERFORM S1131-PREPARA-AREA-LDBSE200-4
                    //              THRU EX-S1131
                    s1131PreparaAreaLdbse2004();
                    //-->             CALL LDBS2420
                    // COB_CODE: PERFORM S1141-CALL-LDBSE200-4
                    //              THRU EX-S1141
                    s1141CallLdbse2004();
                    //-->        SE CI SONO DELLE OCCORRENZE
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO AMMB-FUNZ-BLOCCO
                    ws.getAmmbFunzBlocco().setAmmbFunzBloccoFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE L16-GRAV-FUNZ-BLOCCO
                    //             TO WK-GRAV-FUNZ-BLOCCO
                    ws.getWkGravFunzBlocco().setWkGravFunzBlocco(ws.getAmmbFunzBlocco().getL16GravFunzBlocco());
                    // COB_CODE: IF WK-GRAVITA-AMMESSO
                    //              CONTINUE
                    //           ELSE
                    //              SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isAmmesso()) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                        ws.getWkFlGravPresente().setSi();
                    }
                    // COB_CODE: IF WK-GRAVITA-BLOCCANTE
                    //              ADD 1 TO WK-CONT-ERR-BLOCC
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isBloccante()) {
                        // COB_CODE: ADD 1 TO WK-CONT-ERR-BLOCC
                        ws.setWkContErrBlocc(Trunc.toShort(1 + ws.getWkContErrBlocc(), 4));
                    }
                    break;

                default:break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1180-CALL-LDBSE200-2'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1180-CALL-LDBSE200-2");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBSE200'           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBSE200", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1130-PREPARA-AREA-LDBSE200-2<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSE200)
	 * ----------------------------------------------------------------*</pre>*/
    private void s1130PreparaAreaLdbse2002() {
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
        ws.getAmmbFunzBlocco().setL16CodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
        ws.getAmmbFunzBlocco().setL16CodBlocco(ws.getOggBlocco().getL11CodBlocco());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO      TO L16-TP-MOVI.
        ws.getAmmbFunzBlocco().setL16TpMovi(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimento());
        // COB_CODE: MOVE ZEROES                       TO L16-COD-CAN.
        ws.getAmmbFunzBlocco().setL16CodCan(0);
        // COB_CODE: MOVE 'LDBSE200'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSE200");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S1140-CALL-LDBSE200-2<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSE200)
	 * ----------------------------------------------------------------*
	 * --> EFFETTUO LA LETTURA SUL DB</pre>*/
    private void s1140CallLdbse2002() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //--> SE IL RETURN CODE E' TRUE
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            // COB_CODE:         EVALUATE TRUE
            //           *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->             PREPARA DATI PER CALL LDBSE200
            //                               THRU EX-S1180
            //           *-->        SE CI SONO DELLE OCCORRENZE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                            END-IF
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://-->             PREPARA DATI PER CALL LDBSE200
                    // COB_CODE: PERFORM S1170-PREPARA-AREA-LDBSE200-3
                    //              THRU EX-S1170
                    s1170PreparaAreaLdbse2003();
                    //-->             CALL LDBSE200
                    // COB_CODE: PERFORM S1180-CALL-LDBSE200-3
                    //              THRU EX-S1180
                    s1180CallLdbse2003();
                    //-->        SE CI SONO DELLE OCCORRENZE
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO AMMB-FUNZ-BLOCCO
                    ws.getAmmbFunzBlocco().setAmmbFunzBloccoFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE L16-GRAV-FUNZ-BLOCCO
                    //             TO WK-GRAV-FUNZ-BLOCCO
                    ws.getWkGravFunzBlocco().setWkGravFunzBlocco(ws.getAmmbFunzBlocco().getL16GravFunzBlocco());
                    // COB_CODE: IF WK-GRAVITA-AMMESSO
                    //              CONTINUE
                    //           ELSE
                    //              SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isAmmesso()) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                        ws.getWkFlGravPresente().setSi();
                    }
                    // COB_CODE: IF WK-GRAVITA-BLOCCANTE
                    //              ADD 1 TO WK-CONT-ERR-BLOCC
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isBloccante()) {
                        // COB_CODE: ADD 1 TO WK-CONT-ERR-BLOCC
                        ws.setWkContErrBlocc(Trunc.toShort(1 + ws.getWkContErrBlocc(), 4));
                    }
                    break;

                default:break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1140-CALL-LDBSE200-2'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1140-CALL-LDBSE200-2");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBS2410'           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS2410", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1131-PREPARA-AREA-LDBSE200-4<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER LETTURA AMMISSIB. FUNZIONE BLOCCO (LDBSE200)
	 * ----------------------------------------------------------------*</pre>*/
    private void s1131PreparaAreaLdbse2004() {
        // COB_CODE: MOVE ZERO                     TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                            IDSI0011-DATA-FINE-EFFETTO
        //                                            IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO L16-COD-COMP-ANIA.
        ws.getAmmbFunzBlocco().setL16CodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE L11-COD-BLOCCO               TO L16-COD-BLOCCO.
        ws.getAmmbFunzBlocco().setL16CodBlocco(ws.getOggBlocco().getL11CodBlocco());
        // COB_CODE: MOVE ZEROES                       TO L16-TP-MOVI.
        ws.getAmmbFunzBlocco().setL16TpMovi(0);
        // COB_CODE: MOVE ZEROES                       TO L16-COD-CAN.
        ws.getAmmbFunzBlocco().setL16CodCan(0);
        // COB_CODE: MOVE 'LDBSE200'               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBSE200");
        // COB_CODE: MOVE SPACES                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: MOVE AMMB-FUNZ-BLOCCO         TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getAmmbFunzBlocco().getAmmbFunzBloccoFormatted());
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S1141-CALL-LDBSE200-4<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA AMMISSIBILITA' FUNZIONE BLOCCO (LDBSE200)
	 * ----------------------------------------------------------------*
	 * --> EFFETTUO LA LETTURA SUL DB</pre>*/
    private void s1141CallLdbse2004() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX
        callDispatcher();
        //--> SE IL RETURN CODE E' TRUE
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            // COB_CODE:         EVALUATE TRUE
            //           *-->        SE NON HA TROVATO NESSUNA OCCORRENZA
            //                       WHEN IDSO0011-NOT-FOUND
            //                            CONTINUE
            //           *-->        SE CI SONO DELLE OCCORRENZE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //                            END-IF
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: CONTINUE
                //continue
                //-->        SE CI SONO DELLE OCCORRENZE
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO AMMB-FUNZ-BLOCCO
                    ws.getAmmbFunzBlocco().setAmmbFunzBloccoFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE L16-GRAV-FUNZ-BLOCCO
                    //             TO WK-GRAV-FUNZ-BLOCCO
                    ws.getWkGravFunzBlocco().setWkGravFunzBlocco(ws.getAmmbFunzBlocco().getL16GravFunzBlocco());
                    // COB_CODE: IF WK-GRAVITA-AMMESSO
                    //              CONTINUE
                    //           ELSE
                    //              SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isAmmesso()) {
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else {
                        // COB_CODE: SET  WK-FL-GRAV-PRESENTE-SI  TO TRUE
                        ws.getWkFlGravPresente().setSi();
                    }
                    // COB_CODE: IF WK-GRAVITA-BLOCCANTE
                    //              ADD 1 TO WK-CONT-ERR-BLOCC
                    //           END-IF
                    if (ws.getWkGravFunzBlocco().isBloccante()) {
                        // COB_CODE: ADD 1 TO WK-CONT-ERR-BLOCC
                        ws.setWkContErrBlocc(Trunc.toShort(1 + ws.getWkContErrBlocc(), 4));
                    }
                    break;

                default:break;
            }
        }
        else {
            //-->    ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1141-CALL-LDBSE200-4'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1141-CALL-LDBSE200-4");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING 'LDBS2410'           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LDBS2410", ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }
}
