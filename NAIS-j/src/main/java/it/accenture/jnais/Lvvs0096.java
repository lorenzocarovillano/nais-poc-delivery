package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Ldbv4011;
import it.accenture.jnais.ws.Lvvs0096Data;

/**Original name: LVVS0096<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0007
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0096 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0096Data ws = new Lvvs0096Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0096_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0096 getInstance() {
        return ((Lvvs0096)Programs.getInstance(Lvvs0096.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE LDBV4011.
        initLdbv4011();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-TIT
        //                      WK-DATA-OUTPUT.
        initAreaIoTit();
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           *         IF IVVC0213-TP-LIVELLO = 'P'
        //                       PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
        //           *         ELSE
        //           *            PERFORM S1255-CALCOLA-DATA2         THRU S1255-EX
        //           *         END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            //         IF IVVC0213-TP-LIVELLO = 'P'
            // COB_CODE: PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
            s1250CalcolaData1();
            //         ELSE
            //            PERFORM S1255-CALCOLA-DATA2         THRU S1255-EX
            //         END-IF
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
            s1260CalcolaDiff();
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1265-CALCOLA-DIFF-1        THRU S1265-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1265-CALCOLA-DIFF-1        THRU S1265-EX
            s1265CalcolaDiff1();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TIT-CONT
        //                TO DTIT-AREA-TIT
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTitCont())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTIT-AREA-TIT
            ws.setDtitAreaTitFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1250-CALCOLA-DATA1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATAULTADEGP
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaData1() {
        Ldbs4010 ldbs4010 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE LDBV4011.
        initLdbv4011();
        // COB_CODE: MOVE DTIT-ID-TIT-CONT(IVVC0213-IX-TABB)
        //                                               TO LDBV4011-ID-TIT-CONT.
        ws.getLdbv4011().setIdTitCont(ws.getDtitTabTitCont(ivvc0213.getIxTabb()).getLccvtit1().getDati().getWtitIdTitCont());
        // COB_CODE: SET IDSV0003-WHERE-CONDITION        TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: MOVE 'LDBS4010'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS4010");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 LDBV4011
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs4010 = Ldbs4010.getInstance();
            ldbs4010.run(idsv0003, ws.getLdbv4011());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS4010 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS4010 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: MOVE IDSV0003-SQLCODE
        //             TO WK-SQLCODE
        ws.setWkSqlcode(idsv0003.getSqlcode().getSqlcode());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              MOVE LDBV4011-DT-END-COP            TO WK-DT-END-COP
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBV4011-DT-INI-COP            TO WK-DT-INI-COP
            ws.setWkDtIniCopFormatted(ws.getLdbv4011().getDtIniCopFormatted());
            // COB_CODE: MOVE LDBV4011-DT-END-COP            TO WK-DT-END-COP
            ws.setWkDtEndCopFormatted(ws.getLdbv4011().getDtEndCopFormatted());
        }
        else if (idsv0003.getSqlcode().getSqlcode() == 100) {
            // COB_CODE: IF IDSV0003-SQLCODE = +100
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER        TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS4010 ;'
            //               IDSV0003-RETURN-CODE ';'
            //               IDSV0003-SQLCODE
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS4010 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S1260-CALCOLA-DIFF<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1260CalcolaDiff() {
        Lccs0010 lccs0010 = null;
        GenericParam formato = null;
        GenericParam ggDiff = null;
        GenericParam codiceRitorno = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A'                               TO FORMATO.
        ws.setFormatoFormatted("A");
        // COB_CODE: MOVE IVVC0213-DATA-EFFETTO             TO DATA-INFERIORE.
        ws.getDataInferiore().setDataInferioreFormatted(ivvc0213.getDataEffettoFormatted());
        // COB_CODE: MOVE WK-DT-END-COP                     TO DATA-SUPERIORE.
        ws.getDataSuperiore().setDataSuperioreFormatted(ws.getWkDtEndCopFormatted());
        // COB_CODE: MOVE 'LCCS0010'                        TO WK-CALL-PGM.
        ws.setWkCallPgm("LCCS0010");
        //
        // COB_CODE: CALL WK-CALL-PGM  USING FORMATO,
        //                                   DATA-INFERIORE,
        //                                   DATA-SUPERIORE,
        //                                   GG-DIFF,
        //                                   CODICE-RITORNO
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL.
        try {
            lccs0010 = Lccs0010.getInstance();
            formato = new GenericParam(MarshalByteExt.chToBuffer(ws.getFormato()));
            ggDiff = new GenericParam(MarshalByteExt.strToBuffer(ws.getGgDiffFormatted(), Lvvs0096Data.Len.GG_DIFF));
            codiceRitorno = new GenericParam(MarshalByteExt.chToBuffer(ws.getCodiceRitorno()));
            lccs0010.run(formato, ws.getDataInferiore(), ws.getDataSuperiore(), ggDiff, codiceRitorno);
            ws.setFormatoFromBuffer(formato.getByteData());
            ws.setGgDiffFromBuffer(ggDiff.getByteData());
            ws.setCodiceRitornoFromBuffer(codiceRitorno.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS1660 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS1660 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF CODICE-RITORNO GREATER ZERO
        //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           END-IF.
        if (Conditions.gt(ws.getCodiceRitorno(), '0')) {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LCCS0010 COD-RIT:'
            //                   CODICE-RITORNO ';'
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LCCS0010 COD-RIT:", String.valueOf(ws.getCodiceRitorno()), ";");
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: S1265-CALCOLA-DIFF-1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1265CalcolaDiff1() {
        Lccs0010 lccs0010 = null;
        GenericParam formato = null;
        GenericParam ggDiff1 = null;
        GenericParam codiceRitorno = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A'                               TO FORMATO.
        ws.setFormatoFormatted("A");
        // COB_CODE: MOVE WK-DT-INI-COP                     TO DATA-INFERIORE.
        ws.getDataInferiore().setDataInferioreFormatted(ws.getWkDtIniCopFormatted());
        // COB_CODE: MOVE WK-DT-END-COP                     TO DATA-SUPERIORE.
        ws.getDataSuperiore().setDataSuperioreFormatted(ws.getWkDtEndCopFormatted());
        // COB_CODE: MOVE 'LCCS0010'                        TO WK-CALL-PGM.
        ws.setWkCallPgm("LCCS0010");
        //
        // COB_CODE: CALL WK-CALL-PGM  USING FORMATO,
        //                                   DATA-INFERIORE,
        //                                   DATA-SUPERIORE,
        //                                   GG-DIFF-1,
        //                                   CODICE-RITORNO
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL.
        try {
            lccs0010 = Lccs0010.getInstance();
            formato = new GenericParam(MarshalByteExt.chToBuffer(ws.getFormato()));
            ggDiff1 = new GenericParam(MarshalByteExt.strToBuffer(ws.getGgDiff1Formatted(), Lvvs0096Data.Len.GG_DIFF1));
            codiceRitorno = new GenericParam(MarshalByteExt.chToBuffer(ws.getCodiceRitorno()));
            lccs0010.run(formato, ws.getDataInferiore(), ws.getDataSuperiore(), ggDiff1, codiceRitorno);
            ws.setFormatoFromBuffer(formato.getByteData());
            ws.setGgDiff1FromBuffer(ggDiff1.getByteData());
            ws.setCodiceRitornoFromBuffer(codiceRitorno.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS1660 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS1660 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF CODICE-RITORNO = ZERO
        //              COMPUTE IVVC0213-VAL-IMP-O = GG-DIFF / GG-DIFF-1
        //           ELSE
        //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           END-IF.
        if (Conditions.eq(ws.getCodiceRitorno(), '0')) {
            // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O = GG-DIFF / GG-DIFF-1
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(new AfDecimal(((((double)(ws.getGgDiff()))) / ws.getGgDiff1()), 12, 7), 18, 7));
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LCCS0010 COD-RIT:'
            //                   CODICE-RITORNO ';'
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LCCS0010 COD-RIT:", String.valueOf(ws.getCodiceRitorno()), ";");
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabTit(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initLdbv4011() {
        ws.getLdbv4011().setIdTitCont(0);
        ws.getLdbv4011().setDtIniCopFormatted("00000000");
        ws.getLdbv4011().setDtEndCopFormatted("00000000");
    }

    public void initAreaIoTit() {
        ws.setDtitEleTitMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0096Data.DTIT_TAB_TIT_CONT_MAXOCCURS; idx0++) {
            ws.getDtitTabTitCont(idx0).getLccvtit1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDtitTabTitCont(idx0).getLccvtit1().setIdPtf(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitIdTitCont(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitIdOgg(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpOgg("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitIbRich("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitIdMoviCrz(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitIdMoviChiu().setWtitIdMoviChiu(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDtIniEff(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDtEndEff(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitCodCompAnia(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpTit("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitProgTit().setWtitProgTit(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpPreTit("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpStatTit("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtIniCop().setWtitDtIniCop(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtEndCop().setWtitDtEndCop(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpPag().setWtitImpPag(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlSoll(Types.SPACE_CHAR);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitFraz().setWtitFraz(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtApplzMora().setWtitDtApplzMora(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlMora(Types.SPACE_CHAR);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitIdRappRete().setWtitIdRappRete(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitIdRappAna().setWtitIdRappAna(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitCodDvs("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtEmisTit().setWtitDtEmisTit(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtEsiTit().setWtitDtEsiTit(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotPreNet().setWtitTotPreNet(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrFraz().setWtitTotIntrFraz(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrMora().setWtitTotIntrMora(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrPrest().setWtitTotIntrPrest(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrRetdt().setWtitTotIntrRetdt(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotIntrRiat().setWtitTotIntrRiat(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotDir().setWtitTotDir(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSpeMed().setWtitTotSpeMed(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotTax().setWtitTotTax(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprSan().setWtitTotSoprSan(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprTec().setWtitTotSoprTec(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprSpo().setWtitTotSoprSpo(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprProf().setWtitTotSoprProf(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSoprAlt().setWtitTotSoprAlt(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotPreTot().setWtitTotPreTot(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotPrePpIas().setWtitTotPrePpIas(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCarAcq().setWtitTotCarAcq(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCarGest().setWtitTotCarGest(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCarInc().setWtitTotCarInc(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotPreSoloRsh().setWtitTotPreSoloRsh(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvAcq1aa().setWtitTotProvAcq1aa(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvAcq2aa().setWtitTotProvAcq2aa(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvRicor().setWtitTotProvRicor(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvInc().setWtitTotProvInc(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotProvDaRec().setWtitTotProvDaRec(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpAz().setWtitImpAz(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpAder().setWtitImpAder(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpTfr().setWtitImpTfr(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpVolo().setWtitImpVolo(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotManfeeAntic().setWtitTotManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotManfeeRicor().setWtitTotManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotManfeeRec().setWtitTotManfeeRec(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpMezPagAdd("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitEstrCntCorrAdd("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtVlt().setWtitDtVlt(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlForzDtVlt(Types.SPACE_CHAR);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtCambioVlt().setWtitDtCambioVlt(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotSpeAge().setWtitTotSpeAge(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCarIas().setWtitTotCarIas(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitNumRatAccorpate().setWtitNumRatAccorpate(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsRiga(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsOperSql(Types.SPACE_CHAR);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsVer(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsTsIniCptz(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsTsEndCptz(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsUtente("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitDsStatoElab(Types.SPACE_CHAR);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlTitDaReinvst(Types.SPACE_CHAR);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtRichAddRid().setWtitDtRichAddRid(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpEsiRid("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitCodIban("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpTrasfe().setWtitImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitImpTfrStrc().setWtitImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitDtCertFisc().setWtitDtCertFisc(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTpCausStor().setWtitTpCausStor(0);
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpCausDispStor("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpTitMigraz("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotAcqExp().setWtitTotAcqExp(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotRemunAss().setWtitTotRemunAss(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCommisInter().setWtitTotCommisInter(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitTpCausRimb("");
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().getWtitTotCnbtAntirac().setWtitTotCnbtAntirac(new AfDecimal(0, 15, 3));
            ws.getDtitTabTitCont(idx0).getLccvtit1().getDati().setWtitFlIncAutogen(Types.SPACE_CHAR);
        }
    }
}
