package it.accenture.jnais;

import com.bphx.ctu.af.core.buffer.BasicBytesClass;
import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.DbService;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.pointer.IPointerManager;
import com.bphx.ctu.af.core.program.DynamicCall;
import com.bphx.ctu.af.core.program.DynamicCallException;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.Session;
import com.bphx.ctu.af.core.TerminateException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.io.file.FileAccessStatus;
import com.bphx.ctu.af.io.file.FileBufferedDAO;
import com.bphx.ctu.af.io.file.FileRecordBuffer;
import com.bphx.ctu.af.io.file.OpenMode;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.display.DisplayUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.inspect.InspectPattern;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.bphx.ctu.af.util.types.Booleans;
import com.modernsystems.batch.DdCard;
import com.modernsystems.ctu.core.impl.BatchProgram;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.SqlFunctions;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Iabi0011Area;
import it.accenture.jnais.copy.Iabv0006CustomCounters;
import it.accenture.jnais.copy.Iabv0007;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Lrgc0041;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.copy.ValoriCampiLogErroreReport;
import it.accenture.jnais.copy.WkLogErrore;
import it.accenture.jnais.ws.BtcBatchType;
import it.accenture.jnais.ws.BtcExeMessage;
import it.accenture.jnais.ws.BtcJobSchedule;
import it.accenture.jnais.ws.enums.FileStatus1;
import it.accenture.jnais.ws.enums.FlagGestioneLanciBus;
import it.accenture.jnais.ws.enums.Iabi0011LengthDataGates;
import it.accenture.jnais.ws.enums.Iabi0011TemporaryTable;
import it.accenture.jnais.ws.enums.Idsv0001LivelloDebug;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.enums.WkFlagGuideType;
import it.accenture.jnais.ws.Iabv0003;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Lrgm0380Data;
import it.accenture.jnais.ws.occurs.Iabv0006EleErrori;
import it.accenture.jnais.ws.occurs.WkAppoBlobData;
import it.accenture.jnais.ws.ptr.Idsv8888StrPerformanceDbg;
import it.accenture.jnais.ws.ptr.Ijccmq04VarAmbiente;
import it.accenture.jnais.ws.redefines.BjsDtEnd;
import it.accenture.jnais.ws.redefines.BjsTpMovi;
import it.accenture.jnais.ws.redefines.Iabv0003BlobDataArray;
import it.accenture.jnais.ws.RichIabs0900;
import it.accenture.jnais.ws.WkAppoDataJob;
import javax.inject.Inject;
import static com.bphx.ctu.af.lang.AfSystem.rTrim;
import static com.bphx.ctu.af.util.StringUtil.*;

/**Original name: LRGM0380<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *                                                        ********
 * *                   BATCH EXECUTOR                       ********
 * *                                                        ********
 * *****************************************************************
 * AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.  2014.
 * DATE-COMPILED.
 * *****************************************************************
 *     PROGRAAMMA .... LRGM0380
 *     FUNZIONE ...... BATCH EXECUTOR
 * *****************************************************************
 *  GAP BNL - ESTRATTO CONTO (11-Reporting Tecnico Gestionale)
 *  Batch Diagnostica E/C
 * *****************************************************************
 * ---> FILE PARAM DI INPUT
 * ---> FILE REPORT01 DI OUTPUT
 * *****************************************************************
 * --          GESTIONE GUIDA DA SEQUENZIALE
 * *****************************************************************
 *  da inserire in caso di gestione guida da sequenziale
 * *****************************************************************
 * ---> FILE INPUT GUIDE</pre>*/
public class Lrgm0380 extends BatchProgram {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    @Inject
    private IPointerManager pointerManager;
    public FileRecordBuffer pbtcexecTo = new FileRecordBuffer(Len.PARAM_REC);
    public FileBufferedDAO pbtcexecDAO = new FileBufferedDAO(new FileAccessStatus(), "PBTCEXEC", Len.PARAM_REC);
    public FileRecordBuffer report01To = new FileRecordBuffer(Len.REPORT01_REC);
    public FileBufferedDAO report01DAO = new FileBufferedDAO(new FileAccessStatus(), "REPORT01", Len.REPORT01_REC);
    public FileRecordBuffer seqguideTo = new FileRecordBuffer(Len.SEQGUIDE_REC);
    public FileBufferedDAO seqguideDAO = new FileBufferedDAO(new FileAccessStatus(), "SEQGUIDE", Len.SEQGUIDE_REC);
    //Original name: WORKING-STORAGE
    private Lrgm0380Data ws = new Lrgm0380Data();

    //==== METHODS ====
    /**Original name: PROGRAM_LRGM0380_FIRST_SENTENCES<br>*/
    public long execute() {
        // COB_CODE: MOVE 'PROCEDURE DIVISION'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("PROCEDURE DIVISION");
        // COB_CODE: PERFORM A000-OPERAZ-INIZ      THRU A000-EX.
        a000OperazIniz();
        // COB_CODE: IF WK-ERRORE-NO
        //              PERFORM B000-ELABORA-MACRO THRU B000-EX
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM B000-ELABORA-MACRO THRU B000-EX
            b000ElaboraMacro();
        }
        // COB_CODE: PERFORM Z000-OPERAZ-FINALI    THRU Z000-OPERAZ-FINALI-FINE.
        z000OperazFinali();
        // COB_CODE: STOP RUN.
        throw new TerminateException();
    }

    public static Lrgm0380 getInstance() {
        return ((Lrgm0380)Programs.getInstance(Lrgm0380.class));
    }

    /**Original name: A001-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONI INIZIALI                                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void a001OperazioniIniziali() {
        // COB_CODE: MOVE 'A001-OPERAZ-INIZIALI'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A001-OPERAZ-INIZIALI");
        // COB_CODE: INITIALIZE                           IX-INDICI.
        initIxIndici();
        // COB_CODE: SET PRIMO-LANCIO-SI TO TRUE.
        ws.getFlagLancio().setSi();
        //--> VALORIZZO AREA PER LA GESTIONE PERSONALIZZATA DEI CONTATORI
        // COB_CODE: INITIALIZE IABV0006-CUSTOM-COUNTERS.
        initCustomCounters();
        // COB_CODE: MOVE 10                     TO IABV0006-LIM-CUSTOM-COUNT-MAX
        ws.getIabv0006().getCustomCounters().setLimCustomCountMax(((short)10));
        // COB_CODE: MOVE 10                     TO IABV0006-MAX-ELE-CUSTOM-COUNT
        ws.getIabv0006().getCustomCounters().setMaxEleCustomCount(((short)10));
        // COB_CODE: SET  IABV0006-COUNTER-STD-YES TO TRUE
        ws.getIabv0006().getCustomCounters().getCounterStd().setIabv0006CounterStdYes();
        // COB_CODE: SET  IABV0006-GUIDE-ROWS-READ-YES TO TRUE
        ws.getIabv0006().getCustomCounters().getGuideRowsRead().setIabv0006GuideRowsReadYes();
        //--> CONTATORE 1
        // COB_CODE: SET  IABV0006-COUNTER-DESC(REC-ELABORATI)
        //             TO TRUE
        ws.getIabv0006().getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecElaborati()).getDescType().setIabv0006CounterDesc();
        // COB_CODE: MOVE DESC-CTR-1
        //             TO IABV0006-CUSTOM-COUNT-DESC(REC-ELABORATI)
        ws.getIabv0006().getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecElaborati()).setCustomCountDesc(ws.getWkAreaContatori().getDescCtr1());
        // COB_CODE: MOVE ZEROES
        //             TO IABV0006-CUSTOM-COUNT(REC-ELABORATI).
        ws.getIabv0006().getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecElaborati()).setIabv0006CustomCount(0);
        //--> CONTATORE 2
        // COB_CODE: SET  IABV0006-COUNTER-DESC(REC-SCARTATI)
        //             TO TRUE
        ws.getIabv0006().getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecScartati()).getDescType().setIabv0006CounterDesc();
        // COB_CODE: MOVE DESC-CTR-2
        //             TO IABV0006-CUSTOM-COUNT-DESC(REC-SCARTATI)
        ws.getIabv0006().getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecScartati()).setCustomCountDesc(ws.getWkAreaContatori().getDescCtr2());
        // COB_CODE: MOVE ZEROES
        //             TO IABV0006-CUSTOM-COUNT(REC-SCARTATI).
        ws.getIabv0006().getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecScartati()).setIabv0006CustomCount(0);
        //--> CONTATORE 3
        // COB_CODE: SET  IABV0006-COUNTER-DESC(REC-PERC)
        //             TO TRUE
        ws.getIabv0006().getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecPerc()).getDescType().setIabv0006CounterDesc();
        // COB_CODE: MOVE DESC-CTR-3
        //             TO IABV0006-CUSTOM-COUNT-DESC(REC-PERC)
        ws.getIabv0006().getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecPerc()).setCustomCountDesc(ws.getWkAreaContatori().getDescCtr3());
        // COB_CODE: MOVE ZEROES
        //             TO IABV0006-CUSTOM-COUNT(REC-PERC).
        ws.getIabv0006().getCustomCounters().getEleErrori(ws.getWkAreaContatori().getRecPerc()).setIabv0006CustomCount(0);
    }

    /**Original name: A101-DISPLAY-INIZIO-ELABORA<br>
	 * <pre>----------------------------------------------------------------*
	 *  DISPLAY INIZIO ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void a101DisplayInizioElabora() {
        // COB_CODE: DISPLAY '***********************************************'.
        DisplayUtil.sysout.write("***********************************************");
        // COB_CODE: DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
        DisplayUtil.sysout.write("* NOME PROGRAMMA    = ", ws.getWkPgmFormatted());
        // COB_CODE: DISPLAY '* DESCRIZIONE       = Inizio elaborazione di '
        DisplayUtil.sysout.write("* DESCRIZIONE       = Inizio elaborazione di ");
        // COB_CODE: DISPLAY '* BATCH EXECUTOR '.
        DisplayUtil.sysout.write("* BATCH EXECUTOR ");
        // COB_CODE: DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
        DisplayUtil.sysout.write("* DATA ELABORAZIONE = ", ws.getIabv0007().getWkTimestamp26().getWkDataSistemaFormatted());
        // COB_CODE: DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
        DisplayUtil.sysout.write("* ORA  ELABORAZIONE = ", ws.getIabv0007().getWkTimestamp26().getWkOraSistemaFormatted());
        // COB_CODE: DISPLAY '************************************************'.
        DisplayUtil.sysout.write("************************************************");
        // COB_CODE: DISPLAY ALL SPACES.
        DisplayUtil.sysout.write(String.valueOf(Types.SPACE_CHAR));
    }

    /**Original name: A102-DISPLAY-FINE-ELABORA<br>
	 * <pre>----------------------------------------------------------------*
	 *  DISPLAY FINE ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void a102DisplayFineElabora() {
        // COB_CODE: DISPLAY '***********************************************'.
        DisplayUtil.sysout.write("***********************************************");
        // COB_CODE: DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
        DisplayUtil.sysout.write("* NOME PROGRAMMA    = ", ws.getWkPgmFormatted());
        // COB_CODE: DISPLAY '* DESCRIZIONE       = Fine elaborazione di '
        DisplayUtil.sysout.write("* DESCRIZIONE       = Fine elaborazione di ");
        // COB_CODE: DISPLAY '* BATCH EXECUTOR '.
        DisplayUtil.sysout.write("* BATCH EXECUTOR ");
        // COB_CODE: DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
        DisplayUtil.sysout.write("* DATA ELABORAZIONE = ", ws.getIabv0007().getWkTimestamp26().getWkDataSistemaFormatted());
        // COB_CODE: DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
        DisplayUtil.sysout.write("* ORA  ELABORAZIONE = ", ws.getIabv0007().getWkTimestamp26().getWkOraSistemaFormatted());
        // COB_CODE: DISPLAY '************************************************'.
        DisplayUtil.sysout.write("************************************************");
        // COB_CODE: DISPLAY ALL SPACES.
        DisplayUtil.sysout.write(String.valueOf(Types.SPACE_CHAR));
    }

    /**Original name: L500-PRE-BUSINESS<br>
	 * <pre>----------------------------------------------------------------*
	 *  PRE BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void l500PreBusiness() {
        // COB_CODE: MOVE 'L500-PRE-BUSINESS'                TO WK-LABEL.
        ws.getIabv0007().setWkLabel("L500-PRE-BUSINESS");
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND
        //             TO BUFFER-WHERE-CONDITION
        ws.getBufferWhereCondition().setBufferWhereConditionFormatted(ws.getIabv0007().getIdsv0003().getBufferWhereCondFormatted());
        // COB_CODE: IF WK-FINE-ALIMENTAZIONE-YES
        //              CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().isFlagFineAlimentazione()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE SEQGUIDE-REC                    TO WRIN-REC-FILEIN
            ws.setWrinRecFileinFormatted(seqguideTo.getVariableFormatted());
            // COB_CODE: IF PRIMA-VOLTA-SUSP-YES
            //                TO WK-APPO-BLOB-DATA-REC(IX-IND)
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().isWkPrimaVoltaSuspend()) {
                // COB_CODE: SET PRIMA-VOLTA-SUSP-NO         TO TRUE
                ws.getIabv0007().setWkPrimaVoltaSuspend(false);
                // COB_CODE: SET GESTIONE-SUSPEND            TO TRUE
                ws.getIabv0007().getFlagGestioneLanciBus().setGestioneSuspend();
                // COB_CODE: SET WK-LANCIA-BUSINESS-NO       TO TRUE
                ws.getIabv0007().setFlagLanciaBusiness(false);
                // COB_CODE: ADD  1                          TO IX-IND
                ws.setIxInd(Trunc.toShort(1 + ws.getIxInd(), 4));
                // COB_CODE: MOVE IX-IND                     TO WK-APPO-ELE-MAX
                ws.getWkAppoDataJob().setEleMax(TruncAbs.toShort(ws.getIxInd(), 3));
                // COB_CODE: MOVE WRIN-REC-FILEIN
                //             TO WK-APPO-BLOB-DATA-REC(IX-IND)
                ws.getWkAppoDataJob().getBlobData(ws.getIxInd()).setWkAppoBlobDataRec(ws.getWrinRecFileinFormatted());
            }
            else if (Conditions.eq(ws.getWrinRecOutput().getWrinTpRecord(), "0001")) {
                // COB_CODE:            IF WRIN-TP-RECORD = '0001'
                //                         END-IF
                //           *             WRIN-TP-RECORD = '0006' OR
                //           *             WRIN-TP-RECORD = '0007' OR
                //           *             WRIN-TP-RECORD = '0008' OR
                //           *             WRIN-TP-RECORD = '0009' OR
                //           *             WRIN-TP-RECORD = '0010'
                //                      ELSE
                //                           TO WK-APPO-BLOB-DATA-REC(IX-IND)
                //                      END-IF
                // COB_CODE: IF PRIMO-LANCIO-SI
                //              SET  PRIMO-LANCIO-NO         TO TRUE
                //           ELSE
                //              SET WK-LANCIA-BUSINESS-YES   TO TRUE
                //           END-IF
                if (ws.getFlagLancio().isSi()) {
                    // COB_CODE: SET  PRIMO-LANCIO-NO         TO TRUE
                    ws.getFlagLancio().setNo();
                }
                else {
                    // COB_CODE: SET WK-LANCIA-BUSINESS-YES   TO TRUE
                    ws.getIabv0007().setFlagLanciaBusiness(true);
                }
                //             WRIN-TP-RECORD = '0006' OR
                //             WRIN-TP-RECORD = '0007' OR
                //             WRIN-TP-RECORD = '0008' OR
                //             WRIN-TP-RECORD = '0009' OR
                //             WRIN-TP-RECORD = '0010'
            }
            else {
                // COB_CODE: SET WK-LANCIA-BUSINESS-NO    TO TRUE
                ws.getIabv0007().setFlagLanciaBusiness(false);
                // COB_CODE: ADD  1                       TO IX-IND
                ws.setIxInd(Trunc.toShort(1 + ws.getIxInd(), 4));
                // COB_CODE: MOVE IX-IND                  TO WK-APPO-ELE-MAX
                ws.getWkAppoDataJob().setEleMax(TruncAbs.toShort(ws.getIxInd(), 3));
                // COB_CODE: MOVE WRIN-REC-FILEIN
                //             TO WK-APPO-BLOB-DATA-REC(IX-IND)
                ws.getWkAppoDataJob().getBlobData(ws.getIxInd()).setWkAppoBlobDataRec(ws.getWrinRecFileinFormatted());
            }
        }
    }

    /**Original name: L800-ESEGUI-CALL-BUS<br>
	 * <pre>----------------------------------------------------------------*
	 *  POST BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void l800EseguiCallBus() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'L800-ESEGUI-CALL-BUS'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("L800-ESEGUI-CALL-BUS");
        //                                         AREA-CONTATORI
        // COB_CODE:      CALL WK-PGM-BUSINESS           USING AREA-IDSV0001
        //                                                     WCOM-IO-STATI
        //                                                     IABV0006
        //                                                     WK-APPO-DATA-JOB
        //           *                                         AREA-CONTATORI
        //                ON EXCEPTION
        //                   PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //                END-CALL.
        try {
            DynamicCall.invoke(ws.getIabv0007().getWkPgmBusiness(), ws.getIabv0007().getAreaIdsv0001(), ws.getLccc0001(), ws.getIabv0006(), ws.getWkAppoDataJob());
        }
        catch (DynamicCallException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                   DELIMITED BY SIZE
            //                   WK-PGM-BUSINESS
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getWkPgmBusinessFormatted());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: SET WK-ERRORE-BLOCCANTE     TO TRUE
            ws.getIabv0007().getWkTipoErrore().setBloccante();
            // COB_CODE: MOVE 4                      TO LOR-ID-GRAVITA-ERRORE
            ws.getLogErrore().setLorIdGravitaErrore(4);
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
        // COB_CODE: INITIALIZE                WK-APPO-DATA-JOB.
        initWkAppoDataJob();
        // COB_CODE: MOVE 1                 TO IX-IND.
        ws.setIxInd(((short)1));
        // COB_CODE: MOVE IX-IND            TO WK-APPO-ELE-MAX.
        ws.getWkAppoDataJob().setEleMax(TruncAbs.toShort(ws.getIxInd(), 3));
        // COB_CODE: MOVE WRIN-REC-FILEIN   TO WK-APPO-BLOB-DATA-REC(IX-IND).
        ws.getWkAppoDataJob().getBlobData(ws.getIxInd()).setWkAppoBlobDataRec(ws.getWrinRecFileinFormatted());
        // COB_CODE: IF WRIN-TP-RECORD = '0001'
        //               INITIALIZE WK-AP-GAR-FND
        //           END-IF.
        if (Conditions.eq(ws.getWrinRecOutput().getWrinTpRecord(), "0001")) {
            // COB_CODE: INITIALIZE WK-AP-GAR-FND
            initWkApGarFnd();
        }
    }

    /**Original name: A000-OPERAZ-INIZ<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTIENE STATEMENTS PER LA FASE MACRO DEL BATCH EXECUTOR
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *  OPERAZIONI INIZIALI                                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void a000OperazIniz() {
        // COB_CODE: MOVE 'A000-OPERAZ-INIZ'          TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A000-OPERAZ-INIZ");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM A050-INITIALIZE          THRU A050-EX.
        a050Initialize();
        // COB_CODE: PERFORM T000-ACCEPT-TIMESTAMP    THRU T000-EX.
        t000AcceptTimestamp();
        // COB_CODE: PERFORM A001-OPERAZIONI-INIZIALI THRU A001-EX.
        a001OperazioniIniziali();
    }

    /**Original name: A050-INITIALIZE<br>
	 * <pre>----------------------------------------------------------------*
	 *  INIZIALIZZA AREE DI WORKING                                    *
	 * ----------------------------------------------------------------*</pre>*/
    private void a050Initialize() {
        // COB_CODE: MOVE 'A050-INITIALIZE'           TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A050-INITIALIZE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: INITIALIZE FS-PARA
        //                      TABELLA-STATI-BATCH
        //                      TABELLA-STATI-JOB
        //                      IABV0002
        //                      CNT-NUM-CONTR-LETT
        //                      CNT-NUM-CONTR-ELAB
        //                      CNT-NUM-OPERAZ-LETTE
        //                      WK-STATI-SOSPESI-STR
        //                      WK-COMODO
        //                      VALORI-CAMPI-LOG-ERRORE-REPORT
        //                      IABI0011
        //                      W-AREA-PER-CHIAMATE-WSMQ
        //                      IDSV0003.
        ws.getIabv0007().setFsPara("");
        initTabellaStatiBatch();
        initTabellaStatiJob();
        initIabv0002();
        ws.getIabv0007().setCntNumContrLettFormatted("000000000");
        ws.getIabv0007().setCntNumContrElabFormatted("000000000");
        ws.getIabv0007().setCntNumOperazLetteFormatted("000000000");
        initWkStatiSospesiStr();
        initWkComodo();
        initValoriCampiLogErroreReport();
        initIabi0011();
        initWAreaPerChiamateWsmq();
        initIdsv0003();
        // COB_CODE: MOVE HIGH-VALUE                   TO BTC-PARALLELISM
        ws.getBtcParallelism().initBtcParallelismHighValues();
        // COB_CODE: SET IDSV0001-BATCH-INFR           TO TRUE
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().getModalitaEsecutiva().setIdsv0001BatchInfr();
        // COB_CODE: SET WK-BAT-COLLECTION-KEY-OK      TO TRUE
        ws.getIabv0007().setWkBatCollectionKey(true);
        // COB_CODE: SET WK-JOB-COLLECTION-KEY-OK      TO TRUE
        ws.getIabv0007().setWkJobCollectionKey(true);
        // COB_CODE: SET WK-ERRORE-NO                  TO TRUE
        ws.getIabv0007().setWkErrore(false);
        // COB_CODE: SET WK-ERRORE-BLOCCANTE           TO TRUE
        ws.getIabv0007().getWkTipoErrore().setBloccante();
        // COB_CODE: SET WK-FINE-BTC-BATCH-NO          TO TRUE
        ws.getIabv0007().setFlagFineBtcBatch(false);
        // COB_CODE: SET INIT-COMMIT-FREQUENCY         TO TRUE
        ws.getIabv0007().setInitCommitFrequency();
        // COB_CODE: SET IABV0006-COMMIT-NO            TO TRUE
        ws.getIabv0006().getFlagCommit().setIabv0006CommitNo();
        // COB_CODE: SET WK-LANCIA-BUSINESS-YES        TO TRUE
        ws.getIabv0007().setFlagLanciaBusiness(true);
        // COB_CODE: SET WK-LANCIA-ROTTURA-NO          TO TRUE
        ws.getIabv0007().setFlagLanciaRottura(false);
        // COB_CODE: SET WK-FINE-ALIMENTAZIONE-NO      TO TRUE
        ws.getIabv0007().setFlagFineAlimentazione(false);
        // COB_CODE: SET GESTIONE-PARALLELISMO-NO      TO TRUE
        ws.getIabv0007().getFlagGestioneParallelismo().setNo();
        // COB_CODE: SET BATCH-COMPLETE                TO TRUE
        ws.getIabv0007().getWsBufferWhereCondIabs0130().getFlagBatchComplete().setComplete();
        // COB_CODE: SET BATCH-STATE-OK                TO TRUE
        ws.getIabv0007().getWsBufferWhereCondIabs0130().getFlagBatchState().setOk();
        // COB_CODE: SET WK-ALLINEA-STATI-OK           TO TRUE
        ws.getIabv0007().getFlagAllineaStati().setOk();
        // COB_CODE: SET GESTIONE-REAL-TIME            TO TRUE
        ws.getIabv0007().getFlagGestioneLanciBus().setRealTime();
        // COB_CODE: SET POST-GUIDE-X-JOB-NOT-FOUND-NO TO TRUE
        ws.getIabv0007().setWkPostGuideXJobNotFound(false);
        // COB_CODE: SET PRIMA-VOLTA-YES               TO TRUE
        ws.getIabv0007().setWkPrimaVolta(true);
        // COB_CODE: SET IABV0006-PRIMO-LANCIO         TO TRUE
        ws.getIabv0006().getTipoLancioBus().setIabv0006PrimoLancio();
        // COB_CODE: SET STAMPA-TESTATA-JCL-YES        TO TRUE
        ws.getIabv0007().setFlagStampaTestataJcl(true);
        // COB_CODE: SET STAMPA-LOGO-ERRORE-YES        TO TRUE
        ws.getIabv0007().setFlagStampaLogoErrore(true);
        // COB_CODE: SET IDSV0001-NO-DEBUG             TO TRUE
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().setIdsv0001NoDebug();
        // COB_CODE: SET WK-VERIFICA-PRENOTAZIONE-OK   TO TRUE
        ws.getIabv0007().setFlagVerificaPrenotazione(true);
        // COB_CODE: SET WK-VERIFICA-BATCH-KO          TO TRUE
        ws.getIabv0007().setFlagVerificaBatch(false);
        // COB_CODE: SET WK-ELABORA-BATCH-NO           TO TRUE
        ws.getIabv0007().setFlagElaboraBatch(false);
        // COB_CODE: SET WK-AGGIORNA-PRENOTAZ-YES      TO TRUE
        ws.getIabv0007().setFlagAggiornaPrenotaz(true);
        // COB_CODE: SET BATCH-EXECUTOR-STD-NO         TO TRUE
        ws.getIabv0007().getFlagBatchExecutorStd().setNo();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE SPACES               TO IDSV0003-NOME-TABELLA
        //                                        WK-COD-BATCH-STATE
        //                                        WK-COD-ELAB-STATE
        //                                        WK-DES-BATCH-STATE
        //                                        WK-STATE-CURRENT.
        ws.getIabv0007().getIdsv0003().getCampiEsito().setNomeTabella("");
        ws.getIabv0007().setWkCodBatchState(Types.SPACE_CHAR);
        ws.getIabv0007().setWkCodElabState(Types.SPACE_CHAR);
        ws.getIabv0007().setWkDesBatchState("");
        ws.getIabv0007().getIabv0004().setWkStateCurrent(Types.SPACE_CHAR);
        // COB_CODE: MOVE ZEROES               TO WK-SQLCODE
        //                                        IDSV0003-NUM-RIGHE-LETTE
        //                                        IND-STATI-SOSP
        //                                        WK-VERSIONING
        //                                        IABV0009-VERSIONING
        //                                        CONT-TAB-GUIDE-LETTE
        //                                        CONT-BUS-SERV-ESEG
        //                                        CONT-BUS-SERV-ESITO-OK
        //                                        CONT-BUS-SERV-WARNING
        //                                        CONT-BUS-SERV-ESITO-KO.
        ws.getIabv0007().setWkSqlcode(0);
        ws.getIabv0007().getIdsv0003().getCampiEsito().setNumRigheLette(((short)0));
        ws.getIabv0007().setIndStatiSosp(((short)0));
        ws.getIabv0007().setWkVersioning(0);
        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setVersioning(0);
        ws.getIabv0007().setContTabGuideLette(0);
        ws.getIabv0007().setContBusServEseg(0);
        ws.getIabv0007().setContBusServEsitoOk(0);
        ws.getIabv0007().setContBusServWarning(0);
        ws.getIabv0007().setContBusServEsitoKo(0);
        // COB_CODE: MOVE SPACES               TO IDSV0003-DESCRIZ-ERR-DB2
        //                                        IDSV0003-KEY-TABELLA.
        ws.getIabv0007().getIdsv0003().getCampiEsito().setDescrizErrDb2("");
        ws.getIabv0007().getIdsv0003().getCampiEsito().setKeyTabella("");
        // COB_CODE: SET  IABV0006-GEST-APPL-ROTTURA-IBO  TO TRUE.
        ws.getIabv0006().getGestRotturaIbo().setIabv0006GestApplRotturaIbo();
        // COB_CODE: PERFORM I000-INIT-CUSTOM-COUNT THRU I000-EX.
        i000InitCustomCount();
        //----------------------------------------------------------------*
        //    initializzazione routine degli errori
        //----------------------------------------------------------------*
        // CONTIENE UNA SEQUENZA DI ISTRUZIONI PER AZZERAR 'AREA DEGLI
        //ERRORI DELL'AREA CONTESTO.
        // COB_CODE: MOVE 'OK'   TO IDSV0001-ESITO
        ws.getIabv0007().getAreaIdsv0001().getEsito().setEsito("OK");
        // COB_CODE: MOVE SPACES TO IDSV0001-LOG-ERRORE
        ws.getIabv0007().getAreaIdsv0001().getLogErrore().initLogErroreSpaces();
        // COB_CODE: SET IDSV0001-FORZ-RC-04-NO        TO TRUE
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        // COB_CODE: PERFORM VARYING IDSV0001-MAX-ELE-ERRORI FROM 1 BY 1
        //                     UNTIL IDSV0001-MAX-ELE-ERRORI > 10
        //               TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
        //           END-PERFORM
        ws.getIabv0007().getAreaIdsv0001().setMaxEleErrori(((short)1));
        while (!(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori() > 10)) {
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori()).setDescErrore("");
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori()).setIdsv0001CodErrore(0);
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)0));
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
            ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori()).setTipoTrattFe(Types.SPACE_CHAR);
            ws.getIabv0007().getAreaIdsv0001().setMaxEleErrori(Trunc.toShort(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES TO IDSV0001-MAX-ELE-ERRORI.
        ws.getIabv0007().getAreaIdsv0001().setMaxEleErrori(((short)0));
        // CONTIENE UNA SEQUENZA DI ISTRUZIONI PER AZZERAR 'AREA DEGLI
        //ERRORI DELL'AREA CONTESTO.
        // COB_CODE: PERFORM VARYING IEAV9904-MAX-ELE-ERRORI FROM 1 BY 1
        //                     UNTIL IEAV9904-MAX-ELE-ERRORI >
        //                           IEAV9904-LIMITE-MAX
        //               TO IEAV9904-TIPO-TRATT-FE(IEAV9904-MAX-ELE-ERRORI)
        //           END-PERFORM
        ws.getIeav9904().setMaxEleErrori(((short)1));
        while (!(ws.getIeav9904().getMaxEleErrori() > ws.getIeav9904().getLimiteMax())) {
            // COB_CODE: MOVE SPACES
            //             TO IEAV9904-DESC-ERRORE(IEAV9904-MAX-ELE-ERRORI)
            ws.getIeav9904().getEleErrori(ws.getIeav9904().getMaxEleErrori()).setDescErrore("");
            // COB_CODE: MOVE ZEROES
            //             TO IEAV9904-COD-ERRORE(IEAV9904-MAX-ELE-ERRORI)
            ws.getIeav9904().getEleErrori(ws.getIeav9904().getMaxEleErrori()).setCodErrore(0);
            // COB_CODE: MOVE ZEROES
            //             TO IEAV9904-LIV-GRAVITA-BE(IEAV9904-MAX-ELE-ERRORI)
            ws.getIeav9904().getEleErrori(ws.getIeav9904().getMaxEleErrori()).setLivGravitaBe(((short)0));
            // COB_CODE: MOVE SPACES
            //             TO IEAV9904-TIPO-TRATT-FE(IEAV9904-MAX-ELE-ERRORI)
            ws.getIeav9904().getEleErrori(ws.getIeav9904().getMaxEleErrori()).setTipoTrattFe(Types.SPACE_CHAR);
            ws.getIeav9904().setMaxEleErrori(Trunc.toShort(ws.getIeav9904().getMaxEleErrori() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES TO IEAV9904-MAX-ELE-ERRORI.
        ws.getIeav9904().setMaxEleErrori(((short)0));
    }

    /**Original name: A100-INIZIO-ELABORA<br>
	 * <pre>----------------------------------------------------------------*
	 *  INIZIO ELABORAZIONE                                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void a100InizioElabora() {
        // COB_CODE: MOVE 'A100-INIZIO-ELABORA'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A100-INIZIO-ELABORA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE WK-PGM             TO IDSV0001-COD-MAIN-BATCH
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setCodMainBatch(ws.getWkPgm());
        // COB_CODE: PERFORM A101-DISPLAY-INIZIO-ELABORA THRU A101-EX.
        a101DisplayInizioElabora();
    }

    /**Original name: A103-LEGGI-PARAM-INFR-APPL<br>
	 * <pre>----------------------------------------------------------------*
	 *  LETTURA DELLA TABELLA PARAM_INFR_APPL
	 * ----------------------------------------------------------------*</pre>*/
    private void a103LeggiParamInfrAppl() {
        // COB_CODE: MOVE 'A103-LEGGI-PARAM-INFR-APPL' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A103-LEGGI-PARAM-INFR-APPL");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM A104-CALL-LDBS6730  THRU A104-EX.
        a104CallLdbs6730();
    }

    /**Original name: A104-CALL-LDBS6730<br>
	 * <pre>               IJCCMQ04-VAR-AMBIENTE</pre>*/
    private void a104CallLdbs6730() {
        Ldbs6730 ldbs6730 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE:      INITIALIZE PARAM-INFR-APPL
        //           *               IJCCMQ04-VAR-AMBIENTE
        //                           IDSV0003.
        initParamInfrAppl();
        initIdsv0003();
        // COB_CODE: SET  IDSV0003-SELECT           TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setSelect();
        // COB_CODE: SET  IDSV0003-WHERE-CONDITION  TO TRUE.
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET  IDSV0003-TRATT-SENZA-STOR TO TRUE.
        ws.getIabv0007().getIdsv0003().getTrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE 'LDBS6730'                TO IDSV0003-COD-MAIN-BATCH.
        ws.getIabv0007().getIdsv0003().setCodMainBatch("LDBS6730");
        // COB_CODE: CALL IDSV0003-COD-MAIN-BATCH   USING  IDSV0003
        //                                                 PARAM-INFR-APPL.
        ldbs6730 = Ldbs6730.getInstance();
        ldbs6730.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getParamInfrAppl());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           ELSE
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                     WHEN IDSV0003-SUCCESSFUL-SQL
            //                           THRU A106-EX
            //                     WHEN IDSV0003-NOT-FOUND
            //                          INITIALIZE PARAM-INFR-APPL
            //           *               INITIALIZE WK-LOG-ERRORE
            //           *               MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
            //           *               STRING 'TABELLA PARAM_INFR_APPL VUOTA'
            //           *                       ' - '
            //           *                       'SQLCODE : '
            //           *                       WK-SQLCODE
            //           *                       DELIMITED BY SIZE INTO
            //           *                       WK-LOR-DESC-ERRORE-ESTESA
            //           *               END-STRING
            //           *               MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            //           *               PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                     WHEN OTHER
            //                          PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-EVALUATE
            switch (ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.SUCCESSFUL_SQL:// COB_CODE: PERFORM A106-VALORIZZA-OUTPUT-PIA
                    //              THRU A106-EX
                    a106ValorizzaOutputPia();
                    break;

                case Idsv0003Sqlcode.NOT_FOUND:// COB_CODE: INITIALIZE PARAM-INFR-APPL
                    initParamInfrAppl();
                    //               INITIALIZE WK-LOG-ERRORE
                    //               MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                    //               STRING 'TABELLA PARAM_INFR_APPL VUOTA'
                    //                       ' - '
                    //                       'SQLCODE : '
                    //                       WK-SQLCODE
                    //                       DELIMITED BY SIZE INTO
                    //                       WK-LOR-DESC-ERRORE-ESTESA
                    //               END-STRING
                    //               MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                    //               PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    break;

                default:// COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERR.LETT. PARAM_INFR_APPL '
                    //                  ' RC ' IDSV0003-RETURN-CODE
                    //                   'SQLCODE : '
                    //                   WK-SQLCODE
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERR.LETT. PARAM_INFR_APPL ", " RC ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted(), "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                    break;
            }
        }
        else {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
            // COB_CODE: STRING 'ERRORE CHIAMATA LDBS6730 '
            //                  ' RC ' IDSV0003-RETURN-CODE
            //                   'SQLCODE : '
            //                   WK-SQLCODE
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA LDBS6730 ", " RC ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted(), "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: A106-VALORIZZA-OUTPUT-PIA<br>
	 * <pre>---------------------------------------------------------------*
	 *       VALORIZZAZIONE TRACCIATO DI OUTPUT
	 * ---------------------------------------------------------------*</pre>*/
    private void a106ValorizzaOutputPia() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE D09-COD-COMP-ANIA       TO IJCCMQ04-COD-COMP-ANIA.
        ws.getIabv0007().getIjccmq04VarAmbiente().setCodCompAnia(TruncAbs.toInt(ws.getIabv0007().getParamInfrAppl().getD09CodCompAnia(), 5));
        // COB_CODE: MOVE D09-AMBIENTE            TO IJCCMQ04-AMBIENTE.
        ws.getIabv0007().getIjccmq04VarAmbiente().setAmbiente(ws.getIabv0007().getParamInfrAppl().getD09Ambiente());
        // COB_CODE: MOVE D09-PIATTAFORMA         TO IJCCMQ04-PIATTAFORMA.
        ws.getIabv0007().getIjccmq04VarAmbiente().setPiattaforma(ws.getIabv0007().getParamInfrAppl().getD09Piattaforma());
        // COB_CODE: MOVE D09-TP-COM-COBOL-JAVA   TO IJCCMQ04-COB-TO-JAVA.
        ws.getIabv0007().getIjccmq04VarAmbiente().setCobToJava(ws.getIabv0007().getParamInfrAppl().getD09TpComCobolJava());
        // COB_CODE: IF NOT ( IJCCMQ04-MQ        OR
        //                    IJCCMQ04-CSOCKET   OR
        //                    IJCCMQ04-JCICS-COO )
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           END-IF.
        if (!(ws.getIabv0007().getIjccmq04VarAmbiente().isMq() || ws.getIabv0007().getIjccmq04VarAmbiente().isCsocket() || ws.getIabv0007().getIjccmq04VarAmbiente().isJcicsCoo())) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
            // COB_CODE: STRING 'PARAM_INFR_APPL - COMUNIC. NON VALIDA : '
            //                   IJCCMQ04-COB-TO-JAVA
            //           DELIMITED BY SIZE   INTO WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "PARAM_INFR_APPL - COMUNIC. NON VALIDA : ", ws.getIabv0007().getIjccmq04VarAmbiente().getCobToJavaFormatted());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
        // COB_CODE: IF D09-MQ-TP-UTILIZZO-API-NULL = HIGH-VALUES
        //              MOVE SPACES                  TO IJCCMQ04-TP-UTILIZZO-API
        //           ELSE
        //                END-IF
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getIabv0007().getParamInfrAppl().getD09MqTpUtilizzoApiFormatted())) {
            // COB_CODE: MOVE SPACES                  TO IJCCMQ04-TP-UTILIZZO-API
            ws.getIabv0007().getIjccmq04VarAmbiente().setTpUtilizzoApi("");
        }
        else {
            // COB_CODE: MOVE D09-MQ-TP-UTILIZZO-API   TO IJCCMQ04-TP-UTILIZZO-API
            ws.getIabv0007().getIjccmq04VarAmbiente().setTpUtilizzoApi(ws.getIabv0007().getParamInfrAppl().getD09MqTpUtilizzoApi());
            // COB_CODE: IF  NOT ( IJCCMQ04-CALL-COBOL OR
            //                     IJCCMQ04-CALL-C )
            //             PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //             END-IF
            if (!(ws.getIabv0007().getIjccmq04VarAmbiente().isCallCobol() || ws.getIabv0007().getIjccmq04VarAmbiente().isCallC())) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING 'PARAM_INFR_APPL - TP UTILIZZO API NON VALIDO : '
                //                    IJCCMQ04-TP-UTILIZZO-API
                //           DELIMITED BY SIZE   INTO WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "PARAM_INFR_APPL - TP UTILIZZO API NON VALIDO : ", ws.getIabv0007().getIjccmq04VarAmbiente().getTpUtilizzoApiFormatted());
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
        // COB_CODE: IF D09-MQ-QUEUE-MANAGER-NULL = HIGH-VALUES
        //              MOVE SPACES                  TO IJCCMQ04-QUEUE-MANAGER
        //           ELSE
        //              MOVE D09-MQ-QUEUE-MANAGER    TO IJCCMQ04-QUEUE-MANAGER
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getIabv0007().getParamInfrAppl().getD09MqQueueManagerFormatted())) {
            // COB_CODE: MOVE SPACES                  TO IJCCMQ04-QUEUE-MANAGER
            ws.getIabv0007().getIjccmq04VarAmbiente().setQueueManager("");
        }
        else {
            // COB_CODE: MOVE D09-MQ-QUEUE-MANAGER    TO IJCCMQ04-QUEUE-MANAGER
            ws.getIabv0007().getIjccmq04VarAmbiente().setQueueManager(ws.getIabv0007().getParamInfrAppl().getD09MqQueueManager());
        }
        // COB_CODE: IF D09-MQ-CODA-PUT-NULL = HIGH-VALUES
        //              MOVE SPACES                  TO IJCCMQ04-CODA-PUT
        //           ELSE
        //              MOVE D09-MQ-CODA-PUT         TO IJCCMQ04-CODA-PUT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getIabv0007().getParamInfrAppl().getD09MqCodaPutFormatted())) {
            // COB_CODE: MOVE SPACES                  TO IJCCMQ04-CODA-PUT
            ws.getIabv0007().getIjccmq04VarAmbiente().setCodaPut("");
        }
        else {
            // COB_CODE: MOVE D09-MQ-CODA-PUT         TO IJCCMQ04-CODA-PUT
            ws.getIabv0007().getIjccmq04VarAmbiente().setCodaPut(ws.getIabv0007().getParamInfrAppl().getD09MqCodaPut());
        }
        // COB_CODE: IF D09-MQ-CODA-GET-NULL = HIGH-VALUES
        //              MOVE SPACES                  TO IJCCMQ04-CODA-GET
        //           ELSE
        //              MOVE D09-MQ-CODA-GET         TO IJCCMQ04-CODA-GET
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getIabv0007().getParamInfrAppl().getD09MqCodaGetFormatted())) {
            // COB_CODE: MOVE SPACES                  TO IJCCMQ04-CODA-GET
            ws.getIabv0007().getIjccmq04VarAmbiente().setCodaGet("");
        }
        else {
            // COB_CODE: MOVE D09-MQ-CODA-GET         TO IJCCMQ04-CODA-GET
            ws.getIabv0007().getIjccmq04VarAmbiente().setCodaGet(ws.getIabv0007().getParamInfrAppl().getD09MqCodaGet());
        }
        // COB_CODE: IF D09-MQ-OPZ-PERSISTENZA-NULL = HIGH-VALUES
        //              MOVE SPACE                   TO IJCCMQ04-OPZ-PERSISTENZA
        //           ELSE
        //              END-IF
        //           END-IF
        if (Conditions.eq(ws.getIabv0007().getParamInfrAppl().getD09MqOpzPersistenza(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE SPACE                   TO IJCCMQ04-OPZ-PERSISTENZA
            ws.getIabv0007().getIjccmq04VarAmbiente().setOpzPersistenza(Types.SPACE_CHAR);
        }
        else {
            // COB_CODE: MOVE D09-MQ-OPZ-PERSISTENZA  TO IJCCMQ04-OPZ-PERSISTENZA
            ws.getIabv0007().getIjccmq04VarAmbiente().setOpzPersistenza(ws.getIabv0007().getParamInfrAppl().getD09MqOpzPersistenza());
            // COB_CODE: IF NOT IJCCMQ04-OP-VALIDA
            //             PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            if (!ws.getIabv0007().getIjccmq04VarAmbiente().isOpValida()) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING 'PARAM_INFR_APPL - OPZ PERSISTENZA NON VALIDA : '
                //                  IJCCMQ04-OPZ-PERSISTENZA
                //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "PARAM_INFR_APPL - OPZ PERSISTENZA NON VALIDA : ", String.valueOf(ws.getIabv0007().getIjccmq04VarAmbiente().getOpzPersistenza()));
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
        // COB_CODE: IF D09-MQ-ATTESA-RISPOSTA-NULL = HIGH-VALUES
        //              MOVE SPACE                   TO IJCCMQ04-ATTESA-RISPOSTA
        //           ELSE
        //              END-IF
        //           END-IF
        if (Conditions.eq(ws.getIabv0007().getParamInfrAppl().getD09MqAttesaRisposta(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE SPACE                   TO IJCCMQ04-ATTESA-RISPOSTA
            ws.getIabv0007().getIjccmq04VarAmbiente().setAttesaRisposta(Types.SPACE_CHAR);
        }
        else {
            // COB_CODE: MOVE D09-MQ-ATTESA-RISPOSTA  TO IJCCMQ04-ATTESA-RISPOSTA
            ws.getIabv0007().getIjccmq04VarAmbiente().setAttesaRisposta(ws.getIabv0007().getParamInfrAppl().getD09MqAttesaRisposta());
            // COB_CODE: IF  NOT IJCCMQ04-AR-VALIDA
            //             PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            if (!ws.getIabv0007().getIjccmq04VarAmbiente().isArValida()) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING 'PARAM_INFR_APPL - ATTESA RISPOSTA NON VALIDA : '
                //                    IJCCMQ04-ATTESA-RISPOSTA
                //             DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "PARAM_INFR_APPL - ATTESA RISPOSTA NON VALIDA : ", String.valueOf(ws.getIabv0007().getIjccmq04VarAmbiente().getAttesaRisposta()));
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
        // COB_CODE: IF D09-MQ-OPZ-WAIT-NULL = HIGH-VALUES
        //              MOVE SPACE                   TO IJCCMQ04-OPZ-WAIT
        //           ELSE
        //              END-IF
        //           END-IF
        if (Conditions.eq(ws.getIabv0007().getParamInfrAppl().getD09MqOpzWait(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE SPACE                   TO IJCCMQ04-OPZ-WAIT
            ws.getIabv0007().getIjccmq04VarAmbiente().setOpzWait(Types.SPACE_CHAR);
        }
        else {
            // COB_CODE: MOVE D09-MQ-OPZ-WAIT         TO IJCCMQ04-OPZ-WAIT
            ws.getIabv0007().getIjccmq04VarAmbiente().setOpzWait(ws.getIabv0007().getParamInfrAppl().getD09MqOpzWait());
            // COB_CODE: IF  NOT IJCCMQ04-OW-VALIDA
            //               PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            if (!ws.getIabv0007().getIjccmq04VarAmbiente().isOwValida()) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING 'PARAM_INFR_APPL - OPZ WAIT NON VALIDA : '
                //                    IJCCMQ04-OPZ-WAIT
                //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "PARAM_INFR_APPL - OPZ WAIT NON VALIDA : ", String.valueOf(ws.getIabv0007().getIjccmq04VarAmbiente().getOpzWait()));
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
        // COB_CODE: IF D09-MQ-OPZ-SYNCPOINT-NULL = HIGH-VALUES
        //              MOVE SPACE                   TO IJCCMQ04-OPZ-SYNCPOINT
        //           ELSE
        //              END-IF
        //           END-IF
        if (Conditions.eq(ws.getIabv0007().getParamInfrAppl().getD09MqOpzSyncpoint(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE SPACE                   TO IJCCMQ04-OPZ-SYNCPOINT
            ws.getIabv0007().getIjccmq04VarAmbiente().setOpzSyncpoint(Types.SPACE_CHAR);
        }
        else {
            // COB_CODE: MOVE D09-MQ-OPZ-SYNCPOINT    TO IJCCMQ04-OPZ-SYNCPOINT
            ws.getIabv0007().getIjccmq04VarAmbiente().setOpzSyncpoint(ws.getIabv0007().getParamInfrAppl().getD09MqOpzSyncpoint());
            // COB_CODE: IF NOT IJCCMQ04-OS-VALIDA
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            if (!ws.getIabv0007().getIjccmq04VarAmbiente().isOsValida()) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING 'PARAM_INFR_APPL - OPZ SYNCPOINT NON VALIDA : '
                //                   IJCCMQ04-OPZ-SYNCPOINT
                //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "PARAM_INFR_APPL - OPZ SYNCPOINT NON VALIDA : ", String.valueOf(ws.getIabv0007().getIjccmq04VarAmbiente().getOpzSyncpoint()));
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
        // COB_CODE: IF D09-MQ-TEMPO-ATTESA-1-NULL = HIGH-VALUES
        //              MOVE ZERO                    TO IJCCMQ04-TEMPO-ATTESA-1
        //           ELSE
        //              MOVE D09-MQ-TEMPO-ATTESA-1   TO IJCCMQ04-TEMPO-ATTESA-1
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getIabv0007().getParamInfrAppl().getD09MqTempoAttesa1().getD09MqTempoAttesa1NullFormatted())) {
            // COB_CODE: MOVE ZERO                    TO IJCCMQ04-TEMPO-ATTESA-1
            ws.getIabv0007().getIjccmq04VarAmbiente().setTempoAttesa1(0);
        }
        else {
            // COB_CODE: MOVE D09-MQ-TEMPO-ATTESA-1   TO IJCCMQ04-TEMPO-ATTESA-1
            ws.getIabv0007().getIjccmq04VarAmbiente().setTempoAttesa1(Trunc.toInt(ws.getIabv0007().getParamInfrAppl().getD09MqTempoAttesa1().getD09MqTempoAttesa1(), 9));
        }
        // COB_CODE: IF D09-MQ-TEMPO-ATTESA-2-NULL = HIGH-VALUES
        //              MOVE ZERO                    TO IJCCMQ04-TEMPO-ATTESA-2
        //           ELSE
        //              MOVE D09-MQ-TEMPO-ATTESA-2   TO IJCCMQ04-TEMPO-ATTESA-2
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getIabv0007().getParamInfrAppl().getD09MqTempoAttesa2().getD09MqTempoAttesa2NullFormatted())) {
            // COB_CODE: MOVE ZERO                    TO IJCCMQ04-TEMPO-ATTESA-2
            ws.getIabv0007().getIjccmq04VarAmbiente().setTempoAttesa2(0);
        }
        else {
            // COB_CODE: MOVE D09-MQ-TEMPO-ATTESA-2   TO IJCCMQ04-TEMPO-ATTESA-2
            ws.getIabv0007().getIjccmq04VarAmbiente().setTempoAttesa2(Trunc.toInt(ws.getIabv0007().getParamInfrAppl().getD09MqTempoAttesa2().getD09MqTempoAttesa2(), 9));
        }
        // COB_CODE: IF D09-MQ-TEMPO-EXPIRY-NULL = HIGH-VALUES
        //              MOVE ZERO                 TO IJCCMQ04-TEMPO-EXPIRY
        //           ELSE
        //              MOVE D09-MQ-TEMPO-EXPIRY  TO IJCCMQ04-TEMPO-EXPIRY
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getIabv0007().getParamInfrAppl().getD09MqTempoExpiry().getD09MqTempoExpiryNullFormatted())) {
            // COB_CODE: MOVE ZERO                 TO IJCCMQ04-TEMPO-EXPIRY
            ws.getIabv0007().getIjccmq04VarAmbiente().setTempoExpiry(0);
        }
        else {
            // COB_CODE: MOVE D09-MQ-TEMPO-EXPIRY  TO IJCCMQ04-TEMPO-EXPIRY
            ws.getIabv0007().getIjccmq04VarAmbiente().setTempoExpiry(Trunc.toInt(ws.getIabv0007().getParamInfrAppl().getD09MqTempoExpiry().getD09MqTempoExpiry(), 9));
        }
        // COB_CODE: IF D09-CSOCKET-IP-ADDRESS-NULL = HIGH-VALUES
        //              MOVE SPACES                 TO IJCCMQ04-CSOCKET-IP-ADDRESS
        //           ELSE
        //              MOVE D09-CSOCKET-IP-ADDRESS TO IJCCMQ04-CSOCKET-IP-ADDRESS
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getIabv0007().getParamInfrAppl().getD09CsocketIpAddressFormatted())) {
            // COB_CODE: MOVE SPACES                 TO IJCCMQ04-CSOCKET-IP-ADDRESS
            ws.getIabv0007().getIjccmq04VarAmbiente().setCsocketIpAddress("");
        }
        else {
            // COB_CODE: MOVE D09-CSOCKET-IP-ADDRESS TO IJCCMQ04-CSOCKET-IP-ADDRESS
            ws.getIabv0007().getIjccmq04VarAmbiente().setCsocketIpAddress(ws.getIabv0007().getParamInfrAppl().getD09CsocketIpAddress());
        }
        // COB_CODE: IF D09-CSOCKET-PORT-NUM-NULL = HIGH-VALUES
        //              MOVE ZERO                  TO IJCCMQ04-CSOCKET-PORT-NUM
        //           ELSE
        //              MOVE D09-CSOCKET-PORT-NUM  TO IJCCMQ04-CSOCKET-PORT-NUM
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getIabv0007().getParamInfrAppl().getD09CsocketPortNum().getD09CsocketPortNumNullFormatted())) {
            // COB_CODE: MOVE ZERO                  TO IJCCMQ04-CSOCKET-PORT-NUM
            ws.getIabv0007().getIjccmq04VarAmbiente().setCsocketPortNum(0);
        }
        else {
            // COB_CODE: MOVE D09-CSOCKET-PORT-NUM  TO IJCCMQ04-CSOCKET-PORT-NUM
            ws.getIabv0007().getIjccmq04VarAmbiente().setCsocketPortNum(TruncAbs.toInt(ws.getIabv0007().getParamInfrAppl().getD09CsocketPortNum().getD09CsocketPortNum(), 5));
        }
        // COB_CODE: IF D09-FL-COMPRESSORE-C-NULL = HIGH-VALUES
        //              MOVE SPACES                TO IJCCMQ04-FL-COMPRESSORE-C
        //           ELSE
        //              END-IF
        //           END-IF.
        if (Conditions.eq(ws.getIabv0007().getParamInfrAppl().getD09FlCompressoreC(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE SPACES                TO IJCCMQ04-FL-COMPRESSORE-C
            ws.getIabv0007().getIjccmq04VarAmbiente().setFlCompressoreC(Types.SPACE_CHAR);
        }
        else {
            // COB_CODE: MOVE D09-FL-COMPRESSORE-C  TO IJCCMQ04-FL-COMPRESSORE-C
            ws.getIabv0007().getIjccmq04VarAmbiente().setFlCompressoreC(ws.getIabv0007().getParamInfrAppl().getD09FlCompressoreC());
            // COB_CODE: IF  NOT ( IJCCMQ04-FL-COMPRESSORE-C-SI OR
            //                     IJCCMQ04-FL-COMPRESSORE-C-NO )
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            if (!(ws.getIabv0007().getIjccmq04VarAmbiente().isFlCompressoreCSi() || ws.getIabv0007().getIjccmq04VarAmbiente().isFlCompressoreCNo())) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
                ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING 'PARAM_INFR_APPL - FL COMPRESSORE NON VALIDO : '
                //                   IJCCMQ04-FL-COMPRESSORE-C
                //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "PARAM_INFR_APPL - FL COMPRESSORE NON VALIDO : ", String.valueOf(ws.getIabv0007().getIjccmq04VarAmbiente().getFlCompressoreC()));
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
        // COB_CODE: IF IJCCMQ04-MQ
        //              AND (IJCCMQ04-QUEUE-MANAGER   NOT > SPACES
        //              OR   IJCCMQ04-CODA-PUT        NOT > SPACES
        //              OR   IJCCMQ04-CODA-GET        NOT > SPACES
        //              OR   IJCCMQ04-OPZ-PERSISTENZA NOT > SPACES
        //              OR   IJCCMQ04-OPZ-WAIT        NOT > SPACES
        //              OR   IJCCMQ04-OPZ-SYNCPOINT   NOT > SPACES
        //              OR   IJCCMQ04-ATTESA-RISPOSTA NOT > SPACES)
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           END-IF.
        if (ws.getIabv0007().getIjccmq04VarAmbiente().isMq() && (!Conditions.gt(ws.getIabv0007().getIjccmq04VarAmbiente().getQueueManager(), "") || !Conditions.gt(ws.getIabv0007().getIjccmq04VarAmbiente().getCodaPut(), "") || !Conditions.gt(ws.getIabv0007().getIjccmq04VarAmbiente().getCodaGet(), "") || !Conditions.gt(ws.getIabv0007().getIjccmq04VarAmbiente().getOpzPersistenza(), Types.SPACE_CHAR) || !Conditions.gt(ws.getIabv0007().getIjccmq04VarAmbiente().getOpzWait(), Types.SPACE_CHAR) || !Conditions.gt(ws.getIabv0007().getIjccmq04VarAmbiente().getOpzSyncpoint(), Types.SPACE_CHAR) || !Conditions.gt(ws.getIabv0007().getIjccmq04VarAmbiente().getAttesaRisposta(), Types.SPACE_CHAR))) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE IDSV0003-SQLCODE     TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
            // COB_CODE: MOVE 'PARAM_INFR_APPL - PARAMETRI INCOMPLETI X MQ'
            //                TO  WK-LOR-DESC-ERRORE-ESTESA
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa("PARAM_INFR_APPL - PARAMETRI INCOMPLETI X MQ");
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: A105-ATTIVA-CONNESSIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *  attiva la connessione alle code mq
	 * ----------------------------------------------------------------*</pre>*/
    private void a105AttivaConnessione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IJCCMQ04-QUEUE-MANAGER          TO WSMQ-QM-NAME.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setQmName(ws.getIabv0007().getIjccmq04VarAmbiente().getQueueManager());
        // COB_CODE: MOVE 'MQCONN'                        TO WSMQ-TIPO-OPERAZIONE.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTipoOperazione("MQCONN");
        // COB_CODE: MOVE IJCCMQ04-TP-UTILIZZO-API        TO WSMQ-TP-UTILIZZO-API.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTpUtilizzoApi(ws.getIabv0007().getIjccmq04VarAmbiente().getTpUtilizzoApi());
        // COB_CODE: CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.
        DynamicCall.invoke(ws.getIabv0007().getPgmIjcs0060(), ws.getIabv0007().getwAreaPerChiamateWsmq());
        // COB_CODE: IF WSMQ-REASON-JCALL IS NOT EQUAL 0
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall() != 0) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-REASON-JCALL    TO W-EDIT-REASONS
            ws.getIabv0007().setwEditReasons(ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall());
            // COB_CODE: STRING 'MQCONN REASON-CODE:' W-EDIT-REASONS
            //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "MQCONN REASON-CODE:", ws.getIabv0007().getwEditReasonsAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
        else if (Characters.EQ_SPACE.test(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno())) {
            // COB_CODE: IF WSMQ-MESSAGGIO-INTERNO = SPACES
            //              END-IF
            //           ELSE
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            // COB_CODE: PERFORM  A109-APRI-CODA-JCALL
            //              THRU  A109-EX
            a109ApriCodaJcall();
            // COB_CODE: IF WSMQ-REASON-JCALL IS EQUAL 0
            //                 THRU A110-EX
            //           END-IF
            if (ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall() == 0) {
                // COB_CODE: PERFORM A110-APRI-CODA-JRET
                //              THRU A110-EX
                a110ApriCodaJret();
            }
        }
        else {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-MESSAGGIO-INTERNO
            //                                    TO  WK-LOR-DESC-ERRORE-ESTESA
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno());
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: A107-CHIUDI-CONNESSIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *  Chiude la connessione alle code mq
	 * ----------------------------------------------------------------*</pre>*/
    private void a107ChiudiConnessione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM  A108-CHIUDI-CODA-JRET
        //              THRU  A108-EX.
        a108ChiudiCodaJret();
        // COB_CODE:  IF WSMQ-REASON-JRET IS EQUAL 0
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJret() == 0) {
            // COB_CODE: PERFORM A111-CHIUDI-CODA-JCALL
            //              THRU A111-EX
            a111ChiudiCodaJcall();
            // COB_CODE:  IF WSMQ-REASON-JCALL IS EQUAL 0
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall() == 0) {
                // COB_CODE: MOVE 'MQDISC'             TO WSMQ-TIPO-OPERAZIONE
                ws.getIabv0007().getwAreaPerChiamateWsmq().setTipoOperazione("MQDISC");
                // COB_CODE: MOVE IJCCMQ04-TP-UTILIZZO-API
                //             TO WSMQ-TP-UTILIZZO-API
                ws.getIabv0007().getwAreaPerChiamateWsmq().setTpUtilizzoApi(ws.getIabv0007().getIjccmq04VarAmbiente().getTpUtilizzoApi());
                // COB_CODE: CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ
                DynamicCall.invoke(ws.getIabv0007().getPgmIjcs0060(), ws.getIabv0007().getwAreaPerChiamateWsmq());
                // COB_CODE: IF WSMQ-REASON-JCALL IS NOT EQUAL 0
                //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall() != 0) {
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE WSMQ-REASON-JCALL    TO W-EDIT-REASONS
                    ws.getIabv0007().setwEditReasons(ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall());
                    // COB_CODE: STRING 'MQDISC REASON-CODE:' W-EDIT-REASONS
                    //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "MQDISC REASON-CODE:", ws.getIabv0007().getwEditReasonsAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
                else if (!Characters.EQ_SPACE.test(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno())) {
                    // COB_CODE: IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
                    //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    //           END-IF
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE WSMQ-MESSAGGIO-INTERNO
                    //                              TO  WK-LOR-DESC-ERRORE-ESTESA
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno());
                    // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
        }
    }

    /**Original name: A109-APRI-CODA-JCALL<br>
	 * <pre>----------------------------------------------------------------*
	 *  Apertura coda per operazioni di SCRITTURA
	 * ----------------------------------------------------------------*</pre>*/
    private void a109ApriCodaJcall() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IJCCMQ04-CODA-PUT        TO WSMQ-NOME-CODA.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setNomeCoda(ws.getIabv0007().getIjccmq04VarAmbiente().getCodaPut());
        // COB_CODE: MOVE IJCCMQ04-TP-UTILIZZO-API TO WSMQ-TP-UTILIZZO-API.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTpUtilizzoApi(ws.getIabv0007().getIjccmq04VarAmbiente().getTpUtilizzoApi());
        // COB_CODE: MOVE 'MQOPENJCALL'            TO WSMQ-TIPO-OPERAZIONE.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTipoOperazione("MQOPENJCALL");
        // COB_CODE: CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.
        DynamicCall.invoke(ws.getIabv0007().getPgmIjcs0060(), ws.getIabv0007().getwAreaPerChiamateWsmq());
        // COB_CODE: IF WSMQ-REASON-JCALL NOT = 0
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall() != 0) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-REASON-JCALL    TO W-EDIT-REASONS
            ws.getIabv0007().setwEditReasons(ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall());
            // COB_CODE: STRING 'MQOPEN REASON-CODE:' W-EDIT-REASONS ' O.C.:'
            //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "MQOPEN REASON-CODE:", ws.getIabv0007().getwEditReasonsAsString(), " O.C.:");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
        else if (!Characters.EQ_SPACE.test(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno())) {
            // COB_CODE: IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-MESSAGGIO-INTERNO
            //                              TO  WK-LOR-DESC-ERRORE-ESTESA
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno());
            // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: A110-APRI-CODA-JRET<br>
	 * <pre>----------------------------------------------------------------*
	 *  Apertura coda per operazioni di lettura
	 * ----------------------------------------------------------------*</pre>*/
    private void a110ApriCodaJret() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IJCCMQ04-CODA-GET        TO WSMQ-NOME-CODA.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setNomeCoda(ws.getIabv0007().getIjccmq04VarAmbiente().getCodaGet());
        // COB_CODE: MOVE IJCCMQ04-TP-UTILIZZO-API TO WSMQ-TP-UTILIZZO-API.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTpUtilizzoApi(ws.getIabv0007().getIjccmq04VarAmbiente().getTpUtilizzoApi());
        // COB_CODE: MOVE 'MQOPENJRET'             TO WSMQ-TIPO-OPERAZIONE.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTipoOperazione("MQOPENJRET");
        // COB_CODE: CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.
        DynamicCall.invoke(ws.getIabv0007().getPgmIjcs0060(), ws.getIabv0007().getwAreaPerChiamateWsmq());
        // COB_CODE: IF WSMQ-REASON-JRET NOT = 0
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJret() != 0) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-REASON-JRET TO W-EDIT-REASONS
            ws.getIabv0007().setwEditReasons(ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJret());
            // COB_CODE: STRING 'MQOPEN REASON-CODE:' W-EDIT-REASONS ' O.C.:'
            //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "MQOPEN REASON-CODE:", ws.getIabv0007().getwEditReasonsAsString(), " O.C.:");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
        else if (!Characters.EQ_SPACE.test(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno())) {
            // COB_CODE: IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-MESSAGGIO-INTERNO
            //                              TO  WK-LOR-DESC-ERRORE-ESTESA
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno());
            // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: A108-CHIUDI-CODA-JRET<br>
	 * <pre>----------------------------------------------------------------*
	 *  Chiudi coda JRET
	 * ----------------------------------------------------------------*</pre>*/
    private void a108ChiudiCodaJret() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IJCCMQ04-TP-UTILIZZO-API TO WSMQ-TP-UTILIZZO-API.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTpUtilizzoApi(ws.getIabv0007().getIjccmq04VarAmbiente().getTpUtilizzoApi());
        // COB_CODE: MOVE 'MQCLOSEJRET'            TO WSMQ-TIPO-OPERAZIONE.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTipoOperazione("MQCLOSEJRET");
        // COB_CODE: CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.
        DynamicCall.invoke(ws.getIabv0007().getPgmIjcs0060(), ws.getIabv0007().getwAreaPerChiamateWsmq());
        //
        // COB_CODE: IF WSMQ-REASON-JRET NOT = 0      AND
        //              WSMQ-REASON-JRET NOT = 2019
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJret() != 0 && ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJret() != 2019) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-REASON-JRET TO W-EDIT-REASONS
            ws.getIabv0007().setwEditReasons(ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJret());
            // COB_CODE: STRING 'MQCLOSE REASON-CODE:' W-EDIT-REASONS
            //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "MQCLOSE REASON-CODE:", ws.getIabv0007().getwEditReasonsAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
        else if (!Characters.EQ_SPACE.test(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno())) {
            // COB_CODE: IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-MESSAGGIO-INTERNO
            //                              TO  WK-LOR-DESC-ERRORE-ESTESA
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno());
            // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: A111-CHIUDI-CODA-JCALL<br>
	 * <pre>----------------------------------------------------------------*
	 *  Chiudi coda JCALL
	 * ----------------------------------------------------------------*</pre>*/
    private void a111ChiudiCodaJcall() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE IJCCMQ04-TP-UTILIZZO-API TO WSMQ-TP-UTILIZZO-API.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTpUtilizzoApi(ws.getIabv0007().getIjccmq04VarAmbiente().getTpUtilizzoApi());
        // COB_CODE: MOVE 'MQCLOSEJCALL'           TO WSMQ-TIPO-OPERAZIONE.
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTipoOperazione("MQCLOSEJCALL");
        // COB_CODE: CALL PGM-IJCS0060  USING W-AREA-PER-CHIAMATE-WSMQ.
        DynamicCall.invoke(ws.getIabv0007().getPgmIjcs0060(), ws.getIabv0007().getwAreaPerChiamateWsmq());
        // COB_CODE: IF WSMQ-REASON-JCALL NOT = 0      AND
        //              WSMQ-REASON-JCALL NOT = 2019
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall() != 0 && ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJcall() != 2019) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-REASON-JRET TO W-EDIT-REASONS
            ws.getIabv0007().setwEditReasons(ws.getIabv0007().getwAreaPerChiamateWsmq().getReasonJret());
            // COB_CODE: STRING 'MQCLOSE REASON-CODE:' W-EDIT-REASONS
            //           DELIMITED BY SIZE   INTO  WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "MQCLOSE REASON-CODE:", ws.getIabv0007().getwEditReasonsAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
        else if (!Characters.EQ_SPACE.test(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno())) {
            // COB_CODE: IF WSMQ-MESSAGGIO-INTERNO NOT = SPACES
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE WSMQ-MESSAGGIO-INTERNO
            //                              TO  WK-LOR-DESC-ERRORE-ESTESA
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getwAreaPerChiamateWsmq().getMessaggioInterno());
            // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: A150-SET-CURRENT-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *  SETTA TIMESTAMP
	 * ----------------------------------------------------------------*</pre>*/
    private void a150SetCurrentTimestamp() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A150-SET-CURRENT-TIMESTAMP' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A150-SET-CURRENT-TIMESTAMP");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM ESTRAI-CURRENT-TIMESTAMP
        //                   THRU ESTRAI-CURRENT-TIMESTAMP-EX
        estraiCurrentTimestamp();
        // COB_CODE: MOVE SQLCODE       TO IDSV0003-SQLCODE
        ws.getIabv0007().getIdsv0003().getSqlcode().setSqlcode(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-SQL
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           END-IF.
        if (!ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE SQLCODE             TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(sqlca.getSqlcode());
            // COB_CODE: STRING 'ERRORE ESTRAZIONE CURRENT TIMESTAMP'
            //                   ' - '
            //                   'SQLCODE : '
            //                   WK-SQLCODE
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE ESTRAZIONE CURRENT TIMESTAMP", " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: A160-SET-CURRENT-DATE<br>
	 * <pre>----------------------------------------------------------------*
	 *  SETTA TIMESTAMP
	 * ----------------------------------------------------------------*</pre>*/
    private void a160SetCurrentDate() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A160-SET-DATE'             TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A160-SET-DATE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: PERFORM ESTRAI-CURRENT-DATE
        //                   THRU ESTRAI-CURRENT-DATE-EX
        estraiCurrentDate();
        // COB_CODE: MOVE SQLCODE       TO IDSV0003-SQLCODE
        ws.getIabv0007().getIdsv0003().getSqlcode().setSqlcode(sqlca.getSqlcode());
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-SQL
        //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           END-IF.
        if (!ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
            // COB_CODE: INITIALIZE                  WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE SQLCODE             TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(sqlca.getSqlcode());
            // COB_CODE: STRING 'ERRORE ESTRAZIONE CURRENT DATE'
            //                   ' - '
            //                   'SQLCODE : '
            //                   WK-SQLCODE
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE ESTRAZIONE CURRENT DATE", " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: A200-APRI-FILE<br>
	 * <pre>----------------------------------------------------------------*
	 *  APRI ARCHIVI                                                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void a200ApriFile() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A200-APRI-FILE'            TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A200-APRI-FILE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: OPEN INPUT PBTCEXEC
            pbtcexecDAO.open(OpenMode.READ, "Lrgm0380");
            ws.getIabv0007().setFsPara(pbtcexecDAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: IF FS-PARA    NOT = '00'
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           ELSE
            //              SET FILE-PARAM-APERTO TO TRUE
            //           END-IF
            if (!Conditions.eq(ws.getIabv0007().getFsPara(), "00")) {
                // COB_CODE: INITIALIZE     WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'OPEN ERRATA DEL FILE '
                //                   DELIMITED BY SIZE
                //                  'PBTCEXEC'
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   FS-PARA
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "OPEN ERRATA DEL FILE ", "PBTCEXEC", " - ", "RC : ", ws.getIabv0007().getFsParaFormatted());
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
            else {
                // COB_CODE: SET FILE-PARAM-APERTO TO TRUE
                ws.getIabv0007().setWkOpenParam(true);
            }
        }
    }

    /**Original name: A888-CALL-IABS0140<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALL IABS0140
	 * ----------------------------------------------------------------*</pre>*/
    private void a888CallIabs0140() {
        ConcatUtil concatUtil = null;
        Iabs0140 iabs0140 = null;
        // COB_CODE: MOVE 'A888-CALL-IABS0140'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A888-CALL-IABS0140");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL PGM-IABS0140                 USING IDSV0003
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL
        try {
            iabs0140 = Iabs0140.getInstance();
            iabs0140.run(ws.getIabv0007().getIdsv0003());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
            //                   DELIMITED BY SIZE
            //                   PGM-IABS0140
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0140Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:  IF NOT IDSV0003-SUCCESSFUL-RC
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-IF
            if (!ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0140
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0140Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
    }

    /**Original name: A999-ESEGUI-CONNECT<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESEGUI CONNECT
	 * ----------------------------------------------------------------*</pre>*/
    private void a999EseguiConnect() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A999-ESEGUI-CONNECT'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("A999-ESEGUI-CONNECT");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE IABV0008-DATABASE-NAME       TO AREA0140-DATABASE-NAME
        ws.getIabv0007().getAreaXIabs0140().setDatabaseName(ws.getIabv0008().getDatabaseName());
        // COB_CODE: MOVE IABV0008-USER                TO AREA0140-USER
        ws.getIabv0007().getAreaXIabs0140().setUser(ws.getIabv0008().getUser());
        // COB_CODE: MOVE IABV0008-PASSWORD            TO AREA0140-PASSWORD
        ws.getIabv0007().getAreaXIabs0140().setPassword(ws.getIabv0008().getPassword());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET AREA0140-CONNECT              TO TRUE.
        ws.getIabv0007().getAreaXIabs0140().getOperazione().setArea0140Connect();
        // COB_CODE: MOVE AREA-X-IABS0140     TO IDSV0003-BUFFER-WHERE-COND
        ws.getIabv0007().getIdsv0003().setBufferWhereCond(ws.getIabv0007().getAreaXIabs0140().getAreaXIabs0140Formatted());
        // COB_CODE: PERFORM A888-CALL-IABS0140 THRU A888-EX
        a888CallIabs0140();
        // COB_CODE: IF WK-ERRORE-NO
        //                 END-EVALUATE
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         EVALUATE TRUE
            //                      WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                           CONTINUE
            //                       WHEN IDSV0003-NEGATIVI
            //           *--->      ERRORE DI ACCESSO AL DB
            //                           PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //                      END-EVALUATE
            if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
            //-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE: CONTINUE
            //continue
            }
            else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                //--->      ERRORE DI ACCESSO AL DB
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING 'ERRORE CONNECT AL DB'
                //                  ' - '
                //                  'SQLCODE : '
                //                  WK-SQLCODE
                //                  DELIMITED BY SIZE INTO
                //                  WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CONNECT AL DB", " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
    }

    /**Original name: B050-ESTRAI-PARAMETRI<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAZIONE PARAMETRI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void b050EstraiParametri() {
        // COB_CODE: MOVE 'B050-ESTRAI-PARAMETRI'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B050-ESTRAI-PARAMETRI");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM A200-APRI-FILE        THRU A200-EX.
        a200ApriFile();
        // COB_CODE: PERFORM B100-READ-PARAM       THRU B100-EX.
        b100ReadParam();
        // COB_CODE: PERFORM B200-CONTROL-PARAM    THRU B200-EX
        //             UNTIL FILE-PARAM-FINITO
        //                OR WK-ERRORE-YES.
        while (!(ws.getIabv0007().isWkEofParam() || ws.getIabv0007().isWkErrore())) {
            b200ControlParam();
        }
    }

    /**Original name: B020-ESTRAI-DT-COMPETENZA<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAZIONE DATA COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void b020EstraiDtCompetenza() {
        ConcatUtil concatUtil = null;
        Idss0150 idss0150 = null;
        // COB_CODE: MOVE 'B020-ESTRAI-DT-COMPETENZA' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B020-ESTRAI-DT-COMPETENZA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: CALL PGM-IDSS0150               USING AREA-IDSV0001
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL
        try {
            idss0150 = Idss0150.getInstance();
            idss0150.run(ws.getIabv0007().getAreaIdsv0001());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
            //                   DELIMITED BY SIZE
            //                   PGM-IDSS0150
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIdss0150Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                      END-IF
            //                   ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getAreaIdsv0001().getEsito().isIdsv0001EsitoOk()) {
                //-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: IF IABI0011-LIVELLO-DEBUG IS NUMERIC AND
                //              IABI0011-LIVELLO-DEBUG NOT = ZEROES
                //                             TO IDSV0001-LIVELLO-DEBUG
                //           END-IF
                if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getLivelloDebug().getIdsi0011LivelloDebugFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getLivelloDebug().getIdsi0011LivelloDebugFormatted())) {
                    // COB_CODE: MOVE IABI0011-LIVELLO-DEBUG
                    //                          TO IDSV0001-LIVELLO-DEBUG
                    ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().setIdsi0011LivelloDebugFormatted(ws.getIabv0007().getIabi0011Area().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IDSS0150
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0001-DESC-ERRORE-ESTESA
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIdss0150Formatted(), " - ", ws.getIabv0007().getAreaIdsv0001().getLogErrore().getDescErroreEstesaFormatted());
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: SET  WK-ERRORE-FATALE    TO TRUE
                ws.getIabv0007().getWkTipoErrore().setFatale();
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: B060-VALORIZZA-AREA-CONTESTO<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZA AREA CONTESTO
	 * ----------------------------------------------------------------*</pre>*/
    private void b060ValorizzaAreaContesto() {
        // COB_CODE: MOVE 'B060-VALORIZZA-AREA-CONTESTO' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B060-VALORIZZA-AREA-CONTESTO");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM VARYING IX-ADDRESS FROM 1 BY 1
        //                   UNTIL   IX-ADDRESS > 5
        //                   MOVE SPACE      TO IDSV0001-ADDRESS-TYPE(IX-ADDRESS)
        //           END-PERFORM
        ws.getIabv0007().setIxAddress(1);
        while (!(ws.getIabv0007().getIxAddress() > 5)) {
            // COB_CODE: MOVE SPACE      TO IDSV0001-ADDRESS-TYPE(IX-ADDRESS)
            ws.getIabv0007().getAreaIdsv0001().getAreaComune().getAddresses(ws.getIabv0007().getIxAddress()).setType(Types.SPACE_CHAR);
            ws.getIabv0007().setIxAddress(Trunc.toInt(ws.getIabv0007().getIxAddress() + 1, 2));
        }
        // COB_CODE: MOVE ZERO                          TO IX-ADDRESS1.
        ws.getIabv0007().setIxAddress1(0);
        // COB_CODE: INITIALIZE IJCCMQ04-VAR-AMBIENTE.
        initIjccmq04VarAmbiente();
        // COB_CODE: PERFORM VARYING IX-ADDRESS FROM 1 BY 1
        //                UNTIL   IX-ADDRESS  > 5
        //                END-IF
        //           END-PERFORM.
        ws.getIabv0007().setIxAddress(1);
        while (!(ws.getIabv0007().getIxAddress() > 5)) {
            // COB_CODE: MOVE IABI0011-ADDRESS-TYPE(IX-ADDRESS) TO
            //                TIPO-ADDRESS
            ws.getIabv0007().getIabc0010().getTipoAddress().setTipoAddress(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()));
            // COB_CODE: IF IABI0011-ADDRESS-TYPE(IX-ADDRESS) NOT = SPACE
            //                                            AND NOT = HIGH-VALUE
            //                                            AND NOT = LOW-VALUE
            //                                            AND NOT ADDRESS-AREA-MQ
            //              END-IF
            //           END-IF
            if (!Conditions.eq(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()), Types.SPACE_CHAR) && !Conditions.eq(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()), Types.HIGH_CHAR_VAL) && !Conditions.eq(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()), Types.LOW_CHAR_VAL) && !ws.getIabv0007().getIabc0010().getTipoAddress().isAddressAreaMq()) {
                // COB_CODE: ADD  1                      TO IX-ADDRESS1
                ws.getIabv0007().setIxAddress1(Trunc.toInt(1 + ws.getIabv0007().getIxAddress1(), 2));
                // COB_CODE: MOVE IABI0011-ADDRESS-TYPE(IX-ADDRESS) TO
                //                IDSV0001-ADDRESS-TYPE(IX-ADDRESS1)
                ws.getIabv0007().getAreaIdsv0001().getAreaComune().getAddresses(ws.getIabv0007().getIxAddress1()).setType(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()));
                // COB_CODE: IF   ADDRESS-VAR-AMBIENTE
                //                   THRU A103-EX
                //           END-IF
                if (ws.getIabv0007().getIabc0010().getTipoAddress().isAddressVarAmbiente()) {
                    // COB_CODE: SET IDSV0001-ADDRESS        (IX-ADDRESS1)
                    //            TO ADDRESS OF IJCCMQ04-VAR-AMBIENTE
                    ws.getIabv0007().getAreaIdsv0001().getAreaComune().getAddresses(ws.getIabv0007().getIxAddress1()).setS(pointerManager.addressOf(ws.getIabv0007().getIjccmq04VarAmbiente()));
                    // COB_CODE: PERFORM A103-LEGGI-PARAM-INFR-APPL
                    //              THRU A103-EX
                    a103LeggiParamInfrAppl();
                }
            }
            ws.getIabv0007().setIxAddress(Trunc.toInt(ws.getIabv0007().getIxAddress() + 1, 2));
        }
        // COB_CODE: PERFORM VARYING IX-ADDRESS FROM 1 BY 1
        //                UNTIL   IX-ADDRESS  > 5
        //                END-IF
        //           END-PERFORM.
        ws.getIabv0007().setIxAddress(1);
        while (!(ws.getIabv0007().getIxAddress() > 5)) {
            // COB_CODE: MOVE IABI0011-ADDRESS-TYPE(IX-ADDRESS) TO
            //                TIPO-ADDRESS
            ws.getIabv0007().getIabc0010().getTipoAddress().setTipoAddress(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()));
            // COB_CODE: IF ADDRESS-AREA-MQ AND
            //              IJCCMQ04-MQ
            //                 THRU A105-EX
            //           END-IF
            if (ws.getIabv0007().getIabc0010().getTipoAddress().isAddressAreaMq() && ws.getIabv0007().getIjccmq04VarAmbiente().isMq()) {
                // COB_CODE: ADD  1                      TO IX-ADDRESS1
                ws.getIabv0007().setIxAddress1(Trunc.toInt(1 + ws.getIabv0007().getIxAddress1(), 2));
                // COB_CODE: MOVE TIPO-ADDRESS           TO
                //                IDSV0001-ADDRESS-TYPE(IX-ADDRESS1)
                ws.getIabv0007().getAreaIdsv0001().getAreaComune().getAddresses(ws.getIabv0007().getIxAddress1()).setType(ws.getIabv0007().getIabc0010().getTipoAddress().getTipoAddress());
                // COB_CODE: SET IDSV0001-ADDRESS        (IX-ADDRESS1)
                //                       TO ADDRESS OF W-AREA-PER-CHIAMATE-WSMQ
                ws.getIabv0007().getAreaIdsv0001().getAreaComune().getAddresses(ws.getIabv0007().getIxAddress1()).setS(pointerManager.addressOf(ws.getIabv0007().getwAreaPerChiamateWsmq()));
                // COB_CODE: PERFORM A105-ATTIVA-CONNESSIONE
                //              THRU A105-EX
                a105AttivaConnessione();
            }
            ws.getIabv0007().setIxAddress(Trunc.toInt(ws.getIabv0007().getIxAddress() + 1, 2));
        }
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                                     TO IDSV0003-MODALITA-ESECUTIVA
        ws.getIabv0007().getIdsv0003().getModalitaEsecutiva().setModalitaEsecutiva(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IABI0011-CODICE-COMPAGNIA-ANIA
        //                                     TO IDSV0001-COD-COMPAGNIA-ANIA
        //                                        IDSV0003-CODICE-COMPAGNIA-ANIA
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setIdsv0001CodCompagniaAniaFormatted(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAniaFormatted());
        ws.getIabv0007().getIdsv0003().setCodiceCompagniaAnia(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                                     TO IDSV0003-COD-MAIN-BATCH
        ws.getIabv0007().getIdsv0003().setCodMainBatch(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getCodMainBatch());
        // COB_CODE: IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
        //              IABI0011-TIPO-MOVIMENTO NOT = ZEROES
        //                                           IDSV0003-TIPO-MOVIMENTO
        //           END-IF
        if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted())) {
            // COB_CODE: MOVE IABI0011-TIPO-MOVIMENTO
            //                                     TO IDSV0001-TIPO-MOVIMENTO
            //                                        IDSV0003-TIPO-MOVIMENTO
            ws.getIabv0007().getAreaIdsv0001().getAreaComune().setIdsv0001TipoMovimentoFormatted(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted());
            ws.getIabv0007().getIdsv0003().setTipoMovimentoFormatted(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted());
        }
        // COB_CODE: MOVE IABI0011-LINGUA         TO IDSV0001-LINGUA
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setLingua(ws.getIabv0007().getIabi0011Area().getLingua());
        // COB_CODE: MOVE IABI0011-PAESE          TO IDSV0001-PAESE
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setPaese(ws.getIabv0007().getIabi0011Area().getPaese());
        // COB_CODE: MOVE IABI0011-USER-NAME      TO IDSV0001-USER-NAME
        //                                           IDSV0003-USER-NAME
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setUserName(ws.getIabv0007().getIabi0011Area().getUserName());
        ws.getIabv0007().getIdsv0003().setUserName(ws.getIabv0007().getIabi0011Area().getUserName());
        // COB_CODE: MOVE IABI0011-KEY-BUSINESS1  TO IDSV0001-KEY-BUSINESS1.
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setKeyBusiness1(ws.getIabv0007().getIabi0011Area().getKeyBusiness1());
        // COB_CODE: MOVE IABI0011-KEY-BUSINESS2  TO IDSV0001-KEY-BUSINESS2.
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setKeyBusiness2(ws.getIabv0007().getIabi0011Area().getKeyBusiness2());
        // COB_CODE: MOVE IABI0011-KEY-BUSINESS3  TO IDSV0001-KEY-BUSINESS3.
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setKeyBusiness3(ws.getIabv0007().getIabi0011Area().getKeyBusiness3());
        // COB_CODE: MOVE IABI0011-KEY-BUSINESS4  TO IDSV0001-KEY-BUSINESS4.
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setKeyBusiness4(ws.getIabv0007().getIabi0011Area().getKeyBusiness4());
        // COB_CODE: MOVE IABI0011-KEY-BUSINESS5  TO IDSV0001-KEY-BUSINESS5.
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setKeyBusiness5(ws.getIabv0007().getIabi0011Area().getKeyBusiness5());
        // COB_CODE: PERFORM B070-ESTRAI-SESSIONE THRU B070-EX
        b070EstraiSessione();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: MOVE IDSV0001-SESSIONE    TO IDSV0003-SESSIONE
            ws.getIabv0007().getIdsv0003().setSessione(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getSessione());
            // COB_CODE: MOVE IABV0008-TRATTAMENTO-STORICITA
            //                            TO IDSV0001-TRATTAMENTO-STORICITA
            //                               IDSV0003-TRATTAMENTO-STORICITA
            ws.getIabv0007().getAreaIdsv0001().getAreaComune().getTrattamentoStoricita().setTrattamentoStoricita(ws.getIabv0008().getTrattamentoStoricita().getTrattamentoStoricita());
            ws.getIabv0007().getIdsv0003().getTrattamentoStoricita().setTrattamentoStoricita(ws.getIabv0008().getTrattamentoStoricita().getTrattamentoStoricita());
            // COB_CODE: MOVE IABV0008-FORMATO-DATA-DB
            //                            TO IDSV0001-FORMATO-DATA-DB
            //                               IDSV0003-FORMATO-DATA-DB
            ws.getIabv0007().getAreaIdsv0001().getAreaComune().getFormatoDataDb().setFormatoDataDb(ws.getIabv0008().getFormatoDataDb().getFormatoDataDb());
            ws.getIabv0007().getIdsv0003().getFormatoDataDb().setFormatoDataDb(ws.getIabv0008().getFormatoDataDb().getFormatoDataDb());
            // COB_CODE: IF IABI0011-DATA-EFFETTO IS NUMERIC   AND
            //              IABI0011-DATA-EFFETTO NOT = ZEROES
            //                                       IDSV0003-DATA-INIZIO-EFFETTO
            //           ELSE
            //                                    IDSV0003-DATA-INIZIO-EFFETTO
            //           END-IF
            if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getDataEffettoFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getDataEffettoFormatted())) {
                // COB_CODE: MOVE IABI0011-DATA-EFFETTO
                //                                 TO IDSV0001-DATA-EFFETTO
                //                                    IDSV0003-DATA-INIZIO-EFFETTO
                ws.getIabv0007().getAreaIdsv0001().getAreaComune().setIdsv0001DataEffettoFormatted(ws.getIabv0007().getIabi0011Area().getDataEffettoFormatted());
                ws.getIabv0007().getIdsv0003().setDataInizioEffetto(ws.getIabv0007().getIabi0011Area().getDataEffetto());
            }
            else {
                // COB_CODE: PERFORM A160-SET-CURRENT-DATE THRU A160-EX
                a160SetCurrentDate();
                // COB_CODE: MOVE    WS-DATE-N     TO IDSV0001-DATA-EFFETTO
                //                                 IDSV0003-DATA-INIZIO-EFFETTO
                ws.getIabv0007().getAreaIdsv0001().getAreaComune().setIdsv0001DataEffettoFormatted(ws.getIabv0007().getWsDateNFormatted());
                ws.getIabv0007().getIdsv0003().setDataInizioEffetto(ws.getIabv0007().getWsDateN());
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: IF NOT IABI0011-NO-TEMP-TABLE
                //              SET IDSV0001-ID-TMPRY-DATA-SI  TO TRUE
                //           END-IF
                if (!ws.getIabv0007().getIabi0011Area().getTemporaryTable().isNoTempTable()) {
                    // COB_CODE: SET IDSV0001-ID-TMPRY-DATA-SI  TO TRUE
                    ws.getIabv0007().getAreaIdsv0001().getAreaComune().getFlagIdTmpryData().setIdsv0001IdTmpryDataSi();
                }
                // COB_CODE: PERFORM B020-ESTRAI-DT-COMPETENZA THRU B020-EX
                b020EstraiDtCompetenza();
                // COB_CODE: IF WK-ERRORE-NO
                //                                TO IDSV0003-DATA-COMP-AGG-STOR
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF IABI0011-TS-COMPETENZA IS NUMERIC   AND
                    //              IABI0011-TS-COMPETENZA NOT = ZEROES
                    //                               IDSV0003-DATA-COMPETENZA
                    //           ELSE
                    //                            TO IDSV0003-DATA-COMPETENZA
                    //           END-IF
                    if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getTsCompetenzaFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getTsCompetenzaFormatted())) {
                        // COB_CODE: MOVE IABI0011-TS-COMPETENZA
                        //                         TO IDSV0001-DATA-COMPETENZA
                        //                            IDSV0003-DATA-COMPETENZA
                        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setIdsv0001DataCompetenzaFormatted(ws.getIabv0007().getIabi0011Area().getTsCompetenzaFormatted());
                        ws.getIabv0007().getIdsv0003().setDataCompetenza(ws.getIabv0007().getIabi0011Area().getTsCompetenza());
                    }
                    else {
                        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
                        //                         TO IDSV0003-DATA-COMPETENZA
                        ws.getIabv0007().getIdsv0003().setDataCompetenza(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getIdsv0001DataCompetenza());
                    }
                    // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
                    //                             TO IDSV0003-DATA-COMP-AGG-STOR
                    ws.getIabv0007().getIdsv0003().setDataCompAggStor(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getIdsv0001DataCompAggStor());
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-EVALUATE
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN IABI0011-NO-TEMP-TABLE
            //                   SET IDSV0001-NO-TEMP-TABLE      TO TRUE
            //              WHEN IABI0011-STATIC-TEMP-TABLE
            //                   SET IDSV0001-STATIC-TEMP-TABLE  TO TRUE
            //              WHEN IABI0011-SESSION-TEMP-TABLE
            //                   SET IDSV0001-SESSION-TEMP-TABLE TO TRUE
            //           END-EVALUATE
            switch (ws.getIabv0007().getIabi0011Area().getTemporaryTable().getTemporaryTable()) {

                case Iabi0011TemporaryTable.NO_TEMP_TABLE:// COB_CODE: SET IDSV0001-NO-TEMP-TABLE      TO TRUE
                    ws.getIabv0007().getAreaIdsv0001().getAreaComune().getTemporaryTable().setIdsv0001NoTempTable();
                    break;

                case Iabi0011TemporaryTable.STATIC_TEMP_TABLE:// COB_CODE: SET IDSV0001-STATIC-TEMP-TABLE  TO TRUE
                    ws.getIabv0007().getAreaIdsv0001().getAreaComune().getTemporaryTable().setIdsv0001StaticTempTable();
                    break;

                case Iabi0011TemporaryTable.SESSION_TEMP_TABLE:// COB_CODE: SET IDSV0001-SESSION-TEMP-TABLE TO TRUE
                    ws.getIabv0007().getAreaIdsv0001().getAreaComune().getTemporaryTable().setIdsv0001SessionTempTable();
                    break;

                default:break;
            }
        }
    }

    /**Original name: B070-ESTRAI-SESSIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAI SESSIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void b070EstraiSessione() {
        ConcatUtil concatUtil = null;
        Lccs0090 lccs0090 = null;
        // COB_CODE: MOVE 'B070-ESTRAI-SESSIONE'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B070-ESTRAI-SESSIONE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: MOVE SPACES                   TO LCCC0090.
        ws.getIabv0007().getLccc0090().initLccc0090Spaces();
        // COB_CODE: MOVE 'SEQ_SESSIONE'           TO LINK-NOME-TABELLA.
        ws.getIabv0007().getLccc0090().setNomeTabella("SEQ_SESSIONE");
        // COB_CODE: CALL PGM-LCCS0090             USING LCCC0090
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL
        try {
            lccs0090 = Lccs0090.getInstance();
            lccs0090.run(ws.getIabv0007().getLccc0090());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
            //                   DELIMITED BY SIZE
            //                   PGM-LCCS0090
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmLccs0090Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF LINK-RETURN-CODE NOT = ZERO
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           ELSE
            //              MOVE WK-SEQ-SESSIONE    TO IDSV0001-SESSIONE
            //           END-IF
            if (!Characters.EQ_ZERO.test(ws.getIabv0007().getLccc0090().getReturnCode().getReturnCodeFormatted())) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE IDSV0003-SQLCODE  TO WK-SQLCODE
                ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-LCCS0090
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   ' RC : '
                //                   DELIMITED BY SIZE
                //                   LINK-RETURN-CODE
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   ' SQLCODE : '
                //                   DELIMITED BY SIZE
                //                   LINK-SQLCODE
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   LINK-DESCRIZ-ERR-DB2
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmLccs0090Formatted(), " - ", " RC : ", ws.getIabv0007().getLccc0090().getReturnCode().getReturnCodeFormatted(), " - ", " SQLCODE : ", ws.getIabv0007().getLccc0090().getSqlcode().getIdso0021SqlcodeAsString(), " - ", ws.getIabv0007().getLccc0090().getDescrizErrDb2Formatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
            else {
                // COB_CODE: MOVE LINK-SEQ-TABELLA   TO WK-SEQ-SESSIONE
                ws.getIabv0007().setWkSeqSessione(TruncAbs.toInt(ws.getIabv0007().getLccc0090().getSeqTabella(), 9));
                // COB_CODE: MOVE WK-SEQ-SESSIONE    TO IDSV0001-SESSIONE
                ws.getIabv0007().getAreaIdsv0001().getAreaComune().setSessione(ws.getIabv0007().getWkSeqSessioneFormatted());
            }
        }
    }

    /**Original name: B000-ELABORA-MACRO<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORAZIONE                                                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void b000ElaboraMacro() {
        // COB_CODE: MOVE 'B000-ELABORA-MACRO'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B000-ELABORA-MACRO");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM B050-ESTRAI-PARAMETRI      THRU B050-EX.
        b050EstraiParametri();
        // COB_CODE: IF WK-ERRORE-NO
        //            END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM B300-CTRL-INPUT-SKEDA   THRU B300-EX
            b300CtrlInputSkeda();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: IF IABV0008-CONNESSIONE-YES
                //              PERFORM A999-ESEGUI-CONNECT  THRU A999-EX
                //           END-IF
                if (ws.getIabv0008().isFlagConnessioneDb()) {
                    // COB_CODE: PERFORM A999-ESEGUI-CONNECT  THRU A999-EX
                    a999EseguiConnect();
                }
                // COB_CODE: IF WK-ERRORE-NO
                //             END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: PERFORM A100-INIZIO-ELABORA    THRU A100-EX
                    a100InizioElabora();
                    // COB_CODE: IF WK-ERRORE-NO
                    //            END-IF
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM B060-VALORIZZA-AREA-CONTESTO THRU B060-EX
                        b060ValorizzaAreaContesto();
                        // COB_CODE: SET  IDSV8888-ARCH-BATCH-DBG    TO TRUE
                        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888ArchBatchDbg();
                        // COB_CODE: MOVE WK-PGM                     TO IDSV8888-NOME-PGM
                        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getWkPgm());
                        // COB_CODE: MOVE 'Architettura Batch'       TO IDSV8888-DESC-PGM
                        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Architettura Batch");
                        // COB_CODE: SET  IDSV8888-INIZIO            TO TRUE
                        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
                        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                        eseguiDisplay();
                        // COB_CODE: IF WK-ERRORE-NO
                        //             END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: PERFORM C000-ESTRAI-STATI    THRU C000-EX
                            c000EstraiStati();
                            // COB_CODE: IF WK-ERRORE-NO
                            //             END-IF
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: PERFORM X001-CARICA-STATI-BATCH   THRU X001-EX
                                x001CaricaStatiBatch();
                                // COB_CODE: IF GESTIONE-PARALLELISMO-YES
                                //                TO IABV0002-STATE-ELE(IND-STATI-V0002 + 1)
                                //           END-IF
                                if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
                                    // COB_CODE: MOVE BATCH-IN-ESECUZIONE
                                    //             TO IABV0002-STATE-ELE(IND-STATI-V0002 + 1)
                                    ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setIabv0002StateEle(ws.getIabv0007().getIndStatiV0002() + 1, ws.getIabv0007().getIabv0004().getBatchInEsecuzione());
                                }
                                // COB_CODE: IF IABI0011-SESSION-TEMP-TABLE
                                //              PERFORM D999-SESSION-TABLE        THRU D999-EX
                                //           END-IF
                                if (ws.getIabv0007().getIabi0011Area().getTemporaryTable().isSessionTempTable()) {
                                    // COB_CODE: MOVE IDSV0001-TEMPORARY-TABLE
                                    //                              TO IDSV0301-TEMPORARY-TABLE
                                    ws.getIabv0007().getIdsv0301().getTemporaryTable().setTemporaryTable(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getTemporaryTable().getTemporaryTable());
                                    // COB_CODE: PERFORM D999-SESSION-TABLE        THRU D999-EX
                                    d999SessionTable();
                                }
                                // COB_CODE: IF WK-ERRORE-NO
                                //              END-IF
                                //           END-IF
                                if (!ws.getIabv0007().isWkErrore()) {
                                    // COB_CODE: IF WK-PROTOCOL-YES
                                    //              PERFORM D001-ESTRAI-BATCH-PROT THRU D001-EX
                                    //           ELSE
                                    //              PERFORM D000-ESTRAI-BATCH      THRU D000-EX
                                    //           END-IF
                                    if (ws.getIabv0007().isWkProtocol()) {
                                        // COB_CODE: PERFORM D001-ESTRAI-BATCH-PROT THRU D001-EX
                                        d001EstraiBatchProt();
                                    }
                                    else {
                                        // COB_CODE: PERFORM D000-ESTRAI-BATCH      THRU D000-EX
                                        d000EstraiBatch();
                                    }
                                    // COB_CODE: IF WK-ERRORE-NO
                                    //              END-IF
                                    //           END-IF
                                    if (!ws.getIabv0007().isWkErrore()) {
                                        // COB_CODE: IF WK-ELABORA-BATCH-NO
                                        //                                        THRU W070-EX
                                        //           END-IF
                                        if (!ws.getIabv0007().isFlagElaboraBatch()) {
                                            // COB_CODE: INITIALIZE WK-LOG-ERRORE
                                            initWkLogErrore();
                                            // COB_CODE: MOVE 'NON ESISTONO BATCH ESEGUIBILI'
                                            //                        TO WK-LOR-DESC-ERRORE-ESTESA
                                            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa("NON ESISTONO BATCH ESEGUIBILI");
                                            // COB_CODE: SET WK-RC-04                    TO TRUE
                                            ws.getIabv0007().getFlagReturnCode().setRc04();
                                            // COB_CODE: SET WK-ERRORE-NON-BLOCCANTE     TO TRUE
                                            ws.getIabv0007().getWkTipoErrore().setNonBloccante();
                                            // COB_CODE: MOVE WARNING TO WK-LOR-ID-GRAVITA-ERRORE
                                            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getWarning());
                                            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                                            //                                      THRU W070-EX
                                            w070CaricaLogErrBatExec();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0001-MAX-ELE-ERRORI > 0
        //              PERFORM W000-CARICA-LOG-ERRORE          THRU W000-EX
        //           END-IF.
        if (ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori() > 0) {
            // COB_CODE: PERFORM W000-CARICA-LOG-ERRORE          THRU W000-EX
            w000CaricaLogErrore();
        }
    }

    /**Original name: B100-READ-PARAM<br>
	 * <pre>----------------------------------------------------------------*
	 *  LEGGI FILE PARAM
	 * ----------------------------------------------------------------*</pre>*/
    private void b100ReadParam() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'B100-READ-PARAM'           TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B100-READ-PARAM");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: READ PBTCEXEC.
        pbtcexecTo = pbtcexecDAO.read(pbtcexecTo);
        ws.getIabv0007().setFsPara(pbtcexecDAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: EVALUATE FS-PARA
        //              WHEN '00'
        //                 ADD  1                      TO CNT-NUM-OPERAZ-LETTE
        //              WHEN '10'
        //                 SET FILE-PARAM-FINITO       TO TRUE
        //              WHEN OTHER
        //                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-EVALUATE.
        switch (ws.getIabv0007().getFsPara()) {

            case "00":// COB_CODE: ADD  1                      TO CNT-NUM-OPERAZ-LETTE
                ws.getIabv0007().setCntNumOperazLette(Trunc.toInt(1 + ws.getIabv0007().getCntNumOperazLette(), 9));
                break;

            case "10":// COB_CODE: SET FILE-PARAM-FINITO       TO TRUE
                ws.getIabv0007().setWkEofParam(true);
                break;

            default:// COB_CODE: INITIALIZE               WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
                ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
                // COB_CODE: MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE
                ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'READ ERRATA DEL FILE '
                //                   DELIMITED BY SIZE
                //                   'PBTCEXEC'
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'FS : '
                //                   DELIMITED BY SIZE
                //                   FS-PARA
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "READ ERRATA DEL FILE ", "PBTCEXEC", " - ", "FS : ", ws.getIabv0007().getFsParaFormatted());
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
                break;
        }
    }

    /**Original name: B200-CONTROL-PARAM<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLA PARAMETRI
	 * ----------------------------------------------------------------*</pre>*/
    private void b200ControlParam() {
        // COB_CODE: MOVE 'B200-CONTROL-PARAM'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B200-CONTROL-PARAM");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF PARAM-REC(1:1) = '*' OR '$'
        //              END-IF
        //           ELSE
        //              PERFORM B210-ESTRAI-LUNG-PARAM THRU B210-EX
        //           END-IF
        if (Conditions.eq(pbtcexecTo.getVariableFormatted().substring((1) - 1, 1), "*") || Conditions.eq(pbtcexecTo.getVariableFormatted().substring((1) - 1, 1), "$")) {
            // COB_CODE: IF PARAM-REC(1:1) = '$'
            //              MOVE 1                          TO IND-POSIZ-PARAM
            //           END-IF
            if (Conditions.eq(pbtcexecTo.getVariableFormatted().substring((1) - 1, 1), "$")) {
                // COB_CODE: SET PRIMA-BUFFER-SK-TROVATO-YES TO TRUE
                ws.getIabv0007().setWkPrimaBufferSkTrovato(true);
                // COB_CODE: MOVE 1                          TO IND-POSIZ-PARAM
                ws.getIabv0007().setIndPosizParam(((short)1));
            }
        }
        else {
            // COB_CODE: PERFORM B210-ESTRAI-LUNG-PARAM THRU B210-EX
            b210EstraiLungParam();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              PERFORM B100-READ-PARAM        THRU B100-EX
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM B100-READ-PARAM        THRU B100-EX
            b100ReadParam();
        }
    }

    /**Original name: B210-ESTRAI-LUNG-PARAM<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAI LUNGHEZZA PARAMETRI
	 * ----------------------------------------------------------------*</pre>*/
    private void b210EstraiLungParam() {
        // COB_CODE: MOVE 'B210-ESTRAI-LUNG-PARAM'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B210-ESTRAI-LUNG-PARAM");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: INITIALIZE WK-LUNG-PARAM-X
        //                      IND-LUNG
        ws.getIabv0007().getWkLungParamX().setWkLungParamX("");
        ws.getIabv0007().setIndLungFormatted("0");
        // COB_CODE: SET INIZIO-LETT-LUNG-NO   TO TRUE
        ws.getIabv0007().setFlagInizioLettura(false);
        // COB_CODE: SET FINE-LETT-LUNG-NO     TO TRUE
        ws.getIabv0007().setFlagFineLettura(false);
        // COB_CODE: SET POSIZ-PARAM-NO        TO TRUE
        ws.getIabv0007().setFlagPosizParam(false);
        // COB_CODE: PERFORM VARYING IND-SEARCH FROM 1 BY 1
        //               UNTIL IND-SEARCH = WK-LIMITE-SKEDA-PARAMETRO
        //               OR    POSIZ-PARAM-YES
        //                     END-IF
        //           END-PERFORM.
        ws.getIabv0007().setIndSearch(((short)1));
        while (!(ws.getIabv0007().getIndSearch() == ws.getIabv0007().getWkLimiteSkedaParametro() || ws.getIabv0007().isFlagPosizParam())) {
            // COB_CODE: IF PARAM-REC(IND-SEARCH:1) = '('
            //              SET INIZIO-LETT-LUNG-YES TO TRUE
            //           ELSE
            //              END-IF
            //           END-IF
            if (Conditions.eq(pbtcexecTo.getVariableFormatted().substring((ws.getIabv0007().getIndSearch()) - 1, ws.getIabv0007().getIndSearch()), "(")) {
                // COB_CODE: SET INIZIO-LETT-LUNG-YES TO TRUE
                ws.getIabv0007().setFlagInizioLettura(true);
            }
            else if (Conditions.eq(pbtcexecTo.getVariableFormatted().substring((ws.getIabv0007().getIndSearch()) - 1, ws.getIabv0007().getIndSearch()), ")")) {
                // COB_CODE: IF PARAM-REC(IND-SEARCH:1) = ')'
                //              SET FINE-LETT-LUNG-YES   TO TRUE
                //           ELSE
                //              END-IF
                //           END-IF
                // COB_CODE: SET FINE-LETT-LUNG-YES   TO TRUE
                ws.getIabv0007().setFlagFineLettura(true);
            }
            else if (Conditions.eq(pbtcexecTo.getVariableFormatted().substring((ws.getIabv0007().getIndSearch()) - 1, ws.getIabv0007().getIndSearch()), ":")) {
                // COB_CODE: IF PARAM-REC(IND-SEARCH:1) = ':'
                //              END-IF
                //           ELSE
                //              END-IF
                //           END-IF
                // COB_CODE: SET POSIZ-PARAM-YES   TO TRUE
                ws.getIabv0007().setFlagPosizParam(true);
                // COB_CODE: IF PARAM-REC(IND-SEARCH:1) = 'N'
                //                           THRU B211-EX
                //           ELSE
                //             END-IF
                //           END-IF
                if (Conditions.eq(pbtcexecTo.getVariableFormatted().substring((ws.getIabv0007().getIndSearch()) - 1, ws.getIabv0007().getIndSearch()), "N")) {
                    // COB_CODE: PERFORM B211-CNTL-NUMERICO
                    //                        THRU B211-EX
                    b211CntlNumerico();
                }
                else if (ws.getIabv0007().isWkPrimaBufferSkTrovato()) {
                    // COB_CODE: IF PRIMA-BUFFER-SK-TROVATO-YES
                    //                              WK-LUNG-PARAM-9)
                    //           ELSE
                    //                               WK-LUNG-PARAM-9)
                    //           END-IF
                    // COB_CODE: MOVE PARAM-REC(IND-SEARCH + 1 :
                    //                      WK-LUNG-PARAM-9)
                    //           TO IABI0011-BUFFER-WHERE-COND
                    //                       (IND-POSIZ-PARAM :
                    //                            WK-LUNG-PARAM-9)
                    ws.getIabv0007().getIabi0011Area().setBufferWhereCondSubstring(pbtcexecTo.getVariableFormatted().substring((ws.getIabv0007().getIndSearch() + 1) - 1, ws.getIabv0007().getIndSearch() + 1 + ws.getIabv0007().getWkLungParamX().getWkLungParam9() - 1), ws.getIabv0007().getIndPosizParam(), ws.getIabv0007().getWkLungParamX().getWkLungParam9());
                }
                else {
                    // COB_CODE: MOVE PARAM-REC(IND-SEARCH + 1 :
                    //                      WK-LUNG-PARAM-9)
                    //           TO IABI0011-AREA(IND-POSIZ-PARAM :
                    //                            WK-LUNG-PARAM-9)
                    ws.getIabv0007().getIabi0011Area().setIabi0011AreaFormatted(Functions.setSubstring(ws.getIabv0007().getIabi0011Area().getIabi0011AreaFormatted(), pbtcexecTo.getVariableFormatted().substring((ws.getIabv0007().getIndSearch() + 1) - 1, ws.getIabv0007().getIndSearch() + 1 + ws.getIabv0007().getWkLungParamX().getWkLungParam9() - 1), ws.getIabv0007().getIndPosizParam(), ws.getIabv0007().getWkLungParamX().getWkLungParam9()));
                }
            }
            else if (ws.getIabv0007().isFlagInizioLettura() && !ws.getIabv0007().isFlagFineLettura()) {
                // COB_CODE: IF INIZIO-LETT-LUNG-YES AND
                //              FINE-LETT-LUNG-NO
                //              END-IF
                //           END-IF
                // COB_CODE: IF PARAM-REC(IND-SEARCH:1)
                //                                   IS NUMERIC
                //                   TO WK-LUNG-PARAM-X(IND-LUNG:1)
                //           END-IF
                if (Functions.isNumber(pbtcexecTo.getVariableFormatted().substring((ws.getIabv0007().getIndSearch()) - 1, ws.getIabv0007().getIndSearch()))) {
                    // COB_CODE: ADD 1  TO IND-LUNG
                    ws.getIabv0007().setIndLung(Trunc.toShort(1 + ws.getIabv0007().getIndLung(), 1));
                    // COB_CODE: MOVE PARAM-REC(IND-SEARCH:1)
                    //                TO WK-LUNG-PARAM-X(IND-LUNG:1)
                    ws.getIabv0007().getWkLungParamX().setWkLungParamX(Functions.setSubstring(ws.getIabv0007().getWkLungParamX().getWkLungParamX(), pbtcexecTo.getVariableFormatted().substring((ws.getIabv0007().getIndSearch()) - 1, ws.getIabv0007().getIndSearch()), ws.getIabv0007().getIndLung(), 1));
                }
            }
            ws.getIabv0007().setIndSearch(Trunc.toShort(ws.getIabv0007().getIndSearch() + 1, 2));
        }
        // COB_CODE: IF INIZIO-LETT-LUNG-YES      AND
        //              FINE-LETT-LUNG-YES        AND
        //              POSIZ-PARAM-YES
        //                                           WK-LUNG-PARAM-9
        //           ELSE
        //              PERFORM Z300-SKEDA-PAR-ERRATA   THRU Z300-EX
        //           END-IF.
        if (ws.getIabv0007().isFlagInizioLettura() && ws.getIabv0007().isFlagFineLettura() && ws.getIabv0007().isFlagPosizParam()) {
            // COB_CODE: COMPUTE IND-POSIZ-PARAM = IND-POSIZ-PARAM +
            //                                        WK-LUNG-PARAM-9
            ws.getIabv0007().setIndPosizParam(Trunc.toShort(ws.getIabv0007().getIndPosizParam() + ws.getIabv0007().getWkLungParamX().getWkLungParam9(), 3));
        }
        else {
            // COB_CODE: PERFORM Z300-SKEDA-PAR-ERRATA   THRU Z300-EX
            z300SkedaParErrata();
        }
    }

    /**Original name: B211-CNTL-NUMERICO<br>
	 * <pre>----------------------------------------------------------------*
	 *  NORMALIZZA CAMPO NUMERICO
	 * ----------------------------------------------------------------*</pre>*/
    private void b211CntlNumerico() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'B211-CNTL-NUMERICO'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B211-CNTL-NUMERICO");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF PARAM-REC(IND-SEARCH + 1 : WK-LUNG-PARAM-9)
        //                                     IS NOT NUMERIC
        //                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           END-IF.
        if (!Functions.isNumber(pbtcexecTo.getVariableFormatted().substring((ws.getIabv0007().getIndSearch() + 1) - 1, ws.getIabv0007().getIndSearch() + 1 + ws.getIabv0007().getWkLungParamX().getWkLungParam9() - 1))) {
            // COB_CODE: INITIALIZE               WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
            //                   DELIMITED BY SIZE
            //                   'CAMPO NUMERICO'
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "CAMPO NUMERICO");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: B300-CTRL-INPUT-SKEDA<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLO INPUT SKEDA PARAMTERI
	 * ----------------------------------------------------------------*</pre>*/
    private void b300CtrlInputSkeda() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'B300-CTRL-INPUT-SKEDA'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B300-CTRL-INPUT-SKEDA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF IABI0011-PROTOCOL = LOW-VALUE      OR
        //                                  HIGH-VALUES    OR
        //                                  SPACES
        //              END-IF
        //           ELSE
        //             MOVE IABI0011-PROTOCOL     TO BTC-PROTOCOL
        //           END-IF
        if (Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getProtocol(), Iabi0011Area.Len.PROTOCOL) || Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getProtocol(), Iabi0011Area.Len.PROTOCOL) || Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getProtocol())) {
            // COB_CODE: IF IABI0011-TIPO-BATCH NOT NUMERIC OR
            //              IABI0011-TIPO-BATCH = ZEROES
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           ELSE
            //                              TO BTC-COD-COMP-ANIA
            //           END-IF
            if (!Functions.isNumber(ws.getIabv0007().getIabi0011Area().getTipoBatchFormatted()) || Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getTipoBatchFormatted())) {
                // COB_CODE: INITIALIZE           WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
                // COB_CODE: MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE
                ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                //                   DELIMITED BY SIZE
                //                   'PROTOCOL/TIPO '
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "PROTOCOL/TIPO ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
            else {
                // COB_CODE: SET WK-PROTOCOL-NO         TO TRUE
                ws.getIabv0007().setWkProtocol(false);
                // COB_CODE: MOVE IABI0011-TIPO-BATCH
                //                           TO BTC-COD-BATCH-TYPE
                ws.getBtcBatch().setBtcCodBatchType(ws.getIabv0007().getIabi0011Area().getTipoBatch());
                // COB_CODE: MOVE IABI0011-CODICE-COMPAGNIA-ANIA
                //                           TO BTC-COD-COMP-ANIA
                ws.getBtcBatch().setBtcCodCompAnia(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAnia());
            }
        }
        else {
            // COB_CODE: SET WK-PROTOCOL-YES        TO TRUE
            ws.getIabv0007().setWkProtocol(true);
            // COB_CODE: MOVE IABI0011-PROTOCOL     TO BTC-PROTOCOL
            ws.getBtcBatch().setBtcProtocol(ws.getIabv0007().getIabi0011Area().getProtocol());
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-PROG-PROTOCOL IS NUMERIC AND
            //              IABI0011-PROG-PROTOCOL NOT = ZEROES
            //              SET GESTIONE-PARALLELISMO-YES TO TRUE
            //           END-IF
            if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getProgProtocolFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getProgProtocolFormatted())) {
                // COB_CODE: SET GESTIONE-PARALLELISMO-YES TO TRUE
                ws.getIabv0007().getFlagGestioneParallelismo().setYes();
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-CODICE-COMPAGNIA-ANIA NOT NUMERIC OR
            //              IABI0011-CODICE-COMPAGNIA-ANIA = ZEROES
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           ELSE
            //                                    TO BTC-COD-COMP-ANIA
            //           END-IF
            if (!Functions.isNumber(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAniaFormatted()) || Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAniaFormatted())) {
                // COB_CODE: INITIALIZE           WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
                // COB_CODE: MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE
                ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                //                   DELIMITED BY SIZE
                //                   'CODICE COMPAGNIA '
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "CODICE COMPAGNIA ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
            else {
                // COB_CODE: MOVE IABI0011-CODICE-COMPAGNIA-ANIA
                //                                 TO BTC-COD-COMP-ANIA
                ws.getBtcBatch().setBtcCodCompAnia(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAnia());
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-LINGUA = HIGH-VALUE OR
            //                                LOW-VALUE  OR
            //                                SPACES
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getLinguaFormatted()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getLinguaFormatted()) || Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getLingua())) {
                // COB_CODE: INITIALIZE           WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
                // COB_CODE: MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE
                ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                //                   DELIMITED BY SIZE
                //                   'LINGUA '
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "LINGUA ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-PAESE = HIGH-VALUE OR
            //                               LOW-VALUE  OR
            //                               SPACES
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getPaeseFormatted()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getPaeseFormatted()) || Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getPaese())) {
                // COB_CODE: INITIALIZE           WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
                // COB_CODE: MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE
                ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                //                   DELIMITED BY SIZE
                //                   'PAESE '
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "PAESE ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-USER-NAME = HIGH-VALUE OR
            //                                   LOW-VALUE  OR
            //                                   SPACES
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getUserNameFormatted()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getUserNameFormatted()) || Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getUserName())) {
                // COB_CODE: INITIALIZE           WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
                // COB_CODE: MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE
                ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                //                   DELIMITED BY SIZE
                //                   'USER NAME'
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "USER NAME");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //               END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-GRUPPO-AUTORIZZANTE NOT NUMERIC OR
            //              IABI0011-GRUPPO-AUTORIZZANTE = ZEROES
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //            END-IF
            if (!Functions.isNumber(ws.getIabv0007().getIabi0011Area().getGruppoAutorizzanteFormatted()) || Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getGruppoAutorizzanteFormatted())) {
                // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                //                   DELIMITED BY SIZE
                //                   'GRUPPO AUTORIZZANTE'
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "GRUPPO AUTORIZZANTE");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //               END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-CANALE-VENDITA NOT NUMERIC
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //            END-IF
            if (!Functions.isNumber(ws.getIabv0007().getIabi0011Area().getCanaleVenditaFormatted())) {
                // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                //                   DELIMITED BY SIZE
                //                   'CANALE VENDITA'
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "CANALE VENDITA");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-PUNTO-VENDITA = HIGH-VALUE OR
            //                                       LOW-VALUE OR
            //                                       SPACES
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getPuntoVenditaFormatted()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getPuntoVenditaFormatted()) || Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getPuntoVendita())) {
                // COB_CODE: INITIALIZE           WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
                // COB_CODE: MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE
                ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                //                   DELIMITED BY SIZE
                //                   'PUNTO VENDITA'
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "PUNTO VENDITA");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF BATCH-EXECUTOR-STD-NO
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().getFlagBatchExecutorStd().isNo()) {
                // COB_CODE: IF IABI0011-MACROFUNZIONALITA NOT = HIGH-VALUE AND
                //              IABI0011-MACROFUNZIONALITA NOT = LOW-VALUE  AND
                //              IABI0011-MACROFUNZIONALITA NOT = SPACES
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalitaFormatted()) && !Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalitaFormatted()) && !Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalita())) {
                    // COB_CODE: INITIALIZE           WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                    ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
                    // COB_CODE: MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE
                    ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'MACROFUNZIONALITA NON AMMESSA'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "MACROFUNZIONALITA NON AMMESSA");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
                    //              IABI0011-TIPO-MOVIMENTO NOT = ZEROES
                    //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    //           END-IF
                    if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted())) {
                        // COB_CODE: INITIALIZE           WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                        // COB_CODE: MOVE WK-LABEL      TO WK-LOR-LABEL-ERR
                        ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
                        // COB_CODE: MOVE WK-PGM        TO WK-LOR-COD-SERVIZIO-BE
                        ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                        //                   DELIMITED BY SIZE
                        //                   'TIPO MOVIMENTO NON AMMESSO'
                        //                   DELIMITED BY SIZE INTO
                        //                   WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "TIPO MOVIMENTO NON AMMESSO");
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                        z400ErrorePreDb();
                    }
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-DATA-EFFETTO IS NUMERIC   AND
            //              IABI0011-DATA-EFFETTO NOT = ZEROES
            //                 END-IF
            //           END-IF
            if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getDataEffettoFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getDataEffettoFormatted())) {
                // COB_CODE: MOVE IABI0011-DATA-EFFETTO(1:4)
                //                TO IDSV0016-AAAA
                ws.getIabv0007().getIdsv0016().getIdsv0016Data().setAaaaFormatted(ws.getIabv0007().getIabi0011Area().getDataEffettoFormatted().substring((1) - 1, 4));
                // COB_CODE: MOVE IABI0011-DATA-EFFETTO(5:2)
                //                TO IDSV0016-MM
                ws.getIabv0007().getIdsv0016().getIdsv0016Data().setMmFormatted(ws.getIabv0007().getIabi0011Area().getDataEffettoFormatted().substring((5) - 1, 6));
                // COB_CODE: MOVE IABI0011-DATA-EFFETTO(7:2)
                //                TO IDSV0016-GG
                ws.getIabv0007().getIdsv0016().getIdsv0016Data().setGgFormatted(ws.getIabv0007().getIabi0011Area().getDataEffettoFormatted().substring((7) - 1, 8));
                // COB_CODE: PERFORM CNTL-FORMALE-DATA
                //                   THRU CNTL-FORMALE-DATA-EX
                cntlFormaleData();
                // COB_CODE: IF WK-ERRORE-YES
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'DATA EFFETTO'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "DATA EFFETTO");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-FLAG-STOR-ESECUZ-YES AND
            //              NOT IABI0011-FLAG-STOR-ESECUZ-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().isYes() && !ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().isNo()) {
                // COB_CODE: IF IABI0011-FLAG-STOR-ESECUZ =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                   TO IABI0011-FLAG-STOR-ESECUZ
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Conditions.eq(ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().getFlagStorEsecuz(), Types.SPACE_CHAR) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().getFlagStorEsecuz(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().getFlagStorEsecuz(), Types.HIGH_CHAR_VAL)) {
                    // COB_CODE: MOVE IABV0008-FLAG-STOR-ESECUZ
                    //                TO IABI0011-FLAG-STOR-ESECUZ
                    ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().setFlagStorEsecuzFormatted(Booleans.Y_N.getFormattedValue(ws.getIabv0008().isFlagStorEsecuz()));
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'FLAG STOR. ESECUZ.'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "FLAG STOR. ESECUZ.");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-SIMULAZIONE-INFR     AND
            //              NOT IABI0011-SIMULAZIONE-FITTIZIA AND
            //              NOT IABI0011-SIMULAZIONE-APPL     AND
            //              NOT IABI0011-SIMULAZIONE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getFlagSimulazione().isInfr() && !ws.getIabv0007().getIabi0011Area().getFlagSimulazione().isFittizia() && !ws.getIabv0007().getIabi0011Area().getFlagSimulazione().isAppl() && !ws.getIabv0007().getIabi0011Area().getFlagSimulazione().isNo()) {
                // COB_CODE: IF IABI0011-FLAG-SIMULAZIONE =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                   TO IABI0011-FLAG-SIMULAZIONE
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Conditions.eq(ws.getIabv0007().getIabi0011Area().getFlagSimulazione().getFlagSimulazione(), Types.SPACE_CHAR) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getFlagSimulazione().getFlagSimulazione(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getFlagSimulazione().getFlagSimulazione(), Types.HIGH_CHAR_VAL)) {
                    // COB_CODE: MOVE IABV0008-FLAG-SIMULAZIONE
                    //                TO IABI0011-FLAG-SIMULAZIONE
                    ws.getIabv0007().getIabi0011Area().getFlagSimulazione().setFlagSimulazione(ws.getIabv0008().getFlagSimulazione().getFlagSimulazione());
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'FLAG SIMULAZIONE'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "FLAG SIMULAZIONE");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-TRATTAMENTO-PARTIAL AND
            //              NOT IABI0011-TRATTAMENTO-FULL
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().isPartial() && !ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().isFull()) {
                // COB_CODE: IF IABI0011-TRATTAMENTO-COMMIT =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                   TO IABI0011-TRATTAMENTO-COMMIT
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().getTrattamentoCommit()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().getTrattamentoCommitFormatted()) || Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().getTrattamentoCommitFormatted())) {
                    // COB_CODE: MOVE IABV0008-TRATTAMENTO-COMMIT
                    //                TO IABI0011-TRATTAMENTO-COMMIT
                    ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().setTrattamentoCommit(ws.getIabv0008().getTrattamentoCommit().getTrattamentoCommit());
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'TRATT. COMMIT'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "TRATT. COMMIT");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-SENZA-RIPARTENZA     AND
            //              NOT IABI0011-RIP-CON-MONITORING   AND
            //              NOT IABI0011-RIP-SENZA-MONITORING AND
            //              NOT IABI0011-RIP-CON-VERSIONAMENTO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isSenzaRipartenza() && !ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConMonitoring() && !ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipSenzaMonitoring() && !ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConVersionamento()) {
                // COB_CODE: IF IABI0011-GESTIONE-RIPARTENZA =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                   TO IABI0011-GESTIONE-RIPARTENZA
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().getGestioneRipartenza()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().getGestioneRipartenzaFormatted()) || Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().getGestioneRipartenzaFormatted())) {
                    // COB_CODE: MOVE IABV0008-GESTIONE-RIPARTENZA
                    //                TO IABI0011-GESTIONE-RIPARTENZA
                    ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().setGestioneRipartenza(ws.getIabv0008().getGestioneRipartenza().getGestioneRipartenza());
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'GESTIONE RIPARTENZA'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "GESTIONE RIPARTENZA");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-ORDER-BY-IB-OGG AND
            //              NOT IABI0011-ORDER-BY-ID-JOB
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getTipoOrderBy().isIbOgg() && !ws.getIabv0007().getIabi0011Area().getTipoOrderBy().isIdJob()) {
                // COB_CODE: IF IABI0011-TIPO-ORDER-BY =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                   TO IABI0011-TIPO-ORDER-BY
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getTipoOrderBy().getTipoOrderBy()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getTipoOrderBy().getTipoOrderByFormatted()) || Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getTipoOrderBy().getTipoOrderByFormatted())) {
                    // COB_CODE: MOVE IABV0008-TIPO-ORDER-BY
                    //                TO IABI0011-TIPO-ORDER-BY
                    ws.getIabv0007().getIabi0011Area().getTipoOrderBy().setTipoOrderBy(ws.getIabv0008().getTipoOrderBy().getTipoOrderBy());
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'TIPO ORDER BY'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "TIPO ORDER BY");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-GRAVITA-UNO     AND
            //              NOT IABI0011-GRAVITA-DUE     AND
            //              NOT IABI0011-GRAVITA-TRE     AND
            //              NOT IABI0011-GRAVITA-QUATTRO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().isUno() && !ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().isMcrfnct() && !ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().isTre() && !ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().isQuattro()) {
                // COB_CODE: IF IABI0011-LIVELLO-GRAVITA-LOG =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                   TO IABI0011-LIVELLO-GRAVITA-LOG
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Conditions.eq(ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().getFlagModCalcPreP(), Types.SPACE_CHAR) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().getFlagModCalcPreP(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().getFlagModCalcPreP(), Types.HIGH_CHAR_VAL)) {
                    // COB_CODE: MOVE IABV0008-LIVELLO-GRAVITA-LOG
                    //                TO IABI0011-LIVELLO-GRAVITA-LOG
                    ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().setFlagModCalcPreP(ws.getIabv0008().getLivelloGravitaLog().getLivelloGravitaLog());
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'LIVELLO GRAVITA'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "LIVELLO GRAVITA");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-NO-TEMP-TABLE      AND
            //              NOT IABI0011-STATIC-TEMP-TABLE  AND
            //              NOT IABI0011-SESSION-TEMP-TABLE
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getTemporaryTable().isNoTempTable() && !ws.getIabv0007().getIabi0011Area().getTemporaryTable().isStaticTempTable() && !ws.getIabv0007().getIabi0011Area().getTemporaryTable().isSessionTempTable()) {
                // COB_CODE: IF IABI0011-TEMPORARY-TABLE =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                   TO IABI0011-TEMPORARY-TABLE
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getTemporaryTable().getTemporaryTable()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getTemporaryTable().getTemporaryTableFormatted()) || Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getTemporaryTable().getTemporaryTableFormatted())) {
                    // COB_CODE: MOVE IABV0008-TEMPORARY-TABLE
                    //                TO IABI0011-TEMPORARY-TABLE
                    ws.getIabv0007().getIabi0011Area().getTemporaryTable().setTemporaryTable(ws.getIabv0008().getTemporaryTable().getTemporaryTable());
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'TEMPORARY TABLE'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "TEMPORARY TABLE");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-DATA-1K  AND
            //              NOT IABI0011-DATA-2K  AND
            //              NOT IABI0011-DATA-3K  AND
            //              NOT IABI0011-DATA-10K
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getLengthDataGates().isData1k() && !ws.getIabv0007().getIabi0011Area().getLengthDataGates().isData2k() && !ws.getIabv0007().getIabi0011Area().getLengthDataGates().isData3k() && !ws.getIabv0007().getIabi0011Area().getLengthDataGates().isData10k()) {
                // COB_CODE: IF IABI0011-LENGTH-DATA-GATES =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                   TO IABI0011-LENGTH-DATA-GATES
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getLengthDataGates().getLengthDataGates()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getLengthDataGates().getLengthDataGatesFormatted()) || Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getLengthDataGates().getLengthDataGatesFormatted())) {
                    // COB_CODE: MOVE IABV0008-LENGTH-DATA-GATES
                    //                TO IABI0011-LENGTH-DATA-GATES
                    ws.getIabv0007().getIabi0011Area().getLengthDataGates().setLengthDataGates(ws.getIabv0008().getLengthDataGates().getLengthDataGates());
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'LENGTH DATA GATES'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "LENGTH DATA GATES");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-EVALUATE
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN IABI0011-DATA-1K
            //                   SET WK-LIMITE-ARRAY-BLOB-1K     TO TRUE
            //              WHEN IABI0011-DATA-2K
            //                   SET WK-LIMITE-ARRAY-BLOB-2K     TO TRUE
            //              WHEN IABI0011-DATA-3K
            //                   SET WK-LIMITE-ARRAY-BLOB-3K     TO TRUE
            //              WHEN IABI0011-DATA-10K
            //                   SET WK-LIMITE-ARRAY-BLOB-10K    TO TRUE
            //           END-EVALUATE
            switch (ws.getIabv0007().getIabi0011Area().getLengthDataGates().getLengthDataGates()) {

                case Iabi0011LengthDataGates.DATA1K:// COB_CODE: SET WK-LIMITE-ARRAY-BLOB-1K     TO TRUE
                    ws.getIabv0007().getWkLimiteArrayBlob().setBlob1k();
                    break;

                case Iabi0011LengthDataGates.DATA2K:// COB_CODE: SET WK-LIMITE-ARRAY-BLOB-2K     TO TRUE
                    ws.getIabv0007().getWkLimiteArrayBlob().setBlob2k();
                    break;

                case Iabi0011LengthDataGates.DATA3K:// COB_CODE: SET WK-LIMITE-ARRAY-BLOB-3K     TO TRUE
                    ws.getIabv0007().getWkLimiteArrayBlob().setBlob3k();
                    break;

                case Iabi0011LengthDataGates.DATA10K:// COB_CODE: SET WK-LIMITE-ARRAY-BLOB-10K    TO TRUE
                    ws.getIabv0007().getWkLimiteArrayBlob().setBlob10k();
                    break;

                default:break;
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-APPEND-DATA-GATES-YES AND
            //              NOT IABI0011-APPEND-DATA-GATES-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getAppendDataGates().isYes() && !ws.getIabv0007().getIabi0011Area().getAppendDataGates().isNo()) {
                // COB_CODE: IF IABI0011-APPEND-DATA-GATES =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                   TO IABI0011-APPEND-DATA-GATES
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Conditions.eq(ws.getIabv0007().getIabi0011Area().getAppendDataGates().getAppendDataGates(), Types.SPACE_CHAR) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getAppendDataGates().getAppendDataGates(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getAppendDataGates().getAppendDataGates(), Types.HIGH_CHAR_VAL)) {
                    // COB_CODE: MOVE IABV0008-APPEND-DATA-GATES
                    //                TO IABI0011-APPEND-DATA-GATES
                    ws.getIabv0007().getIabi0011Area().getAppendDataGates().setAppendDataGatesFormatted(Booleans.Y_N.getFormattedValue(ws.getIabv0008().isAppendDataGates()));
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'APPEND DATA GATES'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "APPEND DATA GATES");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IABI0011-APPEND-DATA-GATES-YES AND
            //              IABI0011-NO-TEMP-TABLE
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-IF
            if (ws.getIabv0007().getIabi0011Area().getAppendDataGates().isYes() && ws.getIabv0007().getIabi0011Area().getTemporaryTable().isNoTempTable()) {
                // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                //                   DELIMITED BY SIZE
                //                   'APPEND DATA GATES NON CONGRUENTE'
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "APPEND DATA GATES NON CONGRUENTE");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
        //
        // COB_CODE: IF WK-ERRORE-NO
        //              END-PERFORM
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM VARYING IX-ADDRESS FROM 1 BY 1
            //               UNTIL   IX-ADDRESS > 5
            //               END-IF
            //           END-PERFORM
            ws.getIabv0007().setIxAddress(1);
            while (!(ws.getIabv0007().getIxAddress() > 5)) {
                // COB_CODE: MOVE IABI0011-ADDRESS-TYPE(IX-ADDRESS) TO
                //                TIPO-ADDRESS
                ws.getIabv0007().getIabc0010().getTipoAddress().setTipoAddress(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()));
                // COB_CODE: IF NOT ADDRESS-CACHE-VALIDO
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (!ws.getIabv0007().getIabc0010().getTipoAddress().isAddressCacheValido()) {
                    // COB_CODE: STRING 'INPUT SK. PARAM. : '
                    //             DELIMITED BY SIZE
                    //             'ADDRESS TYPE NON VALIDO'
                    //             DELIMITED BY SIZE INTO
                    //             WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. : ", "ADDRESS TYPE NON VALIDO");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
                ws.getIabv0007().setIxAddress(Trunc.toInt(ws.getIabv0007().getIxAddress() + 1, 2));
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-PERFORM
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM VARYING IX-ADDRESS FROM 1 BY 1
            //               UNTIL   IX-ADDRESS > 4
            //               END-PERFORM
            //           END-PERFORM
            ws.getIabv0007().setIxAddress(1);
            while (!(ws.getIabv0007().getIxAddress() > 4)) {
                // COB_CODE: MOVE IX-ADDRESS    TO IX-ADDRESS1
                ws.getIabv0007().setIxAddress1(ws.getIabv0007().getIxAddress());
                // COB_CODE: ADD  1             TO IX-ADDRESS1
                ws.getIabv0007().setIxAddress1(Trunc.toInt(1 + ws.getIabv0007().getIxAddress1(), 2));
                // COB_CODE: PERFORM VARYING IX-ADDRESS1 FROM IX-ADDRESS1 BY 1
                //           UNTIL   IX-ADDRESS1 > 5
                //                 END-IF
                //           END-PERFORM
                ws.getIabv0007().setIxAddress1(ws.getIabv0007().getIxAddress1());
                while (!(ws.getIabv0007().getIxAddress1() > 5)) {
                    // COB_CODE: IF IABI0011-ADDRESS-TYPE(IX-ADDRESS) =
                    //              IABI0011-ADDRESS-TYPE(IX-ADDRESS1) AND
                    //             (IABI0011-ADDRESS-TYPE(IX-ADDRESS) NOT =
                    //              SPACE AND LOW-VALUE AND HIGH-VALUE)
                    //              MOVE 6     TO IX-ADDRESS IX-ADDRESS1
                    //           END-IF
                    if (ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()) == ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress1()) && !Conditions.eq(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()), Types.SPACE_CHAR) && !Conditions.eq(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()), Types.LOW_CHAR_VAL) && !Conditions.eq(ws.getIabv0007().getIabi0011Area().getAddressType(ws.getIabv0007().getIxAddress()), Types.HIGH_CHAR_VAL)) {
                        // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                        //             DELIMITED BY SIZE
                        //             'ADDRESS TYPE UGUALI'
                        //             DELIMITED BY SIZE INTO
                        //             WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "ADDRESS TYPE UGUALI");
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                        z400ErrorePreDb();
                        // COB_CODE: MOVE 6     TO IX-ADDRESS IX-ADDRESS1
                        ws.getIabv0007().setIxAddress(6);
                        ws.getIabv0007().setIxAddress1(6);
                    }
                    ws.getIabv0007().setIxAddress1(Trunc.toInt(ws.getIabv0007().getIxAddress1() + 1, 2));
                }
                ws.getIabv0007().setIxAddress(Trunc.toInt(ws.getIabv0007().getIxAddress() + 1, 2));
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-FORZ-RC-04-YES AND
            //              NOT IABI0011-FORZ-RC-04-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getForzRc04().isYes() && !ws.getIabv0007().getIabi0011Area().getForzRc04().isNo()) {
                // COB_CODE: IF IABI0011-FORZ-RC-04 =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //              CONTINUE
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Conditions.eq(ws.getIabv0007().getIabi0011Area().getForzRc04().getForzRc04(), Types.SPACE_CHAR) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getForzRc04().getForzRc04(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getIabi0011Area().getForzRc04().getForzRc04(), Types.HIGH_CHAR_VAL)) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                   DELIMITED BY SIZE
                    //                   'FORZATURA RC'
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "FORZATURA RC");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT IABI0011-ACCESSO-X-DT-EFF-MAX AND
            //              NOT IABI0011-ACCESSO-X-DT-EFF-MIN AND
            //              NOT IABI0011-ACCESSO-X-DT-INS-MAX AND
            //              NOT IABI0011-ACCESSO-X-DT-INS-MIN AND
            //              NOT IABI0011-ACCESSO-ALL-BATCH
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getAccessoBatch().isxDtEffMax() && !ws.getIabv0007().getIabi0011Area().getAccessoBatch().isxDtEffMin() && !ws.getIabv0007().getIabi0011Area().getAccessoBatch().isxDtInsMax() && !ws.getIabv0007().getIabi0011Area().getAccessoBatch().isxDtInsMin() && !ws.getIabv0007().getIabi0011Area().getAccessoBatch().isAllBatch()) {
                // COB_CODE: IF IABI0011-ACCESSO-BATCH =
                //              SPACES OR LOW-VALUE OR HIGH-VALUE
                //                TO IABI0011-ACCESSO-BATCH
                //           ELSE
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getAccessoBatch().getAccessoBatch()) || Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getAccessoBatch().getAccessoBatchFormatted()) || Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getAccessoBatch().getAccessoBatchFormatted())) {
                    // COB_CODE: MOVE IABV0008-ACCESSO-BATCH
                    //             TO IABI0011-ACCESSO-BATCH
                    ws.getIabv0007().getIabi0011Area().getAccessoBatch().setAccessoBatch(ws.getIabv0008().getAccessoBatch().getAccessoBatch());
                }
                else {
                    // COB_CODE: STRING 'INPUT SK. PARAM. NON VALIDO : '
                    //                  DELIMITED BY SIZE
                    //                  'ACCESSO BATCH'
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "INPUT SK. PARAM. NON VALIDO : ", "ACCESSO BATCH");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
    }

    /**Original name: C000-ESTRAI-STATI<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAI STATI
	 * ----------------------------------------------------------------*</pre>*/
    private void c000EstraiStati() {
        // COB_CODE: MOVE 'C000-ESTRAI-STATI'         TO WK-LABEL.
        ws.getIabv0007().setWkLabel("C000-ESTRAI-STATI");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: PERFORM C100-ESTRAI-STATI-BATCH  THRU C100-EX
        c100EstraiStatiBatch();
        // COB_CODE: IF WK-ERRORE-NO
        //              PERFORM C200-ESTRAI-STATI-JOB THRU C200-EX
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM C200-ESTRAI-STATI-JOB THRU C200-EX
            c200EstraiStatiJob();
        }
    }

    /**Original name: C100-ESTRAI-STATI-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAI STATI BATCH
	 * ----------------------------------------------------------------*</pre>*/
    private void c100EstraiStatiBatch() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'C100-ESTRAI-STATI-BATCH'   TO WK-LABEL.
        ws.getIabv0007().setWkLabel("C100-ESTRAI-STATI-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE ZEROES                    TO IND-STATI-BATCH
        ws.getIabv0007().setIndStatiBatch(((short)0));
        //----------------------------------------------------------------*
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC  OR
        //                         NOT IDSV0003-SUCCESSFUL-SQL OR
        //                         NOT WK-ERRORE-NO
        //            END-IF
        //           END-PERFORM.
        while (!(!ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc() || !ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql() || !!ws.getIabv0007().isWkErrore())) {
            // COB_CODE: PERFORM CALL-IABS0070        THRU CALL-IABS0070-EX
            callIabs0070();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE:          IF IDSV0003-SUCCESSFUL-RC
                //                      END-EVALUATE
                //                    ELSE
                //           *-->        GESTIRE ERRORE DISPATCHER
                //                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                    END-IF
                if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:            EVALUATE TRUE
                    //                         WHEN IDSV0003-SUCCESSFUL-SQL
                    //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                            END-IF
                    //                         WHEN IDSV0003-NOT-FOUND
                    //           *--->         CAMPO $ NON TROVATO
                    //                            END-IF
                    //                         WHEN IDSV0003-NEGATIVI
                    //           *--->         ERRORE DI ACCESSO AL DB
                    //                            PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    //                      END-EVALUATE
                    if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                        //-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: ADD 1    TO IND-STATI-BATCH
                        ws.getIabv0007().setIndStatiBatch(Trunc.toShort(1 + ws.getIabv0007().getIndStatiBatch(), 2));
                        // COB_CODE: IF IND-STATI-BATCH > WK-LIMITE-STATI-BATCH
                        //                                 THRU W070-EX
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getIabv0007().getIndStatiBatch() > ws.getIabv0007().getWkLimiteStatiBatch()) {
                            // COB_CODE: INITIALIZE        WK-LOG-ERRORE
                            initWkLogErrore();
                            // COB_CODE: STRING 'OVERFLOW STATI '
                            //                  DELIMITED BY SIZE
                            //                  'BATCH_STATE'
                            //                  DELIMITED BY SIZE
                            //                  INTO
                            //                  WK-LOR-DESC-ERRORE-ESTESA
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "OVERFLOW STATI ", "BATCH_STATE");
                            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                            // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                            //                              THRU W070-EX
                            w070CaricaLogErrBatExec();
                        }
                        else {
                            // COB_CODE: MOVE BBS-COD-BATCH-STATE
                            //                        TO BAT-COD-BATCH-STATE
                            //                                       (IND-STATI-BATCH)
                            ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).setCodBatchState(ws.getBtcBatchState().getCodElabState());
                            // COB_CODE: MOVE BBS-DES TO BAT-DES
                            //                                       (IND-STATI-BATCH)
                            ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).setDes(ws.getBtcBatchState().getDes());
                            // COB_CODE: MOVE BBS-FLAG-TO-EXECUTE
                            //                        TO BAT-FLAG-TO-EXECUTE
                            //                                       (IND-STATI-BATCH)
                            ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).setFlagToExecute(ws.getBtcBatchState().getFlagToExecute());
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            ws.getIabv0007().getIdsv0003().getOperazione().setFetchNext();
                        }
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                        //--->         CAMPO $ NON TROVATO
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //                                THRU W070-EX
                        //            END-IF
                        if (ws.getIabv0007().getIdsv0003().getOperazione().isFetchFirst()) {
                            // COB_CODE: INITIALIZE           WK-LOG-ERRORE
                            initWkLogErrore();
                            // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                            ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                            // COB_CODE: STRING 'ERRORE MODULO '
                            //                  DELIMITED BY SIZE
                            //                  PGM-IABS0070
                            //                  DELIMITED BY SIZE
                            //                  ' SQLCODE : '
                            //                  DELIMITED BY SIZE
                            //                  WK-SQLCODE
                            //                  DELIMITED BY SIZE INTO
                            //                  WK-LOR-DESC-ERRORE-ESTESA
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0070Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                            // COB_CODE: MOVE ERRORE-FATALE  TO WK-LOR-ID-GRAVITA-ERRORE
                            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                            //                             THRU W070-EX
                            w070CaricaLogErrBatExec();
                        }
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                        //--->         ERRORE DI ACCESSO AL DB
                        // COB_CODE: INITIALIZE               WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                        ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                        // COB_CODE: STRING 'ERRORE MODULO '
                        //                  DELIMITED BY SIZE
                        //                  PGM-IABS0070
                        //                  DELIMITED BY SIZE
                        //                  ' SQLCODE : '
                        //                  DELIMITED BY SIZE
                        //                  WK-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0070Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                        // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                        w070CaricaLogErrBatExec();
                    }
                }
                else {
                    //-->        GESTIRE ERRORE DISPATCHER
                    // COB_CODE: INITIALIZE  WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                   DELIMITED BY SIZE
                    //                   PGM-IABS0070
                    //                   DELIMITED BY SIZE
                    //                   ' - '
                    //                   DELIMITED BY SIZE
                    //                   IDSV0003-DESCRIZ-ERR-DB2
                    //                   DELIMITED BY '   '
                    //                   ' - '
                    //                   DELIMITED BY SIZE
                    //                   'RC : '
                    //                   DELIMITED BY SIZE
                    //                   IDSV0003-RETURN-CODE
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0070Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
        }
    }

    /**Original name: C200-ESTRAI-STATI-JOB<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAI STATI JOB
	 * ----------------------------------------------------------------*</pre>*/
    private void c200EstraiStatiJob() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'C200-ESTRAI-STATI-JOB'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("C200-ESTRAI-STATI-JOB");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE ZEROES                    TO IND-STATI-JOB
        ws.getIabv0007().setIndStatiJob(((short)0));
        //----------------------------------------------------------------*
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC  OR
        //                         NOT IDSV0003-SUCCESSFUL-SQL OR
        //                         NOT WK-ERRORE-NO
        //           END-IF
        //           END-PERFORM.
        while (!(!ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc() || !ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql() || !!ws.getIabv0007().isWkErrore())) {
            // COB_CODE: PERFORM CALL-IABS0030         THRU CALL-IABS0030-EX
            callIabs0030();
            // COB_CODE: IF WK-ERRORE-NO
            //             END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE:        IF IDSV0003-SUCCESSFUL-RC
                //                     END-EVALUATE
                //                  ELSE
                //           *-->      GESTIRE ERRORE DISPATCHER
                //                     PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                  END-IF
                if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:           EVALUATE TRUE
                    //                         WHEN IDSV0003-SUCCESSFUL-SQL
                    //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                            END-IF
                    //                         WHEN IDSV0003-NOT-FOUND
                    //           *--->         CAMPO $ NON TROVATO
                    //                            END-IF
                    //                         WHEN IDSV0003-NEGATIVI
                    //           *--->         ERRORE DI ACCESSO AL DB
                    //                            PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    //                     END-EVALUATE
                    if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                        //-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: ADD 1    TO IND-STATI-JOB
                        ws.getIabv0007().setIndStatiJob(Trunc.toShort(1 + ws.getIabv0007().getIndStatiJob(), 2));
                        // COB_CODE: IF IND-STATI-JOB > WK-LIMITE-STATI-JOB
                        //                                     THRU W070-EX
                        //           ELSE
                        //              SET IDSV0003-FETCH-NEXT   TO TRUE
                        //           END-IF
                        if (ws.getIabv0007().getIndStatiJob() > ws.getIabv0007().getWkLimiteStatiJob()) {
                            // COB_CODE: INITIALIZE WK-LOG-ERRORE
                            initWkLogErrore();
                            // COB_CODE: STRING 'OVERFLOW STATI '
                            //                  DELIMITED BY SIZE
                            //                  'ELAB_STATE'
                            //                  DELIMITED BY SIZE
                            //                  INTO
                            //                  WK-LOR-DESC-ERRORE-ESTESA
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "OVERFLOW STATI ", "ELAB_STATE");
                            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                            // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                            //                                  THRU W070-EX
                            w070CaricaLogErrBatExec();
                        }
                        else {
                            // COB_CODE: MOVE BES-COD-ELAB-STATE
                            //                                  TO JOB-COD-ELAB-STATE
                            //                                       (IND-STATI-JOB)
                            ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).setCodElabState(ws.getBtcElabState().getCodElabState());
                            // COB_CODE: MOVE BES-DES           TO JOB-DES
                            //                                       (IND-STATI-JOB)
                            ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).setDes(ws.getBtcElabState().getDes());
                            // COB_CODE: MOVE BES-FLAG-TO-EXECUTE
                            //                                  TO JOB-FLAG-TO-EXECUTE
                            //                                       (IND-STATI-JOB)
                            ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).setFlagToExecute(ws.getBtcElabState().getFlagToExecute());
                            // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                            ws.getIabv0007().getIdsv0003().getOperazione().setFetchNext();
                        }
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                        //--->         CAMPO $ NON TROVATO
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //                                 THRU W070-EX
                        //           END-IF
                        if (ws.getIabv0007().getIdsv0003().getOperazione().isFetchFirst()) {
                            // COB_CODE: INITIALIZE WK-LOG-ERRORE
                            initWkLogErrore();
                            // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                            ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                            // COB_CODE: STRING 'ERRORE MODULO '
                            //                  DELIMITED BY SIZE
                            //                  PGM-IABS0030
                            //                  DELIMITED BY SIZE
                            //                  ' SQLCODE : '
                            //                  DELIMITED BY SIZE
                            //                  WK-SQLCODE
                            //                  DELIMITED BY SIZE INTO
                            //                  WK-LOR-DESC-ERRORE-ESTESA
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0030Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                            // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                            //                              THRU W070-EX
                            w070CaricaLogErrBatExec();
                        }
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                        //--->         ERRORE DI ACCESSO AL DB
                        // COB_CODE: INITIALIZE WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                        ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                        // COB_CODE: STRING 'ERRORE MODULO '
                        //                  DELIMITED BY SIZE
                        //                  PGM-IABS0030
                        //                  DELIMITED BY SIZE
                        //                  ' SQLCODE : '
                        //                  DELIMITED BY SIZE
                        //                  WK-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0030Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                        // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                        w070CaricaLogErrBatExec();
                    }
                }
                else {
                    //-->      GESTIRE ERRORE DISPATCHER
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: STRING 'ERRORE MODULO  '
                    //                   DELIMITED BY SIZE
                    //                   PGM-IABS0030
                    //                   DELIMITED BY SIZE
                    //                   ' - '
                    //                   DELIMITED BY SIZE
                    //                   IDSV0003-DESCRIZ-ERR-DB2
                    //                   DELIMITED BY '   '
                    //                   ' - '
                    //                   DELIMITED BY SIZE
                    //                   'RC : '
                    //                   DELIMITED BY SIZE
                    //                   IDSV0003-RETURN-CODE
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO  ", ws.getIabv0007().getIabv0004().getPgmIabs0030Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
        }
    }

    /**Original name: D000-ESTRAI-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAZIONE BATCH X COD-OMP-ANIA AND BATCH-TYPE
	 * ----------------------------------------------------------------*</pre>*/
    private void d000EstraiBatch() {
        // COB_CODE: MOVE 'D000-ESTRAI-BATCH'         TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D000-ESTRAI-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE:      IF  IABI0011-ACCESSO-BATCH NOT = SPACES AND
        //                NOT IABI0011-ACCESSO-ALL-BATCH
        //                   PERFORM D006-CALL-IABS0040  THRU D006-EX
        //                ELSE
        //           *--> TIPO OPERAZIONE
        //                   END-PERFORM
        //                END-IF.
        if (!Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getAccessoBatch().getAccessoBatch()) && !ws.getIabv0007().getIabi0011Area().getAccessoBatch().isAllBatch()) {
            // COB_CODE: PERFORM D006-CALL-IABS0040  THRU D006-EX
            d006CallIabs0040();
        }
        else {
            //--> TIPO OPERAZIONE
            // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
            ws.getIabv0007().getIdsv0003().getOperazione().setFetchFirst();
            // COB_CODE: PERFORM UNTIL WK-FINE-BTC-BATCH-YES OR
            //                         NOT WK-ERRORE-NO
            //               PERFORM D005-CALL-IABS0040  THRU D005-EX
            //           END-PERFORM
            while (!(ws.getIabv0007().isFlagFineBtcBatch() || !!ws.getIabv0007().isWkErrore())) {
                // COB_CODE: PERFORM D005-CALL-IABS0040  THRU D005-EX
                d005CallIabs0040();
            }
        }
    }

    /**Original name: D001-ESTRAI-BATCH-PROT<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAZIONE BATCH PER PROTOCOLLO
	 * ----------------------------------------------------------------*</pre>*/
    private void d001EstraiBatchProt() {
        // COB_CODE: MOVE 'D001-ESTRAI-BATCH-PROT'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D001-ESTRAI-BATCH-PROT");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setSelect();
        // COB_CODE: PERFORM D005-CALL-IABS0040     THRU D005-EX.
        d005CallIabs0040();
    }

    /**Original name: D005-CALL-IABS0040<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL AL MODULO IABS0040
	 * ----------------------------------------------------------------*</pre>*/
    private void d005CallIabs0040() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'D005-CALL-IABS0040'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D005-CALL-IABS0040");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-PRIMARY-KEY                   TO TRUE.
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC                TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL               TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: SET WK-BAT-COLLECTION-KEY-OK               TO TRUE.
        ws.getIabv0007().setWkBatCollectionKey(true);
        // COB_CODE: SET WK-JOB-COLLECTION-KEY-OK               TO TRUE.
        ws.getIabv0007().setWkJobCollectionKey(true);
        // COB_CODE: SET VERIFICA-FINALE-JOBS-NO                TO TRUE
        ws.getIabv0007().getFlagVerificaFinaleJobs().setNo();
        // COB_CODE: PERFORM CALL-IABS0040         THRU CALL-IABS0040-EX
        callIabs0040();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                     END-EVALUATE
            //                   ELSE
            //           *-->      GESTIRE ERRORE DISPATCHER
            //                     PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:           EVALUATE TRUE
                //                         WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                //                            SET IDSV0003-FETCH-NEXT           TO TRUE
                //                         WHEN IDSV0003-NOT-FOUND
                //           *--->         CAMPO $ NON TROVATO
                //                              SET WK-FINE-BTC-BATCH-YES       TO TRUE
                //                         WHEN IDSV0003-NEGATIVI
                //           *--->         ERRORE DI ACCESSO AL DB
                //                                     THRU W070-EX
                //                     END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: PERFORM D100-ELABORA-BATCH        THRU D100-EX
                    d100ElaboraBatch();
                    // COB_CODE: SET IDSV0003-FETCH-NEXT           TO TRUE
                    ws.getIabv0007().getIdsv0003().getOperazione().setFetchNext();
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                    //--->         CAMPO $ NON TROVATO
                    // COB_CODE: SET WK-FINE-BTC-BATCH-YES       TO TRUE
                    ws.getIabv0007().setFlagFineBtcBatch(true);
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->         ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0040
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0040Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE   TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                    //                   THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //-->      GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0040
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0040Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: D006-CALL-IABS0040<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL AL MODULO IABS0040 X MIN/MAX DELLA DT-EFF/DT-INS
	 * ----------------------------------------------------------------*</pre>*/
    private void d006CallIabs0040() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'D006-CALL-IABS0040'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D006-CALL-IABS0040");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-WHERE-CONDITION-01 TO TRUE
        ws.getIabv0007().getIdsv0003().getTipologiaOperazione().setIdsv0003WhereCondition01();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION    TO TRUE
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setWhereCondition();
        // COB_CODE: MOVE IABI0011-ACCESSO-BATCH     TO IDSV0003-BUFFER-WHERE-COND
        ws.getIabv0007().getIdsv0003().setBufferWhereCond(ws.getIabv0007().getIabi0011Area().getAccessoBatch().getAccessoBatch());
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC                TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL               TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: SET WK-BAT-COLLECTION-KEY-OK               TO TRUE.
        ws.getIabv0007().setWkBatCollectionKey(true);
        // COB_CODE: SET WK-JOB-COLLECTION-KEY-OK               TO TRUE.
        ws.getIabv0007().setWkJobCollectionKey(true);
        // COB_CODE: SET VERIFICA-FINALE-JOBS-NO                TO TRUE
        ws.getIabv0007().getFlagVerificaFinaleJobs().setNo();
        // COB_CODE: PERFORM CALL-IABS0040         THRU CALL-IABS0040-EX
        callIabs0040();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                     END-EVALUATE
            //                   ELSE
            //           *-->      GESTIRE ERRORE DISPATCHER
            //                     PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:           EVALUATE TRUE
                //                         WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                //                            PERFORM D100-ELABORA-BATCH        THRU D100-EX
                //                         WHEN IDSV0003-NOT-FOUND
                //           *--->         CAMPO $ NON TROVATO
                //                              SET WK-FINE-BTC-BATCH-YES       TO TRUE
                //                         WHEN IDSV0003-NEGATIVI
                //           *--->         ERRORE DI ACCESSO AL DB
                //                                     THRU W070-EX
                //                     END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: PERFORM D100-ELABORA-BATCH        THRU D100-EX
                    d100ElaboraBatch();
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                    //--->         CAMPO $ NON TROVATO
                    // COB_CODE: SET WK-FINE-BTC-BATCH-YES       TO TRUE
                    ws.getIabv0007().setFlagFineBtcBatch(true);
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->         ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0040
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0040Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE   TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                    //                   THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //-->      GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0040
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0040Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: D100-ELABORA-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORAZIONE BATCH
	 * ----------------------------------------------------------------*</pre>*/
    private void d100ElaboraBatch() {
        // COB_CODE: MOVE 'D100-ELABORA-BATCH'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D100-ELABORA-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET IABV0006-PRIMO-LANCIO        TO TRUE
        ws.getIabv0006().getTipoLancioBus().setIabv0006PrimoLancio();
        // COB_CODE: SET WK-FINE-ALIMENTAZIONE-NO     TO TRUE
        ws.getIabv0007().setFlagFineAlimentazione(false);
        //
        // COB_CODE: MOVE BTC-DATA-BATCH            TO IABV0006-DATA-BATCH
        ws.getIabv0006().setDataBatch(ws.getBtcBatch().getBtcDataBatch());
        // COB_CODE: PERFORM D200-VERIFICA-BATCH THRU D200-EX
        d200VerificaBatch();
        // COB_CODE: IF WK-ERRORE-NO
        //            END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF WK-VERIFICA-BATCH-OK
            //            END-IF
            //           END-IF
            if (ws.getIabv0007().isFlagVerificaBatch()) {
                // COB_CODE: MOVE BTC-ID-BATCH            TO REP-ID-BATCH
                //                                           IABV0006-ID-BATCH
                ws.getIabv0007().getIabv0010().setRepIdBatch(ws.getBtcBatch().getBtcIdBatch());
                ws.getIabv0006().setIdBatch(ws.getBtcBatch().getBtcIdBatch());
                // COB_CODE: IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                //              BTC-ID-RICH      NOT = ZEROES
                //              PERFORM V100-VERIFICA-PRENOTAZIONE THRU V100-EX
                //           END-IF
                if (!Characters.EQ_HIGH.test(ws.getBtcBatch().getBtcIdRich().getBtcIdRichNullFormatted()) && ws.getBtcBatch().getBtcIdRich().getBtcIdRich() != 0) {
                    // COB_CODE: SET IDSV0003-WHERE-CONDITION-02    TO TRUE
                    ws.getIabv0007().getIdsv0003().getTipologiaOperazione().setIdsv0003WhereCondition02();
                    // COB_CODE: PERFORM V100-VERIFICA-PRENOTAZIONE THRU V100-EX
                    v100VerificaPrenotazione();
                }
                // COB_CODE: IF WK-VERIFICA-PRENOTAZIONE-OK
                //              END-IF
                //           END-IF
                if (ws.getIabv0007().isFlagVerificaPrenotazione()) {
                    // COB_CODE: SET WK-ELABORA-BATCH-YES TO TRUE
                    ws.getIabv0007().setFlagElaboraBatch(true);
                    // COB_CODE: MOVE RIC-FL-SIMULAZIONE  TO WK-SIMULAZIONE
                    ws.getIabv0007().getWkSimulazione().setWkSimulazione(ws.getRich().getRicFlSimulazione());
                    // COB_CODE: IF NOT WK-SIMULAZIONE-FITTIZIA AND
                    //              NOT WK-SIMULAZIONE-APPL     AND
                    //              NOT WK-SIMULAZIONE-NO
                    //                                   TO WK-SIMULAZIONE
                    //           END-IF
                    if (!ws.getIabv0007().getWkSimulazione().isFittizia() && !ws.getIabv0007().getWkSimulazione().isAppl() && !ws.getIabv0007().getWkSimulazione().isNo()) {
                        // COB_CODE: MOVE IABI0011-FLAG-SIMULAZIONE
                        //                                TO WK-SIMULAZIONE
                        ws.getIabv0007().getWkSimulazione().setWkSimulazione(ws.getIabv0007().getIabi0011Area().getFlagSimulazione().getFlagSimulazione());
                    }
                    // COB_CODE: MOVE WK-SIMULAZIONE     TO IABV0006-FLAG-SIMULAZIONE
                    ws.getIabv0006().getFlagSimulazione().setFlagSimulazione(ws.getIabv0007().getWkSimulazione().getFlagSimulazione());
                    // COB_CODE: IF IABI0011-RIP-CON-VERSIONAMENTO
                    //              MOVE BTC-ID-BATCH     TO WK-VERSIONING
                    //           END-IF
                    if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConVersionamento()) {
                        // COB_CODE: MOVE BTC-ID-BATCH     TO WK-VERSIONING
                        ws.getIabv0007().setWkVersioning(ws.getBtcBatch().getBtcIdBatch());
                    }
                    // COB_CODE: MOVE BTC-ID-BATCH        TO IABV0009-ID-BATCH
                    ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setIdBatch(ws.getBtcBatch().getBtcIdBatch());
                    // COB_CODE: PERFORM D300-ACCEDI-SU-BATCH-TYPE THRU D300-EX
                    d300AccediSuBatchType();
                    // COB_CODE: IF WK-ERRORE-NO
                    //              END-IF
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: IF STAMPA-TESTATA-JCL-YES
                        //              SET STAMPA-TESTATA-JCL-NO   TO TRUE
                        //           END-IF
                        if (ws.getIabv0007().isFlagStampaTestataJcl()) {
                            // COB_CODE: PERFORM S100-TESTATA-JCL    THRU S100-EX
                            s100TestataJcl();
                            // COB_CODE: SET STAMPA-TESTATA-JCL-NO   TO TRUE
                            ws.getIabv0007().setFlagStampaTestataJcl(false);
                        }
                        // COB_CODE: IF WK-ERRORE-NO
                        //             END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: PERFORM S170-SCRIVI-TESTATA-BATCH THRU S170-EX
                            s170ScriviTestataBatch();
                            // COB_CODE: IF WK-ERRORE-NO
                            //            END-IF
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: IF GESTIONE-PARALLELISMO-YES
                                //                                          THRU P000-EX
                                //           END-IF
                                if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
                                    // COB_CODE: PERFORM P000-PARALLELISMO-INIZIALE
                                    //                                       THRU P000-EX
                                    p000ParallelismoIniziale();
                                }
                                // COB_CODE: IF WK-ERRORE-NO
                                //             END-IF
                                //           END-IF
                                if (!ws.getIabv0007().isWkErrore()) {
                                    // COB_CODE: MOVE BATCH-IN-ESECUZIONE
                                    //                         TO IABV0002-STATE-CURRENT
                                    ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getBatchInEsecuzione());
                                    // COB_CODE: PERFORM A150-SET-CURRENT-TIMESTAMP
                                    //                                   THRU A150-EX
                                    a150SetCurrentTimestamp();
                                    // COB_CODE: MOVE WS-TIMESTAMP-N     TO BTC-DT-START
                                    ws.getBtcBatch().getBtcDtStart().setBtcDtStart(ws.getIabv0007().getWsTimestampN());
                                    // COB_CODE: MOVE IDSV0001-USER-NAME TO BTC-USER-START
                                    ws.getBtcBatch().setBtcUserStart(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getUserName());
                                    // COB_CODE: PERFORM D400-UPD-BATCH  THRU D400-EX
                                    d400UpdBatch();
                                    // COB_CODE: IF WK-ERRORE-NO
                                    //             END-IF
                                    //           END-IF
                                    if (!ws.getIabv0007().isWkErrore()) {
                                        // COB_CODE: IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                                        //              BTC-ID-RICH      NOT = ZEROES
                                        //                                          THRU D950-EX
                                        //           END-IF
                                        if (!Characters.EQ_HIGH.test(ws.getBtcBatch().getBtcIdRich().getBtcIdRichNullFormatted()) && ws.getBtcBatch().getBtcIdRich().getBtcIdRich() != 0) {
                                            // COB_CODE: SET IDSV0003-WHERE-CONDITION-02 TO TRUE
                                            ws.getIabv0007().getIdsv0003().getTipologiaOperazione().setIdsv0003WhereCondition02();
                                            // COB_CODE: SET IDSV0003-FIRST-ACTION       TO TRUE
                                            ws.getIabv0007().getIdsv0003().getLivelloOperazione().setIdsv0003FirstAction();
                                            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
                                            //                           TO RIC-TS-EFF-ESEC-RICH
                                            ws.getRich().getRicTsEffEsecRich().setRicTsEffEsecRich(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getIdsv0001DataCompetenza());
                                            // COB_CODE: PERFORM D950-AGGIORNA-PRENOTAZIONE
                                            //                                       THRU D950-EX
                                            d950AggiornaPrenotazione();
                                        }
                                        // COB_CODE: IF WK-ERRORE-NO
                                        //             END-IF
                                        //           END-IF
                                        if (!ws.getIabv0007().isWkErrore()) {
                                            // COB_CODE: PERFORM Y000-COMMIT  THRU Y000-EX
                                            y000Commit();
                                            // COB_CODE: MOVE BTC-ID-BATCH    TO WK-ID-BATCH
                                            ws.getIabv0007().setWkIdBatch(TruncAbs.toInt(ws.getBtcBatch().getBtcIdBatch(), 9));
                                            // COB_CODE: IF WK-ERRORE-NO
                                            //                                THRU E000-EX
                                            //           END-IF
                                            if (!ws.getIabv0007().isWkErrore()) {
                                                // COB_CODE: PERFORM E000-VERIFICA-JOB
                                                //                             THRU E000-EX
                                                e000VerificaJob();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    // COB_CODE: IF WK-ERRORE-NO
                    //              PERFORM S900-REPORT01O-BATCH  THRU S900-EX
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM S900-REPORT01O-BATCH  THRU S900-EX
                        s900Report01oBatch();
                    }
                }
            }
        }
    }

    /**Original name: D200-VERIFICA-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA BATCH
	 * ----------------------------------------------------------------*</pre>*/
    private void d200VerificaBatch() {
        // COB_CODE: MOVE 'D200-VERIFICA-BATCH'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D200-VERIFICA-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET WK-VERIFICA-BATCH-KO               TO TRUE
        ws.getIabv0007().setFlagVerificaBatch(false);
        // COB_CODE: IF BTC-COD-BATCH-STATE = BATCH-IN-ESECUZIONE
        //               END-IF
        //           ELSE
        //               END-IF
        //           END-IF.
        if (ws.getBtcBatch().getBtcCodBatchState() == ws.getIabv0007().getIabv0004().getBatchInEsecuzione()) {
            // COB_CODE: IF GESTIONE-PARALLELISMO-YES
            //              PERFORM P800-CNTL-INIZIALE      THRU P800-EX
            //           END-IF
            if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
                // COB_CODE: PERFORM P800-CNTL-INIZIALE      THRU P800-EX
                p800CntlIniziale();
            }
        }
        else if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
            // COB_CODE: IF GESTIONE-PARALLELISMO-YES
            //              PERFORM P800-CNTL-INIZIALE      THRU P800-EX
            //           ELSE
            //              SET WK-VERIFICA-BATCH-OK           TO TRUE
            //           END-IF
            // COB_CODE: PERFORM P800-CNTL-INIZIALE      THRU P800-EX
            p800CntlIniziale();
        }
        else {
            // COB_CODE: SET WK-VERIFICA-BATCH-OK           TO TRUE
            ws.getIabv0007().setFlagVerificaBatch(true);
        }
    }

    /**Original name: D300-ACCEDI-SU-BATCH-TYPE<br>
	 * <pre>----------------------------------------------------------------*
	 *  ACCEDI SU BATCH TYPE
	 * ----------------------------------------------------------------*</pre>*/
    private void d300AccediSuBatchType() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'D300-ACCEDI-SU-BATCH-TYPE' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D300-ACCEDI-SU-BATCH-TYPE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-PRIMARY-KEY           TO TRUE.
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-SELECT                TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setSelect();
        // COB_CODE: MOVE BTC-COD-BATCH-TYPE            TO BBT-COD-BATCH-TYPE
        ws.getBtcBatchType().setBbtCodBatchType(ws.getBtcBatch().getBtcCodBatchType());
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC        TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: PERFORM CALL-IABS0050              THRU CALL-IABS0050-EX
        callIabs0050();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                     END-EVALUATE
            //                   ELSE
            //           *-->    GESTIRE ERRORE DISPATCHER
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                       WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                //                          END-IF
                //                       WHEN IDSV0003-NOT-FOUND
                //           *--->       CAMPO $ NON TROVATO
                //                            PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                       WHEN IDSV0003-NEGATIVI
                //           *--->       ERRORE DI ACCESSO AL DB
                //                            PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                     END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //-->        OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: IF WK-PGM-BUSINESS =
                    //              SPACES OR HIGH-VALUE OR LOW-VALUE
                    //                               TO WK-PGM-BUSINESS
                    //           END-IF
                    if (Characters.EQ_SPACE.test(ws.getIabv0007().getWkPgmBusiness()) || Characters.EQ_HIGH.test(ws.getIabv0007().getWkPgmBusinessFormatted()) || Characters.EQ_LOW.test(ws.getIabv0007().getWkPgmBusinessFormatted())) {
                        // COB_CODE: MOVE BBT-SERVICE-NAME
                        //                            TO WK-PGM-BUSINESS
                        ws.getIabv0007().setWkPgmBusiness(ws.getBtcBatchType().getBbtServiceName());
                    }
                    // COB_CODE: IF WK-PGM-GUIDA =
                    //              SPACES OR HIGH-VALUE OR LOW-VALUE
                    //              END-IF
                    //           END-IF
                    if (Characters.EQ_SPACE.test(ws.getIabv0007().getWkPgmGuida()) || Characters.EQ_HIGH.test(ws.getIabv0007().getWkPgmGuidaFormatted()) || Characters.EQ_LOW.test(ws.getIabv0007().getWkPgmGuidaFormatted())) {
                        // COB_CODE: IF BBT-SERVICE-GUIDE-NAME NOT =
                        //              SPACES AND LOW-VALUES AND HIGH-VALUES
                        //              PERFORM S555-SWITCH-GUIDE-TYPE THRU S555-EX
                        //           END-IF
                        if (!Characters.EQ_SPACE.test(ws.getBtcBatchType().getBbtServiceGuideName()) && !Characters.EQ_LOW.test(ws.getBtcBatchType().getBbtServiceGuideName(), BtcBatchType.Len.BBT_SERVICE_GUIDE_NAME) && !Characters.EQ_HIGH.test(ws.getBtcBatchType().getBbtServiceGuideName(), BtcBatchType.Len.BBT_SERVICE_GUIDE_NAME)) {
                            // COB_CODE: SET IABV0006-GUIDE-AD-HOC   TO TRUE
                            ws.getIabv0006().getFlagProcessType().setIabv0006GuideAdHoc();
                            // COB_CODE: PERFORM S555-SWITCH-GUIDE-TYPE THRU S555-EX
                            s555SwitchGuideType();
                        }
                    }
                    // COB_CODE: IF NOT IABV0006-GUIDE-AD-HOC AND
                    //              NOT IABV0006-BY-BOOKING
                    //                                     TO WK-PGM-SERV-SECON-ROTT
                    //           END-IF
                    if (!ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() && !ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking()) {
                        // COB_CODE: MOVE IABI0011-BUFFER-WHERE-COND(01:08)
                        //                                  TO WK-PGM-SERV-ROTTURA
                        ws.setWkPgmServRottura(ws.getIabv0007().getIabi0011Area().getBufferWhereCondFormatted().substring((1) - 1, 8));
                        // COB_CODE: MOVE IABI0011-BUFFER-WHERE-COND(09:08)
                        //                                  TO WK-PGM-SERV-SECON-BUS
                        ws.setWkPgmServSeconBus(ws.getIabv0007().getIabi0011Area().getBufferWhereCondFormatted().substring((9) - 1, 16));
                        // COB_CODE: MOVE IABI0011-BUFFER-WHERE-COND(17:08)
                        //                                  TO WK-PGM-SERV-SECON-ROTT
                        ws.setWkPgmServSeconRott(ws.getIabv0007().getIabi0011Area().getBufferWhereCondFormatted().substring((17) - 1, 24));
                    }
                    // COB_CODE: IF BBT-TP-MOVI-NULL NOT = HIGH-VALUE AND
                    //              BBT-TP-MOVI NOT = ZEROES
                    //                                  IDSV0003-TIPO-MOVIMENTO
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getBtcBatchType().getBbtTpMovi().getBbtTpMoviNullFormatted()) && ws.getBtcBatchType().getBbtTpMovi().getBbtTpMovi() != 0) {
                        // COB_CODE: MOVE BBT-TP-MOVI TO IDSV0001-TIPO-MOVIMENTO
                        //                               IDSV0003-TIPO-MOVIMENTO
                        ws.getIabv0007().getAreaIdsv0001().getAreaComune().setIdsv0001TipoMovimento(TruncAbs.toInt(ws.getBtcBatchType().getBbtTpMovi().getBbtTpMovi(), 5));
                        ws.getIabv0007().getIdsv0003().setTipoMovimento(TruncAbs.toInt(ws.getBtcBatchType().getBbtTpMovi().getBbtTpMovi(), 5));
                    }
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                    //--->       CAMPO $ NON TROVATO
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO'
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0050
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO", ws.getIabv0007().getIabv0004().getPgmIabs0050Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->       ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO'
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0050
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO", ws.getIabv0007().getIabv0004().getPgmIabs0050Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //-->    GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0050
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0050Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: D400-UPD-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  MESSA IN ESECUZIONE DEL BATCH
	 * ----------------------------------------------------------------*</pre>*/
    private void d400UpdBatch() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'D400-UPD-BATCH'            TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D400-UPD-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-PRIMARY-KEY       TO TRUE.
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-UPDATE            TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setIdsi0011Update();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: PERFORM CALL-IABS0040         THRU CALL-IABS0040-EX
        callIabs0040();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                          WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                //                             CONTINUE
                //                          WHEN IDSV0003-NEGATIVI
                //           *--->          ERRORE DI ACCESSO AL DB
                //                                                          THRU W070-EX
                //                      END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                //-->           OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->          ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0040
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0040Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                    //                                      THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0040
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0040Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: D901-VERIFICA-ELAB-STATE<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA ELABT STATE
	 * ----------------------------------------------------------------*</pre>*/
    private void d901VerificaElabState() {
        // COB_CODE: MOVE 'D901-VERIFICA-ELAB-STATE'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D901-VERIFICA-ELAB-STATE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: SET WK-RISULT-VERIF-ELAB-STATE-KO      TO TRUE
        ws.getIabv0007().setFlagRisultVerifElabState(false);
        // COB_CODE: PERFORM VARYING IND-STATI-JOB FROM 1 BY 1
        //              UNTIL IND-STATI-JOB > WK-LIMITE-STATI-JOB OR
        //                    JOB-COD-ELAB-STATE(IND-STATI-JOB) =
        //                   ZEROES OR HIGH-VALUE OR SPACES OR LOW-VALUE
        //                   END-IF
        //           END-PERFORM.
        ws.getIabv0007().setIndStatiJob(((short)1));
        while (!(ws.getIabv0007().getIndStatiJob() > ws.getIabv0007().getWkLimiteStatiJob() || Conditions.eq(ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState(), '0') || Conditions.eq(ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState(), Types.HIGH_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState(), Types.SPACE_CHAR) || Conditions.eq(ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState(), Types.LOW_CHAR_VAL))) {
            // COB_CODE: IF JOB-COD-ELAB-STATE(IND-STATI-JOB) =
            //                                    WK-COD-ELAB-STATE
            //             END-IF
            //           END-IF
            if (ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState() == ws.getIabv0007().getWkCodElabState()) {
                // COB_CODE: IF JOB-FLAG-TO-EXECUTE(IND-STATI-JOB) =
                //                                           WK-ATTIVO
                //             SET WK-RISULT-VERIF-ELAB-STATE-OK TO TRUE
                //           END-IF
                if (ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getFlagToExecute() == ws.getIabv0007().getWkAttivo()) {
                    // COB_CODE: SET WK-RISULT-VERIF-ELAB-STATE-OK TO TRUE
                    ws.getIabv0007().setFlagRisultVerifElabState(true);
                }
            }
            ws.getIabv0007().setIndStatiJob(Trunc.toShort(ws.getIabv0007().getIndStatiJob() + 1, 2));
        }
    }

    /**Original name: D902-REPERISCI-DES-BATCH-STATE<br>
	 * <pre>----------------------------------------------------------------*
	 *  REPERISCI DESCRIZIONE BATCH STATE
	 * ----------------------------------------------------------------*</pre>*/
    private void d902ReperisciDesBatchState() {
        // COB_CODE: MOVE 'D902-REPERISCI-DES-BATCH-STATE' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D902-REPERISCI-DES-BATCH-STATE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO      TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                        TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: SET WK-BATCH-STATE-FOUND-NO               TO TRUE
        ws.getIabv0007().setFlagWkBatchStateFound(false);
        // COB_CODE: PERFORM VARYING IND-STATI-BATCH FROM 1 BY 1
        //              UNTIL IND-STATI-BATCH > WK-LIMITE-STATI-BATCH OR
        //                    WK-BATCH-STATE-FOUND-YES                OR
        //                    BAT-COD-BATCH-STATE(IND-STATI-BATCH) =
        //                   ZEROES OR HIGH-VALUE OR SPACES OR LOW-VALUE
        //                   END-IF
        //           END-PERFORM.
        ws.getIabv0007().setIndStatiBatch(((short)1));
        while (!(ws.getIabv0007().getIndStatiBatch() > ws.getIabv0007().getWkLimiteStatiBatch() || ws.getIabv0007().isFlagWkBatchStateFound() || Conditions.eq(ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState(), '0') || Conditions.eq(ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState(), Types.HIGH_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState(), Types.SPACE_CHAR) || Conditions.eq(ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState(), Types.LOW_CHAR_VAL))) {
            // COB_CODE: IF BAT-COD-BATCH-STATE(IND-STATI-BATCH) =
            //                                    WK-COD-BATCH-STATE
            //                                  TO  WK-DES-BATCH-STATE
            //           END-IF
            if (ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState() == ws.getIabv0007().getWkCodBatchState()) {
                // COB_CODE: SET WK-BATCH-STATE-FOUND-YES     TO TRUE
                ws.getIabv0007().setFlagWkBatchStateFound(true);
                // COB_CODE: MOVE BAT-DES(IND-STATI-BATCH)
                //                               TO  WK-DES-BATCH-STATE
                ws.getIabv0007().setWkDesBatchState(ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getDes());
            }
            ws.getIabv0007().setIndStatiBatch(Trunc.toShort(ws.getIabv0007().getIndStatiBatch() + 1, 2));
        }
    }

    /**Original name: D950-AGGIORNA-PRENOTAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *  AGGIORNA PRENOTAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void d950AggiornaPrenotazione() {
        ConcatUtil concatUtil = null;
        Iabs0900 iabs0900 = null;
        // COB_CODE: MOVE 'D950-AGGIORNA-PRENOTAZIONE' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D950-AGGIORNA-PRENOTAZIONE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE:      IF WK-ERRORE-NO
        //           *--> TIPO OPERAZIONE
        //                   END-IF
        //                END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            //--> TIPO OPERAZIONE
            // COB_CODE: SET IDSV0003-UPDATE            TO TRUE
            ws.getIabv0007().getIdsv0003().getOperazione().setIdsi0011Update();
            //-->    INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
            ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
            ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
            //
            // COB_CODE: CALL PGM-IABS0900 USING IDSV0003
            //                                   IABV0002
            //                                   RICH
            //                                   IABV0003
            //           ON EXCEPTION
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-CALL
            try {
                iabs0900 = Iabs0900.getInstance();
                iabs0900.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getRich(), ws.getIabv0007().getIabv0003());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0900
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                    DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0900Formatted(), " - ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE:            IF IDSV0003-SUCCESSFUL-RC
                //                         END-EVALUATE
                //                      ELSE
                //           *-->          GESTIRE ERRORE DISPATCHER
                //                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      END-IF
                if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:               EVALUATE TRUE
                    //                            WHEN IDSV0003-SUCCESSFUL-SQL
                    //           *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                               CONTINUE
                    //                            WHEN IDSV0003-NEGATIVI
                    //           *--->            ERRORE DI ACCESSO AL DB
                    //                                                         THRU W070-EX
                    //                         END-EVALUATE
                    if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                        //--->            ERRORE DI ACCESSO AL DB
                        // COB_CODE: INITIALIZE WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                        ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                        // COB_CODE: STRING 'ERRORE MODULO '
                        //                  DELIMITED BY SIZE
                        //                  PGM-IABS0900
                        //                  DELIMITED BY SIZE
                        //                  ' SQLCODE : '
                        //                  DELIMITED BY SIZE
                        //                  WK-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0900Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                        // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                        //                                     THRU W070-EX
                        w070CaricaLogErrBatExec();
                    }
                }
                else {
                    //-->          GESTIRE ERRORE DISPATCHER
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                   DELIMITED BY SIZE
                    //                   PGM-IABS0900
                    //                   DELIMITED BY SIZE
                    //                   ' - '
                    //                   DELIMITED BY SIZE
                    //                   IDSV0003-DESCRIZ-ERR-DB2
                    //                   DELIMITED BY '   '
                    //                   ' - '
                    //                   DELIMITED BY SIZE
                    //                   'RC : '
                    //                   DELIMITED BY SIZE
                    //                   IDSV0003-RETURN-CODE
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0900Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
        }
    }

    /**Original name: D951-TRASF-BATCH-TO-PRENOTAZ<br>
	 * <pre>---------------------------------------------------------
	 *  TRASFORMA DA BATCH-STATE A PRENOTAZ-STATE
	 * ---------------------------------------------------------</pre>*/
    private void d951TrasfBatchToPrenotaz() {
        // COB_CODE: MOVE 'D951-TRASF-BATCH-TO-PRENOTAZ' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D951-TRASF-BATCH-TO-PRENOTAZ");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: SET WK-AGGIORNA-PRENOTAZ-YES       TO TRUE
        ws.getIabv0007().setFlagAggiornaPrenotaz(true);
        // COB_CODE: MOVE IABV0002-STATE-CURRENT        TO WK-STATE-CURRENT
        ws.getIabv0007().getIabv0004().setWkStateCurrent(ws.getIabv0007().getIabv0002().getIabv0002StateCurrent());
        // COB_CODE: IF IABV0002-STATE-CURRENT = BATCH-ESEGUITO    OR
        //                                       BATCH-SIMULATO-OK
        //              MOVE ESEGUITA           TO IABV0002-STATE-CURRENT
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getIabv0002().getIabv0002StateCurrent() == ws.getIabv0007().getIabv0004().getBatchEseguito() || ws.getIabv0007().getIabv0002().getIabv0002StateCurrent() == ws.getIabv0007().getIabv0004().getBatchSimulatoOk()) {
            // COB_CODE: MOVE ESEGUITA           TO IABV0002-STATE-CURRENT
            ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getEseguita());
        }
        else if (ws.getIabv0007().getIabv0002().getIabv0002StateCurrent() == ws.getIabv0007().getIabv0004().getBatchDaRieseguire() || ws.getIabv0007().getIabv0002().getIabv0002StateCurrent() == ws.getIabv0007().getIabv0004().getBatchSimulatoKo()) {
            // COB_CODE: IF IABV0002-STATE-CURRENT = BATCH-DA-RIESEGUIRE OR
            //                                       BATCH-SIMULATO-KO
            //              MOVE ESEGUITA-KO     TO  IABV0002-STATE-CURRENT
            //           ELSE
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            // COB_CODE: MOVE ESEGUITA-KO     TO  IABV0002-STATE-CURRENT
            ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getEseguitaKo());
        }
        else {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE 'COD-BATCH-STATE NON VALIDO'
            //                              TO WK-LOR-DESC-ERRORE-ESTESA
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa("COD-BATCH-STATE NON VALIDO");
            // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: D952-TRASF-PRENOTAZ-TO-BATCH<br>
	 * <pre>----------------------------------------------------------------
	 *  TRASFORMA PRENOTAZ-STATE A BATCH-STATE
	 * ----------------------------------------------------------------</pre>*/
    private void d952TrasfPrenotazToBatch() {
        // COB_CODE: MOVE 'D952-TRASF-PRENOTAZ-TO-BATCH' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D952-TRASF-PRENOTAZ-TO-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: MOVE WK-STATE-CURRENT TO IABV0002-STATE-CURRENT.
        ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getWkStateCurrent());
    }

    /**Original name: D953-INITIALIZE-X-BATCH<br>
	 * <pre>----------------------------------------------------------------
	 *  TRASFORMA PRENOTAZ-STATE A BATCH-STATE
	 * ----------------------------------------------------------------</pre>*/
    private void d953InitializeXBatch() {
        // COB_CODE: MOVE 'D953-INITIALIZE-X-BATCH' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D953-INITIALIZE-X-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET PRIMA-VOLTA-YES               TO TRUE.
        ws.getIabv0007().setWkPrimaVolta(true);
    }

    /**Original name: D999-SESSION-TABLE<br>
	 * <pre>----------------------------------------------------------------
	 *  SESSION TABLE
	 * ----------------------------------------------------------------</pre>*/
    private void d999SessionTable() {
        // COB_CODE: MOVE 'D999-SESSION-TABLE'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("D999-SESSION-TABLE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: SET IDSV0301-DECLARE              TO TRUE.
        ws.getIabv0007().getIdsv0301().getOperazione().setIdsv0301Declare();
        // COB_CODE: PERFORM CALL-IDSS0300             THRU CALL-IDSS0300-EX.
        callIdss0300();
    }

    /**Original name: E000-VERIFICA-JOB<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA JOB
	 * ----------------------------------------------------------------*</pre>*/
    private void e000VerificaJob() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'E000-VERIFICA-JOB'         TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E000-VERIFICA-JOB");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setSelect();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC     TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE IABI0011-BUFFER-WHERE-COND
        //                                 TO IDSV0003-BUFFER-WHERE-COND
        ws.getIabv0007().getIdsv0003().setBufferWhereCond(ws.getIabv0007().getIabi0011Area().getBufferWhereCond());
        // COB_CODE: PERFORM X000-CARICA-STATI-JOB     THRU X000-EX
        x000CaricaStatiJob();
        // COB_CODE: IF IABV0006-GUIDE-AD-HOC OR
        //              IABV0006-BY-BOOKING
        //              END-IF
        //           ELSE
        //              PERFORM CALL-IABS0060          THRU CALL-IABS0060-EX
        //           END-IF
        if (ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() || ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking()) {
            // COB_CODE: IF IABI0011-RIP-CON-VERSIONAMENTO
            //                                          THRU V000-EX
            //           END-IF
            if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConVersionamento()) {
                // COB_CODE: PERFORM V000-VALORIZZAZIONE-VERSIONE
                //                                       THRU V000-EX
                v000ValorizzazioneVersione();
            }
            // COB_CODE: PERFORM N501-PRE-GUIDE-AD-HOC  THRU N501-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0011:line=3102, because the code is unreachable.
            // COB_CODE: IF WK-ERRORE-NO
            //              PERFORM N509-SWITCH-GUIDE   THRU N509-EX
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM N509-SWITCH-GUIDE   THRU N509-EX
                n509SwitchGuide();
            }
        }
        else {
            // COB_CODE: MOVE BTC-ID-BATCH              TO BJS-ID-BATCH
            ws.getBtcJobSchedule().setBjsIdBatch(ws.getBtcBatch().getBtcIdBatch());
            // COB_CODE: IF IABI0011-MACROFUNZIONALITA NOT =
            //              SPACES AND LOW-VALUE AND HIGH-VALUE
            //              SET VERIFICA-FINALE-JOBS-SI TO TRUE
            //           END-IF
            if (!Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalita()) && !Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalitaFormatted()) && !Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalitaFormatted())) {
                // COB_CODE: MOVE IABI0011-MACROFUNZIONALITA
                //                                      TO BJS-COD-MACROFUNCT
                ws.getBtcJobSchedule().setBjsCodMacrofunct(ws.getIabv0007().getIabi0011Area().getMacrofunzionalita());
                // COB_CODE: SET VERIFICA-FINALE-JOBS-SI TO TRUE
                ws.getIabv0007().getFlagVerificaFinaleJobs().setSi();
            }
            // COB_CODE: IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
            //              IABI0011-TIPO-MOVIMENTO NOT = ZEROES
            //              SET VERIFICA-FINALE-JOBS-SI   TO TRUE
            //           END-IF
            if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted())) {
                // COB_CODE: MOVE IABI0011-TIPO-MOVIMENTO TO BJS-TP-MOVI
                ws.getBtcJobSchedule().getBjsTpMovi().setBjsTpMovi(ws.getIabv0007().getIabi0011Area().getTipoMovimento());
                // COB_CODE: SET VERIFICA-FINALE-JOBS-SI   TO TRUE
                ws.getIabv0007().getFlagVerificaFinaleJobs().setSi();
            }
            // COB_CODE: MOVE PGM-IABS0060              TO WK-PGM-ERRORE
            ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getIabv0004().getPgmIabs0060());
            // COB_CODE: PERFORM CALL-IABS0060          THRU CALL-IABS0060-EX
            callIabs0060();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           ELSE
        //              PERFORM  Z500-GESTIONE-ERR-MAIN  THRU Z500-EX
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *--> GESTIRE ERRORE DISPATCHER
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                      WHEN IDSV0003-SUCCESSFUL-SQL
                //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                //                         PERFORM F000-FASE-MICRO THRU F000-EX
                //                      WHEN IDSV0003-NOT-FOUND
                //           *--> CAMPO $ NON TROVATO
                //                         END-IF
                //                      WHEN IDSV0003-NEGATIVI
                //           *--> ERRORE DI ACCESSO AL DB
                //                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: PERFORM F000-FASE-MICRO THRU F000-EX
                    f000FaseMicro();
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                    //--> CAMPO $ NON TROVATO
                    // COB_CODE: IF POST-GUIDE-X-JOB-NOT-FOUND-YES
                    //              PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
                    //           END-IF
                    if (ws.getIabv0007().isWkPostGuideXJobNotFound()) {
                        // COB_CODE: PERFORM N504-POST-GUIDE-AD-HOC THRU N504-EX
                        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0011:line=3145, because the code is unreachable.
                        // COB_CODE: PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
                        w000CaricaLogErrore();
                    }
                    // COB_CODE: IF WK-ERRORE-NO
                    //              PERFORM Y000-COMMIT                  THRU Y000-EX
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: IF GESTIONE-PARALLELISMO-YES
                        //              END-IF
                        //           END-IF
                        if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
                            // COB_CODE: PERFORM P100-PARALLELISMO-FINALE
                            //                                             THRU P100-EX
                            p100ParallelismoFinale();
                            // COB_CODE: IF WK-ERRORE-NO
                            //              PERFORM P900-CNTL-FINALE    THRU P900-EX
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: PERFORM P900-CNTL-FINALE    THRU P900-EX
                                p900CntlFinale();
                            }
                        }
                        // COB_CODE: IF BATCH-COMPLETE
                        //              END-IF
                        //           END-IF
                        if (ws.getIabv0007().getWsBufferWhereCondIabs0130().getFlagBatchComplete().isBatchComplete()) {
                            // COB_CODE: IF WK-BAT-COLLECTION-KEY-OK AND
                            //              BATCH-STATE-OK
                            //              PERFORM I100-INIT-ESEGUITO THRU I100-EX
                            //           ELSE
                            //                 THRU I200-EX
                            //           END-IF
                            if (ws.getIabv0007().isWkBatCollectionKey() && ws.getIabv0007().getWsBufferWhereCondIabs0130().getFlagBatchState().isBatchStateOk()) {
                                // COB_CODE: PERFORM I100-INIT-ESEGUITO THRU I100-EX
                                i100InitEseguito();
                            }
                            else {
                                // COB_CODE: PERFORM I200-INIT-DA-ESEGUIRE
                                //              THRU I200-EX
                                i200InitDaEseguire();
                            }
                            // COB_CODE: PERFORM A150-SET-CURRENT-TIMESTAMP
                            //                                    THRU A150-EX
                            a150SetCurrentTimestamp();
                            // COB_CODE: MOVE WS-TIMESTAMP-N      TO BTC-DT-END
                            ws.getBtcBatch().getBtcDtEnd().setBtcDtEnd(ws.getIabv0007().getWsTimestampN());
                            // COB_CODE: PERFORM D400-UPD-BATCH   THRU D400-EX
                            d400UpdBatch();
                            // COB_CODE: IF WK-ERRORE-NO
                            //              END-IF
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                                //              BTC-ID-RICH      NOT = ZEROES
                                //              END-IF
                                //           END-IF
                                if (!Characters.EQ_HIGH.test(ws.getBtcBatch().getBtcIdRich().getBtcIdRichNullFormatted()) && ws.getBtcBatch().getBtcIdRich().getBtcIdRich() != 0) {
                                    // COB_CODE: SET IDSV0003-WHERE-CONDITION-02 TO TRUE
                                    ws.getIabv0007().getIdsv0003().getTipologiaOperazione().setIdsv0003WhereCondition02();
                                    // COB_CODE: PERFORM D951-TRASF-BATCH-TO-PRENOTAZ
                                    //                                       THRU D951-EX
                                    d951TrasfBatchToPrenotaz();
                                    // COB_CODE: IF WK-AGGIORNA-PRENOTAZ-YES
                                    //              END-IF
                                    //           END-IF
                                    if (ws.getIabv0007().isFlagAggiornaPrenotaz()) {
                                        // COB_CODE: SET IDSV0003-NONE-ACTION    TO TRUE
                                        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setIdsv0003NoneAction();
                                        // COB_CODE: PERFORM D950-AGGIORNA-PRENOTAZIONE
                                        //                                    THRU D950-EX
                                        d950AggiornaPrenotazione();
                                        // COB_CODE: IF WK-ERRORE-NO
                                        //                                    THRU D952-EX
                                        //           END-IF
                                        if (!ws.getIabv0007().isWkErrore()) {
                                            // COB_CODE: PERFORM D952-TRASF-PRENOTAZ-TO-BATCH
                                            //                                   THRU D952-EX
                                            d952TrasfPrenotazToBatch();
                                        }
                                    }
                                }
                            }
                        }
                        // COB_CODE: IF WK-ERRORE-NO
                        //                                    THRU D953-EX
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: PERFORM Z002-OPERAZ-FINALI-X-BATCH
                            //                                 THRU Z002-EX
                            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0011:line=3197, because the code is unreachable.
                            // COB_CODE: PERFORM D953-INITIALIZE-X-BATCH
                            //                                 THRU D953-EX
                            d953InitializeXBatch();
                        }
                        // COB_CODE: MOVE BTC-ID-BATCH TO WK-ID-BATCH
                        ws.getIabv0007().setWkIdBatch(TruncAbs.toInt(ws.getBtcBatch().getBtcIdBatch(), 9));
                        // COB_CODE: INITIALIZE WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: STRING 'NON ESISTONO DATI DA ELABORARE'
                        //                  DELIMITED BY SIZE
                        //                  ' X '
                        //                  DELIMITED BY SIZE
                        //                  'ID_BATCH : '
                        //                  DELIMITED BY SIZE
                        //                  WK-ID-BATCH
                        //                  DELIMITED BY SIZE
                        //                  INTO
                        //                  WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "NON ESISTONO DATI DA ELABORARE", " X ", "ID_BATCH : ", ws.getIabv0007().getWkIdBatchAsString());
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: IF  WK-RC-08
                        //               CONTINUE
                        //           ELSE
                        //               SET WK-RC-04                          TO TRUE
                        //           END-IF
                        if (ws.getIabv0007().getFlagReturnCode().isRc08()) {
                        // COB_CODE: CONTINUE
                        //continue
                        }
                        else {
                            // COB_CODE: SET WK-RC-04                          TO TRUE
                            ws.getIabv0007().getFlagReturnCode().setRc04();
                        }
                        // COB_CODE: SET WK-ERRORE-NON-BLOCCANTE           TO TRUE
                        ws.getIabv0007().getWkTipoErrore().setNonBloccante();
                        // COB_CODE: MOVE WARNING       TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getWarning());
                        // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                        w070CaricaLogErrBatExec();
                        // COB_CODE: PERFORM Y000-COMMIT                  THRU Y000-EX
                        y000Commit();
                    }
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--> ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0060
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0060Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //--> GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   WK-PGM-ERRORE
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getWkPgmErroreFormatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
        else {
            // COB_CODE: PERFORM  Z500-GESTIONE-ERR-MAIN  THRU Z500-EX
            z500GestioneErrMain();
        }
    }

    /**Original name: I000-INIT-CUSTOM-COUNT<br>
	 * <pre>----------------------------------------------------------------*
	 *  INIZIALIZZA CUSTOM COUNT
	 * ----------------------------------------------------------------*</pre>*/
    private void i000InitCustomCount() {
        // COB_CODE: MOVE 'I000-INIT-CUSTOM-COUNT'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("I000-INIT-CUSTOM-COUNT");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM VARYING IABV0006-MAX-ELE-CUSTOM-COUNT FROM 1 BY 1
        //                     UNTIL IABV0006-MAX-ELE-CUSTOM-COUNT >
        //                           IABV0006-LIM-CUSTOM-COUNT-MAX
        //                            (IABV0006-MAX-ELE-CUSTOM-COUNT)
        //           END-PERFORM
        ws.getIabv0006().getCustomCounters().setMaxEleCustomCount(((short)1));
        while (!(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount() > ws.getIabv0006().getCustomCounters().getLimCustomCountMax())) {
            // COB_CODE: MOVE SPACES TO IABV0006-CUSTOM-COUNT-DESC
            //                         (IABV0006-MAX-ELE-CUSTOM-COUNT)
            ws.getIabv0006().getCustomCounters().getEleErrori(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount()).setCustomCountDesc("");
            // COB_CODE: MOVE ZEROES TO IABV0006-CUSTOM-COUNT
            //                         (IABV0006-MAX-ELE-CUSTOM-COUNT)
            ws.getIabv0006().getCustomCounters().getEleErrori(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount()).setIabv0006CustomCount(0);
            ws.getIabv0006().getCustomCounters().setMaxEleCustomCount(Trunc.toShort(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES    TO IABV0006-MAX-ELE-CUSTOM-COUNT.
        ws.getIabv0006().getCustomCounters().setMaxEleCustomCount(((short)0));
    }

    /**Original name: X000-CARICA-STATI-JOB<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA STATI JOB
	 * ----------------------------------------------------------------*</pre>*/
    private void x000CaricaStatiJob() {
        // COB_CODE: MOVE 'X000-CARICA-STATI-JOB'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("X000-CARICA-STATI-JOB");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: INITIALIZE IABV0002-STATE-GLOBAL.
        initIabv0002StateGlobal();
        // COB_CODE: MOVE ZEROES                  TO IND-STATI-V0002
        ws.getIabv0007().setIndStatiV0002(((short)0));
        // COB_CODE: PERFORM VARYING IND-STATI-JOB FROM 1 BY 1
        //                   UNTIL IND-STATI-JOB > WK-LIMITE-STATI-JOB OR
        //                   JOB-COD-ELAB-STATE(IND-STATI-JOB) =
        //                   SPACES OR HIGH-VALUE OR LOW-VALUE OR ZERO
        //                         TO IABV0002-STATE-ELE(IND-STATI-V0002)
        //           END-PERFORM.
        ws.getIabv0007().setIndStatiJob(((short)1));
        while (!(ws.getIabv0007().getIndStatiJob() > ws.getIabv0007().getWkLimiteStatiJob() || Conditions.eq(ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState(), Types.SPACE_CHAR) || Conditions.eq(ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState(), Types.HIGH_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState(), '0'))) {
            // COB_CODE: ADD 1               TO IND-STATI-V0002
            ws.getIabv0007().setIndStatiV0002(Trunc.toShort(1 + ws.getIabv0007().getIndStatiV0002(), 2));
            // COB_CODE: MOVE JOB-COD-ELAB-STATE(IND-STATI-JOB)
            //                TO IABV0002-STATE-ELE(IND-STATI-V0002)
            ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setIabv0002StateEle(ws.getIabv0007().getIndStatiV0002(), ws.getIabv0007().getTabellaStatiJobElements(ws.getIabv0007().getIndStatiJob()).getCodElabState());
            ws.getIabv0007().setIndStatiJob(Trunc.toShort(ws.getIabv0007().getIndStatiJob() + 1, 2));
        }
    }

    /**Original name: X001-CARICA-STATI-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA STATI BATCH
	 * ----------------------------------------------------------------*</pre>*/
    private void x001CaricaStatiBatch() {
        // COB_CODE: MOVE 'X001-CARICA-STATI-BATCH'   TO WK-LABEL.
        ws.getIabv0007().setWkLabel("X001-CARICA-STATI-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: INITIALIZE IABV0002-STATE-GLOBAL.
        initIabv0002StateGlobal();
        // COB_CODE: MOVE ZEROES                  TO IND-STATI-V0002
        ws.getIabv0007().setIndStatiV0002(((short)0));
        // COB_CODE: PERFORM VARYING IND-STATI-BATCH FROM 1 BY 1
        //                   UNTIL (IND-STATI-BATCH > WK-LIMITE-STATI-BATCH - 1)
        //                   OR     BAT-COD-BATCH-STATE(IND-STATI-BATCH) =
        //                          SPACES OR HIGH-VALUE OR LOW-VALUE OR ZERO
        //                   END-IF
        //           END-PERFORM.
        ws.getIabv0007().setIndStatiBatch(((short)1));
        while (!(ws.getIabv0007().getIndStatiBatch() > ws.getIabv0007().getWkLimiteStatiBatch() - 1 || Conditions.eq(ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState(), Types.SPACE_CHAR) || Conditions.eq(ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState(), Types.HIGH_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState(), Types.LOW_CHAR_VAL) || Conditions.eq(ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState(), '0'))) {
            // COB_CODE: IF BAT-FLAG-TO-EXECUTE(IND-STATI-BATCH) =
            //                                             WK-ATTIVO
            //                   TO IABV0002-STATE-ELE(IND-STATI-V0002)
            //           END-IF
            if (ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getFlagToExecute() == ws.getIabv0007().getWkAttivo()) {
                // COB_CODE: ADD 1               TO IND-STATI-V0002
                ws.getIabv0007().setIndStatiV0002(Trunc.toShort(1 + ws.getIabv0007().getIndStatiV0002(), 2));
                // COB_CODE: MOVE BAT-COD-BATCH-STATE(IND-STATI-BATCH)
                //                TO IABV0002-STATE-ELE(IND-STATI-V0002)
                ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setIabv0002StateEle(ws.getIabv0007().getIndStatiV0002(), ws.getIabv0007().getTabellaStatiBatchElements(ws.getIabv0007().getIndStatiBatch()).getCodBatchState());
            }
            ws.getIabv0007().setIndStatiBatch(Trunc.toShort(ws.getIabv0007().getIndStatiBatch() + 1, 2));
        }
    }

    /**Original name: F000-FASE-MICRO<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE T1
	 * ----------------------------------------------------------------*</pre>*/
    private void f000FaseMicro() {
        // COB_CODE: MOVE 'F000-FASE-MICRO'           TO WK-LABEL.
        ws.getIabv0007().setWkLabel("F000-FASE-MICRO");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: PERFORM F100-ESTRAI-LIV-ORG               THRU F100-EX.
        f100EstraiLivOrg();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM B500-ELABORA-MICRO             THRU B500-EX
            b500ElaboraMicro();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: IF GESTIONE-PARALLELISMO-YES
                //              END-IF
                //           END-IF
                if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
                    // COB_CODE: PERFORM P100-PARALLELISMO-FINALE THRU P100-EX
                    p100ParallelismoFinale();
                    // COB_CODE: IF WK-ERRORE-NO
                    //              PERFORM P900-CNTL-FINALE      THRU P900-EX
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM P900-CNTL-FINALE      THRU P900-EX
                        p900CntlFinale();
                    }
                }
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF BATCH-COMPLETE
                    //              END-IF
                    //           END-IF
                    if (ws.getIabv0007().getWsBufferWhereCondIabs0130().getFlagBatchComplete().isBatchComplete()) {
                        // COB_CODE: IF WK-BAT-COLLECTION-KEY-OK AND
                        //              BATCH-STATE-OK
                        //              PERFORM I100-INIT-ESEGUITO THRU I100-EX
                        //           ELSE
                        //              END-IF
                        //           END-IF
                        if (ws.getIabv0007().isWkBatCollectionKey() && ws.getIabv0007().getWsBufferWhereCondIabs0130().getFlagBatchState().isBatchStateOk()) {
                            // COB_CODE: IF WK-RC-04-NON-FORZATO
                            //              END-IF
                            //           ELSE
                            //              END-IF
                            //           END-IF
                            if (ws.getIabv0007().getFlagRc04().isRc04NonForzato()) {
                                // COB_CODE: IF  WK-RC-08
                                //               CONTINUE
                                //           ELSE
                                //               SET WK-RC-04           TO TRUE
                                //           END-IF
                                if (ws.getIabv0007().getFlagReturnCode().isRc08()) {
                                // COB_CODE: CONTINUE
                                //continue
                                }
                                else {
                                    // COB_CODE: SET WK-RC-04           TO TRUE
                                    ws.getIabv0007().getFlagReturnCode().setRc04();
                                }
                            }
                            else if (ws.getIabv0007().getFlagReturnCode().isRc04() || ws.getIabv0007().getFlagReturnCode().isRc08()) {
                            // COB_CODE: IF  WK-RC-04 OR
                            //               WK-RC-08
                            //               CONTINUE
                            //           ELSE
                            //               SET WK-RC-00           TO TRUE
                            //           END-IF
                            // COB_CODE: CONTINUE
                            //continue
                            }
                            else {
                                // COB_CODE: SET WK-RC-00           TO TRUE
                                ws.getIabv0007().getFlagReturnCode().setRc00();
                            }
                            // COB_CODE: PERFORM I100-INIT-ESEGUITO THRU I100-EX
                            i100InitEseguito();
                        }
                        else {
                            // COB_CODE: IF WK-ERRORE-FATALE
                            //              SET WK-RC-12         TO TRUE
                            //           ELSE
                            //              END-IF
                            //           END-IF
                            if (ws.getIabv0007().getWkTipoErrore().isFatale()) {
                                // COB_CODE: SET WK-RC-12         TO TRUE
                                ws.getIabv0007().getFlagReturnCode().setRc12();
                            }
                            else if (ws.getIabv0007().getIabi0011Area().getForzRc04().isYes() || ws.getIabv0007().getFlagRc04().isRc04Forzato()) {
                                // COB_CODE: IF  IABI0011-FORZ-RC-04-YES OR
                                //               WK-RC-04-FORZATO
                                //               SET WK-RC-04         TO TRUE
                                //           ELSE
                                //               SET WK-RC-08         TO TRUE
                                //           END-IF
                                // COB_CODE: SET WK-RC-04         TO TRUE
                                ws.getIabv0007().getFlagReturnCode().setRc04();
                            }
                            else {
                                // COB_CODE: SET WK-RC-08         TO TRUE
                                ws.getIabv0007().getFlagReturnCode().setRc08();
                            }
                            // COB_CODE: IF WK-ERRORE-FATALE
                            //                 THRU I200-EX
                            //           ELSE
                            //              END-IF
                            //           END-IF
                            if (ws.getIabv0007().getWkTipoErrore().isFatale()) {
                                // COB_CODE: PERFORM I200-INIT-DA-ESEGUIRE
                                //              THRU I200-EX
                                i200InitDaEseguire();
                            }
                            else if (ws.getIabv0007().getIabi0011Area().getForzRc04().isYes() || ws.getIabv0007().getFlagRc04().isRc04Forzato()) {
                                // COB_CODE: IF  IABI0011-FORZ-RC-04-YES OR
                                //               WK-RC-04-FORZATO
                                //              PERFORM I100-INIT-ESEGUITO THRU I100-EX
                                //           ELSE
                                //                 THRU I200-EX
                                //           END-IF
                                // COB_CODE: PERFORM I100-INIT-ESEGUITO THRU I100-EX
                                i100InitEseguito();
                            }
                            else {
                                // COB_CODE: PERFORM I200-INIT-DA-ESEGUIRE
                                //              THRU I200-EX
                                i200InitDaEseguire();
                            }
                        }
                        // COB_CODE: PERFORM A150-SET-CURRENT-TIMESTAMP THRU A150-EX
                        a150SetCurrentTimestamp();
                        // COB_CODE: MOVE WS-TIMESTAMP-N        TO   BTC-DT-END
                        ws.getBtcBatch().getBtcDtEnd().setBtcDtEnd(ws.getIabv0007().getWsTimestampN());
                        // COB_CODE: IF WK-SIMULAZIONE-INFR
                        //              PERFORM Y200-ROLLBACK   THRU Y200-EX
                        //           END-IF
                        if (ws.getIabv0007().getWkSimulazione().isInfr()) {
                            // COB_CODE: PERFORM Y200-ROLLBACK   THRU Y200-EX
                            y200Rollback();
                        }
                        // COB_CODE: PERFORM D400-UPD-BATCH     THRU D400-EX
                        d400UpdBatch();
                        // COB_CODE: IF WK-ERRORE-NO
                        //              END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                            //              BTC-ID-RICH      NOT = ZEROES
                            //              END-IF
                            //           END-IF
                            if (!Characters.EQ_HIGH.test(ws.getBtcBatch().getBtcIdRich().getBtcIdRichNullFormatted()) && ws.getBtcBatch().getBtcIdRich().getBtcIdRich() != 0) {
                                // COB_CODE: SET IDSV0003-WHERE-CONDITION-02 TO TRUE
                                ws.getIabv0007().getIdsv0003().getTipologiaOperazione().setIdsv0003WhereCondition02();
                                // COB_CODE: PERFORM D951-TRASF-BATCH-TO-PRENOTAZ
                                //                                          THRU D951-EX
                                d951TrasfBatchToPrenotaz();
                                // COB_CODE: IF WK-AGGIORNA-PRENOTAZ-YES
                                //              END-IF
                                //           END-IF
                                if (ws.getIabv0007().isFlagAggiornaPrenotaz()) {
                                    // COB_CODE: SET IDSV0003-NONE-ACTION       TO TRUE
                                    ws.getIabv0007().getIdsv0003().getLivelloOperazione().setIdsv0003NoneAction();
                                    // COB_CODE: PERFORM D950-AGGIORNA-PRENOTAZIONE
                                    //                                       THRU D950-EX
                                    d950AggiornaPrenotazione();
                                    // COB_CODE: IF WK-ERRORE-NO
                                    //                                       THRU D952-EX
                                    //           END-IF
                                    if (!ws.getIabv0007().isWkErrore()) {
                                        // COB_CODE: PERFORM D952-TRASF-PRENOTAZ-TO-BATCH
                                        //                                    THRU D952-EX
                                        d952TrasfPrenotazToBatch();
                                    }
                                }
                            }
                            // COB_CODE: IF WK-ERRORE-NO
                            //                                    THRU D953-EX
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: PERFORM Z002-OPERAZ-FINALI-X-BATCH
                                //                                 THRU Z002-EX
                                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0011:line=3464, because the code is unreachable.
                                // COB_CODE: PERFORM D953-INITIALIZE-X-BATCH
                                //                                 THRU D953-EX
                                d953InitializeXBatch();
                            }
                        }
                    }
                    // COB_CODE: IF WK-ERRORE-NO
                    //              END-IF
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM Y000-COMMIT        THRU Y000-EX
                        y000Commit();
                        // COB_CODE: IF (IABV0006-STD-TYPE-REC-NO   OR
                        //               IABV0006-STD-TYPE-REC-YES) AND
                        //               IDSV0001-ARCH-BATCH-DBG
                        //               MOVE WK-ID-JOB      TO WK-ID-JOB-ULT-COMMIT
                        //           END-IF
                        if ((ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecNo() || ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecYes()) && ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().isIdsv0001ArchBatchDbg()) {
                            // COB_CODE: MOVE WK-ID-JOB      TO WK-ID-JOB-ULT-COMMIT
                            ws.getIabv0007().setWkIdJobUltCommitFormatted(ws.getIabv0007().getWkIdJobFormatted());
                        }
                        // COB_CODE: IF WK-ERRORE-NO
                        //              PERFORM Y050-CONTROLLO-COMMIT THRU Y050-EX
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: PERFORM Y050-CONTROLLO-COMMIT THRU Y050-EX
                            y050ControlloCommit();
                        }
                    }
                }
            }
        }
    }

    /**Original name: F100-ESTRAI-LIV-ORG<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAI JOB
	 * ----------------------------------------------------------------*</pre>*/
    private void f100EstraiLivOrg() {
        ConcatUtil concatUtil = null;
        Ides0020 ides0020 = null;
        // COB_CODE: MOVE 'F100-ESTRAI-LIV-ORG'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("F100-ESTRAI-LIV-ORG");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setSelect();
        // COB_CODE: MOVE IABI0011-GRUPPO-AUTORIZZANTE TO GRU-COD-GRU-ARZ.
        ws.getGruArz().setCodGruArz(ws.getIabv0007().getIabi0011Area().getGruppoAutorizzante());
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: MOVE PGM-IDES0020              TO WK-PGM-ERRORE
        ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getIabv0004().getPgmIdes0020());
        // COB_CODE: CALL PGM-IDES0020              USING IDSV0003
        //                                          GRU-ARZ
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL
        try {
            ides0020 = Ides0020.getInstance();
            ides0020.run(ws.getIabv0007().getIdsv0003(), ws.getGruArz());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
            //                DELIMITED BY SIZE
            //                PGM-IDES0020
            //                DELIMITED BY SIZE
            //                ' - '
            //                 DELIMITED BY SIZE INTO
            //                WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIdes0020Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *--> GESTIRE ERRORE CALL
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                      WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                       TO IABV0006-PUNTO-VENDITA
                //                      WHEN IDSV0003-NEGATIVI
                //           *--->      ERRORE DI ACCESSO AL DB
                //                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE GRU-COD-LIV-OGZ TO IABV0006-COD-LIV-AUT-PROFIL
                    ws.getIabv0006().setCodLivAutProfil(ws.getGruArz().getCodLivOgz());
                    // COB_CODE: MOVE IABI0011-GRUPPO-AUTORIZZANTE
                    //                                TO IABV0006-COD-GRU-AUT-PROFIL
                    ws.getIabv0006().setCodGruAutProfil(ws.getIabv0007().getIabi0011Area().getGruppoAutorizzante());
                    // COB_CODE: MOVE IABI0011-CANALE-VENDITA
                    //                         TO IABV0006-CANALE-VENDITA
                    ws.getIabv0006().setCanaleVendita(ws.getIabv0007().getIabi0011Area().getCanaleVendita());
                    // COB_CODE: MOVE IABI0011-PUNTO-VENDITA
                    //                         TO IABV0006-PUNTO-VENDITA
                    ws.getIabv0006().setPuntoVendita(ws.getIabv0007().getIabi0011Area().getPuntoVendita());
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IDES0020
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIdes0020Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //--> GESTIRE ERRORE CALL
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   WK-PGM-ERRORE
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getWkPgmErroreFormatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: I100-INIT-ESEGUITO<br>
	 * <pre>****************************************************************</pre>*/
    private void i100InitEseguito() {
        // COB_CODE: MOVE 'I100-INIT-ESEGUITO'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("I100-INIT-ESEGUITO");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET WK-CONFERMA-BATCH-ESEGUITO-OK TO TRUE
        ws.getIabv0007().setFlagConfermaBatchEseguito(true);
        // COB_CODE: IF WK-SIMULAZIONE-INFR OR
        //               WK-SIMULAZIONE-APPL
        //              MOVE BATCH-SIMULATO-OK   TO IABV0002-STATE-CURRENT
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getWkSimulazione().isInfr() || ws.getIabv0007().getWkSimulazione().isAppl()) {
            // COB_CODE: MOVE BATCH-SIMULATO-OK   TO IABV0002-STATE-CURRENT
            ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getBatchSimulatoOk());
        }
        else {
            // COB_CODE: IF NOT IABI0011-SENZA-RIPARTENZA
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isSenzaRipartenza()) {
                // COB_CODE: IF VERIFICA-FINALE-JOBS-SI
                //               PERFORM I300-VERIFICA-FINALE-JOBS THRU I300-EX
                //           END-IF
                if (ws.getIabv0007().getFlagVerificaFinaleJobs().isSi()) {
                    // COB_CODE: PERFORM I300-VERIFICA-FINALE-JOBS THRU I300-EX
                    i300VerificaFinaleJobs();
                }
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: IF WK-CONFERMA-BATCH-ESEGUITO-OK
                //              MOVE BATCH-ESEGUITO TO IABV0002-STATE-CURRENT
                //           END-IF
                if (ws.getIabv0007().isFlagConfermaBatchEseguito()) {
                    // COB_CODE: MOVE BATCH-ESEGUITO TO IABV0002-STATE-CURRENT
                    ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getBatchEseguito());
                }
            }
        }
    }

    /**Original name: I200-INIT-DA-ESEGUIRE<br>
	 * <pre>****************************************************************</pre>*/
    private void i200InitDaEseguire() {
        // COB_CODE: MOVE 'I200-INIT-DA-ESEGUIRE'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("I200-INIT-DA-ESEGUIRE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF WK-SIMULAZIONE-INFR OR
        //              WK-SIMULAZIONE-APPL
        //              MOVE BATCH-SIMULATO-KO   TO IABV0002-STATE-CURRENT
        //           ELSE
        //              MOVE BATCH-DA-RIESEGUIRE TO IABV0002-STATE-CURRENT
        //           END-IF.
        if (ws.getIabv0007().getWkSimulazione().isInfr() || ws.getIabv0007().getWkSimulazione().isAppl()) {
            // COB_CODE: MOVE BATCH-SIMULATO-KO   TO IABV0002-STATE-CURRENT
            ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getBatchSimulatoKo());
        }
        else {
            // COB_CODE: MOVE BATCH-DA-RIESEGUIRE TO IABV0002-STATE-CURRENT
            ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getBatchDaRieseguire());
        }
    }

    /**Original name: I300-VERIFICA-FINALE-JOBS<br>
	 * <pre>****************************************************************</pre>*/
    private void i300VerificaFinaleJobs() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'I300-VERIFICA-FINALE-JOB'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("I300-VERIFICA-FINALE-JOB");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setSelect();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC     TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE IABI0011-BUFFER-WHERE-COND
        //                                 TO IDSV0003-BUFFER-WHERE-COND
        ws.getIabv0007().getIdsv0003().setBufferWhereCond(ws.getIabv0007().getIabi0011Area().getBufferWhereCond());
        // COB_CODE: IF IABV0006-GUIDE-AD-HOC OR
        //              IABV0006-BY-BOOKING
        //              END-IF
        //           ELSE
        //              PERFORM CALL-IABS0060      THRU CALL-IABS0060-EX
        //           END-IF
        if (ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() || ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking()) {
            // COB_CODE: IF IABI0011-RIP-CON-VERSIONAMENTO
            //                                             THRU V000-EX
            //           END-IF
            if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConVersionamento()) {
                // COB_CODE: PERFORM V000-VALORIZZAZIONE-VERSIONE
                //                                          THRU V000-EX
                v000ValorizzazioneVersione();
            }
            // COB_CODE: PERFORM N501-PRE-GUIDE-AD-HOC     THRU N501-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0011:line=3666, because the code is unreachable.
            // COB_CODE: MOVE HIGH-VALUE            TO RIC-COD-MACROFUNCT
            ws.getRich().setRicCodMacrofunct(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_COD_MACROFUNCT));
            // COB_CODE: MOVE ZEROES                TO RIC-TP-MOVI
            ws.getRich().setRicTpMovi(0);
            // COB_CODE: IF WK-ERRORE-NO
            //              PERFORM N509-SWITCH-GUIDE      THRU N509-EX
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM N509-SWITCH-GUIDE      THRU N509-EX
                n509SwitchGuide();
            }
        }
        else {
            // COB_CODE: MOVE BTC-ID-BATCH          TO BJS-ID-BATCH
            ws.getBtcJobSchedule().setBjsIdBatch(ws.getBtcBatch().getBtcIdBatch());
            // COB_CODE: MOVE HIGH-VALUE            TO BJS-COD-MACROFUNCT-NULL
            //                                         BJS-TP-MOVI-NULL
            ws.getBtcJobSchedule().setBjsCodMacrofunct(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcJobSchedule.Len.BJS_COD_MACROFUNCT));
            ws.getBtcJobSchedule().getBjsTpMovi().setBjsTpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BjsTpMovi.Len.BJS_TP_MOVI_NULL));
            // COB_CODE: MOVE PGM-IABS0060          TO WK-PGM-ERRORE
            ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getIabv0004().getPgmIabs0060());
            // COB_CODE: PERFORM CALL-IABS0060      THRU CALL-IABS0060-EX
            callIabs0060();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                      WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                //                         SET WK-CONFERMA-BATCH-ESEGUITO-KO TO TRUE
                //                      WHEN IDSV0003-NOT-FOUND
                //           *--->      CAMPO $ NON TROVATO
                //                         SET WK-CONFERMA-BATCH-ESEGUITO-OK TO TRUE
                //                      WHEN IDSV0003-NEGATIVI
                //           *--->      ERRORE DI ACCESSO AL DB
                //                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: SET WK-CONFERMA-BATCH-ESEGUITO-KO TO TRUE
                    ws.getIabv0007().setFlagConfermaBatchEseguito(false);
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                    //--->      CAMPO $ NON TROVATO
                    // COB_CODE: SET WK-CONFERMA-BATCH-ESEGUITO-OK TO TRUE
                    ws.getIabv0007().setFlagConfermaBatchEseguito(true);
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0060
                    //                  DELIMITED BY SIZE
                    //                  'SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0060Formatted(), "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   WK-PGM-ERRORE
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getWkPgmErroreFormatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: P000-PARALLELISMO-INIZIALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE PARALLELISMO INIZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void p000ParallelismoIniziale() {
        // COB_CODE: MOVE 'P000-PARALLELISMO-INIZIALE' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P000-PARALLELISMO-INIZIALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE IABI0011-CODICE-COMPAGNIA-ANIA TO BPA-COD-COMP-ANIA
        ws.getBtcParallelism().setBpaCodCompAnia(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAnia());
        // COB_CODE: MOVE BTC-PROTOCOL                   TO BPA-PROTOCOL
        ws.getBtcParallelism().setBpaProtocol(ws.getBtcBatch().getBtcProtocol());
        // COB_CODE: MOVE IABI0011-PROG-PROTOCOL         TO BPA-PROG-PROTOCOL
        ws.getBtcParallelism().setBpaProgProtocol(ws.getIabv0007().getIabi0011Area().getProgProtocol());
        // COB_CODE: PERFORM P050-PREPARA-FASE-INIZIALE  THRU P050-EX.
        p050PreparaFaseIniziale();
        // COB_CODE: PERFORM P500-CALL-IABS0130          THRU P500-EX.
        p500CallIabs0130();
        // COB_CODE: PERFORM P051-ESITO-PARALLELO        THRU P051-EX.
        p051EsitoParallelo();
    }

    /**Original name: P100-PARALLELISMO-FINALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE PARALLELISMO FINALE
	 * ----------------------------------------------------------------*</pre>*/
    private void p100ParallelismoFinale() {
        // COB_CODE: MOVE 'P100-PARALLELISMO-FINALE'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P100-PARALLELISMO-FINALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE IABI0011-CODICE-COMPAGNIA-ANIA TO BPA-COD-COMP-ANIA
        ws.getBtcParallelism().setBpaCodCompAnia(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAnia());
        // COB_CODE: MOVE BTC-PROTOCOL                   TO BPA-PROTOCOL
        ws.getBtcParallelism().setBpaProtocol(ws.getBtcBatch().getBtcProtocol());
        // COB_CODE: PERFORM P150-PREPARA-FASE-FINALE    THRU P150-EX.
        p150PreparaFaseFinale();
        // COB_CODE: IF WK-ERRORE-NO
        //              PERFORM P051-ESITO-PARALLELO        THRU P051-EX
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM P500-CALL-IABS0130          THRU P500-EX
            p500CallIabs0130();
            // COB_CODE: PERFORM P051-ESITO-PARALLELO        THRU P051-EX
            p051EsitoParallelo();
        }
    }

    /**Original name: P050-PREPARA-FASE-INIZIALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA FASE INIZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void p050PreparaFaseIniziale() {
        // COB_CODE: MOVE 'P050-PREPARA-FASE-INIZIALE'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P050-PREPARA-FASE-INIZIALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET WS-FASE-INIZIALE               TO TRUE.
        ws.getIabv0007().getWsBufferWhereCondIabs0130().getWsFlagFase().setWsFaseIniziale();
        // COB_CODE: PERFORM X000-CARICA-STATI-JOB     THRU X000-EX
        x000CaricaStatiJob();
        // COB_CODE: MOVE WS-BUFFER-WHERE-COND-IABS0130
        //                                      TO IDSV0003-BUFFER-WHERE-COND.
        ws.getIabv0007().getIdsv0003().setBufferWhereCond(ws.getIabv0007().getWsBufferWhereCondIabs0130().getWsBufferWhereCondIabs0130Formatted());
    }

    /**Original name: P150-PREPARA-FASE-FINALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA FASE FINALE
	 * ----------------------------------------------------------------*</pre>*/
    private void p150PreparaFaseFinale() {
        // COB_CODE: MOVE 'P150-PREPARA-FASE-FINALE'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P150-PREPARA-FASE-FINALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET WS-FASE-FINALE               TO TRUE.
        ws.getIabv0007().getWsBufferWhereCondIabs0130().getWsFlagFase().setWsFaseFinale();
        // COB_CODE: INITIALIZE IABV0002-STATE-GLOBAL
        initIabv0002StateGlobal();
        // COB_CODE: MOVE JOB-IN-ESECUZIONE           TO IABV0002-STATE-01
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState01(ws.getIabv0007().getIabv0004().getJobInEsecuzione());
        // COB_CODE: IF WK-BAT-COLLECTION-KEY-OK
        //              PERFORM I100-INIT-ESEGUITO    THRU I100-EX
        //           ELSE
        //              PERFORM I200-INIT-DA-ESEGUIRE THRU I200-EX
        //           END-IF
        if (ws.getIabv0007().isWkBatCollectionKey()) {
            // COB_CODE: PERFORM I100-INIT-ESEGUITO    THRU I100-EX
            i100InitEseguito();
        }
        else {
            // COB_CODE: PERFORM I200-INIT-DA-ESEGUIRE THRU I200-EX
            i200InitDaEseguire();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //                                     TO IDSV0003-BUFFER-WHERE-COND
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT   TO BPA-COD-BATCH-STATE
            ws.getBtcParallelism().setBpaCodBatchState(ws.getIabv0007().getIabv0002().getIabv0002StateCurrent());
            // COB_CODE: MOVE WS-BUFFER-WHERE-COND-IABS0130
            //                                  TO IDSV0003-BUFFER-WHERE-COND
            ws.getIabv0007().getIdsv0003().setBufferWhereCond(ws.getIabv0007().getWsBufferWhereCondIabs0130().getWsBufferWhereCondIabs0130Formatted());
        }
    }

    /**Original name: P500-CALL-IABS0130<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL IABS0130
	 * ----------------------------------------------------------------*</pre>*/
    private void p500CallIabs0130() {
        ConcatUtil concatUtil = null;
        Iabs0130 iabs0130 = null;
        // COB_CODE: MOVE 'P500-CALL-IABS0130'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P500-CALL-IABS0130");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL PGM-IABS0130 USING IDSV0003
        //                                   IABV0002
        //                                   BTC-PARALLELISM
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL.
        try {
            iabs0130 = Iabs0130.getInstance();
            iabs0130.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getBtcParallelism());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
            //                DELIMITED BY SIZE
            //                PGM-IABS0130
            //                DELIMITED BY SIZE
            //                ' - '
            //                 DELIMITED BY SIZE INTO
            //                WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: P051-ESITO-PARALLELO<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLA ESITO PARALLELISMO
	 * ----------------------------------------------------------------*</pre>*/
    private void p051EsitoParallelo() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'P051-ESITO-PARALLELO'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P051-ESITO-PARALLELO");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                      WHEN IDSV0003-SUCCESSFUL-SQL
                //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                //                         CONTINUE
                //                      WHEN IDSV0003-NOT-FOUND
                //           *--> CAMPO $ NON TROVATO
                //                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      WHEN IDSV0003-NEGATIVI
                //           *--->      ERRORE DI ACCESSO AL DB
                //                        PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                //--> OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                    //--> CAMPO $ NON TROVATO
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
                    ws.getIabv0007().setComodoCodCompAnia(TruncAbs.toInt(ws.getBtcParallelism().getBpaCodCompAnia(), 9));
                    // COB_CODE: MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL
                    ws.getIabv0007().setComodoProgProtocol(TruncAbs.toInt(ws.getBtcParallelism().getBpaProgProtocol(), 9));
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0130
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  ' '
                    //                  DELIMITED BY SIZE
                    //                  COMODO-COD-COMP-ANIA
                    //                  ' / '
                    //                  DELIMITED BY SIZE
                    //                  BPA-PROTOCOL
                    //                  DELIMITED BY SPACES
                    //                  ' / '
                    //                  DELIMITED BY SIZE
                    //                  COMODO-PROG-PROTOCOL
                    //                  DELIMITED BY SPACES
                    //                  INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString(), " ", ws.getIabv0007().getComodoCodCompAniaAsString(), " / ", Functions.substringBefore(ws.getBtcParallelism().getBpaProtocolFormatted(), Types.SPACE_STRING), " / ", Functions.substringBefore(ws.getIabv0007().getComodoProgProtocolAsString(), Types.SPACE_STRING)});
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0130
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0130
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: P899-ESITO-CNTL-INIZIALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLA ESITO PARALLELISMO SU CONTROLLO INIZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void p899EsitoCntlIniziale() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'P899-ESITO-CNTL-INIZIALE'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P899-ESITO-CNTL-INIZIALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *--> GESTIRE ERRORE DISPATCHER
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                      WHEN IDSV0003-SUCCESSFUL-SQL
                //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                //                         MOVE BPA-TP-OGG       TO IABV0009-TP-OGG
                //           *              IF BATCH-COMPLETE
                //           *                 INITIALIZE WK-LOG-ERRORE
                //           *                 MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                //           *                 STRING 'INCONG. BTC_BATCH - BTC_PARALLELISM'
                //           *                        ' - '
                //           *                        'RC : '
                //           *                        IDSV0003-RETURN-CODE
                //           *                        DELIMITED BY SIZE INTO
                //           *                        WK-LOR-DESC-ERRORE-ESTESA
                //           *                 END-STRING
                //           *                 MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERROR
                //           *                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                //           *                          THRU W070-EX
                //           *              END-IF
                //                      WHEN IDSV0003-NOT-FOUND
                //           *--> CAMPO $ NON TROVATO
                //           *--> ERRORE DI ACCESSO AL DB
                //                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      WHEN IDSV0003-NEGATIVI
                //           *--> ERRORE DI ACCESSO AL DB
                //                        PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: SET WK-VERIFICA-BATCH-OK     TO TRUE
                    ws.getIabv0007().setFlagVerificaBatch(true);
                    // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND
                    //                         TO WS-BUFFER-WHERE-COND-IABS0130
                    ws.getIabv0007().getWsBufferWhereCondIabs0130().setWsBufferWhereCondIabs0130Formatted(ws.getIabv0007().getIdsv0003().getBufferWhereCondFormatted());
                    // COB_CODE: IF BPA-ID-OGG-DA IS NUMERIC AND
                    //              BPA-ID-OGG-DA NOT = ZEROES
                    //              MOVE BPA-ID-OGG-DA TO IABV0009-ID-OGG-DA
                    //           END-IF
                    if (Functions.isNumber(ws.getBtcParallelism().getBpaIdOggDa().getBpaIdOggDa()) && ws.getBtcParallelism().getBpaIdOggDa().getBpaIdOggDa() != 0) {
                        // COB_CODE: MOVE BPA-ID-OGG-DA TO IABV0009-ID-OGG-DA
                        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setIdOggDa(ws.getBtcParallelism().getBpaIdOggDa().getBpaIdOggDa());
                    }
                    // COB_CODE: IF BPA-ID-OGG-A  IS NUMERIC AND
                    //              BPA-ID-OGG-A  NOT = ZEROES
                    //              MOVE BPA-ID-OGG-A  TO IABV0009-ID-OGG-A
                    //           END-IF
                    if (Functions.isNumber(ws.getBtcParallelism().getBpaIdOggA().getBpaIdOggA()) && ws.getBtcParallelism().getBpaIdOggA().getBpaIdOggA() != 0) {
                        // COB_CODE: MOVE BPA-ID-OGG-A  TO IABV0009-ID-OGG-A
                        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setIdOggA(ws.getBtcParallelism().getBpaIdOggA().getBpaIdOggA());
                    }
                    // COB_CODE: MOVE BPA-TP-OGG       TO IABV0009-TP-OGG
                    ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setTpOgg(ws.getBtcParallelism().getBpaTpOgg());
                    //              IF BATCH-COMPLETE
                    //                 INITIALIZE WK-LOG-ERRORE
                    //                 MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    //                 STRING 'INCONG. BTC_BATCH - BTC_PARALLELISM'
                    //                        ' - '
                    //                        'RC : '
                    //                        IDSV0003-RETURN-CODE
                    //                        DELIMITED BY SIZE INTO
                    //                        WK-LOR-DESC-ERRORE-ESTESA
                    //                 END-STRING
                    //                 MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERROR
                    //                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                    //                          THRU W070-EX
                    //              END-IF
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                    //--> CAMPO $ NON TROVATO
                    //--> ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
                    ws.getIabv0007().setComodoCodCompAnia(TruncAbs.toInt(ws.getBtcParallelism().getBpaCodCompAnia(), 9));
                    // COB_CODE: MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL
                    ws.getIabv0007().setComodoProgProtocol(TruncAbs.toInt(ws.getBtcParallelism().getBpaProgProtocol(), 9));
                    // COB_CODE: MOVE IDSV0003-SQLCODE  TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0130
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  ' '
                    //                  DELIMITED BY SIZE
                    //                  COMODO-COD-COMP-ANIA
                    //                  ' / '
                    //                  DELIMITED BY SIZE
                    //                  BPA-PROTOCOL
                    //                  DELIMITED BY SPACES
                    //                  INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString(), " ", ws.getIabv0007().getComodoCodCompAniaAsString(), " / ", Functions.substringBefore(ws.getBtcParallelism().getBpaProtocolFormatted(), Types.SPACE_STRING)});
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--> ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0130
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //--> GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0130
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: P951-ESITO-CNTL-FINALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLA ESITO PARALLELISMO SU CONTROLLO FINALE
	 * ----------------------------------------------------------------*</pre>*/
    private void p951EsitoCntlFinale() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'P951-ESITO-CNTL-FINALE'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P951-ESITO-CNTL-FINALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                      WHEN IDSV0003-SUCCESSFUL-SQL
                //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                         TO WS-BUFFER-WHERE-COND-IABS0130
                //                      WHEN IDSV0003-NOT-FOUND
                //           *--> CAMPO $ NON TROVATO
                //           *--> ERRORE DI ACCESSO AL DB
                //                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      WHEN IDSV0003-NEGATIVI
                //           *--->      ERRORE DI ACCESSO AL DB
                //                        PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND
                    //                           TO WS-BUFFER-WHERE-COND-IABS0130
                    ws.getIabv0007().getWsBufferWhereCondIabs0130().setWsBufferWhereCondIabs0130Formatted(ws.getIabv0007().getIdsv0003().getBufferWhereCondFormatted());
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                    //--> CAMPO $ NON TROVATO
                    //--> ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE BPA-COD-COMP-ANIA TO COMODO-COD-COMP-ANIA
                    ws.getIabv0007().setComodoCodCompAnia(TruncAbs.toInt(ws.getBtcParallelism().getBpaCodCompAnia(), 9));
                    // COB_CODE: MOVE BPA-PROG-PROTOCOL TO COMODO-PROG-PROTOCOL
                    ws.getIabv0007().setComodoProgProtocol(TruncAbs.toInt(ws.getBtcParallelism().getBpaProgProtocol(), 9));
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0130
                    //                  DELIMITED BY SIZE
                    //                  ' '
                    //                  DELIMITED BY SIZE
                    //                  COMODO-COD-COMP-ANIA
                    //                  DELIMITED BY SIZE
                    //                  ' / '
                    //                  DELIMITED BY SIZE
                    //                  BPA-PROTOCOL
                    //                  DELIMITED BY SPACES
                    //                  ' / '
                    //                  DELIMITED BY SIZE
                    //                  COMODO-PROG-PROTOCOL
                    //                  DELIMITED BY SPACES
                    //                  INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " ", ws.getIabv0007().getComodoCodCompAniaAsString(), " / ", Functions.substringBefore(ws.getBtcParallelism().getBpaProtocolFormatted(), Types.SPACE_STRING), " / ", Functions.substringBefore(ws.getIabv0007().getComodoProgProtocolAsString(), Types.SPACE_STRING)});
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0130
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  DELIMITED BY SIZE
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0130
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0130Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: P800-CNTL-INIZIALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  FASE CONTROLLO INIZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void p800CntlIniziale() {
        // COB_CODE: MOVE 'P800-CNTL-INIZIALE'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P800-CNTL-INIZIALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE IABI0011-CODICE-COMPAGNIA-ANIA TO BPA-COD-COMP-ANIA
        ws.getBtcParallelism().setBpaCodCompAnia(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAnia());
        // COB_CODE: MOVE BTC-PROTOCOL                   TO BPA-PROTOCOL
        ws.getBtcParallelism().setBpaProtocol(ws.getBtcBatch().getBtcProtocol());
        // COB_CODE: MOVE IABI0011-PROG-PROTOCOL         TO BPA-PROG-PROTOCOL
        ws.getBtcParallelism().setBpaProgProtocol(ws.getIabv0007().getIabi0011Area().getProgProtocol());
        // COB_CODE: PERFORM P850-PREPARA-CNTL-INIZIALE  THRU P850-EX.
        p850PreparaCntlIniziale();
        // COB_CODE: PERFORM P500-CALL-IABS0130          THRU P500-EX.
        p500CallIabs0130();
        // COB_CODE: PERFORM P899-ESITO-CNTL-INIZIALE    THRU P899-EX.
        p899EsitoCntlIniziale();
    }

    /**Original name: P850-PREPARA-CNTL-INIZIALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA FASE CONTROLLO
	 * ----------------------------------------------------------------*</pre>*/
    private void p850PreparaCntlIniziale() {
        // COB_CODE: MOVE 'P850-PREPARA-CNTL-INIZIALE'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P850-PREPARA-CNTL-INIZIALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-WHERE-CONDITION       TO TRUE.
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET WS-FASE-CONTROLLO-INIZIALE     TO TRUE.
        ws.getIabv0007().getWsBufferWhereCondIabs0130().getWsFlagFase().setWsFaseControlloIniziale();
        // COB_CODE: MOVE WS-BUFFER-WHERE-COND-IABS0130
        //                                      TO IDSV0003-BUFFER-WHERE-COND.
        ws.getIabv0007().getIdsv0003().setBufferWhereCond(ws.getIabv0007().getWsBufferWhereCondIabs0130().getWsBufferWhereCondIabs0130Formatted());
    }

    /**Original name: S100-TESTATA-JCL<br>
	 * <pre>----------------------------------------------------------------*
	 *  TRATTA TESTATA JCL
	 * ----------------------------------------------------------------*</pre>*/
    private void s100TestataJcl() {
        // COB_CODE: MOVE 'S100-TESTATA-JCL'          TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S100-TESTATA-JCL");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM OPEN-REPORT01          THRU OPEN-REPORT01-EX
            openReport01();
            // COB_CODE: IF WK-ERRORE-NO
            //              PERFORM S149-SCRIVI-TESTATA-JCL THRU S149-EX
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM S149-SCRIVI-TESTATA-JCL THRU S149-EX
                s149ScriviTestataJcl();
            }
        }
    }

    /**Original name: S149-SCRIVI-TESTATA-JCL<br>
	 * <pre>----------------------------------------------------------------*
	 *  SCRIVI TESTATA JCL
	 * ----------------------------------------------------------------*</pre>*/
    private void s149ScriviTestataJcl() {
        // COB_CODE: MOVE 'S149-SCRIVI-TESTATA-JCL'   TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S149-SCRIVI-TESTATA-JCL");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM S151-PREPARA-TESTATA-JCL  THRU S151-EX
        s151PreparaTestataJcl();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE '* '                     TO REPORT01-REC
        report01To.setVariable("* ");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE LOGO-JCL-REPORT          TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getLogoJclReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE '* '                     TO REPORT01-REC
        report01To.setVariable("* ");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE DATA-ELABORAZIONE-REPORT TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getDataElaborazioneReport().getDataElaborazioneReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ORA-ELABORAZIONE-REPORT  TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getOraElaborazioneReport().getOraElaborazioneReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE COD-COMPAGNIA-REPORT     TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getCodCompagniaReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE DESC-BATCH-REPORT        TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getDescBatchReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE USER-NAME-REPORT         TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getUserNameReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE GRUPPO-AUT-REPORT        TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getGruppoAutReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: IF REP-MACROFUNZIONALITA NOT =
        //              SPACES AND LOW-VALUE AND HIGH-VALUE
        //              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        //           END-IF
        if (!Characters.EQ_SPACE.test(ws.getIabv0007().getIabv0010().getRepMacrofunzionalita()) && !Characters.EQ_LOW.test(ws.getIabv0007().getIabv0010().getRepMacrofunzionalitaFormatted()) && !Characters.EQ_HIGH.test(ws.getIabv0007().getIabv0010().getRepMacrofunzionalitaFormatted())) {
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE MACROFUNZIONALITA-REPORT TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getMacrofunzionalitaReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
        }
        // COB_CODE: IF REP-TIPO-MOVIMENTO IS NUMERIC AND
        //              REP-TIPO-MOVIMENTO NOT = ZEROES
        //              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        //           END-IF
        if (Functions.isNumber(ws.getIabv0007().getIabv0010().getRepTipoMovimentoFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabv0010().getRepTipoMovimentoFormatted())) {
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE TIPO-MOVIMENTO-REPORT    TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getTipoMovimentoReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
        }
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE DATA-EFFETTO-REPORT      TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getDataEffettoReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE TIMESTAMP-COMPETENZA-REPORT
        //                                         TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getTimestampCompetenzaReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE SIMULAZIONE-REPORT       TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getSimulazioneReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE CANALE-VENDITA-REPORT    TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getCanaleVenditaReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE PUNTO-VENDITA-REPORT     TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getPuntoVenditaReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE '* '                     TO REPORT01-REC
        report01To.setVariable("* ");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX.
        writeReport01();
    }

    /**Original name: S151-PREPARA-TESTATA-JCL<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA TESTATA JCL
	 * ----------------------------------------------------------------*</pre>*/
    private void s151PreparaTestataJcl() {
        // COB_CODE: MOVE 'S151-PREPARA-TESTATA-JCL'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S151-PREPARA-TESTATA-JCL");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE GG-SYS                   TO REP-GIORNO
        ws.getIabv0007().getIabv0010().getDataElaborazioneReport().setGiorno(ws.getIabv0007().getWkTimestamp26().getGgSys());
        // COB_CODE: MOVE MM-SYS                   TO REP-MESE
        ws.getIabv0007().getIabv0010().getDataElaborazioneReport().setMese(ws.getIabv0007().getWkTimestamp26().getMmSys());
        // COB_CODE: MOVE AAAA-SYS                 TO REP-ANNO
        ws.getIabv0007().getIabv0010().getDataElaborazioneReport().setAnno(ws.getIabv0007().getWkTimestamp26().getAaaaSys());
        // COB_CODE: MOVE HH-SYS                   TO REP-ORA
        ws.getIabv0007().getIabv0010().getOraElaborazioneReport().setOra(ws.getIabv0007().getWkTimestamp26().getHhSys());
        // COB_CODE: MOVE MI-SYS                   TO REP-MINUTI
        ws.getIabv0007().getIabv0010().getOraElaborazioneReport().setMinuti(ws.getIabv0007().getWkTimestamp26().getMiSys());
        // COB_CODE: MOVE SS-SYS                   TO REP-SECONDI
        ws.getIabv0007().getIabv0010().getOraElaborazioneReport().setSecondi(ws.getIabv0007().getWkTimestamp26().getSsSys());
        // COB_CODE: MOVE BBT-DES                  TO REP-DESC-BATCH
        ws.getIabv0007().getIabv0010().setRepDescBatch(ws.getBtcBatchType().getBbtDes());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                         TO REP-COD-COMPAGNIA
        ws.getIabv0007().getIabv0010().setRepCodCompagnia(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IABI0011-USER-NAME       TO REP-USER-NAME
        ws.getIabv0007().getIabv0010().setRepUserName(ws.getIabv0007().getIabi0011Area().getUserName());
        // COB_CODE: MOVE IABI0011-GRUPPO-AUTORIZZANTE TO REP-GRUPPO-AUT
        ws.getIabv0007().getIabv0010().setRepGruppoAutFormatted(ws.getIabv0007().getIabi0011Area().getGruppoAutorizzanteFormatted());
        // COB_CODE: IF IABI0011-MACROFUNZIONALITA NOT =
        //              SPACES AND LOW-VALUE AND HIGH-VALUE
        //                                           TO REP-MACROFUNZIONALITA
        //           END-IF
        if (!Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalita()) && !Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalitaFormatted()) && !Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalitaFormatted())) {
            // COB_CODE: MOVE IABI0011-MACROFUNZIONALITA
            //                                        TO REP-MACROFUNZIONALITA
            ws.getIabv0007().getIabv0010().setRepMacrofunzionalita(ws.getIabv0007().getIabi0011Area().getMacrofunzionalita());
        }
        // COB_CODE: IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
        //              IABI0011-TIPO-MOVIMENTO NOT = ZEROES
        //              MOVE IABI0011-TIPO-MOVIMENTO TO REP-TIPO-MOVIMENTO
        //           END-IF
        if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted())) {
            // COB_CODE: MOVE IABI0011-TIPO-MOVIMENTO TO REP-TIPO-MOVIMENTO
            ws.getIabv0007().getIabv0010().setRepTipoMovimentoFormatted(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted());
        }
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
        ws.getIabv0007().setWsDateN(TruncAbs.toInt(ws.getIabv0007().getIdsv0003().getDataInizioEffetto(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X            THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X                    TO REP-DATA-EFFETTO
        ws.getIabv0007().getIabv0010().setRepDataEffetto(ws.getIabv0007().getWsDateX());
        // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA     TO WS-TIMESTAMP-N
        ws.getIabv0007().setWsTimestampN(TruncAbs.toLong(ws.getIabv0007().getIdsv0003().getDataCompetenza(), 18));
        // COB_CODE: PERFORM Z701-TS-N-TO-X            THRU Z701-EX
        z701TsNToX();
        // COB_CODE: MOVE WS-TIMESTAMP-X               TO REP-TIMESTAMP-COMPETENZA
        ws.getIabv0007().getIabv0010().setRepTimestampCompetenza(ws.getIabv0007().getWsTimestampX());
        // COB_CODE: IF WK-SIMULAZIONE-INFR OR
        //              WK-SIMULAZIONE-APPL
        //              MOVE ESSE                      TO REP-SIMULAZIONE
        //           ELSE
        //              MOVE ENNE                      TO REP-SIMULAZIONE
        //           END-IF
        if (ws.getIabv0007().getWkSimulazione().isInfr() || ws.getIabv0007().getWkSimulazione().isAppl()) {
            // COB_CODE: MOVE ESSE                      TO REP-SIMULAZIONE
            ws.getIabv0007().getIabv0010().setRepSimulazione(ws.getIabv0007().getEsse());
        }
        else {
            // COB_CODE: MOVE ENNE                      TO REP-SIMULAZIONE
            ws.getIabv0007().getIabv0010().setRepSimulazione(ws.getIabv0007().getEnne());
        }
        // COB_CODE: MOVE IABI0011-CANALE-VENDITA  TO REP-CANALE-VENDITA
        ws.getIabv0007().getIabv0010().setRepCanaleVendita(ws.getIabv0007().getIabi0011Area().getCanaleVendita());
        // COB_CODE: MOVE IABI0011-PUNTO-VENDITA   TO REP-PUNTO-VENDITA.
        ws.getIabv0007().getIabv0010().setRepPuntoVenditaFormatted(ws.getIabv0007().getIabi0011Area().getPuntoVenditaFormatted());
    }

    /**Original name: S170-SCRIVI-TESTATA-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  SCRIVI TESTATA BATCH
	 * ----------------------------------------------------------------*</pre>*/
    private void s170ScriviTestataBatch() {
        // COB_CODE: MOVE 'S170-SCRIVI-TESTATA-BATCH' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S170-SCRIVI-TESTATA-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM S171-PREPARA-TESTATA-BATCH  THRU S171-EX
        s171PreparaTestataBatch();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE LOGO-BATCH-REPORT        TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getLogoBatchReport());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ID-BATCH-REPORT          TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getIdBatchReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE PROTOCOL-REPORT          TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getProtocolReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: IF GESTIONE-PARALLELISMO-YES
        //              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
        //           END-IF.
        if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE PROGR-PROTOCOL-REPORT TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getProgrProtocolReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
        }
    }

    /**Original name: S171-PREPARA-TESTATA-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA TESTATA BATCH
	 * ----------------------------------------------------------------*</pre>*/
    private void s171PreparaTestataBatch() {
        // COB_CODE: MOVE 'S171-PREPARA-TESTATA-BATCH' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S171-PREPARA-TESTATA-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE BTC-ID-BATCH                   TO REP-ID-BATCH
        ws.getIabv0007().getIabv0010().setRepIdBatch(ws.getBtcBatch().getBtcIdBatch());
        // COB_CODE: MOVE BTC-PROTOCOL                   TO REP-PROTOCOL
        ws.getIabv0007().getIabv0010().setRepProtocol(ws.getBtcBatch().getBtcProtocol());
        // COB_CODE: IF GESTIONE-PARALLELISMO-YES
        //               MOVE IABI0011-PROG-PROTOCOL     TO REP-PROGR-PROTOCOL
        //           END-IF.
        if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
            // COB_CODE: MOVE IABI0011-PROG-PROTOCOL     TO REP-PROGR-PROTOCOL
            ws.getIabv0007().getIabv0010().setRepProgrProtocol(ws.getIabv0007().getIabi0011Area().getProgProtocol());
        }
    }

    /**Original name: S200-DETTAGLIO-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *  SCRIVI DETTAGLIO ERRORE
	 * ----------------------------------------------------------------*</pre>*/
    private void s200DettaglioErrore() {
        // COB_CODE: MOVE 'S200-DETTAGLIO-ERRORE'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S200-DETTAGLIO-ERRORE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE SPACES                   TO REP-DESC-ERRORE
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDescErrore("");
        // COB_CODE: PERFORM S210-PREPARA-DETT-ERR THRU S210-EX
        s210PreparaDettErr();
        // COB_CODE: IF STAMPA-LOGO-ERRORE-YES
        //              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        //           END-IF
        if (ws.getIabv0007().isFlagStampaLogoErrore()) {
            // COB_CODE: PERFORM S202-LOGO-DETTAGLIO-ERRORE THRU S202-EX
            s202LogoDettaglioErrore();
            // COB_CODE: SET STAMPA-LOGO-ERRORE-NO          TO TRUE
            ws.getIabv0007().setFlagStampaLogoErrore(false);
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE CAMPI-LOG-ERRORE-REPORT-RIGA-1
            //                                         TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getCampiLogErroreReportRiga1().getCampiLogErroreReportRiga1Formatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE CAMPI-LOG-ERRORE-REPORT-RIGA-2
            //                                         TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getCampiLogErroreReportRiga2().getCampiLogErroreReportRiga2Formatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE SL-CAMPI-LOG-ERRORE-REPORT
            //                                      TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getSlCampiLogErroreReport().getSlCampiLogErroreReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
        }
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE VALORI-CAMPI-LOG-ERRORE-REPORT
        //                                         TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().getValoriCampiLogErroreReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: IF DESC-ERR-MORE-100-YES
        //              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        //           END-IF.
        if (ws.getIabv0007().isFlagDescErrMore100()) {
            // COB_CODE: MOVE SPACES       TO VALORI-CAMPI-LOG-ERRORE-REPORT
            ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().initValoriCampiLogErroreReportSpaces();
            // COB_CODE: MOVE IDSV0001-DESC-ERRORE(IND-MAX-ELE-ERRORI)(101:100)
            //                                         TO REP-DESC-ERRORE
            ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDescErrore(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001DescErroreFormatted().substring((101) - 1, 200));
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE VALORI-CAMPI-LOG-ERRORE-REPORT
            //                                         TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().getValoriCampiLogErroreReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
        }
        // COB_CODE: INITIALIZE                    VALORI-CAMPI-LOG-ERRORE-REPORT.
        initValoriCampiLogErroreReport();
    }

    /**Original name: S205-DETTAGLIO-ERRORE-EXTRA<br>
	 * <pre>----------------------------------------------------------------*
	 *  SCRIVI DETTAGLIO ERRORE EXTRA
	 * ----------------------------------------------------------------*</pre>*/
    private void s205DettaglioErroreExtra() {
        // COB_CODE: MOVE 'S205-DETTAGLIO-ERRORE-EXTRA' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S205-DETTAGLIO-ERRORE-EXTRA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE SPACES                         TO REP-DESC-ERRORE
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDescErrore("");
        // COB_CODE: PERFORM S215-PREPARA-DETT-ERR-EXTRA THRU S215-EX
        s215PreparaDettErrExtra();
        // COB_CODE: IF STAMPA-LOGO-ERRORE-YES
        //              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        //           END-IF
        if (ws.getIabv0007().isFlagStampaLogoErrore()) {
            // COB_CODE: PERFORM S202-LOGO-DETTAGLIO-ERRORE THRU S202-EX
            s202LogoDettaglioErrore();
            // COB_CODE: SET STAMPA-LOGO-ERRORE-NO          TO TRUE
            ws.getIabv0007().setFlagStampaLogoErrore(false);
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE CAMPI-LOG-ERRORE-REPORT-RIGA-1
            //                                         TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getCampiLogErroreReportRiga1().getCampiLogErroreReportRiga1Formatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE CAMPI-LOG-ERRORE-REPORT-RIGA-2
            //                                         TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getCampiLogErroreReportRiga2().getCampiLogErroreReportRiga2Formatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE SL-CAMPI-LOG-ERRORE-REPORT
            //                                      TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getSlCampiLogErroreReport().getSlCampiLogErroreReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
        }
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE VALORI-CAMPI-LOG-ERRORE-REPORT
        //                                         TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().getValoriCampiLogErroreReportFormatted());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: IF DESC-ERR-MORE-100-YES
        //              PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        //           END-IF.
        if (ws.getIabv0007().isFlagDescErrMore100()) {
            // COB_CODE: MOVE SPACES              TO VALORI-CAMPI-LOG-ERRORE-REPORT
            ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().initValoriCampiLogErroreReportSpaces();
            // COB_CODE: MOVE WK-LOR-DESC-ERRORE-ESTESA (101:100)
            //                                         TO REP-DESC-ERRORE
            ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDescErrore(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted().substring((101) - 1, 200));
            // COB_CODE: MOVE SPACES                   TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE VALORI-CAMPI-LOG-ERRORE-REPORT
            //                                         TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().getValoriCampiLogErroreReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
            writeReport01();
        }
        // COB_CODE: INITIALIZE                    VALORI-CAMPI-LOG-ERRORE-REPORT.
        initValoriCampiLogErroreReport();
    }

    /**Original name: S210-PREPARA-DETT-ERR<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA DETTAGLIO ERRORE
	 * ----------------------------------------------------------------*</pre>*/
    private void s210PreparaDettErr() {
        // COB_CODE: MOVE 'S210-PREPARA-DETT-ERR'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S210-PREPARA-DETT-ERR");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE IABV0006-OGG-BUSINESS       TO REP-OGG-BUSINESS
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setOggBusiness(ws.getIabv0006().getReport().getOggBusiness());
        // COB_CODE: MOVE IABV0006-TP-OGG-BUSINESS    TO REP-TP-OGG-BUSINESS
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setTpOggBusiness(ws.getIabv0006().getReport().getTpOggBusiness());
        // COB_CODE: MOVE IABV0006-IB-OGG-POLI        TO REP-IB-OGG-POLI
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIbOggPoli(ws.getIabv0006().getReport().getIbOggPoli());
        // COB_CODE: MOVE IABV0006-IB-OGG-ADES        TO REP-IB-OGG-ADES
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIbOggAdes(ws.getIabv0006().getReport().getIbOggAdes());
        // COB_CODE: MOVE IABV0006-ID-POLI            TO REP-ID-POLI
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIdPoli(ws.getIabv0006().getReport().getIdPoli());
        // COB_CODE: MOVE IABV0006-ID-ADES            TO REP-ID-ADES
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIdAdes(ws.getIabv0006().getReport().getIdAdes());
        // COB_CODE: MOVE IABV0006-DT-EFF-BUSINESS    TO REP-DT-EFF-BUSINESS
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDtEffBusiness(ws.getIabv0006().getReport().getDtEffBusiness());
        // COB_CODE: INSPECT IDSV0001-DESC-ERRORE (IND-MAX-ELE-ERRORI)
        //                                            REPLACING ALL X'09' BY ' '
        ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).setDescErrore(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001DescErroreFormatted().replace(parseHex("09"), " "));
        // COB_CODE: INSPECT IDSV0001-DESC-ERRORE (IND-MAX-ELE-ERRORI)
        //                                            REPLACING ALL X'0A' BY ' '
        ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).setDescErrore(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001DescErroreFormatted().replace(parseHex("0A"), " "));
        // COB_CODE: SET DESC-ERR-MORE-100-NO         TO TRUE
        ws.getIabv0007().setFlagDescErrMore100(false);
        // COB_CODE: IF IDSV0001-DESC-ERRORE(IND-MAX-ELE-ERRORI)(101:100) NOT =
        //              HIGH-VALUE AND LOW-VALUE AND SPACES
        //              SET DESC-ERR-MORE-100-YES     TO TRUE
        //           END-IF.
        if (!Conditions.eq(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001DescErroreFormatted().substring((101) - 1, 200), LiteralGenerator.create(Types.HIGH_CHAR_VAL, 100)) && !Conditions.eq(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001DescErroreFormatted().substring((101) - 1, 200), LiteralGenerator.create(Types.LOW_CHAR_VAL, 100)) && !Conditions.eq(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001DescErroreFormatted().substring((101) - 1, 200), "")) {
            // COB_CODE: SET DESC-ERR-MORE-100-YES     TO TRUE
            ws.getIabv0007().setFlagDescErrMore100(true);
        }
        // COB_CODE: MOVE IDSV0001-DESC-ERRORE(IND-MAX-ELE-ERRORI)(1:100)
        //                                            TO REP-DESC-ERRORE.
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDescErrore(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001DescErroreFormatted().substring((1) - 1, 100));
        // COB_CODE: MOVE WK-LOR-ID-GRAVITA-ERRORE    TO REP-LIV-GRAVITA
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setLivGravita(TruncAbs.toShort(ws.getIabv0007().getWkLogErrore().getIdGravitaErrore(), 1));
        // COB_CODE: MOVE WK-LOR-COD-MAIN-BATCH       TO REP-COD-MAIN-BATCH
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setCodMainBatch(ws.getIabv0007().getWkLogErrore().getCodMainBatch());
        // COB_CODE: MOVE WK-LOR-COD-SERVIZIO-BE      TO REP-SERVIZIO-BE
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setServizioBe(ws.getIabv0007().getWkLogErrore().getCodServizioBe());
        // COB_CODE: MOVE WK-LOR-LABEL-ERR            TO REP-LABEL-ERR
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setLabelErr(ws.getIabv0007().getWkLogErrore().getLabelErr());
        // COB_CODE: MOVE WK-LOR-OPER-TABELLA         TO REP-OPER-TABELLA
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setOperTabella(ws.getIabv0007().getWkLogErrore().getOperTabella());
        // COB_CODE: MOVE WK-LOR-NOME-TABELLA         TO REP-NOME-TABELLA
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setNomeTabella(ws.getIabv0007().getWkLogErrore().getNomeTabella());
        // COB_CODE: MOVE WK-LOR-STATUS-TABELLA       TO REP-STATUS-TABELLA
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setStatusTabella(ws.getIabv0007().getWkLogErrore().getStatusTabella());
        // COB_CODE: MOVE WS-TIMESTAMP-X              TO REP-TMST-ERRORE
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setTmstErrore(ws.getIabv0007().getWsTimestampX());
        // COB_CODE: MOVE WK-LOR-KEY-TABELLA          TO REP-KEY-TABELLA.
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setKeyTabella(ws.getIabv0007().getWkLogErrore().getKeyTabella());
    }

    /**Original name: S215-PREPARA-DETT-ERR-EXTRA<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA DETTAGLIO ERRORE
	 * ----------------------------------------------------------------*</pre>*/
    private void s215PreparaDettErrExtra() {
        // COB_CODE: MOVE 'S215-PREPARA-DETT-ERR-EXTRA' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S215-PREPARA-DETT-ERR-EXTRA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE IABV0006-OGG-BUSINESS       TO REP-OGG-BUSINESS
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setOggBusiness(ws.getIabv0006().getReport().getOggBusiness());
        // COB_CODE: MOVE IABV0006-TP-OGG-BUSINESS    TO REP-TP-OGG-BUSINESS
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setTpOggBusiness(ws.getIabv0006().getReport().getTpOggBusiness());
        // COB_CODE: MOVE IABV0006-IB-OGG-POLI        TO REP-IB-OGG-POLI
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIbOggPoli(ws.getIabv0006().getReport().getIbOggPoli());
        // COB_CODE: MOVE IABV0006-IB-OGG-ADES        TO REP-IB-OGG-ADES
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIbOggAdes(ws.getIabv0006().getReport().getIbOggAdes());
        // COB_CODE: MOVE IABV0006-ID-POLI            TO REP-ID-POLI
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIdPoli(ws.getIabv0006().getReport().getIdPoli());
        // COB_CODE: MOVE IABV0006-ID-ADES            TO REP-ID-ADES
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIdAdes(ws.getIabv0006().getReport().getIdAdes());
        // COB_CODE: MOVE IABV0006-DT-EFF-BUSINESS    TO REP-DT-EFF-BUSINESS
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDtEffBusiness(ws.getIabv0006().getReport().getDtEffBusiness());
        // COB_CODE: INSPECT WK-LOR-DESC-ERRORE-ESTESA
        //                                            REPLACING ALL X'09' BY ' '
        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted().replace(parseHex("09"), " "));
        // COB_CODE: INSPECT WK-LOR-DESC-ERRORE-ESTESA
        //                                            REPLACING ALL X'0A' BY ' '
        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted().replace(parseHex("0A"), " "));
        // COB_CODE: SET DESC-ERR-MORE-100-NO         TO TRUE
        ws.getIabv0007().setFlagDescErrMore100(false);
        // COB_CODE: IF WK-LOR-DESC-ERRORE-ESTESA (101:100) NOT =
        //              HIGH-VALUE AND LOW-VALUE AND SPACES
        //              SET DESC-ERR-MORE-100-YES     TO TRUE
        //           END-IF.
        if (!Conditions.eq(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted().substring((101) - 1, 200), LiteralGenerator.create(Types.HIGH_CHAR_VAL, 100)) && !Conditions.eq(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted().substring((101) - 1, 200), LiteralGenerator.create(Types.LOW_CHAR_VAL, 100)) && !Conditions.eq(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted().substring((101) - 1, 200), "")) {
            // COB_CODE: SET DESC-ERR-MORE-100-YES     TO TRUE
            ws.getIabv0007().setFlagDescErrMore100(true);
        }
        // COB_CODE: MOVE WK-LOR-DESC-ERRORE-ESTESA(1:100)
        //                                            TO REP-DESC-ERRORE.
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDescErrore(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted().substring((1) - 1, 100));
        // COB_CODE: MOVE WK-LOR-ID-GRAVITA-ERRORE    TO REP-LIV-GRAVITA
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setLivGravita(TruncAbs.toShort(ws.getIabv0007().getWkLogErrore().getIdGravitaErrore(), 1));
        // COB_CODE: MOVE WK-LOR-COD-MAIN-BATCH       TO REP-COD-MAIN-BATCH
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setCodMainBatch(ws.getIabv0007().getWkLogErrore().getCodMainBatch());
        // COB_CODE: MOVE WK-LOR-COD-SERVIZIO-BE      TO REP-SERVIZIO-BE
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setServizioBe(ws.getIabv0007().getWkLogErrore().getCodServizioBe());
        // COB_CODE: MOVE WK-LOR-LABEL-ERR            TO REP-LABEL-ERR
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setLabelErr(ws.getIabv0007().getWkLogErrore().getLabelErr());
        // COB_CODE: MOVE WK-LOR-OPER-TABELLA         TO REP-OPER-TABELLA
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setOperTabella(ws.getIabv0007().getWkLogErrore().getOperTabella());
        // COB_CODE: MOVE WK-LOR-NOME-TABELLA         TO REP-NOME-TABELLA
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setNomeTabella(ws.getIabv0007().getWkLogErrore().getNomeTabella());
        // COB_CODE: MOVE WK-LOR-STATUS-TABELLA       TO REP-STATUS-TABELLA
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setStatusTabella(ws.getIabv0007().getWkLogErrore().getStatusTabella());
        // COB_CODE: MOVE WS-TIMESTAMP-X              TO REP-TMST-ERRORE
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setTmstErrore(ws.getIabv0007().getWsTimestampX());
        // COB_CODE: MOVE WK-LOR-KEY-TABELLA          TO REP-KEY-TABELLA.
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setKeyTabella(ws.getIabv0007().getWkLogErrore().getKeyTabella());
    }

    /**Original name: S202-LOGO-DETTAGLIO-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *  logo dettaglio errori
	 * ----------------------------------------------------------------*</pre>*/
    private void s202LogoDettaglioErrore() {
        // COB_CODE: MOVE 'S202-LOGO-DETTAGLIO-ERRORE' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S202-LOGO-DETTAGLIO-ERRORE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE DETT-LOG-ERRORE-REPORT   TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getDettLogErroreReport());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX.
        writeReport01();
    }

    /**Original name: S555-SWITCH-GUIDE-TYPE<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s555SwitchGuideType() {
        // COB_CODE: MOVE 'S555-SWITCH-GUIDE-TYPE'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S555-SWITCH-GUIDE-TYPE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: EVALUATE BBT-SERVICE-GUIDE-NAME
        //              WHEN CONST-SEQUENTIAL-GUIDE
        //                   SET SEQUENTIAL-GUIDE       TO TRUE
        //              WHEN OTHER
        //                                         TO WK-PGM-GUIDA
        //           END-EVALUATE.
        if (Conditions.eq(ws.getBtcBatchType().getBbtServiceGuideName(), ws.getIabv0007().getConstSequentialGuide())) {
            // COB_CODE: SET SEQUENTIAL-GUIDE       TO TRUE
            ws.getIabv0007().getWkFlagGuideType().setSequentialGuide();
        }
        else {
            // COB_CODE: SET DB-GUIDE               TO TRUE
            ws.getIabv0007().getWkFlagGuideType().setDbGuide();
            // COB_CODE: MOVE BBT-SERVICE-GUIDE-NAME
            //                                 TO WK-PGM-GUIDA
            ws.getIabv0007().setWkPgmGuida(ws.getBtcBatchType().getBbtServiceGuideName());
        }
    }

    /**Original name: S900-REPORT01O-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  SCRIVI REPORT01O BATCH
	 * ----------------------------------------------------------------*</pre>*/
    private void s900Report01oBatch() {
        // COB_CODE: MOVE 'S900-REPORT01O-BATCH'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S900-REPORT01O-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM S901-PREPARA-RIEP-BATCH THRU S901-EX
        s901PreparaRiepBatch();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE '* '                     TO REPORT01-REC
        report01To.setVariable("* ");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE LOGO-ESITI-BATCH-REPORT  TO REPORT01-REC
        report01To.setVariable(ws.getIabv0007().getIabv0010().getLogoEsitiBatchReport());
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE '* '                     TO REPORT01-REC
        report01To.setVariable("* ");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: IF IABV0006-COUNTER-STD-YES
        //              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
        //           END-IF
        if (ws.getIabv0006().getCustomCounters().getCounterStd().isIabv0006CounterStdYes()) {
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE SL-COUNT              TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getSlCount());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE LOGO-STANDARD-COUNT   TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getLogoStandardCountFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE SL-COUNT              TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getSlCount());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE BUS-SERV-ESEG-REPORT  TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getBusServEsegReport().getBusServEsegReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE BUS-SERV-OK-REPORT    TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getBusServOkReport().getBusServOkReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE BUS-SERV-OK-WARN-REPORT
            //                                      TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getBusServOkWarnReport().getBusServOkWarnReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE BUS-SERV-KO-REPORT    TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getBusServKoReport().getBusServKoReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
        }
        // COB_CODE: IF IABV0006-GUIDE-ROWS-READ-YES
        //              PERFORM WRITE-REPORT01      THRU WRITE-REPORT01-EX
        //           END-IF
        if (ws.getIabv0006().getCustomCounters().getGuideRowsRead().isIabv0006GuideRowsReadYes()) {
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE SL-COUNT              TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getSlCount());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE LOGO-CUSTOM-COUNT     TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getLogoCustomCountFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE SL-COUNT              TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getSlCount());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                 TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE TAB-GUIDE-LETTE-REPORT TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getTabGuideLetteReport().getTabGuideLetteReportFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01      THRU WRITE-REPORT01-EX
            writeReport01();
        }
        // COB_CODE: IF IABV0006-MAX-ELE-CUSTOM-COUNT > 0
        //              PERFORM S950-TRATTA-CUSTOM-COUNT THRU S950-EX
        //           END-IF
        if (ws.getIabv0006().getCustomCounters().getMaxEleCustomCount() > 0) {
            // COB_CODE: PERFORM S950-TRATTA-CUSTOM-COUNT THRU S950-EX
            s950TrattaCustomCount();
        }
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: IF GESTIONE-PARALLELISMO-YES
        //                                         TO REPORT01-REC
        //           ELSE
        //                                         TO REPORT01-REC
        //           END-IF.
        if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
            // COB_CODE: MOVE STATO-FINALE-PARALLELO-REPORT
            //                                      TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getStatoFinaleParalleloReport().getStatoFinaleParalleloReportFormatted());
        }
        else {
            // COB_CODE: MOVE STATO-FINALE-BATCH-REPORT
            //                                      TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getStatoFinaleBatchReport().getStatoFinaleBatchReportFormatted());
        }
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: MOVE ALL '*'                  TO REPORT01-REC
        report01To.setVariable(LiteralGenerator.create("*", Len.REPORT01_REC));
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX
        writeReport01();
        // COB_CODE: MOVE SPACES                   TO REPORT01-REC
        report01To.setVariable("");
        // COB_CODE: PERFORM WRITE-REPORT01        THRU WRITE-REPORT01-EX.
        writeReport01();
    }

    /**Original name: S901-PREPARA-RIEP-BATCH<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA REPORT01O BATCH
	 * ----------------------------------------------------------------*</pre>*/
    private void s901PreparaRiepBatch() {
        // COB_CODE: MOVE 'S901-PREPARA-RIEP-BATCH'   TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S901-PREPARA-RIEP-BATCH");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE CONT-TAB-GUIDE-LETTE      TO REP-TAB-GUIDE-LETTE
        ws.getIabv0007().getIabv0010().getTabGuideLetteReport().setRepTabGuideLette(ws.getIabv0007().getContTabGuideLette());
        // COB_CODE: MOVE CONT-BUS-SERV-ESEG        TO REP-BUS-SERV-ESEGUITI
        ws.getIabv0007().getIabv0010().getBusServEsegReport().setRepBusServEseguiti(ws.getIabv0007().getContBusServEseg());
        // COB_CODE: MOVE CONT-BUS-SERV-ESITO-OK    TO REP-BUS-SERV-ESITO-OK
        ws.getIabv0007().getIabv0010().getBusServOkReport().setRepBusServEsitoOk(ws.getIabv0007().getContBusServEsitoOk());
        // COB_CODE: MOVE CONT-BUS-SERV-WARNING     TO REP-BUS-SERV-WARNING
        ws.getIabv0007().getIabv0010().getBusServOkWarnReport().setRepBusServWarning(ws.getIabv0007().getContBusServWarning());
        // COB_CODE: MOVE CONT-BUS-SERV-ESITO-KO    TO REP-BUS-SERV-ESITO-KO.
        ws.getIabv0007().getIabv0010().getBusServKoReport().setRepBusServEsitoKo(ws.getIabv0007().getContBusServEsitoKo());
        // COB_CODE: MOVE ZEROES                    TO CONT-TAB-GUIDE-LETTE
        //                                             CONT-BUS-SERV-ESEG
        //                                             CONT-BUS-SERV-ESITO-OK
        //                                             CONT-BUS-SERV-WARNING
        //                                             CONT-BUS-SERV-ESITO-KO.
        ws.getIabv0007().setContTabGuideLette(0);
        ws.getIabv0007().setContBusServEseg(0);
        ws.getIabv0007().setContBusServEsitoOk(0);
        ws.getIabv0007().setContBusServWarning(0);
        ws.getIabv0007().setContBusServEsitoKo(0);
        // COB_CODE: IF GESTIONE-PARALLELISMO-YES
        //              MOVE IABV0002-STATE-CURRENT TO WK-COD-BATCH-STATE
        //           ELSE
        //              MOVE BTC-COD-BATCH-STATE    TO WK-COD-BATCH-STATE
        //           END-IF.
        if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
            // COB_CODE: MOVE IABV0002-STATE-CURRENT TO WK-COD-BATCH-STATE
            ws.getIabv0007().setWkCodBatchState(ws.getIabv0007().getIabv0002().getIabv0002StateCurrent());
        }
        else {
            // COB_CODE: MOVE BTC-COD-BATCH-STATE    TO WK-COD-BATCH-STATE
            ws.getIabv0007().setWkCodBatchState(ws.getBtcBatch().getBtcCodBatchState());
        }
        // COB_CODE: PERFORM D902-REPERISCI-DES-BATCH-STATE
        //                                          THRU D902-EX
        d902ReperisciDesBatchState();
        // COB_CODE: IF GESTIONE-PARALLELISMO-YES
        //              MOVE WK-DES-BATCH-STATE     TO REP-STATO-FINALE-P
        //           ELSE
        //              MOVE WK-DES-BATCH-STATE     TO REP-STATO-FINALE
        //           END-IF.
        if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
            // COB_CODE: MOVE WK-DES-BATCH-STATE     TO REP-STATO-FINALE-P
            ws.getIabv0007().getIabv0010().getStatoFinaleParalleloReport().setRepStatoFinaleP(ws.getIabv0007().getWkDesBatchState());
        }
        else {
            // COB_CODE: MOVE WK-DES-BATCH-STATE     TO REP-STATO-FINALE
            ws.getIabv0007().getIabv0010().getStatoFinaleBatchReport().setRepStatoFinale(ws.getIabv0007().getWkDesBatchState());
        }
    }

    /**Original name: S950-TRATTA-CUSTOM-COUNT<br>
	 * <pre>----------------------------------------------------------------*
	 *  TRATTA CUSTOM COUNT
	 * ----------------------------------------------------------------*</pre>*/
    private void s950TrattaCustomCount() {
        // COB_CODE: MOVE 'S950-TRATTA-CUSTOM-COUNT'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("S950-TRATTA-CUSTOM-COUNT");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF IABV0006-GUIDE-ROWS-READ-NO
        //              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
        //           END-IF
        if (ws.getIabv0006().getCustomCounters().getGuideRowsRead().isIabv0006GuideRowsReadNo()) {
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE SL-COUNT              TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getSlCount());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE LOGO-CUSTOM-COUNT     TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getLogoCustomCountFormatted());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: MOVE SL-COUNT              TO REPORT01-REC
            report01To.setVariable(ws.getIabv0007().getIabv0010().getSlCount());
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
        }
        // COB_CODE: PERFORM VARYING IABV0006-MAX-ELE-CUSTOM-COUNT FROM 1 BY 1
        //                     UNTIL IABV0006-MAX-ELE-CUSTOM-COUNT >
        //                           IABV0006-LIM-CUSTOM-COUNT-MAX OR
        //                           IABV0006-CUSTOM-COUNT-DESC
        //                           (IABV0006-MAX-ELE-CUSTOM-COUNT) =
        //                           SPACES OR HIGH-VALUES OR LOW-VALUES
        //              PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
        //           END-PERFORM.
        ws.getIabv0006().getCustomCounters().setMaxEleCustomCount(((short)1));
        while (!(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount() > ws.getIabv0006().getCustomCounters().getLimCustomCountMax() || Characters.EQ_SPACE.test(ws.getIabv0006().getCustomCounters().getEleErrori(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount()).getCustomCountDesc()) || Characters.EQ_HIGH.test(ws.getIabv0006().getCustomCounters().getEleErrori(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount()).getCustomCountDesc(), Iabv0006EleErrori.Len.CUSTOM_COUNT_DESC) || Characters.EQ_LOW.test(ws.getIabv0006().getCustomCounters().getEleErrori(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount()).getCustomCountDesc(), Iabv0006EleErrori.Len.CUSTOM_COUNT_DESC))) {
            // COB_CODE: MOVE SPACES                TO REPORT01-REC
            report01To.setVariable("");
            // COB_CODE: IF IABV0006-ONLY-DESC
            //                         (IABV0006-MAX-ELE-CUSTOM-COUNT)
            //                                         TO REPORT01-REC
            //           ELSE
            //              MOVE CUSTOM-REPORT         TO REPORT01-REC
            //           END-IF
            if (ws.getIabv0006().getCustomCounters().getEleErrori(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount()).getDescType().isIabv0006OnlyDesc()) {
                // COB_CODE: MOVE IABV0006-CUSTOM-COUNT-DESC
                //                      (IABV0006-MAX-ELE-CUSTOM-COUNT)
                //                                      TO REPORT01-REC
                report01To.setVariable(ws.getIabv0006().getCustomCounters().getEleErrori(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount()).getCustomCountDesc());
            }
            else {
                // COB_CODE: MOVE IABV0006-CUSTOM-COUNT-DESC
                //                      (IABV0006-MAX-ELE-CUSTOM-COUNT)
                //                    TO REP-CUSTOM-COUNT-DESC
                ws.getIabv0007().getIabv0010().getCustomReport().setDesc(ws.getIabv0006().getCustomCounters().getEleErrori(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount()).getCustomCountDesc());
                // COB_CODE: MOVE IABV0006-CUSTOM-COUNT
                //                      (IABV0006-MAX-ELE-CUSTOM-COUNT)
                //                                      TO REP-CUSTOM-COUNT
                ws.getIabv0007().getIabv0010().getCustomReport().setT(ws.getIabv0006().getCustomCounters().getEleErrori(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount()).getIabv0006CustomCount());
                // COB_CODE: MOVE CUSTOM-REPORT         TO REPORT01-REC
                report01To.setVariable(ws.getIabv0007().getIabv0010().getCustomReport().getCustomReportFormatted());
            }
            // COB_CODE: PERFORM WRITE-REPORT01     THRU WRITE-REPORT01-EX
            writeReport01();
            ws.getIabv0006().getCustomCounters().setMaxEleCustomCount(Trunc.toShort(ws.getIabv0006().getCustomCounters().getMaxEleCustomCount() + 1, 4));
        }
        // COB_CODE: PERFORM I000-INIT-CUSTOM-COUNT THRU I000-EX.
        i000InitCustomCount();
    }

    /**Original name: OPEN-REPORT01<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPEN FILE SEQUENZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void openReport01() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'OPEN-REPORT01'             TO WK-LABEL.
        ws.getIabv0007().setWkLabel("OPEN-REPORT01");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: OPEN OUTPUT REPORT01.
        report01DAO.open(OpenMode.WRITE, "Lrgm0380");
        ws.getIabv0007().setFsReport01(report01DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: IF FS-REPORT01 NOT = '00'
        //                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           ELSE
        //              SET FILE-REPORT01-APERTO      TO TRUE
        //           END-IF.
        if (!Conditions.eq(ws.getIabv0007().getFsReport01(), "00")) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
            ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
            // COB_CODE: MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE
            ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'OPEN ERRATA DEL FILE '
            //                   DELIMITED BY SIZE
            //                   'REPORT01'
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                   DELIMITED BY SIZE
            //                   'FS : '
            //                   DELIMITED BY SIZE
            //                   FS-REPORT01
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "OPEN ERRATA DEL FILE ", "REPORT01", " - ", "FS : ", ws.getIabv0007().getFsReport01Formatted());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
        else {
            // COB_CODE: SET FILE-REPORT01-APERTO      TO TRUE
            ws.getIabv0007().setWkOpenReport(true);
        }
    }

    /**Original name: WRITE-REPORT01<br>
	 * <pre>----------------------------------------------------------------*
	 *  WRITE FILE SEQUENZIALE
	 * ----------------------------------------------------------------*</pre>*/
    private void writeReport01() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'WRITE-REPORT01'            TO WK-LABEL
        ws.getIabv0007().setWkLabel("WRITE-REPORT01");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: WRITE REPORT01-REC.
        report01DAO.write(report01To);
        ws.getIabv0007().setFsReport01(report01DAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: IF FS-REPORT01 NOT = '00'
        //                 PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-IF.
        if (!Conditions.eq(ws.getIabv0007().getFsReport01(), "00")) {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
            ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
            // COB_CODE: MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE
            ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'WRITE ERRATA DEL FILE '
            //                   DELIMITED BY SIZE
            //                   'REPORT01'
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                   DELIMITED BY SIZE
            //                   'FS : '
            //                   DELIMITED BY SIZE
            //                   FS-REPORT01
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "WRITE ERRATA DEL FILE ", "REPORT01", " - ", "FS : ", ws.getIabv0007().getFsReport01Formatted());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: P900-CNTL-FINALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLO FINALE
	 * ----------------------------------------------------------------*</pre>*/
    private void p900CntlFinale() {
        // COB_CODE: MOVE 'P900-CNTL-FINALE'          TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P900-CNTL-FINALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE IABI0011-CODICE-COMPAGNIA-ANIA TO BPA-COD-COMP-ANIA
        ws.getBtcParallelism().setBpaCodCompAnia(ws.getIabv0007().getIabi0011Area().getCodiceCompagniaAnia());
        // COB_CODE: MOVE BTC-PROTOCOL                   TO BPA-PROTOCOL
        ws.getBtcParallelism().setBpaProtocol(ws.getBtcBatch().getBtcProtocol());
        // COB_CODE: MOVE IABI0011-PROG-PROTOCOL         TO BPA-PROG-PROTOCOL
        ws.getBtcParallelism().setBpaProgProtocol(ws.getIabv0007().getIabi0011Area().getProgProtocol());
        // COB_CODE: PERFORM P950-PREPARA-CNTL-FINALE    THRU P950-EX.
        p950PreparaCntlFinale();
        // COB_CODE: PERFORM P500-CALL-IABS0130          THRU P500-EX.
        p500CallIabs0130();
        // COB_CODE: PERFORM P951-ESITO-CNTL-FINALE      THRU P951-EX.
        p951EsitoCntlFinale();
    }

    /**Original name: P950-PREPARA-CNTL-FINALE<br>
	 * <pre>----------------------------------------------------------------*
	 *  PREPARA CONTROLLO FINALE
	 * ----------------------------------------------------------------*</pre>*/
    private void p950PreparaCntlFinale() {
        // COB_CODE: MOVE 'P950-PREPARA-CNTL-FINALE'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("P950-PREPARA-CNTL-FINALE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET WS-FASE-CONTROLLO-FINALE    TO TRUE.
        ws.getIabv0007().getWsBufferWhereCondIabs0130().getWsFlagFase().setWsFaseControlloFinale();
        // COB_CODE: MOVE WS-BUFFER-WHERE-COND-IABS0130
        //                                      TO IDSV0003-BUFFER-WHERE-COND.
        ws.getIabv0007().getIdsv0003().setBufferWhereCond(ws.getIabv0007().getWsBufferWhereCondIabs0130().getWsBufferWhereCondIabs0130Formatted());
    }

    /**Original name: T000-ACCEPT-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *  ACCEPT TIMESTAMP
	 * ----------------------------------------------------------------*</pre>*/
    private void t000AcceptTimestamp() {
        // COB_CODE: ACCEPT WK-TIMESTAMP(1:8)         FROM DATE YYYYMMDD.
        ws.getIabv0007().setWkTimestampFormatted(Functions.setSubstring(ws.getIabv0007().getWkTimestampFormatted(), CalendarUtil.getDateYYYYMMDD(), 1, 8));
        // COB_CODE: ACCEPT WK-TIMESTAMP(9:6)         FROM TIME.
        ws.getIabv0007().setWkTimestampFormatted(Functions.setSubstring(ws.getIabv0007().getWkTimestampFormatted(), CalendarUtil.getTimeHHMMSSMM(), 9, 6));
        // COB_CODE: MOVE   WK-TIMESTAMP(1:4)         TO AAAA-SYS.
        ws.getIabv0007().getWkTimestamp26().setAaaaSys(ws.getIabv0007().getWkTimestampFormatted().substring((1) - 1, 4));
        // COB_CODE: MOVE   WK-TIMESTAMP(5:2)         TO MM-SYS.
        ws.getIabv0007().getWkTimestamp26().setMmSys(ws.getIabv0007().getWkTimestampFormatted().substring((5) - 1, 6));
        // COB_CODE: MOVE   WK-TIMESTAMP(7:2)         TO GG-SYS.
        ws.getIabv0007().getWkTimestamp26().setGgSys(ws.getIabv0007().getWkTimestampFormatted().substring((7) - 1, 8));
        // COB_CODE: MOVE   WK-TIMESTAMP(09:2)        TO HH-SYS.
        ws.getIabv0007().getWkTimestamp26().setHhSys(ws.getIabv0007().getWkTimestampFormatted().substring((9) - 1, 10));
        // COB_CODE: MOVE   WK-TIMESTAMP(11:2)        TO MI-SYS.
        ws.getIabv0007().getWkTimestamp26().setMiSys(ws.getIabv0007().getWkTimestampFormatted().substring((11) - 1, 12));
        // COB_CODE: MOVE   WK-TIMESTAMP(13:2)        TO SS-SYS.
        ws.getIabv0007().getWkTimestamp26().setSsSys(ws.getIabv0007().getWkTimestampFormatted().substring((13) - 1, 14));
    }

    /**Original name: V000-VALORIZZAZIONE-VERSIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *  valorizzazione versione
	 * ----------------------------------------------------------------*</pre>*/
    private void v000ValorizzazioneVersione() {
        // COB_CODE: MOVE 'V000-VALORIZZAZIONE-VERSIONE' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("V000-VALORIZZAZIONE-VERSIONE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--    eventuale valorizzazione
        //--    del campo IABV0009-VERSIONING PER TABELLA GUIDA
        //--    con il campo WK-VERSIONING
        //--    modalit&#x17; "RIPARTENZA CON VERSIONAMENTO"
        // COB_CODE: MOVE WK-VERSIONING       TO IABV0009-VERSIONING.
        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setVersioning(ws.getIabv0007().getWkVersioning());
    }

    /**Original name: V100-VERIFICA-PRENOTAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA ESEGUIBILITA' PRENOTAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void v100VerificaPrenotazione() {
        ConcatUtil concatUtil = null;
        Iabs0900 iabs0900 = null;
        // COB_CODE: MOVE 'V100-VERIFICA-PRENOTAZIONE'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("V100-VERIFICA-PRENOTAZIONE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE BTC-ID-RICH                   TO RIC-ID-RICH
        //                                                 WK-ID-RICH
        //                                                 IABV0006-ID-RICH
        ws.getRich().setRicIdRich(ws.getBtcBatch().getBtcIdRich().getBtcIdRich());
        ws.getIabv0007().setWkIdRich(TruncAbs.toInt(ws.getBtcBatch().getBtcIdRich().getBtcIdRich(), 9));
        ws.getIabv0006().setIdRich(ws.getBtcBatch().getBtcIdRich().getBtcIdRich());
        // COB_CODE: SET WK-VERIFICA-PRENOTAZIONE-KO    TO TRUE
        ws.getIabv0007().setFlagVerificaPrenotazione(false);
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-SELECT                TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setSelect();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC        TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: CALL PGM-IABS0900 USING IDSV0003
        //                                   IABV0002
        //                                   RICH
        //                                   IABV0003
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL
        try {
            iabs0900 = Iabs0900.getInstance();
            iabs0900.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getRich(), ws.getIabv0007().getIabv0003());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                DELIMITED BY SIZE
            //                PGM-IABS0900
            //                DELIMITED BY SIZE
            //                ' - '
            //                 DELIMITED BY SIZE INTO
            //                WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getIabv0004().getPgmIabs0900Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:         IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                      WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                //                         END-IF
                //                      WHEN IDSV0003-NEGATIVI
                //           *--->      ERRORE DI ACCESSO AL DB
                //                        PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE RIC-DS-STATO-ELAB TO WK-COD-ELAB-STATE
                    ws.getIabv0007().setWkCodElabState(ws.getRich().getRicDsStatoElab());
                    // COB_CODE: PERFORM D901-VERIFICA-ELAB-STATE THRU D901-EX
                    d901VerificaElabState();
                    // COB_CODE: IF WK-RISULT-VERIF-ELAB-STATE-OK
                    //              SET WK-VERIFICA-PRENOTAZIONE-OK  TO TRUE
                    //           END-IF
                    if (ws.getIabv0007().isFlagRisultVerifElabState()) {
                        // COB_CODE: SET WK-VERIFICA-PRENOTAZIONE-OK  TO TRUE
                        ws.getIabv0007().setFlagVerificaPrenotazione(true);
                    }
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->      ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0900
                    //                  DELIMITED BY SIZE
                    //                  IDSV0003-DESCRIZ-ERR-DB2
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0900Formatted(), ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0900
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0900Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: W000-CARICA-LOG-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA LOG_ERRORE IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void w000CaricaLogErrore() {
        // COB_CODE: MOVE 'W000-CARICA-LOG-ERRORE'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("W000-CARICA-LOG-ERRORE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: PERFORM VARYING IND-MAX-ELE-ERRORI FROM 1 BY 1
        //               UNTIL IND-MAX-ELE-ERRORI > IDSV0001-MAX-ELE-ERRORI
        //                  OR WK-ERRORE-YES
        //                  END-IF
        //           END-PERFORM.
        ws.getIabv0007().setIndMaxEleErrori(((short)1));
        while (!(ws.getIabv0007().getIndMaxEleErrori() > ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori() || ws.getIabv0007().isWkErrore())) {
            // COB_CODE: IF IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
            //              ERRORE-FATALE
            //              SET WK-ERRORE-FATALE          TO TRUE
            //           END-IF
            if (ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001LivGravitaBe() == ws.getIabv0007().getWkLivelloGravita().getErroreFatale()) {
                // COB_CODE: SET WK-SKIP-BATCH-YES            TO TRUE
                ws.getIabv0007().setFlagSkipBatch(true);
                // COB_CODE: SET WK-ERRORE-FATALE          TO TRUE
                ws.getIabv0007().getWkTipoErrore().setFatale();
            }
            // COB_CODE: IF IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
            //              ERRORE-BLOCCANTE
            //              SET WK-RC-08-TROVATO           TO TRUE
            //           END-IF
            if (ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001LivGravitaBe() == ws.getIabv0007().getWkLivelloGravita().getErroreBloccante()) {
                // COB_CODE: SET WK-RC-08-TROVATO           TO TRUE
                ws.getIabv0007().getFlagRc04().setRc08Trovato();
            }
            // COB_CODE: IF IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
            //              WARNING
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001LivGravitaBe() == ws.getIabv0007().getWkLivelloGravita().getWarning()) {
                // COB_CODE: SET WARNING-TROVATO-YES TO TRUE
                ws.getIabv0007().setFlagWarningTrovato(true);
                // COB_CODE: IF  NOT WK-RC-08-TROVATO
                //               END-IF
                //           END-IF
                if (!ws.getIabv0007().getFlagRc04().isRc08Trovato()) {
                    // COB_CODE: IF  IDSV0001-FORZ-RC-04-YES
                    //               SET WK-RC-04-FORZATO        TO TRUE
                    //           END-IF
                    if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getForzRc04().isIdsv0001ForzRc04Yes()) {
                        // COB_CODE: SET WK-RC-04-FORZATO        TO TRUE
                        ws.getIabv0007().getFlagRc04().setRc04Forzato();
                    }
                    // COB_CODE: IF  NOT WK-RC-04-FORZATO
                    //               SET WK-RC-04-NON-FORZATO    TO TRUE
                    //           END-IF
                    if (!ws.getIabv0007().getFlagRc04().isRc04Forzato()) {
                        // COB_CODE: SET WK-RC-04-NON-FORZATO    TO TRUE
                        ws.getIabv0007().getFlagRc04().setRc04NonForzato();
                    }
                }
            }
            // COB_CODE: IF IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) >=
            //              IABI0011-LIVELLO-GRAVITA-LOG
            //              END-IF
            //           END-IF
            if (Conditions.gte(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001LivGravitaBeFormatted(), rTrim(String.valueOf(ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().getFlagModCalcPreP())))) {
                // COB_CODE: INITIALIZE                WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE WK-SESSIONE-N09   TO WK-LOR-ID-LOG-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdLogErrore(ws.getIabv0007().getWkSessioneN09());
                // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
                //                                  TO WK-LOR-COD-MAIN-BATCH
                ws.getIabv0007().getWkLogErrore().setCodMainBatch(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getCodMainBatch());
                // COB_CODE: MOVE IDSV0001-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI)
                //                                  TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getIdsv0001LivGravitaBe());
                // COB_CODE: MOVE IDSV0001-DESC-ERRORE
                //                                  (IND-MAX-ELE-ERRORI)
                //                                  TO WK-LOR-DESC-ERRORE-ESTESA
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getDescErrore());
                // COB_CODE: IF IABV0008-SCRIVI-LOG-ERRORE-SI
                //              PERFORM W100-INS-LOG-ERRORE  THRU W100-EX
                //           END-IF
                if (ws.getIabv0008().isScritturaLogErrore()) {
                    // COB_CODE: MOVE IDSV0001-SESSIONE       TO WK-SESSIONE
                    ws.getIabv0007().setWkSessioneFormatted(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getSessioneFormatted());
                    // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
                    ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
                    // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
                    ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
                    // COB_CODE: PERFORM W100-INS-LOG-ERRORE  THRU W100-EX
                    w100InsLogErrore();
                }
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF IND-MAX-ELE-ERRORI =
                    //              IDSV0001-MAX-ELE-ERRORI
                    //              END-IF
                    //           END-IF
                    if (ws.getIabv0007().getIndMaxEleErrori() == ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori()) {
                        // COB_CODE: IF IDSV0001-DESC-ERRORE-ESTESA =
                        //                WK-LOR-DESC-ERRORE-ESTESA
                        //                                      THRU W005-EX
                        //           END-IF
                        if (Conditions.eq(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getDescErroreEstesa(), ws.getIabv0007().getWkLogErrore().getDescErroreEstesa())) {
                            // COB_CODE: PERFORM W005-VALORIZZA-DA-ULTIMO
                            //                                 THRU W005-EX
                            w005ValorizzaDaUltimo();
                        }
                    }
                    // COB_CODE: IF WK-ERRORE-NO
                    //              PERFORM S200-DETTAGLIO-ERRORE THRU S200-EX
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM S200-DETTAGLIO-ERRORE THRU S200-EX
                        s200DettaglioErrore();
                    }
                }
            }
            ws.getIabv0007().setIndMaxEleErrori(Trunc.toShort(ws.getIabv0007().getIndMaxEleErrori() + 1, 4));
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              PERFORM W001-CARICA-LOG-ERRORE-EXTRA THRU W001-EX
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM W001-CARICA-LOG-ERRORE-EXTRA THRU W001-EX
            w001CaricaLogErroreExtra();
        }
        // CONTIENE UNA SEQUENZA DI ISTRUZIONI PER AZZERAR 'AREA DEGLI
        //ERRORI DELL'AREA CONTESTO.
        // COB_CODE: MOVE 'OK'   TO IDSV0001-ESITO
        ws.getIabv0007().getAreaIdsv0001().getEsito().setEsito("OK");
        // COB_CODE: MOVE SPACES TO IDSV0001-LOG-ERRORE
        ws.getIabv0007().getAreaIdsv0001().getLogErrore().initLogErroreSpaces();
        // COB_CODE: SET IDSV0001-FORZ-RC-04-NO        TO TRUE
        ws.getIabv0007().getAreaIdsv0001().getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        // COB_CODE: PERFORM VARYING IDSV0001-MAX-ELE-ERRORI FROM 1 BY 1
        //                     UNTIL IDSV0001-MAX-ELE-ERRORI > 10
        //               TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
        //           END-PERFORM
        ws.getIabv0007().getAreaIdsv0001().setMaxEleErrori(((short)1));
        while (!(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori() > 10)) {
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori()).setDescErrore("");
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori()).setIdsv0001CodErrore(0);
            // COB_CODE: MOVE ZEROES
            //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)0));
            // COB_CODE: MOVE SPACES
            //             TO IDSV0001-TIPO-TRATT-FE(IDSV0001-MAX-ELE-ERRORI)
            ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori()).setTipoTrattFe(Types.SPACE_CHAR);
            ws.getIabv0007().getAreaIdsv0001().setMaxEleErrori(Trunc.toShort(ws.getIabv0007().getAreaIdsv0001().getMaxEleErrori() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES TO IDSV0001-MAX-ELE-ERRORI.
        ws.getIabv0007().getAreaIdsv0001().setMaxEleErrori(((short)0));
    }

    /**Original name: W001-CARICA-LOG-ERRORE-EXTRA<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA LOG_ERRORE IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void w001CaricaLogErroreExtra() {
        // COB_CODE: MOVE 'W001-CARICA-LOG-ERRORE-EXTRA' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("W001-CARICA-LOG-ERRORE-EXTRA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: PERFORM VARYING IND-MAX-ELE-ERRORI FROM 1 BY 1
        //               UNTIL IND-MAX-ELE-ERRORI > IEAV9904-MAX-ELE-ERRORI
        //                  OR WK-ERRORE-YES
        //                  END-IF
        //           END-PERFORM.
        ws.getIabv0007().setIndMaxEleErrori(((short)1));
        while (!(ws.getIabv0007().getIndMaxEleErrori() > ws.getIeav9904().getMaxEleErrori() || ws.getIabv0007().isWkErrore())) {
            // COB_CODE: IF IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
            //              ERRORE-FATALE
            //              SET WK-ERRORE-FATALE          TO TRUE
            //           END-IF
            if (ws.getIeav9904().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getLivGravitaBe() == ws.getIabv0007().getWkLivelloGravita().getErroreFatale()) {
                // COB_CODE: SET WK-SKIP-BATCH-YES            TO TRUE
                ws.getIabv0007().setFlagSkipBatch(true);
                // COB_CODE: SET WK-ERRORE-FATALE          TO TRUE
                ws.getIabv0007().getWkTipoErrore().setFatale();
            }
            // COB_CODE: IF IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
            //              ERRORE-BLOCCANTE
            //              SET WK-RC-08-TROVATO           TO TRUE
            //           END-IF
            if (ws.getIeav9904().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getLivGravitaBe() == ws.getIabv0007().getWkLivelloGravita().getErroreBloccante()) {
                // COB_CODE: SET WK-RC-08-TROVATO           TO TRUE
                ws.getIabv0007().getFlagRc04().setRc08Trovato();
            }
            // COB_CODE: IF IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) =
            //              WARNING
            //              END-IF
            //           END-IF
            if (ws.getIeav9904().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getLivGravitaBe() == ws.getIabv0007().getWkLivelloGravita().getWarning()) {
                // COB_CODE: SET WARNING-TROVATO-YES TO TRUE
                ws.getIabv0007().setFlagWarningTrovato(true);
                // COB_CODE: IF  NOT WK-RC-08-TROVATO
                //               END-IF
                //           END-IF
                if (!ws.getIabv0007().getFlagRc04().isRc08Trovato()) {
                    // COB_CODE: IF  IDSV0001-FORZ-RC-04-YES
                    //               SET WK-RC-04-FORZATO        TO TRUE
                    //           END-IF
                    if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getForzRc04().isIdsv0001ForzRc04Yes()) {
                        // COB_CODE: SET WK-RC-04-FORZATO        TO TRUE
                        ws.getIabv0007().getFlagRc04().setRc04Forzato();
                    }
                    // COB_CODE: IF  NOT WK-RC-04-FORZATO
                    //               SET WK-RC-04-NON-FORZATO    TO TRUE
                    //           END-IF
                    if (!ws.getIabv0007().getFlagRc04().isRc04Forzato()) {
                        // COB_CODE: SET WK-RC-04-NON-FORZATO    TO TRUE
                        ws.getIabv0007().getFlagRc04().setRc04NonForzato();
                    }
                }
            }
            // COB_CODE: IF IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI) >=
            //              IABI0011-LIVELLO-GRAVITA-LOG
            //              END-IF
            //           END-IF
            if (Conditions.gte(ws.getIeav9904().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getLivGravitaBeFormatted(), rTrim(String.valueOf(ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().getFlagModCalcPreP())))) {
                // COB_CODE: INITIALIZE                WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE WK-SESSIONE-N09   TO WK-LOR-ID-LOG-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdLogErrore(ws.getIabv0007().getWkSessioneN09());
                // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
                //                                  TO WK-LOR-COD-MAIN-BATCH
                ws.getIabv0007().getWkLogErrore().setCodMainBatch(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getCodMainBatch());
                // COB_CODE: MOVE IEAV9904-LIV-GRAVITA-BE(IND-MAX-ELE-ERRORI)
                //                                  TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIeav9904().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getLivGravitaBe());
                // COB_CODE: MOVE IEAV9904-DESC-ERRORE
                //                                  (IND-MAX-ELE-ERRORI)
                //                                  TO WK-LOR-DESC-ERRORE-ESTESA
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIeav9904().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).getDescErrore());
                // COB_CODE: IF IABV0008-SCRIVI-LOG-ERRORE-SI
                //              PERFORM W100-INS-LOG-ERRORE  THRU W100-EX
                //           END-IF
                if (ws.getIabv0008().isScritturaLogErrore()) {
                    // COB_CODE: MOVE IDSV0001-SESSIONE       TO WK-SESSIONE
                    ws.getIabv0007().setWkSessioneFormatted(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getSessioneFormatted());
                    // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC  TO TRUE
                    ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
                    // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL TO TRUE
                    ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
                    // COB_CODE: PERFORM W100-INS-LOG-ERRORE  THRU W100-EX
                    w100InsLogErrore();
                }
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF IND-MAX-ELE-ERRORI = IEAV9904-MAX-ELE-ERRORI
                    //              END-IF
                    //           END-IF
                    if (ws.getIabv0007().getIndMaxEleErrori() == ws.getIeav9904().getMaxEleErrori()) {
                        // COB_CODE: IF IDSV0001-DESC-ERRORE-ESTESA =
                        //                WK-LOR-DESC-ERRORE-ESTESA
                        //                                     THRU W005-EX
                        //           END-IF
                        if (Conditions.eq(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getDescErroreEstesa(), ws.getIabv0007().getWkLogErrore().getDescErroreEstesa())) {
                            // COB_CODE: PERFORM W005-VALORIZZA-DA-ULTIMO
                            //                                  THRU W005-EX
                            w005ValorizzaDaUltimo();
                        }
                    }
                    // COB_CODE: IF WK-ERRORE-NO
                    //                                        THRU S205-EX
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM S205-DETTAGLIO-ERRORE-EXTRA
                        //                                     THRU S205-EX
                        s205DettaglioErroreExtra();
                    }
                }
            }
            ws.getIabv0007().setIndMaxEleErrori(Trunc.toShort(ws.getIabv0007().getIndMaxEleErrori() + 1, 4));
        }
        // COB_CODE: IF IEAV9904-MAX-ELE-ERRORI > 0
        //              END-IF
        //           END-IF.
        if (ws.getIeav9904().getMaxEleErrori() > 0) {
            // COB_CODE: IF IDSV0001-DESC-ERRORE-ESTESA NOT =
            //                WK-LOR-DESC-ERRORE-ESTESA
            //              END-IF
            //           END-IF
            if (!Conditions.eq(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getDescErroreEstesa(), ws.getIabv0007().getWkLogErrore().getDescErroreEstesa())) {
                // COB_CODE: MOVE IDSV0001-DESC-ERRORE-ESTESA
                //                       TO WK-LOR-DESC-ERRORE-ESTESA
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getDescErroreEstesa());
                // COB_CODE: IF WK-ERRORE-NO
                //                                               THRU S205-EX
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: PERFORM S205-DETTAGLIO-ERRORE-EXTRA
                    //                                            THRU S205-EX
                    s205DettaglioErroreExtra();
                }
            }
        }
        // CONTIENE UNA SEQUENZA DI ISTRUZIONI PER AZZERAR 'AREA DEGLI
        //ERRORI DELL'AREA CONTESTO.
        // COB_CODE: PERFORM VARYING IEAV9904-MAX-ELE-ERRORI FROM 1 BY 1
        //                     UNTIL IEAV9904-MAX-ELE-ERRORI >
        //                           IEAV9904-LIMITE-MAX
        //               TO IEAV9904-TIPO-TRATT-FE(IEAV9904-MAX-ELE-ERRORI)
        //           END-PERFORM
        ws.getIeav9904().setMaxEleErrori(((short)1));
        while (!(ws.getIeav9904().getMaxEleErrori() > ws.getIeav9904().getLimiteMax())) {
            // COB_CODE: MOVE SPACES
            //             TO IEAV9904-DESC-ERRORE(IEAV9904-MAX-ELE-ERRORI)
            ws.getIeav9904().getEleErrori(ws.getIeav9904().getMaxEleErrori()).setDescErrore("");
            // COB_CODE: MOVE ZEROES
            //             TO IEAV9904-COD-ERRORE(IEAV9904-MAX-ELE-ERRORI)
            ws.getIeav9904().getEleErrori(ws.getIeav9904().getMaxEleErrori()).setCodErrore(0);
            // COB_CODE: MOVE ZEROES
            //             TO IEAV9904-LIV-GRAVITA-BE(IEAV9904-MAX-ELE-ERRORI)
            ws.getIeav9904().getEleErrori(ws.getIeav9904().getMaxEleErrori()).setLivGravitaBe(((short)0));
            // COB_CODE: MOVE SPACES
            //             TO IEAV9904-TIPO-TRATT-FE(IEAV9904-MAX-ELE-ERRORI)
            ws.getIeav9904().getEleErrori(ws.getIeav9904().getMaxEleErrori()).setTipoTrattFe(Types.SPACE_CHAR);
            ws.getIeav9904().setMaxEleErrori(Trunc.toShort(ws.getIeav9904().getMaxEleErrori() + 1, 4));
        }
        // COB_CODE: MOVE ZEROES TO IEAV9904-MAX-ELE-ERRORI.
        ws.getIeav9904().setMaxEleErrori(((short)0));
    }

    /**Original name: W005-VALORIZZA-DA-ULTIMO<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    private void w005ValorizzaDaUltimo() {
        // COB_CODE: MOVE IDSV0001-COD-SERVIZIO-BE TO WK-LOR-COD-SERVIZIO-BE
        ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getCodServizioBe());
        // COB_CODE: MOVE IDSV0001-LABEL-ERR       TO WK-LOR-LABEL-ERR
        ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getLabelErr());
        // COB_CODE: MOVE IDSV0001-OPER-TABELLA    TO WK-LOR-OPER-TABELLA
        ws.getIabv0007().getWkLogErrore().setOperTabella(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getOperTabella());
        // COB_CODE: MOVE IDSV0001-NOME-TABELLA    TO WK-LOR-NOME-TABELLA
        ws.getIabv0007().getWkLogErrore().setNomeTabella(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getNomeTabella());
        // COB_CODE: MOVE IDSV0001-STATUS-TABELLA  TO WK-LOR-STATUS-TABELLA
        ws.getIabv0007().getWkLogErrore().setStatusTabella(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getStatusTabella());
        // COB_CODE: MOVE IDSV0001-KEY-TABELLA     TO WK-LOR-KEY-TABELLA.
        ws.getIabv0007().getWkLogErrore().setKeyTabella(ws.getIabv0007().getAreaIdsv0001().getLogErrore().getKeyTabella());
    }

    /**Original name: W070-CARICA-LOG-ERR-BAT-EXEC<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA LOG_ERRORE IN WORKING DA BATCH EXECUTOR
	 * ----------------------------------------------------------------*</pre>*/
    private void w070CaricaLogErrBatExec() {
        // COB_CODE: MOVE 'W070-CARICA-LOG-ERR-BAT-EXEC' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("W070-CARICA-LOG-ERR-BAT-EXEC");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: MOVE HIGH-VALUES                 TO LOG-ERRORE
        ws.getLogErrore().initLogErroreHighValues();
        // COB_CODE: MOVE IDSV0001-SESSIONE           TO WK-SESSIONE
        ws.getIabv0007().setWkSessioneFormatted(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getSessioneFormatted());
        // COB_CODE: MOVE 1                           TO IND-MAX-ELE-ERRORI
        ws.getIabv0007().setIndMaxEleErrori(((short)1));
        // COB_CODE: MOVE WK-SESSIONE-N09             TO WK-LOR-ID-LOG-ERRORE
        ws.getIabv0007().getWkLogErrore().setIdLogErrore(ws.getIabv0007().getWkSessioneN09());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH     TO WK-LOR-COD-MAIN-BATCH
        ws.getIabv0007().getWkLogErrore().setCodMainBatch(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE WK-LOR-DESC-ERRORE-ESTESA   TO IDSV0001-DESC-ERRORE
        //                                               (IND-MAX-ELE-ERRORI)
        ws.getIabv0007().getAreaIdsv0001().getEleErrori(ws.getIabv0007().getIndMaxEleErrori()).setDescErrore(ws.getIabv0007().getWkLogErrore().getDescErroreEstesa());
        // COB_CODE: MOVE WK-PGM                      TO WK-LOR-COD-SERVIZIO-BE
        ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
        // COB_CODE: MOVE WK-LABEL                    TO WK-LOR-LABEL-ERR
        ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
        // COB_CODE: IF IABV0008-SCRIVI-LOG-ERRORE-SI
        //              PERFORM W100-INS-LOG-ERRORE   THRU W100-EX
        //           END-IF
        if (ws.getIabv0008().isScritturaLogErrore()) {
            // COB_CODE: PERFORM W100-INS-LOG-ERRORE   THRU W100-EX
            w100InsLogErrore();
        }
        // COB_CODE: IF STAMPA-TESTATA-JCL-YES
        //              SET STAMPA-TESTATA-JCL-NO     TO TRUE
        //           END-IF
        if (ws.getIabv0007().isFlagStampaTestataJcl()) {
            // COB_CODE: PERFORM S100-TESTATA-JCL      THRU S100-EX
            s100TestataJcl();
            // COB_CODE: SET STAMPA-TESTATA-JCL-NO     TO TRUE
            ws.getIabv0007().setFlagStampaTestataJcl(false);
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              PERFORM S200-DETTAGLIO-ERRORE THRU S200-EX
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM S200-DETTAGLIO-ERRORE THRU S200-EX
            s200DettaglioErrore();
        }
        // COB_CODE: IF WK-ERRORE-BLOCCANTE
        //              SET WK-RC-08                     TO TRUE
        //           END-IF
        if (ws.getIabv0007().getWkTipoErrore().isBloccante()) {
            // COB_CODE: SET WK-ERRORE-YES                TO TRUE
            ws.getIabv0007().setWkErrore(true);
            // COB_CODE: SET WK-RC-08                     TO TRUE
            ws.getIabv0007().getFlagReturnCode().setRc08();
        }
        // COB_CODE: IF WK-ERRORE-FATALE
        //              SET WK-RC-12                     TO TRUE
        //           END-IF
        if (ws.getIabv0007().getWkTipoErrore().isFatale()) {
            // COB_CODE: SET WK-ERRORE-YES                TO TRUE
            ws.getIabv0007().setWkErrore(true);
            // COB_CODE: SET WK-RC-12                     TO TRUE
            ws.getIabv0007().getFlagReturnCode().setRc12();
        }
        // COB_CODE: SET WK-ERRORE-BLOCCANTE          TO TRUE.
        ws.getIabv0007().getWkTipoErrore().setBloccante();
        // COB_CODE: MOVE 0                           TO IND-MAX-ELE-ERRORI.
        ws.getIabv0007().setIndMaxEleErrori(((short)0));
    }

    /**Original name: W100-INS-LOG-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *  INSERIMENTO LOG ERRORE
	 * ----------------------------------------------------------------*</pre>*/
    private void w100InsLogErrore() {
        ConcatUtil concatUtil = null;
        Iabs0120 iabs0120 = null;
        // COB_CODE: MOVE 'W100-INS-LOG-ERRORE'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("W100-INS-LOG-ERRORE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE HIGH-VALUES               TO LOG-ERRORE.
        ws.getLogErrore().initLogErroreHighValues();
        // COB_CODE: MOVE WK-LOR-ID-LOG-ERRORE      TO LOR-ID-LOG-ERRORE
        ws.getLogErrore().setLorIdLogErrore(ws.getIabv0007().getWkLogErrore().getIdLogErrore());
        // COB_CODE: MOVE WK-LOR-ID-GRAVITA-ERRORE  TO LOR-ID-GRAVITA-ERRORE
        ws.getLogErrore().setLorIdGravitaErrore(ws.getIabv0007().getWkLogErrore().getIdGravitaErrore());
        // COB_CODE: MOVE WK-LOR-DESC-ERRORE-ESTESA TO LOR-DESC-ERRORE-ESTESA
        ws.getLogErrore().setLorDescErroreEstesa(ws.getIabv0007().getWkLogErrore().getDescErroreEstesa());
        // COB_CODE: MOVE WK-LOR-COD-MAIN-BATCH     TO LOR-COD-MAIN-BATCH
        ws.getLogErrore().setLorCodMainBatch(ws.getIabv0007().getWkLogErrore().getCodMainBatch());
        // COB_CODE: MOVE WK-LOR-COD-SERVIZIO-BE    TO LOR-COD-SERVIZIO-BE
        ws.getLogErrore().setLorCodServizioBe(ws.getIabv0007().getWkLogErrore().getCodServizioBe());
        // COB_CODE: MOVE WK-LOR-LABEL-ERR          TO LOR-LABEL-ERR
        ws.getLogErrore().setLorLabelErr(ws.getIabv0007().getWkLogErrore().getLabelErr());
        // COB_CODE: MOVE WK-LOR-OPER-TABELLA       TO LOR-OPER-TABELLA
        ws.getLogErrore().setLorOperTabella(ws.getIabv0007().getWkLogErrore().getOperTabella());
        // COB_CODE: MOVE WK-LOR-NOME-TABELLA       TO LOR-NOME-TABELLA
        ws.getLogErrore().setLorNomeTabella(ws.getIabv0007().getWkLogErrore().getNomeTabella());
        // COB_CODE: MOVE WK-LOR-STATUS-TABELLA     TO LOR-STATUS-TABELLA
        ws.getLogErrore().setLorStatusTabella(ws.getIabv0007().getWkLogErrore().getStatusTabella());
        // COB_CODE: MOVE WK-LOR-KEY-TABELLA        TO LOR-KEY-TABELLA
        ws.getLogErrore().setLorKeyTabella(ws.getIabv0007().getWkLogErrore().getKeyTabella());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-INSERT            TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setInsert();
        // COB_CODE: SET IDSV0003-PRIMARY-KEY       TO TRUE.
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR  TO TRUE.
        ws.getIabv0007().getIdsv0003().getTrattamentoStoricita().setTrattSenzaStor();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL PGM-IABS0120              USING IDSV0003
        //                                                LOG-ERRORE
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL
        try {
            iabs0120 = Iabs0120.getInstance();
            iabs0120.run(ws.getIabv0007().getIdsv0003(), ws.getLogErrore());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                   DELIMITED BY SIZE
            //                   PGM-IABS0120
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getIabv0004().getPgmIabs0120Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //                 END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE:            IF IDSV0003-SUCCESSFUL-RC
            //                         END-EVALUATE
            //                      ELSE
            //           *-->          GESTIRE ERRORE DISPATCHER
            //                         PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //                      END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:               EVALUATE TRUE
                //                             WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                  CONTINUE
                //                             WHEN IDSV0003-NEGATIVI
                //           *--->   ERRORE DI ACCESSO AL DB
                //                            PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //                         END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                //-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->   ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                  DELIMITED BY SIZE
                    //                  PGM-IABS0120
                    //                  DELIMITED BY SIZE
                    //                  ' SQLCODE : '
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0120Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
            else {
                //-->          GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0120
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-DESCRIZ-ERR-DB2
                //                   DELIMITED BY '   '
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0120Formatted(), " - ", Functions.substringBefore(ws.getIabv0007().getIdsv0003().getCampiEsito().getDescrizErrDb2Formatted(), "   "), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted()});
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
    }

    /**Original name: Y000-COMMIT<br>
	 * <pre>****************************************************************</pre>*/
    private void y000Commit() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'Y000-COMMIT'               TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Y000-COMMIT");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: EXEC SQL
        //                COMMIT WORK
        //           END-EXEC.
        DbService.getCurrent().commit(dbAccessStatus);
        // COB_CODE: MOVE SQLCODE             TO IDSV0003-SQLCODE
        ws.getIabv0007().getIdsv0003().getSqlcode().setSqlcode(sqlca.getSqlcode());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Y350-SAVEPOINT-TOT THRU Y350-EX
        //           ELSE
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
            // COB_CODE: SET INIT-COMMIT-FREQUENCY TO TRUE
            ws.getIabv0007().setInitCommitFrequency();
            // COB_CODE: PERFORM Y350-SAVEPOINT-TOT THRU Y350-EX
            y350SavepointTot();
        }
        else {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
            ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
            // COB_CODE: MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE
            ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE SQLCODE          TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(sqlca.getSqlcode());
            // COB_CODE: STRING 'ERRORE COMMIT WORK'
            //                   ' - '
            //                   'SQLCODE : '
            //                   WK-SQLCODE
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE COMMIT WORK", " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: Y050-CONTROLLO-COMMIT<br>
	 * <pre>****************************************************************</pre>*/
    private void y050ControlloCommit() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF IDSV0001-ARCH-BATCH-DBG AND
        //              CONT-TAB-GUIDE-LETTE > ZERO
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().isIdsv0001ArchBatchDbg() && Characters.GT_ZERO.test(ws.getIabv0007().getContTabGuideLetteFormatted())) {
            // COB_CODE: SET      IDSV8888-ARCH-BATCH-DBG TO TRUE
            ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888ArchBatchDbg();
            // COB_CODE: MOVE 'Architettura Batch'        TO IDSV8888-DESC-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Architettura Batch");
            // COB_CODE: STRING WK-PGM '/'
            //                  'Rec. Commit:'
            //                   CONT-TAB-GUIDE-LETTE
            //                   DELIMITED BY SIZE
            //                   INTO IDSV8888-NOME-PGM
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv8888StrPerformanceDbg.Len.NOME_PGM, ws.getWkPgmFormatted(), "/", "Rec. Commit:", ws.getIabv0007().getContTabGuideLetteAsString());
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(concatUtil.replaceInString(ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().getIdsv8888NomePgmFormatted()));
            // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: IF  IABV0006-STD-TYPE-REC-NO       OR
            //               IABV0006-STD-TYPE-REC-YES
            //               PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            //           END-IF
            if (ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecNo() || ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecYes()) {
                // COB_CODE: SET      IDSV8888-ARCH-BATCH-DBG TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888ArchBatchDbg();
                // COB_CODE: MOVE 'Architettura Batch'        TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Architettura Batch");
                // COB_CODE: STRING WK-PGM '/'
                //                  'Ult. Id Job:'
                //                   WK-ID-JOB-ULT-COMMIT
                //                   DELIMITED BY SIZE
                //                   INTO IDSV8888-NOME-PGM
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv8888StrPerformanceDbg.Len.NOME_PGM, ws.getWkPgmFormatted(), "/", "Ult. Id Job:", ws.getIabv0007().getWkIdJobUltCommitAsString());
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(concatUtil.replaceInString(ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().getIdsv8888NomePgmFormatted()));
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
            }
        }
    }

    /**Original name: Y100-ROLLBACK-SAVEPOINT<br>
	 * <pre>****************************************************************</pre>*/
    private void y100RollbackSavepoint() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'Y100-ROLLBACK-SAVEPOINT'   TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Y100-ROLLBACK-SAVEPOINT");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: EXEC SQL
        //                ROLLBACK WORK TO SAVEPOINT A
        //           END-EXEC.
        DbService.getCurrent().rollback(dbAccessStatus);
        // COB_CODE: MOVE SQLCODE       TO IDSV0003-SQLCODE
        ws.getIabv0007().getIdsv0003().getSqlcode().setSqlcode(sqlca.getSqlcode());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //           OR IDSV0003-OPER-NF-X-UNDONE
        //              CONTINUE
        //           ELSE
        //              SET WK-ERRORE-YES                 TO TRUE
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql() || ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003OperNfXUndone()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
            ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
            // COB_CODE: MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE
            ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE SQLCODE          TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(sqlca.getSqlcode());
            // COB_CODE: STRING 'ERRORE ROLLBACK WORK TO SAVEPOINT'
            //                   ' - '
            //                   'SQLCODE : '
            //                   WK-SQLCODE
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE ROLLBACK WORK TO SAVEPOINT", " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
            // COB_CODE: SET WK-ERRORE-NO                  TO TRUE
            ws.getIabv0007().setWkErrore(false);
            // COB_CODE: PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
            w000CaricaLogErrore();
            // COB_CODE: SET WK-ERRORE-YES                 TO TRUE
            ws.getIabv0007().setWkErrore(true);
        }
    }

    /**Original name: Y200-ROLLBACK<br>
	 * <pre>****************************************************************</pre>*/
    private void y200Rollback() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'Y200-ROLLBACK'             TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Y200-ROLLBACK");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: EXEC SQL
        //                ROLLBACK WORK TO SAVEPOINT TOT
        //           END-EXEC.
        DbService.getCurrent().rollback(dbAccessStatus);
        // COB_CODE: MOVE SQLCODE       TO IDSV0003-SQLCODE
        ws.getIabv0007().getIdsv0003().getSqlcode().setSqlcode(sqlca.getSqlcode());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //           OR IDSV0003-OPER-NF-X-UNDONE
        //              CONTINUE
        //           ELSE
        //              SET WK-ERRORE-YES                 TO TRUE
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql() || ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003OperNfXUndone()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
            ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
            // COB_CODE: MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE
            ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE SQLCODE          TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(sqlca.getSqlcode());
            // COB_CODE: STRING 'ERRORE ROLLBACK'
            //                   ' - '
            //                   'SQLCODE : '
            //                   WK-SQLCODE
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE ROLLBACK", " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
            // COB_CODE: SET WK-ERRORE-NO                  TO TRUE
            ws.getIabv0007().setWkErrore(false);
            // COB_CODE: PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
            w000CaricaLogErrore();
            // COB_CODE: SET WK-ERRORE-YES                 TO TRUE
            ws.getIabv0007().setWkErrore(true);
        }
    }

    /**Original name: Y300-SAVEPOINT<br>
	 * <pre>****************************************************************</pre>*/
    private void y300Savepoint() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'Y300-SAVEPOINT'            TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Y300-SAVEPOINT");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: EXEC SQL
        //                SAVEPOINT A ON ROLLBACK RETAIN CURSORS
        //           END-EXEC.
        //TODO: Implement: SavepointStmt;
        // COB_CODE: MOVE SQLCODE       TO IDSV0003-SQLCODE
        ws.getIabv0007().getIdsv0003().getSqlcode().setSqlcode(sqlca.getSqlcode());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF (IABV0006-STD-TYPE-REC-NO   OR
            //               IABV0006-STD-TYPE-REC-YES) AND
            //               IDSV0001-ARCH-BATCH-DBG
            //               MOVE WK-ID-JOB      TO WK-ID-JOB-ULT-COMMIT
            //           END-IF
            if ((ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecNo() || ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecYes()) && ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().isIdsv0001ArchBatchDbg()) {
                // COB_CODE: MOVE WK-ID-JOB      TO WK-ID-JOB-ULT-COMMIT
                ws.getIabv0007().setWkIdJobUltCommitFormatted(ws.getIabv0007().getWkIdJobFormatted());
            }
        }
        else {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
            ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
            // COB_CODE: MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE
            ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE SQLCODE          TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(sqlca.getSqlcode());
            // COB_CODE: STRING 'ERRORE SAVEPOINT'
            //                   ' - '
            //                   'SQLCODE : '
            //                   WK-SQLCODE
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE SAVEPOINT", " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: Y350-SAVEPOINT-TOT<br>
	 * <pre>****************************************************************</pre>*/
    private void y350SavepointTot() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'Y350-SAVEPOINT-TOT'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Y350-SAVEPOINT-TOT");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: EXEC SQL
        //                SAVEPOINT TOT ON ROLLBACK RETAIN CURSORS
        //           END-EXEC.
        //TODO: Implement: SavepointStmt;
        // COB_CODE: MOVE SQLCODE       TO IDSV0003-SQLCODE
        ws.getIabv0007().getIdsv0003().getSqlcode().setSqlcode(sqlca.getSqlcode());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              CONTINUE
        //           ELSE
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: MOVE WK-LABEL         TO WK-LOR-LABEL-ERR
            ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
            // COB_CODE: MOVE WK-PGM           TO WK-LOR-COD-SERVIZIO-BE
            ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE SQLCODE          TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(sqlca.getSqlcode());
            // COB_CODE: STRING 'ERRORE SAVEPOINT TOT'
            //                   ' - '
            //                   'SQLCODE : '
            //                   WK-SQLCODE
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE SAVEPOINT TOT", " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: Y400-ESITO-OK-OTHER-SERV<br>
	 * <pre>****************************************************************</pre>*/
    private void y400EsitoOkOtherServ() {
        // COB_CODE: MOVE 'Y400-ESITO-OK-OTHER-SERV'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Y400-ESITO-OK-OTHER-SERV");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF WK-SIMULAZIONE-INFR
        //              PERFORM Y200-ROLLBACK            THRU Y200-EX
        //           ELSE
        //              PERFORM Y000-COMMIT              THRU Y000-EX
        //           END-IF.
        if (ws.getIabv0007().getWkSimulazione().isInfr()) {
            // COB_CODE: PERFORM Y200-ROLLBACK            THRU Y200-EX
            y200Rollback();
        }
        else {
            // COB_CODE: PERFORM Y000-COMMIT              THRU Y000-EX
            y000Commit();
        }
    }

    /**Original name: Y500-ESITO-KO-OTHER-SERV<br>
	 * <pre>****************************************************************</pre>*/
    private void y500EsitoKoOtherServ() {
        // COB_CODE: MOVE 'Y500-ESITO-KO-OTHER-SERV'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Y500-ESITO-KO-OTHER-SERV");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET WK-ERRORE-NO                    TO TRUE
        ws.getIabv0007().setWkErrore(false);
        // COB_CODE: PERFORM Y200-ROLLBACK               THRU Y200-EX
        y200Rollback();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM W000-CARICA-LOG-ERRORE   THRU W000-EX
            w000CaricaLogErrore();
            // COB_CODE: IF WK-ERRORE-NO
            //              PERFORM Y000-COMMIT           THRU Y000-EX
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM Y000-COMMIT           THRU Y000-EX
                y000Commit();
            }
        }
    }

    /**Original name: Z000-OPERAZ-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *  OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void z000OperazFinali() {
        // COB_CODE: MOVE 'Z000-OPERAZ-FINALI'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Z000-OPERAZ-FINALI");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: PERFORM Z001-OPERAZIONI-FINALI THRU Z001-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0011:line=5926, because the code is unreachable.
        // COB_CODE: IF IJCCMQ04-MQ AND
        //              WSMQ-HCONN NOT = ZERO
        //              PERFORM A107-CHIUDI-CONNESSIONE THRU A107-EX
        //           END-IF.
        if (ws.getIabv0007().getIjccmq04VarAmbiente().isMq() && ws.getIabv0007().getwAreaPerChiamateWsmq().getHconn() != 0) {
            // COB_CODE: PERFORM A107-CHIUDI-CONNESSIONE THRU A107-EX
            a107ChiudiConnessione();
        }
        // COB_CODE: PERFORM Z100-CHIUDI-FILE       THRU Z100-EX.
        z100ChiudiFile();
        // COB_CODE: PERFORM Z200-STATISTICHE       THRU Z200-STATISTICHE-FINE.
        z200Statistiche();
        // COB_CODE: SET  IDSV8888-ARCH-BATCH-DBG    TO TRUE.
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888ArchBatchDbg();
        // COB_CODE: MOVE WK-PGM                     TO IDSV8888-NOME-PGM.
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getWkPgm());
        // COB_CODE: MOVE 'Architettura Batch'       TO IDSV8888-DESC-PGM.
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Architettura Batch");
        // COB_CODE: SET  IDSV8888-FINE              TO TRUE.
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: PERFORM Z999-SETTAGGIO-RC      THRU Z999-EX.
        z999SettaggioRc();
    }

    /**Original name: Z100-CHIUDI-FILE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CHIUDI FILE PARAMETRI
	 * ----------------------------------------------------------------*</pre>*/
    private void z100ChiudiFile() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'Z100-CHIUDI-FILE'          TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Z100-CHIUDI-FILE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF FILE-PARAM-APERTO
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().isWkOpenParam()) {
            // COB_CODE: CLOSE PBTCEXEC
            pbtcexecDAO.close();
            ws.getIabv0007().setFsPara(pbtcexecDAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: IF FS-PARA NOT = '00'
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            if (!Conditions.eq(ws.getIabv0007().getFsPara(), "00")) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'CLOSE ERRATA DEL FILE '
                //                   DELIMITED BY SIZE
                //                  'PBTCEXEC'
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   FS-PARA
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "CLOSE ERRATA DEL FILE ", "PBTCEXEC", " - ", "RC : ", ws.getIabv0007().getFsParaFormatted());
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
        // COB_CODE: IF FILE-REPORT01-APERTO
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().isWkOpenReport()) {
            // COB_CODE: CLOSE REPORT01
            report01DAO.close();
            ws.getIabv0007().setFsReport01(report01DAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: IF FS-REPORT01 NOT = '00'
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            if (!Conditions.eq(ws.getIabv0007().getFsReport01(), "00")) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'CLOSE ERRATA DEL FILE '
                //                   DELIMITED BY SIZE
                //                  'REPORT01'
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                   DELIMITED BY SIZE
                //                   'RC : '
                //                   DELIMITED BY SIZE
                //                   FS-REPORT01
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "CLOSE ERRATA DEL FILE ", "REPORT01", " - ", "RC : ", ws.getIabv0007().getFsReport01Formatted());
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: Z200-STATISTICHE<br>
	 * <pre>----------------------------------------------------------------*
	 *  STATISTICHE FINALI                                             *
	 * ----------------------------------------------------------------*</pre>*/
    private void z200Statistiche() {
        // COB_CODE: PERFORM T000-ACCEPT-TIMESTAMP     THRU T000-EX.
        t000AcceptTimestamp();
        // COB_CODE: PERFORM A102-DISPLAY-FINE-ELABORA THRU A102-EX.
        a102DisplayFineElabora();
    }

    /**Original name: Z300-SKEDA-PAR-ERRATA<br>
	 * <pre>----------------------------------------------------------------*
	 *  DISPLAY SKEDA PARAMETRI
	 * ----------------------------------------------------------------*</pre>*/
    private void z300SkedaParErrata() {
        // COB_CODE: SET WK-RC-12                      TO TRUE
        ws.getIabv0007().getFlagReturnCode().setRc12();
        // COB_CODE: SET WK-ERRORE-YES                 TO TRUE.
        ws.getIabv0007().setWkErrore(true);
        // COB_CODE: DISPLAY '***********************************************'.
        DisplayUtil.sysout.write("***********************************************");
        // COB_CODE: DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
        DisplayUtil.sysout.write("* NOME PROGRAMMA    = ", ws.getWkPgmFormatted());
        // COB_CODE: DISPLAY '* DESCRIZIONE       = Fine anomala di '
        DisplayUtil.sysout.write("* DESCRIZIONE       = Fine anomala di ");
        // COB_CODE: DISPLAY '* BATCH EXECUTOR '.
        DisplayUtil.sysout.write("* BATCH EXECUTOR ");
        // COB_CODE: DISPLAY '* SKEDA PARAMETRI ERRATA '.
        DisplayUtil.sysout.write("* SKEDA PARAMETRI ERRATA ");
        // COB_CODE: DISPLAY '* SU RIGA : ' CNT-NUM-OPERAZ-LETTE
        DisplayUtil.sysout.write("* SU RIGA : ", ws.getIabv0007().getCntNumOperazLetteAsString());
        // COB_CODE: DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
        DisplayUtil.sysout.write("* DATA ELABORAZIONE = ", ws.getIabv0007().getWkTimestamp26().getWkDataSistemaFormatted());
        // COB_CODE: DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
        DisplayUtil.sysout.write("* ORA  ELABORAZIONE = ", ws.getIabv0007().getWkTimestamp26().getWkOraSistemaFormatted());
        // COB_CODE: DISPLAY '************************************************'.
        DisplayUtil.sysout.write("************************************************");
        // COB_CODE: DISPLAY ALL SPACES.
        DisplayUtil.sysout.write(String.valueOf(Types.SPACE_CHAR));
    }

    /**Original name: Z400-ERRORE-PRE-DB<br>
	 * <pre>----------------------------------------------------------------*
	 *  DISPLAY ERRORE PRE CONNESIONE DB
	 * ----------------------------------------------------------------*</pre>*/
    private void z400ErrorePreDb() {
        // COB_CODE: SET WK-RC-12                      TO TRUE
        ws.getIabv0007().getFlagReturnCode().setRc12();
        // COB_CODE: SET WK-ERRORE-YES                 TO TRUE.
        ws.getIabv0007().setWkErrore(true);
        // COB_CODE: DISPLAY '***********************************************'.
        DisplayUtil.sysout.write("***********************************************");
        // COB_CODE: DISPLAY '* NOME PROGRAMMA    = ' WK-PGM.
        DisplayUtil.sysout.write("* NOME PROGRAMMA    = ", ws.getWkPgmFormatted());
        // COB_CODE: DISPLAY '* DESCRIZIONE       = Fine anomala di '
        DisplayUtil.sysout.write("* DESCRIZIONE       = Fine anomala di ");
        // COB_CODE: DISPLAY '* BATCH EXECUTOR '.
        DisplayUtil.sysout.write("* BATCH EXECUTOR ");
        // COB_CODE: DISPLAY WK-LOR-DESC-ERRORE-ESTESA.
        DisplayUtil.sysout.write(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted());
        // COB_CODE: DISPLAY '* DATA ELABORAZIONE = ' WK-DATA-SISTEMA.
        DisplayUtil.sysout.write("* DATA ELABORAZIONE = ", ws.getIabv0007().getWkTimestamp26().getWkDataSistemaFormatted());
        // COB_CODE: DISPLAY '* ORA  ELABORAZIONE = ' WK-ORA-SISTEMA.
        DisplayUtil.sysout.write("* ORA  ELABORAZIONE = ", ws.getIabv0007().getWkTimestamp26().getWkOraSistemaFormatted());
        // COB_CODE: DISPLAY '************************************************'.
        DisplayUtil.sysout.write("************************************************");
        // COB_CODE: DISPLAY ALL SPACES.
        DisplayUtil.sysout.write(String.valueOf(Types.SPACE_CHAR));
    }

    /**Original name: Z500-GESTIONE-ERR-MAIN<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE DELL'ERRORE CHE SI VERIFICA NEL PGM MAIN
	 * ----------------------------------------------------------------*</pre>*/
    private void z500GestioneErrMain() {
        // COB_CODE: SET WK-ERRORE-NO TO TRUE.
        ws.getIabv0007().setWkErrore(false);
        // COB_CODE: SET WK-BAT-COLLECTION-KEY-KO  TO TRUE.
        ws.getIabv0007().setWkBatCollectionKey(false);
        // COB_CODE: IF GESTIONE-PARALLELISMO-YES
        //              END-IF
        //           END-IF
        if (ws.getIabv0007().getFlagGestioneParallelismo().isYes()) {
            // COB_CODE: PERFORM P100-PARALLELISMO-FINALE THRU P100-EX
            p100ParallelismoFinale();
            // COB_CODE: IF WK-ERRORE-NO
            //              PERFORM P900-CNTL-FINALE      THRU P900-EX
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM P900-CNTL-FINALE      THRU P900-EX
                p900CntlFinale();
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF BATCH-COMPLETE
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().getWsBufferWhereCondIabs0130().getFlagBatchComplete().isBatchComplete()) {
                // COB_CODE: PERFORM I200-INIT-DA-ESEGUIRE
                //              THRU I200-EX
                i200InitDaEseguire();
                // COB_CODE: PERFORM A150-SET-CURRENT-TIMESTAMP THRU A150-EX
                a150SetCurrentTimestamp();
                // COB_CODE: MOVE WS-TIMESTAMP-N        TO   BTC-DT-END
                ws.getBtcBatch().getBtcDtEnd().setBtcDtEnd(ws.getIabv0007().getWsTimestampN());
                // COB_CODE: IF WK-SIMULAZIONE-INFR
                //              PERFORM Y200-ROLLBACK   THRU Y200-EX
                //           END-IF
                if (ws.getIabv0007().getWkSimulazione().isInfr()) {
                    // COB_CODE: PERFORM Y200-ROLLBACK   THRU Y200-EX
                    y200Rollback();
                }
                // COB_CODE: PERFORM D400-UPD-BATCH     THRU D400-EX
                d400UpdBatch();
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF BTC-ID-RICH-NULL NOT = HIGH-VALUE AND
                    //              BTC-ID-RICH      NOT = ZEROES
                    //              END-IF
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getBtcBatch().getBtcIdRich().getBtcIdRichNullFormatted()) && ws.getBtcBatch().getBtcIdRich().getBtcIdRich() != 0) {
                        // COB_CODE: SET IDSV0003-WHERE-CONDITION-02 TO TRUE
                        ws.getIabv0007().getIdsv0003().getTipologiaOperazione().setIdsv0003WhereCondition02();
                        // COB_CODE: PERFORM D951-TRASF-BATCH-TO-PRENOTAZ
                        //                                          THRU D951-EX
                        d951TrasfBatchToPrenotaz();
                        // COB_CODE: IF WK-AGGIORNA-PRENOTAZ-YES
                        //              END-IF
                        //           END-IF
                        if (ws.getIabv0007().isFlagAggiornaPrenotaz()) {
                            // COB_CODE: SET IDSV0003-NONE-ACTION       TO TRUE
                            ws.getIabv0007().getIdsv0003().getLivelloOperazione().setIdsv0003NoneAction();
                            // COB_CODE: PERFORM D950-AGGIORNA-PRENOTAZIONE
                            //                                       THRU D950-EX
                            d950AggiornaPrenotazione();
                            // COB_CODE: IF WK-ERRORE-NO
                            //                                       THRU D952-EX
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: PERFORM D952-TRASF-PRENOTAZ-TO-BATCH
                                //                                    THRU D952-EX
                                d952TrasfPrenotazToBatch();
                            }
                        }
                    }
                }
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              PERFORM Y000-COMMIT        THRU Y000-EX
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM Y000-COMMIT        THRU Y000-EX
                y000Commit();
            }
        }
    }

    /**Original name: Z999-SETTAGGIO-RC<br>
	 * <pre>----------------------------------------------------------------*
	 *  SETTAGGIO RETURN CODE
	 * ----------------------------------------------------------------*</pre>*/
    private void z999SettaggioRc() {
        // COB_CODE: MOVE FLAG-RETURN-CODE          TO RETURN-CODE.
        Session.setReturnCode(ws.getIabv0007().getFlagReturnCode().getFlagReturnCode());
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 * ----------------------------------------------------------------*</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIabv0007().setWsDateX(Functions.setSubstring(ws.getIabv0007().getWsDateX(), ws.getIabv0007().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIabv0007().setWsDateX(Functions.setSubstring(ws.getIabv0007().getWsDateX(), ws.getIabv0007().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIabv0007().setWsDateX(Functions.setSubstring(ws.getIabv0007().getWsDateX(), ws.getIabv0007().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIabv0007().setWsDateX(Functions.setSubstring(ws.getIabv0007().getWsDateX(), "-", 5, 1));
        ws.getIabv0007().setWsDateX(Functions.setSubstring(ws.getIabv0007().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z701-TS-N-TO-X<br>*/
    private void z701TsNToX() {
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(1:4)
        //                TO WS-TIMESTAMP-X(1:4)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ws.getIabv0007().getWsStrTimestampNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(5:2)
        //                TO WS-TIMESTAMP-X(6:2)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ws.getIabv0007().getWsStrTimestampNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(7:2)
        //                TO WS-TIMESTAMP-X(9:2)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ws.getIabv0007().getWsStrTimestampNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(9:2)
        //                TO WS-TIMESTAMP-X(12:2)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ws.getIabv0007().getWsStrTimestampNFormatted().substring((9) - 1, 10), 12, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(11:2)
        //                TO WS-TIMESTAMP-X(15:2)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ws.getIabv0007().getWsStrTimestampNFormatted().substring((11) - 1, 12), 15, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(13:2)
        //                TO WS-TIMESTAMP-X(18:2)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ws.getIabv0007().getWsStrTimestampNFormatted().substring((13) - 1, 14), 18, 2));
        // COB_CODE: MOVE WS-STR-TIMESTAMP-N(15:4)
        //                TO WS-TIMESTAMP-X(21:4)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ws.getIabv0007().getWsStrTimestampNFormatted().substring((15) - 1, 18), 21, 4));
        // COB_CODE: MOVE '00'
        //                TO WS-TIMESTAMP-X(25:2)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), "00", 25, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-TIMESTAMP-X(5:1)
        //                   WS-TIMESTAMP-X(8:1)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), "-", 5, 1));
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), "-", 8, 1));
        // COB_CODE: MOVE SPACE
        //                TO WS-TIMESTAMP-X(11:1)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), LiteralGenerator.create(Types.SPACE_CHAR, 1), 11, 1));
        // COB_CODE: MOVE ':'
        //                TO WS-TIMESTAMP-X(14:1)
        //                   WS-TIMESTAMP-X(17:1)
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ":", 14, 1));
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ":", 17, 1));
        // COB_CODE: MOVE '.'
        //                TO WS-TIMESTAMP-X(20:1).
        ws.getIabv0007().setWsTimestampX(Functions.setSubstring(ws.getIabv0007().getWsTimestampX(), ".", 20, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIabv0007().setWsStrDateNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrDateNFormatted(), ws.getIabv0007().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIabv0007().setWsStrDateNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrDateNFormatted(), ws.getIabv0007().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIabv0007().setWsStrDateNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrDateNFormatted(), ws.getIabv0007().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIabv0007().setWsStrDateNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrDateNFormatted(), ws.getIabv0007().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIabv0007().setWsStrDateNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrDateNFormatted(), ws.getIabv0007().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIabv0007().setWsStrDateNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrDateNFormatted(), ws.getIabv0007().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    /**Original name: Z801-TS-X-TO-N<br>*/
    private void z801TsXToN() {
        // COB_CODE: MOVE WS-TIMESTAMP-X(1:4)
        //                TO WS-STR-TIMESTAMP-N(1:4)
        ws.getIabv0007().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrTimestampNFormatted(), ws.getIabv0007().getWsTimestampXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-TIMESTAMP-X(6:2)
        //                TO WS-STR-TIMESTAMP-N(5:2)
        ws.getIabv0007().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrTimestampNFormatted(), ws.getIabv0007().getWsTimestampXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(9:2)
        //                TO WS-STR-TIMESTAMP-N(7:2)
        ws.getIabv0007().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrTimestampNFormatted(), ws.getIabv0007().getWsTimestampXFormatted().substring((9) - 1, 10), 7, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(12:2)
        //                TO WS-STR-TIMESTAMP-N(9:2)
        ws.getIabv0007().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrTimestampNFormatted(), ws.getIabv0007().getWsTimestampXFormatted().substring((12) - 1, 13), 9, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(15:2)
        //                TO WS-STR-TIMESTAMP-N(11:2)
        ws.getIabv0007().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrTimestampNFormatted(), ws.getIabv0007().getWsTimestampXFormatted().substring((15) - 1, 16), 11, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(18:2)
        //                TO WS-STR-TIMESTAMP-N(13:2)
        ws.getIabv0007().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrTimestampNFormatted(), ws.getIabv0007().getWsTimestampXFormatted().substring((18) - 1, 19), 13, 2));
        // COB_CODE: MOVE WS-TIMESTAMP-X(21:4)
        //                TO WS-STR-TIMESTAMP-N(15:4).
        ws.getIabv0007().setWsStrTimestampNFormatted(Functions.setSubstring(ws.getIabv0007().getWsStrTimestampNFormatted(), ws.getIabv0007().getWsTimestampXFormatted().substring((21) - 1, 24), 15, 4));
    }

    /**Original name: CNTL-FORMALE-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONTROLLO DATA FORMALE
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *  INIZIO STATEMENTS DA UTILIZZARE PRETTAMENTE CON IDSV0016
	 * *****************************************************************</pre>*/
    private void cntlFormaleData() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'CNTL-CAMPI-DATA'           TO WK-LABEL.
        ws.getIabv0007().setWkLabel("CNTL-CAMPI-DATA");
        // COB_CODE: IF IDSV0016-AAAA NOT NUMERIC OR
        //              IDSV0016-AAAA = 0
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           ELSE
        //                      THRU CNTL-ANNO-BISESTILE-EX
        //           END-IF
        if (!Functions.isNumber(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getAaaaFormatted()) || ws.getIabv0007().getIdsv0016().getIdsv0016Data().getAaaa() == 0) {
            // COB_CODE: MOVE HIGH-VALUES  TO WK-LOG-ERRORE
            ws.getIabv0007().getWkLogErrore().initWkLogErroreHighValues();
            // COB_CODE: MOVE 4            TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(4);
            // COB_CODE: MOVE WK-LABEL     TO WK-LOR-LABEL-ERR
            ws.getIabv0007().getWkLogErrore().setLabelErr(ws.getIabv0007().getWkLabel());
            // COB_CODE: MOVE WK-PGM       TO WK-LOR-COD-SERVIZIO-BE
            ws.getIabv0007().getWkLogErrore().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'ANNO NON VALIDO'
            //                   DELIMITED BY SIZE INTO
            //                   IDSV0016-DESC-ERRORE
            //           END-STRING
            ws.getIabv0007().getIdsv0016().setIdsv0016DescErrore("ANNO NON VALIDO");
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
        else {
            // COB_CODE: PERFORM CNTL-ANNO-BISESTILE
            //                   THRU CNTL-ANNO-BISESTILE-EX
            cntlAnnoBisestile();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IDSV0016-MM NOT NUMERIC OR
            //              IDSV0016-MM < 01           OR
            //              IDSV0016-MM > 12
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           ELSE
            //              END-IF
            //           END-IF
            if (!Functions.isNumber(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getMm()) || ws.getIabv0007().getIdsv0016().getIdsv0016Data().getMm() < 1 || ws.getIabv0007().getIdsv0016().getIdsv0016Data().getMm() > 12) {
                // COB_CODE: STRING 'MESE NON VALIDO'
                //                   DELIMITED BY SIZE INTO
                //                   IDSV0016-DESC-ERRORE
                //           END-STRING
                ws.getIabv0007().getIdsv0016().setIdsv0016DescErrore("MESE NON VALIDO");
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
            else if (ws.getIabv0007().getIdsv0016().getIdsv0016TipoAnno().isBisestile()) {
                // COB_CODE: IF IDSV0016-ANNO-BISESTILE
                //                               (IDSV0016-CONST-FEBBRAIO)
                //           END-IF
                // COB_CODE: ADD 1          TO IDSV0016-STR-GG-ANNUAL
                //                            (IDSV0016-CONST-FEBBRAIO)
                ws.getIabv0007().getIdsv0016().getIdsv0016StrGiorniAnnual().setGgAnnual(ws.getIabv0007().getIdsv0016().getIdsv0016ConstFebbraio(), Trunc.toShort(1 + ws.getIabv0007().getIdsv0016().getIdsv0016StrGiorniAnnual().getGgAnnual(ws.getIabv0007().getIdsv0016().getIdsv0016ConstFebbraio()), 2));
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: IF IDSV0016-GG NOT NUMERIC OR
                //              IDSV0016-GG < 0         OR
                //              IDSV0016-GG > IDSV0016-STR-GG-ANNUAL(IDSV0016-MM)
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-IF
                if (!Functions.isNumber(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getGgFormatted()) || ws.getIabv0007().getIdsv0016().getIdsv0016Data().getGg() < 0 || ws.getIabv0007().getIdsv0016().getIdsv0016Data().getGg() > ws.getIabv0007().getIdsv0016().getIdsv0016StrGiorniAnnual().getGgAnnual(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getMm())) {
                    // COB_CODE: STRING 'GIORNO NON VALIDO'
                    //                   DELIMITED BY SIZE INTO
                    //                   IDSV0016-DESC-ERRORE
                    //           END-STRING
                    ws.getIabv0007().getIdsv0016().setIdsv0016DescErrore("GIORNO NON VALIDO");
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
            }
        }
    }

    /**Original name: CNTL-ANNO-BISESTILE<br>
	 * <pre>*****************************************************************
	 * *****************************************************************</pre>*/
    private void cntlAnnoBisestile() {
        // COB_CODE: MOVE 'CNTL-ANNO-BISESTILE'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("CNTL-ANNO-BISESTILE");
        // COB_CODE: SET IDSV0016-ANNO-STANDARD           TO TRUE
        ws.getIabv0007().getIdsv0016().getIdsv0016TipoAnno().setStandard();
        // COB_CODE: DIVIDE IDSV0016-AAAA        BY 4
        //                                       GIVING IDSV0016-RISULTATO
        //                                       REMAINDER IDSV0016-RESTO
        ws.getIabv0007().getIdsv0016().setIdsv0016Risultato(((short)(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getAaaa() / 4)));
        ws.getIabv0007().getIdsv0016().setIdsv0016Resto(((short)(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getAaaa() % 4)));
        // COB_CODE: IF IDSV0016-RESTO = 0
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getIdsv0016().getIdsv0016Resto() == 0) {
            // COB_CODE: DIVIDE IDSV0016-AAAA     BY 100
            //                                    GIVING IDSV0016-RISULTATO
            //                                    REMAINDER IDSV0016-RESTO
            ws.getIabv0007().getIdsv0016().setIdsv0016Risultato(((short)(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getAaaa() / 100)));
            ws.getIabv0007().getIdsv0016().setIdsv0016Resto(((short)(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getAaaa() % 100)));
            // COB_CODE: IF IDSV0016-RESTO NOT = 0
            //              SET IDSV0016-ANNO-BISESTILE      TO TRUE
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().getIdsv0016().getIdsv0016Resto() != 0) {
                // COB_CODE: SET IDSV0016-ANNO-BISESTILE      TO TRUE
                ws.getIabv0007().getIdsv0016().getIdsv0016TipoAnno().setBisestile();
            }
            else {
                // COB_CODE: DIVIDE IDSV0016-AAAA  BY 400
                //                                 GIVING IDSV0016-RISULTATO
                //                                 REMAINDER IDSV0016-RESTO
                ws.getIabv0007().getIdsv0016().setIdsv0016Risultato(((short)(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getAaaa() / 400)));
                ws.getIabv0007().getIdsv0016().setIdsv0016Resto(((short)(ws.getIabv0007().getIdsv0016().getIdsv0016Data().getAaaa() % 400)));
                // COB_CODE: IF IDSV0016-RESTO = 0
                //              SET IDSV0016-ANNO-BISESTILE   TO TRUE
                //           END-IF
                if (ws.getIabv0007().getIdsv0016().getIdsv0016Resto() == 0) {
                    // COB_CODE: SET IDSV0016-ANNO-BISESTILE   TO TRUE
                    ws.getIabv0007().getIdsv0016().getIdsv0016TipoAnno().setBisestile();
                }
            }
        }
    }

    /**Original name: ESTRAI-CURRENT-TIMESTAMP<br>
	 * <pre>****************************************************************
	 *  N.B. - UTILIZZABILE SOLO INCLUDENDO LE COPY :
	 *         IDSV0014
	 *         IDSP0014
	 * ****************************************************************
	 * ----------------------------------------------------------------*
	 *  SETTA TIMESTAMP
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *   FINE STATEMENTS DA UTILIZZARE PRETTAMENTE CON IDSV0016
	 * *****************************************************************</pre>*/
    private void estraiCurrentTimestamp() {
        // COB_CODE: EXEC SQL
        //                 VALUES CURRENT TIMESTAMP
        //                   INTO :WS-TIMESTAMP-X
        //           END-EXEC
        ws.getIabv0007().setWsTimestampX(SqlFunctions.currentTimestamp());
        // COB_CODE: IF SQLCODE = 0
        //              PERFORM Z801-TS-X-TO-N   THRU Z801-EX
        //           END-IF.
        if (sqlca.getSqlcode() == 0) {
            // COB_CODE: PERFORM Z801-TS-X-TO-N   THRU Z801-EX
            z801TsXToN();
        }
    }

    /**Original name: ESTRAI-CURRENT-DATE<br>
	 * <pre>----------------------------------------------------------------*
	 *  SETTA TIMESTAMP
	 * ----------------------------------------------------------------*</pre>*/
    private void estraiCurrentDate() {
        // COB_CODE: EXEC SQL
        //                 VALUES CURRENT DATE
        //                   INTO :WS-DATE-X
        //           END-EXEC
        ws.getIabv0007().setWsDateX(SqlFunctions.currentDate());
        // COB_CODE: IF SQLCODE = 0
        //              PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        //           END-IF.
        if (sqlca.getSqlcode() == 0) {
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
        }
    }

    /**Original name: CALL-IABS0030<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL IABS0030
	 * ----------------------------------------------------------------*</pre>*/
    private void callIabs0030() {
        ConcatUtil concatUtil = null;
        Iabs0030 iabs0030 = null;
        // COB_CODE:   CALL PGM-IABS0030 USING IDSV0003
        //                                     IABV0002
        //                                     BTC-ELAB-STATE
        //             ON EXCEPTION
        //                PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL.
        try {
            iabs0030 = Iabs0030.getInstance();
            iabs0030.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getBtcElabState());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                 DELIMITED BY SIZE
            //                 PGM-IABS0030
            //                 DELIMITED BY SIZE
            //                 ' - '
            //                  DELIMITED BY SIZE INTO
            //                 WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getIabv0004().getPgmIabs0030Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: CALL-IABS0040<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL IABS0040
	 * ----------------------------------------------------------------*</pre>*/
    private void callIabs0040() {
        ConcatUtil concatUtil = null;
        Iabs0040 iabs0040 = null;
        // COB_CODE: CALL PGM-IABS0040   USING IDSV0003
        //                                   IABV0002
        //                                   BTC-BATCH
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL.
        try {
            iabs0040 = Iabs0040.getInstance();
            iabs0040.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getBtcBatch());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                   DELIMITED BY SIZE
            //                   PGM-IABS0040
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getIabv0004().getPgmIabs0040Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: CALL-IABS0050<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL IABS0050
	 * ----------------------------------------------------------------*</pre>*/
    private void callIabs0050() {
        ConcatUtil concatUtil = null;
        Iabs0050 iabs0050 = null;
        // COB_CODE: CALL PGM-IABS0050 USING IDSV0003
        //                                   BTC-BATCH-TYPE
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL.
        try {
            iabs0050 = Iabs0050.getInstance();
            iabs0050.run(ws.getIabv0007().getIdsv0003(), ws.getBtcBatchType());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                   DELIMITED BY SIZE
            //                   PGM-IABS0050
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getIabv0004().getPgmIabs0050Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: CALL-IABS0060<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL IABS0060
	 * ----------------------------------------------------------------*</pre>*/
    private void callIabs0060() {
        ConcatUtil concatUtil = null;
        Iabs0060 iabs0060 = null;
        // COB_CODE: CALL PGM-IABS0060              USING IDSV0003
        //                                                IABV0002
        //                                                BTC-JOB-SCHEDULE
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL.
        try {
            iabs0060 = Iabs0060.getInstance();
            iabs0060.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getBtcJobSchedule());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                DELIMITED BY SIZE
            //                PGM-IABS0060
            //                DELIMITED BY SIZE
            //                ' - '
            //                 DELIMITED BY SIZE INTO
            //                WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getIabv0004().getPgmIabs0060Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: CALL-IABS0070<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL IABS0070
	 * ----------------------------------------------------------------*</pre>*/
    private void callIabs0070() {
        ConcatUtil concatUtil = null;
        Iabs0070 iabs0070 = null;
        // COB_CODE: CALL PGM-IABS0070 USING      IDSV0003
        //                                        IABV0002
        //                                        BTC-BATCH-STATE
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL.
        try {
            iabs0070 = Iabs0070.getInstance();
            iabs0070.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getBtcBatchState());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                   DELIMITED BY SIZE
            //                   PGM-IABS0070
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getIabv0004().getPgmIabs0070Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: CALL-IDSS0300<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL IDSS0300
	 * ----------------------------------------------------------------*</pre>*/
    private void callIdss0300() {
        ConcatUtil concatUtil = null;
        Idss0300 idss0300 = null;
        // COB_CODE: CALL PGM-IDSS0300 USING      IDSV0301
        //           ON EXCEPTION
        //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
        //           END-CALL.
        try {
            idss0300 = Idss0300.getInstance();
            idss0300.run(ws.getIabv0007().getIdsv0301());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //                   DELIMITED BY SIZE
            //                   PGM-IDSS0300
            //                   DELIMITED BY SIZE
            //                   ' - '
            //                    DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getIabv0004().getPgmIdss0300Formatted(), " - ");
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            z400ErrorePreDb();
        }
    }

    /**Original name: ESEGUI-DISPLAY<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI GESTIONE DISPLAY
	 * ----------------------------------------------------------------*</pre>*/
    private void eseguiDisplay() {
        Idss8880 idss8880 = null;
        GenericParam idsv8888DisplayAddress = null;
        // COB_CODE: IF IDSV0001-ANY-TUNING-DBG AND
        //              IDSV8888-ANY-TUNING-DBG
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().isAnyTuningDbg() && ws.getIabv0007().getIdsv8888().getLivelloDebug().isAnyTuningDbg()) {
            // COB_CODE: IF IDSV0001-TOT-TUNING-DBG
            //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().isTotTuningDbg()) {
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIabv0007().getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIabv0007().getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIabv0007().getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else if (ws.getIabv0007().getIdsv8888().getLivelloDebug().isArchBatchDbg()) {
                    // COB_CODE: IF IDSV8888-ARCH-BATCH-DBG
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           ELSE
                    //              MOVE 'ERRO'   TO IDSV8888-FASE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                else {
                    // COB_CODE: MOVE 'ERRO'   TO IDSV8888-FASE
                    ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFase("ERRO");
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIabv0007().getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //                TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIabv0007().getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIabv0007().getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIabv0007().getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIabv0007().getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initIdsv8888StrPerformanceDbg();
            }
            else if (ws.getIabv0007().getIdsv8888().getLivelloDebug().getLivelloDebug() == ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: IF IDSV8888-LIVELLO-DEBUG =
                //              IDSV0001-LIVELLO-DEBUG
                //              INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                //           END-IF
                // COB_CODE: IF IDSV8888-STRESS-TEST-DBG
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIabv0007().getIdsv8888().getLivelloDebug().isStressTestDbg()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIabv0007().getIdsv8888().getLivelloDebug().isComCobJavDbg()) {
                    // COB_CODE: IF IDSV8888-COM-COB-JAV-DBG
                    //              SET IDSV8888-COM-COB-JAV   TO TRUE
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    // COB_CODE: SET IDSV8888-COM-COB-JAV   TO TRUE
                    ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setComCobJav();
                }
                else if (ws.getIabv0007().getIdsv8888().getLivelloDebug().isBusinessDbg()) {
                    // COB_CODE: IF IDSV8888-BUSINESS-DBG
                    //              SET IDSV8888-BUSINESS   TO TRUE
                    //           ELSE
                    //              SET IDSV8888-ARCH-BATCH TO TRUE
                    //           END-IF
                    // COB_CODE: SET IDSV8888-BUSINESS   TO TRUE
                    ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setBusiness();
                }
                else {
                    // COB_CODE: SET IDSV8888-ARCH-BATCH TO TRUE
                    ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setArchBatch();
                }
                // COB_CODE: CALL IDSV8888-CALL-TIMESTAMP
                //                USING IDSV8888-TIMESTAMP
                DynamicCall.invoke(ws.getIabv0007().getIdsv8888().getCallTimestamp(), new BasicBytesClass(ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().getArray(), Idsv8888StrPerformanceDbg.Pos.TIMESTAMP_FLD - 1));
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-STR-PERFORMANCE-DBG
                ws.getIabv0007().getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIabv0007().getIdsv8888().getStrPerformanceDbg()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIabv0007().getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIabv0007().getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: INITIALIZE IDSV8888-STR-PERFORMANCE-DBG
                initIdsv8888StrPerformanceDbg();
            }
        }
        else if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().isAnyApplDbg() && ws.getIabv0007().getIdsv8888().getLivelloDebug().isAnyApplDbg()) {
            // COB_CODE: IF IDSV0001-ANY-APPL-DBG AND
            //              IDSV8888-ANY-APPL-DBG
            //               END-IF
            //           END-IF
            // COB_CODE: IF IDSV8888-LIVELLO-DEBUG <=
            //              IDSV0001-LIVELLO-DEBUG
            //              MOVE SPACES TO IDSV8888-AREA-DISPLAY
            //           END-IF
            if (ws.getIabv0007().getIdsv8888().getLivelloDebug().getLivelloDebug() <= ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().getIdsi0011LivelloDebug()) {
                // COB_CODE: SET IDSV8888-DISPLAY-ADDRESS
                //               TO ADDRESS OF IDSV8888-AREA-DISPLAY
                ws.getIabv0007().getIdsv8888().setDisplayAddress(pointerManager.addressOf(ws.getIabv0007().getIdsv8888().getAreaDisplay()));
                // COB_CODE: CALL IDSV8888-CALL-DISPLAY
                //                USING IDSV8888-DISPLAY-ADDRESS
                idss8880 = Idss8880.getInstance();
                idsv8888DisplayAddress = new GenericParam(MarshalByteExt.binIntToBuffer(ws.getIabv0007().getIdsv8888().getDisplayAddress()));
                idss8880.run(idsv8888DisplayAddress);
                ws.getIabv0007().getIdsv8888().setDisplayAddressFromBuffer(idsv8888DisplayAddress.getByteData());
                // COB_CODE: MOVE SPACES TO IDSV8888-AREA-DISPLAY
                ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay("");
            }
        }
        // COB_CODE: SET IDSV8888-NO-DEBUG              TO TRUE.
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setNoDebug();
    }

    /**Original name: B500-ELABORA-MICRO<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTIENE STATEMENTS PER LA FASE MICRO DEL BATCH EXECUTOR
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *  ELABORAZIONE                                                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void b500ElaboraMicro() {
        // COB_CODE: MOVE 'B500-ELABORA-MICRO'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B500-ELABORA-MICRO");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF IABI0011-RIP-CON-MONITORING
        //              PERFORM M600-UPD-STARTING            THRU M600-EX
        //           END-IF
        if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConMonitoring()) {
            // COB_CODE: PERFORM M600-UPD-STARTING            THRU M600-EX
            m600UpdStarting();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM N500-ESTRAI-JOB              THRU N500-EX
            n500EstraiJob();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM E601-VERIFICA-LETTURA     THRU E601-EX
                e601VerificaLettura();
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF IABI0011-RIP-CON-MONITORING
                    //              PERFORM M601-UPD-RIESEGUIRE THRU M601-EX
                    //           END-IF
                    if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConMonitoring()) {
                        // COB_CODE: PERFORM M601-UPD-RIESEGUIRE THRU M601-EX
                        m601UpdRieseguire();
                    }
                }
            }
        }
    }

    /**Original name: B700-UPD-ESEGUITO-OK<br>
	 * <pre>----------------------------------------------------------------*
	 *  UPDATE JOB ESEGUITO OK
	 * ----------------------------------------------------------------*</pre>*/
    private void b700UpdEseguitoOk() {
        // COB_CODE: MOVE 'B700-UPD-ESEGUITO-OK'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B700-UPD-ESEGUITO-OK");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET IDSV0003-PRIMARY-KEY           TO TRUE
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
        // COB_CODE: SET WK-NOT-FOUND-NOT-ALLOWED       TO TRUE
        ws.getIabv0007().setFlagFoundNotFound(false);
        // COB_CODE: IF IABI0011-RIP-CON-VERSIONAMENTO AND
        //              (IABV0006-GUIDE-AD-HOC          OR
        //               IABV0006-BY-BOOKING)
        //              MOVE JOB-DA-ESEGUIRE            TO IABV0002-STATE-CURRENT
        //           ELSE
        //              MOVE JOB-ESEGUITO-OK            TO IABV0002-STATE-CURRENT
        //           END-IF
        if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConVersionamento() && (ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() || ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking())) {
            // COB_CODE: IF IABI0011-RIP-CON-VERSIONAMENTO
            //                                           THRU V000-EX
            //           END-IF
            if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConVersionamento()) {
                // COB_CODE: PERFORM V000-VALORIZZAZIONE-VERSIONE
                //                                        THRU V000-EX
                v000ValorizzazioneVersione();
            }
            // COB_CODE: MOVE JOB-DA-ESEGUIRE            TO IABV0002-STATE-CURRENT
            ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getJobDaEseguire());
        }
        else {
            // COB_CODE: MOVE JOB-ESEGUITO-OK            TO IABV0002-STATE-CURRENT
            ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getJobEseguitoOk());
        }
        // COB_CODE: PERFORM A150-SET-CURRENT-TIMESTAMP THRU A150-EX
        a150SetCurrentTimestamp();
        // COB_CODE: MOVE WS-TIMESTAMP-N                TO   BJS-DT-END
        ws.getBtcJobSchedule().getBjsDtEnd().setBjsDtEnd(ws.getIabv0007().getWsTimestampN());
        // COB_CODE: PERFORM U000-UPD-JOB               THRU U000-EX
        u000UpdJob();
        // COB_CODE: IF WK-ERRORE-NO
        //              PERFORM E550-UPD-JOB-EXECUTION  THRU E550-EX
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM E550-UPD-JOB-EXECUTION  THRU E550-EX
            e550UpdJobExecution();
        }
    }

    /**Original name: B800-UPD-ESEGUITO-KO<br>
	 * <pre>----------------------------------------------------------------*
	 *  UPDATE JOB ESEGUITO KO
	 * ----------------------------------------------------------------*</pre>*/
    private void b800UpdEseguitoKo() {
        // COB_CODE: MOVE 'B800-UPD-ESEGUITO-KO'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B800-UPD-ESEGUITO-KO");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET IDSV0003-PRIMARY-KEY            TO TRUE
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
        // COB_CODE: SET WK-NOT-FOUND-NOT-ALLOWED        TO TRUE
        ws.getIabv0007().setFlagFoundNotFound(false);
        // COB_CODE: MOVE JOB-ESEGUITO-KO                TO IABV0002-STATE-CURRENT
        ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getJobEseguitoKo());
        // COB_CODE: PERFORM A150-SET-CURRENT-TIMESTAMP  THRU A150-EX
        a150SetCurrentTimestamp();
        // COB_CODE: MOVE WS-TIMESTAMP-N                 TO BJS-DT-END
        ws.getBtcJobSchedule().getBjsDtEnd().setBjsDtEnd(ws.getIabv0007().getWsTimestampN());
        // COB_CODE: IF IABI0011-RIP-CON-VERSIONAMENTO
        //              MOVE ZEROES                      TO IABV0009-VERSIONING
        //           END-IF
        if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConVersionamento()) {
            // COB_CODE: MOVE ZEROES                      TO IABV0009-VERSIONING
            ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setVersioning(0);
        }
        // COB_CODE: PERFORM U000-UPD-JOB                THRU U000-EX
        u000UpdJob();
        // COB_CODE: IF WK-ERRORE-NO
        //              PERFORM E550-UPD-JOB-EXECUTION   THRU E550-EX
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM E550-UPD-JOB-EXECUTION   THRU E550-EX
            e550UpdJobExecution();
        }
    }

    /**Original name: B900-CTRL-STATI-SOSP<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void b900CtrlStatiSosp() {
        // COB_CODE: MOVE 'B900-CTRL-STATI-SOSP'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("B900-CTRL-STATI-SOSP");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF NOT SEQUENTIAL-GUIDE
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().getWkFlagGuideType().isSequentialGuide()) {
            // COB_CODE: PERFORM VARYING IND-STATI-SOSP FROM 1 BY 1
            //               UNTIL IND-STATI-SOSP > WK-LIMITE-STATI-SOSP     OR
            //                     WK-STATI-SOSP-PK(IND-STATI-SOSP) = ZEROES OR
            //                     NOT WK-ERRORE-NO
            //               END-IF
            //           END-PERFORM
            ws.getIabv0007().setIndStatiSosp(((short)1));
            while (!(ws.getIabv0007().getIndStatiSosp() > ws.getIabv0007().getWkLimiteStatiSosp() || ws.getIabv0007().getWkStatiSospesiEle(ws.getIabv0007().getIndStatiSosp()).getWkStatiSospPk() == 0 || !!ws.getIabv0007().isWkErrore())) {
                // COB_CODE: IF IABV0006-GUIDE-AD-HOC OR
                //              IABV0006-BY-BOOKING
                //              PERFORM L802-CARICA-STATI-SOSPESI     THRU L802-EX
                //           ELSE
                //              MOVE WK-STATI-SOSP-PK(IND-STATI-SOSP) TO BJS-ID-JOB
                //           END-IF
                if (ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() || ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking()) {
                // COB_CODE: PERFORM L802-CARICA-STATI-SOSPESI     THRU L802-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=126, because the code is unreachable.
                }
                else {
                    // COB_CODE: MOVE WK-STATI-SOSP-PK(IND-STATI-SOSP) TO BJS-ID-JOB
                    ws.getBtcJobSchedule().setBjsIdJob(((int)(ws.getIabv0007().getWkStatiSospesiEle(ws.getIabv0007().getIndStatiSosp()).getWkStatiSospPk())));
                }
                // COB_CODE: IF NOT WK-SIMULAZIONE-INFR       AND
                //              NOT WK-SIMULAZIONE-APPL       AND
                //              NOT IABI0011-SENZA-RIPARTENZA
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().getWkSimulazione().isInfr() && !ws.getIabv0007().getWkSimulazione().isAppl() && !ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isSenzaRipartenza()) {
                    // COB_CODE: IF WK-ALLINEA-STATI-OK
                    //              PERFORM B700-UPD-ESEGUITO-OK     THRU B700-EX
                    //           ELSE
                    //              PERFORM B800-UPD-ESEGUITO-KO     THRU B800-EX
                    //           END-IF
                    if (ws.getIabv0007().getFlagAllineaStati().isOk()) {
                        // COB_CODE: PERFORM B700-UPD-ESEGUITO-OK     THRU B700-EX
                        b700UpdEseguitoOk();
                    }
                    else {
                        // COB_CODE: PERFORM B800-UPD-ESEGUITO-KO     THRU B800-EX
                        b800UpdEseguitoKo();
                    }
                }
                ws.getIabv0007().setIndStatiSosp(Trunc.toShort(ws.getIabv0007().getIndStatiSosp() + 1, 2));
            }
            // COB_CODE: IF WK-ERRORE-NO
            //                               IND-STATI-SOSP
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: MOVE 1             TO IND-STATI-SOSP
                ws.getIabv0007().setIndStatiSosp(((short)1));
                // COB_CODE: MOVE WK-CURRENT-PK TO WK-STATI-SOSP-PK(IND-STATI-SOSP)
                ws.getIabv0007().getWkStatiSospesiEle(ws.getIabv0007().getIndStatiSosp()).setWkStatiSospPk(ws.getIabv0007().getWkCurrentPk());
                // COB_CODE: PERFORM L802-CARICA-STATI-SOSPESI THRU L802-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=149, because the code is unreachable.
                // COB_CODE: INITIALIZE WK-STATI-SOSPESI-STR
                //                            IND-STATI-SOSP
                initWkStatiSospesiStr();
                ws.getIabv0007().setIndStatiSosp(((short)0));
            }
        }
    }

    /**Original name: M600-UPD-STARTING<br>
	 * <pre>----------------------------------------------------------------*
	 *  UPDATE JOB IN "STARTING"
	 * ----------------------------------------------------------------*</pre>*/
    private void m600UpdStarting() {
        // COB_CODE: MOVE 'M600-UPD-STARTING'         TO WK-LABEL.
        ws.getIabv0007().setWkLabel("M600-UPD-STARTING");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: MOVE JOB-STARTING                TO IABV0002-STATE-CURRENT
        ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getJobStarting());
        // COB_CODE: PERFORM M650-UPD-CON-MONITORING  THRU M650-EX.
        m650UpdConMonitoring();
    }

    /**Original name: M601-UPD-RIESEGUIRE<br>
	 * <pre>----------------------------------------------------------------*
	 *  UPDATE JOB IN "DA RIESEGUIRE" DEI JOB IN "STARTING"
	 * ----------------------------------------------------------------*</pre>*/
    private void m601UpdRieseguire() {
        // COB_CODE: MOVE 'M601-UPD-RIESEGUIRE'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("M601-UPD-RIESEGUIRE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: INITIALIZE IABV0002-STATE
        initIabv0002State();
        // COB_CODE: SET WK-UPDATE-FOR-RESTORE-YES    TO TRUE
        ws.getIabv0007().setFlagUpdateForRestore(true);
        // COB_CODE: MOVE JOB-DA-RIESEGUIRE           TO IABV0002-STATE-CURRENT
        ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getJobDaRieseguire());
        // COB_CODE: MOVE JOB-STARTING                TO IABV0002-STATE-01.
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState01(ws.getIabv0007().getIabv0004().getJobStarting());
        // COB_CODE: PERFORM M650-UPD-CON-MONITORING  THRU M650-EX.
        m650UpdConMonitoring();
    }

    /**Original name: M650-UPD-CON-MONITORING<br>
	 * <pre>----------------------------------------------------------------*
	 *  UPDATE DELLO STATE CON GESTIONE "MONITORING"
	 * ----------------------------------------------------------------*</pre>*/
    private void m650UpdConMonitoring() {
        // COB_CODE: MOVE 'M650-UPD-CON-MONITORING'   TO WK-LABEL.
        ws.getIabv0007().setWkLabel("M650-UPD-CON-MONITORING");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET IDSV0003-WHERE-CONDITION     TO TRUE
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET WK-NOT-FOUND-ALLOWED         TO TRUE
        ws.getIabv0007().setFlagFoundNotFound(true);
        // COB_CODE: PERFORM U000-UPD-JOB             THRU U000-EX.
        u000UpdJob();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF NOT WK-SIMULAZIONE-INFR
            //              PERFORM Y000-COMMIT        THRU Y000-EX
            //           END-IF
            if (!ws.getIabv0007().getWkSimulazione().isInfr()) {
                // COB_CODE: PERFORM Y000-COMMIT        THRU Y000-EX
                y000Commit();
            }
        }
    }

    /**Original name: N500-ESTRAI-JOB<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAI JOB
	 * ----------------------------------------------------------------*</pre>*/
    private void n500EstraiJob() {
        ConcatUtil concatUtil = null;
        Iabs0060 iabs0060 = null;
        // COB_CODE: MOVE 'N500-ESTRAI-JOB'           TO WK-LABEL.
        ws.getIabv0007().setWkLabel("N500-ESTRAI-JOB");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST           TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setFetchFirst();
        // COB_CODE: SET WK-ESTRAI-JOB-YES              TO TRUE.
        ws.getIabv0007().setFlagEstraiJob(true);
        // COB_CODE: SET WK-SKIP-BATCH-NO               TO TRUE.
        ws.getIabv0007().setFlagSkipBatch(false);
        // COB_CODE: MOVE IABI0011-TIPO-ORDER-BY        TO IABV0002-TIPO-ORDER-BY.
        ws.getIabv0007().getIabv0002().getIabv0002TipoOrderBy().setIabv0002TipoOrderBy(ws.getIabv0007().getIabi0011Area().getTipoOrderBy().getTipoOrderBy());
        // COB_CODE: IF IABI0011-RIP-CON-MONITORING
        //              MOVE JOB-STARTING               TO IABV0002-STATE-01
        //           END-IF
        if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConMonitoring()) {
            // COB_CODE: INITIALIZE IABV0002-STATE
            initIabv0002State();
            // COB_CODE: MOVE JOB-STARTING               TO IABV0002-STATE-01
            ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState01(ws.getIabv0007().getIabv0004().getJobStarting());
        }
        // COB_CODE:      PERFORM UNTIL WK-ESTRAI-JOB-NO  OR
        //                              WK-ERRORE-YES     OR
        //                              WK-SKIP-BATCH-YES
        //           *--> INIZIALIZZA CODICE DI RITORNO
        //                    END-IF
        //                END-PERFORM.
        while (!(!ws.getIabv0007().isFlagEstraiJob() || ws.getIabv0007().isWkErrore() || ws.getIabv0007().isFlagSkipBatch())) {
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
            ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
            ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
            // COB_CODE: IF IABV0006-GUIDE-AD-HOC OR
            //              IABV0006-BY-BOOKING
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() || ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking()) {
                // COB_CODE: IF IABI0011-RIP-CON-VERSIONAMENTO
                //                                THRU V000-EX
                //           END-IF
                if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConVersionamento()) {
                    // COB_CODE: PERFORM V000-VALORIZZAZIONE-VERSIONE
                    //                             THRU V000-EX
                    v000ValorizzazioneVersione();
                }
                // COB_CODE: PERFORM N501-PRE-GUIDE-AD-HOC  THRU N501-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=262, because the code is unreachable.
                // COB_CODE: IF WK-ERRORE-NO
                //              PERFORM N509-SWITCH-GUIDE   THRU N509-EX
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: PERFORM N509-SWITCH-GUIDE   THRU N509-EX
                    n509SwitchGuide();
                }
            }
            else {
                // COB_CODE: MOVE BTC-ID-BATCH           TO BJS-ID-BATCH
                ws.getBtcJobSchedule().setBjsIdBatch(ws.getBtcBatch().getBtcIdBatch());
                // COB_CODE: IF IABI0011-MACROFUNZIONALITA NOT =
                //              SPACES AND LOW-VALUE AND HIGH-VALUE
                //              SET VERIFICA-FINALE-JOBS-SI TO TRUE
                //           END-IF
                if (!Characters.EQ_SPACE.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalita()) && !Characters.EQ_LOW.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalitaFormatted()) && !Characters.EQ_HIGH.test(ws.getIabv0007().getIabi0011Area().getMacrofunzionalitaFormatted())) {
                    // COB_CODE: MOVE IABI0011-MACROFUNZIONALITA
                    //                                     TO BJS-COD-MACROFUNCT
                    ws.getBtcJobSchedule().setBjsCodMacrofunct(ws.getIabv0007().getIabi0011Area().getMacrofunzionalita());
                    // COB_CODE: SET VERIFICA-FINALE-JOBS-SI TO TRUE
                    ws.getIabv0007().getFlagVerificaFinaleJobs().setSi();
                }
                // COB_CODE: IF IABI0011-TIPO-MOVIMENTO IS NUMERIC AND
                //              IABI0011-TIPO-MOVIMENTO NOT = ZEROES
                //              SET VERIFICA-FINALE-JOBS-SI   TO TRUE
                //           END-IF
                if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted()) && !Characters.EQ_ZERO.test(ws.getIabv0007().getIabi0011Area().getTipoMovimentoFormatted())) {
                    // COB_CODE: MOVE IABI0011-TIPO-MOVIMENTO TO BJS-TP-MOVI
                    ws.getBtcJobSchedule().getBjsTpMovi().setBjsTpMovi(ws.getIabv0007().getIabi0011Area().getTipoMovimento());
                    // COB_CODE: SET VERIFICA-FINALE-JOBS-SI   TO TRUE
                    ws.getIabv0007().getFlagVerificaFinaleJobs().setSi();
                }
                // COB_CODE: MOVE PGM-IABS0060           TO WK-PGM-ERRORE
                ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getIabv0004().getPgmIabs0060());
                // COB_CODE: CALL PGM-IABS0060           USING IDSV0003
                //                                             IABV0002
                //                                             BTC-JOB-SCHEDULE
                //           ON EXCEPTION
                //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                //           END-CALL
                try {
                    iabs0060 = Iabs0060.getInstance();
                    iabs0060.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getBtcJobSchedule());
                }
                catch (ProgramExecutionException __ex) {
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
                    //                   DELIMITED BY SIZE
                    //                   PGM-IABS0060
                    //                   DELIMITED BY SIZE
                    //                   ' - '
                    //                    DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO : ", ws.getIabv0007().getIabv0004().getPgmIabs0060Formatted(), " - ");
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                    z400ErrorePreDb();
                }
                // COB_CODE: IF WK-ERRORE-NO
                //              MOVE BJS-COD-MACROFUNCT TO IABV0003-COD-MACROFUNCT
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF BJS-TP-MOVI-NULL NOT = HIGH-VALUE AND
                    //              BJS-TP-MOVI      NOT = ZEROES
                    //              MOVE BJS-TP-MOVI    TO IABV0003-TP-MOVI-BUSINESS
                    //           ELSE
                    //                                  TO IABV0003-TP-MOVI-BUSINESS
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getBtcJobSchedule().getBjsTpMovi().getBjsTpMoviNullFormatted()) && ws.getBtcJobSchedule().getBjsTpMovi().getBjsTpMovi() != 0) {
                        // COB_CODE: MOVE BJS-TP-MOVI    TO IABV0003-TP-MOVI-BUSINESS
                        ws.getIabv0007().getIabv0003().setIabv0003TpMoviBusiness(ws.getBtcJobSchedule().getBjsTpMovi().getBjsTpMovi());
                    }
                    else {
                        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
                        //                               TO IABV0003-TP-MOVI-BUSINESS
                        ws.getIabv0007().getIabv0003().setIabv0003TpMoviBusiness(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getIdsv0001TipoMovimento());
                    }
                    // COB_CODE: MOVE BJS-IB-OGG         TO IABV0003-IB-OGG
                    ws.getIabv0007().getIabv0003().setIabv0003IbOgg(ws.getBtcJobSchedule().getBjsIbOgg());
                    // COB_CODE: MOVE BJS-COD-MACROFUNCT TO IABV0003-COD-MACROFUNCT
                    ws.getIabv0007().getIabv0003().setIabv0003CodMacrofunct(ws.getBtcJobSchedule().getBjsCodMacrofunct());
                }
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              PERFORM N503-CNTL-GUIDE        THRU N503-EX
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM N503-CNTL-GUIDE        THRU N503-EX
                n503CntlGuide();
            }
        }
    }

    /**Original name: N503-CNTL-GUIDE<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLA GUIDE
	 * ----------------------------------------------------------------*</pre>*/
    private void n503CntlGuide() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'N503-CNTL-GUIDE'           TO WK-LABEL.
        ws.getIabv0007().setWkLabel("N503-CNTL-GUIDE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIRE ERRORE DISPATCHER
        //                  PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //                END-IF.
        if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE:         EVALUATE TRUE
            //                   WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
            //                      END-IF
            //                   WHEN IDSV0003-NOT-FOUND
            //           *--->   CAMPO $ NON TROVATO
            //                      END-IF
            //                   WHEN IDSV0003-NEGATIVI
            //           *--->   ERRORE DI ACCESSO AL DB
            //                      PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //                   END-EVALUATE
            if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                //-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: ADD 1                    TO CONT-TAB-GUIDE-LETTE
                ws.getIabv0007().setContTabGuideLette(Trunc.toInt(1 + ws.getIabv0007().getContTabGuideLette(), 9));
                // COB_CODE: PERFORM Z000-DISP-OCCORR-GUIDE THRU Z000-EX
                z000DispOccorrGuide();
                // COB_CODE: PERFORM E601-VERIFICA-LETTURA    THRU E601-EX
                e601VerificaLettura();
                // COB_CODE: IF WK-ERRORE-NO
                //              SET IDSV0003-FETCH-NEXT       TO TRUE
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: PERFORM E600-ELABORA-BUSINESS THRU E600-EX
                    e600ElaboraBusiness();
                    // COB_CODE: SET IDSV0003-FETCH-NEXT       TO TRUE
                    ws.getIabv0007().getIdsv0003().getOperazione().setFetchNext();
                }
            }
            else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                //--->   CAMPO $ NON TROVATO
                // COB_CODE: IF IDSV0003-FETCH-FIRST
                //                                                 THRU W070-EX
                //           ELSE
                //              PERFORM Z000-DISP-OCCORR-GUIDE THRU Z000-EX
                //           END-IF
                if (ws.getIabv0007().getIdsv0003().getOperazione().isFetchFirst()) {
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE 'OCCORRENZE GUIDA NON TROVATE'
                    //                            TO WK-LOR-DESC-ERRORE-ESTESA
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa("OCCORRENZE GUIDA NON TROVATE");
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                    //                                              THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
                else {
                    // COB_CODE: SET WK-FINE-ALIMENTAZIONE-YES   TO TRUE
                    ws.getIabv0007().setFlagFineAlimentazione(true);
                    // COB_CODE: SET WK-ESTRAI-JOB-NO            TO TRUE
                    ws.getIabv0007().setFlagEstraiJob(false);
                    // COB_CODE: PERFORM Z000-DISP-OCCORR-GUIDE THRU Z000-EX
                    z000DispOccorrGuide();
                }
            }
            else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                //--->   ERRORE DI ACCESSO AL DB
                // COB_CODE: SET WK-ESTRAI-JOB-NO          TO TRUE
                ws.getIabv0007().setFlagEstraiJob(false);
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                // COB_CODE: STRING WK-PGM-ERRORE
                //                  ' - '
                //                  'SQLCODE : '
                //                  WK-SQLCODE
                //                  DELIMITED BY SIZE INTO
                //                  WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, ws.getIabv0007().getWkPgmErroreFormatted(), " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
        else {
            //-->    GESTIRE ERRORE DISPATCHER
            // COB_CODE: SET WK-ESTRAI-JOB-NO TO TRUE
            ws.getIabv0007().setFlagEstraiJob(false);
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: STRING 'ERRORE MODULO '
            //                   WK-PGM-ERRORE
            //                   ' - '
            //                   'RC : '
            //                   IDSV0003-RETURN-CODE
            //                   DELIMITED BY SIZE INTO
            //                   WK-LOR-DESC-ERRORE-ESTESA
            //           END-STRING
            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getWkPgmErroreFormatted(), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted());
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
            // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: N509-SWITCH-GUIDE<br>
	 * <pre>----------------------------------------------------------------*
	 *  SWITCH GUIDE
	 * ----------------------------------------------------------------*</pre>*/
    private void n509SwitchGuide() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'N509-SWITCH-GUIDE'         TO WK-LABEL.
        ws.getIabv0007().setWkLabel("N509-SWITCH-GUIDE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: EVALUATE TRUE
        //              WHEN SEQUENTIAL-GUIDE
        //                 PERFORM CALL-SEQGUIDE THRU CALL-SEQGUIDE-EX
        //              WHEN DB-GUIDE
        //                 MOVE WK-PGM-GUIDA              TO WK-PGM-ERRORE
        //              WHEN OTHER
        //                 PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
        //           END-EVALUATE.
        switch (ws.getIabv0007().getWkFlagGuideType().getWkFlagGuideType()) {

            case WkFlagGuideType.SEQUENTIAL_GUIDE:// COB_CODE: PERFORM CALL-SEQGUIDE THRU CALL-SEQGUIDE-EX
                callSeqguide();
                break;

            case WkFlagGuideType.DB_GUIDE:// COB_CODE: PERFORM N502-CALL-GUIDE-AD-HOC THRU N502-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=430, because the code is unreachable.
                // COB_CODE: MOVE WK-PGM-GUIDA              TO WK-PGM-ERRORE
                ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getWkPgmGuida());
                break;

            default:// COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'TIPO GUIDA NON VALIDO '
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa("TIPO GUIDA NON VALIDO ");
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                w070CaricaLogErrBatExec();
                break;
        }
    }

    /**Original name: Q503-CNTL-SERV-ROTTURA<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL ATTIVATORE
	 * ----------------------------------------------------------------*</pre>*/
    private void q503CntlServRottura() {
        // COB_CODE: MOVE 'Q503-CNTL-SERV-ROTTURA'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Q503-CNTL-SERV-ROTTURA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              PERFORM Y400-ESITO-OK-OTHER-SERV  THRU Y400-EX
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getAreaIdsv0001().getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM Y400-ESITO-OK-OTHER-SERV  THRU Y400-EX
            y400EsitoOkOtherServ();
        }
        else {
            // COB_CODE: PERFORM Y500-ESITO-KO-OTHER-SERV  THRU Y500-EX
            y500EsitoKoOtherServ();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: SET WK-PGM-ARCHITECTURE          TO TRUE
                ws.getIabv0007().getFlagTipoPgm().setArchitecture();
                // COB_CODE: IF WK-PGM-SERV-SECON-ROTT NOT =
                //              SPACES AND LOW-VALUE AND HIGH-VALUE
                //              PERFORM L450-TRATTA-SERV-SECOND-ROTT THRU L450-EX
                //           END-IF
                if (!Characters.EQ_SPACE.test(ws.getWkPgmServSeconRott()) && !Characters.EQ_LOW.test(ws.getWkPgmServSeconRottFormatted()) && !Characters.EQ_HIGH.test(ws.getWkPgmServSeconRottFormatted())) {
                    // COB_CODE: PERFORM L450-TRATTA-SERV-SECOND-ROTT THRU L450-EX
                    l450TrattaServSecondRott();
                }
            }
        }
    }

    /**Original name: E600-ELABORA-BUSINESS<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORA SERVIZIO BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void e600ElaboraBusiness() {
        // COB_CODE: MOVE 'E600-ELABORA-BUSINESS'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E600-ELABORA-BUSINESS");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF IABI0011-RIP-CON-MONITORING
        //              PERFORM E150-UPD-IN-ESECUZIONE      THRU E150-EX
        //           END-IF
        if (ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isRipConMonitoring()) {
            // COB_CODE: PERFORM E150-UPD-IN-ESECUZIONE      THRU E150-EX
            e150UpdInEsecuzione();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM E500-INS-JOB-EXECUTION   THRU E500-EX
            e500InsJobExecution();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM R000-ESTRAI-DATI-ELABORAZIONE  THRU R000-EX
                r000EstraiDatiElaborazione();
                // COB_CODE: IF WK-ERRORE-NO
                //              PERFORM L000-LANCIA-BUSINESS        THRU L000-EX
                //           ELSE
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: PERFORM L000-LANCIA-BUSINESS        THRU L000-EX
                    l000LanciaBusiness();
                }
                else {
                    // COB_CODE: SET WK-BAT-COLLECTION-KEY-KO        TO TRUE
                    ws.getIabv0007().setWkBatCollectionKey(false);
                    // COB_CODE: SET WK-JOB-COLLECTION-KEY-KO        TO TRUE
                    ws.getIabv0007().setWkJobCollectionKey(false);
                    // COB_CODE: IF NOT WK-SIMULAZIONE-INFR       AND
                    //              NOT WK-SIMULAZIONE-APPL       AND
                    //              NOT IABI0011-SENZA-RIPARTENZA AND
                    //              NOT SEQUENTIAL-GUIDE
                    //              PERFORM B800-UPD-ESEGUITO-KO     THRU B800-EX
                    //           END-IF
                    if (!ws.getIabv0007().getWkSimulazione().isInfr() && !ws.getIabv0007().getWkSimulazione().isAppl() && !ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isSenzaRipartenza() && !ws.getIabv0007().getWkFlagGuideType().isSequentialGuide()) {
                        // COB_CODE: PERFORM B800-UPD-ESEGUITO-KO     THRU B800-EX
                        b800UpdEseguitoKo();
                    }
                }
            }
        }
    }

    /**Original name: E601-VERIFICA-LETTURA<br>
	 * <pre>----------------------------------------------------------------*
	 *  VERIFICA GUIDE
	 * ----------------------------------------------------------------*</pre>*/
    private void e601VerificaLettura() {
        // COB_CODE: MOVE 'E601-VERIFICA-LETTURA'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E601-VERIFICA-LETTURA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF WK-SKIP-BATCH-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isFlagSkipBatch()) {
            // COB_CODE: IF WK-FINE-ALIMENTAZIONE-NO
            //              END-IF
            //           ELSE
            //               END-IF
            //           END-IF
            if (!ws.getIabv0007().isFlagFineAlimentazione()) {
                // COB_CODE: IF IABV0006-GUIDE-AD-HOC OR
                //              IABV0006-BY-BOOKING
                //              PERFORM N504-POST-GUIDE-AD-HOC          THRU N504-EX
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() || ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking()) {
                // COB_CODE: PERFORM N504-POST-GUIDE-AD-HOC          THRU N504-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=534, because the code is unreachable.
                }
                else if (!Characters.EQ_SPACE.test(ws.getWkPgmServRottura()) && !Characters.EQ_LOW.test(ws.getWkPgmServRotturaFormatted()) && !Characters.EQ_HIGH.test(ws.getWkPgmServRotturaFormatted())) {
                // COB_CODE: IF WK-PGM-SERV-ROTTURA NOT =
                //              SPACES AND LOW-VALUE AND HIGH-VALUE
                //              PERFORM E602-CNTL-ROTTURA-EST        THRU E602-EX
                //           END-IF
                // COB_CODE: PERFORM E602-CNTL-ROTTURA-EST        THRU E602-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=538, because the code is unreachable.
                }
            }
            else if (!Characters.EQ_SPACE.test(ws.getWkPgmServRottura()) && !Characters.EQ_LOW.test(ws.getWkPgmServRotturaFormatted()) && !Characters.EQ_HIGH.test(ws.getWkPgmServRotturaFormatted())) {
                // COB_CODE: IF WK-PGM-SERV-ROTTURA NOT =
                //              SPACES AND LOW-VALUE AND HIGH-VALUE
                //              PERFORM Q001-ELABORA-ROTTURA           THRU Q001-EX
                //           ELSE
                //              END-IF
                //           END-IF
                // COB_CODE: PERFORM Q001-ELABORA-ROTTURA           THRU Q001-EX
                q001ElaboraRottura();
            }
            else if (ws.getIabv0007().getFlagGestioneLanciBus().isSuspend()) {
                // COB_CODE: IF GESTIONE-SUSPEND
                //              END-IF
                //           ELSE
                //              END-IF
                //           END-IF
                // COB_CODE: PERFORM Y300-SAVEPOINT              THRU Y300-EX
                y300Savepoint();
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF BBT-DEF-CONTENT-TYPE(4:) =
                    //              ULTIMO-LANCIO-X-CLOSE-FILE
                    //              END-IF
                    //           ELSE
                    //              PERFORM L000-LANCIA-BUSINESS  THRU L000-EX
                    //           END-IF
                    if (Conditions.eq(ws.getBtcBatchType().getBbtDefContentTypeFormatted().substring((4) - 1), ws.getIabv0007().getUltimoLancioXCloseFile())) {
                        // COB_CODE: PERFORM L000-LANCIA-BUSINESS  THRU L000-EX
                        l000LanciaBusiness();
                        // COB_CODE: IF WK-ERRORE-NO
                        //              END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: PERFORM Y300-SAVEPOINT     THRU Y300-EX
                            y300Savepoint();
                            // COB_CODE: IF WK-ERRORE-NO
                            //                                      THRU L602-EX
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: SET IABV0006-ULTIMO-LANCIO TO TRUE
                                ws.getIabv0006().getTipoLancioBus().setIabv0006UltimoLancio();
                                // COB_CODE: PERFORM L602-CALL-BUS-EOF
                                //                                   THRU L602-EX
                                l602CallBusEof();
                            }
                        }
                    }
                    else {
                        // COB_CODE: SET IABV0006-ULTIMO-LANCIO    TO TRUE
                        ws.getIabv0006().getTipoLancioBus().setIabv0006UltimoLancio();
                        // COB_CODE: PERFORM L000-LANCIA-BUSINESS  THRU L000-EX
                        l000LanciaBusiness();
                    }
                }
            }
            else {
                // COB_CODE: SET IABV0006-ULTIMO-LANCIO          TO TRUE
                ws.getIabv0006().getTipoLancioBus().setIabv0006UltimoLancio();
                // COB_CODE: IF BBT-DEF-CONTENT-TYPE(4:) =
                //              ULTIMO-LANCIO-X-CLOSE-FILE
                //              PERFORM L602-CALL-BUS-EOF        THRU L602-EX
                //           END-IF
                if (Conditions.eq(ws.getBtcBatchType().getBbtDefContentTypeFormatted().substring((4) - 1), ws.getIabv0007().getUltimoLancioXCloseFile())) {
                    // COB_CODE: PERFORM L602-CALL-BUS-EOF        THRU L602-EX
                    l602CallBusEof();
                }
            }
        }
    }

    /**Original name: E620-ESTRAI-RECORD<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAI RECORD
	 * ----------------------------------------------------------------*</pre>*/
    private void e620EstraiRecord() {
        ConcatUtil concatUtil = null;
        Iabs0110 iabs0110 = null;
        // COB_CODE: MOVE 'E620-ESTRAI-RECORD'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E620-ESTRAI-RECORD");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setFetchFirst();
        // COB_CODE: SET WK-ESTRAI-REC-YES          TO TRUE.
        ws.getIabv0007().setFlagEstraiRec(true);
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: INITIALIZE IABV0003-DATA-JOB.
        initIabv0003DataJob();
        // COB_CODE: MOVE 0                         TO IABV0003-ELE-MAX
        ws.getIabv0007().getIabv0003().setIabv0003EleMax(((short)0));
        // COB_CODE: IF IABI0011-APPEND-DATA-GATES-YES
        //              PERFORM E625-CONFIG-APPEND-GATES THRU E625-EX
        //           END-IF
        if (ws.getIabv0007().getIabi0011Area().getAppendDataGates().isYes()) {
            // COB_CODE: PERFORM E625-CONFIG-APPEND-GATES THRU E625-EX
            e625ConfigAppendGates();
        }
        // COB_CODE: PERFORM UNTIL  WK-ESTRAI-REC-NO OR
        //                          NOT WK-ERRORE-NO
        //              END-IF
        //           END-PERFORM.
        while (!(!ws.getIabv0007().isFlagEstraiRec() || !!ws.getIabv0007().isWkErrore())) {
            // COB_CODE: MOVE BJS-ID-BATCH           TO BRS-ID-BATCH
            ws.getBtcRecSchedule().setIdBatch(ws.getBtcJobSchedule().getBjsIdBatch());
            // COB_CODE: MOVE BJS-ID-JOB             TO BRS-ID-JOB
            //                                          WK-ID-JOB
            ws.getBtcRecSchedule().setIdJob(ws.getBtcJobSchedule().getBjsIdJob());
            ws.getIabv0007().setWkIdJob(TruncAbs.toInt(ws.getBtcJobSchedule().getBjsIdJob(), 9));
            // COB_CODE: MOVE PGM-IABS0110           TO WK-PGM-ERRORE
            ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getIabv0004().getPgmIabs0110());
            // COB_CODE: CALL PGM-IABS0110           USING IDSV0003
            //                                             IABV0002
            //                                             BTC-REC-SCHEDULE
            //           ON EXCEPTION
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-CALL
            try {
                iabs0110 = Iabs0110.getInstance();
                iabs0110.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getBtcRecSchedule());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0110
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                    DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0110Formatted(), " - ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE:            IF IDSV0003-SUCCESSFUL-RC
                //                        END-EVALUATE
                //                      ELSE
                //           *--> GESTIRE ERRORE DISPATCHER
                //                                             THRU W070-EX
                //                      END-IF
                if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:               EVALUATE TRUE
                    //                         WHEN IDSV0003-SUCCESSFUL-SQL
                    //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                            SET IDSV0003-FETCH-NEXT     TO TRUE
                    //                         WHEN IDSV0003-NOT-FOUND
                    //           *--> CAMPO $ NON TROVATO
                    //                            END-IF
                    //                           WHEN IDSV0003-NEGATIVI
                    //           *--> ERRORE DI ACCESSO AL DB
                    //                              END-IF
                    //                        END-EVALUATE
                    if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                        //--> OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: PERFORM E630-VALOR-DA-GATES THRU E630-EX
                        e630ValorDaGates();
                        // COB_CODE: SET IDSV0003-FETCH-NEXT     TO TRUE
                        ws.getIabv0007().getIdsv0003().getOperazione().setFetchNext();
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                        //--> CAMPO $ NON TROVATO
                        // COB_CODE: SET WK-ESTRAI-REC-NO            TO TRUE
                        ws.getIabv0007().setFlagEstraiRec(false);
                        // COB_CODE: IF IDSV0003-FETCH-FIRST
                        //              END-IF
                        //           END-IF
                        if (ws.getIabv0007().getIdsv0003().getOperazione().isFetchFirst()) {
                            // COB_CODE: SET WK-BAT-COLLECTION-KEY-KO TO TRUE
                            ws.getIabv0007().setWkBatCollectionKey(false);
                            // COB_CODE: SET WK-JOB-COLLECTION-KEY-KO TO TRUE
                            ws.getIabv0007().setWkJobCollectionKey(false);
                            // COB_CODE: INITIALIZE WK-LOG-ERRORE
                            initWkLogErrore();
                            // COB_CODE: STRING
                            //               'ID-BATCH : '
                            //               WK-ID-BATCH ' - '
                            //               'ID-JOB : '
                            //               WK-ID-JOB ' - '
                            //               'REC_SCHEDULE - OCCORR. NON TROVATE'
                            //               DELIMITED BY SIZE INTO
                            //               WK-LOR-DESC-ERRORE-ESTESA
                            //           END-STRING
                            concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ID-BATCH : ", ws.getIabv0007().getWkIdBatchAsString(), " - ", "ID-JOB : ", ws.getIabv0007().getWkIdJobAsString(), " - ", "REC_SCHEDULE - OCCORR. NON TROVATE"});
                            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                            // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                            //                          THRU W070-EX
                            w070CaricaLogErrBatExec();
                            // COB_CODE: IF WK-ERRORE-NO
                            //              PERFORM E800-INS-EXE-MESSAGE THRU E800-EX
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: PERFORM E800-INS-EXE-MESSAGE THRU E800-EX
                                e800InsExeMessage();
                            }
                        }
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                        //--> ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET WK-ESTRAI-REC-NO           TO TRUE
                        ws.getIabv0007().setFlagEstraiRec(false);
                        // COB_CODE: SET WK-BAT-COLLECTION-KEY-KO   TO TRUE
                        ws.getIabv0007().setWkBatCollectionKey(false);
                        // COB_CODE: SET WK-JOB-COLLECTION-KEY-KO   TO TRUE
                        ws.getIabv0007().setWkJobCollectionKey(false);
                        // COB_CODE: INITIALIZE WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                        ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                        // COB_CODE: STRING 'ERRORE MODULO '
                        //                  WK-PGM-ERRORE
                        //                  ' SQLCODE : '
                        //                  WK-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getWkPgmErroreFormatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: MOVE ERRORE-FATALE  TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                        // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                        //                             THRU W070-EX
                        w070CaricaLogErrBatExec();
                        // COB_CODE: IF WK-ERRORE-NO
                        //              PERFORM E800-INS-EXE-MESSAGE   THRU E800-EX
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: PERFORM E800-INS-EXE-MESSAGE   THRU E800-EX
                            e800InsExeMessage();
                        }
                    }
                }
                else {
                    //--> GESTIRE ERRORE DISPATCHER
                    // COB_CODE: SET WK-ESTRAI-REC-NO           TO TRUE
                    ws.getIabv0007().setFlagEstraiRec(false);
                    // COB_CODE: SET WK-BAT-COLLECTION-KEY-KO   TO TRUE
                    ws.getIabv0007().setWkBatCollectionKey(false);
                    // COB_CODE: SET WK-JOB-COLLECTION-KEY-KO   TO TRUE
                    ws.getIabv0007().setWkJobCollectionKey(false);
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                   PGM-IABS0110
                    //                   ' - '
                    //                   'RC : '
                    //                   IDSV0003-RETURN-CODE
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0110Formatted(), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                    //                               THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
        }
    }

    /**Original name: E625-CONFIG-APPEND-GATES<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONFIGURA CALL APPEND
	 * ----------------------------------------------------------------*</pre>*/
    private void e625ConfigAppendGates() {
        // COB_CODE: MOVE 'E625-CONFIG-APPEND-GATES'  TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E625-CONFIG-APPEND-GATES");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                                    TO IDSV0301-COD-COMP-ANIA
        ws.getIabv0007().getIdsv0301().setCodCompAnia(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-ID-TEMPORARY-DATA
        //                                    TO IDSV0301-ID-TEMPORARY-DATA
        ws.getIabv0007().getIdsv0301().setIdTemporaryData(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getIdTemporaryData());
        // COB_CODE: MOVE 'BTC_REC_SCHEDULE'  TO IDSV0301-ALIAS-STR-DATO
        ws.getIabv0007().getIdsv0301().setAliasStrDato("BTC_REC_SCHEDULE");
        // COB_CODE: MOVE IDSV0001-SESSIONE   TO IDSV0301-ID-SESSION
        ws.getIabv0007().getIdsv0301().setIdSession(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getSessione());
        // COB_CODE: SET IDSV0301-CONTIGUOUS-NO TO TRUE
        ws.getIabv0007().getIdsv0301().getFlContiguous().setIdsv0301ContiguousNo();
        // COB_CODE: MOVE 0                   TO IDSV0301-TOTAL-RECURRENCE
        //                                       IDSV0301-PARTIAL-RECURRENCE
        //                                       IDSV0301-ACTUAL-RECURRENCE
        ws.getIabv0007().getIdsv0301().setTotalRecurrence(0);
        ws.getIabv0007().getIdsv0301().setPartialRecurrence(0);
        ws.getIabv0007().getIdsv0301().setActualRecurrence(0);
        // COB_CODE: SET IDSV0301-WRITE                 TO TRUE
        ws.getIabv0007().getIdsv0301().getOperazione().setIdsv0301Write();
        // COB_CODE: SET IDSV0301-DELETE-ACTION         TO TRUE.
        ws.getIabv0007().getIdsv0301().getActionType().setIdsv0301DeleteAction();
    }

    /**Original name: E630-VALOR-DA-GATES<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZA DA GATES
	 * ----------------------------------------------------------------*</pre>*/
    private void e630ValorDaGates() {
        // COB_CODE: MOVE 'E630-VALOR-DA-GATES'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E630-VALOR-DA-GATES");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF IABV0003-ELE-MAX < WK-LIMITE-ARRAY-BLOB
        //              PERFORM E635-VALORIZZA-BLOB THRU E635-EX
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getIabv0003().getIabv0003EleMax() < ws.getIabv0007().getWkLimiteArrayBlob().getWkLimiteArrayBlob()) {
            // COB_CODE: PERFORM E635-VALORIZZA-BLOB THRU E635-EX
            e635ValorizzaBlob();
        }
        else if (ws.getIabv0007().getIabi0011Area().getAppendDataGates().isYes()) {
            // COB_CODE: IF IABI0011-APPEND-DATA-GATES-YES
            //              PERFORM CALL-IDSS0300 THRU CALL-IDSS0300-EX
            //           ELSE
            //              PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            //           END-IF
            // COB_CODE: MOVE BRS-TYPE-RECORD TO IDSV0301-TYPE-RECORD
            ws.getIabv0007().getIdsv0301().setTypeRecord(ws.getBtcRecSchedule().getTypeRecord());
            // COB_CODE: MOVE BRS-DATA-RECORD-LEN
            //                                TO IDSV0301-BUFFER-DATA-LEN
            ws.getIabv0007().getIdsv0301().setBufferDataLen(TruncAbs.toInt(ws.getBtcRecSchedule().getDataRecordLen(), 7));
            // COB_CODE: SET IDSV0301-ADDRESS TO ADDRESS OF BRS-DATA-RECORD
            ws.getIabv0007().getIdsv0301().setAddress(pointerManager.addressOf(ws.getBtcRecSchedule().getDataRecord()));
            // COB_CODE: PERFORM CALL-IDSS0300 THRU CALL-IDSS0300-EX
            callIdss0300();
        }
        else {
            // COB_CODE: INITIALIZE WK-LOG-ERRORE
            initWkLogErrore();
            // COB_CODE: MOVE 'OVERFLOW CANALE DATA GATES'
            //                                    TO WK-LOR-DESC-ERRORE-ESTESA
            ws.getIabv0007().getWkLogErrore().setDescErroreEstesa("OVERFLOW CANALE DATA GATES");
            // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
            ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
            // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
            w070CaricaLogErrBatExec();
        }
    }

    /**Original name: E635-VALORIZZA-BLOB<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZA BLOB
	 * ----------------------------------------------------------------*</pre>*/
    private void e635ValorizzaBlob() {
        // COB_CODE: MOVE 'E635-VALORIZZA-BLOB'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E635-VALORIZZA-BLOB");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: ADD 1                      TO IABV0003-ELE-MAX
        ws.getIabv0007().getIabv0003().setIabv0003EleMax(Trunc.toShort(1 + ws.getIabv0007().getIabv0003().getIabv0003EleMax(), 3));
        // COB_CODE: INITIALIZE                    IABV0003-BLOB-DATA-ELE
        //                                        (IABV0003-ELE-MAX)
        initIabv0003BlobDataEle();
        // COB_CODE: EVALUATE TRUE
        //              WHEN IABI0011-DATA-1K
        //                                      TO WS-LUNG-EFF-BLOB
        //              WHEN IABI0011-DATA-2K
        //                                      TO WS-LUNG-EFF-BLOB
        //              WHEN IABI0011-DATA-3K
        //                                      TO WS-LUNG-EFF-BLOB
        //              WHEN IABI0011-DATA-10K
        //                                      TO WS-LUNG-EFF-BLOB
        //           END-EVALUATE.
        switch (ws.getIabv0007().getIabi0011Area().getLengthDataGates().getLengthDataGates()) {

            case Iabi0011LengthDataGates.DATA1K:// COB_CODE: MOVE BRS-TYPE-RECORD TO IABV0003-BLOB-DATA-TYPE-REC-1K
                //                                  (IABV0003-ELE-MAX)
                ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setIabv0003BlobDataTypeRec1k(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), ws.getBtcRecSchedule().getTypeRecord());
                // COB_CODE: MOVE BRS-DATA-RECORD TO IABV0003-BLOB-DATA-REC-1K
                //                                  (IABV0003-ELE-MAX)
                ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setIabv0003BlobDataRec1k(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), ws.getBtcRecSchedule().getDataRecord().getDataRecord());
                // COB_CODE: ADD  LENGTH OF IABV0003-BLOB-DATA-REC-1K
                //                                TO WS-LUNG-EFF-BLOB
                ws.getIabv0007().setWsLungEffBlob(Iabv0003BlobDataArray.Len.DATA_REC1K + ws.getIabv0007().getWsLungEffBlob());
                break;

            case Iabi0011LengthDataGates.DATA2K:// COB_CODE: MOVE BRS-TYPE-RECORD TO IABV0003-BLOB-DATA-TYPE-REC-2K
                //                                  (IABV0003-ELE-MAX)
                ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setIabv0003BlobDataTypeRec2k(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), ws.getBtcRecSchedule().getTypeRecord());
                // COB_CODE: MOVE BRS-DATA-RECORD TO IABV0003-BLOB-DATA-REC-2K
                //                                  (IABV0003-ELE-MAX)
                ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setIabv0003BlobDataRec2k(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), ws.getBtcRecSchedule().getDataRecord().getDataRecord());
                // COB_CODE: ADD  LENGTH OF IABV0003-BLOB-DATA-REC-2K
                //                                TO WS-LUNG-EFF-BLOB
                ws.getIabv0007().setWsLungEffBlob(Iabv0003BlobDataArray.Len.DATA_REC2K + ws.getIabv0007().getWsLungEffBlob());
                break;

            case Iabi0011LengthDataGates.DATA3K:// COB_CODE: MOVE BRS-TYPE-RECORD TO IABV0003-BLOB-DATA-TYPE-REC-3K
                //                                  (IABV0003-ELE-MAX)
                ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setIabv0003BlobDataTypeRec3k(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), ws.getBtcRecSchedule().getTypeRecord());
                // COB_CODE: MOVE BRS-DATA-RECORD TO IABV0003-BLOB-DATA-REC-3K
                //                                  (IABV0003-ELE-MAX)
                ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setIabv0003BlobDataRec3k(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), ws.getBtcRecSchedule().getDataRecord().getDataRecord());
                // COB_CODE: ADD  LENGTH OF IABV0003-BLOB-DATA-REC-3K
                //                                TO WS-LUNG-EFF-BLOB
                ws.getIabv0007().setWsLungEffBlob(Iabv0003BlobDataArray.Len.DATA_REC3K + ws.getIabv0007().getWsLungEffBlob());
                break;

            case Iabi0011LengthDataGates.DATA10K:// COB_CODE: MOVE BRS-TYPE-RECORD TO IABV0003-BLOB-DATA-TYPE-REC
                //                                  (IABV0003-ELE-MAX)
                ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setDataTypeRec(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), ws.getBtcRecSchedule().getTypeRecord());
                // COB_CODE: MOVE BRS-DATA-RECORD TO IABV0003-BLOB-DATA-REC
                //                                  (IABV0003-ELE-MAX)
                ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setDataRec(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), ws.getBtcRecSchedule().getDataRecord().getDataRecord());
                // COB_CODE: ADD  LENGTH OF IABV0003-BLOB-DATA-REC
                //                                TO WS-LUNG-EFF-BLOB
                ws.getIabv0007().setWsLungEffBlob(Iabv0003BlobDataArray.Len.DATA_REC + ws.getIabv0007().getWsLungEffBlob());
                break;

            default:break;
        }
    }

    /**Original name: E150-UPD-IN-ESECUZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORA SERVIZIO BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void e150UpdInEsecuzione() {
        // COB_CODE: MOVE 'E150-UPD-IN-ESECUZIONE'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E150-UPD-IN-ESECUZIONE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: MOVE JOB-IN-ESECUZIONE           TO IABV0002-STATE-CURRENT
        ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(ws.getIabv0007().getIabv0004().getJobInEsecuzione());
        // COB_CODE: ADD 1                            TO BJS-EXECUTIONS-COUNT
        ws.getBtcJobSchedule().getBjsExecutionsCount().setBjsExecutionsCount(1 + ws.getBtcJobSchedule().getBjsExecutionsCount().getBjsExecutionsCount());
        // COB_CODE: SET IDSV0003-PRIMARY-KEY         TO TRUE
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
        // COB_CODE: SET WK-NOT-FOUND-NOT-ALLOWED     TO TRUE
        ws.getIabv0007().setFlagFoundNotFound(false);
        // COB_CODE: MOVE IDSV0001-USER-NAME          TO BJS-USER-START
        ws.getBtcJobSchedule().setBjsUserStart(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getUserName());
        // COB_CODE: PERFORM A150-SET-CURRENT-TIMESTAMP       THRU A150-EX
        a150SetCurrentTimestamp();
        // COB_CODE: MOVE WS-TIMESTAMP-N              TO BJS-DT-START
        ws.getBtcJobSchedule().getBjsDtStart().setBjsDtStart(ws.getIabv0007().getWsTimestampN());
        // COB_CODE: MOVE HIGH-VALUE                  TO BJS-DT-END-NULL
        ws.getBtcJobSchedule().getBjsDtEnd().setBjsDtEndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BjsDtEnd.Len.BJS_DT_END_NULL));
        // COB_CODE: MOVE HIGH-VALUE                  TO BJS-FLAG-WARNINGS
        ws.getBtcJobSchedule().setBjsFlagWarnings(Types.HIGH_CHAR_VAL);
        // COB_CODE: PERFORM U000-UPD-JOB             THRU U000-EX.
        u000UpdJob();
    }

    /**Original name: E500-INS-JOB-EXECUTION<br>
	 * <pre>----------------------------------------------------------------*
	 *  INSERT IN JOB EXECUTION
	 * ----------------------------------------------------------------*</pre>*/
    private void e500InsJobExecution() {
        ConcatUtil concatUtil = null;
        Iabs0080 iabs0080 = null;
        // COB_CODE: MOVE 'E500-INS-JOB-EXECUTION'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E500-INS-JOB-EXECUTION");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE:      IF IABI0011-FLAG-STOR-ESECUZ-YES AND
        //                   NOT IABV0006-GUIDE-AD-HOC     AND
        //                   NOT IABV0006-BY-BOOKING
        //           *--> TIPO OPERAZIONE
        //                   END-IF
        //                END-IF.
        if (ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().isYes() && !ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() && !ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking()) {
            //--> TIPO OPERAZIONE
            // COB_CODE: SET IDSV0003-INSERT            TO TRUE
            ws.getIabv0007().getIdsv0003().getOperazione().setInsert();
            // COB_CODE: SET IDSV0003-PRIMARY-KEY       TO TRUE
            ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
            ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
            ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
            // COB_CODE: PERFORM E700-VALORIZZA-STD    THRU E700-EX
            e700ValorizzaStd();
            // COB_CODE: MOVE PGM-IABS0080       TO WK-PGM-ERRORE
            ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getIabv0004().getPgmIabs0080());
            // COB_CODE: CALL PGM-IABS0080       USING IDSV0003
            //                                         BTC-JOB-EXECUTION
            //           ON EXCEPTION
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-CALL
            try {
                iabs0080 = Iabs0080.getInstance();
                iabs0080.run(ws.getIabv0007().getIdsv0003(), ws.getBtcJobExecution());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0080
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                    DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0080Formatted(), " - ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE:            IF IDSV0003-SUCCESSFUL-RC
                //                         END-EVALUATE
                //                      ELSE
                //           *--> GESTIRE ERRORE DISPATCHER
                //                                             THRU W070-EX
                //                      END-IF
                if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:               EVALUATE TRUE
                    //                            WHEN IDSV0003-SUCCESSFUL-SQL
                    //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                              CONTINUE
                    //                         WHEN IDSV0003-NEGATIVI
                    //           *--> ERRORE DI ACCESSO AL DB
                    //                                                THRU W070-EX
                    //                         END-EVALUATE
                    if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                        //--> ERRORE DI ACCESSO AL DB
                        // COB_CODE: INITIALIZE WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                        ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                        // COB_CODE: STRING 'ERRORE MODULO '
                        //                  DELIMITED BY SIZE
                        //                  PGM-IABS0080
                        //                  DELIMITED BY SIZE
                        //                  ' SQLCODE : '
                        //                  DELIMITED BY SIZE
                        //                  WK-SQLCODE ' - '
                        //                  DELIMITED BY SIZE
                        //                  'ID-BATCH : '
                        //                  WK-ID-BATCH ' - '
                        //                  DELIMITED BY SIZE
                        //                  'ID-JOB : '
                        //                  WK-ID-JOB ' - '
                        //                  DELIMITED BY SIZE INTO
                        //                  WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0080Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString(), " - ", "ID-BATCH : ", ws.getIabv0007().getWkIdBatchAsString(), " - ", "ID-JOB : ", ws.getIabv0007().getWkIdJobAsString(), " - "});
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: MOVE ERRORE-FATALE  TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                        // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                        //                               THRU W070-EX
                        w070CaricaLogErrBatExec();
                    }
                }
                else {
                    //--> GESTIRE ERRORE DISPATCHER
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                   PGM-IABS0080
                    //                   ' - '
                    //                   'RC : '
                    //                   IDSV0003-RETURN-CODE
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0080Formatted(), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                    //                               THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
        }
    }

    /**Original name: E550-UPD-JOB-EXECUTION<br>
	 * <pre>----------------------------------------------------------------*
	 *  UPDATE IN JOB EXECUTION
	 * ----------------------------------------------------------------*</pre>*/
    private void e550UpdJobExecution() {
        ConcatUtil concatUtil = null;
        Iabs0080 iabs0080 = null;
        // COB_CODE: MOVE 'E550-UPD-JOB-EXECUTION'    TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E550-UPD-JOB-EXECUTION");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE:      IF NOT IABV0006-GUIDE-AD-HOC     AND
        //                   NOT IABV0006-BY-BOOKING       AND
        //                   NOT WK-SIMULAZIONE-INFR       AND
        //                   IABI0011-FLAG-STOR-ESECUZ-YES
        //           *-->    TIPO OPERAZIONE
        //                   END-IF
        //                END-IF.
        if (!ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() && !ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking() && !ws.getIabv0007().getWkSimulazione().isInfr() && ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().isYes()) {
            //-->    TIPO OPERAZIONE
            // COB_CODE: SET IDSV0003-UPDATE            TO TRUE
            ws.getIabv0007().getIdsv0003().getOperazione().setIdsi0011Update();
            // COB_CODE: SET IDSV0003-PRIMARY-KEY       TO TRUE
            ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
            //-->    INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
            ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
            ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
            // COB_CODE: PERFORM E700-VALORIZZA-STD     THRU E700-EX
            e700ValorizzaStd();
            // COB_CODE: MOVE PGM-IABS0080              TO WK-PGM-ERRORE
            ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getIabv0004().getPgmIabs0080());
            // COB_CODE: CALL PGM-IABS0080              USING IDSV0003
            //                                                BTC-JOB-EXECUTION
            //           ON EXCEPTION
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-CALL
            try {
                iabs0080 = Iabs0080.getInstance();
                iabs0080.run(ws.getIabv0007().getIdsv0003(), ws.getBtcJobExecution());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0080
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                    DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0080Formatted(), " - ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE:            IF IDSV0003-SUCCESSFUL-RC
                //                         END-EVALUATE
                //                      ELSE
                //           *--> GESTIRE ERRORE DISPATCHER
                //                                             THRU W070-EX
                //                      END-IF
                if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:               EVALUATE TRUE
                    //                            WHEN IDSV0003-SUCCESSFUL-SQL
                    //           *-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                                 PERFORM E800-INS-EXE-MESSAGE THRU E800-EX
                    //                            WHEN IDSV0003-NEGATIVI
                    //           *--->            ERRORE DI ACCESSO AL DB
                    //                                                  THRU W070-EX
                    //                         END-EVALUATE
                    if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                        //-->             OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: PERFORM E800-INS-EXE-MESSAGE THRU E800-EX
                        e800InsExeMessage();
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                        //--->            ERRORE DI ACCESSO AL DB
                        // COB_CODE: INITIALIZE WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                        ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                        // COB_CODE: STRING 'ERRORE MODULO '
                        //                  DELIMITED BY SIZE
                        //                  PGM-IABS0080
                        //                  DELIMITED BY SIZE
                        //                  ' SQLCODE : '
                        //                  WK-SQLCODE ' - '
                        //                  'ID-BATCH : '
                        //                  WK-ID-BATCH ' - '
                        //                  'ID-JOB : '
                        //                  WK-ID-JOB ' - '
                        //                  DELIMITED BY SIZE INTO
                        //                  WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {"ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0080Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString(), " - ", "ID-BATCH : ", ws.getIabv0007().getWkIdBatchAsString(), " - ", "ID-JOB : ", ws.getIabv0007().getWkIdJobAsString(), " - "});
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                        // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                        //                               THRU W070-EX
                        w070CaricaLogErrBatExec();
                    }
                }
                else {
                    //--> GESTIRE ERRORE DISPATCHER
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                   PGM-IABS0080
                    //                   ' - '
                    //                   'RC : '
                    //                   IDSV0003-RETURN-CODE
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0080Formatted(), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                    //                               THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
        }
    }

    /**Original name: E700-VALORIZZA-STD<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZA JOB EXECUTION DA STD
	 * ----------------------------------------------------------------*</pre>*/
    private void e700ValorizzaStd() {
        // COB_CODE: MOVE 'E700-VALORIZZA-STD'        TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E700-VALORIZZA-STD");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: MOVE WS-LUNG-EFF-BLOB         TO IDSV0003-BUFFER-WHERE-COND.
        ws.getIabv0007().getIdsv0003().setBufferWhereCond(ws.getIabv0007().getWsLungEffBlobFormatted());
        // COB_CODE: MOVE BJS-ID-BATCH             TO BJE-ID-BATCH
        ws.getBtcJobExecution().setBjeIdBatch(ws.getBtcJobSchedule().getBjsIdBatch());
        // COB_CODE: MOVE BJS-ID-JOB               TO BJE-ID-JOB
        //                                            WK-ID-JOB
        ws.getBtcJobExecution().setBjeIdJob(ws.getBtcJobSchedule().getBjsIdJob());
        ws.getIabv0007().setWkIdJob(TruncAbs.toInt(ws.getBtcJobSchedule().getBjsIdJob(), 9));
        // COB_CODE: MOVE BJS-DT-START             TO BJE-DT-START
        ws.getBtcJobExecution().setBjeDtStart(ws.getBtcJobSchedule().getBjsDtStart().getBjsDtStart());
        // COB_CODE: IF BJS-DT-END-NULL NOT = HIGH-VALUE
        //              MOVE BJS-DT-END            TO BJE-DT-END
        //           ELSE
        //              MOVE BJS-DT-END-NULL       TO BJE-DT-END-NULL
        //           END-IF
        if (!Characters.EQ_HIGH.test(ws.getBtcJobSchedule().getBjsDtEnd().getBjsDtEndNullFormatted())) {
            // COB_CODE: MOVE BJS-DT-END            TO BJE-DT-END
            ws.getBtcJobExecution().getBjeDtEnd().setBjeDtEnd(ws.getBtcJobSchedule().getBjsDtEnd().getBjsDtEnd());
        }
        else {
            // COB_CODE: MOVE BJS-DT-END-NULL       TO BJE-DT-END-NULL
            ws.getBtcJobExecution().getBjeDtEnd().setBjeDtEndNull(ws.getBtcJobSchedule().getBjsDtEnd().getBjsDtEndNull());
        }
        // COB_CODE: MOVE BJS-USER-START           TO BJE-USER-START
        ws.getBtcJobExecution().setBjeUserStart(ws.getBtcJobSchedule().getBjsUserStart());
        // COB_CODE: MOVE IABV0003-BLOB-DATA       TO BJE-DATA
        ws.getBtcJobExecution().setBjeData(ws.getIabv0007().getIabv0003().getIabv0003BlobDataFormatted());
        // COB_CODE: MOVE IABV0002-STATE-CURRENT   TO BJE-COD-ELAB-STATE
        ws.getBtcJobExecution().setBjeCodElabState(ws.getIabv0007().getIabv0002().getIabv0002StateCurrent());
        // COB_CODE: MOVE BJS-FLAG-WARNINGS        TO BJE-FLAG-WARNINGS.
        ws.getBtcJobExecution().setBjeFlagWarnings(ws.getBtcJobSchedule().getBjsFlagWarnings());
    }

    /**Original name: E800-INS-EXE-MESSAGE<br>
	 * <pre>----------------------------------------------------------------*
	 *  INSERT IN EXE MESSAGE
	 * ----------------------------------------------------------------*</pre>*/
    private void e800InsExeMessage() {
        ConcatUtil concatUtil = null;
        Iabs0090 iabs0090 = null;
        // COB_CODE: MOVE 'E800-INS-EXE-MESSAGE'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E800-INS-EXE-MESSAGE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE:      IF NOT IABV0006-GUIDE-AD-HOC     AND
        //                   NOT IABV0006-BY-BOOKING       AND
        //                   NOT WK-SIMULAZIONE-INFR       AND
        //                   IABI0011-FLAG-STOR-ESECUZ-YES
        //           *--> TIPO OPERAZIONE
        //                   END-IF
        //                END-IF.
        if (!ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() && !ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking() && !ws.getIabv0007().getWkSimulazione().isInfr() && ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().isYes()) {
            //--> TIPO OPERAZIONE
            // COB_CODE: SET IDSV0003-INSERT            TO TRUE
            ws.getIabv0007().getIdsv0003().getOperazione().setInsert();
            // COB_CODE: SET IDSV0003-PRIMARY-KEY       TO TRUE
            ws.getIabv0007().getIdsv0003().getLivelloOperazione().setPrimaryKey();
            //--> INIZIALIZZA CODICE DI RITORNO
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
            ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
            // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
            ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
            // COB_CODE: PERFORM E850-VALORIZZA-EXE-MESSAGE THRU E850-EX
            e850ValorizzaExeMessage();
            // COB_CODE: MOVE PGM-IABS0090           TO WK-PGM-ERRORE
            ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getIabv0004().getPgmIabs0090());
            // COB_CODE: CALL PGM-IABS0090           USING IDSV0003
            //                                             BTC-EXE-MESSAGE
            //           ON EXCEPTION
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-CALL
            try {
                iabs0090 = Iabs0090.getInstance();
                iabs0090.run(ws.getIabv0007().getIdsv0003(), ws.getBtcExeMessage());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0090
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                    DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0090Formatted(), " - ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE:            IF IDSV0003-SUCCESSFUL-RC
                //                         END-EVALUATE
                //                      ELSE
                //           *-->       GESTIRE ERRORE DISPATCHER
                //                         PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                //                      END-IF
                if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:               EVALUATE TRUE
                    //                         WHEN IDSV0003-SUCCESSFUL-SQL
                    //           *--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                            CONTINUE
                    //                         WHEN IDSV0003-NEGATIVI
                    //           *--> ERRORE DI ACCESSO AL DB
                    //                            PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    //                         END-EVALUATE
                    if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                    //--> OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: CONTINUE
                    //continue
                    }
                    else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                        //--> ERRORE DI ACCESSO AL DB
                        // COB_CODE: INITIALIZE WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                        ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                        // COB_CODE: STRING 'ERRORE MODULO '
                        //                  DELIMITED BY SIZE
                        //                  PGM-IABS0090
                        //                  DELIMITED BY SIZE
                        //                  ' SQLCODE : '
                        //                  WK-SQLCODE
                        //                  DELIMITED BY SIZE INTO
                        //                  WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0090Formatted(), " SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: MOVE ERRORE-FATALE    TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                        // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                        w070CaricaLogErrBatExec();
                    }
                }
                else {
                    //-->       GESTIRE ERRORE DISPATCHER
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: STRING 'ERRORE MODULO '
                    //                   PGM-IABS0090
                    //                   ' - '
                    //                   'RC : '
                    //                   IDSV0003-RETURN-CODE
                    //                   DELIMITED BY SIZE INTO
                    //                   WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0090Formatted(), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
        }
    }

    /**Original name: E850-VALORIZZA-EXE-MESSAGE<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALORIZZA EXE MESSAGE
	 * ----------------------------------------------------------------*</pre>*/
    private void e850ValorizzaExeMessage() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'E850-VALORIZZA-EXE-MESSAGE' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("E850-VALORIZZA-EXE-MESSAGE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO   TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                     TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: INITIALIZE BTC-EXE-MESSAGE.
        initBtcExeMessage();
        // COB_CODE: MOVE BJE-ID-BATCH           TO BEM-ID-BATCH
        ws.getBtcExeMessage().setIdBatch(ws.getBtcJobExecution().getBjeIdBatch());
        // COB_CODE: MOVE BJE-ID-JOB             TO BEM-ID-JOB
        ws.getBtcExeMessage().setIdJob(ws.getBtcJobExecution().getBjeIdJob());
        // COB_CODE: MOVE BJE-ID-EXECUTION       TO BEM-ID-EXECUTION
        ws.getBtcExeMessage().setIdExecution(ws.getBtcJobExecution().getBjeIdExecution());
        // COB_CODE: IF WK-PGM-BUS-SERVICE
        //              END-STRING
        //           ELSE
        //              END-STRING
        //           END-IF.
        if (ws.getIabv0007().getFlagTipoPgm().isBusService()) {
            // COB_CODE: STRING IDSV0001-COD-SERVIZIO-BE ' - '
            //               IDSV0001-DESC-ERRORE-ESTESA
            //               DELIMITED BY SIZE INTO
            //               BEM-MESSAGE
            //           END-STRING
            concatUtil = ConcatUtil.buildString(BtcExeMessage.Len.MESSAGE, ws.getIabv0007().getAreaIdsv0001().getLogErrore().getCodServizioBeFormatted(), " - ", ws.getIabv0007().getAreaIdsv0001().getLogErrore().getDescErroreEstesaFormatted());
            ws.getBtcExeMessage().setMessage(concatUtil.replaceInString(ws.getBtcExeMessage().getMessageFormatted()));
        }
        else {
            // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
            ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
            // COB_CODE: STRING WK-PGM-ERRORE        ' - '
            //                  'RC : '  IDSV0003-RETURN-CODE
            //                  ' - SQL : ' WK-SQLCODE ' - '
            //               IDSV0001-DESC-ERRORE-ESTESA
            //               DELIMITED BY SIZE INTO
            //               BEM-MESSAGE
            //           END-STRING
            concatUtil = ConcatUtil.buildString(BtcExeMessage.Len.MESSAGE, new String[] {ws.getIabv0007().getWkPgmErroreFormatted(), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted(), " - SQL : ", ws.getIabv0007().getWkSqlcodeAsString(), " - ", ws.getIabv0007().getAreaIdsv0001().getLogErrore().getDescErroreEstesaFormatted()});
            ws.getBtcExeMessage().setMessage(concatUtil.replaceInString(ws.getBtcExeMessage().getMessageFormatted()));
        }
    }

    /**Original name: L000-LANCIA-BUSINESS<br>
	 * <pre>----------------------------------------------------------------*
	 *  LANCIA BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void l000LanciaBusiness() {
        // COB_CODE: MOVE 'L000-LANCIA-BUSINESS'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("L000-LANCIA-BUSINESS");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET WK-LANCIA-BUSINESS-YES         TO TRUE
        ws.getIabv0007().setFlagLanciaBusiness(true);
        // COB_CODE: PERFORM L500-PRE-BUSINESS          THRU L500-EX
        l500PreBusiness();
        // COB_CODE: INITIALIZE IABV0006-REPORT.
        initReport();
        // COB_CODE: EVALUATE TRUE
        //           WHEN GESTIONE-REAL-TIME
        //              PERFORM L600-CALL-BUS-REAL-TIME THRU L600-EX
        //           WHEN GESTIONE-SUSPEND
        //              PERFORM L601-CALL-BUS-SUSPEND   THRU L601-EX
        //           END-EVALUATE.
        switch (ws.getIabv0007().getFlagGestioneLanciBus().getFlagGestioneLanciBus()) {

            case FlagGestioneLanciBus.REAL_TIME:// COB_CODE: PERFORM L600-CALL-BUS-REAL-TIME THRU L600-EX
                l600CallBusRealTime();
                break;

            case FlagGestioneLanciBus.SUSPEND:// COB_CODE: IF WK-FINE-ALIMENTAZIONE-YES
                //              SET WK-LANCIA-BUSINESS-YES   TO TRUE
                //           END-IF
                if (ws.getIabv0007().isFlagFineAlimentazione()) {
                    // COB_CODE: SET WK-LANCIA-BUSINESS-YES   TO TRUE
                    ws.getIabv0007().setFlagLanciaBusiness(true);
                }
                // COB_CODE: PERFORM L601-CALL-BUS-SUSPEND   THRU L601-EX
                l601CallBusSuspend();
                break;

            default:break;
        }
        // COB_CODE: PERFORM L700-POST-BUSINESS         THRU L700-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1258, because the code is unreachable.
    }

    /**Original name: L600-CALL-BUS-REAL-TIME<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL BUSINESS GESTIONE REAL TIME
	 * ----------------------------------------------------------------*</pre>*/
    private void l600CallBusRealTime() {
        // COB_CODE: MOVE 'L600-CALL-BUS-REAL-TIME'   TO WK-LABEL.
        ws.getIabv0007().setWkLabel("L600-CALL-BUS-REAL-TIME");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF WK-LANCIA-BUSINESS-YES
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().isFlagLanciaBusiness()) {
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              SET IABV0006-CORRENTE-LANCIO TO TRUE
            //           END-IF
            if (ws.getIabv0007().getAreaIdsv0001().getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: ADD 1                        TO CONT-BUS-SERV-ESEG
                ws.getIabv0007().setContBusServEseg(Trunc.toInt(1 + ws.getIabv0007().getContBusServEseg(), 9));
                // COB_CODE: SET  IDSV8888-BUSINESS-DBG   TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
                // COB_CODE: MOVE WK-PGM-BUSINESS         TO IDSV8888-NOME-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
                // COB_CODE: MOVE 'Business Service'      TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service");
                // COB_CODE: SET  IDSV8888-INIZIO         TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
                // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setStressTestDbg();
                // COB_CODE: SET  IDSV8888-SONDA-S8           TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setIdsv8888SondaS8();
                // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
                //                TO IDSV8888-MODALITA-ESECUTIVA
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
                // COB_CODE: MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setUserName(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getUserName());
                // COB_CODE: MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
                // COB_CODE: MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service LIV-1");
                // COB_CODE: SET  IDSV8888-INIZIO             TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
                // COB_CODE: PERFORM L800-ESEGUI-CALL-BUS THRU L800-EX
                l800EseguiCallBus();
                // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setStressTestDbg();
                // COB_CODE: SET  IDSV8888-SONDA-S8           TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setIdsv8888SondaS8();
                // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
                //                TO IDSV8888-MODALITA-ESECUTIVA
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
                // COB_CODE: MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setUserName(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getUserName());
                // COB_CODE: MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
                // COB_CODE: MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service LIV-1");
                // COB_CODE: SET  IDSV8888-FINE             TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
                // COB_CODE: SET  IDSV8888-BUSINESS-DBG   TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
                // COB_CODE: MOVE WK-PGM-BUSINESS         TO IDSV8888-NOME-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
                // COB_CODE: MOVE 'Business Service'      TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service");
                // COB_CODE: SET  IDSV8888-FINE           TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
                // COB_CODE: SET IABV0006-CORRENTE-LANCIO TO TRUE
                ws.getIabv0006().getTipoLancioBus().setIabv0006CorrenteLancio();
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: IF IDSV0001-ESITO-OK
                //              END-IF
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIabv0007().getAreaIdsv0001().getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: ADD 1               TO CONT-BUS-SERV-ESITO-OK
                    ws.getIabv0007().setContBusServEsitoOk(Trunc.toInt(1 + ws.getIabv0007().getContBusServEsitoOk(), 9));
                    // COB_CODE: IF WK-SIMULAZIONE-INFR
                    //              PERFORM Y100-ROLLBACK-SAVEPOINT THRU Y100-EX
                    //           END-IF
                    if (ws.getIabv0007().getWkSimulazione().isInfr()) {
                        // COB_CODE: PERFORM Y100-ROLLBACK-SAVEPOINT THRU Y100-EX
                        y100RollbackSavepoint();
                    }
                    // COB_CODE: IF WK-ERRORE-NO
                    //              END-IF
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
                        w000CaricaLogErrore();
                        // COB_CODE: IF WARNING-TROVATO-YES
                        //              SET WARNING-TROVATO-NO         TO TRUE
                        //           END-IF
                        if (ws.getIabv0007().isFlagWarningTrovato()) {
                            // COB_CODE: ADD 1              TO CONT-BUS-SERV-WARNING
                            ws.getIabv0007().setContBusServWarning(Trunc.toInt(1 + ws.getIabv0007().getContBusServWarning(), 9));
                            // COB_CODE: SET WARNING-TROVATO-NO         TO TRUE
                            ws.getIabv0007().setFlagWarningTrovato(false);
                        }
                        // COB_CODE: IF WK-ERRORE-NO
                        //              END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: IF NOT WK-SIMULAZIONE-INFR       AND
                            //              NOT WK-SIMULAZIONE-APPL       AND
                            //              NOT IABI0011-SENZA-RIPARTENZA AND
                            //              NOT SEQUENTIAL-GUIDE
                            //                                         THRU B700-EX
                            //           END-IF
                            if (!ws.getIabv0007().getWkSimulazione().isInfr() && !ws.getIabv0007().getWkSimulazione().isAppl() && !ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isSenzaRipartenza() && !ws.getIabv0007().getWkFlagGuideType().isSequentialGuide()) {
                                // COB_CODE: PERFORM B700-UPD-ESEGUITO-OK
                                //                                      THRU B700-EX
                                b700UpdEseguitoOk();
                            }
                            // COB_CODE: IF WK-ERRORE-NO
                            //              END-IF
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: IF ((BBT-COMMIT-FREQUENCY > 0) AND
                                //              (WK-COMMIT-FREQUENCY >=
                                //              BBT-COMMIT-FREQUENCY)) OR
                                //              IABV0006-COMMIT-YES
                                //              END-IF
                                //           ELSE
                                //              PERFORM Y300-SAVEPOINT  THRU Y300-EX
                                //           END-IF
                                if (ws.getBtcBatchType().getBbtCommitFrequency().getBbtCommitFrequency() > 0 && ws.getIabv0007().getWkCommitFrequency() >= ws.getBtcBatchType().getBbtCommitFrequency().getBbtCommitFrequency() || ws.getIabv0006().getFlagCommit().isIabv0006CommitYes()) {
                                    // COB_CODE: SET IABV0006-COMMIT-NO   TO TRUE
                                    ws.getIabv0006().getFlagCommit().setIabv0006CommitNo();
                                    // COB_CODE: IF NOT WK-SIMULAZIONE-INFR
                                    //              END-IF
                                    //           END-IF
                                    if (!ws.getIabv0007().getWkSimulazione().isInfr()) {
                                        // COB_CODE: PERFORM Y000-COMMIT  THRU Y000-EX
                                        y000Commit();
                                        // COB_CODE: IF (IABV0006-STD-TYPE-REC-NO   OR
                                        //               IABV0006-STD-TYPE-REC-YES) AND
                                        //               IDSV0001-ARCH-BATCH-DBG
                                        //                 TO WK-ID-JOB-ULT-COMMIT
                                        //           END-IF
                                        if ((ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecNo() || ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecYes()) && ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().isIdsv0001ArchBatchDbg()) {
                                            // COB_CODE: MOVE WK-ID-JOB
                                            //             TO WK-ID-JOB-ULT-COMMIT
                                            ws.getIabv0007().setWkIdJobUltCommitFormatted(ws.getIabv0007().getWkIdJobFormatted());
                                        }
                                        // COB_CODE: IF WK-ERRORE-NO
                                        //                 THRU Y050-EX
                                        //           END-IF
                                        if (!ws.getIabv0007().isWkErrore()) {
                                            // COB_CODE: PERFORM Y050-CONTROLLO-COMMIT
                                            //              THRU Y050-EX
                                            y050ControlloCommit();
                                        }
                                    }
                                }
                                else {
                                    // COB_CODE: ADD 1 TO WK-COMMIT-FREQUENCY
                                    ws.getIabv0007().setWkCommitFrequency(1 + ws.getIabv0007().getWkCommitFrequency());
                                    // COB_CODE: PERFORM Y300-SAVEPOINT  THRU Y300-EX
                                    y300Savepoint();
                                }
                            }
                        }
                    }
                }
                else {
                    // COB_CODE: IF IDSV0001-FORZ-RC-04-YES
                    //               ADD 1               TO CONT-BUS-SERV-ESITO-OK
                    //           ELSE
                    //               ADD 1               TO CONT-BUS-SERV-ESITO-KO
                    //           END-IF
                    if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getForzRc04().isIdsv0001ForzRc04Yes()) {
                        // COB_CODE: ADD 1               TO CONT-BUS-SERV-ESITO-OK
                        ws.getIabv0007().setContBusServEsitoOk(Trunc.toInt(1 + ws.getIabv0007().getContBusServEsitoOk(), 9));
                    }
                    else {
                        // COB_CODE: ADD 1               TO CONT-BUS-SERV-ESITO-KO
                        ws.getIabv0007().setContBusServEsitoKo(Trunc.toInt(1 + ws.getIabv0007().getContBusServEsitoKo(), 9));
                    }
                    // COB_CODE: SET  WK-PGM-BUS-SERVICE         TO TRUE
                    ws.getIabv0007().getFlagTipoPgm().setBusService();
                    // COB_CODE: MOVE WK-PGM-BUSINESS            TO WK-PGM-ERRORE
                    ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getWkPgmBusiness());
                    // COB_CODE: SET WK-BAT-COLLECTION-KEY-KO         TO TRUE
                    ws.getIabv0007().setWkBatCollectionKey(false);
                    // COB_CODE: SET WK-JOB-COLLECTION-KEY-KO         TO TRUE
                    ws.getIabv0007().setWkJobCollectionKey(false);
                    // COB_CODE: IF IABI0011-TRATTAMENTO-FULL
                    //              END-IF
                    //           ELSE
                    //              PERFORM Y100-ROLLBACK-SAVEPOINT   THRU Y100-EX
                    //           END-IF
                    if (ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().isFull()) {
                        // COB_CODE: PERFORM Y200-ROLLBACK             THRU Y200-EX
                        y200Rollback();
                        // COB_CODE: SET WK-SKIP-BATCH-YES             TO TRUE
                        ws.getIabv0007().setFlagSkipBatch(true);
                        // COB_CODE: IF WK-FINE-ALIMENTAZIONE-NO
                        //              END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isFlagFineAlimentazione()) {
                            // COB_CODE: PERFORM N501-PRE-GUIDE-AD-HOC     THRU N501-EX
                            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1390, because the code is unreachable.
                            // COB_CODE: IF WK-ERRORE-NO
                            //              PERFORM N509-SWITCH-GUIDE      THRU N509-EX
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: SET  IDSV0003-CLOSE-CURSOR     TO TRUE
                                ws.getIabv0007().getIdsv0003().getOperazione().setIdsv0003CloseCursor();
                                // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
                                ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
                                // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
                                ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
                                // COB_CODE: PERFORM N509-SWITCH-GUIDE      THRU N509-EX
                                n509SwitchGuide();
                            }
                        }
                    }
                    else {
                        // COB_CODE: PERFORM Y100-ROLLBACK-SAVEPOINT   THRU Y100-EX
                        y100RollbackSavepoint();
                    }
                    // COB_CODE: IF WK-ERRORE-NO
                    //             END-IF
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
                        w000CaricaLogErrore();
                        // COB_CODE: IF IDSV0001-FORZ-RC-04-YES AND
                        //              WARNING-TROVATO-YES
                        //              SET WARNING-TROVATO-NO         TO TRUE
                        //           END-IF
                        if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getForzRc04().isIdsv0001ForzRc04Yes() && ws.getIabv0007().isFlagWarningTrovato()) {
                            // COB_CODE: ADD 1              TO CONT-BUS-SERV-WARNING
                            ws.getIabv0007().setContBusServWarning(Trunc.toInt(1 + ws.getIabv0007().getContBusServWarning(), 9));
                            // COB_CODE: SET WARNING-TROVATO-NO         TO TRUE
                            ws.getIabv0007().setFlagWarningTrovato(false);
                        }
                        // COB_CODE: IF WK-ERRORE-NO
                        //              END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: IF NOT WK-SIMULAZIONE-INFR       AND
                            //              NOT WK-SIMULAZIONE-APPL       AND
                            //              NOT IABI0011-SENZA-RIPARTENZA AND
                            //              NOT SEQUENTIAL-GUIDE
                            //                                        THRU B800-EX
                            //           END-IF
                            if (!ws.getIabv0007().getWkSimulazione().isInfr() && !ws.getIabv0007().getWkSimulazione().isAppl() && !ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().isSenzaRipartenza() && !ws.getIabv0007().getWkFlagGuideType().isSequentialGuide()) {
                                // COB_CODE: PERFORM B800-UPD-ESEGUITO-KO
                                //                                 THRU B800-EX
                                b800UpdEseguitoKo();
                            }
                            // COB_CODE: IF WK-ERRORE-NO
                            //              END-IF
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: PERFORM Y000-COMMIT        THRU Y000-EX
                                y000Commit();
                                // COB_CODE: IF WK-ERRORE-NO
                                //                 THRU Y050-EX
                                //           END-IF
                                if (!ws.getIabv0007().isWkErrore()) {
                                    // COB_CODE: PERFORM Y050-CONTROLLO-COMMIT
                                    //              THRU Y050-EX
                                    y050ControlloCommit();
                                }
                                // COB_CODE: IF WK-ERRORE-NO
                                //              END-IF
                                //           END-IF
                                if (!ws.getIabv0007().isWkErrore()) {
                                    // COB_CODE: SET WK-PGM-ARCHITECTURE    TO TRUE
                                    ws.getIabv0007().getFlagTipoPgm().setArchitecture();
                                    // COB_CODE: IF WK-PGM-SERV-SECON-BUS NOT =
                                    //              SPACES AND LOW-VALUE AND HIGH-VALUE
                                    //                                    THRU L400-EX
                                    //           END-IF
                                    if (!Characters.EQ_SPACE.test(ws.getWkPgmServSeconBus()) && !Characters.EQ_LOW.test(ws.getWkPgmServSeconBusFormatted()) && !Characters.EQ_HIGH.test(ws.getWkPgmServSeconBusFormatted())) {
                                        // COB_CODE: PERFORM L400-TRATTA-SERV-SECOND-BUS
                                        //                                 THRU L400-EX
                                        l400TrattaServSecondBus();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**Original name: L601-CALL-BUS-SUSPEND<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL BUSINESS GESTIONE SUSPEND
	 * ----------------------------------------------------------------*</pre>*/
    private void l601CallBusSuspend() {
        // COB_CODE: MOVE 'L601-CALL-BUS-SUSPEND'     TO WK-LABEL.
        ws.getIabv0007().setWkLabel("L601-CALL-BUS-SUSPEND");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF WK-LANCIA-BUSINESS-YES
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().isFlagLanciaBusiness()) {
            // COB_CODE: IF IDSV0001-ESITO-OK
            //              SET IABV0006-CORRENTE-LANCIO TO TRUE
            //           END-IF
            if (ws.getIabv0007().getAreaIdsv0001().getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: ADD 1                        TO CONT-BUS-SERV-ESEG
                ws.getIabv0007().setContBusServEseg(Trunc.toInt(1 + ws.getIabv0007().getContBusServEseg(), 9));
                // COB_CODE: SET  IDSV8888-BUSINESS-DBG   TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
                // COB_CODE: MOVE WK-PGM-BUSINESS         TO IDSV8888-NOME-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
                // COB_CODE: MOVE 'Business Service'      TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service");
                // COB_CODE: SET  IDSV8888-INIZIO         TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
                // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setStressTestDbg();
                // COB_CODE: SET  IDSV8888-SONDA-S8           TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setIdsv8888SondaS8();
                // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
                //                TO IDSV8888-MODALITA-ESECUTIVA
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
                // COB_CODE: MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setUserName(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getUserName());
                // COB_CODE: MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
                // COB_CODE: MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service LIV-1");
                // COB_CODE: SET  IDSV8888-INIZIO             TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
                // COB_CODE: PERFORM L800-ESEGUI-CALL-BUS THRU L800-EX
                l800EseguiCallBus();
                // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setStressTestDbg();
                // COB_CODE: SET  IDSV8888-SONDA-S8           TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setIdsv8888SondaS8();
                // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
                //                TO IDSV8888-MODALITA-ESECUTIVA
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
                // COB_CODE: MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setUserName(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getUserName());
                // COB_CODE: MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
                // COB_CODE: MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service LIV-1");
                // COB_CODE: SET  IDSV8888-FINE               TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
                // COB_CODE: SET  IDSV8888-BUSINESS-DBG   TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
                // COB_CODE: MOVE WK-PGM-BUSINESS         TO IDSV8888-NOME-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
                // COB_CODE: MOVE 'Business Service'      TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service");
                // COB_CODE: SET  IDSV8888-FINE           TO TRUE
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
                // COB_CODE: SET IABV0006-CORRENTE-LANCIO TO TRUE
                ws.getIabv0006().getTipoLancioBus().setIabv0006CorrenteLancio();
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: IF IDSV0001-ESITO-OK
                //              END-IF
                //           ELSE
                //              END-IF
                //           END-IF
                if (ws.getIabv0007().getAreaIdsv0001().getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: ADD 1               TO CONT-BUS-SERV-ESITO-OK
                    ws.getIabv0007().setContBusServEsitoOk(Trunc.toInt(1 + ws.getIabv0007().getContBusServEsitoOk(), 9));
                    // COB_CODE: IF WK-SIMULAZIONE-INFR
                    //              PERFORM Y100-ROLLBACK-SAVEPOINT   THRU Y100-EX
                    //           END-IF
                    if (ws.getIabv0007().getWkSimulazione().isInfr()) {
                        // COB_CODE: PERFORM Y100-ROLLBACK-SAVEPOINT   THRU Y100-EX
                        y100RollbackSavepoint();
                    }
                    // COB_CODE: IF WK-ERRORE-NO
                    //              END-IF
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
                        w000CaricaLogErrore();
                        // COB_CODE: IF WARNING-TROVATO-YES
                        //              SET WARNING-TROVATO-NO         TO TRUE
                        //           END-IF
                        if (ws.getIabv0007().isFlagWarningTrovato()) {
                            // COB_CODE: ADD 1              TO CONT-BUS-SERV-WARNING
                            ws.getIabv0007().setContBusServWarning(Trunc.toInt(1 + ws.getIabv0007().getContBusServWarning(), 9));
                            // COB_CODE: SET WARNING-TROVATO-NO         TO TRUE
                            ws.getIabv0007().setFlagWarningTrovato(false);
                        }
                        // COB_CODE: IF WK-ERRORE-NO
                        //              END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: SET WK-ALLINEA-STATI-OK        TO TRUE
                            ws.getIabv0007().getFlagAllineaStati().setOk();
                            // COB_CODE: PERFORM B900-CTRL-STATI-SOSP THRU B900-EX
                            b900CtrlStatiSosp();
                            // COB_CODE: IF WK-ERRORE-NO
                            //              END-IF
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: IF ((BBT-COMMIT-FREQUENCY > 0) AND
                                //              (WK-COMMIT-FREQUENCY >=
                                //              BBT-COMMIT-FREQUENCY)) OR
                                //              IABV0006-COMMIT-YES
                                //              END-IF
                                //           ELSE
                                //              PERFORM Y300-SAVEPOINT THRU Y300-EX
                                //           END-IF
                                if (ws.getBtcBatchType().getBbtCommitFrequency().getBbtCommitFrequency() > 0 && ws.getIabv0007().getWkCommitFrequency() >= ws.getBtcBatchType().getBbtCommitFrequency().getBbtCommitFrequency() || ws.getIabv0006().getFlagCommit().isIabv0006CommitYes()) {
                                    // COB_CODE: SET IABV0006-COMMIT-NO   TO TRUE
                                    ws.getIabv0006().getFlagCommit().setIabv0006CommitNo();
                                    // COB_CODE: IF NOT WK-SIMULAZIONE-INFR
                                    //              END-IF
                                    //           END-IF
                                    if (!ws.getIabv0007().getWkSimulazione().isInfr()) {
                                        // COB_CODE: PERFORM Y000-COMMIT THRU Y000-EX
                                        y000Commit();
                                        // COB_CODE: IF (IABV0006-STD-TYPE-REC-NO   OR
                                        //               IABV0006-STD-TYPE-REC-YES) AND
                                        //               IDSV0001-ARCH-BATCH-DBG
                                        //                 TO WK-ID-JOB-ULT-COMMIT
                                        //           END-IF
                                        if ((ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecNo() || ws.getIabv0006().getFlagProcessType().isIabv0006StdTypeRecYes()) && ws.getIabv0007().getAreaIdsv0001().getAreaComune().getLivelloDebug().isIdsv0001ArchBatchDbg()) {
                                            // COB_CODE: MOVE WK-ID-JOB
                                            //             TO WK-ID-JOB-ULT-COMMIT
                                            ws.getIabv0007().setWkIdJobUltCommitFormatted(ws.getIabv0007().getWkIdJobFormatted());
                                        }
                                        // COB_CODE: IF WK-ERRORE-NO
                                        //                 THRU Y050-EX
                                        //           END-IF
                                        if (!ws.getIabv0007().isWkErrore()) {
                                            // COB_CODE: PERFORM Y050-CONTROLLO-COMMIT
                                            //              THRU Y050-EX
                                            y050ControlloCommit();
                                        }
                                    }
                                }
                                else {
                                    // COB_CODE: ADD 1 TO WK-COMMIT-FREQUENCY
                                    ws.getIabv0007().setWkCommitFrequency(1 + ws.getIabv0007().getWkCommitFrequency());
                                    // COB_CODE: PERFORM Y300-SAVEPOINT THRU Y300-EX
                                    y300Savepoint();
                                }
                            }
                        }
                    }
                }
                else {
                    // COB_CODE: IF IDSV0001-FORZ-RC-04-YES
                    //               ADD 1               TO CONT-BUS-SERV-ESITO-OK
                    //           ELSE
                    //               ADD 1               TO CONT-BUS-SERV-ESITO-KO
                    //           END-IF
                    if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getForzRc04().isIdsv0001ForzRc04Yes()) {
                        // COB_CODE: ADD 1               TO CONT-BUS-SERV-ESITO-OK
                        ws.getIabv0007().setContBusServEsitoOk(Trunc.toInt(1 + ws.getIabv0007().getContBusServEsitoOk(), 9));
                    }
                    else {
                        // COB_CODE: ADD 1               TO CONT-BUS-SERV-ESITO-KO
                        ws.getIabv0007().setContBusServEsitoKo(Trunc.toInt(1 + ws.getIabv0007().getContBusServEsitoKo(), 9));
                    }
                    // COB_CODE: SET  WK-PGM-BUS-SERVICE          TO TRUE
                    ws.getIabv0007().getFlagTipoPgm().setBusService();
                    // COB_CODE: MOVE WK-PGM-BUSINESS             TO WK-PGM-ERRORE
                    ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getWkPgmBusiness());
                    // COB_CODE: SET WK-BAT-COLLECTION-KEY-KO        TO TRUE
                    ws.getIabv0007().setWkBatCollectionKey(false);
                    // COB_CODE: SET WK-JOB-COLLECTION-KEY-KO        TO TRUE
                    ws.getIabv0007().setWkJobCollectionKey(false);
                    // COB_CODE: IF IABI0011-TRATTAMENTO-FULL
                    //              END-IF
                    //           ELSE
                    //              PERFORM Y100-ROLLBACK-SAVEPOINT THRU Y100-EX
                    //           END-IF
                    if (ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().isFull()) {
                        // COB_CODE: PERFORM Y200-ROLLBACK            THRU Y200-EX
                        y200Rollback();
                        // COB_CODE: SET WK-SKIP-BATCH-YES            TO TRUE
                        ws.getIabv0007().setFlagSkipBatch(true);
                        // COB_CODE: IF WK-FINE-ALIMENTAZIONE-NO
                        //              END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isFlagFineAlimentazione()) {
                            // COB_CODE: PERFORM N501-PRE-GUIDE-AD-HOC     THRU N501-EX
                            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1563, because the code is unreachable.
                            // COB_CODE: IF WK-ERRORE-NO
                            //              PERFORM N509-SWITCH-GUIDE      THRU N509-EX
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: SET  IDSV0003-CLOSE-CURSOR     TO TRUE
                                ws.getIabv0007().getIdsv0003().getOperazione().setIdsv0003CloseCursor();
                                // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE
                                ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
                                // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE
                                ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
                                // COB_CODE: PERFORM N509-SWITCH-GUIDE      THRU N509-EX
                                n509SwitchGuide();
                            }
                        }
                    }
                    else {
                        // COB_CODE: PERFORM Y100-ROLLBACK-SAVEPOINT THRU Y100-EX
                        y100RollbackSavepoint();
                    }
                    // COB_CODE: IF WK-ERRORE-NO
                    //              END-IF
                    //           END-IF
                    if (!ws.getIabv0007().isWkErrore()) {
                        // COB_CODE: PERFORM W000-CARICA-LOG-ERRORE THRU W000-EX
                        w000CaricaLogErrore();
                        // COB_CODE: IF IDSV0001-FORZ-RC-04-YES AND
                        //              WARNING-TROVATO-YES
                        //              SET WARNING-TROVATO-NO         TO TRUE
                        //           END-IF
                        if (ws.getIabv0007().getAreaIdsv0001().getAreaComune().getForzRc04().isIdsv0001ForzRc04Yes() && ws.getIabv0007().isFlagWarningTrovato()) {
                            // COB_CODE: ADD 1              TO CONT-BUS-SERV-WARNING
                            ws.getIabv0007().setContBusServWarning(Trunc.toInt(1 + ws.getIabv0007().getContBusServWarning(), 9));
                            // COB_CODE: SET WARNING-TROVATO-NO         TO TRUE
                            ws.getIabv0007().setFlagWarningTrovato(false);
                        }
                        // COB_CODE: IF WK-ERRORE-NO
                        //              END-IF
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: SET WK-ALLINEA-STATI-KO       TO TRUE
                            ws.getIabv0007().getFlagAllineaStati().setKo();
                            // COB_CODE: PERFORM B900-CTRL-STATI-SOSP  THRU B900-EX
                            b900CtrlStatiSosp();
                            // COB_CODE: IF WK-ERRORE-NO
                            //              END-IF
                            //           END-IF
                            if (!ws.getIabv0007().isWkErrore()) {
                                // COB_CODE: PERFORM Y000-COMMIT        THRU Y000-EX
                                y000Commit();
                                // COB_CODE: IF WK-ERRORE-NO
                                //                 THRU Y050-EX
                                //           END-IF
                                if (!ws.getIabv0007().isWkErrore()) {
                                    // COB_CODE: PERFORM Y050-CONTROLLO-COMMIT
                                    //              THRU Y050-EX
                                    y050ControlloCommit();
                                }
                                // COB_CODE: IF WK-ERRORE-NO
                                //              END-IF
                                //           END-IF
                                if (!ws.getIabv0007().isWkErrore()) {
                                    // COB_CODE: SET WK-PGM-ARCHITECTURE    TO TRUE
                                    ws.getIabv0007().getFlagTipoPgm().setArchitecture();
                                    // COB_CODE: IF WK-PGM-SERV-SECON-BUS NOT =
                                    //              SPACES AND LOW-VALUE AND HIGH-VALUE
                                    //                                     THRU L400-EX
                                    //           END-IF
                                    if (!Characters.EQ_SPACE.test(ws.getWkPgmServSeconBus()) && !Characters.EQ_LOW.test(ws.getWkPgmServSeconBusFormatted()) && !Characters.EQ_HIGH.test(ws.getWkPgmServSeconBusFormatted())) {
                                        // COB_CODE: PERFORM L400-TRATTA-SERV-SECOND-BUS
                                        //                                  THRU L400-EX
                                        l400TrattaServSecondBus();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF WK-LANCIA-ROTTURA-NO
            //              PERFORM L801-SOSPENDI-STATI   THRU L801-EX
            //           END-IF
            if (!ws.getIabv0007().isFlagLanciaRottura()) {
                // COB_CODE: ADD 1                         TO IND-STATI-SOSP
                ws.getIabv0007().setIndStatiSosp(Trunc.toShort(1 + ws.getIabv0007().getIndStatiSosp(), 2));
                // COB_CODE: PERFORM L801-SOSPENDI-STATI   THRU L801-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1615, because the code is unreachable.
            }
        }
    }

    /**Original name: L602-CALL-BUS-EOF<br>
	 * <pre>----------------------------------------------------------------*
	 *  CALL BUSINESS EOF
	 * ----------------------------------------------------------------*</pre>*/
    private void l602CallBusEof() {
        // COB_CODE: MOVE 'L602-CALL-BUS-EOF'          TO WK-LABEL.
        ws.getIabv0007().setWkLabel("L602-CALL-BUS-EOF");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: SET  IDSV8888-BUSINESS-DBG TO TRUE
            ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
            // COB_CODE: MOVE WK-PGM-BUSINESS       TO IDSV8888-NOME-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
            // COB_CODE: MOVE 'Business Service'    TO IDSV8888-DESC-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service");
            // COB_CODE: SET  IDSV8888-INIZIO       TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
            // COB_CODE: PERFORM ESEGUI-DISPLAY     THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
            ws.getIabv0007().getIdsv8888().getLivelloDebug().setStressTestDbg();
            // COB_CODE: SET  IDSV8888-SONDA-S8           TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setIdsv8888SondaS8();
            // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
            //                TO IDSV8888-MODALITA-ESECUTIVA
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
            // COB_CODE: MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setUserName(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getUserName());
            // COB_CODE: MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
            // COB_CODE: MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service LIV-1");
            // COB_CODE: SET  IDSV8888-INIZIO             TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
            // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: PERFORM L800-ESEGUI-CALL-BUS THRU L800-EX
            l800EseguiCallBus();
            // COB_CODE: SET  IDSV8888-STRESS-TEST-DBG    TO TRUE
            ws.getIabv0007().getIdsv8888().getLivelloDebug().setStressTestDbg();
            // COB_CODE: SET  IDSV8888-SONDA-S8           TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setIdsv8888SondaS8();
            // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
            //                TO IDSV8888-MODALITA-ESECUTIVA
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
            // COB_CODE: MOVE IDSV0001-USER-NAME          TO IDSV8888-USER-NAME
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setUserName(ws.getIabv0007().getAreaIdsv0001().getAreaComune().getUserName());
            // COB_CODE: MOVE WK-PGM-BUSINESS             TO IDSV8888-NOME-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
            // COB_CODE: MOVE 'Business Service LIV-1'    TO IDSV8888-DESC-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service LIV-1");
            // COB_CODE: SET  IDSV8888-FINE               TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
            // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: SET  IDSV8888-BUSINESS-DBG TO TRUE
            ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
            // COB_CODE: MOVE WK-PGM-BUSINESS       TO IDSV8888-NOME-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getIabv0007().getWkPgmBusiness());
            // COB_CODE: MOVE 'Business Service'    TO IDSV8888-DESC-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Business Service");
            // COB_CODE: SET  IDSV8888-FINE         TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
            // COB_CODE: PERFORM ESEGUI-DISPLAY     THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: IF IDSV0001-ESITO-OK
                //              PERFORM Y400-ESITO-OK-OTHER-SERV THRU Y400-EX
                //           ELSE
                //              PERFORM Y500-ESITO-KO-OTHER-SERV THRU Y500-EX
                //           END-IF
                if (ws.getIabv0007().getAreaIdsv0001().getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM Y400-ESITO-OK-OTHER-SERV THRU Y400-EX
                    y400EsitoOkOtherServ();
                }
                else {
                    // COB_CODE: PERFORM Y500-ESITO-KO-OTHER-SERV THRU Y500-EX
                    y500EsitoKoOtherServ();
                }
            }
        }
    }

    /**Original name: L400-TRATTA-SERV-SECOND-BUS<br>
	 * <pre>----------------------------------------------------------------*
	 *  TRATTA SERVIZIO SECONDARIO DI BUSINESS
	 * ----------------------------------------------------------------*</pre>*/
    private void l400TrattaServSecondBus() {
        // COB_CODE: MOVE 'L400-TRATTA-SERV-SECOND-BUS' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("L400-TRATTA-SERV-SECOND-BUS");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO    TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                      TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: PERFORM L901-PRE-SERV-SECOND-BUS             THRU L901-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1690, because the code is unreachable.
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: SET  IDSV8888-BUSINESS-DBG         TO TRUE
            ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
            // COB_CODE: MOVE WK-PGM-SERV-SECON-BUS         TO IDSV8888-NOME-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getWkPgmServSeconBus());
            // COB_CODE: MOVE 'Serv second di business'     TO IDSV8888-DESC-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv second di business");
            // COB_CODE: SET  IDSV8888-INIZIO               TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
            // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: PERFORM L900-CALL-SERV-SECOND-BUS         THRU L900-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1700, because the code is unreachable.
            // COB_CODE: SET  IDSV8888-BUSINESS-DBG         TO TRUE
            ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
            // COB_CODE: MOVE WK-PGM-SERV-SECON-BUS         TO IDSV8888-NOME-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getWkPgmServSeconBus());
            // COB_CODE: MOVE 'Serv second di business'     TO IDSV8888-DESC-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv second di business");
            // COB_CODE: SET  IDSV8888-FINE                 TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
            // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM L902-POST-SERV-SECOND-BUS      THRU L902-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1709, because the code is unreachable.
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              PERFORM Y400-ESITO-OK-OTHER-SERV THRU Y400-EX
                    //           ELSE
                    //              PERFORM Y500-ESITO-KO-OTHER-SERV THRU Y500-EX
                    //           END-IF
                    if (ws.getIabv0007().getAreaIdsv0001().getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: PERFORM Y400-ESITO-OK-OTHER-SERV THRU Y400-EX
                        y400EsitoOkOtherServ();
                    }
                    else {
                        // COB_CODE: PERFORM Y500-ESITO-KO-OTHER-SERV THRU Y500-EX
                        y500EsitoKoOtherServ();
                    }
                }
            }
        }
    }

    /**Original name: L450-TRATTA-SERV-SECOND-ROTT<br>
	 * <pre>----------------------------------------------------------------*
	 *  TRATTA SERVIZIO SECONDARIO DI ROTTURA
	 * ----------------------------------------------------------------*</pre>*/
    private void l450TrattaServSecondRott() {
        // COB_CODE: MOVE 'L450-TRATTA-SERV-SECOND-ROTT' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("L450-TRATTA-SERV-SECOND-ROTT");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO     TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                       TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: PERFORM L951-PRE-SERV-SECOND-ROTT   THRU L951-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1736, because the code is unreachable.
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
            ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
            // COB_CODE: MOVE WK-PGM-SERV-SECON-ROTT         TO IDSV8888-NOME-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getWkPgmServSeconRott());
            // COB_CODE: MOVE 'Serv second. di rottura'      TO IDSV8888-DESC-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv second. di rottura");
            // COB_CODE: SET  IDSV8888-INIZIO                TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
            // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: PERFORM L950-CALL-SERV-SECOND-ROTT  THRU L950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1746, because the code is unreachable.
            // COB_CODE: SET  IDSV8888-BUSINESS-DBG          TO TRUE
            ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
            // COB_CODE: MOVE WK-PGM-SERV-SECON-ROTT         TO IDSV8888-NOME-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getWkPgmServSeconRott());
            // COB_CODE: MOVE 'Serv second. di rottura'      TO IDSV8888-DESC-PGM
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Serv second. di rottura");
            // COB_CODE: SET  IDSV8888-FINE                  TO TRUE
            ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
            // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            eseguiDisplay();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM L952-POST-SERV-SECOND-ROTT  THRU L952-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1755, because the code is unreachable.
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              PERFORM Y400-ESITO-OK-OTHER-SERV   THRU Y400-EX
                    //           ELSE
                    //              PERFORM Y500-ESITO-KO-OTHER-SERV   THRU Y500-EX
                    //           END-IF
                    if (ws.getIabv0007().getAreaIdsv0001().getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: PERFORM Y400-ESITO-OK-OTHER-SERV   THRU Y400-EX
                        y400EsitoOkOtherServ();
                    }
                    else {
                        // COB_CODE: PERFORM Y500-ESITO-KO-OTHER-SERV   THRU Y500-EX
                        y500EsitoKoOtherServ();
                    }
                }
            }
        }
    }

    /**Original name: Q001-ELABORA-ROTTURA<br>
	 * <pre>----------------------------------------------------------------*
	 *  ELABORAZIONE IN FASE DI ROTTURA
	 * ----------------------------------------------------------------*</pre>*/
    private void q001ElaboraRottura() {
        // COB_CODE: MOVE 'Q001-ELABORA-ROTTURA'      TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Q001-ELABORA-ROTTURA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: SET WK-LANCIA-ROTTURA-YES         TO TRUE
        ws.getIabv0007().setFlagLanciaRottura(true);
        // COB_CODE: IF IABI0011-TRATTAMENTO-PARTIAL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF
        if (ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().isPartial()) {
            // COB_CODE: IF NOT WK-SIMULAZIONE-INFR
            //              PERFORM Y000-COMMIT      THRU Y000-EX
            //           END-IF
            if (!ws.getIabv0007().getWkSimulazione().isInfr()) {
                // COB_CODE: PERFORM Y000-COMMIT      THRU Y000-EX
                y000Commit();
            }
        }
        else if (!ws.getIabv0007().isWkJobCollectionKey()) {
            // COB_CODE: IF WK-JOB-COLLECTION-KEY-KO
            //              PERFORM Y200-ROLLBACK    THRU Y200-EX
            //           END-IF
            // COB_CODE: PERFORM Y200-ROLLBACK    THRU Y200-EX
            y200Rollback();
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: PERFORM Q002-LANCIA-ROTTURA      THRU Q002-EX
            q002LanciaRottura();
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: IF WK-LANCIA-ROTTURA-YES
                //              SET WK-LANCIA-ROTTURA-NO       TO TRUE
                //           END-IF
                if (ws.getIabv0007().isFlagLanciaRottura()) {
                    // COB_CODE: PERFORM Q503-CNTL-SERV-ROTTURA THRU Q503-EX
                    q503CntlServRottura();
                    // COB_CODE: SET WK-JOB-COLLECTION-KEY-OK   TO TRUE
                    ws.getIabv0007().setWkJobCollectionKey(true);
                    // COB_CODE: SET WK-LANCIA-ROTTURA-NO       TO TRUE
                    ws.getIabv0007().setFlagLanciaRottura(false);
                }
            }
        }
    }

    /**Original name: Q002-LANCIA-ROTTURA<br>
	 * <pre>----------------------------------------------------------------*
	 *  LANCIO DEL MODULO DI ROTTURA
	 * ----------------------------------------------------------------*</pre>*/
    private void q002LanciaRottura() {
        // COB_CODE: MOVE 'Q002-LANCIA-ROTTURA'       TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Q002-LANCIA-ROTTURA");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF GESTIONE-SUSPEND
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().getFlagGestioneLanciBus().isSuspend()) {
                // COB_CODE: PERFORM Y300-SAVEPOINT                    THRU Y300-EX
                y300Savepoint();
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF WK-FINE-ALIMENTAZIONE-NO
                    //              PERFORM L000-LANCIA-BUSINESS        THRU L000-EX
                    //           ELSE
                    //              END-IF
                    //           END-IF
                    if (!ws.getIabv0007().isFlagFineAlimentazione()) {
                        // COB_CODE: PERFORM L000-LANCIA-BUSINESS        THRU L000-EX
                        l000LanciaBusiness();
                    }
                    else if (Conditions.eq(ws.getBtcBatchType().getBbtDefContentTypeFormatted().substring((4) - 1), ws.getIabv0007().getUltimoLancioXCloseFile())) {
                        // COB_CODE: IF BBT-DEF-CONTENT-TYPE(4:) =
                        //              ULTIMO-LANCIO-X-CLOSE-FILE
                        //              END-IF
                        //           ELSE
                        //              PERFORM L000-LANCIA-BUSINESS     THRU L000-EX
                        //           END-IF
                        // COB_CODE: PERFORM L000-LANCIA-BUSINESS     THRU L000-EX
                        l000LanciaBusiness();
                        // COB_CODE: IF WK-ERRORE-NO
                        //              PERFORM L602-CALL-BUS-EOF     THRU L602-EX
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: SET IABV0006-ULTIMO-LANCIO    TO TRUE
                            ws.getIabv0006().getTipoLancioBus().setIabv0006UltimoLancio();
                            // COB_CODE: PERFORM L602-CALL-BUS-EOF     THRU L602-EX
                            l602CallBusEof();
                        }
                    }
                    else {
                        // COB_CODE: SET IABV0006-ULTIMO-LANCIO       TO TRUE
                        ws.getIabv0006().getTipoLancioBus().setIabv0006UltimoLancio();
                        // COB_CODE: PERFORM L000-LANCIA-BUSINESS     THRU L000-EX
                        l000LanciaBusiness();
                    }
                }
            }
            // COB_CODE: IF WK-ERRORE-NO
            //              END-IF
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM Q501-PRE-SERV-ROTTURA             THRU Q501-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1846, because the code is unreachable.
                // COB_CODE: IF WK-ERRORE-NO
                //              END-IF
                //           END-IF
                if (!ws.getIabv0007().isWkErrore()) {
                    // COB_CODE: IF WK-LANCIA-ROTTURA-YES
                    //              END-IF
                    //           END-IF
                    if (ws.getIabv0007().isFlagLanciaRottura()) {
                        // COB_CODE: SET  IDSV8888-BUSINESS-DBG   TO TRUE
                        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
                        // COB_CODE: MOVE WK-PGM-SERV-ROTTURA     TO IDSV8888-NOME-PGM
                        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getWkPgmServRottura());
                        // COB_CODE: MOVE 'Servizio di rottura'   TO IDSV8888-DESC-PGM
                        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Servizio di rottura");
                        // COB_CODE: SET  IDSV8888-INIZIO         TO TRUE
                        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setInizio();
                        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                        eseguiDisplay();
                        // COB_CODE: PERFORM Q500-CALL-SERV-ROTTURA    THRU Q500-EX
                        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1856, because the code is unreachable.
                        // COB_CODE: SET  IDSV8888-BUSINESS-DBG   TO TRUE
                        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888BusinessDbg();
                        // COB_CODE: MOVE WK-PGM-SERV-ROTTURA     TO IDSV8888-NOME-PGM
                        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(ws.getWkPgmServRottura());
                        // COB_CODE: MOVE 'Servizio di rottura'   TO IDSV8888-DESC-PGM
                        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Servizio di rottura");
                        // COB_CODE: SET  IDSV8888-FINE           TO TRUE
                        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFine();
                        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                        eseguiDisplay();
                        // COB_CODE: IF WK-ERRORE-NO
                        //              PERFORM Q502-POST-SERV-ROTTURA THRU Q502-EX
                        //           END-IF
                        if (!ws.getIabv0007().isWkErrore()) {
                            // COB_CODE: SET WK-LANCIA-ROTTURA-YES      TO TRUE
                            ws.getIabv0007().setFlagLanciaRottura(true);
                            // COB_CODE: PERFORM Q502-POST-SERV-ROTTURA THRU Q502-EX
                            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1866, because the code is unreachable.
                        }
                    }
                }
            }
        }
    }

    /**Original name: R000-ESTRAI-DATI-ELABORAZIONE<br>
	 * <pre>****************************************************************</pre>*/
    private void r000EstraiDatiElaborazione() {
        // COB_CODE: MOVE 'R000-ESTRAI-DATI-ELABORAZIONE' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("R000-ESTRAI-DATI-ELABORAZIONE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO      TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                        TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: PERFORM Y300-SAVEPOINT           THRU Y300-EX
        y300Savepoint();
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: MOVE 0                           TO WS-LUNG-EFF-BLOB
            ws.getIabv0007().setWsLungEffBlob(0);
            // COB_CODE: IF NOT IABV0006-GUIDE-AD-HOC AND
            //              NOT IABV0006-BY-BOOKING
            //              END-IF
            //           END-IF
            if (!ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() && !ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking()) {
                // COB_CODE: IF BJS-DATA =  SPACES OR LOW-VALUES OR HIGH-VALUES
                //              PERFORM E620-ESTRAI-RECORD    THRU E620-EX
                //           ELSE
                //              MOVE BJS-DATA                TO IABV0003-BLOB-DATA
                //           END-IF
                if (Characters.EQ_SPACE.test(ws.getBtcJobSchedule().getBjsData()) || Characters.EQ_LOW.test(ws.getBtcJobSchedule().getBjsData(), BtcJobSchedule.Len.BJS_DATA) || Characters.EQ_HIGH.test(ws.getBtcJobSchedule().getBjsData(), BtcJobSchedule.Len.BJS_DATA)) {
                    // COB_CODE: SET IABV0006-STD-TYPE-REC-YES TO TRUE
                    ws.getIabv0006().getFlagProcessType().setIabv0006StdTypeRecYes();
                    // COB_CODE: PERFORM E620-ESTRAI-RECORD    THRU E620-EX
                    e620EstraiRecord();
                }
                else {
                    // COB_CODE: SET IABV0006-STD-TYPE-REC-NO TO TRUE
                    ws.getIabv0006().getFlagProcessType().setIabv0006StdTypeRecNo();
                    // COB_CODE: INITIALIZE IABV0003
                    initIabv0003();
                    // COB_CODE: MOVE BJS-DATA-LEN            TO WS-LUNG-EFF-BLOB
                    ws.getIabv0007().setWsLungEffBlob(TruncAbs.toInt(ws.getBtcJobSchedule().getBjsDataLen(), 9));
                    // COB_CODE: MOVE BJS-DATA                TO IABV0003-BLOB-DATA
                    ws.getIabv0007().getIabv0003().setIabv0003BlobDataFormatted(ws.getBtcJobSchedule().getBjsDataFormatted());
                }
            }
        }
    }

    /**Original name: U000-UPD-JOB<br>
	 * <pre>----------------------------------------------------------------*
	 *  UPDATE DEI JOB
	 * ----------------------------------------------------------------*</pre>*/
    private void u000UpdJob() {
        ConcatUtil concatUtil = null;
        Iabs0060 iabs0060 = null;
        // COB_CODE: MOVE 'U000-UPD-JOB'              TO WK-LABEL.
        ws.getIabv0007().setWkLabel("U000-UPD-JOB");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO  TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                    TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        //
        // COB_CODE: MOVE 'U000-UPD-JOB'            TO WK-LABEL.
        ws.getIabv0007().setWkLabel("U000-UPD-JOB");
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-UPDATE            TO TRUE.
        ws.getIabv0007().getIdsv0003().getOperazione().setIdsi0011Update();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: IF IABV0006-GUIDE-AD-HOC OR
        //              IABV0006-BY-BOOKING
        //              END-IF
        //           ELSE
        //              END-CALL
        //           END-IF
        if (ws.getIabv0006().getFlagProcessType().isIabv0006GuideAdHoc() || ws.getIabv0006().getFlagProcessType().isIabv0006ByBooking()) {
            // COB_CODE: PERFORM N501-PRE-GUIDE-AD-HOC  THRU N501-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IABP0012:line=1928, because the code is unreachable.
            // COB_CODE: IF WK-ERRORE-NO
            //              PERFORM N509-SWITCH-GUIDE   THRU N509-EX
            //           END-IF
            if (!ws.getIabv0007().isWkErrore()) {
                // COB_CODE: PERFORM N509-SWITCH-GUIDE   THRU N509-EX
                n509SwitchGuide();
            }
        }
        else {
            // COB_CODE: MOVE BTC-ID-BATCH           TO BJS-ID-BATCH
            ws.getBtcJobSchedule().setBjsIdBatch(ws.getBtcBatch().getBtcIdBatch());
            // COB_CODE: MOVE PGM-IABS0060           TO WK-PGM-ERRORE
            ws.getIabv0007().setWkPgmErrore(ws.getIabv0007().getIabv0004().getPgmIabs0060());
            // COB_CODE: CALL PGM-IABS0060           USING IDSV0003
            //                                             IABV0002
            //                                             BTC-JOB-SCHEDULE
            //           ON EXCEPTION
            //              PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
            //           END-CALL
            try {
                iabs0060 = Iabs0060.getInstance();
                iabs0060.run(ws.getIabv0007().getIdsv0003(), ws.getIabv0007().getIabv0002(), ws.getBtcJobSchedule());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE CHIAMATA MODULO '
                //                   DELIMITED BY SIZE
                //                   PGM-IABS0060
                //                   DELIMITED BY SIZE
                //                   ' - '
                //                    DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE CHIAMATA MODULO ", ws.getIabv0007().getIabv0004().getPgmIabs0060Formatted(), " - ");
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: PERFORM Z400-ERRORE-PRE-DB THRU Z400-EX
                z400ErrorePreDb();
            }
        }
        // COB_CODE: IF WK-ERRORE-NO
        //              END-IF
        //           END-IF.
        if (!ws.getIabv0007().isWkErrore()) {
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE:            EVALUATE TRUE
                //                         WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                //                            CONTINUE
                //                         WHEN IDSV0003-NOT-FOUND
                //           *--->         CAMPO $ NON TROVATO
                //                            END-IF
                //                          WHEN IDSV0003-NEGATIVI
                //           *--->          ERRORE DI ACCESSO AL DB
                //                                                 THRU W070-EX
                //                      END-EVALUATE
                if (ws.getIabv0007().getIdsv0003().getSqlcode().isSuccessfulSql()) {
                //-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: CONTINUE
                //continue
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                    //--->         CAMPO $ NON TROVATO
                    // COB_CODE: IF WK-NOT-FOUND-NOT-ALLOWED
                    //                                  THRU W070-EX
                    //           END-IF
                    if (!ws.getIabv0007().isFlagFoundNotFound()) {
                        // COB_CODE: INITIALIZE WK-LOG-ERRORE
                        initWkLogErrore();
                        // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                        ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                        // COB_CODE: STRING WK-PGM-ERRORE
                        //                  ' - '
                        //                  'SQLCODE : '
                        //                  WK-SQLCODE ' - '
                        //                  'ID-BATCH : '
                        //                  WK-ID-BATCH ' - '
                        //                  DELIMITED BY SIZE INTO
                        //                  WK-LOR-DESC-ERRORE-ESTESA
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, new String[] {ws.getIabv0007().getWkPgmErroreFormatted(), " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString(), " - ", "ID-BATCH : ", ws.getIabv0007().getWkIdBatchAsString(), " - "});
                        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                        // COB_CODE: MOVE ERRORE-FATALE TO WK-LOR-ID-GRAVITA-ERRORE
                        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                        // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                        //                               THRU W070-EX
                        w070CaricaLogErrBatExec();
                    }
                }
                else if (ws.getIabv0007().getIdsv0003().getSqlcode().isIdsv0003Negativi()) {
                    //--->          ERRORE DI ACCESSO AL DB
                    // COB_CODE: INITIALIZE WK-LOG-ERRORE
                    initWkLogErrore();
                    // COB_CODE: MOVE IDSV0003-SQLCODE TO WK-SQLCODE
                    ws.getIabv0007().setWkSqlcode(ws.getIabv0007().getIdsv0003().getSqlcode().getSqlcode());
                    // COB_CODE: STRING WK-PGM-ERRORE
                    //                  ' - '
                    //                  'SQLCODE : '
                    //                  WK-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  WK-LOR-DESC-ERRORE-ESTESA
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, ws.getIabv0007().getWkPgmErroreFormatted(), " - ", "SQLCODE : ", ws.getIabv0007().getWkSqlcodeAsString());
                    ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                    // COB_CODE: MOVE ERRORE-FATALE  TO WK-LOR-ID-GRAVITA-ERRORE
                    ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                    // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                    //                               THRU W070-EX
                    w070CaricaLogErrBatExec();
                }
            }
            else if (!ws.getIabv0007().isFlagUpdateForRestore()) {
                // COB_CODE:            IF WK-UPDATE-FOR-RESTORE-NO
                //           *-->       GESTIRE ERRORE DISPATCHER
                //                                             THRU W070-EX
                //                      END-IF
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: INITIALIZE WK-LOG-ERRORE
                initWkLogErrore();
                // COB_CODE: STRING 'ERRORE MODULO '
                //                   WK-PGM-ERRORE
                //                   ' - '
                //                   'RC : '
                //                   IDSV0003-RETURN-CODE
                //                   DELIMITED BY SIZE INTO
                //                   WK-LOR-DESC-ERRORE-ESTESA
                //           END-STRING
                concatUtil = ConcatUtil.buildString(WkLogErrore.Len.DESC_ERRORE_ESTESA, "ERRORE MODULO ", ws.getIabv0007().getWkPgmErroreFormatted(), " - ", "RC : ", ws.getIabv0007().getIdsv0003().getReturnCode().getReturnCodeFormatted());
                ws.getIabv0007().getWkLogErrore().setDescErroreEstesa(concatUtil.replaceInString(ws.getIabv0007().getWkLogErrore().getDescErroreEstesaFormatted()));
                // COB_CODE: MOVE ERRORE-FATALE       TO WK-LOR-ID-GRAVITA-ERRORE
                ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(ws.getIabv0007().getWkLivelloGravita().getErroreFatale());
                // COB_CODE: PERFORM W070-CARICA-LOG-ERR-BAT-EXEC
                //                               THRU W070-EX
                w070CaricaLogErrBatExec();
            }
        }
    }

    /**Original name: Z000-DISP-OCCORR-GUIDE<br>
	 * <pre>****************************************************************</pre>*/
    private void z000DispOccorrGuide() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'Z000-CONTA-OCCORRENZE-GUIDE' TO WK-LABEL.
        ws.getIabv0007().setWkLabel("Z000-CONTA-OCCORRENZE-GUIDE");
        // COB_CODE: SET   IDSV8888-DEBUG-ESASPERATO      TO TRUE
        ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888DebugEsasperato();
        // COB_CODE: MOVE WK-LABEL                        TO IDSV8888-AREA-DISPLAY
        ws.getIabv0007().getIdsv8888().getAreaDisplay().setAreaDisplay(ws.getIabv0007().getWkLabel());
        // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX.
        eseguiDisplay();
        // COB_CODE: IF IABI0011-CONTATORE IS NUMERIC AND
        //                    IABI0011-CONTATORE GREATER ZERO
        //               END-IF
        //            END-IF.
        if (Functions.isNumber(ws.getIabv0007().getIabi0011Area().getContatoreFormatted()) && Characters.GT_ZERO.test(ws.getIabv0007().getIabi0011Area().getContatoreFormatted())) {
            // COB_CODE: DIVIDE CONT-TAB-GUIDE-LETTE
            //                BY IABI0011-CONTATORE
            //                  GIVING RESULT REMAINDER RESTO
            ws.getIabv0007().getIabv0010().setResult(ws.getIabv0007().getContTabGuideLette() / ws.getIabv0007().getIabi0011Area().getContatore());
            ws.getIabv0007().getIabv0010().setResto(Trunc.toInt(ws.getIabv0007().getContTabGuideLette() % ws.getIabv0007().getIabi0011Area().getContatore(), 5));
            // COB_CODE: IF RESTO = 0 OR IDSV0003-FETCH-FIRST  OR
            //                         WK-FINE-ALIMENTAZIONE-YES
            //             PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
            //           END-IF
            if (ws.getIabv0007().getIabv0010().getResto() == 0 || ws.getIabv0007().getIdsv0003().getOperazione().isFetchFirst() || ws.getIabv0007().isFlagFineAlimentazione()) {
                // COB_CODE: SET  IDSV8888-ARCH-BATCH-DBG TO TRUE
                ws.getIabv0007().getIdsv8888().getLivelloDebug().setIdsv8888ArchBatchDbg();
                // COB_CODE: MOVE 'Architettura Batch'
                //                        TO IDSV8888-DESC-PGM
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("Architettura Batch");
                // COB_CODE: STRING WK-PGM '/'
                //                 'Rec. Letti: '
                //                 CONT-TAB-GUIDE-LETTE
                //           DELIMITED BY SIZE
                //               INTO IDSV8888-NOME-PGM
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv8888StrPerformanceDbg.Len.NOME_PGM, ws.getWkPgmFormatted(), "/", "Rec. Letti: ", ws.getIabv0007().getContTabGuideLetteAsString());
                ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm(concatUtil.replaceInString(ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().getIdsv8888NomePgmFormatted()));
                // COB_CODE: PERFORM ESEGUI-DISPLAY THRU ESEGUI-DISPLAY-EX
                eseguiDisplay();
            }
        }
    }

    /**Original name: CALL-SEQGUIDE<br>
	 * <pre>*****************************************************************
	 * --          GESTIONE FILE SEQUENZIALI
	 * *****************************************************************
	 *  - Copy da inserire X Guida da Sequenziale (SEQGUIDE) : IABPSQG1
	 * *****************************************************************
	 * *************************************************************
	 *  STATEMETS X GESTIONE GUIDA DA SEQUENZIALE
	 *  N.B. - DA UTILIZZARE CON LA COPY IABVSQG1
	 * *************************************************************</pre>*/
    private void callSeqguide() {
        // COB_CODE: PERFORM INIZIO-SEQGUIDE     THRU INIZIO-SEQGUIDE-EX.
        inizioSeqguide();
        // COB_CODE: PERFORM ELABORA-SEQGUIDE    THRU ELABORA-SEQGUIDE-EX
        elaboraSeqguide();
        // COB_CODE: PERFORM FINE-SEQGUIDE       THRU FINE-SEQGUIDE-EX.
        fineSeqguide();
    }

    /**Original name: INIZIO-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void inizioSeqguide() {
        // COB_CODE: MOVE NOME-SEQGUIDE           TO   IDSV0003-NOME-TABELLA.
        ws.getIabv0007().getIdsv0003().getCampiEsito().setNomeTabella(ws.getIabvsqg1().getNomeSeqguide());
        // COB_CODE: MOVE ZEROES                  TO   IDSV0003-NUM-RIGHE-LETTE.
        ws.getIabv0007().getIdsv0003().getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC   TO TRUE
        ws.getIabv0007().getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL  TO TRUE.
        ws.getIabv0007().getIdsv0003().getSqlcode().setSuccessfulSql();
    }

    /**Original name: CHECK-RETURN-CODE-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void checkReturnCodeSeqguide() {
        // COB_CODE: MOVE FS-SEQGUIDE          TO FILE-STATUS.
        ws.getIabcsq99().getFileStatus().setFileStatus(ws.getIabvsqg1().getFsSeqguide());
        // COB_CODE: EVALUATE TRUE
        //               WHEN FILE-STATUS-OK
        //                    CONTINUE
        //               WHEN FILE-STATUS-END-OF-FILE
        //                  END-IF
        //               WHEN OTHER
        //                                            THRU COMPONI-MSG-SEQGUIDE-EX
        //           END-EVALUATE.
        switch (ws.getIabcsq99().getFileStatus().getFileStatus()) {

            case FileStatus1.OK:// COB_CODE: CONTINUE
            //continue
                break;

            case FileStatus1.END_OF_FILE:// COB_CODE: IF IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT
                //              SET IDSV0003-NOT-FOUND     TO TRUE
                //           ELSE
                //                                     THRU COMPONI-MSG-SEQGUIDE-EX
                //           END-IF
                if (ws.getIabv0007().getIdsv0003().getOperazione().isSelect() || ws.getIabv0007().getIdsv0003().getOperazione().isFetchFirst() || ws.getIabv0007().getIdsv0003().getOperazione().isFetchNext()) {
                    // COB_CODE: SET IDSV0003-NOT-FOUND     TO TRUE
                    ws.getIabv0007().getIdsv0003().getSqlcode().setNotFound();
                }
                else {
                    // COB_CODE: SET IDSV0003-GENERIC-ERROR TO TRUE
                    ws.getIabv0007().getIdsv0003().getReturnCode().setGenericError();
                    // COB_CODE: PERFORM COMPONI-MSG-SEQGUIDE
                    //                                  THRU COMPONI-MSG-SEQGUIDE-EX
                    componiMsgSeqguide();
                }
                break;

            default:// COB_CODE: SET IDSV0003-GENERIC-ERROR    TO TRUE
                ws.getIabv0007().getIdsv0003().getReturnCode().setGenericError();
                // COB_CODE: PERFORM COMPONI-MSG-SEQGUIDE
                //                                     THRU COMPONI-MSG-SEQGUIDE-EX
                componiMsgSeqguide();
                break;
        }
    }

    /**Original name: COMPONI-MSG-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void componiMsgSeqguide() {
        // COB_CODE: MOVE IDSV0003-NOME-TABELLA     TO MSG-NOME-FILE.
        ws.getIabcsq99().getMsgErrFile().setNomeFile(ws.getIabv0007().getIdsv0003().getCampiEsito().getNomeTabella());
        // COB_CODE: MOVE FILE-STATUS               TO MSG-RC
        ws.getIabcsq99().getMsgErrFile().setRc(ws.getIabcsq99().getFileStatus().getFileStatus());
        // COB_CODE: MOVE MSG-ERR-FILE              TO IDSV0003-DESCRIZ-ERR-DB2.
        ws.getIabv0007().getIdsv0003().getCampiEsito().setDescrizErrDb2(ws.getIabcsq99().getMsgErrFile().getMsgErrFileFormatted());
    }

    /**Original name: ELABORA-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void elaboraSeqguide() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                                        THRU UNIQUE-READ-SEQGUIDE-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                                             THRU OPEN-FILE-SEQGUIDE-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                                             THRU CLOSE-FILE-SEQGUIDE-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                                             THRU READ-FIRST-SEQGUIDE-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                                             THRU READ-NEXT-SEQGUIDE-EX
        //              WHEN IDSV0003-UPDATE
        //                 CONTINUE
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-EVALUATE.
        if (ws.getIabv0007().getIdsv0003().getOperazione().isSelect()) {
            // COB_CODE: PERFORM UNIQUE-READ-SEQGUIDE
            //                                  THRU UNIQUE-READ-SEQGUIDE-EX
            uniqueReadSeqguide();
        }
        else if (ws.getIabv0007().getIdsv0003().getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM OPEN-FILE-SEQGUIDE
            //                                       THRU OPEN-FILE-SEQGUIDE-EX
            openFileSeqguide();
        }
        else if (ws.getIabv0007().getIdsv0003().getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM CLOSE-FILE-SEQGUIDE
            //                                       THRU CLOSE-FILE-SEQGUIDE-EX
            closeFileSeqguide();
        }
        else if (ws.getIabv0007().getIdsv0003().getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM READ-FIRST-SEQGUIDE
            //                                       THRU READ-FIRST-SEQGUIDE-EX
            readFirstSeqguide();
        }
        else if (ws.getIabv0007().getIdsv0003().getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM READ-NEXT-SEQGUIDE
            //                                       THRU READ-NEXT-SEQGUIDE-EX
            readNextSeqguide();
        }
        else if (ws.getIabv0007().getIdsv0003().getOperazione().isUpdate()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            ws.getIabv0007().getIdsv0003().getReturnCode().setInvalidOper();
        }
    }

    /**Original name: UNIQUE-READ-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void uniqueReadSeqguide() {
        // COB_CODE: PERFORM OPEN-FILE-SEQGUIDE        THRU OPEN-FILE-SEQGUIDE-EX.
        openFileSeqguide();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM READ-NEXT-SEQGUIDE     THRU READ-NEXT-SEQGUIDE-EX
            readNextSeqguide();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              PERFORM CLOSE-FILE-SEQGUIDE THRU CLOSE-FILE-SEQGUIDE-EX
            //           END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: PERFORM CLOSE-FILE-SEQGUIDE THRU CLOSE-FILE-SEQGUIDE-EX
                closeFileSeqguide();
            }
        }
    }

    /**Original name: OPEN-FILE-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void openFileSeqguide() {
        // COB_CODE: SET MSG-OPEN              TO TRUE.
        ws.getIabcsq99().getMsgErrFile().getOperazioni().setOpen();
        // COB_CODE: OPEN INPUT SEQGUIDE.
        seqguideDAO.open(OpenMode.READ, "Lrgm0380");
        ws.getIabvsqg1().setFsSeqguide(seqguideDAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-SEQGUIDE
        //                                     THRU CHECK-RETURN-CODE-SEQGUIDE-EX.
        checkReturnCodeSeqguide();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              SET SEQGUIDE-APERTO    TO TRUE
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: SET SEQGUIDE-APERTO    TO TRUE
            ws.getIabvsqg1().getStatoSeqguide().setAperto();
        }
    }

    /**Original name: CLOSE-FILE-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void closeFileSeqguide() {
        // COB_CODE: IF SEQGUIDE-APERTO
        //              END-IF
        //           END-IF.
        if (ws.getIabvsqg1().getStatoSeqguide().isAperto()) {
            // COB_CODE: SET MSG-CLOSE                    TO TRUE
            ws.getIabcsq99().getMsgErrFile().getOperazioni().setClose();
            // COB_CODE: CLOSE SEQGUIDE
            seqguideDAO.close();
            ws.getIabvsqg1().setFsSeqguide(seqguideDAO.getFileStatus().getStatusCodeFormatted());
            // COB_CODE: PERFORM CHECK-RETURN-CODE-SEQGUIDE
            //                                   THRU CHECK-RETURN-CODE-SEQGUIDE-EX
            checkReturnCodeSeqguide();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
            //              SET SEQGUIDE-CHIUSO           TO TRUE
            //           END-IF
            if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: SET SEQGUIDE-CHIUSO           TO TRUE
                ws.getIabvsqg1().getStatoSeqguide().setChiuso();
            }
        }
    }

    /**Original name: READ-FIRST-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void readFirstSeqguide() {
        // COB_CODE: PERFORM OPEN-FILE-SEQGUIDE      THRU OPEN-FILE-SEQGUIDE-EX.
        openFileSeqguide();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM READ-NEXT-SEQGUIDE   THRU READ-NEXT-SEQGUIDE-EX
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM READ-NEXT-SEQGUIDE   THRU READ-NEXT-SEQGUIDE-EX
            readNextSeqguide();
        }
    }

    /**Original name: READ-NEXT-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void readNextSeqguide() {
        // COB_CODE: SET MSG-READ                    TO TRUE.
        ws.getIabcsq99().getMsgErrFile().getOperazioni().setRead();
        // COB_CODE: READ SEQGUIDE.
        seqguideTo = seqguideDAO.read(seqguideTo);
        ws.getIabvsqg1().setFsSeqguide(seqguideDAO.getFileStatus().getStatusCodeFormatted());
        // COB_CODE: PERFORM CHECK-RETURN-CODE-SEQGUIDE
        //                                     THRU CHECK-RETURN-CODE-SEQGUIDE-EX.
        checkReturnCodeSeqguide();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            if (ws.getIabv0007().getIdsv0003().getSqlcode().isNotFound()) {
                // COB_CODE: PERFORM CLOSE-FILE-SEQGUIDE THRU CLOSE-FILE-SEQGUIDE-EX
                closeFileSeqguide();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
                //              SET IDSV0003-NOT-FOUND   TO TRUE
                //           END-IF
                if (ws.getIabv0007().getIdsv0003().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE: SET IDSV0003-NOT-FOUND   TO TRUE
                    ws.getIabv0007().getIdsv0003().getSqlcode().setNotFound();
                }
            }
        }
    }

    /**Original name: FINE-SEQGUIDE<br>
	 * <pre>*****************************************************************</pre>*/
    private void fineSeqguide() {
    // COB_CODE: CONTINUE.
    //continue
    }

    @Override
    public DdCard[] getDefaultDdCards() {
        return new DdCard[] {new DdCard("PBTCEXEC", 80), new DdCard("REPORT01", 500), new DdCard("SEQGUIDE", 2500)};
    }

    public void initIxIndici() {
        ws.setIxInd(((short)0));
    }

    public void initCustomCounters() {
        ws.getIabv0006().getCustomCounters().getCounterStd().setCounterStd(Types.SPACE_CHAR);
        ws.getIabv0006().getCustomCounters().getGuideRowsRead().setGuideRowsRead(Types.SPACE_CHAR);
        ws.getIabv0006().getCustomCounters().setLimCustomCountMax(((short)0));
        ws.getIabv0006().getCustomCounters().setMaxEleCustomCount(((short)0));
        for (int idx0 = 1; idx0 <= Iabv0006CustomCounters.ELE_ERRORI_MAXOCCURS; idx0++) {
            ws.getIabv0006().getCustomCounters().getEleErrori(idx0).getDescType().setDescType(Types.SPACE_CHAR);
            ws.getIabv0006().getCustomCounters().getEleErrori(idx0).setCustomCountDesc("");
            ws.getIabv0006().getCustomCounters().getEleErrori(idx0).setIabv0006CustomCountFormatted("000000000");
        }
    }

    public void initWkAppoDataJob() {
        ws.getWkAppoDataJob().setEleMaxFormatted("000");
        ws.getWkAppoDataJob().getBlobDataObj().fill(new WkAppoBlobData().initWkAppoBlobData());
    }

    public void initWkApGarFnd() {
        ws.getLrgc0041().setWkApIdGarFormatted("000000000");
        ws.getLrgc0041().setWkApPrestMatur(new AfDecimal(0, 15, 3));
        ws.getLrgc0041().setWkApCntrvalTot(new AfDecimal(0, 15, 3));
        ws.getLrgc0041().setWkApFndEleMaxFormatted("000");
        for (int idx0 = 1; idx0 <= Lrgc0041.WK_AP_FND_TAB_MAXOCCURS; idx0++) {
            ws.getLrgc0041().getWkApFndTab(idx0).setCodFnd("");
            ws.getLrgc0041().getWkApFndTab(idx0).setCntrval(new AfDecimal(0, 15, 3));
        }
    }

    public void initTabellaStatiBatch() {
        for (int idx0 = 1; idx0 <= Iabv0007.TABELLA_STATI_BATCH_ELEMENTS_MAXOCCURS; idx0++) {
            ws.getIabv0007().getTabellaStatiBatchElements(idx0).setCodBatchState(Types.SPACE_CHAR);
            ws.getIabv0007().getTabellaStatiBatchElements(idx0).setDes("");
            ws.getIabv0007().getTabellaStatiBatchElements(idx0).setFlagToExecute(Types.SPACE_CHAR);
        }
    }

    public void initTabellaStatiJob() {
        for (int idx0 = 1; idx0 <= Iabv0007.TABELLA_STATI_JOB_ELEMENTS_MAXOCCURS; idx0++) {
            ws.getIabv0007().getTabellaStatiJobElements(idx0).setCodElabState(Types.SPACE_CHAR);
            ws.getIabv0007().getTabellaStatiJobElements(idx0).setDes("");
            ws.getIabv0007().getTabellaStatiJobElements(idx0).setFlagToExecute(Types.SPACE_CHAR);
        }
    }

    public void initIabv0002() {
        ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState01(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState02(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState03(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState04(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState05(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState06(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState07(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState08(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState09(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState10(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().setIabv0002FlagAlwaysExe(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002TipoOrderBy().setIabv0002TipoOrderBy("");
        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setIdBatch(0);
        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setIdOggDa(0);
        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setIdOggA(0);
        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setTpOgg("");
        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setVersioning(0);
        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setBlobDataTypeRec("");
        ws.getIabv0007().getIabv0002().getIabv0009GestGuideService().setBlobDataRec("");
    }

    public void initWkStatiSospesiStr() {
        for (int idx0 = 1; idx0 <= Iabv0007.WK_STATI_SOSPESI_ELE_MAXOCCURS; idx0++) {
            ws.getIabv0007().getWkStatiSospesiEle(idx0).setWkStatiSospPk(0);
        }
    }

    public void initWkComodo() {
        ws.getIabv0007().setComodoCodCompAniaFormatted("000000000");
        ws.getIabv0007().setComodoProgProtocolFormatted("000000000");
    }

    public void initValoriCampiLogErroreReport() {
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setLivGravitaFormatted("0");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setOggBusiness("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setTpOggBusiness("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIbOggPoli("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIbOggAdes("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIdPoli("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setIdAdes("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDtEffBusiness("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setDescErrore("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setCodMainBatch("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setServizioBe("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setLabelErr("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setOperTabella("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setNomeTabella("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setStatusTabella("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setTmstErrore("");
        ws.getIabv0007().getIabv0010().getValoriCampiLogErroreReport().setKeyTabella("");
    }

    public void initIabi0011() {
        ws.getIabv0007().getIabi0011Area().setProtocol("");
        ws.getIabv0007().getIabi0011Area().setProgProtocolFormatted("000000000");
        ws.getIabv0007().getIabi0011Area().setTipoBatchFormatted("000000000");
        ws.getIabv0007().getIabi0011Area().setCodiceCompagniaAniaFormatted("00000");
        ws.getIabv0007().getIabi0011Area().setLingua("");
        ws.getIabv0007().getIabi0011Area().setPaese("");
        ws.getIabv0007().getIabi0011Area().setUserName("");
        ws.getIabv0007().getIabi0011Area().setGruppoAutorizzanteFormatted("0000000000");
        ws.getIabv0007().getIabi0011Area().setCanaleVenditaFormatted("00000");
        ws.getIabv0007().getIabi0011Area().setPuntoVendita("");
        ws.getIabv0007().getIabi0011Area().setKeyBusiness1("");
        ws.getIabv0007().getIabi0011Area().setKeyBusiness2("");
        ws.getIabv0007().getIabi0011Area().setKeyBusiness3("");
        ws.getIabv0007().getIabi0011Area().setKeyBusiness4("");
        ws.getIabv0007().getIabi0011Area().setKeyBusiness5("");
        ws.getIabv0007().getIabi0011Area().setMacrofunzionalita("");
        ws.getIabv0007().getIabi0011Area().setTipoMovimentoFormatted("00000");
        ws.getIabv0007().getIabi0011Area().setDataEffettoFormatted("00000000");
        ws.getIabv0007().getIabi0011Area().setTsCompetenza(0);
        ws.getIabv0007().getIabi0011Area().getFlagStorEsecuz().setFlagStorEsecuz(Types.SPACE_CHAR);
        ws.getIabv0007().getIabi0011Area().getFlagSimulazione().setFlagSimulazione(Types.SPACE_CHAR);
        ws.getIabv0007().getIabi0011Area().getTrattamentoCommit().setTrattamentoCommit("");
        ws.getIabv0007().getIabi0011Area().getGestioneRipartenza().setGestioneRipartenza("");
        ws.getIabv0007().getIabi0011Area().getTipoOrderBy().setTipoOrderBy("");
        ws.getIabv0007().getIabi0011Area().getLivelloGravitaLog().setFlagModCalcPreP(Types.SPACE_CHAR);
        ws.getIabv0007().getIabi0011Area().getLivelloDebug().setIdsi0011LivelloDebugFormatted("0");
        ws.getIabv0007().getIabi0011Area().getTemporaryTable().setTemporaryTable("");
        ws.getIabv0007().getIabi0011Area().getLengthDataGates().setLengthDataGates("");
        ws.getIabv0007().getIabi0011Area().getAppendDataGates().setAppendDataGates(Types.SPACE_CHAR);
        for (int idx0 = 1; idx0 <= Iabi0011Area.ADDRESS_TYPE_MAXOCCURS; idx0++) {
            ws.getIabv0007().getIabi0011Area().setAddressType(idx0, Types.SPACE_CHAR);
        }
        ws.getIabv0007().getIabi0011Area().getForzRc04().setForzRc04(Types.SPACE_CHAR);
        ws.getIabv0007().getIabi0011Area().setContatoreFormatted("00000");
        ws.getIabv0007().getIabi0011Area().getAccessoBatch().setAccessoBatch("");
        ws.getIabv0007().getIabi0011Area().setBufferWhereCond("");
    }

    public void initWAreaPerChiamateWsmq() {
        ws.getIabv0007().getwAreaPerChiamateWsmq().setQmName("");
        ws.getIabv0007().getwAreaPerChiamateWsmq().setHconn(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setHconnJcall(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setqHandleJcall(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setOptionsJcall(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setCompletionCodeJcall(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setOpenCodeJcall(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setConReasonJcall(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setReasonJcall(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setHconnJret(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setqHandleJret(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setOptionsJret(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setCompletionCodeJret(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setOpenCodeJret(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setConReasonJret(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setReasonJret(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setBufferLengthJcall(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setBufferLengthJret(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setDataLength(0);
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTipoOperazione("");
        ws.getIabv0007().getwAreaPerChiamateWsmq().setNomeCoda("");
        ws.getIabv0007().getwAreaPerChiamateWsmq().setTpUtilizzoApi("");
        ws.getIabv0007().getwAreaPerChiamateWsmq().setMessaggioInterno("");
    }

    public void initIdsv0003() {
        ws.getIabv0007().getIdsv0003().getModalitaEsecutiva().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIabv0007().getIdsv0003().setCodiceCompagniaAnia(0);
        ws.getIabv0007().getIdsv0003().setCodMainBatch("");
        ws.getIabv0007().getIdsv0003().setTipoMovimentoFormatted("00000");
        ws.getIabv0007().getIdsv0003().setSessione("");
        ws.getIabv0007().getIdsv0003().setUserName("");
        ws.getIabv0007().getIdsv0003().setDataInizioEffetto(0);
        ws.getIabv0007().getIdsv0003().setDataFineEffetto(0);
        ws.getIabv0007().getIdsv0003().setDataCompetenza(0);
        ws.getIabv0007().getIdsv0003().setDataCompAggStor(0);
        ws.getIabv0007().getIdsv0003().getTrattamentoStoricita().setTrattamentoStoricita("");
        ws.getIabv0007().getIdsv0003().getFormatoDataDb().setFormatoDataDb("");
        ws.getIabv0007().getIdsv0003().setIdMoviAnnullatoFormatted("000000000");
        ws.getIabv0007().getIdsv0003().getIdentitaChiamante().setIdentitaChiamante("");
        ws.getIabv0007().getIdsv0003().getTipologiaOperazione().setTipologiaOperazione("");
        ws.getIabv0007().getIdsv0003().getLivelloOperazione().setLivelloOperazione("");
        ws.getIabv0007().getIdsv0003().getOperazione().setOperazione("");
        ws.getIabv0007().getIdsv0003().getFlagCodaTs().setFlagCodaTs(Types.SPACE_CHAR);
        ws.getIabv0007().getIdsv0003().setBufferWhereCond("");
        ws.getIabv0007().getIdsv0003().getReturnCode().setReturnCode("");
        ws.getIabv0007().getIdsv0003().getSqlcode().setSqlcode(0);
        ws.getIabv0007().getIdsv0003().getCampiEsito().setDescrizErrDb2("");
        ws.getIabv0007().getIdsv0003().getCampiEsito().setCodServizioBe("");
        ws.getIabv0007().getIdsv0003().getCampiEsito().setNomeTabella("");
        ws.getIabv0007().getIdsv0003().getCampiEsito().setKeyTabella("");
        ws.getIabv0007().getIdsv0003().getCampiEsito().setIdsv0003NumRigheLetteFormatted("00");
    }

    public void initParamInfrAppl() {
        ws.getIabv0007().getParamInfrAppl().setD09CodCompAnia(0);
        ws.getIabv0007().getParamInfrAppl().setD09Ambiente("");
        ws.getIabv0007().getParamInfrAppl().setD09Piattaforma("");
        ws.getIabv0007().getParamInfrAppl().setD09TpComCobolJava("");
        ws.getIabv0007().getParamInfrAppl().setD09MqTpUtilizzoApi("");
        ws.getIabv0007().getParamInfrAppl().setD09MqQueueManager("");
        ws.getIabv0007().getParamInfrAppl().setD09MqCodaPut("");
        ws.getIabv0007().getParamInfrAppl().setD09MqCodaGet("");
        ws.getIabv0007().getParamInfrAppl().setD09MqOpzPersistenza(Types.SPACE_CHAR);
        ws.getIabv0007().getParamInfrAppl().setD09MqOpzWait(Types.SPACE_CHAR);
        ws.getIabv0007().getParamInfrAppl().setD09MqOpzSyncpoint(Types.SPACE_CHAR);
        ws.getIabv0007().getParamInfrAppl().setD09MqAttesaRisposta(Types.SPACE_CHAR);
        ws.getIabv0007().getParamInfrAppl().getD09MqTempoAttesa1().setD09MqTempoAttesa1(0);
        ws.getIabv0007().getParamInfrAppl().getD09MqTempoAttesa2().setD09MqTempoAttesa2(0);
        ws.getIabv0007().getParamInfrAppl().getD09MqTempoExpiry().setD09MqTempoExpiry(0);
        ws.getIabv0007().getParamInfrAppl().setD09CsocketIpAddress("");
        ws.getIabv0007().getParamInfrAppl().getD09CsocketPortNum().setD09CsocketPortNum(0);
        ws.getIabv0007().getParamInfrAppl().setD09FlCompressoreC(Types.SPACE_CHAR);
    }

    public void initWkLogErrore() {
        ws.getIabv0007().getWkLogErrore().setIdLogErrore(0);
        ws.getIabv0007().getWkLogErrore().setIdGravitaErrore(0);
        ws.getIabv0007().getWkLogErrore().setDescErroreEstesa("");
        ws.getIabv0007().getWkLogErrore().setCodMainBatch("");
        ws.getIabv0007().getWkLogErrore().setCodServizioBe("");
        ws.getIabv0007().getWkLogErrore().setLabelErr("");
        ws.getIabv0007().getWkLogErrore().setOperTabella("");
        ws.getIabv0007().getWkLogErrore().setNomeTabella("");
        ws.getIabv0007().getWkLogErrore().setStatusTabella("");
        ws.getIabv0007().getWkLogErrore().setKeyTabella("");
    }

    public void initIjccmq04VarAmbiente() {
        ws.getIabv0007().getIjccmq04VarAmbiente().setCodCompAniaFormatted("00000");
        ws.getIabv0007().getIjccmq04VarAmbiente().setAmbiente("");
        ws.getIabv0007().getIjccmq04VarAmbiente().setPiattaforma("");
        ws.getIabv0007().getIjccmq04VarAmbiente().setCobToJava("");
        ws.getIabv0007().getIjccmq04VarAmbiente().setTpUtilizzoApi("");
        ws.getIabv0007().getIjccmq04VarAmbiente().setQueueManager("");
        ws.getIabv0007().getIjccmq04VarAmbiente().setCodaPut("");
        ws.getIabv0007().getIjccmq04VarAmbiente().setCodaGet("");
        ws.getIabv0007().getIjccmq04VarAmbiente().setOpzPersistenza(Types.SPACE_CHAR);
        ws.getIabv0007().getIjccmq04VarAmbiente().setOpzWait(Types.SPACE_CHAR);
        ws.getIabv0007().getIjccmq04VarAmbiente().setOpzSyncpoint(Types.SPACE_CHAR);
        ws.getIabv0007().getIjccmq04VarAmbiente().setAttesaRisposta(Types.SPACE_CHAR);
        ws.getIabv0007().getIjccmq04VarAmbiente().setTempoAttesa1(0);
        ws.getIabv0007().getIjccmq04VarAmbiente().setTempoAttesa2(0);
        ws.getIabv0007().getIjccmq04VarAmbiente().setTempoExpiry(0);
        ws.getIabv0007().getIjccmq04VarAmbiente().setCsocketIpAddress("");
        ws.getIabv0007().getIjccmq04VarAmbiente().setCsocketPortNumFormatted("00000");
        ws.getIabv0007().getIjccmq04VarAmbiente().setFlCompressoreC(Types.SPACE_CHAR);
    }

    public void initIabv0002StateGlobal() {
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState01(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState02(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState03(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState04(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState05(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState06(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState07(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState08(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState09(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState10(Types.SPACE_CHAR);
    }

    public void initIdsv8888StrPerformanceDbg() {
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setFase("");
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setNomePgm("");
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setDescPgm("");
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setTimestampFld("");
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setStato("");
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setModalitaEsecutiva(Types.SPACE_CHAR);
        ws.getIabv0007().getIdsv8888().getStrPerformanceDbg().setUserName("");
    }

    public void initIabv0002State() {
        ws.getIabv0007().getIabv0002().setIabv0002StateCurrent(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState01(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState02(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState03(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState04(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState05(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState06(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState07(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState08(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState09(Types.SPACE_CHAR);
        ws.getIabv0007().getIabv0002().getIabv0002StateGlobal().setState10(Types.SPACE_CHAR);
    }

    public void initIabv0003DataJob() {
        ws.getIabv0007().getIabv0003().setIabv0003EleMax(((short)0));
        ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setIabv0003BlobDataArray("");
        ws.getIabv0007().getIabv0003().setIabv0003LungEffBlobFormatted("000000000");
    }

    public void initIabv0003BlobDataEle() {
        ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setDataTypeRec(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), "");
        ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setDataRec(ws.getIabv0007().getIabv0003().getIabv0003EleMax(), "");
    }

    public void initBtcExeMessage() {
        ws.getBtcExeMessage().setIdBatch(0);
        ws.getBtcExeMessage().setIdJob(0);
        ws.getBtcExeMessage().setIdExecution(0);
        ws.getBtcExeMessage().setIdMessage(0);
        ws.getBtcExeMessage().setCodMessage(0);
        ws.getBtcExeMessage().setMessageLen(((short)0));
        ws.getBtcExeMessage().setMessage("");
    }

    public void initReport() {
        ws.getIabv0006().getReport().setOggBusiness("");
        ws.getIabv0006().getReport().setTpOggBusiness("");
        ws.getIabv0006().getReport().setIbOggPoli("");
        ws.getIabv0006().getReport().setIbOggAdes("");
        ws.getIabv0006().getReport().setIdPoli("");
        ws.getIabv0006().getReport().setIdAdes("");
        ws.getIabv0006().getReport().setDtEffBusiness("");
    }

    public void initIabv0003() {
        ws.getIabv0007().getIabv0003().setIabv0003IbOgg("");
        ws.getIabv0007().getIabv0003().setIabv0003CodMacrofunct("");
        ws.getIabv0007().getIabv0003().setIabv0003TpMoviBusiness(0);
        ws.getIabv0007().getIabv0003().setIabv0003EleMax(((short)0));
        ws.getIabv0007().getIabv0003().getIabv0003BlobDataArray().setIabv0003BlobDataArray("");
        ws.getIabv0007().getIabv0003().setIabv0003LungEffBlobFormatted("000000000");
        ws.getIabv0007().getIabv0003().setIabv0003AliasStrDato("");
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int PARAM_REC = 80;
        public static final int REPORT01_REC = 500;
        public static final int SEQGUIDE_REC = 2500;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
