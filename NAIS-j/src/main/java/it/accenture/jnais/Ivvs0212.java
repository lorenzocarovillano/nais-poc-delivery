package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.CtrlAutOperDao;
import it.accenture.jnais.copy.CtrlAutOper;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.AreaIoIvvs0212;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvs0212Data;
import it.accenture.jnais.ws.occurs.Ivvv0212TabParam;
import it.accenture.jnais.ws.redefines.CaoProgCond;
import it.accenture.jnais.ws.redefines.CaoTpMovi;
import it.accenture.jnais.ws.redefines.UnzipTab;
import it.accenture.jnais.ws.UnzipStructure;
import static com.bphx.ctu.af.lang.AfSystem.rTrim;

/**Original name: IVVS0212<br>
 * <pre>****************************************************************
 *                                                                *
 *                    IDENTIFICATION DIVISION                     *
 *                                                                *
 * ****************************************************************
 * AUTHOR.        ACCENTURE.
 * DATE-WRITTEN.  21/11/2007.
 * ****************************************************************
 *                                                                *
 *     NOME :           IVVS0212                                  *
 *     TIPO :                                                     *
 *     DESCRIZIONE :    RICERCA AUTONOMIA OPERATIVA               *
 *                                                                *
 *     AREE DI PASSAGGIO DATI                                     *
 *                                                                *
 *     DATI DI INPUT/OUTPUT : IVVV0212                            *
 *                                                                *
 * ****************************************************************
 *                       LOG MODIFICHE                            *
 * ****************************************************************
 *                                                                *
 *  CODICE MODIF.            AUTORE          DATA                 *
 *  ---------------  **   -----------   **   --/--/----           *
 *                                                                *
 *  DESCRZIONE: XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX           *
 *              XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX           *
 *                                                                *
 * ****************************************************************
 * ****************************************************************
 *                                                                *
 *                    ENVIRONMENT  DIVISION                       *
 *                                                                *
 * ****************************************************************
 *     NULL-IND     Carattere usato per indicare campi NULL;</pre>*/
public class Ivvs0212 extends Program {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private CtrlAutOperDao ctrlAutOperDao = new CtrlAutOperDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ivvs0212Data ws = new Ivvs0212Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: AREA-IO-IVVS0212
    private AreaIoIvvs0212 areaIoIvvs0212;

    //==== METHODS ====
    /**Original name: MAIN_SUBROUTINE<br>*/
    public long execute(Idsv0003 idsv0003, AreaIoIvvs0212 areaIoIvvs0212) {
        this.idsv0003 = idsv0003;
        this.areaIoIvvs0212 = areaIoIvvs0212;
        principale();
        principaleFine();
        return 0;
    }

    public static Ivvs0212 getInstance() {
        return ((Ivvs0212)Programs.getInstance(Ivvs0212.class));
    }

    /**Original name: 1000-PRINCIPALE<br>
	 * <pre>****************************************************************
	 *                                                                *
	 *                    1000-PRINCIPALE                             *
	 *                                                                *
	 * ****************************************************************</pre>*/
    private void principale() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET  NO-TROVATO                      TO TRUE
        ws.getSwSwitch().setSwTrovato(false);
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA  TO WS-COMPAGNIA
        ws.getWsVariabili().setCompagnia(TruncAbs.toInt(idsv0003.getCodiceCompagniaAnia(), 5));
        // COB_CODE: MOVE IDSV0003-TIPO-MOVIMENTO         TO WS-MOVIMENTO
        ws.getWsVariabili().setMovimentoFormatted(idsv0003.getTipoMovimentoFormatted());
        // COB_CODE: SET  LEGGI-ELEMENTI                  TO TRUE
        ws.getSwSwitch().getSwFineElementi().setLeggiElementi();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC          TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: PERFORM 1100-RICERCA-AUT-OPER
        //              THRU 1100-RICERCA-AUT-OPER-EX
        //           VARYING IX-TAB-CAO FROM 1 BY 1
        //             UNTIL IX-TAB-CAO > 6
        //                OR IDSV0003-SQL-ERROR
        //                OR FINE-ELEMENTI
        ws.getIndici().setIxTabCao(((short)1));
        while (!(ws.getIndici().getIxTabCao() > 6 || idsv0003.getReturnCode().isIdsv0003SqlError() || ws.getSwSwitch().getSwFineElementi().isFineElementi())) {
            ricercaAutOper();
            ws.getIndici().setIxTabCao(Trunc.toShort(ws.getIndici().getIxTabCao() + 1, 4));
        }
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //               END-STRING
        //            END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE WK-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'ERRORE ESTRAZIONE DATI TABELLA CTRL-AUT-OPER '
            //                  '- CURSORE CAO-' WS-NUM-ACCESSO
            //                  '(' DESCRIZ-ERR-DB2 ')'
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, new String[] {"ERRORE ESTRAZIONE DATI TABELLA CTRL-AUT-OPER ", "- CURSORE CAO-", ws.getWsVariabili().getNumAccessoAsString(), "(", ws.getDescrizErrDb2Formatted(), ")"});
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: 1000-PRINCIPALE-FINE<br>*/
    private void principaleFine() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: 1100-RICERCA-AUT-OPER<br>
	 * <pre>*****************************************************************
	 *                                                                 *
	 *               RICERCA AUTONOMIA OPERATIVA                       *
	 *                                                                 *
	 * *****************************************************************</pre>*/
    private void ricercaAutOper() {
        // COB_CODE: INITIALIZE CTRL-AUT-OPER
        initCtrlAutOper();
        // COB_CODE: MOVE IX-TAB-CAO                   TO WS-NUM-ACCESSO
        ws.getWsVariabili().setNumAccesso(TruncAbs.toShort(ws.getIndici().getIxTabCao(), 2));
        // COB_CODE: MOVE WS-COMPAGNIA                 TO CAO-COD-COMPAGNIA-ANIA
        ws.getCtrlAutOper().setCaoCodCompagniaAnia(ws.getWsVariabili().getCompagnia());
        // COB_CODE: MOVE WS-MOVIMENTO                 TO CAO-TP-MOVI
        ws.getCtrlAutOper().getCaoTpMovi().setCaoTpMovi(ws.getWsVariabili().getMovimento());
        // COB_CODE: MOVE IVVV0212-KEY-AUT-OPER1       TO CAO-KEY-AUT-OPER1
        ws.getCtrlAutOper().setCaoKeyAutOper1(areaIoIvvs0212.getKeyAutOper1());
        // COB_CODE: MOVE IVVV0212-KEY-AUT-OPER2       TO CAO-KEY-AUT-OPER2
        ws.getCtrlAutOper().setCaoKeyAutOper2(areaIoIvvs0212.getKeyAutOper2());
        // COB_CODE: MOVE IVVV0212-KEY-AUT-OPER3       TO CAO-KEY-AUT-OPER3
        ws.getCtrlAutOper().setCaoKeyAutOper3(areaIoIvvs0212.getKeyAutOper3());
        // COB_CODE: MOVE IVVV0212-KEY-AUT-OPER4       TO CAO-KEY-AUT-OPER4
        ws.getCtrlAutOper().setCaoKeyAutOper4(areaIoIvvs0212.getKeyAutOper4());
        // COB_CODE: MOVE IVVV0212-KEY-AUT-OPER5       TO CAO-KEY-AUT-OPER5
        ws.getCtrlAutOper().setCaoKeyAutOper5(areaIoIvvs0212.getKeyAutOper5());
        // COB_CODE: PERFORM S1130-LENGTH-VCHAR-GER    THRU EX-S1130
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IVVS0212.cbl:line=392, because the code is unreachable.
        // COB_CODE: PERFORM S1100-OPEN-CURSOR         THRU EX-S1100
        s1100OpenCursor();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM S1110-FETCH-NEXT       THRU EX-S1110
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1110-FETCH-NEXT       THRU EX-S1110
            s1110FetchNext();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //                   OR IDSV0003-SQL-ERROR
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S2000-VALORIZZA-OUTPUT
            //              THRU EX-S2000
            //             UNTIL FINE-ELEMENTI
            //                OR IDSV0003-SQL-ERROR
            while (!(ws.getSwSwitch().getSwFineElementi().isFineElementi() || idsv0003.getReturnCode().isIdsv0003SqlError())) {
                s2000ValorizzaOutput();
            }
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           ELSE
            //              SET IDSV0003-SQL-ERROR       TO TRUE
            //           END-IF
            // COB_CODE: PERFORM S1120-CLOSE-CURSOR   THRU EX-S1120
            s1120CloseCursor();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND    TO TRUE
            //           ELSE
            //              SET IDSV0003-SQL-ERROR    TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND    TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR    TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
        else {
            // COB_CODE: SET IDSV0003-SQL-ERROR       TO TRUE
            idsv0003.getReturnCode().setSqlError();
        }
    }

    /**Original name: S1100-OPEN-CURSOR<br>*/
    private void s1100OpenCursor() {
        // COB_CODE: EVALUATE WS-NUM-ACCESSO
        //               WHEN 1
        //                      THRU OPEN-CURSORE-1-EX
        //               WHEN 2
        //                      THRU OPEN-CURSORE-2-EX
        //               WHEN 3
        //                      THRU OPEN-CURSORE-3-EX
        //               WHEN 4
        //                      THRU OPEN-CURSORE-4-EX
        //               WHEN 5
        //                      THRU OPEN-CURSORE-5-EX
        //               WHEN 6
        //                      THRU OPEN-CURSORE-6-EX
        //               WHEN OTHER
        //                   CONTINUE
        //           END-EVALUATE.
        switch (ws.getWsVariabili().getNumAccesso()) {

            case ((short)1):// COB_CODE: PERFORM OPEN-CURSORE-1
                //              THRU OPEN-CURSORE-1-EX
                openCursore1();
                break;

            case ((short)2):// COB_CODE: PERFORM OPEN-CURSORE-2
                //              THRU OPEN-CURSORE-2-EX
                openCursore2();
                break;

            case ((short)3):// COB_CODE: PERFORM OPEN-CURSORE-3
                //              THRU OPEN-CURSORE-3-EX
                openCursore3();
                break;

            case ((short)4):// COB_CODE: PERFORM OPEN-CURSORE-4
                //              THRU OPEN-CURSORE-4-EX
                openCursore4();
                break;

            case ((short)5):// COB_CODE: PERFORM OPEN-CURSORE-5
                //              THRU OPEN-CURSORE-5-EX
                openCursore5();
                break;

            case ((short)6):// COB_CODE: PERFORM OPEN-CURSORE-6
                //              THRU OPEN-CURSORE-6-EX
                openCursore6();
                break;

            default:// COB_CODE: CONTINUE
            //continue
                break;
        }
        // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE.
        idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
    }

    /**Original name: S1110-FETCH-NEXT<br>*/
    private void s1110FetchNext() {
        // COB_CODE: EVALUATE WS-NUM-ACCESSO
        //               WHEN 1
        //                      THRU CAO-1-FETCH-NEXT-EX
        //               WHEN 2
        //                      THRU CAO-2-FETCH-NEXT-EX
        //               WHEN 3
        //                      THRU CAO-3-FETCH-NEXT-EX
        //               WHEN 4
        //                      THRU CAO-4-FETCH-NEXT-EX
        //               WHEN 5
        //                      THRU CAO-5-FETCH-NEXT-EX
        //               WHEN 6
        //                      THRU CAO-6-FETCH-NEXT-EX
        //               WHEN OTHER
        //                   CONTINUE
        //           END-EVALUATE.
        switch (ws.getWsVariabili().getNumAccesso()) {

            case ((short)1):// COB_CODE: PERFORM CAO-1-FETCH-NEXT
                //              THRU CAO-1-FETCH-NEXT-EX
                cao1FetchNext();
                break;

            case ((short)2):// COB_CODE: PERFORM CAO-2-FETCH-NEXT
                //              THRU CAO-2-FETCH-NEXT-EX
                cao2FetchNext();
                break;

            case ((short)3):// COB_CODE: PERFORM CAO-3-FETCH-NEXT
                //              THRU CAO-3-FETCH-NEXT-EX
                cao3FetchNext();
                break;

            case ((short)4):// COB_CODE: PERFORM CAO-4-FETCH-NEXT
                //              THRU CAO-4-FETCH-NEXT-EX
                cao4FetchNext();
                break;

            case ((short)5):// COB_CODE: PERFORM CAO-5-FETCH-NEXT
                //              THRU CAO-5-FETCH-NEXT-EX
                cao5FetchNext();
                break;

            case ((short)6):// COB_CODE: PERFORM CAO-6-FETCH-NEXT
                //              THRU CAO-6-FETCH-NEXT-EX
                cao6FetchNext();
                break;

            default:// COB_CODE: CONTINUE
            //continue
                break;
        }
        // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE.
        idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
    }

    /**Original name: S1120-CLOSE-CURSOR<br>*/
    private void s1120CloseCursor() {
        // COB_CODE: EVALUATE WS-NUM-ACCESSO
        //               WHEN 1
        //                      THRU CAO-1-CLOSE-CURSOR-EX
        //               WHEN 2
        //                      THRU CAO-2-CLOSE-CURSOR-EX
        //               WHEN 3
        //                      THRU CAO-3-CLOSE-CURSOR-EX
        //               WHEN 4
        //                      THRU CAO-4-CLOSE-CURSOR-EX
        //               WHEN 5
        //                      THRU CAO-5-CLOSE-CURSOR-EX
        //               WHEN 6
        //                      THRU CAO-6-CLOSE-CURSOR-EX
        //               WHEN OTHER
        //                   CONTINUE
        //           END-EVALUATE.
        switch (ws.getWsVariabili().getNumAccesso()) {

            case ((short)1):// COB_CODE: PERFORM CAO-1-CLOSE-CURSOR
                //              THRU CAO-1-CLOSE-CURSOR-EX
                cao1CloseCursor();
                break;

            case ((short)2):// COB_CODE: PERFORM CAO-2-CLOSE-CURSOR
                //              THRU CAO-2-CLOSE-CURSOR-EX
                cao2CloseCursor();
                break;

            case ((short)3):// COB_CODE: PERFORM CAO-3-CLOSE-CURSOR
                //              THRU CAO-3-CLOSE-CURSOR-EX
                cao3CloseCursor();
                break;

            case ((short)4):// COB_CODE: PERFORM CAO-4-CLOSE-CURSOR
                //              THRU CAO-4-CLOSE-CURSOR-EX
                cao4CloseCursor();
                break;

            case ((short)5):// COB_CODE: PERFORM CAO-5-CLOSE-CURSOR
                //              THRU CAO-5-CLOSE-CURSOR-EX
                cao5CloseCursor();
                break;

            case ((short)6):// COB_CODE: PERFORM CAO-6-CLOSE-CURSOR
                //              THRU CAO-6-CLOSE-CURSOR-EX
                cao6CloseCursor();
                break;

            default:// COB_CODE: CONTINUE
            //continue
                break;
        }
        // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE.
        idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
    }

    /**Original name: S2000-VALORIZZA-OUTPUT<br>*/
    private void s2000ValorizzaOutput() {
        // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        z100SetColonneNull();
        // COB_CODE: ADD 1  TO IX-AUT-OPER
        ws.getIndici().setIxAutOper(Trunc.toShort(1 + ws.getIndici().getIxAutOper(), 4));
        // COB_CODE: MOVE IX-AUT-OPER
        //              TO IVVV0212-ELE-CTRL-AUT-OPER-MAX
        areaIoIvvs0212.setEleCtrlAutOperMax(ws.getIndici().getIxAutOper());
        // COB_CODE: MOVE CAO-COD-ERRORE
        //              TO IVVV0212-COD-ERRORE(IX-AUT-OPER)
        areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).setCodErrore(ws.getCtrlAutOper().getCaoCodErrore());
        // COB_CODE: MOVE CAO-COD-LIV-AUT
        //              TO IVVV0212-COD-LIV-AUT(IX-AUT-OPER)
        areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).setCodLivAut(ws.getCtrlAutOper().getCaoCodLivAut());
        // COB_CODE: MOVE CAO-TP-MOT-DEROGA
        //              TO IVVV0212-TP-MOT-DEROGA(IX-AUT-OPER)
        areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).setTpMotDeroga(ws.getCtrlAutOper().getCaoTpMotDeroga());
        // COB_CODE: MOVE CAO-MODULO-VERIFICA
        //              TO IVVV0212-MOD-VERIFICA(IX-AUT-OPER)
        areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).setModVerifica(ws.getCtrlAutOper().getCaoModuloVerifica());
        // COB_CODE: MOVE CAO-COD-COND
        //              TO IVVV0212-CODICE-CONDIZIONE(IX-AUT-OPER)
        areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).setCodiceCondizione(ws.getCtrlAutOper().getCaoCodCond());
        // COB_CODE: MOVE CAO-PROG-COND
        //              TO IVVV0212-PROGRESS-CONDITION(IX-AUT-OPER)
        areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).setProgressCondition(ws.getCtrlAutOper().getCaoProgCond().getCaoProgCond());
        // COB_CODE: MOVE CAO-RISULT-COND
        //              TO IVVV0212-RISULTATO-CONDIZIONE(IX-AUT-OPER)
        areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).setRisultatoCondizione(ws.getCtrlAutOper().getCaoRisultCond());
        // COB_CODE: ADD 1 TO IX-PARAM
        ws.getIndici().setIxParam(Trunc.toShort(1 + ws.getIndici().getIxParam(), 4));
        // COB_CODE: PERFORM E501-PREPARA-UNZIP-PARAM    THRU E501-EX
        e501PreparaUnzipParam();
        // COB_CODE: PERFORM U999-UNZIP-STRING           THRU U999-EX
        u999UnzipString();
        // COB_CODE: PERFORM E502-CARICA-PARAM-UNZIPPED  THRU E502-EX.
        e502CaricaParamUnzipped();
        // COB_CODE: MOVE IX-PARAM
        //              TO IVVV0212-ELE-PARAM-MAX(IX-AUT-OPER)
        areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).setEleParamMax(ws.getIndici().getIxParam());
        // COB_CODE: PERFORM S1110-FETCH-NEXT THRU EX-S1110
        s1110FetchNext();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              SET LEGGI-ELEMENTI             TO TRUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: SET LEGGI-ELEMENTI             TO TRUE
            ws.getSwSwitch().getSwFineElementi().setLeggiElementi();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET FINE-ELEMENTI  TO TRUE
            //           ELSE
            //             SET IDSV0003-SQL-ERROR       TO TRUE
            //           END-IF
            // COB_CODE: SET FINE-ELEMENTI  TO TRUE
            ws.getSwSwitch().getSwFineElementi().setFineElementi();
        }
        else {
            // COB_CODE: SET IDSV0003-SQL-ERROR       TO TRUE
            idsv0003.getReturnCode().setSqlError();
        }
    }

    /**Original name: E501-PREPARA-UNZIP-PARAM<br>
	 * <pre>----------------------------------------------------------------*
	 *    PREPARA STRINGA DA UNZIPPARE
	 * ----------------------------------------------------------------*
	 *      INITIALIZE UNZIP-STRUCTURE</pre>*/
    private void e501PreparaUnzipParam() {
        // COB_CODE: INITIALIZE              UNZIP-LENGTH-STR-MAX
        //                                   UNZIP-LENGTH-FIELD
        //                                   UNZIP-STRING-ZIPPED
        //                                   UNZIP-IDENTIFICATORE
        //                                   UNZIP-ELE-VARIABILI-MAX
        //                                   UNZIP-COD-VARIABILE(1).
        ws.getUnzipStructure().setUnzipLengthStrMaxFormatted("0000");
        ws.getUnzipStructure().setUnzipLengthFieldFormatted("00");
        ws.getUnzipStructure().setUnzipStringZipped("");
        ws.getUnzipStructure().setUnzipIdentificatore(Types.SPACE_CHAR);
        ws.getUnzipStructure().setUnzipEleVariabiliMax(((short)0));
        ws.getUnzipStructure().getUnzipTab().setCodVariabile(1, "");
        // COB_CODE: MOVE UNZIP-TAB       TO  UNZIP-RESTO-TAB.
        ws.getUnzipStructure().getUnzipTab().setRestoTab(ws.getUnzipStructure().getUnzipTab().getUnzipTabFormatted());
        // COB_CODE: MOVE LENGTH OF CAO-PARAM-AUT-OPER TO UNZIP-LENGTH-STR-MAX
        ws.getUnzipStructure().setUnzipLengthStrMax(((short)CtrlAutOper.Len.CAO_PARAM_AUT_OPER));
        // COB_CODE: MOVE LENGTH OF IVVV0212-COD-PARAM(IX-AUT-OPER, IX-PARAM)
        //                                              TO UNZIP-LENGTH-FIELD
        ws.getUnzipStructure().setUnzipLengthField(Trunc.toShort(Ivvv0212TabParam.Len.COD_PARAM, 2));
        // COB_CODE: MOVE CAO-PARAM-AUT-OPER            TO UNZIP-STRING-ZIPPED
        ws.getUnzipStructure().setUnzipStringZipped(ws.getCtrlAutOper().getCaoParamAutOper());
        // COB_CODE: MOVE ','                           TO UNZIP-IDENTIFICATORE.
        ws.getUnzipStructure().setUnzipIdentificatoreFormatted(",");
    }

    /**Original name: U999-UNZIP-STRING<br>
	 * <pre>----------------------------------------------------------------*
	 *    ESTRAZIONE NOME VARIABILE DALLA STRINGA GLOBALVARLIST
	 * ----------------------------------------------------------------*
	 * --> IND-START-VAR  = INDICA IL PRIMO CHAR DI UNA VARIABILE(FISSO)
	 * --> IND-END-VAR    = INDICA L'ULTIMO CHAR DI UNA VARIABILE(VAR)
	 * --> IND-UNZIP      = E' IL CONTATORE DI VARIABILI TROVATE DA USARE
	 *                      PER LA OCCURS 100 DI OUTPUT(AREA VARIABILI)
	 * --> IND-CHAR       = INDICA LA LUNGHEZZA DI UNA VARIABILE
	 * --> WK-GLOVAR-MAX</pre>*/
    private void u999UnzipString() {
        // COB_CODE: MOVE 1                             TO IND-START-VAR.
        ws.getIndici().setIndStartVar(((short)1));
        // COB_CODE: MOVE ZEROES                        TO IND-UNZIP
        //                                                 IND-CHAR.
        ws.getIndici().setIndUnzip(((short)0));
        ws.getIndici().setIndChar(((short)0));
        // COB_CODE: SET NO-FINE-VARLIST                TO TRUE.
        ws.getSwSwitch().getWkVarlist().setNoFineVarlist();
        // COB_CODE: PERFORM VARYING IND-END-VAR FROM 1 BY 1
        //                     UNTIL IND-END-VAR > UNZIP-LENGTH-STR-MAX
        //                        OR SI-FINE-VARLIST
        //             END-IF
        //           END-PERFORM.
        ws.getIndici().setIndEndVar(((short)1));
        while (!(ws.getIndici().getIndEndVar() > ws.getUnzipStructure().getUnzipLengthStrMax() || ws.getSwSwitch().getWkVarlist().isSiFineVarlist())) {
            // COB_CODE:        IF UNZIP-STRING-ZIPPED(IND-END-VAR:1) =
            //                                                  UNZIP-IDENTIFICATORE OR
            //                                                  SPACES               OR
            //                                                  LOW-VALUE            OR
            //                                                  HIGH-VALUE
            //           *-->   E' STATA TROVATA UNA VARIABILE E VIENE CARICATA IN
            //           *-->   OUTPUT
            //                     END-IF
            //                  ELSE
            //                     ADD  1                     TO IND-CHAR
            //                  END-IF
            if (Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIndici().getIndEndVar()) - 1, ws.getIndici().getIndEndVar()), rTrim(String.valueOf(ws.getUnzipStructure().getUnzipIdentificatore()))) || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIndici().getIndEndVar()) - 1, ws.getIndici().getIndEndVar()), "") || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIndici().getIndEndVar()) - 1, ws.getIndici().getIndEndVar()), LiteralGenerator.create(Types.LOW_CHAR_VAL, 1)) || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIndici().getIndEndVar()) - 1, ws.getIndici().getIndEndVar()), LiteralGenerator.create(Types.HIGH_CHAR_VAL, 1))) {
                //-->   E' STATA TROVATA UNA VARIABILE E VIENE CARICATA IN
                //-->   OUTPUT
                // COB_CODE: IF IND-CHAR > 0
                //              END-IF
                //           END-IF
                if (ws.getIndici().getIndChar() > 0) {
                    // COB_CODE: ADD  1                         TO IND-UNZIP
                    ws.getIndici().setIndUnzip(Trunc.toShort(1 + ws.getIndici().getIndUnzip(), 4));
                    // COB_CODE: MOVE UNZIP-STRING-ZIPPED(IND-START-VAR:IND-CHAR)
                    //             TO UNZIP-COD-VARIABILE(IND-UNZIP)
                    //                                   (1:UNZIP-LENGTH-FIELD)
                    ws.getUnzipStructure().getUnzipTab().setCodVariabile(ws.getIndici().getIndUnzip(), Functions.setSubstring(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIndici().getIndUnzip()), ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIndici().getIndStartVar()) - 1, ws.getIndici().getIndStartVar() + ws.getIndici().getIndChar() - 1), 1, ws.getUnzipStructure().getUnzipLengthField()));
                    // COB_CODE: COMPUTE IND-START-VAR = IND-END-VAR + 1
                    ws.getIndici().setIndStartVar(Trunc.toShort(ws.getIndici().getIndEndVar() + 1, 4));
                    // COB_CODE: MOVE ZEROES                          TO IND-CHAR
                    ws.getIndici().setIndChar(((short)0));
                    // COB_CODE: IF UNZIP-STRING-ZIPPED(IND-END-VAR:1) = SPACES   OR
                    //                                                LOW-VALUE   OR
                    //                                                HIGH-VALUE
                    //              SET SI-FINE-VARLIST               TO TRUE
                    //           END-IF
                    if (Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIndici().getIndEndVar()) - 1, ws.getIndici().getIndEndVar()), "") || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIndici().getIndEndVar()) - 1, ws.getIndici().getIndEndVar()), LiteralGenerator.create(Types.LOW_CHAR_VAL, 1)) || Conditions.eq(ws.getUnzipStructure().getUnzipStringZippedFormatted().substring((ws.getIndici().getIndEndVar()) - 1, ws.getIndici().getIndEndVar()), LiteralGenerator.create(Types.HIGH_CHAR_VAL, 1))) {
                        // COB_CODE: SET SI-FINE-VARLIST               TO TRUE
                        ws.getSwSwitch().getWkVarlist().setSiFineVarlist();
                    }
                }
            }
            else {
                // COB_CODE: ADD  1                     TO IND-CHAR
                ws.getIndici().setIndChar(Trunc.toShort(1 + ws.getIndici().getIndChar(), 4));
            }
            ws.getIndici().setIndEndVar(Trunc.toShort(ws.getIndici().getIndEndVar() + 1, 4));
        }
        // COB_CODE: MOVE IND-UNZIP                  TO UNZIP-ELE-VARIABILI-MAX.
        ws.getUnzipStructure().setUnzipEleVariabiliMax(ws.getIndici().getIndUnzip());
    }

    /**Original name: E502-CARICA-PARAM-UNZIPPED<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICA CAMPI UNZIPPED
	 * ----------------------------------------------------------------*</pre>*/
    private void e502CaricaParamUnzipped() {
        // COB_CODE: PERFORM VARYING IND-UNZIP FROM 1 BY 1
        //                   UNTIL IND-UNZIP  >= LIMITE-VARIABILI-UNZIPPED OR
        //                         IND-UNZIP  >  UNZIP-ELE-VARIABILI-MAX   OR
        //                         UNZIP-COD-VARIABILE(IND-UNZIP) =
        //                         SPACES OR LOW-VALUE OR HIGH-VALUE
        //                     TO IVVV0212-VAL-PARAM(IX-AUT-OPER, IX-PARAM)
        //           END-PERFORM.
        ws.getIndici().setIndUnzip(((short)1));
        while (!(ws.getIndici().getIndUnzip() >= ws.getLimiteVariabiliUnzipped() || ws.getIndici().getIndUnzip() > ws.getUnzipStructure().getUnzipEleVariabiliMax() || Characters.EQ_SPACE.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIndici().getIndUnzip())) || Characters.EQ_LOW.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIndici().getIndUnzip()), UnzipTab.Len.COD_VARIABILE) || Characters.EQ_HIGH.test(ws.getUnzipStructure().getUnzipTab().getCodVariabile(ws.getIndici().getIndUnzip()), UnzipTab.Len.COD_VARIABILE))) {
            // COB_CODE: MOVE IND-UNZIP  TO IX-PARAM
            ws.getIndici().setIxParam(ws.getIndici().getIndUnzip());
            // COB_CODE: MOVE UNZIP-COD-VARIABILE(IND-UNZIP)
            //                                   (1:UNZIP-LENGTH-FIELD)
            //             TO IVVV0212-COD-PARAM(IX-AUT-OPER, IX-PARAM)
            areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).getTabParam(ws.getIndici().getIxParam()).setCodParam(ws.getUnzipStructure().getUnzipTab().getCodVariabileFormatted(ws.getIndici().getIndUnzip()).substring((1) - 1, ws.getUnzipStructure().getUnzipLengthField()));
            // COB_CODE: MOVE SPACES
            //             TO IVVV0212-VAL-PARAM(IX-AUT-OPER, IX-PARAM)
            areaIoIvvs0212.getCtrlAutOper(ws.getIndici().getIxAutOper()).getTabParam(ws.getIndici().getIxParam()).setValParam("");
            ws.getIndici().setIndUnzip(Trunc.toShort(ws.getIndici().getIndUnzip() + 1, 4));
        }
    }

    /**Original name: OPEN-CURSORE-1<br>*/
    private void openCursore1() {
        // COB_CODE: EXEC SQL
        //                OPEN CAO-1
        //           END-EXEC.
        ctrlAutOperDao.openCao1(ws);
    }

    /**Original name: OPEN-CURSORE-2<br>*/
    private void openCursore2() {
        // COB_CODE: EXEC SQL
        //                OPEN CAO-2
        //           END-EXEC.
        ctrlAutOperDao.openCao2(ws);
    }

    /**Original name: OPEN-CURSORE-3<br>*/
    private void openCursore3() {
        // COB_CODE: EXEC SQL
        //                OPEN CAO-3
        //           END-EXEC.
        ctrlAutOperDao.openCao3(ws);
    }

    /**Original name: OPEN-CURSORE-4<br>*/
    private void openCursore4() {
        // COB_CODE: EXEC SQL
        //                OPEN CAO-4
        //           END-EXEC.
        ctrlAutOperDao.openCao4(ws.getCtrlAutOper().getCaoCodCompagniaAnia(), ws.getCtrlAutOper().getCaoTpMovi().getCaoTpMovi(), ws.getCtrlAutOper().getCaoKeyAutOper1(), ws.getCtrlAutOper().getCaoKeyAutOper2());
    }

    /**Original name: OPEN-CURSORE-5<br>*/
    private void openCursore5() {
        // COB_CODE: EXEC SQL
        //                OPEN CAO-5
        //           END-EXEC.
        ctrlAutOperDao.openCao5(ws.getCtrlAutOper().getCaoCodCompagniaAnia(), ws.getCtrlAutOper().getCaoTpMovi().getCaoTpMovi(), ws.getCtrlAutOper().getCaoKeyAutOper1());
    }

    /**Original name: OPEN-CURSORE-6<br>*/
    private void openCursore6() {
        // COB_CODE: EXEC SQL
        //                OPEN CAO-6
        //           END-EXEC.
        ctrlAutOperDao.openCao6(ws.getCtrlAutOper().getCaoCodCompagniaAnia(), ws.getCtrlAutOper().getCaoTpMovi().getCaoTpMovi());
    }

    /**Original name: CAO-1-FETCH-NEXT<br>*/
    private void cao1FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CAO-1
        //             INTO
        //                :CAO-ID-CTRL-AUT-OPER
        //               ,:CAO-COD-COMPAGNIA-ANIA
        //               ,:CAO-TP-MOVI
        //                :IND-CAO-TP-MOVI
        //               ,:CAO-COD-ERRORE
        //               ,:CAO-KEY-AUT-OPER1
        //                :IND-CAO-KEY-AUT-OPER1
        //               ,:CAO-KEY-AUT-OPER2
        //                :IND-CAO-KEY-AUT-OPER2
        //               ,:CAO-KEY-AUT-OPER3
        //                :IND-CAO-KEY-AUT-OPER3
        //               ,:CAO-KEY-AUT-OPER4
        //                :IND-CAO-KEY-AUT-OPER4
        //               ,:CAO-KEY-AUT-OPER5
        //                :IND-CAO-KEY-AUT-OPER5
        //               ,:CAO-COD-LIV-AUT
        //               ,:CAO-TP-MOT-DEROGA
        //               ,:CAO-MODULO-VERIFICA
        //                :IND-CAO-MODULO-VERIFICA
        //               ,:CAO-COD-COND
        //                :IND-CAO-COD-COND
        //               ,:CAO-PROG-COND
        //                :IND-CAO-PROG-COND
        //               ,:CAO-RISULT-COND
        //                :IND-CAO-RISULT-COND
        //               ,:CAO-PARAM-AUT-OPER-VCHAR
        //                :IND-CAO-PARAM-AUT-OPER
        //               ,:CAO-STATO-ATTIVAZIONE
        //                :IND-CAO-STATO-ATTIVAZIONE
        //           END-EXEC.
        ctrlAutOperDao.fetchCao1(ws);
    }

    /**Original name: CAO-2-FETCH-NEXT<br>*/
    private void cao2FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CAO-2
        //             INTO
        //                :CAO-ID-CTRL-AUT-OPER
        //               ,:CAO-COD-COMPAGNIA-ANIA
        //               ,:CAO-TP-MOVI
        //                :IND-CAO-TP-MOVI
        //               ,:CAO-COD-ERRORE
        //               ,:CAO-KEY-AUT-OPER1
        //                :IND-CAO-KEY-AUT-OPER1
        //               ,:CAO-KEY-AUT-OPER2
        //                :IND-CAO-KEY-AUT-OPER2
        //               ,:CAO-KEY-AUT-OPER3
        //                :IND-CAO-KEY-AUT-OPER3
        //               ,:CAO-KEY-AUT-OPER4
        //                :IND-CAO-KEY-AUT-OPER4
        //               ,:CAO-KEY-AUT-OPER5
        //                :IND-CAO-KEY-AUT-OPER5
        //               ,:CAO-COD-LIV-AUT
        //               ,:CAO-TP-MOT-DEROGA
        //               ,:CAO-MODULO-VERIFICA
        //                :IND-CAO-MODULO-VERIFICA
        //               ,:CAO-COD-COND
        //                :IND-CAO-COD-COND
        //               ,:CAO-PROG-COND
        //                :IND-CAO-PROG-COND
        //               ,:CAO-RISULT-COND
        //                :IND-CAO-RISULT-COND
        //               ,:CAO-PARAM-AUT-OPER-VCHAR
        //                :IND-CAO-PARAM-AUT-OPER
        //               ,:CAO-STATO-ATTIVAZIONE
        //                :IND-CAO-STATO-ATTIVAZIONE
        //           END-EXEC.
        ctrlAutOperDao.fetchCao2(ws);
        // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE.
        idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
    }

    /**Original name: CAO-3-FETCH-NEXT<br>*/
    private void cao3FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CAO-3
        //             INTO
        //                :CAO-ID-CTRL-AUT-OPER
        //               ,:CAO-COD-COMPAGNIA-ANIA
        //               ,:CAO-TP-MOVI
        //                :IND-CAO-TP-MOVI
        //               ,:CAO-COD-ERRORE
        //               ,:CAO-KEY-AUT-OPER1
        //                :IND-CAO-KEY-AUT-OPER1
        //               ,:CAO-KEY-AUT-OPER2
        //                :IND-CAO-KEY-AUT-OPER2
        //               ,:CAO-KEY-AUT-OPER3
        //                :IND-CAO-KEY-AUT-OPER3
        //               ,:CAO-KEY-AUT-OPER4
        //                :IND-CAO-KEY-AUT-OPER4
        //               ,:CAO-KEY-AUT-OPER5
        //                :IND-CAO-KEY-AUT-OPER5
        //               ,:CAO-COD-LIV-AUT
        //               ,:CAO-TP-MOT-DEROGA
        //               ,:CAO-MODULO-VERIFICA
        //                :IND-CAO-MODULO-VERIFICA
        //               ,:CAO-COD-COND
        //                :IND-CAO-COD-COND
        //               ,:CAO-PROG-COND
        //                :IND-CAO-PROG-COND
        //               ,:CAO-RISULT-COND
        //                :IND-CAO-RISULT-COND
        //               ,:CAO-PARAM-AUT-OPER-VCHAR
        //                :IND-CAO-PARAM-AUT-OPER
        //               ,:CAO-STATO-ATTIVAZIONE
        //                :IND-CAO-STATO-ATTIVAZIONE
        //           END-EXEC.
        ctrlAutOperDao.fetchCao3(ws);
        // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE.
        idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
    }

    /**Original name: CAO-4-FETCH-NEXT<br>*/
    private void cao4FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CAO-4
        //             INTO
        //                :CAO-ID-CTRL-AUT-OPER
        //               ,:CAO-COD-COMPAGNIA-ANIA
        //               ,:CAO-TP-MOVI
        //                :IND-CAO-TP-MOVI
        //               ,:CAO-COD-ERRORE
        //               ,:CAO-KEY-AUT-OPER1
        //                :IND-CAO-KEY-AUT-OPER1
        //               ,:CAO-KEY-AUT-OPER2
        //                :IND-CAO-KEY-AUT-OPER2
        //               ,:CAO-KEY-AUT-OPER3
        //                :IND-CAO-KEY-AUT-OPER3
        //               ,:CAO-KEY-AUT-OPER4
        //                :IND-CAO-KEY-AUT-OPER4
        //               ,:CAO-KEY-AUT-OPER5
        //                :IND-CAO-KEY-AUT-OPER5
        //               ,:CAO-COD-LIV-AUT
        //               ,:CAO-TP-MOT-DEROGA
        //               ,:CAO-MODULO-VERIFICA
        //                :IND-CAO-MODULO-VERIFICA
        //               ,:CAO-COD-COND
        //                :IND-CAO-COD-COND
        //               ,:CAO-PROG-COND
        //                :IND-CAO-PROG-COND
        //               ,:CAO-RISULT-COND
        //                :IND-CAO-RISULT-COND
        //               ,:CAO-PARAM-AUT-OPER-VCHAR
        //                :IND-CAO-PARAM-AUT-OPER
        //               ,:CAO-STATO-ATTIVAZIONE
        //                :IND-CAO-STATO-ATTIVAZIONE
        //           END-EXEC.
        ctrlAutOperDao.fetchCao4(ws);
        // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE.
        idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
    }

    /**Original name: CAO-5-FETCH-NEXT<br>*/
    private void cao5FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CAO-5
        //             INTO
        //                :CAO-ID-CTRL-AUT-OPER
        //               ,:CAO-COD-COMPAGNIA-ANIA
        //               ,:CAO-TP-MOVI
        //                :IND-CAO-TP-MOVI
        //               ,:CAO-COD-ERRORE
        //               ,:CAO-KEY-AUT-OPER1
        //                :IND-CAO-KEY-AUT-OPER1
        //               ,:CAO-KEY-AUT-OPER2
        //                :IND-CAO-KEY-AUT-OPER2
        //               ,:CAO-KEY-AUT-OPER3
        //                :IND-CAO-KEY-AUT-OPER3
        //               ,:CAO-KEY-AUT-OPER4
        //                :IND-CAO-KEY-AUT-OPER4
        //               ,:CAO-KEY-AUT-OPER5
        //                :IND-CAO-KEY-AUT-OPER5
        //               ,:CAO-COD-LIV-AUT
        //               ,:CAO-TP-MOT-DEROGA
        //               ,:CAO-MODULO-VERIFICA
        //                :IND-CAO-MODULO-VERIFICA
        //               ,:CAO-COD-COND
        //                :IND-CAO-COD-COND
        //               ,:CAO-PROG-COND
        //                :IND-CAO-PROG-COND
        //               ,:CAO-RISULT-COND
        //                :IND-CAO-RISULT-COND
        //               ,:CAO-PARAM-AUT-OPER-VCHAR
        //                :IND-CAO-PARAM-AUT-OPER
        //               ,:CAO-STATO-ATTIVAZIONE
        //                :IND-CAO-STATO-ATTIVAZIONE
        //           END-EXEC.
        ctrlAutOperDao.fetchCao5(ws);
        // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE.
        idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
    }

    /**Original name: CAO-6-FETCH-NEXT<br>*/
    private void cao6FetchNext() {
        // COB_CODE: EXEC SQL
        //                FETCH CAO-6
        //             INTO
        //                :CAO-ID-CTRL-AUT-OPER
        //               ,:CAO-COD-COMPAGNIA-ANIA
        //               ,:CAO-TP-MOVI
        //                :IND-CAO-TP-MOVI
        //               ,:CAO-COD-ERRORE
        //               ,:CAO-KEY-AUT-OPER1
        //                :IND-CAO-KEY-AUT-OPER1
        //               ,:CAO-KEY-AUT-OPER2
        //                :IND-CAO-KEY-AUT-OPER2
        //               ,:CAO-KEY-AUT-OPER3
        //                :IND-CAO-KEY-AUT-OPER3
        //               ,:CAO-KEY-AUT-OPER4
        //                :IND-CAO-KEY-AUT-OPER4
        //               ,:CAO-KEY-AUT-OPER5
        //                :IND-CAO-KEY-AUT-OPER5
        //               ,:CAO-COD-LIV-AUT
        //               ,:CAO-TP-MOT-DEROGA
        //               ,:CAO-MODULO-VERIFICA
        //                :IND-CAO-MODULO-VERIFICA
        //               ,:CAO-COD-COND
        //                :IND-CAO-COD-COND
        //               ,:CAO-PROG-COND
        //                :IND-CAO-PROG-COND
        //               ,:CAO-RISULT-COND
        //                :IND-CAO-RISULT-COND
        //               ,:CAO-PARAM-AUT-OPER-VCHAR
        //                :IND-CAO-PARAM-AUT-OPER
        //               ,:CAO-STATO-ATTIVAZIONE
        //                :IND-CAO-STATO-ATTIVAZIONE
        //           END-EXEC.
        ctrlAutOperDao.fetchCao6(ws);
        // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE.
        idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
    }

    /**Original name: CAO-1-CLOSE-CURSOR<br>*/
    private void cao1CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CAO-1
        //           END-EXEC.
        ctrlAutOperDao.closeCao1();
    }

    /**Original name: CAO-2-CLOSE-CURSOR<br>*/
    private void cao2CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CAO-2
        //           END-EXEC.
        ctrlAutOperDao.closeCao2();
    }

    /**Original name: CAO-3-CLOSE-CURSOR<br>*/
    private void cao3CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CAO-3
        //           END-EXEC.
        ctrlAutOperDao.closeCao3();
    }

    /**Original name: CAO-4-CLOSE-CURSOR<br>*/
    private void cao4CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CAO-4
        //           END-EXEC.
        ctrlAutOperDao.closeCao4();
    }

    /**Original name: CAO-5-CLOSE-CURSOR<br>*/
    private void cao5CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CAO-5
        //           END-EXEC.
        ctrlAutOperDao.closeCao5();
    }

    /**Original name: CAO-6-CLOSE-CURSOR<br>*/
    private void cao6CloseCursor() {
        // COB_CODE: EXEC SQL
        //                CLOSE CAO-6
        //           END-EXEC.
        ctrlAutOperDao.closeCao6();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-CAO-TP-MOVI = -1
        //              MOVE HIGH-VALUES TO CAO-TP-MOVI-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getTpMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-TP-MOVI-NULL
            ws.getCtrlAutOper().getCaoTpMovi().setCaoTpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CaoTpMovi.Len.CAO_TP_MOVI_NULL));
        }
        // COB_CODE: IF IND-CAO-KEY-AUT-OPER1 = -1
        //              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER1-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getKeyAutOper1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER1-NULL
            ws.getCtrlAutOper().setCaoKeyAutOper1(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CtrlAutOper.Len.CAO_KEY_AUT_OPER1));
        }
        // COB_CODE: IF IND-CAO-KEY-AUT-OPER2 = -1
        //              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER2-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getKeyAutOper2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER2-NULL
            ws.getCtrlAutOper().setCaoKeyAutOper2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CtrlAutOper.Len.CAO_KEY_AUT_OPER2));
        }
        // COB_CODE: IF IND-CAO-KEY-AUT-OPER3 = -1
        //              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER3-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getKeyAutOper3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER3-NULL
            ws.getCtrlAutOper().setCaoKeyAutOper3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CtrlAutOper.Len.CAO_KEY_AUT_OPER3));
        }
        // COB_CODE: IF IND-CAO-KEY-AUT-OPER4 = -1
        //              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER4-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getKeyAutOper4() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER4-NULL
            ws.getCtrlAutOper().setCaoKeyAutOper4(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CtrlAutOper.Len.CAO_KEY_AUT_OPER4));
        }
        // COB_CODE: IF IND-CAO-KEY-AUT-OPER5 = -1
        //              MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER5-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getKeyAutOper5() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-KEY-AUT-OPER5-NULL
            ws.getCtrlAutOper().setCaoKeyAutOper5(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CtrlAutOper.Len.CAO_KEY_AUT_OPER5));
        }
        // COB_CODE: IF IND-CAO-MODULO-VERIFICA = -1
        //              MOVE HIGH-VALUES TO CAO-MODULO-VERIFICA-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getModuloVerifica() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-MODULO-VERIFICA-NULL
            ws.getCtrlAutOper().setCaoModuloVerifica(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CtrlAutOper.Len.CAO_MODULO_VERIFICA));
        }
        // COB_CODE: IF IND-CAO-COD-COND = -1
        //              MOVE HIGH-VALUES TO CAO-COD-COND-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getCodCond() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-COD-COND-NULL
            ws.getCtrlAutOper().setCaoCodCond(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CtrlAutOper.Len.CAO_COD_COND));
        }
        // COB_CODE: IF IND-CAO-PROG-COND = -1
        //              MOVE HIGH-VALUES TO CAO-PROG-COND-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getProgCond() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-PROG-COND-NULL
            ws.getCtrlAutOper().getCaoProgCond().setCaoProgCondNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CaoProgCond.Len.CAO_PROG_COND_NULL));
        }
        // COB_CODE: IF IND-CAO-RISULT-COND = -1
        //              MOVE HIGH-VALUES TO CAO-RISULT-COND-NULL
        //           END-IF
        if (ws.getIndCtrlAutOper().getRisultCond() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-RISULT-COND-NULL
            ws.getCtrlAutOper().setCaoRisultCond(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-CAO-PARAM-AUT-OPER = -1
        //              MOVE HIGH-VALUES TO CAO-PARAM-AUT-OPER
        //           END-IF
        if (ws.getIndCtrlAutOper().getParamAutOper() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-PARAM-AUT-OPER
            ws.getCtrlAutOper().setCaoParamAutOper(LiteralGenerator.create(Types.HIGH_CHAR_VAL, CtrlAutOper.Len.CAO_PARAM_AUT_OPER));
        }
        // COB_CODE: IF IND-CAO-STATO-ATTIVAZIONE = -1
        //              MOVE HIGH-VALUES TO CAO-STATO-ATTIVAZIONE-NULL
        //           END-IF.
        if (ws.getIndCtrlAutOper().getStatoAttivazione() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO CAO-STATO-ATTIVAZIONE-NULL
            ws.getCtrlAutOper().setCaoStatoAttivazione(Types.HIGH_CHAR_VAL);
        }
    }

    public void initCtrlAutOper() {
        ws.getCtrlAutOper().setCaoIdCtrlAutOper(0);
        ws.getCtrlAutOper().setCaoCodCompagniaAnia(0);
        ws.getCtrlAutOper().getCaoTpMovi().setCaoTpMovi(0);
        ws.getCtrlAutOper().setCaoCodErrore(0);
        ws.getCtrlAutOper().setCaoKeyAutOper1("");
        ws.getCtrlAutOper().setCaoKeyAutOper2("");
        ws.getCtrlAutOper().setCaoKeyAutOper3("");
        ws.getCtrlAutOper().setCaoKeyAutOper4("");
        ws.getCtrlAutOper().setCaoKeyAutOper5("");
        ws.getCtrlAutOper().setCaoCodLivAut(0);
        ws.getCtrlAutOper().setCaoTpMotDeroga("");
        ws.getCtrlAutOper().setCaoModuloVerifica("");
        ws.getCtrlAutOper().setCaoCodCond("");
        ws.getCtrlAutOper().getCaoProgCond().setCaoProgCond(((short)0));
        ws.getCtrlAutOper().setCaoRisultCond(Types.SPACE_CHAR);
        ws.getCtrlAutOper().setCaoParamAutOperLen(((short)0));
        ws.getCtrlAutOper().setCaoParamAutOper("");
        ws.getCtrlAutOper().setCaoStatoAttivazione(Types.SPACE_CHAR);
    }
}
