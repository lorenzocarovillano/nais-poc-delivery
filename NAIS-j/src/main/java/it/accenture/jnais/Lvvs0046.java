package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0046Data;

/**Original name: LVVS0046<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0046
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... VARIABILE GIORNICED
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0046 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0046Data ws = new Lvvs0046Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0046
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0046_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0046 getInstance() {
        return ((Lvvs0046)Programs.getInstance(Lvvs0046.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             WK-APPO-DT-9.
        initIxIndici();
        initTabOutput();
        ws.setWkAppoDt9Formatted("00000000");
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: INITIALIZE PARAM-MOVI.
        initParamMovi();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1250-CALCOLA-DATA1         THRU S1250-EX
            s1250CalcolaData1();
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
            s1260CalcolaDiff();
        }
    }

    /**Original name: S1250-CALCOLA-DATA1<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATAULTCED
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaData1() {
        Ldbs5760 ldbs5760 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 6007                           TO PMO-TP-MOVI.
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(6007);
        // COB_CODE: MOVE DGRZ-ID-GAR(IVVC0213-IX-TABB)  TO PMO-ID-OGG.
        ws.getParamMovi().setPmoIdOgg(ws.getDgrzTabGar(ivvc0213.getIxTabb()).getLccvgrz1().getDati().getWgrzIdGar());
        // COB_CODE: MOVE 'GA'                           TO PMO-TP-OGG.
        ws.getParamMovi().setPmoTpOgg("GA");
        //
        // COB_CODE: MOVE 'LDBS5760'                     TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS5760");
        //
        //
        // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 PARAM-MOVI
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs5760 = Ldbs5760.getInstance();
            ldbs5760.run(idsv0003, ws.getParamMovi());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS1780 ERRORE CHIAMATA - T0000-TRATTA-MATRICE'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS1780 ERRORE CHIAMATA - T0000-TRATTA-MATRICE");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM              TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS1780 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS1780 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER            TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1260-CALCOLA-DIFF<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1260CalcolaDiff() {
        Lccs0010 lccs0010 = null;
        GenericParam formato = null;
        GenericParam ggDiff = null;
        GenericParam codiceRitorno = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A'                            TO FORMATO
        ws.setFormatoFormatted("A");
        // COB_CODE: IF PMO-DT-RICOR-PREC-NULL = HIGH-VALUE
        //                TO DATA-INFERIORE
        //           ELSE
        //                TO DATA-INFERIORE
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrecNullFormatted())) {
            // COB_CODE: MOVE DGRZ-DT-DECOR(IVVC0213-IX-TABB)
            //             TO WK-APPO-DT-9
            ws.setWkAppoDt9(TruncAbs.toInt(ws.getDgrzTabGar(ivvc0213.getIxTabb()).getLccvgrz1().getDati().getWgrzDtDecor().getWgrzDtDecor(), 8));
            // COB_CODE: MOVE WK-APPO-DT-9
            //             TO DATA-INFERIORE
            ws.getDataInferiore().setDataInferioreFormatted(ws.getWkAppoDt9Formatted());
        }
        else {
            // COB_CODE: MOVE PMO-DT-RICOR-PREC
            //             TO WK-APPO-DT-9
            ws.setWkAppoDt9(TruncAbs.toInt(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrec(), 8));
            // COB_CODE: MOVE WK-APPO-DT-9
            //             TO DATA-INFERIORE
            ws.getDataInferiore().setDataInferioreFormatted(ws.getWkAppoDt9Formatted());
        }
        // COB_CODE: MOVE IVVC0213-DATA-EFFETTO             TO DATA-SUPERIORE.
        ws.getDataSuperiore().setDataSuperioreFormatted(ivvc0213.getDataEffettoFormatted());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE 'LCCS0010'                     TO WK-CALL-PGM
            ws.setWkCallPgm("LCCS0010");
            //
            // COB_CODE: CALL WK-CALL-PGM  USING FORMATO,
            //                                   DATA-INFERIORE,
            //                                   DATA-SUPERIORE,
            //                                   GG-DIFF,
            //                                   CODICE-RITORNO
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                lccs0010 = Lccs0010.getInstance();
                formato = new GenericParam(MarshalByteExt.chToBuffer(ws.getFormato()));
                ggDiff = new GenericParam(MarshalByteExt.strToBuffer(ws.getGgDiffFormatted(), Lvvs0046Data.Len.GG_DIFF));
                codiceRitorno = new GenericParam(MarshalByteExt.chToBuffer(ws.getCodiceRitorno()));
                lccs0010.run(formato, ws.getDataInferiore(), ws.getDataSuperiore(), ggDiff, codiceRitorno);
                ws.setFormatoFromBuffer(formato.getByteData());
                ws.setGgDiffFromBuffer(ggDiff.getByteData());
                ws.setCodiceRitornoFromBuffer(codiceRitorno.getByteData());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: MOVE 'S1260-CALCOLA-DIFFA'
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("S1260-CALCOLA-DIFFA");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE: IF CODICE-RITORNO = ZERO
            //                TO IVVC0213-VAL-IMP-O
            //           ELSE
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           END-IF
            if (Conditions.eq(ws.getCodiceRitorno(), '0')) {
                // COB_CODE: MOVE GG-DIFF
                //             TO IVVC0213-VAL-IMP-O
                ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getGgDiff(), 18, 7));
            }
            else {
                // COB_CODE: MOVE WK-CALL-PGM           TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CHIAMATA LCCS0010 COD-RIT:'
                //                   CODICE-RITORNO ';'
                //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LCCS0010 COD-RIT:", String.valueOf(ws.getCodiceRitorno()), ";");
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-GARANZIA
        //                TO DGRZ-AREA-GRZ
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasGaranzia())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DGRZ-AREA-GRZ
            ws.setDgrzAreaGrzFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabTga(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initParamMovi() {
        ws.getParamMovi().setPmoIdParamMovi(0);
        ws.getParamMovi().setPmoIdOgg(0);
        ws.getParamMovi().setPmoTpOgg("");
        ws.getParamMovi().setPmoIdMoviCrz(0);
        ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(0);
        ws.getParamMovi().setPmoDtIniEff(0);
        ws.getParamMovi().setPmoDtEndEff(0);
        ws.getParamMovi().setPmoCodCompAnia(0);
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(0);
        ws.getParamMovi().getPmoFrqMovi().setPmoFrqMovi(0);
        ws.getParamMovi().getPmoDurAa().setPmoDurAa(0);
        ws.getParamMovi().getPmoDurMm().setPmoDurMm(0);
        ws.getParamMovi().getPmoDurGg().setPmoDurGg(0);
        ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrec(0);
        ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(0);
        ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFraz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaSco(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBns(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoTpRinnColl("");
        ws.getParamMovi().setPmoTpRivalPre("");
        ws.getParamMovi().setPmoTpRivalPrstz("");
        ws.getParamMovi().setPmoFlEvidRival(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerd(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaPror(0);
        ws.getParamMovi().setPmoTpOpz("");
        ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCer(0);
        ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRat(new AfDecimal(0, 15, 3));
        ws.getParamMovi().setPmoIbOgg("");
        ws.getParamMovi().getPmoCosOner().setPmoCosOner(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoSpePc().setPmoSpePc(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoFlAttivGar(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCambioVerProd(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoMmDiff().setPmoMmDiff(((short)0));
        ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfee(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfee(0);
        ws.getParamMovi().setPmoTpOggRival("");
        ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGarac(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(0);
        ws.getParamMovi().setPmoIdPoli(0);
        ws.getParamMovi().setPmoTpFrmAssva("");
        ws.getParamMovi().setPmoDsRiga(0);
        ws.getParamMovi().setPmoDsOperSql(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoDsVer(0);
        ws.getParamMovi().setPmoDsTsIniCptz(0);
        ws.getParamMovi().setPmoDsTsEndCptz(0);
        ws.getParamMovi().setPmoDsUtente("");
        ws.getParamMovi().setPmoDsStatoElab(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoTpEstrCnt("");
        ws.getParamMovi().setPmoCodRamo("");
        ws.getParamMovi().setPmoGenDaSin(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCodTari("");
        ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPre(0);
        ws.getParamMovi().getPmoPcServVal().setPmoPcServVal(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(((short)0));
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }
}
