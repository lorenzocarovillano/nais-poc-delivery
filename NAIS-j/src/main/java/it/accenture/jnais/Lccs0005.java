package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.inspect.InspectPattern;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0002;
import it.accenture.jnais.copy.StatRichEst;
import it.accenture.jnais.copy.WadeDati;
import it.accenture.jnais.copy.WkVarLvec0202;
import it.accenture.jnais.copy.WmovDati;
import it.accenture.jnais.copy.WodeDati;
import it.accenture.jnais.copy.WpagDatiInput;
import it.accenture.jnais.copy.WpolDati;
import it.accenture.jnais.copy.WricDati;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.enums.WpolStatus;
import it.accenture.jnais.ws.enums.WsMovimento;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lccs0005Data;
import it.accenture.jnais.ws.Lccv0005;
import it.accenture.jnais.ws.redefines.StwIdMoviChiu;
import it.accenture.jnais.ws.redefines.WmovIdRich;
import it.accenture.jnais.ws.redefines.WodeIdMoviChiu;
import it.accenture.jnais.ws.WadeAreaAdesioneLccs0005;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WkVariabili;
import it.accenture.jnais.ws.WmovAreaMovimento;
import it.accenture.jnais.ws.WnotAreaNoteOgg;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WricAreaRichiesta;
import it.accenture.jnais.ws.WrreAreaRappRete;
import it.accenture.jnais.ws.WtgaAreaTranche;
import static java.lang.Math.abs;

/**Original name: LCCS0005<br>
 * <pre>****************************************************************
 * *                                                              *
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          *
 * *                                                              *
 * ****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * *--------------------------------------------------------------**
 *     PROGRAMMA ..... LCCS0005
 *     TIPOLOGIA...... EOC COMUNE - Per tutte le funzionalita'
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... Gestione del movimento, richiesta e Oggetti
 *                     Deroga
 * *--------------------------------------------------------------**
 * ----------------------------------------------------------------*
 *     COPY VALORIZZAZIONE DCLGEN
 * ----------------------------------------------------------------*
 *      COPY LCCVNOT5.</pre>*/
public class Lccs0005 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0005Data ws = new Lccs0005Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: WMOV-AREA-MOVIMENTO
    private WmovAreaMovimento wmovAreaMovimento;
    //Original name: WRIC-AREA-RICHIESTA
    private WricAreaRichiesta wricAreaRichiesta;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLccs0005 wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTranche wtgaAreaTranche;
    //Original name: WCOM-LCCS0005
    private Lccv0005 lccv0005;
    //Original name: WRRE-AREA-RAPP-RETE
    private WrreAreaRappRete wrreAreaRappRete;
    //Original name: WNOT-AREA-NOTE-OGG
    private WnotAreaNoteOgg wnotAreaNoteOgg;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0005_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, WmovAreaMovimento wmovAreaMovimento, WricAreaRichiesta wricAreaRichiesta, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLccs0005 wadeAreaAdesione, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTranche wtgaAreaTranche, Lccv0005 lccv0005, WrreAreaRappRete wrreAreaRappRete, WnotAreaNoteOgg wnotAreaNoteOgg) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.wmovAreaMovimento = wmovAreaMovimento;
        this.wricAreaRichiesta = wricAreaRichiesta;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.lccv0005 = lccv0005;
        this.wrreAreaRappRete = wrreAreaRappRete;
        this.wnotAreaNoteOgg = wnotAreaNoteOgg;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0005 getInstance() {
        return ((Lccs0005)Programs.getInstance(Lccs0005.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                         WK-VARIABILI
        //                                              WS-INDICI
        //                                              RICH
        //                                              MOVI
        //                                              STAT-OGG-WF
        //                                              OGG-DEROGA
        //                                              MOT-DEROGA.
        initWkVariabili();
        initWsIndici();
        initRich();
        initMovi();
        initStatOggWf();
        initOggDeroga();
        initMotDeroga();
        //--  calcolo data competenza
        // COB_CODE: IF NOT IDSV0001-ON-LINE
        //              MOVE WK-LIVELLO-DEBUG        TO IDSV0001-LIVELLO-DEBUG
        //           END-IF.
        if (!areaIdsv0001.getAreaComune().getModalitaEsecutiva().isIdsv0001OnLine()) {
            // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG  TO WK-LIVELLO-DEBUG
            ws.setWkLivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
            // COB_CODE: PERFORM B020-ESTRAI-DT-COMPETENZA  THRU B020-EX
            b020EstraiDtCompetenza();
            // COB_CODE: MOVE WK-LIVELLO-DEBUG        TO IDSV0001-LIVELLO-DEBUG
            areaIdsv0001.getAreaComune().getLivelloDebug().setIdsi0011LivelloDebugFormatted(ws.getWkLivelloDebugFormatted());
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO    TO WS-MOVIMENTO
            ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
            // COB_CODE: IF WCOM-ID-OGG-DEROGA NOT NUMERIC
            //              MOVE ZEROES                  TO WCOM-ID-OGG-DEROGA
            //           END-IF
            if (!Functions.isNumber(lccc0001.getDatiDeroghe().getOggettoDeroga().getIdOggDeroga())) {
                // COB_CODE: MOVE ZEROES                  TO WCOM-ID-OGG-DEROGA
                lccc0001.getDatiDeroghe().getOggettoDeroga().setIdOggDeroga(0);
            }
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> AGGIORNA RICHIESTA</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: IF LCCV0005-RICHIESTA-SI
        //                 THRU AGGIORNA-RICH-EX
        //           END-IF
        if (lccv0005.getRichiesta().isSi()) {
            // COB_CODE: PERFORM AGGIORNA-RICH
            //              THRU AGGIORNA-RICH-EX
            aggiornaRich();
        }
        //--> AGGIORNA MOVIMENTO
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF NOT WMOV-ST-INV
            //              END-IF
            //           ELSE
            //                TO WMOV-ID-PTF
            //           END-IF
            if (!wmovAreaMovimento.getLccvmov1().getStatus().isInv()) {
                // COB_CODE: IF LCCV0005-PREVENTIVAZIONE
                //              END-IF
                //           END-IF
                if (lccv0005.getMacroFunzione().isPreventivazione()) {
                    // COB_CODE: IF REVIS-INDIVI
                    //                TO MOV-ID-MOVI
                    //           END-IF
                    if (ws.getWsMovimento().isRevisIndivi()) {
                        // COB_CODE: MOVE WMOV-ID-MOVI
                        //             TO MOV-ID-MOVI
                        ws.getMovi().setMovIdMovi(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdMovi());
                    }
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //                 THRU AGGIORNA-MOVI-EX
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: PERFORM AGGIORNA-MOVI
                        //              THRU AGGIORNA-MOVI-EX
                        aggiornaMovi();
                    }
                }
                // COB_CODE: IF LCCV0005-VENDITA
                //                 THRU S1006-EX
                //           END-IF
                if (lccv0005.getMacroFunzione().isVendita()) {
                    // COB_CODE: PERFORM S1006-CALC-IB-OGGETTO
                    //              THRU S1006-EX
                    s1006CalcIbOggetto();
                }
                // COB_CODE: IF IDSV0001-ESITO-OK
                //              END-IF
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: IF NOT LCCV0005-PREVENTIVAZIONE
                    //                 THRU AGGIORNA-MOVI-EX
                    //           END-IF
                    if (!lccv0005.getMacroFunzione().isPreventivazione()) {
                        // COB_CODE: PERFORM AGGIORNA-MOVI
                        //              THRU AGGIORNA-MOVI-EX
                        aggiornaMovi();
                    }
                }
            }
            else {
                // COB_CODE: MOVE WMOV-ID-MOVI
                //             TO WMOV-ID-PTF
                wmovAreaMovimento.getLccvmov1().setIdPtf(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdMovi());
            }
        }
        //--> CAMPO DELL'AREA STATI UTILIZZATO DAL PROCESSO DI STAMPA
        //--> F.E. - INOLTRE PER IL MOVIMENTO FITTIZIO 1006 NON VA
        //--> PASSATO L'ID
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                TO WS-MOVIMENTO
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE WMOV-TP-MOVI
            //             TO WS-MOVIMENTO
            ws.getWsMovimento().setWsMovimento(TruncAbs.toInt(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpMovi().getWmovTpMovi(), 5));
            // COB_CODE: IF NOT EMISS-POLIZZA
            //                TO WCOM-ID-MOVI-CRZ
            //           END-IF
            if (!ws.getWsMovimento().isEmissPolizza()) {
                // COB_CODE: MOVE WMOV-ID-PTF
                //             TO WCOM-ID-MOVI-CRZ
                lccc0001.setIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
            }
            //-->    RIPRISTINO IL TIPO MOVIMENTO DI PARTENZA
            // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
            //             TO WS-MOVIMENTO
            ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        }
        //--> AGGIORNA STATO OGGETTO WORK-FLOW - MOVIMENTO
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1010
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1010-AGGIORNA-WORK-FLOW-MO
            //              THRU EX-S1010
            s1010AggiornaWorkFlowMo();
        }
        //--> AGGIORNA NOTE OGGETTO - MOVIMENTO
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU AGGIORNA-NOTE-OGG-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM AGGIORNA-NOTE-OGG
            //              THRU AGGIORNA-NOTE-OGG-EX
            aggiornaNoteOgg();
        }
        //--> SETTIAMO LO STATUS DELLA NOTE OGGETTO A 'INVARIATO' PER
        //--> EVITARE DI SCRIVERE LA STESSA NOTA LEGATA A PIU' MOVIMENTI
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              SET WNOT-ST-INV     TO TRUE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: SET WNOT-ST-INV     TO TRUE
            wnotAreaNoteOgg.getLccvnot1().getStatus().setInv();
        }
        //--> SE CI SONO DEROGHE
        // COB_CODE:      IF WCOM-NUM-ELE-MOT-DEROGA > 0
        //           *-->    AGGIORNA DEROGHE
        //                   END-IF
        //                END-IF.
        if (lccc0001.getDatiDeroghe().getNumEleMotDeroga() > 0) {
            //-->    AGGIORNA DEROGHE
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU EX-S1060
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S1060-AGGIORNA-DEROGHE
                //              THRU EX-S1060
                s1060AggiornaDeroghe();
            }
            //-->    AGGIORNA STATO OGGETTO WORK-FLOW - OGGETTO DEROGA
            // COB_CODE: IF IDSV0001-ESITO-OK
            //                 THRU EX-S1090
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE: PERFORM S1090-AGGIORNA-WORK-FLOW-DE
                //              THRU EX-S1090
                s1090AggiornaWorkFlowDe();
            }
        }
        //--> SE C'E' UNA RICHIESTA ESTERNA
        // COB_CODE: IF  WCOM-ID-RICH-EST IS NUMERIC
        //           AND WCOM-ID-RICH-EST > 0
        //               END-IF
        //           END-IF.
        if (Functions.isNumber(lccc0001.getIdRichEst()) && lccc0001.getIdRichEst() > 0) {
            // COB_CODE:          IF IDSV0001-ESITO-OK
            //           *-->        AGGIORNA STATO RICHIESTA ESTERNA
            //                          THRU EX-S1100
            //                    END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->        AGGIORNA STATO RICHIESTA ESTERNA
                // COB_CODE: PERFORM S1100-AGGIORNA-STATO-RIC-EST
                //              THRU EX-S1100
                s1100AggiornaStatoRicEst();
            }
            // COB_CODE:          IF IDSV0001-ESITO-OK
            //           *-->        AGGIORNA RICHIESTA ESTERNA
            //                          THRU EX-S1200
            //                    END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->        AGGIORNA RICHIESTA ESTERNA
                // COB_CODE: PERFORM S1200-AGGIORNA-RIC-EST
                //              THRU EX-S1200
                s1200AggiornaRicEst();
            }
        }
    }

    /**Original name: S1006-CALC-IB-OGGETTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALCOLA IB-OGGETTO
	 * ----------------------------------------------------------------*
	 *     Estrazione del sequence della polizza o adesione per il
	 *     calcolo del relativo IB-OGGETTO che viene salvato nel campo
	 *     IB-OGGETTO del Movimento</pre>*/
    private void s1006CalcIbOggetto() {
        // COB_CODE:      EVALUATE TRUE
        //                    WHEN CREAZ-COLLET
        //                         END-IF
        //                    WHEN CREAZ-INDIVI
        //                    WHEN TRFOR-INDIVI
        //                    WHEN CREAZ-INDIVI-REINV
        //                         END-IF
        //                    WHEN CREAZ-ADESIO
        //                    WHEN TRASF-ADESIO
        //                    WHEN TRASFER-TESTA-POS
        //                         END-IF
        //                    WHEN ATTIV-COLLET
        //                    WHEN ATTIV-INDIVI
        //           *             ESTRAZIONE IB-OGGETTO DELLA POLIZZA
        //                            THRU ESTRAZIONE-IB-OGGETTO-EX
        //                END-EVALUATE.
        switch (ws.getWsMovimento().getWsMovimentoFormatted()) {

            case WsMovimento.CREAZ_COLLET:// COB_CODE: PERFORM ESTR-SEQUENCE-POLI
                //              THRU ESTR-SEQUENCE-POLI-EX
                estrSequencePoli();
                // COB_CODE:               IF IDSV0001-ESITO-OK
                //           *                ESTRAZIONE IB PROPOSTA POLIZZA
                //                               THRU ESTRAZIONE-IB-PROPOSTA-EX
                //                         END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //                ESTRAZIONE IB PROPOSTA POLIZZA
                    // COB_CODE: PERFORM ESTRAZIONE-IB-PROPOSTA
                    //              THRU ESTRAZIONE-IB-PROPOSTA-EX
                    estrazioneIbProposta();
                }
                break;

            case WsMovimento.CREAZ_INDIVI:
            case WsMovimento.TRFOR_INDIVI:
            case WsMovimento.CREAZ_INDIVI_REINV:// COB_CODE: PERFORM ESTR-SEQUENCE-POLI
                //              THRU ESTR-SEQUENCE-POLI-EX
                estrSequencePoli();
                // COB_CODE:               IF IDSV0001-ESITO-OK
                //           *                ESTRAZIONE IB PROPOSTA POLIZZA
                //                               THRU ESTRAZIONE-IB-PROPOSTA-EX
                //                         END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //                ESTRAZIONE IB PROPOSTA POLIZZA
                    // COB_CODE: PERFORM ESTRAZIONE-IB-PROPOSTA
                    //              THRU ESTRAZIONE-IB-PROPOSTA-EX
                    estrazioneIbProposta();
                }
                // COB_CODE: IF IDSV0001-ESITO-OK
                //                 THRU ESTR-SEQUENCE-ADES-EX
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: PERFORM ESTR-SEQUENCE-ADES
                    //              THRU ESTR-SEQUENCE-ADES-EX
                    estrSequenceAdes();
                }
                // COB_CODE:               IF IDSV0001-ESITO-OK
                //           *                ESTRAZIONE IB OGGETTO ADESIONE
                //                               THRU ESTR-IB-OGG-ADE-EX
                //                         END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //                ESTRAZIONE IB OGGETTO ADESIONE
                    // COB_CODE: PERFORM ESTR-IB-OGG-ADE
                    //              THRU ESTR-IB-OGG-ADE-EX
                    estrIbOggAde();
                }
                break;

            case WsMovimento.CREAZ_ADESIO:
            case WsMovimento.TRASF_ADESIO:
            case WsMovimento.TRASFER_TESTA_POS:// COB_CODE: PERFORM ESTR-SEQUENCE-ADES
                //              THRU ESTR-SEQUENCE-ADES-EX
                estrSequenceAdes();
                // COB_CODE:               IF IDSV0001-ESITO-OK
                //           *                ESTRAZIONE IB OGGETTO ADESIONE
                //                               THRU ESTR-IB-OGG-ADE-EX
                //                         END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    //                ESTRAZIONE IB OGGETTO ADESIONE
                    // COB_CODE: PERFORM ESTR-IB-OGG-ADE
                    //              THRU ESTR-IB-OGG-ADE-EX
                    estrIbOggAde();
                }
                break;

            case WsMovimento.ATTIV_COLLET:
            case WsMovimento.ATTIV_INDIVI://             ESTRAZIONE IB-OGGETTO DELLA POLIZZA
                // COB_CODE: PERFORM ESTRAZIONE-IB-OGGETTO
                //              THRU ESTRAZIONE-IB-OGGETTO-EX
                estrazioneIbOggetto();
                break;

            default:break;
        }
    }

    /**Original name: S1010-AGGIORNA-WORK-FLOW-MO<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE STATO OGGETTO WORK FLOW MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1010AggiornaWorkFlowMo() {
        // COB_CODE:      IF WMOV-ST-ADD
        //                      THRU EX-S1040
        //                ELSE
        //           *-->    LETTURA STATO OGGETTO WORK-FLOW - OGGETTO MOVIMENTO
        //                   END-IF
        //                END-IF.
        if (wmovAreaMovimento.getLccvmov1().getStatus().isAdd()) {
            // COB_CODE: MOVE WCOM-STATO-MOVIMENTO           TO WK-STAT-OGG-WF
            ws.setWkStatOggWf(lccc0001.getStati().getStatoMovimento());
            // COB_CODE: MOVE WCOM-MOV-FLAG-ST-FINALE        TO WK-STAT-END-WF
            ws.setWkStatEndWf(lccc0001.getStati().getMovFlagStFinale().getMovFlagStFinale());
            // COB_CODE: SET IDSI0011-AGG-STORICO-SOLO-INS   TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggStoricoSoloIns();
            // COB_CODE: MOVE WMOV-ID-PTF                    TO WK-ID-OGG
            ws.setWkIdOgg(wmovAreaMovimento.getLccvmov1().getIdPtf());
            // COB_CODE: MOVE 'MO'                           TO WK-TP-OGG
            ws.setWkTpOgg("MO");
            // COB_CODE: PERFORM S1040-INSERT-STW
            //              THRU EX-S1040
            s1040InsertStw();
        }
        else {
            //-->    LETTURA STATO OGGETTO WORK-FLOW - OGGETTO MOVIMENTO
            // COB_CODE: MOVE WMOV-ID-PTF                    TO WK-ID-OGG
            ws.setWkIdOgg(wmovAreaMovimento.getLccvmov1().getIdPtf());
            // COB_CODE: MOVE 'MO'                           TO WK-TP-OGG
            ws.setWkTpOgg("MO");
            // COB_CODE: PERFORM S1020-SELECT-STW
            //              THRU EX-S1020
            s1020SelectStw();
            //--     AGGIORNA STATO OGGETTO WORK-FLOW - OGGETTO MOVIMENTO
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *--        IMPOSTAZIONE DELLO STATO WORK FLOW - MOVIMENTO
            //                      END-EVALUATE
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //--        IMPOSTAZIONE DELLO STATO WORK FLOW - MOVIMENTO
                // COB_CODE: MOVE WCOM-STATO-MOVIMENTO        TO WK-STAT-OGG-WF
                ws.setWkStatOggWf(lccc0001.getStati().getStatoMovimento());
                // COB_CODE: MOVE WCOM-MOV-FLAG-ST-FINALE     TO WK-STAT-END-WF
                ws.setWkStatEndWf(lccc0001.getStati().getMovFlagStFinale().getMovFlagStFinale());
                //-->         OCCORRENZA NON TROVATA (SQLCODE = +100)
                // COB_CODE:            EVALUATE TRUE
                //           *-->         OCCORRENZA NON TROVATA (SQLCODE = +100)
                //                        WHEN IDSO0011-NOT-FOUND
                //                              THRU EX-S1040
                //           *-->         OCCORRENZA TROVATA (SQLCODE = +0)
                //                        WHEN IDSO0011-SUCCESSFUL-SQL
                //                              THRU EX-S1030
                //                      END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: SET IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                        // COB_CODE: PERFORM S1040-INSERT-STW
                        //              THRU EX-S1040
                        s1040InsertStw();
                        //-->         OCCORRENZA TROVATA (SQLCODE = +0)
                        break;

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: PERFORM S1030-UPDATE-STW
                        //              THRU EX-S1030
                        s1030UpdateStw();
                        break;

                    default:break;
                }
            }
        }
    }

    /**Original name: S1020-SELECT-STW<br>
	 * <pre>----------------------------------------------------------------*
	 *     SELECT STATO OGGETTO WORK-FLOW (ID-OGGETTO/TP-OGGETTO)
	 * ----------------------------------------------------------------*</pre>*/
    private void s1020SelectStw() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'STAT-OGG-WF'          TO WK-TABELLA.
        ws.setWkTabella("STAT-OGG-WF");
        //--> VALORIZZAZIONE DELLA CHIAVE DI LETTURA
        // COB_CODE: MOVE WK-ID-OGG              TO STW-ID-OGG.
        ws.getStatOggWf().setStwIdOgg(ws.getWkIdOgg());
        // COB_CODE: MOVE WK-TP-OGG              TO STW-TP-OGG.
        ws.getStatOggWf().setStwTpOgg(ws.getWkTpOgg());
        //--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
        // COB_CODE: MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                          IDSI0011-DATA-FINE-EFFETTO
        //                                          IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-X-EFFETTO
        //                                       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattXEffetto();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE STAT-OGG-WF            TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStatOggWf().getStatOggWfFormatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID-OGGETTO    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *-->    GESTIONE ERRORE DB
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIONE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->    GESTIONE ERRORE DB
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-NOT-FOUND
            //                          CONTINUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--            OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            TO STAT-OGG-WF
            //                       WHEN OTHER
            //           *--            ERRORE DI ACCESSO AL DB
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: CONTINUE
                //continue
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--            OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO STAT-OGG-WF
                    ws.getStatOggWf().setStatOggWfFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default://--            ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1150-SELECT-STB'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1150-SELECT-STB");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    GESTIONE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1150-SELECT-STB'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1150-SELECT-STB");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1030-UPDATE-STW<br>
	 * <pre>----------------------------------------------------------------*
	 *     UPDATE STATO OGGETTO WORK-FLOW
	 * ----------------------------------------------------------------*</pre>*/
    private void s1030UpdateStw() {
        // COB_CODE: MOVE 'STAT-OGG-WF'           TO WK-TABELLA.
        ws.setWkTabella("STAT-OGG-WF");
        //--  CAMPI VARIATI DELLA STATO OGGETTO WORK FLOW
        // COB_CODE: MOVE WMOV-ID-PTF             TO STW-ID-MOVI-CRZ
        ws.getStatOggWf().setStwIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
        // COB_CODE: MOVE WK-STAT-OGG-WF          TO STW-STAT-OGG-WF
        ws.getStatOggWf().setStwStatOggWf(ws.getWkStatOggWf());
        // COB_CODE: MOVE WK-STAT-END-WF          TO STW-FL-STAT-END
        ws.getStatOggWf().setStwFlStatEnd(ws.getWkStatEndWf());
        //--> DATA EFFETTO
        // COB_CODE: MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(wmovAreaMovimento.getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
        //                                           IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //--> TABELLA STORICA
        // COB_CODE: SET  IDSI0011-TRATT-X-EFFETTO  TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattXEffetto();
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE STAT-OGG-WF               TO IDSI0011-BUFFER-DATI
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStatOggWf().getStatOggWfFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID               TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
        //--> CALL DISPATCHER PER AGGIORNAMENTO
        // COB_CODE: PERFORM AGGIORNA-TABELLA
        //              THRU AGGIORNA-TABELLA-EX.
        aggiornaTabella();
    }

    /**Original name: S1040-INSERT-STW<br>
	 * <pre>----------------------------------------------------------------*
	 *     INSERT STATO OGGETTO WORK-FLOW
	 * ----------------------------------------------------------------*</pre>*/
    private void s1040InsertStw() {
        // COB_CODE: MOVE 'STAT-OGG-WF'           TO WK-TABELLA.
        ws.setWkTabella("STAT-OGG-WF");
        //--> VALORIZZAZIONE DCLGEN STATO OGGETTO WORK FLOW
        // COB_CODE: PERFORM ESTR-SEQUENCE
        //              THRU ESTR-SEQUENCE-EX.
        estrSequence();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU AGGIORNA-TABELLA-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE S090-SEQ-TABELLA            TO STW-ID-STAT-OGG-WF
            ws.getStatOggWf().setStwIdStatOggWf(ws.getAreaIoLccs0090().getSeqTabella());
            // COB_CODE: MOVE WK-ID-OGG                   TO STW-ID-OGG
            ws.getStatOggWf().setStwIdOgg(ws.getWkIdOgg());
            // COB_CODE: MOVE WK-TP-OGG                   TO STW-TP-OGG
            ws.getStatOggWf().setStwTpOgg(ws.getWkTpOgg());
            // COB_CODE: MOVE WMOV-ID-PTF                 TO STW-ID-MOVI-CRZ
            ws.getStatOggWf().setStwIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
            // COB_CODE: MOVE HIGH-VALUE                  TO STW-ID-MOVI-CHIU-NULL
            ws.getStatOggWf().getStwIdMoviChiu().setStwIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StwIdMoviChiu.Len.STW_ID_MOVI_CHIU_NULL));
            // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO STW-COD-COMP-ANIA
            ws.getStatOggWf().setStwCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
            // COB_CODE: MOVE SPACES                      TO STW-COD-PRCS
            ws.getStatOggWf().setStwCodPrcs("");
            // COB_CODE: IF WK-TP-OGG = 'MO'
            //              MOVE 'MOV'                    TO STW-COD-PRCS
            //           END-IF
            if (Conditions.eq(ws.getWkTpOgg(), "MO")) {
                // COB_CODE: MOVE 'MOV'                    TO STW-COD-PRCS
                ws.getStatOggWf().setStwCodPrcs("MOV");
            }
            // COB_CODE: IF WK-TP-OGG = 'DE'
            //                TO STW-COD-PRCS
            //           END-IF
            if (Conditions.eq(ws.getWkTpOgg(), "DE")) {
                // COB_CODE: MOVE WCOM-COD-PROCESSO-WF
                //             TO STW-COD-PRCS
                ws.getStatOggWf().setStwCodPrcs(lccc0001.getDatiDeroghe().getCodProcessoWf());
            }
            // COB_CODE: MOVE 'ATT01'                     TO STW-COD-ATTVT
            ws.getStatOggWf().setStwCodAttvt("ATT01");
            // COB_CODE: MOVE WK-STAT-OGG-WF              TO STW-STAT-OGG-WF
            ws.getStatOggWf().setStwStatOggWf(ws.getWkStatOggWf());
            // COB_CODE: MOVE WK-STAT-END-WF              TO STW-FL-STAT-END
            ws.getStatOggWf().setStwFlStatEnd(ws.getWkStatEndWf());
            //-->    DATA EFFETTO
            // COB_CODE: MOVE WMOV-DT-EFF           TO IDSI0011-DATA-INIZIO-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(wmovAreaMovimento.getLccvmov1().getDati().getWmovDtEff());
            // COB_CODE: MOVE ZERO                  TO IDSI0011-DATA-FINE-EFFETTO
            //                                         IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
            //-->    TABELLA STORICA
            // COB_CODE: SET  IDSI0011-TRATT-X-EFFETTO  TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattXEffetto();
            //-->    DCLGEN TABELLA
            // COB_CODE: MOVE STAT-OGG-WF               TO IDSI0011-BUFFER-DATI
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStatOggWf().getStatOggWfFormatted());
            //-->    MODALITA DI ACCESSO
            // COB_CODE: SET  IDSI0011-ID               TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
            //-->    CALL DISPATCHER PER AGGIORNAMENTO
            // COB_CODE: PERFORM AGGIORNA-TABELLA
            //              THRU AGGIORNA-TABELLA-EX
            aggiornaTabella();
        }
    }

    /**Original name: S1060-AGGIORNA-DEROGHE<br>
	 * <pre>----------------------------------------------------------------*
	 *     AGGIORMENTO OGGETTO DEROGA E MOTIVI DEROGA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1060AggiornaDeroghe() {
        // COB_CODE: MOVE ZEROES                 TO WK-COD-GR-AUT-SUP
        ws.setWkCodGrAutSup(0);
        // COB_CODE: MOVE ZEROES                 TO WK-COD-LIV-AUT-SUP
        ws.setWkCodLivAutSup(0);
        // COB_CODE: IF WCOM-ID-OGG-DEROGA GREATER ZERO
        //                 THRU EX-S1061
        //           END-IF
        if (lccc0001.getDatiDeroghe().getOggettoDeroga().getIdOggDeroga() > 0) {
            // COB_CODE: PERFORM S1061-LEGGI-OGG-DEROGA
            //              THRU EX-S1061
            s1061LeggiOggDeroga();
        }
        //--> VALORIZZAZIONE OGGETTO DEROGA
        // COB_CODE: PERFORM S1070-VAL-AREA-ODE
        //              THRU EX-S1070
        s1070ValAreaOde();
        //--> AGGIORNA OGGETTO DEROGA
        // COB_CODE: PERFORM AGGIORNA-OGG-DEROGA
        //              THRU AGGIORNA-OGG-DEROGA-EX
        aggiornaOggDeroga();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                TO WCOM-ID-OGG-DEROGA
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE WODE-ID-PTF
            //             TO WCOM-ID-OGG-DEROGA
            lccc0001.getDatiDeroghe().getOggettoDeroga().setIdOggDeroga(ws.getWkVariabili().getLccvode1().getIdPtf());
        }
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *-->    VALORIZZAZIONE MOTIVI DEROGA
        //                  END-PERFORM
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //-->    VALORIZZAZIONE MOTIVI DEROGA
            // COB_CODE: PERFORM VARYING IX-TAB-MDE FROM 1 BY 1
            //                     UNTIL IX-TAB-MDE > WCOM-NUM-ELE-MOT-DEROGA
            //                 THRU EX-S1080
            //           END-PERFORM
            ws.getWsIndici().setMde(((short)1));
            while (!(ws.getWsIndici().getMde() > lccc0001.getDatiDeroghe().getNumEleMotDeroga())) {
                // COB_CODE: PERFORM S1080-VAL-AREA-MDE
                //              THRU EX-S1080
                s1080ValAreaMde();
                ws.getWsIndici().setMde(Trunc.toShort(ws.getWsIndici().getMde() + 1, 4));
            }
            //-->    AGGIORNA MOTIVI DEROGA
            // COB_CODE:  PERFORM VARYING IX-TAB-MDE FROM 1 BY 1
            //                      UNTIL IX-TAB-MDE > WMDE-ELE-MDE-MAX
            //                     THRU AGGIORNA-MOT-DEROGA-EX
            //           END-PERFORM
            ws.getWsIndici().setMde(((short)1));
            while (!(ws.getWsIndici().getMde() > ws.getWkVariabili().getWmdeEleMdeMax())) {
                // COB_CODE: PERFORM AGGIORNA-MOT-DEROGA
                //              THRU AGGIORNA-MOT-DEROGA-EX
                aggiornaMotDeroga();
                ws.getWsIndici().setMde(Trunc.toShort(ws.getWsIndici().getMde() + 1, 4));
            }
        }
    }

    /**Original name: S1061-LEGGI-OGG-DEROGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     LEGGI OGGETTO DEROGA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1061LeggiOggDeroga() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'OGG-DEROGA'           TO WK-TABELLA.
        ws.setWkTabella("OGG-DEROGA");
        //--> VALORIZZAZIONE DELLA CHIAVE DI LETTURA
        // COB_CODE: MOVE WCOM-ID-OGG-DEROGA     TO ODE-ID-OGG-DEROGA
        ws.getOggDeroga().setOdeIdOggDeroga(lccc0001.getDatiDeroghe().getOggettoDeroga().getIdOggDeroga());
        //--> LA DATA EFFETTO VIENE SEMPRE VALORIZZATA
        // COB_CODE: MOVE ZERO                   TO IDSI0011-DATA-INIZIO-EFFETTO
        //                                          IDSI0011-DATA-FINE-EFFETTO
        //                                          IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-X-EFFETTO
        //                                       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattXEffetto();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE WK-TABELLA             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE OGG-DEROGA             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getOggDeroga().getOggDerogaFormatted());
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSI0011-SELECT         TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *-->    GESTIRE ERRORE DB
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIRE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->    GESTIRE ERRORE DB
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-NOT-FOUND
            //           *-->           GESTIRE ERRORE DISPATCHER
            //                             THRU EX-S0300
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--            OPERAZIONE ESEGUITA CORRETTAMENTE
            //                          MOVE ODE-COD-LIV-AUT-SUP  TO WK-COD-LIV-AUT-SUP
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://-->           GESTIRE ERRORE DISPATCHER
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1061-LEGGI-OGG-DEROGA'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1061-LEGGI-OGG-DEROGA");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--            OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO OGG-DEROGA
                    ws.getOggDeroga().setOggDerogaFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: MOVE ODE-COD-GR-AUT-SUP   TO WK-COD-GR-AUT-SUP
                    ws.setWkCodGrAutSup(ws.getOggDeroga().getOdeCodGrAutSup());
                    // COB_CODE: MOVE ODE-COD-LIV-AUT-SUP  TO WK-COD-LIV-AUT-SUP
                    ws.setWkCodLivAutSup(ws.getOggDeroga().getOdeCodLivAutSup());
                    break;

                default:break;
            }
        }
        else {
            //-->    GESTIRE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1061-LEGGI-OGG-DEROGA'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1061-LEGGI-OGG-DEROGA");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1070-VAL-AREA-ODE<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE OGGETTO DEROGA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1070ValAreaOde() {
        // COB_CODE: INITIALIZE WODE-AREA-OGG-DEROGA
        initWodeAreaOggDeroga();
        // COB_CODE: MOVE WCOM-ID-OGG-DEROGA           TO WODE-ID-OGG-DEROGA
        //                                                WODE-ID-PTF
        ws.getWkVariabili().getLccvode1().getDati().setWodeIdOggDeroga(lccc0001.getDatiDeroghe().getOggettoDeroga().getIdOggDeroga());
        ws.getWkVariabili().getLccvode1().setIdPtf(lccc0001.getDatiDeroghe().getOggettoDeroga().getIdOggDeroga());
        // COB_CODE: IF WODE-ID-OGG-DEROGA = ZERO
        //              SET WODE-ST-ADD                TO TRUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.getWkVariabili().getLccvode1().getDati().getWodeIdOggDeroga() == 0) {
            // COB_CODE: SET WODE-ST-ADD                TO TRUE
            ws.getWkVariabili().getLccvode1().getStatus().setWcomStAdd();
        }
        else if (lccc0001.getStatusDer().isWcomStDeMod()) {
            // COB_CODE: IF WCOM-ST-DE-MOD
            //              MOVE WMOV-ID-PTF            TO WODE-ID-MOVI-CRZ
            //           ELSE
            //              SET WODE-ST-INV             TO TRUE
            //           END-IF
            // COB_CODE: SET WODE-ST-MOD             TO TRUE
            ws.getWkVariabili().getLccvode1().getStatus().setMod();
            // COB_CODE: MOVE WMOV-ID-PTF            TO WODE-ID-MOVI-CRZ
            ws.getWkVariabili().getLccvode1().getDati().setWodeIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
        }
        else {
            // COB_CODE: SET WODE-ST-INV             TO TRUE
            ws.getWkVariabili().getLccvode1().getStatus().setInv();
        }
        // COB_CODE: IF WPOL-TP-FRM-ASSVA = TP-INDIVIDUALE
        //              MOVE WS-POLIZZA                TO WODE-TP-OGG
        //           ELSE
        //              END-IF
        //           END-IF
        if (Conditions.eq(wpolAreaPolizza.getLccvpol1().getDati().getWpolTpFrmAssva(), ws.getTpIndividuale())) {
            // COB_CODE: MOVE WPOL-ID-PTF               TO WODE-ID-OGG
            ws.getWkVariabili().getLccvode1().getDati().setWodeIdOgg(wpolAreaPolizza.getLccvpol1().getIdPtf());
            // COB_CODE: MOVE WS-POLIZZA                TO WODE-TP-OGG
            ws.getWkVariabili().getLccvode1().getDati().setWodeTpOgg(ws.getWsTpOgg().getPolizza());
        }
        else if (Conditions.eq(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpOgg(), ws.getWsTpOgg().getPolizza()) || Conditions.eq(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpOgg(), ws.getWsTpOgg().getAdesione())) {
            // COB_CODE: IF WMOV-TP-OGG = WS-POLIZZA OR WS-ADESIONE
            //              MOVE WMOV-TP-OGG            TO WODE-TP-OGG
            //           ELSE
            //              MOVE WS-ADESIONE            TO WODE-TP-OGG
            //           END-IF
            // COB_CODE: MOVE WMOV-ID-OGG            TO WODE-ID-OGG
            ws.getWkVariabili().getLccvode1().getDati().setWodeIdOgg(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdOgg().getWmovIdOgg());
            // COB_CODE: MOVE WMOV-TP-OGG            TO WODE-TP-OGG
            ws.getWkVariabili().getLccvode1().getDati().setWodeTpOgg(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpOgg());
        }
        else {
            // COB_CODE: MOVE WADE-ID-PTF            TO WODE-ID-OGG
            ws.getWkVariabili().getLccvode1().getDati().setWodeIdOgg(wadeAreaAdesione.getLccvade1().getIdPtf());
            // COB_CODE: MOVE WS-ADESIONE            TO WODE-TP-OGG
            ws.getWkVariabili().getLccvode1().getDati().setWodeTpOgg(ws.getWsTpOgg().getAdesione());
        }
        // COB_CODE: MOVE HIGH-VALUE                   TO WODE-ID-MOVI-CHIU-NULL
        ws.getWkVariabili().getLccvode1().getDati().getWodeIdMoviChiu().setWodeIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WodeIdMoviChiu.Len.WODE_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA  TO WODE-COD-COMP-ANIA
        ws.getWkVariabili().getLccvode1().getDati().setWodeCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE WCOM-IB-OGG                  TO WODE-IB-OGG
        ws.getWkVariabili().getLccvode1().getDati().setWodeIbOgg(lccc0001.getDatiDeroghe().getOggettoDeroga().getIbOgg());
        // COB_CODE: MOVE WCOM-TIPO-DEROGA             TO WODE-TP-DEROGA
        ws.getWkVariabili().getLccvode1().getDati().setWodeTpDeroga(lccc0001.getDatiDeroghe().getOggettoDeroga().getTipoDeroga());
        // COB_CODE: IF WCOM-ST-DE-MOD
        //              MOVE ODE-COD-LIV-AUT-APPRT     TO WODE-COD-LIV-AUT-APPRT
        //           ELSE
        //              MOVE WCOM-COD-LIV-AUT-APPARTZA TO WODE-COD-LIV-AUT-APPRT
        //           END-IF.
        if (lccc0001.getStatusDer().isWcomStDeMod()) {
            // COB_CODE: MOVE ODE-COD-GR-AUT-SUP        TO WODE-COD-GR-AUT-SUP
            ws.getWkVariabili().getLccvode1().getDati().setWodeCodGrAutSup(ws.getOggDeroga().getOdeCodGrAutSup());
            // COB_CODE: MOVE ODE-COD-LIV-AUT-SUP       TO WODE-COD-LIV-AUT-SUP
            ws.getWkVariabili().getLccvode1().getDati().setWodeCodLivAutSup(ws.getOggDeroga().getOdeCodLivAutSup());
            // COB_CODE: MOVE ODE-COD-GR-AUT-APPRT      TO WODE-COD-GR-AUT-APPRT
            ws.getWkVariabili().getLccvode1().getDati().setWodeCodGrAutApprt(ws.getOggDeroga().getOdeCodGrAutApprt());
            // COB_CODE: MOVE ODE-COD-LIV-AUT-APPRT     TO WODE-COD-LIV-AUT-APPRT
            ws.getWkVariabili().getLccvode1().getDati().getWodeCodLivAutApprt().setWodeCodLivAutApprt(ws.getOggDeroga().getOdeCodLivAutApprt().getOdeCodLivAutApprt());
        }
        else {
            // COB_CODE: MOVE WCOM-COD-GRU-AUT-APPARTZA TO WODE-COD-GR-AUT-APPRT
            ws.getWkVariabili().getLccvode1().getDati().setWodeCodGrAutApprt(lccc0001.getDatiDeroghe().getOggettoDeroga().getCodGruAutAppartza());
            // COB_CODE: MOVE WCOM-COD-LIV-AUT-APPARTZA TO WODE-COD-LIV-AUT-APPRT
            ws.getWkVariabili().getLccvode1().getDati().getWodeCodLivAutApprt().setWodeCodLivAutApprt(lccc0001.getDatiDeroghe().getOggettoDeroga().getCodLivAutAppartza());
        }
        // COB_CODE: IF (WK-COD-GR-AUT-SUP  NOT EQUAL WCOM-COD-GRU-AUT-SUPER)
        //           OR (WK-COD-LIV-AUT-SUP NOT EQUAL WCOM-COD-LIV-AUT-SUPER)
        //              END-IF
        //           END-IF
        if (ws.getWkCodGrAutSup() != lccc0001.getDatiDeroghe().getOggettoDeroga().getCodGruAutSuper() || ws.getWkCodLivAutSup() != lccc0001.getDatiDeroghe().getOggettoDeroga().getCodLivAutSuper()) {
            // COB_CODE: MOVE WCOM-COD-GRU-AUT-SUPER   TO WODE-COD-GR-AUT-SUP
            ws.getWkVariabili().getLccvode1().getDati().setWodeCodGrAutSup(lccc0001.getDatiDeroghe().getOggettoDeroga().getCodGruAutSuper());
            // COB_CODE: MOVE WCOM-COD-LIV-AUT-SUPER   TO WODE-COD-LIV-AUT-SUP
            ws.getWkVariabili().getLccvode1().getDati().setWodeCodLivAutSup(lccc0001.getDatiDeroghe().getOggettoDeroga().getCodLivAutSuper());
            // COB_CODE: IF NOT WODE-ST-ADD
            //              MOVE WMOV-ID-PTF           TO WODE-ID-MOVI-CRZ
            //           END-IF
            if (!ws.getWkVariabili().getLccvode1().getStatus().isAdd()) {
                // COB_CODE: SET WODE-ST-MOD            TO TRUE
                ws.getWkVariabili().getLccvode1().getStatus().setMod();
                // COB_CODE: MOVE WMOV-ID-PTF           TO WODE-ID-MOVI-CRZ
                ws.getWkVariabili().getLccvode1().getDati().setWodeIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
            }
        }
        // COB_CODE: MOVE WCOM-DS-VER                 TO WODE-DS-VER
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsVer(lccc0001.getDatiDeroghe().getOggettoDeroga().getDsVer());
        // COB_CODE: ADD 1 TO WODE-ELE-ODE-MAX.
        ws.getWkVariabili().setWodeEleOdeMax(Trunc.toShort(1 + ws.getWkVariabili().getWodeEleOdeMax(), 4));
    }

    /**Original name: S1080-VAL-AREA-MDE<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE MOTIVI DEROGA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1080ValAreaMde() {
        // COB_CODE: IF WCOM-ST-DE-MOD
        //                TO WMDE-ID-PTF  (IX-TAB-MDE)
        //           END-IF
        if (lccc0001.getStatusDer().isWcomStDeMod()) {
            // COB_CODE: MOVE WCOM-ID-MOT-DEROGA  (IX-TAB-MDE)
            //             TO WMDE-ID-PTF  (IX-TAB-MDE)
            ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().setIdPtf(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getWsIndici().getMde()).getIdMotDeroga());
        }
        // COB_CODE: MOVE WCOM-ID-MOT-DEROGA  (IX-TAB-MDE)
        //             TO WMDE-ID-MOT-DEROGA  (IX-TAB-MDE)
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().setWmdeIdMotDeroga(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getWsIndici().getMde()).getIdMotDeroga());
        // COB_CODE: MOVE WODE-ID-PTF
        //             TO WMDE-ID-OGG-DEROGA  (IX-TAB-MDE)
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().setWmdeIdOggDeroga(ws.getWkVariabili().getLccvode1().getIdPtf());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO WMDE-COD-COMP-ANIA  (IX-TAB-MDE)
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().setWmdeCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE WCOM-TP-MOT-DEROGA  (IX-TAB-MDE)
        //             TO WMDE-TP-MOT-DEROGA  (IX-TAB-MDE)
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().setWmdeTpMotDeroga(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getWsIndici().getMde()).getTpMotDeroga());
        // COB_CODE: MOVE WCOM-COD-LIV-AUTORIZZATIVO (IX-TAB-MDE)
        //             TO WMDE-COD-LIV-AUT           (IX-TAB-MDE)
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().setWmdeCodLivAut(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getWsIndici().getMde()).getCodLivAutorizzativo());
        // COB_CODE: MOVE WCOM-COD-ERR        (IX-TAB-MDE)
        //             TO WMDE-COD-ERR        (IX-TAB-MDE)
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().setWmdeCodErr(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getWsIndici().getMde()).getCodErr());
        // COB_CODE: MOVE WCOM-TP-ERR         (IX-TAB-MDE)
        //             TO WMDE-TP-ERR         (IX-TAB-MDE)
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().setWmdeTpErr(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getWsIndici().getMde()).getTpErr());
        // COB_CODE: MOVE WCOM-DESCRIZIONE-ERR (IX-TAB-MDE)
        //             TO WMDE-DESC-ERR-BREVE  (IX-TAB-MDE)
        //                WMDE-DESC-ERR-EST    (IX-TAB-MDE)
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().setWmdeDescErrBreve(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getWsIndici().getMde()).getDescrizioneErr());
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().setWmdeDescErrEst(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getWsIndici().getMde()).getDescrizioneErr());
        // COB_CODE: MOVE WCOM-STATUS        (IX-TAB-MDE)
        //             TO WMDE-STATUS        (IX-TAB-MDE).
        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getStatus().setStatus(lccc0001.getDatiDeroghe().getTabMotDeroga(ws.getWsIndici().getMde()).getStatus().getStatus());
        // COB_CODE: ADD 1 TO WMDE-ELE-MDE-MAX.
        ws.getWkVariabili().setWmdeEleMdeMax(Trunc.toShort(1 + ws.getWkVariabili().getWmdeEleMdeMax(), 4));
    }

    /**Original name: S1090-AGGIORNA-WORK-FLOW-DE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE STATO OGGETTO WORK FLOW DEROGA
	 * ----------------------------------------------------------------*
	 * --> LETTURA STATO OGGETTO WORK-FLOW - OGGETTO DEROGA</pre>*/
    private void s1090AggiornaWorkFlowDe() {
        // COB_CODE: MOVE WODE-ID-PTF            TO WK-ID-OGG
        ws.setWkIdOgg(ws.getWkVariabili().getLccvode1().getIdPtf());
        // COB_CODE: MOVE 'DE'                   TO WK-TP-OGG
        ws.setWkTpOgg("DE");
        // COB_CODE: PERFORM S1020-SELECT-STW
        //              THRU EX-S1020
        s1020SelectStw();
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *-->    IMPOSTAZIONE DELLO STATO WORK FLOW - OGGETTO DEROGA
        //                   END-EVALUATE
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //-->    IMPOSTAZIONE DELLO STATO WORK FLOW - OGGETTO DEROGA
            // COB_CODE: MOVE WCOM-STATO-OGG-DEROGA   TO WK-STAT-OGG-WF
            ws.setWkStatOggWf(lccc0001.getStati().getStatoOggDeroga());
            // COB_CODE: MOVE WCOM-DER-FLAG-ST-FINALE TO WK-STAT-END-WF
            ws.setWkStatEndWf(lccc0001.getStati().getDerFlagStFinale().getDerFlagStFinale());
            //*--      OCCORRENZA NON TROVATA (SQLCODE = +100)
            // COB_CODE:         EVALUATE TRUE
            //           **--      OCCORRENZA NON TROVATA (SQLCODE = +100)
            //                     WHEN IDSO0011-NOT-FOUND
            //           *-->         TIPO OPERAZIONE
            //                           THRU EX-S1040
            //           *-->      OCCORRENZA TROVATA (SQLCODE = +0)
            //                     WHEN IDSO0011-SUCCESSFUL-SQL
            //                           THRU EX-S1030
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://-->         TIPO OPERAZIONE
                    // COB_CODE: SET IDSI0011-AGGIORNAMENTO-STORICO   TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    // COB_CODE: PERFORM S1040-INSERT-STW
                    //              THRU EX-S1040
                    s1040InsertStw();
                    //-->      OCCORRENZA TROVATA (SQLCODE = +0)
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: PERFORM S1030-UPDATE-STW
                    //              THRU EX-S1030
                    s1030UpdateStw();
                    break;

                default:break;
            }
        }
    }

    /**Original name: S1100-AGGIORNA-STATO-RIC-EST<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE STATO RICHIESTA ESTERNA
	 * ----------------------------------------------------------------*
	 *  --> LETTURA STATO RICHIESTA ESTERNA</pre>*/
    private void s1100AggiornaStatoRicEst() {
        // COB_CODE: PERFORM S1120-SELECT-STATO-RIC
        //              THRU EX-S1120.
        s1120SelectStatoRic();
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *-->    IMPOSTAZIONE DELLO STATO RICHIESTA ESTERNA
        //           *-->    INSERISCO
        //                   END-EVALUATE
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //-->    IMPOSTAZIONE DELLO STATO RICHIESTA ESTERNA
            //-->    INSERISCO
            // COB_CODE:         EVALUATE TRUE
            //                     WHEN IDSO0011-NOT-FOUND
            //                           THRU EX-S1140
            //           *-->    CHIUDO ULTIMO STATO RICHIESTA ESTERNA
            //           *-->    INSERISCO ULTIMO STATO APERTO DELLA RICHIESTA
            //                     WHEN IDSO0011-SUCCESSFUL-SQL
            //                        END-IF
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: PERFORM S1140-INSERT-STATO-RIC
                    //              THRU EX-S1140
                    s1140InsertStatoRic();
                    //-->    CHIUDO ULTIMO STATO RICHIESTA ESTERNA
                    //-->    INSERISCO ULTIMO STATO APERTO DELLA RICHIESTA
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: PERFORM S1130-UPDATE-STATO-RIC
                    //              THRU EX-S1130
                    s1130UpdateStatoRic();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //                 THRU EX-S1140
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: PERFORM S1140-INSERT-STATO-RIC
                        //              THRU EX-S1140
                        s1140InsertStatoRic();
                    }
                    break;

                default:break;
            }
        }
    }

    /**Original name: S1120-SELECT-STATO-RIC<br>
	 * <pre>----------------------------------------------------------------*
	 *     SELECT STATO DELLA RICHIESTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1120SelectStatoRic() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'STAT-RICH-EST'            TO WK-TABELLA.
        ws.setWkTabella("STAT-RICH-EST");
        // COB_CODE: MOVE ZERO                       TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-WHERE-CONDITION    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSI0011-SELECT             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: MOVE WCOM-ID-RICH-EST           TO P04-ID-RICH-EST.
        ws.getStatRichEst().setP04IdRichEst(lccc0001.getIdRichEst());
        // COB_CODE: MOVE LDBS8170                   TO IDSI0011-CODICE-STR-DATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getLdbs8170());
        // COB_CODE: MOVE STAT-RICH-EST              TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStatRichEst().getStatRichEstFormatted());
        // COB_CODE: MOVE SPACES                     TO IDSI0011-BUFFER-WHERE-COND
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond("");
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *-->    GESTIONE ERRORE DB
        //                   END-EVALUATE
        //                ELSE
        //           *-->    GESTIONE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->    GESTIONE ERRORE DB
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-NOT-FOUND
            //                          CONTINUE
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--            OPERAZIONE ESEGUITA CORRETTAMENTE
            //                            TO STAT-RICH-EST
            //                       WHEN OTHER
            //           *--            ERRORE DI ACCESSO AL DB
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: CONTINUE
                //continue
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--            OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO STAT-RICH-EST
                    ws.getStatRichEst().setStatRichEstFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default://--            ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1120-SELECT-STATO-RIC'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1120-SELECT-STATO-RIC");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    GESTIONE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1120-SELECT-STATO-RIC'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1120-SELECT-STATO-RIC");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1130-UPDATE-STATO-RIC<br>
	 * <pre>----------------------------------------------------------------*
	 *     UPDATE STATO DELLA RICHIESTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1130UpdateStatoRic() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'STAT-RICH-EST'             TO WK-TABELLA.
        ws.setWkTabella("STAT-RICH-EST");
        //     MOVE WCOM-STATO-RICHIESTA-EST    TO P04-STAT-RICH-EST
        // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA    TO P04-TS-END-VLDT.
        ws.getStatRichEst().setP04TsEndVldt(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: MOVE STAT-RICH-EST               TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStatRichEst().getStatRichEstFormatted());
        // COB_CODE: SET  IDSI0011-UPDATE             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Update();
        // COB_CODE: SET  IDSI0011-PRIMARY-KEY        TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: MOVE WK-TABELLA                  TO
        //                                               IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                      END-EVALUATE
        //                ELSE
        //           *-->    GESTIONE ERRORE DISPATCHER
        //                         THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN IDSO0011-SUCCESSFUL-SQL
            //                   CONTINUE
            //              WHEN OTHER
            //                      THRU EX-S0300
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: MOVE WK-PGM      TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1130-UPDATE-STATO-RIC'
                    //                            TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1130-UPDATE-STATO-RIC");
                    // COB_CODE: MOVE '005016'    TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: MOVE SPACES      TO IEAI9901-PARAMETRI-ERR
                    //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                    // COB_CODE: STRING IDBSP040             ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getIdbsp040Formatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    GESTIONE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1130-UPDATE-STATO-RIC'
            //                                 TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1130-UPDATE-STATO-RIC");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1140-INSERT-STATO-RIC<br>
	 * <pre>----------------------------------------------------------------*
	 *     INSERT STATO DELLA RICHIESTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1140InsertStatoRic() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF IDSV0001-MAX-ELE-ERRORI  >  0
        //              CONTINUE
        //           ELSE
        //                                             P04-COD-ERR-SCARTO
        //           END-IF
        if (areaIdsv0001.getMaxEleErrori() > 0) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE SPACES                 TO P04-DESC-ERR
            //                                          P04-COD-ERR-SCARTO
            ws.getStatRichEst().setP04DescErr("");
            ws.getStatRichEst().setP04CodErrScarto("");
        }
        // COB_CODE: MOVE HIGH-VALUE                TO P04-TP-CAUS-SCARTO-NULL.
        ws.getStatRichEst().setP04TpCausScarto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StatRichEst.Len.P04_TP_CAUS_SCARTO));
        // COB_CODE: MOVE 'STAT-RICH-EST'           TO WK-TABELLA.
        ws.setWkTabella("STAT-RICH-EST");
        //--> VALORIZZAZIONE DCLGEN STATO DELLA RICHIESTA
        // COB_CODE: PERFORM ESTR-SEQUENCE
        //              THRU ESTR-SEQUENCE-EX.
        estrSequence();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE S090-SEQ-TABELLA            TO P04-ID-STAT-RICH-EST
            ws.getStatRichEst().setP04IdStatRichEst(ws.getAreaIoLccs0090().getSeqTabella());
            // COB_CODE: MOVE WCOM-ID-RICH-EST            TO P04-ID-RICH-EST
            ws.getStatRichEst().setP04IdRichEst(lccc0001.getIdRichEst());
            // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO P04-COD-COMP-ANIA
            ws.getStatRichEst().setP04CodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
            // COB_CODE: MOVE 'GRE'                       TO P04-COD-PRCS
            ws.getStatRichEst().setP04CodPrcs("GRE");
            // COB_CODE: MOVE WCOM-COD-ATTIVITA-WF        TO P04-COD-ATTVT
            ws.getStatRichEst().setP04CodAttvt(lccc0001.getDatiDeroghe().getCodAttivitaWf());
            // COB_CODE: MOVE WCOM-STATO-RICHIESTA-EST    TO P04-STAT-RICH-EST
            ws.getStatRichEst().setP04StatRichEst(lccc0001.getStati().getStatoRichiestaEst());
            // COB_CODE: MOVE WMOV-ID-PTF                 TO P04-ID-MOVI-CRZ
            ws.getStatRichEst().getP04IdMoviCrz().setP04IdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
            // COB_CODE: IF WCOM-STATO-RICHIESTA-EST = 'DE'
            //              MOVE WS-TP-CAUS-SCARTO        TO P04-TP-CAUS-SCARTO
            //           END-IF
            if (Conditions.eq(lccc0001.getStati().getStatoRichiestaEst(), "DE")) {
                // COB_CODE: SET SCARTO-DEROGHE            TO TRUE
                ws.getWsTpCausScarto().setScartoDeroghe();
                // COB_CODE: MOVE WS-TP-CAUS-SCARTO        TO P04-TP-CAUS-SCARTO
                ws.getStatRichEst().setP04TpCausScarto(ws.getWsTpCausScarto().getWsTpCausScarto());
            }
            // COB_CODE: MOVE IDSV0001-USER-NAME          TO P04-UTENTE-INS-AGG
            ws.getStatRichEst().setP04UtenteInsAgg(areaIdsv0001.getAreaComune().getUserName());
            // COB_CODE: MOVE WCOM-RIC-FLAG-ST-FINALE     TO P04-FL-STAT-END
            ws.getStatRichEst().setP04FlStatEnd(lccc0001.getStati().getRicFlagStFinale().getRicFlagStFinale());
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA    TO P04-TS-INI-VLDT
            ws.getStatRichEst().setP04TsIniVldt(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
            // COB_CODE: MOVE WS-TS-INFINITO              TO P04-TS-END-VLDT
            ws.getStatRichEst().setP04TsEndVldt(ws.getIdsv0010().getWsTsInfinito());
            // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR   TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
            // COB_CODE: SET  IDSI0011-INSERT             TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
            // COB_CODE: SET  IDSI0011-PRIMARY-KEY        TO TRUE
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
            // COB_CODE: MOVE WK-TABELLA                  TO
            //                                             IDSI0011-CODICE-STR-DATO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
            // COB_CODE: MOVE  STAT-RICH-EST              TO IDSI0011-BUFFER-DATI
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getStatRichEst().getStatRichEstFormatted());
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:         IF IDSO0011-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                   ELSE
            //           *-->    GESTIONE ERRORE DISPATCHER
            //                         THRU EX-S0300
            //                   END-IF
            if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSO0011-SUCCESSFUL-SQL
                //                   CONTINUE
                //              WHEN OTHER
                //                      THRU EX-S0300
                //           END-EVALUATE
                switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                    case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                    //continue
                        break;

                    default:// COB_CODE: MOVE WK-PGM      TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'S1140-INSERT-STATO-RIC'
                        //                            TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("S1140-INSERT-STATO-RIC");
                        // COB_CODE: MOVE '005016'    TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005016");
                        // COB_CODE: MOVE SPACES      TO IEAI9901-PARAMETRI-ERR
                        //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                        // COB_CODE: STRING IDBSP040             ';'
                        //                  IDSO0011-RETURN-CODE ';'
                        //                  IDSO0011-SQLCODE
                        //                  DELIMITED BY SIZE
                        //                  INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getIdbsp040Formatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                        break;
                }
            }
            else {
                //-->    GESTIONE ERRORE DISPATCHER
                // COB_CODE: MOVE WK-PGM
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'S1140-INSERT-STATO-RIC'
                //                                 TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S1140-INSERT-STATO-RIC");
                // COB_CODE: MOVE '005016'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005016");
                // COB_CODE: STRING WK-TABELLA           ';'
                //                  IDSO0011-RETURN-CODE ';'
                //                  IDSO0011-SQLCODE
                //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: S1200-AGGIORNA-RIC-EST<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE RICHIESTA ESTERNA
	 * ----------------------------------------------------------------*
	 *  --> LETTURA RICHIESTA ESTERNA</pre>*/
    private void s1200AggiornaRicEst() {
        // COB_CODE: PERFORM S1220-SELECT-RIC
        //              THRU EX-S1220.
        s1220SelectRic();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1230
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1230-UPDATE-RIC
            //              THRU EX-S1230
            s1230UpdateRic();
        }
    }

    /**Original name: S1220-SELECT-RIC<br>
	 * <pre>----------------------------------------------------------------*
	 *     SELECT RICHIESTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1220SelectRic() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'RICH-EST'            TO WK-TABELLA.
        ws.setWkTabella("RICH-EST");
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-SELECT             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY        TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: MOVE WCOM-ID-RICH-EST           TO P01-ID-RICH-EST.
        ws.getRichEst().setP01IdRichEst(lccc0001.getIdRichEst());
        // COB_CODE: MOVE 'RICH-EST'                 TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("RICH-EST");
        // COB_CODE: MOVE RICH-EST                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRichEst().getRichEstFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *-->    GESTIONE ERRORE DB
        //                       END-EVALUATE
        //                ELSE
        //           *-->    GESTIONE ERRORE DISPATCHER
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->    GESTIONE ERRORE DB
            // COB_CODE:         EVALUATE TRUE
            //                       WHEN IDSO0011-NOT-FOUND
            //                               THRU EX-S0300
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *--            OPERAZIONE ESEGUITA CORRETTAMENTE
            //                              TO RICH-EST
            //                       WHEN OTHER
            //           *--              ERRORE DI ACCESSO AL DB
            //                               THRU EX-S0300
            //                       END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND:// COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1220-SELECT-RIC'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1220-SELECT-RIC");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://--            OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO RICH-EST
                    ws.getRichEst().setRichEstFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    break;

                default://--              ERRORE DI ACCESSO AL DB
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1220-SELECT-RIC'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1220-SELECT-RIC");
                    // COB_CODE: MOVE '005016'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA           ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE INTO
                    //                  IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    GESTIONE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1220-SELECT-RIC'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1220-SELECT-RIC");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1230-UPDATE-RIC<br>
	 * <pre>----------------------------------------------------------------*
	 *     UPDATE RICHIESTA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1230UpdateRic() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'RICH-EST'                  TO WK-TABELLA.
        ws.setWkTabella("RICH-EST");
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR   TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-UPDATE             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Update();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY        TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        // COB_CODE: MOVE WCOM-ID-RICH-EST           TO P01-ID-RICH-EST.
        ws.getRichEst().setP01IdRichEst(lccc0001.getIdRichEst());
        // COB_CODE: MOVE 'RICH-EST'                 TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("RICH-EST");
        // COB_CODE: MOVE RICH-EST                   TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRichEst().getRichEstFormatted());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                      END-EVALUATE
        //                ELSE
        //           *-->    GESTIONE ERRORE DISPATCHER
        //                         THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN IDSO0011-SUCCESSFUL-SQL
            //                   CONTINUE
            //              WHEN OTHER
            //                      THRU EX-S0300
            //           END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                    break;

                default:// COB_CODE: MOVE WK-PGM      TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1230-UPDATE-RIC'
                    //                            TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1230-UPDATE-RIC");
                    // COB_CODE: MOVE '005016'    TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: MOVE SPACES      TO IEAI9901-PARAMETRI-ERR
                    //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                    // COB_CODE: STRING IDBSP010             ';'
                    //                  IDSO0011-RETURN-CODE ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getIdbsp010Formatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->    GESTIONE ERRORE DISPATCHER
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1230-UPDATE-RIC'
            //                                 TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1230-UPDATE-RIC");
            // COB_CODE: MOVE '005016'
            //             TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA           ';'
            //                  IDSO0011-RETURN-CODE ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: CALL-LCCS0234<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALL SERVIZIO ESTRAZIONE OGGETTO PTF LCCS0234
	 * ----------------------------------------------------------------*</pre>*/
    private void callLccs0234() {
        Lccs0234 lccs0234 = null;
        // COB_CODE: CALL LCCS0234 USING AREA-IDSV0001
        //                               WPOL-AREA-POLIZZA
        //                               WADE-AREA-ADESIONE
        //                               WGRZ-AREA-GARANZIA
        //                               WTGA-AREA-TRANCHE
        //                               S234-DATI-INPUT
        //                               S234-DATI-OUTPUT
        //             ON EXCEPTION
        //                     THRU EX-S0290
        //           END-CALL.
        try {
            lccs0234 = Lccs0234.getInstance();
            lccs0234.run(new Object[] {areaIdsv0001, wpolAreaPolizza, wadeAreaAdesione, wgrzAreaGaranzia, wtgaAreaTranche, ws.getS234DatiInput(), ws.getS234DatiOutput()});
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'IL REPERIMENTO ID PORTAFOGLIO'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("IL REPERIMENTO ID PORTAFOGLIO");
            // COB_CODE: MOVE 'S1100-CALL-LCCS0234'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1100-CALL-LCCS0234");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: B020-ESTRAI-DT-COMPETENZA<br>
	 * <pre>----------------------------------------------------------------*
	 *  ESTRAZIONE DATA COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void b020EstraiDtCompetenza() {
        ConcatUtil concatUtil = null;
        Idss0150 idss0150 = null;
        // COB_CODE: CALL PGM-IDSS0150               USING AREA-IDSV0001
        //           ON EXCEPTION
        //                  PERFORM S0290-ERRORE-DI-SISTEMA     THRU EX-S0290
        //           END-CALL.
        try {
            idss0150 = Idss0150.getInstance();
            idss0150.run(areaIdsv0001);
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'ERRORE CHIAMATA MODULO : '
            //               DELIMITED BY SIZE
            //               PGM-IDSS0150
            //               DELIMITED BY SIZE
            //               ' - '
            //                DELIMITED BY SIZE INTO
            //               CALL-DESC
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0002.Len.CALL_DESC, "ERRORE CHIAMATA MODULO : ", ws.getPgmIdss0150Formatted(), " - ");
            ws.getIdsv0002().setCallDesc(concatUtil.replaceInString(ws.getIdsv0002().getCallDescFormatted()));
            // COB_CODE: MOVE 'B020-ESTRAI-DT-COMPETENZA'
            //                                       TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("B020-ESTRAI-DT-COMPETENZA");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA     THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>---------------------------------------------------------------
	 *     OPERAZIONI FINALI
	 * ---------------------------------------------------------------</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VALORIZZA-AREE-S0088<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES PER AREA DINAMICA IDBV0161.
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    PROCESSO DI VENDITA - PORTAFOGLIO VITA
	 *    AREA VALORIZZAZIONE AREA VARIABILE DI INPUT
	 *    SERVIZIO ESTRAZIONE NUMERAZIONE OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void valorizzaAreeS0088() {
        // COB_CODE: MOVE ZERO                        TO (SF)-IX-CONTA.
        ws.getAreaEstrazioneIb().getDatiInput().setIxConta(0);
        // COB_CODE: MOVE 1                           TO (SF)-APPO-LUNGHEZZA.
        ws.getAreaEstrazioneIb().getDatiInput().setAppoLunghezza(1);
        //--> POLIZZA
        // COB_CODE: IF WPOL-ELE-POLI-MAX GREATER ZEROES
        //                                    (SF)-LUNGHEZZA((SF)-IX-CONTA))
        //           END-IF.
        if (wpolAreaPolizza.getWpolElePoliMax() > 0) {
            // COB_CODE: ADD  1                      TO (SF)-IX-CONTA
            ws.getAreaEstrazioneIb().getDatiInput().setIxConta(Trunc.toInt(1 + ws.getAreaEstrazioneIb().getDatiInput().getIxConta(), 4));
            // COB_CODE: MOVE (SF)-ALIAS-POLI
            //             TO (SF)-TAB-ALIAS((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setTabAlias(ws.getIvvc0218().getAliasPoli());
            // COB_CODE: MOVE WPOL-ELE-POLI-MAX
            //             TO (SF)-NUM-OCCORRENZE((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setNumOccorrenze(wpolAreaPolizza.getWpolElePoliMax());
            // COB_CODE: MOVE (SF)-APPO-LUNGHEZZA
            //             TO (SF)-POSIZ-INI((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setPosizIni(ws.getAreaEstrazioneIb().getDatiInput().getAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WPOL-AREA-POLIZZA
            //             TO (SF)-LUNGHEZZA((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setLunghezza(WpolAreaPolizzaLccs0005.Len.WPOL_AREA_POLIZZA);
            // COB_CODE: COMPUTE (SF)-APPO-LUNGHEZZA = (SF)-APPO-LUNGHEZZA +
            //                   (SF)-LUNGHEZZA((SF)-IX-CONTA) + 1
            ws.getAreaEstrazioneIb().getDatiInput().setAppoLunghezza(Trunc.toInt(ws.getAreaEstrazioneIb().getDatiInput().getAppoLunghezza() + ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).getLunghezza() + 1, 9));
            //-->     Valorizzazione del buffer dati
            // COB_CODE: MOVE WPOL-AREA-POLIZZA
            //             TO (SF)-BUFFER-DATI((SF)-POSIZ-INI((SF)-IX-CONTA):
            //                                 (SF)-LUNGHEZZA((SF)-IX-CONTA))
            ws.getAreaEstrazioneIb().getDatiInput().setC161BufferDatiSubstring(wpolAreaPolizza.getWpolAreaPolizzaFormatted(), ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).getPosizIni(), ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).getLunghezza());
        }
        //--> ADESIONE
        // COB_CODE: IF WADE-ELE-ADES-MAX GREATER ZEROES
        //                                    (SF)-LUNGHEZZA((SF)-IX-CONTA))
        //           END-IF.
        if (wadeAreaAdesione.getWadeEleAdesMax() > 0) {
            // COB_CODE: ADD 1                         TO (SF)-IX-CONTA
            ws.getAreaEstrazioneIb().getDatiInput().setIxConta(Trunc.toInt(1 + ws.getAreaEstrazioneIb().getDatiInput().getIxConta(), 4));
            // COB_CODE: MOVE (SF)-ALIAS-ADES
            //             TO (SF)-TAB-ALIAS((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setTabAlias(ws.getIvvc0218().getAliasAdes());
            // COB_CODE: MOVE WADE-ELE-ADES-MAX
            //             TO (SF)-NUM-OCCORRENZE((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setNumOccorrenze(wadeAreaAdesione.getWadeEleAdesMax());
            // COB_CODE: MOVE (SF)-APPO-LUNGHEZZA
            //             TO (SF)-POSIZ-INI((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setPosizIni(ws.getAreaEstrazioneIb().getDatiInput().getAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WADE-AREA-ADESIONE
            //             TO (SF)-LUNGHEZZA((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setLunghezza(WadeAreaAdesioneLccs0005.Len.WADE_AREA_ADESIONE);
            // COB_CODE: COMPUTE (SF)-APPO-LUNGHEZZA = (SF)-APPO-LUNGHEZZA +
            //                   (SF)-LUNGHEZZA((SF)-IX-CONTA) + 1
            ws.getAreaEstrazioneIb().getDatiInput().setAppoLunghezza(Trunc.toInt(ws.getAreaEstrazioneIb().getDatiInput().getAppoLunghezza() + ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).getLunghezza() + 1, 9));
            //-->  Valorizzazione del buffer dati
            // COB_CODE: MOVE WADE-AREA-ADESIONE
            //             TO (SF)-BUFFER-DATI((SF)-POSIZ-INI((SF)-IX-CONTA):
            //                                 (SF)-LUNGHEZZA((SF)-IX-CONTA))
            ws.getAreaEstrazioneIb().getDatiInput().setC161BufferDatiSubstring(wadeAreaAdesione.getWadeAreaAdesioneFormatted(), ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).getPosizIni(), ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).getLunghezza());
        }
        //--> RAPPORTO RETE
        // COB_CODE: IF WRRE-ELE-RAPP-RETE-MAX GREATER ZEROES
        //                                   (SF)-LUNGHEZZA((SF)-IX-CONTA))
        //           END-IF.
        if (wrreAreaRappRete.getEleRappReteMax() > 0) {
            // COB_CODE: ADD 1                         TO (SF)-IX-CONTA
            ws.getAreaEstrazioneIb().getDatiInput().setIxConta(Trunc.toInt(1 + ws.getAreaEstrazioneIb().getDatiInput().getIxConta(), 4));
            // COB_CODE: MOVE (SF)-ALIAS-RAPP-RETE
            //             TO (SF)-TAB-ALIAS((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setTabAlias(ws.getIvvc0218().getAliasRappRete());
            // COB_CODE: MOVE WRRE-ELE-RAPP-RETE-MAX
            //             TO (SF)-NUM-OCCORRENZE((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setNumOccorrenze(wrreAreaRappRete.getEleRappReteMax());
            // COB_CODE: MOVE (SF)-APPO-LUNGHEZZA
            //             TO (SF)-POSIZ-INI((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setPosizIni(ws.getAreaEstrazioneIb().getDatiInput().getAppoLunghezza());
            // COB_CODE: MOVE LENGTH OF WRRE-AREA-RAPP-RETE
            //             TO (SF)-LUNGHEZZA((SF)-IX-CONTA)
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).setLunghezza(WrreAreaRappRete.Len.WRRE_AREA_RAPP_RETE);
            // COB_CODE: COMPUTE (SF)-APPO-LUNGHEZZA = (SF)-APPO-LUNGHEZZA +
            //                   (SF)-LUNGHEZZA((SF)-IX-CONTA) + 1
            ws.getAreaEstrazioneIb().getDatiInput().setAppoLunghezza(Trunc.toInt(ws.getAreaEstrazioneIb().getDatiInput().getAppoLunghezza() + ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).getLunghezza() + 1, 9));
            //-->    Valorizzazione del buffer dati
            // COB_CODE: MOVE WRRE-AREA-RAPP-RETE
            //             TO (SF)-BUFFER-DATI((SF)-POSIZ-INI((SF)-IX-CONTA):
            //                                (SF)-LUNGHEZZA((SF)-IX-CONTA))
            ws.getAreaEstrazioneIb().getDatiInput().setC161BufferDatiSubstring(wrreAreaRappRete.getWrreAreaRappReteFormatted(), ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).getPosizIni(), ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(ws.getAreaEstrazioneIb().getDatiInput().getIxConta()).getLunghezza());
        }
        // COB_CODE: MOVE (SF)-IX-CONTA                TO (SF)-ELE-INFO-MAX.
        ws.getAreaEstrazioneIb().getDatiInput().setEleInfoMax(((short)(ws.getAreaEstrazioneIb().getDatiInput().getIxConta())));
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>---------------------------------------------------------------
	 *     ROUTINES GESTIONE ERRORI
	 * ---------------------------------------------------------------
	 * **-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES CALL DISPATCHER
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: ESTRAZIONE-IB-PROPOSTA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PROCEDURE PER NUMERAZIONE OGGETTI
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     GESTIONE ESTRAZIONE IB PROPOSTA (DISPATCHER)
	 * ----------------------------------------------------------------*
	 *     SE STIAMO SCRIVENDO PER LA PRIMA VOLTA LA POLIZZA</pre>*/
    private void estrazioneIbProposta() {
        // COB_CODE:      IF WPOL-ST-ADD
        //           *       SE L'IB PROPOSTA NON E' VALORIZZATO
        //                   END-IF
        //                END-IF.
        if (wpolAreaPolizza.getLccvpol1().getStatus().isAdd()) {
            //       SE L'IB PROPOSTA NON E' VALORIZZATO
            // COB_CODE:         IF (WPOL-IB-PROP = HIGH-VALUES OR SPACES OR LOW-VALUES)
            //           *          NUMERAZIONE AUTOMATICA
            //                         THRU ESTR-IB-PROP-AUT-ASSET-EX
            //                   ELSE
            //           *          NUMERAZIONE MANUALE
            //                         THRU ESTRAZIONE-IB-PROPOSTA-MAN-EX
            //                   END-IF
            if (Characters.EQ_HIGH.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbProp(), WpolDati.Len.WPOL_IB_PROP) || Characters.EQ_SPACE.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbProp()) || Characters.EQ_LOW.test(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbProp(), WpolDati.Len.WPOL_IB_PROP)) {
                //          NUMERAZIONE AUTOMATICA
                // COB_CODE: PERFORM ESTR-IB-PROP-AUT-ASSET
                //              THRU ESTR-IB-PROP-AUT-ASSET-EX
                estrIbPropAutAsset();
            }
            else {
                //          NUMERAZIONE MANUALE
                // COB_CODE: PERFORM ESTRAZIONE-IB-PROPOSTA-MAN
                //              THRU ESTRAZIONE-IB-PROPOSTA-MAN-EX
                estrazioneIbPropostaMan();
            }
        }
    }

    /**Original name: ESTRAZIONE-IB-OGGETTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     GESTIONE ESTRAZIONE IB OGGETTO (DISPATCHER)
	 * ----------------------------------------------------------------*</pre>*/
    private void estrazioneIbOggetto() {
        // COB_CODE: PERFORM ESTRAZIONE-IB-OGG-BNL
        //              THRU ESTRAZIONE-IB-OGG-BNL-EX.
        estrazioneIbOggBnl();
    }

    /**Original name: ESTR-IB-PROP-AUT-ASSET<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE IB PROPOSTA (GESTIONE AUTOMATICA)
	 * ----------------------------------------------------------------*</pre>*/
    private void estrIbPropAutAsset() {
        // COB_CODE: INITIALIZE AREA-ESTRAZIONE-IB.
        initAreaEstrazioneIb();
        // COB_CODE: SET C161-NUM-PROP
        //            TO TRUE.
        ws.getAreaEstrazioneIb().getDatiInput().getCodOggetto().setC161NumProp();
        // COB_CODE: SET C161-AUTOMATICA
        //            TO TRUE.
        ws.getAreaEstrazioneIb().getDatiInput().getTipoNumerazione().setC161Automatica();
        // COB_CODE: MOVE SPACES
        //             TO C161-IB-OGGETTO-I.
        ws.getAreaEstrazioneIb().getDatiInput().setIbOggettoI("");
        //    INVOCAZIONE AL SERVIZIO IDSS0160 PER LA NUMERAZIONE OGGETTI
        // COB_CODE: PERFORM CALL-IDSS0160
        //              THRU CALL-IDSS0160-EX.
        callIdss0160();
        //    VALORIZZAZIONE IB-PROPOSTA DELLA POLIZZA
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              MOVE C161-IB-OGGETTO            TO WPOL-IB-PROP
        //           END-IF
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE C161-IB-OGGETTO            TO WPOL-IB-PROP
            wpolAreaPolizza.getLccvpol1().getDati().setWpolIbProp(ws.getAreaEstrazioneIb().getIbOggetto());
        }
        //    VERIFICA ESISTENZA POLIZZA
        // COB_CODE: MOVE WPOL-IB-PROP                  TO LCCS0025-IB
        ws.getLccc0025AreaComunicaz().setIb(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbProp());
        // COB_CODE: SET  LCCS0025-FL-IB-PROP           TO TRUE
        ws.getLccc0025AreaComunicaz().getFlTipoIb().setLccs0025FlIbProp();
        // COB_CODE: PERFORM VERIF-ESIST-IB
        //              THRU VERIF-ESIST-IB-EX
        verifEsistIb();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF LCCS0025-FL-PRES-PTF-SI
            //           *          NUMERO PROPOSTA GIA' PRESENTE IN PORTAFOGLIO
            //                         THRU EX-S0300
            //                   END-IF
            if (ws.getLccc0025AreaComunicaz().getFlPresInPtf().isLccs0025FlPresPtfSi()) {
                //          NUMERO PROPOSTA GIA' PRESENTE IN PORTAFOGLIO
                // COB_CODE: MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ESTR-IB-PROP-AUT-ASSET'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("ESTR-IB-PROP-AUT-ASSET");
                // COB_CODE: MOVE '005211'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005211");
                // COB_CODE: MOVE SPACES                TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: ESTRAZIONE-IB-PROPOSTA-MAN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE IB PROPOSTA (GESTIONE MANUALE)
	 * ----------------------------------------------------------------*
	 *     VERIFICA ESISTENZA POLIZZA</pre>*/
    private void estrazioneIbPropostaMan() {
        // COB_CODE: MOVE WPOL-IB-PROP                  TO WK-IB-PROP-APPO
        ws.getWkVarLvec0202().setWkIbPropAppo(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbProp());
        //    CALCOLA LA LUNGHEZZA DI UN CAMPO (SOLO I BYTE RIEMPITI)
        // COB_CODE: PERFORM CALCOLA-LUNGHEZZA
        //              THRU CALCOLA-LUNGHEZZA-EX
        calcolaLunghezza();
        //    INSERISCE ZERI NON SIGNIFICATIVI IN UNA STRINGA
        // COB_CODE: PERFORM INSERT-ZERO
        //              THRU INSERT-ZERO-EX
        insertZero();
        // COB_CODE: MOVE WK-IB-PROP-APPO               TO LCCS0025-IB
        //                                                 WPOL-IB-PROP
        ws.getLccc0025AreaComunicaz().setIb(ws.getWkVarLvec0202().getWkIbPropAppo());
        wpolAreaPolizza.getLccvpol1().getDati().setWpolIbProp(ws.getWkVarLvec0202().getWkIbPropAppo());
        // COB_CODE: SET  LCCS0025-FL-IB-PROP           TO TRUE
        ws.getLccc0025AreaComunicaz().getFlTipoIb().setLccs0025FlIbProp();
        // COB_CODE: PERFORM VERIF-ESIST-IB
        //              THRU VERIF-ESIST-IB-EX
        verifEsistIb();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF LCCS0025-FL-PRES-PTF-SI
            //           *          NUMERO PROPOSTA GIA' PRESENTE IN PORTAFOGLIO
            //                         THRU EX-S0300
            //                   END-IF
            if (ws.getLccc0025AreaComunicaz().getFlPresInPtf().isLccs0025FlPresPtfSi()) {
                //          NUMERO PROPOSTA GIA' PRESENTE IN PORTAFOGLIO
                // COB_CODE: MOVE WK-PGM              TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'ESTRAZIONE-IB-PROPOSTA-MAN'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("ESTRAZIONE-IB-PROPOSTA-MAN");
                // COB_CODE: MOVE '005211'              TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005211");
                // COB_CODE: MOVE SPACES                TO IEAI9901-PARAMETRI-ERR
                ws.getIeai9901Area().setParametriErr("");
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
        //    CALL SERVIZIO NUMERAZIONE OGGETTI IN MODALITA' MANUALE
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU CALL-IDSS0160-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: INITIALIZE AREA-ESTRAZIONE-IB
            initAreaEstrazioneIb();
            // COB_CODE: SET C161-NUM-PROP
            //            TO TRUE
            ws.getAreaEstrazioneIb().getDatiInput().getCodOggetto().setC161NumProp();
            // COB_CODE: SET C161-MANUALE
            //            TO TRUE
            ws.getAreaEstrazioneIb().getDatiInput().getTipoNumerazione().setC161Manuale();
            // COB_CODE: MOVE WPOL-IB-PROP
            //             TO C161-IB-OGGETTO-I
            ws.getAreaEstrazioneIb().getDatiInput().setIbOggettoI(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbProp());
            //       INVOCAZIONE AL SERVIZIO IDSS0160 PER LA NUMERAZIONE OGGETTI
            // COB_CODE: PERFORM CALL-IDSS0160
            //              THRU CALL-IDSS0160-EX
            callIdss0160();
        }
    }

    /**Original name: ESTRAZIONE-IB-OGG-BNL<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE IB OGGETTO PER BNL
	 * ----------------------------------------------------------------*
	 *     PER BNL L'IB-OGGETTO DEVE COINCIDERE CON L'IB-PROPOSTA</pre>*/
    private void estrazioneIbOggBnl() {
        // COB_CODE: MOVE WPOL-IB-PROP                 TO WPOL-IB-OGG.
        wpolAreaPolizza.getLccvpol1().getDati().setWpolIbOgg(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbProp());
    }

    /**Original name: CALL-IDSS0160<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALL IDSS0160
	 * ----------------------------------------------------------------*</pre>*/
    private void callIdss0160() {
        Idss0160 idss0160 = null;
        // COB_CODE: PERFORM VALORIZZA-AREE-S0088
        //              THRU VALORIZZA-AREE-S0088-EX.
        valorizzaAreeS0088();
        // COB_CODE:      CALL IDSS0160          USING  AREA-IDSV0001
        //                                              AREA-ESTRAZIONE-IB
        //                ON EXCEPTION
        //           *       "ERRORE DI SISTEMA DURANTE..."
        //                      THRU EX-S0290
        //                END-CALL.
        try {
            idss0160 = Idss0160.getInstance();
            idss0160.run(areaIdsv0001, ws.getAreaEstrazioneIb());
        }
        catch (ProgramExecutionException __ex) {
            //       "ERRORE DI SISTEMA DURANTE..."
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALL NUMERAZIONE OGGETTI (IDSS0160)'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALL NUMERAZIONE OGGETTI (IDSS0160)");
            // COB_CODE: MOVE 'CALL-IDSS0160'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-IDSS0160");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: VERIF-ESIST-IB<br>
	 * <pre>----------------------------------------------------------------*
	 *    VERIFICA ESISTENZA IB
	 * ----------------------------------------------------------------*</pre>*/
    private void verifEsistIb() {
        Lccs0025 lccs0025 = null;
        // COB_CODE:      CALL LCCS0025 USING AREA-IDSV0001
        //                                    WCOM-AREA-STATI
        //                                    LCCC0025-AREA-COMUNICAZ
        //                ON EXCEPTION
        //           *       "ERRORE DI SISTEMA DURANTE..."
        //                      THRU EX-S0290
        //                END-CALL.
        try {
            lccs0025 = Lccs0025.getInstance();
            lccs0025.run(areaIdsv0001, lccc0001, ws.getLccc0025AreaComunicaz());
        }
        catch (ProgramExecutionException __ex) {
            //       "ERRORE DI SISTEMA DURANTE..."
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO VERIFICA ESISTENZA POLIZZA (LCCS0025)'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO VERIFICA ESISTENZA POLIZZA (LCCS0025)");
            // COB_CODE: MOVE 'VERIF-ESIST-IB'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("VERIF-ESIST-IB");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: ESTR-IB-OGG-ADE<br>
	 * <pre>*****************************************************************
	 *   COPY DI PROCEDURE PER GESTIONE NUMERAZIONE OGGETTI (VENDITA)  *
	 * *****************************************************************
	 *   ISTRUZIONI: PER POTER UTILIZZARE QUESTA COPY DI PROCEDURE
	 *   BISOGNA DICHIARARE LE SEGUENTI COPY:
	 * ----------------------------------------------------------------*
	 *     ESTRAZIONE IB-OGGETTO PER ADESIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void estrIbOggAde() {
        // COB_CODE: IF  WPOL-ID-PTF IS NUMERIC
        //           AND WPOL-ID-PTF > ZEROES
        //               END-IF
        //           END-IF.
        if (Functions.isNumber(wpolAreaPolizza.getLccvpol1().getIdPtf()) && wpolAreaPolizza.getLccvpol1().getIdPtf() > 0) {
            // COB_CODE: IF   WADE-ST-ADD
            //           AND (WADE-IB-OGG-NULL = HIGH-VALUE
            //            OR  WADE-IB-OGG-NULL = SPACES)
            //              END-IF
            //           END-IF
            if (wadeAreaAdesione.getLccvade1().getStatus().isAdd() && (Characters.EQ_HIGH.test(wadeAreaAdesione.getLccvade1().getDati().getWadeIbOgg(), WadeDati.Len.WADE_IB_OGG) || Characters.EQ_SPACE.test(wadeAreaAdesione.getLccvade1().getDati().getWadeIbOgg()))) {
                // COB_CODE: INITIALIZE AREA-IO-LCCS0070
                initAreaIoLccs0070();
                // COB_CODE: SET LCCC0070-CALC-IB-ADE        TO TRUE
                ws.getAreaIoLccs0070().getFunzione().setLccc0070CalcIbAde();
                // COB_CODE: MOVE WPOL-ID-PTF                TO LCCC0070-ID-OGGETTO
                ws.getAreaIoLccs0070().setIdOggetto(wpolAreaPolizza.getLccvpol1().getIdPtf());
                // COB_CODE: MOVE 'PO'                       TO LCCC0070-TP-OGGETTO
                ws.getAreaIoLccs0070().setTpOggetto("PO");
                // COB_CODE: PERFORM CALL-LCCS0070
                //              THRU CALL-LCCS0070-EX
                callLccs0070();
                // COB_CODE: IF IDSV0001-ESITO-OK
                //              MOVE LCCC0070-IB-OGGETTO       TO WADE-IB-OGG
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                    // COB_CODE: MOVE LCCC0070-IB-OGGETTO       TO WADE-IB-OGG
                    wadeAreaAdesione.getLccvade1().getDati().setWadeIbOgg(ws.getAreaIoLccs0070().getIbOggetto());
                }
            }
        }
    }

    /**Original name: CALL-LCCS0070<br>
	 * <pre>----------------------------------------------------------------*
	 *     CALL LCCS0070
	 * ----------------------------------------------------------------*</pre>*/
    private void callLccs0070() {
        Lccs0070 lccs0070 = null;
        // COB_CODE:      CALL LCCS0070          USING  AREA-IDSV0001
        //                                              AREA-IO-LCCS0070
        //                ON EXCEPTION
        //           *       "ERRORE DI SISTEMA DURANTE..."
        //                      THRU EX-S0290
        //                END-CALL.
        try {
            lccs0070 = Lccs0070.getInstance();
            lccs0070.run(areaIdsv0001, ws.getAreaIoLccs0070());
        }
        catch (ProgramExecutionException __ex) {
            //       "ERRORE DI SISTEMA DURANTE..."
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'CALL NUMERAZIONE OGGETTI ADE/GAR/TRA (LCCS0070)'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("CALL NUMERAZIONE OGGETTI ADE/GAR/TRA (LCCS0070)");
            // COB_CODE: MOVE 'CALL-LCCS0070'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("CALL-LCCS0070");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
    }

    /**Original name: CALCOLA-LUNGHEZZA<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES OPERAZIONI SULLE STRINGHE
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    ROUTINES OPERAZIONI SULLE STRINGHE
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *    CALCOLA LA LUNGHEZZA DI UN CAMPO (SOLO I BYTE RIEMPITI)
	 * ----------------------------------------------------------------*
	 *     RECUPERO LA LUNGHEZZA TOTALE DEL CAMPO DI INPUT</pre>*/
    private void calcolaLunghezza() {
        // COB_CODE: MOVE LENGTH OF WK-IB-PROP-APPO
        //             TO WK-LUNG-TOT-CAMPO
        ws.getWkVarLvec0202().setWkLungTotCampo(((short)WkVarLvec0202.Len.WK_IB_PROP_APPO));
        // COB_CODE: IF WK-IB-PROP-APPO = SPACES OR HIGH-VALUE
        //                TO WK-LUNG-CAMPO
        //           ELSE
        //              END-IF
        //           END-IF.
        if (Characters.EQ_SPACE.test(ws.getWkVarLvec0202().getWkIbPropAppo()) || Characters.EQ_HIGH.test(ws.getWkVarLvec0202().getWkIbPropAppo(), WkVarLvec0202.Len.WK_IB_PROP_APPO)) {
            // COB_CODE: MOVE ZEROES
            //             TO WK-LUNG-CAMPO
            ws.getWkVarLvec0202().setWkLungCampo(((short)0));
        }
        else {
            // COB_CODE: SET FINE-CICLO-NO
            //            TO TRUE
            ws.getWkVarLvec0202().getFgFineCiclo().setCicloNo();
            // COB_CODE:         PERFORM VARYING IX-TAB FROM 2 BY 1
            //                             UNTIL IX-TAB > WK-LUNG-TOT-CAMPO
            //                                OR FINE-CICLO
            //           *          CARATTERI RIMANENTI
            //                      END-IF
            //                   END-PERFORM
            ws.getWkVarLvec0202().setIxTab(((short)2));
            while (!(ws.getWkVarLvec0202().getIxTab() > ws.getWkVarLvec0202().getWkLungTotCampo() || ws.getWkVarLvec0202().getFgFineCiclo().isCiclo())) {
                //          CARATTERI RIMANENTI
                // COB_CODE: COMPUTE WK-NUM-CAR-RIMAN = WK-LUNG-TOT-CAMPO - IX-TAB
                ws.getWkVarLvec0202().setWkNumCarRiman(Trunc.toShort(abs(ws.getWkVarLvec0202().getWkLungTotCampo() - ws.getWkVarLvec0202().getIxTab()), 2));
                //          SE LA PARTE RIMANENTE DEL CAMPO NON E' VALORIZZATO
                //          VUOL DIRE CHE LA LUNGHEZZA DEL CAMPO E' UGUALE A
                //          IX-TAB - 1
                // COB_CODE: IF WK-IB-PROP-APPO(IX-TAB:WK-NUM-CAR-RIMAN)
                //                                            = SPACES OR HIGH-VALUE
                //               TO TRUE
                //           END-IF
                if (Conditions.eq(ws.getWkVarLvec0202().getWkIbPropAppoFormatted().substring((ws.getWkVarLvec0202().getIxTab()) - 1, ws.getWkVarLvec0202().getIxTab() + ws.getWkVarLvec0202().getWkNumCarRiman() - 1), "") || Conditions.eq(ws.getWkVarLvec0202().getWkIbPropAppoFormatted().substring((ws.getWkVarLvec0202().getIxTab()) - 1, ws.getWkVarLvec0202().getIxTab() + ws.getWkVarLvec0202().getWkNumCarRiman() - 1), LiteralGenerator.create(Types.HIGH_CHAR_VAL, ws.getWkVarLvec0202().getWkNumCarRiman()))) {
                    // COB_CODE: COMPUTE WK-LUNG-CAMPO = IX-TAB - 1
                    ws.getWkVarLvec0202().setWkLungCampo(Trunc.toShort(abs(ws.getWkVarLvec0202().getIxTab() - 1), 2));
                    // COB_CODE: SET FINE-CICLO
                    //            TO TRUE
                    ws.getWkVarLvec0202().getFgFineCiclo().setCiclo();
                }
                ws.getWkVarLvec0202().setIxTab(Trunc.toShort(ws.getWkVarLvec0202().getIxTab() + 1, 4));
            }
            //       SE IL CICLO E' TERMINATO PERCHE' SONO STATI CONTROLLATI
            //       TUTTI I BYTE VUOL DIRE CHE IL CAMPO E' VALORIZZATO
            //       COMPLETAMENTE
            // COB_CODE: IF NOT FINE-CICLO
            //                TO WK-LUNG-CAMPO
            //           END-IF
            if (!ws.getWkVarLvec0202().getFgFineCiclo().isCiclo()) {
                // COB_CODE: MOVE WK-LUNG-TOT-CAMPO
                //             TO WK-LUNG-CAMPO
                ws.getWkVarLvec0202().setWkLungCampoFormatted(ws.getWkVarLvec0202().getWkLungTotCampoFormatted());
            }
        }
    }

    /**Original name: INSERT-ZERO<br>
	 * <pre>----------------------------------------------------------------*
	 *    INSERISCE ZERI NON SIGNIFICATIVI IN UNA STRINGA
	 * ----------------------------------------------------------------*
	 *     POICHE' LA LUNGHEZZA DELL'IB-PROPOSTA, ESCLUSI I 3 BYTE DEL
	 *     CODICE RAMO E' DI 9 BYTE, ESEGUO QUESTA FUNZIONE SOLO SE
	 *     LA LUNGHEZZA EFFETTIVA DEL CAMPO E' MINORE DI 9</pre>*/
    private void insertZero() {
        // COB_CODE: IF WK-LUNG-CAMPO < 9
        //                TO WK-IB-PROP-APPO
        //           END-IF.
        if (ws.getWkVarLvec0202().getWkLungCampo() < 9) {
            // COB_CODE: COMPUTE WK-NUM-CAR-ZERO = 9 - WK-LUNG-CAMPO
            ws.getWkVarLvec0202().setWkNumCarZero(Trunc.toShort(abs(9 - ws.getWkVarLvec0202().getWkLungCampo()), 2));
            // COB_CODE: MOVE SPACES
            //             TO WK-IB-PROP-APPO2
            ws.getWkVarLvec0202().setWkIbPropAppo2("");
            // COB_CODE: MOVE WK-IB-PROP-APPO(1:WK-LUNG-CAMPO)
            //           TO WK-IB-PROP-APPO2(9 - WK-LUNG-CAMPO + 1:WK-LUNG-CAMPO)
            ws.getWkVarLvec0202().setWkIbPropAppo2(Functions.setSubstring(ws.getWkVarLvec0202().getWkIbPropAppo2(), ws.getWkVarLvec0202().getWkIbPropAppoFormatted().substring((1) - 1, ws.getWkVarLvec0202().getWkLungCampo()), 9 - ws.getWkVarLvec0202().getWkLungCampo() + 1, ws.getWkVarLvec0202().getWkLungCampo()));
            // COB_CODE: MOVE ZEROES
            //             TO WK-IB-PROP-APPO2(1:WK-NUM-CAR-ZERO)
            ws.getWkVarLvec0202().setWkIbPropAppo2(Functions.setSubstring(ws.getWkVarLvec0202().getWkIbPropAppo2(), LiteralGenerator.create('0', ws.getWkVarLvec0202().getWkNumCarZero()), 1, ws.getWkVarLvec0202().getWkNumCarZero()));
            // COB_CODE: MOVE SPACES
            //             TO WK-IB-PROP-APPO
            ws.getWkVarLvec0202().setWkIbPropAppo("");
            // COB_CODE: MOVE WK-IB-PROP-APPO2
            //             TO WK-IB-PROP-APPO
            ws.getWkVarLvec0202().setWkIbPropAppo(ws.getWkVarLvec0202().getWkIbPropAppo2());
        }
    }

    /**Original name: AGGIORNA-TABELLA<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY PROCEDURE AGGIORNAMENTO PORTAFOGLIO
	 * ----------------------------------------------------------------*
	 * --> AGGIORNA TABELLA
	 * ----------------------------------------------------------------*
	 *     COPY      ..... LCCP0001
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     AGGIORNAMENTO TABELLA
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void aggiornaTabella() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA               TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato(ws.getWkTabella());
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //                   END-EVALUATE
        //                ELSE
        //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            // COB_CODE:         EVALUATE TRUE
            //           *-->      OPERAZIONE ESEGUITA CORRETTAMENTE
            //                     WHEN IDSO0011-SUCCESSFUL-SQL
            //                        CONTINUE
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                     WHEN OTHER
            //                           THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL:// COB_CODE: CONTINUE
                //continue
                //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    break;

                default:// COB_CODE: MOVE WK-PGM           TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'AGGIORNA-TABELLA'
                    //                                 TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
                    // COB_CODE: MOVE '005016'         TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA            ';'
                    //                  IDSO0011-RETURN-CODE  ';'
                    //                  IDSO0011-SQLCODE
                    //           DELIMITED BY SIZE   INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;
            }
        }
        else {
            //-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            // COB_CODE: MOVE WK-PGM                TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'AGGIORNA-TABELLA'
            //                                      TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("AGGIORNA-TABELLA");
            // COB_CODE: MOVE '005016'              TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005016");
            // COB_CODE: STRING WK-TABELLA            ';'
            //                  IDSO0011-RETURN-CODE  ';'
            //                  IDSO0011-SQLCODE
            //           DELIMITED BY SIZE        INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
            ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: ESTR-SEQUENCE<br>
	 * <pre>--> ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*
	 *     COPY      ..... LCCP0002
	 *     TIPOLOGIA...... COPY PROCEDURE (COMPONENTI COMUNI)
	 *     DESCRIZIONE.... ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE
	 * ----------------------------------------------------------------*</pre>*/
    private void estrSequence() {
        Lccs0090 lccs0090 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-TABELLA TO S090-NOME-TABELLA.
        ws.getAreaIoLccs0090().setNomeTabella(ws.getWkTabella());
        // COB_CODE: CALL LCCS0090 USING AREA-IO-LCCS0090
        //           ON EXCEPTION
        //                 THRU EX-S0290
        //           END-CALL.
        try {
            lccs0090 = Lccs0090.getInstance();
            lccs0090.run(ws.getAreaIoLccs0090());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'SERVIZIO ESTRAZIONE SEQUENCE'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("SERVIZIO ESTRAZIONE SEQUENCE");
            // COB_CODE: MOVE 'ESTR-SEQUENCE'
            //             TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("ESTR-SEQUENCE");
            // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
            //              THRU EX-S0290
            s0290ErroreDiSistema();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-EVALUATE
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         EVALUATE S090-RETURN-CODE
            //                       WHEN '00'
            //                            CONTINUE
            //                       WHEN 'S1'
            //           *-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
            //                             THRU EX-S0300
            //                       WHEN 'D3'
            //           *-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
            //                             THRU EX-S0300
            //                   END-EVALUATE
            switch (ws.getAreaIoLccs0090().getReturnCode().getReturnCode()) {

                case "00":// COB_CODE: CONTINUE
                //continue
                    break;

                case "S1"://-->ERRORE ESTRAZIONE SEQUENCE SULLA TABELLA $ - RC=$ - SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005015'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005015");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                case "D3"://-->ERRORE ACCESSO ALLA BASE DATI SULLA TABELLA $  RC=$  SQLCODE=$
                    // COB_CODE: MOVE WK-PGM            TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1900-CALL-LCCS0090'
                    //                                  TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1900-CALL-LCCS0090");
                    // COB_CODE: MOVE '005016'          TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005016");
                    // COB_CODE: STRING WK-TABELLA        ';'
                    //                  S090-RETURN-CODE  ';'
                    //                  S090-SQLCODE
                    //           DELIMITED BY SIZE    INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkTabellaFormatted(), ";", ws.getAreaIoLccs0090().getReturnCode().getReturnCodeFormatted(), ";", ws.getAreaIoLccs0090().getSqlcode().getIdso0021SqlcodeAsString());
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                    break;

                default:break;
            }
        }
    }

    /**Original name: VAL-DCLGEN-MOV<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES AGGIORNAMENTO DB
	 * ----------------------------------------------------------------*
	 * --  MOVIMENTO
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVMOV5
	 *    ULTIMO AGG. 04 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenMov() {
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA
        //              TO MOV-COD-COMP-ANIA
        ws.getMovi().setMovCodCompAnia(wmovAreaMovimento.getLccvmov1().getDati().getWmovCodCompAnia());
        // COB_CODE: IF (SF)-IB-OGG-NULL = HIGH-VALUES
        //              TO MOV-IB-OGG-NULL
        //           ELSE
        //              TO MOV-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(wmovAreaMovimento.getLccvmov1().getDati().getWmovIbOgg(), WmovDati.Len.WMOV_IB_OGG)) {
            // COB_CODE: MOVE (SF)-IB-OGG-NULL
            //           TO MOV-IB-OGG-NULL
            ws.getMovi().setMovIbOgg(wmovAreaMovimento.getLccvmov1().getDati().getWmovIbOgg());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-OGG
            //           TO MOV-IB-OGG
            ws.getMovi().setMovIbOgg(wmovAreaMovimento.getLccvmov1().getDati().getWmovIbOgg());
        }
        // COB_CODE: IF (SF)-IB-MOVI-NULL = HIGH-VALUES
        //              TO MOV-IB-MOVI-NULL
        //           ELSE
        //              TO MOV-IB-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(wmovAreaMovimento.getLccvmov1().getDati().getWmovIbMovi(), WmovDati.Len.WMOV_IB_MOVI)) {
            // COB_CODE: MOVE (SF)-IB-MOVI-NULL
            //           TO MOV-IB-MOVI-NULL
            ws.getMovi().setMovIbMovi(wmovAreaMovimento.getLccvmov1().getDati().getWmovIbMovi());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-MOVI
            //           TO MOV-IB-MOVI
            ws.getMovi().setMovIbMovi(wmovAreaMovimento.getLccvmov1().getDati().getWmovIbMovi());
        }
        // COB_CODE: IF (SF)-TP-OGG-NULL = HIGH-VALUES
        //              TO MOV-TP-OGG-NULL
        //           ELSE
        //              TO MOV-TP-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpOggFormatted())) {
            // COB_CODE: MOVE (SF)-TP-OGG-NULL
            //           TO MOV-TP-OGG-NULL
            ws.getMovi().setMovTpOgg(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpOgg());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-OGG
            //           TO MOV-TP-OGG
            ws.getMovi().setMovTpOgg(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpOgg());
        }
        // COB_CODE: IF (SF)-ID-RICH-NULL = HIGH-VALUES
        //              TO MOV-ID-RICH-NULL
        //           ELSE
        //              TO MOV-ID-RICH
        //           END-IF
        if (Characters.EQ_HIGH.test(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdRich().getWmovIdRichNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-RICH-NULL
            //           TO MOV-ID-RICH-NULL
            ws.getMovi().getMovIdRich().setMovIdRichNull(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdRich().getWmovIdRichNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-RICH
            //           TO MOV-ID-RICH
            ws.getMovi().getMovIdRich().setMovIdRich(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdRich().getWmovIdRich());
        }
        // COB_CODE: IF (SF)-TP-MOVI-NULL = HIGH-VALUES
        //              TO MOV-TP-MOVI-NULL
        //           ELSE
        //              TO MOV-TP-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpMovi().getWmovTpMoviNullFormatted())) {
            // COB_CODE: MOVE (SF)-TP-MOVI-NULL
            //           TO MOV-TP-MOVI-NULL
            ws.getMovi().getMovTpMovi().setMovTpMoviNull(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpMovi().getWmovTpMoviNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-MOVI
            //           TO MOV-TP-MOVI
            ws.getMovi().getMovTpMovi().setMovTpMovi(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpMovi().getWmovTpMovi());
        }
        // COB_CODE: MOVE (SF)-DT-EFF
        //              TO MOV-DT-EFF
        ws.getMovi().setMovDtEff(wmovAreaMovimento.getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: IF (SF)-ID-MOVI-ANN-NULL = HIGH-VALUES
        //              TO MOV-ID-MOVI-ANN-NULL
        //           ELSE
        //              TO MOV-ID-MOVI-ANN
        //           END-IF
        if (Characters.EQ_HIGH.test(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdMoviAnn().getWmovIdMoviAnnNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-MOVI-ANN-NULL
            //           TO MOV-ID-MOVI-ANN-NULL
            ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnnNull(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdMoviAnn().getWmovIdMoviAnnNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-MOVI-ANN
            //           TO MOV-ID-MOVI-ANN
            ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdMoviAnn().getWmovIdMoviAnn());
        }
        // COB_CODE: IF (SF)-ID-MOVI-COLLG-NULL = HIGH-VALUES
        //              TO MOV-ID-MOVI-COLLG-NULL
        //           ELSE
        //              TO MOV-ID-MOVI-COLLG
        //           END-IF
        if (Characters.EQ_HIGH.test(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdMoviCollg().getWmovIdMoviCollgNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-MOVI-COLLG-NULL
            //           TO MOV-ID-MOVI-COLLG-NULL
            ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollgNull(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdMoviCollg().getWmovIdMoviCollgNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-MOVI-COLLG
            //           TO MOV-ID-MOVI-COLLG
            ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdMoviCollg().getWmovIdMoviCollg());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL
        //              TO MOV-DS-OPER-SQL
        ws.getMovi().setMovDsOperSql(wmovAreaMovimento.getLccvmov1().getDati().getWmovDsOperSql());
        // COB_CODE: IF (SF)-DS-VER NOT NUMERIC
        //              MOVE 0 TO MOV-DS-VER
        //           ELSE
        //              TO MOV-DS-VER
        //           END-IF
        if (!Functions.isNumber(wmovAreaMovimento.getLccvmov1().getDati().getWmovDsVer())) {
            // COB_CODE: MOVE 0 TO MOV-DS-VER
            ws.getMovi().setMovDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER
            //           TO MOV-DS-VER
            ws.getMovi().setMovDsVer(wmovAreaMovimento.getLccvmov1().getDati().getWmovDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-CPTZ NOT NUMERIC
        //              MOVE 0 TO MOV-DS-TS-CPTZ
        //           ELSE
        //              TO MOV-DS-TS-CPTZ
        //           END-IF
        if (!Functions.isNumber(wmovAreaMovimento.getLccvmov1().getDati().getWmovDsTsCptz())) {
            // COB_CODE: MOVE 0 TO MOV-DS-TS-CPTZ
            ws.getMovi().setMovDsTsCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-CPTZ
            //           TO MOV-DS-TS-CPTZ
            ws.getMovi().setMovDsTsCptz(wmovAreaMovimento.getLccvmov1().getDati().getWmovDsTsCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE
        //              TO MOV-DS-UTENTE
        ws.getMovi().setMovDsUtente(wmovAreaMovimento.getLccvmov1().getDati().getWmovDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB
        //              TO MOV-DS-STATO-ELAB.
        ws.getMovi().setMovDsStatoElab(wmovAreaMovimento.getLccvmov1().getDati().getWmovDsStatoElab());
    }

    /**Original name: AGGIORNA-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVMOV6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO MOVIMENTO
	 * ----------------------------------------------------------------*
	 * --> CONTROLLO DELLO STATUS
	 * -->     INSERIMENTO</pre>*/
    private void aggiornaMovi() {
        // COB_CODE:      EVALUATE TRUE
        //           *-->     INSERIMENTO
        //                    WHEN WMOV-ST-ADD
        //           *-->        ESTRAZIONE E VALORIZZAZIONE SEQUENCE MOVIMENTO
        //                       SET  IDSI0011-INSERT TO TRUE
        //                    WHEN WMOV-ST-MOD
        //           *-->        TIPO OPERAZIONE DISPATCHER
        //                       SET  IDSI0011-UPDATE TO TRUE
        //                END-EVALUATE.
        switch (wmovAreaMovimento.getLccvmov1().getStatus().getStatus()) {

            case WpolStatus.ADD://-->        ESTRAZIONE E VALORIZZAZIONE SEQUENCE MOVIMENTO
                // COB_CODE: PERFORM ESTR-SEQUENCE-MOVI
                //              THRU ESTR-SEQUENCE-MOVI-EX
                estrSequenceMovi();
                //-->        TIPO OPERAZIONE DISPATCHER
                // COB_CODE: SET  IDSI0011-INSERT TO TRUE
                ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
                break;

            case WpolStatus.MOD://-->        TIPO OPERAZIONE DISPATCHER
                // COB_CODE: SET  IDSI0011-UPDATE TO TRUE
                ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Update();
                break;

            default:break;
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU AGGIORNA-TABELLA-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE:         IF LCCV0005-PREVENTIVAZIONE
            //                      MOVE WMOV-ID-OGG  TO MOV-ID-OGG
            //                   ELSE
            //           *-->       PRE VALORIZZA DCLGEN MOVI
            //                         THRU PREVAL-DCLGEN-MOVI-EX
            //                   END-IF
            if (lccv0005.getMacroFunzione().isPreventivazione()) {
                // COB_CODE: MOVE WMOV-ID-OGG  TO MOV-ID-OGG
                ws.getMovi().getMovIdOgg().setMovIdOgg(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdOgg().getWmovIdOgg());
            }
            else {
                //-->       PRE VALORIZZA DCLGEN MOVI
                // COB_CODE: PERFORM PREVAL-DCLGEN-MOVI
                //              THRU PREVAL-DCLGEN-MOVI-EX
                prevalDclgenMovi();
            }
            //-->    VALORIZZA DCLGEN MOVI
            // COB_CODE: PERFORM VAL-DCLGEN-MOV
            //              THRU VAL-DCLGEN-MOV-EX
            valDclgenMov();
            //-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
            // COB_CODE: PERFORM VALORIZZA-AREA-DSH-MOV
            //              THRU VALORIZZA-AREA-DSH-MOV-EX
            valorizzaAreaDshMov();
            //-->    CALL DISPATCHER PER AGGIORNAMENTO
            // COB_CODE: PERFORM AGGIORNA-TABELLA
            //              THRU AGGIORNA-TABELLA-EX
            aggiornaTabella();
        }
    }

    /**Original name: ESTR-SEQUENCE-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void estrSequenceMovi() {
        // COB_CODE: MOVE 'MOVI' TO WK-TABELLA.
        ws.setWkTabella("MOVI");
        // COB_CODE: PERFORM ESTR-SEQUENCE
        //              THRU ESTR-SEQUENCE-EX.
        estrSequence();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                                              MOV-ID-MOVI
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE S090-SEQ-TABELLA        TO WMOV-ID-PTF
            //                                           MOV-ID-MOVI
            wmovAreaMovimento.getLccvmov1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
            ws.getMovi().setMovIdMovi(ws.getAreaIoLccs0090().getSeqTabella());
        }
    }

    /**Original name: ESTR-SEQUENCE-POLI<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE POLIZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void estrSequencePoli() {
        // COB_CODE: MOVE 'POLI' TO WK-TABELLA.
        ws.setWkTabella("POLI");
        // COB_CODE: PERFORM ESTR-SEQUENCE
        //              THRU ESTR-SEQUENCE-EX.
        estrSequence();
        // COB_CODE: MOVE 'MOVI' TO WK-TABELLA.
        ws.setWkTabella("MOVI");
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              MOVE S090-SEQ-TABELLA TO WPOL-ID-PTF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE S090-SEQ-TABELLA TO WPOL-ID-PTF
            wpolAreaPolizza.getLccvpol1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
        }
    }

    /**Original name: ESTR-SEQUENCE-ADES<br>
	 * <pre>----------------------------------------------------------------*
	 *     ESTRAZIONE SEQUENCE ADESIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void estrSequenceAdes() {
        // COB_CODE: MOVE 'ADES' TO WK-TABELLA.
        ws.setWkTabella("ADES");
        // COB_CODE: PERFORM ESTR-SEQUENCE
        //              THRU ESTR-SEQUENCE-EX.
        estrSequence();
        // COB_CODE: MOVE 'MOVI' TO WK-TABELLA.
        ws.setWkTabella("MOVI");
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                TO WADE-ID-PTF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: MOVE S090-SEQ-TABELLA
            //             TO WADE-ID-PTF
            wadeAreaAdesione.getLccvade1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
        }
    }

    /**Original name: S1200-VAL-IB-MOVIMENTO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA IB MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ValIbMovimento() {
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO WS-COD-COMPAGNIA.
        ws.getWkVariabili().setWsCodCompagnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: MOVE WMOV-ID-PTF      TO  WS-ID-MOVI-PTF.
        ws.getWkVariabili().setWsIdMoviPtf(TruncAbs.toInt(wmovAreaMovimento.getLccvmov1().getIdPtf(), 9));
        // COB_CODE: MOVE WS-ID-MOVI-PTF   TO  WS-SEQUENCE-PTF.
        ws.getWkVariabili().setWsSequencePtfFormatted(ws.getWkVariabili().getWsIdMoviPtfFormatted());
        // COB_CODE: MOVE WS-OGGETTO       TO  WS-MOVIMENTO-APPO.
        ws.getWkVariabili().setWsMovimentoAppoFromBuffer(ws.getWkVariabili().getWsOggettoBytes());
        // COB_CODE: INSPECT WS-MOVIMENTO-APPO REPLACING ALL ' ' BY '0'.
        ws.getWkVariabili().setWsMovimentoAppoFormatted(ws.getWkVariabili().getWsMovimentoAppoFormatted().replace(" ", "0"));
        // COB_CODE: MOVE WS-MOVIMENTO-APPO  TO  WMOV-IB-MOVI.
        wmovAreaMovimento.getLccvmov1().getDati().setWmovIbMovi(ws.getWkVariabili().getWsMovimentoAppo());
    }

    /**Original name: S1210-PREPARA-AREA-LCCS0234<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER CALL LCCS0234
	 * ----------------------------------------------------------------*</pre>*/
    private void s1210PreparaAreaLccs0234() {
        // COB_CODE: MOVE WMOV-ID-OGG                TO S234-ID-OGG-EOC.
        ws.getS234DatiInput().setIdOggEoc(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdOgg().getWmovIdOgg());
        // COB_CODE: MOVE WMOV-TP-OGG                TO S234-TIPO-OGG-EOC.
        ws.getS234DatiInput().setTipoOggEoc(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpOgg());
    }

    /**Original name: S1220-VALORIZZA-ID-OGG-MOV<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA ID OGGETTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1220ValorizzaIdOggMov() {
        // COB_CODE: MOVE S234-ID-OGG-PTF-EOC             TO  MOV-ID-OGG
        //                                                    WMOV-ID-OGG.
        ws.getMovi().getMovIdOgg().setMovIdOgg(ws.getS234DatiOutput().getIdOggPtfEoc());
        wmovAreaMovimento.getLccvmov1().getDati().getWmovIdOgg().setWmovIdOgg(ws.getS234DatiOutput().getIdOggPtfEoc());
        // COB_CODE: MOVE S234-IB-OGG-PTF-EOC             TO  MOV-IB-OGG
        //                                                    WMOV-IB-OGG.
        ws.getMovi().setMovIbOgg(ws.getS234DatiOutput().getIbOggPtfEoc());
        wmovAreaMovimento.getLccvmov1().getDati().setWmovIbOgg(ws.getS234DatiOutput().getIbOggPtfEoc());
    }

    /**Original name: PREVAL-DCLGEN-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     PRE VALORIZZA DCLGEN MOVI
	 * ----------------------------------------------------------------*</pre>*/
    private void prevalDclgenMovi() {
        // COB_CODE: EVALUATE TRUE
        //               WHEN WMOV-ST-ADD
        //                     THRU S1200-VAL-IB-MOVIMENTO-EX
        //           END-EVALUATE.
        switch (wmovAreaMovimento.getLccvmov1().getStatus().getStatus()) {

            case WpolStatus.ADD:// COB_CODE:             IF WMOV-TP-OGG-NULL = HIGH-VALUE
                //                          CONTINUE
                //                       ELSE
                //           *--            Estrazione IB-OGGETTO/ID-OGGETTO
                //                          END-IF
                //                       END-IF
                if (Characters.EQ_HIGH.test(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpOggFormatted())) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    //--            Estrazione IB-OGGETTO/ID-OGGETTO
                    // COB_CODE: PERFORM S1210-PREPARA-AREA-LCCS0234
                    //              THRU S1210-EX
                    s1210PreparaAreaLccs0234();
                    // COB_CODE: PERFORM CALL-LCCS0234
                    //              THRU CALL-LCCS0234-EX
                    callLccs0234();
                    //--            Valorizzazione ID-OGGETTO e IB-OGGETTO
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //                 THRU S1220-EX
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: PERFORM S1220-VALORIZZA-ID-OGG-MOV
                        //              THRU S1220-EX
                        s1220ValorizzaIdOggMov();
                    }
                }
                //--         Valorizzazione ID-RICH Moviemnto
                // COB_CODE: IF  IDSV0001-ESITO-OK
                //           AND LCCV0005-RICHIESTA-SI
                //               END-IF
                //           END-IF
                if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && lccv0005.getRichiesta().isSi()) {
                    // COB_CODE: IF  WRIC-ID-RICH IS NUMERIC
                    //           AND WRIC-ID-RICH > ZERO
                    //               MOVE WRIC-ID-RICH   TO WMOV-ID-RICH
                    //           ELSE
                    //               MOVE HIGH-VALUE     TO WMOV-ID-RICH-NULL
                    //           END-IF
                    if (Functions.isNumber(wricAreaRichiesta.getLccvric1().getDati().getWricIdRich()) && wricAreaRichiesta.getLccvric1().getDati().getWricIdRich() > 0) {
                        // COB_CODE: MOVE WRIC-ID-RICH   TO WMOV-ID-RICH
                        wmovAreaMovimento.getLccvmov1().getDati().getWmovIdRich().setWmovIdRich(wricAreaRichiesta.getLccvric1().getDati().getWricIdRich());
                    }
                    else {
                        // COB_CODE: MOVE HIGH-VALUE     TO WMOV-ID-RICH-NULL
                        wmovAreaMovimento.getLccvmov1().getDati().getWmovIdRich().setWmovIdRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WmovIdRich.Len.WMOV_ID_RICH_NULL));
                    }
                }
                //--         Valorizzazione IB-Movimento
                // COB_CODE: PERFORM S1200-VAL-IB-MOVIMENTO
                //              THRU S1200-VAL-IB-MOVIMENTO-EX
                s1200ValIbMovimento();
                break;

            default:break;
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-MOV<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void valorizzaAreaDshMov() {
        // COB_CODE: MOVE 'MOVI'                  TO   WK-TABELLA.
        ws.setWkTabella("MOVI");
        //--> DCLGEN TABELLA
        // COB_CODE: MOVE MOVI                    TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getMovi().getMoviFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-PRIMARY-KEY    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(wmovAreaMovimento.getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: VAL-DCLGEN-RIC<br>
	 * <pre>--  RICHIESTA
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVRIC5
	 *    ULTIMO AGG. 21 NOV 2013
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenRic() {
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA
        //              TO RIC-COD-COMP-ANIA
        ws.getRich().setRicCodCompAnia(wricAreaRichiesta.getLccvric1().getDati().getWricCodCompAnia());
        // COB_CODE: MOVE (SF)-TP-RICH
        //              TO RIC-TP-RICH
        ws.getRich().setRicTpRich(wricAreaRichiesta.getLccvric1().getDati().getWricTpRich());
        // COB_CODE: MOVE (SF)-COD-MACROFUNCT
        //              TO RIC-COD-MACROFUNCT
        ws.getRich().setRicCodMacrofunct(wricAreaRichiesta.getLccvric1().getDati().getWricCodMacrofunct());
        // COB_CODE: MOVE (SF)-TP-MOVI
        //              TO RIC-TP-MOVI
        ws.getRich().setRicTpMovi(wricAreaRichiesta.getLccvric1().getDati().getWricTpMovi());
        // COB_CODE: IF (SF)-IB-RICH-NULL = HIGH-VALUES
        //              TO RIC-IB-RICH-NULL
        //           ELSE
        //              TO RIC-IB-RICH
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricIbRich(), WricDati.Len.WRIC_IB_RICH)) {
            // COB_CODE: MOVE (SF)-IB-RICH-NULL
            //           TO RIC-IB-RICH-NULL
            ws.getRich().setRicIbRich(wricAreaRichiesta.getLccvric1().getDati().getWricIbRich());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-RICH
            //           TO RIC-IB-RICH
            ws.getRich().setRicIbRich(wricAreaRichiesta.getLccvric1().getDati().getWricIbRich());
        }
        // COB_CODE: MOVE (SF)-DT-EFF
        //              TO RIC-DT-EFF
        ws.getRich().setRicDtEff(wricAreaRichiesta.getLccvric1().getDati().getWricDtEff());
        // COB_CODE: MOVE (SF)-DT-RGSTRZ-RICH
        //              TO RIC-DT-RGSTRZ-RICH
        ws.getRich().setRicDtRgstrzRich(wricAreaRichiesta.getLccvric1().getDati().getWricDtRgstrzRich());
        // COB_CODE: MOVE (SF)-DT-PERV-RICH
        //              TO RIC-DT-PERV-RICH
        ws.getRich().setRicDtPervRich(wricAreaRichiesta.getLccvric1().getDati().getWricDtPervRich());
        // COB_CODE: MOVE (SF)-DT-ESEC-RICH
        //              TO RIC-DT-ESEC-RICH
        ws.getRich().setRicDtEsecRich(wricAreaRichiesta.getLccvric1().getDati().getWricDtEsecRich());
        // COB_CODE: IF (SF)-TS-EFF-ESEC-RICH-NULL = HIGH-VALUES
        //              TO RIC-TS-EFF-ESEC-RICH-NULL
        //           ELSE
        //              TO RIC-TS-EFF-ESEC-RICH
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricTsEffEsecRich().getWricTsEffEsecRichNullFormatted())) {
            // COB_CODE: MOVE (SF)-TS-EFF-ESEC-RICH-NULL
            //           TO RIC-TS-EFF-ESEC-RICH-NULL
            ws.getRich().getRicTsEffEsecRich().setRicTsEffEsecRichNull(wricAreaRichiesta.getLccvric1().getDati().getWricTsEffEsecRich().getWricTsEffEsecRichNull());
        }
        else {
            // COB_CODE: MOVE (SF)-TS-EFF-ESEC-RICH
            //           TO RIC-TS-EFF-ESEC-RICH
            ws.getRich().getRicTsEffEsecRich().setRicTsEffEsecRich(wricAreaRichiesta.getLccvric1().getDati().getWricTsEffEsecRich().getWricTsEffEsecRich());
        }
        // COB_CODE: IF (SF)-TP-OGG-NULL = HIGH-VALUES
        //              TO RIC-TP-OGG-NULL
        //           ELSE
        //              TO RIC-TP-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricTpOggFormatted())) {
            // COB_CODE: MOVE (SF)-TP-OGG-NULL
            //           TO RIC-TP-OGG-NULL
            ws.getRich().setRicTpOgg(wricAreaRichiesta.getLccvric1().getDati().getWricTpOgg());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-OGG
            //           TO RIC-TP-OGG
            ws.getRich().setRicTpOgg(wricAreaRichiesta.getLccvric1().getDati().getWricTpOgg());
        }
        // COB_CODE: IF (SF)-IB-POLI-NULL = HIGH-VALUES
        //              TO RIC-IB-POLI-NULL
        //           ELSE
        //              TO RIC-IB-POLI
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricIbPoli(), WricDati.Len.WRIC_IB_POLI)) {
            // COB_CODE: MOVE (SF)-IB-POLI-NULL
            //           TO RIC-IB-POLI-NULL
            ws.getRich().setRicIbPoli(wricAreaRichiesta.getLccvric1().getDati().getWricIbPoli());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-POLI
            //           TO RIC-IB-POLI
            ws.getRich().setRicIbPoli(wricAreaRichiesta.getLccvric1().getDati().getWricIbPoli());
        }
        // COB_CODE: IF (SF)-IB-ADES-NULL = HIGH-VALUES
        //              TO RIC-IB-ADES-NULL
        //           ELSE
        //              TO RIC-IB-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricIbAdes(), WricDati.Len.WRIC_IB_ADES)) {
            // COB_CODE: MOVE (SF)-IB-ADES-NULL
            //           TO RIC-IB-ADES-NULL
            ws.getRich().setRicIbAdes(wricAreaRichiesta.getLccvric1().getDati().getWricIbAdes());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-ADES
            //           TO RIC-IB-ADES
            ws.getRich().setRicIbAdes(wricAreaRichiesta.getLccvric1().getDati().getWricIbAdes());
        }
        // COB_CODE: IF (SF)-IB-GAR-NULL = HIGH-VALUES
        //              TO RIC-IB-GAR-NULL
        //           ELSE
        //              TO RIC-IB-GAR
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricIbGar(), WricDati.Len.WRIC_IB_GAR)) {
            // COB_CODE: MOVE (SF)-IB-GAR-NULL
            //           TO RIC-IB-GAR-NULL
            ws.getRich().setRicIbGar(wricAreaRichiesta.getLccvric1().getDati().getWricIbGar());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-GAR
            //           TO RIC-IB-GAR
            ws.getRich().setRicIbGar(wricAreaRichiesta.getLccvric1().getDati().getWricIbGar());
        }
        // COB_CODE: IF (SF)-IB-TRCH-DI-GAR-NULL = HIGH-VALUES
        //              TO RIC-IB-TRCH-DI-GAR-NULL
        //           ELSE
        //              TO RIC-IB-TRCH-DI-GAR
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricIbTrchDiGar(), WricDati.Len.WRIC_IB_TRCH_DI_GAR)) {
            // COB_CODE: MOVE (SF)-IB-TRCH-DI-GAR-NULL
            //           TO RIC-IB-TRCH-DI-GAR-NULL
            ws.getRich().setRicIbTrchDiGar(wricAreaRichiesta.getLccvric1().getDati().getWricIbTrchDiGar());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-TRCH-DI-GAR
            //           TO RIC-IB-TRCH-DI-GAR
            ws.getRich().setRicIbTrchDiGar(wricAreaRichiesta.getLccvric1().getDati().getWricIbTrchDiGar());
        }
        // COB_CODE: IF (SF)-ID-BATCH-NULL = HIGH-VALUES
        //              TO RIC-ID-BATCH-NULL
        //           ELSE
        //              TO RIC-ID-BATCH
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricIdBatch().getWricIdBatchNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-BATCH-NULL
            //           TO RIC-ID-BATCH-NULL
            ws.getRich().getRicIdBatch().setRicIdBatchNull(wricAreaRichiesta.getLccvric1().getDati().getWricIdBatch().getWricIdBatchNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-BATCH
            //           TO RIC-ID-BATCH
            ws.getRich().getRicIdBatch().setRicIdBatch(wricAreaRichiesta.getLccvric1().getDati().getWricIdBatch().getWricIdBatch());
        }
        // COB_CODE: IF (SF)-ID-JOB-NULL = HIGH-VALUES
        //              TO RIC-ID-JOB-NULL
        //           ELSE
        //              TO RIC-ID-JOB
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricIdJob().getWricIdJobNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-JOB-NULL
            //           TO RIC-ID-JOB-NULL
            ws.getRich().getRicIdJob().setRicIdJobNull(wricAreaRichiesta.getLccvric1().getDati().getWricIdJob().getWricIdJobNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-JOB
            //           TO RIC-ID-JOB
            ws.getRich().getRicIdJob().setRicIdJob(wricAreaRichiesta.getLccvric1().getDati().getWricIdJob().getWricIdJob());
        }
        // COB_CODE: IF (SF)-FL-SIMULAZIONE-NULL = HIGH-VALUES
        //              TO RIC-FL-SIMULAZIONE-NULL
        //           ELSE
        //              TO RIC-FL-SIMULAZIONE
        //           END-IF
        if (Conditions.eq(wricAreaRichiesta.getLccvric1().getDati().getWricFlSimulazione(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE (SF)-FL-SIMULAZIONE-NULL
            //           TO RIC-FL-SIMULAZIONE-NULL
            ws.getRich().setRicFlSimulazione(wricAreaRichiesta.getLccvric1().getDati().getWricFlSimulazione());
        }
        else {
            // COB_CODE: MOVE (SF)-FL-SIMULAZIONE
            //           TO RIC-FL-SIMULAZIONE
            ws.getRich().setRicFlSimulazione(wricAreaRichiesta.getLccvric1().getDati().getWricFlSimulazione());
        }
        // COB_CODE: MOVE (SF)-KEY-ORDINAMENTO
        //              TO RIC-KEY-ORDINAMENTO
        ws.getRich().setRicKeyOrdinamento(wricAreaRichiesta.getLccvric1().getDati().getWricKeyOrdinamento());
        // COB_CODE: IF (SF)-ID-RICH-COLLG-NULL = HIGH-VALUES
        //              TO RIC-ID-RICH-COLLG-NULL
        //           ELSE
        //              TO RIC-ID-RICH-COLLG
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricIdRichCollg().getWricIdRichCollgNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-RICH-COLLG-NULL
            //           TO RIC-ID-RICH-COLLG-NULL
            ws.getRich().getRicIdRichCollg().setRicIdRichCollgNull(wricAreaRichiesta.getLccvric1().getDati().getWricIdRichCollg().getWricIdRichCollgNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-RICH-COLLG
            //           TO RIC-ID-RICH-COLLG
            ws.getRich().getRicIdRichCollg().setRicIdRichCollg(wricAreaRichiesta.getLccvric1().getDati().getWricIdRichCollg().getWricIdRichCollg());
        }
        // COB_CODE: IF (SF)-TP-RAMO-BILA-NULL = HIGH-VALUES
        //              TO RIC-TP-RAMO-BILA-NULL
        //           ELSE
        //              TO RIC-TP-RAMO-BILA
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricTpRamoBilaFormatted())) {
            // COB_CODE: MOVE (SF)-TP-RAMO-BILA-NULL
            //           TO RIC-TP-RAMO-BILA-NULL
            ws.getRich().setRicTpRamoBila(wricAreaRichiesta.getLccvric1().getDati().getWricTpRamoBila());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-RAMO-BILA
            //           TO RIC-TP-RAMO-BILA
            ws.getRich().setRicTpRamoBila(wricAreaRichiesta.getLccvric1().getDati().getWricTpRamoBila());
        }
        // COB_CODE: IF (SF)-TP-FRM-ASSVA-NULL = HIGH-VALUES
        //              TO RIC-TP-FRM-ASSVA-NULL
        //           ELSE
        //              TO RIC-TP-FRM-ASSVA
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricTpFrmAssvaFormatted())) {
            // COB_CODE: MOVE (SF)-TP-FRM-ASSVA-NULL
            //           TO RIC-TP-FRM-ASSVA-NULL
            ws.getRich().setRicTpFrmAssva(wricAreaRichiesta.getLccvric1().getDati().getWricTpFrmAssva());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-FRM-ASSVA
            //           TO RIC-TP-FRM-ASSVA
            ws.getRich().setRicTpFrmAssva(wricAreaRichiesta.getLccvric1().getDati().getWricTpFrmAssva());
        }
        // COB_CODE: IF (SF)-TP-CALC-RIS-NULL = HIGH-VALUES
        //              TO RIC-TP-CALC-RIS-NULL
        //           ELSE
        //              TO RIC-TP-CALC-RIS
        //           END-IF
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricTpCalcRisFormatted())) {
            // COB_CODE: MOVE (SF)-TP-CALC-RIS-NULL
            //           TO RIC-TP-CALC-RIS-NULL
            ws.getRich().setRicTpCalcRis(wricAreaRichiesta.getLccvric1().getDati().getWricTpCalcRis());
        }
        else {
            // COB_CODE: MOVE (SF)-TP-CALC-RIS
            //           TO RIC-TP-CALC-RIS
            ws.getRich().setRicTpCalcRis(wricAreaRichiesta.getLccvric1().getDati().getWricTpCalcRis());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL
        //              TO RIC-DS-OPER-SQL
        ws.getRich().setRicDsOperSql(wricAreaRichiesta.getLccvric1().getDati().getWricDsOperSql());
        // COB_CODE: IF (SF)-DS-VER NOT NUMERIC
        //              MOVE 0 TO RIC-DS-VER
        //           ELSE
        //              TO RIC-DS-VER
        //           END-IF
        if (!Functions.isNumber(wricAreaRichiesta.getLccvric1().getDati().getWricDsVer())) {
            // COB_CODE: MOVE 0 TO RIC-DS-VER
            ws.getRich().setRicDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER
            //           TO RIC-DS-VER
            ws.getRich().setRicDsVer(wricAreaRichiesta.getLccvric1().getDati().getWricDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-CPTZ NOT NUMERIC
        //              MOVE 0 TO RIC-DS-TS-CPTZ
        //           ELSE
        //              TO RIC-DS-TS-CPTZ
        //           END-IF
        if (!Functions.isNumber(wricAreaRichiesta.getLccvric1().getDati().getWricDsTsCptz())) {
            // COB_CODE: MOVE 0 TO RIC-DS-TS-CPTZ
            ws.getRich().setRicDsTsCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-CPTZ
            //           TO RIC-DS-TS-CPTZ
            ws.getRich().setRicDsTsCptz(wricAreaRichiesta.getLccvric1().getDati().getWricDsTsCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE
        //              TO RIC-DS-UTENTE
        ws.getRich().setRicDsUtente(wricAreaRichiesta.getLccvric1().getDati().getWricDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB
        //              TO RIC-DS-STATO-ELAB
        ws.getRich().setRicDsStatoElab(wricAreaRichiesta.getLccvric1().getDati().getWricDsStatoElab());
        // COB_CODE: IF (SF)-RAMO-BILA-NULL = HIGH-VALUES
        //              TO RIC-RAMO-BILA-NULL
        //           ELSE
        //              TO RIC-RAMO-BILA
        //           END-IF.
        if (Characters.EQ_HIGH.test(wricAreaRichiesta.getLccvric1().getDati().getWricRamoBilaFormatted())) {
            // COB_CODE: MOVE (SF)-RAMO-BILA-NULL
            //           TO RIC-RAMO-BILA-NULL
            ws.getRich().setRicRamoBila(wricAreaRichiesta.getLccvric1().getDati().getWricRamoBila());
        }
        else {
            // COB_CODE: MOVE (SF)-RAMO-BILA
            //           TO RIC-RAMO-BILA
            ws.getRich().setRicRamoBila(wricAreaRichiesta.getLccvric1().getDati().getWricRamoBila());
        }
    }

    /**Original name: AGGIORNA-RICH<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVRIC6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO RICHIESTA
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCVSDI5 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN STRATEGIA DI INVESTIMENTO (LCCVSDI1)
	 *   - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI</pre>*/
    private void aggiornaRich() {
        // COB_CODE: INITIALIZE RICH
        initRich();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'RICH'                        TO WK-TABELLA
        ws.setWkTabella("RICH");
        //--> IN ATTESA DELLA GESTIONE DINAMICA
        // COB_CODE: MOVE 'PG'                          TO WRIC-COD-MACROFUNCT.
        wricAreaRichiesta.getLccvric1().getDati().setWricCodMacrofunct("PG");
        //--> SE LO STATUS NON E' "INVARIATO"
        // COB_CODE:      IF NOT WRIC-ST-INV
        //                   AND WRIC-ELE-RICH-MAX NOT = 0
        //           *-->    CONTROLLO DELLO STATUS
        //                END-IF.
        if (!wricAreaRichiesta.getLccvric1().getStatus().isInv() && wricAreaRichiesta.getWricEleRichMax() != 0) {
            //-->    CONTROLLO DELLO STATUS
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WRIC-ST-ADD
            //           *-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
            //                          SET  IDSI0011-INSERT         TO TRUE
            //           *-->        MODIFICA
            //                       WHEN WRIC-ST-MOD
            //                          SET  IDSI0011-UPDATE         TO TRUE
            //           *-->        CANCELLAZIONE
            //                       WHEN WRIC-ST-DEL
            //                          SET  IDSI0011-DELETE         TO TRUE
            //                END-EVALUATE
            switch (wricAreaRichiesta.getLccvric1().getStatus().getStatus()) {

                case WpolStatus.ADD://-->           ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                    // COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              END-IF
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA          TO WRIC-ID-PTF
                        wricAreaRichiesta.getLccvric1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
                        // COB_CODE: MOVE WRIC-ID-PTF               TO  RIC-ID-RICH
                        //                                             WRIC-ID-RICH
                        ws.getRich().setRicIdRich(wricAreaRichiesta.getLccvric1().getIdPtf());
                        wricAreaRichiesta.getLccvric1().getDati().setWricIdRich(wricAreaRichiesta.getLccvric1().getIdPtf());
                        //-->              VALORIZZAZIONE  IB RICHIESTA
                        // COB_CODE: PERFORM VAL-IB-RICHIESTA
                        //              THRU VAL-IB-RICHIESTA-EX
                        valIbRichiesta();
                        //-->              PREPARAZIONE ED ESTRAZIONE OGGETTO PTF
                        // COB_CODE: PERFORM PREPARA-AREA-LCCS0234-RIC
                        //              THRU PREPARA-AREA-LCCS0234-RIC-EX
                        preparaAreaLccs0234Ric();
                        // COB_CODE: PERFORM CALL-LCCS0234
                        //              THRU CALL-LCCS0234-EX
                        callLccs0234();
                        // COB_CODE: IF IDSV0001-ESITO-OK
                        //              END-EVALUATE
                        //           END-IF
                        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                            // COB_CODE: MOVE S234-ID-OGG-PTF-EOC
                            //             TO RIC-ID-OGG
                            ws.getRich().getRicIdOgg().setRicIdOgg(ws.getS234DatiOutput().getIdOggPtfEoc());
                            // COB_CODE: EVALUATE WRIC-TP-OGG
                            //              WHEN WS-POLIZZA
                            //                   TO WRIC-IB-POLI
                            //              WHEN WS-ADESIONE
                            //                   TO WRIC-IB-ADES
                            //              WHEN WS-GARANZIA
                            //                   TO WRIC-IB-GAR
                            //              WHEN WS-TRANCHE
                            //                   TO WRIC-IB-TRCH-DI-GAR
                            //           END-EVALUATE
                            if (Conditions.eq(wricAreaRichiesta.getLccvric1().getDati().getWricTpOgg(), ws.getWsTpOgg().getPolizza())) {
                                // COB_CODE: MOVE S234-IB-OGG-PTF-EOC
                                //             TO WRIC-IB-POLI
                                wricAreaRichiesta.getLccvric1().getDati().setWricIbPoli(ws.getS234DatiOutput().getIbOggPtfEoc());
                            }
                            else if (Conditions.eq(wricAreaRichiesta.getLccvric1().getDati().getWricTpOgg(), ws.getWsTpOgg().getAdesione())) {
                                // COB_CODE: MOVE S234-IB-OGG-PTF-EOC
                                //             TO WRIC-IB-ADES
                                wricAreaRichiesta.getLccvric1().getDati().setWricIbAdes(ws.getS234DatiOutput().getIbOggPtfEoc());
                            }
                            else if (Conditions.eq(wricAreaRichiesta.getLccvric1().getDati().getWricTpOgg(), ws.getWsTpOgg().getGaranzia())) {
                                // COB_CODE: MOVE S234-IB-OGG-PTF-EOC
                                //             TO WRIC-IB-GAR
                                wricAreaRichiesta.getLccvric1().getDati().setWricIbGar(ws.getS234DatiOutput().getIbOggPtfEoc());
                            }
                            else if (Conditions.eq(wricAreaRichiesta.getLccvric1().getDati().getWricTpOgg(), ws.getWsTpOgg().getTranche())) {
                                // COB_CODE: MOVE S234-IB-OGG-PTF-EOC
                                //             TO WRIC-IB-TRCH-DI-GAR
                                wricAreaRichiesta.getLccvric1().getDati().setWricIbTrchDiGar(ws.getS234DatiOutput().getIbOggPtfEoc());
                            }
                        }
                    }
                    // COB_CODE: MOVE WMOV-TP-MOVI              TO  WRIC-TP-MOVI
                    wricAreaRichiesta.getLccvric1().getDati().setWricTpMovi(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpMovi().getWmovTpMovi());
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-INSERT         TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
                    //-->        MODIFICA
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WRIC-ID-RICH       TO RIC-ID-RICH
                    ws.getRich().setRicIdRich(wricAreaRichiesta.getLccvric1().getDati().getWricIdRich());
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-UPDATE         TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Update();
                    //-->        CANCELLAZIONE
                    break;

                case WpolStatus.DEL:// COB_CODE: MOVE WRIC-ID-RICH       TO RIC-ID-RICH
                    ws.getRich().setRicIdRich(wricAreaRichiesta.getLccvric1().getDati().getWricIdRich());
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-DELETE         TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Delete();
                    break;

                default:break;
            }
            // COB_CODE:      IF IDSV0001-ESITO-OK
            //           *-->    VALORIZZA DCLGEN ADESIONE
            //                      THRU AGGIORNA-TABELLA-EX
            //                END-IF.
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->    VALORIZZA DCLGEN ADESIONE
                // COB_CODE: PERFORM VAL-DCLGEN-RIC
                //              THRU VAL-DCLGEN-RIC-EX
                valDclgenRic();
                //-->    VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-RIC
                //              THRU VALORIZZA-AREA-DSH-RIC-EX
                valorizzaAreaDshRic();
                //-->    CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-RIC<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshRic() {
        // COB_CODE: MOVE RICH                    TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getRich().getRichFormatted());
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        // COB_CODE: SET IDSI0011-PRIMARY-KEY      TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: VAL-IB-RICHIESTA<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA IB RICHIESTA
	 * ----------------------------------------------------------------*
	 * --> L'IB RICHIESTA E' COMPOSTO DAL CODICE COMPAGNIA +
	 * --> SEQUENCE DELLA RICHIESTA.</pre>*/
    private void valIbRichiesta() {
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO WS-COD-COMPAGNIA.
        ws.getWkVariabili().setWsCodCompagnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: MOVE WRIC-ID-PTF           TO  WS-ID-RICH-PTF.
        ws.getWkVariabili().setWsIdRichPtf(TruncAbs.toInt(wricAreaRichiesta.getLccvric1().getIdPtf(), 9));
        // COB_CODE: MOVE WS-ID-RICH-PTF        TO  WS-SEQUENCE-PTF.
        ws.getWkVariabili().setWsSequencePtfFormatted(ws.getWkVariabili().getWsIdRichPtfFormatted());
        // COB_CODE: MOVE WS-OGGETTO            TO  WS-RICHIESTA.
        ws.getWkVariabili().setWsRichiestaFromBuffer(ws.getWkVariabili().getWsOggettoBytes());
        // COB_CODE: INSPECT WS-RICHIESTA
        //                REPLACING ALL ' ' BY '0'.
        ws.getWkVariabili().setWsRichiestaFormatted(ws.getWkVariabili().getWsRichiestaFormatted().replace(" ", "0"));
        // COB_CODE: MOVE WS-RICHIESTA       TO WRIC-IB-RICH.
        wricAreaRichiesta.getLccvric1().getDati().setWricIbRich(ws.getWkVariabili().getWsRichiesta());
    }

    /**Original name: PREPARA-AREA-LCCS0234-RIC<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER CALL LCCS0234
	 * ----------------------------------------------------------------*</pre>*/
    private void preparaAreaLccs0234Ric() {
        // COB_CODE: MOVE WMOV-ID-OGG             TO WRIC-ID-OGG
        wricAreaRichiesta.getLccvric1().getDati().getWricIdOgg().setWricIdOgg(wmovAreaMovimento.getLccvmov1().getDati().getWmovIdOgg().getWmovIdOgg());
        // COB_CODE: MOVE WMOV-TP-OGG             TO WRIC-TP-OGG
        wricAreaRichiesta.getLccvric1().getDati().setWricTpOgg(wmovAreaMovimento.getLccvmov1().getDati().getWmovTpOgg());
        // COB_CODE: MOVE WRIC-ID-OGG             TO S234-ID-OGG-EOC.
        ws.getS234DatiInput().setIdOggEoc(wricAreaRichiesta.getLccvric1().getDati().getWricIdOgg().getWricIdOgg());
        // COB_CODE: MOVE WRIC-TP-OGG             TO S234-TIPO-OGG-EOC.
        ws.getS234DatiInput().setTipoOggEoc(wricAreaRichiesta.getLccvric1().getDati().getWricTpOgg());
    }

    /**Original name: VAL-DCLGEN-ODE<br>
	 * <pre>--  OGGETTO DEROGA
	 * ----------------------------------------------------------------*
	 *     COPY VALORIZZAZIONE DCLGEN
	 * ----------------------------------------------------------------*
	 *      COPY LCCVRIC5.
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVODE5
	 *    ULTIMO AGG. 04 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenOde() {
        // COB_CODE: IF (SF)-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              TO ODE-ID-MOVI-CHIU-NULL
        //           ELSE
        //              TO ODE-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWkVariabili().getLccvode1().getDati().getWodeIdMoviChiu().getWodeIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU-NULL
            //           TO ODE-ID-MOVI-CHIU-NULL
            ws.getOggDeroga().getOdeIdMoviChiu().setOdeIdMoviChiuNull(ws.getWkVariabili().getLccvode1().getDati().getWodeIdMoviChiu().getWodeIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU
            //           TO ODE-ID-MOVI-CHIU
            ws.getOggDeroga().getOdeIdMoviChiu().setOdeIdMoviChiu(ws.getWkVariabili().getLccvode1().getDati().getWodeIdMoviChiu().getWodeIdMoviChiu());
        }
        // COB_CODE: IF (SF)-DT-INI-EFF NOT NUMERIC
        //              MOVE 0 TO ODE-DT-INI-EFF
        //           ELSE
        //              TO ODE-DT-INI-EFF
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getLccvode1().getDati().getWodeDtIniEff())) {
            // COB_CODE: MOVE 0 TO ODE-DT-INI-EFF
            ws.getOggDeroga().setOdeDtIniEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-EFF
            //           TO ODE-DT-INI-EFF
            ws.getOggDeroga().setOdeDtIniEff(ws.getWkVariabili().getLccvode1().getDati().getWodeDtIniEff());
        }
        // COB_CODE: IF (SF)-DT-END-EFF NOT NUMERIC
        //              MOVE 0 TO ODE-DT-END-EFF
        //           ELSE
        //              TO ODE-DT-END-EFF
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getLccvode1().getDati().getWodeDtEndEff())) {
            // COB_CODE: MOVE 0 TO ODE-DT-END-EFF
            ws.getOggDeroga().setOdeDtEndEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-END-EFF
            //           TO ODE-DT-END-EFF
            ws.getOggDeroga().setOdeDtEndEff(ws.getWkVariabili().getLccvode1().getDati().getWodeDtEndEff());
        }
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA
        //              TO ODE-COD-COMP-ANIA
        ws.getOggDeroga().setOdeCodCompAnia(ws.getWkVariabili().getLccvode1().getDati().getWodeCodCompAnia());
        // COB_CODE: MOVE (SF)-TP-OGG
        //              TO ODE-TP-OGG
        ws.getOggDeroga().setOdeTpOgg(ws.getWkVariabili().getLccvode1().getDati().getWodeTpOgg());
        // COB_CODE: IF (SF)-IB-OGG-NULL = HIGH-VALUES
        //              TO ODE-IB-OGG-NULL
        //           ELSE
        //              TO ODE-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWkVariabili().getLccvode1().getDati().getWodeIbOgg(), WodeDati.Len.WODE_IB_OGG)) {
            // COB_CODE: MOVE (SF)-IB-OGG-NULL
            //           TO ODE-IB-OGG-NULL
            ws.getOggDeroga().setOdeIbOgg(ws.getWkVariabili().getLccvode1().getDati().getWodeIbOgg());
        }
        else {
            // COB_CODE: MOVE (SF)-IB-OGG
            //           TO ODE-IB-OGG
            ws.getOggDeroga().setOdeIbOgg(ws.getWkVariabili().getLccvode1().getDati().getWodeIbOgg());
        }
        // COB_CODE: MOVE (SF)-TP-DEROGA
        //              TO ODE-TP-DEROGA
        ws.getOggDeroga().setOdeTpDeroga(ws.getWkVariabili().getLccvode1().getDati().getWodeTpDeroga());
        // COB_CODE: MOVE (SF)-COD-GR-AUT-APPRT
        //              TO ODE-COD-GR-AUT-APPRT
        ws.getOggDeroga().setOdeCodGrAutApprt(ws.getWkVariabili().getLccvode1().getDati().getWodeCodGrAutApprt());
        // COB_CODE: IF (SF)-COD-LIV-AUT-APPRT-NULL = HIGH-VALUES
        //              TO ODE-COD-LIV-AUT-APPRT-NULL
        //           ELSE
        //              TO ODE-COD-LIV-AUT-APPRT
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWkVariabili().getLccvode1().getDati().getWodeCodLivAutApprt().getWodeCodLivAutApprtNullFormatted())) {
            // COB_CODE: MOVE (SF)-COD-LIV-AUT-APPRT-NULL
            //           TO ODE-COD-LIV-AUT-APPRT-NULL
            ws.getOggDeroga().getOdeCodLivAutApprt().setOdeCodLivAutApprtNull(ws.getWkVariabili().getLccvode1().getDati().getWodeCodLivAutApprt().getWodeCodLivAutApprtNull());
        }
        else {
            // COB_CODE: MOVE (SF)-COD-LIV-AUT-APPRT
            //           TO ODE-COD-LIV-AUT-APPRT
            ws.getOggDeroga().getOdeCodLivAutApprt().setOdeCodLivAutApprt(ws.getWkVariabili().getLccvode1().getDati().getWodeCodLivAutApprt().getWodeCodLivAutApprt());
        }
        // COB_CODE: MOVE (SF)-COD-GR-AUT-SUP
        //              TO ODE-COD-GR-AUT-SUP
        ws.getOggDeroga().setOdeCodGrAutSup(ws.getWkVariabili().getLccvode1().getDati().getWodeCodGrAutSup());
        // COB_CODE: MOVE (SF)-COD-LIV-AUT-SUP
        //              TO ODE-COD-LIV-AUT-SUP
        ws.getOggDeroga().setOdeCodLivAutSup(ws.getWkVariabili().getLccvode1().getDati().getWodeCodLivAutSup());
        // COB_CODE: IF (SF)-DS-RIGA NOT NUMERIC
        //              MOVE 0 TO ODE-DS-RIGA
        //           ELSE
        //              TO ODE-DS-RIGA
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getLccvode1().getDati().getWodeDsRiga())) {
            // COB_CODE: MOVE 0 TO ODE-DS-RIGA
            ws.getOggDeroga().setOdeDsRiga(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-RIGA
            //           TO ODE-DS-RIGA
            ws.getOggDeroga().setOdeDsRiga(ws.getWkVariabili().getLccvode1().getDati().getWodeDsRiga());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL
        //              TO ODE-DS-OPER-SQL
        ws.getOggDeroga().setOdeDsOperSql(ws.getWkVariabili().getLccvode1().getDati().getWodeDsOperSql());
        // COB_CODE: IF (SF)-DS-VER NOT NUMERIC
        //              MOVE 0 TO ODE-DS-VER
        //           ELSE
        //              TO ODE-DS-VER
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getLccvode1().getDati().getWodeDsVer())) {
            // COB_CODE: MOVE 0 TO ODE-DS-VER
            ws.getOggDeroga().setOdeDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER
            //           TO ODE-DS-VER
            ws.getOggDeroga().setOdeDsVer(ws.getWkVariabili().getLccvode1().getDati().getWodeDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-INI-CPTZ NOT NUMERIC
        //              MOVE 0 TO ODE-DS-TS-INI-CPTZ
        //           ELSE
        //              TO ODE-DS-TS-INI-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getLccvode1().getDati().getWodeDsTsIniCptz())) {
            // COB_CODE: MOVE 0 TO ODE-DS-TS-INI-CPTZ
            ws.getOggDeroga().setOdeDsTsIniCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-INI-CPTZ
            //           TO ODE-DS-TS-INI-CPTZ
            ws.getOggDeroga().setOdeDsTsIniCptz(ws.getWkVariabili().getLccvode1().getDati().getWodeDsTsIniCptz());
        }
        // COB_CODE: IF (SF)-DS-TS-END-CPTZ NOT NUMERIC
        //              MOVE 0 TO ODE-DS-TS-END-CPTZ
        //           ELSE
        //              TO ODE-DS-TS-END-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getLccvode1().getDati().getWodeDsTsEndCptz())) {
            // COB_CODE: MOVE 0 TO ODE-DS-TS-END-CPTZ
            ws.getOggDeroga().setOdeDsTsEndCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-END-CPTZ
            //           TO ODE-DS-TS-END-CPTZ
            ws.getOggDeroga().setOdeDsTsEndCptz(ws.getWkVariabili().getLccvode1().getDati().getWodeDsTsEndCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE
        //              TO ODE-DS-UTENTE
        ws.getOggDeroga().setOdeDsUtente(ws.getWkVariabili().getLccvode1().getDati().getWodeDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB
        //              TO ODE-DS-STATO-ELAB.
        ws.getOggDeroga().setOdeDsStatoElab(ws.getWkVariabili().getLccvode1().getDati().getWodeDsStatoElab());
    }

    /**Original name: AGGIORNA-OGG-DEROGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     PROGRAMMA ..... LCCVODE6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO OGGETTO DEROGA
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCVALL5 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN ASSET ALLOCATION (LCCVALL1)
	 *   - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI</pre>*/
    private void aggiornaOggDeroga() {
        // COB_CODE: INITIALIZE OGG-DEROGA
        initOggDeroga();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'OGG-DEROGA'                   TO WK-TABELLA
        ws.setWkTabella("OGG-DEROGA");
        //--> SE LO STATUS NON E' "INVARIATO"
        // COB_CODE:      IF NOT WODE-ST-INV
        //                   AND WODE-ELE-ODE-MAX NOT = 0
        //           *-->    CONTROLLO DELLO STATUS
        //                   END-IF
        //                END-IF.
        if (!ws.getWkVariabili().getLccvode1().getStatus().isInv() && ws.getWkVariabili().getWodeEleOdeMax() != 0) {
            //-->    CONTROLLO DELLO STATUS
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WODE-ST-ADD
            //                            SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
            //           *-->        MODIFICA
            //                       WHEN WODE-ST-MOD
            //                            SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
            //           *-->        CANCELLAZIONE
            //                       WHEN WODE-ST-DEL
            //                            SET  IDSI0011-DELETE-LOGICA    TO TRUE
            //                   END-EVALUATE
            switch (ws.getWkVariabili().getLccvode1().getStatus().getStatus()) {

                case WpolStatus.ADD:// COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //                                        WODE-ID-MOVI-CRZ
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA  TO WODE-ID-PTF
                        ws.getWkVariabili().getLccvode1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
                        // COB_CODE: MOVE WODE-ID-PTF       TO ODE-ID-OGG-DEROGA
                        //                                     WODE-ID-OGG-DEROGA
                        ws.getOggDeroga().setOdeIdOggDeroga(ws.getWkVariabili().getLccvode1().getIdPtf());
                        ws.getWkVariabili().getLccvode1().getDati().setWodeIdOggDeroga(ws.getWkVariabili().getLccvode1().getIdPtf());
                        // COB_CODE: MOVE WODE-ID-OGG       TO ODE-ID-OGG
                        ws.getOggDeroga().setOdeIdOgg(ws.getWkVariabili().getLccvode1().getDati().getWodeIdOgg());
                        //                   MOVE WMOV-ID-OGG       TO ODE-ID-OGG
                        //                                             WODE-ID-OGG
                        //                   MOVE WMOV-TP-OGG       TO ODE-TP-OGG
                        //                                             WODE-TP-OGG
                        // COB_CODE: MOVE WMOV-ID-PTF       TO ODE-ID-MOVI-CRZ
                        //                                     WODE-ID-MOVI-CRZ
                        ws.getOggDeroga().setOdeIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
                        ws.getWkVariabili().getLccvode1().getDati().setWodeIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
                    }
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    //-->        MODIFICA
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WODE-ID-OGG-DEROGA TO ODE-ID-OGG-DEROGA
                    ws.getOggDeroga().setOdeIdOggDeroga(ws.getWkVariabili().getLccvode1().getDati().getWodeIdOggDeroga());
                    // COB_CODE: MOVE WODE-ID-OGG        TO ODE-ID-OGG
                    ws.getOggDeroga().setOdeIdOgg(ws.getWkVariabili().getLccvode1().getDati().getWodeIdOgg());
                    // COB_CODE: MOVE WODE-TP-OGG        TO ODE-TP-OGG
                    ws.getOggDeroga().setOdeTpOgg(ws.getWkVariabili().getLccvode1().getDati().getWodeTpOgg());
                    // COB_CODE: MOVE WODE-ID-MOVI-CRZ   TO ODE-ID-MOVI-CRZ
                    ws.getOggDeroga().setOdeIdMoviCrz(ws.getWkVariabili().getLccvode1().getDati().getWodeIdMoviCrz());
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    //-->        CANCELLAZIONE
                    break;

                case WpolStatus.DEL:// COB_CODE: MOVE WODE-ID-PTF
                    //             TO ODE-ID-OGG-DEROGA
                    ws.getOggDeroga().setOdeIdOggDeroga(ws.getWkVariabili().getLccvode1().getIdPtf());
                    // COB_CODE: MOVE WODE-ID-OGG
                    //             TO ODE-ID-OGG
                    ws.getOggDeroga().setOdeIdOgg(ws.getWkVariabili().getLccvode1().getDati().getWodeIdOgg());
                    // COB_CODE: MOVE WMOV-ID-PTF
                    //             TO ODE-ID-MOVI-CRZ
                    ws.getOggDeroga().setOdeIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-DELETE-LOGICA    TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011DeleteLogica();
                    break;

                default:break;
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZA DCLGEN
            //                         THRU AGGIORNA-TABELLA-EX
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZA DCLGEN
                // COB_CODE: PERFORM VAL-DCLGEN-ODE
                //              THRU VAL-DCLGEN-ODE-EX
                valDclgenOde();
                //-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-ODE
                //              THRU VALORIZZA-AREA-DSH-ODE-EX
                valorizzaAreaDshOde();
                //-->       CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-ODE<br>
	 * <pre>----------------------------------------------------------------*
	 *     PREPARA AREA PER CALL LCCS0234
	 * ----------------------------------------------------------------*
	 * PREPARA-AREA-LCCS0234-ODE.
	 *     MOVE WODE-ID-OGG              TO S234-ID-OGG-EOC.
	 *     MOVE WODE-TP-OGG              TO S234-TIPO-OGG-EOC.
	 * PREPARA-AREA-LCCS0234-ODE-EX.
	 *     EXIT.
	 * ----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshOde() {
        // COB_CODE: MOVE OGG-DEROGA              TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getOggDeroga().getOggDerogaFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-DEFAULT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(wmovAreaMovimento.getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: VAL-DCLGEN-MDE<br>
	 * <pre>--  MOTIVI DEROGA
	 * ----------------------------------------------------------------*
	 *     COPY VALORIZZAZIONE DCLGEN
	 * ----------------------------------------------------------------*
	 *      COPY LCCVODE5.
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVMDE5
	 *    ULTIMO AGG. 04 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenMde() {
        // COB_CODE: MOVE (SF)-ID-OGG-DEROGA(IX-TAB-MDE)
        //              TO MDE-ID-OGG-DEROGA
        ws.getMotDeroga().setMdeIdOggDeroga(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeIdOggDeroga());
        // COB_CODE: IF (SF)-ID-MOVI-CHIU-NULL(IX-TAB-MDE) = HIGH-VALUES
        //              TO MDE-ID-MOVI-CHIU-NULL
        //           ELSE
        //              TO MDE-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeIdMoviChiu().getWmdeIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU-NULL(IX-TAB-MDE)
            //           TO MDE-ID-MOVI-CHIU-NULL
            ws.getMotDeroga().getMdeIdMoviChiu().setMdeIdMoviChiuNull(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeIdMoviChiu().getWmdeIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE (SF)-ID-MOVI-CHIU(IX-TAB-MDE)
            //           TO MDE-ID-MOVI-CHIU
            ws.getMotDeroga().getMdeIdMoviChiu().setMdeIdMoviChiu(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeIdMoviChiu().getWmdeIdMoviChiu());
        }
        // COB_CODE: IF (SF)-DT-INI-EFF(IX-TAB-MDE) NOT NUMERIC
        //              MOVE 0 TO MDE-DT-INI-EFF
        //           ELSE
        //              TO MDE-DT-INI-EFF
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDtIniEff())) {
            // COB_CODE: MOVE 0 TO MDE-DT-INI-EFF
            ws.getMotDeroga().setMdeDtIniEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-INI-EFF(IX-TAB-MDE)
            //           TO MDE-DT-INI-EFF
            ws.getMotDeroga().setMdeDtIniEff(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDtIniEff());
        }
        // COB_CODE: IF (SF)-DT-END-EFF(IX-TAB-MDE) NOT NUMERIC
        //              MOVE 0 TO MDE-DT-END-EFF
        //           ELSE
        //              TO MDE-DT-END-EFF
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDtEndEff())) {
            // COB_CODE: MOVE 0 TO MDE-DT-END-EFF
            ws.getMotDeroga().setMdeDtEndEff(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DT-END-EFF(IX-TAB-MDE)
            //           TO MDE-DT-END-EFF
            ws.getMotDeroga().setMdeDtEndEff(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDtEndEff());
        }
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA(IX-TAB-MDE)
        //              TO MDE-COD-COMP-ANIA
        ws.getMotDeroga().setMdeCodCompAnia(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeCodCompAnia());
        // COB_CODE: MOVE (SF)-TP-MOT-DEROGA(IX-TAB-MDE)
        //              TO MDE-TP-MOT-DEROGA
        ws.getMotDeroga().setMdeTpMotDeroga(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeTpMotDeroga());
        // COB_CODE: MOVE (SF)-COD-LIV-AUT(IX-TAB-MDE)
        //              TO MDE-COD-LIV-AUT
        ws.getMotDeroga().setMdeCodLivAut(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeCodLivAut());
        // COB_CODE: MOVE (SF)-COD-ERR(IX-TAB-MDE)
        //              TO MDE-COD-ERR
        ws.getMotDeroga().setMdeCodErr(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeCodErr());
        // COB_CODE: MOVE (SF)-TP-ERR(IX-TAB-MDE)
        //              TO MDE-TP-ERR
        ws.getMotDeroga().setMdeTpErr(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeTpErr());
        // COB_CODE: MOVE (SF)-DESC-ERR-BREVE(IX-TAB-MDE)
        //              TO MDE-DESC-ERR-BREVE
        ws.getMotDeroga().setMdeDescErrBreve(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDescErrBreve());
        // COB_CODE: MOVE (SF)-DESC-ERR-EST(IX-TAB-MDE)
        //              TO MDE-DESC-ERR-EST
        ws.getMotDeroga().setMdeDescErrEst(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDescErrEst());
        // COB_CODE: IF (SF)-DS-RIGA(IX-TAB-MDE) NOT NUMERIC
        //              MOVE 0 TO MDE-DS-RIGA
        //           ELSE
        //              TO MDE-DS-RIGA
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsRiga())) {
            // COB_CODE: MOVE 0 TO MDE-DS-RIGA
            ws.getMotDeroga().setMdeDsRiga(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-RIGA(IX-TAB-MDE)
            //           TO MDE-DS-RIGA
            ws.getMotDeroga().setMdeDsRiga(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsRiga());
        }
        // COB_CODE: MOVE (SF)-DS-OPER-SQL(IX-TAB-MDE)
        //              TO MDE-DS-OPER-SQL
        ws.getMotDeroga().setMdeDsOperSql(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsOperSql());
        // COB_CODE: IF (SF)-DS-VER(IX-TAB-MDE) NOT NUMERIC
        //              MOVE 0 TO MDE-DS-VER
        //           ELSE
        //              TO MDE-DS-VER
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsVer())) {
            // COB_CODE: MOVE 0 TO MDE-DS-VER
            ws.getMotDeroga().setMdeDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER(IX-TAB-MDE)
            //           TO MDE-DS-VER
            ws.getMotDeroga().setMdeDsVer(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-INI-CPTZ(IX-TAB-MDE) NOT NUMERIC
        //              MOVE 0 TO MDE-DS-TS-INI-CPTZ
        //           ELSE
        //              TO MDE-DS-TS-INI-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsTsIniCptz())) {
            // COB_CODE: MOVE 0 TO MDE-DS-TS-INI-CPTZ
            ws.getMotDeroga().setMdeDsTsIniCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-INI-CPTZ(IX-TAB-MDE)
            //           TO MDE-DS-TS-INI-CPTZ
            ws.getMotDeroga().setMdeDsTsIniCptz(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsTsIniCptz());
        }
        // COB_CODE: IF (SF)-DS-TS-END-CPTZ(IX-TAB-MDE) NOT NUMERIC
        //              MOVE 0 TO MDE-DS-TS-END-CPTZ
        //           ELSE
        //              TO MDE-DS-TS-END-CPTZ
        //           END-IF
        if (!Functions.isNumber(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsTsEndCptz())) {
            // COB_CODE: MOVE 0 TO MDE-DS-TS-END-CPTZ
            ws.getMotDeroga().setMdeDsTsEndCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-END-CPTZ(IX-TAB-MDE)
            //           TO MDE-DS-TS-END-CPTZ
            ws.getMotDeroga().setMdeDsTsEndCptz(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsTsEndCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE(IX-TAB-MDE)
        //              TO MDE-DS-UTENTE
        ws.getMotDeroga().setMdeDsUtente(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB(IX-TAB-MDE)
        //              TO MDE-DS-STATO-ELAB.
        ws.getMotDeroga().setMdeDsStatoElab(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeDsStatoElab());
    }

    /**Original name: AGGIORNA-MOT-DEROGA<br>
	 * <pre>----------------------------------------------------------------*
	 *     PROGRAMMA ..... LCCVMDE6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO MOTIVO DEROGA
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN (LCCV*1)
	 *   - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
	 * ----------------------------------------------------------------*
	 * --> INIZIALIZZO LA DCLGEN PER EVITARE DI TROVARE CAMPI SPORCHI</pre>*/
    private void aggiornaMotDeroga() {
        // COB_CODE: INITIALIZE MOT-DEROGA
        initMotDeroga();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'MOT-DEROGA'                   TO WK-TABELLA
        ws.setWkTabella("MOT-DEROGA");
        //--> SE LO STATUS NON E' "INVARIATO"
        // COB_CODE:      IF NOT WMDE-ST-INV(IX-TAB-MDE)
        //                   AND WMDE-ELE-MDE-MAX NOT = 0
        //           *-->    CONTROLLO DELLO STATUS
        //                   END-IF
        //                END-IF.
        if (!ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getStatus().isInv() && ws.getWkVariabili().getWmdeEleMdeMax() != 0) {
            //-->    CONTROLLO DELLO STATUS
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WMDE-ST-ADD(IX-TAB-MDE)
            //                            SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
            //           *-->        MODIFICA
            //                       WHEN WMDE-ST-MOD(IX-TAB-MDE)
            //                            SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
            //           *-->        CANCELLAZIONE
            //                       WHEN WMDE-ST-DEL(IX-TAB-MDE)
            //                            SET  IDSI0011-DELETE-LOGICA    TO TRUE
            //                   END-EVALUATE
            switch (ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getStatus().getStatus()) {

                case WpolStatus.ADD:// COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //                TO MDE-ID-MOVI-CRZ
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA
                        //             TO WMDE-ID-PTF(IX-TAB-MDE)
                        ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
                        // COB_CODE: MOVE WMDE-ID-PTF(IX-TAB-MDE)
                        //             TO MDE-ID-MOT-DEROGA
                        ws.getMotDeroga().setMdeIdMotDeroga(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getIdPtf());
                        // COB_CODE: MOVE WMOV-ID-PTF
                        //             TO MDE-ID-MOVI-CRZ
                        ws.getMotDeroga().setMdeIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
                    }
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    //-->        MODIFICA
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WMDE-ID-PTF(IX-TAB-MDE)
                    //             TO MDE-ID-MOT-DEROGA
                    ws.getMotDeroga().setMdeIdMotDeroga(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getIdPtf());
                    // COB_CODE: MOVE WMDE-ID-OGG-DEROGA(IX-TAB-MDE)
                    //             TO MDE-ID-OGG-DEROGA
                    ws.getMotDeroga().setMdeIdOggDeroga(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeIdOggDeroga());
                    // COB_CODE: MOVE WMOV-ID-PTF
                    //             TO MDE-ID-MOVI-CRZ
                    ws.getMotDeroga().setMdeIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-AGGIORNAMENTO-STORICO     TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011AggiornamentoStorico();
                    //-->        CANCELLAZIONE
                    break;

                case WpolStatus.DEL:// COB_CODE: MOVE WMDE-ID-PTF(IX-TAB-MDE)
                    //             TO MDE-ID-MOT-DEROGA
                    ws.getMotDeroga().setMdeIdMotDeroga(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getIdPtf());
                    // COB_CODE: MOVE WMDE-ID-OGG-DEROGA(IX-TAB-MDE)
                    //             TO MDE-ID-OGG-DEROGA
                    ws.getMotDeroga().setMdeIdOggDeroga(ws.getWkVariabili().getWmdeTabMde(ws.getWsIndici().getMde()).getLccvmde1().getDati().getWmdeIdOggDeroga());
                    // COB_CODE: MOVE WMOV-ID-PTF
                    //             TO MDE-ID-MOVI-CRZ
                    ws.getMotDeroga().setMdeIdMoviCrz(wmovAreaMovimento.getLccvmov1().getIdPtf());
                    //-->             TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-DELETE-LOGICA    TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011DeleteLogica();
                    break;

                default:break;
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZA DCLGEN ADESIONE
            //                         THRU AGGIORNA-TABELLA-EX
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZA DCLGEN ADESIONE
                // COB_CODE: PERFORM VAL-DCLGEN-MDE
                //              THRU VAL-DCLGEN-MDE-EX
                valDclgenMde();
                //-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-MDE
                //              THRU VALORIZZA-AREA-DSH-MDE-EX
                valorizzaAreaDshMde();
                //-->       CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-MDE<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshMde() {
        // COB_CODE: MOVE MOT-DEROGA               TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getMotDeroga().getMotDerogaFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-ID             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011Id();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-DEFAULT           TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(wmovAreaMovimento.getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    /**Original name: VAL-DCLGEN-NOT<br>
	 * <pre>--  NOTE OGGETTO
	 * ----------------------------------------------------------------*
	 *     COPY VALORIZZAZIONE DCLGEN
	 * ----------------------------------------------------------------*
	 *      COPY LCCVMDE5.
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    COPY LCCVNOT5
	 *    ULTIMO AGG. 04 SET 2008
	 * ------------------------------------------------------------</pre>*/
    private void valDclgenNot() {
        // COB_CODE: MOVE (SF)-COD-COMP-ANIA
        //              TO NOT-COD-COMP-ANIA
        ws.getNoteOgg().setCodCompAnia(wnotAreaNoteOgg.getLccvnot1().getDati().getCodCompAnia());
        // COB_CODE: MOVE (SF)-TP-OGG
        //              TO NOT-TP-OGG
        ws.getNoteOgg().setTpOgg(wnotAreaNoteOgg.getLccvnot1().getDati().getTpOgg());
        // COB_CODE: MOVE (SF)-NOTA-OGG
        //              TO NOT-NOTA-OGG
        ws.getNoteOgg().setNotaOgg(wnotAreaNoteOgg.getLccvnot1().getDati().getNotaOgg());
        // COB_CODE: MOVE (SF)-DS-OPER-SQL
        //              TO NOT-DS-OPER-SQL
        ws.getNoteOgg().setDsOperSql(wnotAreaNoteOgg.getLccvnot1().getDati().getDsOperSql());
        // COB_CODE: IF (SF)-DS-VER NOT NUMERIC
        //              MOVE 0 TO NOT-DS-VER
        //           ELSE
        //              TO NOT-DS-VER
        //           END-IF
        if (!Functions.isNumber(wnotAreaNoteOgg.getLccvnot1().getDati().getDsVer())) {
            // COB_CODE: MOVE 0 TO NOT-DS-VER
            ws.getNoteOgg().setDsVer(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-VER
            //           TO NOT-DS-VER
            ws.getNoteOgg().setDsVer(wnotAreaNoteOgg.getLccvnot1().getDati().getDsVer());
        }
        // COB_CODE: IF (SF)-DS-TS-CPTZ NOT NUMERIC
        //              MOVE 0 TO NOT-DS-TS-CPTZ
        //           ELSE
        //              TO NOT-DS-TS-CPTZ
        //           END-IF
        if (!Functions.isNumber(wnotAreaNoteOgg.getLccvnot1().getDati().getDsTsCptz())) {
            // COB_CODE: MOVE 0 TO NOT-DS-TS-CPTZ
            ws.getNoteOgg().setDsTsCptz(0);
        }
        else {
            // COB_CODE: MOVE (SF)-DS-TS-CPTZ
            //           TO NOT-DS-TS-CPTZ
            ws.getNoteOgg().setDsTsCptz(wnotAreaNoteOgg.getLccvnot1().getDati().getDsTsCptz());
        }
        // COB_CODE: MOVE (SF)-DS-UTENTE
        //              TO NOT-DS-UTENTE
        ws.getNoteOgg().setDsUtente(wnotAreaNoteOgg.getLccvnot1().getDati().getDsUtente());
        // COB_CODE: MOVE (SF)-DS-STATO-ELAB
        //              TO NOT-DS-STATO-ELAB.
        ws.getNoteOgg().setDsStatoElab(wnotAreaNoteOgg.getLccvnot1().getDati().getDsStatoElab());
    }

    /**Original name: AGGIORNA-NOTE-OGG<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY      ..... LCCVNOT6
	 *     TIPOLOGIA...... SERVIZIO DI EOC
	 *     DESCRIZIONE.... AGGIORNAMENTO NOTE OGGETTO
	 * ----------------------------------------------------------------*
	 *     N.B. PER IL CORRETTO FUNZIONAMENTO DI QUESTA COPY,
	 *     ALL'INTERNO DEL PROGRAMMA CHIAMANTE BISOGNA INCLUDERE:
	 *   - COPY LCCV*5 (VALORIZZAZIONE DCLGEN)
	 *   - COPY LCCP0001 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - COPY LCCP0002 (COPY PROCEDURE COMPONENTI COMUNI)
	 *   - DICHIARAZIONE DCLGEN (LCCV*1)
	 *   - DICHIARAZIONE AREA COMUNICAZIONE PER CALL AL LCCS0090
	 * ----------------------------------------------------------------*
	 * --> NOME TABELLA FISICA DB</pre>*/
    private void aggiornaNoteOgg() {
        // COB_CODE: INITIALIZE NOTE-OGG.
        initNoteOgg();
        //--> NOME TABELLA FISICA DB
        // COB_CODE: MOVE 'NOTE-OGG'                   TO WK-TABELLA.
        ws.setWkTabella("NOTE-OGG");
        //--> SE LO STATUS NON E' "INVARIATO"
        // COB_CODE: IF NOT WNOT-ST-INV
        //              AND WNOT-ELE-NOTE-OGG-MAX NOT = 0
        //              END-IF
        //           END-IF.
        if (!wnotAreaNoteOgg.getLccvnot1().getStatus().isInv() && wnotAreaNoteOgg.getWnotEleNoteOggMax() != 0) {
            //-->        INSERIMENTO
            // COB_CODE:         EVALUATE TRUE
            //           *-->        INSERIMENTO
            //                       WHEN WNOT-ST-ADD
            //           *--->          ESTRAZIONE E VALORIZZAZIONE SEQUENCE
            //                          SET  IDSI0011-INSERT   TO TRUE
            //           *-->        CANCELLAZIONE
            //                       WHEN WNOT-ST-DEL
            //                          SET  IDSI0011-DELETE   TO TRUE
            //           *-->        MODIFICA
            //                       WHEN WNOT-ST-MOD
            //                          SET  IDSI0011-UPDATE   TO TRUE
            //                   END-EVALUATE
            switch (wnotAreaNoteOgg.getLccvnot1().getStatus().getStatus()) {

                case WpolStatus.ADD://--->          ESTRAZIONE E VALORIZZAZIONE SEQUENCE
                    // COB_CODE: PERFORM ESTR-SEQUENCE
                    //              THRU ESTR-SEQUENCE-EX
                    estrSequence();
                    // COB_CODE: IF IDSV0001-ESITO-OK
                    //              MOVE WMOV-ID-PTF       TO NOT-ID-OGG
                    //           END-IF
                    if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                        // COB_CODE: MOVE S090-SEQ-TABELLA  TO WNOT-ID-PTF
                        //                                     NOT-ID-NOTE-OGG
                        wnotAreaNoteOgg.getLccvnot1().setIdPtf(ws.getAreaIoLccs0090().getSeqTabella());
                        ws.getNoteOgg().setIdNoteOgg(ws.getAreaIoLccs0090().getSeqTabella());
                        // COB_CODE: MOVE WMOV-ID-PTF       TO NOT-ID-OGG
                        ws.getNoteOgg().setIdOgg(wmovAreaMovimento.getLccvmov1().getIdPtf());
                    }
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-INSERT   TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setInsert();
                    //-->        CANCELLAZIONE
                    break;

                case WpolStatus.DEL:// COB_CODE: MOVE WNOT-ID-NOTE-OGG  TO NOT-ID-NOTE-OGG
                    ws.getNoteOgg().setIdNoteOgg(wnotAreaNoteOgg.getLccvnot1().getDati().getIdNoteOgg());
                    // COB_CODE: MOVE WNOT-ID-OGG       TO NOT-ID-OGG
                    ws.getNoteOgg().setIdOgg(wnotAreaNoteOgg.getLccvnot1().getDati().getIdOgg());
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-DELETE   TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Delete();
                    //-->        MODIFICA
                    break;

                case WpolStatus.MOD:// COB_CODE: MOVE WNOT-ID-NOTE-OGG  TO NOT-ID-NOTE-OGG
                    ws.getNoteOgg().setIdNoteOgg(wnotAreaNoteOgg.getLccvnot1().getDati().getIdNoteOgg());
                    // COB_CODE: MOVE WNOT-ID-OGG       TO NOT-ID-OGG
                    ws.getNoteOgg().setIdOgg(wnotAreaNoteOgg.getLccvnot1().getDati().getIdOgg());
                    //-->           TIPO OPERAZIONE DISPATCHER
                    // COB_CODE: SET  IDSI0011-UPDATE   TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setIdsi0011Update();
                    break;

                default:break;
            }
            // COB_CODE:         IF IDSV0001-ESITO-OK
            //           *-->       VALORIZZA DCLGEN NOTE OGGETTO
            //                         THRU AGGIORNA-TABELLA-EX
            //                   END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                //-->       VALORIZZA DCLGEN NOTE OGGETTO
                // COB_CODE: PERFORM VAL-DCLGEN-NOT
                //              THRU VAL-DCLGEN-NOT-EX
                valDclgenNot();
                //-->       VALORIZZAZIONI AREA PER CHIAMATA AL DISPATCHER
                // COB_CODE: PERFORM VALORIZZA-AREA-DSH-NOT
                //              THRU VALORIZZA-AREA-DSH-NOT-EX
                valorizzaAreaDshNot();
                //-->       CALL DISPATCHER PER AGGIORNAMENTO
                // COB_CODE: PERFORM AGGIORNA-TABELLA
                //              THRU AGGIORNA-TABELLA-EX
                aggiornaTabella();
            }
        }
    }

    /**Original name: VALORIZZA-AREA-DSH-NOT<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZA AREA COMUNE DISPATCHER
	 * ----------------------------------------------------------------*
	 * --> DCLGEN TABELLA</pre>*/
    private void valorizzaAreaDshNot() {
        // COB_CODE: MOVE NOTE-OGG                TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getNoteOgg().getNoteOggFormatted());
        //--> MODALITA DI ACCESSO
        // COB_CODE: SET  IDSI0011-PRIMARY-KEY    TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //--> TIPO TABELLA (STORICA/NON STORICA)
        // COB_CODE: SET  IDSI0011-TRATT-SENZA-STOR  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //--> VALORIZZAZIONE DATE EFFETTO
        // COB_CODE: MOVE WMOV-DT-EFF             TO IDSI0011-DATA-INIZIO-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(wmovAreaMovimento.getLccvmov1().getDati().getWmovDtEff());
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO                    TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
    }

    public void initWkVariabili() {
        ws.getWkVariabili().setWsCodCompagnia("");
        ws.getWkVariabili().setWsSequencePtf("");
        ws.getWkVariabili().setWsRichiesta("");
        ws.getWkVariabili().setWsMovimentoAppo("");
        ws.getWkVariabili().setWsIdRichPtfFormatted("000000000");
        ws.getWkVariabili().setWsEleUltProg(((short)0));
        ws.getWkVariabili().setWsIdMoviPtfFormatted("000000000");
        ws.getWkVariabili().setWodeEleOdeMax(((short)0));
        ws.getWkVariabili().getLccvode1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getWkVariabili().getLccvode1().setIdPtf(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeIdOggDeroga(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeIdMoviCrz(0);
        ws.getWkVariabili().getLccvode1().getDati().getWodeIdMoviChiu().setWodeIdMoviChiu(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDtIniEff(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDtEndEff(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeCodCompAnia(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeIdOgg(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeTpOgg("");
        ws.getWkVariabili().getLccvode1().getDati().setWodeIbOgg("");
        ws.getWkVariabili().getLccvode1().getDati().setWodeTpDeroga("");
        ws.getWkVariabili().getLccvode1().getDati().setWodeCodGrAutApprt(0);
        ws.getWkVariabili().getLccvode1().getDati().getWodeCodLivAutApprt().setWodeCodLivAutApprt(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeCodGrAutSup(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeCodLivAutSup(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsRiga(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsOperSql(Types.SPACE_CHAR);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsVer(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsTsIniCptz(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsTsEndCptz(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsUtente("");
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsStatoElab(Types.SPACE_CHAR);
        ws.getWkVariabili().setWmdeEleMdeMax(((short)0));
        for (int idx0 = 1; idx0 <= WkVariabili.WMDE_TAB_MDE_MAXOCCURS; idx0++) {
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().setIdPtf(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeIdMotDeroga(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeIdOggDeroga(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeIdMoviCrz(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().getWmdeIdMoviChiu().setWmdeIdMoviChiu(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDtIniEff(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDtEndEff(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeCodCompAnia(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeTpMotDeroga("");
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeCodLivAut(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeCodErr("");
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeTpErr("");
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDescErrBreve("");
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDescErrEst("");
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDsRiga(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDsOperSql(Types.SPACE_CHAR);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDsVer(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDsTsIniCptz(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDsTsEndCptz(0);
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDsUtente("");
            ws.getWkVariabili().getWmdeTabMde(idx0).getLccvmde1().getDati().setWmdeDsStatoElab(Types.SPACE_CHAR);
        }
        ws.getWkVariabili().setWl27ElePreventivoMax(((short)0));
        ws.getWkVariabili().getLccvl271().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getWkVariabili().getLccvl271().setIdPtf(0);
        ws.getWkVariabili().getLccvl271().getDati().setIdPrev(0);
        ws.getWkVariabili().getLccvl271().getDati().setCodCompAnia(0);
        ws.getWkVariabili().getLccvl271().getDati().setStatPrev("");
        ws.getWkVariabili().getLccvl271().getDati().setIdOgg(0);
        ws.getWkVariabili().getLccvl271().getDati().setTpOgg("");
        ws.getWkVariabili().getLccvl271().getDati().setIbOgg("");
        ws.getWkVariabili().getLccvl271().getDati().setDsOperSql(Types.SPACE_CHAR);
        ws.getWkVariabili().getLccvl271().getDati().setDsVer(0);
        ws.getWkVariabili().getLccvl271().getDati().setDsTsCptz(0);
        ws.getWkVariabili().getLccvl271().getDati().setDsUtente("");
        ws.getWkVariabili().getLccvl271().getDati().setDsStatoElab(Types.SPACE_CHAR);
    }

    public void initWsIndici() {
        ws.getWsIndici().setMde(((short)0));
        ws.getWsIndici().setTga(((short)0));
        ws.getWsIndici().setGrz(((short)0));
    }

    public void initRich() {
        ws.getRich().setRicIdRich(0);
        ws.getRich().setRicCodCompAnia(0);
        ws.getRich().setRicTpRich("");
        ws.getRich().setRicCodMacrofunct("");
        ws.getRich().setRicTpMovi(0);
        ws.getRich().setRicIbRich("");
        ws.getRich().setRicDtEff(0);
        ws.getRich().setRicDtRgstrzRich(0);
        ws.getRich().setRicDtPervRich(0);
        ws.getRich().setRicDtEsecRich(0);
        ws.getRich().getRicTsEffEsecRich().setRicTsEffEsecRich(0);
        ws.getRich().getRicIdOgg().setRicIdOgg(0);
        ws.getRich().setRicTpOgg("");
        ws.getRich().setRicIbPoli("");
        ws.getRich().setRicIbAdes("");
        ws.getRich().setRicIbGar("");
        ws.getRich().setRicIbTrchDiGar("");
        ws.getRich().getRicIdBatch().setRicIdBatch(0);
        ws.getRich().getRicIdJob().setRicIdJob(0);
        ws.getRich().setRicFlSimulazione(Types.SPACE_CHAR);
        ws.getRich().setRicKeyOrdinamentoLen(((short)0));
        ws.getRich().setRicKeyOrdinamento("");
        ws.getRich().getRicIdRichCollg().setRicIdRichCollg(0);
        ws.getRich().setRicTpRamoBila("");
        ws.getRich().setRicTpFrmAssva("");
        ws.getRich().setRicTpCalcRis("");
        ws.getRich().setRicDsOperSql(Types.SPACE_CHAR);
        ws.getRich().setRicDsVer(0);
        ws.getRich().setRicDsTsCptz(0);
        ws.getRich().setRicDsUtente("");
        ws.getRich().setRicDsStatoElab(Types.SPACE_CHAR);
        ws.getRich().setRicRamoBila("");
    }

    public void initMovi() {
        ws.getMovi().setMovIdMovi(0);
        ws.getMovi().setMovCodCompAnia(0);
        ws.getMovi().getMovIdOgg().setMovIdOgg(0);
        ws.getMovi().setMovIbOgg("");
        ws.getMovi().setMovIbMovi("");
        ws.getMovi().setMovTpOgg("");
        ws.getMovi().getMovIdRich().setMovIdRich(0);
        ws.getMovi().getMovTpMovi().setMovTpMovi(0);
        ws.getMovi().setMovDtEff(0);
        ws.getMovi().getMovIdMoviAnn().setMovIdMoviAnn(0);
        ws.getMovi().getMovIdMoviCollg().setMovIdMoviCollg(0);
        ws.getMovi().setMovDsOperSql(Types.SPACE_CHAR);
        ws.getMovi().setMovDsVer(0);
        ws.getMovi().setMovDsTsCptz(0);
        ws.getMovi().setMovDsUtente("");
        ws.getMovi().setMovDsStatoElab(Types.SPACE_CHAR);
    }

    public void initStatOggWf() {
        ws.getStatOggWf().setStwIdStatOggWf(0);
        ws.getStatOggWf().setStwIdOgg(0);
        ws.getStatOggWf().setStwTpOgg("");
        ws.getStatOggWf().setStwIdMoviCrz(0);
        ws.getStatOggWf().getStwIdMoviChiu().setStwIdMoviChiu(0);
        ws.getStatOggWf().setStwDtIniEff(0);
        ws.getStatOggWf().setStwDtEndEff(0);
        ws.getStatOggWf().setStwCodCompAnia(0);
        ws.getStatOggWf().setStwCodPrcs("");
        ws.getStatOggWf().setStwCodAttvt("");
        ws.getStatOggWf().setStwStatOggWf("");
        ws.getStatOggWf().setStwFlStatEnd(Types.SPACE_CHAR);
        ws.getStatOggWf().setStwDsRiga(0);
        ws.getStatOggWf().setStwDsOperSql(Types.SPACE_CHAR);
        ws.getStatOggWf().setStwDsVer(0);
        ws.getStatOggWf().setStwDsTsIniCptz(0);
        ws.getStatOggWf().setStwDsTsEndCptz(0);
        ws.getStatOggWf().setStwDsUtente("");
        ws.getStatOggWf().setStwDsStatoElab(Types.SPACE_CHAR);
    }

    public void initOggDeroga() {
        ws.getOggDeroga().setOdeIdOggDeroga(0);
        ws.getOggDeroga().setOdeIdMoviCrz(0);
        ws.getOggDeroga().getOdeIdMoviChiu().setOdeIdMoviChiu(0);
        ws.getOggDeroga().setOdeDtIniEff(0);
        ws.getOggDeroga().setOdeDtEndEff(0);
        ws.getOggDeroga().setOdeCodCompAnia(0);
        ws.getOggDeroga().setOdeIdOgg(0);
        ws.getOggDeroga().setOdeTpOgg("");
        ws.getOggDeroga().setOdeIbOgg("");
        ws.getOggDeroga().setOdeTpDeroga("");
        ws.getOggDeroga().setOdeCodGrAutApprt(0);
        ws.getOggDeroga().getOdeCodLivAutApprt().setOdeCodLivAutApprt(0);
        ws.getOggDeroga().setOdeCodGrAutSup(0);
        ws.getOggDeroga().setOdeCodLivAutSup(0);
        ws.getOggDeroga().setOdeDsRiga(0);
        ws.getOggDeroga().setOdeDsOperSql(Types.SPACE_CHAR);
        ws.getOggDeroga().setOdeDsVer(0);
        ws.getOggDeroga().setOdeDsTsIniCptz(0);
        ws.getOggDeroga().setOdeDsTsEndCptz(0);
        ws.getOggDeroga().setOdeDsUtente("");
        ws.getOggDeroga().setOdeDsStatoElab(Types.SPACE_CHAR);
    }

    public void initMotDeroga() {
        ws.getMotDeroga().setMdeIdMotDeroga(0);
        ws.getMotDeroga().setMdeIdOggDeroga(0);
        ws.getMotDeroga().setMdeIdMoviCrz(0);
        ws.getMotDeroga().getMdeIdMoviChiu().setMdeIdMoviChiu(0);
        ws.getMotDeroga().setMdeDtIniEff(0);
        ws.getMotDeroga().setMdeDtEndEff(0);
        ws.getMotDeroga().setMdeCodCompAnia(0);
        ws.getMotDeroga().setMdeTpMotDeroga("");
        ws.getMotDeroga().setMdeCodLivAut(0);
        ws.getMotDeroga().setMdeCodErr("");
        ws.getMotDeroga().setMdeTpErr("");
        ws.getMotDeroga().setMdeDescErrBreveLen(((short)0));
        ws.getMotDeroga().setMdeDescErrBreve("");
        ws.getMotDeroga().setMdeDescErrEstLen(((short)0));
        ws.getMotDeroga().setMdeDescErrEst("");
        ws.getMotDeroga().setMdeDsRiga(0);
        ws.getMotDeroga().setMdeDsOperSql(Types.SPACE_CHAR);
        ws.getMotDeroga().setMdeDsVer(0);
        ws.getMotDeroga().setMdeDsTsIniCptz(0);
        ws.getMotDeroga().setMdeDsTsEndCptz(0);
        ws.getMotDeroga().setMdeDsUtente("");
        ws.getMotDeroga().setMdeDsStatoElab(Types.SPACE_CHAR);
    }

    public void initWodeAreaOggDeroga() {
        ws.getWkVariabili().setWodeEleOdeMax(((short)0));
        ws.getWkVariabili().getLccvode1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getWkVariabili().getLccvode1().setIdPtf(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeIdOggDeroga(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeIdMoviCrz(0);
        ws.getWkVariabili().getLccvode1().getDati().getWodeIdMoviChiu().setWodeIdMoviChiu(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDtIniEff(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDtEndEff(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeCodCompAnia(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeIdOgg(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeTpOgg("");
        ws.getWkVariabili().getLccvode1().getDati().setWodeIbOgg("");
        ws.getWkVariabili().getLccvode1().getDati().setWodeTpDeroga("");
        ws.getWkVariabili().getLccvode1().getDati().setWodeCodGrAutApprt(0);
        ws.getWkVariabili().getLccvode1().getDati().getWodeCodLivAutApprt().setWodeCodLivAutApprt(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeCodGrAutSup(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeCodLivAutSup(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsRiga(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsOperSql(Types.SPACE_CHAR);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsVer(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsTsIniCptz(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsTsEndCptz(0);
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsUtente("");
        ws.getWkVariabili().getLccvode1().getDati().setWodeDsStatoElab(Types.SPACE_CHAR);
    }

    public void initAreaEstrazioneIb() {
        ws.getAreaEstrazioneIb().getDatiInput().getCodOggetto().setCodOggetto("");
        ws.getAreaEstrazioneIb().getDatiInput().getTipoNumerazione().setTipoNumerazione(Types.SPACE_CHAR);
        ws.getAreaEstrazioneIb().getDatiInput().setIbOggettoI("");
        ws.getAreaEstrazioneIb().getDatiInput().setIxConta(0);
        ws.getAreaEstrazioneIb().getDatiInput().setAppoLunghezza(0);
        ws.getAreaEstrazioneIb().getDatiInput().setEleInfoMax(((short)0));
        for (int idx0 = 1; idx0 <= WpagDatiInput.TAB_INFO_MAXOCCURS; idx0++) {
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(idx0).setTabAlias("");
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(idx0).setNumOccorrenze(0);
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(idx0).setPosizIni(0);
            ws.getAreaEstrazioneIb().getDatiInput().getTabInfo(idx0).setLunghezza(0);
        }
        ws.getAreaEstrazioneIb().getDatiInput().setBufferDati("");
        ws.getAreaEstrazioneIb().setIbOggetto("");
    }

    public void initAreaIoLccs0070() {
        ws.getAreaIoLccs0070().getFunzione().setFunzione("");
        ws.getAreaIoLccs0070().setIdOggetto(0);
        ws.getAreaIoLccs0070().setTpOggetto("");
        ws.getAreaIoLccs0070().setIbOggetto("");
    }

    public void initNoteOgg() {
        ws.getNoteOgg().setIdNoteOgg(0);
        ws.getNoteOgg().setCodCompAnia(0);
        ws.getNoteOgg().setIdOgg(0);
        ws.getNoteOgg().setTpOgg("");
        ws.getNoteOgg().setNotaOggLen(((short)0));
        ws.getNoteOgg().setNotaOgg("");
        ws.getNoteOgg().setDsOperSql(Types.SPACE_CHAR);
        ws.getNoteOgg().setDsVer(0);
        ws.getNoteOgg().setDsTsCptz(0);
        ws.getNoteOgg().setDsUtente("");
        ws.getNoteOgg().setDsStatoElab(Types.SPACE_CHAR);
    }
}
