package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.RisDiTrchDao;
import it.accenture.jnais.commons.data.to.IRisDiTrch;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsrst0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.RstDtCalc;
import it.accenture.jnais.ws.redefines.RstDtElab;
import it.accenture.jnais.ws.redefines.RstFrazPrePp;
import it.accenture.jnais.ws.redefines.RstIdMoviChiu;
import it.accenture.jnais.ws.redefines.RstIncrXRival;
import it.accenture.jnais.ws.redefines.RstRisAbb;
import it.accenture.jnais.ws.redefines.RstRisAcq;
import it.accenture.jnais.ws.redefines.RstRisBila;
import it.accenture.jnais.ws.redefines.RstRisBnsfdt;
import it.accenture.jnais.ws.redefines.RstRisBnsric;
import it.accenture.jnais.ws.redefines.RstRisComponAssva;
import it.accenture.jnais.ws.redefines.RstRisCosAmmtz;
import it.accenture.jnais.ws.redefines.RstRisFaivl;
import it.accenture.jnais.ws.redefines.RstRisGarCasoMor;
import it.accenture.jnais.ws.redefines.RstRisIntegBasTec;
import it.accenture.jnais.ws.redefines.RstRisIntegDecrTs;
import it.accenture.jnais.ws.redefines.RstRisMat;
import it.accenture.jnais.ws.redefines.RstRisMatEff;
import it.accenture.jnais.ws.redefines.RstRisMinGarto;
import it.accenture.jnais.ws.redefines.RstRisMoviNonInves;
import it.accenture.jnais.ws.redefines.RstRisPrestFaivl;
import it.accenture.jnais.ws.redefines.RstRisRistorniCap;
import it.accenture.jnais.ws.redefines.RstRisRshDflt;
import it.accenture.jnais.ws.redefines.RstRisSopr;
import it.accenture.jnais.ws.redefines.RstRisSpe;
import it.accenture.jnais.ws.redefines.RstRisSpeFaivl;
import it.accenture.jnais.ws.redefines.RstRisTot;
import it.accenture.jnais.ws.redefines.RstRisTrmBns;
import it.accenture.jnais.ws.redefines.RstRisUti;
import it.accenture.jnais.ws.redefines.RstRisZil;
import it.accenture.jnais.ws.redefines.RstRptoPre;
import it.accenture.jnais.ws.redefines.RstUltCoeffAggRis;
import it.accenture.jnais.ws.redefines.RstUltCoeffRis;
import it.accenture.jnais.ws.redefines.RstUltRm;
import it.accenture.jnais.ws.RisDiTrchIdbsrst0;

/**Original name: IDBSRST0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  09 LUG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsrst0 extends Program implements IRisDiTrch {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private RisDiTrchDao risDiTrchDao = new RisDiTrchDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsrst0Data ws = new Idbsrst0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: RIS-DI-TRCH
    private RisDiTrchIdbsrst0 risDiTrch;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSRST0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, RisDiTrchIdbsrst0 risDiTrch) {
        this.idsv0003 = idsv0003;
        this.risDiTrch = risDiTrch;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsrst0 getInstance() {
        return ((Idbsrst0)Programs.getInstance(Idbsrst0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSRST0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSRST0");
        // COB_CODE: MOVE 'RIS_DI_TRCH' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("RIS_DI_TRCH");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RIS_DI_TRCH
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_CALC_RIS
        //                ,ULT_RM
        //                ,DT_CALC
        //                ,DT_ELAB
        //                ,RIS_BILA
        //                ,RIS_MAT
        //                ,INCR_X_RIVAL
        //                ,RPTO_PRE
        //                ,FRAZ_PRE_PP
        //                ,RIS_TOT
        //                ,RIS_SPE
        //                ,RIS_ABB
        //                ,RIS_BNSFDT
        //                ,RIS_SOPR
        //                ,RIS_INTEG_BAS_TEC
        //                ,RIS_INTEG_DECR_TS
        //                ,RIS_GAR_CASO_MOR
        //                ,RIS_ZIL
        //                ,RIS_FAIVL
        //                ,RIS_COS_AMMTZ
        //                ,RIS_SPE_FAIVL
        //                ,RIS_PREST_FAIVL
        //                ,RIS_COMPON_ASSVA
        //                ,ULT_COEFF_RIS
        //                ,ULT_COEFF_AGG_RIS
        //                ,RIS_ACQ
        //                ,RIS_UTI
        //                ,RIS_MAT_EFF
        //                ,RIS_RISTORNI_CAP
        //                ,RIS_TRM_BNS
        //                ,RIS_BNSRIC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_TRCH_DI_GAR
        //                ,COD_FND
        //                ,ID_POLI
        //                ,ID_ADES
        //                ,ID_GAR
        //                ,RIS_MIN_GARTO
        //                ,RIS_RSH_DFLT
        //                ,RIS_MOVI_NON_INVES
        //             INTO
        //                :RST-ID-RIS-DI-TRCH
        //               ,:RST-ID-MOVI-CRZ
        //               ,:RST-ID-MOVI-CHIU
        //                :IND-RST-ID-MOVI-CHIU
        //               ,:RST-DT-INI-EFF-DB
        //               ,:RST-DT-END-EFF-DB
        //               ,:RST-COD-COMP-ANIA
        //               ,:RST-TP-CALC-RIS
        //                :IND-RST-TP-CALC-RIS
        //               ,:RST-ULT-RM
        //                :IND-RST-ULT-RM
        //               ,:RST-DT-CALC-DB
        //                :IND-RST-DT-CALC
        //               ,:RST-DT-ELAB-DB
        //                :IND-RST-DT-ELAB
        //               ,:RST-RIS-BILA
        //                :IND-RST-RIS-BILA
        //               ,:RST-RIS-MAT
        //                :IND-RST-RIS-MAT
        //               ,:RST-INCR-X-RIVAL
        //                :IND-RST-INCR-X-RIVAL
        //               ,:RST-RPTO-PRE
        //                :IND-RST-RPTO-PRE
        //               ,:RST-FRAZ-PRE-PP
        //                :IND-RST-FRAZ-PRE-PP
        //               ,:RST-RIS-TOT
        //                :IND-RST-RIS-TOT
        //               ,:RST-RIS-SPE
        //                :IND-RST-RIS-SPE
        //               ,:RST-RIS-ABB
        //                :IND-RST-RIS-ABB
        //               ,:RST-RIS-BNSFDT
        //                :IND-RST-RIS-BNSFDT
        //               ,:RST-RIS-SOPR
        //                :IND-RST-RIS-SOPR
        //               ,:RST-RIS-INTEG-BAS-TEC
        //                :IND-RST-RIS-INTEG-BAS-TEC
        //               ,:RST-RIS-INTEG-DECR-TS
        //                :IND-RST-RIS-INTEG-DECR-TS
        //               ,:RST-RIS-GAR-CASO-MOR
        //                :IND-RST-RIS-GAR-CASO-MOR
        //               ,:RST-RIS-ZIL
        //                :IND-RST-RIS-ZIL
        //               ,:RST-RIS-FAIVL
        //                :IND-RST-RIS-FAIVL
        //               ,:RST-RIS-COS-AMMTZ
        //                :IND-RST-RIS-COS-AMMTZ
        //               ,:RST-RIS-SPE-FAIVL
        //                :IND-RST-RIS-SPE-FAIVL
        //               ,:RST-RIS-PREST-FAIVL
        //                :IND-RST-RIS-PREST-FAIVL
        //               ,:RST-RIS-COMPON-ASSVA
        //                :IND-RST-RIS-COMPON-ASSVA
        //               ,:RST-ULT-COEFF-RIS
        //                :IND-RST-ULT-COEFF-RIS
        //               ,:RST-ULT-COEFF-AGG-RIS
        //                :IND-RST-ULT-COEFF-AGG-RIS
        //               ,:RST-RIS-ACQ
        //                :IND-RST-RIS-ACQ
        //               ,:RST-RIS-UTI
        //                :IND-RST-RIS-UTI
        //               ,:RST-RIS-MAT-EFF
        //                :IND-RST-RIS-MAT-EFF
        //               ,:RST-RIS-RISTORNI-CAP
        //                :IND-RST-RIS-RISTORNI-CAP
        //               ,:RST-RIS-TRM-BNS
        //                :IND-RST-RIS-TRM-BNS
        //               ,:RST-RIS-BNSRIC
        //                :IND-RST-RIS-BNSRIC
        //               ,:RST-DS-RIGA
        //               ,:RST-DS-OPER-SQL
        //               ,:RST-DS-VER
        //               ,:RST-DS-TS-INI-CPTZ
        //               ,:RST-DS-TS-END-CPTZ
        //               ,:RST-DS-UTENTE
        //               ,:RST-DS-STATO-ELAB
        //               ,:RST-ID-TRCH-DI-GAR
        //               ,:RST-COD-FND
        //                :IND-RST-COD-FND
        //               ,:RST-ID-POLI
        //               ,:RST-ID-ADES
        //               ,:RST-ID-GAR
        //               ,:RST-RIS-MIN-GARTO
        //                :IND-RST-RIS-MIN-GARTO
        //               ,:RST-RIS-RSH-DFLT
        //                :IND-RST-RIS-RSH-DFLT
        //               ,:RST-RIS-MOVI-NON-INVES
        //                :IND-RST-RIS-MOVI-NON-INVES
        //             FROM RIS_DI_TRCH
        //             WHERE     DS_RIGA = :RST-DS-RIGA
        //           END-EXEC.
        risDiTrchDao.selectByRstDsRiga(risDiTrch.getRstDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO RIS_DI_TRCH
            //                  (
            //                     ID_RIS_DI_TRCH
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,TP_CALC_RIS
            //                    ,ULT_RM
            //                    ,DT_CALC
            //                    ,DT_ELAB
            //                    ,RIS_BILA
            //                    ,RIS_MAT
            //                    ,INCR_X_RIVAL
            //                    ,RPTO_PRE
            //                    ,FRAZ_PRE_PP
            //                    ,RIS_TOT
            //                    ,RIS_SPE
            //                    ,RIS_ABB
            //                    ,RIS_BNSFDT
            //                    ,RIS_SOPR
            //                    ,RIS_INTEG_BAS_TEC
            //                    ,RIS_INTEG_DECR_TS
            //                    ,RIS_GAR_CASO_MOR
            //                    ,RIS_ZIL
            //                    ,RIS_FAIVL
            //                    ,RIS_COS_AMMTZ
            //                    ,RIS_SPE_FAIVL
            //                    ,RIS_PREST_FAIVL
            //                    ,RIS_COMPON_ASSVA
            //                    ,ULT_COEFF_RIS
            //                    ,ULT_COEFF_AGG_RIS
            //                    ,RIS_ACQ
            //                    ,RIS_UTI
            //                    ,RIS_MAT_EFF
            //                    ,RIS_RISTORNI_CAP
            //                    ,RIS_TRM_BNS
            //                    ,RIS_BNSRIC
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,ID_TRCH_DI_GAR
            //                    ,COD_FND
            //                    ,ID_POLI
            //                    ,ID_ADES
            //                    ,ID_GAR
            //                    ,RIS_MIN_GARTO
            //                    ,RIS_RSH_DFLT
            //                    ,RIS_MOVI_NON_INVES
            //                  )
            //              VALUES
            //                  (
            //                    :RST-ID-RIS-DI-TRCH
            //                    ,:RST-ID-MOVI-CRZ
            //                    ,:RST-ID-MOVI-CHIU
            //                     :IND-RST-ID-MOVI-CHIU
            //                    ,:RST-DT-INI-EFF-DB
            //                    ,:RST-DT-END-EFF-DB
            //                    ,:RST-COD-COMP-ANIA
            //                    ,:RST-TP-CALC-RIS
            //                     :IND-RST-TP-CALC-RIS
            //                    ,:RST-ULT-RM
            //                     :IND-RST-ULT-RM
            //                    ,:RST-DT-CALC-DB
            //                     :IND-RST-DT-CALC
            //                    ,:RST-DT-ELAB-DB
            //                     :IND-RST-DT-ELAB
            //                    ,:RST-RIS-BILA
            //                     :IND-RST-RIS-BILA
            //                    ,:RST-RIS-MAT
            //                     :IND-RST-RIS-MAT
            //                    ,:RST-INCR-X-RIVAL
            //                     :IND-RST-INCR-X-RIVAL
            //                    ,:RST-RPTO-PRE
            //                     :IND-RST-RPTO-PRE
            //                    ,:RST-FRAZ-PRE-PP
            //                     :IND-RST-FRAZ-PRE-PP
            //                    ,:RST-RIS-TOT
            //                     :IND-RST-RIS-TOT
            //                    ,:RST-RIS-SPE
            //                     :IND-RST-RIS-SPE
            //                    ,:RST-RIS-ABB
            //                     :IND-RST-RIS-ABB
            //                    ,:RST-RIS-BNSFDT
            //                     :IND-RST-RIS-BNSFDT
            //                    ,:RST-RIS-SOPR
            //                     :IND-RST-RIS-SOPR
            //                    ,:RST-RIS-INTEG-BAS-TEC
            //                     :IND-RST-RIS-INTEG-BAS-TEC
            //                    ,:RST-RIS-INTEG-DECR-TS
            //                     :IND-RST-RIS-INTEG-DECR-TS
            //                    ,:RST-RIS-GAR-CASO-MOR
            //                     :IND-RST-RIS-GAR-CASO-MOR
            //                    ,:RST-RIS-ZIL
            //                     :IND-RST-RIS-ZIL
            //                    ,:RST-RIS-FAIVL
            //                     :IND-RST-RIS-FAIVL
            //                    ,:RST-RIS-COS-AMMTZ
            //                     :IND-RST-RIS-COS-AMMTZ
            //                    ,:RST-RIS-SPE-FAIVL
            //                     :IND-RST-RIS-SPE-FAIVL
            //                    ,:RST-RIS-PREST-FAIVL
            //                     :IND-RST-RIS-PREST-FAIVL
            //                    ,:RST-RIS-COMPON-ASSVA
            //                     :IND-RST-RIS-COMPON-ASSVA
            //                    ,:RST-ULT-COEFF-RIS
            //                     :IND-RST-ULT-COEFF-RIS
            //                    ,:RST-ULT-COEFF-AGG-RIS
            //                     :IND-RST-ULT-COEFF-AGG-RIS
            //                    ,:RST-RIS-ACQ
            //                     :IND-RST-RIS-ACQ
            //                    ,:RST-RIS-UTI
            //                     :IND-RST-RIS-UTI
            //                    ,:RST-RIS-MAT-EFF
            //                     :IND-RST-RIS-MAT-EFF
            //                    ,:RST-RIS-RISTORNI-CAP
            //                     :IND-RST-RIS-RISTORNI-CAP
            //                    ,:RST-RIS-TRM-BNS
            //                     :IND-RST-RIS-TRM-BNS
            //                    ,:RST-RIS-BNSRIC
            //                     :IND-RST-RIS-BNSRIC
            //                    ,:RST-DS-RIGA
            //                    ,:RST-DS-OPER-SQL
            //                    ,:RST-DS-VER
            //                    ,:RST-DS-TS-INI-CPTZ
            //                    ,:RST-DS-TS-END-CPTZ
            //                    ,:RST-DS-UTENTE
            //                    ,:RST-DS-STATO-ELAB
            //                    ,:RST-ID-TRCH-DI-GAR
            //                    ,:RST-COD-FND
            //                     :IND-RST-COD-FND
            //                    ,:RST-ID-POLI
            //                    ,:RST-ID-ADES
            //                    ,:RST-ID-GAR
            //                    ,:RST-RIS-MIN-GARTO
            //                     :IND-RST-RIS-MIN-GARTO
            //                    ,:RST-RIS-RSH-DFLT
            //                     :IND-RST-RIS-RSH-DFLT
            //                    ,:RST-RIS-MOVI-NON-INVES
            //                     :IND-RST-RIS-MOVI-NON-INVES
            //                  )
            //           END-EXEC
            risDiTrchDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE RIS_DI_TRCH SET
        //                   ID_RIS_DI_TRCH         =
        //                :RST-ID-RIS-DI-TRCH
        //                  ,ID_MOVI_CRZ            =
        //                :RST-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :RST-ID-MOVI-CHIU
        //                                       :IND-RST-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :RST-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :RST-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :RST-COD-COMP-ANIA
        //                  ,TP_CALC_RIS            =
        //                :RST-TP-CALC-RIS
        //                                       :IND-RST-TP-CALC-RIS
        //                  ,ULT_RM                 =
        //                :RST-ULT-RM
        //                                       :IND-RST-ULT-RM
        //                  ,DT_CALC                =
        //           :RST-DT-CALC-DB
        //                                       :IND-RST-DT-CALC
        //                  ,DT_ELAB                =
        //           :RST-DT-ELAB-DB
        //                                       :IND-RST-DT-ELAB
        //                  ,RIS_BILA               =
        //                :RST-RIS-BILA
        //                                       :IND-RST-RIS-BILA
        //                  ,RIS_MAT                =
        //                :RST-RIS-MAT
        //                                       :IND-RST-RIS-MAT
        //                  ,INCR_X_RIVAL           =
        //                :RST-INCR-X-RIVAL
        //                                       :IND-RST-INCR-X-RIVAL
        //                  ,RPTO_PRE               =
        //                :RST-RPTO-PRE
        //                                       :IND-RST-RPTO-PRE
        //                  ,FRAZ_PRE_PP            =
        //                :RST-FRAZ-PRE-PP
        //                                       :IND-RST-FRAZ-PRE-PP
        //                  ,RIS_TOT                =
        //                :RST-RIS-TOT
        //                                       :IND-RST-RIS-TOT
        //                  ,RIS_SPE                =
        //                :RST-RIS-SPE
        //                                       :IND-RST-RIS-SPE
        //                  ,RIS_ABB                =
        //                :RST-RIS-ABB
        //                                       :IND-RST-RIS-ABB
        //                  ,RIS_BNSFDT             =
        //                :RST-RIS-BNSFDT
        //                                       :IND-RST-RIS-BNSFDT
        //                  ,RIS_SOPR               =
        //                :RST-RIS-SOPR
        //                                       :IND-RST-RIS-SOPR
        //                  ,RIS_INTEG_BAS_TEC      =
        //                :RST-RIS-INTEG-BAS-TEC
        //                                       :IND-RST-RIS-INTEG-BAS-TEC
        //                  ,RIS_INTEG_DECR_TS      =
        //                :RST-RIS-INTEG-DECR-TS
        //                                       :IND-RST-RIS-INTEG-DECR-TS
        //                  ,RIS_GAR_CASO_MOR       =
        //                :RST-RIS-GAR-CASO-MOR
        //                                       :IND-RST-RIS-GAR-CASO-MOR
        //                  ,RIS_ZIL                =
        //                :RST-RIS-ZIL
        //                                       :IND-RST-RIS-ZIL
        //                  ,RIS_FAIVL              =
        //                :RST-RIS-FAIVL
        //                                       :IND-RST-RIS-FAIVL
        //                  ,RIS_COS_AMMTZ          =
        //                :RST-RIS-COS-AMMTZ
        //                                       :IND-RST-RIS-COS-AMMTZ
        //                  ,RIS_SPE_FAIVL          =
        //                :RST-RIS-SPE-FAIVL
        //                                       :IND-RST-RIS-SPE-FAIVL
        //                  ,RIS_PREST_FAIVL        =
        //                :RST-RIS-PREST-FAIVL
        //                                       :IND-RST-RIS-PREST-FAIVL
        //                  ,RIS_COMPON_ASSVA       =
        //                :RST-RIS-COMPON-ASSVA
        //                                       :IND-RST-RIS-COMPON-ASSVA
        //                  ,ULT_COEFF_RIS          =
        //                :RST-ULT-COEFF-RIS
        //                                       :IND-RST-ULT-COEFF-RIS
        //                  ,ULT_COEFF_AGG_RIS      =
        //                :RST-ULT-COEFF-AGG-RIS
        //                                       :IND-RST-ULT-COEFF-AGG-RIS
        //                  ,RIS_ACQ                =
        //                :RST-RIS-ACQ
        //                                       :IND-RST-RIS-ACQ
        //                  ,RIS_UTI                =
        //                :RST-RIS-UTI
        //                                       :IND-RST-RIS-UTI
        //                  ,RIS_MAT_EFF            =
        //                :RST-RIS-MAT-EFF
        //                                       :IND-RST-RIS-MAT-EFF
        //                  ,RIS_RISTORNI_CAP       =
        //                :RST-RIS-RISTORNI-CAP
        //                                       :IND-RST-RIS-RISTORNI-CAP
        //                  ,RIS_TRM_BNS            =
        //                :RST-RIS-TRM-BNS
        //                                       :IND-RST-RIS-TRM-BNS
        //                  ,RIS_BNSRIC             =
        //                :RST-RIS-BNSRIC
        //                                       :IND-RST-RIS-BNSRIC
        //                  ,DS_RIGA                =
        //                :RST-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :RST-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :RST-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :RST-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :RST-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :RST-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :RST-DS-STATO-ELAB
        //                  ,ID_TRCH_DI_GAR         =
        //                :RST-ID-TRCH-DI-GAR
        //                  ,COD_FND                =
        //                :RST-COD-FND
        //                                       :IND-RST-COD-FND
        //                  ,ID_POLI                =
        //                :RST-ID-POLI
        //                  ,ID_ADES                =
        //                :RST-ID-ADES
        //                  ,ID_GAR                 =
        //                :RST-ID-GAR
        //                  ,RIS_MIN_GARTO          =
        //                :RST-RIS-MIN-GARTO
        //                                       :IND-RST-RIS-MIN-GARTO
        //                  ,RIS_RSH_DFLT           =
        //                :RST-RIS-RSH-DFLT
        //                                       :IND-RST-RIS-RSH-DFLT
        //                  ,RIS_MOVI_NON_INVES     =
        //                :RST-RIS-MOVI-NON-INVES
        //                                       :IND-RST-RIS-MOVI-NON-INVES
        //                WHERE     DS_RIGA = :RST-DS-RIGA
        //           END-EXEC.
        risDiTrchDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM RIS_DI_TRCH
        //                WHERE     DS_RIGA = :RST-DS-RIGA
        //           END-EXEC.
        risDiTrchDao.deleteByRstDsRiga(risDiTrch.getRstDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-RST CURSOR FOR
        //              SELECT
        //                     ID_RIS_DI_TRCH
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_CALC_RIS
        //                    ,ULT_RM
        //                    ,DT_CALC
        //                    ,DT_ELAB
        //                    ,RIS_BILA
        //                    ,RIS_MAT
        //                    ,INCR_X_RIVAL
        //                    ,RPTO_PRE
        //                    ,FRAZ_PRE_PP
        //                    ,RIS_TOT
        //                    ,RIS_SPE
        //                    ,RIS_ABB
        //                    ,RIS_BNSFDT
        //                    ,RIS_SOPR
        //                    ,RIS_INTEG_BAS_TEC
        //                    ,RIS_INTEG_DECR_TS
        //                    ,RIS_GAR_CASO_MOR
        //                    ,RIS_ZIL
        //                    ,RIS_FAIVL
        //                    ,RIS_COS_AMMTZ
        //                    ,RIS_SPE_FAIVL
        //                    ,RIS_PREST_FAIVL
        //                    ,RIS_COMPON_ASSVA
        //                    ,ULT_COEFF_RIS
        //                    ,ULT_COEFF_AGG_RIS
        //                    ,RIS_ACQ
        //                    ,RIS_UTI
        //                    ,RIS_MAT_EFF
        //                    ,RIS_RISTORNI_CAP
        //                    ,RIS_TRM_BNS
        //                    ,RIS_BNSRIC
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,ID_TRCH_DI_GAR
        //                    ,COD_FND
        //                    ,ID_POLI
        //                    ,ID_ADES
        //                    ,ID_GAR
        //                    ,RIS_MIN_GARTO
        //                    ,RIS_RSH_DFLT
        //                    ,RIS_MOVI_NON_INVES
        //              FROM RIS_DI_TRCH
        //              WHERE     ID_RIS_DI_TRCH = :RST-ID-RIS-DI-TRCH
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RIS_DI_TRCH
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_CALC_RIS
        //                ,ULT_RM
        //                ,DT_CALC
        //                ,DT_ELAB
        //                ,RIS_BILA
        //                ,RIS_MAT
        //                ,INCR_X_RIVAL
        //                ,RPTO_PRE
        //                ,FRAZ_PRE_PP
        //                ,RIS_TOT
        //                ,RIS_SPE
        //                ,RIS_ABB
        //                ,RIS_BNSFDT
        //                ,RIS_SOPR
        //                ,RIS_INTEG_BAS_TEC
        //                ,RIS_INTEG_DECR_TS
        //                ,RIS_GAR_CASO_MOR
        //                ,RIS_ZIL
        //                ,RIS_FAIVL
        //                ,RIS_COS_AMMTZ
        //                ,RIS_SPE_FAIVL
        //                ,RIS_PREST_FAIVL
        //                ,RIS_COMPON_ASSVA
        //                ,ULT_COEFF_RIS
        //                ,ULT_COEFF_AGG_RIS
        //                ,RIS_ACQ
        //                ,RIS_UTI
        //                ,RIS_MAT_EFF
        //                ,RIS_RISTORNI_CAP
        //                ,RIS_TRM_BNS
        //                ,RIS_BNSRIC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_TRCH_DI_GAR
        //                ,COD_FND
        //                ,ID_POLI
        //                ,ID_ADES
        //                ,ID_GAR
        //                ,RIS_MIN_GARTO
        //                ,RIS_RSH_DFLT
        //                ,RIS_MOVI_NON_INVES
        //             INTO
        //                :RST-ID-RIS-DI-TRCH
        //               ,:RST-ID-MOVI-CRZ
        //               ,:RST-ID-MOVI-CHIU
        //                :IND-RST-ID-MOVI-CHIU
        //               ,:RST-DT-INI-EFF-DB
        //               ,:RST-DT-END-EFF-DB
        //               ,:RST-COD-COMP-ANIA
        //               ,:RST-TP-CALC-RIS
        //                :IND-RST-TP-CALC-RIS
        //               ,:RST-ULT-RM
        //                :IND-RST-ULT-RM
        //               ,:RST-DT-CALC-DB
        //                :IND-RST-DT-CALC
        //               ,:RST-DT-ELAB-DB
        //                :IND-RST-DT-ELAB
        //               ,:RST-RIS-BILA
        //                :IND-RST-RIS-BILA
        //               ,:RST-RIS-MAT
        //                :IND-RST-RIS-MAT
        //               ,:RST-INCR-X-RIVAL
        //                :IND-RST-INCR-X-RIVAL
        //               ,:RST-RPTO-PRE
        //                :IND-RST-RPTO-PRE
        //               ,:RST-FRAZ-PRE-PP
        //                :IND-RST-FRAZ-PRE-PP
        //               ,:RST-RIS-TOT
        //                :IND-RST-RIS-TOT
        //               ,:RST-RIS-SPE
        //                :IND-RST-RIS-SPE
        //               ,:RST-RIS-ABB
        //                :IND-RST-RIS-ABB
        //               ,:RST-RIS-BNSFDT
        //                :IND-RST-RIS-BNSFDT
        //               ,:RST-RIS-SOPR
        //                :IND-RST-RIS-SOPR
        //               ,:RST-RIS-INTEG-BAS-TEC
        //                :IND-RST-RIS-INTEG-BAS-TEC
        //               ,:RST-RIS-INTEG-DECR-TS
        //                :IND-RST-RIS-INTEG-DECR-TS
        //               ,:RST-RIS-GAR-CASO-MOR
        //                :IND-RST-RIS-GAR-CASO-MOR
        //               ,:RST-RIS-ZIL
        //                :IND-RST-RIS-ZIL
        //               ,:RST-RIS-FAIVL
        //                :IND-RST-RIS-FAIVL
        //               ,:RST-RIS-COS-AMMTZ
        //                :IND-RST-RIS-COS-AMMTZ
        //               ,:RST-RIS-SPE-FAIVL
        //                :IND-RST-RIS-SPE-FAIVL
        //               ,:RST-RIS-PREST-FAIVL
        //                :IND-RST-RIS-PREST-FAIVL
        //               ,:RST-RIS-COMPON-ASSVA
        //                :IND-RST-RIS-COMPON-ASSVA
        //               ,:RST-ULT-COEFF-RIS
        //                :IND-RST-ULT-COEFF-RIS
        //               ,:RST-ULT-COEFF-AGG-RIS
        //                :IND-RST-ULT-COEFF-AGG-RIS
        //               ,:RST-RIS-ACQ
        //                :IND-RST-RIS-ACQ
        //               ,:RST-RIS-UTI
        //                :IND-RST-RIS-UTI
        //               ,:RST-RIS-MAT-EFF
        //                :IND-RST-RIS-MAT-EFF
        //               ,:RST-RIS-RISTORNI-CAP
        //                :IND-RST-RIS-RISTORNI-CAP
        //               ,:RST-RIS-TRM-BNS
        //                :IND-RST-RIS-TRM-BNS
        //               ,:RST-RIS-BNSRIC
        //                :IND-RST-RIS-BNSRIC
        //               ,:RST-DS-RIGA
        //               ,:RST-DS-OPER-SQL
        //               ,:RST-DS-VER
        //               ,:RST-DS-TS-INI-CPTZ
        //               ,:RST-DS-TS-END-CPTZ
        //               ,:RST-DS-UTENTE
        //               ,:RST-DS-STATO-ELAB
        //               ,:RST-ID-TRCH-DI-GAR
        //               ,:RST-COD-FND
        //                :IND-RST-COD-FND
        //               ,:RST-ID-POLI
        //               ,:RST-ID-ADES
        //               ,:RST-ID-GAR
        //               ,:RST-RIS-MIN-GARTO
        //                :IND-RST-RIS-MIN-GARTO
        //               ,:RST-RIS-RSH-DFLT
        //                :IND-RST-RIS-RSH-DFLT
        //               ,:RST-RIS-MOVI-NON-INVES
        //                :IND-RST-RIS-MOVI-NON-INVES
        //             FROM RIS_DI_TRCH
        //             WHERE     ID_RIS_DI_TRCH = :RST-ID-RIS-DI-TRCH
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        risDiTrchDao.selectRec(risDiTrch.getRstIdRisDiTrch(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE RIS_DI_TRCH SET
        //                   ID_RIS_DI_TRCH         =
        //                :RST-ID-RIS-DI-TRCH
        //                  ,ID_MOVI_CRZ            =
        //                :RST-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :RST-ID-MOVI-CHIU
        //                                       :IND-RST-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :RST-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :RST-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :RST-COD-COMP-ANIA
        //                  ,TP_CALC_RIS            =
        //                :RST-TP-CALC-RIS
        //                                       :IND-RST-TP-CALC-RIS
        //                  ,ULT_RM                 =
        //                :RST-ULT-RM
        //                                       :IND-RST-ULT-RM
        //                  ,DT_CALC                =
        //           :RST-DT-CALC-DB
        //                                       :IND-RST-DT-CALC
        //                  ,DT_ELAB                =
        //           :RST-DT-ELAB-DB
        //                                       :IND-RST-DT-ELAB
        //                  ,RIS_BILA               =
        //                :RST-RIS-BILA
        //                                       :IND-RST-RIS-BILA
        //                  ,RIS_MAT                =
        //                :RST-RIS-MAT
        //                                       :IND-RST-RIS-MAT
        //                  ,INCR_X_RIVAL           =
        //                :RST-INCR-X-RIVAL
        //                                       :IND-RST-INCR-X-RIVAL
        //                  ,RPTO_PRE               =
        //                :RST-RPTO-PRE
        //                                       :IND-RST-RPTO-PRE
        //                  ,FRAZ_PRE_PP            =
        //                :RST-FRAZ-PRE-PP
        //                                       :IND-RST-FRAZ-PRE-PP
        //                  ,RIS_TOT                =
        //                :RST-RIS-TOT
        //                                       :IND-RST-RIS-TOT
        //                  ,RIS_SPE                =
        //                :RST-RIS-SPE
        //                                       :IND-RST-RIS-SPE
        //                  ,RIS_ABB                =
        //                :RST-RIS-ABB
        //                                       :IND-RST-RIS-ABB
        //                  ,RIS_BNSFDT             =
        //                :RST-RIS-BNSFDT
        //                                       :IND-RST-RIS-BNSFDT
        //                  ,RIS_SOPR               =
        //                :RST-RIS-SOPR
        //                                       :IND-RST-RIS-SOPR
        //                  ,RIS_INTEG_BAS_TEC      =
        //                :RST-RIS-INTEG-BAS-TEC
        //                                       :IND-RST-RIS-INTEG-BAS-TEC
        //                  ,RIS_INTEG_DECR_TS      =
        //                :RST-RIS-INTEG-DECR-TS
        //                                       :IND-RST-RIS-INTEG-DECR-TS
        //                  ,RIS_GAR_CASO_MOR       =
        //                :RST-RIS-GAR-CASO-MOR
        //                                       :IND-RST-RIS-GAR-CASO-MOR
        //                  ,RIS_ZIL                =
        //                :RST-RIS-ZIL
        //                                       :IND-RST-RIS-ZIL
        //                  ,RIS_FAIVL              =
        //                :RST-RIS-FAIVL
        //                                       :IND-RST-RIS-FAIVL
        //                  ,RIS_COS_AMMTZ          =
        //                :RST-RIS-COS-AMMTZ
        //                                       :IND-RST-RIS-COS-AMMTZ
        //                  ,RIS_SPE_FAIVL          =
        //                :RST-RIS-SPE-FAIVL
        //                                       :IND-RST-RIS-SPE-FAIVL
        //                  ,RIS_PREST_FAIVL        =
        //                :RST-RIS-PREST-FAIVL
        //                                       :IND-RST-RIS-PREST-FAIVL
        //                  ,RIS_COMPON_ASSVA       =
        //                :RST-RIS-COMPON-ASSVA
        //                                       :IND-RST-RIS-COMPON-ASSVA
        //                  ,ULT_COEFF_RIS          =
        //                :RST-ULT-COEFF-RIS
        //                                       :IND-RST-ULT-COEFF-RIS
        //                  ,ULT_COEFF_AGG_RIS      =
        //                :RST-ULT-COEFF-AGG-RIS
        //                                       :IND-RST-ULT-COEFF-AGG-RIS
        //                  ,RIS_ACQ                =
        //                :RST-RIS-ACQ
        //                                       :IND-RST-RIS-ACQ
        //                  ,RIS_UTI                =
        //                :RST-RIS-UTI
        //                                       :IND-RST-RIS-UTI
        //                  ,RIS_MAT_EFF            =
        //                :RST-RIS-MAT-EFF
        //                                       :IND-RST-RIS-MAT-EFF
        //                  ,RIS_RISTORNI_CAP       =
        //                :RST-RIS-RISTORNI-CAP
        //                                       :IND-RST-RIS-RISTORNI-CAP
        //                  ,RIS_TRM_BNS            =
        //                :RST-RIS-TRM-BNS
        //                                       :IND-RST-RIS-TRM-BNS
        //                  ,RIS_BNSRIC             =
        //                :RST-RIS-BNSRIC
        //                                       :IND-RST-RIS-BNSRIC
        //                  ,DS_RIGA                =
        //                :RST-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :RST-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :RST-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :RST-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :RST-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :RST-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :RST-DS-STATO-ELAB
        //                  ,ID_TRCH_DI_GAR         =
        //                :RST-ID-TRCH-DI-GAR
        //                  ,COD_FND                =
        //                :RST-COD-FND
        //                                       :IND-RST-COD-FND
        //                  ,ID_POLI                =
        //                :RST-ID-POLI
        //                  ,ID_ADES                =
        //                :RST-ID-ADES
        //                  ,ID_GAR                 =
        //                :RST-ID-GAR
        //                  ,RIS_MIN_GARTO          =
        //                :RST-RIS-MIN-GARTO
        //                                       :IND-RST-RIS-MIN-GARTO
        //                  ,RIS_RSH_DFLT           =
        //                :RST-RIS-RSH-DFLT
        //                                       :IND-RST-RIS-RSH-DFLT
        //                  ,RIS_MOVI_NON_INVES     =
        //                :RST-RIS-MOVI-NON-INVES
        //                                       :IND-RST-RIS-MOVI-NON-INVES
        //                WHERE     DS_RIGA = :RST-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        risDiTrchDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-RST
        //           END-EXEC.
        risDiTrchDao.openCIdUpdEffRst(risDiTrch.getRstIdRisDiTrch(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-RST
        //           END-EXEC.
        risDiTrchDao.closeCIdUpdEffRst();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-RST
        //           INTO
        //                :RST-ID-RIS-DI-TRCH
        //               ,:RST-ID-MOVI-CRZ
        //               ,:RST-ID-MOVI-CHIU
        //                :IND-RST-ID-MOVI-CHIU
        //               ,:RST-DT-INI-EFF-DB
        //               ,:RST-DT-END-EFF-DB
        //               ,:RST-COD-COMP-ANIA
        //               ,:RST-TP-CALC-RIS
        //                :IND-RST-TP-CALC-RIS
        //               ,:RST-ULT-RM
        //                :IND-RST-ULT-RM
        //               ,:RST-DT-CALC-DB
        //                :IND-RST-DT-CALC
        //               ,:RST-DT-ELAB-DB
        //                :IND-RST-DT-ELAB
        //               ,:RST-RIS-BILA
        //                :IND-RST-RIS-BILA
        //               ,:RST-RIS-MAT
        //                :IND-RST-RIS-MAT
        //               ,:RST-INCR-X-RIVAL
        //                :IND-RST-INCR-X-RIVAL
        //               ,:RST-RPTO-PRE
        //                :IND-RST-RPTO-PRE
        //               ,:RST-FRAZ-PRE-PP
        //                :IND-RST-FRAZ-PRE-PP
        //               ,:RST-RIS-TOT
        //                :IND-RST-RIS-TOT
        //               ,:RST-RIS-SPE
        //                :IND-RST-RIS-SPE
        //               ,:RST-RIS-ABB
        //                :IND-RST-RIS-ABB
        //               ,:RST-RIS-BNSFDT
        //                :IND-RST-RIS-BNSFDT
        //               ,:RST-RIS-SOPR
        //                :IND-RST-RIS-SOPR
        //               ,:RST-RIS-INTEG-BAS-TEC
        //                :IND-RST-RIS-INTEG-BAS-TEC
        //               ,:RST-RIS-INTEG-DECR-TS
        //                :IND-RST-RIS-INTEG-DECR-TS
        //               ,:RST-RIS-GAR-CASO-MOR
        //                :IND-RST-RIS-GAR-CASO-MOR
        //               ,:RST-RIS-ZIL
        //                :IND-RST-RIS-ZIL
        //               ,:RST-RIS-FAIVL
        //                :IND-RST-RIS-FAIVL
        //               ,:RST-RIS-COS-AMMTZ
        //                :IND-RST-RIS-COS-AMMTZ
        //               ,:RST-RIS-SPE-FAIVL
        //                :IND-RST-RIS-SPE-FAIVL
        //               ,:RST-RIS-PREST-FAIVL
        //                :IND-RST-RIS-PREST-FAIVL
        //               ,:RST-RIS-COMPON-ASSVA
        //                :IND-RST-RIS-COMPON-ASSVA
        //               ,:RST-ULT-COEFF-RIS
        //                :IND-RST-ULT-COEFF-RIS
        //               ,:RST-ULT-COEFF-AGG-RIS
        //                :IND-RST-ULT-COEFF-AGG-RIS
        //               ,:RST-RIS-ACQ
        //                :IND-RST-RIS-ACQ
        //               ,:RST-RIS-UTI
        //                :IND-RST-RIS-UTI
        //               ,:RST-RIS-MAT-EFF
        //                :IND-RST-RIS-MAT-EFF
        //               ,:RST-RIS-RISTORNI-CAP
        //                :IND-RST-RIS-RISTORNI-CAP
        //               ,:RST-RIS-TRM-BNS
        //                :IND-RST-RIS-TRM-BNS
        //               ,:RST-RIS-BNSRIC
        //                :IND-RST-RIS-BNSRIC
        //               ,:RST-DS-RIGA
        //               ,:RST-DS-OPER-SQL
        //               ,:RST-DS-VER
        //               ,:RST-DS-TS-INI-CPTZ
        //               ,:RST-DS-TS-END-CPTZ
        //               ,:RST-DS-UTENTE
        //               ,:RST-DS-STATO-ELAB
        //               ,:RST-ID-TRCH-DI-GAR
        //               ,:RST-COD-FND
        //                :IND-RST-COD-FND
        //               ,:RST-ID-POLI
        //               ,:RST-ID-ADES
        //               ,:RST-ID-GAR
        //               ,:RST-RIS-MIN-GARTO
        //                :IND-RST-RIS-MIN-GARTO
        //               ,:RST-RIS-RSH-DFLT
        //                :IND-RST-RIS-RSH-DFLT
        //               ,:RST-RIS-MOVI-NON-INVES
        //                :IND-RST-RIS-MOVI-NON-INVES
        //           END-EXEC.
        risDiTrchDao.fetchCIdUpdEffRst(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-RST CURSOR FOR
        //              SELECT
        //                     ID_RIS_DI_TRCH
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_CALC_RIS
        //                    ,ULT_RM
        //                    ,DT_CALC
        //                    ,DT_ELAB
        //                    ,RIS_BILA
        //                    ,RIS_MAT
        //                    ,INCR_X_RIVAL
        //                    ,RPTO_PRE
        //                    ,FRAZ_PRE_PP
        //                    ,RIS_TOT
        //                    ,RIS_SPE
        //                    ,RIS_ABB
        //                    ,RIS_BNSFDT
        //                    ,RIS_SOPR
        //                    ,RIS_INTEG_BAS_TEC
        //                    ,RIS_INTEG_DECR_TS
        //                    ,RIS_GAR_CASO_MOR
        //                    ,RIS_ZIL
        //                    ,RIS_FAIVL
        //                    ,RIS_COS_AMMTZ
        //                    ,RIS_SPE_FAIVL
        //                    ,RIS_PREST_FAIVL
        //                    ,RIS_COMPON_ASSVA
        //                    ,ULT_COEFF_RIS
        //                    ,ULT_COEFF_AGG_RIS
        //                    ,RIS_ACQ
        //                    ,RIS_UTI
        //                    ,RIS_MAT_EFF
        //                    ,RIS_RISTORNI_CAP
        //                    ,RIS_TRM_BNS
        //                    ,RIS_BNSRIC
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,ID_TRCH_DI_GAR
        //                    ,COD_FND
        //                    ,ID_POLI
        //                    ,ID_ADES
        //                    ,ID_GAR
        //                    ,RIS_MIN_GARTO
        //                    ,RIS_RSH_DFLT
        //                    ,RIS_MOVI_NON_INVES
        //              FROM RIS_DI_TRCH
        //              WHERE     ID_TRCH_DI_GAR = :RST-ID-TRCH-DI-GAR
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_RIS_DI_TRCH ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RIS_DI_TRCH
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_CALC_RIS
        //                ,ULT_RM
        //                ,DT_CALC
        //                ,DT_ELAB
        //                ,RIS_BILA
        //                ,RIS_MAT
        //                ,INCR_X_RIVAL
        //                ,RPTO_PRE
        //                ,FRAZ_PRE_PP
        //                ,RIS_TOT
        //                ,RIS_SPE
        //                ,RIS_ABB
        //                ,RIS_BNSFDT
        //                ,RIS_SOPR
        //                ,RIS_INTEG_BAS_TEC
        //                ,RIS_INTEG_DECR_TS
        //                ,RIS_GAR_CASO_MOR
        //                ,RIS_ZIL
        //                ,RIS_FAIVL
        //                ,RIS_COS_AMMTZ
        //                ,RIS_SPE_FAIVL
        //                ,RIS_PREST_FAIVL
        //                ,RIS_COMPON_ASSVA
        //                ,ULT_COEFF_RIS
        //                ,ULT_COEFF_AGG_RIS
        //                ,RIS_ACQ
        //                ,RIS_UTI
        //                ,RIS_MAT_EFF
        //                ,RIS_RISTORNI_CAP
        //                ,RIS_TRM_BNS
        //                ,RIS_BNSRIC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_TRCH_DI_GAR
        //                ,COD_FND
        //                ,ID_POLI
        //                ,ID_ADES
        //                ,ID_GAR
        //                ,RIS_MIN_GARTO
        //                ,RIS_RSH_DFLT
        //                ,RIS_MOVI_NON_INVES
        //             INTO
        //                :RST-ID-RIS-DI-TRCH
        //               ,:RST-ID-MOVI-CRZ
        //               ,:RST-ID-MOVI-CHIU
        //                :IND-RST-ID-MOVI-CHIU
        //               ,:RST-DT-INI-EFF-DB
        //               ,:RST-DT-END-EFF-DB
        //               ,:RST-COD-COMP-ANIA
        //               ,:RST-TP-CALC-RIS
        //                :IND-RST-TP-CALC-RIS
        //               ,:RST-ULT-RM
        //                :IND-RST-ULT-RM
        //               ,:RST-DT-CALC-DB
        //                :IND-RST-DT-CALC
        //               ,:RST-DT-ELAB-DB
        //                :IND-RST-DT-ELAB
        //               ,:RST-RIS-BILA
        //                :IND-RST-RIS-BILA
        //               ,:RST-RIS-MAT
        //                :IND-RST-RIS-MAT
        //               ,:RST-INCR-X-RIVAL
        //                :IND-RST-INCR-X-RIVAL
        //               ,:RST-RPTO-PRE
        //                :IND-RST-RPTO-PRE
        //               ,:RST-FRAZ-PRE-PP
        //                :IND-RST-FRAZ-PRE-PP
        //               ,:RST-RIS-TOT
        //                :IND-RST-RIS-TOT
        //               ,:RST-RIS-SPE
        //                :IND-RST-RIS-SPE
        //               ,:RST-RIS-ABB
        //                :IND-RST-RIS-ABB
        //               ,:RST-RIS-BNSFDT
        //                :IND-RST-RIS-BNSFDT
        //               ,:RST-RIS-SOPR
        //                :IND-RST-RIS-SOPR
        //               ,:RST-RIS-INTEG-BAS-TEC
        //                :IND-RST-RIS-INTEG-BAS-TEC
        //               ,:RST-RIS-INTEG-DECR-TS
        //                :IND-RST-RIS-INTEG-DECR-TS
        //               ,:RST-RIS-GAR-CASO-MOR
        //                :IND-RST-RIS-GAR-CASO-MOR
        //               ,:RST-RIS-ZIL
        //                :IND-RST-RIS-ZIL
        //               ,:RST-RIS-FAIVL
        //                :IND-RST-RIS-FAIVL
        //               ,:RST-RIS-COS-AMMTZ
        //                :IND-RST-RIS-COS-AMMTZ
        //               ,:RST-RIS-SPE-FAIVL
        //                :IND-RST-RIS-SPE-FAIVL
        //               ,:RST-RIS-PREST-FAIVL
        //                :IND-RST-RIS-PREST-FAIVL
        //               ,:RST-RIS-COMPON-ASSVA
        //                :IND-RST-RIS-COMPON-ASSVA
        //               ,:RST-ULT-COEFF-RIS
        //                :IND-RST-ULT-COEFF-RIS
        //               ,:RST-ULT-COEFF-AGG-RIS
        //                :IND-RST-ULT-COEFF-AGG-RIS
        //               ,:RST-RIS-ACQ
        //                :IND-RST-RIS-ACQ
        //               ,:RST-RIS-UTI
        //                :IND-RST-RIS-UTI
        //               ,:RST-RIS-MAT-EFF
        //                :IND-RST-RIS-MAT-EFF
        //               ,:RST-RIS-RISTORNI-CAP
        //                :IND-RST-RIS-RISTORNI-CAP
        //               ,:RST-RIS-TRM-BNS
        //                :IND-RST-RIS-TRM-BNS
        //               ,:RST-RIS-BNSRIC
        //                :IND-RST-RIS-BNSRIC
        //               ,:RST-DS-RIGA
        //               ,:RST-DS-OPER-SQL
        //               ,:RST-DS-VER
        //               ,:RST-DS-TS-INI-CPTZ
        //               ,:RST-DS-TS-END-CPTZ
        //               ,:RST-DS-UTENTE
        //               ,:RST-DS-STATO-ELAB
        //               ,:RST-ID-TRCH-DI-GAR
        //               ,:RST-COD-FND
        //                :IND-RST-COD-FND
        //               ,:RST-ID-POLI
        //               ,:RST-ID-ADES
        //               ,:RST-ID-GAR
        //               ,:RST-RIS-MIN-GARTO
        //                :IND-RST-RIS-MIN-GARTO
        //               ,:RST-RIS-RSH-DFLT
        //                :IND-RST-RIS-RSH-DFLT
        //               ,:RST-RIS-MOVI-NON-INVES
        //                :IND-RST-RIS-MOVI-NON-INVES
        //             FROM RIS_DI_TRCH
        //             WHERE     ID_TRCH_DI_GAR = :RST-ID-TRCH-DI-GAR
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        risDiTrchDao.selectRec1(risDiTrch.getRstIdTrchDiGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-RST
        //           END-EXEC.
        risDiTrchDao.openCIdpEffRst(risDiTrch.getRstIdTrchDiGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-RST
        //           END-EXEC.
        risDiTrchDao.closeCIdpEffRst();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-RST
        //           INTO
        //                :RST-ID-RIS-DI-TRCH
        //               ,:RST-ID-MOVI-CRZ
        //               ,:RST-ID-MOVI-CHIU
        //                :IND-RST-ID-MOVI-CHIU
        //               ,:RST-DT-INI-EFF-DB
        //               ,:RST-DT-END-EFF-DB
        //               ,:RST-COD-COMP-ANIA
        //               ,:RST-TP-CALC-RIS
        //                :IND-RST-TP-CALC-RIS
        //               ,:RST-ULT-RM
        //                :IND-RST-ULT-RM
        //               ,:RST-DT-CALC-DB
        //                :IND-RST-DT-CALC
        //               ,:RST-DT-ELAB-DB
        //                :IND-RST-DT-ELAB
        //               ,:RST-RIS-BILA
        //                :IND-RST-RIS-BILA
        //               ,:RST-RIS-MAT
        //                :IND-RST-RIS-MAT
        //               ,:RST-INCR-X-RIVAL
        //                :IND-RST-INCR-X-RIVAL
        //               ,:RST-RPTO-PRE
        //                :IND-RST-RPTO-PRE
        //               ,:RST-FRAZ-PRE-PP
        //                :IND-RST-FRAZ-PRE-PP
        //               ,:RST-RIS-TOT
        //                :IND-RST-RIS-TOT
        //               ,:RST-RIS-SPE
        //                :IND-RST-RIS-SPE
        //               ,:RST-RIS-ABB
        //                :IND-RST-RIS-ABB
        //               ,:RST-RIS-BNSFDT
        //                :IND-RST-RIS-BNSFDT
        //               ,:RST-RIS-SOPR
        //                :IND-RST-RIS-SOPR
        //               ,:RST-RIS-INTEG-BAS-TEC
        //                :IND-RST-RIS-INTEG-BAS-TEC
        //               ,:RST-RIS-INTEG-DECR-TS
        //                :IND-RST-RIS-INTEG-DECR-TS
        //               ,:RST-RIS-GAR-CASO-MOR
        //                :IND-RST-RIS-GAR-CASO-MOR
        //               ,:RST-RIS-ZIL
        //                :IND-RST-RIS-ZIL
        //               ,:RST-RIS-FAIVL
        //                :IND-RST-RIS-FAIVL
        //               ,:RST-RIS-COS-AMMTZ
        //                :IND-RST-RIS-COS-AMMTZ
        //               ,:RST-RIS-SPE-FAIVL
        //                :IND-RST-RIS-SPE-FAIVL
        //               ,:RST-RIS-PREST-FAIVL
        //                :IND-RST-RIS-PREST-FAIVL
        //               ,:RST-RIS-COMPON-ASSVA
        //                :IND-RST-RIS-COMPON-ASSVA
        //               ,:RST-ULT-COEFF-RIS
        //                :IND-RST-ULT-COEFF-RIS
        //               ,:RST-ULT-COEFF-AGG-RIS
        //                :IND-RST-ULT-COEFF-AGG-RIS
        //               ,:RST-RIS-ACQ
        //                :IND-RST-RIS-ACQ
        //               ,:RST-RIS-UTI
        //                :IND-RST-RIS-UTI
        //               ,:RST-RIS-MAT-EFF
        //                :IND-RST-RIS-MAT-EFF
        //               ,:RST-RIS-RISTORNI-CAP
        //                :IND-RST-RIS-RISTORNI-CAP
        //               ,:RST-RIS-TRM-BNS
        //                :IND-RST-RIS-TRM-BNS
        //               ,:RST-RIS-BNSRIC
        //                :IND-RST-RIS-BNSRIC
        //               ,:RST-DS-RIGA
        //               ,:RST-DS-OPER-SQL
        //               ,:RST-DS-VER
        //               ,:RST-DS-TS-INI-CPTZ
        //               ,:RST-DS-TS-END-CPTZ
        //               ,:RST-DS-UTENTE
        //               ,:RST-DS-STATO-ELAB
        //               ,:RST-ID-TRCH-DI-GAR
        //               ,:RST-COD-FND
        //                :IND-RST-COD-FND
        //               ,:RST-ID-POLI
        //               ,:RST-ID-ADES
        //               ,:RST-ID-GAR
        //               ,:RST-RIS-MIN-GARTO
        //                :IND-RST-RIS-MIN-GARTO
        //               ,:RST-RIS-RSH-DFLT
        //                :IND-RST-RIS-RSH-DFLT
        //               ,:RST-RIS-MOVI-NON-INVES
        //                :IND-RST-RIS-MOVI-NON-INVES
        //           END-EXEC.
        risDiTrchDao.fetchCIdpEffRst(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RIS_DI_TRCH
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_CALC_RIS
        //                ,ULT_RM
        //                ,DT_CALC
        //                ,DT_ELAB
        //                ,RIS_BILA
        //                ,RIS_MAT
        //                ,INCR_X_RIVAL
        //                ,RPTO_PRE
        //                ,FRAZ_PRE_PP
        //                ,RIS_TOT
        //                ,RIS_SPE
        //                ,RIS_ABB
        //                ,RIS_BNSFDT
        //                ,RIS_SOPR
        //                ,RIS_INTEG_BAS_TEC
        //                ,RIS_INTEG_DECR_TS
        //                ,RIS_GAR_CASO_MOR
        //                ,RIS_ZIL
        //                ,RIS_FAIVL
        //                ,RIS_COS_AMMTZ
        //                ,RIS_SPE_FAIVL
        //                ,RIS_PREST_FAIVL
        //                ,RIS_COMPON_ASSVA
        //                ,ULT_COEFF_RIS
        //                ,ULT_COEFF_AGG_RIS
        //                ,RIS_ACQ
        //                ,RIS_UTI
        //                ,RIS_MAT_EFF
        //                ,RIS_RISTORNI_CAP
        //                ,RIS_TRM_BNS
        //                ,RIS_BNSRIC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_TRCH_DI_GAR
        //                ,COD_FND
        //                ,ID_POLI
        //                ,ID_ADES
        //                ,ID_GAR
        //                ,RIS_MIN_GARTO
        //                ,RIS_RSH_DFLT
        //                ,RIS_MOVI_NON_INVES
        //             INTO
        //                :RST-ID-RIS-DI-TRCH
        //               ,:RST-ID-MOVI-CRZ
        //               ,:RST-ID-MOVI-CHIU
        //                :IND-RST-ID-MOVI-CHIU
        //               ,:RST-DT-INI-EFF-DB
        //               ,:RST-DT-END-EFF-DB
        //               ,:RST-COD-COMP-ANIA
        //               ,:RST-TP-CALC-RIS
        //                :IND-RST-TP-CALC-RIS
        //               ,:RST-ULT-RM
        //                :IND-RST-ULT-RM
        //               ,:RST-DT-CALC-DB
        //                :IND-RST-DT-CALC
        //               ,:RST-DT-ELAB-DB
        //                :IND-RST-DT-ELAB
        //               ,:RST-RIS-BILA
        //                :IND-RST-RIS-BILA
        //               ,:RST-RIS-MAT
        //                :IND-RST-RIS-MAT
        //               ,:RST-INCR-X-RIVAL
        //                :IND-RST-INCR-X-RIVAL
        //               ,:RST-RPTO-PRE
        //                :IND-RST-RPTO-PRE
        //               ,:RST-FRAZ-PRE-PP
        //                :IND-RST-FRAZ-PRE-PP
        //               ,:RST-RIS-TOT
        //                :IND-RST-RIS-TOT
        //               ,:RST-RIS-SPE
        //                :IND-RST-RIS-SPE
        //               ,:RST-RIS-ABB
        //                :IND-RST-RIS-ABB
        //               ,:RST-RIS-BNSFDT
        //                :IND-RST-RIS-BNSFDT
        //               ,:RST-RIS-SOPR
        //                :IND-RST-RIS-SOPR
        //               ,:RST-RIS-INTEG-BAS-TEC
        //                :IND-RST-RIS-INTEG-BAS-TEC
        //               ,:RST-RIS-INTEG-DECR-TS
        //                :IND-RST-RIS-INTEG-DECR-TS
        //               ,:RST-RIS-GAR-CASO-MOR
        //                :IND-RST-RIS-GAR-CASO-MOR
        //               ,:RST-RIS-ZIL
        //                :IND-RST-RIS-ZIL
        //               ,:RST-RIS-FAIVL
        //                :IND-RST-RIS-FAIVL
        //               ,:RST-RIS-COS-AMMTZ
        //                :IND-RST-RIS-COS-AMMTZ
        //               ,:RST-RIS-SPE-FAIVL
        //                :IND-RST-RIS-SPE-FAIVL
        //               ,:RST-RIS-PREST-FAIVL
        //                :IND-RST-RIS-PREST-FAIVL
        //               ,:RST-RIS-COMPON-ASSVA
        //                :IND-RST-RIS-COMPON-ASSVA
        //               ,:RST-ULT-COEFF-RIS
        //                :IND-RST-ULT-COEFF-RIS
        //               ,:RST-ULT-COEFF-AGG-RIS
        //                :IND-RST-ULT-COEFF-AGG-RIS
        //               ,:RST-RIS-ACQ
        //                :IND-RST-RIS-ACQ
        //               ,:RST-RIS-UTI
        //                :IND-RST-RIS-UTI
        //               ,:RST-RIS-MAT-EFF
        //                :IND-RST-RIS-MAT-EFF
        //               ,:RST-RIS-RISTORNI-CAP
        //                :IND-RST-RIS-RISTORNI-CAP
        //               ,:RST-RIS-TRM-BNS
        //                :IND-RST-RIS-TRM-BNS
        //               ,:RST-RIS-BNSRIC
        //                :IND-RST-RIS-BNSRIC
        //               ,:RST-DS-RIGA
        //               ,:RST-DS-OPER-SQL
        //               ,:RST-DS-VER
        //               ,:RST-DS-TS-INI-CPTZ
        //               ,:RST-DS-TS-END-CPTZ
        //               ,:RST-DS-UTENTE
        //               ,:RST-DS-STATO-ELAB
        //               ,:RST-ID-TRCH-DI-GAR
        //               ,:RST-COD-FND
        //                :IND-RST-COD-FND
        //               ,:RST-ID-POLI
        //               ,:RST-ID-ADES
        //               ,:RST-ID-GAR
        //               ,:RST-RIS-MIN-GARTO
        //                :IND-RST-RIS-MIN-GARTO
        //               ,:RST-RIS-RSH-DFLT
        //                :IND-RST-RIS-RSH-DFLT
        //               ,:RST-RIS-MOVI-NON-INVES
        //                :IND-RST-RIS-MOVI-NON-INVES
        //             FROM RIS_DI_TRCH
        //             WHERE     ID_RIS_DI_TRCH = :RST-ID-RIS-DI-TRCH
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        risDiTrchDao.selectRec2(risDiTrch.getRstIdRisDiTrch(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-RST CURSOR FOR
        //              SELECT
        //                     ID_RIS_DI_TRCH
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,TP_CALC_RIS
        //                    ,ULT_RM
        //                    ,DT_CALC
        //                    ,DT_ELAB
        //                    ,RIS_BILA
        //                    ,RIS_MAT
        //                    ,INCR_X_RIVAL
        //                    ,RPTO_PRE
        //                    ,FRAZ_PRE_PP
        //                    ,RIS_TOT
        //                    ,RIS_SPE
        //                    ,RIS_ABB
        //                    ,RIS_BNSFDT
        //                    ,RIS_SOPR
        //                    ,RIS_INTEG_BAS_TEC
        //                    ,RIS_INTEG_DECR_TS
        //                    ,RIS_GAR_CASO_MOR
        //                    ,RIS_ZIL
        //                    ,RIS_FAIVL
        //                    ,RIS_COS_AMMTZ
        //                    ,RIS_SPE_FAIVL
        //                    ,RIS_PREST_FAIVL
        //                    ,RIS_COMPON_ASSVA
        //                    ,ULT_COEFF_RIS
        //                    ,ULT_COEFF_AGG_RIS
        //                    ,RIS_ACQ
        //                    ,RIS_UTI
        //                    ,RIS_MAT_EFF
        //                    ,RIS_RISTORNI_CAP
        //                    ,RIS_TRM_BNS
        //                    ,RIS_BNSRIC
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,ID_TRCH_DI_GAR
        //                    ,COD_FND
        //                    ,ID_POLI
        //                    ,ID_ADES
        //                    ,ID_GAR
        //                    ,RIS_MIN_GARTO
        //                    ,RIS_RSH_DFLT
        //                    ,RIS_MOVI_NON_INVES
        //              FROM RIS_DI_TRCH
        //              WHERE     ID_TRCH_DI_GAR = :RST-ID-TRCH-DI-GAR
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_RIS_DI_TRCH ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RIS_DI_TRCH
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,TP_CALC_RIS
        //                ,ULT_RM
        //                ,DT_CALC
        //                ,DT_ELAB
        //                ,RIS_BILA
        //                ,RIS_MAT
        //                ,INCR_X_RIVAL
        //                ,RPTO_PRE
        //                ,FRAZ_PRE_PP
        //                ,RIS_TOT
        //                ,RIS_SPE
        //                ,RIS_ABB
        //                ,RIS_BNSFDT
        //                ,RIS_SOPR
        //                ,RIS_INTEG_BAS_TEC
        //                ,RIS_INTEG_DECR_TS
        //                ,RIS_GAR_CASO_MOR
        //                ,RIS_ZIL
        //                ,RIS_FAIVL
        //                ,RIS_COS_AMMTZ
        //                ,RIS_SPE_FAIVL
        //                ,RIS_PREST_FAIVL
        //                ,RIS_COMPON_ASSVA
        //                ,ULT_COEFF_RIS
        //                ,ULT_COEFF_AGG_RIS
        //                ,RIS_ACQ
        //                ,RIS_UTI
        //                ,RIS_MAT_EFF
        //                ,RIS_RISTORNI_CAP
        //                ,RIS_TRM_BNS
        //                ,RIS_BNSRIC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,ID_TRCH_DI_GAR
        //                ,COD_FND
        //                ,ID_POLI
        //                ,ID_ADES
        //                ,ID_GAR
        //                ,RIS_MIN_GARTO
        //                ,RIS_RSH_DFLT
        //                ,RIS_MOVI_NON_INVES
        //             INTO
        //                :RST-ID-RIS-DI-TRCH
        //               ,:RST-ID-MOVI-CRZ
        //               ,:RST-ID-MOVI-CHIU
        //                :IND-RST-ID-MOVI-CHIU
        //               ,:RST-DT-INI-EFF-DB
        //               ,:RST-DT-END-EFF-DB
        //               ,:RST-COD-COMP-ANIA
        //               ,:RST-TP-CALC-RIS
        //                :IND-RST-TP-CALC-RIS
        //               ,:RST-ULT-RM
        //                :IND-RST-ULT-RM
        //               ,:RST-DT-CALC-DB
        //                :IND-RST-DT-CALC
        //               ,:RST-DT-ELAB-DB
        //                :IND-RST-DT-ELAB
        //               ,:RST-RIS-BILA
        //                :IND-RST-RIS-BILA
        //               ,:RST-RIS-MAT
        //                :IND-RST-RIS-MAT
        //               ,:RST-INCR-X-RIVAL
        //                :IND-RST-INCR-X-RIVAL
        //               ,:RST-RPTO-PRE
        //                :IND-RST-RPTO-PRE
        //               ,:RST-FRAZ-PRE-PP
        //                :IND-RST-FRAZ-PRE-PP
        //               ,:RST-RIS-TOT
        //                :IND-RST-RIS-TOT
        //               ,:RST-RIS-SPE
        //                :IND-RST-RIS-SPE
        //               ,:RST-RIS-ABB
        //                :IND-RST-RIS-ABB
        //               ,:RST-RIS-BNSFDT
        //                :IND-RST-RIS-BNSFDT
        //               ,:RST-RIS-SOPR
        //                :IND-RST-RIS-SOPR
        //               ,:RST-RIS-INTEG-BAS-TEC
        //                :IND-RST-RIS-INTEG-BAS-TEC
        //               ,:RST-RIS-INTEG-DECR-TS
        //                :IND-RST-RIS-INTEG-DECR-TS
        //               ,:RST-RIS-GAR-CASO-MOR
        //                :IND-RST-RIS-GAR-CASO-MOR
        //               ,:RST-RIS-ZIL
        //                :IND-RST-RIS-ZIL
        //               ,:RST-RIS-FAIVL
        //                :IND-RST-RIS-FAIVL
        //               ,:RST-RIS-COS-AMMTZ
        //                :IND-RST-RIS-COS-AMMTZ
        //               ,:RST-RIS-SPE-FAIVL
        //                :IND-RST-RIS-SPE-FAIVL
        //               ,:RST-RIS-PREST-FAIVL
        //                :IND-RST-RIS-PREST-FAIVL
        //               ,:RST-RIS-COMPON-ASSVA
        //                :IND-RST-RIS-COMPON-ASSVA
        //               ,:RST-ULT-COEFF-RIS
        //                :IND-RST-ULT-COEFF-RIS
        //               ,:RST-ULT-COEFF-AGG-RIS
        //                :IND-RST-ULT-COEFF-AGG-RIS
        //               ,:RST-RIS-ACQ
        //                :IND-RST-RIS-ACQ
        //               ,:RST-RIS-UTI
        //                :IND-RST-RIS-UTI
        //               ,:RST-RIS-MAT-EFF
        //                :IND-RST-RIS-MAT-EFF
        //               ,:RST-RIS-RISTORNI-CAP
        //                :IND-RST-RIS-RISTORNI-CAP
        //               ,:RST-RIS-TRM-BNS
        //                :IND-RST-RIS-TRM-BNS
        //               ,:RST-RIS-BNSRIC
        //                :IND-RST-RIS-BNSRIC
        //               ,:RST-DS-RIGA
        //               ,:RST-DS-OPER-SQL
        //               ,:RST-DS-VER
        //               ,:RST-DS-TS-INI-CPTZ
        //               ,:RST-DS-TS-END-CPTZ
        //               ,:RST-DS-UTENTE
        //               ,:RST-DS-STATO-ELAB
        //               ,:RST-ID-TRCH-DI-GAR
        //               ,:RST-COD-FND
        //                :IND-RST-COD-FND
        //               ,:RST-ID-POLI
        //               ,:RST-ID-ADES
        //               ,:RST-ID-GAR
        //               ,:RST-RIS-MIN-GARTO
        //                :IND-RST-RIS-MIN-GARTO
        //               ,:RST-RIS-RSH-DFLT
        //                :IND-RST-RIS-RSH-DFLT
        //               ,:RST-RIS-MOVI-NON-INVES
        //                :IND-RST-RIS-MOVI-NON-INVES
        //             FROM RIS_DI_TRCH
        //             WHERE     ID_TRCH_DI_GAR = :RST-ID-TRCH-DI-GAR
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        risDiTrchDao.selectRec3(risDiTrch.getRstIdTrchDiGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-RST
        //           END-EXEC.
        risDiTrchDao.openCIdpCpzRst(risDiTrch.getRstIdTrchDiGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-RST
        //           END-EXEC.
        risDiTrchDao.closeCIdpCpzRst();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-RST
        //           INTO
        //                :RST-ID-RIS-DI-TRCH
        //               ,:RST-ID-MOVI-CRZ
        //               ,:RST-ID-MOVI-CHIU
        //                :IND-RST-ID-MOVI-CHIU
        //               ,:RST-DT-INI-EFF-DB
        //               ,:RST-DT-END-EFF-DB
        //               ,:RST-COD-COMP-ANIA
        //               ,:RST-TP-CALC-RIS
        //                :IND-RST-TP-CALC-RIS
        //               ,:RST-ULT-RM
        //                :IND-RST-ULT-RM
        //               ,:RST-DT-CALC-DB
        //                :IND-RST-DT-CALC
        //               ,:RST-DT-ELAB-DB
        //                :IND-RST-DT-ELAB
        //               ,:RST-RIS-BILA
        //                :IND-RST-RIS-BILA
        //               ,:RST-RIS-MAT
        //                :IND-RST-RIS-MAT
        //               ,:RST-INCR-X-RIVAL
        //                :IND-RST-INCR-X-RIVAL
        //               ,:RST-RPTO-PRE
        //                :IND-RST-RPTO-PRE
        //               ,:RST-FRAZ-PRE-PP
        //                :IND-RST-FRAZ-PRE-PP
        //               ,:RST-RIS-TOT
        //                :IND-RST-RIS-TOT
        //               ,:RST-RIS-SPE
        //                :IND-RST-RIS-SPE
        //               ,:RST-RIS-ABB
        //                :IND-RST-RIS-ABB
        //               ,:RST-RIS-BNSFDT
        //                :IND-RST-RIS-BNSFDT
        //               ,:RST-RIS-SOPR
        //                :IND-RST-RIS-SOPR
        //               ,:RST-RIS-INTEG-BAS-TEC
        //                :IND-RST-RIS-INTEG-BAS-TEC
        //               ,:RST-RIS-INTEG-DECR-TS
        //                :IND-RST-RIS-INTEG-DECR-TS
        //               ,:RST-RIS-GAR-CASO-MOR
        //                :IND-RST-RIS-GAR-CASO-MOR
        //               ,:RST-RIS-ZIL
        //                :IND-RST-RIS-ZIL
        //               ,:RST-RIS-FAIVL
        //                :IND-RST-RIS-FAIVL
        //               ,:RST-RIS-COS-AMMTZ
        //                :IND-RST-RIS-COS-AMMTZ
        //               ,:RST-RIS-SPE-FAIVL
        //                :IND-RST-RIS-SPE-FAIVL
        //               ,:RST-RIS-PREST-FAIVL
        //                :IND-RST-RIS-PREST-FAIVL
        //               ,:RST-RIS-COMPON-ASSVA
        //                :IND-RST-RIS-COMPON-ASSVA
        //               ,:RST-ULT-COEFF-RIS
        //                :IND-RST-ULT-COEFF-RIS
        //               ,:RST-ULT-COEFF-AGG-RIS
        //                :IND-RST-ULT-COEFF-AGG-RIS
        //               ,:RST-RIS-ACQ
        //                :IND-RST-RIS-ACQ
        //               ,:RST-RIS-UTI
        //                :IND-RST-RIS-UTI
        //               ,:RST-RIS-MAT-EFF
        //                :IND-RST-RIS-MAT-EFF
        //               ,:RST-RIS-RISTORNI-CAP
        //                :IND-RST-RIS-RISTORNI-CAP
        //               ,:RST-RIS-TRM-BNS
        //                :IND-RST-RIS-TRM-BNS
        //               ,:RST-RIS-BNSRIC
        //                :IND-RST-RIS-BNSRIC
        //               ,:RST-DS-RIGA
        //               ,:RST-DS-OPER-SQL
        //               ,:RST-DS-VER
        //               ,:RST-DS-TS-INI-CPTZ
        //               ,:RST-DS-TS-END-CPTZ
        //               ,:RST-DS-UTENTE
        //               ,:RST-DS-STATO-ELAB
        //               ,:RST-ID-TRCH-DI-GAR
        //               ,:RST-COD-FND
        //                :IND-RST-COD-FND
        //               ,:RST-ID-POLI
        //               ,:RST-ID-ADES
        //               ,:RST-ID-GAR
        //               ,:RST-RIS-MIN-GARTO
        //                :IND-RST-RIS-MIN-GARTO
        //               ,:RST-RIS-RSH-DFLT
        //                :IND-RST-RIS-RSH-DFLT
        //               ,:RST-RIS-MOVI-NON-INVES
        //                :IND-RST-RIS-MOVI-NON-INVES
        //           END-EXEC.
        risDiTrchDao.fetchCIdpCpzRst(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-RST-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO RST-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-ID-MOVI-CHIU-NULL
            risDiTrch.getRstIdMoviChiu().setRstIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstIdMoviChiu.Len.RST_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-RST-TP-CALC-RIS = -1
        //              MOVE HIGH-VALUES TO RST-TP-CALC-RIS-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getTpCalcRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-TP-CALC-RIS-NULL
            risDiTrch.setRstTpCalcRis(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RisDiTrchIdbsrst0.Len.RST_TP_CALC_RIS));
        }
        // COB_CODE: IF IND-RST-ULT-RM = -1
        //              MOVE HIGH-VALUES TO RST-ULT-RM-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getUltRm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-ULT-RM-NULL
            risDiTrch.getRstUltRm().setRstUltRmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstUltRm.Len.RST_ULT_RM_NULL));
        }
        // COB_CODE: IF IND-RST-DT-CALC = -1
        //              MOVE HIGH-VALUES TO RST-DT-CALC-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getDtCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-DT-CALC-NULL
            risDiTrch.getRstDtCalc().setRstDtCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstDtCalc.Len.RST_DT_CALC_NULL));
        }
        // COB_CODE: IF IND-RST-DT-ELAB = -1
        //              MOVE HIGH-VALUES TO RST-DT-ELAB-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getDtElab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-DT-ELAB-NULL
            risDiTrch.getRstDtElab().setRstDtElabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstDtElab.Len.RST_DT_ELAB_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-BILA = -1
        //              MOVE HIGH-VALUES TO RST-RIS-BILA-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisBila() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-BILA-NULL
            risDiTrch.getRstRisBila().setRstRisBilaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisBila.Len.RST_RIS_BILA_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO RST-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-MAT-NULL
            risDiTrch.getRstRisMat().setRstRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisMat.Len.RST_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-RST-INCR-X-RIVAL = -1
        //              MOVE HIGH-VALUES TO RST-INCR-X-RIVAL-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getIncrXRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-INCR-X-RIVAL-NULL
            risDiTrch.getRstIncrXRival().setRstIncrXRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstIncrXRival.Len.RST_INCR_X_RIVAL_NULL));
        }
        // COB_CODE: IF IND-RST-RPTO-PRE = -1
        //              MOVE HIGH-VALUES TO RST-RPTO-PRE-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRptoPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RPTO-PRE-NULL
            risDiTrch.getRstRptoPre().setRstRptoPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRptoPre.Len.RST_RPTO_PRE_NULL));
        }
        // COB_CODE: IF IND-RST-FRAZ-PRE-PP = -1
        //              MOVE HIGH-VALUES TO RST-FRAZ-PRE-PP-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getFrazPrePp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-FRAZ-PRE-PP-NULL
            risDiTrch.getRstFrazPrePp().setRstFrazPrePpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstFrazPrePp.Len.RST_FRAZ_PRE_PP_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-TOT = -1
        //              MOVE HIGH-VALUES TO RST-RIS-TOT-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-TOT-NULL
            risDiTrch.getRstRisTot().setRstRisTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisTot.Len.RST_RIS_TOT_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-SPE = -1
        //              MOVE HIGH-VALUES TO RST-RIS-SPE-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisSpe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-SPE-NULL
            risDiTrch.getRstRisSpe().setRstRisSpeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisSpe.Len.RST_RIS_SPE_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-ABB = -1
        //              MOVE HIGH-VALUES TO RST-RIS-ABB-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-ABB-NULL
            risDiTrch.getRstRisAbb().setRstRisAbbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisAbb.Len.RST_RIS_ABB_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-BNSFDT = -1
        //              MOVE HIGH-VALUES TO RST-RIS-BNSFDT-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisBnsfdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-BNSFDT-NULL
            risDiTrch.getRstRisBnsfdt().setRstRisBnsfdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisBnsfdt.Len.RST_RIS_BNSFDT_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-SOPR = -1
        //              MOVE HIGH-VALUES TO RST-RIS-SOPR-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisSopr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-SOPR-NULL
            risDiTrch.getRstRisSopr().setRstRisSoprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisSopr.Len.RST_RIS_SOPR_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-INTEG-BAS-TEC = -1
        //              MOVE HIGH-VALUES TO RST-RIS-INTEG-BAS-TEC-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisIntegBasTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-INTEG-BAS-TEC-NULL
            risDiTrch.getRstRisIntegBasTec().setRstRisIntegBasTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisIntegBasTec.Len.RST_RIS_INTEG_BAS_TEC_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-INTEG-DECR-TS = -1
        //              MOVE HIGH-VALUES TO RST-RIS-INTEG-DECR-TS-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisIntegDecrTs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-INTEG-DECR-TS-NULL
            risDiTrch.getRstRisIntegDecrTs().setRstRisIntegDecrTsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisIntegDecrTs.Len.RST_RIS_INTEG_DECR_TS_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-GAR-CASO-MOR = -1
        //              MOVE HIGH-VALUES TO RST-RIS-GAR-CASO-MOR-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisGarCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-GAR-CASO-MOR-NULL
            risDiTrch.getRstRisGarCasoMor().setRstRisGarCasoMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisGarCasoMor.Len.RST_RIS_GAR_CASO_MOR_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-ZIL = -1
        //              MOVE HIGH-VALUES TO RST-RIS-ZIL-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisZil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-ZIL-NULL
            risDiTrch.getRstRisZil().setRstRisZilNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisZil.Len.RST_RIS_ZIL_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-FAIVL = -1
        //              MOVE HIGH-VALUES TO RST-RIS-FAIVL-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisFaivl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-FAIVL-NULL
            risDiTrch.getRstRisFaivl().setRstRisFaivlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisFaivl.Len.RST_RIS_FAIVL_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-COS-AMMTZ = -1
        //              MOVE HIGH-VALUES TO RST-RIS-COS-AMMTZ-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisCosAmmtz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-COS-AMMTZ-NULL
            risDiTrch.getRstRisCosAmmtz().setRstRisCosAmmtzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisCosAmmtz.Len.RST_RIS_COS_AMMTZ_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-SPE-FAIVL = -1
        //              MOVE HIGH-VALUES TO RST-RIS-SPE-FAIVL-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisSpeFaivl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-SPE-FAIVL-NULL
            risDiTrch.getRstRisSpeFaivl().setRstRisSpeFaivlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisSpeFaivl.Len.RST_RIS_SPE_FAIVL_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-PREST-FAIVL = -1
        //              MOVE HIGH-VALUES TO RST-RIS-PREST-FAIVL-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisPrestFaivl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-PREST-FAIVL-NULL
            risDiTrch.getRstRisPrestFaivl().setRstRisPrestFaivlNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisPrestFaivl.Len.RST_RIS_PREST_FAIVL_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-COMPON-ASSVA = -1
        //              MOVE HIGH-VALUES TO RST-RIS-COMPON-ASSVA-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisComponAssva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-COMPON-ASSVA-NULL
            risDiTrch.getRstRisComponAssva().setRstRisComponAssvaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisComponAssva.Len.RST_RIS_COMPON_ASSVA_NULL));
        }
        // COB_CODE: IF IND-RST-ULT-COEFF-RIS = -1
        //              MOVE HIGH-VALUES TO RST-ULT-COEFF-RIS-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getUltCoeffRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-ULT-COEFF-RIS-NULL
            risDiTrch.getRstUltCoeffRis().setRstUltCoeffRisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstUltCoeffRis.Len.RST_ULT_COEFF_RIS_NULL));
        }
        // COB_CODE: IF IND-RST-ULT-COEFF-AGG-RIS = -1
        //              MOVE HIGH-VALUES TO RST-ULT-COEFF-AGG-RIS-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getUltCoeffAggRis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-ULT-COEFF-AGG-RIS-NULL
            risDiTrch.getRstUltCoeffAggRis().setRstUltCoeffAggRisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstUltCoeffAggRis.Len.RST_ULT_COEFF_AGG_RIS_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-ACQ = -1
        //              MOVE HIGH-VALUES TO RST-RIS-ACQ-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-ACQ-NULL
            risDiTrch.getRstRisAcq().setRstRisAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisAcq.Len.RST_RIS_ACQ_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-UTI = -1
        //              MOVE HIGH-VALUES TO RST-RIS-UTI-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisUti() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-UTI-NULL
            risDiTrch.getRstRisUti().setRstRisUtiNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisUti.Len.RST_RIS_UTI_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-MAT-EFF = -1
        //              MOVE HIGH-VALUES TO RST-RIS-MAT-EFF-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisMatEff() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-MAT-EFF-NULL
            risDiTrch.getRstRisMatEff().setRstRisMatEffNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisMatEff.Len.RST_RIS_MAT_EFF_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-RISTORNI-CAP = -1
        //              MOVE HIGH-VALUES TO RST-RIS-RISTORNI-CAP-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisRistorniCap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-RISTORNI-CAP-NULL
            risDiTrch.getRstRisRistorniCap().setRstRisRistorniCapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisRistorniCap.Len.RST_RIS_RISTORNI_CAP_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-TRM-BNS = -1
        //              MOVE HIGH-VALUES TO RST-RIS-TRM-BNS-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisTrmBns() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-TRM-BNS-NULL
            risDiTrch.getRstRisTrmBns().setRstRisTrmBnsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisTrmBns.Len.RST_RIS_TRM_BNS_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-BNSRIC = -1
        //              MOVE HIGH-VALUES TO RST-RIS-BNSRIC-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisBnsric() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-BNSRIC-NULL
            risDiTrch.getRstRisBnsric().setRstRisBnsricNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisBnsric.Len.RST_RIS_BNSRIC_NULL));
        }
        // COB_CODE: IF IND-RST-COD-FND = -1
        //              MOVE HIGH-VALUES TO RST-COD-FND-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-COD-FND-NULL
            risDiTrch.setRstCodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RisDiTrchIdbsrst0.Len.RST_COD_FND));
        }
        // COB_CODE: IF IND-RST-RIS-MIN-GARTO = -1
        //              MOVE HIGH-VALUES TO RST-RIS-MIN-GARTO-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisMinGarto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-MIN-GARTO-NULL
            risDiTrch.getRstRisMinGarto().setRstRisMinGartoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisMinGarto.Len.RST_RIS_MIN_GARTO_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-RSH-DFLT = -1
        //              MOVE HIGH-VALUES TO RST-RIS-RSH-DFLT-NULL
        //           END-IF
        if (ws.getIndRisDiTrch().getRisRshDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-RSH-DFLT-NULL
            risDiTrch.getRstRisRshDflt().setRstRisRshDfltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisRshDflt.Len.RST_RIS_RSH_DFLT_NULL));
        }
        // COB_CODE: IF IND-RST-RIS-MOVI-NON-INVES = -1
        //              MOVE HIGH-VALUES TO RST-RIS-MOVI-NON-INVES-NULL
        //           END-IF.
        if (ws.getIndRisDiTrch().getRisMoviNonInves() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RST-RIS-MOVI-NON-INVES-NULL
            risDiTrch.getRstRisMoviNonInves().setRstRisMoviNonInvesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstRisMoviNonInves.Len.RST_RIS_MOVI_NON_INVES_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO RST-DS-OPER-SQL
        risDiTrch.setRstDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO RST-DS-VER
        risDiTrch.setRstDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO RST-DS-UTENTE
        risDiTrch.setRstDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO RST-DS-STATO-ELAB.
        risDiTrch.setRstDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO RST-DS-OPER-SQL
        risDiTrch.setRstDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO RST-DS-UTENTE.
        risDiTrch.setRstDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF RST-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-RST-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstIdMoviChiu().getRstIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-ID-MOVI-CHIU
            ws.getIndRisDiTrch().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-ID-MOVI-CHIU
            ws.getIndRisDiTrch().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF RST-TP-CALC-RIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-TP-CALC-RIS
        //           ELSE
        //              MOVE 0 TO IND-RST-TP-CALC-RIS
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstTpCalcRisFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-TP-CALC-RIS
            ws.getIndRisDiTrch().setTpCalcRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-TP-CALC-RIS
            ws.getIndRisDiTrch().setTpCalcRis(((short)0));
        }
        // COB_CODE: IF RST-ULT-RM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-ULT-RM
        //           ELSE
        //              MOVE 0 TO IND-RST-ULT-RM
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstUltRm().getRstUltRmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-ULT-RM
            ws.getIndRisDiTrch().setUltRm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-ULT-RM
            ws.getIndRisDiTrch().setUltRm(((short)0));
        }
        // COB_CODE: IF RST-DT-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-DT-CALC
        //           ELSE
        //              MOVE 0 TO IND-RST-DT-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstDtCalc().getRstDtCalcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-DT-CALC
            ws.getIndRisDiTrch().setDtCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-DT-CALC
            ws.getIndRisDiTrch().setDtCalc(((short)0));
        }
        // COB_CODE: IF RST-DT-ELAB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-DT-ELAB
        //           ELSE
        //              MOVE 0 TO IND-RST-DT-ELAB
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstDtElab().getRstDtElabNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-DT-ELAB
            ws.getIndRisDiTrch().setDtElab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-DT-ELAB
            ws.getIndRisDiTrch().setDtElab(((short)0));
        }
        // COB_CODE: IF RST-RIS-BILA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-BILA
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-BILA
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisBila().getRstRisBilaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-BILA
            ws.getIndRisDiTrch().setRisBila(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-BILA
            ws.getIndRisDiTrch().setRisBila(((short)0));
        }
        // COB_CODE: IF RST-RIS-MAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-MAT
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-MAT
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisMat().getRstRisMatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-MAT
            ws.getIndRisDiTrch().setRisMat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-MAT
            ws.getIndRisDiTrch().setRisMat(((short)0));
        }
        // COB_CODE: IF RST-INCR-X-RIVAL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-INCR-X-RIVAL
        //           ELSE
        //              MOVE 0 TO IND-RST-INCR-X-RIVAL
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstIncrXRival().getRstIncrXRivalNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-INCR-X-RIVAL
            ws.getIndRisDiTrch().setIncrXRival(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-INCR-X-RIVAL
            ws.getIndRisDiTrch().setIncrXRival(((short)0));
        }
        // COB_CODE: IF RST-RPTO-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RPTO-PRE
        //           ELSE
        //              MOVE 0 TO IND-RST-RPTO-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRptoPre().getRstRptoPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RPTO-PRE
            ws.getIndRisDiTrch().setRptoPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RPTO-PRE
            ws.getIndRisDiTrch().setRptoPre(((short)0));
        }
        // COB_CODE: IF RST-FRAZ-PRE-PP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-FRAZ-PRE-PP
        //           ELSE
        //              MOVE 0 TO IND-RST-FRAZ-PRE-PP
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstFrazPrePp().getRstFrazPrePpNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-FRAZ-PRE-PP
            ws.getIndRisDiTrch().setFrazPrePp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-FRAZ-PRE-PP
            ws.getIndRisDiTrch().setFrazPrePp(((short)0));
        }
        // COB_CODE: IF RST-RIS-TOT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-TOT
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-TOT
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisTot().getRstRisTotNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-TOT
            ws.getIndRisDiTrch().setRisTot(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-TOT
            ws.getIndRisDiTrch().setRisTot(((short)0));
        }
        // COB_CODE: IF RST-RIS-SPE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-SPE
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-SPE
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisSpe().getRstRisSpeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-SPE
            ws.getIndRisDiTrch().setRisSpe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-SPE
            ws.getIndRisDiTrch().setRisSpe(((short)0));
        }
        // COB_CODE: IF RST-RIS-ABB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-ABB
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-ABB
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisAbb().getRstRisAbbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-ABB
            ws.getIndRisDiTrch().setRisAbb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-ABB
            ws.getIndRisDiTrch().setRisAbb(((short)0));
        }
        // COB_CODE: IF RST-RIS-BNSFDT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-BNSFDT
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-BNSFDT
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisBnsfdt().getRstRisBnsfdtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-BNSFDT
            ws.getIndRisDiTrch().setRisBnsfdt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-BNSFDT
            ws.getIndRisDiTrch().setRisBnsfdt(((short)0));
        }
        // COB_CODE: IF RST-RIS-SOPR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-SOPR
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-SOPR
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisSopr().getRstRisSoprNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-SOPR
            ws.getIndRisDiTrch().setRisSopr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-SOPR
            ws.getIndRisDiTrch().setRisSopr(((short)0));
        }
        // COB_CODE: IF RST-RIS-INTEG-BAS-TEC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-INTEG-BAS-TEC
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-INTEG-BAS-TEC
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisIntegBasTec().getRstRisIntegBasTecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-INTEG-BAS-TEC
            ws.getIndRisDiTrch().setRisIntegBasTec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-INTEG-BAS-TEC
            ws.getIndRisDiTrch().setRisIntegBasTec(((short)0));
        }
        // COB_CODE: IF RST-RIS-INTEG-DECR-TS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-INTEG-DECR-TS
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-INTEG-DECR-TS
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisIntegDecrTs().getRstRisIntegDecrTsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-INTEG-DECR-TS
            ws.getIndRisDiTrch().setRisIntegDecrTs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-INTEG-DECR-TS
            ws.getIndRisDiTrch().setRisIntegDecrTs(((short)0));
        }
        // COB_CODE: IF RST-RIS-GAR-CASO-MOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-GAR-CASO-MOR
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-GAR-CASO-MOR
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisGarCasoMor().getRstRisGarCasoMorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-GAR-CASO-MOR
            ws.getIndRisDiTrch().setRisGarCasoMor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-GAR-CASO-MOR
            ws.getIndRisDiTrch().setRisGarCasoMor(((short)0));
        }
        // COB_CODE: IF RST-RIS-ZIL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-ZIL
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-ZIL
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisZil().getRstRisZilNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-ZIL
            ws.getIndRisDiTrch().setRisZil(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-ZIL
            ws.getIndRisDiTrch().setRisZil(((short)0));
        }
        // COB_CODE: IF RST-RIS-FAIVL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-FAIVL
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-FAIVL
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisFaivl().getRstRisFaivlNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-FAIVL
            ws.getIndRisDiTrch().setRisFaivl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-FAIVL
            ws.getIndRisDiTrch().setRisFaivl(((short)0));
        }
        // COB_CODE: IF RST-RIS-COS-AMMTZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-COS-AMMTZ
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-COS-AMMTZ
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisCosAmmtz().getRstRisCosAmmtzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-COS-AMMTZ
            ws.getIndRisDiTrch().setRisCosAmmtz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-COS-AMMTZ
            ws.getIndRisDiTrch().setRisCosAmmtz(((short)0));
        }
        // COB_CODE: IF RST-RIS-SPE-FAIVL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-SPE-FAIVL
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-SPE-FAIVL
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisSpeFaivl().getRstRisSpeFaivlNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-SPE-FAIVL
            ws.getIndRisDiTrch().setRisSpeFaivl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-SPE-FAIVL
            ws.getIndRisDiTrch().setRisSpeFaivl(((short)0));
        }
        // COB_CODE: IF RST-RIS-PREST-FAIVL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-PREST-FAIVL
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-PREST-FAIVL
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisPrestFaivl().getRstRisPrestFaivlNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-PREST-FAIVL
            ws.getIndRisDiTrch().setRisPrestFaivl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-PREST-FAIVL
            ws.getIndRisDiTrch().setRisPrestFaivl(((short)0));
        }
        // COB_CODE: IF RST-RIS-COMPON-ASSVA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-COMPON-ASSVA
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-COMPON-ASSVA
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisComponAssva().getRstRisComponAssvaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-COMPON-ASSVA
            ws.getIndRisDiTrch().setRisComponAssva(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-COMPON-ASSVA
            ws.getIndRisDiTrch().setRisComponAssva(((short)0));
        }
        // COB_CODE: IF RST-ULT-COEFF-RIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-ULT-COEFF-RIS
        //           ELSE
        //              MOVE 0 TO IND-RST-ULT-COEFF-RIS
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstUltCoeffRis().getRstUltCoeffRisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-ULT-COEFF-RIS
            ws.getIndRisDiTrch().setUltCoeffRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-ULT-COEFF-RIS
            ws.getIndRisDiTrch().setUltCoeffRis(((short)0));
        }
        // COB_CODE: IF RST-ULT-COEFF-AGG-RIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-ULT-COEFF-AGG-RIS
        //           ELSE
        //              MOVE 0 TO IND-RST-ULT-COEFF-AGG-RIS
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstUltCoeffAggRis().getRstUltCoeffAggRisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-ULT-COEFF-AGG-RIS
            ws.getIndRisDiTrch().setUltCoeffAggRis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-ULT-COEFF-AGG-RIS
            ws.getIndRisDiTrch().setUltCoeffAggRis(((short)0));
        }
        // COB_CODE: IF RST-RIS-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-ACQ
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisAcq().getRstRisAcqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-ACQ
            ws.getIndRisDiTrch().setRisAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-ACQ
            ws.getIndRisDiTrch().setRisAcq(((short)0));
        }
        // COB_CODE: IF RST-RIS-UTI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-UTI
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-UTI
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisUti().getRstRisUtiNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-UTI
            ws.getIndRisDiTrch().setRisUti(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-UTI
            ws.getIndRisDiTrch().setRisUti(((short)0));
        }
        // COB_CODE: IF RST-RIS-MAT-EFF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-MAT-EFF
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-MAT-EFF
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisMatEff().getRstRisMatEffNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-MAT-EFF
            ws.getIndRisDiTrch().setRisMatEff(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-MAT-EFF
            ws.getIndRisDiTrch().setRisMatEff(((short)0));
        }
        // COB_CODE: IF RST-RIS-RISTORNI-CAP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-RISTORNI-CAP
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-RISTORNI-CAP
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisRistorniCap().getRstRisRistorniCapNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-RISTORNI-CAP
            ws.getIndRisDiTrch().setRisRistorniCap(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-RISTORNI-CAP
            ws.getIndRisDiTrch().setRisRistorniCap(((short)0));
        }
        // COB_CODE: IF RST-RIS-TRM-BNS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-TRM-BNS
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-TRM-BNS
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisTrmBns().getRstRisTrmBnsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-TRM-BNS
            ws.getIndRisDiTrch().setRisTrmBns(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-TRM-BNS
            ws.getIndRisDiTrch().setRisTrmBns(((short)0));
        }
        // COB_CODE: IF RST-RIS-BNSRIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-BNSRIC
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-BNSRIC
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisBnsric().getRstRisBnsricNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-BNSRIC
            ws.getIndRisDiTrch().setRisBnsric(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-BNSRIC
            ws.getIndRisDiTrch().setRisBnsric(((short)0));
        }
        // COB_CODE: IF RST-COD-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-COD-FND
        //           ELSE
        //              MOVE 0 TO IND-RST-COD-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstCodFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-COD-FND
            ws.getIndRisDiTrch().setCodFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-COD-FND
            ws.getIndRisDiTrch().setCodFnd(((short)0));
        }
        // COB_CODE: IF RST-RIS-MIN-GARTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-MIN-GARTO
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-MIN-GARTO
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisMinGarto().getRstRisMinGartoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-MIN-GARTO
            ws.getIndRisDiTrch().setRisMinGarto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-MIN-GARTO
            ws.getIndRisDiTrch().setRisMinGarto(((short)0));
        }
        // COB_CODE: IF RST-RIS-RSH-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-RSH-DFLT
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-RSH-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisRshDflt().getRstRisRshDfltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-RSH-DFLT
            ws.getIndRisDiTrch().setRisRshDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-RSH-DFLT
            ws.getIndRisDiTrch().setRisRshDflt(((short)0));
        }
        // COB_CODE: IF RST-RIS-MOVI-NON-INVES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RST-RIS-MOVI-NON-INVES
        //           ELSE
        //              MOVE 0 TO IND-RST-RIS-MOVI-NON-INVES
        //           END-IF.
        if (Characters.EQ_HIGH.test(risDiTrch.getRstRisMoviNonInves().getRstRisMoviNonInvesNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RST-RIS-MOVI-NON-INVES
            ws.getIndRisDiTrch().setRisMoviNonInves(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RST-RIS-MOVI-NON-INVES
            ws.getIndRisDiTrch().setRisMoviNonInves(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : RST-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE RIS-DI-TRCH TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(risDiTrch.getRisDiTrchFormatted());
        // COB_CODE: MOVE RST-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(risDiTrch.getRstIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO RST-ID-MOVI-CHIU
                risDiTrch.getRstIdMoviChiu().setRstIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO RST-DS-TS-END-CPTZ
                risDiTrch.setRstDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO RST-ID-MOVI-CRZ
                    risDiTrch.setRstIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO RST-ID-MOVI-CHIU-NULL
                    risDiTrch.getRstIdMoviChiu().setRstIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstIdMoviChiu.Len.RST_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO RST-DT-END-EFF
                    risDiTrch.setRstDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO RST-DS-TS-INI-CPTZ
                    risDiTrch.setRstDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO RST-DS-TS-END-CPTZ
                    risDiTrch.setRstDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE RIS-DI-TRCH TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(risDiTrch.getRisDiTrchFormatted());
        // COB_CODE: MOVE RST-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(risDiTrch.getRstIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO RIS-DI-TRCH.
        risDiTrch.setRisDiTrchFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO RST-ID-MOVI-CRZ.
        risDiTrch.setRstIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO RST-ID-MOVI-CHIU-NULL.
        risDiTrch.getRstIdMoviChiu().setRstIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RstIdMoviChiu.Len.RST_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO RST-DT-INI-EFF.
        risDiTrch.setRstDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO RST-DT-END-EFF.
        risDiTrch.setRstDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO RST-DS-TS-INI-CPTZ.
        risDiTrch.setRstDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO RST-DS-TS-END-CPTZ.
        risDiTrch.setRstDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO RST-COD-COMP-ANIA.
        risDiTrch.setRstCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE RST-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(risDiTrch.getRstDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RST-DT-INI-EFF-DB
        ws.getRisDiTrchDb().setEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE RST-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(risDiTrch.getRstDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RST-DT-END-EFF-DB
        ws.getRisDiTrchDb().setRgstrzRichDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-RST-DT-CALC = 0
        //               MOVE WS-DATE-X      TO RST-DT-CALC-DB
        //           END-IF
        if (ws.getIndRisDiTrch().getDtCalc() == 0) {
            // COB_CODE: MOVE RST-DT-CALC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(risDiTrch.getRstDtCalc().getRstDtCalc(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO RST-DT-CALC-DB
            ws.getRisDiTrchDb().setPervRichDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-RST-DT-ELAB = 0
        //               MOVE WS-DATE-X      TO RST-DT-ELAB-DB
        //           END-IF.
        if (ws.getIndRisDiTrch().getDtElab() == 0) {
            // COB_CODE: MOVE RST-DT-ELAB TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(risDiTrch.getRstDtElab().getRstDtElab(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO RST-DT-ELAB-DB
            ws.getRisDiTrchDb().setEsecRichDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE RST-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRisDiTrchDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RST-DT-INI-EFF
        risDiTrch.setRstDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RST-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRisDiTrchDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RST-DT-END-EFF
        risDiTrch.setRstDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-RST-DT-CALC = 0
        //               MOVE WS-DATE-N      TO RST-DT-CALC
        //           END-IF
        if (ws.getIndRisDiTrch().getDtCalc() == 0) {
            // COB_CODE: MOVE RST-DT-CALC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRisDiTrchDb().getPervRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RST-DT-CALC
            risDiTrch.getRstDtCalc().setRstDtCalc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-RST-DT-ELAB = 0
        //               MOVE WS-DATE-N      TO RST-DT-ELAB
        //           END-IF.
        if (ws.getIndRisDiTrch().getDtElab() == 0) {
            // COB_CODE: MOVE RST-DT-ELAB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRisDiTrchDb().getEsecRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RST-DT-ELAB
            risDiTrch.getRstDtElab().setRstDtElab(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return risDiTrch.getRstCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.risDiTrch.setRstCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFnd() {
        return risDiTrch.getRstCodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.risDiTrch.setRstCodFnd(codFnd);
    }

    @Override
    public String getCodFndObj() {
        if (ws.getIndRisDiTrch().getCodFnd() >= 0) {
            return getCodFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        if (codFndObj != null) {
            setCodFnd(codFndObj);
            ws.getIndRisDiTrch().setCodFnd(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setCodFnd(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return risDiTrch.getRstDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.risDiTrch.setRstDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return risDiTrch.getRstDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.risDiTrch.setRstDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return risDiTrch.getRstDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.risDiTrch.setRstDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return risDiTrch.getRstDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.risDiTrch.setRstDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return risDiTrch.getRstDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.risDiTrch.setRstDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return risDiTrch.getRstDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.risDiTrch.setRstDsVer(dsVer);
    }

    @Override
    public String getDtCalcDb() {
        return ws.getRisDiTrchDb().getPervRichDb();
    }

    @Override
    public void setDtCalcDb(String dtCalcDb) {
        this.ws.getRisDiTrchDb().setPervRichDb(dtCalcDb);
    }

    @Override
    public String getDtCalcDbObj() {
        if (ws.getIndRisDiTrch().getDtCalc() >= 0) {
            return getDtCalcDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtCalcDbObj(String dtCalcDbObj) {
        if (dtCalcDbObj != null) {
            setDtCalcDb(dtCalcDbObj);
            ws.getIndRisDiTrch().setDtCalc(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setDtCalc(((short)-1));
        }
    }

    @Override
    public String getDtElabDb() {
        return ws.getRisDiTrchDb().getEsecRichDb();
    }

    @Override
    public void setDtElabDb(String dtElabDb) {
        this.ws.getRisDiTrchDb().setEsecRichDb(dtElabDb);
    }

    @Override
    public String getDtElabDbObj() {
        if (ws.getIndRisDiTrch().getDtElab() >= 0) {
            return getDtElabDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtElabDbObj(String dtElabDbObj) {
        if (dtElabDbObj != null) {
            setDtElabDb(dtElabDbObj);
            ws.getIndRisDiTrch().setDtElab(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setDtElab(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getRisDiTrchDb().getRgstrzRichDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getRisDiTrchDb().setRgstrzRichDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getRisDiTrchDb().getEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getRisDiTrchDb().setEffDb(dtIniEffDb);
    }

    @Override
    public AfDecimal getFrazPrePp() {
        return risDiTrch.getRstFrazPrePp().getRstFrazPrePp();
    }

    @Override
    public void setFrazPrePp(AfDecimal frazPrePp) {
        this.risDiTrch.getRstFrazPrePp().setRstFrazPrePp(frazPrePp.copy());
    }

    @Override
    public AfDecimal getFrazPrePpObj() {
        if (ws.getIndRisDiTrch().getFrazPrePp() >= 0) {
            return getFrazPrePp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazPrePpObj(AfDecimal frazPrePpObj) {
        if (frazPrePpObj != null) {
            setFrazPrePp(new AfDecimal(frazPrePpObj, 15, 3));
            ws.getIndRisDiTrch().setFrazPrePp(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setFrazPrePp(((short)-1));
        }
    }

    @Override
    public int getIdAdes() {
        return risDiTrch.getRstIdAdes();
    }

    @Override
    public void setIdAdes(int idAdes) {
        this.risDiTrch.setRstIdAdes(idAdes);
    }

    @Override
    public int getIdGar() {
        return risDiTrch.getRstIdGar();
    }

    @Override
    public void setIdGar(int idGar) {
        this.risDiTrch.setRstIdGar(idGar);
    }

    @Override
    public int getIdMoviChiu() {
        return risDiTrch.getRstIdMoviChiu().getRstIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.risDiTrch.getRstIdMoviChiu().setRstIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndRisDiTrch().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndRisDiTrch().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return risDiTrch.getRstIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.risDiTrch.setRstIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return risDiTrch.getRstIdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.risDiTrch.setRstIdPoli(idPoli);
    }

    @Override
    public int getIdRisDiTrch() {
        return risDiTrch.getRstIdRisDiTrch();
    }

    @Override
    public void setIdRisDiTrch(int idRisDiTrch) {
        this.risDiTrch.setRstIdRisDiTrch(idRisDiTrch);
    }

    @Override
    public int getIdTrchDiGar() {
        return risDiTrch.getRstIdTrchDiGar();
    }

    @Override
    public void setIdTrchDiGar(int idTrchDiGar) {
        this.risDiTrch.setRstIdTrchDiGar(idTrchDiGar);
    }

    @Override
    public AfDecimal getIncrXRival() {
        return risDiTrch.getRstIncrXRival().getRstIncrXRival();
    }

    @Override
    public void setIncrXRival(AfDecimal incrXRival) {
        this.risDiTrch.getRstIncrXRival().setRstIncrXRival(incrXRival.copy());
    }

    @Override
    public AfDecimal getIncrXRivalObj() {
        if (ws.getIndRisDiTrch().getIncrXRival() >= 0) {
            return getIncrXRival();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIncrXRivalObj(AfDecimal incrXRivalObj) {
        if (incrXRivalObj != null) {
            setIncrXRival(new AfDecimal(incrXRivalObj, 15, 3));
            ws.getIndRisDiTrch().setIncrXRival(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setIncrXRival(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisAbb() {
        return risDiTrch.getRstRisAbb().getRstRisAbb();
    }

    @Override
    public void setRisAbb(AfDecimal risAbb) {
        this.risDiTrch.getRstRisAbb().setRstRisAbb(risAbb.copy());
    }

    @Override
    public AfDecimal getRisAbbObj() {
        if (ws.getIndRisDiTrch().getRisAbb() >= 0) {
            return getRisAbb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisAbbObj(AfDecimal risAbbObj) {
        if (risAbbObj != null) {
            setRisAbb(new AfDecimal(risAbbObj, 15, 3));
            ws.getIndRisDiTrch().setRisAbb(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisAbb(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisAcq() {
        return risDiTrch.getRstRisAcq().getRstRisAcq();
    }

    @Override
    public void setRisAcq(AfDecimal risAcq) {
        this.risDiTrch.getRstRisAcq().setRstRisAcq(risAcq.copy());
    }

    @Override
    public AfDecimal getRisAcqObj() {
        if (ws.getIndRisDiTrch().getRisAcq() >= 0) {
            return getRisAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisAcqObj(AfDecimal risAcqObj) {
        if (risAcqObj != null) {
            setRisAcq(new AfDecimal(risAcqObj, 15, 3));
            ws.getIndRisDiTrch().setRisAcq(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisBila() {
        return risDiTrch.getRstRisBila().getRstRisBila();
    }

    @Override
    public void setRisBila(AfDecimal risBila) {
        this.risDiTrch.getRstRisBila().setRstRisBila(risBila.copy());
    }

    @Override
    public AfDecimal getRisBilaObj() {
        if (ws.getIndRisDiTrch().getRisBila() >= 0) {
            return getRisBila();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisBilaObj(AfDecimal risBilaObj) {
        if (risBilaObj != null) {
            setRisBila(new AfDecimal(risBilaObj, 15, 3));
            ws.getIndRisDiTrch().setRisBila(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisBila(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisBnsfdt() {
        return risDiTrch.getRstRisBnsfdt().getRstRisBnsfdt();
    }

    @Override
    public void setRisBnsfdt(AfDecimal risBnsfdt) {
        this.risDiTrch.getRstRisBnsfdt().setRstRisBnsfdt(risBnsfdt.copy());
    }

    @Override
    public AfDecimal getRisBnsfdtObj() {
        if (ws.getIndRisDiTrch().getRisBnsfdt() >= 0) {
            return getRisBnsfdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisBnsfdtObj(AfDecimal risBnsfdtObj) {
        if (risBnsfdtObj != null) {
            setRisBnsfdt(new AfDecimal(risBnsfdtObj, 15, 3));
            ws.getIndRisDiTrch().setRisBnsfdt(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisBnsfdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisBnsric() {
        return risDiTrch.getRstRisBnsric().getRstRisBnsric();
    }

    @Override
    public void setRisBnsric(AfDecimal risBnsric) {
        this.risDiTrch.getRstRisBnsric().setRstRisBnsric(risBnsric.copy());
    }

    @Override
    public AfDecimal getRisBnsricObj() {
        if (ws.getIndRisDiTrch().getRisBnsric() >= 0) {
            return getRisBnsric();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisBnsricObj(AfDecimal risBnsricObj) {
        if (risBnsricObj != null) {
            setRisBnsric(new AfDecimal(risBnsricObj, 15, 3));
            ws.getIndRisDiTrch().setRisBnsric(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisBnsric(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisComponAssva() {
        return risDiTrch.getRstRisComponAssva().getRstRisComponAssva();
    }

    @Override
    public void setRisComponAssva(AfDecimal risComponAssva) {
        this.risDiTrch.getRstRisComponAssva().setRstRisComponAssva(risComponAssva.copy());
    }

    @Override
    public AfDecimal getRisComponAssvaObj() {
        if (ws.getIndRisDiTrch().getRisComponAssva() >= 0) {
            return getRisComponAssva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisComponAssvaObj(AfDecimal risComponAssvaObj) {
        if (risComponAssvaObj != null) {
            setRisComponAssva(new AfDecimal(risComponAssvaObj, 15, 3));
            ws.getIndRisDiTrch().setRisComponAssva(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisComponAssva(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisCosAmmtz() {
        return risDiTrch.getRstRisCosAmmtz().getRstRisCosAmmtz();
    }

    @Override
    public void setRisCosAmmtz(AfDecimal risCosAmmtz) {
        this.risDiTrch.getRstRisCosAmmtz().setRstRisCosAmmtz(risCosAmmtz.copy());
    }

    @Override
    public AfDecimal getRisCosAmmtzObj() {
        if (ws.getIndRisDiTrch().getRisCosAmmtz() >= 0) {
            return getRisCosAmmtz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisCosAmmtzObj(AfDecimal risCosAmmtzObj) {
        if (risCosAmmtzObj != null) {
            setRisCosAmmtz(new AfDecimal(risCosAmmtzObj, 15, 3));
            ws.getIndRisDiTrch().setRisCosAmmtz(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisCosAmmtz(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisFaivl() {
        return risDiTrch.getRstRisFaivl().getRstRisFaivl();
    }

    @Override
    public void setRisFaivl(AfDecimal risFaivl) {
        this.risDiTrch.getRstRisFaivl().setRstRisFaivl(risFaivl.copy());
    }

    @Override
    public AfDecimal getRisFaivlObj() {
        if (ws.getIndRisDiTrch().getRisFaivl() >= 0) {
            return getRisFaivl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisFaivlObj(AfDecimal risFaivlObj) {
        if (risFaivlObj != null) {
            setRisFaivl(new AfDecimal(risFaivlObj, 15, 3));
            ws.getIndRisDiTrch().setRisFaivl(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisFaivl(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisGarCasoMor() {
        return risDiTrch.getRstRisGarCasoMor().getRstRisGarCasoMor();
    }

    @Override
    public void setRisGarCasoMor(AfDecimal risGarCasoMor) {
        this.risDiTrch.getRstRisGarCasoMor().setRstRisGarCasoMor(risGarCasoMor.copy());
    }

    @Override
    public AfDecimal getRisGarCasoMorObj() {
        if (ws.getIndRisDiTrch().getRisGarCasoMor() >= 0) {
            return getRisGarCasoMor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisGarCasoMorObj(AfDecimal risGarCasoMorObj) {
        if (risGarCasoMorObj != null) {
            setRisGarCasoMor(new AfDecimal(risGarCasoMorObj, 15, 3));
            ws.getIndRisDiTrch().setRisGarCasoMor(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisGarCasoMor(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisIntegBasTec() {
        return risDiTrch.getRstRisIntegBasTec().getRstRisIntegBasTec();
    }

    @Override
    public void setRisIntegBasTec(AfDecimal risIntegBasTec) {
        this.risDiTrch.getRstRisIntegBasTec().setRstRisIntegBasTec(risIntegBasTec.copy());
    }

    @Override
    public AfDecimal getRisIntegBasTecObj() {
        if (ws.getIndRisDiTrch().getRisIntegBasTec() >= 0) {
            return getRisIntegBasTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisIntegBasTecObj(AfDecimal risIntegBasTecObj) {
        if (risIntegBasTecObj != null) {
            setRisIntegBasTec(new AfDecimal(risIntegBasTecObj, 15, 3));
            ws.getIndRisDiTrch().setRisIntegBasTec(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisIntegBasTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisIntegDecrTs() {
        return risDiTrch.getRstRisIntegDecrTs().getRstRisIntegDecrTs();
    }

    @Override
    public void setRisIntegDecrTs(AfDecimal risIntegDecrTs) {
        this.risDiTrch.getRstRisIntegDecrTs().setRstRisIntegDecrTs(risIntegDecrTs.copy());
    }

    @Override
    public AfDecimal getRisIntegDecrTsObj() {
        if (ws.getIndRisDiTrch().getRisIntegDecrTs() >= 0) {
            return getRisIntegDecrTs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisIntegDecrTsObj(AfDecimal risIntegDecrTsObj) {
        if (risIntegDecrTsObj != null) {
            setRisIntegDecrTs(new AfDecimal(risIntegDecrTsObj, 15, 3));
            ws.getIndRisDiTrch().setRisIntegDecrTs(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisIntegDecrTs(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMat() {
        return risDiTrch.getRstRisMat().getRstRisMat();
    }

    @Override
    public void setRisMat(AfDecimal risMat) {
        this.risDiTrch.getRstRisMat().setRstRisMat(risMat.copy());
    }

    @Override
    public AfDecimal getRisMatEff() {
        return risDiTrch.getRstRisMatEff().getRstRisMatEff();
    }

    @Override
    public void setRisMatEff(AfDecimal risMatEff) {
        this.risDiTrch.getRstRisMatEff().setRstRisMatEff(risMatEff.copy());
    }

    @Override
    public AfDecimal getRisMatEffObj() {
        if (ws.getIndRisDiTrch().getRisMatEff() >= 0) {
            return getRisMatEff();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatEffObj(AfDecimal risMatEffObj) {
        if (risMatEffObj != null) {
            setRisMatEff(new AfDecimal(risMatEffObj, 15, 3));
            ws.getIndRisDiTrch().setRisMatEff(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisMatEff(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMatObj() {
        if (ws.getIndRisDiTrch().getRisMat() >= 0) {
            return getRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatObj(AfDecimal risMatObj) {
        if (risMatObj != null) {
            setRisMat(new AfDecimal(risMatObj, 15, 3));
            ws.getIndRisDiTrch().setRisMat(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisMat(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMinGarto() {
        return risDiTrch.getRstRisMinGarto().getRstRisMinGarto();
    }

    @Override
    public void setRisMinGarto(AfDecimal risMinGarto) {
        this.risDiTrch.getRstRisMinGarto().setRstRisMinGarto(risMinGarto.copy());
    }

    @Override
    public AfDecimal getRisMinGartoObj() {
        if (ws.getIndRisDiTrch().getRisMinGarto() >= 0) {
            return getRisMinGarto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMinGartoObj(AfDecimal risMinGartoObj) {
        if (risMinGartoObj != null) {
            setRisMinGarto(new AfDecimal(risMinGartoObj, 15, 3));
            ws.getIndRisDiTrch().setRisMinGarto(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisMinGarto(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMoviNonInves() {
        return risDiTrch.getRstRisMoviNonInves().getRstRisMoviNonInves();
    }

    @Override
    public void setRisMoviNonInves(AfDecimal risMoviNonInves) {
        this.risDiTrch.getRstRisMoviNonInves().setRstRisMoviNonInves(risMoviNonInves.copy());
    }

    @Override
    public AfDecimal getRisMoviNonInvesObj() {
        if (ws.getIndRisDiTrch().getRisMoviNonInves() >= 0) {
            return getRisMoviNonInves();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMoviNonInvesObj(AfDecimal risMoviNonInvesObj) {
        if (risMoviNonInvesObj != null) {
            setRisMoviNonInves(new AfDecimal(risMoviNonInvesObj, 15, 3));
            ws.getIndRisDiTrch().setRisMoviNonInves(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisMoviNonInves(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisPrestFaivl() {
        return risDiTrch.getRstRisPrestFaivl().getRstRisPrestFaivl();
    }

    @Override
    public void setRisPrestFaivl(AfDecimal risPrestFaivl) {
        this.risDiTrch.getRstRisPrestFaivl().setRstRisPrestFaivl(risPrestFaivl.copy());
    }

    @Override
    public AfDecimal getRisPrestFaivlObj() {
        if (ws.getIndRisDiTrch().getRisPrestFaivl() >= 0) {
            return getRisPrestFaivl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisPrestFaivlObj(AfDecimal risPrestFaivlObj) {
        if (risPrestFaivlObj != null) {
            setRisPrestFaivl(new AfDecimal(risPrestFaivlObj, 15, 3));
            ws.getIndRisDiTrch().setRisPrestFaivl(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisPrestFaivl(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisRistorniCap() {
        return risDiTrch.getRstRisRistorniCap().getRstRisRistorniCap();
    }

    @Override
    public void setRisRistorniCap(AfDecimal risRistorniCap) {
        this.risDiTrch.getRstRisRistorniCap().setRstRisRistorniCap(risRistorniCap.copy());
    }

    @Override
    public AfDecimal getRisRistorniCapObj() {
        if (ws.getIndRisDiTrch().getRisRistorniCap() >= 0) {
            return getRisRistorniCap();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisRistorniCapObj(AfDecimal risRistorniCapObj) {
        if (risRistorniCapObj != null) {
            setRisRistorniCap(new AfDecimal(risRistorniCapObj, 15, 3));
            ws.getIndRisDiTrch().setRisRistorniCap(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisRistorniCap(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisRshDflt() {
        return risDiTrch.getRstRisRshDflt().getRstRisRshDflt();
    }

    @Override
    public void setRisRshDflt(AfDecimal risRshDflt) {
        this.risDiTrch.getRstRisRshDflt().setRstRisRshDflt(risRshDflt.copy());
    }

    @Override
    public AfDecimal getRisRshDfltObj() {
        if (ws.getIndRisDiTrch().getRisRshDflt() >= 0) {
            return getRisRshDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisRshDfltObj(AfDecimal risRshDfltObj) {
        if (risRshDfltObj != null) {
            setRisRshDflt(new AfDecimal(risRshDfltObj, 15, 3));
            ws.getIndRisDiTrch().setRisRshDflt(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisRshDflt(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisSopr() {
        return risDiTrch.getRstRisSopr().getRstRisSopr();
    }

    @Override
    public void setRisSopr(AfDecimal risSopr) {
        this.risDiTrch.getRstRisSopr().setRstRisSopr(risSopr.copy());
    }

    @Override
    public AfDecimal getRisSoprObj() {
        if (ws.getIndRisDiTrch().getRisSopr() >= 0) {
            return getRisSopr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisSoprObj(AfDecimal risSoprObj) {
        if (risSoprObj != null) {
            setRisSopr(new AfDecimal(risSoprObj, 15, 3));
            ws.getIndRisDiTrch().setRisSopr(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisSopr(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisSpe() {
        return risDiTrch.getRstRisSpe().getRstRisSpe();
    }

    @Override
    public void setRisSpe(AfDecimal risSpe) {
        this.risDiTrch.getRstRisSpe().setRstRisSpe(risSpe.copy());
    }

    @Override
    public AfDecimal getRisSpeFaivl() {
        return risDiTrch.getRstRisSpeFaivl().getRstRisSpeFaivl();
    }

    @Override
    public void setRisSpeFaivl(AfDecimal risSpeFaivl) {
        this.risDiTrch.getRstRisSpeFaivl().setRstRisSpeFaivl(risSpeFaivl.copy());
    }

    @Override
    public AfDecimal getRisSpeFaivlObj() {
        if (ws.getIndRisDiTrch().getRisSpeFaivl() >= 0) {
            return getRisSpeFaivl();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisSpeFaivlObj(AfDecimal risSpeFaivlObj) {
        if (risSpeFaivlObj != null) {
            setRisSpeFaivl(new AfDecimal(risSpeFaivlObj, 15, 3));
            ws.getIndRisDiTrch().setRisSpeFaivl(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisSpeFaivl(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisSpeObj() {
        if (ws.getIndRisDiTrch().getRisSpe() >= 0) {
            return getRisSpe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisSpeObj(AfDecimal risSpeObj) {
        if (risSpeObj != null) {
            setRisSpe(new AfDecimal(risSpeObj, 15, 3));
            ws.getIndRisDiTrch().setRisSpe(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisSpe(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisTot() {
        return risDiTrch.getRstRisTot().getRstRisTot();
    }

    @Override
    public void setRisTot(AfDecimal risTot) {
        this.risDiTrch.getRstRisTot().setRstRisTot(risTot.copy());
    }

    @Override
    public AfDecimal getRisTotObj() {
        if (ws.getIndRisDiTrch().getRisTot() >= 0) {
            return getRisTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisTotObj(AfDecimal risTotObj) {
        if (risTotObj != null) {
            setRisTot(new AfDecimal(risTotObj, 15, 3));
            ws.getIndRisDiTrch().setRisTot(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisTrmBns() {
        return risDiTrch.getRstRisTrmBns().getRstRisTrmBns();
    }

    @Override
    public void setRisTrmBns(AfDecimal risTrmBns) {
        this.risDiTrch.getRstRisTrmBns().setRstRisTrmBns(risTrmBns.copy());
    }

    @Override
    public AfDecimal getRisTrmBnsObj() {
        if (ws.getIndRisDiTrch().getRisTrmBns() >= 0) {
            return getRisTrmBns();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisTrmBnsObj(AfDecimal risTrmBnsObj) {
        if (risTrmBnsObj != null) {
            setRisTrmBns(new AfDecimal(risTrmBnsObj, 15, 3));
            ws.getIndRisDiTrch().setRisTrmBns(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisTrmBns(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisUti() {
        return risDiTrch.getRstRisUti().getRstRisUti();
    }

    @Override
    public void setRisUti(AfDecimal risUti) {
        this.risDiTrch.getRstRisUti().setRstRisUti(risUti.copy());
    }

    @Override
    public AfDecimal getRisUtiObj() {
        if (ws.getIndRisDiTrch().getRisUti() >= 0) {
            return getRisUti();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisUtiObj(AfDecimal risUtiObj) {
        if (risUtiObj != null) {
            setRisUti(new AfDecimal(risUtiObj, 15, 3));
            ws.getIndRisDiTrch().setRisUti(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisUti(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisZil() {
        return risDiTrch.getRstRisZil().getRstRisZil();
    }

    @Override
    public void setRisZil(AfDecimal risZil) {
        this.risDiTrch.getRstRisZil().setRstRisZil(risZil.copy());
    }

    @Override
    public AfDecimal getRisZilObj() {
        if (ws.getIndRisDiTrch().getRisZil() >= 0) {
            return getRisZil();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisZilObj(AfDecimal risZilObj) {
        if (risZilObj != null) {
            setRisZil(new AfDecimal(risZilObj, 15, 3));
            ws.getIndRisDiTrch().setRisZil(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRisZil(((short)-1));
        }
    }

    @Override
    public AfDecimal getRptoPre() {
        return risDiTrch.getRstRptoPre().getRstRptoPre();
    }

    @Override
    public void setRptoPre(AfDecimal rptoPre) {
        this.risDiTrch.getRstRptoPre().setRstRptoPre(rptoPre.copy());
    }

    @Override
    public AfDecimal getRptoPreObj() {
        if (ws.getIndRisDiTrch().getRptoPre() >= 0) {
            return getRptoPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRptoPreObj(AfDecimal rptoPreObj) {
        if (rptoPreObj != null) {
            setRptoPre(new AfDecimal(rptoPreObj, 15, 3));
            ws.getIndRisDiTrch().setRptoPre(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setRptoPre(((short)-1));
        }
    }

    @Override
    public long getRstDsRiga() {
        return risDiTrch.getRstDsRiga();
    }

    @Override
    public void setRstDsRiga(long rstDsRiga) {
        this.risDiTrch.setRstDsRiga(rstDsRiga);
    }

    @Override
    public String getTpCalcRis() {
        return risDiTrch.getRstTpCalcRis();
    }

    @Override
    public void setTpCalcRis(String tpCalcRis) {
        this.risDiTrch.setRstTpCalcRis(tpCalcRis);
    }

    @Override
    public String getTpCalcRisObj() {
        if (ws.getIndRisDiTrch().getTpCalcRis() >= 0) {
            return getTpCalcRis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCalcRisObj(String tpCalcRisObj) {
        if (tpCalcRisObj != null) {
            setTpCalcRis(tpCalcRisObj);
            ws.getIndRisDiTrch().setTpCalcRis(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setTpCalcRis(((short)-1));
        }
    }

    @Override
    public AfDecimal getUltCoeffAggRis() {
        return risDiTrch.getRstUltCoeffAggRis().getRstUltCoeffAggRis();
    }

    @Override
    public void setUltCoeffAggRis(AfDecimal ultCoeffAggRis) {
        this.risDiTrch.getRstUltCoeffAggRis().setRstUltCoeffAggRis(ultCoeffAggRis.copy());
    }

    @Override
    public AfDecimal getUltCoeffAggRisObj() {
        if (ws.getIndRisDiTrch().getUltCoeffAggRis() >= 0) {
            return getUltCoeffAggRis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUltCoeffAggRisObj(AfDecimal ultCoeffAggRisObj) {
        if (ultCoeffAggRisObj != null) {
            setUltCoeffAggRis(new AfDecimal(ultCoeffAggRisObj, 14, 9));
            ws.getIndRisDiTrch().setUltCoeffAggRis(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setUltCoeffAggRis(((short)-1));
        }
    }

    @Override
    public AfDecimal getUltCoeffRis() {
        return risDiTrch.getRstUltCoeffRis().getRstUltCoeffRis();
    }

    @Override
    public void setUltCoeffRis(AfDecimal ultCoeffRis) {
        this.risDiTrch.getRstUltCoeffRis().setRstUltCoeffRis(ultCoeffRis.copy());
    }

    @Override
    public AfDecimal getUltCoeffRisObj() {
        if (ws.getIndRisDiTrch().getUltCoeffRis() >= 0) {
            return getUltCoeffRis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUltCoeffRisObj(AfDecimal ultCoeffRisObj) {
        if (ultCoeffRisObj != null) {
            setUltCoeffRis(new AfDecimal(ultCoeffRisObj, 14, 9));
            ws.getIndRisDiTrch().setUltCoeffRis(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setUltCoeffRis(((short)-1));
        }
    }

    @Override
    public AfDecimal getUltRm() {
        return risDiTrch.getRstUltRm().getRstUltRm();
    }

    @Override
    public void setUltRm(AfDecimal ultRm) {
        this.risDiTrch.getRstUltRm().setRstUltRm(ultRm.copy());
    }

    @Override
    public AfDecimal getUltRmObj() {
        if (ws.getIndRisDiTrch().getUltRm() >= 0) {
            return getUltRm();
        }
        else {
            return null;
        }
    }

    @Override
    public void setUltRmObj(AfDecimal ultRmObj) {
        if (ultRmObj != null) {
            setUltRm(new AfDecimal(ultRmObj, 15, 3));
            ws.getIndRisDiTrch().setUltRm(((short)0));
        }
        else {
            ws.getIndRisDiTrch().setUltRm(((short)-1));
        }
    }
}
