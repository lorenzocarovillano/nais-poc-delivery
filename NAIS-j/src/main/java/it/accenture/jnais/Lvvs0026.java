package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.ParamDiCalcLdbs1650;

/**Original name: LVVS0026<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 *  >>2107 CORRETTA MAPPATURA DELL'OUTPUT ALLA VARIABILE
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0026
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... MODULO DI CALCOLO
 *   DESCRIZIONE.... Accesso su Param_Di_Calc (LDBS1650) e
 *                   ritorno in Output del campo Val_Pc
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0026 extends Program {

    //==== PROPERTIES ====
    //Original name: WK-PGM-CALLED
    private String wkPgmCalled = "LDBS1650";
    /**Original name: WK-TIPO-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *     COSTANTI
	 * ----------------------------------------------------------------*</pre>*/
    private String wkTipoTranche = "TG";
    //Original name: PARAM-DI-CALC
    private ParamDiCalcLdbs1650 paramDiCalc = new ParamDiCalcLdbs1650();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0026
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0026_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM L000-OPERAZIONI-INIZIALI
        //              THRU L000-EX.
        l000OperazioniIniziali();
        // COB_CODE: PERFORM L100-ELABORAZIONE
        //              THRU L100-EX.
        l100Elaborazione();
        // COB_CODE: PERFORM L900-OPERAZIONI-FINALI
        //              THRU L900-EX.
        l900OperazioniFinali();
        return 0;
    }

    public static Lvvs0026 getInstance() {
        return ((Lvvs0026)Programs.getInstance(Lvvs0026.class));
    }

    /**Original name: L000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void l000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IVVC0213-TAB-OUTPUT.
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE      TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: L100-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void l100Elaborazione() {
        // COB_CODE: PERFORM L500-PREPARA-CALL-LDBS1650 THRU L500-EX.
        l500PreparaCallLdbs1650();
        // COB_CODE: PERFORM L700-CALL-LDBS1650         THRU L700-EX.
        l700CallLdbs1650();
    }

    /**Original name: L500-PREPARA-CALL-LDBS1650<br>
	 * <pre>----------------------------------------------------------------*
	 *   PRAPARA CALL LDBS1650
	 * ----------------------------------------------------------------*</pre>*/
    private void l500PreparaCallLdbs1650() {
        // COB_CODE: SET IDSV0003-SELECT           TO TRUE
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION
        //                                         TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA
        //                                         TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: MOVE IVVC0213-ID-LIVELLO      TO PCA-ID-OGG
        paramDiCalc.getPcaIdOgg().setPcaIdOgg(ivvc0213.getDatiLivello().getIdLivello());
        // COB_CODE: MOVE WK-TIPO-TRANCHE          TO PCA-TP-OGG
        paramDiCalc.setPcaTpOgg(wkTipoTranche);
        // COB_CODE: MOVE IVVC0213-COD-VARIABILE   TO PCA-COD-PARAM.
        paramDiCalc.setPcaCodParam(ivvc0213.getDatiLivello().getCodVariabile());
    }

    /**Original name: L700-CALL-LDBS1650<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALL LDBS1650
	 * ----------------------------------------------------------------*</pre>*/
    private void l700CallLdbs1650() {
        Ldbs1650 ldbs1650 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE:      CALL WK-PGM-CALLED  USING  IDSV0003 PARAM-DI-CALC
        //           *
        //                ON EXCEPTION
        //                     SET IDSV0003-INVALID-OPER  TO TRUE
        //                END-CALL.
        try {
            ldbs1650 = Ldbs1650.getInstance();
            ldbs1650.run(idsv0003, paramDiCalc);
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM-CALLED
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(wkPgmCalled);
            // COB_CODE: MOVE 'CALL LDBS1650 ERRORE CHIAMATA'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL LDBS1650 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF PCA-VAL-PC IS NUMERIC
            //              END-IF
            //           END-IF
            if (Functions.isNumber(paramDiCalc.getPcaValPc().getPcaValPc())) {
                // COB_CODE: IF PCA-VAL-PC GREATER ZERO
                //              MOVE PCA-VAL-PC        TO IVVC0213-VAL-PERC-O
                //           END-IF
                if (paramDiCalc.getPcaValPc().getPcaValPc().compareTo(0) > 0) {
                    // COB_CODE: MOVE PCA-VAL-PC        TO IVVC0213-VAL-PERC-O
                    ivvc0213.getTabOutput().setValPercO(TruncAbs.toDecimal(paramDiCalc.getPcaValPc().getPcaValPc(), 14, 9));
                }
            }
            // COB_CODE: IF PCA-VAL-IMP IS NUMERIC
            //              END-IF
            //           END-IF
            if (Functions.isNumber(paramDiCalc.getPcaValImp().getPcaValImp())) {
                // COB_CODE: IF PCA-VAL-IMP GREATER ZERO
                //              MOVE PCA-VAL-IMP       TO IVVC0213-VAL-PERC-O
                //           END-IF
                if (paramDiCalc.getPcaValImp().getPcaValImp().compareTo(0) > 0) {
                    // COB_CODE: MOVE PCA-VAL-IMP       TO IVVC0213-VAL-PERC-O
                    ivvc0213.getTabOutput().setValPercO(TruncAbs.toDecimal(paramDiCalc.getPcaValImp().getPcaValImp(), 14, 9));
                }
            }
            // COB_CODE: IF PCA-VAL-TS IS NUMERIC
            //              END-IF
            //           END-IF
            if (Functions.isNumber(paramDiCalc.getPcaValTs().getPcaValTs())) {
                // COB_CODE: IF PCA-VAL-TS GREATER ZERO
                //              MOVE PCA-VAL-TS        TO IVVC0213-VAL-IMP-O
                //           END-IF
                if (paramDiCalc.getPcaValTs().getPcaValTs().compareTo(0) > 0) {
                    // COB_CODE: MOVE PCA-VAL-TS        TO IVVC0213-VAL-PERC-O
                    ivvc0213.getTabOutput().setValPercO(TruncAbs.toDecimal(paramDiCalc.getPcaValTs().getPcaValTs(), 14, 9));
                    // COB_CODE: MOVE PCA-VAL-TS        TO IVVC0213-VAL-IMP-O
                    ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(paramDiCalc.getPcaValTs().getPcaValTs(), 18, 7));
                }
            }
            // COB_CODE: IF PCA-VAL-NUM IS NUMERIC
            //              END-IF
            //           END-IF
            if (Functions.isNumber(paramDiCalc.getPcaValNum().getPcaValNum())) {
                // COB_CODE: IF PCA-VAL-NUM GREATER ZERO
                //              MOVE PCA-VAL-NUM       TO IVVC0213-VAL-PERC-O
                //           END-IF
                if (paramDiCalc.getPcaValNum().getPcaValNum() > 0) {
                    // COB_CODE: MOVE PCA-VAL-NUM       TO IVVC0213-VAL-PERC-O
                    ivvc0213.getTabOutput().setValPercO(TruncAbs.toDecimal(paramDiCalc.getPcaValNum().getPcaValNum(), 14, 9));
                }
            }
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           ELSE
            //              END-STRING
            //           END-IF
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM-CALLED          TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(wkPgmCalled);
            // COB_CODE: STRING 'CHIAMATA LDBS1650 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS1650 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: L900-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void l900OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public String getWkPgmCalled() {
        return this.wkPgmCalled;
    }

    public String getWkTipoTranche() {
        return this.wkTipoTranche;
    }
}
