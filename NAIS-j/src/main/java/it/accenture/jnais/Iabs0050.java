package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcBatchTypeDao;
import it.accenture.jnais.commons.data.to.IBtcBatchType;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.IndLogErrore;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcBatchType;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.BbtCommitFrequency;
import it.accenture.jnais.ws.redefines.BbtTpMovi;

/**Original name: IABS0050<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE
 *  F A S E         : GESTIONE TABELLA BTC_BATCH_TYPE
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0050 extends Program implements IBtcBatchType {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcBatchTypeDao btcBatchTypeDao = new BtcBatchTypeDao(dbAccessStatus);
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IND-BTC-BATCH-TYPE
    private IndLogErrore indBtcBatchType = new IndLogErrore();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BTC-BATCH-TYPE
    private BtcBatchType btcBatchType;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0050_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BtcBatchType btcBatchType) {
        this.idsv0003 = idsv0003;
        this.btcBatchType = btcBatchType;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: PERFORM A300-ELABORA             THRU A300-EX
        a300Elabora();
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0050 getInstance() {
        return ((Iabs0050)Programs.getInstance(Iabs0050.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0050'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0050");
        // COB_CODE: MOVE 'BTC_BATCH_TYPE'        TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_BATCH_TYPE");
        // COB_CODE: MOVE '00'                    TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                  TO   IDSV0003-SQLCODE
        //                                             IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                  TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                             IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(descrizErrDb2);
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A300-ELABORA<br>
	 * <pre>*****************************************************************</pre>*/
    private void a300Elabora() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                 PERFORM A301-ELABORA-PK             THRU A301-EX
        //              WHEN IDSV0003-WHERE-CONDITION
        //                 PERFORM A302-ELABORA-WC             THRU A302-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE.
        switch (idsv0003.getLivelloOperazione().getLivelloOperazione()) {

            case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A301-ELABORA-PK             THRU A301-EX
                a301ElaboraPk();
                break;

            case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A302-ELABORA-WC             THRU A302-EX
                a302ElaboraWc();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                idsv0003.getReturnCode().setInvalidLevelOper();
                break;
        }
    }

    /**Original name: A301-ELABORA-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a301ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-PK             THRU A310-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-PK             THRU A310-EX
            a310SelectPk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A302-ELABORA-WC<br>
	 * <pre>*****************************************************************</pre>*/
    private void a302ElaboraWc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM W000-SELECT-WC             THRU W000-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM W000-SELECT-WC             THRU W000-EX
            w000SelectWc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A310-SELECT-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a310SelectPk() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                COD_BATCH_TYPE
        //                ,DES
        //                ,DEF_CONTENT_TYPE
        //                ,COMMIT_FREQUENCY
        //                ,SERVICE_NAME
        //                ,SERVICE_GUIDE_NAME
        //                ,SERVICE_ALPO_NAME
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //             INTO
        //                :BBT-COD-BATCH-TYPE
        //               ,:BBT-DES-VCHAR
        //               ,:BBT-DEF-CONTENT-TYPE
        //                :IND-BBT-DEF-CONTENT-TYPE
        //               ,:BBT-COMMIT-FREQUENCY
        //                :IND-BBT-COMMIT-FREQUENCY
        //               ,:BBT-SERVICE-NAME
        //                :IND-BBT-SERVICE-NAME
        //               ,:BBT-SERVICE-GUIDE-NAME
        //                :IND-BBT-SERVICE-GUIDE-NAME
        //               ,:BBT-SERVICE-ALPO-NAME
        //                :IND-BBT-SERVICE-ALPO-NAME
        //               ,:BBT-COD-MACROFUNCT
        //                :IND-BBT-COD-MACROFUNCT
        //               ,:BBT-TP-MOVI
        //                :IND-BBT-TP-MOVI
        //             FROM  BTC_BATCH_TYPE
        //             WHERE COD_BATCH_TYPE = :BBT-COD-BATCH-TYPE
        //           END-EXEC.
        btcBatchTypeDao.selectByBbtCodBatchType(btcBatchType.getBbtCodBatchType(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
    }

    /**Original name: W000-SELECT-WC<br>
	 * <pre>*****************************************************************</pre>*/
    private void w000SelectWc() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                COD_BATCH_TYPE
        //                ,DES
        //                ,DEF_CONTENT_TYPE
        //                ,COMMIT_FREQUENCY
        //                ,SERVICE_NAME
        //                ,SERVICE_GUIDE_NAME
        //                ,SERVICE_ALPO_NAME
        //                ,COD_MACROFUNCT
        //                ,TP_MOVI
        //             INTO
        //                :BBT-COD-BATCH-TYPE
        //               ,:BBT-DES-VCHAR
        //               ,:BBT-DEF-CONTENT-TYPE
        //                :IND-BBT-DEF-CONTENT-TYPE
        //               ,:BBT-COMMIT-FREQUENCY
        //                :IND-BBT-COMMIT-FREQUENCY
        //               ,:BBT-SERVICE-NAME
        //                :IND-BBT-SERVICE-NAME
        //               ,:BBT-SERVICE-GUIDE-NAME
        //                :IND-BBT-SERVICE-GUIDE-NAME
        //               ,:BBT-SERVICE-ALPO-NAME
        //                :IND-BBT-SERVICE-ALPO-NAME
        //               ,:BBT-COD-MACROFUNCT
        //                :IND-BBT-COD-MACROFUNCT
        //               ,:BBT-TP-MOVI
        //                :IND-BBT-TP-MOVI
        //             FROM  BTC_BATCH_TYPE
        //             WHERE COD_MACROFUNCT   = :BBT-COD-MACROFUNCT
        //                                      :IND-BBT-COD-MACROFUNCT AND
        //                   TP_MOVI          = :BBT-TP-MOVI
        //                                      :IND-BBT-TP-MOVI        AND
        //                   SUBSTR(DEF_CONTENT_TYPE, 1, 2)
        //                                    = :BBT-DEF-CONTENT-TYPE
        //           END-EXEC.
        btcBatchTypeDao.selectRec(btcBatchType.getBbtCodMacrofunct(), ((Integer)btcBatchType.getBbtTpMovi().getBbtTpMovi()), btcBatchType.getBbtDefContentType(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: IF IND-BBT-DEF-CONTENT-TYPE = -1
        //              MOVE HIGH-VALUES TO BBT-DEF-CONTENT-TYPE-NULL
        //           END-IF
        if (indBtcBatchType.getLabelErr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BBT-DEF-CONTENT-TYPE-NULL
            btcBatchType.setBbtDefContentType(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_DEF_CONTENT_TYPE));
        }
        // COB_CODE: IF IND-BBT-COMMIT-FREQUENCY = -1
        //              MOVE HIGH-VALUES TO BBT-COMMIT-FREQUENCY-NULL
        //           END-IF
        if (indBtcBatchType.getOperTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BBT-COMMIT-FREQUENCY-NULL
            btcBatchType.getBbtCommitFrequency().setBbtCommitFrequencyNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BbtCommitFrequency.Len.BBT_COMMIT_FREQUENCY_NULL));
        }
        // COB_CODE: IF IND-BBT-SERVICE-NAME = -1
        //              MOVE HIGH-VALUES TO BBT-SERVICE-NAME-NULL
        //           END-IF
        if (indBtcBatchType.getNomeTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BBT-SERVICE-NAME-NULL
            btcBatchType.setBbtServiceName(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_SERVICE_NAME));
        }
        // COB_CODE: IF IND-BBT-SERVICE-GUIDE-NAME = -1
        //              MOVE HIGH-VALUES TO BBT-SERVICE-GUIDE-NAME-NULL
        //           END-IF
        if (indBtcBatchType.getStatusTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BBT-SERVICE-GUIDE-NAME-NULL
            btcBatchType.setBbtServiceGuideName(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_SERVICE_GUIDE_NAME));
        }
        // COB_CODE: IF IND-BBT-SERVICE-ALPO-NAME = -1
        //              MOVE HIGH-VALUES TO BBT-SERVICE-ALPO-NAME-NULL
        //           END-IF
        if (indBtcBatchType.getKeyTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BBT-SERVICE-ALPO-NAME-NULL
            btcBatchType.setBbtServiceAlpoName(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_SERVICE_ALPO_NAME));
        }
        // COB_CODE: IF IND-BBT-COD-MACROFUNCT = -1
        //              MOVE HIGH-VALUES TO BBT-COD-MACROFUNCT-NULL
        //           END-IF
        if (indBtcBatchType.getTipoOggetto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BBT-COD-MACROFUNCT-NULL
            btcBatchType.setBbtCodMacrofunct(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BtcBatchType.Len.BBT_COD_MACROFUNCT));
        }
        // COB_CODE: IF IND-BBT-TP-MOVI = -1
        //              MOVE HIGH-VALUES TO BBT-TP-MOVI-NULL
        //           END-IF.
        if (indBtcBatchType.getIbOggetto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BBT-TP-MOVI-NULL
            btcBatchType.getBbtTpMovi().setBbtTpMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BbtTpMovi.Len.BBT_TP_MOVI_NULL));
        }
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BBT-DEF-CONTENT-TYPE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BBT-DEF-CONTENT-TYPE
        //           ELSE
        //              MOVE 0 TO IND-BBT-DEF-CONTENT-TYPE
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatchType.getBbtDefContentType(), BtcBatchType.Len.BBT_DEF_CONTENT_TYPE)) {
            // COB_CODE: MOVE -1 TO IND-BBT-DEF-CONTENT-TYPE
            indBtcBatchType.setLabelErr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BBT-DEF-CONTENT-TYPE
            indBtcBatchType.setLabelErr(((short)0));
        }
        // COB_CODE: IF BBT-COMMIT-FREQUENCY-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BBT-COMMIT-FREQUENCY
        //           ELSE
        //              MOVE 0 TO IND-BBT-COMMIT-FREQUENCY
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatchType.getBbtCommitFrequency().getBbtCommitFrequencyNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BBT-COMMIT-FREQUENCY
            indBtcBatchType.setOperTabella(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BBT-COMMIT-FREQUENCY
            indBtcBatchType.setOperTabella(((short)0));
        }
        // COB_CODE: IF BBT-SERVICE-NAME-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BBT-SERVICE-NAME
        //           ELSE
        //              MOVE 0 TO IND-BBT-SERVICE-NAME
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatchType.getBbtServiceName(), BtcBatchType.Len.BBT_SERVICE_NAME)) {
            // COB_CODE: MOVE -1 TO IND-BBT-SERVICE-NAME
            indBtcBatchType.setNomeTabella(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BBT-SERVICE-NAME
            indBtcBatchType.setNomeTabella(((short)0));
        }
        // COB_CODE: IF BBT-SERVICE-GUIDE-NAME-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BBT-SERVICE-GUIDE-NAME
        //           ELSE
        //              MOVE 0 TO IND-BBT-SERVICE-GUIDE-NAME
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatchType.getBbtServiceGuideName(), BtcBatchType.Len.BBT_SERVICE_GUIDE_NAME)) {
            // COB_CODE: MOVE -1 TO IND-BBT-SERVICE-GUIDE-NAME
            indBtcBatchType.setStatusTabella(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BBT-SERVICE-GUIDE-NAME
            indBtcBatchType.setStatusTabella(((short)0));
        }
        // COB_CODE: IF BBT-SERVICE-ALPO-NAME-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BBT-SERVICE-ALPO-NAME
        //           ELSE
        //              MOVE 0 TO IND-BBT-SERVICE-ALPO-NAME
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatchType.getBbtServiceAlpoName(), BtcBatchType.Len.BBT_SERVICE_ALPO_NAME)) {
            // COB_CODE: MOVE -1 TO IND-BBT-SERVICE-ALPO-NAME
            indBtcBatchType.setKeyTabella(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BBT-SERVICE-ALPO-NAME
            indBtcBatchType.setKeyTabella(((short)0));
        }
        // COB_CODE: IF BBT-COD-MACROFUNCT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BBT-COD-MACROFUNCT
        //           ELSE
        //              MOVE 0 TO IND-BBT-COD-MACROFUNCT
        //           END-IF
        if (Characters.EQ_HIGH.test(btcBatchType.getBbtCodMacrofunctFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BBT-COD-MACROFUNCT
            indBtcBatchType.setTipoOggetto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BBT-COD-MACROFUNCT
            indBtcBatchType.setTipoOggetto(((short)0));
        }
        // COB_CODE: IF BBT-TP-MOVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BBT-TP-MOVI
        //           ELSE
        //              MOVE 0 TO IND-BBT-TP-MOVI
        //           END-IF.
        if (Characters.EQ_HIGH.test(btcBatchType.getBbtTpMovi().getBbtTpMoviNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BBT-TP-MOVI
            indBtcBatchType.setIbOggetto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BBT-TP-MOVI
            indBtcBatchType.setIbOggetto(((short)0));
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            idsv0010.setWsDataInizioEffettoDb(idsv0010.getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                idsv0010.setWsDataFineEffettoDb(idsv0010.getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            idsv0010.setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            idsv0010.setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    @Override
    public int getBbtCodBatchType() {
        return btcBatchType.getBbtCodBatchType();
    }

    @Override
    public void setBbtCodBatchType(int bbtCodBatchType) {
        this.btcBatchType.setBbtCodBatchType(bbtCodBatchType);
    }

    @Override
    public String getCodMacrofunct() {
        return btcBatchType.getBbtCodMacrofunct();
    }

    @Override
    public void setCodMacrofunct(String codMacrofunct) {
        this.btcBatchType.setBbtCodMacrofunct(codMacrofunct);
    }

    @Override
    public String getCodMacrofunctObj() {
        if (indBtcBatchType.getTipoOggetto() >= 0) {
            return getCodMacrofunct();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodMacrofunctObj(String codMacrofunctObj) {
        if (codMacrofunctObj != null) {
            setCodMacrofunct(codMacrofunctObj);
            indBtcBatchType.setTipoOggetto(((short)0));
        }
        else {
            indBtcBatchType.setTipoOggetto(((short)-1));
        }
    }

    @Override
    public int getCommitFrequency() {
        return btcBatchType.getBbtCommitFrequency().getBbtCommitFrequency();
    }

    @Override
    public void setCommitFrequency(int commitFrequency) {
        this.btcBatchType.getBbtCommitFrequency().setBbtCommitFrequency(commitFrequency);
    }

    @Override
    public Integer getCommitFrequencyObj() {
        if (indBtcBatchType.getOperTabella() >= 0) {
            return ((Integer)getCommitFrequency());
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommitFrequencyObj(Integer commitFrequencyObj) {
        if (commitFrequencyObj != null) {
            setCommitFrequency(((int)commitFrequencyObj));
            indBtcBatchType.setOperTabella(((short)0));
        }
        else {
            indBtcBatchType.setOperTabella(((short)-1));
        }
    }

    @Override
    public String getDefContentType() {
        return btcBatchType.getBbtDefContentType();
    }

    @Override
    public void setDefContentType(String defContentType) {
        this.btcBatchType.setBbtDefContentType(defContentType);
    }

    @Override
    public String getDefContentTypeObj() {
        if (indBtcBatchType.getLabelErr() >= 0) {
            return getDefContentType();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDefContentTypeObj(String defContentTypeObj) {
        if (defContentTypeObj != null) {
            setDefContentType(defContentTypeObj);
            indBtcBatchType.setLabelErr(((short)0));
        }
        else {
            indBtcBatchType.setLabelErr(((short)-1));
        }
    }

    @Override
    public String getDesVchar() {
        return btcBatchType.getBbtDesVcharFormatted();
    }

    @Override
    public void setDesVchar(String desVchar) {
        this.btcBatchType.setBbtDesVcharFormatted(desVchar);
    }

    @Override
    public String getServiceAlpoName() {
        return btcBatchType.getBbtServiceAlpoName();
    }

    @Override
    public void setServiceAlpoName(String serviceAlpoName) {
        this.btcBatchType.setBbtServiceAlpoName(serviceAlpoName);
    }

    @Override
    public String getServiceAlpoNameObj() {
        if (indBtcBatchType.getKeyTabella() >= 0) {
            return getServiceAlpoName();
        }
        else {
            return null;
        }
    }

    @Override
    public void setServiceAlpoNameObj(String serviceAlpoNameObj) {
        if (serviceAlpoNameObj != null) {
            setServiceAlpoName(serviceAlpoNameObj);
            indBtcBatchType.setKeyTabella(((short)0));
        }
        else {
            indBtcBatchType.setKeyTabella(((short)-1));
        }
    }

    @Override
    public String getServiceGuideName() {
        return btcBatchType.getBbtServiceGuideName();
    }

    @Override
    public void setServiceGuideName(String serviceGuideName) {
        this.btcBatchType.setBbtServiceGuideName(serviceGuideName);
    }

    @Override
    public String getServiceGuideNameObj() {
        if (indBtcBatchType.getStatusTabella() >= 0) {
            return getServiceGuideName();
        }
        else {
            return null;
        }
    }

    @Override
    public void setServiceGuideNameObj(String serviceGuideNameObj) {
        if (serviceGuideNameObj != null) {
            setServiceGuideName(serviceGuideNameObj);
            indBtcBatchType.setStatusTabella(((short)0));
        }
        else {
            indBtcBatchType.setStatusTabella(((short)-1));
        }
    }

    @Override
    public String getServiceName() {
        return btcBatchType.getBbtServiceName();
    }

    @Override
    public void setServiceName(String serviceName) {
        this.btcBatchType.setBbtServiceName(serviceName);
    }

    @Override
    public String getServiceNameObj() {
        if (indBtcBatchType.getNomeTabella() >= 0) {
            return getServiceName();
        }
        else {
            return null;
        }
    }

    @Override
    public void setServiceNameObj(String serviceNameObj) {
        if (serviceNameObj != null) {
            setServiceName(serviceNameObj);
            indBtcBatchType.setNomeTabella(((short)0));
        }
        else {
            indBtcBatchType.setNomeTabella(((short)-1));
        }
    }

    @Override
    public int getTpMovi() {
        return btcBatchType.getBbtTpMovi().getBbtTpMovi();
    }

    @Override
    public void setTpMovi(int tpMovi) {
        this.btcBatchType.getBbtTpMovi().setBbtTpMovi(tpMovi);
    }

    @Override
    public Integer getTpMoviObj() {
        if (indBtcBatchType.getIbOggetto() >= 0) {
            return ((Integer)getTpMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMoviObj(Integer tpMoviObj) {
        if (tpMoviObj != null) {
            setTpMovi(((int)tpMoviObj));
            indBtcBatchType.setIbOggetto(((short)0));
        }
        else {
            indBtcBatchType.setIbOggetto(((short)-1));
        }
    }
}
