package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.StatRichEstDao;
import it.accenture.jnais.commons.data.to.IStatRichEst;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs8170Data;
import it.accenture.jnais.ws.redefines.P04IdMoviCrz;
import it.accenture.jnais.ws.StatRichEstIdbsp040;

/**Original name: LDBS8170<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  23 FEB 2011.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs8170 extends Program implements IStatRichEst {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private StatRichEstDao statRichEstDao = new StatRichEstDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs8170Data ws = new Ldbs8170Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: STAT-RICH-EST
    private StatRichEstIdbsp040 statRichEst;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS8170_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, StatRichEstIdbsp040 statRichEst) {
        this.idsv0003 = idsv0003;
        this.statRichEst = statRichEst;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs8170 getInstance() {
        return ((Ldbs8170)Programs.getInstance(Ldbs8170.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS8170'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS8170");
        // COB_CODE: MOVE 'STAT-RICH-EST' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("STAT-RICH-EST");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-NST CURSOR FOR
        //              SELECT
        //                     ID_STAT_RICH_EST
        //                    ,ID_RICH_EST
        //                    ,TS_INI_VLDT
        //                    ,TS_END_VLDT
        //                    ,COD_COMP_ANIA
        //                    ,COD_PRCS
        //                    ,COD_ATTVT
        //                    ,STAT_RICH_EST
        //                    ,FL_STAT_END
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_CAUS_SCARTO
        //                    ,DESC_ERR
        //                    ,UTENTE_INS_AGG
        //                    ,COD_ERR_SCARTO
        //                    ,ID_MOVI_CRZ
        //              FROM STAT_RICH_EST
        //              WHERE      ID_RICH_EST = :P04-ID-RICH-EST
        //                        AND TS_INI_VLDT <= :WS-TS-COMPETENZA
        //                        AND TS_END_VLDT >  :WS-TS-COMPETENZA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_STAT_RICH_EST
        //                    ,ID_RICH_EST
        //                    ,TS_INI_VLDT
        //                    ,TS_END_VLDT
        //                    ,COD_COMP_ANIA
        //                    ,COD_PRCS
        //                    ,COD_ATTVT
        //                    ,STAT_RICH_EST
        //                    ,FL_STAT_END
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,TP_CAUS_SCARTO
        //                    ,DESC_ERR
        //                    ,UTENTE_INS_AGG
        //                    ,COD_ERR_SCARTO
        //                    ,ID_MOVI_CRZ
        //             INTO
        //                :P04-ID-STAT-RICH-EST
        //               ,:P04-ID-RICH-EST
        //               ,:P04-TS-INI-VLDT
        //               ,:P04-TS-END-VLDT
        //               ,:P04-COD-COMP-ANIA
        //               ,:P04-COD-PRCS
        //               ,:P04-COD-ATTVT
        //               ,:P04-STAT-RICH-EST
        //               ,:P04-FL-STAT-END
        //                :IND-P04-FL-STAT-END
        //               ,:P04-DS-OPER-SQL
        //               ,:P04-DS-VER
        //               ,:P04-DS-TS-CPTZ
        //               ,:P04-DS-UTENTE
        //               ,:P04-DS-STATO-ELAB
        //               ,:P04-TP-CAUS-SCARTO
        //                :IND-P04-TP-CAUS-SCARTO
        //               ,:P04-DESC-ERR-VCHAR
        //                :IND-P04-DESC-ERR
        //               ,:P04-UTENTE-INS-AGG
        //               ,:P04-COD-ERR-SCARTO
        //                :IND-P04-COD-ERR-SCARTO
        //               ,:P04-ID-MOVI-CRZ
        //                :IND-P04-ID-MOVI-CRZ
        //             FROM STAT_RICH_EST
        //             WHERE      ID_RICH_EST = :P04-ID-RICH-EST
        //                    AND TS_INI_VLDT <= :WS-TS-COMPETENZA
        //                    AND TS_END_VLDT >  :WS-TS-COMPETENZA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        statRichEstDao.selectRec(statRichEst.getP04IdRichEst(), ws.getIdsv0010().getWsTsCompetenza(), idsv0003.getCodiceCompagniaAnia(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: EXEC SQL
        //                OPEN C-NST
        //           END-EXEC.
        statRichEstDao.openCNst19(statRichEst.getP04IdRichEst(), ws.getIdsv0010().getWsTsCompetenza(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-NST
        //           END-EXEC.
        statRichEstDao.closeCNst19();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: EXEC SQL
        //                FETCH C-NST
        //           INTO
        //                :P04-ID-STAT-RICH-EST
        //               ,:P04-ID-RICH-EST
        //               ,:P04-TS-INI-VLDT
        //               ,:P04-TS-END-VLDT
        //               ,:P04-COD-COMP-ANIA
        //               ,:P04-COD-PRCS
        //               ,:P04-COD-ATTVT
        //               ,:P04-STAT-RICH-EST
        //               ,:P04-FL-STAT-END
        //                :IND-P04-FL-STAT-END
        //               ,:P04-DS-OPER-SQL
        //               ,:P04-DS-VER
        //               ,:P04-DS-TS-CPTZ
        //               ,:P04-DS-UTENTE
        //               ,:P04-DS-STATO-ELAB
        //               ,:P04-TP-CAUS-SCARTO
        //                :IND-P04-TP-CAUS-SCARTO
        //               ,:P04-DESC-ERR-VCHAR
        //                :IND-P04-DESC-ERR
        //               ,:P04-UTENTE-INS-AGG
        //               ,:P04-COD-ERR-SCARTO
        //                :IND-P04-COD-ERR-SCARTO
        //               ,:P04-ID-MOVI-CRZ
        //                :IND-P04-ID-MOVI-CRZ
        //           END-EXEC.
        statRichEstDao.fetchCNst19(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST THRU C270-EX
            c270CloseCursorWcNst();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-P04-FL-STAT-END = -1
        //              MOVE HIGH-VALUES TO P04-FL-STAT-END-NULL
        //           END-IF
        if (ws.getIndStatRichEst().getValQuo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P04-FL-STAT-END-NULL
            statRichEst.setP04FlStatEnd(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-P04-TP-CAUS-SCARTO = -1
        //              MOVE HIGH-VALUES TO P04-TP-CAUS-SCARTO-NULL
        //           END-IF
        if (ws.getIndStatRichEst().getValQuoManfee() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P04-TP-CAUS-SCARTO-NULL
            statRichEst.setP04TpCausScarto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StatRichEstIdbsp040.Len.P04_TP_CAUS_SCARTO));
        }
        // COB_CODE: IF IND-P04-DESC-ERR = -1
        //              MOVE HIGH-VALUES TO P04-DESC-ERR
        //           END-IF
        if (ws.getIndStatRichEst().getDtRilevazioneNav() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P04-DESC-ERR
            statRichEst.setP04DescErr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StatRichEstIdbsp040.Len.P04_DESC_ERR));
        }
        // COB_CODE: IF IND-P04-COD-ERR-SCARTO = -1
        //              MOVE HIGH-VALUES TO P04-COD-ERR-SCARTO-NULL
        //           END-IF
        if (ws.getIndStatRichEst().getValQuoAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P04-COD-ERR-SCARTO-NULL
            statRichEst.setP04CodErrScarto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StatRichEstIdbsp040.Len.P04_COD_ERR_SCARTO));
        }
        // COB_CODE: IF IND-P04-ID-MOVI-CRZ = -1
        //              MOVE HIGH-VALUES TO P04-ID-MOVI-CRZ-NULL
        //           END-IF.
        if (ws.getIndStatRichEst().getFlNoNav() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P04-ID-MOVI-CRZ-NULL
            statRichEst.getP04IdMoviCrz().setP04IdMoviCrzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P04IdMoviCrz.Len.P04_ID_MOVI_CRZ_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF P04-DESC-ERR
        //                       TO P04-DESC-ERR-LEN.
        statRichEst.setP04DescErrLen(((short)StatRichEstIdbsp040.Len.P04_DESC_ERR));
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    @Override
    public String getCodAttvt() {
        return statRichEst.getP04CodAttvt();
    }

    @Override
    public void setCodAttvt(String codAttvt) {
        this.statRichEst.setP04CodAttvt(codAttvt);
    }

    @Override
    public int getCodCompAnia() {
        return statRichEst.getP04CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.statRichEst.setP04CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodErrScarto() {
        return statRichEst.getP04CodErrScarto();
    }

    @Override
    public void setCodErrScarto(String codErrScarto) {
        this.statRichEst.setP04CodErrScarto(codErrScarto);
    }

    @Override
    public String getCodErrScartoObj() {
        if (ws.getIndStatRichEst().getValQuoAcq() >= 0) {
            return getCodErrScarto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodErrScartoObj(String codErrScartoObj) {
        if (codErrScartoObj != null) {
            setCodErrScarto(codErrScartoObj);
            ws.getIndStatRichEst().setValQuoAcq(((short)0));
        }
        else {
            ws.getIndStatRichEst().setValQuoAcq(((short)-1));
        }
    }

    @Override
    public String getCodPrcs() {
        return statRichEst.getP04CodPrcs();
    }

    @Override
    public void setCodPrcs(String codPrcs) {
        this.statRichEst.setP04CodPrcs(codPrcs);
    }

    @Override
    public String getDescErrVchar() {
        return statRichEst.getP04DescErrVcharFormatted();
    }

    @Override
    public void setDescErrVchar(String descErrVchar) {
        this.statRichEst.setP04DescErrVcharFormatted(descErrVchar);
    }

    @Override
    public String getDescErrVcharObj() {
        if (ws.getIndStatRichEst().getDtRilevazioneNav() >= 0) {
            return getDescErrVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescErrVcharObj(String descErrVcharObj) {
        if (descErrVcharObj != null) {
            setDescErrVchar(descErrVcharObj);
            ws.getIndStatRichEst().setDtRilevazioneNav(((short)0));
        }
        else {
            ws.getIndStatRichEst().setDtRilevazioneNav(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return statRichEst.getP04DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.statRichEst.setP04DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return statRichEst.getP04DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.statRichEst.setP04DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return statRichEst.getP04DsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.statRichEst.setP04DsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return statRichEst.getP04DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.statRichEst.setP04DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return statRichEst.getP04DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.statRichEst.setP04DsVer(dsVer);
    }

    @Override
    public char getFlStatEnd() {
        return statRichEst.getP04FlStatEnd();
    }

    @Override
    public void setFlStatEnd(char flStatEnd) {
        this.statRichEst.setP04FlStatEnd(flStatEnd);
    }

    @Override
    public Character getFlStatEndObj() {
        if (ws.getIndStatRichEst().getValQuo() >= 0) {
            return ((Character)getFlStatEnd());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlStatEndObj(Character flStatEndObj) {
        if (flStatEndObj != null) {
            setFlStatEnd(((char)flStatEndObj));
            ws.getIndStatRichEst().setValQuo(((short)0));
        }
        else {
            ws.getIndStatRichEst().setValQuo(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return statRichEst.getP04IdMoviCrz().getP04IdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.statRichEst.getP04IdMoviCrz().setP04IdMoviCrz(idMoviCrz);
    }

    @Override
    public Integer getIdMoviCrzObj() {
        if (ws.getIndStatRichEst().getFlNoNav() >= 0) {
            return ((Integer)getIdMoviCrz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviCrzObj(Integer idMoviCrzObj) {
        if (idMoviCrzObj != null) {
            setIdMoviCrz(((int)idMoviCrzObj));
            ws.getIndStatRichEst().setFlNoNav(((short)0));
        }
        else {
            ws.getIndStatRichEst().setFlNoNav(((short)-1));
        }
    }

    @Override
    public int getIdRichEst() {
        return statRichEst.getP04IdRichEst();
    }

    @Override
    public void setIdRichEst(int idRichEst) {
        this.statRichEst.setP04IdRichEst(idRichEst);
    }

    @Override
    public int getP04IdStatRichEst() {
        return statRichEst.getP04IdStatRichEst();
    }

    @Override
    public void setP04IdStatRichEst(int p04IdStatRichEst) {
        this.statRichEst.setP04IdStatRichEst(p04IdStatRichEst);
    }

    @Override
    public String getStatRichEst() {
        return statRichEst.getP04StatRichEst();
    }

    @Override
    public void setStatRichEst(String statRichEst) {
        this.statRichEst.setP04StatRichEst(statRichEst);
    }

    @Override
    public String getTpCausScarto() {
        return statRichEst.getP04TpCausScarto();
    }

    @Override
    public void setTpCausScarto(String tpCausScarto) {
        this.statRichEst.setP04TpCausScarto(tpCausScarto);
    }

    @Override
    public String getTpCausScartoObj() {
        if (ws.getIndStatRichEst().getValQuoManfee() >= 0) {
            return getTpCausScarto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausScartoObj(String tpCausScartoObj) {
        if (tpCausScartoObj != null) {
            setTpCausScarto(tpCausScartoObj);
            ws.getIndStatRichEst().setValQuoManfee(((short)0));
        }
        else {
            ws.getIndStatRichEst().setValQuoManfee(((short)-1));
        }
    }

    @Override
    public long getTsEndVldt() {
        return statRichEst.getP04TsEndVldt();
    }

    @Override
    public void setTsEndVldt(long tsEndVldt) {
        this.statRichEst.setP04TsEndVldt(tsEndVldt);
    }

    @Override
    public long getTsIniVldt() {
        return statRichEst.getP04TsIniVldt();
    }

    @Override
    public void setTsIniVldt(long tsIniVldt) {
        this.statRichEst.setP04TsIniVldt(tsIniVldt);
    }

    @Override
    public String getUtenteInsAgg() {
        return statRichEst.getP04UtenteInsAgg();
    }

    @Override
    public void setUtenteInsAgg(String utenteInsAgg) {
        this.statRichEst.setP04UtenteInsAgg(utenteInsAgg);
    }
}
