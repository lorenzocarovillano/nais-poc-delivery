package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Lccc0019CampiEsito;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.AreaLccc0019;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lves0270Data;
import it.accenture.jnais.ws.ptr.WallTabella;
import it.accenture.jnais.ws.WadeAreaAdesioneLccs0005;
import it.accenture.jnais.ws.WallAreaAsset;
import it.accenture.jnais.ws.WbepAreaBeneficiari;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.Wl23AreaVincPeg;
import it.accenture.jnais.ws.WpmoAreaParamMovi;
import it.accenture.jnais.ws.WpogAreaParamOggLves0245;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WtgaAreaTranche;

/**Original name: LVES0270<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2008.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     PROGRAMMA ..... LVES0270
 *     TIPOLOGIA...... CONTROLLI
 *     PROCESSO....... XXX
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... EMISSIONE CONTRATTO INDIVIDUALE
 *     PAGINA WEB..... CALCOLI
 * **------------------------------------------------------------***</pre>*/
public class Lves0270 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lves0270Data ws = new Lves0270Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLccs0005 wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTranche wtgaAreaTranche;
    //Original name: WL23-AREA-VINC-PEG
    private Wl23AreaVincPeg wl23AreaVincPeg;
    //Original name: WALL-AREA-ASSET
    private WallAreaAsset wallAreaAsset;
    //Original name: WPOG-AREA-PARAM-OGG
    private WpogAreaParamOggLves0245 wpogAreaParamOgg;
    //Original name: WPMO-AREA-PARAM-MOV
    private WpmoAreaParamMovi wpmoAreaParamMov;
    //Original name: WBEP-AREA-BENEFICIARI
    private WbepAreaBeneficiari wbepAreaBeneficiari;

    //==== METHODS ====
    /**Original name: PROGRAM_LVES0270_FIRST_SENTENCES<br>
	 * <pre>MOD20110512
	 * ---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLccs0005 wadeAreaAdesione, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTranche wtgaAreaTranche, Wl23AreaVincPeg wl23AreaVincPeg, WallAreaAsset wallAreaAsset, WpogAreaParamOggLves0245 wpogAreaParamOgg, WpmoAreaParamMovi wpmoAreaParamMov, WbepAreaBeneficiari wbepAreaBeneficiari) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.wl23AreaVincPeg = wl23AreaVincPeg;
        this.wallAreaAsset = wallAreaAsset;
        this.wpogAreaParamOgg = wpogAreaParamOgg;
        this.wpmoAreaParamMov = wpmoAreaParamMov;
        this.wbepAreaBeneficiari = wbepAreaBeneficiari;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lves0270 getInstance() {
        return ((Lves0270)Programs.getInstance(Lves0270.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *                                                                 *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                     AREA-LCCC0019
        //                                          IX-INDICI.
        initAreaLccc0019();
        initIxIndici();
        // COB_CODE: IF WALL-ELE-ASSET-ALL-MAX < 0
        //              INITIALIZE                  WALL-AREA-ASSET
        //           END-IF.
        if (wallAreaAsset.getEleAssetAllMax() < 0) {
            // COB_CODE: INITIALIZE                  WALL-AREA-ASSET
            initWallAreaAsset();
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *    CORPO ELABORATIVO                                            *
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-CONTROLLI-BUSINESS
        //              THRU EX-S1100.
        s1100ControlliBusiness();
    }

    /**Original name: S1100-CONTROLLI-BUSINESS<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLO DATI INPUT
	 * ----------------------------------------------------------------*
	 *  -- CONTROLLI CAMPI VINCOLO</pre>*/
    private void s1100ControlliBusiness() {
        ConcatUtil concatUtil = null;
        // COB_CODE: IF NOT WL23-ST-DEL(1)
        //              END-IF
        //           END-IF.
        if (!wl23AreaVincPeg.getTabVincPeg(1).getLccvl231().getStatus().isDel()) {
            // COB_CODE: IF WL23-DT-CHIU-VINPG(1) IS NUMERIC
            //           AND WL23-DT-CHIU-VINPG(1) GREATER ZEROES
            //               END-IF
            //           END-IF
            if (Functions.isNumber(wl23AreaVincPeg.getTabVincPeg(1).getLccvl231().getDati().getWl23DtChiuVinpg().getWl23DtChiuVinpg()) && wl23AreaVincPeg.getTabVincPeg(1).getLccvl231().getDati().getWl23DtChiuVinpg().getWl23DtChiuVinpg() > 0) {
                // COB_CODE: IF WL23-DT-CHIU-VINPG(1) NOT GREATER
                //              WPOL-DT-DECOR
                //                 THRU EX-S0300
                //           END-IF
                if (wl23AreaVincPeg.getTabVincPeg(1).getLccvl231().getDati().getWl23DtChiuVinpg().getWl23DtChiuVinpg() <= wpolAreaPolizza.getLccvpol1().getDati().getWpolDtDecor()) {
                    // COB_CODE: MOVE WK-PGM
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: MOVE 'S1100-CONTROLLI-BUSINESS'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S1100-CONTROLLI-BUSINESS");
                    // COB_CODE: MOVE '005021'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005021");
                    // COB_CODE: STRING 'LA DATA CHIUSURA VINCOLO ;'
                    //                  'DELLA DATA DECORRENZA POLIZZA ;'
                    //             DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LA DATA CHIUSURA VINCOLO ;", "DELLA DATA DECORRENZA POLIZZA ;");
                    ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
                // COB_CODE: IF WPOL-DT-SCAD IS NUMERIC
                //            END-IF
                //           END-IF
                if (Functions.isNumber(wpolAreaPolizza.getLccvpol1().getDati().getWpolDtScad().getWpolDtScad())) {
                    // COB_CODE: IF WL23-DT-CHIU-VINPG(1) NOT LESS
                    //             WPOL-DT-SCAD
                    //                THRU EX-S0300
                    //           END-IF
                    if (wl23AreaVincPeg.getTabVincPeg(1).getLccvl231().getDati().getWl23DtChiuVinpg().getWl23DtChiuVinpg() >= wpolAreaPolizza.getLccvpol1().getDati().getWpolDtScad().getWpolDtScad()) {
                        // COB_CODE: MOVE WK-PGM
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'S1100-CONTROLLI-BUSINESS'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("S1100-CONTROLLI-BUSINESS");
                        // COB_CODE: MOVE '005022'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005022");
                        // COB_CODE: STRING 'LA DATA CHIUSURA VINCOLO ;'
                        //                  'DELLA DATA SCADENZA POLIZZA ;'
                        //             DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "LA DATA CHIUSURA VINCOLO ;", "DELLA DATA SCADENZA POLIZZA ;");
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                    }
                }
            }
            // COB_CODE: IF  WL23-FL-VINPG-INT-PRSTZ(1) = 'N'
            //           AND WL23-CPT-VINCTO-PIGN-NULL(1) NOT = HIGH-VALUE
            //              END-IF
            //           END-IF
            if (wl23AreaVincPeg.getTabVincPeg(1).getLccvl231().getDati().getWl23FlVinpgIntPrstz() == 'N' && !Characters.EQ_HIGH.test(wl23AreaVincPeg.getTabVincPeg(1).getLccvl231().getDati().getWl23CptVinctoPign().getWl23CptVinctoPignNullFormatted())) {
                // COB_CODE: PERFORM S1200-CALCOLA-PREST-INI      THRU S1200-EX
                s1200CalcolaPrestIni();
                // COB_CODE: IF WK-PREST-INI-TOT IS NUMERIC
                //           AND WK-PREST-INI-TOT GREATER ZEROES
                //               END-IF
                //           END-IF
                if (Functions.isNumber(ws.getWkPrestIniTot()) && ws.getWkPrestIniTot().compareTo(0) > 0) {
                    // COB_CODE: IF WL23-CPT-VINCTO-PIGN(1) GREATER
                    //              WK-PREST-INI-TOT
                    //                 THRU EX-S0300
                    //           END-IF
                    if (wl23AreaVincPeg.getTabVincPeg(1).getLccvl231().getDati().getWl23CptVinctoPign().getWl23CptVinctoPign().compareTo(ws.getWkPrestIniTot()) > 0) {
                        // COB_CODE: MOVE WK-PGM
                        //             TO IEAI9901-COD-SERVIZIO-BE
                        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: MOVE 'S1100-CONTROLLI-BUSINESS'
                        //             TO IEAI9901-LABEL-ERR
                        ws.getIeai9901Area().setLabelErr("S1100-CONTROLLI-BUSINESS");
                        // COB_CODE: MOVE '005022'
                        //             TO IEAI9901-COD-ERRORE
                        ws.getIeai9901Area().setCodErroreFormatted("005022");
                        // COB_CODE: STRING 'L IMPORTO LIBERO DEL VINCOLO ;'
                        //                  'DEL PREMIO INIZIALE ;'
                        //             DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, "L IMPORTO LIBERO DEL VINCOLO ;", "DEL PREMIO INIZIALE ;");
                        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
                        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                        //              THRU EX-S0300
                        s0300RicercaGravitaErrore();
                    }
                }
            }
        }
        //    IF IDSV0001-ESITO-OK
        //-->    PER LINEAR I CONTROLLI SUL PLATFOND AVVENGONO VIA WEB
        //-->    E NON DEVONO ESSERE FATTI IN FASE DI CREAZIONE PROPOSTA
        //       IF IDSV0001-COD-COMPAGNIA-ANIA = 8
        //          CONTINUE
        //       ELSE
        //       IF WCOM-ELE-MAX-PLATFOND GREATER ZERO
        //       AND WALL-ELE-ASSET-ALL-MAX   GREATER ZERO
        //          SET LCCC0019-LETTURA-VPL      TO TRUE
        //          SET LCCC0019-CONTROLLO-SI     TO TRUE
        //          PERFORM VERIFICA-PLATFOND
        //             THRU VERIFICA-PLATFOND-EX
        //
        //-- IN CASO DI NON DISPONIBILITA' DI PLAFOND SEGNALO ERRORE
        //
        //          IF LCCC0019-GENERIC-ERROR
        //             MOVE LCCC0019-COD-SERVIZIO-BE
        //               TO IEAI9901-COD-SERVIZIO-BE
        //             MOVE 'S1100-CONTROLLI-BUSINESS'
        //               TO IEAI9901-LABEL-ERR
        //             MOVE LCCC0019-COD-ERRORE
        //               TO IEAI9901-COD-ERRORE
        //             MOVE LCCC0019-PARAMETRI-ERR
        //               TO IEAI9901-PARAMETRI-ERR
        //             PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //                THRU EX-S0300
        //          END-IF
        //       END-IF
        //    END-IF.
        //MOD20110512
        // COB_CODE: IF IDSV0001-ESITO-OK
        //             AND WPOL-ST-ADD
        //                 THRU S1170-EX
        //            END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && wpolAreaPolizza.getLccvpol1().getStatus().isAdd()) {
            // COB_CODE: PERFORM S1170-CTRL-BENEFICIARI
            //              THRU S1170-EX
            s1170CtrlBeneficiari();
        }
    }

    /**Original name: S1170-CTRL-BENEFICIARI<br>
	 * <pre>MOD20110512
	 * ----------------------------------------------------------------*
	 *  Controllo beneficiario
	 * ----------------------------------------------------------------*</pre>*/
    private void s1170CtrlBeneficiari() {
        ConcatUtil concatUtil = null;
        // COB_CODE: SET MOVIMENTO-CEDOLA-NO    TO TRUE.
        ws.getMovimentoCedola().setNo();
        // COB_CODE: SET BENEFICIARIO-CEDOLA-NO TO TRUE.
        ws.getBeneficiarioCedola().setNo();
        // COB_CODE: PERFORM VARYING IX-TAB-PMO FROM 1 BY 1
        //             UNTIL IX-TAB-PMO GREATER WPMO-ELE-PARAM-MOV-MAX
        //                OR IDSV0001-ESITO-KO
        //              END-IF
        //           END-PERFORM.
        ws.getIxIndici().setIxAutOper(((short)1));
        while (!(ws.getIxIndici().getIxAutOper() > wpmoAreaParamMov.getEleParamMovMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            // COB_CODE: IF WPMO-TP-MOVI(IX-TAB-PMO) IS NUMERIC AND
            //              WPMO-TP-MOVI(IX-TAB-PMO) = 6007
            //              END-PERFORM
            //            END-IF
            if (Functions.isNumber(wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getIxAutOper()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi()) && wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getIxAutOper()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi() == 6007) {
                // COB_CODE: SET MOVIMENTO-CEDOLA-SI  TO TRUE
                ws.getMovimentoCedola().setSi();
                // COB_CODE: PERFORM VARYING IX-TAB-BEP FROM 1 BY 1
                //             UNTIL IX-TAB-BEP GREATER WBEP-ELE-BENEFICIARI
                //                END-IF
                //           END-PERFORM
                ws.getIxIndici().setIxParam(((short)1));
                while (!(ws.getIxIndici().getIxParam() > wbepAreaBeneficiari.getEleBeneficiari())) {
                    // COB_CODE: IF WBEP-COD-BNFIC(IX-TAB-BEP) IS NUMERIC AND
                    //              WBEP-COD-BNFIC(IX-TAB-BEP) = 99
                    //              SET BENEFICIARIO-CEDOLA-SI TO TRUE
                    //           END-IF
                    if (Functions.isNumber(wbepAreaBeneficiari.getTabBeneficiari(ws.getIxIndici().getIxParam()).getLccvbep1().getDati().getWbepCodBnfic().getWbepCodBnfic()) && wbepAreaBeneficiari.getTabBeneficiari(ws.getIxIndici().getIxParam()).getLccvbep1().getDati().getWbepCodBnfic().getWbepCodBnfic() == 99) {
                        // COB_CODE: SET MOVIMENTO-CEDOLA-SI  TO TRUE
                        ws.getMovimentoCedola().setSi();
                        // COB_CODE: SET BENEFICIARIO-CEDOLA-SI TO TRUE
                        ws.getBeneficiarioCedola().setSi();
                    }
                    ws.getIxIndici().setIxParam(Trunc.toShort(ws.getIxIndici().getIxParam() + 1, 4));
                }
            }
            ws.getIxIndici().setIxAutOper(Trunc.toShort(ws.getIxIndici().getIxAutOper() + 1, 4));
        }
        // COB_CODE: IF MOVIMENTO-CEDOLA-SI     AND
        //              BENEFICIARIO-CEDOLA-NO
        //                  THRU EX-S0300
        //           END-IF.
        if (ws.getMovimentoCedola().isSi() && ws.getBeneficiarioCedola().isNo()) {
            // COB_CODE: MOVE WK-PGM  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1100-CONTROLLI-BUSINESS' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1100-CONTROLLI-BUSINESS");
            // COB_CODE: MOVE '005166'                   TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005166");
            // COB_CODE: STRING 'BENEFICIARIO CEDOLE NON PRESENTE ;'
            //            DELIMITED BY SIZE INTO IEAI9901-PARAMETRI-ERR
            //           END-STRING
            ws.getIeai9901Area().setParametriErr("BENEFICIARIO CEDOLE NON PRESENTE ;");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //               THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1200-CALCOLA-PREST-INI<br>
	 * <pre>----------------------------------------------------------------*
	 *    SOMMA LE PRESTAZIONI INIZIALI DELLA TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200CalcolaPrestIni() {
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA GREATER WTGA-ELE-TRAN-MAX
        //           *
        //                  END-IF
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setIndUnzip(((short)1));
        while (!(ws.getIxIndici().getIndUnzip() > wtgaAreaTranche.getEleTranMax())) {
            //
            // COB_CODE: IF WTGA-PRSTZ-INI(IX-TAB-TGA) IS NUMERIC
            //                                         WTGA-PRSTZ-INI(IX-TAB-TGA)
            //           END-IF
            if (Functions.isNumber(wtgaAreaTranche.getTabTran(ws.getIxIndici().getIndUnzip()).getLccvtga1().getDati().getWtgaPrstzIni().getWtgaPrstzIni())) {
                // COB_CODE: COMPUTE WK-PREST-INI-TOT = WK-PREST-INI-TOT +
                //                                      WTGA-PRSTZ-INI(IX-TAB-TGA)
                ws.setWkPrestIniTot(Trunc.toDecimal(ws.getWkPrestIniTot().add(wtgaAreaTranche.getTabTran(ws.getIxIndici().getIndUnzip()).getLccvtga1().getDati().getWtgaPrstzIni().getWtgaPrstzIni()), 15, 3));
            }
            //
            ws.getIxIndici().setIndUnzip(Trunc.toShort(ws.getIxIndici().getIndUnzip() + 1, 4));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    public void initAreaLccc0019() {
        ws.getAreaLccc0019().getTpOperaz().setTpOperaz("");
        ws.getAreaLccc0019().getTariffa().setTariffa("");
        ws.getAreaLccc0019().getControllo().setControllo("");
        ws.getAreaLccc0019().getImportoPlat().setImportoPlat("");
        ws.getAreaLccc0019().getTipoAgg().setTipoAgg("");
        ws.getAreaLccc0019().setEleMaxFondi(((short)0));
        for (int idx0 = 1; idx0 <= AreaLccc0019.TAB_FONDI_MAXOCCURS; idx0++) {
            ws.getAreaLccc0019().getTabFondi(idx0).setImpPlatfond(new AfDecimal(0, 18, 3));
            ws.getAreaLccc0019().getTabFondi(idx0).setCodFondo("");
            ws.getAreaLccc0019().getTabFondi(idx0).setPercFondo(new AfDecimal(0, 6, 3));
            ws.getAreaLccc0019().getTabFondi(idx0).setPercFondoOld(new AfDecimal(0, 6, 3));
        }
        ws.getAreaLccc0019().setImpDaScalare(new AfDecimal(0, 15, 3));
        ws.getAreaLccc0019().setImpDaScalareOld(new AfDecimal(0, 15, 3));
        ws.getAreaLccc0019().setImpDispPlafond(new AfDecimal(0, 15, 3));
        ws.getAreaLccc0019().getReturnCode().setReturnCode("");
        ws.getAreaLccc0019().getSqlcodeSigned().setSqlcodeSigned(0);
        ws.getAreaLccc0019().setSqlcodeFormatted("000000000");
        ws.getAreaLccc0019().getCampiEsito().setDescrizErrDb2("");
        ws.getAreaLccc0019().getCampiEsito().setCodServizioBe("");
        ws.getAreaLccc0019().getCampiEsito().setLabelErr("");
        ws.getAreaLccc0019().getCampiEsito().setCodErroreFormatted("000000");
        ws.getAreaLccc0019().getCampiEsito().setParametriErr("");
        ws.getAreaLccc0019().getCampiEsito().setNomeTabella("");
        ws.getAreaLccc0019().getCampiEsito().setKeyTabella("");
    }

    public void initIxIndici() {
        ws.getIxIndici().setIndUnzip(((short)0));
        ws.getIxIndici().setIndStartVar(((short)0));
        ws.getIxIndici().setIndEndVar(((short)0));
        ws.getIxIndici().setIndChar(((short)0));
        ws.getIxIndici().setIxTabCao(((short)0));
        ws.getIxIndici().setIxAutOper(((short)0));
        ws.getIxIndici().setIxParam(((short)0));
    }

    public void initWallAreaAsset() {
        wallAreaAsset.setEleAssetAllMax(((short)0));
        for (int idx0 = 1; idx0 <= WallTabella.TAB_ASSET_ALL_MAXOCCURS; idx0++) {
            wallAreaAsset.getTabella().setStatus(idx0, Types.SPACE_CHAR);
            wallAreaAsset.getTabella().setIdPtf(idx0, 0);
            wallAreaAsset.getTabella().setIdAstAlloc(idx0, 0);
            wallAreaAsset.getTabella().setIdStraDiInvst(idx0, 0);
            wallAreaAsset.getTabella().setIdMoviCrz(idx0, 0);
            wallAreaAsset.getTabella().setIdMoviChiu(idx0, 0);
            wallAreaAsset.getTabella().setDtIniVldt(idx0, 0);
            wallAreaAsset.getTabella().setDtEndVldt(idx0, 0);
            wallAreaAsset.getTabella().setDtIniEff(idx0, 0);
            wallAreaAsset.getTabella().setDtEndEff(idx0, 0);
            wallAreaAsset.getTabella().setCodCompAnia(idx0, 0);
            wallAreaAsset.getTabella().setCodFnd(idx0, "");
            wallAreaAsset.getTabella().setCodTari(idx0, "");
            wallAreaAsset.getTabella().setTpApplzAst(idx0, "");
            wallAreaAsset.getTabella().setPcRipAst(idx0, new AfDecimal(0, 6, 3));
            wallAreaAsset.getTabella().setTpFnd(idx0, Types.SPACE_CHAR);
            wallAreaAsset.getTabella().setDsRiga(idx0, 0);
            wallAreaAsset.getTabella().setDsOperSql(idx0, Types.SPACE_CHAR);
            wallAreaAsset.getTabella().setDsVer(idx0, 0);
            wallAreaAsset.getTabella().setDsTsIniCptz(idx0, 0);
            wallAreaAsset.getTabella().setDsTsEndCptz(idx0, 0);
            wallAreaAsset.getTabella().setDsUtente(idx0, "");
            wallAreaAsset.getTabella().setDsStatoElab(idx0, Types.SPACE_CHAR);
            wallAreaAsset.getTabella().setPeriodo(idx0, ((short)0));
            wallAreaAsset.getTabella().setTpRibilFnd(idx0, "");
        }
    }
}
