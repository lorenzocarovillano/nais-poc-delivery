package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.AdesStatOggBusDao;
import it.accenture.jnais.commons.data.dao.StatOggBusTrchDiGarDao;
import it.accenture.jnais.commons.data.to.IAdesStatOggBus;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.copy.TrchDiGarIvvs0216;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbo0731;
import it.accenture.jnais.ws.Ldbs0730Data;
import it.accenture.jnais.ws.redefines.StbIdMoviChiu;
import it.accenture.jnais.ws.redefines.TgaAbbAnnuUlt;
import it.accenture.jnais.ws.redefines.TgaAbbTotIni;
import it.accenture.jnais.ws.redefines.TgaAbbTotUlt;
import it.accenture.jnais.ws.redefines.TgaAcqExp;
import it.accenture.jnais.ws.redefines.TgaAlqCommisInter;
import it.accenture.jnais.ws.redefines.TgaAlqProvAcq;
import it.accenture.jnais.ws.redefines.TgaAlqProvInc;
import it.accenture.jnais.ws.redefines.TgaAlqProvRicor;
import it.accenture.jnais.ws.redefines.TgaAlqRemunAss;
import it.accenture.jnais.ws.redefines.TgaAlqScon;
import it.accenture.jnais.ws.redefines.TgaBnsGiaLiqto;
import it.accenture.jnais.ws.redefines.TgaCommisGest;
import it.accenture.jnais.ws.redefines.TgaCommisInter;
import it.accenture.jnais.ws.redefines.TgaCosRunAssva;
import it.accenture.jnais.ws.redefines.TgaCosRunAssvaIdc;
import it.accenture.jnais.ws.redefines.TgaCptInOpzRivto;
import it.accenture.jnais.ws.redefines.TgaCptMinScad;
import it.accenture.jnais.ws.redefines.TgaCptRshMor;
import it.accenture.jnais.ws.redefines.TgaDtEffStab;
import it.accenture.jnais.ws.redefines.TgaDtEmis;
import it.accenture.jnais.ws.redefines.TgaDtIniValTar;
import it.accenture.jnais.ws.redefines.TgaDtScad;
import it.accenture.jnais.ws.redefines.TgaDtUltAdegPrePr;
import it.accenture.jnais.ws.redefines.TgaDtVldtProd;
import it.accenture.jnais.ws.redefines.TgaDurAa;
import it.accenture.jnais.ws.redefines.TgaDurAbb;
import it.accenture.jnais.ws.redefines.TgaDurGg;
import it.accenture.jnais.ws.redefines.TgaDurMm;
import it.accenture.jnais.ws.redefines.TgaEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.TgaEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.TgaIdMoviChiu;
import it.accenture.jnais.ws.redefines.TgaImpAder;
import it.accenture.jnais.ws.redefines.TgaImpAltSopr;
import it.accenture.jnais.ws.redefines.TgaImpAz;
import it.accenture.jnais.ws.redefines.TgaImpbCommisInter;
import it.accenture.jnais.ws.redefines.TgaImpBns;
import it.accenture.jnais.ws.redefines.TgaImpBnsAntic;
import it.accenture.jnais.ws.redefines.TgaImpbProvAcq;
import it.accenture.jnais.ws.redefines.TgaImpbProvInc;
import it.accenture.jnais.ws.redefines.TgaImpbProvRicor;
import it.accenture.jnais.ws.redefines.TgaImpbRemunAss;
import it.accenture.jnais.ws.redefines.TgaImpbVisEnd2000;
import it.accenture.jnais.ws.redefines.TgaImpCarAcq;
import it.accenture.jnais.ws.redefines.TgaImpCarGest;
import it.accenture.jnais.ws.redefines.TgaImpCarInc;
import it.accenture.jnais.ws.redefines.TgaImpScon;
import it.accenture.jnais.ws.redefines.TgaImpSoprProf;
import it.accenture.jnais.ws.redefines.TgaImpSoprSan;
import it.accenture.jnais.ws.redefines.TgaImpSoprSpo;
import it.accenture.jnais.ws.redefines.TgaImpSoprTec;
import it.accenture.jnais.ws.redefines.TgaImpTfr;
import it.accenture.jnais.ws.redefines.TgaImpTfrStrc;
import it.accenture.jnais.ws.redefines.TgaImpTrasfe;
import it.accenture.jnais.ws.redefines.TgaImpVolo;
import it.accenture.jnais.ws.redefines.TgaIncrPre;
import it.accenture.jnais.ws.redefines.TgaIncrPrstz;
import it.accenture.jnais.ws.redefines.TgaIntrMora;
import it.accenture.jnais.ws.redefines.TgaManfeeAntic;
import it.accenture.jnais.ws.redefines.TgaManfeeRicor;
import it.accenture.jnais.ws.redefines.TgaMatuEnd2000;
import it.accenture.jnais.ws.redefines.TgaMinGarto;
import it.accenture.jnais.ws.redefines.TgaMinTrnut;
import it.accenture.jnais.ws.redefines.TgaNumGgRival;
import it.accenture.jnais.ws.redefines.TgaOldTsTec;
import it.accenture.jnais.ws.redefines.TgaPcCommisGest;
import it.accenture.jnais.ws.redefines.TgaPcIntrRiat;
import it.accenture.jnais.ws.redefines.TgaPcRetr;
import it.accenture.jnais.ws.redefines.TgaPcRipPre;
import it.accenture.jnais.ws.redefines.TgaPreAttDiTrch;
import it.accenture.jnais.ws.redefines.TgaPreCasoMor;
import it.accenture.jnais.ws.redefines.TgaPreIniNet;
import it.accenture.jnais.ws.redefines.TgaPreInvrioIni;
import it.accenture.jnais.ws.redefines.TgaPreInvrioUlt;
import it.accenture.jnais.ws.redefines.TgaPreLrd;
import it.accenture.jnais.ws.redefines.TgaPrePattuito;
import it.accenture.jnais.ws.redefines.TgaPrePpIni;
import it.accenture.jnais.ws.redefines.TgaPrePpUlt;
import it.accenture.jnais.ws.redefines.TgaPreRivto;
import it.accenture.jnais.ws.redefines.TgaPreStab;
import it.accenture.jnais.ws.redefines.TgaPreTariIni;
import it.accenture.jnais.ws.redefines.TgaPreTariUlt;
import it.accenture.jnais.ws.redefines.TgaPreUniRivto;
import it.accenture.jnais.ws.redefines.TgaProv1aaAcq;
import it.accenture.jnais.ws.redefines.TgaProv2aaAcq;
import it.accenture.jnais.ws.redefines.TgaProvInc;
import it.accenture.jnais.ws.redefines.TgaProvRicor;
import it.accenture.jnais.ws.redefines.TgaPrstzAggIni;
import it.accenture.jnais.ws.redefines.TgaPrstzAggUlt;
import it.accenture.jnais.ws.redefines.TgaPrstzIni;
import it.accenture.jnais.ws.redefines.TgaPrstzIniNewfis;
import it.accenture.jnais.ws.redefines.TgaPrstzIniNforz;
import it.accenture.jnais.ws.redefines.TgaPrstzIniStab;
import it.accenture.jnais.ws.redefines.TgaPrstzRidIni;
import it.accenture.jnais.ws.redefines.TgaPrstzUlt;
import it.accenture.jnais.ws.redefines.TgaRatLrd;
import it.accenture.jnais.ws.redefines.TgaRemunAss;
import it.accenture.jnais.ws.redefines.TgaRendtoLrd;
import it.accenture.jnais.ws.redefines.TgaRendtoRetr;
import it.accenture.jnais.ws.redefines.TgaRenIniTsTec0;
import it.accenture.jnais.ws.redefines.TgaRisMat;
import it.accenture.jnais.ws.redefines.TgaTsRivalFis;
import it.accenture.jnais.ws.redefines.TgaTsRivalIndiciz;
import it.accenture.jnais.ws.redefines.TgaTsRivalNet;
import it.accenture.jnais.ws.redefines.TgaVisEnd2000;
import it.accenture.jnais.ws.redefines.TgaVisEnd2000Nforz;

/**Original name: LDBS0730<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  19 GIUGN 2007.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                      *
 *  F A S E         : MODULI PER ACCESSO RISORSE DB                *
 *  F A S E         : LETTURA PORTAFOGLIO                          *
 * ----------------------------------------------------------------*
 *  IL PROGRAMMA VINE RICHIAMTO CON DUE MODALITA DIVERSE           *
 *     LDBI0731-TP-CALL = '01'                                     *
 *       ESTRAE L'AREA STAT-OGG-BUS IN JOIN CON L'AREA ADES        *
 *       PRENDENDO SOLO LE OCCORRENZE CON TP_STAT_BUS = 'TR'       *
 *     LDBI0731-TP-CALL = '02'                                     *
 *       ESTRAE L'AREA STAT-OGG-BUS IN JOIN CON L'AREA TRCH-DI-GAR *
 *       PRENDENDO SOLO LE OCCORRENZE CON TP_STAT_BUS = 'TR'
 * UTILIZZATI I CURSORI WC03 e WC04, SPECULARI RISPETTIVAMENTE AI  *
 * CURSORI WC01 e WC02 PER EVENTUALE RICORSIONE                    *
 * ----------------------------------------------------------------*</pre>*/
public class Ldbs0730 extends Program implements IAdesStatOggBus {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private StatOggBusTrchDiGarLdbs0730 statOggBusTrchDiGarLdbs0730 = new StatOggBusTrchDiGarLdbs0730(this);
    private StatOggBusTrchDiGarLdbs07302 statOggBusTrchDiGarLdbs07302 = new StatOggBusTrchDiGarLdbs07302(this);
    private AdesStatOggBusDao adesStatOggBusDao = new AdesStatOggBusDao(dbAccessStatus);
    private StatOggBusTrchDiGarDao statOggBusTrchDiGarDao = new StatOggBusTrchDiGarDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs0730Data ws = new Ldbs0730Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LDBO0731
    private Ldbo0731 ldbo0731;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS0730_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ldbo0731 ldbo0731) {
        this.idsv0003 = idsv0003;
        this.ldbo0731 = ldbo0731;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
        //           OR IDSV0003-TRATT-X-COMPETENZA
        //              END-EVALUATE
        //           ELSE
        //             SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-IF.
        if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto() || this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN IDSV0003-WHERE-CONDITION
            //                 PERFORM A205-ELABORA-WC       THRU A205-EX
            //              WHEN OTHER
            //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            //           END-EVALUATE
            switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A205-ELABORA-WC       THRU A205-EX
                    a205ElaboraWc();
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                    this.idsv0003.getReturnCode().setInvalidLevelOper();
                    break;
            }
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            this.idsv0003.getReturnCode().setInvalidLevelOper();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs0730 getInstance() {
        return ((Ldbs0730)Programs.getInstance(Ldbs0730.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS0730'               TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS0730");
        // COB_CODE: MOVE 'GAR'           TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("GAR");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: MOVE ZEROES                  TO WS-TIMESTAMP-NUM.
        ws.getWsTimestamp().setWsTimestampNum(0);
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
        // COB_CODE: ACCEPT WS-TIMESTAMP(1:8)     FROM DATE YYYYMMDD.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getDateYYYYMMDD(), 1, 8));
        // COB_CODE: ACCEPT WS-TIMESTAMP(9:6)     FROM TIME.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getTimeHHMMSSMM(), 9, 6));
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A205-ELABORA-WC<br>*/
    private void a205ElaboraWc() {
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBI0731.
        ws.getLdbi0731().setLdbi0731Formatted(idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: EVALUATE LDBI0731-TP-CALL
        //               WHEN '01'
        //                 END-IF
        //               WHEN '02'
        //                 END-IF
        //           END-EVALUATE.
        switch (ws.getLdbi0731().getTpCall()) {

            case "01":// COB_CODE: IF LDBI0731-TRASFORMATA NOT = 'S'
                //              PERFORM A206-ELABORA-WC01          THRU A206-EX
                //           ELSE
                //              PERFORM A208-ELABORA-WC03          THRU A208-EX
                //           END-IF
                if (ws.getLdbi0731().getTrasformata() != 'S') {
                    // COB_CODE: PERFORM A206-ELABORA-WC01          THRU A206-EX
                    a206ElaboraWc01();
                }
                else {
                    // COB_CODE: PERFORM A208-ELABORA-WC03          THRU A208-EX
                    a208ElaboraWc03();
                }
                break;

            case "02":// COB_CODE: IF LDBI0731-TRASFORMATA NOT = 'S'
                //              PERFORM A207-ELABORA-WC02          THRU A207-EX
                //           ELSE
                //              PERFORM A209-ELABORA-WC04          THRU A209-EX
                //           END-IF
                if (ws.getLdbi0731().getTrasformata() != 'S') {
                    // COB_CODE: PERFORM A207-ELABORA-WC02          THRU A207-EX
                    a207ElaboraWc02();
                }
                else {
                    // COB_CODE: PERFORM A209-ELABORA-WC04          THRU A209-EX
                    a209ElaboraWc04();
                }
                break;

            default:break;
        }
    }

    /**Original name: A206-ELABORA-WC01<br>*/
    private void a206ElaboraWc01() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM AA260-OPEN-CURSOR-WC1         THRU AA260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM AA270-CLOSE-CURSOR-WC1        THRU AA270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM AA280-FETCH-FIRST-WC1         THRU AA280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM AA290-FETCH-NEXT-WC1          THRU AA290-EX
        //              WHEN IDSV0003-SELECT
        //                 PERFORM AA295-SELECT-WC1              THRU AA295-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM AA260-OPEN-CURSOR-WC1         THRU AA260-EX
            aa260OpenCursorWc1();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM AA270-CLOSE-CURSOR-WC1        THRU AA270-EX
            aa270CloseCursorWc1();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM AA280-FETCH-FIRST-WC1         THRU AA280-EX
            aa280FetchFirstWc1();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM AA290-FETCH-NEXT-WC1          THRU AA290-EX
            aa290FetchNextWc1();
        }
        else if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM AA295-SELECT-WC1              THRU AA295-EX
            aa295SelectWc1();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A207-ELABORA-WC02<br>*/
    private void a207ElaboraWc02() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM AB260-OPEN-CURSOR-WC2         THRU AB260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM AB270-CLOSE-CURSOR-WC2        THRU AB270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM AB280-FETCH-FIRST-WC2         THRU AB280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM AB290-FETCH-NEXT-WC2          THRU AB290-EX
        //              WHEN IDSV0003-SELECT
        //                 PERFORM AB295-SELECT-WC2              THRU AB295-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM AB260-OPEN-CURSOR-WC2         THRU AB260-EX
            ab260OpenCursorWc2();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM AB270-CLOSE-CURSOR-WC2        THRU AB270-EX
            ab270CloseCursorWc2();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM AB280-FETCH-FIRST-WC2         THRU AB280-EX
            ab280FetchFirstWc2();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM AB290-FETCH-NEXT-WC2          THRU AB290-EX
            ab290FetchNextWc2();
        }
        else if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM AB295-SELECT-WC2              THRU AB295-EX
            ab295SelectWc2();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: AA250-DECLARE-CURSOR-WC1<br>
	 * <pre>----
	 * ----  gestione WHERE CONDITION 1
	 * ----</pre>*/
    private void aa250DeclareCursorWc1() {
    // COB_CODE: EXEC SQL
    //                 DECLARE CUR-WC1-STB CURSOR FOR
    //              SELECT
    //                 A.ID_POLI
    //                ,B.ID_STAT_OGG_BUS
    //                ,B.ID_OGG
    //                ,B.TP_OGG
    //                ,B.ID_MOVI_CRZ
    //                ,B.ID_MOVI_CHIU
    //                ,B.DT_INI_EFF
    //                ,B.DT_END_EFF
    //                ,B.COD_COMP_ANIA
    //                ,B.TP_STAT_BUS
    //                ,B.TP_CAUS
    //                ,B.DS_RIGA
    //                ,B.DS_OPER_SQL
    //                ,B.DS_VER
    //                ,B.DS_TS_INI_CPTZ
    //                ,B.DS_TS_END_CPTZ
    //                ,B.DS_UTENTE
    //                ,B.DS_STATO_ELAB
    //               FROM     ADES A,     STAT_OGG_BUS B
    //             WHERE A.ID_POLI = :LDBI0731-ID-POLI
    //               AND A.ID_ADES = B.ID_OGG
    //               AND B.TP_OGG = 'AD'
    //               AND B.TP_STAT_BUS = :LDBI0731-TP-STAT-BUS
    //               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //               AND A.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
    //               AND A.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
    //               AND A.DS_TS_INI_CPTZ <=
    //                   :WS-TS-COMPETENZA
    //               AND A.DS_TS_END_CPTZ >
    //                   :WS-TS-COMPETENZA
    //               AND B.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //               AND B.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
    //               AND B.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
    //               AND B.DS_TS_INI_CPTZ <=
    //                   :WS-TS-COMPETENZA
    //               AND B.DS_TS_END_CPTZ >
    //                   :WS-TS-COMPETENZA
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: AA260-OPEN-CURSOR-WC1<br>*/
    private void aa260OpenCursorWc1() {
        // COB_CODE: PERFORM AA250-DECLARE-CURSOR-WC1 THRU AA250-EX.
        aa250DeclareCursorWc1();
        // modificare il nome del cursore
        // COB_CODE: EXEC SQL
        //                OPEN  CUR-WC1-STB
        //           END-EXEC.
        adesStatOggBusDao.openCurWc1Stb(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: AA270-CLOSE-CURSOR-WC1<br>
	 * <pre> modificare il nome del cursore</pre>*/
    private void aa270CloseCursorWc1() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-WC1-STB
        //           END-EXEC.
        adesStatOggBusDao.closeCurWc1Stb();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: AA280-FETCH-FIRST-WC1<br>*/
    private void aa280FetchFirstWc1() {
        // COB_CODE: PERFORM AA260-OPEN-CURSOR-WC1    THRU AA260-EX.
        aa260OpenCursorWc1();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM AA290-FETCH-NEXT-WC1 THRU AA290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM AA290-FETCH-NEXT-WC1 THRU AA290-EX
            aa290FetchNextWc1();
        }
    }

    /**Original name: AA290-FETCH-NEXT-WC1<br>*/
    private void aa290FetchNextWc1() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-WC1-STB
        //           INTO
        //                :ADE-ID-POLI
        //               ,:STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        adesStatOggBusDao.fetchCurWc1Stb(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL     THRU Z100-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N-TGA  THRU Z950-EX
            z950ConvertiXToNTga();
            // COB_CODE: PERFORM Z951-CONVERTI-X-TO-N-STB  THRU Z951-EX
            z951ConvertiXToNStb();
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL     THRU Z100-EX
            z100SetColonneNull();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM AA270-CLOSE-CURSOR-WC1 THRU AA270-EX
            aa270CloseCursorWc1();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: AA295-SELECT-WC1<br>*/
    private void aa295SelectWc1() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 A.ID_POLI
        //                ,B.ID_STAT_OGG_BUS
        //                ,B.ID_OGG
        //                ,B.TP_OGG
        //                ,B.ID_MOVI_CRZ
        //                ,B.ID_MOVI_CHIU
        //                ,B.DT_INI_EFF
        //                ,B.DT_END_EFF
        //                ,B.COD_COMP_ANIA
        //                ,B.TP_STAT_BUS
        //                ,B.TP_CAUS
        //                ,B.DS_RIGA
        //                ,B.DS_OPER_SQL
        //                ,B.DS_VER
        //                ,B.DS_TS_INI_CPTZ
        //                ,B.DS_TS_END_CPTZ
        //                ,B.DS_UTENTE
        //                ,B.DS_STATO_ELAB
        //             INTO
        //                  :ADE-ID-POLI
        //                 ,:STB-ID-STAT-OGG-BUS
        //                 ,:STB-ID-OGG
        //                 ,:STB-TP-OGG
        //                 ,:STB-ID-MOVI-CRZ
        //                 ,:STB-ID-MOVI-CHIU
        //                  :IND-STB-ID-MOVI-CHIU
        //                 ,:STB-DT-INI-EFF-DB
        //                 ,:STB-DT-END-EFF-DB
        //                 ,:STB-COD-COMP-ANIA
        //                 ,:STB-TP-STAT-BUS
        //                 ,:STB-TP-CAUS
        //                 ,:STB-DS-RIGA
        //                 ,:STB-DS-OPER-SQL
        //                 ,:STB-DS-VER
        //                 ,:STB-DS-TS-INI-CPTZ
        //                 ,:STB-DS-TS-END-CPTZ
        //                 ,:STB-DS-UTENTE
        //                 ,:STB-DS-STATO-ELAB
        //             FROM     ADES A,     STAT_OGG_BUS B
        //             WHERE A.ID_ADES = :LDBI0731-ID-ADES
        //               AND A.ID_POLI = :LDBI0731-ID-POLI
        //               AND A.ID_ADES = B.ID_OGG
        //               AND B.TP_OGG = 'AD'
        //               AND B.TP_STAT_BUS = :LDBI0731-TP-STAT-BUS
        //               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND A.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
        //               AND A.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //               AND A.DS_TS_INI_CPTZ <=
        //                   :WS-TS-COMPETENZA
        //               AND A.DS_TS_END_CPTZ >
        //                   :WS-TS-COMPETENZA
        //               AND B.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND B.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
        //               AND B.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //               AND B.DS_TS_INI_CPTZ <=
        //                   :WS-TS-COMPETENZA
        //               AND B.DS_TS_END_CPTZ >
        //                   :WS-TS-COMPETENZA
        //           END-EXEC.
        adesStatOggBusDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z951-CONVERTI-X-TO-N-STB  THRU Z951-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z951-CONVERTI-X-TO-N-STB  THRU Z951-EX
            z951ConvertiXToNStb();
        }
    }

    /**Original name: AB250-DECLARE-CURSOR-WC2<br>
	 * <pre>----
	 * ----  gestione WHERE CONDITION 2
	 * ----</pre>*/
    private void ab250DeclareCursorWc2() {
    // COB_CODE: EXEC SQL
    //                 DECLARE CUR-WC2-STB CURSOR FOR
    //              SELECT
    //                 A.ID_TRCH_DI_GAR
    //                ,A.ID_GAR
    //                ,A.ID_ADES
    //                ,A.ID_POLI
    //                ,A.ID_MOVI_CRZ
    //                ,A.ID_MOVI_CHIU
    //                ,A.DT_INI_EFF
    //                ,A.DT_END_EFF
    //                ,A.COD_COMP_ANIA
    //                ,A.DT_DECOR
    //                ,A.DT_SCAD
    //                ,A.IB_OGG
    //                ,A.TP_RGM_FISC
    //                ,A.DT_EMIS
    //                ,A.TP_TRCH
    //                ,A.DUR_AA
    //                ,A.DUR_MM
    //                ,A.DUR_GG
    //                ,A.PRE_CASO_MOR
    //                ,A.PC_INTR_RIAT
    //                ,A.IMP_BNS_ANTIC
    //                ,A.PRE_INI_NET
    //                ,A.PRE_PP_INI
    //                ,A.PRE_PP_ULT
    //                ,A.PRE_TARI_INI
    //                ,A.PRE_TARI_ULT
    //                ,A.PRE_INVRIO_INI
    //                ,A.PRE_INVRIO_ULT
    //                ,A.PRE_RIVTO
    //                ,A.IMP_SOPR_PROF
    //                ,A.IMP_SOPR_SAN
    //                ,A.IMP_SOPR_SPO
    //                ,A.IMP_SOPR_TEC
    //                ,A.IMP_ALT_SOPR
    //                ,A.PRE_STAB
    //                ,A.DT_EFF_STAB
    //                ,A.TS_RIVAL_FIS
    //                ,A.TS_RIVAL_INDICIZ
    //                ,A.OLD_TS_TEC
    //                ,A.RAT_LRD
    //                ,A.PRE_LRD
    //                ,A.PRSTZ_INI
    //                ,A.PRSTZ_ULT
    //                ,A.CPT_IN_OPZ_RIVTO
    //                ,A.PRSTZ_INI_STAB
    //                ,A.CPT_RSH_MOR
    //                ,A.PRSTZ_RID_INI
    //                ,A.FL_CAR_CONT
    //                ,A.BNS_GIA_LIQTO
    //                ,A.IMP_BNS
    //                ,A.COD_DVS
    //                ,A.PRSTZ_INI_NEWFIS
    //                ,A.IMP_SCON
    //                ,A.ALQ_SCON
    //                ,A.IMP_CAR_ACQ
    //                ,A.IMP_CAR_INC
    //                ,A.IMP_CAR_GEST
    //                ,A.ETA_AA_1O_ASSTO
    //                ,A.ETA_MM_1O_ASSTO
    //                ,A.ETA_AA_2O_ASSTO
    //                ,A.ETA_MM_2O_ASSTO
    //                ,A.ETA_AA_3O_ASSTO
    //                ,A.ETA_MM_3O_ASSTO
    //                ,A.RENDTO_LRD
    //                ,A.PC_RETR
    //                ,A.RENDTO_RETR
    //                ,A.MIN_GARTO
    //                ,A.MIN_TRNUT
    //                ,A.PRE_ATT_DI_TRCH
    //                ,A.MATU_END2000
    //                ,A.ABB_TOT_INI
    //                ,A.ABB_TOT_ULT
    //                ,A.ABB_ANNU_ULT
    //                ,A.DUR_ABB
    //                ,A.TP_ADEG_ABB
    //                ,A.MOD_CALC
    //                ,A.IMP_AZ
    //                ,A.IMP_ADER
    //                ,A.IMP_TFR
    //                ,A.IMP_VOLO
    //                ,A.VIS_END2000
    //                ,A.DT_VLDT_PROD
    //                ,A.DT_INI_VAL_TAR
    //                ,A.IMPB_VIS_END2000
    //                ,A.REN_INI_TS_TEC_0
    //                ,A.PC_RIP_PRE
    //                ,A.FL_IMPORTI_FORZ
    //                ,A.PRSTZ_INI_NFORZ
    //                ,A.VIS_END2000_NFORZ
    //                ,A.INTR_MORA
    //                ,A.MANFEE_ANTIC
    //                ,A.MANFEE_RICOR
    //                ,A.PRE_UNI_RIVTO
    //                ,A.PROV_1AA_ACQ
    //                ,A.PROV_2AA_ACQ
    //                ,A.PROV_RICOR
    //                ,A.PROV_INC
    //                ,A.ALQ_PROV_ACQ
    //                ,A.ALQ_PROV_INC
    //                ,A.ALQ_PROV_RICOR
    //                ,A.IMPB_PROV_ACQ
    //                ,A.IMPB_PROV_INC
    //                ,A.IMPB_PROV_RICOR
    //                ,A.FL_PROV_FORZ
    //                ,A.PRSTZ_AGG_INI
    //                ,A.INCR_PRE
    //                ,A.INCR_PRSTZ
    //                ,A.DT_ULT_ADEG_PRE_PR
    //                ,A.PRSTZ_AGG_ULT
    //                ,A.TS_RIVAL_NET
    //                ,A.PRE_PATTUITO
    //                ,A.TP_RIVAL
    //                ,A.RIS_MAT
    //                ,A.CPT_MIN_SCAD
    //                ,A.COMMIS_GEST
    //                ,A.TP_MANFEE_APPL
    //                ,A.DS_RIGA
    //                ,A.DS_OPER_SQL
    //                ,A.DS_VER
    //                ,A.DS_TS_INI_CPTZ
    //                ,A.DS_TS_END_CPTZ
    //                ,A.DS_UTENTE
    //                ,A.DS_STATO_ELAB
    //                ,A.PC_COMMIS_GEST
    //                ,A.NUM_GG_RIVAL
    //                ,A.IMP_TRASFE
    //                ,A.IMP_TFR_STRC
    //                ,A.ACQ_EXP
    //                ,A.REMUN_ASS
    //                ,A.COMMIS_INTER
    //                ,A.ALQ_REMUN_ASS
    //                ,A.ALQ_COMMIS_INTER
    //                ,A.IMPB_REMUN_ASS
    //                ,A.IMPB_COMMIS_INTER
    //                ,A.COS_RUN_ASSVA
    //                ,A.COS_RUN_ASSVA_IDC
    //                ,B.ID_STAT_OGG_BUS
    //                ,B.ID_OGG
    //                ,B.TP_OGG
    //                ,B.ID_MOVI_CRZ
    //                ,B.ID_MOVI_CHIU
    //                ,B.DT_INI_EFF
    //                ,B.DT_END_EFF
    //                ,B.COD_COMP_ANIA
    //                ,B.TP_STAT_BUS
    //                ,B.TP_CAUS
    //                ,B.DS_RIGA
    //                ,B.DS_OPER_SQL
    //                ,B.DS_VER
    //                ,B.DS_TS_INI_CPTZ
    //                ,B.DS_TS_END_CPTZ
    //                ,B.DS_UTENTE
    //                ,B.DS_STATO_ELAB
    //               FROM     TRCH_DI_GAR A,     STAT_OGG_BUS B
    //             WHERE A.ID_POLI = :LDBI0731-ID-POLI
    //               AND A.ID_ADES = :LDBI0731-ID-ADES
    //               AND A.ID_TRCH_DI_GAR = B.ID_OGG
    //               AND B.TP_OGG = 'TG'
    //               AND B.TP_STAT_BUS = :LDBI0731-TP-STAT-BUS
    //               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //               AND A.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
    //               AND A.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
    //               AND A.DS_TS_INI_CPTZ <=
    //                   :WS-TS-COMPETENZA
    //               AND A.DS_TS_END_CPTZ >
    //                   :WS-TS-COMPETENZA
    //               AND B.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //               AND B.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
    //               AND B.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
    //               AND B.DS_TS_INI_CPTZ <=
    //                   :WS-TS-COMPETENZA
    //               AND B.DS_TS_END_CPTZ >
    //                   :WS-TS-COMPETENZA
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: AB260-OPEN-CURSOR-WC2<br>*/
    private void ab260OpenCursorWc2() {
        // COB_CODE: PERFORM AB250-DECLARE-CURSOR-WC2 THRU AB250-EX.
        ab250DeclareCursorWc2();
        // modificare il nome del cursore
        // COB_CODE: EXEC SQL
        //                OPEN  CUR-WC2-STB
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCurWc2Stb(this.getStatOggBusTrchDiGarLdbs0730());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: AB270-CLOSE-CURSOR-WC2<br>
	 * <pre> modificare il nome del cursore</pre>*/
    private void ab270CloseCursorWc2() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-WC2-STB
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCurWc2Stb();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: AB280-FETCH-FIRST-WC2<br>*/
    private void ab280FetchFirstWc2() {
        // COB_CODE: PERFORM AB260-OPEN-CURSOR-WC2    THRU AB260-EX.
        ab260OpenCursorWc2();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM AB290-FETCH-NEXT-WC2 THRU AB290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM AB290-FETCH-NEXT-WC2 THRU AB290-EX
            ab290FetchNextWc2();
        }
    }

    /**Original name: AB290-FETCH-NEXT-WC2<br>*/
    private void ab290FetchNextWc2() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-WC2-STB
        //           INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //               ,:STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCurWc2Stb(ws.getStatOggBusTrchDiGarLdbs07301());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N-TGA  THRU Z950-EX
            z950ConvertiXToNTga();
            // COB_CODE: PERFORM Z951-CONVERTI-X-TO-N-STB  THRU Z951-EX
            z951ConvertiXToNStb();
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM AB270-CLOSE-CURSOR-WC2 THRU AB270-EX
            ab270CloseCursorWc2();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: AB295-SELECT-WC2<br>*/
    private void ab295SelectWc2() {
        // COB_CODE: EXEC SQL
        //              SELECT
        //                 A.ID_TRCH_DI_GAR
        //                ,A.ID_GAR
        //                ,A.ID_ADES
        //                ,A.ID_POLI
        //                ,A.ID_MOVI_CRZ
        //                ,A.ID_MOVI_CHIU
        //                ,A.DT_INI_EFF
        //                ,A.DT_END_EFF
        //                ,A.COD_COMP_ANIA
        //                ,A.DT_DECOR
        //                ,A.DT_SCAD
        //                ,A.IB_OGG
        //                ,A.TP_RGM_FISC
        //                ,A.DT_EMIS
        //                ,A.TP_TRCH
        //                ,A.DUR_AA
        //                ,A.DUR_MM
        //                ,A.DUR_GG
        //                ,A.PRE_CASO_MOR
        //                ,A.PC_INTR_RIAT
        //                ,A.IMP_BNS_ANTIC
        //                ,A.PRE_INI_NET
        //                ,A.PRE_PP_INI
        //                ,A.PRE_PP_ULT
        //                ,A.PRE_TARI_INI
        //                ,A.PRE_TARI_ULT
        //                ,A.PRE_INVRIO_INI
        //                ,A.PRE_INVRIO_ULT
        //                ,A.PRE_RIVTO
        //                ,A.IMP_SOPR_PROF
        //                ,A.IMP_SOPR_SAN
        //                ,A.IMP_SOPR_SPO
        //                ,A.IMP_SOPR_TEC
        //                ,A.IMP_ALT_SOPR
        //                ,A.PRE_STAB
        //                ,A.DT_EFF_STAB
        //                ,A.TS_RIVAL_FIS
        //                ,A.TS_RIVAL_INDICIZ
        //                ,A.OLD_TS_TEC
        //                ,A.RAT_LRD
        //                ,A.PRE_LRD
        //                ,A.PRSTZ_INI
        //                ,A.PRSTZ_ULT
        //                ,A.CPT_IN_OPZ_RIVTO
        //                ,A.PRSTZ_INI_STAB
        //                ,A.CPT_RSH_MOR
        //                ,A.PRSTZ_RID_INI
        //                ,A.FL_CAR_CONT
        //                ,A.BNS_GIA_LIQTO
        //                ,A.IMP_BNS
        //                ,A.COD_DVS
        //                ,A.PRSTZ_INI_NEWFIS
        //                ,A.IMP_SCON
        //                ,A.ALQ_SCON
        //                ,A.IMP_CAR_ACQ
        //                ,A.IMP_CAR_INC
        //                ,A.IMP_CAR_GEST
        //                ,A.ETA_AA_1O_ASSTO
        //                ,A.ETA_MM_1O_ASSTO
        //                ,A.ETA_AA_2O_ASSTO
        //                ,A.ETA_MM_2O_ASSTO
        //                ,A.ETA_AA_3O_ASSTO
        //                ,A.ETA_MM_3O_ASSTO
        //                ,A.RENDTO_LRD
        //                ,A.PC_RETR
        //                ,A.RENDTO_RETR
        //                ,A.MIN_GARTO
        //                ,A.MIN_TRNUT
        //                ,A.PRE_ATT_DI_TRCH
        //                ,A.MATU_END2000
        //                ,A.ABB_TOT_INI
        //                ,A.ABB_TOT_ULT
        //                ,A.ABB_ANNU_ULT
        //                ,A.DUR_ABB
        //                ,A.TP_ADEG_ABB
        //                ,A.MOD_CALC
        //                ,A.IMP_AZ
        //                ,A.IMP_ADER
        //                ,A.IMP_TFR
        //                ,A.IMP_VOLO
        //                ,A.VIS_END2000
        //                ,A.DT_VLDT_PROD
        //                ,A.DT_INI_VAL_TAR
        //                ,A.IMPB_VIS_END2000
        //                ,A.REN_INI_TS_TEC_0
        //                ,A.PC_RIP_PRE
        //                ,A.FL_IMPORTI_FORZ
        //                ,A.PRSTZ_INI_NFORZ
        //                ,A.VIS_END2000_NFORZ
        //                ,A.INTR_MORA
        //                ,A.MANFEE_ANTIC
        //                ,A.MANFEE_RICOR
        //                ,A.PRE_UNI_RIVTO
        //                ,A.PROV_1AA_ACQ
        //                ,A.PROV_2AA_ACQ
        //                ,A.PROV_RICOR
        //                ,A.PROV_INC
        //                ,A.ALQ_PROV_ACQ
        //                ,A.ALQ_PROV_INC
        //                ,A.ALQ_PROV_RICOR
        //                ,A.IMPB_PROV_ACQ
        //                ,A.IMPB_PROV_INC
        //                ,A.IMPB_PROV_RICOR
        //                ,A.FL_PROV_FORZ
        //                ,A.PRSTZ_AGG_INI
        //                ,A.INCR_PRE
        //                ,A.INCR_PRSTZ
        //                ,A.DT_ULT_ADEG_PRE_PR
        //                ,A.PRSTZ_AGG_ULT
        //                ,A.TS_RIVAL_NET
        //                ,A.PRE_PATTUITO
        //                ,A.TP_RIVAL
        //                ,A.RIS_MAT
        //                ,A.CPT_MIN_SCAD
        //                ,A.COMMIS_GEST
        //                ,A.TP_MANFEE_APPL
        //                ,A.DS_RIGA
        //                ,A.DS_OPER_SQL
        //                ,A.DS_VER
        //                ,A.DS_TS_INI_CPTZ
        //                ,A.DS_TS_END_CPTZ
        //                ,A.DS_UTENTE
        //                ,A.DS_STATO_ELAB
        //                ,A.PC_COMMIS_GEST
        //                ,A.NUM_GG_RIVAL
        //                ,A.IMP_TRASFE
        //                ,A.IMP_TFR_STRC
        //                ,A.ACQ_EXP
        //                ,A.REMUN_ASS
        //                ,A.COMMIS_INTER
        //                ,A.ALQ_REMUN_ASS
        //                ,A.ALQ_COMMIS_INTER
        //                ,A.IMPB_REMUN_ASS
        //                ,A.IMPB_COMMIS_INTER
        //                ,A.COS_RUN_ASSVA
        //                ,A.COS_RUN_ASSVA_IDC
        //                ,B.ID_STAT_OGG_BUS
        //                ,B.ID_OGG
        //                ,B.TP_OGG
        //                ,B.ID_MOVI_CRZ
        //                ,B.ID_MOVI_CHIU
        //                ,B.DT_INI_EFF
        //                ,B.DT_END_EFF
        //                ,B.COD_COMP_ANIA
        //                ,B.TP_STAT_BUS
        //                ,B.TP_CAUS
        //                ,B.DS_RIGA
        //                ,B.DS_OPER_SQL
        //                ,B.DS_VER
        //                ,B.DS_TS_INI_CPTZ
        //                ,B.DS_TS_END_CPTZ
        //                ,B.DS_UTENTE
        //                ,B.DS_STATO_ELAB
        //             INTO
        //                :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-RIS-MAT
        //                :IND-TGA-RIS-MAT
        //               ,:TGA-CPT-MIN-SCAD
        //                :IND-TGA-CPT-MIN-SCAD
        //               ,:TGA-COMMIS-GEST
        //                :IND-TGA-COMMIS-GEST
        //               ,:TGA-TP-MANFEE-APPL
        //                :IND-TGA-TP-MANFEE-APPL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //               ,:STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //             FROM     TRCH_DI_GAR A,     STAT_OGG_BUS B
        //             WHERE A.ID_TRCH_DI_GAR = :LDBI0731-ID-TRCH
        //               AND A.ID_TRCH_DI_GAR = B.ID_OGG
        //               AND B.TP_OGG = 'TG'
        //               AND B.TP_STAT_BUS = :LDBI0731-TP-STAT-BUS
        //               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND A.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
        //               AND A.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //               AND A.DS_TS_INI_CPTZ <=
        //                   :WS-TS-COMPETENZA
        //               AND A.DS_TS_END_CPTZ >
        //                   :WS-TS-COMPETENZA
        //               AND B.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND B.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
        //               AND B.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //               AND B.DS_TS_INI_CPTZ <=
        //                   :WS-TS-COMPETENZA
        //               AND B.DS_TS_END_CPTZ >
        //                   :WS-TS-COMPETENZA
        //           END-EXEC.
        statOggBusTrchDiGarDao.selectRec(this.getStatOggBusTrchDiGarLdbs07302());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N-TGA  THRU Z950-EX
            z950ConvertiXToNTga();
            // COB_CODE: PERFORM Z951-CONVERTI-X-TO-N-STB  THRU Z951-EX
            z951ConvertiXToNStb();
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
    }

    /**Original name: A208-ELABORA-WC03<br>*/
    private void a208ElaboraWc03() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM AA360-OPEN-CURSOR-WC3         THRU AA360-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM AA370-CLOSE-CURSOR-WC3        THRU AA370-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM AA380-FETCH-FIRST-WC3         THRU AA380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM AA390-FETCH-NEXT-WC3          THRU AA390-EX
        //              WHEN IDSV0003-SELECT
        //                 PERFORM AA295-SELECT-WC1              THRU AA295-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM AA360-OPEN-CURSOR-WC3         THRU AA360-EX
            aa360OpenCursorWc3();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM AA370-CLOSE-CURSOR-WC3        THRU AA370-EX
            aa370CloseCursorWc3();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM AA380-FETCH-FIRST-WC3         THRU AA380-EX
            aa380FetchFirstWc3();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM AA390-FETCH-NEXT-WC3          THRU AA390-EX
            aa390FetchNextWc3();
        }
        else if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM AA295-SELECT-WC1              THRU AA295-EX
            aa295SelectWc1();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A209-ELABORA-WC04<br>*/
    private void a209ElaboraWc04() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM AB360-OPEN-CURSOR-WC4         THRU AB360-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM AB370-CLOSE-CURSOR-WC4        THRU AB370-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM AB380-FETCH-FIRST-WC4         THRU AB380-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM AB390-FETCH-NEXT-WC4          THRU AB390-EX
        //              WHEN IDSV0003-SELECT
        //                 PERFORM AB295-SELECT-WC2              THRU AB295-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM AB360-OPEN-CURSOR-WC4         THRU AB360-EX
            ab360OpenCursorWc4();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM AB370-CLOSE-CURSOR-WC4        THRU AB370-EX
            ab370CloseCursorWc4();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM AB380-FETCH-FIRST-WC4         THRU AB380-EX
            ab380FetchFirstWc4();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM AB390-FETCH-NEXT-WC4          THRU AB390-EX
            ab390FetchNextWc4();
        }
        else if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM AB295-SELECT-WC2              THRU AB295-EX
            ab295SelectWc2();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: AA350-DECLARE-CURSOR-WC3<br>
	 * <pre>----
	 * ----  gestione WHERE CONDITION 3 PER TRASFORMAZIONE
	 * ----</pre>*/
    private void aa350DeclareCursorWc3() {
    // COB_CODE: EXEC SQL
    //                 DECLARE CUR-WC3-STB CURSOR FOR
    //              SELECT
    //                 A.ID_POLI
    //                ,B.ID_STAT_OGG_BUS
    //                ,B.ID_OGG
    //                ,B.TP_OGG
    //                ,B.ID_MOVI_CRZ
    //                ,B.ID_MOVI_CHIU
    //                ,B.DT_INI_EFF
    //                ,B.DT_END_EFF
    //                ,B.COD_COMP_ANIA
    //                ,B.TP_STAT_BUS
    //                ,B.TP_CAUS
    //                ,B.DS_RIGA
    //                ,B.DS_OPER_SQL
    //                ,B.DS_VER
    //                ,B.DS_TS_INI_CPTZ
    //                ,B.DS_TS_END_CPTZ
    //                ,B.DS_UTENTE
    //                ,B.DS_STATO_ELAB
    //               FROM     ADES A,     STAT_OGG_BUS B
    //             WHERE A.ID_POLI = :LDBI0731-ID-POLI
    //               AND A.ID_ADES = B.ID_OGG
    //               AND B.TP_OGG = 'AD'
    //               AND B.TP_STAT_BUS = :LDBI0731-TP-STAT-BUS
    //               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //               AND A.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
    //               AND A.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
    //               AND A.DS_TS_INI_CPTZ <=
    //                   :WS-TS-COMPETENZA
    //               AND A.DS_TS_END_CPTZ >
    //                   :WS-TS-COMPETENZA
    //               AND B.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //               AND B.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
    //               AND B.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
    //               AND B.DS_TS_INI_CPTZ <=
    //                   :WS-TS-COMPETENZA
    //               AND B.DS_TS_END_CPTZ >
    //                   :WS-TS-COMPETENZA
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: AA360-OPEN-CURSOR-WC3<br>*/
    private void aa360OpenCursorWc3() {
        // COB_CODE: PERFORM AA350-DECLARE-CURSOR-WC3 THRU AA350-EX.
        aa350DeclareCursorWc3();
        // modificare il nome del cursore
        // COB_CODE: EXEC SQL
        //                OPEN  CUR-WC3-STB
        //           END-EXEC.
        adesStatOggBusDao.openCurWc1Stb(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: AA370-CLOSE-CURSOR-WC3<br>
	 * <pre> modificare il nome del cursore</pre>*/
    private void aa370CloseCursorWc3() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-WC3-STB
        //           END-EXEC.
        adesStatOggBusDao.closeCurWc1Stb();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: AA380-FETCH-FIRST-WC3<br>*/
    private void aa380FetchFirstWc3() {
        // COB_CODE: PERFORM AA360-OPEN-CURSOR-WC3    THRU AA360-EX.
        aa360OpenCursorWc3();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM AA390-FETCH-NEXT-WC3 THRU AA390-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM AA390-FETCH-NEXT-WC3 THRU AA390-EX
            aa390FetchNextWc3();
        }
    }

    /**Original name: AA390-FETCH-NEXT-WC3<br>*/
    private void aa390FetchNextWc3() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-WC3-STB
        //           INTO
        //                :ADE-ID-POLI
        //               ,:STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        adesStatOggBusDao.fetchCurWc1Stb(ws);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z951-CONVERTI-X-TO-N-STB  THRU Z951-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z951-CONVERTI-X-TO-N-STB  THRU Z951-EX
            z951ConvertiXToNStb();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM AA370-CLOSE-CURSOR-WC3 THRU AA370-EX
            aa370CloseCursorWc3();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: AB350-DECLARE-CURSOR-WC4<br>
	 * <pre>----
	 * ----  gestione WHERE CONDITION 4 PER TRASFORMAZIONE
	 * ----</pre>*/
    private void ab350DeclareCursorWc4() {
    // COB_CODE: EXEC SQL
    //                 DECLARE CUR-WC4-STB CURSOR FOR
    //              SELECT
    //                 A.ID_TRCH_DI_GAR
    //                ,A.ID_GAR
    //                ,A.ID_ADES
    //                ,A.ID_POLI
    //                ,A.ID_MOVI_CRZ
    //                ,A.ID_MOVI_CHIU
    //                ,A.DT_INI_EFF
    //                ,A.DT_END_EFF
    //                ,A.COD_COMP_ANIA
    //                ,A.DT_DECOR
    //                ,A.DT_SCAD
    //                ,A.IB_OGG
    //                ,A.TP_RGM_FISC
    //                ,A.DT_EMIS
    //                ,A.TP_TRCH
    //                ,A.DUR_AA
    //                ,A.DUR_MM
    //                ,A.DUR_GG
    //                ,A.PRE_CASO_MOR
    //                ,A.PC_INTR_RIAT
    //                ,A.IMP_BNS_ANTIC
    //                ,A.PRE_INI_NET
    //                ,A.PRE_PP_INI
    //                ,A.PRE_PP_ULT
    //                ,A.PRE_TARI_INI
    //                ,A.PRE_TARI_ULT
    //                ,A.PRE_INVRIO_INI
    //                ,A.PRE_INVRIO_ULT
    //                ,A.PRE_RIVTO
    //                ,A.IMP_SOPR_PROF
    //                ,A.IMP_SOPR_SAN
    //                ,A.IMP_SOPR_SPO
    //                ,A.IMP_SOPR_TEC
    //                ,A.IMP_ALT_SOPR
    //                ,A.PRE_STAB
    //                ,A.DT_EFF_STAB
    //                ,A.TS_RIVAL_FIS
    //                ,A.TS_RIVAL_INDICIZ
    //                ,A.OLD_TS_TEC
    //                ,A.RAT_LRD
    //                ,A.PRE_LRD
    //                ,A.PRSTZ_INI
    //                ,A.PRSTZ_ULT
    //                ,A.CPT_IN_OPZ_RIVTO
    //                ,A.PRSTZ_INI_STAB
    //                ,A.CPT_RSH_MOR
    //                ,A.PRSTZ_RID_INI
    //                ,A.FL_CAR_CONT
    //                ,A.BNS_GIA_LIQTO
    //                ,A.IMP_BNS
    //                ,A.COD_DVS
    //                ,A.PRSTZ_INI_NEWFIS
    //                ,A.IMP_SCON
    //                ,A.ALQ_SCON
    //                ,A.IMP_CAR_ACQ
    //                ,A.IMP_CAR_INC
    //                ,A.IMP_CAR_GEST
    //                ,A.ETA_AA_1O_ASSTO
    //                ,A.ETA_MM_1O_ASSTO
    //                ,A.ETA_AA_2O_ASSTO
    //                ,A.ETA_MM_2O_ASSTO
    //                ,A.ETA_AA_3O_ASSTO
    //                ,A.ETA_MM_3O_ASSTO
    //                ,A.RENDTO_LRD
    //                ,A.PC_RETR
    //                ,A.RENDTO_RETR
    //                ,A.MIN_GARTO
    //                ,A.MIN_TRNUT
    //                ,A.PRE_ATT_DI_TRCH
    //                ,A.MATU_END2000
    //                ,A.ABB_TOT_INI
    //                ,A.ABB_TOT_ULT
    //                ,A.ABB_ANNU_ULT
    //                ,A.DUR_ABB
    //                ,A.TP_ADEG_ABB
    //                ,A.MOD_CALC
    //                ,A.IMP_AZ
    //                ,A.IMP_ADER
    //                ,A.IMP_TFR
    //                ,A.IMP_VOLO
    //                ,A.VIS_END2000
    //                ,A.DT_VLDT_PROD
    //                ,A.DT_INI_VAL_TAR
    //                ,A.IMPB_VIS_END2000
    //                ,A.REN_INI_TS_TEC_0
    //                ,A.PC_RIP_PRE
    //                ,A.FL_IMPORTI_FORZ
    //                ,A.PRSTZ_INI_NFORZ
    //                ,A.VIS_END2000_NFORZ
    //                ,A.INTR_MORA
    //                ,A.MANFEE_ANTIC
    //                ,A.MANFEE_RICOR
    //                ,A.PRE_UNI_RIVTO
    //                ,A.PROV_1AA_ACQ
    //                ,A.PROV_2AA_ACQ
    //                ,A.PROV_RICOR
    //                ,A.PROV_INC
    //                ,A.ALQ_PROV_ACQ
    //                ,A.ALQ_PROV_INC
    //                ,A.ALQ_PROV_RICOR
    //                ,A.IMPB_PROV_ACQ
    //                ,A.IMPB_PROV_INC
    //                ,A.IMPB_PROV_RICOR
    //                ,A.FL_PROV_FORZ
    //                ,A.PRSTZ_AGG_INI
    //                ,A.INCR_PRE
    //                ,A.INCR_PRSTZ
    //                ,A.DT_ULT_ADEG_PRE_PR
    //                ,A.PRSTZ_AGG_ULT
    //                ,A.TS_RIVAL_NET
    //                ,A.PRE_PATTUITO
    //                ,A.TP_RIVAL
    //                ,A.RIS_MAT
    //                ,A.CPT_MIN_SCAD
    //                ,A.COMMIS_GEST
    //                ,A.TP_MANFEE_APPL
    //                ,A.DS_RIGA
    //                ,A.DS_OPER_SQL
    //                ,A.DS_VER
    //                ,A.DS_TS_INI_CPTZ
    //                ,A.DS_TS_END_CPTZ
    //                ,A.DS_UTENTE
    //                ,A.DS_STATO_ELAB
    //                ,A.PC_COMMIS_GEST
    //                ,A.NUM_GG_RIVAL
    //                ,A.IMP_TRASFE
    //                ,A.IMP_TFR_STRC
    //                ,A.ACQ_EXP
    //                ,A.REMUN_ASS
    //                ,A.COMMIS_INTER
    //                ,A.ALQ_REMUN_ASS
    //                ,A.ALQ_COMMIS_INTER
    //                ,A.IMPB_REMUN_ASS
    //                ,A.IMPB_COMMIS_INTER
    //                ,A.COS_RUN_ASSVA
    //                ,A.COS_RUN_ASSVA_IDC
    //                ,B.ID_STAT_OGG_BUS
    //                ,B.ID_OGG
    //                ,B.TP_OGG
    //                ,B.ID_MOVI_CRZ
    //                ,B.ID_MOVI_CHIU
    //                ,B.DT_INI_EFF
    //                ,B.DT_END_EFF
    //                ,B.COD_COMP_ANIA
    //                ,B.TP_STAT_BUS
    //                ,B.TP_CAUS
    //                ,B.DS_RIGA
    //                ,B.DS_OPER_SQL
    //                ,B.DS_VER
    //                ,B.DS_TS_INI_CPTZ
    //                ,B.DS_TS_END_CPTZ
    //                ,B.DS_UTENTE
    //                ,B.DS_STATO_ELAB
    //               FROM     TRCH_DI_GAR A,     STAT_OGG_BUS B
    //             WHERE A.ID_POLI = :LDBI0731-ID-POLI
    //               AND A.ID_ADES = :LDBI0731-ID-ADES
    //               AND A.ID_TRCH_DI_GAR = B.ID_OGG
    //               AND B.TP_OGG = 'TG'
    //               AND B.TP_STAT_BUS = :LDBI0731-TP-STAT-BUS
    //               AND A.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //               AND A.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
    //               AND A.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
    //               AND A.DS_TS_INI_CPTZ <=
    //                   :WS-TS-COMPETENZA
    //               AND A.DS_TS_END_CPTZ >
    //                   :WS-TS-COMPETENZA
    //               AND B.COD_COMP_ANIA = :IDSV0003-CODICE-COMPAGNIA-ANIA
    //               AND B.DT_INI_EFF <= :WS-DATA-INIZIO-EFFETTO-DB
    //               AND B.DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
    //               AND B.DS_TS_INI_CPTZ <=
    //                   :WS-TS-COMPETENZA
    //               AND B.DS_TS_END_CPTZ >
    //                   :WS-TS-COMPETENZA
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: AB360-OPEN-CURSOR-WC4<br>*/
    private void ab360OpenCursorWc4() {
        // COB_CODE: PERFORM AB350-DECLARE-CURSOR-WC4 THRU AB350-EX.
        ab350DeclareCursorWc4();
        // modificare il nome del cursore
        // COB_CODE: EXEC SQL
        //                OPEN  CUR-WC4-STB
        //           END-EXEC.
        statOggBusTrchDiGarDao.openCurWc2Stb(this.getStatOggBusTrchDiGarLdbs0730());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: AB370-CLOSE-CURSOR-WC4<br>
	 * <pre> modificare il nome del cursore</pre>*/
    private void ab370CloseCursorWc4() {
        // COB_CODE: EXEC SQL
        //                CLOSE CUR-WC4-STB
        //           END-EXEC.
        statOggBusTrchDiGarDao.closeCurWc2Stb();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: AB380-FETCH-FIRST-WC4<br>*/
    private void ab380FetchFirstWc4() {
        // COB_CODE: PERFORM AB360-OPEN-CURSOR-WC4    THRU AB360-EX.
        ab360OpenCursorWc4();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM AB390-FETCH-NEXT-WC4 THRU AB390-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM AB390-FETCH-NEXT-WC4 THRU AB390-EX
            ab390FetchNextWc4();
        }
    }

    /**Original name: AB390-FETCH-NEXT-WC4<br>*/
    private void ab390FetchNextWc4() {
        // COB_CODE: EXEC SQL
        //                FETCH CUR-WC4-STB
        //           INTO
        //               :TGA-ID-TRCH-DI-GAR
        //               ,:TGA-ID-GAR
        //               ,:TGA-ID-ADES
        //               ,:TGA-ID-POLI
        //               ,:TGA-ID-MOVI-CRZ
        //               ,:TGA-ID-MOVI-CHIU
        //                :IND-TGA-ID-MOVI-CHIU
        //               ,:TGA-DT-INI-EFF-DB
        //               ,:TGA-DT-END-EFF-DB
        //               ,:TGA-COD-COMP-ANIA
        //               ,:TGA-DT-DECOR-DB
        //               ,:TGA-DT-SCAD-DB
        //                :IND-TGA-DT-SCAD
        //               ,:TGA-IB-OGG
        //                :IND-TGA-IB-OGG
        //               ,:TGA-TP-RGM-FISC
        //               ,:TGA-DT-EMIS-DB
        //                :IND-TGA-DT-EMIS
        //               ,:TGA-TP-TRCH
        //               ,:TGA-DUR-AA
        //                :IND-TGA-DUR-AA
        //               ,:TGA-DUR-MM
        //                :IND-TGA-DUR-MM
        //               ,:TGA-DUR-GG
        //                :IND-TGA-DUR-GG
        //               ,:TGA-PRE-CASO-MOR
        //                :IND-TGA-PRE-CASO-MOR
        //               ,:TGA-PC-INTR-RIAT
        //                :IND-TGA-PC-INTR-RIAT
        //               ,:TGA-IMP-BNS-ANTIC
        //                :IND-TGA-IMP-BNS-ANTIC
        //               ,:TGA-PRE-INI-NET
        //                :IND-TGA-PRE-INI-NET
        //               ,:TGA-PRE-PP-INI
        //                :IND-TGA-PRE-PP-INI
        //               ,:TGA-PRE-PP-ULT
        //                :IND-TGA-PRE-PP-ULT
        //               ,:TGA-PRE-TARI-INI
        //                :IND-TGA-PRE-TARI-INI
        //               ,:TGA-PRE-TARI-ULT
        //                :IND-TGA-PRE-TARI-ULT
        //               ,:TGA-PRE-INVRIO-INI
        //                :IND-TGA-PRE-INVRIO-INI
        //               ,:TGA-PRE-INVRIO-ULT
        //                :IND-TGA-PRE-INVRIO-ULT
        //               ,:TGA-PRE-RIVTO
        //                :IND-TGA-PRE-RIVTO
        //               ,:TGA-IMP-SOPR-PROF
        //                :IND-TGA-IMP-SOPR-PROF
        //               ,:TGA-IMP-SOPR-SAN
        //                :IND-TGA-IMP-SOPR-SAN
        //               ,:TGA-IMP-SOPR-SPO
        //                :IND-TGA-IMP-SOPR-SPO
        //               ,:TGA-IMP-SOPR-TEC
        //                :IND-TGA-IMP-SOPR-TEC
        //               ,:TGA-IMP-ALT-SOPR
        //                :IND-TGA-IMP-ALT-SOPR
        //               ,:TGA-PRE-STAB
        //                :IND-TGA-PRE-STAB
        //               ,:TGA-DT-EFF-STAB-DB
        //                :IND-TGA-DT-EFF-STAB
        //               ,:TGA-TS-RIVAL-FIS
        //                :IND-TGA-TS-RIVAL-FIS
        //               ,:TGA-TS-RIVAL-INDICIZ
        //                :IND-TGA-TS-RIVAL-INDICIZ
        //               ,:TGA-OLD-TS-TEC
        //                :IND-TGA-OLD-TS-TEC
        //               ,:TGA-RAT-LRD
        //                :IND-TGA-RAT-LRD
        //               ,:TGA-PRE-LRD
        //                :IND-TGA-PRE-LRD
        //               ,:TGA-PRSTZ-INI
        //                :IND-TGA-PRSTZ-INI
        //               ,:TGA-PRSTZ-ULT
        //                :IND-TGA-PRSTZ-ULT
        //               ,:TGA-CPT-IN-OPZ-RIVTO
        //                :IND-TGA-CPT-IN-OPZ-RIVTO
        //               ,:TGA-PRSTZ-INI-STAB
        //                :IND-TGA-PRSTZ-INI-STAB
        //               ,:TGA-CPT-RSH-MOR
        //                :IND-TGA-CPT-RSH-MOR
        //               ,:TGA-PRSTZ-RID-INI
        //                :IND-TGA-PRSTZ-RID-INI
        //               ,:TGA-FL-CAR-CONT
        //                :IND-TGA-FL-CAR-CONT
        //               ,:TGA-BNS-GIA-LIQTO
        //                :IND-TGA-BNS-GIA-LIQTO
        //               ,:TGA-IMP-BNS
        //                :IND-TGA-IMP-BNS
        //               ,:TGA-COD-DVS
        //               ,:TGA-PRSTZ-INI-NEWFIS
        //                :IND-TGA-PRSTZ-INI-NEWFIS
        //               ,:TGA-IMP-SCON
        //                :IND-TGA-IMP-SCON
        //               ,:TGA-ALQ-SCON
        //                :IND-TGA-ALQ-SCON
        //               ,:TGA-IMP-CAR-ACQ
        //                :IND-TGA-IMP-CAR-ACQ
        //               ,:TGA-IMP-CAR-INC
        //                :IND-TGA-IMP-CAR-INC
        //               ,:TGA-IMP-CAR-GEST
        //                :IND-TGA-IMP-CAR-GEST
        //               ,:TGA-ETA-AA-1O-ASSTO
        //                :IND-TGA-ETA-AA-1O-ASSTO
        //               ,:TGA-ETA-MM-1O-ASSTO
        //                :IND-TGA-ETA-MM-1O-ASSTO
        //               ,:TGA-ETA-AA-2O-ASSTO
        //                :IND-TGA-ETA-AA-2O-ASSTO
        //               ,:TGA-ETA-MM-2O-ASSTO
        //                :IND-TGA-ETA-MM-2O-ASSTO
        //               ,:TGA-ETA-AA-3O-ASSTO
        //                :IND-TGA-ETA-AA-3O-ASSTO
        //               ,:TGA-ETA-MM-3O-ASSTO
        //                :IND-TGA-ETA-MM-3O-ASSTO
        //               ,:TGA-RENDTO-LRD
        //                :IND-TGA-RENDTO-LRD
        //               ,:TGA-PC-RETR
        //                :IND-TGA-PC-RETR
        //               ,:TGA-RENDTO-RETR
        //                :IND-TGA-RENDTO-RETR
        //               ,:TGA-MIN-GARTO
        //                :IND-TGA-MIN-GARTO
        //               ,:TGA-MIN-TRNUT
        //                :IND-TGA-MIN-TRNUT
        //               ,:TGA-PRE-ATT-DI-TRCH
        //                :IND-TGA-PRE-ATT-DI-TRCH
        //               ,:TGA-MATU-END2000
        //                :IND-TGA-MATU-END2000
        //               ,:TGA-ABB-TOT-INI
        //                :IND-TGA-ABB-TOT-INI
        //               ,:TGA-ABB-TOT-ULT
        //                :IND-TGA-ABB-TOT-ULT
        //               ,:TGA-ABB-ANNU-ULT
        //                :IND-TGA-ABB-ANNU-ULT
        //               ,:TGA-DUR-ABB
        //                :IND-TGA-DUR-ABB
        //               ,:TGA-TP-ADEG-ABB
        //                :IND-TGA-TP-ADEG-ABB
        //               ,:TGA-MOD-CALC
        //                :IND-TGA-MOD-CALC
        //               ,:TGA-IMP-AZ
        //                :IND-TGA-IMP-AZ
        //               ,:TGA-IMP-ADER
        //                :IND-TGA-IMP-ADER
        //               ,:TGA-IMP-TFR
        //                :IND-TGA-IMP-TFR
        //               ,:TGA-IMP-VOLO
        //                :IND-TGA-IMP-VOLO
        //               ,:TGA-VIS-END2000
        //                :IND-TGA-VIS-END2000
        //               ,:TGA-DT-VLDT-PROD-DB
        //                :IND-TGA-DT-VLDT-PROD
        //               ,:TGA-DT-INI-VAL-TAR-DB
        //                :IND-TGA-DT-INI-VAL-TAR
        //               ,:TGA-IMPB-VIS-END2000
        //                :IND-TGA-IMPB-VIS-END2000
        //               ,:TGA-REN-INI-TS-TEC-0
        //                :IND-TGA-REN-INI-TS-TEC-0
        //               ,:TGA-PC-RIP-PRE
        //                :IND-TGA-PC-RIP-PRE
        //               ,:TGA-FL-IMPORTI-FORZ
        //                :IND-TGA-FL-IMPORTI-FORZ
        //               ,:TGA-PRSTZ-INI-NFORZ
        //                :IND-TGA-PRSTZ-INI-NFORZ
        //               ,:TGA-VIS-END2000-NFORZ
        //                :IND-TGA-VIS-END2000-NFORZ
        //               ,:TGA-INTR-MORA
        //                :IND-TGA-INTR-MORA
        //               ,:TGA-MANFEE-ANTIC
        //                :IND-TGA-MANFEE-ANTIC
        //               ,:TGA-MANFEE-RICOR
        //                :IND-TGA-MANFEE-RICOR
        //               ,:TGA-PRE-UNI-RIVTO
        //                :IND-TGA-PRE-UNI-RIVTO
        //               ,:TGA-PROV-1AA-ACQ
        //                :IND-TGA-PROV-1AA-ACQ
        //               ,:TGA-PROV-2AA-ACQ
        //                :IND-TGA-PROV-2AA-ACQ
        //               ,:TGA-PROV-RICOR
        //                :IND-TGA-PROV-RICOR
        //               ,:TGA-PROV-INC
        //                :IND-TGA-PROV-INC
        //               ,:TGA-ALQ-PROV-ACQ
        //                :IND-TGA-ALQ-PROV-ACQ
        //               ,:TGA-ALQ-PROV-INC
        //                :IND-TGA-ALQ-PROV-INC
        //               ,:TGA-ALQ-PROV-RICOR
        //                :IND-TGA-ALQ-PROV-RICOR
        //               ,:TGA-IMPB-PROV-ACQ
        //                :IND-TGA-IMPB-PROV-ACQ
        //               ,:TGA-IMPB-PROV-INC
        //                :IND-TGA-IMPB-PROV-INC
        //               ,:TGA-IMPB-PROV-RICOR
        //                :IND-TGA-IMPB-PROV-RICOR
        //               ,:TGA-FL-PROV-FORZ
        //                :IND-TGA-FL-PROV-FORZ
        //               ,:TGA-PRSTZ-AGG-INI
        //                :IND-TGA-PRSTZ-AGG-INI
        //               ,:TGA-INCR-PRE
        //                :IND-TGA-INCR-PRE
        //               ,:TGA-INCR-PRSTZ
        //                :IND-TGA-INCR-PRSTZ
        //               ,:TGA-DT-ULT-ADEG-PRE-PR-DB
        //                :IND-TGA-DT-ULT-ADEG-PRE-PR
        //               ,:TGA-PRSTZ-AGG-ULT
        //                :IND-TGA-PRSTZ-AGG-ULT
        //               ,:TGA-TS-RIVAL-NET
        //                :IND-TGA-TS-RIVAL-NET
        //               ,:TGA-PRE-PATTUITO
        //                :IND-TGA-PRE-PATTUITO
        //               ,:TGA-TP-RIVAL
        //                :IND-TGA-TP-RIVAL
        //               ,:TGA-DS-RIGA
        //               ,:TGA-DS-OPER-SQL
        //               ,:TGA-DS-VER
        //               ,:TGA-DS-TS-INI-CPTZ
        //               ,:TGA-DS-TS-END-CPTZ
        //               ,:TGA-DS-UTENTE
        //               ,:TGA-DS-STATO-ELAB
        //               ,:TGA-PC-COMMIS-GEST
        //                :IND-TGA-PC-COMMIS-GEST
        //               ,:TGA-NUM-GG-RIVAL
        //                :IND-TGA-NUM-GG-RIVAL
        //               ,:TGA-IMP-TRASFE
        //                :IND-TGA-IMP-TRASFE
        //               ,:TGA-IMP-TFR-STRC
        //                :IND-TGA-IMP-TFR-STRC
        //               ,:TGA-ACQ-EXP
        //                :IND-TGA-ACQ-EXP
        //               ,:TGA-REMUN-ASS
        //                :IND-TGA-REMUN-ASS
        //               ,:TGA-COMMIS-INTER
        //                :IND-TGA-COMMIS-INTER
        //               ,:TGA-ALQ-REMUN-ASS
        //                :IND-TGA-ALQ-REMUN-ASS
        //               ,:TGA-ALQ-COMMIS-INTER
        //                :IND-TGA-ALQ-COMMIS-INTER
        //               ,:TGA-IMPB-REMUN-ASS
        //                :IND-TGA-IMPB-REMUN-ASS
        //               ,:TGA-IMPB-COMMIS-INTER
        //                :IND-TGA-IMPB-COMMIS-INTER
        //               ,:TGA-COS-RUN-ASSVA
        //                :IND-TGA-COS-RUN-ASSVA
        //               ,:TGA-COS-RUN-ASSVA-IDC
        //                :IND-TGA-COS-RUN-ASSVA-IDC
        //               ,:STB-ID-STAT-OGG-BUS
        //               ,:STB-ID-OGG
        //               ,:STB-TP-OGG
        //               ,:STB-ID-MOVI-CRZ
        //               ,:STB-ID-MOVI-CHIU
        //                :IND-STB-ID-MOVI-CHIU
        //               ,:STB-DT-INI-EFF-DB
        //               ,:STB-DT-END-EFF-DB
        //               ,:STB-COD-COMP-ANIA
        //               ,:STB-TP-STAT-BUS
        //               ,:STB-TP-CAUS
        //               ,:STB-DS-RIGA
        //               ,:STB-DS-OPER-SQL
        //               ,:STB-DS-VER
        //               ,:STB-DS-TS-INI-CPTZ
        //               ,:STB-DS-TS-END-CPTZ
        //               ,:STB-DS-UTENTE
        //               ,:STB-DS-STATO-ELAB
        //           END-EXEC.
        statOggBusTrchDiGarDao.fetchCurWc2Stb1(ws.getStatOggBusTrchDiGarLdbs07301());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N-TGA  THRU Z950-EX
            z950ConvertiXToNTga();
            // COB_CODE: PERFORM Z951-CONVERTI-X-TO-N-STB  THRU Z951-EX
            z951ConvertiXToNStb();
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM AB370-CLOSE-CURSOR-WC4 THRU AB370-EX
            ab370CloseCursorWc4();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: INITIALIZE AREA-LDBO0731.
        initAreaLdbo0731();
        // COB_CODE: IF IND-STB-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
        //           END-IF.
        if (ws.getIndStbIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO STB-ID-MOVI-CHIU-NULL
            ws.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, StbIdMoviChiu.Len.STB_ID_MOVI_CHIU_NULL));
        }
        //
        // COB_CODE: EVALUATE LDBI0731-TP-CALL
        //               WHEN '01'
        //                   TO LDBO0731-ID-PADRE
        //               WHEN '02'
        //                     THRU Z102-EX
        //           END-EVALUATE.
        switch (ws.getLdbi0731().getTpCall()) {

            case "01":// COB_CODE: MOVE ADE-ID-POLI
                //             TO LDBO0731-ID-PADRE
                ldbo0731.setLdbo0731IdPadre(ws.getAdes().getAdeIdPoli());
                break;

            case "02":// COB_CODE: MOVE TGA-ID-GAR
                //             TO LDBO0731-ID-PADRE
                ldbo0731.setLdbo0731IdPadre(ws.getTrchDiGar().getTgaIdGar());
                //
                // COB_CODE: PERFORM Z101-SET-NULL-TGA
                //              THRU Z101-EX
                z101SetNullTga();
                //
                // COB_CODE: PERFORM Z102-VALORIZZA-TGA
                //              THRU Z102-EX
                z102ValorizzaTga();
                break;

            default:break;
        }
        // COB_CODE: MOVE STAT-OGG-BUS
        //             TO LDBO0731-STB.
        ldbo0731.setLdbo0731Stb(ws.getStatOggBus().getStatOggBusFormatted());
    }

    /**Original name: Z101-SET-NULL-TGA<br>*/
    private void z101SetNullTga() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TGA-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TGA-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ID-MOVI-CHIU-NULL
            ws.getTrchDiGar().getTgaIdMoviChiu().setTgaIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIdMoviChiu.Len.TGA_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO TGA-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-SCAD-NULL
            ws.getTrchDiGar().getTgaDtScad().setTgaDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtScad.Len.TGA_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-TGA-IB-OGG = -1
        //              MOVE HIGH-VALUES TO TGA-IB-OGG-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IB-OGG-NULL
            ws.getTrchDiGar().setTgaIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGarIvvs0216.Len.TGA_IB_OGG));
        }
        // COB_CODE: IF IND-TGA-DT-EMIS = -1
        //              MOVE HIGH-VALUES TO TGA-DT-EMIS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEmis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-EMIS-NULL
            ws.getTrchDiGar().getTgaDtEmis().setTgaDtEmisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtEmis.Len.TGA_DT_EMIS_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-AA = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-AA-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-AA-NULL
            ws.getTrchDiGar().getTgaDurAa().setTgaDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurAa.Len.TGA_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-MM = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-MM-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-MM-NULL
            ws.getTrchDiGar().getTgaDurMm().setTgaDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurMm.Len.TGA_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-GG = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-GG-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-GG-NULL
            ws.getTrchDiGar().getTgaDurGg().setTgaDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurGg.Len.TGA_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-CASO-MOR = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-CASO-MOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreCasoMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-CASO-MOR-NULL
            ws.getTrchDiGar().getTgaPreCasoMor().setTgaPreCasoMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreCasoMor.Len.TGA_PRE_CASO_MOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PC-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO TGA-PC-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-INTR-RIAT-NULL
            ws.getTrchDiGar().getTgaPcIntrRiat().setTgaPcIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcIntrRiat.Len.TGA_PC_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-BNS-ANTIC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-BNS-ANTIC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpBnsAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-BNS-ANTIC-NULL
            ws.getTrchDiGar().getTgaImpBnsAntic().setTgaImpBnsAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpBnsAntic.Len.TGA_IMP_BNS_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-INI-NET = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-INI-NET-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreIniNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-INI-NET-NULL
            ws.getTrchDiGar().getTgaPreIniNet().setTgaPreIniNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreIniNet.Len.TGA_PRE_INI_NET_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-PP-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-PP-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrePpIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-PP-INI-NULL
            ws.getTrchDiGar().getTgaPrePpIni().setTgaPrePpIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrePpIni.Len.TGA_PRE_PP_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-PP-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-PP-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrePpUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-PP-ULT-NULL
            ws.getTrchDiGar().getTgaPrePpUlt().setTgaPrePpUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrePpUlt.Len.TGA_PRE_PP_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-TARI-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-TARI-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreTariIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-TARI-INI-NULL
            ws.getTrchDiGar().getTgaPreTariIni().setTgaPreTariIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreTariIni.Len.TGA_PRE_TARI_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-TARI-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-TARI-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreTariUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-TARI-ULT-NULL
            ws.getTrchDiGar().getTgaPreTariUlt().setTgaPreTariUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreTariUlt.Len.TGA_PRE_TARI_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-INVRIO-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreInvrioIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-INVRIO-INI-NULL
            ws.getTrchDiGar().getTgaPreInvrioIni().setTgaPreInvrioIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreInvrioIni.Len.TGA_PRE_INVRIO_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-INVRIO-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-INVRIO-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreInvrioUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-INVRIO-ULT-NULL
            ws.getTrchDiGar().getTgaPreInvrioUlt().setTgaPreInvrioUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreInvrioUlt.Len.TGA_PRE_INVRIO_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-RIVTO = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-RIVTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreRivto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-RIVTO-NULL
            ws.getTrchDiGar().getTgaPreRivto().setTgaPreRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreRivto.Len.TGA_PRE_RIVTO_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-PROF-NULL
            ws.getTrchDiGar().getTgaImpSoprProf().setTgaImpSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprProf.Len.TGA_IMP_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-SAN-NULL
            ws.getTrchDiGar().getTgaImpSoprSan().setTgaImpSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprSan.Len.TGA_IMP_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-SPO-NULL
            ws.getTrchDiGar().getTgaImpSoprSpo().setTgaImpSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprSpo.Len.TGA_IMP_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SOPR-TEC-NULL
            ws.getTrchDiGar().getTgaImpSoprTec().setTgaImpSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpSoprTec.Len.TGA_IMP_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-ALT-SOPR = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-ALT-SOPR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpAltSopr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-ALT-SOPR-NULL
            ws.getTrchDiGar().getTgaImpAltSopr().setTgaImpAltSoprNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpAltSopr.Len.TGA_IMP_ALT_SOPR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-STAB = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-STAB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-STAB-NULL
            ws.getTrchDiGar().getTgaPreStab().setTgaPreStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreStab.Len.TGA_PRE_STAB_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-EFF-STAB = -1
        //              MOVE HIGH-VALUES TO TGA-DT-EFF-STAB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEffStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-EFF-STAB-NULL
            ws.getTrchDiGar().getTgaDtEffStab().setTgaDtEffStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtEffStab.Len.TGA_DT_EFF_STAB_NULL));
        }
        // COB_CODE: IF IND-TGA-TS-RIVAL-FIS = -1
        //              MOVE HIGH-VALUES TO TGA-TS-RIVAL-FIS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTsRivalFis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TS-RIVAL-FIS-NULL
            ws.getTrchDiGar().getTgaTsRivalFis().setTgaTsRivalFisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaTsRivalFis.Len.TGA_TS_RIVAL_FIS_NULL));
        }
        // COB_CODE: IF IND-TGA-TS-RIVAL-INDICIZ = -1
        //              MOVE HIGH-VALUES TO TGA-TS-RIVAL-INDICIZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTsRivalIndiciz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TS-RIVAL-INDICIZ-NULL
            ws.getTrchDiGar().getTgaTsRivalIndiciz().setTgaTsRivalIndicizNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaTsRivalIndiciz.Len.TGA_TS_RIVAL_INDICIZ_NULL));
        }
        // COB_CODE: IF IND-TGA-OLD-TS-TEC = -1
        //              MOVE HIGH-VALUES TO TGA-OLD-TS-TEC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getOldTsTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-OLD-TS-TEC-NULL
            ws.getTrchDiGar().getTgaOldTsTec().setTgaOldTsTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaOldTsTec.Len.TGA_OLD_TS_TEC_NULL));
        }
        // COB_CODE: IF IND-TGA-RAT-LRD = -1
        //              MOVE HIGH-VALUES TO TGA-RAT-LRD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRatLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RAT-LRD-NULL
            ws.getTrchDiGar().getTgaRatLrd().setTgaRatLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRatLrd.Len.TGA_RAT_LRD_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-LRD = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-LRD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-LRD-NULL
            ws.getTrchDiGar().getTgaPreLrd().setTgaPreLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreLrd.Len.TGA_PRE_LRD_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NULL
            ws.getTrchDiGar().getTgaPrstzIni().setTgaPrstzIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIni.Len.TGA_PRSTZ_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-ULT-NULL
            ws.getTrchDiGar().getTgaPrstzUlt().setTgaPrstzUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzUlt.Len.TGA_PRSTZ_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-CPT-IN-OPZ-RIVTO = -1
        //              MOVE HIGH-VALUES TO TGA-CPT-IN-OPZ-RIVTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCptInOpzRivto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-CPT-IN-OPZ-RIVTO-NULL
            ws.getTrchDiGar().getTgaCptInOpzRivto().setTgaCptInOpzRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCptInOpzRivto.Len.TGA_CPT_IN_OPZ_RIVTO_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI-STAB = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-STAB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIniStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-STAB-NULL
            ws.getTrchDiGar().getTgaPrstzIniStab().setTgaPrstzIniStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIniStab.Len.TGA_PRSTZ_INI_STAB_NULL));
        }
        // COB_CODE: IF IND-TGA-CPT-RSH-MOR = -1
        //              MOVE HIGH-VALUES TO TGA-CPT-RSH-MOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCptRshMor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-CPT-RSH-MOR-NULL
            ws.getTrchDiGar().getTgaCptRshMor().setTgaCptRshMorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCptRshMor.Len.TGA_CPT_RSH_MOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-RID-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-RID-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzRidIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-RID-INI-NULL
            ws.getTrchDiGar().getTgaPrstzRidIni().setTgaPrstzRidIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzRidIni.Len.TGA_PRSTZ_RID_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-FL-CAR-CONT = -1
        //              MOVE HIGH-VALUES TO TGA-FL-CAR-CONT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getFlCarCont() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-FL-CAR-CONT-NULL
            ws.getTrchDiGar().setTgaFlCarCont(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-BNS-GIA-LIQTO = -1
        //              MOVE HIGH-VALUES TO TGA-BNS-GIA-LIQTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getBnsGiaLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-BNS-GIA-LIQTO-NULL
            ws.getTrchDiGar().getTgaBnsGiaLiqto().setTgaBnsGiaLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaBnsGiaLiqto.Len.TGA_BNS_GIA_LIQTO_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-BNS = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-BNS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpBns() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-BNS-NULL
            ws.getTrchDiGar().getTgaImpBns().setTgaImpBnsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpBns.Len.TGA_IMP_BNS_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI-NEWFIS = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NEWFIS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIniNewfis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NEWFIS-NULL
            ws.getTrchDiGar().getTgaPrstzIniNewfis().setTgaPrstzIniNewfisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIniNewfis.Len.TGA_PRSTZ_INI_NEWFIS_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-SCON = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-SCON-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-SCON-NULL
            ws.getTrchDiGar().getTgaImpScon().setTgaImpSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpScon.Len.TGA_IMP_SCON_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-SCON = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-SCON-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqScon() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-SCON-NULL
            ws.getTrchDiGar().getTgaAlqScon().setTgaAlqSconNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqScon.Len.TGA_ALQ_SCON_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-CAR-ACQ-NULL
            ws.getTrchDiGar().getTgaImpCarAcq().setTgaImpCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpCarAcq.Len.TGA_IMP_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-CAR-INC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-CAR-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-CAR-INC-NULL
            ws.getTrchDiGar().getTgaImpCarInc().setTgaImpCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpCarInc.Len.TGA_IMP_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-CAR-GEST-NULL
            ws.getTrchDiGar().getTgaImpCarGest().setTgaImpCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpCarGest.Len.TGA_IMP_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-AA-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-AA-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaAa1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-AA-1O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaAa1oAssto().setTgaEtaAa1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaAa1oAssto.Len.TGA_ETA_AA1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-MM-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-MM-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaMm1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-MM-1O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaMm1oAssto().setTgaEtaMm1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaMm1oAssto.Len.TGA_ETA_MM1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-AA-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-AA-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaAa2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-AA-2O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaAa2oAssto().setTgaEtaAa2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaAa2oAssto.Len.TGA_ETA_AA2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-MM-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-MM-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaMm2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-MM-2O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaMm2oAssto().setTgaEtaMm2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaMm2oAssto.Len.TGA_ETA_MM2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-AA-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-AA-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaAa3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-AA-3O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaAa3oAssto().setTgaEtaAa3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaAa3oAssto.Len.TGA_ETA_AA3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-ETA-MM-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO TGA-ETA-MM-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getEtaMm3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ETA-MM-3O-ASSTO-NULL
            ws.getTrchDiGar().getTgaEtaMm3oAssto().setTgaEtaMm3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaEtaMm3oAssto.Len.TGA_ETA_MM3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-TGA-RENDTO-LRD = -1
        //              MOVE HIGH-VALUES TO TGA-RENDTO-LRD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRendtoLrd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RENDTO-LRD-NULL
            ws.getTrchDiGar().getTgaRendtoLrd().setTgaRendtoLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRendtoLrd.Len.TGA_RENDTO_LRD_NULL));
        }
        // COB_CODE: IF IND-TGA-PC-RETR = -1
        //              MOVE HIGH-VALUES TO TGA-PC-RETR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcRetr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-RETR-NULL
            ws.getTrchDiGar().getTgaPcRetr().setTgaPcRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcRetr.Len.TGA_PC_RETR_NULL));
        }
        // COB_CODE: IF IND-TGA-RENDTO-RETR = -1
        //              MOVE HIGH-VALUES TO TGA-RENDTO-RETR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRendtoRetr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RENDTO-RETR-NULL
            ws.getTrchDiGar().getTgaRendtoRetr().setTgaRendtoRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRendtoRetr.Len.TGA_RENDTO_RETR_NULL));
        }
        // COB_CODE: IF IND-TGA-MIN-GARTO = -1
        //              MOVE HIGH-VALUES TO TGA-MIN-GARTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getMinGarto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MIN-GARTO-NULL
            ws.getTrchDiGar().getTgaMinGarto().setTgaMinGartoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaMinGarto.Len.TGA_MIN_GARTO_NULL));
        }
        // COB_CODE: IF IND-TGA-MIN-TRNUT = -1
        //              MOVE HIGH-VALUES TO TGA-MIN-TRNUT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getMinTrnut() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MIN-TRNUT-NULL
            ws.getTrchDiGar().getTgaMinTrnut().setTgaMinTrnutNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaMinTrnut.Len.TGA_MIN_TRNUT_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-ATT-DI-TRCH = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-ATT-DI-TRCH-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreAttDiTrch() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-ATT-DI-TRCH-NULL
            ws.getTrchDiGar().getTgaPreAttDiTrch().setTgaPreAttDiTrchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreAttDiTrch.Len.TGA_PRE_ATT_DI_TRCH_NULL));
        }
        // COB_CODE: IF IND-TGA-MATU-END2000 = -1
        //              MOVE HIGH-VALUES TO TGA-MATU-END2000-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getMatuEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MATU-END2000-NULL
            ws.getTrchDiGar().getTgaMatuEnd2000().setTgaMatuEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaMatuEnd2000.Len.TGA_MATU_END2000_NULL));
        }
        // COB_CODE: IF IND-TGA-ABB-TOT-INI = -1
        //              MOVE HIGH-VALUES TO TGA-ABB-TOT-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAbbTotIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ABB-TOT-INI-NULL
            ws.getTrchDiGar().getTgaAbbTotIni().setTgaAbbTotIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAbbTotIni.Len.TGA_ABB_TOT_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-ABB-TOT-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-ABB-TOT-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAbbTotUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ABB-TOT-ULT-NULL
            ws.getTrchDiGar().getTgaAbbTotUlt().setTgaAbbTotUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAbbTotUlt.Len.TGA_ABB_TOT_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-ABB-ANNU-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-ABB-ANNU-ULT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAbbAnnuUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ABB-ANNU-ULT-NULL
            ws.getTrchDiGar().getTgaAbbAnnuUlt().setTgaAbbAnnuUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAbbAnnuUlt.Len.TGA_ABB_ANNU_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-DUR-ABB = -1
        //              MOVE HIGH-VALUES TO TGA-DUR-ABB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDurAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DUR-ABB-NULL
            ws.getTrchDiGar().getTgaDurAbb().setTgaDurAbbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDurAbb.Len.TGA_DUR_ABB_NULL));
        }
        // COB_CODE: IF IND-TGA-TP-ADEG-ABB = -1
        //              MOVE HIGH-VALUES TO TGA-TP-ADEG-ABB-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTpAdegAbb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TP-ADEG-ABB-NULL
            ws.getTrchDiGar().setTgaTpAdegAbb(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-MOD-CALC = -1
        //              MOVE HIGH-VALUES TO TGA-MOD-CALC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getModCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MOD-CALC-NULL
            ws.getTrchDiGar().setTgaModCalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGarIvvs0216.Len.TGA_MOD_CALC));
        }
        // COB_CODE: IF IND-TGA-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-AZ-NULL
            ws.getTrchDiGar().getTgaImpAz().setTgaImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpAz.Len.TGA_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-ADER-NULL
            ws.getTrchDiGar().getTgaImpAder().setTgaImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpAder.Len.TGA_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-TFR-NULL
            ws.getTrchDiGar().getTgaImpTfr().setTgaImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpTfr.Len.TGA_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-VOLO-NULL
            ws.getTrchDiGar().getTgaImpVolo().setTgaImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpVolo.Len.TGA_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-TGA-VIS-END2000 = -1
        //              MOVE HIGH-VALUES TO TGA-VIS-END2000-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getVisEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-VIS-END2000-NULL
            ws.getTrchDiGar().getTgaVisEnd2000().setTgaVisEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaVisEnd2000.Len.TGA_VIS_END2000_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-VLDT-PROD = -1
        //              MOVE HIGH-VALUES TO TGA-DT-VLDT-PROD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtVldtProd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-VLDT-PROD-NULL
            ws.getTrchDiGar().getTgaDtVldtProd().setTgaDtVldtProdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtVldtProd.Len.TGA_DT_VLDT_PROD_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-INI-VAL-TAR = -1
        //              MOVE HIGH-VALUES TO TGA-DT-INI-VAL-TAR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtIniValTar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-INI-VAL-TAR-NULL
            ws.getTrchDiGar().getTgaDtIniValTar().setTgaDtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtIniValTar.Len.TGA_DT_INI_VAL_TAR_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-VIS-END2000 = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-VIS-END2000-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbVisEnd2000() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-VIS-END2000-NULL
            ws.getTrchDiGar().getTgaImpbVisEnd2000().setTgaImpbVisEnd2000Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbVisEnd2000.Len.TGA_IMPB_VIS_END2000_NULL));
        }
        // COB_CODE: IF IND-TGA-REN-INI-TS-TEC-0 = -1
        //              MOVE HIGH-VALUES TO TGA-REN-INI-TS-TEC-0-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRenIniTsTec0() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-REN-INI-TS-TEC-0-NULL
            ws.getTrchDiGar().getTgaRenIniTsTec0().setTgaRenIniTsTec0Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRenIniTsTec0.Len.TGA_REN_INI_TS_TEC0_NULL));
        }
        // COB_CODE: IF IND-TGA-PC-RIP-PRE = -1
        //              MOVE HIGH-VALUES TO TGA-PC-RIP-PRE-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcRipPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-RIP-PRE-NULL
            ws.getTrchDiGar().getTgaPcRipPre().setTgaPcRipPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcRipPre.Len.TGA_PC_RIP_PRE_NULL));
        }
        // COB_CODE: IF IND-TGA-FL-IMPORTI-FORZ = -1
        //              MOVE HIGH-VALUES TO TGA-FL-IMPORTI-FORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getFlImportiForz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-FL-IMPORTI-FORZ-NULL
            ws.getTrchDiGar().setTgaFlImportiForz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-PRSTZ-INI-NFORZ = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NFORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzIniNforz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-INI-NFORZ-NULL
            ws.getTrchDiGar().getTgaPrstzIniNforz().setTgaPrstzIniNforzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzIniNforz.Len.TGA_PRSTZ_INI_NFORZ_NULL));
        }
        // COB_CODE: IF IND-TGA-VIS-END2000-NFORZ = -1
        //              MOVE HIGH-VALUES TO TGA-VIS-END2000-NFORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getVisEnd2000Nforz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-VIS-END2000-NFORZ-NULL
            ws.getTrchDiGar().getTgaVisEnd2000Nforz().setTgaVisEnd2000NforzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaVisEnd2000Nforz.Len.TGA_VIS_END2000_NFORZ_NULL));
        }
        // COB_CODE: IF IND-TGA-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO TGA-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-INTR-MORA-NULL
            ws.getTrchDiGar().getTgaIntrMora().setTgaIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIntrMora.Len.TGA_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-TGA-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO TGA-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MANFEE-ANTIC-NULL
            ws.getTrchDiGar().getTgaManfeeAntic().setTgaManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaManfeeAntic.Len.TGA_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TGA-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-MANFEE-RICOR-NULL
            ws.getTrchDiGar().getTgaManfeeRicor().setTgaManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaManfeeRicor.Len.TGA_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-UNI-RIVTO = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-UNI-RIVTO-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPreUniRivto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-UNI-RIVTO-NULL
            ws.getTrchDiGar().getTgaPreUniRivto().setTgaPreUniRivtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPreUniRivto.Len.TGA_PRE_UNI_RIVTO_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-1AA-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-1AA-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProv1aaAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-1AA-ACQ-NULL
            ws.getTrchDiGar().getTgaProv1aaAcq().setTgaProv1aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProv1aaAcq.Len.TGA_PROV1AA_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-2AA-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-2AA-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProv2aaAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-2AA-ACQ-NULL
            ws.getTrchDiGar().getTgaProv2aaAcq().setTgaProv2aaAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProv2aaAcq.Len.TGA_PROV2AA_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-RICOR-NULL
            ws.getTrchDiGar().getTgaProvRicor().setTgaProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProvRicor.Len.TGA_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TGA-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PROV-INC-NULL
            ws.getTrchDiGar().getTgaProvInc().setTgaProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaProvInc.Len.TGA_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-PROV-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-PROV-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqProvAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-PROV-ACQ-NULL
            ws.getTrchDiGar().getTgaAlqProvAcq().setTgaAlqProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqProvAcq.Len.TGA_ALQ_PROV_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-PROV-INC-NULL
            ws.getTrchDiGar().getTgaAlqProvInc().setTgaAlqProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqProvInc.Len.TGA_ALQ_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-PROV-RICOR-NULL
            ws.getTrchDiGar().getTgaAlqProvRicor().setTgaAlqProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqProvRicor.Len.TGA_ALQ_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-PROV-ACQ = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-PROV-ACQ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbProvAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-PROV-ACQ-NULL
            ws.getTrchDiGar().getTgaImpbProvAcq().setTgaImpbProvAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbProvAcq.Len.TGA_IMPB_PROV_ACQ_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-PROV-INC-NULL
            ws.getTrchDiGar().getTgaImpbProvInc().setTgaImpbProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbProvInc.Len.TGA_IMPB_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-PROV-RICOR-NULL
            ws.getTrchDiGar().getTgaImpbProvRicor().setTgaImpbProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbProvRicor.Len.TGA_IMPB_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TGA-FL-PROV-FORZ = -1
        //              MOVE HIGH-VALUES TO TGA-FL-PROV-FORZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getFlProvForz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-FL-PROV-FORZ-NULL
            ws.getTrchDiGar().setTgaFlProvForz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TGA-PRSTZ-AGG-INI = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-INI-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPrstzAggIni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-INI-NULL
            ws.getTrchDiGar().getTgaPrstzAggIni().setTgaPrstzAggIniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzAggIni.Len.TGA_PRSTZ_AGG_INI_NULL));
        }
        // COB_CODE: IF IND-TGA-INCR-PRE = -1
        //              MOVE HIGH-VALUES TO TGA-INCR-PRE-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIncrPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-INCR-PRE-NULL
            ws.getTrchDiGar().getTgaIncrPre().setTgaIncrPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIncrPre.Len.TGA_INCR_PRE_NULL));
        }
        // COB_CODE: IF IND-TGA-INCR-PRSTZ = -1
        //              MOVE HIGH-VALUES TO TGA-INCR-PRSTZ-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getIncrPrstz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-INCR-PRSTZ-NULL
            ws.getTrchDiGar().getTgaIncrPrstz().setTgaIncrPrstzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaIncrPrstz.Len.TGA_INCR_PRSTZ_NULL));
        }
        // COB_CODE: IF IND-TGA-DT-ULT-ADEG-PRE-PR = -1
        //              MOVE HIGH-VALUES TO TGA-DT-ULT-ADEG-PRE-PR-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getDtUltAdegPrePr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-DT-ULT-ADEG-PRE-PR-NULL
            ws.getTrchDiGar().getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaDtUltAdegPrePr.Len.TGA_DT_ULT_ADEG_PRE_PR_NULL));
        }
        // COB_CODE: IF IND-TGA-PRSTZ-AGG-ULT = -1
        //              MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-ULT-NULL
        //           END-IF.
        if (ws.getIndTrchDiGar().getPrstzAggUlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRSTZ-AGG-ULT-NULL
            ws.getTrchDiGar().getTgaPrstzAggUlt().setTgaPrstzAggUltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrstzAggUlt.Len.TGA_PRSTZ_AGG_ULT_NULL));
        }
        // COB_CODE: IF IND-TGA-TS-RIVAL-NET  = -1
        //              MOVE HIGH-VALUES TO TGA-TS-RIVAL-NET-NULL
        //           END-IF.
        if (ws.getIndTrchDiGar().getTsRivalNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TS-RIVAL-NET-NULL
            ws.getTrchDiGar().getTgaTsRivalNet().setTgaTsRivalNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaTsRivalNet.Len.TGA_TS_RIVAL_NET_NULL));
        }
        // COB_CODE: IF IND-TGA-PRE-PATTUITO  = -1
        //              MOVE HIGH-VALUES TO TGA-PRE-PATTUITO-NULL
        //           END-IF.
        if (ws.getIndTrchDiGar().getPrePattuito() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PRE-PATTUITO-NULL
            ws.getTrchDiGar().getTgaPrePattuito().setTgaPrePattuitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPrePattuito.Len.TGA_PRE_PATTUITO_NULL));
        }
        // COB_CODE: IF IND-TGA-TP-RIVAL      = -1
        //              MOVE HIGH-VALUES TO TGA-TP-RIVAL-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTpRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TP-RIVAL-NULL
            ws.getTrchDiGar().setTgaTpRival(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGarIvvs0216.Len.TGA_TP_RIVAL));
        }
        // COB_CODE: IF IND-TGA-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO TGA-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-RIS-MAT-NULL
            ws.getTrchDiGar().getTgaRisMat().setTgaRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRisMat.Len.TGA_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-TGA-CPT-MIN-SCAD = -1
        //              MOVE HIGH-VALUES TO TGA-CPT-MIN-SCAD-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCptMinScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-CPT-MIN-SCAD-NULL
            ws.getTrchDiGar().getTgaCptMinScad().setTgaCptMinScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCptMinScad.Len.TGA_CPT_MIN_SCAD_NULL));
        }
        // COB_CODE: IF IND-TGA-COMMIS-GEST = -1
        //              MOVE HIGH-VALUES TO TGA-COMMIS-GEST-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCommisGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COMMIS-GEST-NULL
            ws.getTrchDiGar().getTgaCommisGest().setTgaCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCommisGest.Len.TGA_COMMIS_GEST_NULL));
        }
        // COB_CODE: IF IND-TGA-TP-MANFEE-APPL = -1
        //              MOVE HIGH-VALUES TO TGA-TP-MANFEE-APPL-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getTpManfeeAppl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-TP-MANFEE-APPL-NULL
            ws.getTrchDiGar().setTgaTpManfeeAppl(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TrchDiGarIvvs0216.Len.TGA_TP_MANFEE_APPL));
        }
        // COB_CODE: IF IND-TGA-PC-COMMIS-GEST = -1
        //              MOVE HIGH-VALUES TO TGA-PC-COMMIS-GEST-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getPcCommisGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-PC-COMMIS-GEST-NULL
            ws.getTrchDiGar().getTgaPcCommisGest().setTgaPcCommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaPcCommisGest.Len.TGA_PC_COMMIS_GEST_NULL));
        }
        // COB_CODE: IF IND-TGA-NUM-GG-RIVAL = -1
        //              MOVE HIGH-VALUES TO TGA-NUM-GG-RIVAL-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getNumGgRival() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-NUM-GG-RIVAL-NULL
            ws.getTrchDiGar().getTgaNumGgRival().setTgaNumGgRivalNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaNumGgRival.Len.TGA_NUM_GG_RIVAL_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-TRASFE-NULL
            ws.getTrchDiGar().getTgaImpTrasfe().setTgaImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpTrasfe.Len.TGA_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-TGA-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO TGA-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMP-TFR-STRC-NULL
            ws.getTrchDiGar().getTgaImpTfrStrc().setTgaImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpTfrStrc.Len.TGA_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-TGA-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO TGA-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ACQ-EXP-NULL
            ws.getTrchDiGar().getTgaAcqExp().setTgaAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAcqExp.Len.TGA_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-TGA-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TGA-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-REMUN-ASS-NULL
            ws.getTrchDiGar().getTgaRemunAss().setTgaRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaRemunAss.Len.TGA_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TGA-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TGA-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COMMIS-INTER-NULL
            ws.getTrchDiGar().getTgaCommisInter().setTgaCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCommisInter.Len.TGA_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-REMUN-ASS-NULL
            ws.getTrchDiGar().getTgaAlqRemunAss().setTgaAlqRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqRemunAss.Len.TGA_ALQ_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TGA-ALQ-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TGA-ALQ-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getAlqCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-ALQ-COMMIS-INTER-NULL
            ws.getTrchDiGar().getTgaAlqCommisInter().setTgaAlqCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaAlqCommisInter.Len.TGA_ALQ_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getImpbRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-REMUN-ASS-NULL
            ws.getTrchDiGar().getTgaImpbRemunAss().setTgaImpbRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbRemunAss.Len.TGA_IMPB_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-TGA-IMPB-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO TGA-IMPB-COMMIS-INTER-NULL
        //           END-IF.
        if (ws.getIndTrchDiGar().getImpbCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-IMPB-COMMIS-INTER-NULL
            ws.getTrchDiGar().getTgaImpbCommisInter().setTgaImpbCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaImpbCommisInter.Len.TGA_IMPB_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-TGA-COS-RUN-ASSVA = -1
        //              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-NULL
        //           END-IF
        if (ws.getIndTrchDiGar().getCosRunAssva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-NULL
            ws.getTrchDiGar().getTgaCosRunAssva().setTgaCosRunAssvaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCosRunAssva.Len.TGA_COS_RUN_ASSVA_NULL));
        }
        // COB_CODE: IF IND-TGA-COS-RUN-ASSVA-IDC = -1
        //              MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-IDC-NULL
        //           END-IF.
        if (ws.getIndTrchDiGar().getCosRunAssvaIdc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TGA-COS-RUN-ASSVA-IDC-NULL
            ws.getTrchDiGar().getTgaCosRunAssvaIdc().setTgaCosRunAssvaIdcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TgaCosRunAssvaIdc.Len.TGA_COS_RUN_ASSVA_IDC_NULL));
        }
    }

    /**Original name: Z102-VALORIZZA-TGA<br>*/
    private void z102ValorizzaTga() {
        // COB_CODE: MOVE TRCH-DI-GAR             TO LDBO0731-TGA.
        ldbo0731.setLdbo0731Tga(ws.getTrchDiGar().getTrchDiGarFormatted());
    }

    /**Original name: Z950-CONVERTI-X-TO-N-TGA<br>*/
    private void z950ConvertiXToNTga() {
        // COB_CODE: MOVE TGA-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-INI-EFF
        ws.getTrchDiGar().setTgaDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TGA-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-END-EFF
        ws.getTrchDiGar().setTgaDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE TGA-DT-DECOR-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getDecorDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-DECOR
        ws.getTrchDiGar().setTgaDtDecor(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-TGA-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO TGA-DT-SCAD
        //           END-IF
        if (ws.getIndTrchDiGar().getDtScad() == 0) {
            // COB_CODE: MOVE TGA-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-SCAD
            ws.getTrchDiGar().getTgaDtScad().setTgaDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-EMIS = 0
        //               MOVE WS-DATE-N      TO TGA-DT-EMIS
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEmis() == 0) {
            // COB_CODE: MOVE TGA-DT-EMIS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getEmisDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-EMIS
            ws.getTrchDiGar().getTgaDtEmis().setTgaDtEmis(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-EFF-STAB = 0
        //               MOVE WS-DATE-N      TO TGA-DT-EFF-STAB
        //           END-IF
        if (ws.getIndTrchDiGar().getDtEffStab() == 0) {
            // COB_CODE: MOVE TGA-DT-EFF-STAB-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getEffStabDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-EFF-STAB
            ws.getTrchDiGar().getTgaDtEffStab().setTgaDtEffStab(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-VLDT-PROD = 0
        //               MOVE WS-DATE-N      TO TGA-DT-VLDT-PROD
        //           END-IF
        if (ws.getIndTrchDiGar().getDtVldtProd() == 0) {
            // COB_CODE: MOVE TGA-DT-VLDT-PROD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getVldtProdDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-VLDT-PROD
            ws.getTrchDiGar().getTgaDtVldtProd().setTgaDtVldtProd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-INI-VAL-TAR = 0
        //               MOVE WS-DATE-N      TO TGA-DT-INI-VAL-TAR
        //           END-IF
        if (ws.getIndTrchDiGar().getDtIniValTar() == 0) {
            // COB_CODE: MOVE TGA-DT-INI-VAL-TAR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-INI-VAL-TAR
            ws.getTrchDiGar().getTgaDtIniValTar().setTgaDtIniValTar(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-TGA-DT-ULT-ADEG-PRE-PR = 0
        //               MOVE WS-DATE-N      TO TGA-DT-ULT-ADEG-PRE-PR
        //           END-IF.
        if (ws.getIndTrchDiGar().getDtUltAdegPrePr() == 0) {
            // COB_CODE: MOVE TGA-DT-ULT-ADEG-PRE-PR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getTrchDiGarDb().getUltAdegPrePrDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO TGA-DT-ULT-ADEG-PRE-PR
            ws.getTrchDiGar().getTgaDtUltAdegPrePr().setTgaDtUltAdegPrePr(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z951-CONVERTI-X-TO-N-STB<br>*/
    private void z951ConvertiXToNStb() {
        // COB_CODE: MOVE STB-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvstb3().getStbDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO STB-DT-INI-EFF
        ws.getStatOggBus().setStbDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE STB-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getIdbvstb3().getStbDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO STB-DT-END-EFF.
        ws.getStatOggBus().setStbDtEndEff(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    public void initAreaLdbo0731() {
        ldbo0731.setLdbo0731IdPadre(0);
        ldbo0731.setLdbo0731Stb("");
        ldbo0731.setLdbo0731Tga("");
    }

    @Override
    public int getAdeIdPoli() {
        return ws.getAdes().getAdeIdPoli();
    }

    @Override
    public void setAdeIdPoli(int adeIdPoli) {
        this.ws.getAdes().setAdeIdPoli(adeIdPoli);
    }

    public Idsv0003 getIdsv0003() {
        return idsv0003;
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public int getLdbi0731IdAdes() {
        return ws.getLdbi0731().getIdAdes();
    }

    @Override
    public void setLdbi0731IdAdes(int ldbi0731IdAdes) {
        this.ws.getLdbi0731().setIdAdes(ldbi0731IdAdes);
    }

    @Override
    public int getLdbi0731IdPoli() {
        return ws.getLdbi0731().getIdPoli();
    }

    @Override
    public void setLdbi0731IdPoli(int ldbi0731IdPoli) {
        this.ws.getLdbi0731().setIdPoli(ldbi0731IdPoli);
    }

    @Override
    public String getLdbi0731TpStatBus() {
        return ws.getLdbi0731().getTpStatBus();
    }

    @Override
    public void setLdbi0731TpStatBus(String ldbi0731TpStatBus) {
        this.ws.getLdbi0731().setTpStatBus(ldbi0731TpStatBus);
    }

    public StatOggBusTrchDiGarLdbs07302 getStatOggBusTrchDiGarLdbs07302() {
        return statOggBusTrchDiGarLdbs07302;
    }

    public StatOggBusTrchDiGarLdbs0730 getStatOggBusTrchDiGarLdbs0730() {
        return statOggBusTrchDiGarLdbs0730;
    }

    @Override
    public int getStbCodCompAnia() {
        return ws.getStatOggBus().getStbCodCompAnia();
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        this.ws.getStatOggBus().setStbCodCompAnia(stbCodCompAnia);
    }

    @Override
    public char getStbDsOperSql() {
        return ws.getStatOggBus().getStbDsOperSql();
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        this.ws.getStatOggBus().setStbDsOperSql(stbDsOperSql);
    }

    @Override
    public long getStbDsRiga() {
        return ws.getStatOggBus().getStbDsRiga();
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        this.ws.getStatOggBus().setStbDsRiga(stbDsRiga);
    }

    @Override
    public char getStbDsStatoElab() {
        return ws.getStatOggBus().getStbDsStatoElab();
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        this.ws.getStatOggBus().setStbDsStatoElab(stbDsStatoElab);
    }

    @Override
    public long getStbDsTsEndCptz() {
        return ws.getStatOggBus().getStbDsTsEndCptz();
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        this.ws.getStatOggBus().setStbDsTsEndCptz(stbDsTsEndCptz);
    }

    @Override
    public long getStbDsTsIniCptz() {
        return ws.getStatOggBus().getStbDsTsIniCptz();
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        this.ws.getStatOggBus().setStbDsTsIniCptz(stbDsTsIniCptz);
    }

    @Override
    public String getStbDsUtente() {
        return ws.getStatOggBus().getStbDsUtente();
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        this.ws.getStatOggBus().setStbDsUtente(stbDsUtente);
    }

    @Override
    public int getStbDsVer() {
        return ws.getStatOggBus().getStbDsVer();
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        this.ws.getStatOggBus().setStbDsVer(stbDsVer);
    }

    @Override
    public String getStbDtEndEffDb() {
        return ws.getIdbvstb3().getStbDtEndEffDb();
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        this.ws.getIdbvstb3().setStbDtEndEffDb(stbDtEndEffDb);
    }

    @Override
    public String getStbDtIniEffDb() {
        return ws.getIdbvstb3().getStbDtIniEffDb();
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        this.ws.getIdbvstb3().setStbDtIniEffDb(stbDtIniEffDb);
    }

    @Override
    public int getStbIdMoviChiu() {
        return ws.getStatOggBus().getStbIdMoviChiu().getStbIdMoviChiu();
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        this.ws.getStatOggBus().getStbIdMoviChiu().setStbIdMoviChiu(stbIdMoviChiu);
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        if (ws.getIndStbIdMoviChiu() >= 0) {
            return ((Integer)getStbIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        if (stbIdMoviChiuObj != null) {
            setStbIdMoviChiu(((int)stbIdMoviChiuObj));
            ws.setIndStbIdMoviChiu(((short)0));
        }
        else {
            ws.setIndStbIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getStbIdMoviCrz() {
        return ws.getStatOggBus().getStbIdMoviCrz();
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        this.ws.getStatOggBus().setStbIdMoviCrz(stbIdMoviCrz);
    }

    @Override
    public int getStbIdOgg() {
        return ws.getStatOggBus().getStbIdOgg();
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        this.ws.getStatOggBus().setStbIdOgg(stbIdOgg);
    }

    @Override
    public int getStbIdStatOggBus() {
        return ws.getStatOggBus().getStbIdStatOggBus();
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        this.ws.getStatOggBus().setStbIdStatOggBus(stbIdStatOggBus);
    }

    @Override
    public String getStbTpCaus() {
        return ws.getStatOggBus().getStbTpCaus();
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        this.ws.getStatOggBus().setStbTpCaus(stbTpCaus);
    }

    @Override
    public String getStbTpOgg() {
        return ws.getStatOggBus().getStbTpOgg();
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        this.ws.getStatOggBus().setStbTpOgg(stbTpOgg);
    }

    @Override
    public String getStbTpStatBus() {
        return ws.getStatOggBus().getStbTpStatBus();
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        this.ws.getStatOggBus().setStbTpStatBus(stbTpStatBus);
    }

    public Ldbs0730Data getWs() {
        return ws;
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
