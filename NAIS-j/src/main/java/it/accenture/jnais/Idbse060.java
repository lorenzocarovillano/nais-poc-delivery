package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.EstPoliDao;
import it.accenture.jnais.commons.data.to.IEstPoli;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.EstPoli;
import it.accenture.jnais.ws.Idbse060Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.E06CptProtetto;
import it.accenture.jnais.ws.redefines.E06CumPreAttTakeP;
import it.accenture.jnais.ws.redefines.E06DtEmisPartner;
import it.accenture.jnais.ws.redefines.E06DtUltPerd;
import it.accenture.jnais.ws.redefines.E06IdMoviChiu;
import it.accenture.jnais.ws.redefines.E06PcUltPerd;

/**Original name: IDBSE060<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  04 MAR 2020.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbse060 extends Program implements IEstPoli {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private EstPoliDao estPoliDao = new EstPoliDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbse060Data ws = new Idbse060Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: EST-POLI
    private EstPoli estPoli;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSE060_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, EstPoli estPoli) {
        this.idsv0003 = idsv0003;
        this.estPoli = estPoli;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbse060 getInstance() {
        return ((Idbse060)Programs.getInstance(Idbse060.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSE060'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSE060");
        // COB_CODE: MOVE 'EST_POLI' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("EST_POLI");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI
        //                ,ID_POLI
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_ULT_PERD
        //                ,PC_ULT_PERD
        //                ,FL_ESCL_SWITCH_MAX
        //                ,ESI_ADEGZ_ISVAP
        //                ,NUM_INA
        //                ,TP_MOD_PROV
        //                ,CPT_PROTETTO
        //                ,CUM_PRE_ATT_TAKE_P
        //                ,DT_EMIS_PARTNER
        //             INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //             FROM EST_POLI
        //             WHERE     DS_RIGA = :E06-DS-RIGA
        //           END-EXEC.
        estPoliDao.selectByE06DsRiga(estPoli.getE06DsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO EST_POLI
            //                  (
            //                     ID_EST_POLI
            //                    ,ID_POLI
            //                    ,IB_OGG
            //                    ,IB_PROP
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,DT_ULT_PERD
            //                    ,PC_ULT_PERD
            //                    ,FL_ESCL_SWITCH_MAX
            //                    ,ESI_ADEGZ_ISVAP
            //                    ,NUM_INA
            //                    ,TP_MOD_PROV
            //                    ,CPT_PROTETTO
            //                    ,CUM_PRE_ATT_TAKE_P
            //                    ,DT_EMIS_PARTNER
            //                  )
            //              VALUES
            //                  (
            //                    :E06-ID-EST-POLI
            //                    ,:E06-ID-POLI
            //                    ,:E06-IB-OGG
            //                     :IND-E06-IB-OGG
            //                    ,:E06-IB-PROP
            //                    ,:E06-ID-MOVI-CRZ
            //                    ,:E06-ID-MOVI-CHIU
            //                     :IND-E06-ID-MOVI-CHIU
            //                    ,:E06-DT-INI-EFF-DB
            //                    ,:E06-DT-END-EFF-DB
            //                    ,:E06-COD-COMP-ANIA
            //                    ,:E06-DS-RIGA
            //                    ,:E06-DS-OPER-SQL
            //                    ,:E06-DS-VER
            //                    ,:E06-DS-TS-INI-CPTZ
            //                    ,:E06-DS-TS-END-CPTZ
            //                    ,:E06-DS-UTENTE
            //                    ,:E06-DS-STATO-ELAB
            //                    ,:E06-DT-ULT-PERD-DB
            //                     :IND-E06-DT-ULT-PERD
            //                    ,:E06-PC-ULT-PERD
            //                     :IND-E06-PC-ULT-PERD
            //                    ,:E06-FL-ESCL-SWITCH-MAX
            //                     :IND-E06-FL-ESCL-SWITCH-MAX
            //                    ,:E06-ESI-ADEGZ-ISVAP
            //                     :IND-E06-ESI-ADEGZ-ISVAP
            //                    ,:E06-NUM-INA
            //                     :IND-E06-NUM-INA
            //                    ,:E06-TP-MOD-PROV
            //                     :IND-E06-TP-MOD-PROV
            //                    ,:E06-CPT-PROTETTO
            //                     :IND-E06-CPT-PROTETTO
            //                    ,:E06-CUM-PRE-ATT-TAKE-P
            //                     :IND-E06-CUM-PRE-ATT-TAKE-P
            //                    ,:E06-DT-EMIS-PARTNER-DB
            //                     :IND-E06-DT-EMIS-PARTNER
            //                  )
            //           END-EXEC
            estPoliDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE EST_POLI SET
        //                   ID_EST_POLI            =
        //                :E06-ID-EST-POLI
        //                  ,ID_POLI                =
        //                :E06-ID-POLI
        //                  ,IB_OGG                 =
        //                :E06-IB-OGG
        //                                       :IND-E06-IB-OGG
        //                  ,IB_PROP                =
        //                :E06-IB-PROP
        //                  ,ID_MOVI_CRZ            =
        //                :E06-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :E06-ID-MOVI-CHIU
        //                                       :IND-E06-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :E06-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :E06-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :E06-COD-COMP-ANIA
        //                  ,DS_RIGA                =
        //                :E06-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :E06-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :E06-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :E06-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :E06-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :E06-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :E06-DS-STATO-ELAB
        //                  ,DT_ULT_PERD            =
        //           :E06-DT-ULT-PERD-DB
        //                                       :IND-E06-DT-ULT-PERD
        //                  ,PC_ULT_PERD            =
        //                :E06-PC-ULT-PERD
        //                                       :IND-E06-PC-ULT-PERD
        //                  ,FL_ESCL_SWITCH_MAX     =
        //                :E06-FL-ESCL-SWITCH-MAX
        //                                       :IND-E06-FL-ESCL-SWITCH-MAX
        //                  ,ESI_ADEGZ_ISVAP        =
        //                :E06-ESI-ADEGZ-ISVAP
        //                                       :IND-E06-ESI-ADEGZ-ISVAP
        //                  ,NUM_INA                =
        //                :E06-NUM-INA
        //                                       :IND-E06-NUM-INA
        //                  ,TP_MOD_PROV            =
        //                :E06-TP-MOD-PROV
        //                                       :IND-E06-TP-MOD-PROV
        //                  ,CPT_PROTETTO           =
        //                :E06-CPT-PROTETTO
        //                                       :IND-E06-CPT-PROTETTO
        //                  ,CUM_PRE_ATT_TAKE_P     =
        //                :E06-CUM-PRE-ATT-TAKE-P
        //                                       :IND-E06-CUM-PRE-ATT-TAKE-P
        //                  ,DT_EMIS_PARTNER        =
        //           :E06-DT-EMIS-PARTNER-DB
        //                                       :IND-E06-DT-EMIS-PARTNER
        //                WHERE     DS_RIGA = :E06-DS-RIGA
        //           END-EXEC.
        estPoliDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM EST_POLI
        //                WHERE     DS_RIGA = :E06-DS-RIGA
        //           END-EXEC.
        estPoliDao.deleteByE06DsRiga(estPoli.getE06DsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-E06 CURSOR FOR
        //              SELECT
        //                     ID_EST_POLI
        //                    ,ID_POLI
        //                    ,IB_OGG
        //                    ,IB_PROP
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,DT_ULT_PERD
        //                    ,PC_ULT_PERD
        //                    ,FL_ESCL_SWITCH_MAX
        //                    ,ESI_ADEGZ_ISVAP
        //                    ,NUM_INA
        //                    ,TP_MOD_PROV
        //                    ,CPT_PROTETTO
        //                    ,CUM_PRE_ATT_TAKE_P
        //                    ,DT_EMIS_PARTNER
        //              FROM EST_POLI
        //              WHERE     ID_EST_POLI = :E06-ID-EST-POLI
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI
        //                ,ID_POLI
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_ULT_PERD
        //                ,PC_ULT_PERD
        //                ,FL_ESCL_SWITCH_MAX
        //                ,ESI_ADEGZ_ISVAP
        //                ,NUM_INA
        //                ,TP_MOD_PROV
        //                ,CPT_PROTETTO
        //                ,CUM_PRE_ATT_TAKE_P
        //                ,DT_EMIS_PARTNER
        //             INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //             FROM EST_POLI
        //             WHERE     ID_EST_POLI = :E06-ID-EST-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estPoliDao.selectRec(estPoli.getE06IdEstPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE EST_POLI SET
        //                   ID_EST_POLI            =
        //                :E06-ID-EST-POLI
        //                  ,ID_POLI                =
        //                :E06-ID-POLI
        //                  ,IB_OGG                 =
        //                :E06-IB-OGG
        //                                       :IND-E06-IB-OGG
        //                  ,IB_PROP                =
        //                :E06-IB-PROP
        //                  ,ID_MOVI_CRZ            =
        //                :E06-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :E06-ID-MOVI-CHIU
        //                                       :IND-E06-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :E06-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :E06-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :E06-COD-COMP-ANIA
        //                  ,DS_RIGA                =
        //                :E06-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :E06-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :E06-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :E06-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :E06-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :E06-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :E06-DS-STATO-ELAB
        //                  ,DT_ULT_PERD            =
        //           :E06-DT-ULT-PERD-DB
        //                                       :IND-E06-DT-ULT-PERD
        //                  ,PC_ULT_PERD            =
        //                :E06-PC-ULT-PERD
        //                                       :IND-E06-PC-ULT-PERD
        //                  ,FL_ESCL_SWITCH_MAX     =
        //                :E06-FL-ESCL-SWITCH-MAX
        //                                       :IND-E06-FL-ESCL-SWITCH-MAX
        //                  ,ESI_ADEGZ_ISVAP        =
        //                :E06-ESI-ADEGZ-ISVAP
        //                                       :IND-E06-ESI-ADEGZ-ISVAP
        //                  ,NUM_INA                =
        //                :E06-NUM-INA
        //                                       :IND-E06-NUM-INA
        //                  ,TP_MOD_PROV            =
        //                :E06-TP-MOD-PROV
        //                                       :IND-E06-TP-MOD-PROV
        //                  ,CPT_PROTETTO           =
        //                :E06-CPT-PROTETTO
        //                                       :IND-E06-CPT-PROTETTO
        //                  ,CUM_PRE_ATT_TAKE_P     =
        //                :E06-CUM-PRE-ATT-TAKE-P
        //                                       :IND-E06-CUM-PRE-ATT-TAKE-P
        //                  ,DT_EMIS_PARTNER        =
        //           :E06-DT-EMIS-PARTNER-DB
        //                                       :IND-E06-DT-EMIS-PARTNER
        //                WHERE     DS_RIGA = :E06-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estPoliDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-E06
        //           END-EXEC.
        estPoliDao.openCIdUpdEffE06(estPoli.getE06IdEstPoli(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-E06
        //           END-EXEC.
        estPoliDao.closeCIdUpdEffE06();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-E06
        //           INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //           END-EXEC.
        estPoliDao.fetchCIdUpdEffE06(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-E06 CURSOR FOR
        //              SELECT
        //                     ID_EST_POLI
        //                    ,ID_POLI
        //                    ,IB_OGG
        //                    ,IB_PROP
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,DT_ULT_PERD
        //                    ,PC_ULT_PERD
        //                    ,FL_ESCL_SWITCH_MAX
        //                    ,ESI_ADEGZ_ISVAP
        //                    ,NUM_INA
        //                    ,TP_MOD_PROV
        //                    ,CPT_PROTETTO
        //                    ,CUM_PRE_ATT_TAKE_P
        //                    ,DT_EMIS_PARTNER
        //              FROM EST_POLI
        //              WHERE     ID_POLI = :E06-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_EST_POLI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI
        //                ,ID_POLI
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_ULT_PERD
        //                ,PC_ULT_PERD
        //                ,FL_ESCL_SWITCH_MAX
        //                ,ESI_ADEGZ_ISVAP
        //                ,NUM_INA
        //                ,TP_MOD_PROV
        //                ,CPT_PROTETTO
        //                ,CUM_PRE_ATT_TAKE_P
        //                ,DT_EMIS_PARTNER
        //             INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //             FROM EST_POLI
        //             WHERE     ID_POLI = :E06-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estPoliDao.selectRec1(estPoli.getE06IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-E06
        //           END-EXEC.
        estPoliDao.openCIdpEffE06(estPoli.getE06IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-E06
        //           END-EXEC.
        estPoliDao.closeCIdpEffE06();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-E06
        //           INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //           END-EXEC.
        estPoliDao.fetchCIdpEffE06(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-EFF-E06 CURSOR FOR
        //              SELECT
        //                     ID_EST_POLI
        //                    ,ID_POLI
        //                    ,IB_OGG
        //                    ,IB_PROP
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,DT_ULT_PERD
        //                    ,PC_ULT_PERD
        //                    ,FL_ESCL_SWITCH_MAX
        //                    ,ESI_ADEGZ_ISVAP
        //                    ,NUM_INA
        //                    ,TP_MOD_PROV
        //                    ,CPT_PROTETTO
        //                    ,CUM_PRE_ATT_TAKE_P
        //                    ,DT_EMIS_PARTNER
        //              FROM EST_POLI
        //              WHERE     IB_OGG = :E06-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_EST_POLI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI
        //                ,ID_POLI
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_ULT_PERD
        //                ,PC_ULT_PERD
        //                ,FL_ESCL_SWITCH_MAX
        //                ,ESI_ADEGZ_ISVAP
        //                ,NUM_INA
        //                ,TP_MOD_PROV
        //                ,CPT_PROTETTO
        //                ,CUM_PRE_ATT_TAKE_P
        //                ,DT_EMIS_PARTNER
        //             INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //             FROM EST_POLI
        //             WHERE     IB_OGG = :E06-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estPoliDao.selectRec2(estPoli.getE06IbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-EFF-E06
        //           END-EXEC.
        estPoliDao.openCIboEffE06(estPoli.getE06IbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-EFF-E06
        //           END-EXEC.
        estPoliDao.closeCIboEffE06();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-EFF-E06
        //           INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //           END-EXEC.
        estPoliDao.fetchCIboEffE06(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX
            a570CloseCursorIbo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A605-DCL-CUR-IBS-PROP<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DclCurIbsProp() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-E06-0 CURSOR FOR
    //              SELECT
    //                     ID_EST_POLI
    //                    ,ID_POLI
    //                    ,IB_OGG
    //                    ,IB_PROP
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,DT_ULT_PERD
    //                    ,PC_ULT_PERD
    //                    ,FL_ESCL_SWITCH_MAX
    //                    ,ESI_ADEGZ_ISVAP
    //                    ,NUM_INA
    //                    ,TP_MOD_PROV
    //                    ,CPT_PROTETTO
    //                    ,CUM_PRE_ATT_TAKE_P
    //                    ,DT_EMIS_PARTNER
    //              FROM EST_POLI
    //              WHERE     IB_PROP = :E06-IB-PROP
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_EST_POLI ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //                  THRU A605-PROP-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-PROP
            //              THRU A605-PROP-EX
            a605DclCurIbsProp();
        }
    }

    /**Original name: A610-SELECT-IBS-PROP<br>*/
    private void a610SelectIbsProp() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI
        //                ,ID_POLI
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_ULT_PERD
        //                ,PC_ULT_PERD
        //                ,FL_ESCL_SWITCH_MAX
        //                ,ESI_ADEGZ_ISVAP
        //                ,NUM_INA
        //                ,TP_MOD_PROV
        //                ,CPT_PROTETTO
        //                ,CUM_PRE_ATT_TAKE_P
        //                ,DT_EMIS_PARTNER
        //             INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //             FROM EST_POLI
        //             WHERE     IB_PROP = :E06-IB-PROP
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estPoliDao.selectRec3(estPoli.getE06IbProp(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //                  THRU A610-PROP-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: PERFORM A610-SELECT-IBS-PROP
            //              THRU A610-PROP-EX
            a610SelectIbsProp();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-E06-0
            //           END-EXEC
            estPoliDao.openCIbsEffE060(estPoli.getE06IbProp(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-E06-0
            //           END-EXEC
            estPoliDao.closeCIbsEffE060();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FN-IBS-PROP<br>*/
    private void a690FnIbsProp() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-E06-0
        //           INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //           END-EXEC.
        estPoliDao.fetchCIbsEffE060(this);
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //                  THRU A690-PROP-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: PERFORM A690-FN-IBS-PROP
            //              THRU A690-PROP-EX
            a690FnIbsProp();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX
            a670CloseCursorIbs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI
        //                ,ID_POLI
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_ULT_PERD
        //                ,PC_ULT_PERD
        //                ,FL_ESCL_SWITCH_MAX
        //                ,ESI_ADEGZ_ISVAP
        //                ,NUM_INA
        //                ,TP_MOD_PROV
        //                ,CPT_PROTETTO
        //                ,CUM_PRE_ATT_TAKE_P
        //                ,DT_EMIS_PARTNER
        //             INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //             FROM EST_POLI
        //             WHERE     ID_EST_POLI = :E06-ID-EST-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        estPoliDao.selectRec4(estPoli.getE06IdEstPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-E06 CURSOR FOR
        //              SELECT
        //                     ID_EST_POLI
        //                    ,ID_POLI
        //                    ,IB_OGG
        //                    ,IB_PROP
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,DT_ULT_PERD
        //                    ,PC_ULT_PERD
        //                    ,FL_ESCL_SWITCH_MAX
        //                    ,ESI_ADEGZ_ISVAP
        //                    ,NUM_INA
        //                    ,TP_MOD_PROV
        //                    ,CPT_PROTETTO
        //                    ,CUM_PRE_ATT_TAKE_P
        //                    ,DT_EMIS_PARTNER
        //              FROM EST_POLI
        //              WHERE     ID_POLI = :E06-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_EST_POLI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI
        //                ,ID_POLI
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_ULT_PERD
        //                ,PC_ULT_PERD
        //                ,FL_ESCL_SWITCH_MAX
        //                ,ESI_ADEGZ_ISVAP
        //                ,NUM_INA
        //                ,TP_MOD_PROV
        //                ,CPT_PROTETTO
        //                ,CUM_PRE_ATT_TAKE_P
        //                ,DT_EMIS_PARTNER
        //             INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //             FROM EST_POLI
        //             WHERE     ID_POLI = :E06-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        estPoliDao.selectRec5(estPoli.getE06IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-E06
        //           END-EXEC.
        estPoliDao.openCIdpCpzE06(estPoli.getE06IdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-E06
        //           END-EXEC.
        estPoliDao.closeCIdpCpzE06();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-E06
        //           INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //           END-EXEC.
        estPoliDao.fetchCIdpCpzE06(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-CPZ-E06 CURSOR FOR
        //              SELECT
        //                     ID_EST_POLI
        //                    ,ID_POLI
        //                    ,IB_OGG
        //                    ,IB_PROP
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,DT_ULT_PERD
        //                    ,PC_ULT_PERD
        //                    ,FL_ESCL_SWITCH_MAX
        //                    ,ESI_ADEGZ_ISVAP
        //                    ,NUM_INA
        //                    ,TP_MOD_PROV
        //                    ,CPT_PROTETTO
        //                    ,CUM_PRE_ATT_TAKE_P
        //                    ,DT_EMIS_PARTNER
        //              FROM EST_POLI
        //              WHERE     IB_OGG = :E06-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_EST_POLI ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI
        //                ,ID_POLI
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_ULT_PERD
        //                ,PC_ULT_PERD
        //                ,FL_ESCL_SWITCH_MAX
        //                ,ESI_ADEGZ_ISVAP
        //                ,NUM_INA
        //                ,TP_MOD_PROV
        //                ,CPT_PROTETTO
        //                ,CUM_PRE_ATT_TAKE_P
        //                ,DT_EMIS_PARTNER
        //             INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //             FROM EST_POLI
        //             WHERE     IB_OGG = :E06-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        estPoliDao.selectRec6(estPoli.getE06IbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-CPZ-E06
        //           END-EXEC.
        estPoliDao.openCIboCpzE06(estPoli.getE06IbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-CPZ-E06
        //           END-EXEC.
        estPoliDao.closeCIboCpzE06();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-CPZ-E06
        //           INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //           END-EXEC.
        estPoliDao.fetchCIboCpzE06(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX
            b570CloseCursorIboCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B605-DCL-CUR-IBS-PROP<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DclCurIbsProp() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-E06-0 CURSOR FOR
    //              SELECT
    //                     ID_EST_POLI
    //                    ,ID_POLI
    //                    ,IB_OGG
    //                    ,IB_PROP
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,COD_COMP_ANIA
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,DT_ULT_PERD
    //                    ,PC_ULT_PERD
    //                    ,FL_ESCL_SWITCH_MAX
    //                    ,ESI_ADEGZ_ISVAP
    //                    ,NUM_INA
    //                    ,TP_MOD_PROV
    //                    ,CPT_PROTETTO
    //                    ,CUM_PRE_ATT_TAKE_P
    //                    ,DT_EMIS_PARTNER
    //              FROM EST_POLI
    //              WHERE     IB_PROP = :E06-IB-PROP
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_EST_POLI ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //                  THRU B605-PROP-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: PERFORM B605-DCL-CUR-IBS-PROP
            //              THRU B605-PROP-EX
            b605DclCurIbsProp();
        }
    }

    /**Original name: B610-SELECT-IBS-PROP<br>*/
    private void b610SelectIbsProp() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI
        //                ,ID_POLI
        //                ,IB_OGG
        //                ,IB_PROP
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DT_ULT_PERD
        //                ,PC_ULT_PERD
        //                ,FL_ESCL_SWITCH_MAX
        //                ,ESI_ADEGZ_ISVAP
        //                ,NUM_INA
        //                ,TP_MOD_PROV
        //                ,CPT_PROTETTO
        //                ,CUM_PRE_ATT_TAKE_P
        //                ,DT_EMIS_PARTNER
        //             INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //             FROM EST_POLI
        //             WHERE     IB_PROP = :E06-IB-PROP
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        estPoliDao.selectRec7(estPoli.getE06IbProp(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //                  THRU B610-PROP-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: PERFORM B610-SELECT-IBS-PROP
            //              THRU B610-PROP-EX
            b610SelectIbsProp();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-E06-0
            //           END-EXEC
            estPoliDao.openCIbsCpzE060(estPoli.getE06IbProp(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //              END-EXEC
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-E06-0
            //           END-EXEC
            estPoliDao.closeCIbsCpzE060();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FN-IBS-PROP<br>*/
    private void b690FnIbsProp() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-E06-0
        //           INTO
        //                :E06-ID-EST-POLI
        //               ,:E06-ID-POLI
        //               ,:E06-IB-OGG
        //                :IND-E06-IB-OGG
        //               ,:E06-IB-PROP
        //               ,:E06-ID-MOVI-CRZ
        //               ,:E06-ID-MOVI-CHIU
        //                :IND-E06-ID-MOVI-CHIU
        //               ,:E06-DT-INI-EFF-DB
        //               ,:E06-DT-END-EFF-DB
        //               ,:E06-COD-COMP-ANIA
        //               ,:E06-DS-RIGA
        //               ,:E06-DS-OPER-SQL
        //               ,:E06-DS-VER
        //               ,:E06-DS-TS-INI-CPTZ
        //               ,:E06-DS-TS-END-CPTZ
        //               ,:E06-DS-UTENTE
        //               ,:E06-DS-STATO-ELAB
        //               ,:E06-DT-ULT-PERD-DB
        //                :IND-E06-DT-ULT-PERD
        //               ,:E06-PC-ULT-PERD
        //                :IND-E06-PC-ULT-PERD
        //               ,:E06-FL-ESCL-SWITCH-MAX
        //                :IND-E06-FL-ESCL-SWITCH-MAX
        //               ,:E06-ESI-ADEGZ-ISVAP
        //                :IND-E06-ESI-ADEGZ-ISVAP
        //               ,:E06-NUM-INA
        //                :IND-E06-NUM-INA
        //               ,:E06-TP-MOD-PROV
        //                :IND-E06-TP-MOD-PROV
        //               ,:E06-CPT-PROTETTO
        //                :IND-E06-CPT-PROTETTO
        //               ,:E06-CUM-PRE-ATT-TAKE-P
        //                :IND-E06-CUM-PRE-ATT-TAKE-P
        //               ,:E06-DT-EMIS-PARTNER-DB
        //                :IND-E06-DT-EMIS-PARTNER
        //           END-EXEC.
        estPoliDao.fetchCIbsCpzE060(this);
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: IF E06-IB-PROP NOT = HIGH-VALUES
        //                  THRU B690-PROP-EX
        //           END-IF.
        if (!Characters.EQ_HIGH.test(estPoli.getE06IbProp(), EstPoli.Len.E06_IB_PROP)) {
            // COB_CODE: PERFORM B690-FN-IBS-PROP
            //              THRU B690-PROP-EX
            b690FnIbsProp();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX
            b670CloseCursorIbsCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-E06-IB-OGG = -1
        //              MOVE HIGH-VALUES TO E06-IB-OGG-NULL
        //           END-IF
        if (ws.getIndEstPoli().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-IB-OGG-NULL
            estPoli.setE06IbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstPoli.Len.E06_IB_OGG));
        }
        // COB_CODE: IF IND-E06-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO E06-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndEstPoli().getCodParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-ID-MOVI-CHIU-NULL
            estPoli.getE06IdMoviChiu().setE06IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E06IdMoviChiu.Len.E06_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-E06-DT-ULT-PERD = -1
        //              MOVE HIGH-VALUES TO E06-DT-ULT-PERD-NULL
        //           END-IF
        if (ws.getIndEstPoli().getTpParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-DT-ULT-PERD-NULL
            estPoli.getE06DtUltPerd().setE06DtUltPerdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E06DtUltPerd.Len.E06_DT_ULT_PERD_NULL));
        }
        // COB_CODE: IF IND-E06-PC-ULT-PERD = -1
        //              MOVE HIGH-VALUES TO E06-PC-ULT-PERD-NULL
        //           END-IF
        if (ws.getIndEstPoli().getTpD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-PC-ULT-PERD-NULL
            estPoli.getE06PcUltPerd().setE06PcUltPerdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E06PcUltPerd.Len.E06_PC_ULT_PERD_NULL));
        }
        // COB_CODE: IF IND-E06-FL-ESCL-SWITCH-MAX = -1
        //              MOVE HIGH-VALUES TO E06-FL-ESCL-SWITCH-MAX-NULL
        //           END-IF
        if (ws.getIndEstPoli().getValImp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-FL-ESCL-SWITCH-MAX-NULL
            estPoli.setE06FlEsclSwitchMax(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-E06-ESI-ADEGZ-ISVAP = -1
        //              MOVE HIGH-VALUES TO E06-ESI-ADEGZ-ISVAP-NULL
        //           END-IF
        if (ws.getIndEstPoli().getValDt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-ESI-ADEGZ-ISVAP-NULL
            estPoli.setE06EsiAdegzIsvap(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstPoli.Len.E06_ESI_ADEGZ_ISVAP));
        }
        // COB_CODE: IF IND-E06-NUM-INA = -1
        //              MOVE HIGH-VALUES TO E06-NUM-INA-NULL
        //           END-IF
        if (ws.getIndEstPoli().getValTs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-NUM-INA-NULL
            estPoli.setE06NumIna(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstPoli.Len.E06_NUM_INA));
        }
        // COB_CODE: IF IND-E06-TP-MOD-PROV = -1
        //              MOVE HIGH-VALUES TO E06-TP-MOD-PROV-NULL
        //           END-IF
        if (ws.getIndEstPoli().getValTxt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-TP-MOD-PROV-NULL
            estPoli.setE06TpModProv(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstPoli.Len.E06_TP_MOD_PROV));
        }
        // COB_CODE: IF IND-E06-CPT-PROTETTO = -1
        //              MOVE HIGH-VALUES TO E06-CPT-PROTETTO-NULL
        //           END-IF
        if (ws.getIndEstPoli().getValFl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-CPT-PROTETTO-NULL
            estPoli.getE06CptProtetto().setE06CptProtettoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E06CptProtetto.Len.E06_CPT_PROTETTO_NULL));
        }
        // COB_CODE: IF IND-E06-CUM-PRE-ATT-TAKE-P = -1
        //              MOVE HIGH-VALUES TO E06-CUM-PRE-ATT-TAKE-P-NULL
        //           END-IF
        if (ws.getIndEstPoli().getValNum() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-CUM-PRE-ATT-TAKE-P-NULL
            estPoli.getE06CumPreAttTakeP().setE06CumPreAttTakePNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E06CumPreAttTakeP.Len.E06_CUM_PRE_ATT_TAKE_P_NULL));
        }
        // COB_CODE: IF IND-E06-DT-EMIS-PARTNER = -1
        //              MOVE HIGH-VALUES TO E06-DT-EMIS-PARTNER-NULL
        //           END-IF.
        if (ws.getIndEstPoli().getValPc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO E06-DT-EMIS-PARTNER-NULL
            estPoli.getE06DtEmisPartner().setE06DtEmisPartnerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E06DtEmisPartner.Len.E06_DT_EMIS_PARTNER_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO E06-DS-OPER-SQL
        estPoli.setE06DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO E06-DS-VER
        estPoli.setE06DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO E06-DS-UTENTE
        estPoli.setE06DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO E06-DS-STATO-ELAB.
        estPoli.setE06DsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO E06-DS-OPER-SQL
        estPoli.setE06DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO E06-DS-UTENTE.
        estPoli.setE06DsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF E06-IB-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-IB-OGG
        //           ELSE
        //              MOVE 0 TO IND-E06-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoli.getE06IbOgg(), EstPoli.Len.E06_IB_OGG)) {
            // COB_CODE: MOVE -1 TO IND-E06-IB-OGG
            ws.getIndEstPoli().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-IB-OGG
            ws.getIndEstPoli().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF E06-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-E06-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoli.getE06IdMoviChiu().getE06IdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E06-ID-MOVI-CHIU
            ws.getIndEstPoli().setCodParam(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-ID-MOVI-CHIU
            ws.getIndEstPoli().setCodParam(((short)0));
        }
        // COB_CODE: IF E06-DT-ULT-PERD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-DT-ULT-PERD
        //           ELSE
        //              MOVE 0 TO IND-E06-DT-ULT-PERD
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoli.getE06DtUltPerd().getE06DtUltPerdNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E06-DT-ULT-PERD
            ws.getIndEstPoli().setTpParam(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-DT-ULT-PERD
            ws.getIndEstPoli().setTpParam(((short)0));
        }
        // COB_CODE: IF E06-PC-ULT-PERD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-PC-ULT-PERD
        //           ELSE
        //              MOVE 0 TO IND-E06-PC-ULT-PERD
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoli.getE06PcUltPerd().getE06PcUltPerdNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E06-PC-ULT-PERD
            ws.getIndEstPoli().setTpD(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-PC-ULT-PERD
            ws.getIndEstPoli().setTpD(((short)0));
        }
        // COB_CODE: IF E06-FL-ESCL-SWITCH-MAX-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-FL-ESCL-SWITCH-MAX
        //           ELSE
        //              MOVE 0 TO IND-E06-FL-ESCL-SWITCH-MAX
        //           END-IF
        if (Conditions.eq(estPoli.getE06FlEsclSwitchMax(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-E06-FL-ESCL-SWITCH-MAX
            ws.getIndEstPoli().setValImp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-FL-ESCL-SWITCH-MAX
            ws.getIndEstPoli().setValImp(((short)0));
        }
        // COB_CODE: IF E06-ESI-ADEGZ-ISVAP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-ESI-ADEGZ-ISVAP
        //           ELSE
        //              MOVE 0 TO IND-E06-ESI-ADEGZ-ISVAP
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoli.getE06EsiAdegzIsvapFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E06-ESI-ADEGZ-ISVAP
            ws.getIndEstPoli().setValDt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-ESI-ADEGZ-ISVAP
            ws.getIndEstPoli().setValDt(((short)0));
        }
        // COB_CODE: IF E06-NUM-INA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-NUM-INA
        //           ELSE
        //              MOVE 0 TO IND-E06-NUM-INA
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoli.getE06NumInaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E06-NUM-INA
            ws.getIndEstPoli().setValTs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-NUM-INA
            ws.getIndEstPoli().setValTs(((short)0));
        }
        // COB_CODE: IF E06-TP-MOD-PROV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-TP-MOD-PROV
        //           ELSE
        //              MOVE 0 TO IND-E06-TP-MOD-PROV
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoli.getE06TpModProvFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E06-TP-MOD-PROV
            ws.getIndEstPoli().setValTxt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-TP-MOD-PROV
            ws.getIndEstPoli().setValTxt(((short)0));
        }
        // COB_CODE: IF E06-CPT-PROTETTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-CPT-PROTETTO
        //           ELSE
        //              MOVE 0 TO IND-E06-CPT-PROTETTO
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoli.getE06CptProtetto().getE06CptProtettoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E06-CPT-PROTETTO
            ws.getIndEstPoli().setValFl(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-CPT-PROTETTO
            ws.getIndEstPoli().setValFl(((short)0));
        }
        // COB_CODE: IF E06-CUM-PRE-ATT-TAKE-P-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-CUM-PRE-ATT-TAKE-P
        //           ELSE
        //              MOVE 0 TO IND-E06-CUM-PRE-ATT-TAKE-P
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoli.getE06CumPreAttTakeP().getE06CumPreAttTakePNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E06-CUM-PRE-ATT-TAKE-P
            ws.getIndEstPoli().setValNum(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-CUM-PRE-ATT-TAKE-P
            ws.getIndEstPoli().setValNum(((short)0));
        }
        // COB_CODE: IF E06-DT-EMIS-PARTNER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-E06-DT-EMIS-PARTNER
        //           ELSE
        //              MOVE 0 TO IND-E06-DT-EMIS-PARTNER
        //           END-IF.
        if (Characters.EQ_HIGH.test(estPoli.getE06DtEmisPartner().getE06DtEmisPartnerNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-E06-DT-EMIS-PARTNER
            ws.getIndEstPoli().setValPc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-E06-DT-EMIS-PARTNER
            ws.getIndEstPoli().setValPc(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : E06-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE EST-POLI TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(estPoli.getEstPoliFormatted());
        // COB_CODE: MOVE E06-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(estPoli.getE06IdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO E06-ID-MOVI-CHIU
                estPoli.getE06IdMoviChiu().setE06IdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO E06-DS-TS-END-CPTZ
                estPoli.setE06DsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO E06-ID-MOVI-CRZ
                    estPoli.setE06IdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO E06-ID-MOVI-CHIU-NULL
                    estPoli.getE06IdMoviChiu().setE06IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E06IdMoviChiu.Len.E06_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO E06-DT-END-EFF
                    estPoli.setE06DtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO E06-DS-TS-INI-CPTZ
                    estPoli.setE06DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO E06-DS-TS-END-CPTZ
                    estPoli.setE06DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE EST-POLI TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(estPoli.getEstPoliFormatted());
        // COB_CODE: MOVE E06-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(estPoli.getE06IdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO EST-POLI.
        estPoli.setEstPoliFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO E06-ID-MOVI-CRZ.
        estPoli.setE06IdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO E06-ID-MOVI-CHIU-NULL.
        estPoli.getE06IdMoviChiu().setE06IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, E06IdMoviChiu.Len.E06_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO E06-DT-INI-EFF.
        estPoli.setE06DtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO E06-DT-END-EFF.
        estPoli.setE06DtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO E06-DS-TS-INI-CPTZ.
        estPoli.setE06DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO E06-DS-TS-END-CPTZ.
        estPoli.setE06DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO E06-COD-COMP-ANIA.
        estPoli.setE06CodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE E06-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoli.getE06DtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO E06-DT-INI-EFF-DB
        ws.getEstPoliDb().setEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE E06-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoli.getE06DtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO E06-DT-END-EFF-DB
        ws.getEstPoliDb().setRgstrzRichDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-E06-DT-ULT-PERD = 0
        //               MOVE WS-DATE-X      TO E06-DT-ULT-PERD-DB
        //           END-IF
        if (ws.getIndEstPoli().getTpParam() == 0) {
            // COB_CODE: MOVE E06-DT-ULT-PERD TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoli.getE06DtUltPerd().getE06DtUltPerd(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO E06-DT-ULT-PERD-DB
            ws.getEstPoliDb().setPervRichDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-E06-DT-EMIS-PARTNER = 0
        //               MOVE WS-DATE-X      TO E06-DT-EMIS-PARTNER-DB
        //           END-IF.
        if (ws.getIndEstPoli().getValPc() == 0) {
            // COB_CODE: MOVE E06-DT-EMIS-PARTNER TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoli.getE06DtEmisPartner().getE06DtEmisPartner(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO E06-DT-EMIS-PARTNER-DB
            ws.getEstPoliDb().setEsecRichDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE E06-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getEstPoliDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO E06-DT-INI-EFF
        estPoli.setE06DtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE E06-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getEstPoliDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO E06-DT-END-EFF
        estPoli.setE06DtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-E06-DT-ULT-PERD = 0
        //               MOVE WS-DATE-N      TO E06-DT-ULT-PERD
        //           END-IF
        if (ws.getIndEstPoli().getTpParam() == 0) {
            // COB_CODE: MOVE E06-DT-ULT-PERD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliDb().getPervRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO E06-DT-ULT-PERD
            estPoli.getE06DtUltPerd().setE06DtUltPerd(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-E06-DT-EMIS-PARTNER = 0
        //               MOVE WS-DATE-N      TO E06-DT-EMIS-PARTNER
        //           END-IF.
        if (ws.getIndEstPoli().getValPc() == 0) {
            // COB_CODE: MOVE E06-DT-EMIS-PARTNER-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliDb().getEsecRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO E06-DT-EMIS-PARTNER
            estPoli.getE06DtEmisPartner().setE06DtEmisPartner(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return estPoli.getE06CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.estPoli.setE06CodCompAnia(codCompAnia);
    }

    @Override
    public AfDecimal getCptProtetto() {
        return estPoli.getE06CptProtetto().getE06CptProtetto();
    }

    @Override
    public void setCptProtetto(AfDecimal cptProtetto) {
        this.estPoli.getE06CptProtetto().setE06CptProtetto(cptProtetto.copy());
    }

    @Override
    public AfDecimal getCptProtettoObj() {
        if (ws.getIndEstPoli().getValFl() >= 0) {
            return getCptProtetto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptProtettoObj(AfDecimal cptProtettoObj) {
        if (cptProtettoObj != null) {
            setCptProtetto(new AfDecimal(cptProtettoObj, 15, 3));
            ws.getIndEstPoli().setValFl(((short)0));
        }
        else {
            ws.getIndEstPoli().setValFl(((short)-1));
        }
    }

    @Override
    public AfDecimal getCumPreAttTakeP() {
        return estPoli.getE06CumPreAttTakeP().getE06CumPreAttTakeP();
    }

    @Override
    public void setCumPreAttTakeP(AfDecimal cumPreAttTakeP) {
        this.estPoli.getE06CumPreAttTakeP().setE06CumPreAttTakeP(cumPreAttTakeP.copy());
    }

    @Override
    public AfDecimal getCumPreAttTakePObj() {
        if (ws.getIndEstPoli().getValNum() >= 0) {
            return getCumPreAttTakeP();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCumPreAttTakePObj(AfDecimal cumPreAttTakePObj) {
        if (cumPreAttTakePObj != null) {
            setCumPreAttTakeP(new AfDecimal(cumPreAttTakePObj, 15, 3));
            ws.getIndEstPoli().setValNum(((short)0));
        }
        else {
            ws.getIndEstPoli().setValNum(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return estPoli.getE06DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.estPoli.setE06DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return estPoli.getE06DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.estPoli.setE06DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return estPoli.getE06DsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.estPoli.setE06DsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return estPoli.getE06DsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.estPoli.setE06DsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return estPoli.getE06DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.estPoli.setE06DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return estPoli.getE06DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.estPoli.setE06DsVer(dsVer);
    }

    @Override
    public String getDtEmisPartnerDb() {
        return ws.getEstPoliDb().getEsecRichDb();
    }

    @Override
    public void setDtEmisPartnerDb(String dtEmisPartnerDb) {
        this.ws.getEstPoliDb().setEsecRichDb(dtEmisPartnerDb);
    }

    @Override
    public String getDtEmisPartnerDbObj() {
        if (ws.getIndEstPoli().getValPc() >= 0) {
            return getDtEmisPartnerDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEmisPartnerDbObj(String dtEmisPartnerDbObj) {
        if (dtEmisPartnerDbObj != null) {
            setDtEmisPartnerDb(dtEmisPartnerDbObj);
            ws.getIndEstPoli().setValPc(((short)0));
        }
        else {
            ws.getIndEstPoli().setValPc(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getEstPoliDb().getRgstrzRichDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getEstPoliDb().setRgstrzRichDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getEstPoliDb().getEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getEstPoliDb().setEffDb(dtIniEffDb);
    }

    @Override
    public String getDtUltPerdDb() {
        return ws.getEstPoliDb().getPervRichDb();
    }

    @Override
    public void setDtUltPerdDb(String dtUltPerdDb) {
        this.ws.getEstPoliDb().setPervRichDb(dtUltPerdDb);
    }

    @Override
    public String getDtUltPerdDbObj() {
        if (ws.getIndEstPoli().getTpParam() >= 0) {
            return getDtUltPerdDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltPerdDbObj(String dtUltPerdDbObj) {
        if (dtUltPerdDbObj != null) {
            setDtUltPerdDb(dtUltPerdDbObj);
            ws.getIndEstPoli().setTpParam(((short)0));
        }
        else {
            ws.getIndEstPoli().setTpParam(((short)-1));
        }
    }

    @Override
    public long getE06DsRiga() {
        return estPoli.getE06DsRiga();
    }

    @Override
    public void setE06DsRiga(long e06DsRiga) {
        this.estPoli.setE06DsRiga(e06DsRiga);
    }

    @Override
    public String getEsiAdegzIsvap() {
        return estPoli.getE06EsiAdegzIsvap();
    }

    @Override
    public void setEsiAdegzIsvap(String esiAdegzIsvap) {
        this.estPoli.setE06EsiAdegzIsvap(esiAdegzIsvap);
    }

    @Override
    public String getEsiAdegzIsvapObj() {
        if (ws.getIndEstPoli().getValDt() >= 0) {
            return getEsiAdegzIsvap();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEsiAdegzIsvapObj(String esiAdegzIsvapObj) {
        if (esiAdegzIsvapObj != null) {
            setEsiAdegzIsvap(esiAdegzIsvapObj);
            ws.getIndEstPoli().setValDt(((short)0));
        }
        else {
            ws.getIndEstPoli().setValDt(((short)-1));
        }
    }

    @Override
    public char getFlEsclSwitchMax() {
        return estPoli.getE06FlEsclSwitchMax();
    }

    @Override
    public void setFlEsclSwitchMax(char flEsclSwitchMax) {
        this.estPoli.setE06FlEsclSwitchMax(flEsclSwitchMax);
    }

    @Override
    public Character getFlEsclSwitchMaxObj() {
        if (ws.getIndEstPoli().getValImp() >= 0) {
            return ((Character)getFlEsclSwitchMax());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEsclSwitchMaxObj(Character flEsclSwitchMaxObj) {
        if (flEsclSwitchMaxObj != null) {
            setFlEsclSwitchMax(((char)flEsclSwitchMaxObj));
            ws.getIndEstPoli().setValImp(((short)0));
        }
        else {
            ws.getIndEstPoli().setValImp(((short)-1));
        }
    }

    @Override
    public String getIbOgg() {
        return estPoli.getE06IbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.estPoli.setE06IbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndEstPoli().getIdMoviChiu() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndEstPoli().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndEstPoli().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public String getIbProp() {
        return estPoli.getE06IbProp();
    }

    @Override
    public void setIbProp(String ibProp) {
        this.estPoli.setE06IbProp(ibProp);
    }

    @Override
    public String getIdEstPoli() {
        return estPoli.getE06IdEstPoli();
    }

    @Override
    public void setIdEstPoli(String idEstPoli) {
        this.estPoli.setE06IdEstPoli(idEstPoli);
    }

    @Override
    public int getIdMoviChiu() {
        return estPoli.getE06IdMoviChiu().getE06IdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.estPoli.getE06IdMoviChiu().setE06IdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndEstPoli().getCodParam() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndEstPoli().setCodParam(((short)0));
        }
        else {
            ws.getIndEstPoli().setCodParam(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return estPoli.getE06IdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.estPoli.setE06IdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return estPoli.getE06IdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.estPoli.setE06IdPoli(idPoli);
    }

    @Override
    public String getNumIna() {
        return estPoli.getE06NumIna();
    }

    @Override
    public void setNumIna(String numIna) {
        this.estPoli.setE06NumIna(numIna);
    }

    @Override
    public String getNumInaObj() {
        if (ws.getIndEstPoli().getValTs() >= 0) {
            return getNumIna();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumInaObj(String numInaObj) {
        if (numInaObj != null) {
            setNumIna(numInaObj);
            ws.getIndEstPoli().setValTs(((short)0));
        }
        else {
            ws.getIndEstPoli().setValTs(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcUltPerd() {
        return estPoli.getE06PcUltPerd().getE06PcUltPerd();
    }

    @Override
    public void setPcUltPerd(AfDecimal pcUltPerd) {
        this.estPoli.getE06PcUltPerd().setE06PcUltPerd(pcUltPerd.copy());
    }

    @Override
    public AfDecimal getPcUltPerdObj() {
        if (ws.getIndEstPoli().getTpD() >= 0) {
            return getPcUltPerd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcUltPerdObj(AfDecimal pcUltPerdObj) {
        if (pcUltPerdObj != null) {
            setPcUltPerd(new AfDecimal(pcUltPerdObj, 6, 3));
            ws.getIndEstPoli().setTpD(((short)0));
        }
        else {
            ws.getIndEstPoli().setTpD(((short)-1));
        }
    }

    @Override
    public String getTpModProv() {
        return estPoli.getE06TpModProv();
    }

    @Override
    public void setTpModProv(String tpModProv) {
        this.estPoli.setE06TpModProv(tpModProv);
    }

    @Override
    public String getTpModProvObj() {
        if (ws.getIndEstPoli().getValTxt() >= 0) {
            return getTpModProv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpModProvObj(String tpModProvObj) {
        if (tpModProvObj != null) {
            setTpModProv(tpModProvObj);
            ws.getIndEstPoli().setValTxt(((short)0));
        }
        else {
            ws.getIndEstPoli().setValTxt(((short)-1));
        }
    }
}
