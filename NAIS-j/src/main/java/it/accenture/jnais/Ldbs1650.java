package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ParamDiCalcDao;
import it.accenture.jnais.commons.data.to.IParamDiCalc;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs1650Data;
import it.accenture.jnais.ws.ParamDiCalcLdbs1650;
import it.accenture.jnais.ws.redefines.PcaIdMoviChiu;
import it.accenture.jnais.ws.redefines.PcaIdOgg;
import it.accenture.jnais.ws.redefines.PcaValDt;
import it.accenture.jnais.ws.redefines.PcaValImp;
import it.accenture.jnais.ws.redefines.PcaValNum;
import it.accenture.jnais.ws.redefines.PcaValPc;
import it.accenture.jnais.ws.redefines.PcaValTs;

/**Original name: LDBS1650<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs1650 extends Program implements IParamDiCalc {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ParamDiCalcDao paramDiCalcDao = new ParamDiCalcDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs1650Data ws = new Ldbs1650Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PARAM-DI-CALC
    private ParamDiCalcLdbs1650 paramDiCalc;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS1650_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ParamDiCalcLdbs1650 paramDiCalc) {
        this.idsv0003 = idsv0003;
        this.paramDiCalc = paramDiCalc;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs1650 getInstance() {
        return ((Ldbs1650)Programs.getInstance(Ldbs1650.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS1650'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS1650");
        // COB_CODE: MOVE 'PARAM-DI-CALC' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PARAM-DI-CALC");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_PARAM_DI_CALC
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_PARAM
        //                    ,TP_D
        //                    ,VAL_DT
        //                    ,VAL_IMP
        //                    ,VAL_TS
        //                    ,VAL_NUM
        //                    ,VAL_PC
        //                    ,VAL_TXT
        //                    ,VAL_FL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM PARAM_DI_CALC
        //              WHERE  ID_OGG = :PCA-ID-OGG
        //                    AND TP_OGG = :PCA-TP-OGG
        //                    AND COD_PARAM = :PCA-COD-PARAM
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_PARAM_DI_CALC
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_PARAM
        //                    ,TP_D
        //                    ,VAL_DT
        //                    ,VAL_IMP
        //                    ,VAL_TS
        //                    ,VAL_NUM
        //                    ,VAL_PC
        //                    ,VAL_TXT
        //                    ,VAL_FL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //             INTO
        //                :PCA-ID-PARAM-DI-CALC
        //               ,:PCA-ID-OGG
        //                :IND-PCA-ID-OGG
        //               ,:PCA-TP-OGG
        //               ,:PCA-ID-MOVI-CRZ
        //               ,:PCA-ID-MOVI-CHIU
        //                :IND-PCA-ID-MOVI-CHIU
        //               ,:PCA-DT-INI-EFF-DB
        //               ,:PCA-DT-END-EFF-DB
        //               ,:PCA-COD-COMP-ANIA
        //               ,:PCA-COD-PARAM
        //                :IND-PCA-COD-PARAM
        //               ,:PCA-TP-D
        //                :IND-PCA-TP-D
        //               ,:PCA-VAL-DT-DB
        //                :IND-PCA-VAL-DT
        //               ,:PCA-VAL-IMP
        //                :IND-PCA-VAL-IMP
        //               ,:PCA-VAL-TS
        //                :IND-PCA-VAL-TS
        //               ,:PCA-VAL-NUM
        //                :IND-PCA-VAL-NUM
        //               ,:PCA-VAL-PC
        //                :IND-PCA-VAL-PC
        //               ,:PCA-VAL-TXT-VCHAR
        //                :IND-PCA-VAL-TXT
        //               ,:PCA-VAL-FL
        //                :IND-PCA-VAL-FL
        //               ,:PCA-DS-RIGA
        //               ,:PCA-DS-OPER-SQL
        //               ,:PCA-DS-VER
        //               ,:PCA-DS-TS-INI-CPTZ
        //               ,:PCA-DS-TS-END-CPTZ
        //               ,:PCA-DS-UTENTE
        //               ,:PCA-DS-STATO-ELAB
        //             FROM PARAM_DI_CALC
        //             WHERE  ID_OGG = :PCA-ID-OGG
        //                    AND TP_OGG = :PCA-TP-OGG
        //                    AND COD_PARAM = :PCA-COD-PARAM
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        paramDiCalcDao.selectRec4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        paramDiCalcDao.openCEff9(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        paramDiCalcDao.closeCEff9();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :PCA-ID-PARAM-DI-CALC
        //               ,:PCA-ID-OGG
        //                :IND-PCA-ID-OGG
        //               ,:PCA-TP-OGG
        //               ,:PCA-ID-MOVI-CRZ
        //               ,:PCA-ID-MOVI-CHIU
        //                :IND-PCA-ID-MOVI-CHIU
        //               ,:PCA-DT-INI-EFF-DB
        //               ,:PCA-DT-END-EFF-DB
        //               ,:PCA-COD-COMP-ANIA
        //               ,:PCA-COD-PARAM
        //                :IND-PCA-COD-PARAM
        //               ,:PCA-TP-D
        //                :IND-PCA-TP-D
        //               ,:PCA-VAL-DT-DB
        //                :IND-PCA-VAL-DT
        //               ,:PCA-VAL-IMP
        //                :IND-PCA-VAL-IMP
        //               ,:PCA-VAL-TS
        //                :IND-PCA-VAL-TS
        //               ,:PCA-VAL-NUM
        //                :IND-PCA-VAL-NUM
        //               ,:PCA-VAL-PC
        //                :IND-PCA-VAL-PC
        //               ,:PCA-VAL-TXT-VCHAR
        //                :IND-PCA-VAL-TXT
        //               ,:PCA-VAL-FL
        //                :IND-PCA-VAL-FL
        //               ,:PCA-DS-RIGA
        //               ,:PCA-DS-OPER-SQL
        //               ,:PCA-DS-VER
        //               ,:PCA-DS-TS-INI-CPTZ
        //               ,:PCA-DS-TS-END-CPTZ
        //               ,:PCA-DS-UTENTE
        //               ,:PCA-DS-STATO-ELAB
        //           END-EXEC.
        paramDiCalcDao.fetchCEff9(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_PARAM_DI_CALC
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_PARAM
        //                    ,TP_D
        //                    ,VAL_DT
        //                    ,VAL_IMP
        //                    ,VAL_TS
        //                    ,VAL_NUM
        //                    ,VAL_PC
        //                    ,VAL_TXT
        //                    ,VAL_FL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM PARAM_DI_CALC
        //              WHERE  ID_OGG = :PCA-ID-OGG
        //                        AND TP_OGG = :PCA-TP-OGG
        //                        AND COD_PARAM = :PCA-COD-PARAM
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_PARAM_DI_CALC
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_PARAM
        //                    ,TP_D
        //                    ,VAL_DT
        //                    ,VAL_IMP
        //                    ,VAL_TS
        //                    ,VAL_NUM
        //                    ,VAL_PC
        //                    ,VAL_TXT
        //                    ,VAL_FL
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //             INTO
        //                :PCA-ID-PARAM-DI-CALC
        //               ,:PCA-ID-OGG
        //                :IND-PCA-ID-OGG
        //               ,:PCA-TP-OGG
        //               ,:PCA-ID-MOVI-CRZ
        //               ,:PCA-ID-MOVI-CHIU
        //                :IND-PCA-ID-MOVI-CHIU
        //               ,:PCA-DT-INI-EFF-DB
        //               ,:PCA-DT-END-EFF-DB
        //               ,:PCA-COD-COMP-ANIA
        //               ,:PCA-COD-PARAM
        //                :IND-PCA-COD-PARAM
        //               ,:PCA-TP-D
        //                :IND-PCA-TP-D
        //               ,:PCA-VAL-DT-DB
        //                :IND-PCA-VAL-DT
        //               ,:PCA-VAL-IMP
        //                :IND-PCA-VAL-IMP
        //               ,:PCA-VAL-TS
        //                :IND-PCA-VAL-TS
        //               ,:PCA-VAL-NUM
        //                :IND-PCA-VAL-NUM
        //               ,:PCA-VAL-PC
        //                :IND-PCA-VAL-PC
        //               ,:PCA-VAL-TXT-VCHAR
        //                :IND-PCA-VAL-TXT
        //               ,:PCA-VAL-FL
        //                :IND-PCA-VAL-FL
        //               ,:PCA-DS-RIGA
        //               ,:PCA-DS-OPER-SQL
        //               ,:PCA-DS-VER
        //               ,:PCA-DS-TS-INI-CPTZ
        //               ,:PCA-DS-TS-END-CPTZ
        //               ,:PCA-DS-UTENTE
        //               ,:PCA-DS-STATO-ELAB
        //             FROM PARAM_DI_CALC
        //             WHERE  ID_OGG = :PCA-ID-OGG
        //                    AND TP_OGG = :PCA-TP-OGG
        //                    AND COD_PARAM = :PCA-COD-PARAM
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        paramDiCalcDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        paramDiCalcDao.openCCpz9(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        paramDiCalcDao.closeCCpz9();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :PCA-ID-PARAM-DI-CALC
        //               ,:PCA-ID-OGG
        //                :IND-PCA-ID-OGG
        //               ,:PCA-TP-OGG
        //               ,:PCA-ID-MOVI-CRZ
        //               ,:PCA-ID-MOVI-CHIU
        //                :IND-PCA-ID-MOVI-CHIU
        //               ,:PCA-DT-INI-EFF-DB
        //               ,:PCA-DT-END-EFF-DB
        //               ,:PCA-COD-COMP-ANIA
        //               ,:PCA-COD-PARAM
        //                :IND-PCA-COD-PARAM
        //               ,:PCA-TP-D
        //                :IND-PCA-TP-D
        //               ,:PCA-VAL-DT-DB
        //                :IND-PCA-VAL-DT
        //               ,:PCA-VAL-IMP
        //                :IND-PCA-VAL-IMP
        //               ,:PCA-VAL-TS
        //                :IND-PCA-VAL-TS
        //               ,:PCA-VAL-NUM
        //                :IND-PCA-VAL-NUM
        //               ,:PCA-VAL-PC
        //                :IND-PCA-VAL-PC
        //               ,:PCA-VAL-TXT-VCHAR
        //                :IND-PCA-VAL-TXT
        //               ,:PCA-VAL-FL
        //                :IND-PCA-VAL-FL
        //               ,:PCA-DS-RIGA
        //               ,:PCA-DS-OPER-SQL
        //               ,:PCA-DS-VER
        //               ,:PCA-DS-TS-INI-CPTZ
        //               ,:PCA-DS-TS-END-CPTZ
        //               ,:PCA-DS-UTENTE
        //               ,:PCA-DS-STATO-ELAB
        //           END-EXEC.
        paramDiCalcDao.fetchCCpz9(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-PCA-ID-OGG = -1
        //              MOVE HIGH-VALUES TO PCA-ID-OGG-NULL
        //           END-IF
        if (ws.getIndParamDiCalc().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-ID-OGG-NULL
            paramDiCalc.getPcaIdOgg().setPcaIdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcaIdOgg.Len.PCA_ID_OGG_NULL));
        }
        // COB_CODE: IF IND-PCA-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO PCA-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndParamDiCalc().getCodParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-ID-MOVI-CHIU-NULL
            paramDiCalc.getPcaIdMoviChiu().setPcaIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcaIdMoviChiu.Len.PCA_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-PCA-COD-PARAM = -1
        //              MOVE HIGH-VALUES TO PCA-COD-PARAM-NULL
        //           END-IF
        if (ws.getIndParamDiCalc().getTpParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-COD-PARAM-NULL
            paramDiCalc.setPcaCodParam(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamDiCalcLdbs1650.Len.PCA_COD_PARAM));
        }
        // COB_CODE: IF IND-PCA-TP-D = -1
        //              MOVE HIGH-VALUES TO PCA-TP-D-NULL
        //           END-IF
        if (ws.getIndParamDiCalc().getTpD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-TP-D-NULL
            paramDiCalc.setPcaTpD(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamDiCalcLdbs1650.Len.PCA_TP_D));
        }
        // COB_CODE: IF IND-PCA-VAL-DT = -1
        //              MOVE HIGH-VALUES TO PCA-VAL-DT-NULL
        //           END-IF
        if (ws.getIndParamDiCalc().getValImp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-VAL-DT-NULL
            paramDiCalc.getPcaValDt().setPcaValDtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcaValDt.Len.PCA_VAL_DT_NULL));
        }
        // COB_CODE: IF IND-PCA-VAL-IMP = -1
        //              MOVE HIGH-VALUES TO PCA-VAL-IMP-NULL
        //           END-IF
        if (ws.getIndParamDiCalc().getValDt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-VAL-IMP-NULL
            paramDiCalc.getPcaValImp().setPcaValImpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcaValImp.Len.PCA_VAL_IMP_NULL));
        }
        // COB_CODE: IF IND-PCA-VAL-TS = -1
        //              MOVE HIGH-VALUES TO PCA-VAL-TS-NULL
        //           END-IF
        if (ws.getIndParamDiCalc().getValTs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-VAL-TS-NULL
            paramDiCalc.getPcaValTs().setPcaValTsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcaValTs.Len.PCA_VAL_TS_NULL));
        }
        // COB_CODE: IF IND-PCA-VAL-NUM = -1
        //              MOVE HIGH-VALUES TO PCA-VAL-NUM-NULL
        //           END-IF
        if (ws.getIndParamDiCalc().getValTxt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-VAL-NUM-NULL
            paramDiCalc.getPcaValNum().setPcaValNumNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcaValNum.Len.PCA_VAL_NUM_NULL));
        }
        // COB_CODE: IF IND-PCA-VAL-PC = -1
        //              MOVE HIGH-VALUES TO PCA-VAL-PC-NULL
        //           END-IF
        if (ws.getIndParamDiCalc().getValFl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-VAL-PC-NULL
            paramDiCalc.getPcaValPc().setPcaValPcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PcaValPc.Len.PCA_VAL_PC_NULL));
        }
        // COB_CODE: IF IND-PCA-VAL-TXT = -1
        //              MOVE HIGH-VALUES TO PCA-VAL-TXT
        //           END-IF
        if (ws.getIndParamDiCalc().getValNum() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-VAL-TXT
            paramDiCalc.setPcaValTxt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamDiCalcLdbs1650.Len.PCA_VAL_TXT));
        }
        // COB_CODE: IF IND-PCA-VAL-FL = -1
        //              MOVE HIGH-VALUES TO PCA-VAL-FL-NULL
        //           END-IF.
        if (ws.getIndParamDiCalc().getValPc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO PCA-VAL-FL-NULL
            paramDiCalc.setPcaValFl(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE PCA-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getParamDiCalcDb().getDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PCA-DT-INI-EFF
        paramDiCalc.setPcaDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE PCA-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getParamDiCalcDb().getDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO PCA-DT-END-EFF
        paramDiCalc.setPcaDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-PCA-VAL-DT = 0
        //               MOVE WS-DATE-N      TO PCA-VAL-DT
        //           END-IF.
        if (ws.getIndParamDiCalc().getValImp() == 0) {
            // COB_CODE: MOVE PCA-VAL-DT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamDiCalcDb().getValDtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO PCA-VAL-DT
            paramDiCalc.getPcaValDt().setPcaValDt(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF PCA-VAL-TXT
        //                       TO PCA-VAL-TXT-LEN.
        paramDiCalc.setPcaValTxtLen(((short)ParamDiCalcLdbs1650.Len.PCA_VAL_TXT));
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return paramDiCalc.getPcaCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.paramDiCalc.setPcaCodCompAnia(codCompAnia);
    }

    @Override
    public char getDsOperSql() {
        return paramDiCalc.getPcaDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.paramDiCalc.setPcaDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return paramDiCalc.getPcaDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.paramDiCalc.setPcaDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return paramDiCalc.getPcaDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.paramDiCalc.setPcaDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return paramDiCalc.getPcaDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.paramDiCalc.setPcaDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return paramDiCalc.getPcaDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.paramDiCalc.setPcaDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return paramDiCalc.getPcaDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.paramDiCalc.setPcaDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getParamDiCalcDb().getDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getParamDiCalcDb().setDtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getParamDiCalcDb().getDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getParamDiCalcDb().setDtIniEffDb(dtIniEffDb);
    }

    @Override
    public int getIdMoviChiu() {
        return paramDiCalc.getPcaIdMoviChiu().getPcaIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.paramDiCalc.getPcaIdMoviChiu().setPcaIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndParamDiCalc().getCodParam() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndParamDiCalc().setCodParam(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setCodParam(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return paramDiCalc.getPcaIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.paramDiCalc.setPcaIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdParamDiCalc() {
        return paramDiCalc.getPcaIdParamDiCalc();
    }

    @Override
    public void setIdParamDiCalc(int idParamDiCalc) {
        this.paramDiCalc.setPcaIdParamDiCalc(idParamDiCalc);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getPcaCodParam() {
        return paramDiCalc.getPcaCodParam();
    }

    @Override
    public void setPcaCodParam(String pcaCodParam) {
        this.paramDiCalc.setPcaCodParam(pcaCodParam);
    }

    @Override
    public String getPcaCodParamObj() {
        if (ws.getIndParamDiCalc().getTpParam() >= 0) {
            return getPcaCodParam();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcaCodParamObj(String pcaCodParamObj) {
        if (pcaCodParamObj != null) {
            setPcaCodParam(pcaCodParamObj);
            ws.getIndParamDiCalc().setTpParam(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setTpParam(((short)-1));
        }
    }

    @Override
    public long getPcaDsRiga() {
        return paramDiCalc.getPcaDsRiga();
    }

    @Override
    public void setPcaDsRiga(long pcaDsRiga) {
        this.paramDiCalc.setPcaDsRiga(pcaDsRiga);
    }

    @Override
    public int getPcaIdOgg() {
        return paramDiCalc.getPcaIdOgg().getPcaIdOgg();
    }

    @Override
    public void setPcaIdOgg(int pcaIdOgg) {
        this.paramDiCalc.getPcaIdOgg().setPcaIdOgg(pcaIdOgg);
    }

    @Override
    public Integer getPcaIdOggObj() {
        if (ws.getIndParamDiCalc().getIdMoviChiu() >= 0) {
            return ((Integer)getPcaIdOgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcaIdOggObj(Integer pcaIdOggObj) {
        if (pcaIdOggObj != null) {
            setPcaIdOgg(((int)pcaIdOggObj));
            ws.getIndParamDiCalc().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public String getPcaTpOgg() {
        return paramDiCalc.getPcaTpOgg();
    }

    @Override
    public void setPcaTpOgg(String pcaTpOgg) {
        this.paramDiCalc.setPcaTpOgg(pcaTpOgg);
    }

    @Override
    public String getTpD() {
        return paramDiCalc.getPcaTpD();
    }

    @Override
    public void setTpD(String tpD) {
        this.paramDiCalc.setPcaTpD(tpD);
    }

    @Override
    public String getTpDObj() {
        if (ws.getIndParamDiCalc().getTpD() >= 0) {
            return getTpD();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDObj(String tpDObj) {
        if (tpDObj != null) {
            setTpD(tpDObj);
            ws.getIndParamDiCalc().setTpD(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setTpD(((short)-1));
        }
    }

    @Override
    public String getValDtDb() {
        return ws.getParamDiCalcDb().getValDtDb();
    }

    @Override
    public void setValDtDb(String valDtDb) {
        this.ws.getParamDiCalcDb().setValDtDb(valDtDb);
    }

    @Override
    public String getValDtDbObj() {
        if (ws.getIndParamDiCalc().getValImp() >= 0) {
            return getValDtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValDtDbObj(String valDtDbObj) {
        if (valDtDbObj != null) {
            setValDtDb(valDtDbObj);
            ws.getIndParamDiCalc().setValImp(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setValImp(((short)-1));
        }
    }

    @Override
    public char getValFl() {
        return paramDiCalc.getPcaValFl();
    }

    @Override
    public void setValFl(char valFl) {
        this.paramDiCalc.setPcaValFl(valFl);
    }

    @Override
    public Character getValFlObj() {
        if (ws.getIndParamDiCalc().getValPc() >= 0) {
            return ((Character)getValFl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setValFlObj(Character valFlObj) {
        if (valFlObj != null) {
            setValFl(((char)valFlObj));
            ws.getIndParamDiCalc().setValPc(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setValPc(((short)-1));
        }
    }

    @Override
    public AfDecimal getValImp() {
        return paramDiCalc.getPcaValImp().getPcaValImp();
    }

    @Override
    public void setValImp(AfDecimal valImp) {
        this.paramDiCalc.getPcaValImp().setPcaValImp(valImp.copy());
    }

    @Override
    public AfDecimal getValImpObj() {
        if (ws.getIndParamDiCalc().getValDt() >= 0) {
            return getValImp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValImpObj(AfDecimal valImpObj) {
        if (valImpObj != null) {
            setValImp(new AfDecimal(valImpObj, 15, 3));
            ws.getIndParamDiCalc().setValDt(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setValDt(((short)-1));
        }
    }

    @Override
    public long getValNum() {
        return paramDiCalc.getPcaValNum().getPcaValNum();
    }

    @Override
    public void setValNum(long valNum) {
        this.paramDiCalc.getPcaValNum().setPcaValNum(valNum);
    }

    @Override
    public Long getValNumObj() {
        if (ws.getIndParamDiCalc().getValTxt() >= 0) {
            return ((Long)getValNum());
        }
        else {
            return null;
        }
    }

    @Override
    public void setValNumObj(Long valNumObj) {
        if (valNumObj != null) {
            setValNum(((long)valNumObj));
            ws.getIndParamDiCalc().setValTxt(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setValTxt(((short)-1));
        }
    }

    @Override
    public AfDecimal getValPc() {
        return paramDiCalc.getPcaValPc().getPcaValPc();
    }

    @Override
    public void setValPc(AfDecimal valPc) {
        this.paramDiCalc.getPcaValPc().setPcaValPc(valPc.copy());
    }

    @Override
    public AfDecimal getValPcObj() {
        if (ws.getIndParamDiCalc().getValFl() >= 0) {
            return getValPc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValPcObj(AfDecimal valPcObj) {
        if (valPcObj != null) {
            setValPc(new AfDecimal(valPcObj, 14, 9));
            ws.getIndParamDiCalc().setValFl(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setValFl(((short)-1));
        }
    }

    @Override
    public AfDecimal getValTs() {
        return paramDiCalc.getPcaValTs().getPcaValTs();
    }

    @Override
    public void setValTs(AfDecimal valTs) {
        this.paramDiCalc.getPcaValTs().setPcaValTs(valTs.copy());
    }

    @Override
    public AfDecimal getValTsObj() {
        if (ws.getIndParamDiCalc().getValTs() >= 0) {
            return getValTs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValTsObj(AfDecimal valTsObj) {
        if (valTsObj != null) {
            setValTs(new AfDecimal(valTsObj, 14, 9));
            ws.getIndParamDiCalc().setValTs(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setValTs(((short)-1));
        }
    }

    @Override
    public String getValTxtVchar() {
        return paramDiCalc.getPcaValTxtVcharFormatted();
    }

    @Override
    public void setValTxtVchar(String valTxtVchar) {
        this.paramDiCalc.setPcaValTxtVcharFormatted(valTxtVchar);
    }

    @Override
    public String getValTxtVcharObj() {
        if (ws.getIndParamDiCalc().getValNum() >= 0) {
            return getValTxtVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValTxtVcharObj(String valTxtVcharObj) {
        if (valTxtVcharObj != null) {
            setValTxtVchar(valTxtVcharObj);
            ws.getIndParamDiCalc().setValNum(((short)0));
        }
        else {
            ws.getIndParamDiCalc().setValNum(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
