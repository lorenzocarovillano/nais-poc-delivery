package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.Lccs0234Data;
import it.accenture.jnais.ws.WadeAreaAdesione;
import it.accenture.jnais.ws.WcomDatiInput;
import it.accenture.jnais.ws.WcomDatiOutput;
import it.accenture.jnais.ws.WgrzAreaGaranzia;
import it.accenture.jnais.ws.WpolAreaPolizza;
import it.accenture.jnais.ws.WtgaAreaSezTranche;

/**Original name: LCCS0234<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
 * *                                                        ********
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... LCCS0234
 *     TIPOLOGIA......
 *     PROCESSO....... Componenti Comuni
 *     FUNZIONE....... XXXX
 *     DESCRIZIONE.... Estrazione degli ID relativi a gli oggetti
 *                     Polizza,adesione, Garanzia e Tranche.
 * ----------------------------------------------------------------*</pre>*/
public class Lccs0234 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0234Data ws = new Lccs0234Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizza wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesione wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranzia wgrzAreaGaranzia;
    //Original name: WTGA-AREA-SEZ-TRANCHE
    private WtgaAreaSezTranche wtgaAreaSezTranche;
    //Original name: WCOM-DATI-INPUT
    private WcomDatiInput wcomDatiInput;
    //Original name: WCOM-DATI-OUTPUT
    private WcomDatiOutput wcomDatiOutput;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0234_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, WpolAreaPolizza wpolAreaPolizza, WadeAreaAdesione wadeAreaAdesione, WgrzAreaGaranzia wgrzAreaGaranzia, WtgaAreaSezTranche wtgaAreaSezTranche, WcomDatiInput wcomDatiInput, WcomDatiOutput wcomDatiOutput) {
        this.areaIdsv0001 = areaIdsv0001;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaSezTranche = wtgaAreaSezTranche;
        this.wcomDatiInput = wcomDatiInput;
        this.wcomDatiOutput = wcomDatiOutput;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0234 getInstance() {
        return ((Lccs0234)Programs.getInstance(Lccs0234.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE        WCOM-DATI-OUTPUT.
        initWcomDatiOutput();
        // COB_CODE: INITIALIZE        IX-INDICI.
        initIxIndici();
        // COB_CODE: PERFORM S0005-CTRL-DATI-INPUT
        //              THRU EX-S0005.
        s0005CtrlDatiInput();
    }

    /**Original name: S0005-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONTROLLI DATI INPUT
	 * ----------------------------------------------------------------*
	 * --  controllare che il campo WCOM-ID-OGG-EOC   Obbligatorio
	 * --  controllare che il campo WCOM-TIPO-OGG-EOC Obbligatorio</pre>*/
    private void s0005CtrlDatiInput() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: EVALUATE WCOM-TIPO-OGG-EOC
        //               WHEN 'PO'
        //                       THRU EX-S1100
        //               WHEN 'AD'
        //                       THRU EX-S1200
        //               WHEN 'GA'
        //                       THRU EX-S1300
        //               WHEN 'TG'
        //                       THRU EX-S1400
        //               WHEN 'LI'
        //               WHEN 'TL'
        //                       THRU EX-S1500
        //           END-EVALUATE.
        switch (wcomDatiInput.getTipoOggEoc()) {

            case "PO":// COB_CODE: PERFORM S1100-ELABORA-POLIZZA
                //              THRU EX-S1100
                s1100ElaboraPolizza();
                break;

            case "AD":// COB_CODE: PERFORM S1200-ELABORA-ADESIONE
                //              THRU EX-S1200
                s1200ElaboraAdesione();
                break;

            case "GA":// COB_CODE: PERFORM S1300-ELABORA-GARANZIE
                //              THRU EX-S1300
                s1300ElaboraGaranzie();
                break;

            case "TG":// COB_CODE: PERFORM S1400-ELABORA-TRANCHE
                //              THRU EX-S1400
                s1400ElaboraTranche();
                break;

            case "LI":
            case "TL":// COB_CODE: PERFORM S1500-ELABORA-LIQ
                //              THRU EX-S1500
                s1500ElaboraLiq();
                break;

            default:break;
        }
    }

    /**Original name: S1100-ELABORA-POLIZZA<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA DELL'OGGETTO PTF IN TABELLA POLIZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ElaboraPolizza() {
        // COB_CODE:      IF WPOL-ID-POLI = WCOM-ID-OGG-EOC
        //                   MOVE WPOL-ID-PTF              TO WCOM-ID-POLI-PTF
        //                ELSE
        //           *--> OGGETTO PTF NON TROVATO
        //                      THRU EX-S0300
        //                END-IF.
        if (wpolAreaPolizza.getLccvpol1().getDati().getWpolIdPoli() == wcomDatiInput.getIdOggEoc()) {
            // COB_CODE: MOVE WPOL-ID-PTF              TO WCOM-ID-OGG-PTF-EOC
            wcomDatiOutput.setIdOggPtfEoc(wpolAreaPolizza.getLccvpol1().getIdPtf());
            // COB_CODE: MOVE WPOL-IB-OGG              TO WCOM-IB-OGG-PTF-EOC
            wcomDatiOutput.setIbOggPtfEoc(wpolAreaPolizza.getLccvpol1().getDati().getWpolIbOgg());
            // COB_CODE: MOVE WPOL-ID-PTF              TO WCOM-ID-POLI-PTF
            wcomDatiOutput.setIdPoliPtf(wpolAreaPolizza.getLccvpol1().getIdPtf());
        }
        else {
            //--> OGGETTO PTF NON TROVATO
            // COB_CODE: MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1100-ELABORA-POLIZZA'  TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1100-ELABORA-POLIZZA");
            // COB_CODE: MOVE '005027'                 TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005027");
            // COB_CODE: MOVE 'POLIZZA'                TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("POLIZZA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1200-ELABORA-ADESIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA DELL'OGGETTO PTF IN TABELLA ADESIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ElaboraAdesione() {
        // COB_CODE:      IF WADE-ID-ADES = WCOM-ID-OGG-EOC
        //                   SET  SI-TROVATO            TO TRUE
        //                ELSE
        //           *--> OGGETTO PTF NON TROVATO
        //                      THRU EX-S0300
        //                END-IF.
        if (wadeAreaAdesione.getLccvade1().getDati().getWadeIdAdes() == wcomDatiInput.getIdOggEoc()) {
            // COB_CODE: MOVE WADE-ID-PTF           TO WCOM-ID-OGG-PTF-EOC
            wcomDatiOutput.setIdOggPtfEoc(wadeAreaAdesione.getLccvade1().getIdPtf());
            // COB_CODE: MOVE WADE-IB-OGG           TO WCOM-IB-OGG-PTF-EOC
            wcomDatiOutput.setIbOggPtfEoc(wadeAreaAdesione.getLccvade1().getDati().getWadeIbOgg());
            // COB_CODE: MOVE WADE-ID-PTF           TO WCOM-ID-ADES-PTF
            wcomDatiOutput.setIdAdesPtf(wadeAreaAdesione.getLccvade1().getIdPtf());
            // COB_CODE: MOVE WPOL-ID-PTF           TO WCOM-ID-POLI-PTF
            wcomDatiOutput.setIdPoliPtf(wpolAreaPolizza.getLccvpol1().getIdPtf());
            // COB_CODE: SET  SI-TROVATO            TO TRUE
            ws.getFlRicerca().setSiTrovato();
        }
        else {
            //--> OGGETTO PTF NON TROVATO
            // COB_CODE: MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1200-ELABORA-ADESIONE' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1200-ELABORA-ADESIONE");
            // COB_CODE: MOVE '005027'                 TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005027");
            // COB_CODE: MOVE 'ADESIONE'               TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("ADESIONE");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1300-ELABORA-GARANZIE<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA DELL'OGGETTO PTF IN TABELLA GARANZIE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300ElaboraGaranzie() {
        // COB_CODE: SET NO-TROVATO        TO TRUE.
        ws.getFlRicerca().setNoTrovato();
        // COB_CODE: PERFORM VARYING IX-GRZ FROM 1 BY 1
        //                     UNTIL IX-GRZ > WGRZ-ELE-GAR-MAX
        //                        OR SI-TROVATO
        //                END-IF
        //           END-PERFORM.
        ws.setIxGrz(((short)1));
        while (!(ws.getIxGrz() > wgrzAreaGaranzia.getEleGarMax() || ws.getFlRicerca().isSiTrovato())) {
            // COB_CODE: IF WGRZ-ID-GAR(IX-GRZ) = WCOM-ID-OGG-EOC
            //              SET  SI-TROVATO          TO TRUE
            //           END-IF
            if (wgrzAreaGaranzia.getTabGar(ws.getIxGrz()).getLccvgrz1().getDati().getWgrzIdGar() == wcomDatiInput.getIdOggEoc()) {
                // COB_CODE: MOVE WGRZ-ID-PTF(IX-GRZ) TO WCOM-ID-OGG-PTF-EOC
                wcomDatiOutput.setIdOggPtfEoc(wgrzAreaGaranzia.getTabGar(ws.getIxGrz()).getLccvgrz1().getIdPtf());
                // COB_CODE: MOVE WGRZ-IB-OGG(IX-GRZ) TO WCOM-IB-OGG-PTF-EOC
                wcomDatiOutput.setIbOggPtfEoc(wgrzAreaGaranzia.getTabGar(ws.getIxGrz()).getLccvgrz1().getDati().getWgrzIbOgg());
                // COB_CODE: MOVE WADE-ID-PTF         TO WCOM-ID-ADES-PTF
                wcomDatiOutput.setIdAdesPtf(wadeAreaAdesione.getLccvade1().getIdPtf());
                // COB_CODE: MOVE WPOL-ID-PTF         TO WCOM-ID-POLI-PTF
                wcomDatiOutput.setIdPoliPtf(wpolAreaPolizza.getLccvpol1().getIdPtf());
                // COB_CODE: SET  SI-TROVATO          TO TRUE
                ws.getFlRicerca().setSiTrovato();
            }
            ws.setIxGrz(Trunc.toShort(ws.getIxGrz() + 1, 4));
        }
        // COB_CODE:      IF NO-TROVATO
        //           *--> OGGETTO PTF NON TROVATO
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getFlRicerca().isNoTrovato()) {
            //--> OGGETTO PTF NON TROVATO
            // COB_CODE: MOVE WK-PGM                   TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1300-ELABORA-GARANZIE' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1300-ELABORA-GARANZIE");
            // COB_CODE: MOVE '005027'                 TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005027");
            // COB_CODE: MOVE 'GARANZIA'               TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("GARANZIA");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1400-ELABORA-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA DELL'OGGETTO PTF IN TABELLA TRANCHE GARANZIE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1400ElaboraTranche() {
        // COB_CODE: SET NO-TROVATO        TO TRUE.
        ws.getFlRicerca().setNoTrovato();
        // COB_CODE: PERFORM VARYING IX-TGA FROM 1 BY 1
        //                     UNTIL IX-TGA > WTGA-ELE-TRAN-MAX
        //                        OR SI-TROVATO
        //                END-IF
        //           END-PERFORM.
        ws.setIxTga(((short)1));
        while (!(ws.getIxTga() > wtgaAreaSezTranche.getEleTranMax() || ws.getFlRicerca().isSiTrovato())) {
            // COB_CODE: IF WTGA-ID-TRCH-DI-GAR(IX-TGA) = WCOM-ID-OGG-EOC
            //              SET SI-TROVATO           TO TRUE
            //           END-IF
            if (wtgaAreaSezTranche.getTabTran(ws.getIxTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar() == wcomDatiInput.getIdOggEoc()) {
                // COB_CODE: MOVE WTGA-ID-PTF(IX-TGA) TO WCOM-ID-OGG-PTF-EOC
                wcomDatiOutput.setIdOggPtfEoc(wtgaAreaSezTranche.getTabTran(ws.getIxTga()).getLccvtga1().getIdPtf());
                // COB_CODE: MOVE WTGA-IB-OGG(IX-TGA) TO WCOM-IB-OGG-PTF-EOC
                wcomDatiOutput.setIbOggPtfEoc(wtgaAreaSezTranche.getTabTran(ws.getIxTga()).getLccvtga1().getDati().getWtgaIbOgg());
                // COB_CODE: MOVE WADE-ID-PTF         TO WCOM-ID-ADES-PTF
                wcomDatiOutput.setIdAdesPtf(wadeAreaAdesione.getLccvade1().getIdPtf());
                // COB_CODE: MOVE WPOL-ID-PTF         TO WCOM-ID-POLI-PTF
                wcomDatiOutput.setIdPoliPtf(wpolAreaPolizza.getLccvpol1().getIdPtf());
                // COB_CODE: SET SI-TROVATO           TO TRUE
                ws.getFlRicerca().setSiTrovato();
            }
            ws.setIxTga(Trunc.toShort(ws.getIxTga() + 1, 4));
        }
        // COB_CODE:      IF NO-TROVATO
        //           *--> OGGETTO PTF NON TROVATO
        //                      THRU EX-S0300
        //                END-IF.
        if (ws.getFlRicerca().isNoTrovato()) {
            //--> OGGETTO PTF NON TROVATO
            // COB_CODE: MOVE WK-PGM                  TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'S1400-ELABORA-TRANCHE' TO IEAI9901-LABEL-ERR
            ws.getIeai9901Area().setLabelErr("S1400-ELABORA-TRANCHE");
            // COB_CODE: MOVE '005027'                TO IEAI9901-COD-ERRORE
            ws.getIeai9901Area().setCodErroreFormatted("005027");
            // COB_CODE: MOVE 'TRCH-DI-GAR'           TO IEAI9901-PARAMETRI-ERR
            ws.getIeai9901Area().setParametriErr("TRCH-DI-GAR");
            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
            //              THRU EX-S0300
            s0300RicercaGravitaErrore();
        }
    }

    /**Original name: S1500-ELABORA-LIQ<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA DELL'OGGETTO PTF IN TABELLA LIQUIDAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1500ElaboraLiq() {
        // COB_CODE: MOVE WCOM-ID-OGG-EOC              TO WCOM-ID-OGG-PTF-EOC
        wcomDatiOutput.setIdOggPtfEoc(wcomDatiInput.getIdOggEoc());
        // COB_CODE: MOVE WCOM-TIPO-OGG-EOC            TO WCOM-IB-OGG-PTF-EOC
        wcomDatiOutput.setIbOggPtfEoc(wcomDatiInput.getTipoOggEoc());
        // COB_CODE: MOVE WCOM-ID-OGG-EOC              TO WCOM-ID-POLI-PTF.
        wcomDatiOutput.setIdPoliPtf(wcomDatiInput.getIdOggEoc());
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    public void initWcomDatiOutput() {
        wcomDatiOutput.setIdOggPtfEoc(0);
        wcomDatiOutput.setIbOggPtfEoc("");
        wcomDatiOutput.setIdPoliPtf(0);
        wcomDatiOutput.setIdAdesPtf(0);
    }

    public void initIxIndici() {
        ws.setIxGrz(((short)0));
        ws.setIxTga(((short)0));
    }
}
