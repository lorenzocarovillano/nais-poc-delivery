package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.RichDao;
import it.accenture.jnais.commons.data.to.IRich;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs4510Data;
import it.accenture.jnais.ws.redefines.RicIdBatch;
import it.accenture.jnais.ws.redefines.RicIdJob;
import it.accenture.jnais.ws.redefines.RicIdOgg;
import it.accenture.jnais.ws.redefines.RicIdRichCollg;
import it.accenture.jnais.ws.redefines.RicTsEffEsecRich;
import it.accenture.jnais.ws.RichIabs0900;

/**Original name: LDBS4510<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs4510 extends Program implements IRich {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private RichDao richDao = new RichDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs4510Data ws = new Ldbs4510Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: RICH
    private RichIabs0900 rich;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS4510_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, RichIabs0900 rich) {
        this.idsv0003 = idsv0003;
        this.rich = rich;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV4511.
        ws.getLdbv4511().setLdbv4511Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs4510 getInstance() {
        return ((Ldbs4510)Programs.getInstance(Ldbs4510.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS4510'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS4510");
        // COB_CODE: MOVE 'RICH' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("RICH");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-NST CURSOR FOR
        //              SELECT
        //                     ID_RICH
        //                    ,COD_COMP_ANIA
        //                    ,TP_RICH
        //                    ,COD_MACROFUNCT
        //                    ,TP_MOVI
        //                    ,IB_RICH
        //                    ,DT_EFF
        //                    ,DT_RGSTRZ_RICH
        //                    ,DT_PERV_RICH
        //                    ,DT_ESEC_RICH
        //                    ,TS_EFF_ESEC_RICH
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,IB_POLI
        //                    ,IB_ADES
        //                    ,IB_GAR
        //                    ,IB_TRCH_DI_GAR
        //                    ,ID_BATCH
        //                    ,ID_JOB
        //                    ,FL_SIMULAZIONE
        //                    ,KEY_ORDINAMENTO
        //                    ,ID_RICH_COLLG
        //                    ,TP_RAMO_BILA
        //                    ,TP_FRM_ASSVA
        //                    ,TP_CALC_RIS
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,RAMO_BILA
        //              FROM RICH
        //              WHERE COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                     AND ID_RICH          = :RIC-ID-RICH
        //                     AND TP_RICH IN
        //                        (
        //                         :LDBV4511-TP-RICH-00
        //                        ,:LDBV4511-TP-RICH-01
        //                        ,:LDBV4511-TP-RICH-02
        //                        ,:LDBV4511-TP-RICH-03
        //                        ,:LDBV4511-TP-RICH-04
        //                        ,:LDBV4511-TP-RICH-05
        //                        ,:LDBV4511-TP-RICH-06
        //                        ,:LDBV4511-TP-RICH-07
        //                        ,:LDBV4511-TP-RICH-08
        //                        ,:LDBV4511-TP-RICH-09)
        //                        AND DS_STATO_ELAB IN
        //                        (
        //                         :LDBV4511-STATO-ELAB-00
        //                        ,:LDBV4511-STATO-ELAB-01
        //                        ,:LDBV4511-STATO-ELAB-02
        //                        ,:LDBV4511-STATO-ELAB-03
        //                        ,:LDBV4511-STATO-ELAB-04
        //                        ,:LDBV4511-STATO-ELAB-05
        //                        ,:LDBV4511-STATO-ELAB-06
        //                        ,:LDBV4511-STATO-ELAB-07
        //                        ,:LDBV4511-STATO-ELAB-08
        //                        ,:LDBV4511-STATO-ELAB-09
        //                        )
        //              UNION
        //              SELECT
        //                     ID_RICH
        //                    ,COD_COMP_ANIA
        //                    ,TP_RICH
        //                    ,COD_MACROFUNCT
        //                    ,TP_MOVI
        //                    ,IB_RICH
        //                    ,DT_EFF
        //                    ,DT_RGSTRZ_RICH
        //                    ,DT_PERV_RICH
        //                    ,DT_ESEC_RICH
        //                    ,TS_EFF_ESEC_RICH
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,IB_POLI
        //                    ,IB_ADES
        //                    ,IB_GAR
        //                    ,IB_TRCH_DI_GAR
        //                    ,ID_BATCH
        //                    ,ID_JOB
        //                    ,FL_SIMULAZIONE
        //                    ,KEY_ORDINAMENTO
        //                    ,ID_RICH_COLLG
        //                    ,TP_RAMO_BILA
        //                    ,TP_FRM_ASSVA
        //                    ,TP_CALC_RIS
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,RAMO_BILA
        //              FROM RICH
        //              WHERE COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                     AND ID_RICH_COLLG    = :RIC-ID-RICH
        //                     AND TP_RICH IN
        //                        (
        //                         :LDBV4511-TP-RICH-00
        //                        ,:LDBV4511-TP-RICH-01
        //                        ,:LDBV4511-TP-RICH-02
        //                        ,:LDBV4511-TP-RICH-03
        //                        ,:LDBV4511-TP-RICH-04
        //                        ,:LDBV4511-TP-RICH-05
        //                        ,:LDBV4511-TP-RICH-06
        //                        ,:LDBV4511-TP-RICH-07
        //                        ,:LDBV4511-TP-RICH-08
        //                        ,:LDBV4511-TP-RICH-09)
        //                        AND DS_STATO_ELAB IN
        //                        (
        //                         :LDBV4511-STATO-ELAB-00
        //                        ,:LDBV4511-STATO-ELAB-01
        //                        ,:LDBV4511-STATO-ELAB-02
        //                        ,:LDBV4511-STATO-ELAB-03
        //                        ,:LDBV4511-STATO-ELAB-04
        //                        ,:LDBV4511-STATO-ELAB-05
        //                        ,:LDBV4511-STATO-ELAB-06
        //                        ,:LDBV4511-STATO-ELAB-07
        //                        ,:LDBV4511-STATO-ELAB-08
        //                        ,:LDBV4511-STATO-ELAB-09
        //                        )
        //              ORDER BY TS_EFF_ESEC_RICH DESC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: EXEC SQL
        //                OPEN C-NST
        //           END-EXEC.
        richDao.openCNst11(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-NST
        //           END-EXEC.
        richDao.closeCNst11();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: EXEC SQL
        //                FETCH C-NST
        //           INTO
        //                :RIC-ID-RICH
        //               ,:RIC-COD-COMP-ANIA
        //               ,:RIC-TP-RICH
        //               ,:RIC-COD-MACROFUNCT
        //               ,:RIC-TP-MOVI
        //               ,:RIC-IB-RICH
        //                :IND-RIC-IB-RICH
        //               ,:RIC-DT-EFF-DB
        //               ,:RIC-DT-RGSTRZ-RICH-DB
        //               ,:RIC-DT-PERV-RICH-DB
        //               ,:RIC-DT-ESEC-RICH-DB
        //               ,:RIC-TS-EFF-ESEC-RICH
        //                :IND-RIC-TS-EFF-ESEC-RICH
        //               ,:RIC-ID-OGG
        //                :IND-RIC-ID-OGG
        //               ,:RIC-TP-OGG
        //                :IND-RIC-TP-OGG
        //               ,:RIC-IB-POLI
        //                :IND-RIC-IB-POLI
        //               ,:RIC-IB-ADES
        //                :IND-RIC-IB-ADES
        //               ,:RIC-IB-GAR
        //                :IND-RIC-IB-GAR
        //               ,:RIC-IB-TRCH-DI-GAR
        //                :IND-RIC-IB-TRCH-DI-GAR
        //               ,:RIC-ID-BATCH
        //                :IND-RIC-ID-BATCH
        //               ,:RIC-ID-JOB
        //                :IND-RIC-ID-JOB
        //               ,:RIC-FL-SIMULAZIONE
        //                :IND-RIC-FL-SIMULAZIONE
        //               ,:RIC-KEY-ORDINAMENTO-VCHAR
        //                :IND-RIC-KEY-ORDINAMENTO
        //               ,:RIC-ID-RICH-COLLG
        //                :IND-RIC-ID-RICH-COLLG
        //               ,:RIC-TP-RAMO-BILA
        //                :IND-RIC-TP-RAMO-BILA
        //               ,:RIC-TP-FRM-ASSVA
        //                :IND-RIC-TP-FRM-ASSVA
        //               ,:RIC-TP-CALC-RIS
        //                :IND-RIC-TP-CALC-RIS
        //               ,:RIC-DS-OPER-SQL
        //               ,:RIC-DS-VER
        //               ,:RIC-DS-TS-CPTZ
        //               ,:RIC-DS-UTENTE
        //               ,:RIC-DS-STATO-ELAB
        //               ,:RIC-RAMO-BILA
        //                :IND-RIC-RAMO-BILA
        //           END-EXEC.
        richDao.fetchCNst11(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST THRU C270-EX
            c270CloseCursorWcNst();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-RIC-IB-RICH = -1
        //              MOVE HIGH-VALUES TO RIC-IB-RICH-NULL
        //           END-IF
        if (ws.getIndRich().getCodStrDato2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-RICH-NULL
            rich.setRicIbRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_RICH));
        }
        // COB_CODE: IF IND-RIC-TS-EFF-ESEC-RICH = -1
        //              MOVE HIGH-VALUES TO RIC-TS-EFF-ESEC-RICH-NULL
        //           END-IF
        if (ws.getIndRich().getCodDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TS-EFF-ESEC-RICH-NULL
            rich.getRicTsEffEsecRich().setRicTsEffEsecRichNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicTsEffEsecRich.Len.RIC_TS_EFF_ESEC_RICH_NULL));
        }
        // COB_CODE: IF IND-RIC-ID-OGG = -1
        //              MOVE HIGH-VALUES TO RIC-ID-OGG-NULL
        //           END-IF
        if (ws.getIndRich().getFlagKey() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-ID-OGG-NULL
            rich.getRicIdOgg().setRicIdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicIdOgg.Len.RIC_ID_OGG_NULL));
        }
        // COB_CODE: IF IND-RIC-TP-OGG = -1
        //              MOVE HIGH-VALUES TO RIC-TP-OGG-NULL
        //           END-IF
        if (ws.getIndRich().getFlagReturnCode() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TP-OGG-NULL
            rich.setRicTpOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_TP_OGG));
        }
        // COB_CODE: IF IND-RIC-IB-POLI = -1
        //              MOVE HIGH-VALUES TO RIC-IB-POLI-NULL
        //           END-IF
        if (ws.getIndRich().getFlagCallUsing() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-POLI-NULL
            rich.setRicIbPoli(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_POLI));
        }
        // COB_CODE: IF IND-RIC-IB-ADES = -1
        //              MOVE HIGH-VALUES TO RIC-IB-ADES-NULL
        //           END-IF
        if (ws.getIndRich().getFlagWhereCond() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-ADES-NULL
            rich.setRicIbAdes(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_ADES));
        }
        // COB_CODE: IF IND-RIC-IB-GAR = -1
        //              MOVE HIGH-VALUES TO RIC-IB-GAR-NULL
        //           END-IF
        if (ws.getIndRich().getFlagRedefines() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-GAR-NULL
            rich.setRicIbGar(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_GAR));
        }
        // COB_CODE: IF IND-RIC-IB-TRCH-DI-GAR = -1
        //              MOVE HIGH-VALUES TO RIC-IB-TRCH-DI-GAR-NULL
        //           END-IF
        if (ws.getIndRich().getTipoDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-IB-TRCH-DI-GAR-NULL
            rich.setRicIbTrchDiGar(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_IB_TRCH_DI_GAR));
        }
        // COB_CODE: IF IND-RIC-ID-BATCH = -1
        //              MOVE HIGH-VALUES TO RIC-ID-BATCH-NULL
        //           END-IF
        if (ws.getIndRich().getLunghezzaDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-ID-BATCH-NULL
            rich.getRicIdBatchRP().setRicIdBatchNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicIdBatch.Len.RIC_ID_BATCH_NULL));
        }
        // COB_CODE: IF IND-RIC-ID-JOB = -1
        //              MOVE HIGH-VALUES TO RIC-ID-JOB-NULL
        //           END-IF
        if (ws.getIndRich().getPrecisioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-ID-JOB-NULL
            rich.getRicIdJob().setRicIdJobNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicIdJob.Len.RIC_ID_JOB_NULL));
        }
        // COB_CODE: IF IND-RIC-FL-SIMULAZIONE = -1
        //              MOVE HIGH-VALUES TO RIC-FL-SIMULAZIONE-NULL
        //           END-IF
        if (ws.getIndRich().getCodDominio() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-FL-SIMULAZIONE-NULL
            rich.setRicFlSimulazione(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RIC-KEY-ORDINAMENTO = -1
        //              MOVE HIGH-VALUES TO RIC-KEY-ORDINAMENTO
        //           END-IF
        if (ws.getIndRich().getFormattazioneDato() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-KEY-ORDINAMENTO
            rich.setRicKeyOrdinamento(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_KEY_ORDINAMENTO));
        }
        // COB_CODE: IF IND-RIC-ID-RICH-COLLG = -1
        //              MOVE HIGH-VALUES TO RIC-ID-RICH-COLLG-NULL
        //           END-IF
        if (ws.getIndRich().getServizioConvers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-ID-RICH-COLLG-NULL
            rich.getRicIdRichCollg().setRicIdRichCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RicIdRichCollg.Len.RIC_ID_RICH_COLLG_NULL));
        }
        // COB_CODE: IF IND-RIC-TP-RAMO-BILA = -1
        //              MOVE HIGH-VALUES TO RIC-TP-RAMO-BILA-NULL
        //           END-IF
        if (ws.getIndRich().getAreaConvStandard() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TP-RAMO-BILA-NULL
            rich.setRicTpRamoBila(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_TP_RAMO_BILA));
        }
        // COB_CODE: IF IND-RIC-TP-FRM-ASSVA = -1
        //              MOVE HIGH-VALUES TO RIC-TP-FRM-ASSVA-NULL
        //           END-IF
        if (ws.getIndRich().getRicorrenza() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TP-FRM-ASSVA-NULL
            rich.setRicTpFrmAssva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_TP_FRM_ASSVA));
        }
        // COB_CODE: IF IND-RIC-TP-CALC-RIS = -1
        //              MOVE HIGH-VALUES TO RIC-TP-CALC-RIS-NULL
        //           END-IF
        if (ws.getIndRich().getObbligatorieta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-TP-CALC-RIS-NULL
            rich.setRicTpCalcRis(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_TP_CALC_RIS));
        }
        // COB_CODE: IF IND-RIC-RAMO-BILA = -1
        //              MOVE HIGH-VALUES TO RIC-RAMO-BILA-NULL
        //           END-IF.
        if (ws.getIndRich().getValoreDefault() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RIC-RAMO-BILA-NULL
            rich.setRicRamoBila(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RichIabs0900.Len.RIC_RAMO_BILA));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE RIC-DT-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIC-DT-EFF
        rich.setRicDtEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RIC-DT-RGSTRZ-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIC-DT-RGSTRZ-RICH
        rich.setRicDtRgstrzRich(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RIC-DT-PERV-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDb().getPervRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIC-DT-PERV-RICH
        rich.setRicDtPervRich(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RIC-DT-ESEC-RICH-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRichDb().getEsecRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RIC-DT-ESEC-RICH.
        rich.setRicDtEsecRich(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF RIC-KEY-ORDINAMENTO
        //                       TO RIC-KEY-ORDINAMENTO-LEN.
        rich.setRicKeyOrdinamentoLen(((short)RichIabs0900.Len.RIC_KEY_ORDINAMENTO));
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=25, because the code is unreachable.
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=IDSP0003:line=33, because the code is unreachable.
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return rich.getRicCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.rich.setRicCodCompAnia(codCompAnia);
    }

    @Override
    public char getDsOperSql() {
        return rich.getRicDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.rich.setRicDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return rich.getRicDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.rich.setRicDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return rich.getRicDsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.rich.setRicDsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return rich.getRicDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.rich.setRicDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return rich.getRicDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.rich.setRicDsVer(dsVer);
    }

    @Override
    public String getDtEffDb() {
        return ws.getRichDb().getEffDb();
    }

    @Override
    public void setDtEffDb(String dtEffDb) {
        this.ws.getRichDb().setEffDb(dtEffDb);
    }

    @Override
    public String getDtEsecRichDb() {
        return ws.getRichDb().getEsecRichDb();
    }

    @Override
    public void setDtEsecRichDb(String dtEsecRichDb) {
        this.ws.getRichDb().setEsecRichDb(dtEsecRichDb);
    }

    @Override
    public String getDtPervRichDb() {
        return ws.getRichDb().getPervRichDb();
    }

    @Override
    public void setDtPervRichDb(String dtPervRichDb) {
        this.ws.getRichDb().setPervRichDb(dtPervRichDb);
    }

    @Override
    public String getDtRgstrzRichDb() {
        return ws.getRichDb().getRgstrzRichDb();
    }

    @Override
    public void setDtRgstrzRichDb(String dtRgstrzRichDb) {
        this.ws.getRichDb().setRgstrzRichDb(dtRgstrzRichDb);
    }

    @Override
    public char getFlSimulazione() {
        return rich.getRicFlSimulazione();
    }

    @Override
    public void setFlSimulazione(char flSimulazione) {
        this.rich.setRicFlSimulazione(flSimulazione);
    }

    @Override
    public Character getFlSimulazioneObj() {
        if (ws.getIndRich().getCodDominio() >= 0) {
            return ((Character)getFlSimulazione());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlSimulazioneObj(Character flSimulazioneObj) {
        if (flSimulazioneObj != null) {
            setFlSimulazione(((char)flSimulazioneObj));
            ws.getIndRich().setCodDominio(((short)0));
        }
        else {
            ws.getIndRich().setCodDominio(((short)-1));
        }
    }

    @Override
    public char getIabv0002State01() {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public void setIabv0002State01(char iabv0002State01) {
        throw new FieldNotMappedException("iabv0002State01");
    }

    @Override
    public char getIabv0002State02() {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public void setIabv0002State02(char iabv0002State02) {
        throw new FieldNotMappedException("iabv0002State02");
    }

    @Override
    public char getIabv0002State03() {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public void setIabv0002State03(char iabv0002State03) {
        throw new FieldNotMappedException("iabv0002State03");
    }

    @Override
    public char getIabv0002State04() {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public void setIabv0002State04(char iabv0002State04) {
        throw new FieldNotMappedException("iabv0002State04");
    }

    @Override
    public char getIabv0002State05() {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public void setIabv0002State05(char iabv0002State05) {
        throw new FieldNotMappedException("iabv0002State05");
    }

    @Override
    public char getIabv0002State06() {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public void setIabv0002State06(char iabv0002State06) {
        throw new FieldNotMappedException("iabv0002State06");
    }

    @Override
    public char getIabv0002State07() {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public void setIabv0002State07(char iabv0002State07) {
        throw new FieldNotMappedException("iabv0002State07");
    }

    @Override
    public char getIabv0002State08() {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public void setIabv0002State08(char iabv0002State08) {
        throw new FieldNotMappedException("iabv0002State08");
    }

    @Override
    public char getIabv0002State09() {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public void setIabv0002State09(char iabv0002State09) {
        throw new FieldNotMappedException("iabv0002State09");
    }

    @Override
    public char getIabv0002State10() {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public void setIabv0002State10(char iabv0002State10) {
        throw new FieldNotMappedException("iabv0002State10");
    }

    @Override
    public char getIabv0002StateCurrent() {
        throw new FieldNotMappedException("iabv0002StateCurrent");
    }

    @Override
    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        throw new FieldNotMappedException("iabv0002StateCurrent");
    }

    @Override
    public String getIbAdes() {
        return rich.getRicIbAdes();
    }

    @Override
    public void setIbAdes(String ibAdes) {
        this.rich.setRicIbAdes(ibAdes);
    }

    @Override
    public String getIbAdesObj() {
        if (ws.getIndRich().getFlagWhereCond() >= 0) {
            return getIbAdes();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbAdesObj(String ibAdesObj) {
        if (ibAdesObj != null) {
            setIbAdes(ibAdesObj);
            ws.getIndRich().setFlagWhereCond(((short)0));
        }
        else {
            ws.getIndRich().setFlagWhereCond(((short)-1));
        }
    }

    @Override
    public String getIbGar() {
        return rich.getRicIbGar();
    }

    @Override
    public void setIbGar(String ibGar) {
        this.rich.setRicIbGar(ibGar);
    }

    @Override
    public String getIbGarObj() {
        if (ws.getIndRich().getFlagRedefines() >= 0) {
            return getIbGar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbGarObj(String ibGarObj) {
        if (ibGarObj != null) {
            setIbGar(ibGarObj);
            ws.getIndRich().setFlagRedefines(((short)0));
        }
        else {
            ws.getIndRich().setFlagRedefines(((short)-1));
        }
    }

    @Override
    public String getIbPoli() {
        return rich.getRicIbPoli();
    }

    @Override
    public void setIbPoli(String ibPoli) {
        this.rich.setRicIbPoli(ibPoli);
    }

    @Override
    public String getIbPoliObj() {
        if (ws.getIndRich().getFlagCallUsing() >= 0) {
            return getIbPoli();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbPoliObj(String ibPoliObj) {
        if (ibPoliObj != null) {
            setIbPoli(ibPoliObj);
            ws.getIndRich().setFlagCallUsing(((short)0));
        }
        else {
            ws.getIndRich().setFlagCallUsing(((short)-1));
        }
    }

    @Override
    public String getIbRich() {
        return rich.getRicIbRich();
    }

    @Override
    public void setIbRich(String ibRich) {
        this.rich.setRicIbRich(ibRich);
    }

    @Override
    public String getIbRichObj() {
        if (ws.getIndRich().getCodStrDato2() >= 0) {
            return getIbRich();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbRichObj(String ibRichObj) {
        if (ibRichObj != null) {
            setIbRich(ibRichObj);
            ws.getIndRich().setCodStrDato2(((short)0));
        }
        else {
            ws.getIndRich().setCodStrDato2(((short)-1));
        }
    }

    @Override
    public String getIbTrchDiGar() {
        return rich.getRicIbTrchDiGar();
    }

    @Override
    public void setIbTrchDiGar(String ibTrchDiGar) {
        this.rich.setRicIbTrchDiGar(ibTrchDiGar);
    }

    @Override
    public String getIbTrchDiGarObj() {
        if (ws.getIndRich().getTipoDato() >= 0) {
            return getIbTrchDiGar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbTrchDiGarObj(String ibTrchDiGarObj) {
        if (ibTrchDiGarObj != null) {
            setIbTrchDiGar(ibTrchDiGarObj);
            ws.getIndRich().setTipoDato(((short)0));
        }
        else {
            ws.getIndRich().setTipoDato(((short)-1));
        }
    }

    @Override
    public int getIdJob() {
        return rich.getRicIdJob().getRicIdJob();
    }

    @Override
    public void setIdJob(int idJob) {
        this.rich.getRicIdJob().setRicIdJob(idJob);
    }

    @Override
    public Integer getIdJobObj() {
        if (ws.getIndRich().getPrecisioneDato() >= 0) {
            return ((Integer)getIdJob());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdJobObj(Integer idJobObj) {
        if (idJobObj != null) {
            setIdJob(((int)idJobObj));
            ws.getIndRich().setPrecisioneDato(((short)0));
        }
        else {
            ws.getIndRich().setPrecisioneDato(((short)-1));
        }
    }

    @Override
    public int getIdOgg() {
        return rich.getRicIdOgg().getRicIdOgg();
    }

    @Override
    public void setIdOgg(int idOgg) {
        this.rich.getRicIdOgg().setRicIdOgg(idOgg);
    }

    @Override
    public Integer getIdOggObj() {
        if (ws.getIndRich().getFlagKey() >= 0) {
            return ((Integer)getIdOgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdOggObj(Integer idOggObj) {
        if (idOggObj != null) {
            setIdOgg(((int)idOggObj));
            ws.getIndRich().setFlagKey(((short)0));
        }
        else {
            ws.getIndRich().setFlagKey(((short)-1));
        }
    }

    @Override
    public int getIdRichCollg() {
        return rich.getRicIdRichCollg().getRicIdRichCollg();
    }

    @Override
    public void setIdRichCollg(int idRichCollg) {
        this.rich.getRicIdRichCollg().setRicIdRichCollg(idRichCollg);
    }

    @Override
    public Integer getIdRichCollgObj() {
        if (ws.getIndRich().getServizioConvers() >= 0) {
            return ((Integer)getIdRichCollg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichCollgObj(Integer idRichCollgObj) {
        if (idRichCollgObj != null) {
            setIdRichCollg(((int)idRichCollgObj));
            ws.getIndRich().setServizioConvers(((short)0));
        }
        else {
            ws.getIndRich().setServizioConvers(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getKeyOrdinamentoVchar() {
        return rich.getRicKeyOrdinamentoVcharFormatted();
    }

    @Override
    public void setKeyOrdinamentoVchar(String keyOrdinamentoVchar) {
        this.rich.setRicKeyOrdinamentoVcharFormatted(keyOrdinamentoVchar);
    }

    @Override
    public String getKeyOrdinamentoVcharObj() {
        if (ws.getIndRich().getFormattazioneDato() >= 0) {
            return getKeyOrdinamentoVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setKeyOrdinamentoVcharObj(String keyOrdinamentoVcharObj) {
        if (keyOrdinamentoVcharObj != null) {
            setKeyOrdinamentoVchar(keyOrdinamentoVcharObj);
            ws.getIndRich().setFormattazioneDato(((short)0));
        }
        else {
            ws.getIndRich().setFormattazioneDato(((short)-1));
        }
    }

    @Override
    public char getLdbv4511StatoElab00() {
        return ws.getLdbv4511().getStatoElab00();
    }

    @Override
    public void setLdbv4511StatoElab00(char ldbv4511StatoElab00) {
        this.ws.getLdbv4511().setStatoElab00(ldbv4511StatoElab00);
    }

    @Override
    public char getLdbv4511StatoElab01() {
        return ws.getLdbv4511().getStatoElab01();
    }

    @Override
    public void setLdbv4511StatoElab01(char ldbv4511StatoElab01) {
        this.ws.getLdbv4511().setStatoElab01(ldbv4511StatoElab01);
    }

    @Override
    public char getLdbv4511StatoElab02() {
        return ws.getLdbv4511().getStatoElab02();
    }

    @Override
    public void setLdbv4511StatoElab02(char ldbv4511StatoElab02) {
        this.ws.getLdbv4511().setStatoElab02(ldbv4511StatoElab02);
    }

    @Override
    public char getLdbv4511StatoElab03() {
        return ws.getLdbv4511().getStatoElab03();
    }

    @Override
    public void setLdbv4511StatoElab03(char ldbv4511StatoElab03) {
        this.ws.getLdbv4511().setStatoElab03(ldbv4511StatoElab03);
    }

    @Override
    public char getLdbv4511StatoElab04() {
        return ws.getLdbv4511().getStatoElab04();
    }

    @Override
    public void setLdbv4511StatoElab04(char ldbv4511StatoElab04) {
        this.ws.getLdbv4511().setStatoElab04(ldbv4511StatoElab04);
    }

    @Override
    public char getLdbv4511StatoElab05() {
        return ws.getLdbv4511().getStatoElab05();
    }

    @Override
    public void setLdbv4511StatoElab05(char ldbv4511StatoElab05) {
        this.ws.getLdbv4511().setStatoElab05(ldbv4511StatoElab05);
    }

    @Override
    public char getLdbv4511StatoElab06() {
        return ws.getLdbv4511().getStatoElab06();
    }

    @Override
    public void setLdbv4511StatoElab06(char ldbv4511StatoElab06) {
        this.ws.getLdbv4511().setStatoElab06(ldbv4511StatoElab06);
    }

    @Override
    public char getLdbv4511StatoElab07() {
        return ws.getLdbv4511().getStatoElab07();
    }

    @Override
    public void setLdbv4511StatoElab07(char ldbv4511StatoElab07) {
        this.ws.getLdbv4511().setStatoElab07(ldbv4511StatoElab07);
    }

    @Override
    public char getLdbv4511StatoElab08() {
        return ws.getLdbv4511().getStatoElab08();
    }

    @Override
    public void setLdbv4511StatoElab08(char ldbv4511StatoElab08) {
        this.ws.getLdbv4511().setStatoElab08(ldbv4511StatoElab08);
    }

    @Override
    public char getLdbv4511StatoElab09() {
        return ws.getLdbv4511().getStatoElab09();
    }

    @Override
    public void setLdbv4511StatoElab09(char ldbv4511StatoElab09) {
        this.ws.getLdbv4511().setStatoElab09(ldbv4511StatoElab09);
    }

    @Override
    public String getLdbv4511TpRich00() {
        return ws.getLdbv4511().getTpRich00();
    }

    @Override
    public void setLdbv4511TpRich00(String ldbv4511TpRich00) {
        this.ws.getLdbv4511().setTpRich00(ldbv4511TpRich00);
    }

    @Override
    public String getLdbv4511TpRich01() {
        return ws.getLdbv4511().getTpRich01();
    }

    @Override
    public void setLdbv4511TpRich01(String ldbv4511TpRich01) {
        this.ws.getLdbv4511().setTpRich01(ldbv4511TpRich01);
    }

    @Override
    public String getLdbv4511TpRich02() {
        return ws.getLdbv4511().getTpRich02();
    }

    @Override
    public void setLdbv4511TpRich02(String ldbv4511TpRich02) {
        this.ws.getLdbv4511().setTpRich02(ldbv4511TpRich02);
    }

    @Override
    public String getLdbv4511TpRich03() {
        return ws.getLdbv4511().getTpRich03();
    }

    @Override
    public void setLdbv4511TpRich03(String ldbv4511TpRich03) {
        this.ws.getLdbv4511().setTpRich03(ldbv4511TpRich03);
    }

    @Override
    public String getLdbv4511TpRich04() {
        return ws.getLdbv4511().getTpRich04();
    }

    @Override
    public void setLdbv4511TpRich04(String ldbv4511TpRich04) {
        this.ws.getLdbv4511().setTpRich04(ldbv4511TpRich04);
    }

    @Override
    public String getLdbv4511TpRich05() {
        return ws.getLdbv4511().getTpRich05();
    }

    @Override
    public void setLdbv4511TpRich05(String ldbv4511TpRich05) {
        this.ws.getLdbv4511().setTpRich05(ldbv4511TpRich05);
    }

    @Override
    public String getLdbv4511TpRich06() {
        return ws.getLdbv4511().getTpRich06();
    }

    @Override
    public void setLdbv4511TpRich06(String ldbv4511TpRich06) {
        this.ws.getLdbv4511().setTpRich06(ldbv4511TpRich06);
    }

    @Override
    public String getLdbv4511TpRich07() {
        return ws.getLdbv4511().getTpRich07();
    }

    @Override
    public void setLdbv4511TpRich07(String ldbv4511TpRich07) {
        this.ws.getLdbv4511().setTpRich07(ldbv4511TpRich07);
    }

    @Override
    public String getLdbv4511TpRich08() {
        return ws.getLdbv4511().getTpRich08();
    }

    @Override
    public void setLdbv4511TpRich08(String ldbv4511TpRich08) {
        this.ws.getLdbv4511().setTpRich08(ldbv4511TpRich08);
    }

    @Override
    public String getLdbv4511TpRich09() {
        return ws.getLdbv4511().getTpRich09();
    }

    @Override
    public void setLdbv4511TpRich09(String ldbv4511TpRich09) {
        this.ws.getLdbv4511().setTpRich09(ldbv4511TpRich09);
    }

    @Override
    public String getPrenotazione() {
        throw new FieldNotMappedException("prenotazione");
    }

    @Override
    public void setPrenotazione(String prenotazione) {
        throw new FieldNotMappedException("prenotazione");
    }

    @Override
    public String getRamoBila() {
        return rich.getRicRamoBila();
    }

    @Override
    public void setRamoBila(String ramoBila) {
        this.rich.setRicRamoBila(ramoBila);
    }

    @Override
    public String getRamoBilaObj() {
        if (ws.getIndRich().getValoreDefault() >= 0) {
            return getRamoBila();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRamoBilaObj(String ramoBilaObj) {
        if (ramoBilaObj != null) {
            setRamoBila(ramoBilaObj);
            ws.getIndRich().setValoreDefault(((short)0));
        }
        else {
            ws.getIndRich().setValoreDefault(((short)-1));
        }
    }

    @Override
    public String getRicCodMacrofunct() {
        return rich.getRicCodMacrofunct();
    }

    @Override
    public void setRicCodMacrofunct(String ricCodMacrofunct) {
        this.rich.setRicCodMacrofunct(ricCodMacrofunct);
    }

    @Override
    public int getRicIdBatch() {
        return rich.getRicIdBatchRP().getRicIdBatch();
    }

    @Override
    public void setRicIdBatch(int ricIdBatch) {
        this.rich.getRicIdBatchRP().setRicIdBatch(ricIdBatch);
    }

    @Override
    public Integer getRicIdBatchObj() {
        if (ws.getIndRich().getLunghezzaDato() >= 0) {
            return ((Integer)getRicIdBatch());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRicIdBatchObj(Integer ricIdBatchObj) {
        if (ricIdBatchObj != null) {
            setRicIdBatch(((int)ricIdBatchObj));
            ws.getIndRich().setLunghezzaDato(((short)0));
        }
        else {
            ws.getIndRich().setLunghezzaDato(((short)-1));
        }
    }

    @Override
    public int getRicIdRich() {
        return rich.getRicIdRich();
    }

    @Override
    public void setRicIdRich(int ricIdRich) {
        this.rich.setRicIdRich(ricIdRich);
    }

    @Override
    public int getRicTpMovi() {
        return rich.getRicTpMovi();
    }

    @Override
    public void setRicTpMovi(int ricTpMovi) {
        this.rich.setRicTpMovi(ricTpMovi);
    }

    @Override
    public String getTpCalcRis() {
        return rich.getRicTpCalcRis();
    }

    @Override
    public void setTpCalcRis(String tpCalcRis) {
        this.rich.setRicTpCalcRis(tpCalcRis);
    }

    @Override
    public String getTpCalcRisObj() {
        if (ws.getIndRich().getObbligatorieta() >= 0) {
            return getTpCalcRis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCalcRisObj(String tpCalcRisObj) {
        if (tpCalcRisObj != null) {
            setTpCalcRis(tpCalcRisObj);
            ws.getIndRich().setObbligatorieta(((short)0));
        }
        else {
            ws.getIndRich().setObbligatorieta(((short)-1));
        }
    }

    @Override
    public String getTpFrmAssva() {
        return rich.getRicTpFrmAssva();
    }

    @Override
    public void setTpFrmAssva(String tpFrmAssva) {
        this.rich.setRicTpFrmAssva(tpFrmAssva);
    }

    @Override
    public String getTpFrmAssvaObj() {
        if (ws.getIndRich().getRicorrenza() >= 0) {
            return getTpFrmAssva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpFrmAssvaObj(String tpFrmAssvaObj) {
        if (tpFrmAssvaObj != null) {
            setTpFrmAssva(tpFrmAssvaObj);
            ws.getIndRich().setRicorrenza(((short)0));
        }
        else {
            ws.getIndRich().setRicorrenza(((short)-1));
        }
    }

    @Override
    public String getTpOgg() {
        return rich.getRicTpOgg();
    }

    @Override
    public void setTpOgg(String tpOgg) {
        this.rich.setRicTpOgg(tpOgg);
    }

    @Override
    public String getTpOggObj() {
        if (ws.getIndRich().getFlagReturnCode() >= 0) {
            return getTpOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpOggObj(String tpOggObj) {
        if (tpOggObj != null) {
            setTpOgg(tpOggObj);
            ws.getIndRich().setFlagReturnCode(((short)0));
        }
        else {
            ws.getIndRich().setFlagReturnCode(((short)-1));
        }
    }

    @Override
    public String getTpRamoBila() {
        return rich.getRicTpRamoBila();
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        this.rich.setRicTpRamoBila(tpRamoBila);
    }

    @Override
    public String getTpRamoBilaObj() {
        if (ws.getIndRich().getAreaConvStandard() >= 0) {
            return getTpRamoBila();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRamoBilaObj(String tpRamoBilaObj) {
        if (tpRamoBilaObj != null) {
            setTpRamoBila(tpRamoBilaObj);
            ws.getIndRich().setAreaConvStandard(((short)0));
        }
        else {
            ws.getIndRich().setAreaConvStandard(((short)-1));
        }
    }

    @Override
    public String getTpRich() {
        return rich.getRicTpRich();
    }

    @Override
    public void setTpRich(String tpRich) {
        this.rich.setRicTpRich(tpRich);
    }

    @Override
    public long getTsEffEsecRich() {
        return rich.getRicTsEffEsecRich().getRicTsEffEsecRich();
    }

    @Override
    public void setTsEffEsecRich(long tsEffEsecRich) {
        this.rich.getRicTsEffEsecRich().setRicTsEffEsecRich(tsEffEsecRich);
    }

    @Override
    public Long getTsEffEsecRichObj() {
        if (ws.getIndRich().getCodDato() >= 0) {
            return ((Long)getTsEffEsecRich());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsEffEsecRichObj(Long tsEffEsecRichObj) {
        if (tsEffEsecRichObj != null) {
            setTsEffEsecRich(((long)tsEffEsecRichObj));
            ws.getIndRich().setCodDato(((short)0));
        }
        else {
            ws.getIndRich().setCodDato(((short)-1));
        }
    }
}
