package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettQuestDao;
import it.accenture.jnais.commons.data.to.IDettQuest;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DettQuestIdbsdeq0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsdeq0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.DeqIdCompQuest;
import it.accenture.jnais.ws.redefines.DeqIdMoviChiu;
import it.accenture.jnais.ws.redefines.DeqRispDt;
import it.accenture.jnais.ws.redefines.DeqRispImp;
import it.accenture.jnais.ws.redefines.DeqRispNum;
import it.accenture.jnais.ws.redefines.DeqRispPc;
import it.accenture.jnais.ws.redefines.DeqRispTs;

/**Original name: IDBSDEQ0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  03 GIU 2019.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsdeq0 extends Program implements IDettQuest {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DettQuestDao dettQuestDao = new DettQuestDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsdeq0Data ws = new Idbsdeq0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: DETT-QUEST
    private DettQuestIdbsdeq0 dettQuest;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSDEQ0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DettQuestIdbsdeq0 dettQuest) {
        this.idsv0003 = idsv0003;
        this.dettQuest = dettQuest;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsdeq0 getInstance() {
        return ((Idbsdeq0)Programs.getInstance(Idbsdeq0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSDEQ0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSDEQ0");
        // COB_CODE: MOVE 'DETT_QUEST' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("DETT_QUEST");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_QUEST
        //                ,ID_QUEST
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_QUEST
        //                ,COD_DOM
        //                ,COD_DOM_COLLG
        //                ,RISP_NUM
        //                ,RISP_FL
        //                ,RISP_TXT
        //                ,RISP_TS
        //                ,RISP_IMP
        //                ,RISP_PC
        //                ,RISP_DT
        //                ,RISP_KEY
        //                ,TP_RISP
        //                ,ID_COMP_QUEST
        //                ,VAL_RISP
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //             FROM DETT_QUEST
        //             WHERE     DS_RIGA = :DEQ-DS-RIGA
        //           END-EXEC.
        dettQuestDao.selectByDeqDsRiga(dettQuest.getDeqDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO DETT_QUEST
            //                  (
            //                     ID_DETT_QUEST
            //                    ,ID_QUEST
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,COD_QUEST
            //                    ,COD_DOM
            //                    ,COD_DOM_COLLG
            //                    ,RISP_NUM
            //                    ,RISP_FL
            //                    ,RISP_TXT
            //                    ,RISP_TS
            //                    ,RISP_IMP
            //                    ,RISP_PC
            //                    ,RISP_DT
            //                    ,RISP_KEY
            //                    ,TP_RISP
            //                    ,ID_COMP_QUEST
            //                    ,VAL_RISP
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                  )
            //              VALUES
            //                  (
            //                    :DEQ-ID-DETT-QUEST
            //                    ,:DEQ-ID-QUEST
            //                    ,:DEQ-ID-MOVI-CRZ
            //                    ,:DEQ-ID-MOVI-CHIU
            //                     :IND-DEQ-ID-MOVI-CHIU
            //                    ,:DEQ-DT-INI-EFF-DB
            //                    ,:DEQ-DT-END-EFF-DB
            //                    ,:DEQ-COD-COMP-ANIA
            //                    ,:DEQ-COD-QUEST
            //                     :IND-DEQ-COD-QUEST
            //                    ,:DEQ-COD-DOM
            //                     :IND-DEQ-COD-DOM
            //                    ,:DEQ-COD-DOM-COLLG
            //                     :IND-DEQ-COD-DOM-COLLG
            //                    ,:DEQ-RISP-NUM
            //                     :IND-DEQ-RISP-NUM
            //                    ,:DEQ-RISP-FL
            //                     :IND-DEQ-RISP-FL
            //                    ,:DEQ-RISP-TXT-VCHAR
            //                     :IND-DEQ-RISP-TXT
            //                    ,:DEQ-RISP-TS
            //                     :IND-DEQ-RISP-TS
            //                    ,:DEQ-RISP-IMP
            //                     :IND-DEQ-RISP-IMP
            //                    ,:DEQ-RISP-PC
            //                     :IND-DEQ-RISP-PC
            //                    ,:DEQ-RISP-DT-DB
            //                     :IND-DEQ-RISP-DT
            //                    ,:DEQ-RISP-KEY
            //                     :IND-DEQ-RISP-KEY
            //                    ,:DEQ-TP-RISP
            //                     :IND-DEQ-TP-RISP
            //                    ,:DEQ-ID-COMP-QUEST
            //                     :IND-DEQ-ID-COMP-QUEST
            //                    ,:DEQ-VAL-RISP-VCHAR
            //                     :IND-DEQ-VAL-RISP
            //                    ,:DEQ-DS-RIGA
            //                    ,:DEQ-DS-OPER-SQL
            //                    ,:DEQ-DS-VER
            //                    ,:DEQ-DS-TS-INI-CPTZ
            //                    ,:DEQ-DS-TS-END-CPTZ
            //                    ,:DEQ-DS-UTENTE
            //                    ,:DEQ-DS-STATO-ELAB
            //                  )
            //           END-EXEC
            dettQuestDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE DETT_QUEST SET
        //                   ID_DETT_QUEST          =
        //                :DEQ-ID-DETT-QUEST
        //                  ,ID_QUEST               =
        //                :DEQ-ID-QUEST
        //                  ,ID_MOVI_CRZ            =
        //                :DEQ-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DEQ-ID-MOVI-CHIU
        //                                       :IND-DEQ-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :DEQ-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DEQ-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :DEQ-COD-COMP-ANIA
        //                  ,COD_QUEST              =
        //                :DEQ-COD-QUEST
        //                                       :IND-DEQ-COD-QUEST
        //                  ,COD_DOM                =
        //                :DEQ-COD-DOM
        //                                       :IND-DEQ-COD-DOM
        //                  ,COD_DOM_COLLG          =
        //                :DEQ-COD-DOM-COLLG
        //                                       :IND-DEQ-COD-DOM-COLLG
        //                  ,RISP_NUM               =
        //                :DEQ-RISP-NUM
        //                                       :IND-DEQ-RISP-NUM
        //                  ,RISP_FL                =
        //                :DEQ-RISP-FL
        //                                       :IND-DEQ-RISP-FL
        //                  ,RISP_TXT               =
        //                :DEQ-RISP-TXT-VCHAR
        //                                       :IND-DEQ-RISP-TXT
        //                  ,RISP_TS                =
        //                :DEQ-RISP-TS
        //                                       :IND-DEQ-RISP-TS
        //                  ,RISP_IMP               =
        //                :DEQ-RISP-IMP
        //                                       :IND-DEQ-RISP-IMP
        //                  ,RISP_PC                =
        //                :DEQ-RISP-PC
        //                                       :IND-DEQ-RISP-PC
        //                  ,RISP_DT                =
        //           :DEQ-RISP-DT-DB
        //                                       :IND-DEQ-RISP-DT
        //                  ,RISP_KEY               =
        //                :DEQ-RISP-KEY
        //                                       :IND-DEQ-RISP-KEY
        //                  ,TP_RISP                =
        //                :DEQ-TP-RISP
        //                                       :IND-DEQ-TP-RISP
        //                  ,ID_COMP_QUEST          =
        //                :DEQ-ID-COMP-QUEST
        //                                       :IND-DEQ-ID-COMP-QUEST
        //                  ,VAL_RISP               =
        //                :DEQ-VAL-RISP-VCHAR
        //                                       :IND-DEQ-VAL-RISP
        //                  ,DS_RIGA                =
        //                :DEQ-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DEQ-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DEQ-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DEQ-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DEQ-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DEQ-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DEQ-DS-STATO-ELAB
        //                WHERE     DS_RIGA = :DEQ-DS-RIGA
        //           END-EXEC.
        dettQuestDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM DETT_QUEST
        //                WHERE     DS_RIGA = :DEQ-DS-RIGA
        //           END-EXEC.
        dettQuestDao.deleteByDeqDsRiga(dettQuest.getDeqDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-DEQ CURSOR FOR
        //              SELECT
        //                     ID_DETT_QUEST
        //                    ,ID_QUEST
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_QUEST
        //                    ,COD_DOM
        //                    ,COD_DOM_COLLG
        //                    ,RISP_NUM
        //                    ,RISP_FL
        //                    ,RISP_TXT
        //                    ,RISP_TS
        //                    ,RISP_IMP
        //                    ,RISP_PC
        //                    ,RISP_DT
        //                    ,RISP_KEY
        //                    ,TP_RISP
        //                    ,ID_COMP_QUEST
        //                    ,VAL_RISP
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM DETT_QUEST
        //              WHERE     ID_DETT_QUEST = :DEQ-ID-DETT-QUEST
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_QUEST
        //                ,ID_QUEST
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_QUEST
        //                ,COD_DOM
        //                ,COD_DOM_COLLG
        //                ,RISP_NUM
        //                ,RISP_FL
        //                ,RISP_TXT
        //                ,RISP_TS
        //                ,RISP_IMP
        //                ,RISP_PC
        //                ,RISP_DT
        //                ,RISP_KEY
        //                ,TP_RISP
        //                ,ID_COMP_QUEST
        //                ,VAL_RISP
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //             FROM DETT_QUEST
        //             WHERE     ID_DETT_QUEST = :DEQ-ID-DETT-QUEST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettQuestDao.selectRec(dettQuest.getDeqIdDettQuest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE DETT_QUEST SET
        //                   ID_DETT_QUEST          =
        //                :DEQ-ID-DETT-QUEST
        //                  ,ID_QUEST               =
        //                :DEQ-ID-QUEST
        //                  ,ID_MOVI_CRZ            =
        //                :DEQ-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DEQ-ID-MOVI-CHIU
        //                                       :IND-DEQ-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :DEQ-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DEQ-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :DEQ-COD-COMP-ANIA
        //                  ,COD_QUEST              =
        //                :DEQ-COD-QUEST
        //                                       :IND-DEQ-COD-QUEST
        //                  ,COD_DOM                =
        //                :DEQ-COD-DOM
        //                                       :IND-DEQ-COD-DOM
        //                  ,COD_DOM_COLLG          =
        //                :DEQ-COD-DOM-COLLG
        //                                       :IND-DEQ-COD-DOM-COLLG
        //                  ,RISP_NUM               =
        //                :DEQ-RISP-NUM
        //                                       :IND-DEQ-RISP-NUM
        //                  ,RISP_FL                =
        //                :DEQ-RISP-FL
        //                                       :IND-DEQ-RISP-FL
        //                  ,RISP_TXT               =
        //                :DEQ-RISP-TXT-VCHAR
        //                                       :IND-DEQ-RISP-TXT
        //                  ,RISP_TS                =
        //                :DEQ-RISP-TS
        //                                       :IND-DEQ-RISP-TS
        //                  ,RISP_IMP               =
        //                :DEQ-RISP-IMP
        //                                       :IND-DEQ-RISP-IMP
        //                  ,RISP_PC                =
        //                :DEQ-RISP-PC
        //                                       :IND-DEQ-RISP-PC
        //                  ,RISP_DT                =
        //           :DEQ-RISP-DT-DB
        //                                       :IND-DEQ-RISP-DT
        //                  ,RISP_KEY               =
        //                :DEQ-RISP-KEY
        //                                       :IND-DEQ-RISP-KEY
        //                  ,TP_RISP                =
        //                :DEQ-TP-RISP
        //                                       :IND-DEQ-TP-RISP
        //                  ,ID_COMP_QUEST          =
        //                :DEQ-ID-COMP-QUEST
        //                                       :IND-DEQ-ID-COMP-QUEST
        //                  ,VAL_RISP               =
        //                :DEQ-VAL-RISP-VCHAR
        //                                       :IND-DEQ-VAL-RISP
        //                  ,DS_RIGA                =
        //                :DEQ-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DEQ-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DEQ-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DEQ-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DEQ-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DEQ-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DEQ-DS-STATO-ELAB
        //                WHERE     DS_RIGA = :DEQ-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettQuestDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-DEQ
        //           END-EXEC.
        dettQuestDao.openCIdUpdEffDeq(dettQuest.getDeqIdDettQuest(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-DEQ
        //           END-EXEC.
        dettQuestDao.closeCIdUpdEffDeq();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-DEQ
        //           INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //           END-EXEC.
        dettQuestDao.fetchCIdUpdEffDeq(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-DEQ CURSOR FOR
        //              SELECT
        //                     ID_DETT_QUEST
        //                    ,ID_QUEST
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_QUEST
        //                    ,COD_DOM
        //                    ,COD_DOM_COLLG
        //                    ,RISP_NUM
        //                    ,RISP_FL
        //                    ,RISP_TXT
        //                    ,RISP_TS
        //                    ,RISP_IMP
        //                    ,RISP_PC
        //                    ,RISP_DT
        //                    ,RISP_KEY
        //                    ,TP_RISP
        //                    ,ID_COMP_QUEST
        //                    ,VAL_RISP
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM DETT_QUEST
        //              WHERE     ID_QUEST = :DEQ-ID-QUEST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_DETT_QUEST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_QUEST
        //                ,ID_QUEST
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_QUEST
        //                ,COD_DOM
        //                ,COD_DOM_COLLG
        //                ,RISP_NUM
        //                ,RISP_FL
        //                ,RISP_TXT
        //                ,RISP_TS
        //                ,RISP_IMP
        //                ,RISP_PC
        //                ,RISP_DT
        //                ,RISP_KEY
        //                ,TP_RISP
        //                ,ID_COMP_QUEST
        //                ,VAL_RISP
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //             FROM DETT_QUEST
        //             WHERE     ID_QUEST = :DEQ-ID-QUEST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettQuestDao.selectRec1(dettQuest.getDeqIdQuest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-DEQ
        //           END-EXEC.
        dettQuestDao.openCIdpEffDeq(dettQuest.getDeqIdQuest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-DEQ
        //           END-EXEC.
        dettQuestDao.closeCIdpEffDeq();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-DEQ
        //           INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //           END-EXEC.
        dettQuestDao.fetchCIdpEffDeq(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_QUEST
        //                ,ID_QUEST
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_QUEST
        //                ,COD_DOM
        //                ,COD_DOM_COLLG
        //                ,RISP_NUM
        //                ,RISP_FL
        //                ,RISP_TXT
        //                ,RISP_TS
        //                ,RISP_IMP
        //                ,RISP_PC
        //                ,RISP_DT
        //                ,RISP_KEY
        //                ,TP_RISP
        //                ,ID_COMP_QUEST
        //                ,VAL_RISP
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //             FROM DETT_QUEST
        //             WHERE     ID_DETT_QUEST = :DEQ-ID-DETT-QUEST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dettQuestDao.selectRec2(dettQuest.getDeqIdDettQuest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-DEQ CURSOR FOR
        //              SELECT
        //                     ID_DETT_QUEST
        //                    ,ID_QUEST
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_QUEST
        //                    ,COD_DOM
        //                    ,COD_DOM_COLLG
        //                    ,RISP_NUM
        //                    ,RISP_FL
        //                    ,RISP_TXT
        //                    ,RISP_TS
        //                    ,RISP_IMP
        //                    ,RISP_PC
        //                    ,RISP_DT
        //                    ,RISP_KEY
        //                    ,TP_RISP
        //                    ,ID_COMP_QUEST
        //                    ,VAL_RISP
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM DETT_QUEST
        //              WHERE     ID_QUEST = :DEQ-ID-QUEST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_DETT_QUEST ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_QUEST
        //                ,ID_QUEST
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_QUEST
        //                ,COD_DOM
        //                ,COD_DOM_COLLG
        //                ,RISP_NUM
        //                ,RISP_FL
        //                ,RISP_TXT
        //                ,RISP_TS
        //                ,RISP_IMP
        //                ,RISP_PC
        //                ,RISP_DT
        //                ,RISP_KEY
        //                ,TP_RISP
        //                ,ID_COMP_QUEST
        //                ,VAL_RISP
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //             FROM DETT_QUEST
        //             WHERE     ID_QUEST = :DEQ-ID-QUEST
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dettQuestDao.selectRec3(dettQuest.getDeqIdQuest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-DEQ
        //           END-EXEC.
        dettQuestDao.openCIdpCpzDeq(dettQuest.getDeqIdQuest(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-DEQ
        //           END-EXEC.
        dettQuestDao.closeCIdpCpzDeq();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-DEQ
        //           INTO
        //                :DEQ-ID-DETT-QUEST
        //               ,:DEQ-ID-QUEST
        //               ,:DEQ-ID-MOVI-CRZ
        //               ,:DEQ-ID-MOVI-CHIU
        //                :IND-DEQ-ID-MOVI-CHIU
        //               ,:DEQ-DT-INI-EFF-DB
        //               ,:DEQ-DT-END-EFF-DB
        //               ,:DEQ-COD-COMP-ANIA
        //               ,:DEQ-COD-QUEST
        //                :IND-DEQ-COD-QUEST
        //               ,:DEQ-COD-DOM
        //                :IND-DEQ-COD-DOM
        //               ,:DEQ-COD-DOM-COLLG
        //                :IND-DEQ-COD-DOM-COLLG
        //               ,:DEQ-RISP-NUM
        //                :IND-DEQ-RISP-NUM
        //               ,:DEQ-RISP-FL
        //                :IND-DEQ-RISP-FL
        //               ,:DEQ-RISP-TXT-VCHAR
        //                :IND-DEQ-RISP-TXT
        //               ,:DEQ-RISP-TS
        //                :IND-DEQ-RISP-TS
        //               ,:DEQ-RISP-IMP
        //                :IND-DEQ-RISP-IMP
        //               ,:DEQ-RISP-PC
        //                :IND-DEQ-RISP-PC
        //               ,:DEQ-RISP-DT-DB
        //                :IND-DEQ-RISP-DT
        //               ,:DEQ-RISP-KEY
        //                :IND-DEQ-RISP-KEY
        //               ,:DEQ-TP-RISP
        //                :IND-DEQ-TP-RISP
        //               ,:DEQ-ID-COMP-QUEST
        //                :IND-DEQ-ID-COMP-QUEST
        //               ,:DEQ-VAL-RISP-VCHAR
        //                :IND-DEQ-VAL-RISP
        //               ,:DEQ-DS-RIGA
        //               ,:DEQ-DS-OPER-SQL
        //               ,:DEQ-DS-VER
        //               ,:DEQ-DS-TS-INI-CPTZ
        //               ,:DEQ-DS-TS-END-CPTZ
        //               ,:DEQ-DS-UTENTE
        //               ,:DEQ-DS-STATO-ELAB
        //           END-EXEC.
        dettQuestDao.fetchCIdpCpzDeq(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DEQ-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DEQ-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDettQuest().getIdOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-ID-MOVI-CHIU-NULL
            dettQuest.getDeqIdMoviChiu().setDeqIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqIdMoviChiu.Len.DEQ_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DEQ-COD-QUEST = -1
        //              MOVE HIGH-VALUES TO DEQ-COD-QUEST-NULL
        //           END-IF
        if (ws.getIndDettQuest().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-COD-QUEST-NULL
            dettQuest.setDeqCodQuest(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_COD_QUEST));
        }
        // COB_CODE: IF IND-DEQ-COD-DOM = -1
        //              MOVE HIGH-VALUES TO DEQ-COD-DOM-NULL
        //           END-IF
        if (ws.getIndDettQuest().getDtIniPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-COD-DOM-NULL
            dettQuest.setDeqCodDom(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_COD_DOM));
        }
        // COB_CODE: IF IND-DEQ-COD-DOM-COLLG = -1
        //              MOVE HIGH-VALUES TO DEQ-COD-DOM-COLLG-NULL
        //           END-IF
        if (ws.getIndDettQuest().getDtEndPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-COD-DOM-COLLG-NULL
            dettQuest.setDeqCodDomCollg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_COD_DOM_COLLG));
        }
        // COB_CODE: IF IND-DEQ-RISP-NUM = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-NUM-NULL
        //           END-IF
        if (ws.getIndDettQuest().getImpstSost() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-NUM-NULL
            dettQuest.getDeqRispNum().setDeqRispNumNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispNum.Len.DEQ_RISP_NUM_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-FL = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-FL-NULL
        //           END-IF
        if (ws.getIndDettQuest().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-FL-NULL
            dettQuest.setDeqRispFl(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DEQ-RISP-TXT = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-TXT
        //           END-IF
        if (ws.getIndDettQuest().getAlqIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-TXT
            dettQuest.setDeqRispTxt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_RISP_TXT));
        }
        // COB_CODE: IF IND-DEQ-RISP-TS = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-TS-NULL
        //           END-IF
        if (ws.getIndDettQuest().getCodTrb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-TS-NULL
            dettQuest.getDeqRispTs().setDeqRispTsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispTs.Len.DEQ_RISP_TS_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-IMP = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-IMP-NULL
        //           END-IF
        if (ws.getIndDettQuest().getPrstzLrdAnteIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-IMP-NULL
            dettQuest.getDeqRispImp().setDeqRispImpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispImp.Len.DEQ_RISP_IMP_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-PC = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-PC-NULL
        //           END-IF
        if (ws.getIndDettQuest().getRisMatNetPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-PC-NULL
            dettQuest.getDeqRispPc().setDeqRispPcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispPc.Len.DEQ_RISP_PC_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-DT = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-DT-NULL
        //           END-IF
        if (ws.getIndDettQuest().getRisMatAnteTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-DT-NULL
            dettQuest.getDeqRispDt().setDeqRispDtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqRispDt.Len.DEQ_RISP_DT_NULL));
        }
        // COB_CODE: IF IND-DEQ-RISP-KEY = -1
        //              MOVE HIGH-VALUES TO DEQ-RISP-KEY-NULL
        //           END-IF
        if (ws.getIndDettQuest().getRisMatPostTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-RISP-KEY-NULL
            dettQuest.setDeqRispKey(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_RISP_KEY));
        }
        // COB_CODE: IF IND-DEQ-TP-RISP = -1
        //              MOVE HIGH-VALUES TO DEQ-TP-RISP-NULL
        //           END-IF
        if (ws.getIndDettQuest().getPrstzNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-TP-RISP-NULL
            dettQuest.setDeqTpRisp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_TP_RISP));
        }
        // COB_CODE: IF IND-DEQ-ID-COMP-QUEST = -1
        //              MOVE HIGH-VALUES TO DEQ-ID-COMP-QUEST-NULL
        //           END-IF
        if (ws.getIndDettQuest().getPrstzPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-ID-COMP-QUEST-NULL
            dettQuest.getDeqIdCompQuest().setDeqIdCompQuestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqIdCompQuest.Len.DEQ_ID_COMP_QUEST_NULL));
        }
        // COB_CODE: IF IND-DEQ-VAL-RISP = -1
        //              MOVE HIGH-VALUES TO DEQ-VAL-RISP
        //           END-IF.
        if (ws.getIndDettQuest().getCumPreVers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DEQ-VAL-RISP
            dettQuest.setDeqValRisp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettQuestIdbsdeq0.Len.DEQ_VAL_RISP));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO DEQ-DS-OPER-SQL
        dettQuest.setDeqDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO DEQ-DS-VER
        dettQuest.setDeqDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DEQ-DS-UTENTE
        dettQuest.setDeqDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO DEQ-DS-STATO-ELAB.
        dettQuest.setDeqDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO DEQ-DS-OPER-SQL
        dettQuest.setDeqDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DEQ-DS-UTENTE.
        dettQuest.setDeqDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF DEQ-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-DEQ-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqIdMoviChiu().getDeqIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-ID-MOVI-CHIU
            ws.getIndDettQuest().setIdOgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-ID-MOVI-CHIU
            ws.getIndDettQuest().setIdOgg(((short)0));
        }
        // COB_CODE: IF DEQ-COD-QUEST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-COD-QUEST
        //           ELSE
        //              MOVE 0 TO IND-DEQ-COD-QUEST
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqCodQuestFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-COD-QUEST
            ws.getIndDettQuest().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-COD-QUEST
            ws.getIndDettQuest().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF DEQ-COD-DOM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-COD-DOM
        //           ELSE
        //              MOVE 0 TO IND-DEQ-COD-DOM
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqCodDomFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-COD-DOM
            ws.getIndDettQuest().setDtIniPer(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-COD-DOM
            ws.getIndDettQuest().setDtIniPer(((short)0));
        }
        // COB_CODE: IF DEQ-COD-DOM-COLLG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-COD-DOM-COLLG
        //           ELSE
        //              MOVE 0 TO IND-DEQ-COD-DOM-COLLG
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqCodDomCollgFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-COD-DOM-COLLG
            ws.getIndDettQuest().setDtEndPer(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-COD-DOM-COLLG
            ws.getIndDettQuest().setDtEndPer(((short)0));
        }
        // COB_CODE: IF DEQ-RISP-NUM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-RISP-NUM
        //           ELSE
        //              MOVE 0 TO IND-DEQ-RISP-NUM
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqRispNum().getDeqRispNumNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-RISP-NUM
            ws.getIndDettQuest().setImpstSost(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-RISP-NUM
            ws.getIndDettQuest().setImpstSost(((short)0));
        }
        // COB_CODE: IF DEQ-RISP-FL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-RISP-FL
        //           ELSE
        //              MOVE 0 TO IND-DEQ-RISP-FL
        //           END-IF
        if (Conditions.eq(dettQuest.getDeqRispFl(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DEQ-RISP-FL
            ws.getIndDettQuest().setImpbIs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-RISP-FL
            ws.getIndDettQuest().setImpbIs(((short)0));
        }
        // COB_CODE: IF DEQ-RISP-TXT = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-RISP-TXT
        //           ELSE
        //              MOVE 0 TO IND-DEQ-RISP-TXT
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqRispTxt(), DettQuestIdbsdeq0.Len.DEQ_RISP_TXT)) {
            // COB_CODE: MOVE -1 TO IND-DEQ-RISP-TXT
            ws.getIndDettQuest().setAlqIs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-RISP-TXT
            ws.getIndDettQuest().setAlqIs(((short)0));
        }
        // COB_CODE: IF DEQ-RISP-TS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-RISP-TS
        //           ELSE
        //              MOVE 0 TO IND-DEQ-RISP-TS
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqRispTs().getDeqRispTsNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-RISP-TS
            ws.getIndDettQuest().setCodTrb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-RISP-TS
            ws.getIndDettQuest().setCodTrb(((short)0));
        }
        // COB_CODE: IF DEQ-RISP-IMP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-RISP-IMP
        //           ELSE
        //              MOVE 0 TO IND-DEQ-RISP-IMP
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqRispImp().getDeqRispImpNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-RISP-IMP
            ws.getIndDettQuest().setPrstzLrdAnteIs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-RISP-IMP
            ws.getIndDettQuest().setPrstzLrdAnteIs(((short)0));
        }
        // COB_CODE: IF DEQ-RISP-PC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-RISP-PC
        //           ELSE
        //              MOVE 0 TO IND-DEQ-RISP-PC
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqRispPc().getDeqRispPcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-RISP-PC
            ws.getIndDettQuest().setRisMatNetPrec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-RISP-PC
            ws.getIndDettQuest().setRisMatNetPrec(((short)0));
        }
        // COB_CODE: IF DEQ-RISP-DT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-RISP-DT
        //           ELSE
        //              MOVE 0 TO IND-DEQ-RISP-DT
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqRispDt().getDeqRispDtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-RISP-DT
            ws.getIndDettQuest().setRisMatAnteTax(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-RISP-DT
            ws.getIndDettQuest().setRisMatAnteTax(((short)0));
        }
        // COB_CODE: IF DEQ-RISP-KEY-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-RISP-KEY
        //           ELSE
        //              MOVE 0 TO IND-DEQ-RISP-KEY
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqRispKeyFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-RISP-KEY
            ws.getIndDettQuest().setRisMatPostTax(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-RISP-KEY
            ws.getIndDettQuest().setRisMatPostTax(((short)0));
        }
        // COB_CODE: IF DEQ-TP-RISP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-TP-RISP
        //           ELSE
        //              MOVE 0 TO IND-DEQ-TP-RISP
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqTpRispFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-TP-RISP
            ws.getIndDettQuest().setPrstzNet(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-TP-RISP
            ws.getIndDettQuest().setPrstzNet(((short)0));
        }
        // COB_CODE: IF DEQ-ID-COMP-QUEST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-ID-COMP-QUEST
        //           ELSE
        //              MOVE 0 TO IND-DEQ-ID-COMP-QUEST
        //           END-IF
        if (Characters.EQ_HIGH.test(dettQuest.getDeqIdCompQuest().getDeqIdCompQuestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DEQ-ID-COMP-QUEST
            ws.getIndDettQuest().setPrstzPrec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-ID-COMP-QUEST
            ws.getIndDettQuest().setPrstzPrec(((short)0));
        }
        // COB_CODE: IF DEQ-VAL-RISP = HIGH-VALUES
        //              MOVE -1 TO IND-DEQ-VAL-RISP
        //           ELSE
        //              MOVE 0 TO IND-DEQ-VAL-RISP
        //           END-IF.
        if (Characters.EQ_HIGH.test(dettQuest.getDeqValRisp(), DettQuestIdbsdeq0.Len.DEQ_VAL_RISP)) {
            // COB_CODE: MOVE -1 TO IND-DEQ-VAL-RISP
            ws.getIndDettQuest().setCumPreVers(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DEQ-VAL-RISP
            ws.getIndDettQuest().setCumPreVers(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : DEQ-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE DETT-QUEST TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dettQuest.getDettQuestFormatted());
        // COB_CODE: MOVE DEQ-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dettQuest.getDeqIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO DEQ-ID-MOVI-CHIU
                dettQuest.getDeqIdMoviChiu().setDeqIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO DEQ-DS-TS-END-CPTZ
                dettQuest.setDeqDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO DEQ-ID-MOVI-CRZ
                    dettQuest.setDeqIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO DEQ-ID-MOVI-CHIU-NULL
                    dettQuest.getDeqIdMoviChiu().setDeqIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqIdMoviChiu.Len.DEQ_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO DEQ-DT-END-EFF
                    dettQuest.setDeqDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO DEQ-DS-TS-INI-CPTZ
                    dettQuest.setDeqDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO DEQ-DS-TS-END-CPTZ
                    dettQuest.setDeqDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE DETT-QUEST TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dettQuest.getDettQuestFormatted());
        // COB_CODE: MOVE DEQ-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dettQuest.getDeqIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO DETT-QUEST.
        dettQuest.setDettQuestFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO DEQ-ID-MOVI-CRZ.
        dettQuest.setDeqIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO DEQ-ID-MOVI-CHIU-NULL.
        dettQuest.getDeqIdMoviChiu().setDeqIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DeqIdMoviChiu.Len.DEQ_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO DEQ-DT-INI-EFF.
        dettQuest.setDeqDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO DEQ-DT-END-EFF.
        dettQuest.setDeqDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO DEQ-DS-TS-INI-CPTZ.
        dettQuest.setDeqDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO DEQ-DS-TS-END-CPTZ.
        dettQuest.setDeqDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO DEQ-COD-COMP-ANIA.
        dettQuest.setDeqCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE DEQ-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dettQuest.getDeqDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DEQ-DT-INI-EFF-DB
        ws.getDettQuestDb().setDtIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE DEQ-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dettQuest.getDeqDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DEQ-DT-END-EFF-DB
        ws.getDettQuestDb().setDtEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-DEQ-RISP-DT = 0
        //               MOVE WS-DATE-X      TO DEQ-RISP-DT-DB
        //           END-IF.
        if (ws.getIndDettQuest().getRisMatAnteTax() == 0) {
            // COB_CODE: MOVE DEQ-RISP-DT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dettQuest.getDeqRispDt().getDeqRispDt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DEQ-RISP-DT-DB
            ws.getDettQuestDb().setRispDtDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE DEQ-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettQuestDb().getDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DEQ-DT-INI-EFF
        dettQuest.setDeqDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE DEQ-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettQuestDb().getDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DEQ-DT-END-EFF
        dettQuest.setDeqDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-DEQ-RISP-DT = 0
        //               MOVE WS-DATE-N      TO DEQ-RISP-DT
        //           END-IF.
        if (ws.getIndDettQuest().getRisMatAnteTax() == 0) {
            // COB_CODE: MOVE DEQ-RISP-DT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettQuestDb().getRispDtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DEQ-RISP-DT
            dettQuest.getDeqRispDt().setDeqRispDt(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF DEQ-RISP-TXT
        //                       TO DEQ-RISP-TXT-LEN
        dettQuest.setDeqRispTxtLen(((short)DettQuestIdbsdeq0.Len.DEQ_RISP_TXT));
        // COB_CODE: MOVE LENGTH OF DEQ-VAL-RISP
        //                       TO DEQ-VAL-RISP-LEN.
        dettQuest.setDeqValRispLen(((short)DettQuestIdbsdeq0.Len.DEQ_VAL_RISP));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return dettQuest.getDeqCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dettQuest.setDeqCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDom() {
        return dettQuest.getDeqCodDom();
    }

    @Override
    public void setCodDom(String codDom) {
        this.dettQuest.setDeqCodDom(codDom);
    }

    @Override
    public String getCodDomCollg() {
        return dettQuest.getDeqCodDomCollg();
    }

    @Override
    public void setCodDomCollg(String codDomCollg) {
        this.dettQuest.setDeqCodDomCollg(codDomCollg);
    }

    @Override
    public String getCodDomCollgObj() {
        if (ws.getIndDettQuest().getDtEndPer() >= 0) {
            return getCodDomCollg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDomCollgObj(String codDomCollgObj) {
        if (codDomCollgObj != null) {
            setCodDomCollg(codDomCollgObj);
            ws.getIndDettQuest().setDtEndPer(((short)0));
        }
        else {
            ws.getIndDettQuest().setDtEndPer(((short)-1));
        }
    }

    @Override
    public String getCodDomObj() {
        if (ws.getIndDettQuest().getDtIniPer() >= 0) {
            return getCodDom();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDomObj(String codDomObj) {
        if (codDomObj != null) {
            setCodDom(codDomObj);
            ws.getIndDettQuest().setDtIniPer(((short)0));
        }
        else {
            ws.getIndDettQuest().setDtIniPer(((short)-1));
        }
    }

    @Override
    public String getCodQuest() {
        return dettQuest.getDeqCodQuest();
    }

    @Override
    public void setCodQuest(String codQuest) {
        this.dettQuest.setDeqCodQuest(codQuest);
    }

    @Override
    public String getCodQuestObj() {
        if (ws.getIndDettQuest().getIdMoviChiu() >= 0) {
            return getCodQuest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodQuestObj(String codQuestObj) {
        if (codQuestObj != null) {
            setCodQuest(codQuestObj);
            ws.getIndDettQuest().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDettQuest().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public long getDeqDsRiga() {
        return dettQuest.getDeqDsRiga();
    }

    @Override
    public void setDeqDsRiga(long deqDsRiga) {
        this.dettQuest.setDeqDsRiga(deqDsRiga);
    }

    @Override
    public char getDsOperSql() {
        return dettQuest.getDeqDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dettQuest.setDeqDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return dettQuest.getDeqDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dettQuest.setDeqDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return dettQuest.getDeqDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dettQuest.setDeqDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return dettQuest.getDeqDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dettQuest.setDeqDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return dettQuest.getDeqDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dettQuest.setDeqDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dettQuest.getDeqDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dettQuest.setDeqDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getDettQuestDb().getDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getDettQuestDb().setDtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getDettQuestDb().getDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getDettQuestDb().setDtIniEffDb(dtIniEffDb);
    }

    @Override
    public int getIdCompQuest() {
        return dettQuest.getDeqIdCompQuest().getDeqIdCompQuest();
    }

    @Override
    public void setIdCompQuest(int idCompQuest) {
        this.dettQuest.getDeqIdCompQuest().setDeqIdCompQuest(idCompQuest);
    }

    @Override
    public Integer getIdCompQuestObj() {
        if (ws.getIndDettQuest().getPrstzPrec() >= 0) {
            return ((Integer)getIdCompQuest());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdCompQuestObj(Integer idCompQuestObj) {
        if (idCompQuestObj != null) {
            setIdCompQuest(((int)idCompQuestObj));
            ws.getIndDettQuest().setPrstzPrec(((short)0));
        }
        else {
            ws.getIndDettQuest().setPrstzPrec(((short)-1));
        }
    }

    @Override
    public int getIdDettQuest() {
        return dettQuest.getDeqIdDettQuest();
    }

    @Override
    public void setIdDettQuest(int idDettQuest) {
        this.dettQuest.setDeqIdDettQuest(idDettQuest);
    }

    @Override
    public int getIdMoviChiu() {
        return dettQuest.getDeqIdMoviChiu().getDeqIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.dettQuest.getDeqIdMoviChiu().setDeqIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndDettQuest().getIdOgg() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndDettQuest().setIdOgg(((short)0));
        }
        else {
            ws.getIndDettQuest().setIdOgg(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return dettQuest.getDeqIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.dettQuest.setDeqIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdQuest() {
        return dettQuest.getDeqIdQuest();
    }

    @Override
    public void setIdQuest(int idQuest) {
        this.dettQuest.setDeqIdQuest(idQuest);
    }

    @Override
    public String getRispDtDb() {
        return ws.getDettQuestDb().getRispDtDb();
    }

    @Override
    public void setRispDtDb(String rispDtDb) {
        this.ws.getDettQuestDb().setRispDtDb(rispDtDb);
    }

    @Override
    public String getRispDtDbObj() {
        if (ws.getIndDettQuest().getRisMatAnteTax() >= 0) {
            return getRispDtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispDtDbObj(String rispDtDbObj) {
        if (rispDtDbObj != null) {
            setRispDtDb(rispDtDbObj);
            ws.getIndDettQuest().setRisMatAnteTax(((short)0));
        }
        else {
            ws.getIndDettQuest().setRisMatAnteTax(((short)-1));
        }
    }

    @Override
    public char getRispFl() {
        return dettQuest.getDeqRispFl();
    }

    @Override
    public void setRispFl(char rispFl) {
        this.dettQuest.setDeqRispFl(rispFl);
    }

    @Override
    public Character getRispFlObj() {
        if (ws.getIndDettQuest().getImpbIs() >= 0) {
            return ((Character)getRispFl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispFlObj(Character rispFlObj) {
        if (rispFlObj != null) {
            setRispFl(((char)rispFlObj));
            ws.getIndDettQuest().setImpbIs(((short)0));
        }
        else {
            ws.getIndDettQuest().setImpbIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getRispImp() {
        return dettQuest.getDeqRispImp().getDeqRispImp();
    }

    @Override
    public void setRispImp(AfDecimal rispImp) {
        this.dettQuest.getDeqRispImp().setDeqRispImp(rispImp.copy());
    }

    @Override
    public AfDecimal getRispImpObj() {
        if (ws.getIndDettQuest().getPrstzLrdAnteIs() >= 0) {
            return getRispImp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispImpObj(AfDecimal rispImpObj) {
        if (rispImpObj != null) {
            setRispImp(new AfDecimal(rispImpObj, 15, 3));
            ws.getIndDettQuest().setPrstzLrdAnteIs(((short)0));
        }
        else {
            ws.getIndDettQuest().setPrstzLrdAnteIs(((short)-1));
        }
    }

    @Override
    public String getRispKey() {
        return dettQuest.getDeqRispKey();
    }

    @Override
    public void setRispKey(String rispKey) {
        this.dettQuest.setDeqRispKey(rispKey);
    }

    @Override
    public String getRispKeyObj() {
        if (ws.getIndDettQuest().getRisMatPostTax() >= 0) {
            return getRispKey();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispKeyObj(String rispKeyObj) {
        if (rispKeyObj != null) {
            setRispKey(rispKeyObj);
            ws.getIndDettQuest().setRisMatPostTax(((short)0));
        }
        else {
            ws.getIndDettQuest().setRisMatPostTax(((short)-1));
        }
    }

    @Override
    public int getRispNum() {
        return dettQuest.getDeqRispNum().getDeqRispNum();
    }

    @Override
    public void setRispNum(int rispNum) {
        this.dettQuest.getDeqRispNum().setDeqRispNum(rispNum);
    }

    @Override
    public Integer getRispNumObj() {
        if (ws.getIndDettQuest().getImpstSost() >= 0) {
            return ((Integer)getRispNum());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispNumObj(Integer rispNumObj) {
        if (rispNumObj != null) {
            setRispNum(((int)rispNumObj));
            ws.getIndDettQuest().setImpstSost(((short)0));
        }
        else {
            ws.getIndDettQuest().setImpstSost(((short)-1));
        }
    }

    @Override
    public AfDecimal getRispPc() {
        return dettQuest.getDeqRispPc().getDeqRispPc();
    }

    @Override
    public void setRispPc(AfDecimal rispPc) {
        this.dettQuest.getDeqRispPc().setDeqRispPc(rispPc.copy());
    }

    @Override
    public AfDecimal getRispPcObj() {
        if (ws.getIndDettQuest().getRisMatNetPrec() >= 0) {
            return getRispPc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispPcObj(AfDecimal rispPcObj) {
        if (rispPcObj != null) {
            setRispPc(new AfDecimal(rispPcObj, 6, 3));
            ws.getIndDettQuest().setRisMatNetPrec(((short)0));
        }
        else {
            ws.getIndDettQuest().setRisMatNetPrec(((short)-1));
        }
    }

    @Override
    public AfDecimal getRispTs() {
        return dettQuest.getDeqRispTs().getDeqRispTs();
    }

    @Override
    public void setRispTs(AfDecimal rispTs) {
        this.dettQuest.getDeqRispTs().setDeqRispTs(rispTs.copy());
    }

    @Override
    public AfDecimal getRispTsObj() {
        if (ws.getIndDettQuest().getCodTrb() >= 0) {
            return getRispTs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispTsObj(AfDecimal rispTsObj) {
        if (rispTsObj != null) {
            setRispTs(new AfDecimal(rispTsObj, 14, 9));
            ws.getIndDettQuest().setCodTrb(((short)0));
        }
        else {
            ws.getIndDettQuest().setCodTrb(((short)-1));
        }
    }

    @Override
    public String getRispTxtVchar() {
        return dettQuest.getDeqRispTxtVcharFormatted();
    }

    @Override
    public void setRispTxtVchar(String rispTxtVchar) {
        this.dettQuest.setDeqRispTxtVcharFormatted(rispTxtVchar);
    }

    @Override
    public String getRispTxtVcharObj() {
        if (ws.getIndDettQuest().getAlqIs() >= 0) {
            return getRispTxtVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRispTxtVcharObj(String rispTxtVcharObj) {
        if (rispTxtVcharObj != null) {
            setRispTxtVchar(rispTxtVcharObj);
            ws.getIndDettQuest().setAlqIs(((short)0));
        }
        else {
            ws.getIndDettQuest().setAlqIs(((short)-1));
        }
    }

    @Override
    public String getTpRisp() {
        return dettQuest.getDeqTpRisp();
    }

    @Override
    public void setTpRisp(String tpRisp) {
        this.dettQuest.setDeqTpRisp(tpRisp);
    }

    @Override
    public String getTpRispObj() {
        if (ws.getIndDettQuest().getPrstzNet() >= 0) {
            return getTpRisp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRispObj(String tpRispObj) {
        if (tpRispObj != null) {
            setTpRisp(tpRispObj);
            ws.getIndDettQuest().setPrstzNet(((short)0));
        }
        else {
            ws.getIndDettQuest().setPrstzNet(((short)-1));
        }
    }

    @Override
    public String getValRispVchar() {
        return dettQuest.getDeqValRispVcharFormatted();
    }

    @Override
    public void setValRispVchar(String valRispVchar) {
        this.dettQuest.setDeqValRispVcharFormatted(valRispVchar);
    }

    @Override
    public String getValRispVcharObj() {
        if (ws.getIndDettQuest().getCumPreVers() >= 0) {
            return getValRispVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValRispVcharObj(String valRispVcharObj) {
        if (valRispVcharObj != null) {
            setValRispVchar(valRispVcharObj);
            ws.getIndDettQuest().setCumPreVers(((short)0));
        }
        else {
            ws.getIndDettQuest().setCumPreVers(((short)-1));
        }
    }
}
