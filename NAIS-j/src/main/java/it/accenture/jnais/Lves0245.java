package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.WkFrazMm;
import it.accenture.jnais.ws.enums.WsTpMezPag;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Lves0245Data;
import it.accenture.jnais.ws.WadeAreaAdesioneLccs0005;
import it.accenture.jnais.ws.WpmoAreaParamMovi;
import it.accenture.jnais.ws.WpogAreaParamOggLves0245;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WranAreaRappAnag;
import it.accenture.jnais.ws.WsVariabiliLves0245;

/**Original name: LVES0245<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA VER. 1.0                          **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2009.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     PROGRAMMA ..... LVES0245
 *     TIPOLOGIA......
 *     PROCESSO....... Emissione polizza Individuale
 *     FUNZIONE....... XXX
 *     DESCRIZIONE.... Gestione di Business della parametro
 *                     oggetto
 *     PAGINA WEB.....
 * **------------------------------------------------------------***</pre>*/
public class Lves0245 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lves0245Data ws = new Lves0245Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLccs0005 wadeAreaAdesione;
    //Original name: WPMO-AREA-PARAM-MOV
    private WpmoAreaParamMovi wpmoAreaParamMov;
    //Original name: WPOG-AREA-PARAM-OGG
    private WpogAreaParamOggLves0245 wpogAreaParamOgg;
    //Original name: WRAN-AREA-RAPP-ANAG
    private WranAreaRappAnag wranAreaRappAnag;

    //==== METHODS ====
    /**Original name: PROGRAM_LVES0245_FIRST_SENTENCES<br>
	 * <pre>---------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLccs0005 wadeAreaAdesione, WpmoAreaParamMovi wpmoAreaParamMov, WpogAreaParamOggLves0245 wpogAreaParamOgg, WranAreaRappAnag wranAreaRappAnag) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wpmoAreaParamMov = wpmoAreaParamMov;
        this.wpogAreaParamOgg = wpogAreaParamOgg;
        this.wranAreaRappAnag = wranAreaRappAnag;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lves0245 getInstance() {
        return ((Lves0245)Programs.getInstance(Lves0245.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO     TO WS-MOVIMENTO.
        ws.getWsMovimento().setWsMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA TO WS-COMPAGNIA.
        ws.getWsCompagnia().setWsCompagniaFormatted(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted());
        // COB_CODE: INITIALIZE  WS-VARIABILI
        //                       IX-INDICI.
        initWsVariabili();
        initIxIndici();
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: SET QUIETANZAMENTO-NO  TO TRUE
        ws.setWkQuietanzamento(false);
        // COB_CODE: SET MOVIM-QUIETA       TO TRUE
        ws.getWsMovimento().setMovimQuieta();
        // COB_CODE: PERFORM RIC-TAB-PMO
        //              THRU RIC-TAB-PMO-EX
        ricTabPmo();
        // COB_CODE: IF TROVATO
        //              SET QUIETANZAMENTO-SI   TO TRUE
        //           ELSE
        //              END-IF
        //           END-IF
        if (ws.getFlRicerca().isTrovato()) {
            // COB_CODE: SET QUIETANZAMENTO-SI   TO TRUE
            ws.setWkQuietanzamento(true);
        }
        else {
            // COB_CODE: SET GETRA-NO      TO TRUE
            ws.setWkGetra(false);
            // COB_CODE: SET GENER-TRANCH  TO TRUE
            ws.getWsMovimento().setGenerTranch();
            // COB_CODE: PERFORM RIC-TAB-PMO
            //              THRU RIC-TAB-PMO-EX
            ricTabPmo();
            // COB_CODE: IF TROVATO
            //              SET GETRA-SI TO TRUE
            //           END-IF
            if (ws.getFlRicerca().isTrovato()) {
                // COB_CODE: SET GETRA-SI TO TRUE
                ws.setWkGetra(true);
            }
        }
        //--  Calcola il frazionamento / periodicita di polizza
        // COB_CODE: IF QUIETANZAMENTO-SI OR GETRA-SI
        //           END-IF.
        if (ws.isWkQuietanzamento() || ws.isWkGetra()) {
            // COB_CODE:    IF WPMO-FRQ-MOVI-NULL(IX-RIC-PMO) = HIGH-VALUE
            //                 MOVE ZERO  TO WK-FRQ-MOVI
            //              ELSE
            //                 COMPUTE WK-FRAZ-MM = 12 / WK-FRQ-MOVI
            //           END-IF
            if (Characters.EQ_HIGH.test(wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getRicPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMoviNullFormatted())) {
                // COB_CODE: MOVE ZERO  TO WK-FRQ-MOVI
                ws.getWsVariabili().setFrqMovi(0);
            }
            else {
                // COB_CODE: MOVE WPMO-FRQ-MOVI(IX-RIC-PMO) TO WK-FRQ-MOVI
                ws.getWsVariabili().setFrqMovi(TruncAbs.toInt(wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getRicPmo()).getLccvpmo1().getDati().getWpmoFrqMovi().getWpmoFrqMovi(), 5));
                // COB_CODE: COMPUTE WK-FRAZ-MM = 12 / WK-FRQ-MOVI
                ws.getWsVariabili().getFrazMm().setFrazMm((new AfDecimal(((((double)12)) / ws.getWsVariabili().getFrqMovi()), 2, 0)).toInt());
            }
            //--  Ricerca del parametro rateantic
            // COB_CODE: SET WK-PR-RATEANTIC TO TRUE
            ws.getWsVariabili().setPrRateantic();
            // COB_CODE: PERFORM RIC-TAB-POG
            //              THRU RIC-TAB-POG-EX
            ricTabPog();
            // COB_CODE: IF TROVATO
            //              END-EVALUATE
            //           END-IF.
            if (ws.getFlRicerca().isTrovato()) {
                // COB_CODE: MOVE WPOG-VAL-NUM(IX-RIC-POG) TO WK-RATE-ANTIC
                ws.getWsVariabili().setRateAntic(wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getRicPog()).getLccvpog1().getDati().getWpogValNum().getWpogValNum());
                //--         annuale
                // COB_CODE:         EVALUATE TRUE
                //           *--         annuale
                //                       WHEN WK-FRAZ-ANN
                //                             THRU RATEANTIC-ZERO-EX
                //           *--         mensile
                //                       WHEN WK-FRAZ-MEN
                //                             THRU GEST-FRAZ-MEN-EX
                //           *--         bimestrale
                //                       WHEN WK-FRAZ-BIM
                //                             THRU RATEANTIC-ZERO-EX
                //           *--         Trimestrale
                //                       WHEN WK-FRAZ-TRI
                //                             THRU RATEANTIC-ZERO-EX
                //           *--         Quadrimestrale
                //                       WHEN WK-FRAZ-QUA
                //                             THRU RATEANTIC-ZERO-EX
                //           *--         Semestrale
                //                       WHEN WK-FRAZ-SEM
                //                             THRU RATEANTIC-ZERO-EX
                //                   END-EVALUATE
                switch (ws.getWsVariabili().getFrazMm().getFrazMmFormatted()) {

                    case WkFrazMm.ANN:// COB_CODE: PERFORM RATEANTIC-ZERO
                        //              THRU RATEANTIC-ZERO-EX
                        rateanticZero();
                        //--         mensile
                        break;

                    case WkFrazMm.MEN:// COB_CODE: PERFORM GEST-FRAZ-MEN
                        //              THRU GEST-FRAZ-MEN-EX
                        gestFrazMen();
                        //--         bimestrale
                        break;

                    case WkFrazMm.BIM:// COB_CODE: PERFORM RATEANTIC-ZERO
                        //              THRU RATEANTIC-ZERO-EX
                        rateanticZero();
                        //--         Trimestrale
                        break;

                    case WkFrazMm.TRI:// COB_CODE: PERFORM RATEANTIC-ZERO
                        //              THRU RATEANTIC-ZERO-EX
                        rateanticZero();
                        //--         Quadrimestrale
                        break;

                    case WkFrazMm.QUA:// COB_CODE: PERFORM RATEANTIC-ZERO
                        //              THRU RATEANTIC-ZERO-EX
                        rateanticZero();
                        //--         Semestrale
                        break;

                    case WkFrazMm.SEM:// COB_CODE: PERFORM RATEANTIC-ZERO
                        //              THRU RATEANTIC-ZERO-EX
                        rateanticZero();
                        break;

                    default:break;
                }
            }
        }
    }

    /**Original name: GEST-FRAZ-MEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Gestione Rate anticipate per frazionamento mensile
	 * ----------------------------------------------------------------*
	 * --  Ricerca codice soggetto del contraente della polizza emessa</pre>*/
    private void gestFrazMen() {
        // COB_CODE: SET CONTRAENTE       TO TRUE
        ws.getWsTpRappAna().setContraente();
        // COB_CODE: PERFORM RIC-TAB-RAN
        //              THRU RIC-TAB-RAN-EX
        ricTabRan();
        // COB_CODE: IF TROVATO
        //              END-EVALUATE
        //           END-IF.
        if (ws.getFlRicerca().isTrovato()) {
            // COB_CODE: MOVE WRAN-TP-MEZ-PAG-ADD(IX-RIC-RAN) TO WS-TP-MEZ-PAG
            ws.getWsTpMezPag().setWsTpMezPag(wranAreaRappAnag.getTabRappAnag(ws.getIxIndici().getRicRan()).getLccvran1().getDati().getWranTpMezPagAdd());
            //--         Pagamento RID
            // COB_CODE:         EVALUATE TRUE
            //           *--         Pagamento RID
            //                       WHEN MEZ-PAG-RID
            //                          END-IF
            //           *--         Pagamento Trattenuta da stipendio
            //                       WHEN MEZ-PAG-TRAT-STIP
            //                             THRU RATEANTIC-TRAT-STIP-EX
            //                   END-EVALUATE
            switch (ws.getWsTpMezPag().getWsTpMezPag()) {

                case WsTpMezPag.RID:// COB_CODE: IF WK-RATE-ANTIC < 2
                    //                 THRU RATEANTIC-DUE-EX
                    //           END-IF
                    if (ws.getWsVariabili().getRateAntic() < 2) {
                        // COB_CODE: PERFORM RATEANTIC-DUE
                        //              THRU RATEANTIC-DUE-EX
                        rateanticDue();
                    }
                    //--         Pagamento Trattenuta da stipendio
                    break;

                case WsTpMezPag.TRAT_STIP:// COB_CODE: PERFORM RATEANTIC-TRAT-STIP
                    //              THRU RATEANTIC-TRAT-STIP-EX
                    rateanticTratStip();
                    break;

                default:break;
            }
        }
    }

    /**Original name: RATEANTIC-ZERO<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE A ZERO DI RATEANTIC
	 * ----------------------------------------------------------------*</pre>*/
    private void rateanticZero() {
        // COB_CODE: MOVE ZERO TO WPOG-VAL-NUM(IX-RIC-POG).
        wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getRicPog()).getLccvpog1().getDati().getWpogValNum().setWpogValNum(0);
    }

    /**Original name: RATEANTIC-DUE<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE A DUE DI RATEANTIC
	 * ----------------------------------------------------------------*</pre>*/
    private void rateanticDue() {
        // COB_CODE: MOVE 2 TO WPOG-VAL-NUM(IX-RIC-POG).
        wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getRicPog()).getLccvpog1().getDati().getWpogValNum().setWpogValNum(2);
    }

    /**Original name: RIC-TAB-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *     Ricerca della tabella parametro movimento
	 *     Chiave di ricerca : Tipo Movimento
	 * ----------------------------------------------------------------*</pre>*/
    private void ricTabPmo() {
        // COB_CODE: SET NON-TROVATO TO TRUE
        ws.getFlRicerca().setNonTrovato();
        // COB_CODE: MOVE ZERO          TO IX-RIC-PMO
        ws.getIxIndici().setRicPmo(((short)0));
        // COB_CODE: PERFORM UNTIL IX-RIC-PMO >= WPMO-ELE-PARAM-MOV-MAX
        //                      OR TROVATO
        //              END-IF
        //           END-PERFORM.
        while (!(ws.getIxIndici().getRicPmo() >= wpmoAreaParamMov.getEleParamMovMax() || ws.getFlRicerca().isTrovato())) {
            // COB_CODE: ADD 1 TO IX-RIC-PMO
            ws.getIxIndici().setRicPmo(Trunc.toShort(1 + ws.getIxIndici().getRicPmo(), 4));
            // COB_CODE: IF WPMO-TP-MOVI(IX-RIC-PMO) = WS-MOVIMENTO
            //              SET TROVATO TO TRUE
            //           END-IF
            if (wpmoAreaParamMov.getTabParamMov(ws.getIxIndici().getRicPmo()).getLccvpmo1().getDati().getWpmoTpMovi().getWpmoTpMovi() == ws.getWsMovimento().getWsMovimento()) {
                // COB_CODE: SET TROVATO TO TRUE
                ws.getFlRicerca().setTrovato();
            }
        }
    }

    /**Original name: RIC-TAB-POG<br>
	 * <pre>----------------------------------------------------------------*
	 *     Ricerca della tabella parametro oggetto
	 *     Chiave di ricerca : Codice parametro
	 * ----------------------------------------------------------------*</pre>*/
    private void ricTabPog() {
        // COB_CODE: SET NON-TROVATO TO TRUE
        ws.getFlRicerca().setNonTrovato();
        // COB_CODE: MOVE ZERO TO IX-RIC-POG
        ws.getIxIndici().setRicPog(((short)0));
        // COB_CODE: PERFORM UNTIL IX-RIC-POG >= WPOG-ELE-PARAM-OGG-MAX
        //                      OR TROVATO
        //              END-IF
        //           END-PERFORM.
        while (!(ws.getIxIndici().getRicPog() >= wpogAreaParamOgg.getEleParamOggMax() || ws.getFlRicerca().isTrovato())) {
            // COB_CODE: ADD 1 TO IX-RIC-POG
            ws.getIxIndici().setRicPog(Trunc.toShort(1 + ws.getIxIndici().getRicPog(), 4));
            // COB_CODE: IF WPOG-COD-PARAM(IX-RIC-POG) = WK-PARAM-OGG
            //              SET TROVATO TO TRUE
            //           END-IF
            if (Conditions.eq(wpogAreaParamOgg.getTabParamOgg(ws.getIxIndici().getRicPog()).getLccvpog1().getDati().getWpogCodParam(), ws.getWsVariabili().getParamOgg())) {
                // COB_CODE: SET TROVATO TO TRUE
                ws.getFlRicerca().setTrovato();
            }
        }
    }

    /**Original name: RIC-TAB-RAN<br>
	 * <pre>----------------------------------------------------------------*
	 *     Ricerca della tabella Rapporto Anagrafico
	 *     Tipologia Ricerca
	 *     Chiave di ricerca
	 *                     - Tipo rapporto anagrafico
	 * ----------------------------------------------------------------*</pre>*/
    private void ricTabRan() {
        // COB_CODE: SET NON-TROVATO TO TRUE
        ws.getFlRicerca().setNonTrovato();
        // COB_CODE: MOVE ZERO          TO IX-RIC-RAN
        ws.getIxIndici().setRicRan(((short)0));
        // COB_CODE: PERFORM UNTIL IX-RIC-RAN >= WRAN-ELE-RAPP-ANAG-MAX
        //                      OR TROVATO
        //              END-IF
        //           END-PERFORM.
        while (!(ws.getIxIndici().getRicRan() >= wranAreaRappAnag.getEleRappAnagMax() || ws.getFlRicerca().isTrovato())) {
            // COB_CODE: ADD 1 TO IX-RIC-RAN
            ws.getIxIndici().setRicRan(Trunc.toShort(1 + ws.getIxIndici().getRicRan(), 4));
            // COB_CODE: IF WRAN-TP-RAPP-ANA(IX-RIC-RAN) = WS-TP-RAPP-ANA
            //              SET TROVATO TO TRUE
            //           END-IF
            if (Conditions.eq(wranAreaRappAnag.getTabRappAnag(ws.getIxIndici().getRicRan()).getLccvran1().getDati().getWranTpRappAna(), ws.getWsTpRappAna().getWsTpRappAna())) {
                // COB_CODE: SET TROVATO TO TRUE
                ws.getFlRicerca().setTrovato();
            }
        }
    }

    /**Original name: RATEANTIC-TRAT-STIP<br>
	 * <pre>----------------------------------------------------------------*
	 *     RATEANTIC PER LA TRATTENUTA DA STIPENDIO
	 * ----------------------------------------------------------------*
	 * ----------------------------------------------------------------*
	 *     RATEANTIC PER LA TRATTENUTA DA STIPENDIO
	 * ----------------------------------------------------------------*</pre>*/
    private void rateanticTratStip() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initWsVariabili() {
        ws.getWsVariabili().setFrqMoviFormatted("00000");
        ws.getWsVariabili().getFrazMm().setFrazMmFormatted("00000");
        ws.getWsVariabili().setRateAntic(0);
        ws.getWsVariabili().setParamOgg("");
    }

    public void initIxIndici() {
        ws.getIxIndici().setTabPmo(((short)0));
        ws.getIxIndici().setTabPog(((short)0));
        ws.getIxIndici().setRicPmo(((short)0));
        ws.getIxIndici().setRicPog(((short)0));
        ws.getIxIndici().setRicRan(((short)0));
    }
}
