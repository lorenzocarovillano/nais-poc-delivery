package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.A2kInput;
import it.accenture.jnais.copy.A2kOugbmba;
import it.accenture.jnais.copy.A2kOurid;
import it.accenture.jnais.copy.A2kOusta;
import it.accenture.jnais.copy.A2kOutput;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.A2kOuamgX;
import it.accenture.jnais.ws.A2kOugl07X;
import it.accenture.jnais.ws.A2kOugmaX;
import it.accenture.jnais.ws.A2kOujulX;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0127Data;
import static java.lang.Math.abs;

/**Original name: LVVS0127<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2009.
 * DATE-COMPILED.
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0127 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0127Data ws = new Lvvs0127Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0127
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0127_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000.
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0127 getInstance() {
        return ((Lvvs0127)Programs.getInstance(Lvvs0127.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
        // COB_CODE: PERFORM S0001-VALORIZZA-DCLGEN
        //              THRU S0001-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s0001ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
    }

    /**Original name: S0001-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s0001ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO WTGA-AREA-TRANCHE
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO WTGA-AREA-TRANCHE
            ws.setWtgaAreaTrancheFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: IF WTGA-DT-ULT-ADEG-PRE-PR-NULL(IVVC0213-IX-TABB)
        //              = HIGH-VALUES
        //                      THRU EX-S1100
        //           ELSE
        //              END-IF
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePrNullFormatted())) {
            // COB_CODE: PERFORM S1100-LETTURA-POG
            //                   THRU EX-S1100
            s1100LetturaPog();
        }
        else if (Functions.isNumber(ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePr())) {
            // COB_CODE: IF WTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB) IS NUMERIC
            //             END-IF
            //           END-IF
            // COB_CODE: IF WTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB) = ZERO
            //                 THRU EX-S1100
            //           ELSE
            //              PERFORM S1120-CALCOLO-DATA THRU EX-S1120
            //           END-IF
            if (ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePr() == 0) {
                // COB_CODE: PERFORM S1100-LETTURA-POG
                //              THRU EX-S1100
                s1100LetturaPog();
            }
            else {
                // COB_CODE: MOVE WTGA-DT-ULT-ADEG-PRE-PR(IVVC0213-IX-TABB)
                //             TO WK-DATA-9
                ws.setWkData9(TruncAbs.toInt(ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().getWtgaDtUltAdegPrePr(), 8));
                // COB_CODE: PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                s1120CalcoloData();
            }
        }
    }

    /**Original name: S1100-LETTURA-POG<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA PARAMETRO OGGETTO PER ID-OGG, TP-OGG E COD-PARAMETRO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100LetturaPog() {
        Ldbs1130 ldbs1130 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE AREA-LDBV1131.
        initAreaLdbv1131();
        // COB_CODE: MOVE IVVC0213-ID-POLIZZA       TO LDBV1131-ID-OGG.
        ws.getAreaLdbv1131().setIdOgg(ivvc0213.getIdPolizza());
        // COB_CODE: MOVE 'PO'                      TO LDBV1131-TP-OGG.
        ws.getAreaLdbv1131().setTpOgg("PO");
        // COB_CODE: MOVE 'DATARIVALFX$'            TO LDBV1131-COD-PARAM.
        ws.getAreaLdbv1131().setCodParam("DATARIVALFX$");
        // COB_CODE: MOVE AREA-LDBV1131             TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond(ws.getAreaLdbv1131().getAreaLdbv1131Formatted());
        // COB_CODE: MOVE 'LDBS1130'                TO WK-CALL-PGM.
        ws.setWkCallPgm("LDBS1130");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        idsv0003.getOperazione().setSelect();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-WHERE-CONDITION   TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: CALL WK-CALL-PGM USING IDSV0003 PARAM-OGG
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER   TO TRUE
        //           END-CALL.
        try {
            ldbs1130 = Ldbs1130.getInstance();
            ldbs1130.run(idsv0003, ws.getParamOgg());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LDBS1130 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS1130 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF POG-VAL-TXT > '0001'
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF
            if (Conditions.gt(ws.getParamOgg().getPogValTxt(), "0001")) {
                // COB_CODE: MOVE WTGA-DT-DECOR(IVVC0213-IX-TABB) TO WK-DATA-X
                ws.setWkDataX(ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecorFormatted());
                // COB_CODE: MOVE WK-DATA-X(1:4)           TO WK-AAAA-TGA
                ws.getWkDataAmgTga().setAaaaTgaFormatted(ws.getWkDataXFormatted().substring((1) - 1, 4));
                // COB_CODE: MOVE POG-VAL-TXT(1:2)         TO WK-GG-TGA
                ws.getWkDataAmgTga().setGgTgaFormatted(ws.getParamOgg().getPogValTxtFormatted().substring((1) - 1, 2));
                // COB_CODE: MOVE POG-VAL-TXT(3:2)         TO WK-MM-TGA
                ws.getWkDataAmgTga().setMmTgaFormatted(ws.getParamOgg().getPogValTxtFormatted().substring((3) - 1, 4));
                // COB_CODE: MOVE WK-DATA-AMG-TGA-R     TO WK-DT-CALC
                ws.setWkDtCalcFormatted(ws.getWkDataAmgTga().getWkDataAmgTgaRFormatted());
                // COB_CODE: MOVE WK-DT-CALC            TO WK-APPO-DT
                ws.getWkAppoDt().setWkAppoDtFormatted(ws.getWkDtCalcFormatted());
                // COB_CODE: PERFORM S10000-CTRL-FINE-MESE
                //              THRU S10000-CTRL-FINE-MESE-EX
                s10000CtrlFineMese();
                // COB_CODE: MOVE WK-DT-CALC   TO WK-DATA-AMG-TGA-R
                ws.getWkDataAmgTga().setWkDataAmgTgaRFormatted(ws.getWkDtCalcFormatted());
                // COB_CODE: IF WK-DATA-AMG-TGA-R > WTGA-DT-DECOR(IVVC0213-IX-TABB)
                //              MOVE WK-DATA-AMG-TGA-R     TO IVVC0213-VAL-STR-O
                //           ELSE
                //              PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                //           END-IF
                if (ws.getWkDataAmgTga().getWkDataAmgTgaR() > ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecor()) {
                    // COB_CODE: MOVE WK-DATA-AMG-TGA-R     TO IVVC0213-VAL-STR-O
                    ivvc0213.getTabOutput().setValStrO(ws.getWkDataAmgTga().getWkDataAmgTgaRFormatted());
                }
                else {
                    // COB_CODE: MOVE WK-DATA-AMG-TGA-R     TO WK-DATA-9
                    ws.setWkData9Formatted(ws.getWkDataAmgTga().getWkDataAmgTgaRFormatted());
                    // COB_CODE: PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                    s1120CalcoloData();
                }
            }
            else if (Conditions.eq(ws.getParamOgg().getPogValTxt(), "0000")) {
                // COB_CODE: IF POG-VAL-TXT = '0000'
                //              PERFORM S1120-CALCOLO-DATA           THRU EX-S1120
                //           ELSE
                //              END-IF
                //           END-IF
                // COB_CODE: MOVE WTGA-DT-DECOR(IVVC0213-IX-TABB) TO WK-DATA-X
                ws.setWkDataX(ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecorFormatted());
                // COB_CODE: MOVE WK-DATA-X(1:4)                  TO WK-AAAA-TGA
                ws.getWkDataAmgTga().setAaaaTgaFormatted(ws.getWkDataXFormatted().substring((1) - 1, 4));
                // COB_CODE: MOVE WK-DATA-X(5:2)                  TO WK-MM-TGA
                ws.getWkDataAmgTga().setMmTgaFormatted(ws.getWkDataXFormatted().substring((5) - 1, 6));
                // COB_CODE: MOVE WK-DATA-X(7:2)                  TO WK-GG-TGA
                ws.getWkDataAmgTga().setGgTgaFormatted(ws.getWkDataXFormatted().substring((7) - 1, 8));
                // COB_CODE: MOVE WK-DATA-AMG-TGA-R               TO WK-DATA-9
                ws.setWkData9Formatted(ws.getWkDataAmgTga().getWkDataAmgTgaRFormatted());
                // COB_CODE: PERFORM S1120-CALCOLO-DATA           THRU EX-S1120
                s1120CalcoloData();
            }
            else if (Conditions.eq(ws.getParamOgg().getPogValTxt(), "0001")) {
                // COB_CODE: IF POG-VAL-TXT = '0001'
                //              END-IF
                //           END-IF
                // COB_CODE: PERFORM S1130-LETTURA-ADESIONE
                //              THRU EX-S1130
                s1130LetturaAdesione();
                // COB_CODE: IF IDSV0003-INVALID-OPER
                //              CONTINUE
                //           ELSE
                //              END-IF
                //           END-IF
                if (idsv0003.getReturnCode().isIdsv0003InvalidOper()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: MOVE WTGA-DT-DECOR(IVVC0213-IX-TABB)
                    //             TO WK-DATA-X
                    ws.setWkDataX(ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecorFormatted());
                    // COB_CODE: MOVE WK-DATA-X(1:4)         TO WK-AAAA-TGA
                    ws.getWkDataAmgTga().setAaaaTgaFormatted(ws.getWkDataXFormatted().substring((1) - 1, 4));
                    // COB_CODE: MOVE WK-DATA-X(5:2)         TO WK-MM-TGA
                    ws.getWkDataAmgTga().setMmTgaFormatted(ws.getWkDataXFormatted().substring((5) - 1, 6));
                    // COB_CODE: MOVE WK-DATA-X(7:2)         TO WK-GG-TGA
                    ws.getWkDataAmgTga().setGgTgaFormatted(ws.getWkDataXFormatted().substring((7) - 1, 8));
                    // COB_CODE: MOVE ADE-DT-DECOR           TO WK-DATA-X
                    ws.setWkDataX(ws.getAdes().getAdeDtDecor().getAdeDtDecorFormatted());
                    // COB_CODE: MOVE WK-DATA-X(1:4)         TO WK-AAAA-POL
                    ws.getWkDataAmgPol().setAaaaPolFormatted(ws.getWkDataXFormatted().substring((1) - 1, 4));
                    // COB_CODE: MOVE WK-DATA-X(5:2)         TO WK-MM-POL
                    ws.getWkDataAmgPol().setMmPolFormatted(ws.getWkDataXFormatted().substring((5) - 1, 6));
                    // COB_CODE: MOVE WK-DATA-X(7:2)         TO WK-GG-POL
                    ws.getWkDataAmgPol().setGgPolFormatted(ws.getWkDataXFormatted().substring((7) - 1, 8));
                    // COB_CODE: MOVE WK-AAAA-TGA            TO WK-AAAA-POL
                    ws.getWkDataAmgPol().setAaaaPolFormatted(ws.getWkDataAmgTga().getAaaaTgaFormatted());
                    // COB_CODE: MOVE WK-DATA-AMG-POL-R     TO WK-DT-CALC
                    ws.setWkDtCalcFormatted(ws.getWkDataAmgPol().getWkDataAmgPolRFormatted());
                    // COB_CODE: MOVE WK-DT-CALC            TO WK-APPO-DT
                    ws.getWkAppoDt().setWkAppoDtFormatted(ws.getWkDtCalcFormatted());
                    // COB_CODE: PERFORM S10000-CTRL-FINE-MESE
                    //              THRU S10000-CTRL-FINE-MESE-EX
                    s10000CtrlFineMese();
                    // COB_CODE: MOVE WK-DT-CALC TO  WK-DATA-AMG-POL-R
                    ws.getWkDataAmgPol().setWkDataAmgPolRFormatted(ws.getWkDtCalcFormatted());
                    // COB_CODE: IF WK-DATA-AMG-POL-R >
                    //              WTGA-DT-DECOR(IVVC0213-IX-TABB)
                    //                TO IVVC0213-VAL-STR-O
                    //           ELSE
                    //              PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                    //           END-IF
                    if (ws.getWkDataAmgPol().getWkDataAmgPolR() > ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecor()) {
                        // COB_CODE: MOVE WK-DATA-AMG-POL-R
                        //             TO IVVC0213-VAL-STR-O
                        ivvc0213.getTabOutput().setValStrO(ws.getWkDataAmgPol().getWkDataAmgPolRFormatted());
                    }
                    else {
                        // COB_CODE: MOVE WK-DATA-AMG-POL-R     TO WK-DATA-9
                        ws.setWkData9Formatted(ws.getWkDataAmgPol().getWkDataAmgPolRFormatted());
                        // COB_CODE: PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                        s1120CalcoloData();
                    }
                }
            }
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           ELSE
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM S1110-LETTURA-POLI
            //              THRU EX-S1110
            s1110LetturaPoli();
            // COB_CODE: IF IDSV0003-INVALID-OPER
            //              CONTINUE
            //           ELSE
            //              END-IF
            //           END-IF
            if (idsv0003.getReturnCode().isIdsv0003InvalidOper()) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (ws.getPoli().getPolDtDecor() <= ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecor()) {
                // COB_CODE: IF POL-DT-DECOR <= WTGA-DT-DECOR(IVVC0213-IX-TABB)
                //              END-IF
                //           END-IF
                // COB_CODE: MOVE POL-DT-DECOR             TO WK-DATA-X
                ws.setWkDataX(ws.getPoli().getPolDtDecorFormatted());
                // COB_CODE: MOVE WK-DATA-X(1:4)           TO WK-AAAA-POL
                ws.getWkDataAmgPol().setAaaaPolFormatted(ws.getWkDataXFormatted().substring((1) - 1, 4));
                // COB_CODE: MOVE WK-DATA-X(5:2)           TO WK-MM-POL
                ws.getWkDataAmgPol().setMmPolFormatted(ws.getWkDataXFormatted().substring((5) - 1, 6));
                // COB_CODE: MOVE WK-DATA-X(7:2)           TO WK-GG-POL
                ws.getWkDataAmgPol().setGgPolFormatted(ws.getWkDataXFormatted().substring((7) - 1, 8));
                // COB_CODE: MOVE WTGA-DT-DECOR(IVVC0213-IX-TABB)
                //             TO WK-DATA-X
                ws.setWkDataX(ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecorFormatted());
                // COB_CODE: MOVE WK-DATA-X(1:4)           TO WK-AAAA-TGA
                ws.getWkDataAmgTga().setAaaaTgaFormatted(ws.getWkDataXFormatted().substring((1) - 1, 4));
                // COB_CODE: MOVE WK-DATA-X(5:2)           TO WK-MM-TGA
                ws.getWkDataAmgTga().setMmTgaFormatted(ws.getWkDataXFormatted().substring((5) - 1, 6));
                // COB_CODE: MOVE WK-DATA-X(7:2)           TO WK-GG-TGA
                ws.getWkDataAmgTga().setGgTgaFormatted(ws.getWkDataXFormatted().substring((7) - 1, 8));
                // COB_CODE: MOVE WK-AAAA-TGA              TO WK-AAAA-POL
                ws.getWkDataAmgPol().setAaaaPolFormatted(ws.getWkDataAmgTga().getAaaaTgaFormatted());
                // COB_CODE: MOVE WK-DATA-AMG-POL-R     TO WK-DT-CALC
                ws.setWkDtCalcFormatted(ws.getWkDataAmgPol().getWkDataAmgPolRFormatted());
                // COB_CODE: MOVE WK-DT-CALC            TO WK-APPO-DT
                ws.getWkAppoDt().setWkAppoDtFormatted(ws.getWkDtCalcFormatted());
                // COB_CODE: PERFORM S10000-CTRL-FINE-MESE
                //              THRU S10000-CTRL-FINE-MESE-EX
                s10000CtrlFineMese();
                // COB_CODE: MOVE WK-DT-CALC    TO  WK-DATA-AMG-POL-R
                ws.getWkDataAmgPol().setWkDataAmgPolRFormatted(ws.getWkDtCalcFormatted());
                // COB_CODE: IF WK-DATA-AMG-POL-R >
                //              WTGA-DT-DECOR(IVVC0213-IX-TABB)
                //                TO IVVC0213-VAL-STR-O
                //           ELSE
                //              PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                //           END-IF
                if (ws.getWkDataAmgPol().getWkDataAmgPolR() > ws.getWtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaDtDecor()) {
                    // COB_CODE: MOVE WK-DATA-AMG-POL-R
                    //             TO IVVC0213-VAL-STR-O
                    ivvc0213.getTabOutput().setValStrO(ws.getWkDataAmgPol().getWkDataAmgPolRFormatted());
                }
                else {
                    // COB_CODE: MOVE WK-DATA-AMG-POL-R     TO WK-DATA-9
                    ws.setWkData9Formatted(ws.getWkDataAmgPol().getWkDataAmgPolRFormatted());
                    // COB_CODE: PERFORM S1120-CALCOLO-DATA THRU EX-S1120
                    s1120CalcoloData();
                }
            }
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA LDBS1130 ;'
            //               IDSV0003-RETURN-CODE ';'
            //               IDSV0003-SQLCODE
            //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS1130 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER     TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S10100-VALIDA-FINE-MESE<br>
	 * <pre>----------------------------------------------------------------*
	 *  VALIDAZIONE DATA FINE MESE
	 * ----------------------------------------------------------------*</pre>*/
    private void s10100ValidaFineMese() {
        ConcatUtil concatUtil = null;
        Lccs0004 lccs0004 = null;
        GenericParam param = null;
        // COB_CODE: ADD 1
        //             TO IX-CICLO.
        ws.setIxCiclo(Trunc.toShort(1 + ws.getIxCiclo(), 1));
        //
        // COB_CODE: MOVE WK-APPO-AAAA
        //             TO X-AAAA.
        ws.getxData().setAaaaFormatted(ws.getWkAppoDt().getAaaaFormatted());
        // COB_CODE: MOVE WK-APPO-MM
        //             TO X-MM.
        ws.getxData().setMmFormatted(ws.getWkAppoDt().getMmFormatted());
        // COB_CODE: MOVE WK-APPO-GG
        //             TO X-GG.
        ws.getxData().setGgFormatted(ws.getWkAppoDt().getGgFormatted());
        //
        // COB_CODE:      CALL LCCS0004         USING PARAM
        //                                            X-DATA
        //                ON EXCEPTION
        //           *
        //                   SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
        //           *
        //                END-CALL.
        try {
            lccs0004 = Lccs0004.getInstance();
            param = new GenericParam(MarshalByteExt.strToBuffer(ws.getParam2Formatted(), Lvvs0127Data.Len.PARAM2));
            lccs0004.run(param, ws.getxData());
            ws.setParam2FromBuffer(param.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE WK-CALL-PGM               TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'ERRORE CHIAMATA LCCS0004 ;'
            //                  PARAM
            //                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LCCS0004 ;", ws.getParam2AsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            //
        }
        //
        // COB_CODE:      IF PARAM NOT = ZEROES
        //           *
        //                       FROM WK-APPO-GG
        //           *
        //                END-IF.
        if (!Characters.EQ_ZERO.test(ws.getParam2Formatted())) {
            //
            // COB_CODE: SUBTRACT 1
            //               FROM WK-APPO-GG
            ws.getWkAppoDt().setGg(Trunc.toShort(abs(ws.getWkAppoDt().getGg() - 1), 2));
            //
        }
    }

    /**Original name: S1110-LETTURA-POLI<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA POLIZZA PER ID
	 * ----------------------------------------------------------------*</pre>*/
    private void s1110LetturaPoli() {
        Idbspol0 idbspol0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE POLI.
        initPoli();
        // COB_CODE: MOVE IVVC0213-ID-POLIZZA       TO POL-ID-POLI.
        ws.getPoli().setPolIdPoli(ivvc0213.getIdPolizza());
        // COB_CODE: MOVE 'IDBSPOL0'                TO WK-CALL-PGM.
        ws.setWkCallPgm("IDBSPOL0");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        idsv0003.getOperazione().setSelect();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-ID                TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011Id();
        // COB_CODE: CALL WK-CALL-PGM USING IDSV0003 POLI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER   TO TRUE
        //           END-CALL.
        try {
            idbspol0 = Idbspol0.getInstance();
            idbspol0.run(idsv0003, ws.getPoli());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-IDBSPOL0 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSPOL0 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA IDBSPOL0 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSPOL0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER     TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S1120-CALCOLO-DATA<br>
	 * <pre>----------------------------------------------------------------*
	 *  AGGIUNGE AD UNA DATA 1 ANNO.
	 * ----------------------------------------------------------------*</pre>*/
    private void s1120CalcoloData() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE IO-A2K-LCCC0003.
        initIoA2kLccc0003();
        // COB_CODE: MOVE '02'                         TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        // COB_CODE: MOVE '03'                         TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        // COB_CODE: MOVE 1                            TO A2K-DELTA.
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
        // COB_CODE: MOVE 'A'                          TO A2K-TDELTA.
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("A");
        // COB_CODE: MOVE WK-DATA-9                    TO A2K-INDATA.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(ws.getWkData9Formatted());
        // COB_CODE: MOVE '0'                          TO A2K-FISLAV
        //                                                A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        // COB_CODE: PERFORM S1121-CALL-LCCS0003       THRU EX-S1121.
        s1121CallLccs0003();
        // COB_CODE: IF IN-RCODE NOT = '00'
        //              SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
        //           ELSE
        //              MOVE A2K-OUAMG                 TO IVVC0213-VAL-STR-O
        //           END-IF.
        if (!ws.getInRcodeFormatted().equals("00")) {
            // COB_CODE: MOVE WK-CALL-PGM               TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'ERRORE CHIAMATA LCCS0003 ;'
            //                  IN-RCODE
            //                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE CHIAMATA LCCS0003 ;", ws.getInRcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
        else {
            // COB_CODE: MOVE A2K-OUAMG                 TO IVVC0213-VAL-STR-O
            ivvc0213.getTabOutput().setValStrO(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
        }
    }

    /**Original name: S1121-CALL-LCCS0003<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER IL CALCOLO SULLE DATE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1121CallLccs0003() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        // COB_CODE: MOVE 'LCCS0003'                 TO WK-CALL-PGM.
        ws.setWkCallPgm("LCCS0003");
        // COB_CODE: CALL WK-CALL-PGM USING IO-A2K-LCCC0003 IN-RCODE
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER    TO TRUE
        //           END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Lvvs0127Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-LCCS0003 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LCCS0003 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER    TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: S1130-LETTURA-ADESIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *    LETTURA ADESIONE PER ID
	 * ----------------------------------------------------------------*</pre>*/
    private void s1130LetturaAdesione() {
        Idbsade0 idbsade0 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE ADES.
        initAdes();
        // COB_CODE: MOVE IVVC0213-ID-ADESIONE      TO ADE-ID-ADES.
        ws.getAdes().setAdeIdAdes(ivvc0213.getIdAdesione());
        // COB_CODE: MOVE 'IDBSADE0'                TO WK-CALL-PGM.
        ws.setWkCallPgm("IDBSADE0");
        //  --> Tipo operazione
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        idsv0003.getOperazione().setSelect();
        //  --> Tipo livello
        // COB_CODE: SET IDSV0003-ID                TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011Id();
        // COB_CODE: CALL WK-CALL-PGM USING IDSV0003 ADES
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER   TO TRUE
        //           END-CALL.
        try {
            idbsade0 = Idbsade0.getInstance();
            idbsade0.run(idsv0003, ws.getAdes());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'CALL-IDBSADE0 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL-IDBSADE0 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER   TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              CONTINUE
        //           ELSE
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: STRING 'CHIAMATA IDBSADE0 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSADE0 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              SET IDSV0003-INVALID-OPER     TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isNotFound()) {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: S10000-CTRL-FINE-MESE<br>
	 * <pre>---------------------------------------------------------------*
	 * -- validazione della data di fine mese
	 * ---------------------------------------------------------------*</pre>*/
    private void s10000CtrlFineMese() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE ZEROES TO IX-CICLO.
        ws.setIxCiclo(((short)0));
        //
        // COB_CODE: MOVE 1      TO PARAM.
        ws.setParam2(((short)1));
        //
        // COB_CODE: MOVE 31     TO WK-APPO-GG.
        ws.getWkAppoDt().setGg(((short)31));
        //
        // COB_CODE: PERFORM S10100-VALIDA-FINE-MESE
        //              THRU S10100-VALIDA-FINE-MESE-EX
        //             UNTIL PARAM = 0
        //                OR IX-CICLO > 4.
        while (!(ws.getParam2() == 0 || ws.getIxCiclo() > 4)) {
            s10100ValidaFineMese();
        }
        //
        // --> Se Entro La Quarta Chiamata La Data Non H Stata Calcolata
        // --> Viene Imposta L'uscita Attrraverso Un Indice Di Chiamate
        //
        // COB_CODE:         IF IX-CICLO > 4
        //           *
        //                      SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
        //           *
        //                   ELSE
        //           *
        //                      END-IF
        //           *
        //                   END-IF.
        if (ws.getIxCiclo() > 4) {
            //
            // COB_CODE: MOVE LCCS0004         TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLccs0004());
            // COB_CODE: STRING 'IMPOSSIBILE CALCOLARE LA DATA FINE MESE';
            //               DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            idsv0003.getCampiEsito().setDescrizErrDb2("IMPOSSIBILE CALCOLARE LA DATA FINE MESE");
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED  TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            //
        }
        else {
            //
            // COB_CODE: IF WK-APPO-DT   < WK-DT-CALC
            //             MOVE WK-APPO-DT   TO WK-DT-CALC
            //           END-IF
            if (Conditions.lt(ws.getWkAppoDt().getWkAppoDtBytes(), MarshalByteExt.strToBuffer(ws.getWkDtCalcFormatted(), Lvvs0127Data.Len.WK_DT_CALC))) {
                // COB_CODE: MOVE WK-APPO-DT   TO WK-DT-CALC
                ws.setWkDtCalcFromBuffer(ws.getWkAppoDt().getWkAppoDtBytes());
            }
            //
        }
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaLdbv1131() {
        ws.getAreaLdbv1131().setIdOgg(0);
        ws.getAreaLdbv1131().setTpOgg("");
        ws.getAreaLdbv1131().setCodParam("");
    }

    public void initPoli() {
        ws.getPoli().setPolIdPoli(0);
        ws.getPoli().setPolIdMoviCrz(0);
        ws.getPoli().getPolIdMoviChiu().setPolIdMoviChiu(0);
        ws.getPoli().setPolIbOgg("");
        ws.getPoli().setPolIbProp("");
        ws.getPoli().getPolDtProp().setPolDtProp(0);
        ws.getPoli().setPolDtIniEff(0);
        ws.getPoli().setPolDtEndEff(0);
        ws.getPoli().setPolCodCompAnia(0);
        ws.getPoli().setPolDtDecor(0);
        ws.getPoli().setPolDtEmis(0);
        ws.getPoli().setPolTpPoli("");
        ws.getPoli().getPolDurAa().setPolDurAa(0);
        ws.getPoli().getPolDurMm().setPolDurMm(0);
        ws.getPoli().getPolDtScad().setPolDtScad(0);
        ws.getPoli().setPolCodProd("");
        ws.getPoli().setPolDtIniVldtProd(0);
        ws.getPoli().setPolCodConv("");
        ws.getPoli().setPolCodRamo("");
        ws.getPoli().getPolDtIniVldtConv().setPolDtIniVldtConv(0);
        ws.getPoli().getPolDtApplzConv().setPolDtApplzConv(0);
        ws.getPoli().setPolTpFrmAssva("");
        ws.getPoli().setPolTpRgmFisc("");
        ws.getPoli().setPolFlEstas(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComun(Types.SPACE_CHAR);
        ws.getPoli().setPolFlRshComunCond(Types.SPACE_CHAR);
        ws.getPoli().setPolTpLivGenzTit("");
        ws.getPoli().setPolFlCopFinanz(Types.SPACE_CHAR);
        ws.getPoli().setPolTpApplzDir("");
        ws.getPoli().getPolSpeMed().setPolSpeMed(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirEmis().setPolDirEmis(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDir1oVers().setPolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getPoli().getPolDirVersAgg().setPolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolCodDvs("");
        ws.getPoli().setPolFlFntAz(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntAder(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntTfr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlFntVolo(Types.SPACE_CHAR);
        ws.getPoli().setPolTpOpzAScad("");
        ws.getPoli().getPolAaDiffProrDflt().setPolAaDiffProrDflt(0);
        ws.getPoli().setPolFlVerProd("");
        ws.getPoli().getPolDurGg().setPolDurGg(0);
        ws.getPoli().getPolDirQuiet().setPolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getPoli().setPolTpPtfEstno("");
        ws.getPoli().setPolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getPoli().setPolConvGeco("");
        ws.getPoli().setPolDsRiga(0);
        ws.getPoli().setPolDsOperSql(Types.SPACE_CHAR);
        ws.getPoli().setPolDsVer(0);
        ws.getPoli().setPolDsTsIniCptz(0);
        ws.getPoli().setPolDsTsEndCptz(0);
        ws.getPoli().setPolDsUtente("");
        ws.getPoli().setPolDsStatoElab(Types.SPACE_CHAR);
        ws.getPoli().setPolFlScudoFisc(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTrasfe(Types.SPACE_CHAR);
        ws.getPoli().setPolFlTfrStrc(Types.SPACE_CHAR);
        ws.getPoli().getPolDtPresc().setPolDtPresc(0);
        ws.getPoli().setPolCodConvAgg("");
        ws.getPoli().setPolSubcatProd("");
        ws.getPoli().setPolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getPoli().setPolCodTpa("");
        ws.getPoli().getPolIdAccComm().setPolIdAccComm(0);
        ws.getPoli().setPolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getPoli().setPolFlPoliBundling(Types.SPACE_CHAR);
        ws.getPoli().setPolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getPoli().setPolFlVndBundle(Types.SPACE_CHAR);
        ws.getPoli().setPolIbBs("");
        ws.getPoli().setPolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initIoA2kLccc0003() {
        ws.getIoA2kLccc0003().getInput().setA2kFunz("");
        ws.getIoA2kLccc0003().getInput().setA2kInfo("");
        ws.getIoA2kLccc0003().getInput().setA2kDeltaFormatted("000");
        ws.getIoA2kLccc0003().getInput().setA2kTdelta(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kFislav(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kInicon(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata("");
        ws.getIoA2kLccc0003().getOutput().getA2kOugmaX().setA2kOugmaFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOugg02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus102(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOumm02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus202(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuss02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuaa02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().setA2kOuamgFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().setA2kOuamgp(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuamg0p(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog9(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprogc(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuproco(0);
        ws.getIoA2kLccc0003().getOutput().getA2kOujulX().setA2kOujulFormatted("0000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOugg04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOumm04("");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuss04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuaa04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOugg05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOumm05("");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOuaa05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg06("");
        ws.getIoA2kLccc0003().getOutput().setA2kOung06Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOutg06(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg07Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugl07X().setA2kOufa07Formatted("000");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg08Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg09(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg10(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().setRcode("");
    }

    public void initAdes() {
        ws.getAdes().setAdeIdAdes(0);
        ws.getAdes().setAdeIdPoli(0);
        ws.getAdes().setAdeIdMoviCrz(0);
        ws.getAdes().getAdeIdMoviChiu().setAdeIdMoviChiu(0);
        ws.getAdes().setAdeDtIniEff(0);
        ws.getAdes().setAdeDtEndEff(0);
        ws.getAdes().setAdeIbPrev("");
        ws.getAdes().setAdeIbOgg("");
        ws.getAdes().setAdeCodCompAnia(0);
        ws.getAdes().getAdeDtDecor().setAdeDtDecor(0);
        ws.getAdes().getAdeDtScad().setAdeDtScad(0);
        ws.getAdes().getAdeEtaAScad().setAdeEtaAScad(0);
        ws.getAdes().getAdeDurAa().setAdeDurAa(0);
        ws.getAdes().getAdeDurMm().setAdeDurMm(0);
        ws.getAdes().getAdeDurGg().setAdeDurGg(0);
        ws.getAdes().setAdeTpRgmFisc("");
        ws.getAdes().setAdeTpRiat("");
        ws.getAdes().setAdeTpModPagTit("");
        ws.getAdes().setAdeTpIas("");
        ws.getAdes().getAdeDtVarzTpIas().setAdeDtVarzTpIas(0);
        ws.getAdes().getAdePreNetInd().setAdePreNetInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePreLrdInd().setAdePreLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeRatLrdInd().setAdeRatLrdInd(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePrstzIniInd().setAdePrstzIniInd(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlCoincAssto(Types.SPACE_CHAR);
        ws.getAdes().setAdeIbDflt("");
        ws.getAdes().setAdeModCalc("");
        ws.getAdes().setAdeTpFntCnbtva("");
        ws.getAdes().getAdeImpAz().setAdeImpAz(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpAder().setAdeImpAder(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpTfr().setAdeImpTfr(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpVolo().setAdeImpVolo(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdePcAz().setAdePcAz(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcAder().setAdePcAder(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcTfr().setAdePcTfr(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdePcVolo().setAdePcVolo(new AfDecimal(0, 6, 3));
        ws.getAdes().getAdeDtNovaRgmFisc().setAdeDtNovaRgmFisc(0);
        ws.getAdes().setAdeFlAttiv(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpRecRitVis().setAdeImpRecRitVis(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpRecRitAcc().setAdeImpRecRitAcc(new AfDecimal(0, 15, 3));
        ws.getAdes().setAdeFlVarzStatTbgc(Types.SPACE_CHAR);
        ws.getAdes().setAdeFlProvzaMigraz(Types.SPACE_CHAR);
        ws.getAdes().getAdeImpbVisDaRec().setAdeImpbVisDaRec(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtDecorPrestBan().setAdeDtDecorPrestBan(0);
        ws.getAdes().getAdeDtEffVarzStatT().setAdeDtEffVarzStatT(0);
        ws.getAdes().setAdeDsRiga(0);
        ws.getAdes().setAdeDsOperSql(Types.SPACE_CHAR);
        ws.getAdes().setAdeDsVer(0);
        ws.getAdes().setAdeDsTsIniCptz(0);
        ws.getAdes().setAdeDsTsEndCptz(0);
        ws.getAdes().setAdeDsUtente("");
        ws.getAdes().setAdeDsStatoElab(Types.SPACE_CHAR);
        ws.getAdes().getAdeCumCnbtCap().setAdeCumCnbtCap(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeImpGarCnbt().setAdeImpGarCnbt(new AfDecimal(0, 15, 3));
        ws.getAdes().getAdeDtUltConsCnbt().setAdeDtUltConsCnbt(0);
        ws.getAdes().setAdeIdenIscFnd("");
        ws.getAdes().getAdeNumRatPian().setAdeNumRatPian(new AfDecimal(0, 12, 5));
        ws.getAdes().getAdeDtPresc().setAdeDtPresc(0);
        ws.getAdes().setAdeConcsPrest(Types.SPACE_CHAR);
    }
}
