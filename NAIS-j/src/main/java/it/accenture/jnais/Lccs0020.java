package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Lccv0021AreaOutput;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.Lccs0020Data;
import it.accenture.jnais.ws.Lccv0021;

/**Original name: LCCS0020<br>
 * <pre>*****************************************************************
 * *                                                        ********
 * *    PORTAFOGLIO VITA ITALIA  VER 1.0                    ********
 * *                                                        ********
 * *****************************************************************
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       11/2007.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *     PROGRAMMA ..... LCCS0020
 *     TIPOLOGIA...... XXX
 *     PROCESSO....... XXX
 *     FUNZIONE....... LETTURA TABELLA MATRICE MOVIMENTO
 *     DESCRIZIONE.... TRASCODIFICA TIPO MOV-NAIS <-> MOV-ACTUATOR
 * **------------------------------------------------------------***</pre>*/
public class Lccs0020 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lccs0020Data ws = new Lccs0020Data();
    //Original name: IDSV0001
    private AreaIdsv0001 idsv0001;
    //Original name: LCCV0021
    private Lccv0021 lccv0021;

    //==== METHODS ====
    /**Original name: PROGRAM_LCCS0020_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(AreaIdsv0001 idsv0001, Lccv0021 lccv0021) {
        this.idsv0001 = idsv0001;
        this.lccv0021 = lccv0021;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: IF WS-ESITO-OK
        //                 THRU EX-S1000
        //           END-IF.
        if (ws.isWsEsito()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lccs0020 getInstance() {
        return ((Lccs0020)Programs.getInstance(Lccs0020.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI INIZIALI                                         *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: PERFORM S0005-INITIALIZE
        //              THRU EX-S0005.
        s0005Initialize();
        // COB_CODE: PERFORM S0010-CTRL-DATI-INPUT
        //              THRU EX-S0010.
        s0010CtrlDatiInput();
    }

    /**Original name: S0005-INITIALIZE<br>
	 * <pre>----------------------------------------------------------------*
	 *     INIZIALIZZAZIONI
	 * ----------------------------------------------------------------*</pre>*/
    private void s0005Initialize() {
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET WS-ESITO-OK                TO TRUE.
        ws.setWsEsito(true);
        // COB_CODE: MOVE ZEROES                    TO IDSV0003-SQLCODE.
        ws.getIdsv0003().getSqlcode().setSqlcode(0);
        // COB_CODE: MOVE SPACES                    TO IDSV0003-DESCRIZ-ERR-DB2
        //                                             IDSV0003-NOME-TABELLA
        //                                             IDSV0003-KEY-TABELLA.
        ws.getIdsv0003().getCampiEsito().setDescrizErrDb2("");
        ws.getIdsv0003().getCampiEsito().setNomeTabella("");
        ws.getIdsv0003().getCampiEsito().setKeyTabella("");
    }

    /**Original name: S0010-CTRL-DATI-INPUT<br>
	 * <pre>----------------------------------------------------------------*
	 *   CONTROLLI DI INPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void s0010CtrlDatiInput() {
        ConcatUtil concatUtil = null;
        // COB_CODE:      IF IDSV0001-COD-COMPAGNIA-ANIA IS NOT NUMERIC
        //                OR IDSV0001-COD-COMPAGNIA-ANIA = ZERO
        //           *-->    ERRORE:IL CAMPO E' OBBLIGATORIO
        //                   END-STRING
        //                ELSE
        //                   END-IF
        //                END-IF.
        if (!Functions.isNumber(idsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted()) || Characters.EQ_ZERO.test(idsv0001.getAreaComune().getIdsv0001CodCompagniaAniaFormatted())) {
            //-->    ERRORE:IL CAMPO E' OBBLIGATORIO
            // COB_CODE: SET  WS-ESITO-KO      TO TRUE
            ws.setWsEsito(false);
            // COB_CODE: MOVE WK-PGM           TO LCCV0021-COD-SERVIZIO-BE
            lccv0021.getAreaOutput().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'CODICE COMPAGNIA NON VALIDO '
            //           DELIMITED BY SIZE   INTO LCCV0021-DESCRIZ-ERR
            //           END-STRING
            lccv0021.getAreaOutput().setDescrizErr("CODICE COMPAGNIA NON VALIDO ");
        }
        else if (!Functions.isNumber(lccv0021.getTpMovPtfFormatted()) || Characters.EQ_ZERO.test(lccv0021.getTpMovPtfFormatted())) {
            // COB_CODE:         IF LCCV0021-TP-MOV-PTF IS NOT NUMERIC
            //                   OR LCCV0021-TP-MOV-PTF = ZERO
            //           *-->       ERRORE: IL CAMPO E' OBBLIGATORIO
            //                      END-STRING
            //                   END-IF
            //-->       ERRORE: IL CAMPO E' OBBLIGATORIO
            // COB_CODE: SET  WS-ESITO-KO      TO TRUE
            ws.setWsEsito(false);
            // COB_CODE: MOVE WK-PGM           TO LCCV0021-COD-SERVIZIO-BE
            lccv0021.getAreaOutput().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: STRING 'TIPO MOVIMENTO NON VALIDO '
            //           DELIMITED BY SIZE   INTO LCCV0021-DESCRIZ-ERR
            //           END-STRING
            lccv0021.getAreaOutput().setDescrizErr("TIPO MOVIMENTO NON VALIDO ");
        }
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE                                                *
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-PREPARA-LDBS0260
        //              THRU EX-S1100.
        s1100PreparaLdbs0260();
        // COB_CODE: PERFORM S1200-CALL-LDBS0260
        //              THRU EX-S1200.
        s1200CallLdbs0260();
    }

    /**Original name: S1100-PREPARA-LDBS0260<br>
	 * <pre>----------------------------------------------------------------*
	 *     VALORIZZAZIONE CAMPI DI INPUT DA PASSARE AL SERVIZIO        *
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100PreparaLdbs0260() {
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        ws.getIdsv0003().getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION   TO TRUE.
        ws.getIdsv0003().getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE.
        ws.getIdsv0003().getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE.
        ws.getIdsv0003().getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
        //             TO IDSV0003-TRATTAMENTO-STORICITA.
        ws.getIdsv0003().getTrattamentoStoricita().setTrattamentoStoricita(idsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //             TO IDSV0003-FORMATO-DATA-DB.
        ws.getIdsv0003().getFormatoDataDb().setFormatoDataDb(idsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO IDSV0003-CODICE-COMPAGNIA-ANIA.
        ws.getIdsv0003().setCodiceCompagniaAnia(idsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE LCCV0021-TP-MOV-PTF
        //             TO LDBI0260-COD-MOVI-NAIS.
        ws.getAreaLdbi0260().setCodMoviNais(lccv0021.getTpMovPtf());
        // COB_CODE: MOVE AREA-LDBI0260
        //             TO IDSV0003-BUFFER-WHERE-COND.
        ws.getIdsv0003().setBufferWhereCond(ws.getAreaLdbi0260().getAreaLdbi0260Formatted());
    }

    /**Original name: S1200-CALL-LDBS0260<br>
	 * <pre>----------------------------------------------------------------*
	 *     CHIAMATA AL SERVIZIO LDBS0260
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200CallLdbs0260() {
        ConcatUtil concatUtil = null;
        Ldbs0260 ldbs0260 = null;
        // COB_CODE: CALL LDBS0260        USING IDSV0003
        //                                      MATR-MOVIMENTO
        //           ON EXCEPTION
        //              END-STRING
        //           END-CALL.
        try {
            ldbs0260 = Ldbs0260.getInstance();
            ldbs0260.run(ws.getIdsv0003(), ws.getMatrMovimento());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-PGM              TO LCCV0021-COD-SERVIZIO-BE
            lccv0021.getAreaOutput().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: SET  IDSV0003-SQL-ERROR  TO TRUE
            ws.getIdsv0003().getReturnCode().setSqlError();
            // COB_CODE: STRING 'ERRORE CALL LDBS0260 '
            //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            ws.getIdsv0003().getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS0260 ");
        }
        // COB_CODE: INITIALIZE                     LCCV0021-AREA-OUTPUT.
        initAreaOutput();
        // COB_CODE: MOVE IDSV0003-RETURN-CODE   TO LCCV0021-RETURN-CODE.
        lccv0021.getAreaOutput().getReturnCode().setReturnCode(ws.getIdsv0003().getReturnCode().getReturnCode());
        // COB_CODE: MOVE IDSV0003-SQLCODE       TO LCCV0021-SQLCODE.
        lccv0021.getAreaOutput().getSqlcode().setSqlcodeSigned(ws.getIdsv0003().getSqlcode().getSqlcode());
        // COB_CODE: MOVE IDSV0003-CAMPI-ESITO   TO LCCV0021-CAMPI-ESITO.
        lccv0021.getAreaOutput().setCampiEsitoBytes(ws.getIdsv0003().getCampiEsito().getCampiEsitoBytes());
        // COB_CODE: IF  LCCV0021-SUCCESSFUL-RC
        //           AND LCCV0021-SUCCESSFUL-SQL
        //               MOVE MMO-COD-PROCESSO-WF TO LCCV0021-COD-PROCESSO-WF
        //           ELSE
        //              END-IF
        //           END-IF.
        if (lccv0021.getAreaOutput().getReturnCode().isSuccessfulRc() && lccv0021.getAreaOutput().getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE MMO-TP-MOVI-ACT    TO WK-MOVI-INP
            ws.setWkMoviInp(ws.getMatrMovimento().getTpMoviAct());
            //--> CODIFICA DEL CODICE MOVIMENTO DA ALFANUMERICO A NUMERICO DI 5
            // COB_CODE: PERFORM S3000-CODIFICA-MOVI
            //              THRU S3000-EX
            s3000CodificaMovi();
            // COB_CODE: MOVE WK-MOVI-OUT         TO LCCV0021-TP-MOV-ACT
            lccv0021.getAreaOutput().setTpMovActFormatted(ws.getWkMoviOutFormatted());
            // COB_CODE: MOVE MMO-COD-PROCESSO-WF TO LCCV0021-COD-PROCESSO-WF
            lccv0021.getAreaOutput().setCodProcessoWf(ws.getMatrMovimento().getCodProcessoWf());
        }
        else if (lccv0021.getAreaOutput().getSqlcode().isNotFound()) {
            // COB_CODE: IF LCCV0021-NOT-FOUND
            //                                      TO LCCV0021-DESCRIZ-ERR
            //           END-IF
            // COB_CODE: SET  LCCV0021-SQL-ERROR TO TRUE
            lccv0021.getAreaOutput().getReturnCode().setSqlError();
            // COB_CODE: MOVE SPACES             TO LCCV0021-DESCRIZ-ERR
            lccv0021.getAreaOutput().setDescrizErr("");
            // COB_CODE: MOVE WK-PGM             TO LCCV0021-COD-SERVIZIO-BE
            lccv0021.getAreaOutput().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE  'OCCORRENZA NON TROVATA SU TAB. MATR_MOVIMENTO'
            //                                   TO LCCV0021-DESCRIZ-ERR
            lccv0021.getAreaOutput().setDescrizErr("OCCORRENZA NON TROVATA SU TAB. MATR_MOVIMENTO");
        }
    }

    /**Original name: S3000-CODIFICA-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *     CONVERTI VALORE DA STRINGA IN NUMERICO
	 * ----------------------------------------------------------------*</pre>*/
    private void s3000CodificaMovi() {
        Iwfs0050 iwfs0050 = null;
        // COB_CODE: MOVE HIGH-VALUE              TO AREA-CALL-IWFS0050.
        ws.getAreaCallIwfs0050().initAreaCallIwfs0050HighValues();
        // COB_CODE: MOVE WK-MOVI-INP             TO IWFI0051-ARRAY-STRINGA-INPUT.
        ws.getAreaCallIwfs0050().setIwfi0051ArrayStringaInputFormatted(ws.getWkMoviInpFormatted());
        // COB_CODE: CALL IWFS0050 USING AREA-CALL-IWFS0050
        iwfs0050 = Iwfs0050.getInstance();
        iwfs0050.run(ws.getAreaCallIwfs0050());
        // COB_CODE: MOVE IWFO0051-ESITO          TO IDSV0001-ESITO.
        idsv0001.getEsito().setEsito(ws.getAreaCallIwfs0050().getIwfo0051().getEsito().getEsito());
        // COB_CODE: MOVE IWFO0051-LOG-ERRORE     TO IDSV0001-LOG-ERRORE.
        idsv0001.getLogErrore().setLogErroreBytes(ws.getAreaCallIwfs0050().getIwfo0051().getLogErrore().getIwfo0051LogErroreBytes());
        // COB_CODE: MOVE IWFO0051-CAMPO-OUTPUT-DEFI
        //                                        TO WK-MOVI-OUT.
        ws.setWkMoviOut(TruncAbs.toInt(ws.getAreaCallIwfs0050().getIwfo0051().getCampoOutputDefi(), 5));
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *     OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initAreaOutput() {
        lccv0021.getAreaOutput().setTpMovActFormatted("00000");
        lccv0021.getAreaOutput().setCodProcessoWf("");
        lccv0021.getAreaOutput().getReturnCode().setReturnCode("");
        lccv0021.getAreaOutput().getSqlcode().setSqlcodeSigned(0);
        lccv0021.getAreaOutput().setDescrizErr("");
        lccv0021.getAreaOutput().setCodServizioBe("");
        lccv0021.getAreaOutput().setNomeTabella("");
        lccv0021.getAreaOutput().setKeyTabella("");
    }
}
