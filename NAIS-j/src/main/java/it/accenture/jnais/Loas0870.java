package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.AreaIdsv0001;
import it.accenture.jnais.ws.enums.Idso0011SqlcodeSigned;
import it.accenture.jnais.ws.Ieai9901Area;
import it.accenture.jnais.ws.Lccc0001;
import it.accenture.jnais.ws.Loas0870Data;
import it.accenture.jnais.ws.W870AreaLoas0870;
import it.accenture.jnais.ws.WadeAreaAdesioneLoas0800;
import it.accenture.jnais.ws.WgrzAreaGaranziaLccs0005;
import it.accenture.jnais.ws.WpmoAreaParamMoviLoas0800;
import it.accenture.jnais.ws.WpolAreaPolizzaLccs0005;
import it.accenture.jnais.ws.WtgaAreaTrancheLoas0800;
import static java.lang.Math.abs;

/**Original name: LOAS0870<br>
 * <pre>  ============================================================= *
 *                                                                 *
 *         PORTAFOGLIO VITA ITALIA  VER 1.0                        *
 *                                                                 *
 *   ============================================================= *
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       SETTEMBRE 2007.
 * DATE-COMPILED.
 * ----------------------------------------------------------------*
 *      PROGRAMMA ..... LOAS0870                                   *
 *      TIPOLOGIA...... OPERAZIONI AUTOMATICHE                     *
 *      PROCESSO....... ADEGUAMENTO PREMIO PRESTAZIONE             *
 *      FUNZIONE....... BATCH                                      *
 *      DESCRIZIONE.... GAP RICH090 COLLETTIVE                     *
 *      PAGINA WEB..... N.A.                                       *
 * ----------------------------------------------------------------*</pre>*/
public class Loas0870 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Loas0870Data ws = new Loas0870Data();
    //Original name: AREA-IDSV0001
    private AreaIdsv0001 areaIdsv0001;
    //Original name: WCOM-AREA-STATI
    private Lccc0001 lccc0001;
    //Original name: WPMO-AREA-PARAM-MOVI
    private WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi;
    //Original name: WPOL-AREA-POLIZZA
    private WpolAreaPolizzaLccs0005 wpolAreaPolizza;
    //Original name: WADE-AREA-ADESIONE
    private WadeAreaAdesioneLoas0800 wadeAreaAdesione;
    //Original name: WGRZ-AREA-GARANZIA
    private WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia;
    //Original name: WTGA-AREA-TRANCHE
    private WtgaAreaTrancheLoas0800 wtgaAreaTranche;
    //Original name: W870-AREA-LOAS0870
    private W870AreaLoas0870 w870AreaLoas0870;

    //==== METHODS ====
    /**Original name: PROGRAM_LOAS0870_FIRST_SENTENCES<br>*/
    public long execute(AreaIdsv0001 areaIdsv0001, Lccc0001 lccc0001, WpmoAreaParamMoviLoas0800 wpmoAreaParamMovi, WpolAreaPolizzaLccs0005 wpolAreaPolizza, WadeAreaAdesioneLoas0800 wadeAreaAdesione, WgrzAreaGaranziaLccs0005 wgrzAreaGaranzia, WtgaAreaTrancheLoas0800 wtgaAreaTranche, W870AreaLoas0870 w870AreaLoas0870) {
        this.areaIdsv0001 = areaIdsv0001;
        this.lccc0001 = lccc0001;
        this.wpmoAreaParamMovi = wpmoAreaParamMovi;
        this.wpolAreaPolizza = wpolAreaPolizza;
        this.wadeAreaAdesione = wadeAreaAdesione;
        this.wgrzAreaGaranzia = wgrzAreaGaranzia;
        this.wtgaAreaTranche = wtgaAreaTranche;
        this.w870AreaLoas0870 = w870AreaLoas0870;
        // COB_CODE: PERFORM S00000-OPERAZ-INIZIALI
        //              THRU S00000-OPERAZ-INIZIALI-EX.
        s00000OperazIniziali();
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10000-ELABORAZIONE-EX
        //           *
        //                END-IF.
        if (this.areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10000-ELABORAZIONE
            //              THRU S10000-ELABORAZIONE-EX
            s10000Elaborazione();
            //
        }
        //
        // COB_CODE: PERFORM S90000-OPERAZ-FINALI
        //              THRU S90000-OPERAZ-FINALI-EX.
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LOAS0870.cbl:line=312, because the code is unreachable.
        //
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Loas0870 getInstance() {
        return ((Loas0870)Programs.getInstance(Loas0870.class));
    }

    /**Original name: S00000-OPERAZ-INIZIALI<br>
	 * <pre> ============================================================== *
	 *  -->           O P E R Z I O N I   I N I Z I A L I          <-- *
	 *  ============================================================== *</pre>*/
    private void s00000OperazIniziali() {
        // COB_CODE: MOVE 'S00000-OPERAZ-INIZIALI'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00000-OPERAZ-INIZIALI");
        //
        // --> Inizializazione delle aree di working storage
        //
        // COB_CODE: PERFORM S00100-INIZIA-AREE-WS
        //              THRU S00100-INIZIA-AREE-WS-EX.
        s00100IniziaAreeWs();
    }

    /**Original name: S00100-INIZIA-AREE-WS<br>
	 * <pre>----------------------------------------------------------------*
	 *      INIZIALIZZAZIONE DELLE AREE DI WORKING STORAGE             *
	 * ----------------------------------------------------------------*</pre>*/
    private void s00100IniziaAreeWs() {
        // COB_CODE: MOVE 'S00100-INIZIA-AREE-WS'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S00100-INIZIA-AREE-WS");
        //
        //  Inizializazione di tutti gli indici e le aree di WS
        //
        // COB_CODE: INITIALIZE IX-INDICI.
        initIxIndici();
        //
        // COB_CODE: SET  IDSV0001-ESITO-OK
        //             TO TRUE.
        areaIdsv0001.getEsito().setIdsv0001EsitoOk();
        // COB_CODE: MOVE  ZEROES  TO  WS-NUM-AA-PAG-PRE
        //                             WS-NUM-AA.
        ws.setWsNumAaPagPre(0);
        ws.setWsNumAa(0);
    }

    /**Original name: S10000-ELABORAZIONE<br>
	 * <pre> ============================================================== *
	 *  -->               E L A B O R A Z I O N E                  <-- *
	 *  ============================================================== *</pre>*/
    private void s10000Elaborazione() {
        // COB_CODE: MOVE 'S10000-ELABORAZIONE'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10000-ELABORAZIONE");
        //
        // COB_CODE: IF IDSV0001-ESITO-OK
        //                 THRU S10050-EX
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: PERFORM S10050-LEGGI-PARAM-COMP
            //              THRU S10050-EX
            s10050LeggiParamComp();
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                   END-IF
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE:         IF W870-TGA-NUM-MAX-ELE = ZEROES
            //           *
            //                         THRU S10200-CARICA-TIT-DB-EX
            //           *
            //                   END-IF
            if (w870AreaLoas0870.getW870TgaNumMaxEle() == 0) {
                //
                // COB_CODE: PERFORM S10200-CARICA-TIT-DB
                //              THRU S10200-CARICA-TIT-DB-EX
                s10200CaricaTitDb();
                //
            }
            //
        }
        //
        // COB_CODE:      IF IDSV0001-ESITO-OK
        //           *
        //                      THRU S10600-DETERMINA-TP-RIVAL-EX
        //           *
        //                END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            //
            // COB_CODE: PERFORM S10600-DETERMINA-TP-RIVAL
            //              THRU S10600-DETERMINA-TP-RIVAL-EX
            s10600DeterminaTpRival();
            //
        }
    }

    /**Original name: S10050-LEGGI-PARAM-COMP<br>
	 * <pre>----------------------------------------------------------------*
	 *               ACCESSO ALLA TABELLA PARAMETRO COMPAGNIA          *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10050LeggiParamComp() {
        // COB_CODE: MOVE 'S10050-LEGGI-PARAM-COMP' TO WK-LABEL-ERR
        ws.getWkGestioneMsgErr().setLabelErr("S10050-LEGGI-PARAM-COMP");
        //
        // COB_CODE: PERFORM S10070-IMPOSTA-PARAM-COMP
        //              THRU S10070-IMPOSTA-PARAM-COMP-EX.
        s10070ImpostaParamComp();
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata
            //           *
            //                            THRU GESTIONE-ERR-STD-EX
            //           *
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                            END-IF
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //           *
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata
                    //
                    // COB_CODE: MOVE 'COMPAGNIA NON TROVATA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("COMPAGNIA NON TROVATA");
                    // COB_CODE: MOVE '005069'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005069");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                    //             TO PARAM-COMP
                    ws.getParamComp().setParamCompFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                    // COB_CODE: IF PCO-NUM-MM-CALC-MORA-NULL = HIGH-VALUES
                    //              MOVE ZEROES TO PCO-NUM-MM-CALC-MORA
                    //           END-IF
                    if (Characters.EQ_HIGH.test(ws.getParamComp().getPcoNumMmCalcMora().getPcoNumMmCalcMoraNullFormatted())) {
                        // COB_CODE: MOVE ZEROES TO PCO-NUM-MM-CALC-MORA
                        ws.getParamComp().getPcoNumMmCalcMora().setPcoNumMmCalcMora(0);
                    }
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    //
                    // COB_CODE: MOVE 'ERRORE LETTURA PARAMETRO COMPAGNIA'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA PARAMETRO COMPAGNIA");
                    // COB_CODE: MOVE '005016'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005016");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA PARAMETRO COMPAGNIA'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE DISPATCHER LETTURA PARAMETRO COMPAGNIA");
            // COB_CODE: MOVE '005016'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005016");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10070-IMPOSTA-PARAM-COMP<br>
	 * <pre> -------------------------------------------------------------- *
	 *  VALORIZZAZIONE DELLE CHIAVI DELLA TABELLA PARAMETRO COMPAGNIA  *
	 *  -------------------------------------------------------------- *</pre>*/
    private void s10070ImpostaParamComp() {
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //             TO PCO-COD-COMP-ANIA.
        ws.getParamComp().setPcoCodCompAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        //                IDSI0011-DATA-FINE-EFFETTO
        //                IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'PARAM-COMP'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("PARAM-COMP");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE PARAM-COMP
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getParamComp().getParamCompFormatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-SELECT
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setSelect();
        //
        //  --> Tipo livello
        //
        // COB_CODE: SET IDSI0011-PRIMARY-KEY
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setPrimaryKey();
        //
        //  --> Modalita di accesso
        //
        // COB_CODE: SET IDSI0011-TRATT-SENZA-STOR
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattSenzaStor();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S10200-CARICA-TIT-DB<br>
	 * <pre>----------------------------------------------------------------*
	 *  CARICAMENTO TITOLO CONTABILE DA DB
	 * ----------------------------------------------------------------*</pre>*/
    private void s10200CaricaTitDb() {
        // COB_CODE: MOVE 'S10200-CARICA-TIT-DB'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10200-CARICA-TIT-DB");
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                          THRU S10230-RICERCA-TITOLO-EX
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setIxTabTga(((short)1));
        while (!(ws.getIxIndici().getIxTabTga() > wtgaAreaTranche.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE: ADD 1 TO IX-TAB-FLAGS
            ws.getIxIndici().setIxTabFlags(Trunc.toShort(1 + ws.getIxIndici().getIxTabFlags(), 4));
            // COB_CODE: MOVE 'N' TO WS-FLAG-INC-TGA(IX-TAB-FLAGS)
            ws.getWsFg(ws.getIxIndici().getIxTabFlags()).setWsFlagIncTgaFormatted("N");
            // COB_CODE: PERFORM S10230-RICERCA-TITOLO
            //              THRU S10230-RICERCA-TITOLO-EX
            s10230RicercaTitolo();
            //
            ws.getIxIndici().setIxTabTga(Trunc.toShort(ws.getIxIndici().getIxTabTga() + 1, 4));
        }
    }

    /**Original name: S10209-DET-DATA-INI-CALC<br>
	 * <pre>---------------------------------------------------------------
	 *  DETERMINA LA DATA INIZIO DA CUI DETERMINA L'INTERVALLO
	 *  DI ESTRAZIONE DEI TITOLI CONTABILI
	 * ---------------------------------------------------------------</pre>*/
    private void s10209DetDataIniCalc() {
        // COB_CODE: MOVE 'S10209-DET-DATA-INI-CALC' TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10209-DET-DATA-INI-CALC");
        // COB_CODE: IF WPMO-DT-RICOR-PREC-NULL(INDPMO) = HIGH-VALUES
        //              MOVE WTGA-DT-DECOR(IX-TAB-TGA)  TO WK-DT-INIZIO-CALCOLO
        //           ELSE
        //              MOVE WPMO-DT-RICOR-PREC(INDPMO) TO WK-DT-INIZIO-CALCOLO
        //           END-IF.
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getIndpmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrecNullFormatted())) {
            // COB_CODE: MOVE WTGA-DT-DECOR(IX-TAB-TGA)  TO WK-DT-INIZIO-CALCOLO
            ws.setWkDtInizioCalcolo(TruncAbs.toInt(wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtDecor(), 8));
        }
        else {
            // COB_CODE: MOVE WPMO-DT-RICOR-PREC(INDPMO) TO WK-DT-INIZIO-CALCOLO
            ws.setWkDtInizioCalcolo(TruncAbs.toInt(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getIndpmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrec(), 8));
        }
    }

    /**Original name: S10210-DET-DATA-A<br>
	 * <pre>----------------------------------------------------------------*
	 *  VENGONO SOTTRATTI TRE MESI ALL'EFFETTO DI ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s10210DetDataA() {
        // COB_CODE: MOVE 'S10210-DET-DATA-A'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10210-DET-DATA-A");
        //
        // COB_CODE: MOVE 12 TO WK-INTERVALLO
        ws.setWkIntervallo(((short)12));
        // COB_CODE: COMPUTE WK-INTERVALLO =
        //                   WK-INTERVALLO - PCO-NUM-MM-CALC-MORA - 1
        ws.setWkIntervallo(Trunc.toShort(abs(ws.getWkIntervallo() - ws.getParamComp().getPcoNumMmCalcMora().getPcoNumMmCalcMora() - 1), 2));
        // COB_CODE: MOVE ZEROES
        //             TO WK-DT-A.
        ws.setWkDtA(0);
        //
        // --> A2K-DELTA -> Delta Da Sommare
        //
        // COB_CODE: MOVE WK-INTERVALLO
        //             TO A2K-DELTA.
        ws.getIoA2kLccc0003().getInput().setA2kDeltaFormatted(ws.getWkIntervalloFormatted());
        //
        // COB_CODE: MOVE 'M'
        //             TO A2K-TDELTA.
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("M");
        //
        // --> A2K-INAMG -> Data Di Partenza Alla Quale Sommare Il Delta
        //
        // COB_CODE: MOVE WK-DT-INIZIO-CALCOLO
        //             TO A2K-INAMG.
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamgFormatted(ws.getWkDtInizioCalcoloFormatted());
        //
        // --> A2K-FUNZ -> Tipo Funzione: Somma Delta
        //
        // COB_CODE: MOVE '02'
        //             TO A2K-FUNZ.
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //
        // --> A2K-INFO -> Tipo Formato Data: AAAAMMGG
        //
        // COB_CODE: MOVE '03'
        //             TO A2K-INFO.
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //
        // --> A2K-INICON -> Giorno Inizio Conteggio (Stesso Giorno)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-INICON.
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //
        // --> A2K-FISLAV -> Tipo Giorni Da Sommare (Fissi)
        //
        // COB_CODE: MOVE '0'
        //             TO A2K-FISLAV.
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //
        // COB_CODE: PERFORM S10220-CHIAMA-LCCS0003
        //              THRU S10220-CHIAMA-LCCS0003-EX.
        s10220ChiamaLccs0003();
        //
        // COB_CODE: IF IN-RCODE  = '00'
        //              MOVE A2K-OUAMG TO WK-DT-A
        //           END-IF.
        if (ws.getInRcodeFormatted().equals("00")) {
            // COB_CODE: MOVE A2K-OUAMG TO WK-DT-A
            ws.setWkDtAFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
        }
    }

    /**Original name: S10211-DET-DATA-DA<br>
	 * <pre>----------------------------------------------------------------*
	 *  DATA-DA PER PREMIO ANNUO
	 * ----------------------------------------------------------------*</pre>*/
    private void s10211DetDataDa() {
        // COB_CODE: MOVE 'S10211-DET-DATA-DA'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10211-DET-DATA-DA");
        //
        // COB_CODE: MOVE ZEROES                        TO WK-DT-DA.
        ws.setWkDtDa(0);
        //
        // COB_CODE: MOVE PCO-NUM-MM-CALC-MORA       TO A2K-DELTA
        ws.getIoA2kLccc0003().getInput().setA2kDelta(TruncAbs.toShort(ws.getParamComp().getPcoNumMmCalcMora().getPcoNumMmCalcMora(), 3));
        // COB_CODE: MOVE 'M'                        TO A2K-TDELTA
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("M");
        // COB_CODE: MOVE WK-DT-INIZIO-CALCOLO       TO A2K-INAMG
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kInamgFormatted(ws.getWkDtInizioCalcoloFormatted());
        // COB_CODE: MOVE '03'                       TO A2K-FUNZ
        ws.getIoA2kLccc0003().getInput().setA2kFunz("03");
        // COB_CODE: MOVE '03'                       TO A2K-INFO
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        // COB_CODE: MOVE '0'                        TO A2K-INICON
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        // COB_CODE: MOVE '0'                        TO A2K-FISLAV
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        // COB_CODE: PERFORM S10220-CHIAMA-LCCS0003
        //              THRU S10220-CHIAMA-LCCS0003-EX
        s10220ChiamaLccs0003();
        // COB_CODE: IF IN-RCODE  = '00'
        //                 MOVE A2K-OUAMG               TO WK-DT-DA
        //           END-IF.
        if (ws.getInRcodeFormatted().equals("00")) {
            // COB_CODE: MOVE A2K-OUAMG               TO WK-DT-DA
            ws.setWkDtDaFormatted(ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamgFormatted());
        }
    }

    /**Original name: S10215-DET-DATE-PUR<br>
	 * <pre>----------------------------------------------------------------*
	 *  DATA-DA DATA-A PER PREMIO UNICO E UNICO RICORRENTE
	 * ----------------------------------------------------------------*</pre>*/
    private void s10215DetDatePur() {
        // COB_CODE: MOVE 'S10215-DET-DATE-PUR'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10215-DET-DATE-PUR");
        //
        // COB_CODE: MOVE ZEROES TO WK-DT-DA.
        ws.setWkDtDa(0);
        // COB_CODE: MOVE ZEROES TO WK-DT-A.
        ws.setWkDtA(0);
        //
        // COB_CODE: IF WPMO-DT-RICOR-PREC-NULL(INDPMO) = HIGH-VALUES
        //              MOVE WTGA-DT-DECOR(IX-TAB-TGA)  TO WK-DT-DA
        //           ELSE
        //              MOVE WPMO-DT-RICOR-PREC(INDPMO) TO WK-DT-DA
        //           END-IF
        if (Characters.EQ_HIGH.test(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getIndpmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrecNullFormatted())) {
            // COB_CODE: MOVE WTGA-DT-DECOR(IX-TAB-TGA)  TO WK-DT-DA
            ws.setWkDtDa(TruncAbs.toInt(wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaDtDecor(), 8));
        }
        else {
            // COB_CODE: MOVE WPMO-DT-RICOR-PREC(INDPMO) TO WK-DT-DA
            ws.setWkDtDa(TruncAbs.toInt(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getIndpmo()).getLccvpmo1().getDati().getWpmoDtRicorPrec().getWpmoDtRicorPrec(), 8));
        }
        //
        // COB_CODE: MOVE WPMO-DT-RICOR-SUCC(INDPMO) TO WK-DT-A.
        ws.setWkDtA(TruncAbs.toInt(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getIndpmo()).getLccvpmo1().getDati().getWpmoDtRicorSucc().getWpmoDtRicorSucc(), 8));
    }

    /**Original name: S10220-CHIAMA-LCCS0003<br>
	 * <pre>----------------------------------------------------------------*
	 *                     CALL AL MODULO LCCS0003                     *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10220ChiamaLccs0003() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        // COB_CODE: MOVE 'S10220-CHIAMA-LCCS0003'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10220-CHIAMA-LCCS0003");
        //
        //
        // COB_CODE:      CALL LCCS0003         USING IO-A2K-LCCC0003
        //                                            IN-RCODE
        //           *
        //                ON EXCEPTION
        //           *
        //                         THRU GESTIONE-ERR-SIST-EX
        //           *
        //                END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Loas0870Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            //
            // COB_CODE: MOVE 'LCCS0003'
            //             TO IEAI9901-COD-SERVIZIO-BE
            ws.getIeai9901Area().setCodServizioBe("LCCS0003");
            // COB_CODE: MOVE 'ERRORE PROGRAMMA LCCS0003'
            //             TO CALL-DESC
            ws.getIdsv0002().setCallDesc("ERRORE PROGRAMMA LCCS0003");
            // COB_CODE: MOVE 'S10220-CHIAMA-LCCS0003'
            //             TO WK-LABEL-ERR
            ws.getWkGestioneMsgErr().setLabelErr("S10220-CHIAMA-LCCS0003");
            //
            // COB_CODE: PERFORM GESTIONE-ERR-SIST
            //              THRU GESTIONE-ERR-SIST-EX
            gestioneErrSist();
            //
        }
        //
        // COB_CODE: IF IN-RCODE NOT = '00'
        //                 THRU GESTIONE-ERR-STD-EX
        //           END-IF.
        if (!ws.getInRcodeFormatted().equals("00")) {
            // COB_CODE: MOVE 'ERRORE SOTTRAZIONE TRE MESI'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE SOTTRAZIONE TRE MESI");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
        }
    }

    /**Original name: S10230-RICERCA-TITOLO<br>
	 * <pre>----------------------------------------------------------------*
	 *                      RICERCA TITOLI CONTABILI                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10230RicercaTitolo() {
        // COB_CODE: MOVE 'S10230-RICERCA-TITOLO'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10230-RICERCA-TITOLO");
        //
        // COB_CODE: code not available
        ws.setIntRegister1(((short)1));
        ws.getIxIndici().setIxTgaW870(Trunc.toShort(ws.getIntRegister1() + ws.getIxIndici().getIxTgaW870(), 4));
        w870AreaLoas0870.setW870TgaNumMaxEle(Trunc.toShort(ws.getIntRegister1() + w870AreaLoas0870.getW870TgaNumMaxEle(), 4));
        //
        // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
        //             TO W870-ID-TRCH-DI-GAR(IX-TGA-W870)
        //                WK-ID-TRCH-DI-GAR.
        w870AreaLoas0870.getW870TabTit().setIdTrchDiGar(ws.getIxIndici().getIxTgaW870(), wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        ws.setWkIdTrchDiGar(wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        //
        // -->> Indicatore di fine fetch
        //
        // COB_CODE: SET WK-FINE-FETCH-NO
        //             TO TRUE.
        ws.getWkFineFetch().setNo();
        //
        // -->> Flag Ricerca Titolo Contabile Incassato
        //
        // COB_CODE: SET WK-TIT-NO
        //               TO TRUE.
        ws.getWkTitTrovato().setNo();
        //
        // COB_CODE: MOVE 'IN'
        //             TO LDBV7141-TP-STA-TIT1.
        ws.getLdbv7141().setTpStaTit1("IN");
        //
        // COB_CODE: MOVE 'EM'
        //             TO LDBV7141-TP-STA-TIT2.
        ws.getLdbv7141().setTpStaTit2("EM");
        // COB_CODE: SET IDSI0011-FETCH-FIRST
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        //
        // COB_CODE: PERFORM RICERCA-GAR
        //              THRU RICERCA-GAR-EX.
        ricercaGar();
        //
        // COB_CODE: PERFORM RICERCA-PMO
        //              THRU RICERCA-PMO-EX.
        ricercaPmo();
        //
        // COB_CODE:      IF  WK-GRZ-SI
        //                AND (WS-PREMIO-UNICO OR WS-PREMIO-RICORRENTE)
        //           *    Per le tariffe a PU o PUR i titoli considerati sono
        //           *    con data effetto ompresa tra la decorrenza di polizza
        //           *    e data effetto rivalutazione
        //                       THRU S10215-DET-DATE-PUR-EX
        //                ELSE
        //           *    Sia per le tariffe a premio annuo costante
        //           *    Sia per le tariffe a premio annuo rivalutabile
        //           *    sono selezionati tutti i titoli con effetto 3 mesi prima
        //           *    della data rivalutazione e 3 mesi dopo la data ultima ri
        //           *    valutazione
        //                       THRU S10210-DET-DATA-A-EX
        //                END-IF.
        if (ws.getWkGrzTrovata().isSi() && (ws.getWsTpPerPremio().isUnico() || ws.getWsTpPerPremio().isRicorrente())) {
            //    Per le tariffe a PU o PUR i titoli considerati sono
            //    con data effetto ompresa tra la decorrenza di polizza
            //    e data effetto rivalutazione
            // COB_CODE: PERFORM S10215-DET-DATE-PUR
            //              THRU S10215-DET-DATE-PUR-EX
            s10215DetDatePur();
        }
        else {
            //    Sia per le tariffe a premio annuo costante
            //    Sia per le tariffe a premio annuo rivalutabile
            //    sono selezionati tutti i titoli con effetto 3 mesi prima
            //    della data rivalutazione e 3 mesi dopo la data ultima ri
            //    valutazione
            // COB_CODE: PERFORM S10209-DET-DATA-INI-CALC
            //              THRU S10209-EX
            s10209DetDataIniCalc();
            // COB_CODE: PERFORM S10211-DET-DATA-DA
            //              THRU S10211-DET-DATA-DA-EX
            s10211DetDataDa();
            // COB_CODE: PERFORM S10210-DET-DATA-A
            //              THRU S10210-DET-DATA-A-EX
            s10210DetDataA();
        }
        //
        // COB_CODE: PERFORM S10240-IMPOSTA-TIT-CONT
        //              THRU S10240-IMPOSTA-TIT-CONT-EX
        s10240ImpostaTitCont();
        //
        // COB_CODE: PERFORM S10250-LEGGI-TIT-CONT
        //              THRU S10250-LEGGI-TIT-CONT-EX
        //             UNTIL WK-FINE-FETCH-SI
        //                OR IDSV0001-ESITO-KO
        while (!(ws.getWkFineFetch().isSi() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            s10250LeggiTitCont();
        }
        // COB_CODE: IF IDSV0001-ESITO-OK
        //              END-IF
        //           END-IF.
        if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
            // COB_CODE: IF W870-TIT-NUM-MAX-ELE(IX-TGA-W870) = 0
            //                    W870-TGA-NUM-MAX-ELE - 1
            //           END-IF
            if (w870AreaLoas0870.getW870TabTit().getTitNumMaxEle(ws.getIxIndici().getIxTgaW870()) == 0) {
                // COB_CODE: MOVE ZEROES TO W870-ID-TRCH-DI-GAR(IX-TGA-W870)
                w870AreaLoas0870.getW870TabTit().setIdTrchDiGar(ws.getIxIndici().getIxTgaW870(), 0);
                // COB_CODE: COMPUTE IX-TGA-W870   = IX-TGA-W870 - 1
                ws.getIxIndici().setIxTgaW870(Trunc.toShort(ws.getIxIndici().getIxTgaW870() - 1, 4));
                // COB_CODE: COMPUTE W870-TGA-NUM-MAX-ELE =
                //                   W870-TGA-NUM-MAX-ELE - 1
                w870AreaLoas0870.setW870TgaNumMaxEle(Trunc.toShort(w870AreaLoas0870.getW870TgaNumMaxEle() - 1, 4));
            }
        }
    }

    /**Original name: RICERCA-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *     RICERCA GARANZIA
	 * ----------------------------------------------------------------*</pre>*/
    private void ricercaGar() {
        // COB_CODE: SET WK-GRZ-NO       TO TRUE
        ws.getWkGrzTrovata().setNo();
        // COB_CODE: PERFORM VARYING IX-GRZ FROM 1 BY 1
        //                     UNTIL IX-GRZ > WGRZ-ELE-GAR-MAX
        //                        OR WK-GRZ-SI
        //              END-IF
        //           END-PERFORM.
        ws.getIxIndici().setIxGrz(((short)1));
        while (!(ws.getIxIndici().getIxGrz() > wgrzAreaGaranzia.getEleGarMax() || ws.getWkGrzTrovata().isSi())) {
            // COB_CODE: IF WGRZ-ID-GAR(IX-GRZ) = WTGA-ID-GAR(IX-TAB-TGA)
            //                TO WS-TP-PER-PREMIO
            //           END-IF
            if (wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getIxGrz()).getLccvgrz1().getDati().getWgrzIdGar() == wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIdGar()) {
                // COB_CODE: SET WK-GRZ-SI               TO TRUE
                ws.getWkGrzTrovata().setSi();
                // COB_CODE: MOVE WGRZ-TP-PER-PRE(IX-GRZ)
                //             TO WS-TP-PER-PREMIO
                ws.getWsTpPerPremio().setWsTpPerPremioFormatted(wgrzAreaGaranzia.getTabGar(ws.getIxIndici().getIxGrz()).getLccvgrz1().getDati().getWgrzTpPerPreFormatted());
            }
            ws.getIxIndici().setIxGrz(Trunc.toShort(ws.getIxIndici().getIxGrz() + 1, 4));
        }
        // COB_CODE: SUBTRACT 1 FROM IX-GRZ.
        ws.getIxIndici().setIxGrz(Trunc.toShort(ws.getIxIndici().getIxGrz() - 1, 4));
    }

    /**Original name: RICERCA-PMO<br>
	 * <pre>----------------------------------------------------------------*
	 *  RICERCA PARAMETRO MOVIMENTO LEGATA ALLA GARANZIA DELLA TRANCHE
	 * ----------------------------------------------------------------*</pre>*/
    private void ricercaPmo() {
        // COB_CODE: SET WK-PMO-NO       TO TRUE
        ws.getWkPmoTrovata().setNo();
        // COB_CODE: PERFORM VARYING IX-PMO FROM 1 BY 1
        //                     UNTIL IX-PMO > WPMO-ELE-PARAM-MOV-MAX
        //                        OR WK-PMO-SI
        //              END-IF
        //           END-PERFORM.
        ws.getIxIndici().setIxPmo(((short)1));
        while (!(ws.getIxIndici().getIxPmo() > wpmoAreaParamMovi.getEleParamMovMax() || ws.getWkPmoTrovata().isSi())) {
            // COB_CODE:  IF WPMO-ID-OGG(IX-PMO) = WTGA-ID-GAR(IX-TAB-TGA)
            //           AND WPMO-TP-OGG(IX-PMO) = 'GA'
            //               MOVE IX-PMO   TO INDPMO
            //            END-IF
            if (wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getIxPmo()).getLccvpmo1().getDati().getWpmoIdOgg() == wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIdGar() && Conditions.eq(wpmoAreaParamMovi.getTabParamMov(ws.getIxIndici().getIxPmo()).getLccvpmo1().getDati().getWpmoTpOgg(), "GA")) {
                // COB_CODE: SET WK-PMO-SI TO TRUE
                ws.getWkPmoTrovata().setSi();
                // COB_CODE: MOVE IX-PMO   TO INDPMO
                ws.getIxIndici().setIndpmo(ws.getIxIndici().getIxPmo());
            }
            ws.getIxIndici().setIxPmo(Trunc.toShort(ws.getIxIndici().getIxPmo() + 1, 4));
        }
        // COB_CODE: SUBTRACT 1 FROM IX-PMO.
        ws.getIxIndici().setIxPmo(Trunc.toShort(ws.getIxIndici().getIxPmo() - 1, 4));
    }

    /**Original name: S10240-IMPOSTA-TIT-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *      VALORIZZAZIONI PER LA LETTURA CON WHERE CONDITION AD HOC   *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10240ImpostaTitCont() {
        // COB_CODE: MOVE 'S10240-IMPOSTA-TIT-CONT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10240-IMPOSTA-TIT-CONT");
        //
        // --> Vengono Cercati I Titoli Con Effetto Compreso Tra L'Ultima
        // --> Ricorrenza Elaborata e L'Effetto di Elaborazione
        //
        // COB_CODE: MOVE WK-DT-DA
        //             TO LDBV7141-DT-DA.
        ws.getLdbv7141().setDtDa(ws.getWkDtDa());
        // COB_CODE: MOVE WK-DT-A
        //             TO LDBV7141-DT-A.
        ws.getLdbv7141().setDtA(ws.getWkDtA());
        //
        // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA)
        //             TO LDBV7141-ID-OGG.
        ws.getLdbv7141().setIdOgg(wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        // COB_CODE: MOVE 'TG'
        //             TO LDBV7141-TP-OGG.
        ws.getLdbv7141().setTpOgg("TG");
        // COB_CODE: MOVE 'PR'
        //             TO LDBV7141-TP-TIT.
        ws.getLdbv7141().setTpTit("PR");
        //
        //  --> La data effetto viene sempre valorizzata
        //
        // COB_CODE: MOVE WS-DT-INFINITO-1-N
        //             TO IDSI0011-DATA-INIZIO-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(ws.getIdsv0015().getDtInfinito1N());
        // COB_CODE: MOVE WS-TS-INFINITO-1-N
        //             TO IDSI0011-DATA-COMPETENZA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(ws.getIdsv0015().getTsInfinito1N());
        // COB_CODE: MOVE ZERO
        //             TO IDSI0011-DATA-FINE-EFFETTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        //
        //  --> Nome tabella fisica db
        //
        // COB_CODE: MOVE 'LDBS7140'
        //             TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("LDBS7140");
        //
        //  --> Dclgen tabella
        //
        // COB_CODE: MOVE SPACES
        //             TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati("");
        // COB_CODE: MOVE LDBV7141
        //             TO IDSI0011-BUFFER-WHERE-COND.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferWhereCond(ws.getLdbv7141().getLdbv7141Formatted());
        //
        //  --> Tipo operazione
        //
        // COB_CODE: SET IDSI0011-WHERE-CONDITION
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setWhereCondition();
        //
        //  --> Modalita di accesso
        //
        //    SET IDSI0011-TRATT-DEFAULT
        // COB_CODE: SET IDSI0011-TRATT-X-COMPETENZA
        //             TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattXCompetenza();
        //
        //  --> Inizializzazione Return Code
        //
        // COB_CODE: SET IDSO0011-SUCCESSFUL-RC
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET IDSO0011-SUCCESSFUL-SQL
        //             TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
    }

    /**Original name: S10250-LEGGI-TIT-CONT<br>
	 * <pre>----------------------------------------------------------------*
	 *             LETTURA DELLE TABELLA TRANCHE DI GARANZIA           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s10250LeggiTitCont() {
        // COB_CODE: MOVE 'S10250-LEGGI-TIT-CONT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10250-LEGGI-TIT-CONT");
        //
        // COB_CODE: PERFORM CALL-DISPATCHER
        //              THRU CALL-DISPATCHER-EX.
        callDispatcher();
        //
        // COB_CODE:      IF IDSO0011-SUCCESSFUL-RC
        //           *
        //                   END-EVALUATE
        //           *
        //                ELSE
        //           *
        //           *  --> Errore dispatcher
        //           *
        //                      THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
            //
            //
            // COB_CODE:         EVALUATE TRUE
            //           *
            //                       WHEN IDSO0011-NOT-FOUND
            //           *
            //           *  --> Chiave non trovata, fine occorrenze fetch
            //           *
            //                              TO TRUE
            //           *
            //                       WHEN IDSO0011-SUCCESSFUL-SQL
            //           *
            //           *  --> Operazione eseguita correttamente
            //           *
            //                            END-IF
            //           *
            //                       WHEN OTHER
            //           *
            //           *  --> Errore di accesso al db
            //                               THRU GESTIONE-ERR-STD-EX
            //           *
            //                   END-EVALUATE
            switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                case Idso0011SqlcodeSigned.NOT_FOUND://
                    //  --> Chiave non trovata, fine occorrenze fetch
                    //
                    // COB_CODE: SET WK-FINE-FETCH-SI
                    //             TO TRUE
                    ws.getWkFineFetch().setSi();
                    //
                    break;

                case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://
                    //  --> Operazione eseguita correttamente
                    //
                    // COB_CODE: SET WK-TIT-SI
                    //             TO TRUE
                    ws.getWkTitTrovato().setSi();
                    //
                    // COB_CODE: SET IDSI0011-FETCH-NEXT
                    //             TO TRUE
                    ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchNext();
                    //
                    // COB_CODE: MOVE W870-TIT-NUM-MAX-ELE(IX-TGA-W870)
                    //             TO IX-TIT-W870
                    ws.getIxIndici().setIxTitW870(w870AreaLoas0870.getW870TabTit().getTitNumMaxEle(ws.getIxIndici().getIxTgaW870()));
                    //
                    // COB_CODE: ADD 1
                    //             TO IX-TIT-W870
                    ws.getIxIndici().setIxTitW870(Trunc.toShort(1 + ws.getIxIndici().getIxTitW870(), 4));
                    //
                    // COB_CODE: MOVE IX-TIT-W870
                    //             TO W870-TIT-NUM-MAX-ELE(IX-TGA-W870)
                    w870AreaLoas0870.getW870TabTit().setTitNumMaxEle(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870());
                    //
                    // COB_CODE:                  IF IX-TIT-W870 > WMAX-TAB-TIT
                    //           *
                    //                                  THRU GESTIONE-ERR-STD-EX
                    //           *
                    //                            ELSE
                    //           *
                    //                               END-IF
                    //           *
                    //                            END-IF
                    if (ws.getIxIndici().getIxTitW870() > ws.getWmaxTabTit()) {
                        //
                        // COB_CODE: MOVE 'OVERFLOW CARICAMENTO TITOLI CONTABILI'
                        //             TO WK-STRING
                        ws.getWkGestioneMsgErr().setStringFld("OVERFLOW CARICAMENTO TITOLI CONTABILI");
                        // COB_CODE: MOVE '005059'
                        //             TO WK-COD-ERR
                        ws.getWkGestioneMsgErr().setCodErr("005059");
                        // COB_CODE: PERFORM GESTIONE-ERR-STD
                        //              THRU GESTIONE-ERR-STD-EX
                        gestioneErrStd();
                        //
                    }
                    else {
                        //
                        // COB_CODE: MOVE IDSO0011-BUFFER-DATI
                        //             TO TIT-CONT
                        ws.getTitCont().setTitContFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                        //
                        // COB_CODE: PERFORM S10260-CARICA-TAB-W870
                        //              THRU S10260-CARICA-TAB-W870-EX
                        s10260CaricaTabW870();
                        // COB_CODE: IF  W870-TP-STAT-TIT
                        //              (IX-TGA-W870 , IX-TIT-W870) = 'IN'
                        //              MOVE 'S' TO WS-FLAG-INC-TGA(IX-TAB-FLAGS)
                        //           END-IF
                        if (Conditions.eq(w870AreaLoas0870.getW870TabTit().getTpStatTit(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870()), "IN")) {
                            // COB_CODE: MOVE 'S' TO WS-FLAG-INC-TGA(IX-TAB-FLAGS)
                            ws.getWsFg(ws.getIxIndici().getIxTabFlags()).setWsFlagIncTgaFormatted("S");
                        }
                        //
                        // COB_CODE: IF WS-PREMIO-ANNUALE
                        //                 THRU S10500-CTRL-TIT-EX
                        //           END-IF
                        if (ws.getWsTpPerPremio().isAnnuale()) {
                            // COB_CODE: PERFORM S10500-CTRL-TIT
                            //              THRU S10500-CTRL-TIT-EX
                            s10500CtrlTit();
                        }
                        //
                    }
                    //
                    break;

                default://
                    //  --> Errore di accesso al db
                    // COB_CODE: MOVE 'ERRORE LETTURA TITOLO CONTABILE'
                    //             TO WK-STRING
                    ws.getWkGestioneMsgErr().setStringFld("ERRORE LETTURA TITOLO CONTABILE");
                    // COB_CODE: MOVE '005016'
                    //             TO WK-COD-ERR
                    ws.getWkGestioneMsgErr().setCodErr("005016");
                    // COB_CODE: PERFORM GESTIONE-ERR-STD
                    //              THRU GESTIONE-ERR-STD-EX
                    gestioneErrStd();
                    //
                    break;
            }
            //
        }
        else {
            //
            //  --> Errore dispatcher
            //
            // COB_CODE: MOVE 'ERRORE DISPATCHER LETTURA TITOLO CONTABILE'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("ERRORE DISPATCHER LETTURA TITOLO CONTABILE");
            // COB_CODE: MOVE '005016'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("005016");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10260-CARICA-TAB-W870<br>
	 * <pre>----------------------------------------------------------------*
	 *   CARICAMENTO TABELLA WS
	 * ----------------------------------------------------------------*</pre>*/
    private void s10260CaricaTabW870() {
        // COB_CODE: MOVE 'S10260-CARICA-TAB-W870'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10260-CARICA-TAB-W870");
        //
        // COB_CODE: MOVE TIT-ID-TIT-CONT
        //             TO W870-ID-TIT-CONT(IX-TGA-W870 , IX-TIT-W870).
        w870AreaLoas0870.getW870TabTit().setIdTitCont(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870(), ws.getTitCont().getTitIdTitCont());
        //
        // COB_CODE:      IF TIT-DT-VLT-NULL = HIGH-VALUE OR LOW-VALUE
        //           *
        //                     TO W870-DT-VLT-NULL(IX-TGA-W870 , IX-TIT-W870)
        //           *
        //                ELSE
        //           *
        //                     TO W870-DT-VLT(IX-TGA-W870 , IX-TIT-W870)
        //           *
        //                END-IF.
        if (Characters.EQ_HIGH.test(ws.getTitCont().getTitDtVlt().getTitDtVltNullFormatted()) || Characters.EQ_LOW.test(ws.getTitCont().getTitDtVlt().getTitDtVltNullFormatted())) {
            //
            // COB_CODE: MOVE TIT-DT-VLT-NULL
            //             TO W870-DT-VLT-NULL(IX-TGA-W870 , IX-TIT-W870)
            w870AreaLoas0870.getW870TabTit().setDtVltNull(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870(), ws.getTitCont().getTitDtVlt().getTitDtVltNull());
            //
        }
        else {
            //
            // COB_CODE: MOVE TIT-DT-VLT
            //             TO W870-DT-VLT(IX-TGA-W870 , IX-TIT-W870)
            w870AreaLoas0870.getW870TabTit().setDtVlt(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870(), ws.getTitCont().getTitDtVlt().getTitDtVlt());
            //
        }
        //
        // COB_CODE: MOVE TIT-DT-INI-COP
        //             TO W870-DT-INI-EFF(IX-TGA-W870 , IX-TIT-W870).
        w870AreaLoas0870.getW870TabTit().setDtIniEff(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870(), ws.getTitCont().getTitDtIniCop().getTitDtIniCop());
        //
        // COB_CODE: MOVE TIT-TP-STAT-TIT
        //             TO W870-TP-STAT-TIT(IX-TGA-W870 , IX-TIT-W870).
        w870AreaLoas0870.getW870TabTit().setTpStatTit(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870(), ws.getTitCont().getTitTpStatTit());
    }

    /**Original name: S10500-CTRL-TIT<br>
	 * <pre>----------------------------------------------------------------*
	 *  CONTROLLO CHE PER OGNI TRANCHE VI SIA UN SOLO TITOLO CONTABILE
	 * ----------------------------------------------------------------*</pre>*/
    private void s10500CtrlTit() {
        // COB_CODE: MOVE 'S10500-CTRL-TIT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10500-CTRL-TIT");
        //
        // --> Viene gestito un errore bloccante se esistono
        // --> Titoli Cotabili non incassati per una Tranche
        // --> poichh essendo la rivalutazione per incasso
        // --> il processo dovr` essere eseguito non appena
        // --> arriva l'incasso o il titolo in emesso
        // --> viene stornato
        //
        // COB_CODE:      IF (W870-TP-STAT-TIT
        //                   (IX-TGA-W870 , IX-TIT-W870) = 'EM')
        //                 OR
        //                   (W870-DT-VLT-NULL
        //                   (IX-TGA-W870 , IX-TIT-W870) = HIGH-VALUES
        //                 OR
        //                    W870-DT-VLT
        //                   (IX-TGA-W870 , IX-TIT-W870) = ZEROES)
        //                       THRU GESTIONE-ERR-STD-EX
        //           *
        //                END-IF.
        if (Conditions.eq(w870AreaLoas0870.getW870TabTit().getTpStatTit(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870()), "EM") || Characters.EQ_HIGH.test(w870AreaLoas0870.getW870TabTit().getDtVltNullFormatted(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870())) || w870AreaLoas0870.getW870TabTit().getDtVlt(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870()) == 0) {
            // COB_CODE: MOVE 'TIT.CONT. NON INC., RIESEGUIRE RIVAL'
            //             TO WK-STRING
            ws.getWkGestioneMsgErr().setStringFld("TIT.CONT. NON INC., RIESEGUIRE RIVAL");
            // COB_CODE: MOVE '001114'
            //             TO WK-COD-ERR
            ws.getWkGestioneMsgErr().setCodErr("001114");
            // COB_CODE: PERFORM GESTIONE-ERR-STD
            //              THRU GESTIONE-ERR-STD-EX
            gestioneErrStd();
            //
        }
    }

    /**Original name: S10600-DETERMINA-TP-RIVAL<br>
	 * <pre>----------------------------------------------------------------*
	 *  GESTIONE DEL TIPO DI RIVALUTAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s10600DeterminaTpRival() {
        // COB_CODE: MOVE 'S10600-DETERMINA-TP-RIVAL'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10600-DETERMINA-TP-RIVAL");
        // --> Tipi Rivalutazione :
        //
        //     -> Piena
        //     -> Parziale
        //     -> Da Rieseguire
        //
        // COB_CODE:      PERFORM VARYING IX-TAB-TGA FROM 1 BY 1
        //                  UNTIL IX-TAB-TGA > WTGA-ELE-TRAN-MAX
        //                     OR IDSV0001-ESITO-KO
        //           *
        //                       END-PERFORM
        //                END-PERFORM.
        ws.getIxIndici().setIxTabTga(((short)1));
        while (!(ws.getIxIndici().getIxTabTga() > wtgaAreaTranche.getEleTranMax() || areaIdsv0001.getEsito().isIdsv0001EsitoKo())) {
            //
            // COB_CODE: MOVE 'CO'          TO WTGA-TP-RIVAL(IX-TAB-TGA)
            wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpRival("CO");
            // COB_CODE: MOVE IX-TAB-TGA
            //             TO IX-TAB-FLAGS
            ws.getIxIndici().setIxTabFlags(ws.getIxIndici().getIxTabTga());
            // COB_CODE: MOVE WS-FLAG-INC-TGA(IX-TAB-FLAGS)
            //             TO WK-TGA-INC
            ws.setWkTgaInc(ws.getWsFg(ws.getIxIndici().getIxTabFlags()).getWsFlagIncTga());
            //
            // COB_CODE:             PERFORM VARYING IX-TGA-W870 FROM 1 BY 1
            //                          UNTIL IX-TGA-W870 > W870-TGA-NUM-MAX-ELE
            //           *
            //                            END-IF
            //           *
            //                       END-PERFORM
            ws.getIxIndici().setIxTgaW870(((short)1));
            while (!(ws.getIxIndici().getIxTgaW870() > w870AreaLoas0870.getW870TgaNumMaxEle())) {
                //
                // COB_CODE:                  IF WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) =
                //                               W870-ID-TRCH-DI-GAR(IX-TGA-W870)
                //           *
                //                                  THRU S10610-ALLINEA-AREA-TIT-EX
                //           *
                //                            END-IF
                if (wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar() == w870AreaLoas0870.getW870TabTit().getIdTrchDiGar(ws.getIxIndici().getIxTgaW870())) {
                    //
                    // COB_CODE: PERFORM S10610-ALLINEA-AREA-TIT
                    //              THRU S10610-ALLINEA-AREA-TIT-EX
                    s10610AllineaAreaTit();
                    //
                }
                //
                ws.getIxIndici().setIxTgaW870(Trunc.toShort(ws.getIxIndici().getIxTgaW870() + 1, 4));
            }
            ws.getIxIndici().setIxTabTga(Trunc.toShort(ws.getIxIndici().getIxTabTga() + 1, 4));
        }
    }

    /**Original name: S10610-ALLINEA-AREA-TIT<br>
	 * <pre>----------------------------------------------------------------*
	 *  VIENE ALLINEATA L'AREA DEL TITOLO
	 * ----------------------------------------------------------------*</pre>*/
    private void s10610AllineaAreaTit() {
        // COB_CODE: MOVE 'S10610-ALLINEA-AREA-TIT'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10610-ALLINEA-AREA-TIT");
        //
        // COB_CODE:      PERFORM VARYING IX-TIT-W870 FROM 1 BY 1
        //                  UNTIL IX-TIT-W870 > W870-TIT-NUM-MAX-ELE(IX-TGA-W870)
        //           *
        //                       THRU S10620-RIV-PIENA-PARZ-NULLA-EX
        //           *
        //                END-PERFORM.
        ws.getIxIndici().setIxTitW870(((short)1));
        while (!(ws.getIxIndici().getIxTitW870() > w870AreaLoas0870.getW870TabTit().getTitNumMaxEle(ws.getIxIndici().getIxTgaW870()))) {
            //
            // COB_CODE: PERFORM S10620-RIV-PIENA-PARZ-NULLA
            //              THRU S10620-RIV-PIENA-PARZ-NULLA-EX
            s10620RivPienaParzNulla();
            //
            ws.getIxIndici().setIxTitW870(Trunc.toShort(ws.getIxIndici().getIxTitW870() + 1, 4));
        }
    }

    /**Original name: S10620-RIV-PIENA-PARZ-NULLA<br>
	 * <pre>----------------------------------------------------------------*
	 *           DETERMINA IL TIPO DI RIVALUTAZIONE DA ESEGUIRE
	 *   PER I TITOLI INCASSATI : PIENA , PARZIALE , NULLL
	 * ----------------------------------------------------------------*</pre>*/
    private void s10620RivPienaParzNulla() {
        // COB_CODE: MOVE 'S10620-RIV-PIENA-PARZ-NULLA'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10620-RIV-PIENA-PARZ-NULLA");
        // COB_CODE: IF TIT-NON-INCASSATO
        //              MOVE 'NU' TO WTGA-TP-RIVAL(IX-TAB-TGA)
        //           ELSE
        //              END-IF
        //           END-IF.
        if (ws.isTitNonIncassato()) {
            // COB_CODE: MOVE 'NU' TO WTGA-TP-RIVAL(IX-TAB-TGA)
            wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpRival("NU");
        }
        else {
            // COB_CODE: PERFORM S10630-LEGGE-GG-RIT-PAG
            //              THRU S10630-EX
            s10630LeggeGgRitPag();
            // COB_CODE: IF DTC-NUM-GG-RITARDO-PAG-NULL = HIGH-VALUES
            //              MOVE ZERO TO DTC-NUM-GG-RITARDO-PAG
            //           END-IF
            if (Characters.EQ_HIGH.test(ws.getDettTitCont().getDtcNumGgRitardoPag().getDtcNumGgRitardoPagNullFormatted())) {
                // COB_CODE: MOVE ZERO TO DTC-NUM-GG-RITARDO-PAG
                ws.getDettTitCont().getDtcNumGgRitardoPag().setDtcNumGgRitardoPag(0);
            }
            // COB_CODE: IF  IDSV0001-ESITO-OK
            //           AND DETTAGLIO-TROVATO
            //           AND DTC-NUM-GG-RITARDO-PAG > ZERO
            //               MOVE 'PA' TO WTGA-TP-RIVAL(IX-TAB-TGA)
            //           END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk() && ws.isDettaglioTrovato() && ws.getDettTitCont().getDtcNumGgRitardoPag().getDtcNumGgRitardoPag() > 0) {
                // COB_CODE: MOVE 'PA' TO WTGA-TP-RIVAL(IX-TAB-TGA)
                wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().setWtgaTpRival("PA");
            }
        }
    }

    /**Original name: S10630-LEGGE-GG-RIT-PAG<br>*/
    private void s10630LeggeGgRitPag() {
        // COB_CODE: MOVE 'S10630-LEGGE-GG-RIT-PAG'
        //             TO WK-LABEL-ERR.
        ws.getWkGestioneMsgErr().setLabelErr("S10630-LEGGE-GG-RIT-PAG");
        // COB_CODE: SET IDSI0011-FETCH-FIRST       TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().setFetchFirst();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-RC    TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getReturnCode().setSuccessfulRc();
        // COB_CODE: SET  IDSO0011-SUCCESSFUL-SQL   TO TRUE.
        ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().setSuccessfulSql();
        // COB_CODE: SET INIT-CUR-DTC               TO TRUE.
        ws.setInitCurDtc();
        // COB_CODE: SET IDSI0011-TRATT-DEFAULT  TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setIdsi0011TrattDefault();
        // COB_CODE: MOVE WTGA-ID-TRCH-DI-GAR(IX-TAB-TGA) TO DTC-ID-OGG.
        ws.getDettTitCont().setDtcIdOgg(wtgaAreaTranche.getTabTran(ws.getIxIndici().getIxTabTga()).getLccvtga1().getDati().getWtgaIdTrchDiGar());
        // COB_CODE: SET TRANCHE                 TO TRUE.
        ws.getWsTpOgg().setTranche();
        // COB_CODE: MOVE WS-TP-OGG              TO DTC-TP-OGG.
        ws.getDettTitCont().setDtcTpOgg(ws.getWsTpOgg().getWsTpOgg());
        // COB_CODE: SET IDSI0011-ID-OGGETTO     TO TRUE.
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloOperazione().setIdsi0011IdOggetto();
        // COB_CODE: MOVE 'DETT-TIT-CONT'        TO IDSI0011-CODICE-STR-DATO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceStrDato("DETT-TIT-CONT");
        // COB_CODE: MOVE DETT-TIT-CONT          TO IDSI0011-BUFFER-DATI.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011BufferDati(ws.getDettTitCont().getDettTitContFormatted());
        // COB_CODE: MOVE ZERO TO IDSI0011-DATA-INIZIO-EFFETTO
        //                        IDSI0011-DATA-FINE-EFFETTO.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(0);
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(0);
        // COB_CODE: MOVE ZERO TO IDSI0011-DATA-COMPETENZA.
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(0);
        // COB_CODE: SET DETTAGLIO-NON-TROVATO TO TRUE.
        ws.setDettaglioNonTrovato();
        // COB_CODE: PERFORM UNTIL NOT IDSO0011-SUCCESSFUL-RC
        //                      OR IDSV0001-ESITO-KO
        //                      OR FINE-CUR-DTC
        //                      OR DETTAGLIO-TROVATO
        //                   END-IF
        //           END-PERFORM.
        while (!(!ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc() || areaIdsv0001.getEsito().isIdsv0001EsitoKo() || ws.isFineCurDtc() || ws.isDettaglioTrovato())) {
            // COB_CODE: PERFORM CALL-DISPATCHER
            //              THRU CALL-DISPATCHER-EX
            callDispatcher();
            // COB_CODE:              IF IDSV0001-ESITO-OK
            //                           END-IF
            //                        ELSE
            //           *--> GESTIRE ERRORE
            //                               THRU EX-S0300
            //                        END-IF
            if (areaIdsv0001.getEsito().isIdsv0001EsitoOk()) {
                // COB_CODE:                 IF IDSO0011-SUCCESSFUL-RC
                //                              END-EVALUATE
                //                           ELSE
                //           *--> GESTIRE ERRORE
                //                                   THRU EX-S0300
                //                           END-IF
                if (ws.getDispatcherVariables().getIdso0011Area().getReturnCode().isSuccessfulRc()) {
                    // COB_CODE:                    EVALUATE TRUE
                    //                                       WHEN IDSO0011-NOT-FOUND
                    //           *-->    NESSUN DATO ESTRATTO DALLA TABELLA
                    //                                       END-IF
                    //                              WHEN IDSO0011-SUCCESSFUL-SQL
                    //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    //                                  END-IF
                    //                              WHEN OTHER
                    //           *--->   ERRORE DI ACCESSO AL DB
                    //                                     THRU EX-S0300
                    //                              END-EVALUATE
                    switch (ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned()) {

                        case Idso0011SqlcodeSigned.NOT_FOUND://-->    NESSUN DATO ESTRATTO DALLA TABELLA
                            // COB_CODE: IF IDSI0011-FETCH-FIRST
                            //                THRU EX-S0300
                            //           END-IF
                            if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst()) {
                                // COB_CODE: MOVE 'IDBSDTC0'
                                //             TO IEAI9901-COD-SERVIZIO-BE
                                ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                                // COB_CODE: MOVE 'S10630-LEGGE-GG-RIT-PAG'
                                //             TO IEAI9901-LABEL-ERR
                                ws.getIeai9901Area().setLabelErr("S10630-LEGGE-GG-RIT-PAG");
                                // COB_CODE: MOVE '005166'
                                //             TO IEAI9901-COD-ERRORE
                                ws.getIeai9901Area().setCodErroreFormatted("005166");
                                // COB_CODE: MOVE SPACES
                                //             TO IEAI9901-PARAMETRI-ERR
                                //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                                // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                                //                  IDSO0011-RETURN-CODE     ';'
                                //                  IDSO0011-SQLCODE
                                //                  DELIMITED BY SIZE
                                //                  INTO IEAI9901-PARAMETRI-ERR
                                //           END-STRING
                                ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                                // COB_CODE: PERFORM
                                //                  S0300-RICERCA-GRAVITA-ERRORE
                                //             THRU EX-S0300
                                s0300RicercaGravitaErrore();
                            }
                            break;

                        case Idso0011SqlcodeSigned.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                            // COB_CODE: MOVE IDSO0011-BUFFER-DATI TO DETT-TIT-CONT
                            ws.getDettTitCont().setDettTitContFormatted(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011BufferDatiFormatted());
                            // COB_CODE: IF DTC-ID-TIT-CONT =
                            //            W870-ID-TIT-CONT(IX-TGA-W870, IX-TIT-W870)
                            //              SET DETTAGLIO-TROVATO TO TRUE
                            //           END-IF
                            if (ws.getDettTitCont().getDtcIdTitCont() == w870AreaLoas0870.getW870TabTit().getIdTitCont(ws.getIxIndici().getIxTgaW870(), ws.getIxIndici().getIxTitW870())) {
                                // COB_CODE: SET DETTAGLIO-TROVATO TO TRUE
                                ws.setDettaglioTrovato();
                            }
                            break;

                        default://--->   ERRORE DI ACCESSO AL DB
                            // COB_CODE: MOVE 'IDBSDTC0'
                            //             TO IEAI9901-COD-SERVIZIO-BE
                            ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                            // COB_CODE: MOVE 'S10630-LEGGE-GG-RIT-PAG'
                            //             TO IEAI9901-LABEL-ERR
                            ws.getIeai9901Area().setLabelErr("S10630-LEGGE-GG-RIT-PAG");
                            // COB_CODE: MOVE '005166'
                            //             TO IEAI9901-COD-ERRORE
                            ws.getIeai9901Area().setCodErroreFormatted("005166");
                            // COB_CODE: MOVE SPACES TO IEAI9901-PARAMETRI-ERR
                            //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                            // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                            //                  IDSO0011-RETURN-CODE     ';'
                            //                  IDSO0011-SQLCODE
                            //                  DELIMITED BY SIZE
                            //                  INTO IEAI9901-PARAMETRI-ERR
                            //           END-STRING
                            ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                            // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                            //              THRU EX-S0300
                            s0300RicercaGravitaErrore();
                            break;
                    }
                }
                else {
                    //--> GESTIRE ERRORE
                    // COB_CODE: MOVE 'IDBSDTC0'
                    //             TO IEAI9901-COD-SERVIZIO-BE
                    ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                    // COB_CODE: MOVE 'S10630-LEGGE-GG-RIT-PAG'
                    //             TO IEAI9901-LABEL-ERR
                    ws.getIeai9901Area().setLabelErr("S10630-LEGGE-GG-RIT-PAG");
                    // COB_CODE: MOVE '005166'
                    //             TO IEAI9901-COD-ERRORE
                    ws.getIeai9901Area().setCodErroreFormatted("005166");
                    // COB_CODE: MOVE SPACES
                    //             TO IEAI9901-PARAMETRI-ERR
                    //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                    // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                    //                  IDSO0011-RETURN-CODE     ';'
                    //                  IDSO0011-SQLCODE
                    //                  DELIMITED BY SIZE
                    //                  INTO IEAI9901-PARAMETRI-ERR
                    //           END-STRING
                    ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                    // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                    //              THRU EX-S0300
                    s0300RicercaGravitaErrore();
                }
            }
            else {
                //--> GESTIRE ERRORE
                // COB_CODE: MOVE 'IDBSDTC0'
                //             TO IEAI9901-COD-SERVIZIO-BE
                ws.getIeai9901Area().setCodServizioBe("IDBSDTC0");
                // COB_CODE: MOVE 'S10630-LEGGE-GG-RIT-PAG'
                //             TO IEAI9901-LABEL-ERR
                ws.getIeai9901Area().setLabelErr("S10630-LEGGE-GG-RIT-PAG");
                // COB_CODE: MOVE '005166'
                //             TO IEAI9901-COD-ERRORE
                ws.getIeai9901Area().setCodErroreFormatted("005166");
                // COB_CODE: MOVE SPACES
                //             TO IEAI9901-PARAMETRI-ERR
                //skipped translation for moving SPACES to IEAI9901-PARAMETRI-ERR; considered in STRING statement translation below
                // COB_CODE: STRING IDSI0011-CODICE-STR-DATO ';'
                //                  IDSO0011-RETURN-CODE     ';'
                //                  IDSO0011-SQLCODE
                //                  DELIMITED BY SIZE
                //                  INTO IEAI9901-PARAMETRI-ERR
                //           END-STRING
                ws.getIeai9901Area().setParametriErr(new StringBuffer(256).append(ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011CodiceStrDatoFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted()).append(";").append(ws.getDispatcherVariables().getIdso0011Area().getSqlcode()).toString());
                // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
                //              THRU EX-S0300
                s0300RicercaGravitaErrore();
            }
        }
    }

    /**Original name: GESTIONE-ERR-STD<br>
	 * <pre>----------------------------------------------------------------*
	 *                 GESTIONE STANDARD DELL'ERRORE                   *
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneErrStd() {
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE WK-PGM
        //             TO IEAI9901-COD-SERVIZIO-BE.
        ws.getIeai9901Area().setCodServizioBe(ws.getWkPgm());
        // COB_CODE: MOVE WK-LABEL-ERR
        //             TO IEAI9901-LABEL-ERR.
        ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
        // COB_CODE: MOVE WK-COD-ERR
        //             TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted(ws.getWkGestioneMsgErr().getCodErrFormatted());
        // COB_CODE: STRING WK-STRING ';'
        //                  IDSO0011-RETURN-CODE ';'
        //                  IDSO0011-SQLCODE
        //                  DELIMITED BY SIZE
        //                  INTO IEAI9901-PARAMETRI-ERR
        //           END-STRING.
        concatUtil = ConcatUtil.buildString(Ieai9901Area.Len.PARAMETRI_ERR, ws.getWkGestioneMsgErr().getStringFldFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getReturnCode().getIdso0021ReturnCodeFormatted(), ";", ws.getDispatcherVariables().getIdso0011Area().getSqlcodeAsString());
        ws.getIeai9901Area().setParametriErr(concatUtil.replaceInString(ws.getIeai9901Area().getParametriErrFormatted()));
        //
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    /**Original name: GESTIONE-ERR-SIST<br>
	 * <pre>----------------------------------------------------------------*
	 *            GESTIONE STANDARD DELL'ERRORE DI SISTEMA             *
	 * ----------------------------------------------------------------*</pre>*/
    private void gestioneErrSist() {
        // COB_CODE: MOVE WK-LABEL-ERR
        //             TO IEAI9901-LABEL-ERR
        ws.getIeai9901Area().setLabelErr(ws.getWkGestioneMsgErr().getLabelErr());
        //
        // COB_CODE: PERFORM S0290-ERRORE-DI-SISTEMA
        //              THRU EX-S0290.
        s0290ErroreDiSistema();
    }

    /**Original name: CALL-DISPATCHER<br>
	 * <pre>----------------------------------------------------------------*
	 *      ROUTINES CALL DISPATCHER                                   *
	 * ----------------------------------------------------------------*
	 * *****************************************************************
	 *    CALL DISPATCHER
	 * *****************************************************************</pre>*/
    private void callDispatcher() {
        Idss0010 idss0010 = null;
        // COB_CODE: MOVE IDSV0001-MODALITA-ESECUTIVA
        //                              TO IDSI0011-MODALITA-ESECUTIVA
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011ModalitaEsecutiva().setIdsi0011ModalitaEsecutiva(areaIdsv0001.getAreaComune().getModalitaEsecutiva().getModalitaEsecutiva());
        // COB_CODE: MOVE IDSV0001-COD-COMPAGNIA-ANIA
        //                              TO IDSI0011-CODICE-COMPAGNIA-ANIA
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodiceCompagniaAnia(areaIdsv0001.getAreaComune().getIdsv0001CodCompagniaAnia());
        // COB_CODE: MOVE IDSV0001-COD-MAIN-BATCH
        //                              TO IDSI0011-COD-MAIN-BATCH
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011CodMainBatch(areaIdsv0001.getAreaComune().getCodMainBatch());
        // COB_CODE: MOVE IDSV0001-TIPO-MOVIMENTO
        //                              TO IDSI0011-TIPO-MOVIMENTO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011TipoMovimentoFormatted(areaIdsv0001.getAreaComune().getIdsv0001TipoMovimentoFormatted());
        // COB_CODE: MOVE IDSV0001-SESSIONE
        //                              TO IDSI0011-SESSIONE
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Sessione(areaIdsv0001.getAreaComune().getSessione());
        // COB_CODE: MOVE IDSV0001-USER-NAME
        //                              TO IDSI0011-USER-NAME
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011UserName(areaIdsv0001.getAreaComune().getUserName());
        // COB_CODE: IF IDSI0011-DATA-INIZIO-EFFETTO = 0
        //              END-IF
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataInizioEffetto() == 0) {
            // COB_CODE: IF IDSV0001-LETT-ULT-IMMAGINE-SI
            //              END-IF
            //           ELSE
            //                TO IDSI0011-DATA-INIZIO-EFFETTO
            //           END-IF
            if (areaIdsv0001.getAreaComune().getLettUltImmagine().isIdsv0001LettUltImmagineSi()) {
                // COB_CODE: IF IDSI0011-SELECT
                //           OR IDSI0011-FETCH-FIRST
                //           OR IDSI0011-FETCH-NEXT
                //                TO IDSI0011-DATA-COMPETENZA
                //           ELSE
                //                TO IDSI0011-DATA-INIZIO-EFFETTO
                //           END-IF
                if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isSelect() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchFirst() || ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011Operazione().isFetchNext()) {
                    // COB_CODE: MOVE 99991230
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(99991230);
                    // COB_CODE: MOVE 999912304023595999
                    //             TO IDSI0011-DATA-COMPETENZA
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(999912304023595999L);
                }
                else {
                    // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                    //             TO IDSI0011-DATA-INIZIO-EFFETTO
                    ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
                }
            }
            else {
                // COB_CODE: MOVE IDSV0001-DATA-EFFETTO
                //             TO IDSI0011-DATA-INIZIO-EFFETTO
                ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataInizioEffetto(areaIdsv0001.getAreaComune().getIdsv0001DataEffetto());
            }
        }
        // COB_CODE: IF IDSI0011-DATA-FINE-EFFETTO = 0
        //              MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataFineEffetto() == 0) {
            // COB_CODE: MOVE 99991231   TO IDSI0011-DATA-FINE-EFFETTO
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataFineEffetto(99991231);
        }
        // COB_CODE: IF IDSI0011-DATA-COMPETENZA = 0
        //                              TO IDSI0011-DATA-COMPETENZA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011DataCompetenza() == 0) {
            // COB_CODE: MOVE IDSV0001-DATA-COMPETENZA
            //                           TO IDSI0011-DATA-COMPETENZA
            ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompetenza(areaIdsv0001.getAreaComune().getIdsv0001DataCompetenza());
        }
        // COB_CODE: MOVE IDSV0001-DATA-COMP-AGG-STOR
        //                              TO IDSI0011-DATA-COMP-AGG-STOR
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011DataCompAggStor(areaIdsv0001.getAreaComune().getIdsv0001DataCompAggStor());
        // COB_CODE: IF IDSI0011-TRATT-DEFAULT
        //                              TO IDSI0011-TRATTAMENTO-STORICITA
        //           END-IF
        if (ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().isTrattDefault()) {
            // COB_CODE: MOVE IDSV0001-TRATTAMENTO-STORICITA
            //                           TO IDSI0011-TRATTAMENTO-STORICITA
            ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011TrattamentoStoricita().setTrattamentoStoricita(areaIdsv0001.getAreaComune().getTrattamentoStoricita().getTrattamentoStoricita());
        }
        // COB_CODE: MOVE IDSV0001-FORMATO-DATA-DB
        //                              TO IDSI0011-FORMATO-DATA-DB
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011FormatoDataDb().setIdsi0011FormatoDataDb(areaIdsv0001.getAreaComune().getFormatoDataDb().getFormatoDataDb());
        // COB_CODE: MOVE IDSV0001-LIVELLO-DEBUG
        //                              TO IDSI0011-LIVELLO-DEBUG
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011LivelloDebug().setIdsi0011LivelloDebugFormatted(areaIdsv0001.getAreaComune().getLivelloDebug().getIdsi0011LivelloDebugFormatted());
        // COB_CODE: MOVE 0             TO IDSI0011-ID-MOVI-ANNULLATO
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011IdMoviAnnullato(0);
        // COB_CODE: SET IDSI0011-PTF-NEWLIFE          TO TRUE
        ws.getDispatcherVariables().getIdsi0011Area().getIdsi0011IdentitaChiamante().setPtfNewlife();
        // COB_CODE: SET IDSV0012-STATUS-SHARED-MEM-KO TO TRUE
        ws.getIdsv00122().getStatusSharedMemory().setKo();
        // COB_CODE: MOVE 'IDSS0010'             TO IDSI0011-PGM
        ws.getDispatcherVariables().getIdsi0011Area().setIdsi0011Pgm("IDSS0010");
        //     SET WS-ADDRESS-DIS TO ADDRESS OF IN-OUT-IDSS0010.
        // COB_CODE: CALL IDSI0011-PGM           USING IDSI0011-AREA
        //                                             IDSO0011-AREA.
        idss0010 = Idss0010.getInstance();
        idss0010.run(ws.getDispatcherVariables(), ws.getDispatcherVariables().getIdso0011Area());
        // COB_CODE: MOVE IDSO0011-SQLCODE-SIGNED
        //                                  TO IDSO0011-SQLCODE.
        ws.getDispatcherVariables().getIdso0011Area().setSqlcode(ws.getDispatcherVariables().getIdso0011Area().getSqlcodeSigned().getSqlcodeSigned());
    }

    /**Original name: S0300-RICERCA-GRAVITA-ERRORE<br>
	 * <pre>----------------------------------------------------------------*
	 *   ROUTINES GESTIONE ERRORI                                      *
	 * ----------------------------------------------------------------*
	 * MUOVO L'AREA DEL SERVIZIO NELL'AREA DATI DELL'AREA DI CONTESTO.
	 * ----->VALORIZZO I CAMPI DELLA IDSV0001</pre>*/
    private void s0300RicercaGravitaErrore() {
        Ieas9900 ieas9900 = null;
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR       TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE  'IEAS9900'              TO CALL-PGM
        ws.getIdsv0002().setCallPgm("IEAS9900");
        // COB_CODE: CALL CALL-PGM USING IDSV0001-AREA-CONTESTO
        //                               IEAI9901-AREA
        //                               IEAO9901-AREA
        //             ON EXCEPTION
        //                   THRU EX-S0310-ERRORE-FATALE
        //           END-CALL.
        try {
            ieas9900 = Ieas9900.getInstance();
            ieas9900.run(areaIdsv0001, ws.getIeai9901Area(), ws.getIeao9901Area());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
            areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
            // COB_CODE: PERFORM S0310-ERRORE-FATALE
            //              THRU EX-S0310-ERRORE-FATALE
            s0310ErroreFatale();
        }
        //SE LA CHIAMATA AL SERVIZIO HA ESITO POSITIVO (NESSUN ERRORE DI
        //SISTEMA, VALORIZZO L'AREA DI OUTPUT.
        //INCREMENTO L'INDICE DEGLI ERRORI
        // COB_CODE: ADD 1 TO IDSV0001-MAX-ELE-ERRORI
        areaIdsv0001.setMaxEleErrori(Trunc.toShort(1 + areaIdsv0001.getMaxEleErrori(), 4));
        //SE L'INDICE E' MINORE DI 10 ...
        // COB_CODE:      IF IDSV0001-MAX-ELE-ERRORI NOT GREATER 10
        //           *SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
        //                   END-IF
        //                ELSE
        //           *IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        //                       TO IDSV0001-DESC-ERRORE-ESTESA
        //                END-IF.
        if (areaIdsv0001.getMaxEleErrori() <= 10) {
            //SE L'ESECUZIONE DEL MODULO DI RICERCA GRAVITA E' OK ...
            // COB_CODE: IF IEAO9901-COD-ERRORE-990 = ZEROES
            //                      EX-S0300-IMPOSTA-ERRORE
            //           ELSE
            //                   IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
            //           END-IF
            if (Characters.EQ_ZERO.test(ws.getIeao9901Area().getCodErrore990Formatted())) {
                // COB_CODE: PERFORM S0300-IMPOSTA-ERRORE THRU
                //                   EX-S0300-IMPOSTA-ERRORE
                s0300ImpostaErrore();
            }
            else {
                // COB_CODE: MOVE 'IEAS9900'
                //             TO IDSV0001-COD-SERVIZIO-BE
                areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
                // COB_CODE: MOVE IEAO9901-LABEL-ERR-990
                //             TO IDSV0001-LABEL-ERR
                areaIdsv0001.getLogErrore().setLabelErr(ws.getIeao9901Area().getLabelErr990());
                // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
                //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
                // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
                areaIdsv0001.getEsito().setIdsv0001EsitoKo();
                // IMPOSTO IL CODICE ERRORE GESTITO ALL'INTERNO DEL PROGRAMMA
                // COB_CODE: MOVE IEAO9901-COD-ERRORE-990
                //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeao9901Area().getCodErrore990Formatted());
                // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
                //             TO IDSV0001-DESC-ERRORE-ESTESA
                //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI)
                areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
                areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
            }
        }
        else {
            //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
            // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
            //             TO IDSV0001-LABEL-ERR
            areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
            // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 'IEAS9900'
            //             TO IDSV0001-COD-SERVIZIO-BE
            areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
            // COB_CODE: MOVE 'OVERFLOW IMPAGINAZIONE ERRORI'
            //             TO IDSV0001-DESC-ERRORE-ESTESA
            areaIdsv0001.getLogErrore().setDescErroreEstesa("OVERFLOW IMPAGINAZIONE ERRORI");
        }
    }

    /**Original name: S0300-IMPOSTA-ERRORE<br>
	 * <pre>  se la gravit— di tutti gli errori dell'elaborazione
	 *   h minore di zero ( ad esempio -3) vuol dire che il return-code
	 *   dell'elaborazione complessiva deve essere abbassato da '08' a
	 *   '04'. Per fare cir utilizzo il flag IDSV0001-FORZ-RC-04
	 * IMPOSTO LA GRAVITA'</pre>*/
    private void s0300ImpostaErrore() {
        // COB_CODE: MOVE IEAO9901-LIV-GRAVITA
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(TruncAbs.toShort(ws.getIeao9901Area().getLivGravita(), 1));
        // COB_CODE: EVALUATE TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = -3
        //                       IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        //             WHEN  IEAO9901-LIV-GRAVITA = 0 OR 1 OR 2
        //                   SET IDSV0001-FORZ-RC-04-NO  TO TRUE
        //             WHEN  IEAO9901-LIV-GRAVITA = 3 OR 4
        //                   SET IDSV0001-ESITO-KO       TO TRUE
        //           END-EVALUATE
        if (ws.getIeao9901Area().getLivGravita() == -3) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-YES TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04Yes();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
            // COB_CODE: MOVE 2  TO
            //               IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
            areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)2));
        }
        else if (ws.getIeao9901Area().getLivGravita() == 0 || ws.getIeao9901Area().getLivGravita() == 1 || ws.getIeao9901Area().getLivGravita() == 2) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
        }
        else if (ws.getIeao9901Area().getLivGravita() == 3 || ws.getIeao9901Area().getLivGravita() == 4) {
            // COB_CODE: SET IDSV0001-FORZ-RC-04-NO  TO TRUE
            areaIdsv0001.getAreaComune().getForzRc04().setIdsv0001ForzRc04No();
            // COB_CODE: SET IDSV0001-ESITO-KO       TO TRUE
            areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        }
        // IMPOSTO IL CODICE ERRORE
        // COB_CODE: MOVE IEAI9901-COD-ERRORE
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErroreFormatted(ws.getIeai9901Area().getCodErroreFormatted());
        // SE IL LIVELLO DI GRAVITA' RESTITUITO DALLO IAS9900 E' MAGGIORE
        // DI 2 (ERRORE BLOCCANTE)...IMPOSTO L'ESITO E I DATI DI CONTESTO
        //RELATIVI ALL'ERRORE BLOCCANTE
        // COB_CODE: MOVE IEAI9901-COD-SERVIZIO-BE
        //             TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe(ws.getIeai9901Area().getCodServizioBe());
        // COB_CODE: MOVE IEAI9901-LABEL-ERR
        //             TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr(ws.getIeai9901Area().getLabelErr());
        // COB_CODE: MOVE IEAO9901-DESC-ERRORE-ESTESA
        //             TO IDSV0001-DESC-ERRORE-ESTESA
        //                IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa(ws.getIeao9901Area().getDescErroreEstesa());
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore(ws.getIeao9901Area().getDescErroreEstesa());
        // COB_CODE: MOVE SPACES          TO IEAI9901-COD-SERVIZIO-BE
        //                                  IEAI9901-LABEL-ERR
        //                                   IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setCodServizioBe("");
        ws.getIeai9901Area().setLabelErr("");
        ws.getIeai9901Area().setParametriErr("");
        // COB_CODE: MOVE ZEROES          TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErrore(0);
    }

    /**Original name: S0310-ERRORE-FATALE<br>
	 * <pre>IMPOSTO LA GRAVITA' ERRORE</pre>*/
    private void s0310ErroreFatale() {
        // COB_CODE: MOVE  3
        //             TO IDSV0001-LIV-GRAVITA-BE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001LivGravitaBe(((short)3));
        // COB_CODE: SET IDSV0001-ESITO-KO        TO TRUE
        areaIdsv0001.getEsito().setIdsv0001EsitoKo();
        //IMPOSTO IL CODICE ERRORE (ERRORE FISSO)
        // COB_CODE: MOVE 900001
        //             TO IDSV0001-COD-ERRORE(IDSV0001-MAX-ELE-ERRORI)
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setIdsv0001CodErrore(900001);
        //IMPOSTO NOME DEL MODULO
        // COB_CODE: MOVE 'IEAS9900'               TO IDSV0001-COD-SERVIZIO-BE
        areaIdsv0001.getLogErrore().setCodServizioBe("IEAS9900");
        //IMPOSTO IL NOME DELLA LABEL IN CUI SI E' GENERATO L'ERRORE
        // COB_CODE: MOVE 'S0300-RICERCA-GRAVITA-ERRORE'
        //              TO IDSV0001-LABEL-ERR
        areaIdsv0001.getLogErrore().setLabelErr("S0300-RICERCA-GRAVITA-ERRORE");
        // COB_CODE: MOVE  'ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900'
        //              TO IDSV0001-DESC-ERRORE-ESTESA
        //                 IDSV0001-DESC-ERRORE(IDSV0001-MAX-ELE-ERRORI).
        areaIdsv0001.getLogErrore().setDescErroreEstesa("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
        areaIdsv0001.getEleErrori(areaIdsv0001.getMaxEleErrori()).setDescErrore("ERRORE DI SISTEMA NELLA CHIAMATA AL SERVIZIO IEAS9900");
    }

    /**Original name: S0290-ERRORE-DI-SISTEMA<br>
	 * <pre>**-------------------------------------------------------------**
	 *     PROGRAMMA ..... IERP9902
	 *     TIPOLOGIA...... COPY
	 *     DESCRIZIONE.... ROUTINE GENERALIZZATA GESTIONE ERRORI CALL
	 * **-------------------------------------------------------------**</pre>*/
    private void s0290ErroreDiSistema() {
        // COB_CODE: MOVE '005006'           TO IEAI9901-COD-ERRORE.
        ws.getIeai9901Area().setCodErroreFormatted("005006");
        // COB_CODE: MOVE CALL-DESC          TO IEAI9901-PARAMETRI-ERR.
        ws.getIeai9901Area().setParametriErr(ws.getIdsv0002().getCallDesc());
        // COB_CODE: PERFORM S0300-RICERCA-GRAVITA-ERRORE
        //              THRU EX-S0300.
        s0300RicercaGravitaErrore();
    }

    public void initIxIndici() {
        ws.getIxIndici().setIxTabTga(((short)0));
        ws.getIxIndici().setIxTgaW870(((short)0));
        ws.getIxIndici().setIxTitW870(((short)0));
        ws.getIxIndici().setIxGrz(((short)0));
        ws.getIxIndici().setIxPmo(((short)0));
        ws.getIxIndici().setIndpmo(((short)0));
        ws.getIxIndici().setIxTabErr(((short)0));
        ws.getIxIndici().setIxTabFlags(((short)0));
    }
}
