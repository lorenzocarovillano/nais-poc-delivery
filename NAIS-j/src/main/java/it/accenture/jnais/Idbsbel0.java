package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BnficrLiqDao;
import it.accenture.jnais.commons.data.to.IBnficrLiq;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BnficrLiqIdbsbel0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsbel0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.BelDtDormienza;
import it.accenture.jnais.ws.redefines.BelDtRiserveSomP;
import it.accenture.jnais.ws.redefines.BelDtUltDocto;
import it.accenture.jnais.ws.redefines.BelDtVlt;
import it.accenture.jnais.ws.redefines.BelIdAssto;
import it.accenture.jnais.ws.redefines.BelIdMoviChiu;
import it.accenture.jnais.ws.redefines.BelIdRappAna;
import it.accenture.jnais.ws.redefines.BelImpIntrRitPag;
import it.accenture.jnais.ws.redefines.BelImpLrdLiqto;
import it.accenture.jnais.ws.redefines.BelImpNetLiqto;
import it.accenture.jnais.ws.redefines.BelImpst252Ipt;
import it.accenture.jnais.ws.redefines.BelImpstBolloTotV;
import it.accenture.jnais.ws.redefines.BelImpstIpt;
import it.accenture.jnais.ws.redefines.BelImpstIrpefIpt;
import it.accenture.jnais.ws.redefines.BelImpstPrvrIpt;
import it.accenture.jnais.ws.redefines.BelImpstSost1382011;
import it.accenture.jnais.ws.redefines.BelImpstSost662014;
import it.accenture.jnais.ws.redefines.BelImpstSostIpt;
import it.accenture.jnais.ws.redefines.BelImpstVis1382011;
import it.accenture.jnais.ws.redefines.BelImpstVis662014;
import it.accenture.jnais.ws.redefines.BelPcLiq;
import it.accenture.jnais.ws.redefines.BelRitAccIpt;
import it.accenture.jnais.ws.redefines.BelRitTfrIpt;
import it.accenture.jnais.ws.redefines.BelRitVisIpt;
import it.accenture.jnais.ws.redefines.BelTaxSep;

/**Original name: IDBSBEL0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  02 LUG 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsbel0 extends Program implements IBnficrLiq {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BnficrLiqDao bnficrLiqDao = new BnficrLiqDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsbel0Data ws = new Idbsbel0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BNFICR-LIQ
    private BnficrLiqIdbsbel0 bnficrLiq;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSBEL0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BnficrLiqIdbsbel0 bnficrLiq) {
        this.idsv0003 = idsv0003;
        this.bnficrLiq = bnficrLiq;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsbel0 getInstance() {
        return ((Idbsbel0)Programs.getInstance(Idbsbel0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSBEL0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSBEL0");
        // COB_CODE: MOVE 'BNFICR_LIQ' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BNFICR_LIQ");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFICR_LIQ
        //                ,ID_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,COD_COMP_ANIA
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,ID_RAPP_ANA
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_LIQ
        //                ,ESRCN_ATTVT_IMPRS
        //                ,TP_IND_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,IMP_LRD_LIQTO
        //                ,IMPST_IPT
        //                ,IMP_NET_LIQTO
        //                ,RIT_ACC_IPT
        //                ,RIT_VIS_IPT
        //                ,RIT_TFR_IPT
        //                ,IMPST_IRPEF_IPT
        //                ,IMPST_SOST_IPT
        //                ,IMPST_PRVR_IPT
        //                ,IMPST_252_IPT
        //                ,ID_ASSTO
        //                ,TAX_SEP
        //                ,DT_RISERVE_SOM_P
        //                ,DT_VLT
        //                ,TP_STAT_LIQ_BNFICR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RICH_CALC_CNBT_INP
        //                ,IMP_INTR_RIT_PAG
        //                ,DT_ULT_DOCTO
        //                ,DT_DORMIENZA
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_VIS_1382011
        //                ,IMPST_SOST_1382011
        //                ,IMPST_VIS_662014
        //                ,IMPST_SOST_662014
        //             INTO
        //                :BEL-ID-BNFICR-LIQ
        //               ,:BEL-ID-LIQ
        //               ,:BEL-ID-MOVI-CRZ
        //               ,:BEL-ID-MOVI-CHIU
        //                :IND-BEL-ID-MOVI-CHIU
        //               ,:BEL-COD-COMP-ANIA
        //               ,:BEL-DT-INI-EFF-DB
        //               ,:BEL-DT-END-EFF-DB
        //               ,:BEL-ID-RAPP-ANA
        //                :IND-BEL-ID-RAPP-ANA
        //               ,:BEL-COD-BNFICR
        //                :IND-BEL-COD-BNFICR
        //               ,:BEL-DESC-BNFICR-VCHAR
        //                :IND-BEL-DESC-BNFICR
        //               ,:BEL-PC-LIQ
        //                :IND-BEL-PC-LIQ
        //               ,:BEL-ESRCN-ATTVT-IMPRS
        //                :IND-BEL-ESRCN-ATTVT-IMPRS
        //               ,:BEL-TP-IND-BNFICR
        //                :IND-BEL-TP-IND-BNFICR
        //               ,:BEL-FL-ESE
        //                :IND-BEL-FL-ESE
        //               ,:BEL-FL-IRREV
        //                :IND-BEL-FL-IRREV
        //               ,:BEL-IMP-LRD-LIQTO
        //                :IND-BEL-IMP-LRD-LIQTO
        //               ,:BEL-IMPST-IPT
        //                :IND-BEL-IMPST-IPT
        //               ,:BEL-IMP-NET-LIQTO
        //                :IND-BEL-IMP-NET-LIQTO
        //               ,:BEL-RIT-ACC-IPT
        //                :IND-BEL-RIT-ACC-IPT
        //               ,:BEL-RIT-VIS-IPT
        //                :IND-BEL-RIT-VIS-IPT
        //               ,:BEL-RIT-TFR-IPT
        //                :IND-BEL-RIT-TFR-IPT
        //               ,:BEL-IMPST-IRPEF-IPT
        //                :IND-BEL-IMPST-IRPEF-IPT
        //               ,:BEL-IMPST-SOST-IPT
        //                :IND-BEL-IMPST-SOST-IPT
        //               ,:BEL-IMPST-PRVR-IPT
        //                :IND-BEL-IMPST-PRVR-IPT
        //               ,:BEL-IMPST-252-IPT
        //                :IND-BEL-IMPST-252-IPT
        //               ,:BEL-ID-ASSTO
        //                :IND-BEL-ID-ASSTO
        //               ,:BEL-TAX-SEP
        //                :IND-BEL-TAX-SEP
        //               ,:BEL-DT-RISERVE-SOM-P-DB
        //                :IND-BEL-DT-RISERVE-SOM-P
        //               ,:BEL-DT-VLT-DB
        //                :IND-BEL-DT-VLT
        //               ,:BEL-TP-STAT-LIQ-BNFICR
        //                :IND-BEL-TP-STAT-LIQ-BNFICR
        //               ,:BEL-DS-RIGA
        //               ,:BEL-DS-OPER-SQL
        //               ,:BEL-DS-VER
        //               ,:BEL-DS-TS-INI-CPTZ
        //               ,:BEL-DS-TS-END-CPTZ
        //               ,:BEL-DS-UTENTE
        //               ,:BEL-DS-STATO-ELAB
        //               ,:BEL-RICH-CALC-CNBT-INP
        //                :IND-BEL-RICH-CALC-CNBT-INP
        //               ,:BEL-IMP-INTR-RIT-PAG
        //                :IND-BEL-IMP-INTR-RIT-PAG
        //               ,:BEL-DT-ULT-DOCTO-DB
        //                :IND-BEL-DT-ULT-DOCTO
        //               ,:BEL-DT-DORMIENZA-DB
        //                :IND-BEL-DT-DORMIENZA
        //               ,:BEL-IMPST-BOLLO-TOT-V
        //                :IND-BEL-IMPST-BOLLO-TOT-V
        //               ,:BEL-IMPST-VIS-1382011
        //                :IND-BEL-IMPST-VIS-1382011
        //               ,:BEL-IMPST-SOST-1382011
        //                :IND-BEL-IMPST-SOST-1382011
        //               ,:BEL-IMPST-VIS-662014
        //                :IND-BEL-IMPST-VIS-662014
        //               ,:BEL-IMPST-SOST-662014
        //                :IND-BEL-IMPST-SOST-662014
        //             FROM BNFICR_LIQ
        //             WHERE     DS_RIGA = :BEL-DS-RIGA
        //           END-EXEC.
        bnficrLiqDao.selectByBelDsRiga(bnficrLiq.getBelDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO BNFICR_LIQ
            //                  (
            //                     ID_BNFICR_LIQ
            //                    ,ID_LIQ
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,COD_COMP_ANIA
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,ID_RAPP_ANA
            //                    ,COD_BNFICR
            //                    ,DESC_BNFICR
            //                    ,PC_LIQ
            //                    ,ESRCN_ATTVT_IMPRS
            //                    ,TP_IND_BNFICR
            //                    ,FL_ESE
            //                    ,FL_IRREV
            //                    ,IMP_LRD_LIQTO
            //                    ,IMPST_IPT
            //                    ,IMP_NET_LIQTO
            //                    ,RIT_ACC_IPT
            //                    ,RIT_VIS_IPT
            //                    ,RIT_TFR_IPT
            //                    ,IMPST_IRPEF_IPT
            //                    ,IMPST_SOST_IPT
            //                    ,IMPST_PRVR_IPT
            //                    ,IMPST_252_IPT
            //                    ,ID_ASSTO
            //                    ,TAX_SEP
            //                    ,DT_RISERVE_SOM_P
            //                    ,DT_VLT
            //                    ,TP_STAT_LIQ_BNFICR
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,RICH_CALC_CNBT_INP
            //                    ,IMP_INTR_RIT_PAG
            //                    ,DT_ULT_DOCTO
            //                    ,DT_DORMIENZA
            //                    ,IMPST_BOLLO_TOT_V
            //                    ,IMPST_VIS_1382011
            //                    ,IMPST_SOST_1382011
            //                    ,IMPST_VIS_662014
            //                    ,IMPST_SOST_662014
            //                  )
            //              VALUES
            //                  (
            //                    :BEL-ID-BNFICR-LIQ
            //                    ,:BEL-ID-LIQ
            //                    ,:BEL-ID-MOVI-CRZ
            //                    ,:BEL-ID-MOVI-CHIU
            //                     :IND-BEL-ID-MOVI-CHIU
            //                    ,:BEL-COD-COMP-ANIA
            //                    ,:BEL-DT-INI-EFF-DB
            //                    ,:BEL-DT-END-EFF-DB
            //                    ,:BEL-ID-RAPP-ANA
            //                     :IND-BEL-ID-RAPP-ANA
            //                    ,:BEL-COD-BNFICR
            //                     :IND-BEL-COD-BNFICR
            //                    ,:BEL-DESC-BNFICR-VCHAR
            //                     :IND-BEL-DESC-BNFICR
            //                    ,:BEL-PC-LIQ
            //                     :IND-BEL-PC-LIQ
            //                    ,:BEL-ESRCN-ATTVT-IMPRS
            //                     :IND-BEL-ESRCN-ATTVT-IMPRS
            //                    ,:BEL-TP-IND-BNFICR
            //                     :IND-BEL-TP-IND-BNFICR
            //                    ,:BEL-FL-ESE
            //                     :IND-BEL-FL-ESE
            //                    ,:BEL-FL-IRREV
            //                     :IND-BEL-FL-IRREV
            //                    ,:BEL-IMP-LRD-LIQTO
            //                     :IND-BEL-IMP-LRD-LIQTO
            //                    ,:BEL-IMPST-IPT
            //                     :IND-BEL-IMPST-IPT
            //                    ,:BEL-IMP-NET-LIQTO
            //                     :IND-BEL-IMP-NET-LIQTO
            //                    ,:BEL-RIT-ACC-IPT
            //                     :IND-BEL-RIT-ACC-IPT
            //                    ,:BEL-RIT-VIS-IPT
            //                     :IND-BEL-RIT-VIS-IPT
            //                    ,:BEL-RIT-TFR-IPT
            //                     :IND-BEL-RIT-TFR-IPT
            //                    ,:BEL-IMPST-IRPEF-IPT
            //                     :IND-BEL-IMPST-IRPEF-IPT
            //                    ,:BEL-IMPST-SOST-IPT
            //                     :IND-BEL-IMPST-SOST-IPT
            //                    ,:BEL-IMPST-PRVR-IPT
            //                     :IND-BEL-IMPST-PRVR-IPT
            //                    ,:BEL-IMPST-252-IPT
            //                     :IND-BEL-IMPST-252-IPT
            //                    ,:BEL-ID-ASSTO
            //                     :IND-BEL-ID-ASSTO
            //                    ,:BEL-TAX-SEP
            //                     :IND-BEL-TAX-SEP
            //                    ,:BEL-DT-RISERVE-SOM-P-DB
            //                     :IND-BEL-DT-RISERVE-SOM-P
            //                    ,:BEL-DT-VLT-DB
            //                     :IND-BEL-DT-VLT
            //                    ,:BEL-TP-STAT-LIQ-BNFICR
            //                     :IND-BEL-TP-STAT-LIQ-BNFICR
            //                    ,:BEL-DS-RIGA
            //                    ,:BEL-DS-OPER-SQL
            //                    ,:BEL-DS-VER
            //                    ,:BEL-DS-TS-INI-CPTZ
            //                    ,:BEL-DS-TS-END-CPTZ
            //                    ,:BEL-DS-UTENTE
            //                    ,:BEL-DS-STATO-ELAB
            //                    ,:BEL-RICH-CALC-CNBT-INP
            //                     :IND-BEL-RICH-CALC-CNBT-INP
            //                    ,:BEL-IMP-INTR-RIT-PAG
            //                     :IND-BEL-IMP-INTR-RIT-PAG
            //                    ,:BEL-DT-ULT-DOCTO-DB
            //                     :IND-BEL-DT-ULT-DOCTO
            //                    ,:BEL-DT-DORMIENZA-DB
            //                     :IND-BEL-DT-DORMIENZA
            //                    ,:BEL-IMPST-BOLLO-TOT-V
            //                     :IND-BEL-IMPST-BOLLO-TOT-V
            //                    ,:BEL-IMPST-VIS-1382011
            //                     :IND-BEL-IMPST-VIS-1382011
            //                    ,:BEL-IMPST-SOST-1382011
            //                     :IND-BEL-IMPST-SOST-1382011
            //                    ,:BEL-IMPST-VIS-662014
            //                     :IND-BEL-IMPST-VIS-662014
            //                    ,:BEL-IMPST-SOST-662014
            //                     :IND-BEL-IMPST-SOST-662014
            //                  )
            //           END-EXEC
            bnficrLiqDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BNFICR_LIQ SET
        //                   ID_BNFICR_LIQ          =
        //                :BEL-ID-BNFICR-LIQ
        //                  ,ID_LIQ                 =
        //                :BEL-ID-LIQ
        //                  ,ID_MOVI_CRZ            =
        //                :BEL-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :BEL-ID-MOVI-CHIU
        //                                       :IND-BEL-ID-MOVI-CHIU
        //                  ,COD_COMP_ANIA          =
        //                :BEL-COD-COMP-ANIA
        //                  ,DT_INI_EFF             =
        //           :BEL-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :BEL-DT-END-EFF-DB
        //                  ,ID_RAPP_ANA            =
        //                :BEL-ID-RAPP-ANA
        //                                       :IND-BEL-ID-RAPP-ANA
        //                  ,COD_BNFICR             =
        //                :BEL-COD-BNFICR
        //                                       :IND-BEL-COD-BNFICR
        //                  ,DESC_BNFICR            =
        //                :BEL-DESC-BNFICR-VCHAR
        //                                       :IND-BEL-DESC-BNFICR
        //                  ,PC_LIQ                 =
        //                :BEL-PC-LIQ
        //                                       :IND-BEL-PC-LIQ
        //                  ,ESRCN_ATTVT_IMPRS      =
        //                :BEL-ESRCN-ATTVT-IMPRS
        //                                       :IND-BEL-ESRCN-ATTVT-IMPRS
        //                  ,TP_IND_BNFICR          =
        //                :BEL-TP-IND-BNFICR
        //                                       :IND-BEL-TP-IND-BNFICR
        //                  ,FL_ESE                 =
        //                :BEL-FL-ESE
        //                                       :IND-BEL-FL-ESE
        //                  ,FL_IRREV               =
        //                :BEL-FL-IRREV
        //                                       :IND-BEL-FL-IRREV
        //                  ,IMP_LRD_LIQTO          =
        //                :BEL-IMP-LRD-LIQTO
        //                                       :IND-BEL-IMP-LRD-LIQTO
        //                  ,IMPST_IPT              =
        //                :BEL-IMPST-IPT
        //                                       :IND-BEL-IMPST-IPT
        //                  ,IMP_NET_LIQTO          =
        //                :BEL-IMP-NET-LIQTO
        //                                       :IND-BEL-IMP-NET-LIQTO
        //                  ,RIT_ACC_IPT            =
        //                :BEL-RIT-ACC-IPT
        //                                       :IND-BEL-RIT-ACC-IPT
        //                  ,RIT_VIS_IPT            =
        //                :BEL-RIT-VIS-IPT
        //                                       :IND-BEL-RIT-VIS-IPT
        //                  ,RIT_TFR_IPT            =
        //                :BEL-RIT-TFR-IPT
        //                                       :IND-BEL-RIT-TFR-IPT
        //                  ,IMPST_IRPEF_IPT        =
        //                :BEL-IMPST-IRPEF-IPT
        //                                       :IND-BEL-IMPST-IRPEF-IPT
        //                  ,IMPST_SOST_IPT         =
        //                :BEL-IMPST-SOST-IPT
        //                                       :IND-BEL-IMPST-SOST-IPT
        //                  ,IMPST_PRVR_IPT         =
        //                :BEL-IMPST-PRVR-IPT
        //                                       :IND-BEL-IMPST-PRVR-IPT
        //                  ,IMPST_252_IPT          =
        //                :BEL-IMPST-252-IPT
        //                                       :IND-BEL-IMPST-252-IPT
        //                  ,ID_ASSTO               =
        //                :BEL-ID-ASSTO
        //                                       :IND-BEL-ID-ASSTO
        //                  ,TAX_SEP                =
        //                :BEL-TAX-SEP
        //                                       :IND-BEL-TAX-SEP
        //                  ,DT_RISERVE_SOM_P       =
        //           :BEL-DT-RISERVE-SOM-P-DB
        //                                       :IND-BEL-DT-RISERVE-SOM-P
        //                  ,DT_VLT                 =
        //           :BEL-DT-VLT-DB
        //                                       :IND-BEL-DT-VLT
        //                  ,TP_STAT_LIQ_BNFICR     =
        //                :BEL-TP-STAT-LIQ-BNFICR
        //                                       :IND-BEL-TP-STAT-LIQ-BNFICR
        //                  ,DS_RIGA                =
        //                :BEL-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :BEL-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :BEL-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :BEL-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :BEL-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :BEL-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :BEL-DS-STATO-ELAB
        //                  ,RICH_CALC_CNBT_INP     =
        //                :BEL-RICH-CALC-CNBT-INP
        //                                       :IND-BEL-RICH-CALC-CNBT-INP
        //                  ,IMP_INTR_RIT_PAG       =
        //                :BEL-IMP-INTR-RIT-PAG
        //                                       :IND-BEL-IMP-INTR-RIT-PAG
        //                  ,DT_ULT_DOCTO           =
        //           :BEL-DT-ULT-DOCTO-DB
        //                                       :IND-BEL-DT-ULT-DOCTO
        //                  ,DT_DORMIENZA           =
        //           :BEL-DT-DORMIENZA-DB
        //                                       :IND-BEL-DT-DORMIENZA
        //                  ,IMPST_BOLLO_TOT_V      =
        //                :BEL-IMPST-BOLLO-TOT-V
        //                                       :IND-BEL-IMPST-BOLLO-TOT-V
        //                  ,IMPST_VIS_1382011      =
        //                :BEL-IMPST-VIS-1382011
        //                                       :IND-BEL-IMPST-VIS-1382011
        //                  ,IMPST_SOST_1382011     =
        //                :BEL-IMPST-SOST-1382011
        //                                       :IND-BEL-IMPST-SOST-1382011
        //                  ,IMPST_VIS_662014       =
        //                :BEL-IMPST-VIS-662014
        //                                       :IND-BEL-IMPST-VIS-662014
        //                  ,IMPST_SOST_662014      =
        //                :BEL-IMPST-SOST-662014
        //                                       :IND-BEL-IMPST-SOST-662014
        //                WHERE     DS_RIGA = :BEL-DS-RIGA
        //           END-EXEC.
        bnficrLiqDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM BNFICR_LIQ
        //                WHERE     DS_RIGA = :BEL-DS-RIGA
        //           END-EXEC.
        bnficrLiqDao.deleteByBelDsRiga(bnficrLiq.getBelDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-BEL CURSOR FOR
        //              SELECT
        //                     ID_BNFICR_LIQ
        //                    ,ID_LIQ
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,ID_RAPP_ANA
        //                    ,COD_BNFICR
        //                    ,DESC_BNFICR
        //                    ,PC_LIQ
        //                    ,ESRCN_ATTVT_IMPRS
        //                    ,TP_IND_BNFICR
        //                    ,FL_ESE
        //                    ,FL_IRREV
        //                    ,IMP_LRD_LIQTO
        //                    ,IMPST_IPT
        //                    ,IMP_NET_LIQTO
        //                    ,RIT_ACC_IPT
        //                    ,RIT_VIS_IPT
        //                    ,RIT_TFR_IPT
        //                    ,IMPST_IRPEF_IPT
        //                    ,IMPST_SOST_IPT
        //                    ,IMPST_PRVR_IPT
        //                    ,IMPST_252_IPT
        //                    ,ID_ASSTO
        //                    ,TAX_SEP
        //                    ,DT_RISERVE_SOM_P
        //                    ,DT_VLT
        //                    ,TP_STAT_LIQ_BNFICR
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,RICH_CALC_CNBT_INP
        //                    ,IMP_INTR_RIT_PAG
        //                    ,DT_ULT_DOCTO
        //                    ,DT_DORMIENZA
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_VIS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,IMPST_VIS_662014
        //                    ,IMPST_SOST_662014
        //              FROM BNFICR_LIQ
        //              WHERE     ID_BNFICR_LIQ = :BEL-ID-BNFICR-LIQ
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFICR_LIQ
        //                ,ID_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,COD_COMP_ANIA
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,ID_RAPP_ANA
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_LIQ
        //                ,ESRCN_ATTVT_IMPRS
        //                ,TP_IND_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,IMP_LRD_LIQTO
        //                ,IMPST_IPT
        //                ,IMP_NET_LIQTO
        //                ,RIT_ACC_IPT
        //                ,RIT_VIS_IPT
        //                ,RIT_TFR_IPT
        //                ,IMPST_IRPEF_IPT
        //                ,IMPST_SOST_IPT
        //                ,IMPST_PRVR_IPT
        //                ,IMPST_252_IPT
        //                ,ID_ASSTO
        //                ,TAX_SEP
        //                ,DT_RISERVE_SOM_P
        //                ,DT_VLT
        //                ,TP_STAT_LIQ_BNFICR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RICH_CALC_CNBT_INP
        //                ,IMP_INTR_RIT_PAG
        //                ,DT_ULT_DOCTO
        //                ,DT_DORMIENZA
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_VIS_1382011
        //                ,IMPST_SOST_1382011
        //                ,IMPST_VIS_662014
        //                ,IMPST_SOST_662014
        //             INTO
        //                :BEL-ID-BNFICR-LIQ
        //               ,:BEL-ID-LIQ
        //               ,:BEL-ID-MOVI-CRZ
        //               ,:BEL-ID-MOVI-CHIU
        //                :IND-BEL-ID-MOVI-CHIU
        //               ,:BEL-COD-COMP-ANIA
        //               ,:BEL-DT-INI-EFF-DB
        //               ,:BEL-DT-END-EFF-DB
        //               ,:BEL-ID-RAPP-ANA
        //                :IND-BEL-ID-RAPP-ANA
        //               ,:BEL-COD-BNFICR
        //                :IND-BEL-COD-BNFICR
        //               ,:BEL-DESC-BNFICR-VCHAR
        //                :IND-BEL-DESC-BNFICR
        //               ,:BEL-PC-LIQ
        //                :IND-BEL-PC-LIQ
        //               ,:BEL-ESRCN-ATTVT-IMPRS
        //                :IND-BEL-ESRCN-ATTVT-IMPRS
        //               ,:BEL-TP-IND-BNFICR
        //                :IND-BEL-TP-IND-BNFICR
        //               ,:BEL-FL-ESE
        //                :IND-BEL-FL-ESE
        //               ,:BEL-FL-IRREV
        //                :IND-BEL-FL-IRREV
        //               ,:BEL-IMP-LRD-LIQTO
        //                :IND-BEL-IMP-LRD-LIQTO
        //               ,:BEL-IMPST-IPT
        //                :IND-BEL-IMPST-IPT
        //               ,:BEL-IMP-NET-LIQTO
        //                :IND-BEL-IMP-NET-LIQTO
        //               ,:BEL-RIT-ACC-IPT
        //                :IND-BEL-RIT-ACC-IPT
        //               ,:BEL-RIT-VIS-IPT
        //                :IND-BEL-RIT-VIS-IPT
        //               ,:BEL-RIT-TFR-IPT
        //                :IND-BEL-RIT-TFR-IPT
        //               ,:BEL-IMPST-IRPEF-IPT
        //                :IND-BEL-IMPST-IRPEF-IPT
        //               ,:BEL-IMPST-SOST-IPT
        //                :IND-BEL-IMPST-SOST-IPT
        //               ,:BEL-IMPST-PRVR-IPT
        //                :IND-BEL-IMPST-PRVR-IPT
        //               ,:BEL-IMPST-252-IPT
        //                :IND-BEL-IMPST-252-IPT
        //               ,:BEL-ID-ASSTO
        //                :IND-BEL-ID-ASSTO
        //               ,:BEL-TAX-SEP
        //                :IND-BEL-TAX-SEP
        //               ,:BEL-DT-RISERVE-SOM-P-DB
        //                :IND-BEL-DT-RISERVE-SOM-P
        //               ,:BEL-DT-VLT-DB
        //                :IND-BEL-DT-VLT
        //               ,:BEL-TP-STAT-LIQ-BNFICR
        //                :IND-BEL-TP-STAT-LIQ-BNFICR
        //               ,:BEL-DS-RIGA
        //               ,:BEL-DS-OPER-SQL
        //               ,:BEL-DS-VER
        //               ,:BEL-DS-TS-INI-CPTZ
        //               ,:BEL-DS-TS-END-CPTZ
        //               ,:BEL-DS-UTENTE
        //               ,:BEL-DS-STATO-ELAB
        //               ,:BEL-RICH-CALC-CNBT-INP
        //                :IND-BEL-RICH-CALC-CNBT-INP
        //               ,:BEL-IMP-INTR-RIT-PAG
        //                :IND-BEL-IMP-INTR-RIT-PAG
        //               ,:BEL-DT-ULT-DOCTO-DB
        //                :IND-BEL-DT-ULT-DOCTO
        //               ,:BEL-DT-DORMIENZA-DB
        //                :IND-BEL-DT-DORMIENZA
        //               ,:BEL-IMPST-BOLLO-TOT-V
        //                :IND-BEL-IMPST-BOLLO-TOT-V
        //               ,:BEL-IMPST-VIS-1382011
        //                :IND-BEL-IMPST-VIS-1382011
        //               ,:BEL-IMPST-SOST-1382011
        //                :IND-BEL-IMPST-SOST-1382011
        //               ,:BEL-IMPST-VIS-662014
        //                :IND-BEL-IMPST-VIS-662014
        //               ,:BEL-IMPST-SOST-662014
        //                :IND-BEL-IMPST-SOST-662014
        //             FROM BNFICR_LIQ
        //             WHERE     ID_BNFICR_LIQ = :BEL-ID-BNFICR-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        bnficrLiqDao.selectRec(bnficrLiq.getBelIdBnficrLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE BNFICR_LIQ SET
        //                   ID_BNFICR_LIQ          =
        //                :BEL-ID-BNFICR-LIQ
        //                  ,ID_LIQ                 =
        //                :BEL-ID-LIQ
        //                  ,ID_MOVI_CRZ            =
        //                :BEL-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :BEL-ID-MOVI-CHIU
        //                                       :IND-BEL-ID-MOVI-CHIU
        //                  ,COD_COMP_ANIA          =
        //                :BEL-COD-COMP-ANIA
        //                  ,DT_INI_EFF             =
        //           :BEL-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :BEL-DT-END-EFF-DB
        //                  ,ID_RAPP_ANA            =
        //                :BEL-ID-RAPP-ANA
        //                                       :IND-BEL-ID-RAPP-ANA
        //                  ,COD_BNFICR             =
        //                :BEL-COD-BNFICR
        //                                       :IND-BEL-COD-BNFICR
        //                  ,DESC_BNFICR            =
        //                :BEL-DESC-BNFICR-VCHAR
        //                                       :IND-BEL-DESC-BNFICR
        //                  ,PC_LIQ                 =
        //                :BEL-PC-LIQ
        //                                       :IND-BEL-PC-LIQ
        //                  ,ESRCN_ATTVT_IMPRS      =
        //                :BEL-ESRCN-ATTVT-IMPRS
        //                                       :IND-BEL-ESRCN-ATTVT-IMPRS
        //                  ,TP_IND_BNFICR          =
        //                :BEL-TP-IND-BNFICR
        //                                       :IND-BEL-TP-IND-BNFICR
        //                  ,FL_ESE                 =
        //                :BEL-FL-ESE
        //                                       :IND-BEL-FL-ESE
        //                  ,FL_IRREV               =
        //                :BEL-FL-IRREV
        //                                       :IND-BEL-FL-IRREV
        //                  ,IMP_LRD_LIQTO          =
        //                :BEL-IMP-LRD-LIQTO
        //                                       :IND-BEL-IMP-LRD-LIQTO
        //                  ,IMPST_IPT              =
        //                :BEL-IMPST-IPT
        //                                       :IND-BEL-IMPST-IPT
        //                  ,IMP_NET_LIQTO          =
        //                :BEL-IMP-NET-LIQTO
        //                                       :IND-BEL-IMP-NET-LIQTO
        //                  ,RIT_ACC_IPT            =
        //                :BEL-RIT-ACC-IPT
        //                                       :IND-BEL-RIT-ACC-IPT
        //                  ,RIT_VIS_IPT            =
        //                :BEL-RIT-VIS-IPT
        //                                       :IND-BEL-RIT-VIS-IPT
        //                  ,RIT_TFR_IPT            =
        //                :BEL-RIT-TFR-IPT
        //                                       :IND-BEL-RIT-TFR-IPT
        //                  ,IMPST_IRPEF_IPT        =
        //                :BEL-IMPST-IRPEF-IPT
        //                                       :IND-BEL-IMPST-IRPEF-IPT
        //                  ,IMPST_SOST_IPT         =
        //                :BEL-IMPST-SOST-IPT
        //                                       :IND-BEL-IMPST-SOST-IPT
        //                  ,IMPST_PRVR_IPT         =
        //                :BEL-IMPST-PRVR-IPT
        //                                       :IND-BEL-IMPST-PRVR-IPT
        //                  ,IMPST_252_IPT          =
        //                :BEL-IMPST-252-IPT
        //                                       :IND-BEL-IMPST-252-IPT
        //                  ,ID_ASSTO               =
        //                :BEL-ID-ASSTO
        //                                       :IND-BEL-ID-ASSTO
        //                  ,TAX_SEP                =
        //                :BEL-TAX-SEP
        //                                       :IND-BEL-TAX-SEP
        //                  ,DT_RISERVE_SOM_P       =
        //           :BEL-DT-RISERVE-SOM-P-DB
        //                                       :IND-BEL-DT-RISERVE-SOM-P
        //                  ,DT_VLT                 =
        //           :BEL-DT-VLT-DB
        //                                       :IND-BEL-DT-VLT
        //                  ,TP_STAT_LIQ_BNFICR     =
        //                :BEL-TP-STAT-LIQ-BNFICR
        //                                       :IND-BEL-TP-STAT-LIQ-BNFICR
        //                  ,DS_RIGA                =
        //                :BEL-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :BEL-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :BEL-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :BEL-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :BEL-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :BEL-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :BEL-DS-STATO-ELAB
        //                  ,RICH_CALC_CNBT_INP     =
        //                :BEL-RICH-CALC-CNBT-INP
        //                                       :IND-BEL-RICH-CALC-CNBT-INP
        //                  ,IMP_INTR_RIT_PAG       =
        //                :BEL-IMP-INTR-RIT-PAG
        //                                       :IND-BEL-IMP-INTR-RIT-PAG
        //                  ,DT_ULT_DOCTO           =
        //           :BEL-DT-ULT-DOCTO-DB
        //                                       :IND-BEL-DT-ULT-DOCTO
        //                  ,DT_DORMIENZA           =
        //           :BEL-DT-DORMIENZA-DB
        //                                       :IND-BEL-DT-DORMIENZA
        //                  ,IMPST_BOLLO_TOT_V      =
        //                :BEL-IMPST-BOLLO-TOT-V
        //                                       :IND-BEL-IMPST-BOLLO-TOT-V
        //                  ,IMPST_VIS_1382011      =
        //                :BEL-IMPST-VIS-1382011
        //                                       :IND-BEL-IMPST-VIS-1382011
        //                  ,IMPST_SOST_1382011     =
        //                :BEL-IMPST-SOST-1382011
        //                                       :IND-BEL-IMPST-SOST-1382011
        //                  ,IMPST_VIS_662014       =
        //                :BEL-IMPST-VIS-662014
        //                                       :IND-BEL-IMPST-VIS-662014
        //                  ,IMPST_SOST_662014      =
        //                :BEL-IMPST-SOST-662014
        //                                       :IND-BEL-IMPST-SOST-662014
        //                WHERE     DS_RIGA = :BEL-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        bnficrLiqDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-BEL
        //           END-EXEC.
        bnficrLiqDao.openCIdUpdEffBel(bnficrLiq.getBelIdBnficrLiq(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-BEL
        //           END-EXEC.
        bnficrLiqDao.closeCIdUpdEffBel();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-BEL
        //           INTO
        //                :BEL-ID-BNFICR-LIQ
        //               ,:BEL-ID-LIQ
        //               ,:BEL-ID-MOVI-CRZ
        //               ,:BEL-ID-MOVI-CHIU
        //                :IND-BEL-ID-MOVI-CHIU
        //               ,:BEL-COD-COMP-ANIA
        //               ,:BEL-DT-INI-EFF-DB
        //               ,:BEL-DT-END-EFF-DB
        //               ,:BEL-ID-RAPP-ANA
        //                :IND-BEL-ID-RAPP-ANA
        //               ,:BEL-COD-BNFICR
        //                :IND-BEL-COD-BNFICR
        //               ,:BEL-DESC-BNFICR-VCHAR
        //                :IND-BEL-DESC-BNFICR
        //               ,:BEL-PC-LIQ
        //                :IND-BEL-PC-LIQ
        //               ,:BEL-ESRCN-ATTVT-IMPRS
        //                :IND-BEL-ESRCN-ATTVT-IMPRS
        //               ,:BEL-TP-IND-BNFICR
        //                :IND-BEL-TP-IND-BNFICR
        //               ,:BEL-FL-ESE
        //                :IND-BEL-FL-ESE
        //               ,:BEL-FL-IRREV
        //                :IND-BEL-FL-IRREV
        //               ,:BEL-IMP-LRD-LIQTO
        //                :IND-BEL-IMP-LRD-LIQTO
        //               ,:BEL-IMPST-IPT
        //                :IND-BEL-IMPST-IPT
        //               ,:BEL-IMP-NET-LIQTO
        //                :IND-BEL-IMP-NET-LIQTO
        //               ,:BEL-RIT-ACC-IPT
        //                :IND-BEL-RIT-ACC-IPT
        //               ,:BEL-RIT-VIS-IPT
        //                :IND-BEL-RIT-VIS-IPT
        //               ,:BEL-RIT-TFR-IPT
        //                :IND-BEL-RIT-TFR-IPT
        //               ,:BEL-IMPST-IRPEF-IPT
        //                :IND-BEL-IMPST-IRPEF-IPT
        //               ,:BEL-IMPST-SOST-IPT
        //                :IND-BEL-IMPST-SOST-IPT
        //               ,:BEL-IMPST-PRVR-IPT
        //                :IND-BEL-IMPST-PRVR-IPT
        //               ,:BEL-IMPST-252-IPT
        //                :IND-BEL-IMPST-252-IPT
        //               ,:BEL-ID-ASSTO
        //                :IND-BEL-ID-ASSTO
        //               ,:BEL-TAX-SEP
        //                :IND-BEL-TAX-SEP
        //               ,:BEL-DT-RISERVE-SOM-P-DB
        //                :IND-BEL-DT-RISERVE-SOM-P
        //               ,:BEL-DT-VLT-DB
        //                :IND-BEL-DT-VLT
        //               ,:BEL-TP-STAT-LIQ-BNFICR
        //                :IND-BEL-TP-STAT-LIQ-BNFICR
        //               ,:BEL-DS-RIGA
        //               ,:BEL-DS-OPER-SQL
        //               ,:BEL-DS-VER
        //               ,:BEL-DS-TS-INI-CPTZ
        //               ,:BEL-DS-TS-END-CPTZ
        //               ,:BEL-DS-UTENTE
        //               ,:BEL-DS-STATO-ELAB
        //               ,:BEL-RICH-CALC-CNBT-INP
        //                :IND-BEL-RICH-CALC-CNBT-INP
        //               ,:BEL-IMP-INTR-RIT-PAG
        //                :IND-BEL-IMP-INTR-RIT-PAG
        //               ,:BEL-DT-ULT-DOCTO-DB
        //                :IND-BEL-DT-ULT-DOCTO
        //               ,:BEL-DT-DORMIENZA-DB
        //                :IND-BEL-DT-DORMIENZA
        //               ,:BEL-IMPST-BOLLO-TOT-V
        //                :IND-BEL-IMPST-BOLLO-TOT-V
        //               ,:BEL-IMPST-VIS-1382011
        //                :IND-BEL-IMPST-VIS-1382011
        //               ,:BEL-IMPST-SOST-1382011
        //                :IND-BEL-IMPST-SOST-1382011
        //               ,:BEL-IMPST-VIS-662014
        //                :IND-BEL-IMPST-VIS-662014
        //               ,:BEL-IMPST-SOST-662014
        //                :IND-BEL-IMPST-SOST-662014
        //           END-EXEC.
        bnficrLiqDao.fetchCIdUpdEffBel(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-BEL CURSOR FOR
        //              SELECT
        //                     ID_BNFICR_LIQ
        //                    ,ID_LIQ
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,ID_RAPP_ANA
        //                    ,COD_BNFICR
        //                    ,DESC_BNFICR
        //                    ,PC_LIQ
        //                    ,ESRCN_ATTVT_IMPRS
        //                    ,TP_IND_BNFICR
        //                    ,FL_ESE
        //                    ,FL_IRREV
        //                    ,IMP_LRD_LIQTO
        //                    ,IMPST_IPT
        //                    ,IMP_NET_LIQTO
        //                    ,RIT_ACC_IPT
        //                    ,RIT_VIS_IPT
        //                    ,RIT_TFR_IPT
        //                    ,IMPST_IRPEF_IPT
        //                    ,IMPST_SOST_IPT
        //                    ,IMPST_PRVR_IPT
        //                    ,IMPST_252_IPT
        //                    ,ID_ASSTO
        //                    ,TAX_SEP
        //                    ,DT_RISERVE_SOM_P
        //                    ,DT_VLT
        //                    ,TP_STAT_LIQ_BNFICR
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,RICH_CALC_CNBT_INP
        //                    ,IMP_INTR_RIT_PAG
        //                    ,DT_ULT_DOCTO
        //                    ,DT_DORMIENZA
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_VIS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,IMPST_VIS_662014
        //                    ,IMPST_SOST_662014
        //              FROM BNFICR_LIQ
        //              WHERE     ID_LIQ = :BEL-ID-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_BNFICR_LIQ ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFICR_LIQ
        //                ,ID_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,COD_COMP_ANIA
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,ID_RAPP_ANA
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_LIQ
        //                ,ESRCN_ATTVT_IMPRS
        //                ,TP_IND_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,IMP_LRD_LIQTO
        //                ,IMPST_IPT
        //                ,IMP_NET_LIQTO
        //                ,RIT_ACC_IPT
        //                ,RIT_VIS_IPT
        //                ,RIT_TFR_IPT
        //                ,IMPST_IRPEF_IPT
        //                ,IMPST_SOST_IPT
        //                ,IMPST_PRVR_IPT
        //                ,IMPST_252_IPT
        //                ,ID_ASSTO
        //                ,TAX_SEP
        //                ,DT_RISERVE_SOM_P
        //                ,DT_VLT
        //                ,TP_STAT_LIQ_BNFICR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RICH_CALC_CNBT_INP
        //                ,IMP_INTR_RIT_PAG
        //                ,DT_ULT_DOCTO
        //                ,DT_DORMIENZA
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_VIS_1382011
        //                ,IMPST_SOST_1382011
        //                ,IMPST_VIS_662014
        //                ,IMPST_SOST_662014
        //             INTO
        //                :BEL-ID-BNFICR-LIQ
        //               ,:BEL-ID-LIQ
        //               ,:BEL-ID-MOVI-CRZ
        //               ,:BEL-ID-MOVI-CHIU
        //                :IND-BEL-ID-MOVI-CHIU
        //               ,:BEL-COD-COMP-ANIA
        //               ,:BEL-DT-INI-EFF-DB
        //               ,:BEL-DT-END-EFF-DB
        //               ,:BEL-ID-RAPP-ANA
        //                :IND-BEL-ID-RAPP-ANA
        //               ,:BEL-COD-BNFICR
        //                :IND-BEL-COD-BNFICR
        //               ,:BEL-DESC-BNFICR-VCHAR
        //                :IND-BEL-DESC-BNFICR
        //               ,:BEL-PC-LIQ
        //                :IND-BEL-PC-LIQ
        //               ,:BEL-ESRCN-ATTVT-IMPRS
        //                :IND-BEL-ESRCN-ATTVT-IMPRS
        //               ,:BEL-TP-IND-BNFICR
        //                :IND-BEL-TP-IND-BNFICR
        //               ,:BEL-FL-ESE
        //                :IND-BEL-FL-ESE
        //               ,:BEL-FL-IRREV
        //                :IND-BEL-FL-IRREV
        //               ,:BEL-IMP-LRD-LIQTO
        //                :IND-BEL-IMP-LRD-LIQTO
        //               ,:BEL-IMPST-IPT
        //                :IND-BEL-IMPST-IPT
        //               ,:BEL-IMP-NET-LIQTO
        //                :IND-BEL-IMP-NET-LIQTO
        //               ,:BEL-RIT-ACC-IPT
        //                :IND-BEL-RIT-ACC-IPT
        //               ,:BEL-RIT-VIS-IPT
        //                :IND-BEL-RIT-VIS-IPT
        //               ,:BEL-RIT-TFR-IPT
        //                :IND-BEL-RIT-TFR-IPT
        //               ,:BEL-IMPST-IRPEF-IPT
        //                :IND-BEL-IMPST-IRPEF-IPT
        //               ,:BEL-IMPST-SOST-IPT
        //                :IND-BEL-IMPST-SOST-IPT
        //               ,:BEL-IMPST-PRVR-IPT
        //                :IND-BEL-IMPST-PRVR-IPT
        //               ,:BEL-IMPST-252-IPT
        //                :IND-BEL-IMPST-252-IPT
        //               ,:BEL-ID-ASSTO
        //                :IND-BEL-ID-ASSTO
        //               ,:BEL-TAX-SEP
        //                :IND-BEL-TAX-SEP
        //               ,:BEL-DT-RISERVE-SOM-P-DB
        //                :IND-BEL-DT-RISERVE-SOM-P
        //               ,:BEL-DT-VLT-DB
        //                :IND-BEL-DT-VLT
        //               ,:BEL-TP-STAT-LIQ-BNFICR
        //                :IND-BEL-TP-STAT-LIQ-BNFICR
        //               ,:BEL-DS-RIGA
        //               ,:BEL-DS-OPER-SQL
        //               ,:BEL-DS-VER
        //               ,:BEL-DS-TS-INI-CPTZ
        //               ,:BEL-DS-TS-END-CPTZ
        //               ,:BEL-DS-UTENTE
        //               ,:BEL-DS-STATO-ELAB
        //               ,:BEL-RICH-CALC-CNBT-INP
        //                :IND-BEL-RICH-CALC-CNBT-INP
        //               ,:BEL-IMP-INTR-RIT-PAG
        //                :IND-BEL-IMP-INTR-RIT-PAG
        //               ,:BEL-DT-ULT-DOCTO-DB
        //                :IND-BEL-DT-ULT-DOCTO
        //               ,:BEL-DT-DORMIENZA-DB
        //                :IND-BEL-DT-DORMIENZA
        //               ,:BEL-IMPST-BOLLO-TOT-V
        //                :IND-BEL-IMPST-BOLLO-TOT-V
        //               ,:BEL-IMPST-VIS-1382011
        //                :IND-BEL-IMPST-VIS-1382011
        //               ,:BEL-IMPST-SOST-1382011
        //                :IND-BEL-IMPST-SOST-1382011
        //               ,:BEL-IMPST-VIS-662014
        //                :IND-BEL-IMPST-VIS-662014
        //               ,:BEL-IMPST-SOST-662014
        //                :IND-BEL-IMPST-SOST-662014
        //             FROM BNFICR_LIQ
        //             WHERE     ID_LIQ = :BEL-ID-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        bnficrLiqDao.selectRec1(bnficrLiq.getBelIdLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-BEL
        //           END-EXEC.
        bnficrLiqDao.openCIdpEffBel(bnficrLiq.getBelIdLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-BEL
        //           END-EXEC.
        bnficrLiqDao.closeCIdpEffBel();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-BEL
        //           INTO
        //                :BEL-ID-BNFICR-LIQ
        //               ,:BEL-ID-LIQ
        //               ,:BEL-ID-MOVI-CRZ
        //               ,:BEL-ID-MOVI-CHIU
        //                :IND-BEL-ID-MOVI-CHIU
        //               ,:BEL-COD-COMP-ANIA
        //               ,:BEL-DT-INI-EFF-DB
        //               ,:BEL-DT-END-EFF-DB
        //               ,:BEL-ID-RAPP-ANA
        //                :IND-BEL-ID-RAPP-ANA
        //               ,:BEL-COD-BNFICR
        //                :IND-BEL-COD-BNFICR
        //               ,:BEL-DESC-BNFICR-VCHAR
        //                :IND-BEL-DESC-BNFICR
        //               ,:BEL-PC-LIQ
        //                :IND-BEL-PC-LIQ
        //               ,:BEL-ESRCN-ATTVT-IMPRS
        //                :IND-BEL-ESRCN-ATTVT-IMPRS
        //               ,:BEL-TP-IND-BNFICR
        //                :IND-BEL-TP-IND-BNFICR
        //               ,:BEL-FL-ESE
        //                :IND-BEL-FL-ESE
        //               ,:BEL-FL-IRREV
        //                :IND-BEL-FL-IRREV
        //               ,:BEL-IMP-LRD-LIQTO
        //                :IND-BEL-IMP-LRD-LIQTO
        //               ,:BEL-IMPST-IPT
        //                :IND-BEL-IMPST-IPT
        //               ,:BEL-IMP-NET-LIQTO
        //                :IND-BEL-IMP-NET-LIQTO
        //               ,:BEL-RIT-ACC-IPT
        //                :IND-BEL-RIT-ACC-IPT
        //               ,:BEL-RIT-VIS-IPT
        //                :IND-BEL-RIT-VIS-IPT
        //               ,:BEL-RIT-TFR-IPT
        //                :IND-BEL-RIT-TFR-IPT
        //               ,:BEL-IMPST-IRPEF-IPT
        //                :IND-BEL-IMPST-IRPEF-IPT
        //               ,:BEL-IMPST-SOST-IPT
        //                :IND-BEL-IMPST-SOST-IPT
        //               ,:BEL-IMPST-PRVR-IPT
        //                :IND-BEL-IMPST-PRVR-IPT
        //               ,:BEL-IMPST-252-IPT
        //                :IND-BEL-IMPST-252-IPT
        //               ,:BEL-ID-ASSTO
        //                :IND-BEL-ID-ASSTO
        //               ,:BEL-TAX-SEP
        //                :IND-BEL-TAX-SEP
        //               ,:BEL-DT-RISERVE-SOM-P-DB
        //                :IND-BEL-DT-RISERVE-SOM-P
        //               ,:BEL-DT-VLT-DB
        //                :IND-BEL-DT-VLT
        //               ,:BEL-TP-STAT-LIQ-BNFICR
        //                :IND-BEL-TP-STAT-LIQ-BNFICR
        //               ,:BEL-DS-RIGA
        //               ,:BEL-DS-OPER-SQL
        //               ,:BEL-DS-VER
        //               ,:BEL-DS-TS-INI-CPTZ
        //               ,:BEL-DS-TS-END-CPTZ
        //               ,:BEL-DS-UTENTE
        //               ,:BEL-DS-STATO-ELAB
        //               ,:BEL-RICH-CALC-CNBT-INP
        //                :IND-BEL-RICH-CALC-CNBT-INP
        //               ,:BEL-IMP-INTR-RIT-PAG
        //                :IND-BEL-IMP-INTR-RIT-PAG
        //               ,:BEL-DT-ULT-DOCTO-DB
        //                :IND-BEL-DT-ULT-DOCTO
        //               ,:BEL-DT-DORMIENZA-DB
        //                :IND-BEL-DT-DORMIENZA
        //               ,:BEL-IMPST-BOLLO-TOT-V
        //                :IND-BEL-IMPST-BOLLO-TOT-V
        //               ,:BEL-IMPST-VIS-1382011
        //                :IND-BEL-IMPST-VIS-1382011
        //               ,:BEL-IMPST-SOST-1382011
        //                :IND-BEL-IMPST-SOST-1382011
        //               ,:BEL-IMPST-VIS-662014
        //                :IND-BEL-IMPST-VIS-662014
        //               ,:BEL-IMPST-SOST-662014
        //                :IND-BEL-IMPST-SOST-662014
        //           END-EXEC.
        bnficrLiqDao.fetchCIdpEffBel(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFICR_LIQ
        //                ,ID_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,COD_COMP_ANIA
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,ID_RAPP_ANA
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_LIQ
        //                ,ESRCN_ATTVT_IMPRS
        //                ,TP_IND_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,IMP_LRD_LIQTO
        //                ,IMPST_IPT
        //                ,IMP_NET_LIQTO
        //                ,RIT_ACC_IPT
        //                ,RIT_VIS_IPT
        //                ,RIT_TFR_IPT
        //                ,IMPST_IRPEF_IPT
        //                ,IMPST_SOST_IPT
        //                ,IMPST_PRVR_IPT
        //                ,IMPST_252_IPT
        //                ,ID_ASSTO
        //                ,TAX_SEP
        //                ,DT_RISERVE_SOM_P
        //                ,DT_VLT
        //                ,TP_STAT_LIQ_BNFICR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RICH_CALC_CNBT_INP
        //                ,IMP_INTR_RIT_PAG
        //                ,DT_ULT_DOCTO
        //                ,DT_DORMIENZA
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_VIS_1382011
        //                ,IMPST_SOST_1382011
        //                ,IMPST_VIS_662014
        //                ,IMPST_SOST_662014
        //             INTO
        //                :BEL-ID-BNFICR-LIQ
        //               ,:BEL-ID-LIQ
        //               ,:BEL-ID-MOVI-CRZ
        //               ,:BEL-ID-MOVI-CHIU
        //                :IND-BEL-ID-MOVI-CHIU
        //               ,:BEL-COD-COMP-ANIA
        //               ,:BEL-DT-INI-EFF-DB
        //               ,:BEL-DT-END-EFF-DB
        //               ,:BEL-ID-RAPP-ANA
        //                :IND-BEL-ID-RAPP-ANA
        //               ,:BEL-COD-BNFICR
        //                :IND-BEL-COD-BNFICR
        //               ,:BEL-DESC-BNFICR-VCHAR
        //                :IND-BEL-DESC-BNFICR
        //               ,:BEL-PC-LIQ
        //                :IND-BEL-PC-LIQ
        //               ,:BEL-ESRCN-ATTVT-IMPRS
        //                :IND-BEL-ESRCN-ATTVT-IMPRS
        //               ,:BEL-TP-IND-BNFICR
        //                :IND-BEL-TP-IND-BNFICR
        //               ,:BEL-FL-ESE
        //                :IND-BEL-FL-ESE
        //               ,:BEL-FL-IRREV
        //                :IND-BEL-FL-IRREV
        //               ,:BEL-IMP-LRD-LIQTO
        //                :IND-BEL-IMP-LRD-LIQTO
        //               ,:BEL-IMPST-IPT
        //                :IND-BEL-IMPST-IPT
        //               ,:BEL-IMP-NET-LIQTO
        //                :IND-BEL-IMP-NET-LIQTO
        //               ,:BEL-RIT-ACC-IPT
        //                :IND-BEL-RIT-ACC-IPT
        //               ,:BEL-RIT-VIS-IPT
        //                :IND-BEL-RIT-VIS-IPT
        //               ,:BEL-RIT-TFR-IPT
        //                :IND-BEL-RIT-TFR-IPT
        //               ,:BEL-IMPST-IRPEF-IPT
        //                :IND-BEL-IMPST-IRPEF-IPT
        //               ,:BEL-IMPST-SOST-IPT
        //                :IND-BEL-IMPST-SOST-IPT
        //               ,:BEL-IMPST-PRVR-IPT
        //                :IND-BEL-IMPST-PRVR-IPT
        //               ,:BEL-IMPST-252-IPT
        //                :IND-BEL-IMPST-252-IPT
        //               ,:BEL-ID-ASSTO
        //                :IND-BEL-ID-ASSTO
        //               ,:BEL-TAX-SEP
        //                :IND-BEL-TAX-SEP
        //               ,:BEL-DT-RISERVE-SOM-P-DB
        //                :IND-BEL-DT-RISERVE-SOM-P
        //               ,:BEL-DT-VLT-DB
        //                :IND-BEL-DT-VLT
        //               ,:BEL-TP-STAT-LIQ-BNFICR
        //                :IND-BEL-TP-STAT-LIQ-BNFICR
        //               ,:BEL-DS-RIGA
        //               ,:BEL-DS-OPER-SQL
        //               ,:BEL-DS-VER
        //               ,:BEL-DS-TS-INI-CPTZ
        //               ,:BEL-DS-TS-END-CPTZ
        //               ,:BEL-DS-UTENTE
        //               ,:BEL-DS-STATO-ELAB
        //               ,:BEL-RICH-CALC-CNBT-INP
        //                :IND-BEL-RICH-CALC-CNBT-INP
        //               ,:BEL-IMP-INTR-RIT-PAG
        //                :IND-BEL-IMP-INTR-RIT-PAG
        //               ,:BEL-DT-ULT-DOCTO-DB
        //                :IND-BEL-DT-ULT-DOCTO
        //               ,:BEL-DT-DORMIENZA-DB
        //                :IND-BEL-DT-DORMIENZA
        //               ,:BEL-IMPST-BOLLO-TOT-V
        //                :IND-BEL-IMPST-BOLLO-TOT-V
        //               ,:BEL-IMPST-VIS-1382011
        //                :IND-BEL-IMPST-VIS-1382011
        //               ,:BEL-IMPST-SOST-1382011
        //                :IND-BEL-IMPST-SOST-1382011
        //               ,:BEL-IMPST-VIS-662014
        //                :IND-BEL-IMPST-VIS-662014
        //               ,:BEL-IMPST-SOST-662014
        //                :IND-BEL-IMPST-SOST-662014
        //             FROM BNFICR_LIQ
        //             WHERE     ID_BNFICR_LIQ = :BEL-ID-BNFICR-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        bnficrLiqDao.selectRec2(bnficrLiq.getBelIdBnficrLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-BEL CURSOR FOR
        //              SELECT
        //                     ID_BNFICR_LIQ
        //                    ,ID_LIQ
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,ID_RAPP_ANA
        //                    ,COD_BNFICR
        //                    ,DESC_BNFICR
        //                    ,PC_LIQ
        //                    ,ESRCN_ATTVT_IMPRS
        //                    ,TP_IND_BNFICR
        //                    ,FL_ESE
        //                    ,FL_IRREV
        //                    ,IMP_LRD_LIQTO
        //                    ,IMPST_IPT
        //                    ,IMP_NET_LIQTO
        //                    ,RIT_ACC_IPT
        //                    ,RIT_VIS_IPT
        //                    ,RIT_TFR_IPT
        //                    ,IMPST_IRPEF_IPT
        //                    ,IMPST_SOST_IPT
        //                    ,IMPST_PRVR_IPT
        //                    ,IMPST_252_IPT
        //                    ,ID_ASSTO
        //                    ,TAX_SEP
        //                    ,DT_RISERVE_SOM_P
        //                    ,DT_VLT
        //                    ,TP_STAT_LIQ_BNFICR
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,RICH_CALC_CNBT_INP
        //                    ,IMP_INTR_RIT_PAG
        //                    ,DT_ULT_DOCTO
        //                    ,DT_DORMIENZA
        //                    ,IMPST_BOLLO_TOT_V
        //                    ,IMPST_VIS_1382011
        //                    ,IMPST_SOST_1382011
        //                    ,IMPST_VIS_662014
        //                    ,IMPST_SOST_662014
        //              FROM BNFICR_LIQ
        //              WHERE     ID_LIQ = :BEL-ID-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_BNFICR_LIQ ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_BNFICR_LIQ
        //                ,ID_LIQ
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,COD_COMP_ANIA
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,ID_RAPP_ANA
        //                ,COD_BNFICR
        //                ,DESC_BNFICR
        //                ,PC_LIQ
        //                ,ESRCN_ATTVT_IMPRS
        //                ,TP_IND_BNFICR
        //                ,FL_ESE
        //                ,FL_IRREV
        //                ,IMP_LRD_LIQTO
        //                ,IMPST_IPT
        //                ,IMP_NET_LIQTO
        //                ,RIT_ACC_IPT
        //                ,RIT_VIS_IPT
        //                ,RIT_TFR_IPT
        //                ,IMPST_IRPEF_IPT
        //                ,IMPST_SOST_IPT
        //                ,IMPST_PRVR_IPT
        //                ,IMPST_252_IPT
        //                ,ID_ASSTO
        //                ,TAX_SEP
        //                ,DT_RISERVE_SOM_P
        //                ,DT_VLT
        //                ,TP_STAT_LIQ_BNFICR
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,RICH_CALC_CNBT_INP
        //                ,IMP_INTR_RIT_PAG
        //                ,DT_ULT_DOCTO
        //                ,DT_DORMIENZA
        //                ,IMPST_BOLLO_TOT_V
        //                ,IMPST_VIS_1382011
        //                ,IMPST_SOST_1382011
        //                ,IMPST_VIS_662014
        //                ,IMPST_SOST_662014
        //             INTO
        //                :BEL-ID-BNFICR-LIQ
        //               ,:BEL-ID-LIQ
        //               ,:BEL-ID-MOVI-CRZ
        //               ,:BEL-ID-MOVI-CHIU
        //                :IND-BEL-ID-MOVI-CHIU
        //               ,:BEL-COD-COMP-ANIA
        //               ,:BEL-DT-INI-EFF-DB
        //               ,:BEL-DT-END-EFF-DB
        //               ,:BEL-ID-RAPP-ANA
        //                :IND-BEL-ID-RAPP-ANA
        //               ,:BEL-COD-BNFICR
        //                :IND-BEL-COD-BNFICR
        //               ,:BEL-DESC-BNFICR-VCHAR
        //                :IND-BEL-DESC-BNFICR
        //               ,:BEL-PC-LIQ
        //                :IND-BEL-PC-LIQ
        //               ,:BEL-ESRCN-ATTVT-IMPRS
        //                :IND-BEL-ESRCN-ATTVT-IMPRS
        //               ,:BEL-TP-IND-BNFICR
        //                :IND-BEL-TP-IND-BNFICR
        //               ,:BEL-FL-ESE
        //                :IND-BEL-FL-ESE
        //               ,:BEL-FL-IRREV
        //                :IND-BEL-FL-IRREV
        //               ,:BEL-IMP-LRD-LIQTO
        //                :IND-BEL-IMP-LRD-LIQTO
        //               ,:BEL-IMPST-IPT
        //                :IND-BEL-IMPST-IPT
        //               ,:BEL-IMP-NET-LIQTO
        //                :IND-BEL-IMP-NET-LIQTO
        //               ,:BEL-RIT-ACC-IPT
        //                :IND-BEL-RIT-ACC-IPT
        //               ,:BEL-RIT-VIS-IPT
        //                :IND-BEL-RIT-VIS-IPT
        //               ,:BEL-RIT-TFR-IPT
        //                :IND-BEL-RIT-TFR-IPT
        //               ,:BEL-IMPST-IRPEF-IPT
        //                :IND-BEL-IMPST-IRPEF-IPT
        //               ,:BEL-IMPST-SOST-IPT
        //                :IND-BEL-IMPST-SOST-IPT
        //               ,:BEL-IMPST-PRVR-IPT
        //                :IND-BEL-IMPST-PRVR-IPT
        //               ,:BEL-IMPST-252-IPT
        //                :IND-BEL-IMPST-252-IPT
        //               ,:BEL-ID-ASSTO
        //                :IND-BEL-ID-ASSTO
        //               ,:BEL-TAX-SEP
        //                :IND-BEL-TAX-SEP
        //               ,:BEL-DT-RISERVE-SOM-P-DB
        //                :IND-BEL-DT-RISERVE-SOM-P
        //               ,:BEL-DT-VLT-DB
        //                :IND-BEL-DT-VLT
        //               ,:BEL-TP-STAT-LIQ-BNFICR
        //                :IND-BEL-TP-STAT-LIQ-BNFICR
        //               ,:BEL-DS-RIGA
        //               ,:BEL-DS-OPER-SQL
        //               ,:BEL-DS-VER
        //               ,:BEL-DS-TS-INI-CPTZ
        //               ,:BEL-DS-TS-END-CPTZ
        //               ,:BEL-DS-UTENTE
        //               ,:BEL-DS-STATO-ELAB
        //               ,:BEL-RICH-CALC-CNBT-INP
        //                :IND-BEL-RICH-CALC-CNBT-INP
        //               ,:BEL-IMP-INTR-RIT-PAG
        //                :IND-BEL-IMP-INTR-RIT-PAG
        //               ,:BEL-DT-ULT-DOCTO-DB
        //                :IND-BEL-DT-ULT-DOCTO
        //               ,:BEL-DT-DORMIENZA-DB
        //                :IND-BEL-DT-DORMIENZA
        //               ,:BEL-IMPST-BOLLO-TOT-V
        //                :IND-BEL-IMPST-BOLLO-TOT-V
        //               ,:BEL-IMPST-VIS-1382011
        //                :IND-BEL-IMPST-VIS-1382011
        //               ,:BEL-IMPST-SOST-1382011
        //                :IND-BEL-IMPST-SOST-1382011
        //               ,:BEL-IMPST-VIS-662014
        //                :IND-BEL-IMPST-VIS-662014
        //               ,:BEL-IMPST-SOST-662014
        //                :IND-BEL-IMPST-SOST-662014
        //             FROM BNFICR_LIQ
        //             WHERE     ID_LIQ = :BEL-ID-LIQ
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        bnficrLiqDao.selectRec3(bnficrLiq.getBelIdLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-BEL
        //           END-EXEC.
        bnficrLiqDao.openCIdpCpzBel(bnficrLiq.getBelIdLiq(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-BEL
        //           END-EXEC.
        bnficrLiqDao.closeCIdpCpzBel();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-BEL
        //           INTO
        //                :BEL-ID-BNFICR-LIQ
        //               ,:BEL-ID-LIQ
        //               ,:BEL-ID-MOVI-CRZ
        //               ,:BEL-ID-MOVI-CHIU
        //                :IND-BEL-ID-MOVI-CHIU
        //               ,:BEL-COD-COMP-ANIA
        //               ,:BEL-DT-INI-EFF-DB
        //               ,:BEL-DT-END-EFF-DB
        //               ,:BEL-ID-RAPP-ANA
        //                :IND-BEL-ID-RAPP-ANA
        //               ,:BEL-COD-BNFICR
        //                :IND-BEL-COD-BNFICR
        //               ,:BEL-DESC-BNFICR-VCHAR
        //                :IND-BEL-DESC-BNFICR
        //               ,:BEL-PC-LIQ
        //                :IND-BEL-PC-LIQ
        //               ,:BEL-ESRCN-ATTVT-IMPRS
        //                :IND-BEL-ESRCN-ATTVT-IMPRS
        //               ,:BEL-TP-IND-BNFICR
        //                :IND-BEL-TP-IND-BNFICR
        //               ,:BEL-FL-ESE
        //                :IND-BEL-FL-ESE
        //               ,:BEL-FL-IRREV
        //                :IND-BEL-FL-IRREV
        //               ,:BEL-IMP-LRD-LIQTO
        //                :IND-BEL-IMP-LRD-LIQTO
        //               ,:BEL-IMPST-IPT
        //                :IND-BEL-IMPST-IPT
        //               ,:BEL-IMP-NET-LIQTO
        //                :IND-BEL-IMP-NET-LIQTO
        //               ,:BEL-RIT-ACC-IPT
        //                :IND-BEL-RIT-ACC-IPT
        //               ,:BEL-RIT-VIS-IPT
        //                :IND-BEL-RIT-VIS-IPT
        //               ,:BEL-RIT-TFR-IPT
        //                :IND-BEL-RIT-TFR-IPT
        //               ,:BEL-IMPST-IRPEF-IPT
        //                :IND-BEL-IMPST-IRPEF-IPT
        //               ,:BEL-IMPST-SOST-IPT
        //                :IND-BEL-IMPST-SOST-IPT
        //               ,:BEL-IMPST-PRVR-IPT
        //                :IND-BEL-IMPST-PRVR-IPT
        //               ,:BEL-IMPST-252-IPT
        //                :IND-BEL-IMPST-252-IPT
        //               ,:BEL-ID-ASSTO
        //                :IND-BEL-ID-ASSTO
        //               ,:BEL-TAX-SEP
        //                :IND-BEL-TAX-SEP
        //               ,:BEL-DT-RISERVE-SOM-P-DB
        //                :IND-BEL-DT-RISERVE-SOM-P
        //               ,:BEL-DT-VLT-DB
        //                :IND-BEL-DT-VLT
        //               ,:BEL-TP-STAT-LIQ-BNFICR
        //                :IND-BEL-TP-STAT-LIQ-BNFICR
        //               ,:BEL-DS-RIGA
        //               ,:BEL-DS-OPER-SQL
        //               ,:BEL-DS-VER
        //               ,:BEL-DS-TS-INI-CPTZ
        //               ,:BEL-DS-TS-END-CPTZ
        //               ,:BEL-DS-UTENTE
        //               ,:BEL-DS-STATO-ELAB
        //               ,:BEL-RICH-CALC-CNBT-INP
        //                :IND-BEL-RICH-CALC-CNBT-INP
        //               ,:BEL-IMP-INTR-RIT-PAG
        //                :IND-BEL-IMP-INTR-RIT-PAG
        //               ,:BEL-DT-ULT-DOCTO-DB
        //                :IND-BEL-DT-ULT-DOCTO
        //               ,:BEL-DT-DORMIENZA-DB
        //                :IND-BEL-DT-DORMIENZA
        //               ,:BEL-IMPST-BOLLO-TOT-V
        //                :IND-BEL-IMPST-BOLLO-TOT-V
        //               ,:BEL-IMPST-VIS-1382011
        //                :IND-BEL-IMPST-VIS-1382011
        //               ,:BEL-IMPST-SOST-1382011
        //                :IND-BEL-IMPST-SOST-1382011
        //               ,:BEL-IMPST-VIS-662014
        //                :IND-BEL-IMPST-VIS-662014
        //               ,:BEL-IMPST-SOST-662014
        //                :IND-BEL-IMPST-SOST-662014
        //           END-EXEC.
        bnficrLiqDao.fetchCIdpCpzBel(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-BEL-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO BEL-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-ID-MOVI-CHIU-NULL
            bnficrLiq.getBelIdMoviChiu().setBelIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelIdMoviChiu.Len.BEL_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-BEL-ID-RAPP-ANA = -1
        //              MOVE HIGH-VALUES TO BEL-ID-RAPP-ANA-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getIdRappAna() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-ID-RAPP-ANA-NULL
            bnficrLiq.getBelIdRappAna().setBelIdRappAnaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelIdRappAna.Len.BEL_ID_RAPP_ANA_NULL));
        }
        // COB_CODE: IF IND-BEL-COD-BNFICR = -1
        //              MOVE HIGH-VALUES TO BEL-COD-BNFICR-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getCodBnficr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-COD-BNFICR-NULL
            bnficrLiq.setBelCodBnficr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BnficrLiqIdbsbel0.Len.BEL_COD_BNFICR));
        }
        // COB_CODE: IF IND-BEL-DESC-BNFICR = -1
        //              MOVE HIGH-VALUES TO BEL-DESC-BNFICR
        //           END-IF
        if (ws.getIndBnficrLiq().getDescBnficr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-DESC-BNFICR
            bnficrLiq.setBelDescBnficr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BnficrLiqIdbsbel0.Len.BEL_DESC_BNFICR));
        }
        // COB_CODE: IF IND-BEL-PC-LIQ = -1
        //              MOVE HIGH-VALUES TO BEL-PC-LIQ-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getPcLiq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-PC-LIQ-NULL
            bnficrLiq.getBelPcLiq().setBelPcLiqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelPcLiq.Len.BEL_PC_LIQ_NULL));
        }
        // COB_CODE: IF IND-BEL-ESRCN-ATTVT-IMPRS = -1
        //              MOVE HIGH-VALUES TO BEL-ESRCN-ATTVT-IMPRS-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getEsrcnAttvtImprs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-ESRCN-ATTVT-IMPRS-NULL
            bnficrLiq.setBelEsrcnAttvtImprs(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BEL-TP-IND-BNFICR = -1
        //              MOVE HIGH-VALUES TO BEL-TP-IND-BNFICR-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getTpIndBnficr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-TP-IND-BNFICR-NULL
            bnficrLiq.setBelTpIndBnficr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BnficrLiqIdbsbel0.Len.BEL_TP_IND_BNFICR));
        }
        // COB_CODE: IF IND-BEL-FL-ESE = -1
        //              MOVE HIGH-VALUES TO BEL-FL-ESE-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getFlEse() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-FL-ESE-NULL
            bnficrLiq.setBelFlEse(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BEL-FL-IRREV = -1
        //              MOVE HIGH-VALUES TO BEL-FL-IRREV-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getFlIrrev() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-FL-IRREV-NULL
            bnficrLiq.setBelFlIrrev(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BEL-IMP-LRD-LIQTO = -1
        //              MOVE HIGH-VALUES TO BEL-IMP-LRD-LIQTO-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpLrdLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMP-LRD-LIQTO-NULL
            bnficrLiq.getBelImpLrdLiqto().setBelImpLrdLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpLrdLiqto.Len.BEL_IMP_LRD_LIQTO_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-IPT = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-IPT-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpstIpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-IPT-NULL
            bnficrLiq.getBelImpstIpt().setBelImpstIptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpstIpt.Len.BEL_IMPST_IPT_NULL));
        }
        // COB_CODE: IF IND-BEL-IMP-NET-LIQTO = -1
        //              MOVE HIGH-VALUES TO BEL-IMP-NET-LIQTO-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpNetLiqto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMP-NET-LIQTO-NULL
            bnficrLiq.getBelImpNetLiqto().setBelImpNetLiqtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpNetLiqto.Len.BEL_IMP_NET_LIQTO_NULL));
        }
        // COB_CODE: IF IND-BEL-RIT-ACC-IPT = -1
        //              MOVE HIGH-VALUES TO BEL-RIT-ACC-IPT-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getRitAccIpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-RIT-ACC-IPT-NULL
            bnficrLiq.getBelRitAccIpt().setBelRitAccIptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelRitAccIpt.Len.BEL_RIT_ACC_IPT_NULL));
        }
        // COB_CODE: IF IND-BEL-RIT-VIS-IPT = -1
        //              MOVE HIGH-VALUES TO BEL-RIT-VIS-IPT-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getRitVisIpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-RIT-VIS-IPT-NULL
            bnficrLiq.getBelRitVisIpt().setBelRitVisIptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelRitVisIpt.Len.BEL_RIT_VIS_IPT_NULL));
        }
        // COB_CODE: IF IND-BEL-RIT-TFR-IPT = -1
        //              MOVE HIGH-VALUES TO BEL-RIT-TFR-IPT-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getRitTfrIpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-RIT-TFR-IPT-NULL
            bnficrLiq.getBelRitTfrIpt().setBelRitTfrIptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelRitTfrIpt.Len.BEL_RIT_TFR_IPT_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-IRPEF-IPT = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-IRPEF-IPT-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpstIrpefIpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-IRPEF-IPT-NULL
            bnficrLiq.getBelImpstIrpefIpt().setBelImpstIrpefIptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpstIrpefIpt.Len.BEL_IMPST_IRPEF_IPT_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-SOST-IPT = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-SOST-IPT-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpstSostIpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-SOST-IPT-NULL
            bnficrLiq.getBelImpstSostIpt().setBelImpstSostIptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpstSostIpt.Len.BEL_IMPST_SOST_IPT_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-PRVR-IPT = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-PRVR-IPT-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpstPrvrIpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-PRVR-IPT-NULL
            bnficrLiq.getBelImpstPrvrIpt().setBelImpstPrvrIptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpstPrvrIpt.Len.BEL_IMPST_PRVR_IPT_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-252-IPT = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-252-IPT-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpst252Ipt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-252-IPT-NULL
            bnficrLiq.getBelImpst252Ipt().setBelImpst252IptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpst252Ipt.Len.BEL_IMPST252_IPT_NULL));
        }
        // COB_CODE: IF IND-BEL-ID-ASSTO = -1
        //              MOVE HIGH-VALUES TO BEL-ID-ASSTO-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getIdAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-ID-ASSTO-NULL
            bnficrLiq.getBelIdAssto().setBelIdAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelIdAssto.Len.BEL_ID_ASSTO_NULL));
        }
        // COB_CODE: IF IND-BEL-TAX-SEP = -1
        //              MOVE HIGH-VALUES TO BEL-TAX-SEP-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getTaxSep() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-TAX-SEP-NULL
            bnficrLiq.getBelTaxSep().setBelTaxSepNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelTaxSep.Len.BEL_TAX_SEP_NULL));
        }
        // COB_CODE: IF IND-BEL-DT-RISERVE-SOM-P = -1
        //              MOVE HIGH-VALUES TO BEL-DT-RISERVE-SOM-P-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getDtRiserveSomP() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-DT-RISERVE-SOM-P-NULL
            bnficrLiq.getBelDtRiserveSomP().setBelDtRiserveSomPNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelDtRiserveSomP.Len.BEL_DT_RISERVE_SOM_P_NULL));
        }
        // COB_CODE: IF IND-BEL-DT-VLT = -1
        //              MOVE HIGH-VALUES TO BEL-DT-VLT-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-DT-VLT-NULL
            bnficrLiq.getBelDtVlt().setBelDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelDtVlt.Len.BEL_DT_VLT_NULL));
        }
        // COB_CODE: IF IND-BEL-TP-STAT-LIQ-BNFICR = -1
        //              MOVE HIGH-VALUES TO BEL-TP-STAT-LIQ-BNFICR-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getTpStatLiqBnficr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-TP-STAT-LIQ-BNFICR-NULL
            bnficrLiq.setBelTpStatLiqBnficr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BnficrLiqIdbsbel0.Len.BEL_TP_STAT_LIQ_BNFICR));
        }
        // COB_CODE: IF IND-BEL-RICH-CALC-CNBT-INP = -1
        //              MOVE HIGH-VALUES TO BEL-RICH-CALC-CNBT-INP-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getRichCalcCnbtInp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-RICH-CALC-CNBT-INP-NULL
            bnficrLiq.setBelRichCalcCnbtInp(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-BEL-IMP-INTR-RIT-PAG = -1
        //              MOVE HIGH-VALUES TO BEL-IMP-INTR-RIT-PAG-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpIntrRitPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMP-INTR-RIT-PAG-NULL
            bnficrLiq.getBelImpIntrRitPag().setBelImpIntrRitPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpIntrRitPag.Len.BEL_IMP_INTR_RIT_PAG_NULL));
        }
        // COB_CODE: IF IND-BEL-DT-ULT-DOCTO = -1
        //              MOVE HIGH-VALUES TO BEL-DT-ULT-DOCTO-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getDtUltDocto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-DT-ULT-DOCTO-NULL
            bnficrLiq.getBelDtUltDocto().setBelDtUltDoctoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelDtUltDocto.Len.BEL_DT_ULT_DOCTO_NULL));
        }
        // COB_CODE: IF IND-BEL-DT-DORMIENZA = -1
        //              MOVE HIGH-VALUES TO BEL-DT-DORMIENZA-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getDtDormienza() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-DT-DORMIENZA-NULL
            bnficrLiq.getBelDtDormienza().setBelDtDormienzaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelDtDormienza.Len.BEL_DT_DORMIENZA_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-BOLLO-TOT-V = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-BOLLO-TOT-V-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpstBolloTotV() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-BOLLO-TOT-V-NULL
            bnficrLiq.getBelImpstBolloTotV().setBelImpstBolloTotVNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpstBolloTotV.Len.BEL_IMPST_BOLLO_TOT_V_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-VIS-1382011 = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-VIS-1382011-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpstVis1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-VIS-1382011-NULL
            bnficrLiq.getBelImpstVis1382011().setBelImpstVis1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpstVis1382011.Len.BEL_IMPST_VIS1382011_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-SOST-1382011 = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-SOST-1382011-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpstSost1382011() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-SOST-1382011-NULL
            bnficrLiq.getBelImpstSost1382011().setBelImpstSost1382011Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpstSost1382011.Len.BEL_IMPST_SOST1382011_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-VIS-662014 = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-VIS-662014-NULL
        //           END-IF
        if (ws.getIndBnficrLiq().getImpstVis662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-VIS-662014-NULL
            bnficrLiq.getBelImpstVis662014().setBelImpstVis662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpstVis662014.Len.BEL_IMPST_VIS662014_NULL));
        }
        // COB_CODE: IF IND-BEL-IMPST-SOST-662014 = -1
        //              MOVE HIGH-VALUES TO BEL-IMPST-SOST-662014-NULL
        //           END-IF.
        if (ws.getIndBnficrLiq().getImpstSost662014() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO BEL-IMPST-SOST-662014-NULL
            bnficrLiq.getBelImpstSost662014().setBelImpstSost662014Null(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelImpstSost662014.Len.BEL_IMPST_SOST662014_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO BEL-DS-OPER-SQL
        bnficrLiq.setBelDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO BEL-DS-VER
        bnficrLiq.setBelDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO BEL-DS-UTENTE
        bnficrLiq.setBelDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO BEL-DS-STATO-ELAB.
        bnficrLiq.setBelDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO BEL-DS-OPER-SQL
        bnficrLiq.setBelDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO BEL-DS-UTENTE.
        bnficrLiq.setBelDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF BEL-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-BEL-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelIdMoviChiu().getBelIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-ID-MOVI-CHIU
            ws.getIndBnficrLiq().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-ID-MOVI-CHIU
            ws.getIndBnficrLiq().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF BEL-ID-RAPP-ANA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-ID-RAPP-ANA
        //           ELSE
        //              MOVE 0 TO IND-BEL-ID-RAPP-ANA
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelIdRappAna().getBelIdRappAnaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-ID-RAPP-ANA
            ws.getIndBnficrLiq().setIdRappAna(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-ID-RAPP-ANA
            ws.getIndBnficrLiq().setIdRappAna(((short)0));
        }
        // COB_CODE: IF BEL-COD-BNFICR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-COD-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-BEL-COD-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelCodBnficrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-COD-BNFICR
            ws.getIndBnficrLiq().setCodBnficr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-COD-BNFICR
            ws.getIndBnficrLiq().setCodBnficr(((short)0));
        }
        // COB_CODE: IF BEL-DESC-BNFICR = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-DESC-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-BEL-DESC-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelDescBnficr(), BnficrLiqIdbsbel0.Len.BEL_DESC_BNFICR)) {
            // COB_CODE: MOVE -1 TO IND-BEL-DESC-BNFICR
            ws.getIndBnficrLiq().setDescBnficr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-DESC-BNFICR
            ws.getIndBnficrLiq().setDescBnficr(((short)0));
        }
        // COB_CODE: IF BEL-PC-LIQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-PC-LIQ
        //           ELSE
        //              MOVE 0 TO IND-BEL-PC-LIQ
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelPcLiq().getBelPcLiqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-PC-LIQ
            ws.getIndBnficrLiq().setPcLiq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-PC-LIQ
            ws.getIndBnficrLiq().setPcLiq(((short)0));
        }
        // COB_CODE: IF BEL-ESRCN-ATTVT-IMPRS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-ESRCN-ATTVT-IMPRS
        //           ELSE
        //              MOVE 0 TO IND-BEL-ESRCN-ATTVT-IMPRS
        //           END-IF
        if (Conditions.eq(bnficrLiq.getBelEsrcnAttvtImprs(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BEL-ESRCN-ATTVT-IMPRS
            ws.getIndBnficrLiq().setEsrcnAttvtImprs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-ESRCN-ATTVT-IMPRS
            ws.getIndBnficrLiq().setEsrcnAttvtImprs(((short)0));
        }
        // COB_CODE: IF BEL-TP-IND-BNFICR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-TP-IND-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-BEL-TP-IND-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelTpIndBnficrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-TP-IND-BNFICR
            ws.getIndBnficrLiq().setTpIndBnficr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-TP-IND-BNFICR
            ws.getIndBnficrLiq().setTpIndBnficr(((short)0));
        }
        // COB_CODE: IF BEL-FL-ESE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-FL-ESE
        //           ELSE
        //              MOVE 0 TO IND-BEL-FL-ESE
        //           END-IF
        if (Conditions.eq(bnficrLiq.getBelFlEse(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BEL-FL-ESE
            ws.getIndBnficrLiq().setFlEse(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-FL-ESE
            ws.getIndBnficrLiq().setFlEse(((short)0));
        }
        // COB_CODE: IF BEL-FL-IRREV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-FL-IRREV
        //           ELSE
        //              MOVE 0 TO IND-BEL-FL-IRREV
        //           END-IF
        if (Conditions.eq(bnficrLiq.getBelFlIrrev(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BEL-FL-IRREV
            ws.getIndBnficrLiq().setFlIrrev(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-FL-IRREV
            ws.getIndBnficrLiq().setFlIrrev(((short)0));
        }
        // COB_CODE: IF BEL-IMP-LRD-LIQTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMP-LRD-LIQTO
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMP-LRD-LIQTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpLrdLiqto().getBelImpLrdLiqtoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMP-LRD-LIQTO
            ws.getIndBnficrLiq().setImpLrdLiqto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMP-LRD-LIQTO
            ws.getIndBnficrLiq().setImpLrdLiqto(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-IPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-IPT
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-IPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpstIpt().getBelImpstIptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-IPT
            ws.getIndBnficrLiq().setImpstIpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-IPT
            ws.getIndBnficrLiq().setImpstIpt(((short)0));
        }
        // COB_CODE: IF BEL-IMP-NET-LIQTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMP-NET-LIQTO
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMP-NET-LIQTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpNetLiqto().getBelImpNetLiqtoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMP-NET-LIQTO
            ws.getIndBnficrLiq().setImpNetLiqto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMP-NET-LIQTO
            ws.getIndBnficrLiq().setImpNetLiqto(((short)0));
        }
        // COB_CODE: IF BEL-RIT-ACC-IPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-RIT-ACC-IPT
        //           ELSE
        //              MOVE 0 TO IND-BEL-RIT-ACC-IPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelRitAccIpt().getBelRitAccIptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-RIT-ACC-IPT
            ws.getIndBnficrLiq().setRitAccIpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-RIT-ACC-IPT
            ws.getIndBnficrLiq().setRitAccIpt(((short)0));
        }
        // COB_CODE: IF BEL-RIT-VIS-IPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-RIT-VIS-IPT
        //           ELSE
        //              MOVE 0 TO IND-BEL-RIT-VIS-IPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelRitVisIpt().getBelRitVisIptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-RIT-VIS-IPT
            ws.getIndBnficrLiq().setRitVisIpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-RIT-VIS-IPT
            ws.getIndBnficrLiq().setRitVisIpt(((short)0));
        }
        // COB_CODE: IF BEL-RIT-TFR-IPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-RIT-TFR-IPT
        //           ELSE
        //              MOVE 0 TO IND-BEL-RIT-TFR-IPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelRitTfrIpt().getBelRitTfrIptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-RIT-TFR-IPT
            ws.getIndBnficrLiq().setRitTfrIpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-RIT-TFR-IPT
            ws.getIndBnficrLiq().setRitTfrIpt(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-IRPEF-IPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-IRPEF-IPT
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-IRPEF-IPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpstIrpefIpt().getBelImpstIrpefIptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-IRPEF-IPT
            ws.getIndBnficrLiq().setImpstIrpefIpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-IRPEF-IPT
            ws.getIndBnficrLiq().setImpstIrpefIpt(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-SOST-IPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-SOST-IPT
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-SOST-IPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpstSostIpt().getBelImpstSostIptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-SOST-IPT
            ws.getIndBnficrLiq().setImpstSostIpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-SOST-IPT
            ws.getIndBnficrLiq().setImpstSostIpt(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-PRVR-IPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-PRVR-IPT
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-PRVR-IPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpstPrvrIpt().getBelImpstPrvrIptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-PRVR-IPT
            ws.getIndBnficrLiq().setImpstPrvrIpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-PRVR-IPT
            ws.getIndBnficrLiq().setImpstPrvrIpt(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-252-IPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-252-IPT
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-252-IPT
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpst252Ipt().getBelImpst252IptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-252-IPT
            ws.getIndBnficrLiq().setImpst252Ipt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-252-IPT
            ws.getIndBnficrLiq().setImpst252Ipt(((short)0));
        }
        // COB_CODE: IF BEL-ID-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-ID-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-BEL-ID-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelIdAssto().getBelIdAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-ID-ASSTO
            ws.getIndBnficrLiq().setIdAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-ID-ASSTO
            ws.getIndBnficrLiq().setIdAssto(((short)0));
        }
        // COB_CODE: IF BEL-TAX-SEP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-TAX-SEP
        //           ELSE
        //              MOVE 0 TO IND-BEL-TAX-SEP
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelTaxSep().getBelTaxSepNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-TAX-SEP
            ws.getIndBnficrLiq().setTaxSep(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-TAX-SEP
            ws.getIndBnficrLiq().setTaxSep(((short)0));
        }
        // COB_CODE: IF BEL-DT-RISERVE-SOM-P-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-DT-RISERVE-SOM-P
        //           ELSE
        //              MOVE 0 TO IND-BEL-DT-RISERVE-SOM-P
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelDtRiserveSomP().getBelDtRiserveSomPNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-DT-RISERVE-SOM-P
            ws.getIndBnficrLiq().setDtRiserveSomP(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-DT-RISERVE-SOM-P
            ws.getIndBnficrLiq().setDtRiserveSomP(((short)0));
        }
        // COB_CODE: IF BEL-DT-VLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-DT-VLT
        //           ELSE
        //              MOVE 0 TO IND-BEL-DT-VLT
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelDtVlt().getBelDtVltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-DT-VLT
            ws.getIndBnficrLiq().setDtVlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-DT-VLT
            ws.getIndBnficrLiq().setDtVlt(((short)0));
        }
        // COB_CODE: IF BEL-TP-STAT-LIQ-BNFICR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-TP-STAT-LIQ-BNFICR
        //           ELSE
        //              MOVE 0 TO IND-BEL-TP-STAT-LIQ-BNFICR
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelTpStatLiqBnficrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-TP-STAT-LIQ-BNFICR
            ws.getIndBnficrLiq().setTpStatLiqBnficr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-TP-STAT-LIQ-BNFICR
            ws.getIndBnficrLiq().setTpStatLiqBnficr(((short)0));
        }
        // COB_CODE: IF BEL-RICH-CALC-CNBT-INP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-RICH-CALC-CNBT-INP
        //           ELSE
        //              MOVE 0 TO IND-BEL-RICH-CALC-CNBT-INP
        //           END-IF
        if (Conditions.eq(bnficrLiq.getBelRichCalcCnbtInp(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-BEL-RICH-CALC-CNBT-INP
            ws.getIndBnficrLiq().setRichCalcCnbtInp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-RICH-CALC-CNBT-INP
            ws.getIndBnficrLiq().setRichCalcCnbtInp(((short)0));
        }
        // COB_CODE: IF BEL-IMP-INTR-RIT-PAG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMP-INTR-RIT-PAG
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMP-INTR-RIT-PAG
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpIntrRitPag().getBelImpIntrRitPagNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMP-INTR-RIT-PAG
            ws.getIndBnficrLiq().setImpIntrRitPag(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMP-INTR-RIT-PAG
            ws.getIndBnficrLiq().setImpIntrRitPag(((short)0));
        }
        // COB_CODE: IF BEL-DT-ULT-DOCTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-DT-ULT-DOCTO
        //           ELSE
        //              MOVE 0 TO IND-BEL-DT-ULT-DOCTO
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelDtUltDocto().getBelDtUltDoctoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-DT-ULT-DOCTO
            ws.getIndBnficrLiq().setDtUltDocto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-DT-ULT-DOCTO
            ws.getIndBnficrLiq().setDtUltDocto(((short)0));
        }
        // COB_CODE: IF BEL-DT-DORMIENZA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-DT-DORMIENZA
        //           ELSE
        //              MOVE 0 TO IND-BEL-DT-DORMIENZA
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelDtDormienza().getBelDtDormienzaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-DT-DORMIENZA
            ws.getIndBnficrLiq().setDtDormienza(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-DT-DORMIENZA
            ws.getIndBnficrLiq().setDtDormienza(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-BOLLO-TOT-V-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-BOLLO-TOT-V
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-BOLLO-TOT-V
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpstBolloTotV().getBelImpstBolloTotVNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-BOLLO-TOT-V
            ws.getIndBnficrLiq().setImpstBolloTotV(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-BOLLO-TOT-V
            ws.getIndBnficrLiq().setImpstBolloTotV(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-VIS-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-VIS-1382011
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-VIS-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpstVis1382011().getBelImpstVis1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-VIS-1382011
            ws.getIndBnficrLiq().setImpstVis1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-VIS-1382011
            ws.getIndBnficrLiq().setImpstVis1382011(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-SOST-1382011-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-SOST-1382011
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-SOST-1382011
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpstSost1382011().getBelImpstSost1382011NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-SOST-1382011
            ws.getIndBnficrLiq().setImpstSost1382011(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-SOST-1382011
            ws.getIndBnficrLiq().setImpstSost1382011(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-VIS-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-VIS-662014
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-VIS-662014
        //           END-IF
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpstVis662014().getBelImpstVis662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-VIS-662014
            ws.getIndBnficrLiq().setImpstVis662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-VIS-662014
            ws.getIndBnficrLiq().setImpstVis662014(((short)0));
        }
        // COB_CODE: IF BEL-IMPST-SOST-662014-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-BEL-IMPST-SOST-662014
        //           ELSE
        //              MOVE 0 TO IND-BEL-IMPST-SOST-662014
        //           END-IF.
        if (Characters.EQ_HIGH.test(bnficrLiq.getBelImpstSost662014().getBelImpstSost662014NullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-BEL-IMPST-SOST-662014
            ws.getIndBnficrLiq().setImpstSost662014(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-BEL-IMPST-SOST-662014
            ws.getIndBnficrLiq().setImpstSost662014(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : BEL-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE BNFICR-LIQ TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(bnficrLiq.getBnficrLiqFormatted());
        // COB_CODE: MOVE BEL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(bnficrLiq.getBelIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO BEL-ID-MOVI-CHIU
                bnficrLiq.getBelIdMoviChiu().setBelIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO BEL-DS-TS-END-CPTZ
                bnficrLiq.setBelDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO BEL-ID-MOVI-CRZ
                    bnficrLiq.setBelIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO BEL-ID-MOVI-CHIU-NULL
                    bnficrLiq.getBelIdMoviChiu().setBelIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelIdMoviChiu.Len.BEL_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO BEL-DT-END-EFF
                    bnficrLiq.setBelDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO BEL-DS-TS-INI-CPTZ
                    bnficrLiq.setBelDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO BEL-DS-TS-END-CPTZ
                    bnficrLiq.setBelDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE BNFICR-LIQ TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(bnficrLiq.getBnficrLiqFormatted());
        // COB_CODE: MOVE BEL-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(bnficrLiq.getBelIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO BNFICR-LIQ.
        bnficrLiq.setBnficrLiqFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO BEL-ID-MOVI-CRZ.
        bnficrLiq.setBelIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO BEL-ID-MOVI-CHIU-NULL.
        bnficrLiq.getBelIdMoviChiu().setBelIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, BelIdMoviChiu.Len.BEL_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO BEL-DT-INI-EFF.
        bnficrLiq.setBelDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO BEL-DT-END-EFF.
        bnficrLiq.setBelDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO BEL-DS-TS-INI-CPTZ.
        bnficrLiq.setBelDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO BEL-DS-TS-END-CPTZ.
        bnficrLiq.setBelDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO BEL-COD-COMP-ANIA.
        bnficrLiq.setBelCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE BEL-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bnficrLiq.getBelDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO BEL-DT-INI-EFF-DB
        ws.getBnficrLiqDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE BEL-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bnficrLiq.getBelDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO BEL-DT-END-EFF-DB
        ws.getBnficrLiqDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-BEL-DT-RISERVE-SOM-P = 0
        //               MOVE WS-DATE-X      TO BEL-DT-RISERVE-SOM-P-DB
        //           END-IF
        if (ws.getIndBnficrLiq().getDtRiserveSomP() == 0) {
            // COB_CODE: MOVE BEL-DT-RISERVE-SOM-P TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bnficrLiq.getBelDtRiserveSomP().getBelDtRiserveSomP(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO BEL-DT-RISERVE-SOM-P-DB
            ws.getBnficrLiqDb().setConcsPrestDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-BEL-DT-VLT = 0
        //               MOVE WS-DATE-X      TO BEL-DT-VLT-DB
        //           END-IF
        if (ws.getIndBnficrLiq().getDtVlt() == 0) {
            // COB_CODE: MOVE BEL-DT-VLT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bnficrLiq.getBelDtVlt().getBelDtVlt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO BEL-DT-VLT-DB
            ws.getBnficrLiqDb().setDecorPrestDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-BEL-DT-ULT-DOCTO = 0
        //               MOVE WS-DATE-X      TO BEL-DT-ULT-DOCTO-DB
        //           END-IF
        if (ws.getIndBnficrLiq().getDtUltDocto() == 0) {
            // COB_CODE: MOVE BEL-DT-ULT-DOCTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bnficrLiq.getBelDtUltDocto().getBelDtUltDocto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO BEL-DT-ULT-DOCTO-DB
            ws.getBnficrLiqDb().setRimbDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-BEL-DT-DORMIENZA = 0
        //               MOVE WS-DATE-X      TO BEL-DT-DORMIENZA-DB
        //           END-IF.
        if (ws.getIndBnficrLiq().getDtDormienza() == 0) {
            // COB_CODE: MOVE BEL-DT-DORMIENZA TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(bnficrLiq.getBelDtDormienza().getBelDtDormienza(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO BEL-DT-DORMIENZA-DB
            ws.getBnficrLiqDb().setRichPrestDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE BEL-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBnficrLiqDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO BEL-DT-INI-EFF
        bnficrLiq.setBelDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE BEL-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getBnficrLiqDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO BEL-DT-END-EFF
        bnficrLiq.setBelDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-BEL-DT-RISERVE-SOM-P = 0
        //               MOVE WS-DATE-N      TO BEL-DT-RISERVE-SOM-P
        //           END-IF
        if (ws.getIndBnficrLiq().getDtRiserveSomP() == 0) {
            // COB_CODE: MOVE BEL-DT-RISERVE-SOM-P-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBnficrLiqDb().getConcsPrestDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO BEL-DT-RISERVE-SOM-P
            bnficrLiq.getBelDtRiserveSomP().setBelDtRiserveSomP(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-BEL-DT-VLT = 0
        //               MOVE WS-DATE-N      TO BEL-DT-VLT
        //           END-IF
        if (ws.getIndBnficrLiq().getDtVlt() == 0) {
            // COB_CODE: MOVE BEL-DT-VLT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBnficrLiqDb().getDecorPrestDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO BEL-DT-VLT
            bnficrLiq.getBelDtVlt().setBelDtVlt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-BEL-DT-ULT-DOCTO = 0
        //               MOVE WS-DATE-N      TO BEL-DT-ULT-DOCTO
        //           END-IF
        if (ws.getIndBnficrLiq().getDtUltDocto() == 0) {
            // COB_CODE: MOVE BEL-DT-ULT-DOCTO-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBnficrLiqDb().getRimbDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO BEL-DT-ULT-DOCTO
            bnficrLiq.getBelDtUltDocto().setBelDtUltDocto(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-BEL-DT-DORMIENZA = 0
        //               MOVE WS-DATE-N      TO BEL-DT-DORMIENZA
        //           END-IF.
        if (ws.getIndBnficrLiq().getDtDormienza() == 0) {
            // COB_CODE: MOVE BEL-DT-DORMIENZA-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getBnficrLiqDb().getRichPrestDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO BEL-DT-DORMIENZA
            bnficrLiq.getBelDtDormienza().setBelDtDormienza(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF BEL-DESC-BNFICR
        //                       TO BEL-DESC-BNFICR-LEN.
        bnficrLiq.setBelDescBnficrLen(((short)BnficrLiqIdbsbel0.Len.BEL_DESC_BNFICR));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public long getBelDsRiga() {
        return bnficrLiq.getBelDsRiga();
    }

    @Override
    public void setBelDsRiga(long belDsRiga) {
        this.bnficrLiq.setBelDsRiga(belDsRiga);
    }

    @Override
    public String getCodBnficr() {
        return bnficrLiq.getBelCodBnficr();
    }

    @Override
    public void setCodBnficr(String codBnficr) {
        this.bnficrLiq.setBelCodBnficr(codBnficr);
    }

    @Override
    public String getCodBnficrObj() {
        if (ws.getIndBnficrLiq().getCodBnficr() >= 0) {
            return getCodBnficr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodBnficrObj(String codBnficrObj) {
        if (codBnficrObj != null) {
            setCodBnficr(codBnficrObj);
            ws.getIndBnficrLiq().setCodBnficr(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setCodBnficr(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return bnficrLiq.getBelCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.bnficrLiq.setBelCodCompAnia(codCompAnia);
    }

    @Override
    public String getDescBnficrVchar() {
        return bnficrLiq.getBelDescBnficrVcharFormatted();
    }

    @Override
    public void setDescBnficrVchar(String descBnficrVchar) {
        this.bnficrLiq.setBelDescBnficrVcharFormatted(descBnficrVchar);
    }

    @Override
    public String getDescBnficrVcharObj() {
        if (ws.getIndBnficrLiq().getDescBnficr() >= 0) {
            return getDescBnficrVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescBnficrVcharObj(String descBnficrVcharObj) {
        if (descBnficrVcharObj != null) {
            setDescBnficrVchar(descBnficrVcharObj);
            ws.getIndBnficrLiq().setDescBnficr(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setDescBnficr(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return bnficrLiq.getBelDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.bnficrLiq.setBelDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return bnficrLiq.getBelDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.bnficrLiq.setBelDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return bnficrLiq.getBelDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.bnficrLiq.setBelDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return bnficrLiq.getBelDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.bnficrLiq.setBelDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return bnficrLiq.getBelDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.bnficrLiq.setBelDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return bnficrLiq.getBelDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.bnficrLiq.setBelDsVer(dsVer);
    }

    @Override
    public String getDtDormienzaDb() {
        return ws.getBnficrLiqDb().getRichPrestDb();
    }

    @Override
    public void setDtDormienzaDb(String dtDormienzaDb) {
        this.ws.getBnficrLiqDb().setRichPrestDb(dtDormienzaDb);
    }

    @Override
    public String getDtDormienzaDbObj() {
        if (ws.getIndBnficrLiq().getDtDormienza() >= 0) {
            return getDtDormienzaDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDormienzaDbObj(String dtDormienzaDbObj) {
        if (dtDormienzaDbObj != null) {
            setDtDormienzaDb(dtDormienzaDbObj);
            ws.getIndBnficrLiq().setDtDormienza(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setDtDormienza(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getBnficrLiqDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getBnficrLiqDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getBnficrLiqDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getBnficrLiqDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtRiserveSomPDb() {
        return ws.getBnficrLiqDb().getConcsPrestDb();
    }

    @Override
    public void setDtRiserveSomPDb(String dtRiserveSomPDb) {
        this.ws.getBnficrLiqDb().setConcsPrestDb(dtRiserveSomPDb);
    }

    @Override
    public String getDtRiserveSomPDbObj() {
        if (ws.getIndBnficrLiq().getDtRiserveSomP() >= 0) {
            return getDtRiserveSomPDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtRiserveSomPDbObj(String dtRiserveSomPDbObj) {
        if (dtRiserveSomPDbObj != null) {
            setDtRiserveSomPDb(dtRiserveSomPDbObj);
            ws.getIndBnficrLiq().setDtRiserveSomP(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setDtRiserveSomP(((short)-1));
        }
    }

    @Override
    public String getDtUltDoctoDb() {
        return ws.getBnficrLiqDb().getRimbDb();
    }

    @Override
    public void setDtUltDoctoDb(String dtUltDoctoDb) {
        this.ws.getBnficrLiqDb().setRimbDb(dtUltDoctoDb);
    }

    @Override
    public String getDtUltDoctoDbObj() {
        if (ws.getIndBnficrLiq().getDtUltDocto() >= 0) {
            return getDtUltDoctoDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltDoctoDbObj(String dtUltDoctoDbObj) {
        if (dtUltDoctoDbObj != null) {
            setDtUltDoctoDb(dtUltDoctoDbObj);
            ws.getIndBnficrLiq().setDtUltDocto(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setDtUltDocto(((short)-1));
        }
    }

    @Override
    public String getDtVltDb() {
        return ws.getBnficrLiqDb().getDecorPrestDb();
    }

    @Override
    public void setDtVltDb(String dtVltDb) {
        this.ws.getBnficrLiqDb().setDecorPrestDb(dtVltDb);
    }

    @Override
    public String getDtVltDbObj() {
        if (ws.getIndBnficrLiq().getDtVlt() >= 0) {
            return getDtVltDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVltDbObj(String dtVltDbObj) {
        if (dtVltDbObj != null) {
            setDtVltDb(dtVltDbObj);
            ws.getIndBnficrLiq().setDtVlt(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setDtVlt(((short)-1));
        }
    }

    @Override
    public char getEsrcnAttvtImprs() {
        return bnficrLiq.getBelEsrcnAttvtImprs();
    }

    @Override
    public void setEsrcnAttvtImprs(char esrcnAttvtImprs) {
        this.bnficrLiq.setBelEsrcnAttvtImprs(esrcnAttvtImprs);
    }

    @Override
    public Character getEsrcnAttvtImprsObj() {
        if (ws.getIndBnficrLiq().getEsrcnAttvtImprs() >= 0) {
            return ((Character)getEsrcnAttvtImprs());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEsrcnAttvtImprsObj(Character esrcnAttvtImprsObj) {
        if (esrcnAttvtImprsObj != null) {
            setEsrcnAttvtImprs(((char)esrcnAttvtImprsObj));
            ws.getIndBnficrLiq().setEsrcnAttvtImprs(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setEsrcnAttvtImprs(((short)-1));
        }
    }

    @Override
    public char getFlEse() {
        return bnficrLiq.getBelFlEse();
    }

    @Override
    public void setFlEse(char flEse) {
        this.bnficrLiq.setBelFlEse(flEse);
    }

    @Override
    public Character getFlEseObj() {
        if (ws.getIndBnficrLiq().getFlEse() >= 0) {
            return ((Character)getFlEse());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEseObj(Character flEseObj) {
        if (flEseObj != null) {
            setFlEse(((char)flEseObj));
            ws.getIndBnficrLiq().setFlEse(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setFlEse(((short)-1));
        }
    }

    @Override
    public char getFlIrrev() {
        return bnficrLiq.getBelFlIrrev();
    }

    @Override
    public void setFlIrrev(char flIrrev) {
        this.bnficrLiq.setBelFlIrrev(flIrrev);
    }

    @Override
    public Character getFlIrrevObj() {
        if (ws.getIndBnficrLiq().getFlIrrev() >= 0) {
            return ((Character)getFlIrrev());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlIrrevObj(Character flIrrevObj) {
        if (flIrrevObj != null) {
            setFlIrrev(((char)flIrrevObj));
            ws.getIndBnficrLiq().setFlIrrev(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setFlIrrev(((short)-1));
        }
    }

    @Override
    public int getIdAssto() {
        return bnficrLiq.getBelIdAssto().getBelIdAssto();
    }

    @Override
    public void setIdAssto(int idAssto) {
        this.bnficrLiq.getBelIdAssto().setBelIdAssto(idAssto);
    }

    @Override
    public Integer getIdAsstoObj() {
        if (ws.getIndBnficrLiq().getIdAssto() >= 0) {
            return ((Integer)getIdAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdAsstoObj(Integer idAsstoObj) {
        if (idAsstoObj != null) {
            setIdAssto(((int)idAsstoObj));
            ws.getIndBnficrLiq().setIdAssto(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setIdAssto(((short)-1));
        }
    }

    @Override
    public int getIdBnficrLiq() {
        return bnficrLiq.getBelIdBnficrLiq();
    }

    @Override
    public void setIdBnficrLiq(int idBnficrLiq) {
        this.bnficrLiq.setBelIdBnficrLiq(idBnficrLiq);
    }

    @Override
    public int getIdLiq() {
        return bnficrLiq.getBelIdLiq();
    }

    @Override
    public void setIdLiq(int idLiq) {
        this.bnficrLiq.setBelIdLiq(idLiq);
    }

    @Override
    public int getIdMoviChiu() {
        return bnficrLiq.getBelIdMoviChiu().getBelIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.bnficrLiq.getBelIdMoviChiu().setBelIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndBnficrLiq().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndBnficrLiq().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return bnficrLiq.getBelIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.bnficrLiq.setBelIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdRappAna() {
        return bnficrLiq.getBelIdRappAna().getBelIdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.bnficrLiq.getBelIdRappAna().setBelIdRappAna(idRappAna);
    }

    @Override
    public Integer getIdRappAnaObj() {
        if (ws.getIndBnficrLiq().getIdRappAna() >= 0) {
            return ((Integer)getIdRappAna());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRappAnaObj(Integer idRappAnaObj) {
        if (idRappAnaObj != null) {
            setIdRappAna(((int)idRappAnaObj));
            ws.getIndBnficrLiq().setIdRappAna(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setIdRappAna(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpIntrRitPag() {
        return bnficrLiq.getBelImpIntrRitPag().getBelImpIntrRitPag();
    }

    @Override
    public void setImpIntrRitPag(AfDecimal impIntrRitPag) {
        this.bnficrLiq.getBelImpIntrRitPag().setBelImpIntrRitPag(impIntrRitPag.copy());
    }

    @Override
    public AfDecimal getImpIntrRitPagObj() {
        if (ws.getIndBnficrLiq().getImpIntrRitPag() >= 0) {
            return getImpIntrRitPag();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpIntrRitPagObj(AfDecimal impIntrRitPagObj) {
        if (impIntrRitPagObj != null) {
            setImpIntrRitPag(new AfDecimal(impIntrRitPagObj, 15, 3));
            ws.getIndBnficrLiq().setImpIntrRitPag(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpIntrRitPag(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpLrdLiqto() {
        return bnficrLiq.getBelImpLrdLiqto().getBelImpLrdLiqto();
    }

    @Override
    public void setImpLrdLiqto(AfDecimal impLrdLiqto) {
        this.bnficrLiq.getBelImpLrdLiqto().setBelImpLrdLiqto(impLrdLiqto.copy());
    }

    @Override
    public AfDecimal getImpLrdLiqtoObj() {
        if (ws.getIndBnficrLiq().getImpLrdLiqto() >= 0) {
            return getImpLrdLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpLrdLiqtoObj(AfDecimal impLrdLiqtoObj) {
        if (impLrdLiqtoObj != null) {
            setImpLrdLiqto(new AfDecimal(impLrdLiqtoObj, 15, 3));
            ws.getIndBnficrLiq().setImpLrdLiqto(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpLrdLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpNetLiqto() {
        return bnficrLiq.getBelImpNetLiqto().getBelImpNetLiqto();
    }

    @Override
    public void setImpNetLiqto(AfDecimal impNetLiqto) {
        this.bnficrLiq.getBelImpNetLiqto().setBelImpNetLiqto(impNetLiqto.copy());
    }

    @Override
    public AfDecimal getImpNetLiqtoObj() {
        if (ws.getIndBnficrLiq().getImpNetLiqto() >= 0) {
            return getImpNetLiqto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpNetLiqtoObj(AfDecimal impNetLiqtoObj) {
        if (impNetLiqtoObj != null) {
            setImpNetLiqto(new AfDecimal(impNetLiqtoObj, 15, 3));
            ws.getIndBnficrLiq().setImpNetLiqto(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpNetLiqto(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpst252Ipt() {
        return bnficrLiq.getBelImpst252Ipt().getBelImpst252Ipt();
    }

    @Override
    public void setImpst252Ipt(AfDecimal impst252Ipt) {
        this.bnficrLiq.getBelImpst252Ipt().setBelImpst252Ipt(impst252Ipt.copy());
    }

    @Override
    public AfDecimal getImpst252IptObj() {
        if (ws.getIndBnficrLiq().getImpst252Ipt() >= 0) {
            return getImpst252Ipt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpst252IptObj(AfDecimal impst252IptObj) {
        if (impst252IptObj != null) {
            setImpst252Ipt(new AfDecimal(impst252IptObj, 15, 3));
            ws.getIndBnficrLiq().setImpst252Ipt(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpst252Ipt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstBolloTotV() {
        return bnficrLiq.getBelImpstBolloTotV().getBelImpstBolloTotV();
    }

    @Override
    public void setImpstBolloTotV(AfDecimal impstBolloTotV) {
        this.bnficrLiq.getBelImpstBolloTotV().setBelImpstBolloTotV(impstBolloTotV.copy());
    }

    @Override
    public AfDecimal getImpstBolloTotVObj() {
        if (ws.getIndBnficrLiq().getImpstBolloTotV() >= 0) {
            return getImpstBolloTotV();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj) {
        if (impstBolloTotVObj != null) {
            setImpstBolloTotV(new AfDecimal(impstBolloTotVObj, 15, 3));
            ws.getIndBnficrLiq().setImpstBolloTotV(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpstBolloTotV(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstIpt() {
        return bnficrLiq.getBelImpstIpt().getBelImpstIpt();
    }

    @Override
    public void setImpstIpt(AfDecimal impstIpt) {
        this.bnficrLiq.getBelImpstIpt().setBelImpstIpt(impstIpt.copy());
    }

    @Override
    public AfDecimal getImpstIptObj() {
        if (ws.getIndBnficrLiq().getImpstIpt() >= 0) {
            return getImpstIpt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstIptObj(AfDecimal impstIptObj) {
        if (impstIptObj != null) {
            setImpstIpt(new AfDecimal(impstIptObj, 15, 3));
            ws.getIndBnficrLiq().setImpstIpt(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpstIpt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstIrpefIpt() {
        return bnficrLiq.getBelImpstIrpefIpt().getBelImpstIrpefIpt();
    }

    @Override
    public void setImpstIrpefIpt(AfDecimal impstIrpefIpt) {
        this.bnficrLiq.getBelImpstIrpefIpt().setBelImpstIrpefIpt(impstIrpefIpt.copy());
    }

    @Override
    public AfDecimal getImpstIrpefIptObj() {
        if (ws.getIndBnficrLiq().getImpstIrpefIpt() >= 0) {
            return getImpstIrpefIpt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstIrpefIptObj(AfDecimal impstIrpefIptObj) {
        if (impstIrpefIptObj != null) {
            setImpstIrpefIpt(new AfDecimal(impstIrpefIptObj, 15, 3));
            ws.getIndBnficrLiq().setImpstIrpefIpt(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpstIrpefIpt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstPrvrIpt() {
        return bnficrLiq.getBelImpstPrvrIpt().getBelImpstPrvrIpt();
    }

    @Override
    public void setImpstPrvrIpt(AfDecimal impstPrvrIpt) {
        this.bnficrLiq.getBelImpstPrvrIpt().setBelImpstPrvrIpt(impstPrvrIpt.copy());
    }

    @Override
    public AfDecimal getImpstPrvrIptObj() {
        if (ws.getIndBnficrLiq().getImpstPrvrIpt() >= 0) {
            return getImpstPrvrIpt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstPrvrIptObj(AfDecimal impstPrvrIptObj) {
        if (impstPrvrIptObj != null) {
            setImpstPrvrIpt(new AfDecimal(impstPrvrIptObj, 15, 3));
            ws.getIndBnficrLiq().setImpstPrvrIpt(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpstPrvrIpt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSost1382011() {
        return bnficrLiq.getBelImpstSost1382011().getBelImpstSost1382011();
    }

    @Override
    public void setImpstSost1382011(AfDecimal impstSost1382011) {
        this.bnficrLiq.getBelImpstSost1382011().setBelImpstSost1382011(impstSost1382011.copy());
    }

    @Override
    public AfDecimal getImpstSost1382011Obj() {
        if (ws.getIndBnficrLiq().getImpstSost1382011() >= 0) {
            return getImpstSost1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSost1382011Obj(AfDecimal impstSost1382011Obj) {
        if (impstSost1382011Obj != null) {
            setImpstSost1382011(new AfDecimal(impstSost1382011Obj, 15, 3));
            ws.getIndBnficrLiq().setImpstSost1382011(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpstSost1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSost662014() {
        return bnficrLiq.getBelImpstSost662014().getBelImpstSost662014();
    }

    @Override
    public void setImpstSost662014(AfDecimal impstSost662014) {
        this.bnficrLiq.getBelImpstSost662014().setBelImpstSost662014(impstSost662014.copy());
    }

    @Override
    public AfDecimal getImpstSost662014Obj() {
        if (ws.getIndBnficrLiq().getImpstSost662014() >= 0) {
            return getImpstSost662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSost662014Obj(AfDecimal impstSost662014Obj) {
        if (impstSost662014Obj != null) {
            setImpstSost662014(new AfDecimal(impstSost662014Obj, 15, 3));
            ws.getIndBnficrLiq().setImpstSost662014(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpstSost662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSostIpt() {
        return bnficrLiq.getBelImpstSostIpt().getBelImpstSostIpt();
    }

    @Override
    public void setImpstSostIpt(AfDecimal impstSostIpt) {
        this.bnficrLiq.getBelImpstSostIpt().setBelImpstSostIpt(impstSostIpt.copy());
    }

    @Override
    public AfDecimal getImpstSostIptObj() {
        if (ws.getIndBnficrLiq().getImpstSostIpt() >= 0) {
            return getImpstSostIpt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSostIptObj(AfDecimal impstSostIptObj) {
        if (impstSostIptObj != null) {
            setImpstSostIpt(new AfDecimal(impstSostIptObj, 15, 3));
            ws.getIndBnficrLiq().setImpstSostIpt(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpstSostIpt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis1382011() {
        return bnficrLiq.getBelImpstVis1382011().getBelImpstVis1382011();
    }

    @Override
    public void setImpstVis1382011(AfDecimal impstVis1382011) {
        this.bnficrLiq.getBelImpstVis1382011().setBelImpstVis1382011(impstVis1382011.copy());
    }

    @Override
    public AfDecimal getImpstVis1382011Obj() {
        if (ws.getIndBnficrLiq().getImpstVis1382011() >= 0) {
            return getImpstVis1382011();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis1382011Obj(AfDecimal impstVis1382011Obj) {
        if (impstVis1382011Obj != null) {
            setImpstVis1382011(new AfDecimal(impstVis1382011Obj, 15, 3));
            ws.getIndBnficrLiq().setImpstVis1382011(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpstVis1382011(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstVis662014() {
        return bnficrLiq.getBelImpstVis662014().getBelImpstVis662014();
    }

    @Override
    public void setImpstVis662014(AfDecimal impstVis662014) {
        this.bnficrLiq.getBelImpstVis662014().setBelImpstVis662014(impstVis662014.copy());
    }

    @Override
    public AfDecimal getImpstVis662014Obj() {
        if (ws.getIndBnficrLiq().getImpstVis662014() >= 0) {
            return getImpstVis662014();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstVis662014Obj(AfDecimal impstVis662014Obj) {
        if (impstVis662014Obj != null) {
            setImpstVis662014(new AfDecimal(impstVis662014Obj, 15, 3));
            ws.getIndBnficrLiq().setImpstVis662014(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setImpstVis662014(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcLiq() {
        return bnficrLiq.getBelPcLiq().getBelPcLiq();
    }

    @Override
    public void setPcLiq(AfDecimal pcLiq) {
        this.bnficrLiq.getBelPcLiq().setBelPcLiq(pcLiq.copy());
    }

    @Override
    public AfDecimal getPcLiqObj() {
        if (ws.getIndBnficrLiq().getPcLiq() >= 0) {
            return getPcLiq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcLiqObj(AfDecimal pcLiqObj) {
        if (pcLiqObj != null) {
            setPcLiq(new AfDecimal(pcLiqObj, 6, 3));
            ws.getIndBnficrLiq().setPcLiq(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setPcLiq(((short)-1));
        }
    }

    @Override
    public char getRichCalcCnbtInp() {
        return bnficrLiq.getBelRichCalcCnbtInp();
    }

    @Override
    public void setRichCalcCnbtInp(char richCalcCnbtInp) {
        this.bnficrLiq.setBelRichCalcCnbtInp(richCalcCnbtInp);
    }

    @Override
    public Character getRichCalcCnbtInpObj() {
        if (ws.getIndBnficrLiq().getRichCalcCnbtInp() >= 0) {
            return ((Character)getRichCalcCnbtInp());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRichCalcCnbtInpObj(Character richCalcCnbtInpObj) {
        if (richCalcCnbtInpObj != null) {
            setRichCalcCnbtInp(((char)richCalcCnbtInpObj));
            ws.getIndBnficrLiq().setRichCalcCnbtInp(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setRichCalcCnbtInp(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitAccIpt() {
        return bnficrLiq.getBelRitAccIpt().getBelRitAccIpt();
    }

    @Override
    public void setRitAccIpt(AfDecimal ritAccIpt) {
        this.bnficrLiq.getBelRitAccIpt().setBelRitAccIpt(ritAccIpt.copy());
    }

    @Override
    public AfDecimal getRitAccIptObj() {
        if (ws.getIndBnficrLiq().getRitAccIpt() >= 0) {
            return getRitAccIpt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitAccIptObj(AfDecimal ritAccIptObj) {
        if (ritAccIptObj != null) {
            setRitAccIpt(new AfDecimal(ritAccIptObj, 15, 3));
            ws.getIndBnficrLiq().setRitAccIpt(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setRitAccIpt(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitTfrIpt() {
        return bnficrLiq.getBelRitTfrIpt().getBelRitTfrIpt();
    }

    @Override
    public void setRitTfrIpt(AfDecimal ritTfrIpt) {
        this.bnficrLiq.getBelRitTfrIpt().setBelRitTfrIpt(ritTfrIpt.copy());
    }

    @Override
    public AfDecimal getRitTfrIptObj() {
        if (ws.getIndBnficrLiq().getRitTfrIpt() >= 0) {
            return getRitTfrIpt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitTfrIptObj(AfDecimal ritTfrIptObj) {
        if (ritTfrIptObj != null) {
            setRitTfrIpt(new AfDecimal(ritTfrIptObj, 15, 3));
            ws.getIndBnficrLiq().setRitTfrIpt(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setRitTfrIpt(((short)-1));
        }
    }

    @Override
    public AfDecimal getRitVisIpt() {
        return bnficrLiq.getBelRitVisIpt().getBelRitVisIpt();
    }

    @Override
    public void setRitVisIpt(AfDecimal ritVisIpt) {
        this.bnficrLiq.getBelRitVisIpt().setBelRitVisIpt(ritVisIpt.copy());
    }

    @Override
    public AfDecimal getRitVisIptObj() {
        if (ws.getIndBnficrLiq().getRitVisIpt() >= 0) {
            return getRitVisIpt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRitVisIptObj(AfDecimal ritVisIptObj) {
        if (ritVisIptObj != null) {
            setRitVisIpt(new AfDecimal(ritVisIptObj, 15, 3));
            ws.getIndBnficrLiq().setRitVisIpt(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setRitVisIpt(((short)-1));
        }
    }

    @Override
    public AfDecimal getTaxSep() {
        return bnficrLiq.getBelTaxSep().getBelTaxSep();
    }

    @Override
    public void setTaxSep(AfDecimal taxSep) {
        this.bnficrLiq.getBelTaxSep().setBelTaxSep(taxSep.copy());
    }

    @Override
    public AfDecimal getTaxSepObj() {
        if (ws.getIndBnficrLiq().getTaxSep() >= 0) {
            return getTaxSep();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTaxSepObj(AfDecimal taxSepObj) {
        if (taxSepObj != null) {
            setTaxSep(new AfDecimal(taxSepObj, 15, 3));
            ws.getIndBnficrLiq().setTaxSep(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setTaxSep(((short)-1));
        }
    }

    @Override
    public String getTpIndBnficr() {
        return bnficrLiq.getBelTpIndBnficr();
    }

    @Override
    public void setTpIndBnficr(String tpIndBnficr) {
        this.bnficrLiq.setBelTpIndBnficr(tpIndBnficr);
    }

    @Override
    public String getTpIndBnficrObj() {
        if (ws.getIndBnficrLiq().getTpIndBnficr() >= 0) {
            return getTpIndBnficr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpIndBnficrObj(String tpIndBnficrObj) {
        if (tpIndBnficrObj != null) {
            setTpIndBnficr(tpIndBnficrObj);
            ws.getIndBnficrLiq().setTpIndBnficr(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setTpIndBnficr(((short)-1));
        }
    }

    @Override
    public String getTpStatLiqBnficr() {
        return bnficrLiq.getBelTpStatLiqBnficr();
    }

    @Override
    public void setTpStatLiqBnficr(String tpStatLiqBnficr) {
        this.bnficrLiq.setBelTpStatLiqBnficr(tpStatLiqBnficr);
    }

    @Override
    public String getTpStatLiqBnficrObj() {
        if (ws.getIndBnficrLiq().getTpStatLiqBnficr() >= 0) {
            return getTpStatLiqBnficr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatLiqBnficrObj(String tpStatLiqBnficrObj) {
        if (tpStatLiqBnficrObj != null) {
            setTpStatLiqBnficr(tpStatLiqBnficrObj);
            ws.getIndBnficrLiq().setTpStatLiqBnficr(((short)0));
        }
        else {
            ws.getIndBnficrLiq().setTpStatLiqBnficr(((short)-1));
        }
    }
}
