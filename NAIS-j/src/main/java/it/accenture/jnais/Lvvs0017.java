package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.Lvvc0000DatiInput2;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.InputLvvs0000;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0017Data;

/**Original name: LVVS0017<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0017
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA PRESTITO
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0017 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0017Data ws = new Lvvs0017Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0017
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0017_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0017 getInstance() {
        return ((Lvvs0017)Programs.getInstance(Lvvs0017.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE AREA-IO-PRE
        //                      WK-DATA-OUTPUT
        //                      WK-DATA-X-12.
        initAreaIoPre();
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        initWkDataX12();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> PERFORM DI CONTROLLI SUI I CAMPI CARICATI NELLE
        //--> DCLGEN DI WORKING
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-CONTROLLO-DATI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
        }
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           *--> CALL MODULO PER RECUPERO DATA
        //                    END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            //--> CALL MODULO PER RECUPERO DATA
            // COB_CODE: PERFORM S1300-CALL-LVVS0000
            //              THRU S1300-CALL-LVVS0000-EX
            s1300CallLvvs0000();
            // COB_CODE:          IF IDSV0003-SUCCESSFUL-RC
            //                      END-EVALUATE
            //                    ELSE
            //           *-->       GESTIRE ERRORE DISPATCHER
            //                      END-STRING
            //                    END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                // COB_CODE:           EVALUATE TRUE
                //                         WHEN IDSV0003-SUCCESSFUL-SQL
                //           *-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                //                                TO IVVC0213-VAL-IMP-O
                //                         WHEN OTHER
                //           *--->         ERRORE DI ACCESSO AL DB
                //                              END-STRING
                //                      END-EVALUATE
                switch (idsv0003.getSqlcode().getSqlcode()) {

                    case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->          OPERAZIONE ESEGUITA CORRETTAMENTE
                        // COB_CODE: MOVE LVVC0000-DATA-OUTPUT
                        //             TO IVVC0213-VAL-IMP-O
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getInputLvvs0000().getDataOutput(), 18, 7));
                        break;

                    default://--->         ERRORE DI ACCESSO AL DB
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                        // COB_CODE: MOVE WK-PGM
                        //             TO IDSV0003-COD-SERVIZIO-BE
                        idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                        // COB_CODE: STRING IDSV0003-RETURN-CODE  ';'
                        //                  IDSV0003-SQLCODE
                        //           DELIMITED BY SIZE
                        //           INTO IDSV0003-DESCRIZ-ERR-DB2
                        //           END-STRING
                        concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                        idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                        break;
                }
            }
            else {
                //-->       GESTIRE ERRORE DISPATCHER
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING IDSV0003-RETURN-CODE  ';'
                //                  IDSV0003-SQLCODE
                //           DELIMITED BY SIZE
                //           INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-PRESTITI
        //                TO DPRE-AREA-PRESTITI
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPrestiti())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPRE-AREA-PRESTITI
            ws.setDpreAreaPrestitiFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: PERFORM VARYING IX-TAB-PRE FROM 1 BY 1
        //             UNTIL IX-TAB-PRE > DPRE-ELE-PREST-MAX
        //                END-IF
        //           END-PERFORM.
        ws.setIxTabPre(((short)1));
        while (!(ws.getIxTabPre() > ws.getDpreElePrestMax())) {
            // COB_CODE: IF DPRE-DT-DECOR-PREST(IX-TAB-PRE) NOT NUMERIC
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (!Functions.isNumber(ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreDtDecorPrest().getWpreDtDecorPrest())) {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'DATA-DECORRENZA-PRESTITO NON NUMERICA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("DATA-DECORRENZA-PRESTITO NON NUMERICA");
            }
            // COB_CODE: IF DPRE-DT-DECOR-PREST(IX-TAB-PRE) = 0
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreDtDecorPrest().getWpreDtDecorPrest() == 0) {
                // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'DATA-DECORRENZA-PRESTITO NON VALORIZZATA'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("DATA-DECORRENZA-PRESTITO NON VALORIZZATA");
            }
            ws.setIxTabPre(Trunc.toShort(ws.getIxTabPre() + 1, 4));
        }
    }

    /**Original name: S1300-CALL-LVVS0000<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIAMATA LVVS0000
	 * ----------------------------------------------------------------*
	 * --> COPY LVVC0000</pre>*/
    private void s1300CallLvvs0000() {
        Lvvs0000 lvvs0000 = null;
        // COB_CODE: INITIALIZE INPUT-LVVS0000.
        initInputLvvs0000();
        // COB_CODE: IF IVVC0213-TP-LIVELLO = 'G'
        //               PERFORM S2027-RAN-GAR          THRU S2027-EX
        //            ELSE
        //               PERFORM S3027-RAN-POL          THRU S3027-EX
        //           END-IF.
        if (ivvc0213.getDatiLivello().getTpLivello() == 'G') {
            // COB_CODE: PERFORM S2027-RAN-GAR          THRU S2027-EX
            s2027RanGar();
        }
        else {
            // COB_CODE: PERFORM S3027-RAN-POL          THRU S3027-EX
            s3027RanPol();
        }
        //
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC     TO TRUE
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL    TO TRUE
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: CALL WK-LVVS0000 USING       IDSV0003
        //                                        INPUT-LVVS0000.
        lvvs0000 = Lvvs0000.getInstance();
        lvvs0000.run(idsv0003, ws.getInputLvvs0000());
    }

    /**Original name: S2027-RAN-GAR<br>
	 * <pre>----------------------------------------------------------------*
	 *   ELABORA  RAPP ANA GAR
	 * ----------------------------------------------------------------*
	 * --> COPY LVVC0000</pre>*/
    private void s2027RanGar() {
        // COB_CODE: INITIALIZE INPUT-LVVS0000.
        initInputLvvs0000();
        // COB_CODE: SET WK-LETTO-NO                TO TRUE.
        ws.getWkFindLetto().setNo();
        //
        // COB_CODE: PERFORM VARYING IX-TAB-PRE FROM 1 BY 1
        //             UNTIL IX-TAB-PRE > DPRE-ELE-PREST-MAX
        //                OR WK-LETTO-SI
        //                END-IF
        //           END-PERFORM
        ws.setIxTabPre(((short)1));
        while (!(ws.getIxTabPre() > ws.getDpreElePrestMax() || ws.getWkFindLetto().isSi())) {
            // COB_CODE: IF DPRE-TP-OGG(IX-TAB-PRE) = 'GA'
            //             END-IF
            //           END-IF
            if (Conditions.eq(ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreTpOgg(), "GA")) {
                // COB_CODE: IF IVVC0213-ID-POL-GAR
                //            = DPRE-ID-OGG(IX-TAB-PRE)
                //                TO LVVC0000-DATA-INPUT-1
                //           END-IF
                if (ivvc0213.getIdPolGar() == ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreIdOgg()) {
                    // COB_CODE: SET WK-LETTO-SI              TO TRUE
                    ws.getWkFindLetto().setSi();
                    // COB_CODE: MOVE DPRE-DT-DECOR-PREST(IX-TAB-PRE)
                    //             TO LVVC0000-DATA-INPUT-1
                    ws.getInputLvvs0000().setDataInput1(TruncAbs.toInt(ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreDtDecorPrest().getWpreDtDecorPrest(), 8));
                }
            }
            ws.setIxTabPre(Trunc.toShort(ws.getIxTabPre() + 1, 4));
        }
        //
        // COB_CODE: IF WK-LETTO-NO
        //              END-PERFORM
        //           END-IF.
        if (ws.getWkFindLetto().isNo()) {
            // COB_CODE: PERFORM VARYING IX-TAB-PRE FROM 1 BY 1
            //             UNTIL IX-TAB-PRE > DPRE-ELE-PREST-MAX
            //                OR WK-LETTO-SI
            //                END-IF
            //           END-PERFORM
            ws.setIxTabPre(((short)1));
            while (!(ws.getIxTabPre() > ws.getDpreElePrestMax() || ws.getWkFindLetto().isSi())) {
                // COB_CODE: IF DPRE-TP-OGG(IX-TAB-PRE) = 'AD'
                //             END-IF
                //           END-IF
                if (Conditions.eq(ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreTpOgg(), "AD")) {
                    // COB_CODE: IF IVVC0213-ID-ADESIONE
                    //            = DPRE-ID-OGG(IX-TAB-PRE)
                    //                TO LVVC0000-DATA-INPUT-1
                    //           END-IF
                    if (ivvc0213.getIdAdesione() == ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreIdOgg()) {
                        // COB_CODE: SET WK-LETTO-SI              TO TRUE
                        ws.getWkFindLetto().setSi();
                        // COB_CODE: MOVE DPRE-DT-DECOR-PREST(IX-TAB-PRE)
                        //             TO LVVC0000-DATA-INPUT-1
                        ws.getInputLvvs0000().setDataInput1(TruncAbs.toInt(ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreDtDecorPrest().getWpreDtDecorPrest(), 8));
                    }
                }
                ws.setIxTabPre(Trunc.toShort(ws.getIxTabPre() + 1, 4));
            }
        }
    }

    /**Original name: S3027-RAN-POL<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORA  RAPP ANA GAR
	 * ----------------------------------------------------------------*</pre>*/
    private void s3027RanPol() {
        // COB_CODE: SET WK-LETTO-NO                TO TRUE.
        ws.getWkFindLetto().setNo();
        //
        // COB_CODE: PERFORM VARYING IX-TAB-PRE FROM 1 BY 1
        //             UNTIL IX-TAB-PRE > DPRE-ELE-PREST-MAX
        //                OR WK-LETTO-SI
        //                END-IF
        //           END-PERFORM.
        ws.setIxTabPre(((short)1));
        while (!(ws.getIxTabPre() > ws.getDpreElePrestMax() || ws.getWkFindLetto().isSi())) {
            // COB_CODE: IF DPRE-TP-OGG(IX-TAB-PRE) = 'PO'
            //             END-IF
            //           END-IF
            if (Conditions.eq(ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreTpOgg(), "PO")) {
                // COB_CODE: IF IVVC0213-ID-POL-GAR
                //            = DPRE-ID-OGG(IX-TAB-PRE)
                //                TO LVVC0000-DATA-INPUT-1
                //           END-IF
                if (ivvc0213.getIdPolGar() == ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreIdOgg()) {
                    // COB_CODE: SET WK-LETTO-SI              TO TRUE
                    ws.getWkFindLetto().setSi();
                    // COB_CODE: MOVE DPRE-DT-DECOR-PREST(IX-TAB-PRE)
                    //             TO LVVC0000-DATA-INPUT-1
                    ws.getInputLvvs0000().setDataInput1(TruncAbs.toInt(ws.getDpreTabPre(ws.getIxTabPre()).getLccvpre1().getDati().getWpreDtDecorPrest().getWpreDtDecorPrest(), 8));
                }
            }
            ws.setIxTabPre(Trunc.toShort(ws.getIxTabPre() + 1, 4));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-PERC-O.
        ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(0, 14, 9));
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabPre(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoPre() {
        ws.setDpreElePrestMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0017Data.DPRE_TAB_PRE_MAXOCCURS; idx0++) {
            ws.getDpreTabPre(idx0).getLccvpre1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDpreTabPre(idx0).getLccvpre1().setIdPtf(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreIdPrest(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreIdOgg(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreTpOgg("");
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreIdMoviCrz(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreIdMoviChiu().setWpreIdMoviChiu(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDtIniEff(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDtEndEff(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreCodCompAnia(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreDtConcsPrest().setWpreDtConcsPrest(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreDtDecorPrest().setWpreDtDecorPrest(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreImpPrest().setWpreImpPrest(new AfDecimal(0, 15, 3));
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreIntrPrest().setWpreIntrPrest(new AfDecimal(0, 6, 3));
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreTpPrest("");
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreFrazPagIntr().setWpreFrazPagIntr(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreDtRimb().setWpreDtRimb(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreImpRimb().setWpreImpRimb(new AfDecimal(0, 15, 3));
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreCodDvs("");
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDtRichPrest(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreModIntrPrest("");
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreSpePrest().setWpreSpePrest(new AfDecimal(0, 15, 3));
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreImpPrestLiqto().setWpreImpPrestLiqto(new AfDecimal(0, 15, 3));
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreSdoIntr().setWpreSdoIntr(new AfDecimal(0, 15, 3));
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWpreRimbEff().setWpreRimbEff(new AfDecimal(0, 15, 3));
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().getWprePrestResEff().setWprePrestResEff(new AfDecimal(0, 15, 3));
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDsRiga(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDsOperSql(Types.SPACE_CHAR);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDsVer(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDsTsIniCptz(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDsTsEndCptz(0);
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDsUtente("");
            ws.getDpreTabPre(idx0).getLccvpre1().getDati().setWpreDsStatoElab(Types.SPACE_CHAR);
        }
    }

    public void initWkDataX12() {
        ws.getWkDataX12().setxAa("");
        ws.getWkDataX12().setVirogla(Types.SPACE_CHAR);
        ws.getWkDataX12().setxGg("");
    }

    public void initInputLvvs0000() {
        ws.getInputLvvs0000().getFormatDate().setFormatDate(Types.SPACE_CHAR);
        ws.getInputLvvs0000().setDataInputFormatted("00000000");
        ws.getInputLvvs0000().setDataInput1Formatted("00000000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000AnniInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000MesiInput2Formatted("00000");
        ws.getInputLvvs0000().getDatiInput2().setLvvc0000GiorniInput2Formatted("00000");
        ws.getInputLvvs0000().setAnniInput3Formatted("00000");
        ws.getInputLvvs0000().setMesiInput3Formatted("00000");
        ws.getInputLvvs0000().setDataOutput(new AfDecimal(0, 11, 7));
    }
}
