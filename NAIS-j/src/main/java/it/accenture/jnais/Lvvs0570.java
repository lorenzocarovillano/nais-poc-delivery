package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0570Data;

/**Original name: LVVS0570<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2010.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0570
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE CODICE SOGGETTO DA STRINGA A
 *                   NUMERICO
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0570 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0570Data ws = new Lvvs0570Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0570
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0570_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000.
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0570 getInstance() {
        return ((Lvvs0570)Programs.getInstance(Lvvs0570.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // Conversione da stringa a numerico
        // COB_CODE: PERFORM S1200-CONVERTI-CHAR-NUM
        //              THRU S1200-CONVERTI-CHAR-NUM-EX.
        s1200ConvertiCharNum();
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-RAPP-ANAG
        //                TO DRAN-AREA-RAPP-ANA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasRappAnag())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DRAN-AREA-RAPP-ANA
            ws.setDranAreaRappAnaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONVERTI-CHAR-NUM<br>
	 * <pre>----------------------------------------------------------------*
	 *   CONVERTI VALORE DA STRINGA IN NUMERICO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ConvertiCharNum() {
        Iwfs0050 iwfs0050 = null;
        // COB_CODE: MOVE HIGH-VALUE            TO AREA-CALL-IWFS0050.
        ws.getAreaCallIwfs0050().initAreaCallIwfs0050HighValues();
        // COB_CODE: MOVE DRAN-COD-SOGG(IVVC0213-IX-TABB)
        //                                      TO IWFI0051-ARRAY-STRINGA-INPUT.
        ws.getAreaCallIwfs0050().setIwfi0051ArrayStringaInputFormatted(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getDranCodSoggFormatted());
        // COB_CODE: CALL IWFS0050              USING AREA-CALL-IWFS0050.
        iwfs0050 = Iwfs0050.getInstance();
        iwfs0050.run(ws.getAreaCallIwfs0050());
        // COB_CODE: IF IWFO0051-ESITO-OK
        //                                      TO IVVC0213-VAL-IMP-O
        //           END-IF.
        if (ws.getAreaCallIwfs0050().getIwfo0051().getEsito().isOk()) {
            // COB_CODE: MOVE IWFO0051-CAMPO-OUTPUT-DEFI
            //                                   TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getAreaCallIwfs0050().getIwfo0051().getCampoOutputDefi(), 18, 7));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }
}
