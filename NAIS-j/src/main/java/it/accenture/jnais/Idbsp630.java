package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.AccCommDao;
import it.accenture.jnais.commons.data.to.IAccComm;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.AccComm;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsp630Data;
import it.accenture.jnais.ws.Idsv0003;

/**Original name: IDBSP630<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  24 GEN 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsp630 extends Program implements IAccComm {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private AccCommDao accCommDao = new AccCommDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsp630Data ws = new Idbsp630Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: ACC-COMM
    private AccComm accComm;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSP630_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, AccComm accComm) {
        this.idsv0003 = idsv0003;
        this.accComm = accComm;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsp630 getInstance() {
        return ((Idbsp630)Programs.getInstance(Idbsp630.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSP630'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSP630");
        // COB_CODE: MOVE 'ACC_COMM' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("ACC_COMM");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ACC_COMM
        //                ,TP_ACC_COMM
        //                ,COD_COMP_ANIA
        //                ,COD_PARTNER
        //                ,IB_ACC_COMM
        //                ,IB_ACC_COMM_MASTER
        //                ,DESC_ACC_COMM
        //                ,DT_FIRMA
        //                ,DT_INI_VLDT
        //                ,DT_END_VLDT
        //                ,FL_INVIO_CONFERME
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DESC_ACC_COMM_MAST
        //             INTO
        //                :P63-ID-ACC-COMM
        //               ,:P63-TP-ACC-COMM
        //               ,:P63-COD-COMP-ANIA
        //               ,:P63-COD-PARTNER
        //               ,:P63-IB-ACC-COMM
        //               ,:P63-IB-ACC-COMM-MASTER
        //               ,:P63-DESC-ACC-COMM-VCHAR
        //               ,:P63-DT-FIRMA-DB
        //               ,:P63-DT-INI-VLDT-DB
        //               ,:P63-DT-END-VLDT-DB
        //               ,:P63-FL-INVIO-CONFERME
        //               ,:P63-DS-OPER-SQL
        //               ,:P63-DS-VER
        //               ,:P63-DS-TS-CPTZ
        //               ,:P63-DS-UTENTE
        //               ,:P63-DS-STATO-ELAB
        //               ,:P63-DESC-ACC-COMM-MAST-VCHAR
        //                :IND-P63-DESC-ACC-COMM-MAST
        //             FROM ACC_COMM
        //             WHERE     ID_ACC_COMM = :P63-ID-ACC-COMM
        //           END-EXEC.
        accCommDao.selectByP63IdAccComm(accComm.getIdAccComm(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO ACC_COMM
            //                  (
            //                     ID_ACC_COMM
            //                    ,TP_ACC_COMM
            //                    ,COD_COMP_ANIA
            //                    ,COD_PARTNER
            //                    ,IB_ACC_COMM
            //                    ,IB_ACC_COMM_MASTER
            //                    ,DESC_ACC_COMM
            //                    ,DT_FIRMA
            //                    ,DT_INI_VLDT
            //                    ,DT_END_VLDT
            //                    ,FL_INVIO_CONFERME
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,DESC_ACC_COMM_MAST
            //                  )
            //              VALUES
            //                  (
            //                    :P63-ID-ACC-COMM
            //                    ,:P63-TP-ACC-COMM
            //                    ,:P63-COD-COMP-ANIA
            //                    ,:P63-COD-PARTNER
            //                    ,:P63-IB-ACC-COMM
            //                    ,:P63-IB-ACC-COMM-MASTER
            //                    ,:P63-DESC-ACC-COMM-VCHAR
            //                    ,:P63-DT-FIRMA-DB
            //                    ,:P63-DT-INI-VLDT-DB
            //                    ,:P63-DT-END-VLDT-DB
            //                    ,:P63-FL-INVIO-CONFERME
            //                    ,:P63-DS-OPER-SQL
            //                    ,:P63-DS-VER
            //                    ,:P63-DS-TS-CPTZ
            //                    ,:P63-DS-UTENTE
            //                    ,:P63-DS-STATO-ELAB
            //                    ,:P63-DESC-ACC-COMM-MAST-VCHAR
            //                     :IND-P63-DESC-ACC-COMM-MAST
            //                  )
            //           END-EXEC
            accCommDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE ACC_COMM SET
        //                   ID_ACC_COMM            =
        //                :P63-ID-ACC-COMM
        //                  ,TP_ACC_COMM            =
        //                :P63-TP-ACC-COMM
        //                  ,COD_COMP_ANIA          =
        //                :P63-COD-COMP-ANIA
        //                  ,COD_PARTNER            =
        //                :P63-COD-PARTNER
        //                  ,IB_ACC_COMM            =
        //                :P63-IB-ACC-COMM
        //                  ,IB_ACC_COMM_MASTER     =
        //                :P63-IB-ACC-COMM-MASTER
        //                  ,DESC_ACC_COMM          =
        //                :P63-DESC-ACC-COMM-VCHAR
        //                  ,DT_FIRMA               =
        //           :P63-DT-FIRMA-DB
        //                  ,DT_INI_VLDT            =
        //           :P63-DT-INI-VLDT-DB
        //                  ,DT_END_VLDT            =
        //           :P63-DT-END-VLDT-DB
        //                  ,FL_INVIO_CONFERME      =
        //                :P63-FL-INVIO-CONFERME
        //                  ,DS_OPER_SQL            =
        //                :P63-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :P63-DS-VER
        //                  ,DS_TS_CPTZ             =
        //                :P63-DS-TS-CPTZ
        //                  ,DS_UTENTE              =
        //                :P63-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :P63-DS-STATO-ELAB
        //                  ,DESC_ACC_COMM_MAST     =
        //                :P63-DESC-ACC-COMM-MAST-VCHAR
        //                                       :IND-P63-DESC-ACC-COMM-MAST
        //                WHERE     ID_ACC_COMM = :P63-ID-ACC-COMM
        //           END-EXEC.
        accCommDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM ACC_COMM
        //                WHERE     ID_ACC_COMM = :P63-ID-ACC-COMM
        //           END-EXEC.
        accCommDao.deleteByP63IdAccComm(accComm.getIdAccComm());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DCL-CUR-IBS-ACC-COMM<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DclCurIbsAccComm() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-NST-P63-0 CURSOR FOR
    //              SELECT
    //                     ID_ACC_COMM
    //                    ,TP_ACC_COMM
    //                    ,COD_COMP_ANIA
    //                    ,COD_PARTNER
    //                    ,IB_ACC_COMM
    //                    ,IB_ACC_COMM_MASTER
    //                    ,DESC_ACC_COMM
    //                    ,DT_FIRMA
    //                    ,DT_INI_VLDT
    //                    ,DT_END_VLDT
    //                    ,FL_INVIO_CONFERME
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,DESC_ACC_COMM_MAST
    //              FROM ACC_COMM
    //              WHERE     IB_ACC_COMM = :P63-IB-ACC-COMM
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //              ORDER BY ID_ACC_COMM ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DCL-CUR-IBS-ACC-COMM-MAS<br>*/
    private void a605DclCurIbsAccCommMas() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-NST-P63-1 CURSOR FOR
    //              SELECT
    //                     ID_ACC_COMM
    //                    ,TP_ACC_COMM
    //                    ,COD_COMP_ANIA
    //                    ,COD_PARTNER
    //                    ,IB_ACC_COMM
    //                    ,IB_ACC_COMM_MASTER
    //                    ,DESC_ACC_COMM
    //                    ,DT_FIRMA
    //                    ,DT_INI_VLDT
    //                    ,DT_END_VLDT
    //                    ,FL_INVIO_CONFERME
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,DESC_ACC_COMM_MAST
    //              FROM ACC_COMM
    //              WHERE     IB_ACC_COMM_MASTER = :P63-IB-ACC-COMM-MASTER
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //              ORDER BY ID_ACC_COMM ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF P63-IB-ACC-COMM NOT = HIGH-VALUES
        //                  THRU A605-ACC-COMM-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(accComm.getIbAccComm(), AccComm.Len.IB_ACC_COMM)) {
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-ACC-COMM
            //              THRU A605-ACC-COMM-EX
            a605DclCurIbsAccComm();
        }
        else if (!Characters.EQ_HIGH.test(accComm.getIbAccCommMaster(), AccComm.Len.IB_ACC_COMM_MASTER)) {
            // COB_CODE: IF P63-IB-ACC-COMM-MASTER NOT = HIGH-VALUES
            //                  THRU A605-ACC-COMM-MASTER-EX
            //           END-IF
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-ACC-COMM-MAS
            //              THRU A605-ACC-COMM-MASTER-EX
            a605DclCurIbsAccCommMas();
        }
    }

    /**Original name: A610-SELECT-IBS-ACC-COMM<br>*/
    private void a610SelectIbsAccComm() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ACC_COMM
        //                ,TP_ACC_COMM
        //                ,COD_COMP_ANIA
        //                ,COD_PARTNER
        //                ,IB_ACC_COMM
        //                ,IB_ACC_COMM_MASTER
        //                ,DESC_ACC_COMM
        //                ,DT_FIRMA
        //                ,DT_INI_VLDT
        //                ,DT_END_VLDT
        //                ,FL_INVIO_CONFERME
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DESC_ACC_COMM_MAST
        //             INTO
        //                :P63-ID-ACC-COMM
        //               ,:P63-TP-ACC-COMM
        //               ,:P63-COD-COMP-ANIA
        //               ,:P63-COD-PARTNER
        //               ,:P63-IB-ACC-COMM
        //               ,:P63-IB-ACC-COMM-MASTER
        //               ,:P63-DESC-ACC-COMM-VCHAR
        //               ,:P63-DT-FIRMA-DB
        //               ,:P63-DT-INI-VLDT-DB
        //               ,:P63-DT-END-VLDT-DB
        //               ,:P63-FL-INVIO-CONFERME
        //               ,:P63-DS-OPER-SQL
        //               ,:P63-DS-VER
        //               ,:P63-DS-TS-CPTZ
        //               ,:P63-DS-UTENTE
        //               ,:P63-DS-STATO-ELAB
        //               ,:P63-DESC-ACC-COMM-MAST-VCHAR
        //                :IND-P63-DESC-ACC-COMM-MAST
        //             FROM ACC_COMM
        //             WHERE     IB_ACC_COMM = :P63-IB-ACC-COMM
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        accCommDao.selectRec(accComm.getIbAccComm(), idsv0003.getCodiceCompagniaAnia(), this);
    }

    /**Original name: A610-SELECT-IBS-ACC-COMM-MAS<br>*/
    private void a610SelectIbsAccCommMas() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ACC_COMM
        //                ,TP_ACC_COMM
        //                ,COD_COMP_ANIA
        //                ,COD_PARTNER
        //                ,IB_ACC_COMM
        //                ,IB_ACC_COMM_MASTER
        //                ,DESC_ACC_COMM
        //                ,DT_FIRMA
        //                ,DT_INI_VLDT
        //                ,DT_END_VLDT
        //                ,FL_INVIO_CONFERME
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,DESC_ACC_COMM_MAST
        //             INTO
        //                :P63-ID-ACC-COMM
        //               ,:P63-TP-ACC-COMM
        //               ,:P63-COD-COMP-ANIA
        //               ,:P63-COD-PARTNER
        //               ,:P63-IB-ACC-COMM
        //               ,:P63-IB-ACC-COMM-MASTER
        //               ,:P63-DESC-ACC-COMM-VCHAR
        //               ,:P63-DT-FIRMA-DB
        //               ,:P63-DT-INI-VLDT-DB
        //               ,:P63-DT-END-VLDT-DB
        //               ,:P63-FL-INVIO-CONFERME
        //               ,:P63-DS-OPER-SQL
        //               ,:P63-DS-VER
        //               ,:P63-DS-TS-CPTZ
        //               ,:P63-DS-UTENTE
        //               ,:P63-DS-STATO-ELAB
        //               ,:P63-DESC-ACC-COMM-MAST-VCHAR
        //                :IND-P63-DESC-ACC-COMM-MAST
        //             FROM ACC_COMM
        //             WHERE     IB_ACC_COMM_MASTER = :P63-IB-ACC-COMM-MASTER
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //           END-EXEC.
        accCommDao.selectRec1(accComm.getIbAccCommMaster(), idsv0003.getCodiceCompagniaAnia(), this);
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF P63-IB-ACC-COMM NOT = HIGH-VALUES
        //                  THRU A610-ACC-COMM-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(accComm.getIbAccComm(), AccComm.Len.IB_ACC_COMM)) {
            // COB_CODE: PERFORM A610-SELECT-IBS-ACC-COMM
            //              THRU A610-ACC-COMM-EX
            a610SelectIbsAccComm();
        }
        else if (!Characters.EQ_HIGH.test(accComm.getIbAccCommMaster(), AccComm.Len.IB_ACC_COMM_MASTER)) {
            // COB_CODE: IF P63-IB-ACC-COMM-MASTER NOT = HIGH-VALUES
            //                  THRU A610-ACC-COMM-MASTER-EX
            //           END-IF
            // COB_CODE: PERFORM A610-SELECT-IBS-ACC-COMM-MAS
            //              THRU A610-ACC-COMM-MASTER-EX
            a610SelectIbsAccCommMas();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: IF P63-IB-ACC-COMM NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(accComm.getIbAccComm(), AccComm.Len.IB_ACC_COMM)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-NST-P63-0
            //           END-EXEC
            accCommDao.openCIbsNstP630(accComm.getIbAccComm(), idsv0003.getCodiceCompagniaAnia());
        }
        else if (!Characters.EQ_HIGH.test(accComm.getIbAccCommMaster(), AccComm.Len.IB_ACC_COMM_MASTER)) {
            // COB_CODE: IF P63-IB-ACC-COMM-MASTER NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-NST-P63-1
            //           END-EXEC
            accCommDao.openCIbsNstP631(accComm.getIbAccCommMaster(), idsv0003.getCodiceCompagniaAnia());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: IF P63-IB-ACC-COMM NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(accComm.getIbAccComm(), AccComm.Len.IB_ACC_COMM)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-NST-P63-0
            //           END-EXEC
            accCommDao.closeCIbsNstP630();
        }
        else if (!Characters.EQ_HIGH.test(accComm.getIbAccCommMaster(), AccComm.Len.IB_ACC_COMM_MASTER)) {
            // COB_CODE: IF P63-IB-ACC-COMM-MASTER NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-NST-P63-1
            //           END-EXEC
            accCommDao.closeCIbsNstP631();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FN-IBS-ACC-COMM<br>*/
    private void a690FnIbsAccComm() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-NST-P63-0
        //           INTO
        //                :P63-ID-ACC-COMM
        //               ,:P63-TP-ACC-COMM
        //               ,:P63-COD-COMP-ANIA
        //               ,:P63-COD-PARTNER
        //               ,:P63-IB-ACC-COMM
        //               ,:P63-IB-ACC-COMM-MASTER
        //               ,:P63-DESC-ACC-COMM-VCHAR
        //               ,:P63-DT-FIRMA-DB
        //               ,:P63-DT-INI-VLDT-DB
        //               ,:P63-DT-END-VLDT-DB
        //               ,:P63-FL-INVIO-CONFERME
        //               ,:P63-DS-OPER-SQL
        //               ,:P63-DS-VER
        //               ,:P63-DS-TS-CPTZ
        //               ,:P63-DS-UTENTE
        //               ,:P63-DS-STATO-ELAB
        //               ,:P63-DESC-ACC-COMM-MAST-VCHAR
        //                :IND-P63-DESC-ACC-COMM-MAST
        //           END-EXEC.
        accCommDao.fetchCIbsNstP630(this);
    }

    /**Original name: A690-FN-IBS-ACC-COMM-MASTER<br>*/
    private void a690FnIbsAccCommMaster() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-NST-P63-1
        //           INTO
        //                :P63-ID-ACC-COMM
        //               ,:P63-TP-ACC-COMM
        //               ,:P63-COD-COMP-ANIA
        //               ,:P63-COD-PARTNER
        //               ,:P63-IB-ACC-COMM
        //               ,:P63-IB-ACC-COMM-MASTER
        //               ,:P63-DESC-ACC-COMM-VCHAR
        //               ,:P63-DT-FIRMA-DB
        //               ,:P63-DT-INI-VLDT-DB
        //               ,:P63-DT-END-VLDT-DB
        //               ,:P63-FL-INVIO-CONFERME
        //               ,:P63-DS-OPER-SQL
        //               ,:P63-DS-VER
        //               ,:P63-DS-TS-CPTZ
        //               ,:P63-DS-UTENTE
        //               ,:P63-DS-STATO-ELAB
        //               ,:P63-DESC-ACC-COMM-MAST-VCHAR
        //                :IND-P63-DESC-ACC-COMM-MAST
        //           END-EXEC.
        accCommDao.fetchCIbsNstP631(this);
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: IF P63-IB-ACC-COMM NOT = HIGH-VALUES
        //                  THRU A690-ACC-COMM-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(accComm.getIbAccComm(), AccComm.Len.IB_ACC_COMM)) {
            // COB_CODE: PERFORM A690-FN-IBS-ACC-COMM
            //              THRU A690-ACC-COMM-EX
            a690FnIbsAccComm();
        }
        else if (!Characters.EQ_HIGH.test(accComm.getIbAccCommMaster(), AccComm.Len.IB_ACC_COMM_MASTER)) {
            // COB_CODE: IF P63-IB-ACC-COMM-MASTER NOT = HIGH-VALUES
            //                  THRU A690-ACC-COMM-MASTER-EX
            //           END-IF
            // COB_CODE: PERFORM A690-FN-IBS-ACC-COMM-MASTER
            //              THRU A690-ACC-COMM-MASTER-EX
            a690FnIbsAccCommMaster();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX
            a670CloseCursorIbs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-P63-DESC-ACC-COMM-MAST = -1
        //              MOVE HIGH-VALUES TO P63-DESC-ACC-COMM-MAST
        //           END-IF.
        if (ws.getIndP63DescAccCommMast() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P63-DESC-ACC-COMM-MAST
            accComm.setDescAccCommMast(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AccComm.Len.DESC_ACC_COMM_MAST));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO P63-DS-OPER-SQL
        accComm.setDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO P63-DS-VER
        accComm.setDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P63-DS-UTENTE
        accComm.setDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO P63-DS-STATO-ELAB
        accComm.setDsStatoElabFormatted("1");
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO P63-DS-TS-CPTZ.
        accComm.setDsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO P63-DS-OPER-SQL
        accComm.setDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P63-DS-UTENTE
        accComm.setDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR TO P63-DS-TS-CPTZ.
        accComm.setDsTsCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF P63-DESC-ACC-COMM-MAST = HIGH-VALUES
        //              MOVE -1 TO IND-P63-DESC-ACC-COMM-MAST
        //           ELSE
        //              MOVE 0 TO IND-P63-DESC-ACC-COMM-MAST
        //           END-IF.
        if (Characters.EQ_HIGH.test(accComm.getDescAccCommMast(), AccComm.Len.DESC_ACC_COMM_MAST)) {
            // COB_CODE: MOVE -1 TO IND-P63-DESC-ACC-COMM-MAST
            ws.setIndP63DescAccCommMast(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P63-DESC-ACC-COMM-MAST
            ws.setIndP63DescAccCommMast(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE P63-DT-FIRMA TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(accComm.getDtFirma(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P63-DT-FIRMA-DB
        ws.getAccCommDb().setFirmaDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P63-DT-INI-VLDT TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(accComm.getDtIniVldt(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P63-DT-INI-VLDT-DB
        ws.getAccCommDb().setIniVldtDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P63-DT-END-VLDT TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(accComm.getDtEndVldt(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P63-DT-END-VLDT-DB.
        ws.getAccCommDb().setEndVldtDb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE P63-DT-FIRMA-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getAccCommDb().getFirmaDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P63-DT-FIRMA
        accComm.setDtFirma(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P63-DT-INI-VLDT-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getAccCommDb().getIniVldtDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P63-DT-INI-VLDT
        accComm.setDtIniVldt(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P63-DT-END-VLDT-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getAccCommDb().getEndVldtDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P63-DT-END-VLDT.
        accComm.setDtEndVldt(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF P63-DESC-ACC-COMM
        //                       TO P63-DESC-ACC-COMM-LEN
        accComm.setDescAccCommLen(((short)AccComm.Len.DESC_ACC_COMM));
        // COB_CODE: MOVE LENGTH OF P63-DESC-ACC-COMM-MAST
        //                       TO P63-DESC-ACC-COMM-MAST-LEN.
        accComm.setDescAccCommMastLen(((short)AccComm.Len.DESC_ACC_COMM_MAST));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return accComm.getCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.accComm.setCodCompAnia(codCompAnia);
    }

    @Override
    public long getCodPartner() {
        return accComm.getCodPartner();
    }

    @Override
    public void setCodPartner(long codPartner) {
        this.accComm.setCodPartner(codPartner);
    }

    @Override
    public String getDescAccCommMastVchar() {
        return accComm.getDescAccCommMastVcharFormatted();
    }

    @Override
    public void setDescAccCommMastVchar(String descAccCommMastVchar) {
        this.accComm.setDescAccCommMastVcharFormatted(descAccCommMastVchar);
    }

    @Override
    public String getDescAccCommMastVcharObj() {
        if (ws.getIndP63DescAccCommMast() >= 0) {
            return getDescAccCommMastVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDescAccCommMastVcharObj(String descAccCommMastVcharObj) {
        if (descAccCommMastVcharObj != null) {
            setDescAccCommMastVchar(descAccCommMastVcharObj);
            ws.setIndP63DescAccCommMast(((short)0));
        }
        else {
            ws.setIndP63DescAccCommMast(((short)-1));
        }
    }

    @Override
    public String getDescAccCommVchar() {
        return accComm.getDescAccCommVcharFormatted();
    }

    @Override
    public void setDescAccCommVchar(String descAccCommVchar) {
        this.accComm.setDescAccCommVcharFormatted(descAccCommVchar);
    }

    @Override
    public char getDsOperSql() {
        return accComm.getDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.accComm.setDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return accComm.getDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.accComm.setDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return accComm.getDsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.accComm.setDsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return accComm.getDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.accComm.setDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return accComm.getDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.accComm.setDsVer(dsVer);
    }

    @Override
    public String getDtEndVldtDb() {
        return ws.getAccCommDb().getEndVldtDb();
    }

    @Override
    public void setDtEndVldtDb(String dtEndVldtDb) {
        this.ws.getAccCommDb().setEndVldtDb(dtEndVldtDb);
    }

    @Override
    public String getDtFirmaDb() {
        return ws.getAccCommDb().getFirmaDb();
    }

    @Override
    public void setDtFirmaDb(String dtFirmaDb) {
        this.ws.getAccCommDb().setFirmaDb(dtFirmaDb);
    }

    @Override
    public String getDtIniVldtDb() {
        return ws.getAccCommDb().getIniVldtDb();
    }

    @Override
    public void setDtIniVldtDb(String dtIniVldtDb) {
        this.ws.getAccCommDb().setIniVldtDb(dtIniVldtDb);
    }

    @Override
    public char getFlInvioConferme() {
        return accComm.getFlInvioConferme();
    }

    @Override
    public void setFlInvioConferme(char flInvioConferme) {
        this.accComm.setFlInvioConferme(flInvioConferme);
    }

    @Override
    public String getIbAccComm() {
        return accComm.getIbAccComm();
    }

    @Override
    public void setIbAccComm(String ibAccComm) {
        this.accComm.setIbAccComm(ibAccComm);
    }

    @Override
    public String getIbAccCommMaster() {
        return accComm.getIbAccCommMaster();
    }

    @Override
    public void setIbAccCommMaster(String ibAccCommMaster) {
        this.accComm.setIbAccCommMaster(ibAccCommMaster);
    }

    @Override
    public int getP63IdAccComm() {
        return accComm.getIdAccComm();
    }

    @Override
    public void setP63IdAccComm(int p63IdAccComm) {
        this.accComm.setIdAccComm(p63IdAccComm);
    }

    @Override
    public String getTpAccComm() {
        return accComm.getTpAccComm();
    }

    @Override
    public void setTpAccComm(String tpAccComm) {
        this.accComm.setTpAccComm(tpAccComm);
    }
}
