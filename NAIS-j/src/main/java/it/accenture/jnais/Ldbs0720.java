package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.OggCollgDao;
import it.accenture.jnais.commons.data.to.IOggCollg;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs0720Data;
import it.accenture.jnais.ws.OggCollgIdbsoco0;
import it.accenture.jnais.ws.redefines.OcoCarAcq;
import it.accenture.jnais.ws.redefines.OcoDtDecor;
import it.accenture.jnais.ws.redefines.OcoDtScad;
import it.accenture.jnais.ws.redefines.OcoDtUltPrePag;
import it.accenture.jnais.ws.redefines.OcoIdMoviChiu;
import it.accenture.jnais.ws.redefines.OcoImpCollg;
import it.accenture.jnais.ws.redefines.OcoImpReinvst;
import it.accenture.jnais.ws.redefines.OcoImpTrasf;
import it.accenture.jnais.ws.redefines.OcoImpTrasferito;
import it.accenture.jnais.ws.redefines.OcoPcPreTrasferito;
import it.accenture.jnais.ws.redefines.OcoPcReinvstRilievi;
import it.accenture.jnais.ws.redefines.OcoPre1aAnnualita;
import it.accenture.jnais.ws.redefines.OcoPrePerTrasf;
import it.accenture.jnais.ws.redefines.OcoRecProv;
import it.accenture.jnais.ws.redefines.OcoRisMat;
import it.accenture.jnais.ws.redefines.OcoRisZil;
import it.accenture.jnais.ws.redefines.OcoTotPre;

/**Original name: LDBS0720<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  29 LUG 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs0720 extends Program implements IOggCollg {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private OggCollgDao oggCollgDao = new OggCollgDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs0720Data ws = new Ldbs0720Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: OGG-COLLG
    private OggCollgIdbsoco0 oggCollg;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS0720_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, OggCollgIdbsoco0 oggCollg) {
        this.idsv0003 = idsv0003;
        this.oggCollg = oggCollg;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV0721.
        ws.getLdbv0721().setLdbv0721Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV0721 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv0721().getLdbv0721Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs0720 getInstance() {
        return ((Ldbs0720)Programs.getInstance(Ldbs0720.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS0720'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS0720");
        // COB_CODE: MOVE 'OGG-COLLG' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("OGG-COLLG");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_OGG_COLLG
        //                    ,ID_OGG_COINV
        //                    ,TP_OGG_COINV
        //                    ,ID_OGG_DER
        //                    ,TP_OGG_DER
        //                    ,IB_RIFTO_ESTNO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_PROD
        //                    ,DT_SCAD
        //                    ,TP_COLLGM
        //                    ,TP_TRASF
        //                    ,REC_PROV
        //                    ,DT_DECOR
        //                    ,DT_ULT_PRE_PAG
        //                    ,TOT_PRE
        //                    ,RIS_MAT
        //                    ,RIS_ZIL
        //                    ,IMP_TRASF
        //                    ,IMP_REINVST
        //                    ,PC_REINVST_RILIEVI
        //                    ,IB_2O_RIFTO_ESTNO
        //                    ,IND_LIQ_AGG_MAN
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_COLLG
        //                    ,PRE_PER_TRASF
        //                    ,CAR_ACQ
        //                    ,PRE_1A_ANNUALITA
        //                    ,IMP_TRASFERITO
        //                    ,TP_MOD_ABBINAMENTO
        //                    ,PC_PRE_TRASFERITO
        //              FROM OGG_COLLG
        //              WHERE  ID_OGG_COINV = :LDBV0721-ID-OGG-COLLG
        //                    AND TP_OGG_COINV = :LDBV0721-TP-OGG-COLLG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_OGG_COLLG
        //                    ,ID_OGG_COINV
        //                    ,TP_OGG_COINV
        //                    ,ID_OGG_DER
        //                    ,TP_OGG_DER
        //                    ,IB_RIFTO_ESTNO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_PROD
        //                    ,DT_SCAD
        //                    ,TP_COLLGM
        //                    ,TP_TRASF
        //                    ,REC_PROV
        //                    ,DT_DECOR
        //                    ,DT_ULT_PRE_PAG
        //                    ,TOT_PRE
        //                    ,RIS_MAT
        //                    ,RIS_ZIL
        //                    ,IMP_TRASF
        //                    ,IMP_REINVST
        //                    ,PC_REINVST_RILIEVI
        //                    ,IB_2O_RIFTO_ESTNO
        //                    ,IND_LIQ_AGG_MAN
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_COLLG
        //                    ,PRE_PER_TRASF
        //                    ,CAR_ACQ
        //                    ,PRE_1A_ANNUALITA
        //                    ,IMP_TRASFERITO
        //                    ,TP_MOD_ABBINAMENTO
        //                    ,PC_PRE_TRASFERITO
        //             INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //             FROM OGG_COLLG
        //             WHERE  ID_OGG_COINV = :LDBV0721-ID-OGG-COLLG
        //                    AND TP_OGG_COINV = :LDBV0721-TP-OGG-COLLG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        oggCollgDao.selectRec6(ws.getLdbv0721().getLdbv0721IdOggCollg(), ws.getLdbv0721().getLdbv0721TpOggCollg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        oggCollgDao.openCEff3(ws.getLdbv0721().getLdbv0721IdOggCollg(), ws.getLdbv0721().getLdbv0721TpOggCollg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        oggCollgDao.closeCEff3();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //           END-EXEC.
        oggCollgDao.fetchCEff3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_OGG_COLLG
        //                    ,ID_OGG_COINV
        //                    ,TP_OGG_COINV
        //                    ,ID_OGG_DER
        //                    ,TP_OGG_DER
        //                    ,IB_RIFTO_ESTNO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_PROD
        //                    ,DT_SCAD
        //                    ,TP_COLLGM
        //                    ,TP_TRASF
        //                    ,REC_PROV
        //                    ,DT_DECOR
        //                    ,DT_ULT_PRE_PAG
        //                    ,TOT_PRE
        //                    ,RIS_MAT
        //                    ,RIS_ZIL
        //                    ,IMP_TRASF
        //                    ,IMP_REINVST
        //                    ,PC_REINVST_RILIEVI
        //                    ,IB_2O_RIFTO_ESTNO
        //                    ,IND_LIQ_AGG_MAN
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_COLLG
        //                    ,PRE_PER_TRASF
        //                    ,CAR_ACQ
        //                    ,PRE_1A_ANNUALITA
        //                    ,IMP_TRASFERITO
        //                    ,TP_MOD_ABBINAMENTO
        //                    ,PC_PRE_TRASFERITO
        //              FROM OGG_COLLG
        //              WHERE  ID_OGG_COINV = :LDBV0721-ID-OGG-COLLG
        //                        AND TP_OGG_COINV = :LDBV0721-TP-OGG-COLLG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_OGG_COLLG
        //                    ,ID_OGG_COINV
        //                    ,TP_OGG_COINV
        //                    ,ID_OGG_DER
        //                    ,TP_OGG_DER
        //                    ,IB_RIFTO_ESTNO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_PROD
        //                    ,DT_SCAD
        //                    ,TP_COLLGM
        //                    ,TP_TRASF
        //                    ,REC_PROV
        //                    ,DT_DECOR
        //                    ,DT_ULT_PRE_PAG
        //                    ,TOT_PRE
        //                    ,RIS_MAT
        //                    ,RIS_ZIL
        //                    ,IMP_TRASF
        //                    ,IMP_REINVST
        //                    ,PC_REINVST_RILIEVI
        //                    ,IB_2O_RIFTO_ESTNO
        //                    ,IND_LIQ_AGG_MAN
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_COLLG
        //                    ,PRE_PER_TRASF
        //                    ,CAR_ACQ
        //                    ,PRE_1A_ANNUALITA
        //                    ,IMP_TRASFERITO
        //                    ,TP_MOD_ABBINAMENTO
        //                    ,PC_PRE_TRASFERITO
        //             INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //             FROM OGG_COLLG
        //             WHERE  ID_OGG_COINV = :LDBV0721-ID-OGG-COLLG
        //                    AND TP_OGG_COINV = :LDBV0721-TP-OGG-COLLG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        oggCollgDao.selectRec7(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        oggCollgDao.openCCpz3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        oggCollgDao.closeCCpz3();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :OCO-ID-OGG-COLLG
        //               ,:OCO-ID-OGG-COINV
        //               ,:OCO-TP-OGG-COINV
        //               ,:OCO-ID-OGG-DER
        //               ,:OCO-TP-OGG-DER
        //               ,:OCO-IB-RIFTO-ESTNO
        //                :IND-OCO-IB-RIFTO-ESTNO
        //               ,:OCO-ID-MOVI-CRZ
        //               ,:OCO-ID-MOVI-CHIU
        //                :IND-OCO-ID-MOVI-CHIU
        //               ,:OCO-DT-INI-EFF-DB
        //               ,:OCO-DT-END-EFF-DB
        //               ,:OCO-COD-COMP-ANIA
        //               ,:OCO-COD-PROD
        //               ,:OCO-DT-SCAD-DB
        //                :IND-OCO-DT-SCAD
        //               ,:OCO-TP-COLLGM
        //               ,:OCO-TP-TRASF
        //                :IND-OCO-TP-TRASF
        //               ,:OCO-REC-PROV
        //                :IND-OCO-REC-PROV
        //               ,:OCO-DT-DECOR-DB
        //                :IND-OCO-DT-DECOR
        //               ,:OCO-DT-ULT-PRE-PAG-DB
        //                :IND-OCO-DT-ULT-PRE-PAG
        //               ,:OCO-TOT-PRE
        //                :IND-OCO-TOT-PRE
        //               ,:OCO-RIS-MAT
        //                :IND-OCO-RIS-MAT
        //               ,:OCO-RIS-ZIL
        //                :IND-OCO-RIS-ZIL
        //               ,:OCO-IMP-TRASF
        //                :IND-OCO-IMP-TRASF
        //               ,:OCO-IMP-REINVST
        //                :IND-OCO-IMP-REINVST
        //               ,:OCO-PC-REINVST-RILIEVI
        //                :IND-OCO-PC-REINVST-RILIEVI
        //               ,:OCO-IB-2O-RIFTO-ESTNO
        //                :IND-OCO-IB-2O-RIFTO-ESTNO
        //               ,:OCO-IND-LIQ-AGG-MAN
        //                :IND-OCO-IND-LIQ-AGG-MAN
        //               ,:OCO-DS-RIGA
        //               ,:OCO-DS-OPER-SQL
        //               ,:OCO-DS-VER
        //               ,:OCO-DS-TS-INI-CPTZ
        //               ,:OCO-DS-TS-END-CPTZ
        //               ,:OCO-DS-UTENTE
        //               ,:OCO-DS-STATO-ELAB
        //               ,:OCO-IMP-COLLG
        //                :IND-OCO-IMP-COLLG
        //               ,:OCO-PRE-PER-TRASF
        //                :IND-OCO-PRE-PER-TRASF
        //               ,:OCO-CAR-ACQ
        //                :IND-OCO-CAR-ACQ
        //               ,:OCO-PRE-1A-ANNUALITA
        //                :IND-OCO-PRE-1A-ANNUALITA
        //               ,:OCO-IMP-TRASFERITO
        //                :IND-OCO-IMP-TRASFERITO
        //               ,:OCO-TP-MOD-ABBINAMENTO
        //                :IND-OCO-TP-MOD-ABBINAMENTO
        //               ,:OCO-PC-PRE-TRASFERITO
        //                :IND-OCO-PC-PRE-TRASFERITO
        //           END-EXEC.
        oggCollgDao.fetchCCpz3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-OCO-IB-RIFTO-ESTNO = -1
        //              MOVE HIGH-VALUES TO OCO-IB-RIFTO-ESTNO-NULL
        //           END-IF
        if (ws.getIndOggCollg().getIbRiftoEstno() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IB-RIFTO-ESTNO-NULL
            oggCollg.setOcoIbRiftoEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OggCollgIdbsoco0.Len.OCO_IB_RIFTO_ESTNO));
        }
        // COB_CODE: IF IND-OCO-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO OCO-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndOggCollg().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-ID-MOVI-CHIU-NULL
            oggCollg.getOcoIdMoviChiu().setOcoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoIdMoviChiu.Len.OCO_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-OCO-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO OCO-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndOggCollg().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-DT-SCAD-NULL
            oggCollg.getOcoDtScad().setOcoDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoDtScad.Len.OCO_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-OCO-TP-TRASF = -1
        //              MOVE HIGH-VALUES TO OCO-TP-TRASF-NULL
        //           END-IF
        if (ws.getIndOggCollg().getTpTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-TP-TRASF-NULL
            oggCollg.setOcoTpTrasf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OggCollgIdbsoco0.Len.OCO_TP_TRASF));
        }
        // COB_CODE: IF IND-OCO-REC-PROV = -1
        //              MOVE HIGH-VALUES TO OCO-REC-PROV-NULL
        //           END-IF
        if (ws.getIndOggCollg().getRecProv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-REC-PROV-NULL
            oggCollg.getOcoRecProv().setOcoRecProvNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoRecProv.Len.OCO_REC_PROV_NULL));
        }
        // COB_CODE: IF IND-OCO-DT-DECOR = -1
        //              MOVE HIGH-VALUES TO OCO-DT-DECOR-NULL
        //           END-IF
        if (ws.getIndOggCollg().getDtDecor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-DT-DECOR-NULL
            oggCollg.getOcoDtDecor().setOcoDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoDtDecor.Len.OCO_DT_DECOR_NULL));
        }
        // COB_CODE: IF IND-OCO-DT-ULT-PRE-PAG = -1
        //              MOVE HIGH-VALUES TO OCO-DT-ULT-PRE-PAG-NULL
        //           END-IF
        if (ws.getIndOggCollg().getDtUltPrePag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-DT-ULT-PRE-PAG-NULL
            oggCollg.getOcoDtUltPrePag().setOcoDtUltPrePagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoDtUltPrePag.Len.OCO_DT_ULT_PRE_PAG_NULL));
        }
        // COB_CODE: IF IND-OCO-TOT-PRE = -1
        //              MOVE HIGH-VALUES TO OCO-TOT-PRE-NULL
        //           END-IF
        if (ws.getIndOggCollg().getTotPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-TOT-PRE-NULL
            oggCollg.getOcoTotPre().setOcoTotPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoTotPre.Len.OCO_TOT_PRE_NULL));
        }
        // COB_CODE: IF IND-OCO-RIS-MAT = -1
        //              MOVE HIGH-VALUES TO OCO-RIS-MAT-NULL
        //           END-IF
        if (ws.getIndOggCollg().getRisMat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-RIS-MAT-NULL
            oggCollg.getOcoRisMat().setOcoRisMatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoRisMat.Len.OCO_RIS_MAT_NULL));
        }
        // COB_CODE: IF IND-OCO-RIS-ZIL = -1
        //              MOVE HIGH-VALUES TO OCO-RIS-ZIL-NULL
        //           END-IF
        if (ws.getIndOggCollg().getRisZil() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-RIS-ZIL-NULL
            oggCollg.getOcoRisZil().setOcoRisZilNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoRisZil.Len.OCO_RIS_ZIL_NULL));
        }
        // COB_CODE: IF IND-OCO-IMP-TRASF = -1
        //              MOVE HIGH-VALUES TO OCO-IMP-TRASF-NULL
        //           END-IF
        if (ws.getIndOggCollg().getImpTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IMP-TRASF-NULL
            oggCollg.getOcoImpTrasf().setOcoImpTrasfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoImpTrasf.Len.OCO_IMP_TRASF_NULL));
        }
        // COB_CODE: IF IND-OCO-IMP-REINVST = -1
        //              MOVE HIGH-VALUES TO OCO-IMP-REINVST-NULL
        //           END-IF
        if (ws.getIndOggCollg().getImpReinvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IMP-REINVST-NULL
            oggCollg.getOcoImpReinvst().setOcoImpReinvstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoImpReinvst.Len.OCO_IMP_REINVST_NULL));
        }
        // COB_CODE: IF IND-OCO-PC-REINVST-RILIEVI = -1
        //              MOVE HIGH-VALUES TO OCO-PC-REINVST-RILIEVI-NULL
        //           END-IF
        if (ws.getIndOggCollg().getPcReinvstRilievi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-PC-REINVST-RILIEVI-NULL
            oggCollg.getOcoPcReinvstRilievi().setOcoPcReinvstRilieviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoPcReinvstRilievi.Len.OCO_PC_REINVST_RILIEVI_NULL));
        }
        // COB_CODE: IF IND-OCO-IB-2O-RIFTO-ESTNO = -1
        //              MOVE HIGH-VALUES TO OCO-IB-2O-RIFTO-ESTNO-NULL
        //           END-IF
        if (ws.getIndOggCollg().getIb2oRiftoEstno() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IB-2O-RIFTO-ESTNO-NULL
            oggCollg.setOcoIb2oRiftoEstno(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OggCollgIdbsoco0.Len.OCO_IB2O_RIFTO_ESTNO));
        }
        // COB_CODE: IF IND-OCO-IND-LIQ-AGG-MAN = -1
        //              MOVE HIGH-VALUES TO OCO-IND-LIQ-AGG-MAN-NULL
        //           END-IF
        if (ws.getIndOggCollg().getIndLiqAggMan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IND-LIQ-AGG-MAN-NULL
            oggCollg.setOcoIndLiqAggMan(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-OCO-IMP-COLLG = -1
        //              MOVE HIGH-VALUES TO OCO-IMP-COLLG-NULL
        //           END-IF
        if (ws.getIndOggCollg().getImpCollg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IMP-COLLG-NULL
            oggCollg.getOcoImpCollg().setOcoImpCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoImpCollg.Len.OCO_IMP_COLLG_NULL));
        }
        // COB_CODE: IF IND-OCO-PRE-PER-TRASF = -1
        //              MOVE HIGH-VALUES TO OCO-PRE-PER-TRASF-NULL
        //           END-IF
        if (ws.getIndOggCollg().getPrePerTrasf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-PRE-PER-TRASF-NULL
            oggCollg.getOcoPrePerTrasf().setOcoPrePerTrasfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoPrePerTrasf.Len.OCO_PRE_PER_TRASF_NULL));
        }
        // COB_CODE: IF IND-OCO-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO OCO-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndOggCollg().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-CAR-ACQ-NULL
            oggCollg.getOcoCarAcq().setOcoCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoCarAcq.Len.OCO_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-OCO-PRE-1A-ANNUALITA = -1
        //              MOVE HIGH-VALUES TO OCO-PRE-1A-ANNUALITA-NULL
        //           END-IF
        if (ws.getIndOggCollg().getPre1aAnnualita() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-PRE-1A-ANNUALITA-NULL
            oggCollg.getOcoPre1aAnnualita().setOcoPre1aAnnualitaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoPre1aAnnualita.Len.OCO_PRE1A_ANNUALITA_NULL));
        }
        // COB_CODE: IF IND-OCO-IMP-TRASFERITO = -1
        //              MOVE HIGH-VALUES TO OCO-IMP-TRASFERITO-NULL
        //           END-IF
        if (ws.getIndOggCollg().getImpTrasferito() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-IMP-TRASFERITO-NULL
            oggCollg.getOcoImpTrasferito().setOcoImpTrasferitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoImpTrasferito.Len.OCO_IMP_TRASFERITO_NULL));
        }
        // COB_CODE: IF IND-OCO-TP-MOD-ABBINAMENTO = -1
        //              MOVE HIGH-VALUES TO OCO-TP-MOD-ABBINAMENTO-NULL
        //           END-IF
        if (ws.getIndOggCollg().getTpModAbbinamento() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-TP-MOD-ABBINAMENTO-NULL
            oggCollg.setOcoTpModAbbinamento(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OggCollgIdbsoco0.Len.OCO_TP_MOD_ABBINAMENTO));
        }
        // COB_CODE: IF IND-OCO-PC-PRE-TRASFERITO = -1
        //              MOVE HIGH-VALUES TO OCO-PC-PRE-TRASFERITO-NULL
        //           END-IF.
        if (ws.getIndOggCollg().getPcPreTrasferito() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO OCO-PC-PRE-TRASFERITO-NULL
            oggCollg.getOcoPcPreTrasferito().setOcoPcPreTrasferitoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, OcoPcPreTrasferito.Len.OCO_PC_PRE_TRASFERITO_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE OCO-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-INI-EFF
        oggCollg.setOcoDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE OCO-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-END-EFF
        oggCollg.setOcoDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-OCO-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO OCO-DT-SCAD
        //           END-IF
        if (ws.getIndOggCollg().getDtScad() == 0) {
            // COB_CODE: MOVE OCO-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-SCAD
            oggCollg.getOcoDtScad().setOcoDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-OCO-DT-DECOR = 0
        //               MOVE WS-DATE-N      TO OCO-DT-DECOR
        //           END-IF
        if (ws.getIndOggCollg().getDtDecor() == 0) {
            // COB_CODE: MOVE OCO-DT-DECOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-DECOR
            oggCollg.getOcoDtDecor().setOcoDtDecor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-OCO-DT-ULT-PRE-PAG = 0
        //               MOVE WS-DATE-N      TO OCO-DT-ULT-PRE-PAG
        //           END-IF.
        if (ws.getIndOggCollg().getDtUltPrePag() == 0) {
            // COB_CODE: MOVE OCO-DT-ULT-PRE-PAG-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getOggCollgDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO OCO-DT-ULT-PRE-PAG
            oggCollg.getOcoDtUltPrePag().setOcoDtUltPrePag(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getCarAcq() {
        return oggCollg.getOcoCarAcq().getOcoCarAcq();
    }

    @Override
    public void setCarAcq(AfDecimal carAcq) {
        this.oggCollg.getOcoCarAcq().setOcoCarAcq(carAcq.copy());
    }

    @Override
    public AfDecimal getCarAcqObj() {
        if (ws.getIndOggCollg().getCarAcq() >= 0) {
            return getCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarAcqObj(AfDecimal carAcqObj) {
        if (carAcqObj != null) {
            setCarAcq(new AfDecimal(carAcqObj, 15, 3));
            ws.getIndOggCollg().setCarAcq(((short)0));
        }
        else {
            ws.getIndOggCollg().setCarAcq(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return oggCollg.getOcoCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.oggCollg.setOcoCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodProd() {
        return oggCollg.getOcoCodProd();
    }

    @Override
    public void setCodProd(String codProd) {
        this.oggCollg.setOcoCodProd(codProd);
    }

    @Override
    public char getDsOperSql() {
        return oggCollg.getOcoDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.oggCollg.setOcoDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return oggCollg.getOcoDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.oggCollg.setOcoDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return oggCollg.getOcoDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.oggCollg.setOcoDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return oggCollg.getOcoDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.oggCollg.setOcoDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return oggCollg.getOcoDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.oggCollg.setOcoDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return oggCollg.getOcoDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.oggCollg.setOcoDsVer(dsVer);
    }

    @Override
    public String getDtDecorDb() {
        return ws.getOggCollgDb().getEndCopDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.ws.getOggCollgDb().setEndCopDb(dtDecorDb);
    }

    @Override
    public String getDtDecorDbObj() {
        if (ws.getIndOggCollg().getDtDecor() >= 0) {
            return getDtDecorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorDbObj(String dtDecorDbObj) {
        if (dtDecorDbObj != null) {
            setDtDecorDb(dtDecorDbObj);
            ws.getIndOggCollg().setDtDecor(((short)0));
        }
        else {
            ws.getIndOggCollg().setDtDecor(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getOggCollgDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getOggCollgDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getOggCollgDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getOggCollgDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtScadDb() {
        return ws.getOggCollgDb().getIniCopDb();
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        this.ws.getOggCollgDb().setIniCopDb(dtScadDb);
    }

    @Override
    public String getDtScadDbObj() {
        if (ws.getIndOggCollg().getDtScad() >= 0) {
            return getDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        if (dtScadDbObj != null) {
            setDtScadDb(dtScadDbObj);
            ws.getIndOggCollg().setDtScad(((short)0));
        }
        else {
            ws.getIndOggCollg().setDtScad(((short)-1));
        }
    }

    @Override
    public String getDtUltPrePagDb() {
        return ws.getOggCollgDb().getEsiTitDb();
    }

    @Override
    public void setDtUltPrePagDb(String dtUltPrePagDb) {
        this.ws.getOggCollgDb().setEsiTitDb(dtUltPrePagDb);
    }

    @Override
    public String getDtUltPrePagDbObj() {
        if (ws.getIndOggCollg().getDtUltPrePag() >= 0) {
            return getDtUltPrePagDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltPrePagDbObj(String dtUltPrePagDbObj) {
        if (dtUltPrePagDbObj != null) {
            setDtUltPrePagDb(dtUltPrePagDbObj);
            ws.getIndOggCollg().setDtUltPrePag(((short)0));
        }
        else {
            ws.getIndOggCollg().setDtUltPrePag(((short)-1));
        }
    }

    @Override
    public String getIb2oRiftoEstno() {
        return oggCollg.getOcoIb2oRiftoEstno();
    }

    @Override
    public void setIb2oRiftoEstno(String ib2oRiftoEstno) {
        this.oggCollg.setOcoIb2oRiftoEstno(ib2oRiftoEstno);
    }

    @Override
    public String getIb2oRiftoEstnoObj() {
        if (ws.getIndOggCollg().getIb2oRiftoEstno() >= 0) {
            return getIb2oRiftoEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIb2oRiftoEstnoObj(String ib2oRiftoEstnoObj) {
        if (ib2oRiftoEstnoObj != null) {
            setIb2oRiftoEstno(ib2oRiftoEstnoObj);
            ws.getIndOggCollg().setIb2oRiftoEstno(((short)0));
        }
        else {
            ws.getIndOggCollg().setIb2oRiftoEstno(((short)-1));
        }
    }

    @Override
    public String getIbRiftoEstno() {
        return oggCollg.getOcoIbRiftoEstno();
    }

    @Override
    public void setIbRiftoEstno(String ibRiftoEstno) {
        this.oggCollg.setOcoIbRiftoEstno(ibRiftoEstno);
    }

    @Override
    public String getIbRiftoEstnoObj() {
        if (ws.getIndOggCollg().getIbRiftoEstno() >= 0) {
            return getIbRiftoEstno();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbRiftoEstnoObj(String ibRiftoEstnoObj) {
        if (ibRiftoEstnoObj != null) {
            setIbRiftoEstno(ibRiftoEstnoObj);
            ws.getIndOggCollg().setIbRiftoEstno(((short)0));
        }
        else {
            ws.getIndOggCollg().setIbRiftoEstno(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return oggCollg.getOcoIdMoviChiu().getOcoIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.oggCollg.getOcoIdMoviChiu().setOcoIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndOggCollg().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndOggCollg().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndOggCollg().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return oggCollg.getOcoIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.oggCollg.setOcoIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdOggCoinv() {
        return oggCollg.getOcoIdOggCoinv();
    }

    @Override
    public void setIdOggCoinv(int idOggCoinv) {
        this.oggCollg.setOcoIdOggCoinv(idOggCoinv);
    }

    @Override
    public int getIdOggCollg() {
        return oggCollg.getOcoIdOggCollg();
    }

    @Override
    public void setIdOggCollg(int idOggCollg) {
        this.oggCollg.setOcoIdOggCollg(idOggCollg);
    }

    @Override
    public int getIdOggDer() {
        return oggCollg.getOcoIdOggDer();
    }

    @Override
    public void setIdOggDer(int idOggDer) {
        this.oggCollg.setOcoIdOggDer(idOggDer);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpCollg() {
        return oggCollg.getOcoImpCollg().getOcoImpCollg();
    }

    @Override
    public void setImpCollg(AfDecimal impCollg) {
        this.oggCollg.getOcoImpCollg().setOcoImpCollg(impCollg.copy());
    }

    @Override
    public AfDecimal getImpCollgObj() {
        if (ws.getIndOggCollg().getImpCollg() >= 0) {
            return getImpCollg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCollgObj(AfDecimal impCollgObj) {
        if (impCollgObj != null) {
            setImpCollg(new AfDecimal(impCollgObj, 15, 3));
            ws.getIndOggCollg().setImpCollg(((short)0));
        }
        else {
            ws.getIndOggCollg().setImpCollg(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpReinvst() {
        return oggCollg.getOcoImpReinvst().getOcoImpReinvst();
    }

    @Override
    public void setImpReinvst(AfDecimal impReinvst) {
        this.oggCollg.getOcoImpReinvst().setOcoImpReinvst(impReinvst.copy());
    }

    @Override
    public AfDecimal getImpReinvstObj() {
        if (ws.getIndOggCollg().getImpReinvst() >= 0) {
            return getImpReinvst();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpReinvstObj(AfDecimal impReinvstObj) {
        if (impReinvstObj != null) {
            setImpReinvst(new AfDecimal(impReinvstObj, 15, 3));
            ws.getIndOggCollg().setImpReinvst(((short)0));
        }
        else {
            ws.getIndOggCollg().setImpReinvst(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasf() {
        return oggCollg.getOcoImpTrasf().getOcoImpTrasf();
    }

    @Override
    public void setImpTrasf(AfDecimal impTrasf) {
        this.oggCollg.getOcoImpTrasf().setOcoImpTrasf(impTrasf.copy());
    }

    @Override
    public AfDecimal getImpTrasfObj() {
        if (ws.getIndOggCollg().getImpTrasf() >= 0) {
            return getImpTrasf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasfObj(AfDecimal impTrasfObj) {
        if (impTrasfObj != null) {
            setImpTrasf(new AfDecimal(impTrasfObj, 15, 3));
            ws.getIndOggCollg().setImpTrasf(((short)0));
        }
        else {
            ws.getIndOggCollg().setImpTrasf(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasferito() {
        return oggCollg.getOcoImpTrasferito().getOcoImpTrasferito();
    }

    @Override
    public void setImpTrasferito(AfDecimal impTrasferito) {
        this.oggCollg.getOcoImpTrasferito().setOcoImpTrasferito(impTrasferito.copy());
    }

    @Override
    public AfDecimal getImpTrasferitoObj() {
        if (ws.getIndOggCollg().getImpTrasferito() >= 0) {
            return getImpTrasferito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasferitoObj(AfDecimal impTrasferitoObj) {
        if (impTrasferitoObj != null) {
            setImpTrasferito(new AfDecimal(impTrasferitoObj, 15, 3));
            ws.getIndOggCollg().setImpTrasferito(((short)0));
        }
        else {
            ws.getIndOggCollg().setImpTrasferito(((short)-1));
        }
    }

    @Override
    public char getIndLiqAggMan() {
        return oggCollg.getOcoIndLiqAggMan();
    }

    @Override
    public void setIndLiqAggMan(char indLiqAggMan) {
        this.oggCollg.setOcoIndLiqAggMan(indLiqAggMan);
    }

    @Override
    public Character getIndLiqAggManObj() {
        if (ws.getIndOggCollg().getIndLiqAggMan() >= 0) {
            return ((Character)getIndLiqAggMan());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndLiqAggManObj(Character indLiqAggManObj) {
        if (indLiqAggManObj != null) {
            setIndLiqAggMan(((char)indLiqAggManObj));
            ws.getIndOggCollg().setIndLiqAggMan(((short)0));
        }
        else {
            ws.getIndOggCollg().setIndLiqAggMan(((short)-1));
        }
    }

    @Override
    public int getLdbv0721IdOggCollg() {
        return ws.getLdbv0721().getLdbv0721IdOggCollg();
    }

    @Override
    public void setLdbv0721IdOggCollg(int ldbv0721IdOggCollg) {
        this.ws.getLdbv0721().setLdbv0721IdOggCollg(ldbv0721IdOggCollg);
    }

    @Override
    public String getLdbv0721TpOggCollg() {
        return ws.getLdbv0721().getLdbv0721TpOggCollg();
    }

    @Override
    public void setLdbv0721TpOggCollg(String ldbv0721TpOggCollg) {
        this.ws.getLdbv0721().setLdbv0721TpOggCollg(ldbv0721TpOggCollg);
    }

    @Override
    public int getLdbv5571IdOggDer() {
        throw new FieldNotMappedException("ldbv5571IdOggDer");
    }

    @Override
    public void setLdbv5571IdOggDer(int ldbv5571IdOggDer) {
        throw new FieldNotMappedException("ldbv5571IdOggDer");
    }

    @Override
    public AfDecimal getLdbv5571TotImpColl() {
        throw new FieldNotMappedException("ldbv5571TotImpColl");
    }

    @Override
    public void setLdbv5571TotImpColl(AfDecimal ldbv5571TotImpColl) {
        throw new FieldNotMappedException("ldbv5571TotImpColl");
    }

    @Override
    public AfDecimal getLdbv5571TotImpCollObj() {
        return getLdbv5571TotImpColl();
    }

    @Override
    public void setLdbv5571TotImpCollObj(AfDecimal ldbv5571TotImpCollObj) {
        setLdbv5571TotImpColl(new AfDecimal(ldbv5571TotImpCollObj, 15, 3));
    }

    @Override
    public String getLdbv5571TpColl1() {
        throw new FieldNotMappedException("ldbv5571TpColl1");
    }

    @Override
    public void setLdbv5571TpColl1(String ldbv5571TpColl1) {
        throw new FieldNotMappedException("ldbv5571TpColl1");
    }

    @Override
    public String getLdbv5571TpColl2() {
        throw new FieldNotMappedException("ldbv5571TpColl2");
    }

    @Override
    public void setLdbv5571TpColl2(String ldbv5571TpColl2) {
        throw new FieldNotMappedException("ldbv5571TpColl2");
    }

    @Override
    public String getLdbv5571TpColl3() {
        throw new FieldNotMappedException("ldbv5571TpColl3");
    }

    @Override
    public void setLdbv5571TpColl3(String ldbv5571TpColl3) {
        throw new FieldNotMappedException("ldbv5571TpColl3");
    }

    @Override
    public String getLdbv5571TpOggDer() {
        throw new FieldNotMappedException("ldbv5571TpOggDer");
    }

    @Override
    public void setLdbv5571TpOggDer(String ldbv5571TpOggDer) {
        throw new FieldNotMappedException("ldbv5571TpOggDer");
    }

    @Override
    public long getOcoDsRiga() {
        return oggCollg.getOcoDsRiga();
    }

    @Override
    public void setOcoDsRiga(long ocoDsRiga) {
        this.oggCollg.setOcoDsRiga(ocoDsRiga);
    }

    @Override
    public AfDecimal getPcPreTrasferito() {
        return oggCollg.getOcoPcPreTrasferito().getOcoPcPreTrasferito();
    }

    @Override
    public void setPcPreTrasferito(AfDecimal pcPreTrasferito) {
        this.oggCollg.getOcoPcPreTrasferito().setOcoPcPreTrasferito(pcPreTrasferito.copy());
    }

    @Override
    public AfDecimal getPcPreTrasferitoObj() {
        if (ws.getIndOggCollg().getPcPreTrasferito() >= 0) {
            return getPcPreTrasferito();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcPreTrasferitoObj(AfDecimal pcPreTrasferitoObj) {
        if (pcPreTrasferitoObj != null) {
            setPcPreTrasferito(new AfDecimal(pcPreTrasferitoObj, 6, 3));
            ws.getIndOggCollg().setPcPreTrasferito(((short)0));
        }
        else {
            ws.getIndOggCollg().setPcPreTrasferito(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcReinvstRilievi() {
        return oggCollg.getOcoPcReinvstRilievi().getOcoPcReinvstRilievi();
    }

    @Override
    public void setPcReinvstRilievi(AfDecimal pcReinvstRilievi) {
        this.oggCollg.getOcoPcReinvstRilievi().setOcoPcReinvstRilievi(pcReinvstRilievi.copy());
    }

    @Override
    public AfDecimal getPcReinvstRilieviObj() {
        if (ws.getIndOggCollg().getPcReinvstRilievi() >= 0) {
            return getPcReinvstRilievi();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcReinvstRilieviObj(AfDecimal pcReinvstRilieviObj) {
        if (pcReinvstRilieviObj != null) {
            setPcReinvstRilievi(new AfDecimal(pcReinvstRilieviObj, 6, 3));
            ws.getIndOggCollg().setPcReinvstRilievi(((short)0));
        }
        else {
            ws.getIndOggCollg().setPcReinvstRilievi(((short)-1));
        }
    }

    @Override
    public AfDecimal getPre1aAnnualita() {
        return oggCollg.getOcoPre1aAnnualita().getOcoPre1aAnnualita();
    }

    @Override
    public void setPre1aAnnualita(AfDecimal pre1aAnnualita) {
        this.oggCollg.getOcoPre1aAnnualita().setOcoPre1aAnnualita(pre1aAnnualita.copy());
    }

    @Override
    public AfDecimal getPre1aAnnualitaObj() {
        if (ws.getIndOggCollg().getPre1aAnnualita() >= 0) {
            return getPre1aAnnualita();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPre1aAnnualitaObj(AfDecimal pre1aAnnualitaObj) {
        if (pre1aAnnualitaObj != null) {
            setPre1aAnnualita(new AfDecimal(pre1aAnnualitaObj, 15, 3));
            ws.getIndOggCollg().setPre1aAnnualita(((short)0));
        }
        else {
            ws.getIndOggCollg().setPre1aAnnualita(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePerTrasf() {
        return oggCollg.getOcoPrePerTrasf().getOcoPrePerTrasf();
    }

    @Override
    public void setPrePerTrasf(AfDecimal prePerTrasf) {
        this.oggCollg.getOcoPrePerTrasf().setOcoPrePerTrasf(prePerTrasf.copy());
    }

    @Override
    public AfDecimal getPrePerTrasfObj() {
        if (ws.getIndOggCollg().getPrePerTrasf() >= 0) {
            return getPrePerTrasf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePerTrasfObj(AfDecimal prePerTrasfObj) {
        if (prePerTrasfObj != null) {
            setPrePerTrasf(new AfDecimal(prePerTrasfObj, 15, 3));
            ws.getIndOggCollg().setPrePerTrasf(((short)0));
        }
        else {
            ws.getIndOggCollg().setPrePerTrasf(((short)-1));
        }
    }

    @Override
    public AfDecimal getRecProv() {
        return oggCollg.getOcoRecProv().getOcoRecProv();
    }

    @Override
    public void setRecProv(AfDecimal recProv) {
        this.oggCollg.getOcoRecProv().setOcoRecProv(recProv.copy());
    }

    @Override
    public AfDecimal getRecProvObj() {
        if (ws.getIndOggCollg().getRecProv() >= 0) {
            return getRecProv();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRecProvObj(AfDecimal recProvObj) {
        if (recProvObj != null) {
            setRecProv(new AfDecimal(recProvObj, 15, 3));
            ws.getIndOggCollg().setRecProv(((short)0));
        }
        else {
            ws.getIndOggCollg().setRecProv(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMat() {
        return oggCollg.getOcoRisMat().getOcoRisMat();
    }

    @Override
    public void setRisMat(AfDecimal risMat) {
        this.oggCollg.getOcoRisMat().setOcoRisMat(risMat.copy());
    }

    @Override
    public AfDecimal getRisMatObj() {
        if (ws.getIndOggCollg().getRisMat() >= 0) {
            return getRisMat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatObj(AfDecimal risMatObj) {
        if (risMatObj != null) {
            setRisMat(new AfDecimal(risMatObj, 15, 3));
            ws.getIndOggCollg().setRisMat(((short)0));
        }
        else {
            ws.getIndOggCollg().setRisMat(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisZil() {
        return oggCollg.getOcoRisZil().getOcoRisZil();
    }

    @Override
    public void setRisZil(AfDecimal risZil) {
        this.oggCollg.getOcoRisZil().setOcoRisZil(risZil.copy());
    }

    @Override
    public AfDecimal getRisZilObj() {
        if (ws.getIndOggCollg().getRisZil() >= 0) {
            return getRisZil();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisZilObj(AfDecimal risZilObj) {
        if (risZilObj != null) {
            setRisZil(new AfDecimal(risZilObj, 15, 3));
            ws.getIndOggCollg().setRisZil(((short)0));
        }
        else {
            ws.getIndOggCollg().setRisZil(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotPre() {
        return oggCollg.getOcoTotPre().getOcoTotPre();
    }

    @Override
    public void setTotPre(AfDecimal totPre) {
        this.oggCollg.getOcoTotPre().setOcoTotPre(totPre.copy());
    }

    @Override
    public AfDecimal getTotPreObj() {
        if (ws.getIndOggCollg().getTotPre() >= 0) {
            return getTotPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotPreObj(AfDecimal totPreObj) {
        if (totPreObj != null) {
            setTotPre(new AfDecimal(totPreObj, 15, 3));
            ws.getIndOggCollg().setTotPre(((short)0));
        }
        else {
            ws.getIndOggCollg().setTotPre(((short)-1));
        }
    }

    @Override
    public String getTpCollgm() {
        return oggCollg.getOcoTpCollgm();
    }

    @Override
    public void setTpCollgm(String tpCollgm) {
        this.oggCollg.setOcoTpCollgm(tpCollgm);
    }

    @Override
    public String getTpModAbbinamento() {
        return oggCollg.getOcoTpModAbbinamento();
    }

    @Override
    public void setTpModAbbinamento(String tpModAbbinamento) {
        this.oggCollg.setOcoTpModAbbinamento(tpModAbbinamento);
    }

    @Override
    public String getTpModAbbinamentoObj() {
        if (ws.getIndOggCollg().getTpModAbbinamento() >= 0) {
            return getTpModAbbinamento();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpModAbbinamentoObj(String tpModAbbinamentoObj) {
        if (tpModAbbinamentoObj != null) {
            setTpModAbbinamento(tpModAbbinamentoObj);
            ws.getIndOggCollg().setTpModAbbinamento(((short)0));
        }
        else {
            ws.getIndOggCollg().setTpModAbbinamento(((short)-1));
        }
    }

    @Override
    public String getTpOggCoinv() {
        return oggCollg.getOcoTpOggCoinv();
    }

    @Override
    public void setTpOggCoinv(String tpOggCoinv) {
        this.oggCollg.setOcoTpOggCoinv(tpOggCoinv);
    }

    @Override
    public String getTpOggDer() {
        return oggCollg.getOcoTpOggDer();
    }

    @Override
    public void setTpOggDer(String tpOggDer) {
        this.oggCollg.setOcoTpOggDer(tpOggDer);
    }

    @Override
    public String getTpTrasf() {
        return oggCollg.getOcoTpTrasf();
    }

    @Override
    public void setTpTrasf(String tpTrasf) {
        this.oggCollg.setOcoTpTrasf(tpTrasf);
    }

    @Override
    public String getTpTrasfObj() {
        if (ws.getIndOggCollg().getTpTrasf() >= 0) {
            return getTpTrasf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpTrasfObj(String tpTrasfObj) {
        if (tpTrasfObj != null) {
            setTpTrasf(tpTrasfObj);
            ws.getIndOggCollg().setTpTrasf(((short)0));
        }
        else {
            ws.getIndOggCollg().setTpTrasf(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
