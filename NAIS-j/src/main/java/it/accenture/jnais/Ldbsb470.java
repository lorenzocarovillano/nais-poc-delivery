package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.TitContDao;
import it.accenture.jnais.commons.data.to.ITitCont;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.copy.TitCont;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbsb470Data;
import it.accenture.jnais.ws.Ldbvb471;
import it.accenture.jnais.ws.redefines.Ldbvb471TotIntPre;
import it.accenture.jnais.ws.redefines.TitDtApplzMora;
import it.accenture.jnais.ws.redefines.TitDtCambioVlt;
import it.accenture.jnais.ws.redefines.TitDtCertFisc;
import it.accenture.jnais.ws.redefines.TitDtEmisTit;
import it.accenture.jnais.ws.redefines.TitDtEndCop;
import it.accenture.jnais.ws.redefines.TitDtEsiTit;
import it.accenture.jnais.ws.redefines.TitDtIniCop;
import it.accenture.jnais.ws.redefines.TitDtRichAddRid;
import it.accenture.jnais.ws.redefines.TitDtVlt;
import it.accenture.jnais.ws.redefines.TitFraz;
import it.accenture.jnais.ws.redefines.TitIdMoviChiu;
import it.accenture.jnais.ws.redefines.TitIdRappAna;
import it.accenture.jnais.ws.redefines.TitIdRappRete;
import it.accenture.jnais.ws.redefines.TitImpAder;
import it.accenture.jnais.ws.redefines.TitImpAz;
import it.accenture.jnais.ws.redefines.TitImpPag;
import it.accenture.jnais.ws.redefines.TitImpTfr;
import it.accenture.jnais.ws.redefines.TitImpTfrStrc;
import it.accenture.jnais.ws.redefines.TitImpTrasfe;
import it.accenture.jnais.ws.redefines.TitImpVolo;
import it.accenture.jnais.ws.redefines.TitNumRatAccorpate;
import it.accenture.jnais.ws.redefines.TitProgTit;
import it.accenture.jnais.ws.redefines.TitTotCarAcq;
import it.accenture.jnais.ws.redefines.TitTotCarGest;
import it.accenture.jnais.ws.redefines.TitTotCarIas;
import it.accenture.jnais.ws.redefines.TitTotCarInc;
import it.accenture.jnais.ws.redefines.TitTotDir;
import it.accenture.jnais.ws.redefines.TitTotIntrFraz;
import it.accenture.jnais.ws.redefines.TitTotIntrMora;
import it.accenture.jnais.ws.redefines.TitTotIntrPrest;
import it.accenture.jnais.ws.redefines.TitTotIntrRetdt;
import it.accenture.jnais.ws.redefines.TitTotIntrRiat;
import it.accenture.jnais.ws.redefines.TitTotManfeeAntic;
import it.accenture.jnais.ws.redefines.TitTotManfeeRec;
import it.accenture.jnais.ws.redefines.TitTotManfeeRicor;
import it.accenture.jnais.ws.redefines.TitTotPreNet;
import it.accenture.jnais.ws.redefines.TitTotPrePpIas;
import it.accenture.jnais.ws.redefines.TitTotPreSoloRsh;
import it.accenture.jnais.ws.redefines.TitTotPreTot;
import it.accenture.jnais.ws.redefines.TitTotProvAcq1aa;
import it.accenture.jnais.ws.redefines.TitTotProvAcq2aa;
import it.accenture.jnais.ws.redefines.TitTotProvDaRec;
import it.accenture.jnais.ws.redefines.TitTotProvInc;
import it.accenture.jnais.ws.redefines.TitTotProvRicor;
import it.accenture.jnais.ws.redefines.TitTotSoprAlt;
import it.accenture.jnais.ws.redefines.TitTotSoprProf;
import it.accenture.jnais.ws.redefines.TitTotSoprSan;
import it.accenture.jnais.ws.redefines.TitTotSoprSpo;
import it.accenture.jnais.ws.redefines.TitTotSoprTec;
import it.accenture.jnais.ws.redefines.TitTotSpeAge;
import it.accenture.jnais.ws.redefines.TitTotSpeMed;
import it.accenture.jnais.ws.redefines.TitTotTax;
import it.accenture.jnais.ws.redefines.TitTpCausStor;

/**Original name: LDBSB470<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 LUG 2011.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbsb470 extends Program implements ITitCont {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private TitContDao titContDao = new TitContDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbsb470Data ws = new Ldbsb470Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LDBVB471
    private Ldbvb471 ldbvb471;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBSB470_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ldbvb471 ldbvb471) {
        this.idsv0003 = idsv0003;
        this.ldbvb471 = ldbvb471;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbsb470 getInstance() {
        return ((Ldbsb470)Programs.getInstance(Ldbsb470.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBSB470'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBSB470");
        // COB_CODE: MOVE ' LDBVB471' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella(" LDBVB471");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT  TOT_INTR_PREST
        //                   ,DT_INI_EFF
        //              INTO :TIT-TOT-INTR-PREST
        //                   :IND-TIT-TOT-INTR-PREST
        //                  ,:TIT-DT-INI-EFF-DB
        //              FROM TIT_CONT
        //            WHERE (TP_TIT = :LDBVB471-TP-TIT-01
        //              OR  (TP_TIT = :LDBVB471-TP-TIT-02
        //              AND  TOT_INTR_PREST > 0))
        //              AND TP_STAT_TIT IN (:LDBVB471-TP-STAT-TIT-1,
        //                                  :LDBVB471-TP-STAT-TIT-2,
        //                                  :LDBVB471-TP-STAT-TIT-3)
        //              AND DT_ESI_TIT  = :LDBVB471-DT-MAX-DB
        //              AND ID_OGG      = :LDBVB471-ID-OGG
        //              AND TP_OGG      = :LDBVB471-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY DT_INI_EFF DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        titContDao.selectRec17(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBSB470.cbl:line=250, because the code is unreachable.
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT  TOT_INTR_PREST
        //                   ,DT_INI_EFF
        //              INTO :TIT-TOT-INTR-PREST
        //                   :IND-TIT-TOT-INTR-PREST
        //                  ,:TIT-DT-INI-EFF-DB
        //              FROM TIT_CONT
        //            WHERE (TP_TIT = :LDBVB471-TP-TIT-01
        //              OR  (TP_TIT = :LDBVB471-TP-TIT-02
        //              AND  TOT_INTR_PREST > 0))
        //              AND TP_STAT_TIT IN (:LDBVB471-TP-STAT-TIT-1,
        //                                  :LDBVB471-TP-STAT-TIT-2,
        //                                  :LDBVB471-TP-STAT-TIT-3)
        //              AND DT_ESI_TIT  = :LDBVB471-DT-MAX-DB
        //              AND ID_OGG      = :LDBVB471-ID-OGG
        //              AND TP_OGG      = :LDBVB471-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY DT_INI_EFF DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        titContDao.selectRec18(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LDBSB470.cbl:line=339, because the code is unreachable.
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-TIT-IB-RICH = -1
        //              MOVE HIGH-VALUES TO TIT-IB-RICH-NULL
        //           END-IF
        if (ws.getIndTitCont().getIbRich() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IB-RICH-NULL
            ws.getTitCont().setTitIbRich(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_IB_RICH));
        }
        // COB_CODE: IF IND-TIT-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO TIT-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-MOVI-CHIU-NULL
            ws.getTitCont().getTitIdMoviChiu().setTitIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdMoviChiu.Len.TIT_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-TIT-PROG-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-PROG-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getProgTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-PROG-TIT-NULL
            ws.getTitCont().getTitProgTit().setTitProgTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitProgTit.Len.TIT_PROG_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO TIT-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-INI-COP-NULL
            ws.getTitCont().getTitDtIniCop().setTitDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtIniCop.Len.TIT_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO TIT-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-END-COP-NULL
            ws.getTitCont().getTitDtEndCop().setTitDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEndCop.Len.TIT_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-PAG = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-PAG-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpPag() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-PAG-NULL
            ws.getTitCont().getTitImpPag().setTitImpPagNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpPag.Len.TIT_IMP_PAG_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-SOLL = -1
        //              MOVE HIGH-VALUES TO TIT-FL-SOLL-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlSoll() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-SOLL-NULL
            ws.getTitCont().setTitFlSoll(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-FRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FRAZ-NULL
            ws.getTitCont().getTitFraz().setTitFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitFraz.Len.TIT_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-APPLZ-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-DT-APPLZ-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtApplzMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-APPLZ-MORA-NULL
            ws.getTitCont().getTitDtApplzMora().setTitDtApplzMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtApplzMora.Len.TIT_DT_APPLZ_MORA_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-FL-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-MORA-NULL
            ws.getTitCont().setTitFlMora(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-ID-RAPP-RETE = -1
        //              MOVE HIGH-VALUES TO TIT-ID-RAPP-RETE-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdRappRete() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-RAPP-RETE-NULL
            ws.getTitCont().getTitIdRappRete().setTitIdRappReteNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdRappRete.Len.TIT_ID_RAPP_RETE_NULL));
        }
        // COB_CODE: IF IND-TIT-ID-RAPP-ANA = -1
        //              MOVE HIGH-VALUES TO TIT-ID-RAPP-ANA-NULL
        //           END-IF
        if (ws.getIndTitCont().getIdRappAna() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ID-RAPP-ANA-NULL
            ws.getTitCont().getTitIdRappAna().setTitIdRappAnaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitIdRappAna.Len.TIT_ID_RAPP_ANA_NULL));
        }
        // COB_CODE: IF IND-TIT-COD-DVS = -1
        //              MOVE HIGH-VALUES TO TIT-COD-DVS-NULL
        //           END-IF
        if (ws.getIndTitCont().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-COD-DVS-NULL
            ws.getTitCont().setTitCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_COD_DVS));
        }
        // COB_CODE: IF IND-TIT-DT-EMIS-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-EMIS-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEmisTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-EMIS-TIT-NULL
            ws.getTitCont().getTitDtEmisTit().setTitDtEmisTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEmisTit.Len.TIT_DT_EMIS_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-ESI-TIT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-ESI-TIT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtEsiTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-ESI-TIT-NULL
            ws.getTitCont().getTitDtEsiTit().setTitDtEsiTitNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtEsiTit.Len.TIT_DT_ESI_TIT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-NET = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-NET-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-NET-NULL
            ws.getTitCont().getTitTotPreNet().setTitTotPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreNet.Len.TIT_TOT_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-FRAZ-NULL
            ws.getTitCont().getTitTotIntrFraz().setTitTotIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrFraz.Len.TIT_TOT_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-MORA-NULL
            ws.getTitCont().getTitTotIntrMora().setTitTotIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrMora.Len.TIT_TOT_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-PREST-NULL
            ws.getTitCont().getTitTotIntrPrest().setTitTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrPrest.Len.TIT_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-RETDT-NULL
            ws.getTitCont().getTitTotIntrRetdt().setTitTotIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrRetdt.Len.TIT_TOT_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-INTR-RIAT-NULL
            ws.getTitCont().getTitTotIntrRiat().setTitTotIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotIntrRiat.Len.TIT_TOT_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-DIR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-DIR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-DIR-NULL
            ws.getTitCont().getTitTotDir().setTitTotDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotDir.Len.TIT_TOT_DIR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SPE-MED = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SPE-MED-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SPE-MED-NULL
            ws.getTitCont().getTitTotSpeMed().setTitTotSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSpeMed.Len.TIT_TOT_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-TAX = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-TAX-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-TAX-NULL
            ws.getTitCont().getTitTotTax().setTitTotTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotTax.Len.TIT_TOT_TAX_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-SAN-NULL
            ws.getTitCont().getTitTotSoprSan().setTitTotSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprSan.Len.TIT_TOT_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-TEC-NULL
            ws.getTitCont().getTitTotSoprTec().setTitTotSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprTec.Len.TIT_TOT_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-SPO-NULL
            ws.getTitCont().getTitTotSoprSpo().setTitTotSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprSpo.Len.TIT_TOT_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-PROF-NULL
            ws.getTitCont().getTitTotSoprProf().setTitTotSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprProf.Len.TIT_TOT_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SOPR-ALT-NULL
            ws.getTitCont().getTitTotSoprAlt().setTitTotSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSoprAlt.Len.TIT_TOT_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-TOT-NULL
            ws.getTitCont().getTitTotPreTot().setTitTotPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreTot.Len.TIT_TOT_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-PP-IAS-NULL
            ws.getTitCont().getTitTotPrePpIas().setTitTotPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPrePpIas.Len.TIT_TOT_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-ACQ-NULL
            ws.getTitCont().getTitTotCarAcq().setTitTotCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarAcq.Len.TIT_TOT_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-GEST-NULL
            ws.getTitCont().getTitTotCarGest().setTitTotCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarGest.Len.TIT_TOT_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-INC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-INC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-INC-NULL
            ws.getTitCont().getTitTotCarInc().setTitTotCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarInc.Len.TIT_TOT_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PRE-SOLO-RSH-NULL
            ws.getTitCont().getTitTotPreSoloRsh().setTitTotPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotPreSoloRsh.Len.TIT_TOT_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-1AA-NULL
            ws.getTitCont().getTitTotProvAcq1aa().setTitTotProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvAcq1aa.Len.TIT_TOT_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-ACQ-2AA-NULL
            ws.getTitCont().getTitTotProvAcq2aa().setTitTotProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvAcq2aa.Len.TIT_TOT_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-RICOR-NULL
            ws.getTitCont().getTitTotProvRicor().setTitTotProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvRicor.Len.TIT_TOT_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-INC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-INC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-INC-NULL
            ws.getTitCont().getTitTotProvInc().setTitTotProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvInc.Len.TIT_TOT_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-PROV-DA-REC-NULL
            ws.getTitCont().getTitTotProvDaRec().setTitTotProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotProvDaRec.Len.TIT_TOT_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-AZ-NULL
            ws.getTitCont().getTitImpAz().setTitImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpAz.Len.TIT_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-ADER-NULL
            ws.getTitCont().getTitImpAder().setTitImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpAder.Len.TIT_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TFR-NULL
            ws.getTitCont().getTitImpTfr().setTitImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTfr.Len.TIT_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-VOLO-NULL
            ws.getTitCont().getTitImpVolo().setTitImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpVolo.Len.TIT_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-ANTIC-NULL
            ws.getTitCont().getTitTotManfeeAntic().setTitTotManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeAntic.Len.TIT_TOT_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-RICOR-NULL
            ws.getTitCont().getTitTotManfeeRicor().setTitTotManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeRicor.Len.TIT_TOT_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-MANFEE-REC-NULL
            ws.getTitCont().getTitTotManfeeRec().setTitTotManfeeRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotManfeeRec.Len.TIT_TOT_MANFEE_REC_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-MEZ-PAG-ADD = -1
        //              MOVE HIGH-VALUES TO TIT-TP-MEZ-PAG-ADD-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpMezPagAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-MEZ-PAG-ADD-NULL
            ws.getTitCont().setTitTpMezPagAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_TP_MEZ_PAG_ADD));
        }
        // COB_CODE: IF IND-TIT-ESTR-CNT-CORR-ADD = -1
        //              MOVE HIGH-VALUES TO TIT-ESTR-CNT-CORR-ADD-NULL
        //           END-IF
        if (ws.getIndTitCont().getEstrCntCorrAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-ESTR-CNT-CORR-ADD-NULL
            ws.getTitCont().setTitEstrCntCorrAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_ESTR_CNT_CORR_ADD));
        }
        // COB_CODE: IF IND-TIT-DT-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-VLT-NULL
            ws.getTitCont().getTitDtVlt().setTitDtVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtVlt.Len.TIT_DT_VLT_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-FORZ-DT-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-FL-FORZ-DT-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlForzDtVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-FORZ-DT-VLT-NULL
            ws.getTitCont().setTitFlForzDtVlt(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-DT-CAMBIO-VLT = -1
        //              MOVE HIGH-VALUES TO TIT-DT-CAMBIO-VLT-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtCambioVlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-CAMBIO-VLT-NULL
            ws.getTitCont().getTitDtCambioVlt().setTitDtCambioVltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtCambioVlt.Len.TIT_DT_CAMBIO_VLT_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-SPE-AGE-NULL
            ws.getTitCont().getTitTotSpeAge().setTitTotSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotSpeAge.Len.TIT_TOT_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-TIT-TOT-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO TIT-TOT-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndTitCont().getTotCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TOT-CAR-IAS-NULL
            ws.getTitCont().getTitTotCarIas().setTitTotCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTotCarIas.Len.TIT_TOT_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-TIT-NUM-RAT-ACCORPATE = -1
        //              MOVE HIGH-VALUES TO TIT-NUM-RAT-ACCORPATE-NULL
        //           END-IF
        if (ws.getIndTitCont().getNumRatAccorpate() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-NUM-RAT-ACCORPATE-NULL
            ws.getTitCont().getTitNumRatAccorpate().setTitNumRatAccorpateNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitNumRatAccorpate.Len.TIT_NUM_RAT_ACCORPATE_NULL));
        }
        // COB_CODE: IF IND-TIT-FL-TIT-DA-REINVST = -1
        //              MOVE HIGH-VALUES TO TIT-FL-TIT-DA-REINVST-NULL
        //           END-IF
        if (ws.getIndTitCont().getFlTitDaReinvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-FL-TIT-DA-REINVST-NULL
            ws.getTitCont().setTitFlTitDaReinvst(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-TIT-DT-RICH-ADD-RID = -1
        //              MOVE HIGH-VALUES TO TIT-DT-RICH-ADD-RID-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtRichAddRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-RICH-ADD-RID-NULL
            ws.getTitCont().getTitDtRichAddRid().setTitDtRichAddRidNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtRichAddRid.Len.TIT_DT_RICH_ADD_RID_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-ESI-RID = -1
        //              MOVE HIGH-VALUES TO TIT-TP-ESI-RID-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpEsiRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-ESI-RID-NULL
            ws.getTitCont().setTitTpEsiRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_TP_ESI_RID));
        }
        // COB_CODE: IF IND-TIT-COD-IBAN = -1
        //              MOVE HIGH-VALUES TO TIT-COD-IBAN-NULL
        //           END-IF
        if (ws.getIndTitCont().getCodIban() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-COD-IBAN-NULL
            ws.getTitCont().setTitCodIban(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_COD_IBAN));
        }
        // COB_CODE: IF IND-TIT-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TRASFE-NULL
            ws.getTitCont().getTitImpTrasfe().setTitImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTrasfe.Len.TIT_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-TIT-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO TIT-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndTitCont().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-IMP-TFR-STRC-NULL
            ws.getTitCont().getTitImpTfrStrc().setTitImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitImpTfrStrc.Len.TIT_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-TIT-DT-CERT-FISC = -1
        //              MOVE HIGH-VALUES TO TIT-DT-CERT-FISC-NULL
        //           END-IF
        if (ws.getIndTitCont().getDtCertFisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-DT-CERT-FISC-NULL
            ws.getTitCont().getTitDtCertFisc().setTitDtCertFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitDtCertFisc.Len.TIT_DT_CERT_FISC_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-STOR = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-STOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpCausStor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-STOR-NULL
            ws.getTitCont().getTitTpCausStor().setTitTpCausStorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitTpCausStor.Len.TIT_TP_CAUS_STOR_NULL));
        }
        // COB_CODE: IF IND-TIT-TP-CAUS-DISP-STOR = -1
        //              MOVE HIGH-VALUES TO TIT-TP-CAUS-DISP-STOR-NULL
        //           END-IF
        if (ws.getIndTitCont().getTpCausDispStor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-CAUS-DISP-STOR-NULL
            ws.getTitCont().setTitTpCausDispStor(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_TP_CAUS_DISP_STOR));
        }
        // COB_CODE: IF IND-TIT-TP-TIT-MIGRAZ = -1
        //              MOVE HIGH-VALUES TO TIT-TP-TIT-MIGRAZ-NULL
        //           END-IF.
        if (ws.getIndTitCont().getTpTitMigraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO TIT-TP-TIT-MIGRAZ-NULL
            ws.getTitCont().setTitTpTitMigraz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, TitCont.Len.TIT_TP_TIT_MIGRAZ));
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
        // COB_CODE: IF LDBVB471-DT-MAX IS NUMERIC
        //             END-IF
        //           END-IF.
        if (Functions.isNumber(ldbvb471.getLdbvb471DtMax())) {
            // COB_CODE: IF LDBVB471-DT-MAX GREATER ZERO
            //             MOVE WS-DATE-X       TO LDBVB471-DT-MAX-DB
            //           END-IF
            if (ldbvb471.getLdbvb471DtMax() > 0) {
                // COB_CODE: MOVE LDBVB471-DT-MAX TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ldbvb471.getLdbvb471DtMax(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X       TO LDBVB471-DT-MAX-DB
                ldbvb471.setLdbvb471DtMaxDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
        // COB_CODE: IF IND-TIT-TOT-INTR-PREST = -1
        //                   LDBVB471-TOT-INT-PRE-NULL
        //           ELSE
        //                   LDBVB471-TOT-INT-PRE
        //           END-IF.
        if (ws.getIndTitCont().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO
            //                LDBVB471-TOT-INT-PRE-NULL
            ldbvb471.getLdbvb471TotIntPre().setLdbvb471TotIntPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ldbvb471TotIntPre.Len.LDBVB471_TOT_INT_PRE_NULL));
        }
        else {
            // COB_CODE: MOVE TIT-TOT-INTR-PREST TO
            //                LDBVB471-TOT-INT-PRE
            ldbvb471.getLdbvb471TotIntPre().setLdbvb471TotIntPre(Trunc.toDecimal(ws.getTitCont().getTitTotIntrPrest().getTitTotIntrPrest(), 15, 3));
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    @Override
    public int getCodCompAnia() {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        throw new FieldNotMappedException("codCompAnia");
    }

    @Override
    public String getCodDvs() {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public void setCodDvs(String codDvs) {
        throw new FieldNotMappedException("codDvs");
    }

    @Override
    public String getCodDvsObj() {
        return getCodDvs();
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        setCodDvs(codDvsObj);
    }

    @Override
    public String getCodIban() {
        throw new FieldNotMappedException("codIban");
    }

    @Override
    public void setCodIban(String codIban) {
        throw new FieldNotMappedException("codIban");
    }

    @Override
    public String getCodIbanObj() {
        return getCodIban();
    }

    @Override
    public void setCodIbanObj(String codIbanObj) {
        setCodIban(codIbanObj);
    }

    @Override
    public char getDsOperSql() {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        throw new FieldNotMappedException("dsOperSql");
    }

    @Override
    public char getDsStatoElab() {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        throw new FieldNotMappedException("dsStatoElab");
    }

    @Override
    public long getDsTsEndCptz() {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        throw new FieldNotMappedException("dsTsEndCptz");
    }

    @Override
    public long getDsTsIniCptz() {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        throw new FieldNotMappedException("dsTsIniCptz");
    }

    @Override
    public String getDsUtente() {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public void setDsUtente(String dsUtente) {
        throw new FieldNotMappedException("dsUtente");
    }

    @Override
    public int getDsVer() {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public void setDsVer(int dsVer) {
        throw new FieldNotMappedException("dsVer");
    }

    @Override
    public String getDtApplzMoraDb() {
        throw new FieldNotMappedException("dtApplzMoraDb");
    }

    @Override
    public void setDtApplzMoraDb(String dtApplzMoraDb) {
        throw new FieldNotMappedException("dtApplzMoraDb");
    }

    @Override
    public String getDtApplzMoraDbObj() {
        return getDtApplzMoraDb();
    }

    @Override
    public void setDtApplzMoraDbObj(String dtApplzMoraDbObj) {
        setDtApplzMoraDb(dtApplzMoraDbObj);
    }

    @Override
    public String getDtCambioVltDb() {
        throw new FieldNotMappedException("dtCambioVltDb");
    }

    @Override
    public void setDtCambioVltDb(String dtCambioVltDb) {
        throw new FieldNotMappedException("dtCambioVltDb");
    }

    @Override
    public String getDtCambioVltDbObj() {
        return getDtCambioVltDb();
    }

    @Override
    public void setDtCambioVltDbObj(String dtCambioVltDbObj) {
        setDtCambioVltDb(dtCambioVltDbObj);
    }

    @Override
    public String getDtCertFiscDb() {
        throw new FieldNotMappedException("dtCertFiscDb");
    }

    @Override
    public void setDtCertFiscDb(String dtCertFiscDb) {
        throw new FieldNotMappedException("dtCertFiscDb");
    }

    @Override
    public String getDtCertFiscDbObj() {
        return getDtCertFiscDb();
    }

    @Override
    public void setDtCertFiscDbObj(String dtCertFiscDbObj) {
        setDtCertFiscDb(dtCertFiscDbObj);
    }

    @Override
    public String getDtEmisTitDb() {
        throw new FieldNotMappedException("dtEmisTitDb");
    }

    @Override
    public void setDtEmisTitDb(String dtEmisTitDb) {
        throw new FieldNotMappedException("dtEmisTitDb");
    }

    @Override
    public String getDtEmisTitDbObj() {
        return getDtEmisTitDb();
    }

    @Override
    public void setDtEmisTitDbObj(String dtEmisTitDbObj) {
        setDtEmisTitDb(dtEmisTitDbObj);
    }

    @Override
    public String getDtEndCopDb() {
        throw new FieldNotMappedException("dtEndCopDb");
    }

    @Override
    public void setDtEndCopDb(String dtEndCopDb) {
        throw new FieldNotMappedException("dtEndCopDb");
    }

    @Override
    public String getDtEndCopDbObj() {
        return getDtEndCopDb();
    }

    @Override
    public void setDtEndCopDbObj(String dtEndCopDbObj) {
        setDtEndCopDb(dtEndCopDbObj);
    }

    @Override
    public String getDtEndEffDb() {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        throw new FieldNotMappedException("dtEndEffDb");
    }

    @Override
    public String getDtEsiTitDb() {
        throw new FieldNotMappedException("dtEsiTitDb");
    }

    @Override
    public void setDtEsiTitDb(String dtEsiTitDb) {
        throw new FieldNotMappedException("dtEsiTitDb");
    }

    @Override
    public String getDtEsiTitDbObj() {
        return getDtEsiTitDb();
    }

    @Override
    public void setDtEsiTitDbObj(String dtEsiTitDbObj) {
        setDtEsiTitDb(dtEsiTitDbObj);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getTitContDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getTitContDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtRichAddRidDb() {
        throw new FieldNotMappedException("dtRichAddRidDb");
    }

    @Override
    public void setDtRichAddRidDb(String dtRichAddRidDb) {
        throw new FieldNotMappedException("dtRichAddRidDb");
    }

    @Override
    public String getDtRichAddRidDbObj() {
        return getDtRichAddRidDb();
    }

    @Override
    public void setDtRichAddRidDbObj(String dtRichAddRidDbObj) {
        setDtRichAddRidDb(dtRichAddRidDbObj);
    }

    @Override
    public String getDtVltDb() {
        throw new FieldNotMappedException("dtVltDb");
    }

    @Override
    public void setDtVltDb(String dtVltDb) {
        throw new FieldNotMappedException("dtVltDb");
    }

    @Override
    public String getDtVltDbObj() {
        return getDtVltDb();
    }

    @Override
    public void setDtVltDbObj(String dtVltDbObj) {
        setDtVltDb(dtVltDbObj);
    }

    @Override
    public String getEstrCntCorrAdd() {
        throw new FieldNotMappedException("estrCntCorrAdd");
    }

    @Override
    public void setEstrCntCorrAdd(String estrCntCorrAdd) {
        throw new FieldNotMappedException("estrCntCorrAdd");
    }

    @Override
    public String getEstrCntCorrAddObj() {
        return getEstrCntCorrAdd();
    }

    @Override
    public void setEstrCntCorrAddObj(String estrCntCorrAddObj) {
        setEstrCntCorrAdd(estrCntCorrAddObj);
    }

    @Override
    public char getFlForzDtVlt() {
        throw new FieldNotMappedException("flForzDtVlt");
    }

    @Override
    public void setFlForzDtVlt(char flForzDtVlt) {
        throw new FieldNotMappedException("flForzDtVlt");
    }

    @Override
    public Character getFlForzDtVltObj() {
        return ((Character)getFlForzDtVlt());
    }

    @Override
    public void setFlForzDtVltObj(Character flForzDtVltObj) {
        setFlForzDtVlt(((char)flForzDtVltObj));
    }

    @Override
    public char getFlIncAutogen() {
        throw new FieldNotMappedException("flIncAutogen");
    }

    @Override
    public void setFlIncAutogen(char flIncAutogen) {
        throw new FieldNotMappedException("flIncAutogen");
    }

    @Override
    public Character getFlIncAutogenObj() {
        return ((Character)getFlIncAutogen());
    }

    @Override
    public void setFlIncAutogenObj(Character flIncAutogenObj) {
        setFlIncAutogen(((char)flIncAutogenObj));
    }

    @Override
    public char getFlMora() {
        throw new FieldNotMappedException("flMora");
    }

    @Override
    public void setFlMora(char flMora) {
        throw new FieldNotMappedException("flMora");
    }

    @Override
    public Character getFlMoraObj() {
        return ((Character)getFlMora());
    }

    @Override
    public void setFlMoraObj(Character flMoraObj) {
        setFlMora(((char)flMoraObj));
    }

    @Override
    public char getFlSoll() {
        throw new FieldNotMappedException("flSoll");
    }

    @Override
    public void setFlSoll(char flSoll) {
        throw new FieldNotMappedException("flSoll");
    }

    @Override
    public Character getFlSollObj() {
        return ((Character)getFlSoll());
    }

    @Override
    public void setFlSollObj(Character flSollObj) {
        setFlSoll(((char)flSollObj));
    }

    @Override
    public char getFlTitDaReinvst() {
        throw new FieldNotMappedException("flTitDaReinvst");
    }

    @Override
    public void setFlTitDaReinvst(char flTitDaReinvst) {
        throw new FieldNotMappedException("flTitDaReinvst");
    }

    @Override
    public Character getFlTitDaReinvstObj() {
        return ((Character)getFlTitDaReinvst());
    }

    @Override
    public void setFlTitDaReinvstObj(Character flTitDaReinvstObj) {
        setFlTitDaReinvst(((char)flTitDaReinvstObj));
    }

    @Override
    public int getFraz() {
        throw new FieldNotMappedException("fraz");
    }

    @Override
    public void setFraz(int fraz) {
        throw new FieldNotMappedException("fraz");
    }

    @Override
    public Integer getFrazObj() {
        return ((Integer)getFraz());
    }

    @Override
    public void setFrazObj(Integer frazObj) {
        setFraz(((int)frazObj));
    }

    @Override
    public String getIbRich() {
        throw new FieldNotMappedException("ibRich");
    }

    @Override
    public void setIbRich(String ibRich) {
        throw new FieldNotMappedException("ibRich");
    }

    @Override
    public String getIbRichObj() {
        return getIbRich();
    }

    @Override
    public void setIbRichObj(String ibRichObj) {
        setIbRich(ibRichObj);
    }

    @Override
    public int getIdMoviChiu() {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        throw new FieldNotMappedException("idMoviChiu");
    }

    @Override
    public Integer getIdMoviChiuObj() {
        return ((Integer)getIdMoviChiu());
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        setIdMoviChiu(((int)idMoviChiuObj));
    }

    @Override
    public int getIdMoviCrz() {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        throw new FieldNotMappedException("idMoviCrz");
    }

    @Override
    public int getIdRappAna() {
        throw new FieldNotMappedException("idRappAna");
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        throw new FieldNotMappedException("idRappAna");
    }

    @Override
    public Integer getIdRappAnaObj() {
        return ((Integer)getIdRappAna());
    }

    @Override
    public void setIdRappAnaObj(Integer idRappAnaObj) {
        setIdRappAna(((int)idRappAnaObj));
    }

    @Override
    public int getIdRappRete() {
        throw new FieldNotMappedException("idRappRete");
    }

    @Override
    public void setIdRappRete(int idRappRete) {
        throw new FieldNotMappedException("idRappRete");
    }

    @Override
    public Integer getIdRappReteObj() {
        return ((Integer)getIdRappRete());
    }

    @Override
    public void setIdRappReteObj(Integer idRappReteObj) {
        setIdRappRete(((int)idRappReteObj));
    }

    @Override
    public int getIdTitCont() {
        throw new FieldNotMappedException("idTitCont");
    }

    @Override
    public void setIdTitCont(int idTitCont) {
        throw new FieldNotMappedException("idTitCont");
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        throw new FieldNotMappedException("impAder");
    }

    @Override
    public AfDecimal getImpAderObj() {
        return getImpAder();
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        setImpAder(new AfDecimal(impAderObj, 15, 3));
    }

    @Override
    public AfDecimal getImpAz() {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        throw new FieldNotMappedException("impAz");
    }

    @Override
    public AfDecimal getImpAzObj() {
        return getImpAz();
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        setImpAz(new AfDecimal(impAzObj, 15, 3));
    }

    @Override
    public AfDecimal getImpPag() {
        throw new FieldNotMappedException("impPag");
    }

    @Override
    public void setImpPag(AfDecimal impPag) {
        throw new FieldNotMappedException("impPag");
    }

    @Override
    public AfDecimal getImpPagObj() {
        return getImpPag();
    }

    @Override
    public void setImpPagObj(AfDecimal impPagObj) {
        setImpPag(new AfDecimal(impPagObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTfr() {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        throw new FieldNotMappedException("impTfr");
    }

    @Override
    public AfDecimal getImpTfrObj() {
        return getImpTfr();
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        setImpTfr(new AfDecimal(impTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        throw new FieldNotMappedException("impTfrStrc");
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        throw new FieldNotMappedException("impTfrStrc");
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        return getImpTfrStrc();
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
    }

    @Override
    public AfDecimal getImpTrasfe() {
        throw new FieldNotMappedException("impTrasfe");
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        throw new FieldNotMappedException("impTrasfe");
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        return getImpTrasfe();
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
    }

    @Override
    public AfDecimal getImpVolo() {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        throw new FieldNotMappedException("impVolo");
    }

    @Override
    public AfDecimal getImpVoloObj() {
        return getImpVolo();
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        setImpVolo(new AfDecimal(impVoloObj, 15, 3));
    }

    @Override
    public String getIsoDtIniPerDb() {
        throw new FieldNotMappedException("isoDtIniPerDb");
    }

    @Override
    public void setIsoDtIniPerDb(String isoDtIniPerDb) {
        throw new FieldNotMappedException("isoDtIniPerDb");
    }

    @Override
    public int getLdbv1591IdOgg() {
        throw new FieldNotMappedException("ldbv1591IdOgg");
    }

    @Override
    public void setLdbv1591IdOgg(int ldbv1591IdOgg) {
        throw new FieldNotMappedException("ldbv1591IdOgg");
    }

    @Override
    public AfDecimal getLdbv1591ImpTot() {
        throw new FieldNotMappedException("ldbv1591ImpTot");
    }

    @Override
    public void setLdbv1591ImpTot(AfDecimal ldbv1591ImpTot) {
        throw new FieldNotMappedException("ldbv1591ImpTot");
    }

    @Override
    public String getLdbv1591TpOgg() {
        throw new FieldNotMappedException("ldbv1591TpOgg");
    }

    @Override
    public void setLdbv1591TpOgg(String ldbv1591TpOgg) {
        throw new FieldNotMappedException("ldbv1591TpOgg");
    }

    @Override
    public int getLdbv2091IdOgg() {
        throw new FieldNotMappedException("ldbv2091IdOgg");
    }

    @Override
    public void setLdbv2091IdOgg(int ldbv2091IdOgg) {
        throw new FieldNotMappedException("ldbv2091IdOgg");
    }

    @Override
    public AfDecimal getLdbv2091TotPremi() {
        throw new FieldNotMappedException("ldbv2091TotPremi");
    }

    @Override
    public void setLdbv2091TotPremi(AfDecimal ldbv2091TotPremi) {
        throw new FieldNotMappedException("ldbv2091TotPremi");
    }

    @Override
    public String getLdbv2091TpOgg() {
        throw new FieldNotMappedException("ldbv2091TpOgg");
    }

    @Override
    public void setLdbv2091TpOgg(String ldbv2091TpOgg) {
        throw new FieldNotMappedException("ldbv2091TpOgg");
    }

    @Override
    public String getLdbv2091TpStatTit01() {
        throw new FieldNotMappedException("ldbv2091TpStatTit01");
    }

    @Override
    public void setLdbv2091TpStatTit01(String ldbv2091TpStatTit01) {
        throw new FieldNotMappedException("ldbv2091TpStatTit01");
    }

    @Override
    public String getLdbv2091TpStatTit02() {
        throw new FieldNotMappedException("ldbv2091TpStatTit02");
    }

    @Override
    public void setLdbv2091TpStatTit02(String ldbv2091TpStatTit02) {
        throw new FieldNotMappedException("ldbv2091TpStatTit02");
    }

    @Override
    public String getLdbv2091TpStatTit03() {
        throw new FieldNotMappedException("ldbv2091TpStatTit03");
    }

    @Override
    public void setLdbv2091TpStatTit03(String ldbv2091TpStatTit03) {
        throw new FieldNotMappedException("ldbv2091TpStatTit03");
    }

    @Override
    public String getLdbv2091TpStatTit04() {
        throw new FieldNotMappedException("ldbv2091TpStatTit04");
    }

    @Override
    public void setLdbv2091TpStatTit04(String ldbv2091TpStatTit04) {
        throw new FieldNotMappedException("ldbv2091TpStatTit04");
    }

    @Override
    public String getLdbv2091TpStatTit05() {
        throw new FieldNotMappedException("ldbv2091TpStatTit05");
    }

    @Override
    public void setLdbv2091TpStatTit05(String ldbv2091TpStatTit05) {
        throw new FieldNotMappedException("ldbv2091TpStatTit05");
    }

    @Override
    public String getLdbv6151DtDecorPrestDb() {
        throw new FieldNotMappedException("ldbv6151DtDecorPrestDb");
    }

    @Override
    public void setLdbv6151DtDecorPrestDb(String ldbv6151DtDecorPrestDb) {
        throw new FieldNotMappedException("ldbv6151DtDecorPrestDb");
    }

    @Override
    public String getLdbv6151DtMaxDb() {
        throw new FieldNotMappedException("ldbv6151DtMaxDb");
    }

    @Override
    public void setLdbv6151DtMaxDb(String ldbv6151DtMaxDb) {
        throw new FieldNotMappedException("ldbv6151DtMaxDb");
    }

    @Override
    public String getLdbv6151DtMaxDbObj() {
        return getLdbv6151DtMaxDb();
    }

    @Override
    public void setLdbv6151DtMaxDbObj(String ldbv6151DtMaxDbObj) {
        setLdbv6151DtMaxDb(ldbv6151DtMaxDbObj);
    }

    @Override
    public int getLdbv6151IdOgg() {
        throw new FieldNotMappedException("ldbv6151IdOgg");
    }

    @Override
    public void setLdbv6151IdOgg(int ldbv6151IdOgg) {
        throw new FieldNotMappedException("ldbv6151IdOgg");
    }

    @Override
    public String getLdbv6151TpOgg() {
        throw new FieldNotMappedException("ldbv6151TpOgg");
    }

    @Override
    public void setLdbv6151TpOgg(String ldbv6151TpOgg) {
        throw new FieldNotMappedException("ldbv6151TpOgg");
    }

    @Override
    public String getLdbv6151TpStatTit() {
        throw new FieldNotMappedException("ldbv6151TpStatTit");
    }

    @Override
    public void setLdbv6151TpStatTit(String ldbv6151TpStatTit) {
        throw new FieldNotMappedException("ldbv6151TpStatTit");
    }

    @Override
    public String getLdbv6151TpTit01() {
        throw new FieldNotMappedException("ldbv6151TpTit01");
    }

    @Override
    public void setLdbv6151TpTit01(String ldbv6151TpTit01) {
        throw new FieldNotMappedException("ldbv6151TpTit01");
    }

    @Override
    public String getLdbv6151TpTit02() {
        throw new FieldNotMappedException("ldbv6151TpTit02");
    }

    @Override
    public void setLdbv6151TpTit02(String ldbv6151TpTit02) {
        throw new FieldNotMappedException("ldbv6151TpTit02");
    }

    @Override
    public String getLdbvb441DtMaxDb() {
        throw new FieldNotMappedException("ldbvb441DtMaxDb");
    }

    @Override
    public void setLdbvb441DtMaxDb(String ldbvb441DtMaxDb) {
        throw new FieldNotMappedException("ldbvb441DtMaxDb");
    }

    @Override
    public String getLdbvb441DtMaxDbObj() {
        return getLdbvb441DtMaxDb();
    }

    @Override
    public void setLdbvb441DtMaxDbObj(String ldbvb441DtMaxDbObj) {
        setLdbvb441DtMaxDb(ldbvb441DtMaxDbObj);
    }

    @Override
    public int getLdbvb441IdOgg() {
        throw new FieldNotMappedException("ldbvb441IdOgg");
    }

    @Override
    public void setLdbvb441IdOgg(int ldbvb441IdOgg) {
        throw new FieldNotMappedException("ldbvb441IdOgg");
    }

    @Override
    public String getLdbvb441TpOgg() {
        throw new FieldNotMappedException("ldbvb441TpOgg");
    }

    @Override
    public void setLdbvb441TpOgg(String ldbvb441TpOgg) {
        throw new FieldNotMappedException("ldbvb441TpOgg");
    }

    @Override
    public String getLdbvb441TpStatTit1() {
        throw new FieldNotMappedException("ldbvb441TpStatTit1");
    }

    @Override
    public void setLdbvb441TpStatTit1(String ldbvb441TpStatTit1) {
        throw new FieldNotMappedException("ldbvb441TpStatTit1");
    }

    @Override
    public String getLdbvb441TpStatTit2() {
        throw new FieldNotMappedException("ldbvb441TpStatTit2");
    }

    @Override
    public void setLdbvb441TpStatTit2(String ldbvb441TpStatTit2) {
        throw new FieldNotMappedException("ldbvb441TpStatTit2");
    }

    @Override
    public String getLdbvb441TpStatTit3() {
        throw new FieldNotMappedException("ldbvb441TpStatTit3");
    }

    @Override
    public void setLdbvb441TpStatTit3(String ldbvb441TpStatTit3) {
        throw new FieldNotMappedException("ldbvb441TpStatTit3");
    }

    @Override
    public String getLdbvb441TpTit01() {
        throw new FieldNotMappedException("ldbvb441TpTit01");
    }

    @Override
    public void setLdbvb441TpTit01(String ldbvb441TpTit01) {
        throw new FieldNotMappedException("ldbvb441TpTit01");
    }

    @Override
    public String getLdbvb441TpTit02() {
        throw new FieldNotMappedException("ldbvb441TpTit02");
    }

    @Override
    public void setLdbvb441TpTit02(String ldbvb441TpTit02) {
        throw new FieldNotMappedException("ldbvb441TpTit02");
    }

    @Override
    public String getLdbvb471DtMaxDb() {
        return ldbvb471.getLdbvb471DtMaxDb();
    }

    @Override
    public void setLdbvb471DtMaxDb(String ldbvb471DtMaxDb) {
        this.ldbvb471.setLdbvb471DtMaxDb(ldbvb471DtMaxDb);
    }

    @Override
    public int getLdbvb471IdOgg() {
        return ldbvb471.getLdbvb471IdOgg();
    }

    @Override
    public void setLdbvb471IdOgg(int ldbvb471IdOgg) {
        this.ldbvb471.setLdbvb471IdOgg(ldbvb471IdOgg);
    }

    @Override
    public String getLdbvb471TpOgg() {
        return ldbvb471.getLdbvb471TpOgg();
    }

    @Override
    public void setLdbvb471TpOgg(String ldbvb471TpOgg) {
        this.ldbvb471.setLdbvb471TpOgg(ldbvb471TpOgg);
    }

    @Override
    public String getLdbvb471TpStatTit1() {
        return ldbvb471.getLdbvb471TpStatTit1();
    }

    @Override
    public void setLdbvb471TpStatTit1(String ldbvb471TpStatTit1) {
        this.ldbvb471.setLdbvb471TpStatTit1(ldbvb471TpStatTit1);
    }

    @Override
    public String getLdbvb471TpStatTit2() {
        return ldbvb471.getLdbvb471TpStatTit2();
    }

    @Override
    public void setLdbvb471TpStatTit2(String ldbvb471TpStatTit2) {
        this.ldbvb471.setLdbvb471TpStatTit2(ldbvb471TpStatTit2);
    }

    @Override
    public String getLdbvb471TpStatTit3() {
        return ldbvb471.getLdbvb471TpStatTit3();
    }

    @Override
    public void setLdbvb471TpStatTit3(String ldbvb471TpStatTit3) {
        this.ldbvb471.setLdbvb471TpStatTit3(ldbvb471TpStatTit3);
    }

    @Override
    public String getLdbvb471TpTit01() {
        return ldbvb471.getLdbvb471TpTit01();
    }

    @Override
    public void setLdbvb471TpTit01(String ldbvb471TpTit01) {
        this.ldbvb471.setLdbvb471TpTit01(ldbvb471TpTit01);
    }

    @Override
    public String getLdbvb471TpTit02() {
        return ldbvb471.getLdbvb471TpTit02();
    }

    @Override
    public void setLdbvb471TpTit02(String ldbvb471TpTit02) {
        this.ldbvb471.setLdbvb471TpTit02(ldbvb471TpTit02);
    }

    @Override
    public AfDecimal getLdbvf111CumPreVers() {
        throw new FieldNotMappedException("ldbvf111CumPreVers");
    }

    @Override
    public void setLdbvf111CumPreVers(AfDecimal ldbvf111CumPreVers) {
        throw new FieldNotMappedException("ldbvf111CumPreVers");
    }

    @Override
    public int getLdbvf111IdOgg() {
        throw new FieldNotMappedException("ldbvf111IdOgg");
    }

    @Override
    public void setLdbvf111IdOgg(int ldbvf111IdOgg) {
        throw new FieldNotMappedException("ldbvf111IdOgg");
    }

    @Override
    public String getLdbvf111TpStatTit1() {
        throw new FieldNotMappedException("ldbvf111TpStatTit1");
    }

    @Override
    public void setLdbvf111TpStatTit1(String ldbvf111TpStatTit1) {
        throw new FieldNotMappedException("ldbvf111TpStatTit1");
    }

    @Override
    public String getLdbvf111TpStatTit2() {
        throw new FieldNotMappedException("ldbvf111TpStatTit2");
    }

    @Override
    public void setLdbvf111TpStatTit2(String ldbvf111TpStatTit2) {
        throw new FieldNotMappedException("ldbvf111TpStatTit2");
    }

    @Override
    public String getLdbvf111TpStatTit3() {
        throw new FieldNotMappedException("ldbvf111TpStatTit3");
    }

    @Override
    public void setLdbvf111TpStatTit3(String ldbvf111TpStatTit3) {
        throw new FieldNotMappedException("ldbvf111TpStatTit3");
    }

    @Override
    public String getLdbvf111TpStatTit4() {
        throw new FieldNotMappedException("ldbvf111TpStatTit4");
    }

    @Override
    public void setLdbvf111TpStatTit4(String ldbvf111TpStatTit4) {
        throw new FieldNotMappedException("ldbvf111TpStatTit4");
    }

    @Override
    public String getLdbvf111TpStatTit5() {
        throw new FieldNotMappedException("ldbvf111TpStatTit5");
    }

    @Override
    public void setLdbvf111TpStatTit5(String ldbvf111TpStatTit5) {
        throw new FieldNotMappedException("ldbvf111TpStatTit5");
    }

    @Override
    public int getNumRatAccorpate() {
        throw new FieldNotMappedException("numRatAccorpate");
    }

    @Override
    public void setNumRatAccorpate(int numRatAccorpate) {
        throw new FieldNotMappedException("numRatAccorpate");
    }

    @Override
    public Integer getNumRatAccorpateObj() {
        return ((Integer)getNumRatAccorpate());
    }

    @Override
    public void setNumRatAccorpateObj(Integer numRatAccorpateObj) {
        setNumRatAccorpate(((int)numRatAccorpateObj));
    }

    @Override
    public int getProgTit() {
        throw new FieldNotMappedException("progTit");
    }

    @Override
    public void setProgTit(int progTit) {
        throw new FieldNotMappedException("progTit");
    }

    @Override
    public Integer getProgTitObj() {
        return ((Integer)getProgTit());
    }

    @Override
    public void setProgTitObj(Integer progTitObj) {
        setProgTit(((int)progTitObj));
    }

    @Override
    public long getTitDsRiga() {
        throw new FieldNotMappedException("titDsRiga");
    }

    @Override
    public void setTitDsRiga(long titDsRiga) {
        throw new FieldNotMappedException("titDsRiga");
    }

    @Override
    public String getTitDtIniCopDb() {
        throw new FieldNotMappedException("titDtIniCopDb");
    }

    @Override
    public void setTitDtIniCopDb(String titDtIniCopDb) {
        throw new FieldNotMappedException("titDtIniCopDb");
    }

    @Override
    public String getTitDtIniCopDbObj() {
        return getTitDtIniCopDb();
    }

    @Override
    public void setTitDtIniCopDbObj(String titDtIniCopDbObj) {
        setTitDtIniCopDb(titDtIniCopDbObj);
    }

    @Override
    public int getTitIdOgg() {
        throw new FieldNotMappedException("titIdOgg");
    }

    @Override
    public void setTitIdOgg(int titIdOgg) {
        throw new FieldNotMappedException("titIdOgg");
    }

    @Override
    public String getTitTpOgg() {
        throw new FieldNotMappedException("titTpOgg");
    }

    @Override
    public void setTitTpOgg(String titTpOgg) {
        throw new FieldNotMappedException("titTpOgg");
    }

    @Override
    public String getTitTpPreTit() {
        throw new FieldNotMappedException("titTpPreTit");
    }

    @Override
    public void setTitTpPreTit(String titTpPreTit) {
        throw new FieldNotMappedException("titTpPreTit");
    }

    @Override
    public String getTitTpStatTit() {
        throw new FieldNotMappedException("titTpStatTit");
    }

    @Override
    public void setTitTpStatTit(String titTpStatTit) {
        throw new FieldNotMappedException("titTpStatTit");
    }

    @Override
    public String getTitTpTit() {
        throw new FieldNotMappedException("titTpTit");
    }

    @Override
    public void setTitTpTit(String titTpTit) {
        throw new FieldNotMappedException("titTpTit");
    }

    @Override
    public AfDecimal getTotAcqExp() {
        throw new FieldNotMappedException("totAcqExp");
    }

    @Override
    public void setTotAcqExp(AfDecimal totAcqExp) {
        throw new FieldNotMappedException("totAcqExp");
    }

    @Override
    public AfDecimal getTotAcqExpObj() {
        return getTotAcqExp();
    }

    @Override
    public void setTotAcqExpObj(AfDecimal totAcqExpObj) {
        setTotAcqExp(new AfDecimal(totAcqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCarAcq() {
        throw new FieldNotMappedException("totCarAcq");
    }

    @Override
    public void setTotCarAcq(AfDecimal totCarAcq) {
        throw new FieldNotMappedException("totCarAcq");
    }

    @Override
    public AfDecimal getTotCarAcqObj() {
        return getTotCarAcq();
    }

    @Override
    public void setTotCarAcqObj(AfDecimal totCarAcqObj) {
        setTotCarAcq(new AfDecimal(totCarAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCarGest() {
        throw new FieldNotMappedException("totCarGest");
    }

    @Override
    public void setTotCarGest(AfDecimal totCarGest) {
        throw new FieldNotMappedException("totCarGest");
    }

    @Override
    public AfDecimal getTotCarGestObj() {
        return getTotCarGest();
    }

    @Override
    public void setTotCarGestObj(AfDecimal totCarGestObj) {
        setTotCarGest(new AfDecimal(totCarGestObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCarIas() {
        throw new FieldNotMappedException("totCarIas");
    }

    @Override
    public void setTotCarIas(AfDecimal totCarIas) {
        throw new FieldNotMappedException("totCarIas");
    }

    @Override
    public AfDecimal getTotCarIasObj() {
        return getTotCarIas();
    }

    @Override
    public void setTotCarIasObj(AfDecimal totCarIasObj) {
        setTotCarIas(new AfDecimal(totCarIasObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCarInc() {
        throw new FieldNotMappedException("totCarInc");
    }

    @Override
    public void setTotCarInc(AfDecimal totCarInc) {
        throw new FieldNotMappedException("totCarInc");
    }

    @Override
    public AfDecimal getTotCarIncObj() {
        return getTotCarInc();
    }

    @Override
    public void setTotCarIncObj(AfDecimal totCarIncObj) {
        setTotCarInc(new AfDecimal(totCarIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCnbtAntirac() {
        throw new FieldNotMappedException("totCnbtAntirac");
    }

    @Override
    public void setTotCnbtAntirac(AfDecimal totCnbtAntirac) {
        throw new FieldNotMappedException("totCnbtAntirac");
    }

    @Override
    public AfDecimal getTotCnbtAntiracObj() {
        return getTotCnbtAntirac();
    }

    @Override
    public void setTotCnbtAntiracObj(AfDecimal totCnbtAntiracObj) {
        setTotCnbtAntirac(new AfDecimal(totCnbtAntiracObj, 15, 3));
    }

    @Override
    public AfDecimal getTotCommisInter() {
        throw new FieldNotMappedException("totCommisInter");
    }

    @Override
    public void setTotCommisInter(AfDecimal totCommisInter) {
        throw new FieldNotMappedException("totCommisInter");
    }

    @Override
    public AfDecimal getTotCommisInterObj() {
        return getTotCommisInter();
    }

    @Override
    public void setTotCommisInterObj(AfDecimal totCommisInterObj) {
        setTotCommisInter(new AfDecimal(totCommisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getTotDir() {
        throw new FieldNotMappedException("totDir");
    }

    @Override
    public void setTotDir(AfDecimal totDir) {
        throw new FieldNotMappedException("totDir");
    }

    @Override
    public AfDecimal getTotDirObj() {
        return getTotDir();
    }

    @Override
    public void setTotDirObj(AfDecimal totDirObj) {
        setTotDir(new AfDecimal(totDirObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrFraz() {
        throw new FieldNotMappedException("totIntrFraz");
    }

    @Override
    public void setTotIntrFraz(AfDecimal totIntrFraz) {
        throw new FieldNotMappedException("totIntrFraz");
    }

    @Override
    public AfDecimal getTotIntrFrazObj() {
        return getTotIntrFraz();
    }

    @Override
    public void setTotIntrFrazObj(AfDecimal totIntrFrazObj) {
        setTotIntrFraz(new AfDecimal(totIntrFrazObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrMora() {
        throw new FieldNotMappedException("totIntrMora");
    }

    @Override
    public void setTotIntrMora(AfDecimal totIntrMora) {
        throw new FieldNotMappedException("totIntrMora");
    }

    @Override
    public AfDecimal getTotIntrMoraObj() {
        return getTotIntrMora();
    }

    @Override
    public void setTotIntrMoraObj(AfDecimal totIntrMoraObj) {
        setTotIntrMora(new AfDecimal(totIntrMoraObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrPrest() {
        return ws.getTitCont().getTitTotIntrPrest().getTitTotIntrPrest();
    }

    @Override
    public void setTotIntrPrest(AfDecimal totIntrPrest) {
        this.ws.getTitCont().getTitTotIntrPrest().setTitTotIntrPrest(totIntrPrest.copy());
    }

    @Override
    public AfDecimal getTotIntrPrestObj() {
        if (ws.getIndTitCont().getTotIntrPrest() >= 0) {
            return getTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrPrestObj(AfDecimal totIntrPrestObj) {
        if (totIntrPrestObj != null) {
            setTotIntrPrest(new AfDecimal(totIntrPrestObj, 15, 3));
            ws.getIndTitCont().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndTitCont().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrRetdt() {
        throw new FieldNotMappedException("totIntrRetdt");
    }

    @Override
    public void setTotIntrRetdt(AfDecimal totIntrRetdt) {
        throw new FieldNotMappedException("totIntrRetdt");
    }

    @Override
    public AfDecimal getTotIntrRetdtObj() {
        return getTotIntrRetdt();
    }

    @Override
    public void setTotIntrRetdtObj(AfDecimal totIntrRetdtObj) {
        setTotIntrRetdt(new AfDecimal(totIntrRetdtObj, 15, 3));
    }

    @Override
    public AfDecimal getTotIntrRiat() {
        throw new FieldNotMappedException("totIntrRiat");
    }

    @Override
    public void setTotIntrRiat(AfDecimal totIntrRiat) {
        throw new FieldNotMappedException("totIntrRiat");
    }

    @Override
    public AfDecimal getTotIntrRiatObj() {
        return getTotIntrRiat();
    }

    @Override
    public void setTotIntrRiatObj(AfDecimal totIntrRiatObj) {
        setTotIntrRiat(new AfDecimal(totIntrRiatObj, 15, 3));
    }

    @Override
    public AfDecimal getTotManfeeAntic() {
        throw new FieldNotMappedException("totManfeeAntic");
    }

    @Override
    public void setTotManfeeAntic(AfDecimal totManfeeAntic) {
        throw new FieldNotMappedException("totManfeeAntic");
    }

    @Override
    public AfDecimal getTotManfeeAnticObj() {
        return getTotManfeeAntic();
    }

    @Override
    public void setTotManfeeAnticObj(AfDecimal totManfeeAnticObj) {
        setTotManfeeAntic(new AfDecimal(totManfeeAnticObj, 15, 3));
    }

    @Override
    public AfDecimal getTotManfeeRec() {
        throw new FieldNotMappedException("totManfeeRec");
    }

    @Override
    public void setTotManfeeRec(AfDecimal totManfeeRec) {
        throw new FieldNotMappedException("totManfeeRec");
    }

    @Override
    public AfDecimal getTotManfeeRecObj() {
        return getTotManfeeRec();
    }

    @Override
    public void setTotManfeeRecObj(AfDecimal totManfeeRecObj) {
        setTotManfeeRec(new AfDecimal(totManfeeRecObj, 15, 3));
    }

    @Override
    public AfDecimal getTotManfeeRicor() {
        throw new FieldNotMappedException("totManfeeRicor");
    }

    @Override
    public void setTotManfeeRicor(AfDecimal totManfeeRicor) {
        throw new FieldNotMappedException("totManfeeRicor");
    }

    @Override
    public AfDecimal getTotManfeeRicorObj() {
        return getTotManfeeRicor();
    }

    @Override
    public void setTotManfeeRicorObj(AfDecimal totManfeeRicorObj) {
        setTotManfeeRicor(new AfDecimal(totManfeeRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTotPreNet() {
        throw new FieldNotMappedException("totPreNet");
    }

    @Override
    public void setTotPreNet(AfDecimal totPreNet) {
        throw new FieldNotMappedException("totPreNet");
    }

    @Override
    public AfDecimal getTotPreNetObj() {
        return getTotPreNet();
    }

    @Override
    public void setTotPreNetObj(AfDecimal totPreNetObj) {
        setTotPreNet(new AfDecimal(totPreNetObj, 15, 3));
    }

    @Override
    public AfDecimal getTotPrePpIas() {
        throw new FieldNotMappedException("totPrePpIas");
    }

    @Override
    public void setTotPrePpIas(AfDecimal totPrePpIas) {
        throw new FieldNotMappedException("totPrePpIas");
    }

    @Override
    public AfDecimal getTotPrePpIasObj() {
        return getTotPrePpIas();
    }

    @Override
    public void setTotPrePpIasObj(AfDecimal totPrePpIasObj) {
        setTotPrePpIas(new AfDecimal(totPrePpIasObj, 15, 3));
    }

    @Override
    public AfDecimal getTotPreSoloRsh() {
        throw new FieldNotMappedException("totPreSoloRsh");
    }

    @Override
    public void setTotPreSoloRsh(AfDecimal totPreSoloRsh) {
        throw new FieldNotMappedException("totPreSoloRsh");
    }

    @Override
    public AfDecimal getTotPreSoloRshObj() {
        return getTotPreSoloRsh();
    }

    @Override
    public void setTotPreSoloRshObj(AfDecimal totPreSoloRshObj) {
        setTotPreSoloRsh(new AfDecimal(totPreSoloRshObj, 15, 3));
    }

    @Override
    public AfDecimal getTotPreTot() {
        throw new FieldNotMappedException("totPreTot");
    }

    @Override
    public void setTotPreTot(AfDecimal totPreTot) {
        throw new FieldNotMappedException("totPreTot");
    }

    @Override
    public AfDecimal getTotPreTotObj() {
        return getTotPreTot();
    }

    @Override
    public void setTotPreTotObj(AfDecimal totPreTotObj) {
        setTotPreTot(new AfDecimal(totPreTotObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvAcq1aa() {
        throw new FieldNotMappedException("totProvAcq1aa");
    }

    @Override
    public void setTotProvAcq1aa(AfDecimal totProvAcq1aa) {
        throw new FieldNotMappedException("totProvAcq1aa");
    }

    @Override
    public AfDecimal getTotProvAcq1aaObj() {
        return getTotProvAcq1aa();
    }

    @Override
    public void setTotProvAcq1aaObj(AfDecimal totProvAcq1aaObj) {
        setTotProvAcq1aa(new AfDecimal(totProvAcq1aaObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvAcq2aa() {
        throw new FieldNotMappedException("totProvAcq2aa");
    }

    @Override
    public void setTotProvAcq2aa(AfDecimal totProvAcq2aa) {
        throw new FieldNotMappedException("totProvAcq2aa");
    }

    @Override
    public AfDecimal getTotProvAcq2aaObj() {
        return getTotProvAcq2aa();
    }

    @Override
    public void setTotProvAcq2aaObj(AfDecimal totProvAcq2aaObj) {
        setTotProvAcq2aa(new AfDecimal(totProvAcq2aaObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvDaRec() {
        throw new FieldNotMappedException("totProvDaRec");
    }

    @Override
    public void setTotProvDaRec(AfDecimal totProvDaRec) {
        throw new FieldNotMappedException("totProvDaRec");
    }

    @Override
    public AfDecimal getTotProvDaRecObj() {
        return getTotProvDaRec();
    }

    @Override
    public void setTotProvDaRecObj(AfDecimal totProvDaRecObj) {
        setTotProvDaRec(new AfDecimal(totProvDaRecObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvInc() {
        throw new FieldNotMappedException("totProvInc");
    }

    @Override
    public void setTotProvInc(AfDecimal totProvInc) {
        throw new FieldNotMappedException("totProvInc");
    }

    @Override
    public AfDecimal getTotProvIncObj() {
        return getTotProvInc();
    }

    @Override
    public void setTotProvIncObj(AfDecimal totProvIncObj) {
        setTotProvInc(new AfDecimal(totProvIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTotProvRicor() {
        throw new FieldNotMappedException("totProvRicor");
    }

    @Override
    public void setTotProvRicor(AfDecimal totProvRicor) {
        throw new FieldNotMappedException("totProvRicor");
    }

    @Override
    public AfDecimal getTotProvRicorObj() {
        return getTotProvRicor();
    }

    @Override
    public void setTotProvRicorObj(AfDecimal totProvRicorObj) {
        setTotProvRicor(new AfDecimal(totProvRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTotRemunAss() {
        throw new FieldNotMappedException("totRemunAss");
    }

    @Override
    public void setTotRemunAss(AfDecimal totRemunAss) {
        throw new FieldNotMappedException("totRemunAss");
    }

    @Override
    public AfDecimal getTotRemunAssObj() {
        return getTotRemunAss();
    }

    @Override
    public void setTotRemunAssObj(AfDecimal totRemunAssObj) {
        setTotRemunAss(new AfDecimal(totRemunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprAlt() {
        throw new FieldNotMappedException("totSoprAlt");
    }

    @Override
    public void setTotSoprAlt(AfDecimal totSoprAlt) {
        throw new FieldNotMappedException("totSoprAlt");
    }

    @Override
    public AfDecimal getTotSoprAltObj() {
        return getTotSoprAlt();
    }

    @Override
    public void setTotSoprAltObj(AfDecimal totSoprAltObj) {
        setTotSoprAlt(new AfDecimal(totSoprAltObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprProf() {
        throw new FieldNotMappedException("totSoprProf");
    }

    @Override
    public void setTotSoprProf(AfDecimal totSoprProf) {
        throw new FieldNotMappedException("totSoprProf");
    }

    @Override
    public AfDecimal getTotSoprProfObj() {
        return getTotSoprProf();
    }

    @Override
    public void setTotSoprProfObj(AfDecimal totSoprProfObj) {
        setTotSoprProf(new AfDecimal(totSoprProfObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprSan() {
        throw new FieldNotMappedException("totSoprSan");
    }

    @Override
    public void setTotSoprSan(AfDecimal totSoprSan) {
        throw new FieldNotMappedException("totSoprSan");
    }

    @Override
    public AfDecimal getTotSoprSanObj() {
        return getTotSoprSan();
    }

    @Override
    public void setTotSoprSanObj(AfDecimal totSoprSanObj) {
        setTotSoprSan(new AfDecimal(totSoprSanObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprSpo() {
        throw new FieldNotMappedException("totSoprSpo");
    }

    @Override
    public void setTotSoprSpo(AfDecimal totSoprSpo) {
        throw new FieldNotMappedException("totSoprSpo");
    }

    @Override
    public AfDecimal getTotSoprSpoObj() {
        return getTotSoprSpo();
    }

    @Override
    public void setTotSoprSpoObj(AfDecimal totSoprSpoObj) {
        setTotSoprSpo(new AfDecimal(totSoprSpoObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSoprTec() {
        throw new FieldNotMappedException("totSoprTec");
    }

    @Override
    public void setTotSoprTec(AfDecimal totSoprTec) {
        throw new FieldNotMappedException("totSoprTec");
    }

    @Override
    public AfDecimal getTotSoprTecObj() {
        return getTotSoprTec();
    }

    @Override
    public void setTotSoprTecObj(AfDecimal totSoprTecObj) {
        setTotSoprTec(new AfDecimal(totSoprTecObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSpeAge() {
        throw new FieldNotMappedException("totSpeAge");
    }

    @Override
    public void setTotSpeAge(AfDecimal totSpeAge) {
        throw new FieldNotMappedException("totSpeAge");
    }

    @Override
    public AfDecimal getTotSpeAgeObj() {
        return getTotSpeAge();
    }

    @Override
    public void setTotSpeAgeObj(AfDecimal totSpeAgeObj) {
        setTotSpeAge(new AfDecimal(totSpeAgeObj, 15, 3));
    }

    @Override
    public AfDecimal getTotSpeMed() {
        throw new FieldNotMappedException("totSpeMed");
    }

    @Override
    public void setTotSpeMed(AfDecimal totSpeMed) {
        throw new FieldNotMappedException("totSpeMed");
    }

    @Override
    public AfDecimal getTotSpeMedObj() {
        return getTotSpeMed();
    }

    @Override
    public void setTotSpeMedObj(AfDecimal totSpeMedObj) {
        setTotSpeMed(new AfDecimal(totSpeMedObj, 15, 3));
    }

    @Override
    public AfDecimal getTotTax() {
        throw new FieldNotMappedException("totTax");
    }

    @Override
    public void setTotTax(AfDecimal totTax) {
        throw new FieldNotMappedException("totTax");
    }

    @Override
    public AfDecimal getTotTaxObj() {
        return getTotTax();
    }

    @Override
    public void setTotTaxObj(AfDecimal totTaxObj) {
        setTotTax(new AfDecimal(totTaxObj, 15, 3));
    }

    @Override
    public String getTpCausDispStor() {
        throw new FieldNotMappedException("tpCausDispStor");
    }

    @Override
    public void setTpCausDispStor(String tpCausDispStor) {
        throw new FieldNotMappedException("tpCausDispStor");
    }

    @Override
    public String getTpCausDispStorObj() {
        return getTpCausDispStor();
    }

    @Override
    public void setTpCausDispStorObj(String tpCausDispStorObj) {
        setTpCausDispStor(tpCausDispStorObj);
    }

    @Override
    public String getTpCausRimb() {
        throw new FieldNotMappedException("tpCausRimb");
    }

    @Override
    public void setTpCausRimb(String tpCausRimb) {
        throw new FieldNotMappedException("tpCausRimb");
    }

    @Override
    public String getTpCausRimbObj() {
        return getTpCausRimb();
    }

    @Override
    public void setTpCausRimbObj(String tpCausRimbObj) {
        setTpCausRimb(tpCausRimbObj);
    }

    @Override
    public int getTpCausStor() {
        throw new FieldNotMappedException("tpCausStor");
    }

    @Override
    public void setTpCausStor(int tpCausStor) {
        throw new FieldNotMappedException("tpCausStor");
    }

    @Override
    public Integer getTpCausStorObj() {
        return ((Integer)getTpCausStor());
    }

    @Override
    public void setTpCausStorObj(Integer tpCausStorObj) {
        setTpCausStor(((int)tpCausStorObj));
    }

    @Override
    public String getTpEsiRid() {
        throw new FieldNotMappedException("tpEsiRid");
    }

    @Override
    public void setTpEsiRid(String tpEsiRid) {
        throw new FieldNotMappedException("tpEsiRid");
    }

    @Override
    public String getTpEsiRidObj() {
        return getTpEsiRid();
    }

    @Override
    public void setTpEsiRidObj(String tpEsiRidObj) {
        setTpEsiRid(tpEsiRidObj);
    }

    @Override
    public String getTpMezPagAdd() {
        throw new FieldNotMappedException("tpMezPagAdd");
    }

    @Override
    public void setTpMezPagAdd(String tpMezPagAdd) {
        throw new FieldNotMappedException("tpMezPagAdd");
    }

    @Override
    public String getTpMezPagAddObj() {
        return getTpMezPagAdd();
    }

    @Override
    public void setTpMezPagAddObj(String tpMezPagAddObj) {
        setTpMezPagAdd(tpMezPagAddObj);
    }

    @Override
    public String getTpTitMigraz() {
        throw new FieldNotMappedException("tpTitMigraz");
    }

    @Override
    public void setTpTitMigraz(String tpTitMigraz) {
        throw new FieldNotMappedException("tpTitMigraz");
    }

    @Override
    public String getTpTitMigrazObj() {
        return getTpTitMigraz();
    }

    @Override
    public void setTpTitMigrazObj(String tpTitMigrazObj) {
        setTpTitMigraz(tpTitMigrazObj);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }

    @Override
    public long getWsTsInfinito1() {
        throw new FieldNotMappedException("wsTsInfinito1");
    }

    @Override
    public void setWsTsInfinito1(long wsTsInfinito1) {
        throw new FieldNotMappedException("wsTsInfinito1");
    }
}
