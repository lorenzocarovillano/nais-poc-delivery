package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ParamOggDao;
import it.accenture.jnais.commons.data.to.IParamOgg;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs1130Data;
import it.accenture.jnais.ws.ParamOgg;
import it.accenture.jnais.ws.redefines.PogIdMoviChiu;
import it.accenture.jnais.ws.redefines.PogValDt;
import it.accenture.jnais.ws.redefines.PogValImp;
import it.accenture.jnais.ws.redefines.PogValNum;
import it.accenture.jnais.ws.redefines.PogValPc;
import it.accenture.jnais.ws.redefines.PogValTs;

/**Original name: LDBS1130<br>
 * <pre>AUTHOR.        ATS.
 * DATE-WRITTEN.  SETTEMBRE 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO PER ACCESSO RISORSE DB               *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs1130 extends Program implements IParamOgg {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ParamOggDao paramOggDao = new ParamOggDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs1130Data ws = new Ldbs1130Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: PARAM-OGG
    private ParamOgg paramOgg;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS1130_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ParamOgg paramOgg) {
        this.idsv0003 = idsv0003;
        this.paramOgg = paramOgg;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO AREA-LDBV1131.
        ws.getAreaLdbv1131().setAreaLdbv1131Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              OR IDSV0003-TRATT-X-COMPETENZA
            //              END-EVALUATE
            //           ELSE
            //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto() || this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC           THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC           THRU A200-EX
                        a200ElaboraWc();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs1130 getInstance() {
        return ((Ldbs1130)Programs.getInstance(Ldbs1130.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS1130'             TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS1130");
        // COB_CODE: MOVE 'PARAM_OGG'           TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("PARAM_OGG");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: MOVE ZEROES                   TO WS-TIMESTAMP-NUM.
        ws.getWsTimestamp().setWsTimestampNum(0);
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
        // COB_CODE: ACCEPT WS-TIMESTAMP(1:8)       FROM DATE YYYYMMDD.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getDateYYYYMMDD(), 1, 8));
        // COB_CODE: ACCEPT WS-TIMESTAMP(9:6)       FROM TIME.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getTimeHHMMSSMM(), 9, 6));
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC<br>*/
    private void a200ElaboraWc() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A245-SELECT-WC              THRU A245-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A245-SELECT-WC              THRU A245-EX
            a245SelectWc();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A245-SELECT-WC<br>
	 * <pre>----
	 * ----  gestione WC
	 * ----</pre>*/
    private void a245SelectWc() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_PARAM_OGG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_PARAM
        //                ,TP_PARAM
        //                ,TP_D
        //                ,VAL_IMP
        //                ,VAL_DT
        //                ,VAL_TS
        //                ,VAL_TXT
        //                ,VAL_FL
        //                ,VAL_NUM
        //                ,VAL_PC
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //             INTO
        //                 :POG-ID-PARAM-OGG
        //                 ,:POG-ID-OGG
        //                 ,:POG-TP-OGG
        //                 ,:POG-ID-MOVI-CRZ
        //                 ,:POG-ID-MOVI-CHIU
        //                  :IND-POG-ID-MOVI-CHIU
        //                 ,:POG-DT-INI-EFF-DB
        //                 ,:POG-DT-END-EFF-DB
        //                 ,:POG-COD-COMP-ANIA
        //                 ,:POG-COD-PARAM
        //                  :IND-POG-COD-PARAM
        //                 ,:POG-TP-PARAM
        //                  :IND-POG-TP-PARAM
        //                 ,:POG-TP-D
        //                  :IND-POG-TP-D
        //                 ,:POG-VAL-IMP
        //                  :IND-POG-VAL-IMP
        //                 ,:POG-VAL-DT-DB
        //                  :IND-POG-VAL-DT
        //                 ,:POG-VAL-TS
        //                  :IND-POG-VAL-TS
        //                 ,:POG-VAL-TXT-VCHAR
        //                  :IND-POG-VAL-TXT
        //                 ,:POG-VAL-FL
        //                  :IND-POG-VAL-FL
        //                 ,:POG-VAL-NUM
        //                  :IND-POG-VAL-NUM
        //                 ,:POG-VAL-PC
        //                  :IND-POG-VAL-PC
        //                 ,:POG-DS-RIGA
        //                 ,:POG-DS-OPER-SQL
        //                 ,:POG-DS-VER
        //                 ,:POG-DS-TS-INI-CPTZ
        //                 ,:POG-DS-TS-END-CPTZ
        //                 ,:POG-DS-UTENTE
        //                 ,:POG-DS-STATO-ELAB
        //             FROM PARAM_OGG
        //             WHERE COD_COMP_ANIA  = :IDSV0003-CODICE-COMPAGNIA-ANIA
        //               AND DT_INI_EFF    <= :WS-DATA-INIZIO-EFFETTO-DB
        //               AND DT_END_EFF     > :WS-DATA-INIZIO-EFFETTO-DB
        //               AND DS_TS_INI_CPTZ <= :WS-TS-COMPETENZA
        //               AND DS_TS_END_CPTZ >  :WS-TS-COMPETENZA
        //               AND ID_OGG         = :LDBV1131-ID-OGG
        //               AND TP_OGG         = :LDBV1131-TP-OGG
        //               AND COD_PARAM      = :LDBV1131-COD-PARAM
        //           END-EXEC.
        paramOggDao.selectRec4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-POG-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO POG-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndParamOgg().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-ID-MOVI-CHIU-NULL
            paramOgg.getPogIdMoviChiu().setPogIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PogIdMoviChiu.Len.POG_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-POG-COD-PARAM = -1
        //              MOVE HIGH-VALUES TO POG-COD-PARAM
        //           END-IF
        if (ws.getIndParamOgg().getCodParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-COD-PARAM
            paramOgg.setPogCodParam(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamOgg.Len.POG_COD_PARAM));
        }
        // COB_CODE: IF IND-POG-TP-PARAM = -1
        //              MOVE HIGH-VALUES TO POG-TP-PARAM-NULL
        //           END-IF
        if (ws.getIndParamOgg().getTpParam() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-TP-PARAM-NULL
            paramOgg.setPogTpParam(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POG-TP-D = -1
        //              MOVE HIGH-VALUES TO POG-TP-D-NULL
        //           END-IF
        if (ws.getIndParamOgg().getTpD() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-TP-D-NULL
            paramOgg.setPogTpD(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamOgg.Len.POG_TP_D));
        }
        // COB_CODE: IF IND-POG-VAL-IMP = -1
        //              MOVE HIGH-VALUES TO POG-VAL-IMP-NULL
        //           END-IF
        if (ws.getIndParamOgg().getValImp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-VAL-IMP-NULL
            paramOgg.getPogValImp().setPogValImpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PogValImp.Len.POG_VAL_IMP_NULL));
        }
        // COB_CODE: IF IND-POG-VAL-DT = -1
        //              MOVE HIGH-VALUES TO POG-VAL-DT-NULL
        //           END-IF
        if (ws.getIndParamOgg().getValDt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-VAL-DT-NULL
            paramOgg.getPogValDt().setPogValDtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PogValDt.Len.POG_VAL_DT_NULL));
        }
        // COB_CODE: IF IND-POG-VAL-TS = -1
        //              MOVE HIGH-VALUES TO POG-VAL-TS-NULL
        //           END-IF
        if (ws.getIndParamOgg().getValTs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-VAL-TS-NULL
            paramOgg.getPogValTs().setPogValTsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PogValTs.Len.POG_VAL_TS_NULL));
        }
        // COB_CODE: IF IND-POG-VAL-TXT = -1
        //              MOVE HIGH-VALUES TO POG-VAL-TXT
        //           END-IF
        if (ws.getIndParamOgg().getValTxt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-VAL-TXT
            paramOgg.setPogValTxt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ParamOgg.Len.POG_VAL_TXT));
        }
        // COB_CODE: IF IND-POG-VAL-FL = -1
        //              MOVE HIGH-VALUES TO POG-VAL-FL-NULL
        //           END-IF
        if (ws.getIndParamOgg().getValFl() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-VAL-FL-NULL
            paramOgg.setPogValFl(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-POG-VAL-NUM = -1
        //              MOVE HIGH-VALUES TO POG-VAL-NUM-NULL
        //           END-IF
        if (ws.getIndParamOgg().getValNum() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-VAL-NUM-NULL
            paramOgg.getPogValNum().setPogValNumNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PogValNum.Len.POG_VAL_NUM_NULL));
        }
        // COB_CODE: IF IND-POG-VAL-PC = -1
        //              MOVE HIGH-VALUES TO POG-VAL-PC-NULL
        //           END-IF.
        if (ws.getIndParamOgg().getValPc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO POG-VAL-PC-NULL
            paramOgg.getPogValPc().setPogValPcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PogValPc.Len.POG_VAL_PC_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE POG-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getParamOggDb().getDtIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POG-DT-INI-EFF
        paramOgg.setPogDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE POG-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getParamOggDb().getDtEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO POG-DT-END-EFF
        paramOgg.setPogDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-POG-VAL-DT = -1
        //               MOVE  HIGH-VALUES   TO POG-VAL-DT-NULL
        //           ELSE
        //               MOVE WS-DATE-N      TO POG-VAL-DT
        //           END-IF.
        if (ws.getIndParamOgg().getValDt() == -1) {
            // COB_CODE: MOVE  HIGH-VALUES   TO POG-VAL-DT-NULL
            paramOgg.getPogValDt().setPogValDtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, PogValDt.Len.POG_VAL_DT_NULL));
        }
        else {
            // COB_CODE: MOVE POG-VAL-DT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getParamOggDb().getValDtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO POG-VAL-DT
            paramOgg.getPogValDt().setPogValDt(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return paramOgg.getPogCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.paramOgg.setPogCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodParam() {
        return paramOgg.getPogCodParam();
    }

    @Override
    public void setCodParam(String codParam) {
        this.paramOgg.setPogCodParam(codParam);
    }

    @Override
    public String getCodParamObj() {
        if (ws.getIndParamOgg().getCodParam() >= 0) {
            return getCodParam();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodParamObj(String codParamObj) {
        if (codParamObj != null) {
            setCodParam(codParamObj);
            ws.getIndParamOgg().setCodParam(((short)0));
        }
        else {
            ws.getIndParamOgg().setCodParam(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return paramOgg.getPogDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.paramOgg.setPogDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return paramOgg.getPogDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.paramOgg.setPogDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return paramOgg.getPogDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.paramOgg.setPogDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return paramOgg.getPogDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.paramOgg.setPogDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return paramOgg.getPogDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.paramOgg.setPogDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return paramOgg.getPogDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.paramOgg.setPogDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getParamOggDb().getDtEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getParamOggDb().setDtEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getParamOggDb().getDtIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getParamOggDb().setDtIniEffDb(dtIniEffDb);
    }

    @Override
    public int getIdMoviChiu() {
        return paramOgg.getPogIdMoviChiu().getPogIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.paramOgg.getPogIdMoviChiu().setPogIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndParamOgg().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndParamOgg().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndParamOgg().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return paramOgg.getPogIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.paramOgg.setPogIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdParamOgg() {
        return paramOgg.getPogIdParamOgg();
    }

    @Override
    public void setIdParamOgg(int idParamOgg) {
        this.paramOgg.setPogIdParamOgg(idParamOgg);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getLdbv1131CodParam() {
        return ws.getAreaLdbv1131().getCodParam();
    }

    @Override
    public void setLdbv1131CodParam(String ldbv1131CodParam) {
        this.ws.getAreaLdbv1131().setCodParam(ldbv1131CodParam);
    }

    @Override
    public int getLdbv1131IdOgg() {
        return ws.getAreaLdbv1131().getIdOgg();
    }

    @Override
    public void setLdbv1131IdOgg(int ldbv1131IdOgg) {
        this.ws.getAreaLdbv1131().setIdOgg(ldbv1131IdOgg);
    }

    @Override
    public String getLdbv1131TpOgg() {
        return ws.getAreaLdbv1131().getTpOgg();
    }

    @Override
    public void setLdbv1131TpOgg(String ldbv1131TpOgg) {
        this.ws.getAreaLdbv1131().setTpOgg(ldbv1131TpOgg);
    }

    @Override
    public long getPogDsRiga() {
        return paramOgg.getPogDsRiga();
    }

    @Override
    public void setPogDsRiga(long pogDsRiga) {
        this.paramOgg.setPogDsRiga(pogDsRiga);
    }

    @Override
    public int getPogIdOgg() {
        return paramOgg.getPogIdOgg();
    }

    @Override
    public void setPogIdOgg(int pogIdOgg) {
        this.paramOgg.setPogIdOgg(pogIdOgg);
    }

    @Override
    public String getPogTpOgg() {
        return paramOgg.getPogTpOgg();
    }

    @Override
    public void setPogTpOgg(String pogTpOgg) {
        this.paramOgg.setPogTpOgg(pogTpOgg);
    }

    @Override
    public String getTpD() {
        return paramOgg.getPogTpD();
    }

    @Override
    public void setTpD(String tpD) {
        this.paramOgg.setPogTpD(tpD);
    }

    @Override
    public String getTpDObj() {
        if (ws.getIndParamOgg().getTpD() >= 0) {
            return getTpD();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDObj(String tpDObj) {
        if (tpDObj != null) {
            setTpD(tpDObj);
            ws.getIndParamOgg().setTpD(((short)0));
        }
        else {
            ws.getIndParamOgg().setTpD(((short)-1));
        }
    }

    @Override
    public char getTpParam() {
        return paramOgg.getPogTpParam();
    }

    @Override
    public void setTpParam(char tpParam) {
        this.paramOgg.setPogTpParam(tpParam);
    }

    @Override
    public Character getTpParamObj() {
        if (ws.getIndParamOgg().getTpParam() >= 0) {
            return ((Character)getTpParam());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpParamObj(Character tpParamObj) {
        if (tpParamObj != null) {
            setTpParam(((char)tpParamObj));
            ws.getIndParamOgg().setTpParam(((short)0));
        }
        else {
            ws.getIndParamOgg().setTpParam(((short)-1));
        }
    }

    @Override
    public String getValDtDb() {
        return ws.getParamOggDb().getValDtDb();
    }

    @Override
    public void setValDtDb(String valDtDb) {
        this.ws.getParamOggDb().setValDtDb(valDtDb);
    }

    @Override
    public String getValDtDbObj() {
        if (ws.getIndParamOgg().getValDt() >= 0) {
            return getValDtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValDtDbObj(String valDtDbObj) {
        if (valDtDbObj != null) {
            setValDtDb(valDtDbObj);
            ws.getIndParamOgg().setValDt(((short)0));
        }
        else {
            ws.getIndParamOgg().setValDt(((short)-1));
        }
    }

    @Override
    public char getValFl() {
        return paramOgg.getPogValFl();
    }

    @Override
    public void setValFl(char valFl) {
        this.paramOgg.setPogValFl(valFl);
    }

    @Override
    public Character getValFlObj() {
        if (ws.getIndParamOgg().getValFl() >= 0) {
            return ((Character)getValFl());
        }
        else {
            return null;
        }
    }

    @Override
    public void setValFlObj(Character valFlObj) {
        if (valFlObj != null) {
            setValFl(((char)valFlObj));
            ws.getIndParamOgg().setValFl(((short)0));
        }
        else {
            ws.getIndParamOgg().setValFl(((short)-1));
        }
    }

    @Override
    public AfDecimal getValImp() {
        return paramOgg.getPogValImp().getPogValImp();
    }

    @Override
    public void setValImp(AfDecimal valImp) {
        this.paramOgg.getPogValImp().setPogValImp(valImp.copy());
    }

    @Override
    public AfDecimal getValImpObj() {
        if (ws.getIndParamOgg().getValImp() >= 0) {
            return getValImp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValImpObj(AfDecimal valImpObj) {
        if (valImpObj != null) {
            setValImp(new AfDecimal(valImpObj, 15, 3));
            ws.getIndParamOgg().setValImp(((short)0));
        }
        else {
            ws.getIndParamOgg().setValImp(((short)-1));
        }
    }

    @Override
    public long getValNum() {
        return paramOgg.getPogValNum().getPogValNum();
    }

    @Override
    public void setValNum(long valNum) {
        this.paramOgg.getPogValNum().setPogValNum(valNum);
    }

    @Override
    public Long getValNumObj() {
        if (ws.getIndParamOgg().getValNum() >= 0) {
            return ((Long)getValNum());
        }
        else {
            return null;
        }
    }

    @Override
    public void setValNumObj(Long valNumObj) {
        if (valNumObj != null) {
            setValNum(((long)valNumObj));
            ws.getIndParamOgg().setValNum(((short)0));
        }
        else {
            ws.getIndParamOgg().setValNum(((short)-1));
        }
    }

    @Override
    public AfDecimal getValPc() {
        return paramOgg.getPogValPc().getPogValPc();
    }

    @Override
    public void setValPc(AfDecimal valPc) {
        this.paramOgg.getPogValPc().setPogValPc(valPc.copy());
    }

    @Override
    public AfDecimal getValPcObj() {
        if (ws.getIndParamOgg().getValPc() >= 0) {
            return getValPc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValPcObj(AfDecimal valPcObj) {
        if (valPcObj != null) {
            setValPc(new AfDecimal(valPcObj, 14, 9));
            ws.getIndParamOgg().setValPc(((short)0));
        }
        else {
            ws.getIndParamOgg().setValPc(((short)-1));
        }
    }

    @Override
    public AfDecimal getValTs() {
        return paramOgg.getPogValTs().getPogValTs();
    }

    @Override
    public void setValTs(AfDecimal valTs) {
        this.paramOgg.getPogValTs().setPogValTs(valTs.copy());
    }

    @Override
    public AfDecimal getValTsObj() {
        if (ws.getIndParamOgg().getValTs() >= 0) {
            return getValTs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValTsObj(AfDecimal valTsObj) {
        if (valTsObj != null) {
            setValTs(new AfDecimal(valTsObj, 14, 9));
            ws.getIndParamOgg().setValTs(((short)0));
        }
        else {
            ws.getIndParamOgg().setValTs(((short)-1));
        }
    }

    @Override
    public String getValTxtVchar() {
        return paramOgg.getPogValTxtVcharFormatted();
    }

    @Override
    public void setValTxtVchar(String valTxtVchar) {
        this.paramOgg.setPogValTxtVcharFormatted(valTxtVchar);
    }

    @Override
    public String getValTxtVcharObj() {
        if (ws.getIndParamOgg().getValTxt() >= 0) {
            return getValTxtVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValTxtVcharObj(String valTxtVcharObj) {
        if (valTxtVcharObj != null) {
            setValTxtVchar(valTxtVcharObj);
            ws.getIndParamOgg().setValTxt(((short)0));
        }
        else {
            ws.getIndParamOgg().setValTxt(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
