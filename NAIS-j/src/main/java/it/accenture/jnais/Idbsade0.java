package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.AdesDao;
import it.accenture.jnais.commons.data.to.IAdes;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.Ades;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsade0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.AdeCumCnbtCap;
import it.accenture.jnais.ws.redefines.AdeDtDecor;
import it.accenture.jnais.ws.redefines.AdeDtDecorPrestBan;
import it.accenture.jnais.ws.redefines.AdeDtEffVarzStatT;
import it.accenture.jnais.ws.redefines.AdeDtNovaRgmFisc;
import it.accenture.jnais.ws.redefines.AdeDtPresc;
import it.accenture.jnais.ws.redefines.AdeDtScad;
import it.accenture.jnais.ws.redefines.AdeDtUltConsCnbt;
import it.accenture.jnais.ws.redefines.AdeDtVarzTpIas;
import it.accenture.jnais.ws.redefines.AdeDurAa;
import it.accenture.jnais.ws.redefines.AdeDurGg;
import it.accenture.jnais.ws.redefines.AdeDurMm;
import it.accenture.jnais.ws.redefines.AdeEtaAScad;
import it.accenture.jnais.ws.redefines.AdeIdMoviChiu;
import it.accenture.jnais.ws.redefines.AdeImpAder;
import it.accenture.jnais.ws.redefines.AdeImpAz;
import it.accenture.jnais.ws.redefines.AdeImpbVisDaRec;
import it.accenture.jnais.ws.redefines.AdeImpGarCnbt;
import it.accenture.jnais.ws.redefines.AdeImpRecRitAcc;
import it.accenture.jnais.ws.redefines.AdeImpRecRitVis;
import it.accenture.jnais.ws.redefines.AdeImpTfr;
import it.accenture.jnais.ws.redefines.AdeImpVolo;
import it.accenture.jnais.ws.redefines.AdeNumRatPian;
import it.accenture.jnais.ws.redefines.AdePcAder;
import it.accenture.jnais.ws.redefines.AdePcAz;
import it.accenture.jnais.ws.redefines.AdePcTfr;
import it.accenture.jnais.ws.redefines.AdePcVolo;
import it.accenture.jnais.ws.redefines.AdePreLrdInd;
import it.accenture.jnais.ws.redefines.AdePreNetInd;
import it.accenture.jnais.ws.redefines.AdePrstzIniInd;
import it.accenture.jnais.ws.redefines.AdeRatLrdInd;

/**Original name: IDBSADE0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 DIC 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsade0 extends Program implements IAdes {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private AdesDao adesDao = new AdesDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsade0Data ws = new Idbsade0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: ADES
    private Ades ades;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSADE0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ades ades) {
        this.idsv0003 = idsv0003;
        this.ades = ades;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsade0 getInstance() {
        return ((Idbsade0)Programs.getInstance(Idbsade0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSADE0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSADE0");
        // COB_CODE: MOVE 'ADES' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("ADES");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     DS_RIGA = :ADE-DS-RIGA
        //           END-EXEC.
        adesDao.selectByAdeDsRiga(ades.getAdeDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO ADES
            //                  (
            //                     ID_ADES
            //                    ,ID_POLI
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,IB_PREV
            //                    ,IB_OGG
            //                    ,COD_COMP_ANIA
            //                    ,DT_DECOR
            //                    ,DT_SCAD
            //                    ,ETA_A_SCAD
            //                    ,DUR_AA
            //                    ,DUR_MM
            //                    ,DUR_GG
            //                    ,TP_RGM_FISC
            //                    ,TP_RIAT
            //                    ,TP_MOD_PAG_TIT
            //                    ,TP_IAS
            //                    ,DT_VARZ_TP_IAS
            //                    ,PRE_NET_IND
            //                    ,PRE_LRD_IND
            //                    ,RAT_LRD_IND
            //                    ,PRSTZ_INI_IND
            //                    ,FL_COINC_ASSTO
            //                    ,IB_DFLT
            //                    ,MOD_CALC
            //                    ,TP_FNT_CNBTVA
            //                    ,IMP_AZ
            //                    ,IMP_ADER
            //                    ,IMP_TFR
            //                    ,IMP_VOLO
            //                    ,PC_AZ
            //                    ,PC_ADER
            //                    ,PC_TFR
            //                    ,PC_VOLO
            //                    ,DT_NOVA_RGM_FISC
            //                    ,FL_ATTIV
            //                    ,IMP_REC_RIT_VIS
            //                    ,IMP_REC_RIT_ACC
            //                    ,FL_VARZ_STAT_TBGC
            //                    ,FL_PROVZA_MIGRAZ
            //                    ,IMPB_VIS_DA_REC
            //                    ,DT_DECOR_PREST_BAN
            //                    ,DT_EFF_VARZ_STAT_T
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,CUM_CNBT_CAP
            //                    ,IMP_GAR_CNBT
            //                    ,DT_ULT_CONS_CNBT
            //                    ,IDEN_ISC_FND
            //                    ,NUM_RAT_PIAN
            //                    ,DT_PRESC
            //                    ,CONCS_PREST
            //                  )
            //              VALUES
            //                  (
            //                    :ADE-ID-ADES
            //                    ,:ADE-ID-POLI
            //                    ,:ADE-ID-MOVI-CRZ
            //                    ,:ADE-ID-MOVI-CHIU
            //                     :IND-ADE-ID-MOVI-CHIU
            //                    ,:ADE-DT-INI-EFF-DB
            //                    ,:ADE-DT-END-EFF-DB
            //                    ,:ADE-IB-PREV
            //                     :IND-ADE-IB-PREV
            //                    ,:ADE-IB-OGG
            //                     :IND-ADE-IB-OGG
            //                    ,:ADE-COD-COMP-ANIA
            //                    ,:ADE-DT-DECOR-DB
            //                     :IND-ADE-DT-DECOR
            //                    ,:ADE-DT-SCAD-DB
            //                     :IND-ADE-DT-SCAD
            //                    ,:ADE-ETA-A-SCAD
            //                     :IND-ADE-ETA-A-SCAD
            //                    ,:ADE-DUR-AA
            //                     :IND-ADE-DUR-AA
            //                    ,:ADE-DUR-MM
            //                     :IND-ADE-DUR-MM
            //                    ,:ADE-DUR-GG
            //                     :IND-ADE-DUR-GG
            //                    ,:ADE-TP-RGM-FISC
            //                    ,:ADE-TP-RIAT
            //                     :IND-ADE-TP-RIAT
            //                    ,:ADE-TP-MOD-PAG-TIT
            //                    ,:ADE-TP-IAS
            //                     :IND-ADE-TP-IAS
            //                    ,:ADE-DT-VARZ-TP-IAS-DB
            //                     :IND-ADE-DT-VARZ-TP-IAS
            //                    ,:ADE-PRE-NET-IND
            //                     :IND-ADE-PRE-NET-IND
            //                    ,:ADE-PRE-LRD-IND
            //                     :IND-ADE-PRE-LRD-IND
            //                    ,:ADE-RAT-LRD-IND
            //                     :IND-ADE-RAT-LRD-IND
            //                    ,:ADE-PRSTZ-INI-IND
            //                     :IND-ADE-PRSTZ-INI-IND
            //                    ,:ADE-FL-COINC-ASSTO
            //                     :IND-ADE-FL-COINC-ASSTO
            //                    ,:ADE-IB-DFLT
            //                     :IND-ADE-IB-DFLT
            //                    ,:ADE-MOD-CALC
            //                     :IND-ADE-MOD-CALC
            //                    ,:ADE-TP-FNT-CNBTVA
            //                     :IND-ADE-TP-FNT-CNBTVA
            //                    ,:ADE-IMP-AZ
            //                     :IND-ADE-IMP-AZ
            //                    ,:ADE-IMP-ADER
            //                     :IND-ADE-IMP-ADER
            //                    ,:ADE-IMP-TFR
            //                     :IND-ADE-IMP-TFR
            //                    ,:ADE-IMP-VOLO
            //                     :IND-ADE-IMP-VOLO
            //                    ,:ADE-PC-AZ
            //                     :IND-ADE-PC-AZ
            //                    ,:ADE-PC-ADER
            //                     :IND-ADE-PC-ADER
            //                    ,:ADE-PC-TFR
            //                     :IND-ADE-PC-TFR
            //                    ,:ADE-PC-VOLO
            //                     :IND-ADE-PC-VOLO
            //                    ,:ADE-DT-NOVA-RGM-FISC-DB
            //                     :IND-ADE-DT-NOVA-RGM-FISC
            //                    ,:ADE-FL-ATTIV
            //                     :IND-ADE-FL-ATTIV
            //                    ,:ADE-IMP-REC-RIT-VIS
            //                     :IND-ADE-IMP-REC-RIT-VIS
            //                    ,:ADE-IMP-REC-RIT-ACC
            //                     :IND-ADE-IMP-REC-RIT-ACC
            //                    ,:ADE-FL-VARZ-STAT-TBGC
            //                     :IND-ADE-FL-VARZ-STAT-TBGC
            //                    ,:ADE-FL-PROVZA-MIGRAZ
            //                     :IND-ADE-FL-PROVZA-MIGRAZ
            //                    ,:ADE-IMPB-VIS-DA-REC
            //                     :IND-ADE-IMPB-VIS-DA-REC
            //                    ,:ADE-DT-DECOR-PREST-BAN-DB
            //                     :IND-ADE-DT-DECOR-PREST-BAN
            //                    ,:ADE-DT-EFF-VARZ-STAT-T-DB
            //                     :IND-ADE-DT-EFF-VARZ-STAT-T
            //                    ,:ADE-DS-RIGA
            //                    ,:ADE-DS-OPER-SQL
            //                    ,:ADE-DS-VER
            //                    ,:ADE-DS-TS-INI-CPTZ
            //                    ,:ADE-DS-TS-END-CPTZ
            //                    ,:ADE-DS-UTENTE
            //                    ,:ADE-DS-STATO-ELAB
            //                    ,:ADE-CUM-CNBT-CAP
            //                     :IND-ADE-CUM-CNBT-CAP
            //                    ,:ADE-IMP-GAR-CNBT
            //                     :IND-ADE-IMP-GAR-CNBT
            //                    ,:ADE-DT-ULT-CONS-CNBT-DB
            //                     :IND-ADE-DT-ULT-CONS-CNBT
            //                    ,:ADE-IDEN-ISC-FND
            //                     :IND-ADE-IDEN-ISC-FND
            //                    ,:ADE-NUM-RAT-PIAN
            //                     :IND-ADE-NUM-RAT-PIAN
            //                    ,:ADE-DT-PRESC-DB
            //                     :IND-ADE-DT-PRESC
            //                    ,:ADE-CONCS-PREST
            //                     :IND-ADE-CONCS-PREST
            //                  )
            //           END-EXEC
            adesDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE ADES SET
        //                   ID_ADES                =
        //                :ADE-ID-ADES
        //                  ,ID_POLI                =
        //                :ADE-ID-POLI
        //                  ,ID_MOVI_CRZ            =
        //                :ADE-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :ADE-ID-MOVI-CHIU
        //                                       :IND-ADE-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :ADE-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :ADE-DT-END-EFF-DB
        //                  ,IB_PREV                =
        //                :ADE-IB-PREV
        //                                       :IND-ADE-IB-PREV
        //                  ,IB_OGG                 =
        //                :ADE-IB-OGG
        //                                       :IND-ADE-IB-OGG
        //                  ,COD_COMP_ANIA          =
        //                :ADE-COD-COMP-ANIA
        //                  ,DT_DECOR               =
        //           :ADE-DT-DECOR-DB
        //                                       :IND-ADE-DT-DECOR
        //                  ,DT_SCAD                =
        //           :ADE-DT-SCAD-DB
        //                                       :IND-ADE-DT-SCAD
        //                  ,ETA_A_SCAD             =
        //                :ADE-ETA-A-SCAD
        //                                       :IND-ADE-ETA-A-SCAD
        //                  ,DUR_AA                 =
        //                :ADE-DUR-AA
        //                                       :IND-ADE-DUR-AA
        //                  ,DUR_MM                 =
        //                :ADE-DUR-MM
        //                                       :IND-ADE-DUR-MM
        //                  ,DUR_GG                 =
        //                :ADE-DUR-GG
        //                                       :IND-ADE-DUR-GG
        //                  ,TP_RGM_FISC            =
        //                :ADE-TP-RGM-FISC
        //                  ,TP_RIAT                =
        //                :ADE-TP-RIAT
        //                                       :IND-ADE-TP-RIAT
        //                  ,TP_MOD_PAG_TIT         =
        //                :ADE-TP-MOD-PAG-TIT
        //                  ,TP_IAS                 =
        //                :ADE-TP-IAS
        //                                       :IND-ADE-TP-IAS
        //                  ,DT_VARZ_TP_IAS         =
        //           :ADE-DT-VARZ-TP-IAS-DB
        //                                       :IND-ADE-DT-VARZ-TP-IAS
        //                  ,PRE_NET_IND            =
        //                :ADE-PRE-NET-IND
        //                                       :IND-ADE-PRE-NET-IND
        //                  ,PRE_LRD_IND            =
        //                :ADE-PRE-LRD-IND
        //                                       :IND-ADE-PRE-LRD-IND
        //                  ,RAT_LRD_IND            =
        //                :ADE-RAT-LRD-IND
        //                                       :IND-ADE-RAT-LRD-IND
        //                  ,PRSTZ_INI_IND          =
        //                :ADE-PRSTZ-INI-IND
        //                                       :IND-ADE-PRSTZ-INI-IND
        //                  ,FL_COINC_ASSTO         =
        //                :ADE-FL-COINC-ASSTO
        //                                       :IND-ADE-FL-COINC-ASSTO
        //                  ,IB_DFLT                =
        //                :ADE-IB-DFLT
        //                                       :IND-ADE-IB-DFLT
        //                  ,MOD_CALC               =
        //                :ADE-MOD-CALC
        //                                       :IND-ADE-MOD-CALC
        //                  ,TP_FNT_CNBTVA          =
        //                :ADE-TP-FNT-CNBTVA
        //                                       :IND-ADE-TP-FNT-CNBTVA
        //                  ,IMP_AZ                 =
        //                :ADE-IMP-AZ
        //                                       :IND-ADE-IMP-AZ
        //                  ,IMP_ADER               =
        //                :ADE-IMP-ADER
        //                                       :IND-ADE-IMP-ADER
        //                  ,IMP_TFR                =
        //                :ADE-IMP-TFR
        //                                       :IND-ADE-IMP-TFR
        //                  ,IMP_VOLO               =
        //                :ADE-IMP-VOLO
        //                                       :IND-ADE-IMP-VOLO
        //                  ,PC_AZ                  =
        //                :ADE-PC-AZ
        //                                       :IND-ADE-PC-AZ
        //                  ,PC_ADER                =
        //                :ADE-PC-ADER
        //                                       :IND-ADE-PC-ADER
        //                  ,PC_TFR                 =
        //                :ADE-PC-TFR
        //                                       :IND-ADE-PC-TFR
        //                  ,PC_VOLO                =
        //                :ADE-PC-VOLO
        //                                       :IND-ADE-PC-VOLO
        //                  ,DT_NOVA_RGM_FISC       =
        //           :ADE-DT-NOVA-RGM-FISC-DB
        //                                       :IND-ADE-DT-NOVA-RGM-FISC
        //                  ,FL_ATTIV               =
        //                :ADE-FL-ATTIV
        //                                       :IND-ADE-FL-ATTIV
        //                  ,IMP_REC_RIT_VIS        =
        //                :ADE-IMP-REC-RIT-VIS
        //                                       :IND-ADE-IMP-REC-RIT-VIS
        //                  ,IMP_REC_RIT_ACC        =
        //                :ADE-IMP-REC-RIT-ACC
        //                                       :IND-ADE-IMP-REC-RIT-ACC
        //                  ,FL_VARZ_STAT_TBGC      =
        //                :ADE-FL-VARZ-STAT-TBGC
        //                                       :IND-ADE-FL-VARZ-STAT-TBGC
        //                  ,FL_PROVZA_MIGRAZ       =
        //                :ADE-FL-PROVZA-MIGRAZ
        //                                       :IND-ADE-FL-PROVZA-MIGRAZ
        //                  ,IMPB_VIS_DA_REC        =
        //                :ADE-IMPB-VIS-DA-REC
        //                                       :IND-ADE-IMPB-VIS-DA-REC
        //                  ,DT_DECOR_PREST_BAN     =
        //           :ADE-DT-DECOR-PREST-BAN-DB
        //                                       :IND-ADE-DT-DECOR-PREST-BAN
        //                  ,DT_EFF_VARZ_STAT_T     =
        //           :ADE-DT-EFF-VARZ-STAT-T-DB
        //                                       :IND-ADE-DT-EFF-VARZ-STAT-T
        //                  ,DS_RIGA                =
        //                :ADE-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :ADE-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :ADE-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :ADE-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :ADE-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :ADE-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :ADE-DS-STATO-ELAB
        //                  ,CUM_CNBT_CAP           =
        //                :ADE-CUM-CNBT-CAP
        //                                       :IND-ADE-CUM-CNBT-CAP
        //                  ,IMP_GAR_CNBT           =
        //                :ADE-IMP-GAR-CNBT
        //                                       :IND-ADE-IMP-GAR-CNBT
        //                  ,DT_ULT_CONS_CNBT       =
        //           :ADE-DT-ULT-CONS-CNBT-DB
        //                                       :IND-ADE-DT-ULT-CONS-CNBT
        //                  ,IDEN_ISC_FND           =
        //                :ADE-IDEN-ISC-FND
        //                                       :IND-ADE-IDEN-ISC-FND
        //                  ,NUM_RAT_PIAN           =
        //                :ADE-NUM-RAT-PIAN
        //                                       :IND-ADE-NUM-RAT-PIAN
        //                  ,DT_PRESC               =
        //           :ADE-DT-PRESC-DB
        //                                       :IND-ADE-DT-PRESC
        //                  ,CONCS_PREST            =
        //                :ADE-CONCS-PREST
        //                                       :IND-ADE-CONCS-PREST
        //                WHERE     DS_RIGA = :ADE-DS-RIGA
        //           END-EXEC.
        adesDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM ADES
        //                WHERE     DS_RIGA = :ADE-DS-RIGA
        //           END-EXEC.
        adesDao.deleteByAdeDsRiga(ades.getAdeDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-ADE CURSOR FOR
        //              SELECT
        //                     ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,IB_PREV
        //                    ,IB_OGG
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,ETA_A_SCAD
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,TP_RGM_FISC
        //                    ,TP_RIAT
        //                    ,TP_MOD_PAG_TIT
        //                    ,TP_IAS
        //                    ,DT_VARZ_TP_IAS
        //                    ,PRE_NET_IND
        //                    ,PRE_LRD_IND
        //                    ,RAT_LRD_IND
        //                    ,PRSTZ_INI_IND
        //                    ,FL_COINC_ASSTO
        //                    ,IB_DFLT
        //                    ,MOD_CALC
        //                    ,TP_FNT_CNBTVA
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,PC_AZ
        //                    ,PC_ADER
        //                    ,PC_TFR
        //                    ,PC_VOLO
        //                    ,DT_NOVA_RGM_FISC
        //                    ,FL_ATTIV
        //                    ,IMP_REC_RIT_VIS
        //                    ,IMP_REC_RIT_ACC
        //                    ,FL_VARZ_STAT_TBGC
        //                    ,FL_PROVZA_MIGRAZ
        //                    ,IMPB_VIS_DA_REC
        //                    ,DT_DECOR_PREST_BAN
        //                    ,DT_EFF_VARZ_STAT_T
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,CUM_CNBT_CAP
        //                    ,IMP_GAR_CNBT
        //                    ,DT_ULT_CONS_CNBT
        //                    ,IDEN_ISC_FND
        //                    ,NUM_RAT_PIAN
        //                    ,DT_PRESC
        //                    ,CONCS_PREST
        //              FROM ADES
        //              WHERE     ID_ADES = :ADE-ID-ADES
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     ID_ADES = :ADE-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        adesDao.selectRec(ades.getAdeIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE ADES SET
        //                   ID_ADES                =
        //                :ADE-ID-ADES
        //                  ,ID_POLI                =
        //                :ADE-ID-POLI
        //                  ,ID_MOVI_CRZ            =
        //                :ADE-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :ADE-ID-MOVI-CHIU
        //                                       :IND-ADE-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :ADE-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :ADE-DT-END-EFF-DB
        //                  ,IB_PREV                =
        //                :ADE-IB-PREV
        //                                       :IND-ADE-IB-PREV
        //                  ,IB_OGG                 =
        //                :ADE-IB-OGG
        //                                       :IND-ADE-IB-OGG
        //                  ,COD_COMP_ANIA          =
        //                :ADE-COD-COMP-ANIA
        //                  ,DT_DECOR               =
        //           :ADE-DT-DECOR-DB
        //                                       :IND-ADE-DT-DECOR
        //                  ,DT_SCAD                =
        //           :ADE-DT-SCAD-DB
        //                                       :IND-ADE-DT-SCAD
        //                  ,ETA_A_SCAD             =
        //                :ADE-ETA-A-SCAD
        //                                       :IND-ADE-ETA-A-SCAD
        //                  ,DUR_AA                 =
        //                :ADE-DUR-AA
        //                                       :IND-ADE-DUR-AA
        //                  ,DUR_MM                 =
        //                :ADE-DUR-MM
        //                                       :IND-ADE-DUR-MM
        //                  ,DUR_GG                 =
        //                :ADE-DUR-GG
        //                                       :IND-ADE-DUR-GG
        //                  ,TP_RGM_FISC            =
        //                :ADE-TP-RGM-FISC
        //                  ,TP_RIAT                =
        //                :ADE-TP-RIAT
        //                                       :IND-ADE-TP-RIAT
        //                  ,TP_MOD_PAG_TIT         =
        //                :ADE-TP-MOD-PAG-TIT
        //                  ,TP_IAS                 =
        //                :ADE-TP-IAS
        //                                       :IND-ADE-TP-IAS
        //                  ,DT_VARZ_TP_IAS         =
        //           :ADE-DT-VARZ-TP-IAS-DB
        //                                       :IND-ADE-DT-VARZ-TP-IAS
        //                  ,PRE_NET_IND            =
        //                :ADE-PRE-NET-IND
        //                                       :IND-ADE-PRE-NET-IND
        //                  ,PRE_LRD_IND            =
        //                :ADE-PRE-LRD-IND
        //                                       :IND-ADE-PRE-LRD-IND
        //                  ,RAT_LRD_IND            =
        //                :ADE-RAT-LRD-IND
        //                                       :IND-ADE-RAT-LRD-IND
        //                  ,PRSTZ_INI_IND          =
        //                :ADE-PRSTZ-INI-IND
        //                                       :IND-ADE-PRSTZ-INI-IND
        //                  ,FL_COINC_ASSTO         =
        //                :ADE-FL-COINC-ASSTO
        //                                       :IND-ADE-FL-COINC-ASSTO
        //                  ,IB_DFLT                =
        //                :ADE-IB-DFLT
        //                                       :IND-ADE-IB-DFLT
        //                  ,MOD_CALC               =
        //                :ADE-MOD-CALC
        //                                       :IND-ADE-MOD-CALC
        //                  ,TP_FNT_CNBTVA          =
        //                :ADE-TP-FNT-CNBTVA
        //                                       :IND-ADE-TP-FNT-CNBTVA
        //                  ,IMP_AZ                 =
        //                :ADE-IMP-AZ
        //                                       :IND-ADE-IMP-AZ
        //                  ,IMP_ADER               =
        //                :ADE-IMP-ADER
        //                                       :IND-ADE-IMP-ADER
        //                  ,IMP_TFR                =
        //                :ADE-IMP-TFR
        //                                       :IND-ADE-IMP-TFR
        //                  ,IMP_VOLO               =
        //                :ADE-IMP-VOLO
        //                                       :IND-ADE-IMP-VOLO
        //                  ,PC_AZ                  =
        //                :ADE-PC-AZ
        //                                       :IND-ADE-PC-AZ
        //                  ,PC_ADER                =
        //                :ADE-PC-ADER
        //                                       :IND-ADE-PC-ADER
        //                  ,PC_TFR                 =
        //                :ADE-PC-TFR
        //                                       :IND-ADE-PC-TFR
        //                  ,PC_VOLO                =
        //                :ADE-PC-VOLO
        //                                       :IND-ADE-PC-VOLO
        //                  ,DT_NOVA_RGM_FISC       =
        //           :ADE-DT-NOVA-RGM-FISC-DB
        //                                       :IND-ADE-DT-NOVA-RGM-FISC
        //                  ,FL_ATTIV               =
        //                :ADE-FL-ATTIV
        //                                       :IND-ADE-FL-ATTIV
        //                  ,IMP_REC_RIT_VIS        =
        //                :ADE-IMP-REC-RIT-VIS
        //                                       :IND-ADE-IMP-REC-RIT-VIS
        //                  ,IMP_REC_RIT_ACC        =
        //                :ADE-IMP-REC-RIT-ACC
        //                                       :IND-ADE-IMP-REC-RIT-ACC
        //                  ,FL_VARZ_STAT_TBGC      =
        //                :ADE-FL-VARZ-STAT-TBGC
        //                                       :IND-ADE-FL-VARZ-STAT-TBGC
        //                  ,FL_PROVZA_MIGRAZ       =
        //                :ADE-FL-PROVZA-MIGRAZ
        //                                       :IND-ADE-FL-PROVZA-MIGRAZ
        //                  ,IMPB_VIS_DA_REC        =
        //                :ADE-IMPB-VIS-DA-REC
        //                                       :IND-ADE-IMPB-VIS-DA-REC
        //                  ,DT_DECOR_PREST_BAN     =
        //           :ADE-DT-DECOR-PREST-BAN-DB
        //                                       :IND-ADE-DT-DECOR-PREST-BAN
        //                  ,DT_EFF_VARZ_STAT_T     =
        //           :ADE-DT-EFF-VARZ-STAT-T-DB
        //                                       :IND-ADE-DT-EFF-VARZ-STAT-T
        //                  ,DS_RIGA                =
        //                :ADE-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :ADE-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :ADE-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :ADE-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :ADE-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :ADE-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :ADE-DS-STATO-ELAB
        //                  ,CUM_CNBT_CAP           =
        //                :ADE-CUM-CNBT-CAP
        //                                       :IND-ADE-CUM-CNBT-CAP
        //                  ,IMP_GAR_CNBT           =
        //                :ADE-IMP-GAR-CNBT
        //                                       :IND-ADE-IMP-GAR-CNBT
        //                  ,DT_ULT_CONS_CNBT       =
        //           :ADE-DT-ULT-CONS-CNBT-DB
        //                                       :IND-ADE-DT-ULT-CONS-CNBT
        //                  ,IDEN_ISC_FND           =
        //                :ADE-IDEN-ISC-FND
        //                                       :IND-ADE-IDEN-ISC-FND
        //                  ,NUM_RAT_PIAN           =
        //                :ADE-NUM-RAT-PIAN
        //                                       :IND-ADE-NUM-RAT-PIAN
        //                  ,DT_PRESC               =
        //           :ADE-DT-PRESC-DB
        //                                       :IND-ADE-DT-PRESC
        //                  ,CONCS_PREST            =
        //                :ADE-CONCS-PREST
        //                                       :IND-ADE-CONCS-PREST
        //                WHERE     DS_RIGA = :ADE-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        adesDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-ADE
        //           END-EXEC.
        adesDao.openCIdUpdEffAde(ades.getAdeIdAdes(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-ADE
        //           END-EXEC.
        adesDao.closeCIdUpdEffAde();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-ADE
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //           END-EXEC.
        adesDao.fetchCIdUpdEffAde(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-ADE CURSOR FOR
        //              SELECT
        //                     ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,IB_PREV
        //                    ,IB_OGG
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,ETA_A_SCAD
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,TP_RGM_FISC
        //                    ,TP_RIAT
        //                    ,TP_MOD_PAG_TIT
        //                    ,TP_IAS
        //                    ,DT_VARZ_TP_IAS
        //                    ,PRE_NET_IND
        //                    ,PRE_LRD_IND
        //                    ,RAT_LRD_IND
        //                    ,PRSTZ_INI_IND
        //                    ,FL_COINC_ASSTO
        //                    ,IB_DFLT
        //                    ,MOD_CALC
        //                    ,TP_FNT_CNBTVA
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,PC_AZ
        //                    ,PC_ADER
        //                    ,PC_TFR
        //                    ,PC_VOLO
        //                    ,DT_NOVA_RGM_FISC
        //                    ,FL_ATTIV
        //                    ,IMP_REC_RIT_VIS
        //                    ,IMP_REC_RIT_ACC
        //                    ,FL_VARZ_STAT_TBGC
        //                    ,FL_PROVZA_MIGRAZ
        //                    ,IMPB_VIS_DA_REC
        //                    ,DT_DECOR_PREST_BAN
        //                    ,DT_EFF_VARZ_STAT_T
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,CUM_CNBT_CAP
        //                    ,IMP_GAR_CNBT
        //                    ,DT_ULT_CONS_CNBT
        //                    ,IDEN_ISC_FND
        //                    ,NUM_RAT_PIAN
        //                    ,DT_PRESC
        //                    ,CONCS_PREST
        //              FROM ADES
        //              WHERE     ID_POLI = :ADE-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_ADES ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     ID_POLI = :ADE-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        adesDao.selectRec1(ades.getAdeIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-ADE
        //           END-EXEC.
        adesDao.openCIdpEffAde(ades.getAdeIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-ADE
        //           END-EXEC.
        adesDao.closeCIdpEffAde();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-ADE
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //           END-EXEC.
        adesDao.fetchCIdpEffAde(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-EFF-ADE CURSOR FOR
        //              SELECT
        //                     ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,IB_PREV
        //                    ,IB_OGG
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,ETA_A_SCAD
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,TP_RGM_FISC
        //                    ,TP_RIAT
        //                    ,TP_MOD_PAG_TIT
        //                    ,TP_IAS
        //                    ,DT_VARZ_TP_IAS
        //                    ,PRE_NET_IND
        //                    ,PRE_LRD_IND
        //                    ,RAT_LRD_IND
        //                    ,PRSTZ_INI_IND
        //                    ,FL_COINC_ASSTO
        //                    ,IB_DFLT
        //                    ,MOD_CALC
        //                    ,TP_FNT_CNBTVA
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,PC_AZ
        //                    ,PC_ADER
        //                    ,PC_TFR
        //                    ,PC_VOLO
        //                    ,DT_NOVA_RGM_FISC
        //                    ,FL_ATTIV
        //                    ,IMP_REC_RIT_VIS
        //                    ,IMP_REC_RIT_ACC
        //                    ,FL_VARZ_STAT_TBGC
        //                    ,FL_PROVZA_MIGRAZ
        //                    ,IMPB_VIS_DA_REC
        //                    ,DT_DECOR_PREST_BAN
        //                    ,DT_EFF_VARZ_STAT_T
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,CUM_CNBT_CAP
        //                    ,IMP_GAR_CNBT
        //                    ,DT_ULT_CONS_CNBT
        //                    ,IDEN_ISC_FND
        //                    ,NUM_RAT_PIAN
        //                    ,DT_PRESC
        //                    ,CONCS_PREST
        //              FROM ADES
        //              WHERE     IB_OGG = :ADE-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_ADES ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     IB_OGG = :ADE-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        adesDao.selectRec2(ades.getAdeIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-EFF-ADE
        //           END-EXEC.
        adesDao.openCIboEffAde(ades.getAdeIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-EFF-ADE
        //           END-EXEC.
        adesDao.closeCIboEffAde();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-EFF-ADE
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //           END-EXEC.
        adesDao.fetchCIboEffAde(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX
            a570CloseCursorIbo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A605-DCL-CUR-IBS-PREV<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DclCurIbsPrev() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-ADE-0 CURSOR FOR
    //              SELECT
    //                     ID_ADES
    //                    ,ID_POLI
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,IB_PREV
    //                    ,IB_OGG
    //                    ,COD_COMP_ANIA
    //                    ,DT_DECOR
    //                    ,DT_SCAD
    //                    ,ETA_A_SCAD
    //                    ,DUR_AA
    //                    ,DUR_MM
    //                    ,DUR_GG
    //                    ,TP_RGM_FISC
    //                    ,TP_RIAT
    //                    ,TP_MOD_PAG_TIT
    //                    ,TP_IAS
    //                    ,DT_VARZ_TP_IAS
    //                    ,PRE_NET_IND
    //                    ,PRE_LRD_IND
    //                    ,RAT_LRD_IND
    //                    ,PRSTZ_INI_IND
    //                    ,FL_COINC_ASSTO
    //                    ,IB_DFLT
    //                    ,MOD_CALC
    //                    ,TP_FNT_CNBTVA
    //                    ,IMP_AZ
    //                    ,IMP_ADER
    //                    ,IMP_TFR
    //                    ,IMP_VOLO
    //                    ,PC_AZ
    //                    ,PC_ADER
    //                    ,PC_TFR
    //                    ,PC_VOLO
    //                    ,DT_NOVA_RGM_FISC
    //                    ,FL_ATTIV
    //                    ,IMP_REC_RIT_VIS
    //                    ,IMP_REC_RIT_ACC
    //                    ,FL_VARZ_STAT_TBGC
    //                    ,FL_PROVZA_MIGRAZ
    //                    ,IMPB_VIS_DA_REC
    //                    ,DT_DECOR_PREST_BAN
    //                    ,DT_EFF_VARZ_STAT_T
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,CUM_CNBT_CAP
    //                    ,IMP_GAR_CNBT
    //                    ,DT_ULT_CONS_CNBT
    //                    ,IDEN_ISC_FND
    //                    ,NUM_RAT_PIAN
    //                    ,DT_PRESC
    //                    ,CONCS_PREST
    //              FROM ADES
    //              WHERE     IB_PREV = :ADE-IB-PREV
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_ADES ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DCL-CUR-IBS-DFLT<br>*/
    private void a605DclCurIbsDflt() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-EFF-ADE-1 CURSOR FOR
    //              SELECT
    //                     ID_ADES
    //                    ,ID_POLI
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,IB_PREV
    //                    ,IB_OGG
    //                    ,COD_COMP_ANIA
    //                    ,DT_DECOR
    //                    ,DT_SCAD
    //                    ,ETA_A_SCAD
    //                    ,DUR_AA
    //                    ,DUR_MM
    //                    ,DUR_GG
    //                    ,TP_RGM_FISC
    //                    ,TP_RIAT
    //                    ,TP_MOD_PAG_TIT
    //                    ,TP_IAS
    //                    ,DT_VARZ_TP_IAS
    //                    ,PRE_NET_IND
    //                    ,PRE_LRD_IND
    //                    ,RAT_LRD_IND
    //                    ,PRSTZ_INI_IND
    //                    ,FL_COINC_ASSTO
    //                    ,IB_DFLT
    //                    ,MOD_CALC
    //                    ,TP_FNT_CNBTVA
    //                    ,IMP_AZ
    //                    ,IMP_ADER
    //                    ,IMP_TFR
    //                    ,IMP_VOLO
    //                    ,PC_AZ
    //                    ,PC_ADER
    //                    ,PC_TFR
    //                    ,PC_VOLO
    //                    ,DT_NOVA_RGM_FISC
    //                    ,FL_ATTIV
    //                    ,IMP_REC_RIT_VIS
    //                    ,IMP_REC_RIT_ACC
    //                    ,FL_VARZ_STAT_TBGC
    //                    ,FL_PROVZA_MIGRAZ
    //                    ,IMPB_VIS_DA_REC
    //                    ,DT_DECOR_PREST_BAN
    //                    ,DT_EFF_VARZ_STAT_T
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,CUM_CNBT_CAP
    //                    ,IMP_GAR_CNBT
    //                    ,DT_ULT_CONS_CNBT
    //                    ,IDEN_ISC_FND
    //                    ,NUM_RAT_PIAN
    //                    ,DT_PRESC
    //                    ,CONCS_PREST
    //              FROM ADES
    //              WHERE     IB_DFLT = :ADE-IB-DFLT
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND ID_MOVI_CHIU IS NULL
    //              ORDER BY ID_ADES ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //                  THRU A605-PREV-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-PREV
            //              THRU A605-PREV-EX
            a605DclCurIbsPrev();
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //                  THRU A605-DFLT-EX
            //           END-IF
            // COB_CODE: PERFORM A605-DCL-CUR-IBS-DFLT
            //              THRU A605-DFLT-EX
            a605DclCurIbsDflt();
        }
    }

    /**Original name: A610-SELECT-IBS-PREV<br>*/
    private void a610SelectIbsPrev() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     IB_PREV = :ADE-IB-PREV
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        adesDao.selectRec3(ades.getAdeIbPrev(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS-DFLT<br>*/
    private void a610SelectIbsDflt() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     IB_DFLT = :ADE-IB-DFLT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        adesDao.selectRec4(ades.getAdeIbDflt(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //                  THRU A610-PREV-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: PERFORM A610-SELECT-IBS-PREV
            //              THRU A610-PREV-EX
            a610SelectIbsPrev();
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //                  THRU A610-DFLT-EX
            //           END-IF
            // COB_CODE: PERFORM A610-SELECT-IBS-DFLT
            //              THRU A610-DFLT-EX
            a610SelectIbsDflt();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-ADE-0
            //           END-EXEC
            adesDao.openCIbsEffAde0(ades.getAdeIbPrev(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-EFF-ADE-1
            //           END-EXEC
            adesDao.openCIbsEffAde1(ades.getAdeIbDflt(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-ADE-0
            //           END-EXEC
            adesDao.closeCIbsEffAde0();
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-EFF-ADE-1
            //           END-EXEC
            adesDao.closeCIbsEffAde1();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FN-IBS-PREV<br>*/
    private void a690FnIbsPrev() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-ADE-0
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //           END-EXEC.
        adesDao.fetchCIbsEffAde0(this);
    }

    /**Original name: A690-FN-IBS-DFLT<br>*/
    private void a690FnIbsDflt() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-EFF-ADE-1
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //           END-EXEC.
        adesDao.fetchCIbsEffAde1(this);
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //                  THRU A690-PREV-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: PERFORM A690-FN-IBS-PREV
            //              THRU A690-PREV-EX
            a690FnIbsPrev();
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //                  THRU A690-DFLT-EX
            //           END-IF
            // COB_CODE: PERFORM A690-FN-IBS-DFLT
            //              THRU A690-DFLT-EX
            a690FnIbsDflt();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS     THRU A670-EX
            a670CloseCursorIbs();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     ID_ADES = :ADE-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        adesDao.selectRec5(ades.getAdeIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-ADE CURSOR FOR
        //              SELECT
        //                     ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,IB_PREV
        //                    ,IB_OGG
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,ETA_A_SCAD
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,TP_RGM_FISC
        //                    ,TP_RIAT
        //                    ,TP_MOD_PAG_TIT
        //                    ,TP_IAS
        //                    ,DT_VARZ_TP_IAS
        //                    ,PRE_NET_IND
        //                    ,PRE_LRD_IND
        //                    ,RAT_LRD_IND
        //                    ,PRSTZ_INI_IND
        //                    ,FL_COINC_ASSTO
        //                    ,IB_DFLT
        //                    ,MOD_CALC
        //                    ,TP_FNT_CNBTVA
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,PC_AZ
        //                    ,PC_ADER
        //                    ,PC_TFR
        //                    ,PC_VOLO
        //                    ,DT_NOVA_RGM_FISC
        //                    ,FL_ATTIV
        //                    ,IMP_REC_RIT_VIS
        //                    ,IMP_REC_RIT_ACC
        //                    ,FL_VARZ_STAT_TBGC
        //                    ,FL_PROVZA_MIGRAZ
        //                    ,IMPB_VIS_DA_REC
        //                    ,DT_DECOR_PREST_BAN
        //                    ,DT_EFF_VARZ_STAT_T
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,CUM_CNBT_CAP
        //                    ,IMP_GAR_CNBT
        //                    ,DT_ULT_CONS_CNBT
        //                    ,IDEN_ISC_FND
        //                    ,NUM_RAT_PIAN
        //                    ,DT_PRESC
        //                    ,CONCS_PREST
        //              FROM ADES
        //              WHERE     ID_POLI = :ADE-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_ADES ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     ID_POLI = :ADE-ID-POLI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        adesDao.selectRec6(ades.getAdeIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-ADE
        //           END-EXEC.
        adesDao.openCIdpCpzAde(ades.getAdeIdPoli(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-ADE
        //           END-EXEC.
        adesDao.closeCIdpCpzAde();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-ADE
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //           END-EXEC.
        adesDao.fetchCIdpCpzAde(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-CPZ-ADE CURSOR FOR
        //              SELECT
        //                     ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,IB_PREV
        //                    ,IB_OGG
        //                    ,COD_COMP_ANIA
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,ETA_A_SCAD
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,TP_RGM_FISC
        //                    ,TP_RIAT
        //                    ,TP_MOD_PAG_TIT
        //                    ,TP_IAS
        //                    ,DT_VARZ_TP_IAS
        //                    ,PRE_NET_IND
        //                    ,PRE_LRD_IND
        //                    ,RAT_LRD_IND
        //                    ,PRSTZ_INI_IND
        //                    ,FL_COINC_ASSTO
        //                    ,IB_DFLT
        //                    ,MOD_CALC
        //                    ,TP_FNT_CNBTVA
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,PC_AZ
        //                    ,PC_ADER
        //                    ,PC_TFR
        //                    ,PC_VOLO
        //                    ,DT_NOVA_RGM_FISC
        //                    ,FL_ATTIV
        //                    ,IMP_REC_RIT_VIS
        //                    ,IMP_REC_RIT_ACC
        //                    ,FL_VARZ_STAT_TBGC
        //                    ,FL_PROVZA_MIGRAZ
        //                    ,IMPB_VIS_DA_REC
        //                    ,DT_DECOR_PREST_BAN
        //                    ,DT_EFF_VARZ_STAT_T
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,CUM_CNBT_CAP
        //                    ,IMP_GAR_CNBT
        //                    ,DT_ULT_CONS_CNBT
        //                    ,IDEN_ISC_FND
        //                    ,NUM_RAT_PIAN
        //                    ,DT_PRESC
        //                    ,CONCS_PREST
        //              FROM ADES
        //              WHERE     IB_OGG = :ADE-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_ADES ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     IB_OGG = :ADE-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        adesDao.selectRec7(ades.getAdeIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-CPZ-ADE
        //           END-EXEC.
        adesDao.openCIboCpzAde(ades.getAdeIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-CPZ-ADE
        //           END-EXEC.
        adesDao.closeCIboCpzAde();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-CPZ-ADE
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //           END-EXEC.
        adesDao.fetchCIboCpzAde(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX
            b570CloseCursorIboCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B605-DCL-CUR-IBS-PREV<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DclCurIbsPrev() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-ADE-0 CURSOR FOR
    //              SELECT
    //                     ID_ADES
    //                    ,ID_POLI
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,IB_PREV
    //                    ,IB_OGG
    //                    ,COD_COMP_ANIA
    //                    ,DT_DECOR
    //                    ,DT_SCAD
    //                    ,ETA_A_SCAD
    //                    ,DUR_AA
    //                    ,DUR_MM
    //                    ,DUR_GG
    //                    ,TP_RGM_FISC
    //                    ,TP_RIAT
    //                    ,TP_MOD_PAG_TIT
    //                    ,TP_IAS
    //                    ,DT_VARZ_TP_IAS
    //                    ,PRE_NET_IND
    //                    ,PRE_LRD_IND
    //                    ,RAT_LRD_IND
    //                    ,PRSTZ_INI_IND
    //                    ,FL_COINC_ASSTO
    //                    ,IB_DFLT
    //                    ,MOD_CALC
    //                    ,TP_FNT_CNBTVA
    //                    ,IMP_AZ
    //                    ,IMP_ADER
    //                    ,IMP_TFR
    //                    ,IMP_VOLO
    //                    ,PC_AZ
    //                    ,PC_ADER
    //                    ,PC_TFR
    //                    ,PC_VOLO
    //                    ,DT_NOVA_RGM_FISC
    //                    ,FL_ATTIV
    //                    ,IMP_REC_RIT_VIS
    //                    ,IMP_REC_RIT_ACC
    //                    ,FL_VARZ_STAT_TBGC
    //                    ,FL_PROVZA_MIGRAZ
    //                    ,IMPB_VIS_DA_REC
    //                    ,DT_DECOR_PREST_BAN
    //                    ,DT_EFF_VARZ_STAT_T
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,CUM_CNBT_CAP
    //                    ,IMP_GAR_CNBT
    //                    ,DT_ULT_CONS_CNBT
    //                    ,IDEN_ISC_FND
    //                    ,NUM_RAT_PIAN
    //                    ,DT_PRESC
    //                    ,CONCS_PREST
    //              FROM ADES
    //              WHERE     IB_PREV = :ADE-IB-PREV
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_ADES ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DCL-CUR-IBS-DFLT<br>*/
    private void b605DclCurIbsDflt() {
    // COB_CODE: EXEC SQL
    //                DECLARE C-IBS-CPZ-ADE-1 CURSOR FOR
    //              SELECT
    //                     ID_ADES
    //                    ,ID_POLI
    //                    ,ID_MOVI_CRZ
    //                    ,ID_MOVI_CHIU
    //                    ,DT_INI_EFF
    //                    ,DT_END_EFF
    //                    ,IB_PREV
    //                    ,IB_OGG
    //                    ,COD_COMP_ANIA
    //                    ,DT_DECOR
    //                    ,DT_SCAD
    //                    ,ETA_A_SCAD
    //                    ,DUR_AA
    //                    ,DUR_MM
    //                    ,DUR_GG
    //                    ,TP_RGM_FISC
    //                    ,TP_RIAT
    //                    ,TP_MOD_PAG_TIT
    //                    ,TP_IAS
    //                    ,DT_VARZ_TP_IAS
    //                    ,PRE_NET_IND
    //                    ,PRE_LRD_IND
    //                    ,RAT_LRD_IND
    //                    ,PRSTZ_INI_IND
    //                    ,FL_COINC_ASSTO
    //                    ,IB_DFLT
    //                    ,MOD_CALC
    //                    ,TP_FNT_CNBTVA
    //                    ,IMP_AZ
    //                    ,IMP_ADER
    //                    ,IMP_TFR
    //                    ,IMP_VOLO
    //                    ,PC_AZ
    //                    ,PC_ADER
    //                    ,PC_TFR
    //                    ,PC_VOLO
    //                    ,DT_NOVA_RGM_FISC
    //                    ,FL_ATTIV
    //                    ,IMP_REC_RIT_VIS
    //                    ,IMP_REC_RIT_ACC
    //                    ,FL_VARZ_STAT_TBGC
    //                    ,FL_PROVZA_MIGRAZ
    //                    ,IMPB_VIS_DA_REC
    //                    ,DT_DECOR_PREST_BAN
    //                    ,DT_EFF_VARZ_STAT_T
    //                    ,DS_RIGA
    //                    ,DS_OPER_SQL
    //                    ,DS_VER
    //                    ,DS_TS_INI_CPTZ
    //                    ,DS_TS_END_CPTZ
    //                    ,DS_UTENTE
    //                    ,DS_STATO_ELAB
    //                    ,CUM_CNBT_CAP
    //                    ,IMP_GAR_CNBT
    //                    ,DT_ULT_CONS_CNBT
    //                    ,IDEN_ISC_FND
    //                    ,NUM_RAT_PIAN
    //                    ,DT_PRESC
    //                    ,CONCS_PREST
    //              FROM ADES
    //              WHERE     IB_DFLT = :ADE-IB-DFLT
    //                    AND COD_COMP_ANIA =
    //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
    //                    AND DT_INI_EFF <=
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DT_END_EFF >
    //                        :WS-DATA-INIZIO-EFFETTO-DB
    //                    AND DS_TS_INI_CPTZ <=
    //                         :WS-TS-COMPETENZA
    //                    AND DS_TS_END_CPTZ >
    //                         :WS-TS-COMPETENZA
    //              ORDER BY ID_ADES ASC
    //           END-EXEC.
    // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //                  THRU B605-PREV-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: PERFORM B605-DCL-CUR-IBS-PREV
            //              THRU B605-PREV-EX
            b605DclCurIbsPrev();
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //                  THRU B605-DFLT-EX
            //           END-IF
            // COB_CODE: PERFORM B605-DCL-CUR-IBS-DFLT
            //              THRU B605-DFLT-EX
            b605DclCurIbsDflt();
        }
    }

    /**Original name: B610-SELECT-IBS-PREV<br>*/
    private void b610SelectIbsPrev() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     IB_PREV = :ADE-IB-PREV
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        adesDao.selectRec8(ades.getAdeIbPrev(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-DFLT<br>*/
    private void b610SelectIbsDflt() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,IB_PREV
        //                ,IB_OGG
        //                ,COD_COMP_ANIA
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,ETA_A_SCAD
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,TP_RGM_FISC
        //                ,TP_RIAT
        //                ,TP_MOD_PAG_TIT
        //                ,TP_IAS
        //                ,DT_VARZ_TP_IAS
        //                ,PRE_NET_IND
        //                ,PRE_LRD_IND
        //                ,RAT_LRD_IND
        //                ,PRSTZ_INI_IND
        //                ,FL_COINC_ASSTO
        //                ,IB_DFLT
        //                ,MOD_CALC
        //                ,TP_FNT_CNBTVA
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,PC_AZ
        //                ,PC_ADER
        //                ,PC_TFR
        //                ,PC_VOLO
        //                ,DT_NOVA_RGM_FISC
        //                ,FL_ATTIV
        //                ,IMP_REC_RIT_VIS
        //                ,IMP_REC_RIT_ACC
        //                ,FL_VARZ_STAT_TBGC
        //                ,FL_PROVZA_MIGRAZ
        //                ,IMPB_VIS_DA_REC
        //                ,DT_DECOR_PREST_BAN
        //                ,DT_EFF_VARZ_STAT_T
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,CUM_CNBT_CAP
        //                ,IMP_GAR_CNBT
        //                ,DT_ULT_CONS_CNBT
        //                ,IDEN_ISC_FND
        //                ,NUM_RAT_PIAN
        //                ,DT_PRESC
        //                ,CONCS_PREST
        //             INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //             FROM ADES
        //             WHERE     IB_DFLT = :ADE-IB-DFLT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        adesDao.selectRec9(ades.getAdeIbDflt(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //                  THRU B610-PREV-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: PERFORM B610-SELECT-IBS-PREV
            //              THRU B610-PREV-EX
            b610SelectIbsPrev();
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //                  THRU B610-DFLT-EX
            //           END-IF
            // COB_CODE: PERFORM B610-SELECT-IBS-DFLT
            //              THRU B610-DFLT-EX
            b610SelectIbsDflt();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-ADE-0
            //           END-EXEC
            adesDao.openCIbsCpzAde0(ades.getAdeIbPrev(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                OPEN C-IBS-CPZ-ADE-1
            //           END-EXEC
            adesDao.openCIbsCpzAde1(ades.getAdeIbDflt(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //              END-EXEC
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-ADE-0
            //           END-EXEC
            adesDao.closeCIbsCpzAde0();
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //              END-EXEC
            //           END-IF
            // COB_CODE: EXEC SQL
            //                CLOSE C-IBS-CPZ-ADE-1
            //           END-EXEC
            adesDao.closeCIbsCpzAde1();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FN-IBS-PREV<br>*/
    private void b690FnIbsPrev() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-ADE-0
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //           END-EXEC.
        adesDao.fetchCIbsCpzAde0(this);
    }

    /**Original name: B690-FN-IBS-DFLT<br>*/
    private void b690FnIbsDflt() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBS-CPZ-ADE-1
        //           INTO
        //                :ADE-ID-ADES
        //               ,:ADE-ID-POLI
        //               ,:ADE-ID-MOVI-CRZ
        //               ,:ADE-ID-MOVI-CHIU
        //                :IND-ADE-ID-MOVI-CHIU
        //               ,:ADE-DT-INI-EFF-DB
        //               ,:ADE-DT-END-EFF-DB
        //               ,:ADE-IB-PREV
        //                :IND-ADE-IB-PREV
        //               ,:ADE-IB-OGG
        //                :IND-ADE-IB-OGG
        //               ,:ADE-COD-COMP-ANIA
        //               ,:ADE-DT-DECOR-DB
        //                :IND-ADE-DT-DECOR
        //               ,:ADE-DT-SCAD-DB
        //                :IND-ADE-DT-SCAD
        //               ,:ADE-ETA-A-SCAD
        //                :IND-ADE-ETA-A-SCAD
        //               ,:ADE-DUR-AA
        //                :IND-ADE-DUR-AA
        //               ,:ADE-DUR-MM
        //                :IND-ADE-DUR-MM
        //               ,:ADE-DUR-GG
        //                :IND-ADE-DUR-GG
        //               ,:ADE-TP-RGM-FISC
        //               ,:ADE-TP-RIAT
        //                :IND-ADE-TP-RIAT
        //               ,:ADE-TP-MOD-PAG-TIT
        //               ,:ADE-TP-IAS
        //                :IND-ADE-TP-IAS
        //               ,:ADE-DT-VARZ-TP-IAS-DB
        //                :IND-ADE-DT-VARZ-TP-IAS
        //               ,:ADE-PRE-NET-IND
        //                :IND-ADE-PRE-NET-IND
        //               ,:ADE-PRE-LRD-IND
        //                :IND-ADE-PRE-LRD-IND
        //               ,:ADE-RAT-LRD-IND
        //                :IND-ADE-RAT-LRD-IND
        //               ,:ADE-PRSTZ-INI-IND
        //                :IND-ADE-PRSTZ-INI-IND
        //               ,:ADE-FL-COINC-ASSTO
        //                :IND-ADE-FL-COINC-ASSTO
        //               ,:ADE-IB-DFLT
        //                :IND-ADE-IB-DFLT
        //               ,:ADE-MOD-CALC
        //                :IND-ADE-MOD-CALC
        //               ,:ADE-TP-FNT-CNBTVA
        //                :IND-ADE-TP-FNT-CNBTVA
        //               ,:ADE-IMP-AZ
        //                :IND-ADE-IMP-AZ
        //               ,:ADE-IMP-ADER
        //                :IND-ADE-IMP-ADER
        //               ,:ADE-IMP-TFR
        //                :IND-ADE-IMP-TFR
        //               ,:ADE-IMP-VOLO
        //                :IND-ADE-IMP-VOLO
        //               ,:ADE-PC-AZ
        //                :IND-ADE-PC-AZ
        //               ,:ADE-PC-ADER
        //                :IND-ADE-PC-ADER
        //               ,:ADE-PC-TFR
        //                :IND-ADE-PC-TFR
        //               ,:ADE-PC-VOLO
        //                :IND-ADE-PC-VOLO
        //               ,:ADE-DT-NOVA-RGM-FISC-DB
        //                :IND-ADE-DT-NOVA-RGM-FISC
        //               ,:ADE-FL-ATTIV
        //                :IND-ADE-FL-ATTIV
        //               ,:ADE-IMP-REC-RIT-VIS
        //                :IND-ADE-IMP-REC-RIT-VIS
        //               ,:ADE-IMP-REC-RIT-ACC
        //                :IND-ADE-IMP-REC-RIT-ACC
        //               ,:ADE-FL-VARZ-STAT-TBGC
        //                :IND-ADE-FL-VARZ-STAT-TBGC
        //               ,:ADE-FL-PROVZA-MIGRAZ
        //                :IND-ADE-FL-PROVZA-MIGRAZ
        //               ,:ADE-IMPB-VIS-DA-REC
        //                :IND-ADE-IMPB-VIS-DA-REC
        //               ,:ADE-DT-DECOR-PREST-BAN-DB
        //                :IND-ADE-DT-DECOR-PREST-BAN
        //               ,:ADE-DT-EFF-VARZ-STAT-T-DB
        //                :IND-ADE-DT-EFF-VARZ-STAT-T
        //               ,:ADE-DS-RIGA
        //               ,:ADE-DS-OPER-SQL
        //               ,:ADE-DS-VER
        //               ,:ADE-DS-TS-INI-CPTZ
        //               ,:ADE-DS-TS-END-CPTZ
        //               ,:ADE-DS-UTENTE
        //               ,:ADE-DS-STATO-ELAB
        //               ,:ADE-CUM-CNBT-CAP
        //                :IND-ADE-CUM-CNBT-CAP
        //               ,:ADE-IMP-GAR-CNBT
        //                :IND-ADE-IMP-GAR-CNBT
        //               ,:ADE-DT-ULT-CONS-CNBT-DB
        //                :IND-ADE-DT-ULT-CONS-CNBT
        //               ,:ADE-IDEN-ISC-FND
        //                :IND-ADE-IDEN-ISC-FND
        //               ,:ADE-NUM-RAT-PIAN
        //                :IND-ADE-NUM-RAT-PIAN
        //               ,:ADE-DT-PRESC-DB
        //                :IND-ADE-DT-PRESC
        //               ,:ADE-CONCS-PREST
        //                :IND-ADE-CONCS-PREST
        //           END-EXEC.
        adesDao.fetchCIbsCpzAde1(this);
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: IF ADE-IB-PREV NOT = HIGH-VALUES
        //                  THRU B690-PREV-EX
        //           ELSE
        //           END-IF
        //           END-IF.
        if (!Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: PERFORM B690-FN-IBS-PREV
            //              THRU B690-PREV-EX
            b690FnIbsPrev();
        }
        else if (!Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: IF ADE-IB-DFLT NOT = HIGH-VALUES
            //                  THRU B690-DFLT-EX
            //           END-IF
            // COB_CODE: PERFORM B690-FN-IBS-DFLT
            //              THRU B690-DFLT-EX
            b690FnIbsDflt();
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ     THRU B670-EX
            b670CloseCursorIbsCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-ADE-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndAdes().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-ID-MOVI-CHIU-NULL
            ades.getAdeIdMoviChiu().setAdeIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeIdMoviChiu.Len.ADE_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-ADE-IB-PREV = -1
        //              MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
        //           END-IF
        if (ws.getIndAdes().getIbPrev() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IB-PREV-NULL
            ades.setAdeIbPrev(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IB_PREV));
        }
        // COB_CODE: IF IND-ADE-IB-OGG = -1
        //              MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
        //           END-IF
        if (ws.getIndAdes().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IB-OGG-NULL
            ades.setAdeIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IB_OGG));
        }
        // COB_CODE: IF IND-ADE-DT-DECOR = -1
        //              MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
        //           END-IF
        if (ws.getIndAdes().getDtDecor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-DECOR-NULL
            ades.getAdeDtDecor().setAdeDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtDecor.Len.ADE_DT_DECOR_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndAdes().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-SCAD-NULL
            ades.getAdeDtScad().setAdeDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtScad.Len.ADE_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-ADE-ETA-A-SCAD = -1
        //              MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
        //           END-IF
        if (ws.getIndAdes().getEtaAScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-ETA-A-SCAD-NULL
            ades.getAdeEtaAScad().setAdeEtaAScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeEtaAScad.Len.ADE_ETA_A_SCAD_NULL));
        }
        // COB_CODE: IF IND-ADE-DUR-AA = -1
        //              MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
        //           END-IF
        if (ws.getIndAdes().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-AA-NULL
            ades.getAdeDurAa().setAdeDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurAa.Len.ADE_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-ADE-DUR-MM = -1
        //              MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
        //           END-IF
        if (ws.getIndAdes().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-MM-NULL
            ades.getAdeDurMm().setAdeDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurMm.Len.ADE_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-ADE-DUR-GG = -1
        //              MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
        //           END-IF
        if (ws.getIndAdes().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DUR-GG-NULL
            ades.getAdeDurGg().setAdeDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDurGg.Len.ADE_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-ADE-TP-RIAT = -1
        //              MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
        //           END-IF
        if (ws.getIndAdes().getTpRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-TP-RIAT-NULL
            ades.setAdeTpRiat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_TP_RIAT));
        }
        // COB_CODE: IF IND-ADE-TP-IAS = -1
        //              MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
        //           END-IF
        if (ws.getIndAdes().getTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-TP-IAS-NULL
            ades.setAdeTpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_TP_IAS));
        }
        // COB_CODE: IF IND-ADE-DT-VARZ-TP-IAS = -1
        //              MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
        //           END-IF
        if (ws.getIndAdes().getDtVarzTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-VARZ-TP-IAS-NULL
            ades.getAdeDtVarzTpIas().setAdeDtVarzTpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtVarzTpIas.Len.ADE_DT_VARZ_TP_IAS_NULL));
        }
        // COB_CODE: IF IND-ADE-PRE-NET-IND = -1
        //              MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getPreNetInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PRE-NET-IND-NULL
            ades.getAdePreNetInd().setAdePreNetIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePreNetInd.Len.ADE_PRE_NET_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-PRE-LRD-IND = -1
        //              MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getPreLrdInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PRE-LRD-IND-NULL
            ades.getAdePreLrdInd().setAdePreLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePreLrdInd.Len.ADE_PRE_LRD_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-RAT-LRD-IND = -1
        //              MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getRatLrdInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-RAT-LRD-IND-NULL
            ades.getAdeRatLrdInd().setAdeRatLrdIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeRatLrdInd.Len.ADE_RAT_LRD_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-PRSTZ-INI-IND = -1
        //              MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
        //           END-IF
        if (ws.getIndAdes().getPrstzIniInd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PRSTZ-INI-IND-NULL
            ades.getAdePrstzIniInd().setAdePrstzIniIndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePrstzIniInd.Len.ADE_PRSTZ_INI_IND_NULL));
        }
        // COB_CODE: IF IND-ADE-FL-COINC-ASSTO = -1
        //              MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
        //           END-IF
        if (ws.getIndAdes().getFlCoincAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-COINC-ASSTO-NULL
            ades.setAdeFlCoincAssto(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-IB-DFLT = -1
        //              MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
        //           END-IF
        if (ws.getIndAdes().getIbDflt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IB-DFLT-NULL
            ades.setAdeIbDflt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IB_DFLT));
        }
        // COB_CODE: IF IND-ADE-MOD-CALC = -1
        //              MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
        //           END-IF
        if (ws.getIndAdes().getModCalc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-MOD-CALC-NULL
            ades.setAdeModCalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_MOD_CALC));
        }
        // COB_CODE: IF IND-ADE-TP-FNT-CNBTVA = -1
        //              MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
        //           END-IF
        if (ws.getIndAdes().getTpFntCnbtva() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-TP-FNT-CNBTVA-NULL
            ades.setAdeTpFntCnbtva(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_TP_FNT_CNBTVA));
        }
        // COB_CODE: IF IND-ADE-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndAdes().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-AZ-NULL
            ades.getAdeImpAz().setAdeImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpAz.Len.ADE_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndAdes().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-ADER-NULL
            ades.getAdeImpAder().setAdeImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpAder.Len.ADE_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndAdes().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-TFR-NULL
            ades.getAdeImpTfr().setAdeImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpTfr.Len.ADE_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndAdes().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-VOLO-NULL
            ades.getAdeImpVolo().setAdeImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpVolo.Len.ADE_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-AZ = -1
        //              MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
        //           END-IF
        if (ws.getIndAdes().getPcAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-AZ-NULL
            ades.getAdePcAz().setAdePcAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcAz.Len.ADE_PC_AZ_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-ADER = -1
        //              MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
        //           END-IF
        if (ws.getIndAdes().getPcAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-ADER-NULL
            ades.getAdePcAder().setAdePcAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcAder.Len.ADE_PC_ADER_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-TFR = -1
        //              MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
        //           END-IF
        if (ws.getIndAdes().getPcTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-TFR-NULL
            ades.getAdePcTfr().setAdePcTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcTfr.Len.ADE_PC_TFR_NULL));
        }
        // COB_CODE: IF IND-ADE-PC-VOLO = -1
        //              MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
        //           END-IF
        if (ws.getIndAdes().getPcVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-PC-VOLO-NULL
            ades.getAdePcVolo().setAdePcVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdePcVolo.Len.ADE_PC_VOLO_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-NOVA-RGM-FISC = -1
        //              MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
        //           END-IF
        if (ws.getIndAdes().getDtNovaRgmFisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-NOVA-RGM-FISC-NULL
            ades.getAdeDtNovaRgmFisc().setAdeDtNovaRgmFiscNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtNovaRgmFisc.Len.ADE_DT_NOVA_RGM_FISC_NULL));
        }
        // COB_CODE: IF IND-ADE-FL-ATTIV = -1
        //              MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
        //           END-IF
        if (ws.getIndAdes().getFlAttiv() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-ATTIV-NULL
            ades.setAdeFlAttiv(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-IMP-REC-RIT-VIS = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
        //           END-IF
        if (ws.getIndAdes().getImpRecRitVis() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-VIS-NULL
            ades.getAdeImpRecRitVis().setAdeImpRecRitVisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpRecRitVis.Len.ADE_IMP_REC_RIT_VIS_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-REC-RIT-ACC = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
        //           END-IF
        if (ws.getIndAdes().getImpRecRitAcc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-REC-RIT-ACC-NULL
            ades.getAdeImpRecRitAcc().setAdeImpRecRitAccNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpRecRitAcc.Len.ADE_IMP_REC_RIT_ACC_NULL));
        }
        // COB_CODE: IF IND-ADE-FL-VARZ-STAT-TBGC = -1
        //              MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
        //           END-IF
        if (ws.getIndAdes().getFlVarzStatTbgc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-VARZ-STAT-TBGC-NULL
            ades.setAdeFlVarzStatTbgc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-FL-PROVZA-MIGRAZ = -1
        //              MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
        //           END-IF
        if (ws.getIndAdes().getFlProvzaMigraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-FL-PROVZA-MIGRAZ-NULL
            ades.setAdeFlProvzaMigraz(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-ADE-IMPB-VIS-DA-REC = -1
        //              MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
        //           END-IF
        if (ws.getIndAdes().getImpbVisDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMPB-VIS-DA-REC-NULL
            ades.getAdeImpbVisDaRec().setAdeImpbVisDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpbVisDaRec.Len.ADE_IMPB_VIS_DA_REC_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-DECOR-PREST-BAN = -1
        //              MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
        //           END-IF
        if (ws.getIndAdes().getDtDecorPrestBan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-DECOR-PREST-BAN-NULL
            ades.getAdeDtDecorPrestBan().setAdeDtDecorPrestBanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtDecorPrestBan.Len.ADE_DT_DECOR_PREST_BAN_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-EFF-VARZ-STAT-T = -1
        //              MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
        //           END-IF
        if (ws.getIndAdes().getDtEffVarzStatT() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-EFF-VARZ-STAT-T-NULL
            ades.getAdeDtEffVarzStatT().setAdeDtEffVarzStatTNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtEffVarzStatT.Len.ADE_DT_EFF_VARZ_STAT_T_NULL));
        }
        // COB_CODE: IF IND-ADE-CUM-CNBT-CAP = -1
        //              MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
        //           END-IF
        if (ws.getIndAdes().getCumCnbtCap() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-CUM-CNBT-CAP-NULL
            ades.getAdeCumCnbtCap().setAdeCumCnbtCapNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeCumCnbtCap.Len.ADE_CUM_CNBT_CAP_NULL));
        }
        // COB_CODE: IF IND-ADE-IMP-GAR-CNBT = -1
        //              MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
        //           END-IF
        if (ws.getIndAdes().getImpGarCnbt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IMP-GAR-CNBT-NULL
            ades.getAdeImpGarCnbt().setAdeImpGarCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeImpGarCnbt.Len.ADE_IMP_GAR_CNBT_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-ULT-CONS-CNBT = -1
        //              MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
        //           END-IF
        if (ws.getIndAdes().getDtUltConsCnbt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-ULT-CONS-CNBT-NULL
            ades.getAdeDtUltConsCnbt().setAdeDtUltConsCnbtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtUltConsCnbt.Len.ADE_DT_ULT_CONS_CNBT_NULL));
        }
        // COB_CODE: IF IND-ADE-IDEN-ISC-FND = -1
        //              MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
        //           END-IF
        if (ws.getIndAdes().getIdenIscFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-IDEN-ISC-FND-NULL
            ades.setAdeIdenIscFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ades.Len.ADE_IDEN_ISC_FND));
        }
        // COB_CODE: IF IND-ADE-NUM-RAT-PIAN = -1
        //              MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
        //           END-IF
        if (ws.getIndAdes().getNumRatPian() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-NUM-RAT-PIAN-NULL
            ades.getAdeNumRatPian().setAdeNumRatPianNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeNumRatPian.Len.ADE_NUM_RAT_PIAN_NULL));
        }
        // COB_CODE: IF IND-ADE-DT-PRESC = -1
        //              MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
        //           END-IF
        if (ws.getIndAdes().getDtPresc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-DT-PRESC-NULL
            ades.getAdeDtPresc().setAdeDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeDtPresc.Len.ADE_DT_PRESC_NULL));
        }
        // COB_CODE: IF IND-ADE-CONCS-PREST = -1
        //              MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
        //           END-IF.
        if (ws.getIndAdes().getConcsPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ADE-CONCS-PREST-NULL
            ades.setAdeConcsPrest(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO ADE-DS-OPER-SQL
        ades.setAdeDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO ADE-DS-VER
        ades.setAdeDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO ADE-DS-UTENTE
        ades.setAdeDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO ADE-DS-STATO-ELAB.
        ades.setAdeDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO ADE-DS-OPER-SQL
        ades.setAdeDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO ADE-DS-UTENTE.
        ades.setAdeDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF ADE-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-ADE-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIdMoviChiu().getAdeIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-ID-MOVI-CHIU
            ws.getIndAdes().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-ID-MOVI-CHIU
            ws.getIndAdes().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF ADE-IB-PREV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IB-PREV
        //           ELSE
        //              MOVE 0 TO IND-ADE-IB-PREV
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIbPrev(), Ades.Len.ADE_IB_PREV)) {
            // COB_CODE: MOVE -1 TO IND-ADE-IB-PREV
            ws.getIndAdes().setIbPrev(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IB-PREV
            ws.getIndAdes().setIbPrev(((short)0));
        }
        // COB_CODE: IF ADE-IB-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IB-OGG
        //           ELSE
        //              MOVE 0 TO IND-ADE-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIbOgg(), Ades.Len.ADE_IB_OGG)) {
            // COB_CODE: MOVE -1 TO IND-ADE-IB-OGG
            ws.getIndAdes().setIbOgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IB-OGG
            ws.getIndAdes().setIbOgg(((short)0));
        }
        // COB_CODE: IF ADE-DT-DECOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-DECOR
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-DECOR
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtDecor().getAdeDtDecorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-DECOR
            ws.getIndAdes().setDtDecor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-DECOR
            ws.getIndAdes().setDtDecor(((short)0));
        }
        // COB_CODE: IF ADE-DT-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-SCAD
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtScad().getAdeDtScadNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-SCAD
            ws.getIndAdes().setDtScad(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-SCAD
            ws.getIndAdes().setDtScad(((short)0));
        }
        // COB_CODE: IF ADE-ETA-A-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-ETA-A-SCAD
        //           ELSE
        //              MOVE 0 TO IND-ADE-ETA-A-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeEtaAScad().getAdeEtaAScadNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-ETA-A-SCAD
            ws.getIndAdes().setEtaAScad(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-ETA-A-SCAD
            ws.getIndAdes().setEtaAScad(((short)0));
        }
        // COB_CODE: IF ADE-DUR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DUR-AA
        //           ELSE
        //              MOVE 0 TO IND-ADE-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDurAa().getAdeDurAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DUR-AA
            ws.getIndAdes().setDurAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DUR-AA
            ws.getIndAdes().setDurAa(((short)0));
        }
        // COB_CODE: IF ADE-DUR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DUR-MM
        //           ELSE
        //              MOVE 0 TO IND-ADE-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDurMm().getAdeDurMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DUR-MM
            ws.getIndAdes().setDurMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DUR-MM
            ws.getIndAdes().setDurMm(((short)0));
        }
        // COB_CODE: IF ADE-DUR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DUR-GG
        //           ELSE
        //              MOVE 0 TO IND-ADE-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDurGg().getAdeDurGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DUR-GG
            ws.getIndAdes().setDurGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DUR-GG
            ws.getIndAdes().setDurGg(((short)0));
        }
        // COB_CODE: IF ADE-TP-RIAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-TP-RIAT
        //           ELSE
        //              MOVE 0 TO IND-ADE-TP-RIAT
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeTpRiatFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-TP-RIAT
            ws.getIndAdes().setTpRiat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-TP-RIAT
            ws.getIndAdes().setTpRiat(((short)0));
        }
        // COB_CODE: IF ADE-TP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-TP-IAS
        //           ELSE
        //              MOVE 0 TO IND-ADE-TP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeTpIasFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-TP-IAS
            ws.getIndAdes().setTpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-TP-IAS
            ws.getIndAdes().setTpIas(((short)0));
        }
        // COB_CODE: IF ADE-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-VARZ-TP-IAS
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-VARZ-TP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtVarzTpIas().getAdeDtVarzTpIasNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-VARZ-TP-IAS
            ws.getIndAdes().setDtVarzTpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-VARZ-TP-IAS
            ws.getIndAdes().setDtVarzTpIas(((short)0));
        }
        // COB_CODE: IF ADE-PRE-NET-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PRE-NET-IND
        //           ELSE
        //              MOVE 0 TO IND-ADE-PRE-NET-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePreNetInd().getAdePreNetIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PRE-NET-IND
            ws.getIndAdes().setPreNetInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PRE-NET-IND
            ws.getIndAdes().setPreNetInd(((short)0));
        }
        // COB_CODE: IF ADE-PRE-LRD-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PRE-LRD-IND
        //           ELSE
        //              MOVE 0 TO IND-ADE-PRE-LRD-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePreLrdInd().getAdePreLrdIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PRE-LRD-IND
            ws.getIndAdes().setPreLrdInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PRE-LRD-IND
            ws.getIndAdes().setPreLrdInd(((short)0));
        }
        // COB_CODE: IF ADE-RAT-LRD-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-RAT-LRD-IND
        //           ELSE
        //              MOVE 0 TO IND-ADE-RAT-LRD-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeRatLrdInd().getAdeRatLrdIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-RAT-LRD-IND
            ws.getIndAdes().setRatLrdInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-RAT-LRD-IND
            ws.getIndAdes().setRatLrdInd(((short)0));
        }
        // COB_CODE: IF ADE-PRSTZ-INI-IND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PRSTZ-INI-IND
        //           ELSE
        //              MOVE 0 TO IND-ADE-PRSTZ-INI-IND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePrstzIniInd().getAdePrstzIniIndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PRSTZ-INI-IND
            ws.getIndAdes().setPrstzIniInd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PRSTZ-INI-IND
            ws.getIndAdes().setPrstzIniInd(((short)0));
        }
        // COB_CODE: IF ADE-FL-COINC-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-FL-COINC-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-ADE-FL-COINC-ASSTO
        //           END-IF
        if (Conditions.eq(ades.getAdeFlCoincAssto(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-FL-COINC-ASSTO
            ws.getIndAdes().setFlCoincAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-FL-COINC-ASSTO
            ws.getIndAdes().setFlCoincAssto(((short)0));
        }
        // COB_CODE: IF ADE-IB-DFLT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IB-DFLT
        //           ELSE
        //              MOVE 0 TO IND-ADE-IB-DFLT
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIbDflt(), Ades.Len.ADE_IB_DFLT)) {
            // COB_CODE: MOVE -1 TO IND-ADE-IB-DFLT
            ws.getIndAdes().setIbDflt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IB-DFLT
            ws.getIndAdes().setIbDflt(((short)0));
        }
        // COB_CODE: IF ADE-MOD-CALC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-MOD-CALC
        //           ELSE
        //              MOVE 0 TO IND-ADE-MOD-CALC
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeModCalcFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-MOD-CALC
            ws.getIndAdes().setModCalc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-MOD-CALC
            ws.getIndAdes().setModCalc(((short)0));
        }
        // COB_CODE: IF ADE-TP-FNT-CNBTVA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-TP-FNT-CNBTVA
        //           ELSE
        //              MOVE 0 TO IND-ADE-TP-FNT-CNBTVA
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeTpFntCnbtvaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-TP-FNT-CNBTVA
            ws.getIndAdes().setTpFntCnbtva(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-TP-FNT-CNBTVA
            ws.getIndAdes().setTpFntCnbtva(((short)0));
        }
        // COB_CODE: IF ADE-IMP-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-AZ
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpAz().getAdeImpAzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-AZ
            ws.getIndAdes().setImpAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-AZ
            ws.getIndAdes().setImpAz(((short)0));
        }
        // COB_CODE: IF ADE-IMP-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-ADER
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpAder().getAdeImpAderNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-ADER
            ws.getIndAdes().setImpAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-ADER
            ws.getIndAdes().setImpAder(((short)0));
        }
        // COB_CODE: IF ADE-IMP-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-TFR
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpTfr().getAdeImpTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-TFR
            ws.getIndAdes().setImpTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-TFR
            ws.getIndAdes().setImpTfr(((short)0));
        }
        // COB_CODE: IF ADE-IMP-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-VOLO
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpVolo().getAdeImpVoloNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-VOLO
            ws.getIndAdes().setImpVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-VOLO
            ws.getIndAdes().setImpVolo(((short)0));
        }
        // COB_CODE: IF ADE-PC-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PC-AZ
        //           ELSE
        //              MOVE 0 TO IND-ADE-PC-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePcAz().getAdePcAzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PC-AZ
            ws.getIndAdes().setPcAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PC-AZ
            ws.getIndAdes().setPcAz(((short)0));
        }
        // COB_CODE: IF ADE-PC-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PC-ADER
        //           ELSE
        //              MOVE 0 TO IND-ADE-PC-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePcAder().getAdePcAderNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PC-ADER
            ws.getIndAdes().setPcAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PC-ADER
            ws.getIndAdes().setPcAder(((short)0));
        }
        // COB_CODE: IF ADE-PC-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PC-TFR
        //           ELSE
        //              MOVE 0 TO IND-ADE-PC-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePcTfr().getAdePcTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PC-TFR
            ws.getIndAdes().setPcTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PC-TFR
            ws.getIndAdes().setPcTfr(((short)0));
        }
        // COB_CODE: IF ADE-PC-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-PC-VOLO
        //           ELSE
        //              MOVE 0 TO IND-ADE-PC-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdePcVolo().getAdePcVoloNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-PC-VOLO
            ws.getIndAdes().setPcVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-PC-VOLO
            ws.getIndAdes().setPcVolo(((short)0));
        }
        // COB_CODE: IF ADE-DT-NOVA-RGM-FISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-NOVA-RGM-FISC
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-NOVA-RGM-FISC
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtNovaRgmFisc().getAdeDtNovaRgmFiscNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-NOVA-RGM-FISC
            ws.getIndAdes().setDtNovaRgmFisc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-NOVA-RGM-FISC
            ws.getIndAdes().setDtNovaRgmFisc(((short)0));
        }
        // COB_CODE: IF ADE-FL-ATTIV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-FL-ATTIV
        //           ELSE
        //              MOVE 0 TO IND-ADE-FL-ATTIV
        //           END-IF
        if (Conditions.eq(ades.getAdeFlAttiv(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-FL-ATTIV
            ws.getIndAdes().setFlAttiv(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-FL-ATTIV
            ws.getIndAdes().setFlAttiv(((short)0));
        }
        // COB_CODE: IF ADE-IMP-REC-RIT-VIS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-REC-RIT-VIS
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-REC-RIT-VIS
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpRecRitVis().getAdeImpRecRitVisNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-REC-RIT-VIS
            ws.getIndAdes().setImpRecRitVis(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-REC-RIT-VIS
            ws.getIndAdes().setImpRecRitVis(((short)0));
        }
        // COB_CODE: IF ADE-IMP-REC-RIT-ACC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-REC-RIT-ACC
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-REC-RIT-ACC
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpRecRitAcc().getAdeImpRecRitAccNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-REC-RIT-ACC
            ws.getIndAdes().setImpRecRitAcc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-REC-RIT-ACC
            ws.getIndAdes().setImpRecRitAcc(((short)0));
        }
        // COB_CODE: IF ADE-FL-VARZ-STAT-TBGC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-FL-VARZ-STAT-TBGC
        //           ELSE
        //              MOVE 0 TO IND-ADE-FL-VARZ-STAT-TBGC
        //           END-IF
        if (Conditions.eq(ades.getAdeFlVarzStatTbgc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-FL-VARZ-STAT-TBGC
            ws.getIndAdes().setFlVarzStatTbgc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-FL-VARZ-STAT-TBGC
            ws.getIndAdes().setFlVarzStatTbgc(((short)0));
        }
        // COB_CODE: IF ADE-FL-PROVZA-MIGRAZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-FL-PROVZA-MIGRAZ
        //           ELSE
        //              MOVE 0 TO IND-ADE-FL-PROVZA-MIGRAZ
        //           END-IF
        if (Conditions.eq(ades.getAdeFlProvzaMigraz(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-FL-PROVZA-MIGRAZ
            ws.getIndAdes().setFlProvzaMigraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-FL-PROVZA-MIGRAZ
            ws.getIndAdes().setFlProvzaMigraz(((short)0));
        }
        // COB_CODE: IF ADE-IMPB-VIS-DA-REC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMPB-VIS-DA-REC
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMPB-VIS-DA-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpbVisDaRec().getAdeImpbVisDaRecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMPB-VIS-DA-REC
            ws.getIndAdes().setImpbVisDaRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMPB-VIS-DA-REC
            ws.getIndAdes().setImpbVisDaRec(((short)0));
        }
        // COB_CODE: IF ADE-DT-DECOR-PREST-BAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-DECOR-PREST-BAN
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-DECOR-PREST-BAN
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtDecorPrestBan().getAdeDtDecorPrestBanNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-DECOR-PREST-BAN
            ws.getIndAdes().setDtDecorPrestBan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-DECOR-PREST-BAN
            ws.getIndAdes().setDtDecorPrestBan(((short)0));
        }
        // COB_CODE: IF ADE-DT-EFF-VARZ-STAT-T-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-EFF-VARZ-STAT-T
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-EFF-VARZ-STAT-T
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtEffVarzStatT().getAdeDtEffVarzStatTNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-EFF-VARZ-STAT-T
            ws.getIndAdes().setDtEffVarzStatT(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-EFF-VARZ-STAT-T
            ws.getIndAdes().setDtEffVarzStatT(((short)0));
        }
        // COB_CODE: IF ADE-CUM-CNBT-CAP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-CUM-CNBT-CAP
        //           ELSE
        //              MOVE 0 TO IND-ADE-CUM-CNBT-CAP
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeCumCnbtCap().getAdeCumCnbtCapNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-CUM-CNBT-CAP
            ws.getIndAdes().setCumCnbtCap(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-CUM-CNBT-CAP
            ws.getIndAdes().setCumCnbtCap(((short)0));
        }
        // COB_CODE: IF ADE-IMP-GAR-CNBT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IMP-GAR-CNBT
        //           ELSE
        //              MOVE 0 TO IND-ADE-IMP-GAR-CNBT
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeImpGarCnbt().getAdeImpGarCnbtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-IMP-GAR-CNBT
            ws.getIndAdes().setImpGarCnbt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IMP-GAR-CNBT
            ws.getIndAdes().setImpGarCnbt(((short)0));
        }
        // COB_CODE: IF ADE-DT-ULT-CONS-CNBT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-ULT-CONS-CNBT
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-ULT-CONS-CNBT
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtUltConsCnbt().getAdeDtUltConsCnbtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-ULT-CONS-CNBT
            ws.getIndAdes().setDtUltConsCnbt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-ULT-CONS-CNBT
            ws.getIndAdes().setDtUltConsCnbt(((short)0));
        }
        // COB_CODE: IF ADE-IDEN-ISC-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-IDEN-ISC-FND
        //           ELSE
        //              MOVE 0 TO IND-ADE-IDEN-ISC-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeIdenIscFnd(), Ades.Len.ADE_IDEN_ISC_FND)) {
            // COB_CODE: MOVE -1 TO IND-ADE-IDEN-ISC-FND
            ws.getIndAdes().setIdenIscFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-IDEN-ISC-FND
            ws.getIndAdes().setIdenIscFnd(((short)0));
        }
        // COB_CODE: IF ADE-NUM-RAT-PIAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-NUM-RAT-PIAN
        //           ELSE
        //              MOVE 0 TO IND-ADE-NUM-RAT-PIAN
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeNumRatPian().getAdeNumRatPianNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-NUM-RAT-PIAN
            ws.getIndAdes().setNumRatPian(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-NUM-RAT-PIAN
            ws.getIndAdes().setNumRatPian(((short)0));
        }
        // COB_CODE: IF ADE-DT-PRESC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-DT-PRESC
        //           ELSE
        //              MOVE 0 TO IND-ADE-DT-PRESC
        //           END-IF
        if (Characters.EQ_HIGH.test(ades.getAdeDtPresc().getAdeDtPrescNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-ADE-DT-PRESC
            ws.getIndAdes().setDtPresc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-DT-PRESC
            ws.getIndAdes().setDtPresc(((short)0));
        }
        // COB_CODE: IF ADE-CONCS-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-ADE-CONCS-PREST
        //           ELSE
        //              MOVE 0 TO IND-ADE-CONCS-PREST
        //           END-IF.
        if (Conditions.eq(ades.getAdeConcsPrest(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-ADE-CONCS-PREST
            ws.getIndAdes().setConcsPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-ADE-CONCS-PREST
            ws.getIndAdes().setConcsPrest(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : ADE-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE ADES TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(ades.getAdesFormatted());
        // COB_CODE: MOVE ADE-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(ades.getAdeIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO ADE-ID-MOVI-CHIU
                ades.getAdeIdMoviChiu().setAdeIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO ADE-DS-TS-END-CPTZ
                ades.setAdeDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO ADE-ID-MOVI-CRZ
                    ades.setAdeIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO ADE-ID-MOVI-CHIU-NULL
                    ades.getAdeIdMoviChiu().setAdeIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeIdMoviChiu.Len.ADE_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO ADE-DT-END-EFF
                    ades.setAdeDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO ADE-DS-TS-INI-CPTZ
                    ades.setAdeDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO ADE-DS-TS-END-CPTZ
                    ades.setAdeDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE ADES TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(ades.getAdesFormatted());
        // COB_CODE: MOVE ADE-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(ades.getAdeIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO ADES.
        ades.setAdesFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO ADE-ID-MOVI-CRZ.
        ades.setAdeIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO ADE-ID-MOVI-CHIU-NULL.
        ades.getAdeIdMoviChiu().setAdeIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, AdeIdMoviChiu.Len.ADE_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO ADE-DT-INI-EFF.
        ades.setAdeDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO ADE-DT-END-EFF.
        ades.setAdeDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO ADE-DS-TS-INI-CPTZ.
        ades.setAdeDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO ADE-DS-TS-END-CPTZ.
        ades.setAdeDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO ADE-COD-COMP-ANIA.
        ades.setAdeCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE ADE-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-INI-EFF-DB
        ws.getAdesDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE ADE-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-END-EFF-DB
        ws.getAdesDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-ADE-DT-DECOR = 0
        //               MOVE WS-DATE-X      TO ADE-DT-DECOR-DB
        //           END-IF
        if (ws.getIndAdes().getDtDecor() == 0) {
            // COB_CODE: MOVE ADE-DT-DECOR TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtDecor().getAdeDtDecor(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-DECOR-DB
            ws.getAdesDb().setDecorDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-ADE-DT-SCAD = 0
        //               MOVE WS-DATE-X      TO ADE-DT-SCAD-DB
        //           END-IF
        if (ws.getIndAdes().getDtScad() == 0) {
            // COB_CODE: MOVE ADE-DT-SCAD TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtScad().getAdeDtScad(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-SCAD-DB
            ws.getAdesDb().setScadDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-ADE-DT-VARZ-TP-IAS = 0
        //               MOVE WS-DATE-X      TO ADE-DT-VARZ-TP-IAS-DB
        //           END-IF
        if (ws.getIndAdes().getDtVarzTpIas() == 0) {
            // COB_CODE: MOVE ADE-DT-VARZ-TP-IAS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtVarzTpIas().getAdeDtVarzTpIas(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-VARZ-TP-IAS-DB
            ws.getAdesDb().setVarzTpIasDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-ADE-DT-NOVA-RGM-FISC = 0
        //               MOVE WS-DATE-X      TO ADE-DT-NOVA-RGM-FISC-DB
        //           END-IF
        if (ws.getIndAdes().getDtNovaRgmFisc() == 0) {
            // COB_CODE: MOVE ADE-DT-NOVA-RGM-FISC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtNovaRgmFisc().getAdeDtNovaRgmFisc(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-NOVA-RGM-FISC-DB
            ws.getAdesDb().setNovaRgmFiscDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-ADE-DT-DECOR-PREST-BAN = 0
        //               MOVE WS-DATE-X      TO ADE-DT-DECOR-PREST-BAN-DB
        //           END-IF
        if (ws.getIndAdes().getDtDecorPrestBan() == 0) {
            // COB_CODE: MOVE ADE-DT-DECOR-PREST-BAN TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtDecorPrestBan().getAdeDtDecorPrestBan(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-DECOR-PREST-BAN-DB
            ws.getAdesDb().setDecorPrestBanDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
        //               MOVE WS-DATE-X      TO ADE-DT-EFF-VARZ-STAT-T-DB
        //           END-IF
        if (ws.getIndAdes().getDtEffVarzStatT() == 0) {
            // COB_CODE: MOVE ADE-DT-EFF-VARZ-STAT-T TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtEffVarzStatT().getAdeDtEffVarzStatT(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-EFF-VARZ-STAT-T-DB
            ws.getAdesDb().setEffVarzStatTDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-ADE-DT-ULT-CONS-CNBT = 0
        //               MOVE WS-DATE-X      TO ADE-DT-ULT-CONS-CNBT-DB
        //           END-IF
        if (ws.getIndAdes().getDtUltConsCnbt() == 0) {
            // COB_CODE: MOVE ADE-DT-ULT-CONS-CNBT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtUltConsCnbt().getAdeDtUltConsCnbt(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-ULT-CONS-CNBT-DB
            ws.getAdesDb().setUltConsCnbtDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-ADE-DT-PRESC = 0
        //               MOVE WS-DATE-X      TO ADE-DT-PRESC-DB
        //           END-IF.
        if (ws.getIndAdes().getDtPresc() == 0) {
            // COB_CODE: MOVE ADE-DT-PRESC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ades.getAdeDtPresc().getAdeDtPresc(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO ADE-DT-PRESC-DB
            ws.getAdesDb().setPrescDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE ADE-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getAdesDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-INI-EFF
        ades.setAdeDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE ADE-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getAdesDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-END-EFF
        ades.setAdeDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-ADE-DT-DECOR = 0
        //               MOVE WS-DATE-N      TO ADE-DT-DECOR
        //           END-IF
        if (ws.getIndAdes().getDtDecor() == 0) {
            // COB_CODE: MOVE ADE-DT-DECOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getDecorDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-DECOR
            ades.getAdeDtDecor().setAdeDtDecor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO ADE-DT-SCAD
        //           END-IF
        if (ws.getIndAdes().getDtScad() == 0) {
            // COB_CODE: MOVE ADE-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-SCAD
            ades.getAdeDtScad().setAdeDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-VARZ-TP-IAS = 0
        //               MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
        //           END-IF
        if (ws.getIndAdes().getDtVarzTpIas() == 0) {
            // COB_CODE: MOVE ADE-DT-VARZ-TP-IAS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getVarzTpIasDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-VARZ-TP-IAS
            ades.getAdeDtVarzTpIas().setAdeDtVarzTpIas(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-NOVA-RGM-FISC = 0
        //               MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
        //           END-IF
        if (ws.getIndAdes().getDtNovaRgmFisc() == 0) {
            // COB_CODE: MOVE ADE-DT-NOVA-RGM-FISC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getNovaRgmFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-NOVA-RGM-FISC
            ades.getAdeDtNovaRgmFisc().setAdeDtNovaRgmFisc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-DECOR-PREST-BAN = 0
        //               MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
        //           END-IF
        if (ws.getIndAdes().getDtDecorPrestBan() == 0) {
            // COB_CODE: MOVE ADE-DT-DECOR-PREST-BAN-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getDecorPrestBanDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-DECOR-PREST-BAN
            ades.getAdeDtDecorPrestBan().setAdeDtDecorPrestBan(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-EFF-VARZ-STAT-T = 0
        //               MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
        //           END-IF
        if (ws.getIndAdes().getDtEffVarzStatT() == 0) {
            // COB_CODE: MOVE ADE-DT-EFF-VARZ-STAT-T-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getEffVarzStatTDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-EFF-VARZ-STAT-T
            ades.getAdeDtEffVarzStatT().setAdeDtEffVarzStatT(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-ULT-CONS-CNBT = 0
        //               MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
        //           END-IF
        if (ws.getIndAdes().getDtUltConsCnbt() == 0) {
            // COB_CODE: MOVE ADE-DT-ULT-CONS-CNBT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getUltConsCnbtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-ULT-CONS-CNBT
            ades.getAdeDtUltConsCnbt().setAdeDtUltConsCnbt(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ADE-DT-PRESC = 0
        //               MOVE WS-DATE-N      TO ADE-DT-PRESC
        //           END-IF.
        if (ws.getIndAdes().getDtPresc() == 0) {
            // COB_CODE: MOVE ADE-DT-PRESC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getAdesDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ADE-DT-PRESC
            ades.getAdeDtPresc().setAdeDtPresc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public long getAdeDsRiga() {
        return ades.getAdeDsRiga();
    }

    @Override
    public void setAdeDsRiga(long adeDsRiga) {
        this.ades.setAdeDsRiga(adeDsRiga);
    }

    @Override
    public int getCodCompAnia() {
        return ades.getAdeCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.ades.setAdeCodCompAnia(codCompAnia);
    }

    @Override
    public char getConcsPrest() {
        return ades.getAdeConcsPrest();
    }

    @Override
    public void setConcsPrest(char concsPrest) {
        this.ades.setAdeConcsPrest(concsPrest);
    }

    @Override
    public Character getConcsPrestObj() {
        if (ws.getIndAdes().getConcsPrest() >= 0) {
            return ((Character)getConcsPrest());
        }
        else {
            return null;
        }
    }

    @Override
    public void setConcsPrestObj(Character concsPrestObj) {
        if (concsPrestObj != null) {
            setConcsPrest(((char)concsPrestObj));
            ws.getIndAdes().setConcsPrest(((short)0));
        }
        else {
            ws.getIndAdes().setConcsPrest(((short)-1));
        }
    }

    @Override
    public AfDecimal getCumCnbtCap() {
        return ades.getAdeCumCnbtCap().getAdeCumCnbtCap();
    }

    @Override
    public void setCumCnbtCap(AfDecimal cumCnbtCap) {
        this.ades.getAdeCumCnbtCap().setAdeCumCnbtCap(cumCnbtCap.copy());
    }

    @Override
    public AfDecimal getCumCnbtCapObj() {
        if (ws.getIndAdes().getCumCnbtCap() >= 0) {
            return getCumCnbtCap();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCumCnbtCapObj(AfDecimal cumCnbtCapObj) {
        if (cumCnbtCapObj != null) {
            setCumCnbtCap(new AfDecimal(cumCnbtCapObj, 15, 3));
            ws.getIndAdes().setCumCnbtCap(((short)0));
        }
        else {
            ws.getIndAdes().setCumCnbtCap(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return ades.getAdeDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.ades.setAdeDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return ades.getAdeDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.ades.setAdeDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return ades.getAdeDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.ades.setAdeDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return ades.getAdeDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.ades.setAdeDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return ades.getAdeDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.ades.setAdeDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return ades.getAdeDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.ades.setAdeDsVer(dsVer);
    }

    @Override
    public String getDtDecorDb() {
        return ws.getAdesDb().getDecorDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.ws.getAdesDb().setDecorDb(dtDecorDb);
    }

    @Override
    public String getDtDecorDbObj() {
        if (ws.getIndAdes().getDtDecor() >= 0) {
            return getDtDecorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorDbObj(String dtDecorDbObj) {
        if (dtDecorDbObj != null) {
            setDtDecorDb(dtDecorDbObj);
            ws.getIndAdes().setDtDecor(((short)0));
        }
        else {
            ws.getIndAdes().setDtDecor(((short)-1));
        }
    }

    @Override
    public String getDtDecorPrestBanDb() {
        return ws.getAdesDb().getDecorPrestBanDb();
    }

    @Override
    public void setDtDecorPrestBanDb(String dtDecorPrestBanDb) {
        this.ws.getAdesDb().setDecorPrestBanDb(dtDecorPrestBanDb);
    }

    @Override
    public String getDtDecorPrestBanDbObj() {
        if (ws.getIndAdes().getDtDecorPrestBan() >= 0) {
            return getDtDecorPrestBanDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorPrestBanDbObj(String dtDecorPrestBanDbObj) {
        if (dtDecorPrestBanDbObj != null) {
            setDtDecorPrestBanDb(dtDecorPrestBanDbObj);
            ws.getIndAdes().setDtDecorPrestBan(((short)0));
        }
        else {
            ws.getIndAdes().setDtDecorPrestBan(((short)-1));
        }
    }

    @Override
    public String getDtEffVarzStatTDb() {
        return ws.getAdesDb().getEffVarzStatTDb();
    }

    @Override
    public void setDtEffVarzStatTDb(String dtEffVarzStatTDb) {
        this.ws.getAdesDb().setEffVarzStatTDb(dtEffVarzStatTDb);
    }

    @Override
    public String getDtEffVarzStatTDbObj() {
        if (ws.getIndAdes().getDtEffVarzStatT() >= 0) {
            return getDtEffVarzStatTDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEffVarzStatTDbObj(String dtEffVarzStatTDbObj) {
        if (dtEffVarzStatTDbObj != null) {
            setDtEffVarzStatTDb(dtEffVarzStatTDbObj);
            ws.getIndAdes().setDtEffVarzStatT(((short)0));
        }
        else {
            ws.getIndAdes().setDtEffVarzStatT(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getAdesDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getAdesDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getAdesDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getAdesDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtNovaRgmFiscDb() {
        return ws.getAdesDb().getNovaRgmFiscDb();
    }

    @Override
    public void setDtNovaRgmFiscDb(String dtNovaRgmFiscDb) {
        this.ws.getAdesDb().setNovaRgmFiscDb(dtNovaRgmFiscDb);
    }

    @Override
    public String getDtNovaRgmFiscDbObj() {
        if (ws.getIndAdes().getDtNovaRgmFisc() >= 0) {
            return getDtNovaRgmFiscDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtNovaRgmFiscDbObj(String dtNovaRgmFiscDbObj) {
        if (dtNovaRgmFiscDbObj != null) {
            setDtNovaRgmFiscDb(dtNovaRgmFiscDbObj);
            ws.getIndAdes().setDtNovaRgmFisc(((short)0));
        }
        else {
            ws.getIndAdes().setDtNovaRgmFisc(((short)-1));
        }
    }

    @Override
    public String getDtPrescDb() {
        return ws.getAdesDb().getPrescDb();
    }

    @Override
    public void setDtPrescDb(String dtPrescDb) {
        this.ws.getAdesDb().setPrescDb(dtPrescDb);
    }

    @Override
    public String getDtPrescDbObj() {
        if (ws.getIndAdes().getDtPresc() >= 0) {
            return getDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPrescDbObj(String dtPrescDbObj) {
        if (dtPrescDbObj != null) {
            setDtPrescDb(dtPrescDbObj);
            ws.getIndAdes().setDtPresc(((short)0));
        }
        else {
            ws.getIndAdes().setDtPresc(((short)-1));
        }
    }

    @Override
    public String getDtScadDb() {
        return ws.getAdesDb().getScadDb();
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        this.ws.getAdesDb().setScadDb(dtScadDb);
    }

    @Override
    public String getDtScadDbObj() {
        if (ws.getIndAdes().getDtScad() >= 0) {
            return getDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        if (dtScadDbObj != null) {
            setDtScadDb(dtScadDbObj);
            ws.getIndAdes().setDtScad(((short)0));
        }
        else {
            ws.getIndAdes().setDtScad(((short)-1));
        }
    }

    @Override
    public String getDtUltConsCnbtDb() {
        return ws.getAdesDb().getUltConsCnbtDb();
    }

    @Override
    public void setDtUltConsCnbtDb(String dtUltConsCnbtDb) {
        this.ws.getAdesDb().setUltConsCnbtDb(dtUltConsCnbtDb);
    }

    @Override
    public String getDtUltConsCnbtDbObj() {
        if (ws.getIndAdes().getDtUltConsCnbt() >= 0) {
            return getDtUltConsCnbtDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtUltConsCnbtDbObj(String dtUltConsCnbtDbObj) {
        if (dtUltConsCnbtDbObj != null) {
            setDtUltConsCnbtDb(dtUltConsCnbtDbObj);
            ws.getIndAdes().setDtUltConsCnbt(((short)0));
        }
        else {
            ws.getIndAdes().setDtUltConsCnbt(((short)-1));
        }
    }

    @Override
    public String getDtVarzTpIasDb() {
        return ws.getAdesDb().getVarzTpIasDb();
    }

    @Override
    public void setDtVarzTpIasDb(String dtVarzTpIasDb) {
        this.ws.getAdesDb().setVarzTpIasDb(dtVarzTpIasDb);
    }

    @Override
    public String getDtVarzTpIasDbObj() {
        if (ws.getIndAdes().getDtVarzTpIas() >= 0) {
            return getDtVarzTpIasDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVarzTpIasDbObj(String dtVarzTpIasDbObj) {
        if (dtVarzTpIasDbObj != null) {
            setDtVarzTpIasDb(dtVarzTpIasDbObj);
            ws.getIndAdes().setDtVarzTpIas(((short)0));
        }
        else {
            ws.getIndAdes().setDtVarzTpIas(((short)-1));
        }
    }

    @Override
    public int getDurAa() {
        return ades.getAdeDurAa().getAdeDurAa();
    }

    @Override
    public void setDurAa(int durAa) {
        this.ades.getAdeDurAa().setAdeDurAa(durAa);
    }

    @Override
    public Integer getDurAaObj() {
        if (ws.getIndAdes().getDurAa() >= 0) {
            return ((Integer)getDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        if (durAaObj != null) {
            setDurAa(((int)durAaObj));
            ws.getIndAdes().setDurAa(((short)0));
        }
        else {
            ws.getIndAdes().setDurAa(((short)-1));
        }
    }

    @Override
    public int getDurGg() {
        return ades.getAdeDurGg().getAdeDurGg();
    }

    @Override
    public void setDurGg(int durGg) {
        this.ades.getAdeDurGg().setAdeDurGg(durGg);
    }

    @Override
    public Integer getDurGgObj() {
        if (ws.getIndAdes().getDurGg() >= 0) {
            return ((Integer)getDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        if (durGgObj != null) {
            setDurGg(((int)durGgObj));
            ws.getIndAdes().setDurGg(((short)0));
        }
        else {
            ws.getIndAdes().setDurGg(((short)-1));
        }
    }

    @Override
    public int getDurMm() {
        return ades.getAdeDurMm().getAdeDurMm();
    }

    @Override
    public void setDurMm(int durMm) {
        this.ades.getAdeDurMm().setAdeDurMm(durMm);
    }

    @Override
    public Integer getDurMmObj() {
        if (ws.getIndAdes().getDurMm() >= 0) {
            return ((Integer)getDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        if (durMmObj != null) {
            setDurMm(((int)durMmObj));
            ws.getIndAdes().setDurMm(((short)0));
        }
        else {
            ws.getIndAdes().setDurMm(((short)-1));
        }
    }

    @Override
    public int getEtaAScad() {
        return ades.getAdeEtaAScad().getAdeEtaAScad();
    }

    @Override
    public void setEtaAScad(int etaAScad) {
        this.ades.getAdeEtaAScad().setAdeEtaAScad(etaAScad);
    }

    @Override
    public Integer getEtaAScadObj() {
        if (ws.getIndAdes().getEtaAScad() >= 0) {
            return ((Integer)getEtaAScad());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAScadObj(Integer etaAScadObj) {
        if (etaAScadObj != null) {
            setEtaAScad(((int)etaAScadObj));
            ws.getIndAdes().setEtaAScad(((short)0));
        }
        else {
            ws.getIndAdes().setEtaAScad(((short)-1));
        }
    }

    @Override
    public char getFlAttiv() {
        return ades.getAdeFlAttiv();
    }

    @Override
    public void setFlAttiv(char flAttiv) {
        this.ades.setAdeFlAttiv(flAttiv);
    }

    @Override
    public Character getFlAttivObj() {
        if (ws.getIndAdes().getFlAttiv() >= 0) {
            return ((Character)getFlAttiv());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlAttivObj(Character flAttivObj) {
        if (flAttivObj != null) {
            setFlAttiv(((char)flAttivObj));
            ws.getIndAdes().setFlAttiv(((short)0));
        }
        else {
            ws.getIndAdes().setFlAttiv(((short)-1));
        }
    }

    @Override
    public char getFlCoincAssto() {
        return ades.getAdeFlCoincAssto();
    }

    @Override
    public void setFlCoincAssto(char flCoincAssto) {
        this.ades.setAdeFlCoincAssto(flCoincAssto);
    }

    @Override
    public Character getFlCoincAsstoObj() {
        if (ws.getIndAdes().getFlCoincAssto() >= 0) {
            return ((Character)getFlCoincAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCoincAsstoObj(Character flCoincAsstoObj) {
        if (flCoincAsstoObj != null) {
            setFlCoincAssto(((char)flCoincAsstoObj));
            ws.getIndAdes().setFlCoincAssto(((short)0));
        }
        else {
            ws.getIndAdes().setFlCoincAssto(((short)-1));
        }
    }

    @Override
    public char getFlProvzaMigraz() {
        return ades.getAdeFlProvzaMigraz();
    }

    @Override
    public void setFlProvzaMigraz(char flProvzaMigraz) {
        this.ades.setAdeFlProvzaMigraz(flProvzaMigraz);
    }

    @Override
    public Character getFlProvzaMigrazObj() {
        if (ws.getIndAdes().getFlProvzaMigraz() >= 0) {
            return ((Character)getFlProvzaMigraz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlProvzaMigrazObj(Character flProvzaMigrazObj) {
        if (flProvzaMigrazObj != null) {
            setFlProvzaMigraz(((char)flProvzaMigrazObj));
            ws.getIndAdes().setFlProvzaMigraz(((short)0));
        }
        else {
            ws.getIndAdes().setFlProvzaMigraz(((short)-1));
        }
    }

    @Override
    public char getFlVarzStatTbgc() {
        return ades.getAdeFlVarzStatTbgc();
    }

    @Override
    public void setFlVarzStatTbgc(char flVarzStatTbgc) {
        this.ades.setAdeFlVarzStatTbgc(flVarzStatTbgc);
    }

    @Override
    public Character getFlVarzStatTbgcObj() {
        if (ws.getIndAdes().getFlVarzStatTbgc() >= 0) {
            return ((Character)getFlVarzStatTbgc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlVarzStatTbgcObj(Character flVarzStatTbgcObj) {
        if (flVarzStatTbgcObj != null) {
            setFlVarzStatTbgc(((char)flVarzStatTbgcObj));
            ws.getIndAdes().setFlVarzStatTbgc(((short)0));
        }
        else {
            ws.getIndAdes().setFlVarzStatTbgc(((short)-1));
        }
    }

    @Override
    public char getIabv0002StateCurrent() {
        throw new FieldNotMappedException("iabv0002StateCurrent");
    }

    @Override
    public void setIabv0002StateCurrent(char iabv0002StateCurrent) {
        throw new FieldNotMappedException("iabv0002StateCurrent");
    }

    @Override
    public int getIabv0009Versioning() {
        throw new FieldNotMappedException("iabv0009Versioning");
    }

    @Override
    public void setIabv0009Versioning(int iabv0009Versioning) {
        throw new FieldNotMappedException("iabv0009Versioning");
    }

    @Override
    public String getIbDflt() {
        return ades.getAdeIbDflt();
    }

    @Override
    public void setIbDflt(String ibDflt) {
        this.ades.setAdeIbDflt(ibDflt);
    }

    @Override
    public String getIbDfltObj() {
        if (ws.getIndAdes().getIbDflt() >= 0) {
            return getIbDflt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbDfltObj(String ibDfltObj) {
        if (ibDfltObj != null) {
            setIbDflt(ibDfltObj);
            ws.getIndAdes().setIbDflt(((short)0));
        }
        else {
            ws.getIndAdes().setIbDflt(((short)-1));
        }
    }

    @Override
    public String getIbOgg() {
        return ades.getAdeIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.ades.setAdeIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndAdes().getIbOgg() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndAdes().setIbOgg(((short)0));
        }
        else {
            ws.getIndAdes().setIbOgg(((short)-1));
        }
    }

    @Override
    public String getIbPrev() {
        return ades.getAdeIbPrev();
    }

    @Override
    public void setIbPrev(String ibPrev) {
        this.ades.setAdeIbPrev(ibPrev);
    }

    @Override
    public String getIbPrevObj() {
        if (ws.getIndAdes().getIbPrev() >= 0) {
            return getIbPrev();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbPrevObj(String ibPrevObj) {
        if (ibPrevObj != null) {
            setIbPrev(ibPrevObj);
            ws.getIndAdes().setIbPrev(((short)0));
        }
        else {
            ws.getIndAdes().setIbPrev(((short)-1));
        }
    }

    @Override
    public int getIdAdes() {
        return ades.getAdeIdAdes();
    }

    @Override
    public void setIdAdes(int idAdes) {
        this.ades.setAdeIdAdes(idAdes);
    }

    @Override
    public int getIdMoviChiu() {
        return ades.getAdeIdMoviChiu().getAdeIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.ades.getAdeIdMoviChiu().setAdeIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndAdes().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndAdes().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndAdes().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return ades.getAdeIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.ades.setAdeIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdPoli() {
        return ades.getAdeIdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.ades.setAdeIdPoli(idPoli);
    }

    @Override
    public String getIdenIscFnd() {
        return ades.getAdeIdenIscFnd();
    }

    @Override
    public void setIdenIscFnd(String idenIscFnd) {
        this.ades.setAdeIdenIscFnd(idenIscFnd);
    }

    @Override
    public String getIdenIscFndObj() {
        if (ws.getIndAdes().getIdenIscFnd() >= 0) {
            return getIdenIscFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdenIscFndObj(String idenIscFndObj) {
        if (idenIscFndObj != null) {
            setIdenIscFnd(idenIscFndObj);
            ws.getIndAdes().setIdenIscFnd(((short)0));
        }
        else {
            ws.getIndAdes().setIdenIscFnd(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAder() {
        return ades.getAdeImpAder().getAdeImpAder();
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        this.ades.getAdeImpAder().setAdeImpAder(impAder.copy());
    }

    @Override
    public AfDecimal getImpAderObj() {
        if (ws.getIndAdes().getImpAder() >= 0) {
            return getImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        if (impAderObj != null) {
            setImpAder(new AfDecimal(impAderObj, 15, 3));
            ws.getIndAdes().setImpAder(((short)0));
        }
        else {
            ws.getIndAdes().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAz() {
        return ades.getAdeImpAz().getAdeImpAz();
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        this.ades.getAdeImpAz().setAdeImpAz(impAz.copy());
    }

    @Override
    public AfDecimal getImpAzObj() {
        if (ws.getIndAdes().getImpAz() >= 0) {
            return getImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        if (impAzObj != null) {
            setImpAz(new AfDecimal(impAzObj, 15, 3));
            ws.getIndAdes().setImpAz(((short)0));
        }
        else {
            ws.getIndAdes().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpGarCnbt() {
        return ades.getAdeImpGarCnbt().getAdeImpGarCnbt();
    }

    @Override
    public void setImpGarCnbt(AfDecimal impGarCnbt) {
        this.ades.getAdeImpGarCnbt().setAdeImpGarCnbt(impGarCnbt.copy());
    }

    @Override
    public AfDecimal getImpGarCnbtObj() {
        if (ws.getIndAdes().getImpGarCnbt() >= 0) {
            return getImpGarCnbt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpGarCnbtObj(AfDecimal impGarCnbtObj) {
        if (impGarCnbtObj != null) {
            setImpGarCnbt(new AfDecimal(impGarCnbtObj, 15, 3));
            ws.getIndAdes().setImpGarCnbt(((short)0));
        }
        else {
            ws.getIndAdes().setImpGarCnbt(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRecRitAcc() {
        return ades.getAdeImpRecRitAcc().getAdeImpRecRitAcc();
    }

    @Override
    public void setImpRecRitAcc(AfDecimal impRecRitAcc) {
        this.ades.getAdeImpRecRitAcc().setAdeImpRecRitAcc(impRecRitAcc.copy());
    }

    @Override
    public AfDecimal getImpRecRitAccObj() {
        if (ws.getIndAdes().getImpRecRitAcc() >= 0) {
            return getImpRecRitAcc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRecRitAccObj(AfDecimal impRecRitAccObj) {
        if (impRecRitAccObj != null) {
            setImpRecRitAcc(new AfDecimal(impRecRitAccObj, 15, 3));
            ws.getIndAdes().setImpRecRitAcc(((short)0));
        }
        else {
            ws.getIndAdes().setImpRecRitAcc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRecRitVis() {
        return ades.getAdeImpRecRitVis().getAdeImpRecRitVis();
    }

    @Override
    public void setImpRecRitVis(AfDecimal impRecRitVis) {
        this.ades.getAdeImpRecRitVis().setAdeImpRecRitVis(impRecRitVis.copy());
    }

    @Override
    public AfDecimal getImpRecRitVisObj() {
        if (ws.getIndAdes().getImpRecRitVis() >= 0) {
            return getImpRecRitVis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRecRitVisObj(AfDecimal impRecRitVisObj) {
        if (impRecRitVisObj != null) {
            setImpRecRitVis(new AfDecimal(impRecRitVisObj, 15, 3));
            ws.getIndAdes().setImpRecRitVis(((short)0));
        }
        else {
            ws.getIndAdes().setImpRecRitVis(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfr() {
        return ades.getAdeImpTfr().getAdeImpTfr();
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        this.ades.getAdeImpTfr().setAdeImpTfr(impTfr.copy());
    }

    @Override
    public AfDecimal getImpTfrObj() {
        if (ws.getIndAdes().getImpTfr() >= 0) {
            return getImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        if (impTfrObj != null) {
            setImpTfr(new AfDecimal(impTfrObj, 15, 3));
            ws.getIndAdes().setImpTfr(((short)0));
        }
        else {
            ws.getIndAdes().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpVolo() {
        return ades.getAdeImpVolo().getAdeImpVolo();
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        this.ades.getAdeImpVolo().setAdeImpVolo(impVolo.copy());
    }

    @Override
    public AfDecimal getImpVoloObj() {
        if (ws.getIndAdes().getImpVolo() >= 0) {
            return getImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        if (impVoloObj != null) {
            setImpVolo(new AfDecimal(impVoloObj, 15, 3));
            ws.getIndAdes().setImpVolo(((short)0));
        }
        else {
            ws.getIndAdes().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpbVisDaRec() {
        return ades.getAdeImpbVisDaRec().getAdeImpbVisDaRec();
    }

    @Override
    public void setImpbVisDaRec(AfDecimal impbVisDaRec) {
        this.ades.getAdeImpbVisDaRec().setAdeImpbVisDaRec(impbVisDaRec.copy());
    }

    @Override
    public AfDecimal getImpbVisDaRecObj() {
        if (ws.getIndAdes().getImpbVisDaRec() >= 0) {
            return getImpbVisDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbVisDaRecObj(AfDecimal impbVisDaRecObj) {
        if (impbVisDaRecObj != null) {
            setImpbVisDaRec(new AfDecimal(impbVisDaRecObj, 15, 3));
            ws.getIndAdes().setImpbVisDaRec(((short)0));
        }
        else {
            ws.getIndAdes().setImpbVisDaRec(((short)-1));
        }
    }

    @Override
    public String getModCalc() {
        return ades.getAdeModCalc();
    }

    @Override
    public void setModCalc(String modCalc) {
        this.ades.setAdeModCalc(modCalc);
    }

    @Override
    public String getModCalcObj() {
        if (ws.getIndAdes().getModCalc() >= 0) {
            return getModCalc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModCalcObj(String modCalcObj) {
        if (modCalcObj != null) {
            setModCalc(modCalcObj);
            ws.getIndAdes().setModCalc(((short)0));
        }
        else {
            ws.getIndAdes().setModCalc(((short)-1));
        }
    }

    @Override
    public AfDecimal getNumRatPian() {
        return ades.getAdeNumRatPian().getAdeNumRatPian();
    }

    @Override
    public void setNumRatPian(AfDecimal numRatPian) {
        this.ades.getAdeNumRatPian().setAdeNumRatPian(numRatPian.copy());
    }

    @Override
    public AfDecimal getNumRatPianObj() {
        if (ws.getIndAdes().getNumRatPian() >= 0) {
            return getNumRatPian();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumRatPianObj(AfDecimal numRatPianObj) {
        if (numRatPianObj != null) {
            setNumRatPian(new AfDecimal(numRatPianObj, 12, 5));
            ws.getIndAdes().setNumRatPian(((short)0));
        }
        else {
            ws.getIndAdes().setNumRatPian(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcAder() {
        return ades.getAdePcAder().getAdePcAder();
    }

    @Override
    public void setPcAder(AfDecimal pcAder) {
        this.ades.getAdePcAder().setAdePcAder(pcAder.copy());
    }

    @Override
    public AfDecimal getPcAderObj() {
        if (ws.getIndAdes().getPcAder() >= 0) {
            return getPcAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcAderObj(AfDecimal pcAderObj) {
        if (pcAderObj != null) {
            setPcAder(new AfDecimal(pcAderObj, 6, 3));
            ws.getIndAdes().setPcAder(((short)0));
        }
        else {
            ws.getIndAdes().setPcAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcAz() {
        return ades.getAdePcAz().getAdePcAz();
    }

    @Override
    public void setPcAz(AfDecimal pcAz) {
        this.ades.getAdePcAz().setAdePcAz(pcAz.copy());
    }

    @Override
    public AfDecimal getPcAzObj() {
        if (ws.getIndAdes().getPcAz() >= 0) {
            return getPcAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcAzObj(AfDecimal pcAzObj) {
        if (pcAzObj != null) {
            setPcAz(new AfDecimal(pcAzObj, 6, 3));
            ws.getIndAdes().setPcAz(((short)0));
        }
        else {
            ws.getIndAdes().setPcAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcTfr() {
        return ades.getAdePcTfr().getAdePcTfr();
    }

    @Override
    public void setPcTfr(AfDecimal pcTfr) {
        this.ades.getAdePcTfr().setAdePcTfr(pcTfr.copy());
    }

    @Override
    public AfDecimal getPcTfrObj() {
        if (ws.getIndAdes().getPcTfr() >= 0) {
            return getPcTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcTfrObj(AfDecimal pcTfrObj) {
        if (pcTfrObj != null) {
            setPcTfr(new AfDecimal(pcTfrObj, 6, 3));
            ws.getIndAdes().setPcTfr(((short)0));
        }
        else {
            ws.getIndAdes().setPcTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcVolo() {
        return ades.getAdePcVolo().getAdePcVolo();
    }

    @Override
    public void setPcVolo(AfDecimal pcVolo) {
        this.ades.getAdePcVolo().setAdePcVolo(pcVolo.copy());
    }

    @Override
    public AfDecimal getPcVoloObj() {
        if (ws.getIndAdes().getPcVolo() >= 0) {
            return getPcVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcVoloObj(AfDecimal pcVoloObj) {
        if (pcVoloObj != null) {
            setPcVolo(new AfDecimal(pcVoloObj, 6, 3));
            ws.getIndAdes().setPcVolo(((short)0));
        }
        else {
            ws.getIndAdes().setPcVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreLrdInd() {
        return ades.getAdePreLrdInd().getAdePreLrdInd();
    }

    @Override
    public void setPreLrdInd(AfDecimal preLrdInd) {
        this.ades.getAdePreLrdInd().setAdePreLrdInd(preLrdInd.copy());
    }

    @Override
    public AfDecimal getPreLrdIndObj() {
        if (ws.getIndAdes().getPreLrdInd() >= 0) {
            return getPreLrdInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreLrdIndObj(AfDecimal preLrdIndObj) {
        if (preLrdIndObj != null) {
            setPreLrdInd(new AfDecimal(preLrdIndObj, 15, 3));
            ws.getIndAdes().setPreLrdInd(((short)0));
        }
        else {
            ws.getIndAdes().setPreLrdInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreNetInd() {
        return ades.getAdePreNetInd().getAdePreNetInd();
    }

    @Override
    public void setPreNetInd(AfDecimal preNetInd) {
        this.ades.getAdePreNetInd().setAdePreNetInd(preNetInd.copy());
    }

    @Override
    public AfDecimal getPreNetIndObj() {
        if (ws.getIndAdes().getPreNetInd() >= 0) {
            return getPreNetInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreNetIndObj(AfDecimal preNetIndObj) {
        if (preNetIndObj != null) {
            setPreNetInd(new AfDecimal(preNetIndObj, 15, 3));
            ws.getIndAdes().setPreNetInd(((short)0));
        }
        else {
            ws.getIndAdes().setPreNetInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzIniInd() {
        return ades.getAdePrstzIniInd().getAdePrstzIniInd();
    }

    @Override
    public void setPrstzIniInd(AfDecimal prstzIniInd) {
        this.ades.getAdePrstzIniInd().setAdePrstzIniInd(prstzIniInd.copy());
    }

    @Override
    public AfDecimal getPrstzIniIndObj() {
        if (ws.getIndAdes().getPrstzIniInd() >= 0) {
            return getPrstzIniInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzIniIndObj(AfDecimal prstzIniIndObj) {
        if (prstzIniIndObj != null) {
            setPrstzIniInd(new AfDecimal(prstzIniIndObj, 15, 3));
            ws.getIndAdes().setPrstzIniInd(((short)0));
        }
        else {
            ws.getIndAdes().setPrstzIniInd(((short)-1));
        }
    }

    @Override
    public AfDecimal getRatLrdInd() {
        return ades.getAdeRatLrdInd().getAdeRatLrdInd();
    }

    @Override
    public void setRatLrdInd(AfDecimal ratLrdInd) {
        this.ades.getAdeRatLrdInd().setAdeRatLrdInd(ratLrdInd.copy());
    }

    @Override
    public AfDecimal getRatLrdIndObj() {
        if (ws.getIndAdes().getRatLrdInd() >= 0) {
            return getRatLrdInd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRatLrdIndObj(AfDecimal ratLrdIndObj) {
        if (ratLrdIndObj != null) {
            setRatLrdInd(new AfDecimal(ratLrdIndObj, 15, 3));
            ws.getIndAdes().setRatLrdInd(((short)0));
        }
        else {
            ws.getIndAdes().setRatLrdInd(((short)-1));
        }
    }

    @Override
    public String getTpFntCnbtva() {
        return ades.getAdeTpFntCnbtva();
    }

    @Override
    public void setTpFntCnbtva(String tpFntCnbtva) {
        this.ades.setAdeTpFntCnbtva(tpFntCnbtva);
    }

    @Override
    public String getTpFntCnbtvaObj() {
        if (ws.getIndAdes().getTpFntCnbtva() >= 0) {
            return getTpFntCnbtva();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpFntCnbtvaObj(String tpFntCnbtvaObj) {
        if (tpFntCnbtvaObj != null) {
            setTpFntCnbtva(tpFntCnbtvaObj);
            ws.getIndAdes().setTpFntCnbtva(((short)0));
        }
        else {
            ws.getIndAdes().setTpFntCnbtva(((short)-1));
        }
    }

    @Override
    public String getTpIas() {
        return ades.getAdeTpIas();
    }

    @Override
    public void setTpIas(String tpIas) {
        this.ades.setAdeTpIas(tpIas);
    }

    @Override
    public String getTpIasObj() {
        if (ws.getIndAdes().getTpIas() >= 0) {
            return getTpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpIasObj(String tpIasObj) {
        if (tpIasObj != null) {
            setTpIas(tpIasObj);
            ws.getIndAdes().setTpIas(((short)0));
        }
        else {
            ws.getIndAdes().setTpIas(((short)-1));
        }
    }

    @Override
    public String getTpModPagTit() {
        return ades.getAdeTpModPagTit();
    }

    @Override
    public void setTpModPagTit(String tpModPagTit) {
        this.ades.setAdeTpModPagTit(tpModPagTit);
    }

    @Override
    public String getTpRgmFisc() {
        return ades.getAdeTpRgmFisc();
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        this.ades.setAdeTpRgmFisc(tpRgmFisc);
    }

    @Override
    public String getTpRiat() {
        return ades.getAdeTpRiat();
    }

    @Override
    public void setTpRiat(String tpRiat) {
        this.ades.setAdeTpRiat(tpRiat);
    }

    @Override
    public String getTpRiatObj() {
        if (ws.getIndAdes().getTpRiat() >= 0) {
            return getTpRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRiatObj(String tpRiatObj) {
        if (tpRiatObj != null) {
            setTpRiat(tpRiatObj);
            ws.getIndAdes().setTpRiat(((short)0));
        }
        else {
            ws.getIndAdes().setTpRiat(((short)-1));
        }
    }
}
