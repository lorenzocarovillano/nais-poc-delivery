package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ValAstDao;
import it.accenture.jnais.commons.data.to.IValAst;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs4910Data;
import it.accenture.jnais.ws.redefines.VasDtValzz;
import it.accenture.jnais.ws.redefines.VasDtValzzCalc;
import it.accenture.jnais.ws.redefines.VasIdMoviChiu;
import it.accenture.jnais.ws.redefines.VasIdRichDisFnd;
import it.accenture.jnais.ws.redefines.VasIdRichInvstFnd;
import it.accenture.jnais.ws.redefines.VasIdTrchDiGar;
import it.accenture.jnais.ws.redefines.VasImpMovto;
import it.accenture.jnais.ws.redefines.VasMinusValenza;
import it.accenture.jnais.ws.redefines.VasNumQuo;
import it.accenture.jnais.ws.redefines.VasNumQuoLorde;
import it.accenture.jnais.ws.redefines.VasPcInvDis;
import it.accenture.jnais.ws.redefines.VasPlusValenza;
import it.accenture.jnais.ws.redefines.VasPreMovto;
import it.accenture.jnais.ws.redefines.VasValQuo;
import it.accenture.jnais.ws.ValAstLdbs4910;

/**Original name: LDBS4910<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs4910 extends Program implements IValAst {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ValAstDao valAstDao = new ValAstDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs4910Data ws = new Ldbs4910Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: VAL-AST
    private ValAstLdbs4910 valAst;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS4910_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ValAstLdbs4910 valAst) {
        this.idsv0003 = idsv0003;
        this.valAst = valAst;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBV4911.
        ws.getLdbv4911().setLdbv4911Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBV4911 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbv4911().getLdbv4911Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs4910 getInstance() {
        return ((Ldbs4910)Programs.getInstance(Ldbs4910.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS4910'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS4910");
        // COB_CODE: MOVE 'VAL-AST' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("VAL-AST");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-EFF CURSOR FOR
        //              SELECT
        //                     ID_VAL_AST
        //                    ,ID_TRCH_DI_GAR
        //                    ,ID_MOVI_FINRIO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_FND
        //                    ,NUM_QUO
        //                    ,VAL_QUO
        //                    ,DT_VALZZ
        //                    ,TP_VAL_AST
        //                    ,ID_RICH_INVST_FND
        //                    ,ID_RICH_DIS_FND
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,PRE_MOVTO
        //                    ,IMP_MOVTO
        //                    ,PC_INV_DIS
        //                    ,NUM_QUO_LORDE
        //                    ,DT_VALZZ_CALC
        //                    ,MINUS_VALENZA
        //                    ,PLUS_VALENZA
        //              FROM VAL_AST
        //              WHERE  ID_TRCH_DI_GAR =  :LDBV4911-ID-TRCH-DI-GAR
        //                    AND TP_VAL_AST NOT IN (:LDBV4911-TP-VAL-AST-1,
        //                                           :LDBV4911-TP-VAL-AST-2,
        //                                           :LDBV4911-TP-VAL-AST-3,
        //                                           :LDBV4911-TP-VAL-AST-4,
        //                                           :LDBV4911-TP-VAL-AST-5,
        //                                           :LDBV4911-TP-VAL-AST-6,
        //                                           :LDBV4911-TP-VAL-AST-7,
        //                                           :LDBV4911-TP-VAL-AST-8,
        //                                           :LDBV4911-TP-VAL-AST-9,
        //                                           :LDBV4911-TP-VAL-AST-10)
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_VAL_AST
        //                    ,ID_TRCH_DI_GAR
        //                    ,ID_MOVI_FINRIO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_FND
        //                    ,NUM_QUO
        //                    ,VAL_QUO
        //                    ,DT_VALZZ
        //                    ,TP_VAL_AST
        //                    ,ID_RICH_INVST_FND
        //                    ,ID_RICH_DIS_FND
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,PRE_MOVTO
        //                    ,IMP_MOVTO
        //                    ,PC_INV_DIS
        //                    ,NUM_QUO_LORDE
        //                    ,DT_VALZZ_CALC
        //                    ,MINUS_VALENZA
        //                    ,PLUS_VALENZA
        //             INTO
        //                :VAS-ID-VAL-AST
        //               ,:VAS-ID-TRCH-DI-GAR
        //                :IND-VAS-ID-TRCH-DI-GAR
        //               ,:VAS-ID-MOVI-FINRIO
        //               ,:VAS-ID-MOVI-CRZ
        //               ,:VAS-ID-MOVI-CHIU
        //                :IND-VAS-ID-MOVI-CHIU
        //               ,:VAS-DT-INI-EFF-DB
        //               ,:VAS-DT-END-EFF-DB
        //               ,:VAS-COD-COMP-ANIA
        //               ,:VAS-COD-FND
        //                :IND-VAS-COD-FND
        //               ,:VAS-NUM-QUO
        //                :IND-VAS-NUM-QUO
        //               ,:VAS-VAL-QUO
        //                :IND-VAS-VAL-QUO
        //               ,:VAS-DT-VALZZ-DB
        //                :IND-VAS-DT-VALZZ
        //               ,:VAS-TP-VAL-AST
        //               ,:VAS-ID-RICH-INVST-FND
        //                :IND-VAS-ID-RICH-INVST-FND
        //               ,:VAS-ID-RICH-DIS-FND
        //                :IND-VAS-ID-RICH-DIS-FND
        //               ,:VAS-DS-RIGA
        //               ,:VAS-DS-OPER-SQL
        //               ,:VAS-DS-VER
        //               ,:VAS-DS-TS-INI-CPTZ
        //               ,:VAS-DS-TS-END-CPTZ
        //               ,:VAS-DS-UTENTE
        //               ,:VAS-DS-STATO-ELAB
        //               ,:VAS-PRE-MOVTO
        //                :IND-VAS-PRE-MOVTO
        //               ,:VAS-IMP-MOVTO
        //                :IND-VAS-IMP-MOVTO
        //               ,:VAS-PC-INV-DIS
        //                :IND-VAS-PC-INV-DIS
        //               ,:VAS-NUM-QUO-LORDE
        //                :IND-VAS-NUM-QUO-LORDE
        //               ,:VAS-DT-VALZZ-CALC-DB
        //                :IND-VAS-DT-VALZZ-CALC
        //               ,:VAS-MINUS-VALENZA
        //                :IND-VAS-MINUS-VALENZA
        //               ,:VAS-PLUS-VALENZA
        //                :IND-VAS-PLUS-VALENZA
        //             FROM VAL_AST
        //             WHERE  ID_TRCH_DI_GAR =  :LDBV4911-ID-TRCH-DI-GAR
        //                    AND TP_VAL_AST NOT IN (:LDBV4911-TP-VAL-AST-1,
        //                                           :LDBV4911-TP-VAL-AST-2,
        //                                           :LDBV4911-TP-VAL-AST-3,
        //                                           :LDBV4911-TP-VAL-AST-4,
        //                                           :LDBV4911-TP-VAL-AST-5,
        //                                           :LDBV4911-TP-VAL-AST-6,
        //                                           :LDBV4911-TP-VAL-AST-7,
        //                                           :LDBV4911-TP-VAL-AST-8,
        //                                           :LDBV4911-TP-VAL-AST-9,
        //                                           :LDBV4911-TP-VAL-AST-10)
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        valAstDao.selectRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-EFF
        //           END-EXEC.
        valAstDao.openCEff24(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-EFF
        //           END-EXEC.
        valAstDao.closeCEff24();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-EFF
        //           INTO
        //                :VAS-ID-VAL-AST
        //               ,:VAS-ID-TRCH-DI-GAR
        //                :IND-VAS-ID-TRCH-DI-GAR
        //               ,:VAS-ID-MOVI-FINRIO
        //               ,:VAS-ID-MOVI-CRZ
        //               ,:VAS-ID-MOVI-CHIU
        //                :IND-VAS-ID-MOVI-CHIU
        //               ,:VAS-DT-INI-EFF-DB
        //               ,:VAS-DT-END-EFF-DB
        //               ,:VAS-COD-COMP-ANIA
        //               ,:VAS-COD-FND
        //                :IND-VAS-COD-FND
        //               ,:VAS-NUM-QUO
        //                :IND-VAS-NUM-QUO
        //               ,:VAS-VAL-QUO
        //                :IND-VAS-VAL-QUO
        //               ,:VAS-DT-VALZZ-DB
        //                :IND-VAS-DT-VALZZ
        //               ,:VAS-TP-VAL-AST
        //               ,:VAS-ID-RICH-INVST-FND
        //                :IND-VAS-ID-RICH-INVST-FND
        //               ,:VAS-ID-RICH-DIS-FND
        //                :IND-VAS-ID-RICH-DIS-FND
        //               ,:VAS-DS-RIGA
        //               ,:VAS-DS-OPER-SQL
        //               ,:VAS-DS-VER
        //               ,:VAS-DS-TS-INI-CPTZ
        //               ,:VAS-DS-TS-END-CPTZ
        //               ,:VAS-DS-UTENTE
        //               ,:VAS-DS-STATO-ELAB
        //               ,:VAS-PRE-MOVTO
        //                :IND-VAS-PRE-MOVTO
        //               ,:VAS-IMP-MOVTO
        //                :IND-VAS-IMP-MOVTO
        //               ,:VAS-PC-INV-DIS
        //                :IND-VAS-PC-INV-DIS
        //               ,:VAS-NUM-QUO-LORDE
        //                :IND-VAS-NUM-QUO-LORDE
        //               ,:VAS-DT-VALZZ-CALC-DB
        //                :IND-VAS-DT-VALZZ-CALC
        //               ,:VAS-MINUS-VALENZA
        //                :IND-VAS-MINUS-VALENZA
        //               ,:VAS-PLUS-VALENZA
        //                :IND-VAS-PLUS-VALENZA
        //           END-EXEC.
        valAstDao.fetchCEff24(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF THRU A270-EX
            a270CloseCursorWcEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-CPZ CURSOR FOR
        //              SELECT
        //                     ID_VAL_AST
        //                    ,ID_TRCH_DI_GAR
        //                    ,ID_MOVI_FINRIO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_FND
        //                    ,NUM_QUO
        //                    ,VAL_QUO
        //                    ,DT_VALZZ
        //                    ,TP_VAL_AST
        //                    ,ID_RICH_INVST_FND
        //                    ,ID_RICH_DIS_FND
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,PRE_MOVTO
        //                    ,IMP_MOVTO
        //                    ,PC_INV_DIS
        //                    ,NUM_QUO_LORDE
        //                    ,DT_VALZZ_CALC
        //                    ,MINUS_VALENZA
        //                    ,PLUS_VALENZA
        //              FROM VAL_AST
        //              WHERE  ID_TRCH_DI_GAR =  :LDBV4911-ID-TRCH-DI-GAR
        //                        AND TP_VAL_AST NOT IN (:LDBV4911-TP-VAL-AST-1,
        //                                               :LDBV4911-TP-VAL-AST-2,
        //                                               :LDBV4911-TP-VAL-AST-3,
        //                                               :LDBV4911-TP-VAL-AST-4,
        //                                               :LDBV4911-TP-VAL-AST-5,
        //                                               :LDBV4911-TP-VAL-AST-6,
        //                                               :LDBV4911-TP-VAL-AST-7,
        //                                               :LDBV4911-TP-VAL-AST-8,
        //                                               :LDBV4911-TP-VAL-AST-9,
        //                                               :LDBV4911-TP-VAL-AST-10)
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_VAL_AST
        //                    ,ID_TRCH_DI_GAR
        //                    ,ID_MOVI_FINRIO
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_FND
        //                    ,NUM_QUO
        //                    ,VAL_QUO
        //                    ,DT_VALZZ
        //                    ,TP_VAL_AST
        //                    ,ID_RICH_INVST_FND
        //                    ,ID_RICH_DIS_FND
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,PRE_MOVTO
        //                    ,IMP_MOVTO
        //                    ,PC_INV_DIS
        //                    ,NUM_QUO_LORDE
        //                    ,DT_VALZZ_CALC
        //                    ,MINUS_VALENZA
        //                    ,PLUS_VALENZA
        //             INTO
        //                :VAS-ID-VAL-AST
        //               ,:VAS-ID-TRCH-DI-GAR
        //                :IND-VAS-ID-TRCH-DI-GAR
        //               ,:VAS-ID-MOVI-FINRIO
        //               ,:VAS-ID-MOVI-CRZ
        //               ,:VAS-ID-MOVI-CHIU
        //                :IND-VAS-ID-MOVI-CHIU
        //               ,:VAS-DT-INI-EFF-DB
        //               ,:VAS-DT-END-EFF-DB
        //               ,:VAS-COD-COMP-ANIA
        //               ,:VAS-COD-FND
        //                :IND-VAS-COD-FND
        //               ,:VAS-NUM-QUO
        //                :IND-VAS-NUM-QUO
        //               ,:VAS-VAL-QUO
        //                :IND-VAS-VAL-QUO
        //               ,:VAS-DT-VALZZ-DB
        //                :IND-VAS-DT-VALZZ
        //               ,:VAS-TP-VAL-AST
        //               ,:VAS-ID-RICH-INVST-FND
        //                :IND-VAS-ID-RICH-INVST-FND
        //               ,:VAS-ID-RICH-DIS-FND
        //                :IND-VAS-ID-RICH-DIS-FND
        //               ,:VAS-DS-RIGA
        //               ,:VAS-DS-OPER-SQL
        //               ,:VAS-DS-VER
        //               ,:VAS-DS-TS-INI-CPTZ
        //               ,:VAS-DS-TS-END-CPTZ
        //               ,:VAS-DS-UTENTE
        //               ,:VAS-DS-STATO-ELAB
        //               ,:VAS-PRE-MOVTO
        //                :IND-VAS-PRE-MOVTO
        //               ,:VAS-IMP-MOVTO
        //                :IND-VAS-IMP-MOVTO
        //               ,:VAS-PC-INV-DIS
        //                :IND-VAS-PC-INV-DIS
        //               ,:VAS-NUM-QUO-LORDE
        //                :IND-VAS-NUM-QUO-LORDE
        //               ,:VAS-DT-VALZZ-CALC-DB
        //                :IND-VAS-DT-VALZZ-CALC
        //               ,:VAS-MINUS-VALENZA
        //                :IND-VAS-MINUS-VALENZA
        //               ,:VAS-PLUS-VALENZA
        //                :IND-VAS-PLUS-VALENZA
        //             FROM VAL_AST
        //             WHERE  ID_TRCH_DI_GAR =  :LDBV4911-ID-TRCH-DI-GAR
        //                    AND TP_VAL_AST NOT IN (:LDBV4911-TP-VAL-AST-1,
        //                                           :LDBV4911-TP-VAL-AST-2,
        //                                           :LDBV4911-TP-VAL-AST-3,
        //                                           :LDBV4911-TP-VAL-AST-4,
        //                                           :LDBV4911-TP-VAL-AST-5,
        //                                           :LDBV4911-TP-VAL-AST-6,
        //                                           :LDBV4911-TP-VAL-AST-7,
        //                                           :LDBV4911-TP-VAL-AST-8,
        //                                           :LDBV4911-TP-VAL-AST-9,
        //                                           :LDBV4911-TP-VAL-AST-10)
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        valAstDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-CPZ
        //           END-EXEC.
        valAstDao.openCCpz24(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-CPZ
        //           END-EXEC.
        valAstDao.closeCCpz24();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-CPZ
        //           INTO
        //                :VAS-ID-VAL-AST
        //               ,:VAS-ID-TRCH-DI-GAR
        //                :IND-VAS-ID-TRCH-DI-GAR
        //               ,:VAS-ID-MOVI-FINRIO
        //               ,:VAS-ID-MOVI-CRZ
        //               ,:VAS-ID-MOVI-CHIU
        //                :IND-VAS-ID-MOVI-CHIU
        //               ,:VAS-DT-INI-EFF-DB
        //               ,:VAS-DT-END-EFF-DB
        //               ,:VAS-COD-COMP-ANIA
        //               ,:VAS-COD-FND
        //                :IND-VAS-COD-FND
        //               ,:VAS-NUM-QUO
        //                :IND-VAS-NUM-QUO
        //               ,:VAS-VAL-QUO
        //                :IND-VAS-VAL-QUO
        //               ,:VAS-DT-VALZZ-DB
        //                :IND-VAS-DT-VALZZ
        //               ,:VAS-TP-VAL-AST
        //               ,:VAS-ID-RICH-INVST-FND
        //                :IND-VAS-ID-RICH-INVST-FND
        //               ,:VAS-ID-RICH-DIS-FND
        //                :IND-VAS-ID-RICH-DIS-FND
        //               ,:VAS-DS-RIGA
        //               ,:VAS-DS-OPER-SQL
        //               ,:VAS-DS-VER
        //               ,:VAS-DS-TS-INI-CPTZ
        //               ,:VAS-DS-TS-END-CPTZ
        //               ,:VAS-DS-UTENTE
        //               ,:VAS-DS-STATO-ELAB
        //               ,:VAS-PRE-MOVTO
        //                :IND-VAS-PRE-MOVTO
        //               ,:VAS-IMP-MOVTO
        //                :IND-VAS-IMP-MOVTO
        //               ,:VAS-PC-INV-DIS
        //                :IND-VAS-PC-INV-DIS
        //               ,:VAS-NUM-QUO-LORDE
        //                :IND-VAS-NUM-QUO-LORDE
        //               ,:VAS-DT-VALZZ-CALC-DB
        //                :IND-VAS-DT-VALZZ-CALC
        //               ,:VAS-MINUS-VALENZA
        //                :IND-VAS-MINUS-VALENZA
        //               ,:VAS-PLUS-VALENZA
        //                :IND-VAS-PLUS-VALENZA
        //           END-EXEC.
        valAstDao.fetchCCpz24(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ THRU B270-EX
            b270CloseCursorWcCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-VAS-ID-TRCH-DI-GAR = -1
        //              MOVE HIGH-VALUES TO VAS-ID-TRCH-DI-GAR-NULL
        //           END-IF
        if (ws.getIndValAst().getIdOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-ID-TRCH-DI-GAR-NULL
            valAst.getVasIdTrchDiGar().setVasIdTrchDiGarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasIdTrchDiGar.Len.VAS_ID_TRCH_DI_GAR_NULL));
        }
        // COB_CODE: IF IND-VAS-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO VAS-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndValAst().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-ID-MOVI-CHIU-NULL
            valAst.getVasIdMoviChiu().setVasIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasIdMoviChiu.Len.VAS_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-VAS-COD-FND = -1
        //              MOVE HIGH-VALUES TO VAS-COD-FND-NULL
        //           END-IF
        if (ws.getIndValAst().getDtIniPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-COD-FND-NULL
            valAst.setVasCodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ValAstLdbs4910.Len.VAS_COD_FND));
        }
        // COB_CODE: IF IND-VAS-NUM-QUO = -1
        //              MOVE HIGH-VALUES TO VAS-NUM-QUO-NULL
        //           END-IF
        if (ws.getIndValAst().getDtEndPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-NUM-QUO-NULL
            valAst.getVasNumQuo().setVasNumQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasNumQuo.Len.VAS_NUM_QUO_NULL));
        }
        // COB_CODE: IF IND-VAS-VAL-QUO = -1
        //              MOVE HIGH-VALUES TO VAS-VAL-QUO-NULL
        //           END-IF
        if (ws.getIndValAst().getImpstSost() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-VAL-QUO-NULL
            valAst.getVasValQuo().setVasValQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasValQuo.Len.VAS_VAL_QUO_NULL));
        }
        // COB_CODE: IF IND-VAS-DT-VALZZ = -1
        //              MOVE HIGH-VALUES TO VAS-DT-VALZZ-NULL
        //           END-IF
        if (ws.getIndValAst().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-DT-VALZZ-NULL
            valAst.getVasDtValzz().setVasDtValzzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasDtValzz.Len.VAS_DT_VALZZ_NULL));
        }
        // COB_CODE: IF IND-VAS-ID-RICH-INVST-FND = -1
        //              MOVE HIGH-VALUES TO VAS-ID-RICH-INVST-FND-NULL
        //           END-IF
        if (ws.getIndValAst().getAlqIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-ID-RICH-INVST-FND-NULL
            valAst.getVasIdRichInvstFnd().setVasIdRichInvstFndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasIdRichInvstFnd.Len.VAS_ID_RICH_INVST_FND_NULL));
        }
        // COB_CODE: IF IND-VAS-ID-RICH-DIS-FND = -1
        //              MOVE HIGH-VALUES TO VAS-ID-RICH-DIS-FND-NULL
        //           END-IF
        if (ws.getIndValAst().getCodTrb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-ID-RICH-DIS-FND-NULL
            valAst.getVasIdRichDisFnd().setVasIdRichDisFndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasIdRichDisFnd.Len.VAS_ID_RICH_DIS_FND_NULL));
        }
        // COB_CODE: IF IND-VAS-PRE-MOVTO = -1
        //              MOVE HIGH-VALUES TO VAS-PRE-MOVTO-NULL
        //           END-IF
        if (ws.getIndValAst().getPrstzLrdAnteIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-PRE-MOVTO-NULL
            valAst.getVasPreMovto().setVasPreMovtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasPreMovto.Len.VAS_PRE_MOVTO_NULL));
        }
        // COB_CODE: IF IND-VAS-IMP-MOVTO = -1
        //              MOVE HIGH-VALUES TO VAS-IMP-MOVTO-NULL
        //           END-IF
        if (ws.getIndValAst().getRisMatNetPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-IMP-MOVTO-NULL
            valAst.getVasImpMovto().setVasImpMovtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasImpMovto.Len.VAS_IMP_MOVTO_NULL));
        }
        // COB_CODE: IF IND-VAS-PC-INV-DIS = -1
        //              MOVE HIGH-VALUES TO VAS-PC-INV-DIS-NULL
        //           END-IF
        if (ws.getIndValAst().getRisMatAnteTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-PC-INV-DIS-NULL
            valAst.getVasPcInvDis().setVasPcInvDisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasPcInvDis.Len.VAS_PC_INV_DIS_NULL));
        }
        // COB_CODE: IF IND-VAS-NUM-QUO-LORDE = -1
        //              MOVE HIGH-VALUES TO VAS-NUM-QUO-LORDE-NULL
        //           END-IF
        if (ws.getIndValAst().getRisMatPostTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-NUM-QUO-LORDE-NULL
            valAst.getVasNumQuoLorde().setVasNumQuoLordeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasNumQuoLorde.Len.VAS_NUM_QUO_LORDE_NULL));
        }
        // COB_CODE: IF IND-VAS-DT-VALZZ-CALC = -1
        //              MOVE HIGH-VALUES TO VAS-DT-VALZZ-CALC-NULL
        //           END-IF
        if (ws.getIndValAst().getPrstzNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-DT-VALZZ-CALC-NULL
            valAst.getVasDtValzzCalc().setVasDtValzzCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasDtValzzCalc.Len.VAS_DT_VALZZ_CALC_NULL));
        }
        // COB_CODE: IF IND-VAS-MINUS-VALENZA = -1
        //              MOVE HIGH-VALUES TO VAS-MINUS-VALENZA-NULL
        //           END-IF
        if (ws.getIndValAst().getPrstzPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-MINUS-VALENZA-NULL
            valAst.getVasMinusValenza().setVasMinusValenzaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasMinusValenza.Len.VAS_MINUS_VALENZA_NULL));
        }
        // COB_CODE: IF IND-VAS-PLUS-VALENZA = -1
        //              MOVE HIGH-VALUES TO VAS-PLUS-VALENZA-NULL
        //           END-IF.
        if (ws.getIndValAst().getCumPreVers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VAS-PLUS-VALENZA-NULL
            valAst.getVasPlusValenza().setVasPlusValenzaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VasPlusValenza.Len.VAS_PLUS_VALENZA_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE VAS-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getValAstDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO VAS-DT-INI-EFF
        valAst.setVasDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE VAS-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getValAstDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO VAS-DT-END-EFF
        valAst.setVasDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-VAS-DT-VALZZ = 0
        //               MOVE WS-DATE-N      TO VAS-DT-VALZZ
        //           END-IF
        if (ws.getIndValAst().getImpbIs() == 0) {
            // COB_CODE: MOVE VAS-DT-VALZZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getValAstDb().getPervRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO VAS-DT-VALZZ
            valAst.getVasDtValzz().setVasDtValzz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-VAS-DT-VALZZ-CALC = 0
        //               MOVE WS-DATE-N      TO VAS-DT-VALZZ-CALC
        //           END-IF.
        if (ws.getIndValAst().getPrstzNet() == 0) {
            // COB_CODE: MOVE VAS-DT-VALZZ-CALC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getValAstDb().getEsecRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO VAS-DT-VALZZ-CALC
            valAst.getVasDtValzzCalc().setVasDtValzzCalc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return valAst.getVasCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.valAst.setVasCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFnd() {
        return valAst.getVasCodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.valAst.setVasCodFnd(codFnd);
    }

    @Override
    public String getCodFndObj() {
        if (ws.getIndValAst().getDtIniPer() >= 0) {
            return getCodFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        if (codFndObj != null) {
            setCodFnd(codFndObj);
            ws.getIndValAst().setDtIniPer(((short)0));
        }
        else {
            ws.getIndValAst().setDtIniPer(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return valAst.getVasDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.valAst.setVasDsOperSql(dsOperSql);
    }

    @Override
    public long getDsRiga() {
        return valAst.getVasDsRiga();
    }

    @Override
    public void setDsRiga(long dsRiga) {
        this.valAst.setVasDsRiga(dsRiga);
    }

    @Override
    public char getDsStatoElab() {
        return valAst.getVasDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.valAst.setVasDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return valAst.getVasDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.valAst.setVasDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return valAst.getVasDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.valAst.setVasDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return valAst.getVasDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.valAst.setVasDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return valAst.getVasDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.valAst.setVasDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getValAstDb().getRgstrzRichDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getValAstDb().setRgstrzRichDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getValAstDb().getEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getValAstDb().setEffDb(dtIniEffDb);
    }

    @Override
    public String getDtValzzCalcDb() {
        return ws.getValAstDb().getEsecRichDb();
    }

    @Override
    public void setDtValzzCalcDb(String dtValzzCalcDb) {
        this.ws.getValAstDb().setEsecRichDb(dtValzzCalcDb);
    }

    @Override
    public String getDtValzzCalcDbObj() {
        if (ws.getIndValAst().getPrstzNet() >= 0) {
            return getDtValzzCalcDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtValzzCalcDbObj(String dtValzzCalcDbObj) {
        if (dtValzzCalcDbObj != null) {
            setDtValzzCalcDb(dtValzzCalcDbObj);
            ws.getIndValAst().setPrstzNet(((short)0));
        }
        else {
            ws.getIndValAst().setPrstzNet(((short)-1));
        }
    }

    @Override
    public String getDtValzzDb() {
        return ws.getValAstDb().getPervRichDb();
    }

    @Override
    public void setDtValzzDb(String dtValzzDb) {
        this.ws.getValAstDb().setPervRichDb(dtValzzDb);
    }

    @Override
    public String getDtValzzDbObj() {
        if (ws.getIndValAst().getImpbIs() >= 0) {
            return getDtValzzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtValzzDbObj(String dtValzzDbObj) {
        if (dtValzzDbObj != null) {
            setDtValzzDb(dtValzzDbObj);
            ws.getIndValAst().setImpbIs(((short)0));
        }
        else {
            ws.getIndValAst().setImpbIs(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return valAst.getVasIdMoviChiu().getVasIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.valAst.getVasIdMoviChiu().setVasIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndValAst().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndValAst().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndValAst().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return valAst.getVasIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.valAst.setVasIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdMoviFinrio() {
        return valAst.getVasIdMoviFinrio();
    }

    @Override
    public void setIdMoviFinrio(int idMoviFinrio) {
        this.valAst.setVasIdMoviFinrio(idMoviFinrio);
    }

    @Override
    public int getIdRichDisFnd() {
        return valAst.getVasIdRichDisFnd().getVasIdRichDisFnd();
    }

    @Override
    public void setIdRichDisFnd(int idRichDisFnd) {
        this.valAst.getVasIdRichDisFnd().setVasIdRichDisFnd(idRichDisFnd);
    }

    @Override
    public Integer getIdRichDisFndObj() {
        if (ws.getIndValAst().getCodTrb() >= 0) {
            return ((Integer)getIdRichDisFnd());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichDisFndObj(Integer idRichDisFndObj) {
        if (idRichDisFndObj != null) {
            setIdRichDisFnd(((int)idRichDisFndObj));
            ws.getIndValAst().setCodTrb(((short)0));
        }
        else {
            ws.getIndValAst().setCodTrb(((short)-1));
        }
    }

    @Override
    public int getIdRichInvstFnd() {
        return valAst.getVasIdRichInvstFnd().getVasIdRichInvstFnd();
    }

    @Override
    public void setIdRichInvstFnd(int idRichInvstFnd) {
        this.valAst.getVasIdRichInvstFnd().setVasIdRichInvstFnd(idRichInvstFnd);
    }

    @Override
    public Integer getIdRichInvstFndObj() {
        if (ws.getIndValAst().getAlqIs() >= 0) {
            return ((Integer)getIdRichInvstFnd());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRichInvstFndObj(Integer idRichInvstFndObj) {
        if (idRichInvstFndObj != null) {
            setIdRichInvstFnd(((int)idRichInvstFndObj));
            ws.getIndValAst().setAlqIs(((short)0));
        }
        else {
            ws.getIndValAst().setAlqIs(((short)-1));
        }
    }

    @Override
    public int getIdTrchDiGar() {
        return valAst.getVasIdTrchDiGar().getVasIdTrchDiGar();
    }

    @Override
    public void setIdTrchDiGar(int idTrchDiGar) {
        this.valAst.getVasIdTrchDiGar().setVasIdTrchDiGar(idTrchDiGar);
    }

    @Override
    public Integer getIdTrchDiGarObj() {
        if (ws.getIndValAst().getIdOgg() >= 0) {
            return ((Integer)getIdTrchDiGar());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdTrchDiGarObj(Integer idTrchDiGarObj) {
        if (idTrchDiGarObj != null) {
            setIdTrchDiGar(((int)idTrchDiGarObj));
            ws.getIndValAst().setIdOgg(((short)0));
        }
        else {
            ws.getIndValAst().setIdOgg(((short)-1));
        }
    }

    @Override
    public int getIdValAst() {
        return valAst.getVasIdValAst();
    }

    @Override
    public void setIdValAst(int idValAst) {
        this.valAst.setVasIdValAst(idValAst);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpMovto() {
        return valAst.getVasImpMovto().getVasImpMovto();
    }

    @Override
    public void setImpMovto(AfDecimal impMovto) {
        this.valAst.getVasImpMovto().setVasImpMovto(impMovto.copy());
    }

    @Override
    public AfDecimal getImpMovtoObj() {
        if (ws.getIndValAst().getRisMatNetPrec() >= 0) {
            return getImpMovto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpMovtoObj(AfDecimal impMovtoObj) {
        if (impMovtoObj != null) {
            setImpMovto(new AfDecimal(impMovtoObj, 15, 3));
            ws.getIndValAst().setRisMatNetPrec(((short)0));
        }
        else {
            ws.getIndValAst().setRisMatNetPrec(((short)-1));
        }
    }

    @Override
    public int getLdbv4911IdTrchDiGar() {
        return ws.getLdbv4911().getIdTrchDiGar();
    }

    @Override
    public void setLdbv4911IdTrchDiGar(int ldbv4911IdTrchDiGar) {
        this.ws.getLdbv4911().setIdTrchDiGar(ldbv4911IdTrchDiGar);
    }

    @Override
    public String getLdbv4911TpValAst10() {
        return ws.getLdbv4911().getTpValAst10();
    }

    @Override
    public void setLdbv4911TpValAst10(String ldbv4911TpValAst10) {
        this.ws.getLdbv4911().setTpValAst10(ldbv4911TpValAst10);
    }

    @Override
    public String getLdbv4911TpValAst1() {
        return ws.getLdbv4911().getTpValAst1();
    }

    @Override
    public void setLdbv4911TpValAst1(String ldbv4911TpValAst1) {
        this.ws.getLdbv4911().setTpValAst1(ldbv4911TpValAst1);
    }

    @Override
    public String getLdbv4911TpValAst2() {
        return ws.getLdbv4911().getTpValAst2();
    }

    @Override
    public void setLdbv4911TpValAst2(String ldbv4911TpValAst2) {
        this.ws.getLdbv4911().setTpValAst2(ldbv4911TpValAst2);
    }

    @Override
    public String getLdbv4911TpValAst3() {
        return ws.getLdbv4911().getTpValAst3();
    }

    @Override
    public void setLdbv4911TpValAst3(String ldbv4911TpValAst3) {
        this.ws.getLdbv4911().setTpValAst3(ldbv4911TpValAst3);
    }

    @Override
    public String getLdbv4911TpValAst4() {
        return ws.getLdbv4911().getTpValAst4();
    }

    @Override
    public void setLdbv4911TpValAst4(String ldbv4911TpValAst4) {
        this.ws.getLdbv4911().setTpValAst4(ldbv4911TpValAst4);
    }

    @Override
    public String getLdbv4911TpValAst5() {
        return ws.getLdbv4911().getTpValAst5();
    }

    @Override
    public void setLdbv4911TpValAst5(String ldbv4911TpValAst5) {
        this.ws.getLdbv4911().setTpValAst5(ldbv4911TpValAst5);
    }

    @Override
    public String getLdbv4911TpValAst6() {
        return ws.getLdbv4911().getTpValAst6();
    }

    @Override
    public void setLdbv4911TpValAst6(String ldbv4911TpValAst6) {
        this.ws.getLdbv4911().setTpValAst6(ldbv4911TpValAst6);
    }

    @Override
    public String getLdbv4911TpValAst7() {
        return ws.getLdbv4911().getTpValAst7();
    }

    @Override
    public void setLdbv4911TpValAst7(String ldbv4911TpValAst7) {
        this.ws.getLdbv4911().setTpValAst7(ldbv4911TpValAst7);
    }

    @Override
    public String getLdbv4911TpValAst8() {
        return ws.getLdbv4911().getTpValAst8();
    }

    @Override
    public void setLdbv4911TpValAst8(String ldbv4911TpValAst8) {
        this.ws.getLdbv4911().setTpValAst8(ldbv4911TpValAst8);
    }

    @Override
    public String getLdbv4911TpValAst9() {
        return ws.getLdbv4911().getTpValAst9();
    }

    @Override
    public void setLdbv4911TpValAst9(String ldbv4911TpValAst9) {
        this.ws.getLdbv4911().setTpValAst9(ldbv4911TpValAst9);
    }

    @Override
    public long getLdbv6001AnnDsTsCptz() {
        throw new FieldNotMappedException("ldbv6001AnnDsTsCptz");
    }

    @Override
    public void setLdbv6001AnnDsTsCptz(long ldbv6001AnnDsTsCptz) {
        throw new FieldNotMappedException("ldbv6001AnnDsTsCptz");
    }

    @Override
    public int getLdbv6001IdTrchDiGar() {
        throw new FieldNotMappedException("ldbv6001IdTrchDiGar");
    }

    @Override
    public void setLdbv6001IdTrchDiGar(int ldbv6001IdTrchDiGar) {
        throw new FieldNotMappedException("ldbv6001IdTrchDiGar");
    }

    @Override
    public String getLdbv6001TpValAst10() {
        throw new FieldNotMappedException("ldbv6001TpValAst10");
    }

    @Override
    public void setLdbv6001TpValAst10(String ldbv6001TpValAst10) {
        throw new FieldNotMappedException("ldbv6001TpValAst10");
    }

    @Override
    public String getLdbv6001TpValAst1() {
        throw new FieldNotMappedException("ldbv6001TpValAst1");
    }

    @Override
    public void setLdbv6001TpValAst1(String ldbv6001TpValAst1) {
        throw new FieldNotMappedException("ldbv6001TpValAst1");
    }

    @Override
    public String getLdbv6001TpValAst2() {
        throw new FieldNotMappedException("ldbv6001TpValAst2");
    }

    @Override
    public void setLdbv6001TpValAst2(String ldbv6001TpValAst2) {
        throw new FieldNotMappedException("ldbv6001TpValAst2");
    }

    @Override
    public String getLdbv6001TpValAst3() {
        throw new FieldNotMappedException("ldbv6001TpValAst3");
    }

    @Override
    public void setLdbv6001TpValAst3(String ldbv6001TpValAst3) {
        throw new FieldNotMappedException("ldbv6001TpValAst3");
    }

    @Override
    public String getLdbv6001TpValAst4() {
        throw new FieldNotMappedException("ldbv6001TpValAst4");
    }

    @Override
    public void setLdbv6001TpValAst4(String ldbv6001TpValAst4) {
        throw new FieldNotMappedException("ldbv6001TpValAst4");
    }

    @Override
    public String getLdbv6001TpValAst5() {
        throw new FieldNotMappedException("ldbv6001TpValAst5");
    }

    @Override
    public void setLdbv6001TpValAst5(String ldbv6001TpValAst5) {
        throw new FieldNotMappedException("ldbv6001TpValAst5");
    }

    @Override
    public String getLdbv6001TpValAst6() {
        throw new FieldNotMappedException("ldbv6001TpValAst6");
    }

    @Override
    public void setLdbv6001TpValAst6(String ldbv6001TpValAst6) {
        throw new FieldNotMappedException("ldbv6001TpValAst6");
    }

    @Override
    public String getLdbv6001TpValAst7() {
        throw new FieldNotMappedException("ldbv6001TpValAst7");
    }

    @Override
    public void setLdbv6001TpValAst7(String ldbv6001TpValAst7) {
        throw new FieldNotMappedException("ldbv6001TpValAst7");
    }

    @Override
    public String getLdbv6001TpValAst8() {
        throw new FieldNotMappedException("ldbv6001TpValAst8");
    }

    @Override
    public void setLdbv6001TpValAst8(String ldbv6001TpValAst8) {
        throw new FieldNotMappedException("ldbv6001TpValAst8");
    }

    @Override
    public String getLdbv6001TpValAst9() {
        throw new FieldNotMappedException("ldbv6001TpValAst9");
    }

    @Override
    public void setLdbv6001TpValAst9(String ldbv6001TpValAst9) {
        throw new FieldNotMappedException("ldbv6001TpValAst9");
    }

    @Override
    public AfDecimal getMinusValenza() {
        return valAst.getVasMinusValenza().getVasMinusValenza();
    }

    @Override
    public void setMinusValenza(AfDecimal minusValenza) {
        this.valAst.getVasMinusValenza().setVasMinusValenza(minusValenza.copy());
    }

    @Override
    public AfDecimal getMinusValenzaObj() {
        if (ws.getIndValAst().getPrstzPrec() >= 0) {
            return getMinusValenza();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMinusValenzaObj(AfDecimal minusValenzaObj) {
        if (minusValenzaObj != null) {
            setMinusValenza(new AfDecimal(minusValenzaObj, 15, 3));
            ws.getIndValAst().setPrstzPrec(((short)0));
        }
        else {
            ws.getIndValAst().setPrstzPrec(((short)-1));
        }
    }

    @Override
    public AfDecimal getNumQuo() {
        return valAst.getVasNumQuo().getVasNumQuo();
    }

    @Override
    public void setNumQuo(AfDecimal numQuo) {
        this.valAst.getVasNumQuo().setVasNumQuo(numQuo.copy());
    }

    @Override
    public AfDecimal getNumQuoLorde() {
        return valAst.getVasNumQuoLorde().getVasNumQuoLorde();
    }

    @Override
    public void setNumQuoLorde(AfDecimal numQuoLorde) {
        this.valAst.getVasNumQuoLorde().setVasNumQuoLorde(numQuoLorde.copy());
    }

    @Override
    public AfDecimal getNumQuoLordeObj() {
        if (ws.getIndValAst().getRisMatPostTax() >= 0) {
            return getNumQuoLorde();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumQuoLordeObj(AfDecimal numQuoLordeObj) {
        if (numQuoLordeObj != null) {
            setNumQuoLorde(new AfDecimal(numQuoLordeObj, 12, 5));
            ws.getIndValAst().setRisMatPostTax(((short)0));
        }
        else {
            ws.getIndValAst().setRisMatPostTax(((short)-1));
        }
    }

    @Override
    public AfDecimal getNumQuoObj() {
        if (ws.getIndValAst().getDtEndPer() >= 0) {
            return getNumQuo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumQuoObj(AfDecimal numQuoObj) {
        if (numQuoObj != null) {
            setNumQuo(new AfDecimal(numQuoObj, 12, 5));
            ws.getIndValAst().setDtEndPer(((short)0));
        }
        else {
            ws.getIndValAst().setDtEndPer(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcInvDis() {
        return valAst.getVasPcInvDis().getVasPcInvDis();
    }

    @Override
    public void setPcInvDis(AfDecimal pcInvDis) {
        this.valAst.getVasPcInvDis().setVasPcInvDis(pcInvDis.copy());
    }

    @Override
    public AfDecimal getPcInvDisObj() {
        if (ws.getIndValAst().getRisMatAnteTax() >= 0) {
            return getPcInvDis();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcInvDisObj(AfDecimal pcInvDisObj) {
        if (pcInvDisObj != null) {
            setPcInvDis(new AfDecimal(pcInvDisObj, 14, 9));
            ws.getIndValAst().setRisMatAnteTax(((short)0));
        }
        else {
            ws.getIndValAst().setRisMatAnteTax(((short)-1));
        }
    }

    @Override
    public AfDecimal getPlusValenza() {
        return valAst.getVasPlusValenza().getVasPlusValenza();
    }

    @Override
    public void setPlusValenza(AfDecimal plusValenza) {
        this.valAst.getVasPlusValenza().setVasPlusValenza(plusValenza.copy());
    }

    @Override
    public AfDecimal getPlusValenzaObj() {
        if (ws.getIndValAst().getCumPreVers() >= 0) {
            return getPlusValenza();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPlusValenzaObj(AfDecimal plusValenzaObj) {
        if (plusValenzaObj != null) {
            setPlusValenza(new AfDecimal(plusValenzaObj, 15, 3));
            ws.getIndValAst().setCumPreVers(((short)0));
        }
        else {
            ws.getIndValAst().setCumPreVers(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreMovto() {
        return valAst.getVasPreMovto().getVasPreMovto();
    }

    @Override
    public void setPreMovto(AfDecimal preMovto) {
        this.valAst.getVasPreMovto().setVasPreMovto(preMovto.copy());
    }

    @Override
    public AfDecimal getPreMovtoObj() {
        if (ws.getIndValAst().getPrstzLrdAnteIs() >= 0) {
            return getPreMovto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreMovtoObj(AfDecimal preMovtoObj) {
        if (preMovtoObj != null) {
            setPreMovto(new AfDecimal(preMovtoObj, 15, 3));
            ws.getIndValAst().setPrstzLrdAnteIs(((short)0));
        }
        else {
            ws.getIndValAst().setPrstzLrdAnteIs(((short)-1));
        }
    }

    @Override
    public String getTpValAst() {
        return valAst.getVasTpValAst();
    }

    @Override
    public void setTpValAst(String tpValAst) {
        this.valAst.setVasTpValAst(tpValAst);
    }

    @Override
    public AfDecimal getValQuo() {
        return valAst.getVasValQuo().getVasValQuo();
    }

    @Override
    public void setValQuo(AfDecimal valQuo) {
        this.valAst.getVasValQuo().setVasValQuo(valQuo.copy());
    }

    @Override
    public AfDecimal getValQuoObj() {
        if (ws.getIndValAst().getImpstSost() >= 0) {
            return getValQuo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValQuoObj(AfDecimal valQuoObj) {
        if (valQuoObj != null) {
            setValQuo(new AfDecimal(valQuoObj, 12, 5));
            ws.getIndValAst().setImpstSost(((short)0));
        }
        else {
            ws.getIndValAst().setImpstSost(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
