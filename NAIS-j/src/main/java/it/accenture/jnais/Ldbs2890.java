package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.StatOggBusTrchDiGarDao;
import it.accenture.jnais.commons.data.to.IStatOggBusTrchDiGar;
import it.accenture.jnais.copy.Idsv0010;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbv2891;

/**Original name: LDBS2890<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  27 APR 2010.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs2890 extends Program implements IStatOggBusTrchDiGar {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private StatOggBusTrchDiGarDao statOggBusTrchDiGarDao = new StatOggBusTrchDiGarDao(dbAccessStatus);
    //Original name: DESCRIZ-ERR-DB2
    private String descrizErrDb2 = "";
    //Original name: IDSV0010
    private Idsv0010 idsv0010 = new Idsv0010();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: LDBV2891
    private Ldbv2891 ldbv2891;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS2890_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Ldbv2891 ldbv2891) {
        this.idsv0003 = idsv0003;
        this.ldbv2891 = ldbv2891;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs2890 getInstance() {
        return ((Ldbs2890)Programs.getInstance(Ldbs2890.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS2890'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS2890");
        // COB_CODE: MOVE ' LDBV2891' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella(" LDBV2891");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(descrizErrDb2);
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT SUM (VALUE(A.IMPB_VIS_END2000, 0))
        //               INTO :LDBV2891-IMPB-VIS-END2000
        //            FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE A.ID_POLI = :LDBV2891-ID-POL
        //                AND B.ID_OGG = A.ID_TRCH_DI_GAR
        //                AND B.TP_OGG = 'TG'
        //                AND B.TP_STAT_BUS <> 'ST'
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.ID_MOVI_CHIU IS NULL
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        ldbv2891.setImpbVisEnd2000(statOggBusTrchDiGarDao.selectRec1(ldbv2891.getIdPol(), idsv0003.getCodiceCompagniaAnia(), idsv0010.getWsDataInizioEffettoDb(), ldbv2891.getImpbVisEnd2000()));
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //            SELECT SUM (VALUE(A.IMPB_VIS_END2000, 0))
        //               INTO :LDBV2891-IMPB-VIS-END2000
        //            FROM TRCH_DI_GAR A, STAT_OGG_BUS B
        //              WHERE A.ID_POLI = :LDBV2891-ID-POL
        //                AND B.ID_OGG = A.ID_TRCH_DI_GAR
        //                AND B.TP_OGG = 'TG'
        //                AND B.TP_STAT_BUS <> 'ST'
        //                    AND A.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND A.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND A.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND A.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //                    AND B.COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND B.DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND B.DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND B.DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        ldbv2891.setImpbVisEnd2000(statOggBusTrchDiGarDao.selectRec2(ldbv2891.getIdPol(), idsv0003.getCodiceCompagniaAnia(), idsv0010.getWsDataInizioEffettoDb(), idsv0010.getWsTsCompetenza(), ldbv2891.getImpbVisEnd2000()));
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: CONTINUE.
        //continue
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            idsv0010.setWsDataInizioEffettoDb(idsv0010.getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                idsv0010.setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                idsv0010.setWsDataFineEffettoDb(idsv0010.getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            idsv0010.setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            idsv0010.setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        idsv0010.setWsDateX(Functions.setSubstring(idsv0010.getWsDateX(), idsv0010.getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        idsv0010.setWsDateX(Functions.setSubstring(idsv0010.getWsDateX(), idsv0010.getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        idsv0010.setWsDateX(Functions.setSubstring(idsv0010.getWsDateX(), idsv0010.getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        idsv0010.setWsDateX(Functions.setSubstring(idsv0010.getWsDateX(), "-", 5, 1));
        idsv0010.setWsDateX(Functions.setSubstring(idsv0010.getWsDateX(), "-", 8, 1));
    }

    public String getDescrizErrDb2() {
        return this.descrizErrDb2;
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        throw new FieldNotMappedException("idsv0003CodiceCompagniaAnia");
    }

    @Override
    public int getLc601IdMoviCrz() {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public void setLc601IdMoviCrz(int lc601IdMoviCrz) {
        throw new FieldNotMappedException("lc601IdMoviCrz");
    }

    @Override
    public int getLc601IdTrchDiGar() {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public void setLc601IdTrchDiGar(int lc601IdTrchDiGar) {
        throw new FieldNotMappedException("lc601IdTrchDiGar");
    }

    @Override
    public int getLdbi0731IdAdes() {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public void setLdbi0731IdAdes(int ldbi0731IdAdes) {
        throw new FieldNotMappedException("ldbi0731IdAdes");
    }

    @Override
    public int getLdbi0731IdPoli() {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public void setLdbi0731IdPoli(int ldbi0731IdPoli) {
        throw new FieldNotMappedException("ldbi0731IdPoli");
    }

    @Override
    public int getLdbi0731IdTrch() {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public void setLdbi0731IdTrch(int ldbi0731IdTrch) {
        throw new FieldNotMappedException("ldbi0731IdTrch");
    }

    @Override
    public String getLdbi0731TpStatBus() {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    @Override
    public void setLdbi0731TpStatBus(String ldbi0731TpStatBus) {
        throw new FieldNotMappedException("ldbi0731TpStatBus");
    }

    @Override
    public String getLdbv1361TpCaus01() {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public void setLdbv1361TpCaus01(String ldbv1361TpCaus01) {
        throw new FieldNotMappedException("ldbv1361TpCaus01");
    }

    @Override
    public String getLdbv1361TpCaus02() {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public void setLdbv1361TpCaus02(String ldbv1361TpCaus02) {
        throw new FieldNotMappedException("ldbv1361TpCaus02");
    }

    @Override
    public String getLdbv1361TpCaus03() {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public void setLdbv1361TpCaus03(String ldbv1361TpCaus03) {
        throw new FieldNotMappedException("ldbv1361TpCaus03");
    }

    @Override
    public String getLdbv1361TpCaus04() {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public void setLdbv1361TpCaus04(String ldbv1361TpCaus04) {
        throw new FieldNotMappedException("ldbv1361TpCaus04");
    }

    @Override
    public String getLdbv1361TpCaus05() {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public void setLdbv1361TpCaus05(String ldbv1361TpCaus05) {
        throw new FieldNotMappedException("ldbv1361TpCaus05");
    }

    @Override
    public String getLdbv1361TpCaus06() {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public void setLdbv1361TpCaus06(String ldbv1361TpCaus06) {
        throw new FieldNotMappedException("ldbv1361TpCaus06");
    }

    @Override
    public String getLdbv1361TpCaus07() {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public void setLdbv1361TpCaus07(String ldbv1361TpCaus07) {
        throw new FieldNotMappedException("ldbv1361TpCaus07");
    }

    @Override
    public String getLdbv1361TpStatBus01() {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public void setLdbv1361TpStatBus01(String ldbv1361TpStatBus01) {
        throw new FieldNotMappedException("ldbv1361TpStatBus01");
    }

    @Override
    public String getLdbv1361TpStatBus02() {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public void setLdbv1361TpStatBus02(String ldbv1361TpStatBus02) {
        throw new FieldNotMappedException("ldbv1361TpStatBus02");
    }

    @Override
    public String getLdbv1361TpStatBus03() {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public void setLdbv1361TpStatBus03(String ldbv1361TpStatBus03) {
        throw new FieldNotMappedException("ldbv1361TpStatBus03");
    }

    @Override
    public String getLdbv1361TpStatBus04() {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public void setLdbv1361TpStatBus04(String ldbv1361TpStatBus04) {
        throw new FieldNotMappedException("ldbv1361TpStatBus04");
    }

    @Override
    public String getLdbv1361TpStatBus05() {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public void setLdbv1361TpStatBus05(String ldbv1361TpStatBus05) {
        throw new FieldNotMappedException("ldbv1361TpStatBus05");
    }

    @Override
    public String getLdbv1361TpStatBus06() {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public void setLdbv1361TpStatBus06(String ldbv1361TpStatBus06) {
        throw new FieldNotMappedException("ldbv1361TpStatBus06");
    }

    @Override
    public String getLdbv1361TpStatBus07() {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public void setLdbv1361TpStatBus07(String ldbv1361TpStatBus07) {
        throw new FieldNotMappedException("ldbv1361TpStatBus07");
    }

    @Override
    public int getLdbv2911IdAdes() {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public void setLdbv2911IdAdes(int ldbv2911IdAdes) {
        throw new FieldNotMappedException("ldbv2911IdAdes");
    }

    @Override
    public int getLdbv2911IdPoli() {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public void setLdbv2911IdPoli(int ldbv2911IdPoli) {
        throw new FieldNotMappedException("ldbv2911IdPoli");
    }

    @Override
    public AfDecimal getLdbv2911PrstzIniNewfis() {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public void setLdbv2911PrstzIniNewfis(AfDecimal ldbv2911PrstzIniNewfis) {
        throw new FieldNotMappedException("ldbv2911PrstzIniNewfis");
    }

    @Override
    public int getLdbv3021IdOgg() {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public void setLdbv3021IdOgg(int ldbv3021IdOgg) {
        throw new FieldNotMappedException("ldbv3021IdOgg");
    }

    @Override
    public String getLdbv3021TpCausBus10() {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public void setLdbv3021TpCausBus10(String ldbv3021TpCausBus10) {
        throw new FieldNotMappedException("ldbv3021TpCausBus10");
    }

    @Override
    public String getLdbv3021TpCausBus11() {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public void setLdbv3021TpCausBus11(String ldbv3021TpCausBus11) {
        throw new FieldNotMappedException("ldbv3021TpCausBus11");
    }

    @Override
    public String getLdbv3021TpCausBus12() {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public void setLdbv3021TpCausBus12(String ldbv3021TpCausBus12) {
        throw new FieldNotMappedException("ldbv3021TpCausBus12");
    }

    @Override
    public String getLdbv3021TpCausBus13() {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public void setLdbv3021TpCausBus13(String ldbv3021TpCausBus13) {
        throw new FieldNotMappedException("ldbv3021TpCausBus13");
    }

    @Override
    public String getLdbv3021TpCausBus14() {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public void setLdbv3021TpCausBus14(String ldbv3021TpCausBus14) {
        throw new FieldNotMappedException("ldbv3021TpCausBus14");
    }

    @Override
    public String getLdbv3021TpCausBus15() {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public void setLdbv3021TpCausBus15(String ldbv3021TpCausBus15) {
        throw new FieldNotMappedException("ldbv3021TpCausBus15");
    }

    @Override
    public String getLdbv3021TpCausBus16() {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public void setLdbv3021TpCausBus16(String ldbv3021TpCausBus16) {
        throw new FieldNotMappedException("ldbv3021TpCausBus16");
    }

    @Override
    public String getLdbv3021TpCausBus17() {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public void setLdbv3021TpCausBus17(String ldbv3021TpCausBus17) {
        throw new FieldNotMappedException("ldbv3021TpCausBus17");
    }

    @Override
    public String getLdbv3021TpCausBus18() {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public void setLdbv3021TpCausBus18(String ldbv3021TpCausBus18) {
        throw new FieldNotMappedException("ldbv3021TpCausBus18");
    }

    @Override
    public String getLdbv3021TpCausBus19() {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public void setLdbv3021TpCausBus19(String ldbv3021TpCausBus19) {
        throw new FieldNotMappedException("ldbv3021TpCausBus19");
    }

    @Override
    public String getLdbv3021TpCausBus1() {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public void setLdbv3021TpCausBus1(String ldbv3021TpCausBus1) {
        throw new FieldNotMappedException("ldbv3021TpCausBus1");
    }

    @Override
    public String getLdbv3021TpCausBus20() {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public void setLdbv3021TpCausBus20(String ldbv3021TpCausBus20) {
        throw new FieldNotMappedException("ldbv3021TpCausBus20");
    }

    @Override
    public String getLdbv3021TpCausBus2() {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public void setLdbv3021TpCausBus2(String ldbv3021TpCausBus2) {
        throw new FieldNotMappedException("ldbv3021TpCausBus2");
    }

    @Override
    public String getLdbv3021TpCausBus3() {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public void setLdbv3021TpCausBus3(String ldbv3021TpCausBus3) {
        throw new FieldNotMappedException("ldbv3021TpCausBus3");
    }

    @Override
    public String getLdbv3021TpCausBus4() {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public void setLdbv3021TpCausBus4(String ldbv3021TpCausBus4) {
        throw new FieldNotMappedException("ldbv3021TpCausBus4");
    }

    @Override
    public String getLdbv3021TpCausBus5() {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public void setLdbv3021TpCausBus5(String ldbv3021TpCausBus5) {
        throw new FieldNotMappedException("ldbv3021TpCausBus5");
    }

    @Override
    public String getLdbv3021TpCausBus6() {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public void setLdbv3021TpCausBus6(String ldbv3021TpCausBus6) {
        throw new FieldNotMappedException("ldbv3021TpCausBus6");
    }

    @Override
    public String getLdbv3021TpCausBus7() {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public void setLdbv3021TpCausBus7(String ldbv3021TpCausBus7) {
        throw new FieldNotMappedException("ldbv3021TpCausBus7");
    }

    @Override
    public String getLdbv3021TpCausBus8() {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public void setLdbv3021TpCausBus8(String ldbv3021TpCausBus8) {
        throw new FieldNotMappedException("ldbv3021TpCausBus8");
    }

    @Override
    public String getLdbv3021TpCausBus9() {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public void setLdbv3021TpCausBus9(String ldbv3021TpCausBus9) {
        throw new FieldNotMappedException("ldbv3021TpCausBus9");
    }

    @Override
    public String getLdbv3021TpOgg() {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public void setLdbv3021TpOgg(String ldbv3021TpOgg) {
        throw new FieldNotMappedException("ldbv3021TpOgg");
    }

    @Override
    public String getLdbv3021TpStatBus() {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public void setLdbv3021TpStatBus(String ldbv3021TpStatBus) {
        throw new FieldNotMappedException("ldbv3021TpStatBus");
    }

    @Override
    public int getLdbv3421IdAdes() {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public void setLdbv3421IdAdes(int ldbv3421IdAdes) {
        throw new FieldNotMappedException("ldbv3421IdAdes");
    }

    @Override
    public int getLdbv3421IdGar() {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public void setLdbv3421IdGar(int ldbv3421IdGar) {
        throw new FieldNotMappedException("ldbv3421IdGar");
    }

    @Override
    public int getLdbv3421IdPoli() {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public void setLdbv3421IdPoli(int ldbv3421IdPoli) {
        throw new FieldNotMappedException("ldbv3421IdPoli");
    }

    @Override
    public String getLdbv3421TpOgg() {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public void setLdbv3421TpOgg(String ldbv3421TpOgg) {
        throw new FieldNotMappedException("ldbv3421TpOgg");
    }

    @Override
    public String getLdbv3421TpStatBus1() {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public void setLdbv3421TpStatBus1(String ldbv3421TpStatBus1) {
        throw new FieldNotMappedException("ldbv3421TpStatBus1");
    }

    @Override
    public String getLdbv3421TpStatBus2() {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public void setLdbv3421TpStatBus2(String ldbv3421TpStatBus2) {
        throw new FieldNotMappedException("ldbv3421TpStatBus2");
    }

    @Override
    public int getLdbvd511IdAdes() {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public void setLdbvd511IdAdes(int ldbvd511IdAdes) {
        throw new FieldNotMappedException("ldbvd511IdAdes");
    }

    @Override
    public int getLdbvd511IdPoli() {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public void setLdbvd511IdPoli(int ldbvd511IdPoli) {
        throw new FieldNotMappedException("ldbvd511IdPoli");
    }

    @Override
    public String getLdbvd511TpCaus() {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public void setLdbvd511TpCaus(String ldbvd511TpCaus) {
        throw new FieldNotMappedException("ldbvd511TpCaus");
    }

    @Override
    public String getLdbvd511TpOgg() {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public void setLdbvd511TpOgg(String ldbvd511TpOgg) {
        throw new FieldNotMappedException("ldbvd511TpOgg");
    }

    @Override
    public String getLdbvd511TpStatBus() {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public void setLdbvd511TpStatBus(String ldbvd511TpStatBus) {
        throw new FieldNotMappedException("ldbvd511TpStatBus");
    }

    @Override
    public int getLdbve251IdAdes() {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public void setLdbve251IdAdes(int ldbve251IdAdes) {
        throw new FieldNotMappedException("ldbve251IdAdes");
    }

    @Override
    public int getLdbve251IdMovi() {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public void setLdbve251IdMovi(int ldbve251IdMovi) {
        throw new FieldNotMappedException("ldbve251IdMovi");
    }

    @Override
    public AfDecimal getLdbve251ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public void setLdbve251ImpbVisEnd2000(AfDecimal ldbve251ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve251ImpbVisEnd2000");
    }

    @Override
    public String getLdbve251TpTrch10() {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public void setLdbve251TpTrch10(String ldbve251TpTrch10) {
        throw new FieldNotMappedException("ldbve251TpTrch10");
    }

    @Override
    public String getLdbve251TpTrch11() {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public void setLdbve251TpTrch11(String ldbve251TpTrch11) {
        throw new FieldNotMappedException("ldbve251TpTrch11");
    }

    @Override
    public String getLdbve251TpTrch12() {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public void setLdbve251TpTrch12(String ldbve251TpTrch12) {
        throw new FieldNotMappedException("ldbve251TpTrch12");
    }

    @Override
    public String getLdbve251TpTrch13() {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public void setLdbve251TpTrch13(String ldbve251TpTrch13) {
        throw new FieldNotMappedException("ldbve251TpTrch13");
    }

    @Override
    public String getLdbve251TpTrch14() {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public void setLdbve251TpTrch14(String ldbve251TpTrch14) {
        throw new FieldNotMappedException("ldbve251TpTrch14");
    }

    @Override
    public String getLdbve251TpTrch15() {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public void setLdbve251TpTrch15(String ldbve251TpTrch15) {
        throw new FieldNotMappedException("ldbve251TpTrch15");
    }

    @Override
    public String getLdbve251TpTrch16() {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public void setLdbve251TpTrch16(String ldbve251TpTrch16) {
        throw new FieldNotMappedException("ldbve251TpTrch16");
    }

    @Override
    public String getLdbve251TpTrch1() {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public void setLdbve251TpTrch1(String ldbve251TpTrch1) {
        throw new FieldNotMappedException("ldbve251TpTrch1");
    }

    @Override
    public String getLdbve251TpTrch2() {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public void setLdbve251TpTrch2(String ldbve251TpTrch2) {
        throw new FieldNotMappedException("ldbve251TpTrch2");
    }

    @Override
    public String getLdbve251TpTrch3() {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public void setLdbve251TpTrch3(String ldbve251TpTrch3) {
        throw new FieldNotMappedException("ldbve251TpTrch3");
    }

    @Override
    public String getLdbve251TpTrch4() {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public void setLdbve251TpTrch4(String ldbve251TpTrch4) {
        throw new FieldNotMappedException("ldbve251TpTrch4");
    }

    @Override
    public String getLdbve251TpTrch5() {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public void setLdbve251TpTrch5(String ldbve251TpTrch5) {
        throw new FieldNotMappedException("ldbve251TpTrch5");
    }

    @Override
    public String getLdbve251TpTrch6() {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public void setLdbve251TpTrch6(String ldbve251TpTrch6) {
        throw new FieldNotMappedException("ldbve251TpTrch6");
    }

    @Override
    public String getLdbve251TpTrch7() {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public void setLdbve251TpTrch7(String ldbve251TpTrch7) {
        throw new FieldNotMappedException("ldbve251TpTrch7");
    }

    @Override
    public String getLdbve251TpTrch8() {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public void setLdbve251TpTrch8(String ldbve251TpTrch8) {
        throw new FieldNotMappedException("ldbve251TpTrch8");
    }

    @Override
    public String getLdbve251TpTrch9() {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public void setLdbve251TpTrch9(String ldbve251TpTrch9) {
        throw new FieldNotMappedException("ldbve251TpTrch9");
    }

    @Override
    public int getLdbve261IdAdes() {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public void setLdbve261IdAdes(int ldbve261IdAdes) {
        throw new FieldNotMappedException("ldbve261IdAdes");
    }

    @Override
    public AfDecimal getLdbve261ImpbVisEnd2000() {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public void setLdbve261ImpbVisEnd2000(AfDecimal ldbve261ImpbVisEnd2000) {
        throw new FieldNotMappedException("ldbve261ImpbVisEnd2000");
    }

    @Override
    public String getLdbve261TpTrch10() {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public void setLdbve261TpTrch10(String ldbve261TpTrch10) {
        throw new FieldNotMappedException("ldbve261TpTrch10");
    }

    @Override
    public String getLdbve261TpTrch11() {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public void setLdbve261TpTrch11(String ldbve261TpTrch11) {
        throw new FieldNotMappedException("ldbve261TpTrch11");
    }

    @Override
    public String getLdbve261TpTrch12() {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public void setLdbve261TpTrch12(String ldbve261TpTrch12) {
        throw new FieldNotMappedException("ldbve261TpTrch12");
    }

    @Override
    public String getLdbve261TpTrch13() {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public void setLdbve261TpTrch13(String ldbve261TpTrch13) {
        throw new FieldNotMappedException("ldbve261TpTrch13");
    }

    @Override
    public String getLdbve261TpTrch14() {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public void setLdbve261TpTrch14(String ldbve261TpTrch14) {
        throw new FieldNotMappedException("ldbve261TpTrch14");
    }

    @Override
    public String getLdbve261TpTrch15() {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public void setLdbve261TpTrch15(String ldbve261TpTrch15) {
        throw new FieldNotMappedException("ldbve261TpTrch15");
    }

    @Override
    public String getLdbve261TpTrch16() {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public void setLdbve261TpTrch16(String ldbve261TpTrch16) {
        throw new FieldNotMappedException("ldbve261TpTrch16");
    }

    @Override
    public String getLdbve261TpTrch1() {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public void setLdbve261TpTrch1(String ldbve261TpTrch1) {
        throw new FieldNotMappedException("ldbve261TpTrch1");
    }

    @Override
    public String getLdbve261TpTrch2() {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public void setLdbve261TpTrch2(String ldbve261TpTrch2) {
        throw new FieldNotMappedException("ldbve261TpTrch2");
    }

    @Override
    public String getLdbve261TpTrch3() {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public void setLdbve261TpTrch3(String ldbve261TpTrch3) {
        throw new FieldNotMappedException("ldbve261TpTrch3");
    }

    @Override
    public String getLdbve261TpTrch4() {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public void setLdbve261TpTrch4(String ldbve261TpTrch4) {
        throw new FieldNotMappedException("ldbve261TpTrch4");
    }

    @Override
    public String getLdbve261TpTrch5() {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public void setLdbve261TpTrch5(String ldbve261TpTrch5) {
        throw new FieldNotMappedException("ldbve261TpTrch5");
    }

    @Override
    public String getLdbve261TpTrch6() {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public void setLdbve261TpTrch6(String ldbve261TpTrch6) {
        throw new FieldNotMappedException("ldbve261TpTrch6");
    }

    @Override
    public String getLdbve261TpTrch7() {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public void setLdbve261TpTrch7(String ldbve261TpTrch7) {
        throw new FieldNotMappedException("ldbve261TpTrch7");
    }

    @Override
    public String getLdbve261TpTrch8() {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public void setLdbve261TpTrch8(String ldbve261TpTrch8) {
        throw new FieldNotMappedException("ldbve261TpTrch8");
    }

    @Override
    public String getLdbve261TpTrch9() {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public void setLdbve261TpTrch9(String ldbve261TpTrch9) {
        throw new FieldNotMappedException("ldbve261TpTrch9");
    }

    @Override
    public int getStbCodCompAnia() {
        throw new FieldNotMappedException("stbCodCompAnia");
    }

    @Override
    public void setStbCodCompAnia(int stbCodCompAnia) {
        throw new FieldNotMappedException("stbCodCompAnia");
    }

    @Override
    public char getStbDsOperSql() {
        throw new FieldNotMappedException("stbDsOperSql");
    }

    @Override
    public void setStbDsOperSql(char stbDsOperSql) {
        throw new FieldNotMappedException("stbDsOperSql");
    }

    @Override
    public long getStbDsRiga() {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public void setStbDsRiga(long stbDsRiga) {
        throw new FieldNotMappedException("stbDsRiga");
    }

    @Override
    public char getStbDsStatoElab() {
        throw new FieldNotMappedException("stbDsStatoElab");
    }

    @Override
    public void setStbDsStatoElab(char stbDsStatoElab) {
        throw new FieldNotMappedException("stbDsStatoElab");
    }

    @Override
    public long getStbDsTsEndCptz() {
        throw new FieldNotMappedException("stbDsTsEndCptz");
    }

    @Override
    public void setStbDsTsEndCptz(long stbDsTsEndCptz) {
        throw new FieldNotMappedException("stbDsTsEndCptz");
    }

    @Override
    public long getStbDsTsIniCptz() {
        throw new FieldNotMappedException("stbDsTsIniCptz");
    }

    @Override
    public void setStbDsTsIniCptz(long stbDsTsIniCptz) {
        throw new FieldNotMappedException("stbDsTsIniCptz");
    }

    @Override
    public String getStbDsUtente() {
        throw new FieldNotMappedException("stbDsUtente");
    }

    @Override
    public void setStbDsUtente(String stbDsUtente) {
        throw new FieldNotMappedException("stbDsUtente");
    }

    @Override
    public int getStbDsVer() {
        throw new FieldNotMappedException("stbDsVer");
    }

    @Override
    public void setStbDsVer(int stbDsVer) {
        throw new FieldNotMappedException("stbDsVer");
    }

    @Override
    public String getStbDtEndEffDb() {
        throw new FieldNotMappedException("stbDtEndEffDb");
    }

    @Override
    public void setStbDtEndEffDb(String stbDtEndEffDb) {
        throw new FieldNotMappedException("stbDtEndEffDb");
    }

    @Override
    public String getStbDtIniEffDb() {
        throw new FieldNotMappedException("stbDtIniEffDb");
    }

    @Override
    public void setStbDtIniEffDb(String stbDtIniEffDb) {
        throw new FieldNotMappedException("stbDtIniEffDb");
    }

    @Override
    public int getStbIdMoviChiu() {
        throw new FieldNotMappedException("stbIdMoviChiu");
    }

    @Override
    public void setStbIdMoviChiu(int stbIdMoviChiu) {
        throw new FieldNotMappedException("stbIdMoviChiu");
    }

    @Override
    public Integer getStbIdMoviChiuObj() {
        return ((Integer)getStbIdMoviChiu());
    }

    @Override
    public void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj) {
        setStbIdMoviChiu(((int)stbIdMoviChiuObj));
    }

    @Override
    public int getStbIdMoviCrz() {
        throw new FieldNotMappedException("stbIdMoviCrz");
    }

    @Override
    public void setStbIdMoviCrz(int stbIdMoviCrz) {
        throw new FieldNotMappedException("stbIdMoviCrz");
    }

    @Override
    public int getStbIdOgg() {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public void setStbIdOgg(int stbIdOgg) {
        throw new FieldNotMappedException("stbIdOgg");
    }

    @Override
    public int getStbIdStatOggBus() {
        throw new FieldNotMappedException("stbIdStatOggBus");
    }

    @Override
    public void setStbIdStatOggBus(int stbIdStatOggBus) {
        throw new FieldNotMappedException("stbIdStatOggBus");
    }

    @Override
    public String getStbTpCaus() {
        throw new FieldNotMappedException("stbTpCaus");
    }

    @Override
    public void setStbTpCaus(String stbTpCaus) {
        throw new FieldNotMappedException("stbTpCaus");
    }

    @Override
    public String getStbTpOgg() {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public void setStbTpOgg(String stbTpOgg) {
        throw new FieldNotMappedException("stbTpOgg");
    }

    @Override
    public String getStbTpStatBus() {
        throw new FieldNotMappedException("stbTpStatBus");
    }

    @Override
    public void setStbTpStatBus(String stbTpStatBus) {
        throw new FieldNotMappedException("stbTpStatBus");
    }

    @Override
    public AfDecimal getTgaAbbAnnuUlt() {
        throw new FieldNotMappedException("tgaAbbAnnuUlt");
    }

    @Override
    public void setTgaAbbAnnuUlt(AfDecimal tgaAbbAnnuUlt) {
        throw new FieldNotMappedException("tgaAbbAnnuUlt");
    }

    @Override
    public AfDecimal getTgaAbbAnnuUltObj() {
        return getTgaAbbAnnuUlt();
    }

    @Override
    public void setTgaAbbAnnuUltObj(AfDecimal tgaAbbAnnuUltObj) {
        setTgaAbbAnnuUlt(new AfDecimal(tgaAbbAnnuUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaAbbTotIni() {
        throw new FieldNotMappedException("tgaAbbTotIni");
    }

    @Override
    public void setTgaAbbTotIni(AfDecimal tgaAbbTotIni) {
        throw new FieldNotMappedException("tgaAbbTotIni");
    }

    @Override
    public AfDecimal getTgaAbbTotIniObj() {
        return getTgaAbbTotIni();
    }

    @Override
    public void setTgaAbbTotIniObj(AfDecimal tgaAbbTotIniObj) {
        setTgaAbbTotIni(new AfDecimal(tgaAbbTotIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaAbbTotUlt() {
        throw new FieldNotMappedException("tgaAbbTotUlt");
    }

    @Override
    public void setTgaAbbTotUlt(AfDecimal tgaAbbTotUlt) {
        throw new FieldNotMappedException("tgaAbbTotUlt");
    }

    @Override
    public AfDecimal getTgaAbbTotUltObj() {
        return getTgaAbbTotUlt();
    }

    @Override
    public void setTgaAbbTotUltObj(AfDecimal tgaAbbTotUltObj) {
        setTgaAbbTotUlt(new AfDecimal(tgaAbbTotUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaAcqExp() {
        throw new FieldNotMappedException("tgaAcqExp");
    }

    @Override
    public void setTgaAcqExp(AfDecimal tgaAcqExp) {
        throw new FieldNotMappedException("tgaAcqExp");
    }

    @Override
    public AfDecimal getTgaAcqExpObj() {
        return getTgaAcqExp();
    }

    @Override
    public void setTgaAcqExpObj(AfDecimal tgaAcqExpObj) {
        setTgaAcqExp(new AfDecimal(tgaAcqExpObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaAlqCommisInter() {
        throw new FieldNotMappedException("tgaAlqCommisInter");
    }

    @Override
    public void setTgaAlqCommisInter(AfDecimal tgaAlqCommisInter) {
        throw new FieldNotMappedException("tgaAlqCommisInter");
    }

    @Override
    public AfDecimal getTgaAlqCommisInterObj() {
        return getTgaAlqCommisInter();
    }

    @Override
    public void setTgaAlqCommisInterObj(AfDecimal tgaAlqCommisInterObj) {
        setTgaAlqCommisInter(new AfDecimal(tgaAlqCommisInterObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqProvAcq() {
        throw new FieldNotMappedException("tgaAlqProvAcq");
    }

    @Override
    public void setTgaAlqProvAcq(AfDecimal tgaAlqProvAcq) {
        throw new FieldNotMappedException("tgaAlqProvAcq");
    }

    @Override
    public AfDecimal getTgaAlqProvAcqObj() {
        return getTgaAlqProvAcq();
    }

    @Override
    public void setTgaAlqProvAcqObj(AfDecimal tgaAlqProvAcqObj) {
        setTgaAlqProvAcq(new AfDecimal(tgaAlqProvAcqObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqProvInc() {
        throw new FieldNotMappedException("tgaAlqProvInc");
    }

    @Override
    public void setTgaAlqProvInc(AfDecimal tgaAlqProvInc) {
        throw new FieldNotMappedException("tgaAlqProvInc");
    }

    @Override
    public AfDecimal getTgaAlqProvIncObj() {
        return getTgaAlqProvInc();
    }

    @Override
    public void setTgaAlqProvIncObj(AfDecimal tgaAlqProvIncObj) {
        setTgaAlqProvInc(new AfDecimal(tgaAlqProvIncObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqProvRicor() {
        throw new FieldNotMappedException("tgaAlqProvRicor");
    }

    @Override
    public void setTgaAlqProvRicor(AfDecimal tgaAlqProvRicor) {
        throw new FieldNotMappedException("tgaAlqProvRicor");
    }

    @Override
    public AfDecimal getTgaAlqProvRicorObj() {
        return getTgaAlqProvRicor();
    }

    @Override
    public void setTgaAlqProvRicorObj(AfDecimal tgaAlqProvRicorObj) {
        setTgaAlqProvRicor(new AfDecimal(tgaAlqProvRicorObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqRemunAss() {
        throw new FieldNotMappedException("tgaAlqRemunAss");
    }

    @Override
    public void setTgaAlqRemunAss(AfDecimal tgaAlqRemunAss) {
        throw new FieldNotMappedException("tgaAlqRemunAss");
    }

    @Override
    public AfDecimal getTgaAlqRemunAssObj() {
        return getTgaAlqRemunAss();
    }

    @Override
    public void setTgaAlqRemunAssObj(AfDecimal tgaAlqRemunAssObj) {
        setTgaAlqRemunAss(new AfDecimal(tgaAlqRemunAssObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaAlqScon() {
        throw new FieldNotMappedException("tgaAlqScon");
    }

    @Override
    public void setTgaAlqScon(AfDecimal tgaAlqScon) {
        throw new FieldNotMappedException("tgaAlqScon");
    }

    @Override
    public AfDecimal getTgaAlqSconObj() {
        return getTgaAlqScon();
    }

    @Override
    public void setTgaAlqSconObj(AfDecimal tgaAlqSconObj) {
        setTgaAlqScon(new AfDecimal(tgaAlqSconObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqto() {
        throw new FieldNotMappedException("tgaBnsGiaLiqto");
    }

    @Override
    public void setTgaBnsGiaLiqto(AfDecimal tgaBnsGiaLiqto) {
        throw new FieldNotMappedException("tgaBnsGiaLiqto");
    }

    @Override
    public AfDecimal getTgaBnsGiaLiqtoObj() {
        return getTgaBnsGiaLiqto();
    }

    @Override
    public void setTgaBnsGiaLiqtoObj(AfDecimal tgaBnsGiaLiqtoObj) {
        setTgaBnsGiaLiqto(new AfDecimal(tgaBnsGiaLiqtoObj, 15, 3));
    }

    @Override
    public int getTgaCodCompAnia() {
        throw new FieldNotMappedException("tgaCodCompAnia");
    }

    @Override
    public void setTgaCodCompAnia(int tgaCodCompAnia) {
        throw new FieldNotMappedException("tgaCodCompAnia");
    }

    @Override
    public String getTgaCodDvs() {
        throw new FieldNotMappedException("tgaCodDvs");
    }

    @Override
    public void setTgaCodDvs(String tgaCodDvs) {
        throw new FieldNotMappedException("tgaCodDvs");
    }

    @Override
    public AfDecimal getTgaCommisGest() {
        throw new FieldNotMappedException("tgaCommisGest");
    }

    @Override
    public void setTgaCommisGest(AfDecimal tgaCommisGest) {
        throw new FieldNotMappedException("tgaCommisGest");
    }

    @Override
    public AfDecimal getTgaCommisGestObj() {
        return getTgaCommisGest();
    }

    @Override
    public void setTgaCommisGestObj(AfDecimal tgaCommisGestObj) {
        setTgaCommisGest(new AfDecimal(tgaCommisGestObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCommisInter() {
        throw new FieldNotMappedException("tgaCommisInter");
    }

    @Override
    public void setTgaCommisInter(AfDecimal tgaCommisInter) {
        throw new FieldNotMappedException("tgaCommisInter");
    }

    @Override
    public AfDecimal getTgaCommisInterObj() {
        return getTgaCommisInter();
    }

    @Override
    public void setTgaCommisInterObj(AfDecimal tgaCommisInterObj) {
        setTgaCommisInter(new AfDecimal(tgaCommisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCosRunAssva() {
        throw new FieldNotMappedException("tgaCosRunAssva");
    }

    @Override
    public void setTgaCosRunAssva(AfDecimal tgaCosRunAssva) {
        throw new FieldNotMappedException("tgaCosRunAssva");
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdc() {
        throw new FieldNotMappedException("tgaCosRunAssvaIdc");
    }

    @Override
    public void setTgaCosRunAssvaIdc(AfDecimal tgaCosRunAssvaIdc) {
        throw new FieldNotMappedException("tgaCosRunAssvaIdc");
    }

    @Override
    public AfDecimal getTgaCosRunAssvaIdcObj() {
        return getTgaCosRunAssvaIdc();
    }

    @Override
    public void setTgaCosRunAssvaIdcObj(AfDecimal tgaCosRunAssvaIdcObj) {
        setTgaCosRunAssvaIdc(new AfDecimal(tgaCosRunAssvaIdcObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCosRunAssvaObj() {
        return getTgaCosRunAssva();
    }

    @Override
    public void setTgaCosRunAssvaObj(AfDecimal tgaCosRunAssvaObj) {
        setTgaCosRunAssva(new AfDecimal(tgaCosRunAssvaObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCptInOpzRivto() {
        throw new FieldNotMappedException("tgaCptInOpzRivto");
    }

    @Override
    public void setTgaCptInOpzRivto(AfDecimal tgaCptInOpzRivto) {
        throw new FieldNotMappedException("tgaCptInOpzRivto");
    }

    @Override
    public AfDecimal getTgaCptInOpzRivtoObj() {
        return getTgaCptInOpzRivto();
    }

    @Override
    public void setTgaCptInOpzRivtoObj(AfDecimal tgaCptInOpzRivtoObj) {
        setTgaCptInOpzRivto(new AfDecimal(tgaCptInOpzRivtoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCptMinScad() {
        throw new FieldNotMappedException("tgaCptMinScad");
    }

    @Override
    public void setTgaCptMinScad(AfDecimal tgaCptMinScad) {
        throw new FieldNotMappedException("tgaCptMinScad");
    }

    @Override
    public AfDecimal getTgaCptMinScadObj() {
        return getTgaCptMinScad();
    }

    @Override
    public void setTgaCptMinScadObj(AfDecimal tgaCptMinScadObj) {
        setTgaCptMinScad(new AfDecimal(tgaCptMinScadObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaCptRshMor() {
        throw new FieldNotMappedException("tgaCptRshMor");
    }

    @Override
    public void setTgaCptRshMor(AfDecimal tgaCptRshMor) {
        throw new FieldNotMappedException("tgaCptRshMor");
    }

    @Override
    public AfDecimal getTgaCptRshMorObj() {
        return getTgaCptRshMor();
    }

    @Override
    public void setTgaCptRshMorObj(AfDecimal tgaCptRshMorObj) {
        setTgaCptRshMor(new AfDecimal(tgaCptRshMorObj, 15, 3));
    }

    @Override
    public char getTgaDsOperSql() {
        throw new FieldNotMappedException("tgaDsOperSql");
    }

    @Override
    public void setTgaDsOperSql(char tgaDsOperSql) {
        throw new FieldNotMappedException("tgaDsOperSql");
    }

    @Override
    public long getTgaDsRiga() {
        throw new FieldNotMappedException("tgaDsRiga");
    }

    @Override
    public void setTgaDsRiga(long tgaDsRiga) {
        throw new FieldNotMappedException("tgaDsRiga");
    }

    @Override
    public char getTgaDsStatoElab() {
        throw new FieldNotMappedException("tgaDsStatoElab");
    }

    @Override
    public void setTgaDsStatoElab(char tgaDsStatoElab) {
        throw new FieldNotMappedException("tgaDsStatoElab");
    }

    @Override
    public long getTgaDsTsEndCptz() {
        throw new FieldNotMappedException("tgaDsTsEndCptz");
    }

    @Override
    public void setTgaDsTsEndCptz(long tgaDsTsEndCptz) {
        throw new FieldNotMappedException("tgaDsTsEndCptz");
    }

    @Override
    public long getTgaDsTsIniCptz() {
        throw new FieldNotMappedException("tgaDsTsIniCptz");
    }

    @Override
    public void setTgaDsTsIniCptz(long tgaDsTsIniCptz) {
        throw new FieldNotMappedException("tgaDsTsIniCptz");
    }

    @Override
    public String getTgaDsUtente() {
        throw new FieldNotMappedException("tgaDsUtente");
    }

    @Override
    public void setTgaDsUtente(String tgaDsUtente) {
        throw new FieldNotMappedException("tgaDsUtente");
    }

    @Override
    public int getTgaDsVer() {
        throw new FieldNotMappedException("tgaDsVer");
    }

    @Override
    public void setTgaDsVer(int tgaDsVer) {
        throw new FieldNotMappedException("tgaDsVer");
    }

    @Override
    public String getTgaDtDecorDb() {
        throw new FieldNotMappedException("tgaDtDecorDb");
    }

    @Override
    public void setTgaDtDecorDb(String tgaDtDecorDb) {
        throw new FieldNotMappedException("tgaDtDecorDb");
    }

    @Override
    public String getTgaDtEffStabDb() {
        throw new FieldNotMappedException("tgaDtEffStabDb");
    }

    @Override
    public void setTgaDtEffStabDb(String tgaDtEffStabDb) {
        throw new FieldNotMappedException("tgaDtEffStabDb");
    }

    @Override
    public String getTgaDtEffStabDbObj() {
        return getTgaDtEffStabDb();
    }

    @Override
    public void setTgaDtEffStabDbObj(String tgaDtEffStabDbObj) {
        setTgaDtEffStabDb(tgaDtEffStabDbObj);
    }

    @Override
    public String getTgaDtEmisDb() {
        throw new FieldNotMappedException("tgaDtEmisDb");
    }

    @Override
    public void setTgaDtEmisDb(String tgaDtEmisDb) {
        throw new FieldNotMappedException("tgaDtEmisDb");
    }

    @Override
    public String getTgaDtEmisDbObj() {
        return getTgaDtEmisDb();
    }

    @Override
    public void setTgaDtEmisDbObj(String tgaDtEmisDbObj) {
        setTgaDtEmisDb(tgaDtEmisDbObj);
    }

    @Override
    public String getTgaDtEndEffDb() {
        throw new FieldNotMappedException("tgaDtEndEffDb");
    }

    @Override
    public void setTgaDtEndEffDb(String tgaDtEndEffDb) {
        throw new FieldNotMappedException("tgaDtEndEffDb");
    }

    @Override
    public String getTgaDtIniEffDb() {
        throw new FieldNotMappedException("tgaDtIniEffDb");
    }

    @Override
    public void setTgaDtIniEffDb(String tgaDtIniEffDb) {
        throw new FieldNotMappedException("tgaDtIniEffDb");
    }

    @Override
    public String getTgaDtIniValTarDb() {
        throw new FieldNotMappedException("tgaDtIniValTarDb");
    }

    @Override
    public void setTgaDtIniValTarDb(String tgaDtIniValTarDb) {
        throw new FieldNotMappedException("tgaDtIniValTarDb");
    }

    @Override
    public String getTgaDtIniValTarDbObj() {
        return getTgaDtIniValTarDb();
    }

    @Override
    public void setTgaDtIniValTarDbObj(String tgaDtIniValTarDbObj) {
        setTgaDtIniValTarDb(tgaDtIniValTarDbObj);
    }

    @Override
    public String getTgaDtScadDb() {
        throw new FieldNotMappedException("tgaDtScadDb");
    }

    @Override
    public void setTgaDtScadDb(String tgaDtScadDb) {
        throw new FieldNotMappedException("tgaDtScadDb");
    }

    @Override
    public String getTgaDtScadDbObj() {
        return getTgaDtScadDb();
    }

    @Override
    public void setTgaDtScadDbObj(String tgaDtScadDbObj) {
        setTgaDtScadDb(tgaDtScadDbObj);
    }

    @Override
    public String getTgaDtUltAdegPrePrDb() {
        throw new FieldNotMappedException("tgaDtUltAdegPrePrDb");
    }

    @Override
    public void setTgaDtUltAdegPrePrDb(String tgaDtUltAdegPrePrDb) {
        throw new FieldNotMappedException("tgaDtUltAdegPrePrDb");
    }

    @Override
    public String getTgaDtUltAdegPrePrDbObj() {
        return getTgaDtUltAdegPrePrDb();
    }

    @Override
    public void setTgaDtUltAdegPrePrDbObj(String tgaDtUltAdegPrePrDbObj) {
        setTgaDtUltAdegPrePrDb(tgaDtUltAdegPrePrDbObj);
    }

    @Override
    public String getTgaDtVldtProdDb() {
        throw new FieldNotMappedException("tgaDtVldtProdDb");
    }

    @Override
    public void setTgaDtVldtProdDb(String tgaDtVldtProdDb) {
        throw new FieldNotMappedException("tgaDtVldtProdDb");
    }

    @Override
    public String getTgaDtVldtProdDbObj() {
        return getTgaDtVldtProdDb();
    }

    @Override
    public void setTgaDtVldtProdDbObj(String tgaDtVldtProdDbObj) {
        setTgaDtVldtProdDb(tgaDtVldtProdDbObj);
    }

    @Override
    public int getTgaDurAa() {
        throw new FieldNotMappedException("tgaDurAa");
    }

    @Override
    public void setTgaDurAa(int tgaDurAa) {
        throw new FieldNotMappedException("tgaDurAa");
    }

    @Override
    public Integer getTgaDurAaObj() {
        return ((Integer)getTgaDurAa());
    }

    @Override
    public void setTgaDurAaObj(Integer tgaDurAaObj) {
        setTgaDurAa(((int)tgaDurAaObj));
    }

    @Override
    public int getTgaDurAbb() {
        throw new FieldNotMappedException("tgaDurAbb");
    }

    @Override
    public void setTgaDurAbb(int tgaDurAbb) {
        throw new FieldNotMappedException("tgaDurAbb");
    }

    @Override
    public Integer getTgaDurAbbObj() {
        return ((Integer)getTgaDurAbb());
    }

    @Override
    public void setTgaDurAbbObj(Integer tgaDurAbbObj) {
        setTgaDurAbb(((int)tgaDurAbbObj));
    }

    @Override
    public int getTgaDurGg() {
        throw new FieldNotMappedException("tgaDurGg");
    }

    @Override
    public void setTgaDurGg(int tgaDurGg) {
        throw new FieldNotMappedException("tgaDurGg");
    }

    @Override
    public Integer getTgaDurGgObj() {
        return ((Integer)getTgaDurGg());
    }

    @Override
    public void setTgaDurGgObj(Integer tgaDurGgObj) {
        setTgaDurGg(((int)tgaDurGgObj));
    }

    @Override
    public int getTgaDurMm() {
        throw new FieldNotMappedException("tgaDurMm");
    }

    @Override
    public void setTgaDurMm(int tgaDurMm) {
        throw new FieldNotMappedException("tgaDurMm");
    }

    @Override
    public Integer getTgaDurMmObj() {
        return ((Integer)getTgaDurMm());
    }

    @Override
    public void setTgaDurMmObj(Integer tgaDurMmObj) {
        setTgaDurMm(((int)tgaDurMmObj));
    }

    @Override
    public short getTgaEtaAa1oAssto() {
        throw new FieldNotMappedException("tgaEtaAa1oAssto");
    }

    @Override
    public void setTgaEtaAa1oAssto(short tgaEtaAa1oAssto) {
        throw new FieldNotMappedException("tgaEtaAa1oAssto");
    }

    @Override
    public Short getTgaEtaAa1oAsstoObj() {
        return ((Short)getTgaEtaAa1oAssto());
    }

    @Override
    public void setTgaEtaAa1oAsstoObj(Short tgaEtaAa1oAsstoObj) {
        setTgaEtaAa1oAssto(((short)tgaEtaAa1oAsstoObj));
    }

    @Override
    public short getTgaEtaAa2oAssto() {
        throw new FieldNotMappedException("tgaEtaAa2oAssto");
    }

    @Override
    public void setTgaEtaAa2oAssto(short tgaEtaAa2oAssto) {
        throw new FieldNotMappedException("tgaEtaAa2oAssto");
    }

    @Override
    public Short getTgaEtaAa2oAsstoObj() {
        return ((Short)getTgaEtaAa2oAssto());
    }

    @Override
    public void setTgaEtaAa2oAsstoObj(Short tgaEtaAa2oAsstoObj) {
        setTgaEtaAa2oAssto(((short)tgaEtaAa2oAsstoObj));
    }

    @Override
    public short getTgaEtaAa3oAssto() {
        throw new FieldNotMappedException("tgaEtaAa3oAssto");
    }

    @Override
    public void setTgaEtaAa3oAssto(short tgaEtaAa3oAssto) {
        throw new FieldNotMappedException("tgaEtaAa3oAssto");
    }

    @Override
    public Short getTgaEtaAa3oAsstoObj() {
        return ((Short)getTgaEtaAa3oAssto());
    }

    @Override
    public void setTgaEtaAa3oAsstoObj(Short tgaEtaAa3oAsstoObj) {
        setTgaEtaAa3oAssto(((short)tgaEtaAa3oAsstoObj));
    }

    @Override
    public short getTgaEtaMm1oAssto() {
        throw new FieldNotMappedException("tgaEtaMm1oAssto");
    }

    @Override
    public void setTgaEtaMm1oAssto(short tgaEtaMm1oAssto) {
        throw new FieldNotMappedException("tgaEtaMm1oAssto");
    }

    @Override
    public Short getTgaEtaMm1oAsstoObj() {
        return ((Short)getTgaEtaMm1oAssto());
    }

    @Override
    public void setTgaEtaMm1oAsstoObj(Short tgaEtaMm1oAsstoObj) {
        setTgaEtaMm1oAssto(((short)tgaEtaMm1oAsstoObj));
    }

    @Override
    public short getTgaEtaMm2oAssto() {
        throw new FieldNotMappedException("tgaEtaMm2oAssto");
    }

    @Override
    public void setTgaEtaMm2oAssto(short tgaEtaMm2oAssto) {
        throw new FieldNotMappedException("tgaEtaMm2oAssto");
    }

    @Override
    public Short getTgaEtaMm2oAsstoObj() {
        return ((Short)getTgaEtaMm2oAssto());
    }

    @Override
    public void setTgaEtaMm2oAsstoObj(Short tgaEtaMm2oAsstoObj) {
        setTgaEtaMm2oAssto(((short)tgaEtaMm2oAsstoObj));
    }

    @Override
    public short getTgaEtaMm3oAssto() {
        throw new FieldNotMappedException("tgaEtaMm3oAssto");
    }

    @Override
    public void setTgaEtaMm3oAssto(short tgaEtaMm3oAssto) {
        throw new FieldNotMappedException("tgaEtaMm3oAssto");
    }

    @Override
    public Short getTgaEtaMm3oAsstoObj() {
        return ((Short)getTgaEtaMm3oAssto());
    }

    @Override
    public void setTgaEtaMm3oAsstoObj(Short tgaEtaMm3oAsstoObj) {
        setTgaEtaMm3oAssto(((short)tgaEtaMm3oAsstoObj));
    }

    @Override
    public char getTgaFlCarCont() {
        throw new FieldNotMappedException("tgaFlCarCont");
    }

    @Override
    public void setTgaFlCarCont(char tgaFlCarCont) {
        throw new FieldNotMappedException("tgaFlCarCont");
    }

    @Override
    public Character getTgaFlCarContObj() {
        return ((Character)getTgaFlCarCont());
    }

    @Override
    public void setTgaFlCarContObj(Character tgaFlCarContObj) {
        setTgaFlCarCont(((char)tgaFlCarContObj));
    }

    @Override
    public char getTgaFlImportiForz() {
        throw new FieldNotMappedException("tgaFlImportiForz");
    }

    @Override
    public void setTgaFlImportiForz(char tgaFlImportiForz) {
        throw new FieldNotMappedException("tgaFlImportiForz");
    }

    @Override
    public Character getTgaFlImportiForzObj() {
        return ((Character)getTgaFlImportiForz());
    }

    @Override
    public void setTgaFlImportiForzObj(Character tgaFlImportiForzObj) {
        setTgaFlImportiForz(((char)tgaFlImportiForzObj));
    }

    @Override
    public char getTgaFlProvForz() {
        throw new FieldNotMappedException("tgaFlProvForz");
    }

    @Override
    public void setTgaFlProvForz(char tgaFlProvForz) {
        throw new FieldNotMappedException("tgaFlProvForz");
    }

    @Override
    public Character getTgaFlProvForzObj() {
        return ((Character)getTgaFlProvForz());
    }

    @Override
    public void setTgaFlProvForzObj(Character tgaFlProvForzObj) {
        setTgaFlProvForz(((char)tgaFlProvForzObj));
    }

    @Override
    public String getTgaIbOgg() {
        throw new FieldNotMappedException("tgaIbOgg");
    }

    @Override
    public void setTgaIbOgg(String tgaIbOgg) {
        throw new FieldNotMappedException("tgaIbOgg");
    }

    @Override
    public String getTgaIbOggObj() {
        return getTgaIbOgg();
    }

    @Override
    public void setTgaIbOggObj(String tgaIbOggObj) {
        setTgaIbOgg(tgaIbOggObj);
    }

    @Override
    public int getTgaIdAdes() {
        throw new FieldNotMappedException("tgaIdAdes");
    }

    @Override
    public void setTgaIdAdes(int tgaIdAdes) {
        throw new FieldNotMappedException("tgaIdAdes");
    }

    @Override
    public int getTgaIdGar() {
        throw new FieldNotMappedException("tgaIdGar");
    }

    @Override
    public void setTgaIdGar(int tgaIdGar) {
        throw new FieldNotMappedException("tgaIdGar");
    }

    @Override
    public int getTgaIdMoviChiu() {
        throw new FieldNotMappedException("tgaIdMoviChiu");
    }

    @Override
    public void setTgaIdMoviChiu(int tgaIdMoviChiu) {
        throw new FieldNotMappedException("tgaIdMoviChiu");
    }

    @Override
    public Integer getTgaIdMoviChiuObj() {
        return ((Integer)getTgaIdMoviChiu());
    }

    @Override
    public void setTgaIdMoviChiuObj(Integer tgaIdMoviChiuObj) {
        setTgaIdMoviChiu(((int)tgaIdMoviChiuObj));
    }

    @Override
    public int getTgaIdMoviCrz() {
        throw new FieldNotMappedException("tgaIdMoviCrz");
    }

    @Override
    public void setTgaIdMoviCrz(int tgaIdMoviCrz) {
        throw new FieldNotMappedException("tgaIdMoviCrz");
    }

    @Override
    public int getTgaIdPoli() {
        throw new FieldNotMappedException("tgaIdPoli");
    }

    @Override
    public void setTgaIdPoli(int tgaIdPoli) {
        throw new FieldNotMappedException("tgaIdPoli");
    }

    @Override
    public int getTgaIdTrchDiGar() {
        throw new FieldNotMappedException("tgaIdTrchDiGar");
    }

    @Override
    public void setTgaIdTrchDiGar(int tgaIdTrchDiGar) {
        throw new FieldNotMappedException("tgaIdTrchDiGar");
    }

    @Override
    public AfDecimal getTgaImpAder() {
        throw new FieldNotMappedException("tgaImpAder");
    }

    @Override
    public void setTgaImpAder(AfDecimal tgaImpAder) {
        throw new FieldNotMappedException("tgaImpAder");
    }

    @Override
    public AfDecimal getTgaImpAderObj() {
        return getTgaImpAder();
    }

    @Override
    public void setTgaImpAderObj(AfDecimal tgaImpAderObj) {
        setTgaImpAder(new AfDecimal(tgaImpAderObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpAltSopr() {
        throw new FieldNotMappedException("tgaImpAltSopr");
    }

    @Override
    public void setTgaImpAltSopr(AfDecimal tgaImpAltSopr) {
        throw new FieldNotMappedException("tgaImpAltSopr");
    }

    @Override
    public AfDecimal getTgaImpAltSoprObj() {
        return getTgaImpAltSopr();
    }

    @Override
    public void setTgaImpAltSoprObj(AfDecimal tgaImpAltSoprObj) {
        setTgaImpAltSopr(new AfDecimal(tgaImpAltSoprObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpAz() {
        throw new FieldNotMappedException("tgaImpAz");
    }

    @Override
    public void setTgaImpAz(AfDecimal tgaImpAz) {
        throw new FieldNotMappedException("tgaImpAz");
    }

    @Override
    public AfDecimal getTgaImpAzObj() {
        return getTgaImpAz();
    }

    @Override
    public void setTgaImpAzObj(AfDecimal tgaImpAzObj) {
        setTgaImpAz(new AfDecimal(tgaImpAzObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpBns() {
        throw new FieldNotMappedException("tgaImpBns");
    }

    @Override
    public AfDecimal getTgaImpBnsAntic() {
        throw new FieldNotMappedException("tgaImpBnsAntic");
    }

    @Override
    public void setTgaImpBnsAntic(AfDecimal tgaImpBnsAntic) {
        throw new FieldNotMappedException("tgaImpBnsAntic");
    }

    @Override
    public AfDecimal getTgaImpBnsAnticObj() {
        return getTgaImpBnsAntic();
    }

    @Override
    public void setTgaImpBnsAnticObj(AfDecimal tgaImpBnsAnticObj) {
        setTgaImpBnsAntic(new AfDecimal(tgaImpBnsAnticObj, 15, 3));
    }

    @Override
    public void setTgaImpBns(AfDecimal tgaImpBns) {
        throw new FieldNotMappedException("tgaImpBns");
    }

    @Override
    public AfDecimal getTgaImpBnsObj() {
        return getTgaImpBns();
    }

    @Override
    public void setTgaImpBnsObj(AfDecimal tgaImpBnsObj) {
        setTgaImpBns(new AfDecimal(tgaImpBnsObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpCarAcq() {
        throw new FieldNotMappedException("tgaImpCarAcq");
    }

    @Override
    public void setTgaImpCarAcq(AfDecimal tgaImpCarAcq) {
        throw new FieldNotMappedException("tgaImpCarAcq");
    }

    @Override
    public AfDecimal getTgaImpCarAcqObj() {
        return getTgaImpCarAcq();
    }

    @Override
    public void setTgaImpCarAcqObj(AfDecimal tgaImpCarAcqObj) {
        setTgaImpCarAcq(new AfDecimal(tgaImpCarAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpCarGest() {
        throw new FieldNotMappedException("tgaImpCarGest");
    }

    @Override
    public void setTgaImpCarGest(AfDecimal tgaImpCarGest) {
        throw new FieldNotMappedException("tgaImpCarGest");
    }

    @Override
    public AfDecimal getTgaImpCarGestObj() {
        return getTgaImpCarGest();
    }

    @Override
    public void setTgaImpCarGestObj(AfDecimal tgaImpCarGestObj) {
        setTgaImpCarGest(new AfDecimal(tgaImpCarGestObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpCarInc() {
        throw new FieldNotMappedException("tgaImpCarInc");
    }

    @Override
    public void setTgaImpCarInc(AfDecimal tgaImpCarInc) {
        throw new FieldNotMappedException("tgaImpCarInc");
    }

    @Override
    public AfDecimal getTgaImpCarIncObj() {
        return getTgaImpCarInc();
    }

    @Override
    public void setTgaImpCarIncObj(AfDecimal tgaImpCarIncObj) {
        setTgaImpCarInc(new AfDecimal(tgaImpCarIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpScon() {
        throw new FieldNotMappedException("tgaImpScon");
    }

    @Override
    public void setTgaImpScon(AfDecimal tgaImpScon) {
        throw new FieldNotMappedException("tgaImpScon");
    }

    @Override
    public AfDecimal getTgaImpSconObj() {
        return getTgaImpScon();
    }

    @Override
    public void setTgaImpSconObj(AfDecimal tgaImpSconObj) {
        setTgaImpScon(new AfDecimal(tgaImpSconObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpSoprProf() {
        throw new FieldNotMappedException("tgaImpSoprProf");
    }

    @Override
    public void setTgaImpSoprProf(AfDecimal tgaImpSoprProf) {
        throw new FieldNotMappedException("tgaImpSoprProf");
    }

    @Override
    public AfDecimal getTgaImpSoprProfObj() {
        return getTgaImpSoprProf();
    }

    @Override
    public void setTgaImpSoprProfObj(AfDecimal tgaImpSoprProfObj) {
        setTgaImpSoprProf(new AfDecimal(tgaImpSoprProfObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpSoprSan() {
        throw new FieldNotMappedException("tgaImpSoprSan");
    }

    @Override
    public void setTgaImpSoprSan(AfDecimal tgaImpSoprSan) {
        throw new FieldNotMappedException("tgaImpSoprSan");
    }

    @Override
    public AfDecimal getTgaImpSoprSanObj() {
        return getTgaImpSoprSan();
    }

    @Override
    public void setTgaImpSoprSanObj(AfDecimal tgaImpSoprSanObj) {
        setTgaImpSoprSan(new AfDecimal(tgaImpSoprSanObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpSoprSpo() {
        throw new FieldNotMappedException("tgaImpSoprSpo");
    }

    @Override
    public void setTgaImpSoprSpo(AfDecimal tgaImpSoprSpo) {
        throw new FieldNotMappedException("tgaImpSoprSpo");
    }

    @Override
    public AfDecimal getTgaImpSoprSpoObj() {
        return getTgaImpSoprSpo();
    }

    @Override
    public void setTgaImpSoprSpoObj(AfDecimal tgaImpSoprSpoObj) {
        setTgaImpSoprSpo(new AfDecimal(tgaImpSoprSpoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpSoprTec() {
        throw new FieldNotMappedException("tgaImpSoprTec");
    }

    @Override
    public void setTgaImpSoprTec(AfDecimal tgaImpSoprTec) {
        throw new FieldNotMappedException("tgaImpSoprTec");
    }

    @Override
    public AfDecimal getTgaImpSoprTecObj() {
        return getTgaImpSoprTec();
    }

    @Override
    public void setTgaImpSoprTecObj(AfDecimal tgaImpSoprTecObj) {
        setTgaImpSoprTec(new AfDecimal(tgaImpSoprTecObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpTfr() {
        throw new FieldNotMappedException("tgaImpTfr");
    }

    @Override
    public void setTgaImpTfr(AfDecimal tgaImpTfr) {
        throw new FieldNotMappedException("tgaImpTfr");
    }

    @Override
    public AfDecimal getTgaImpTfrObj() {
        return getTgaImpTfr();
    }

    @Override
    public void setTgaImpTfrObj(AfDecimal tgaImpTfrObj) {
        setTgaImpTfr(new AfDecimal(tgaImpTfrObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpTfrStrc() {
        throw new FieldNotMappedException("tgaImpTfrStrc");
    }

    @Override
    public void setTgaImpTfrStrc(AfDecimal tgaImpTfrStrc) {
        throw new FieldNotMappedException("tgaImpTfrStrc");
    }

    @Override
    public AfDecimal getTgaImpTfrStrcObj() {
        return getTgaImpTfrStrc();
    }

    @Override
    public void setTgaImpTfrStrcObj(AfDecimal tgaImpTfrStrcObj) {
        setTgaImpTfrStrc(new AfDecimal(tgaImpTfrStrcObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpTrasfe() {
        throw new FieldNotMappedException("tgaImpTrasfe");
    }

    @Override
    public void setTgaImpTrasfe(AfDecimal tgaImpTrasfe) {
        throw new FieldNotMappedException("tgaImpTrasfe");
    }

    @Override
    public AfDecimal getTgaImpTrasfeObj() {
        return getTgaImpTrasfe();
    }

    @Override
    public void setTgaImpTrasfeObj(AfDecimal tgaImpTrasfeObj) {
        setTgaImpTrasfe(new AfDecimal(tgaImpTrasfeObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpVolo() {
        throw new FieldNotMappedException("tgaImpVolo");
    }

    @Override
    public void setTgaImpVolo(AfDecimal tgaImpVolo) {
        throw new FieldNotMappedException("tgaImpVolo");
    }

    @Override
    public AfDecimal getTgaImpVoloObj() {
        return getTgaImpVolo();
    }

    @Override
    public void setTgaImpVoloObj(AfDecimal tgaImpVoloObj) {
        setTgaImpVolo(new AfDecimal(tgaImpVoloObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbCommisInter() {
        throw new FieldNotMappedException("tgaImpbCommisInter");
    }

    @Override
    public void setTgaImpbCommisInter(AfDecimal tgaImpbCommisInter) {
        throw new FieldNotMappedException("tgaImpbCommisInter");
    }

    @Override
    public AfDecimal getTgaImpbCommisInterObj() {
        return getTgaImpbCommisInter();
    }

    @Override
    public void setTgaImpbCommisInterObj(AfDecimal tgaImpbCommisInterObj) {
        setTgaImpbCommisInter(new AfDecimal(tgaImpbCommisInterObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbProvAcq() {
        throw new FieldNotMappedException("tgaImpbProvAcq");
    }

    @Override
    public void setTgaImpbProvAcq(AfDecimal tgaImpbProvAcq) {
        throw new FieldNotMappedException("tgaImpbProvAcq");
    }

    @Override
    public AfDecimal getTgaImpbProvAcqObj() {
        return getTgaImpbProvAcq();
    }

    @Override
    public void setTgaImpbProvAcqObj(AfDecimal tgaImpbProvAcqObj) {
        setTgaImpbProvAcq(new AfDecimal(tgaImpbProvAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbProvInc() {
        throw new FieldNotMappedException("tgaImpbProvInc");
    }

    @Override
    public void setTgaImpbProvInc(AfDecimal tgaImpbProvInc) {
        throw new FieldNotMappedException("tgaImpbProvInc");
    }

    @Override
    public AfDecimal getTgaImpbProvIncObj() {
        return getTgaImpbProvInc();
    }

    @Override
    public void setTgaImpbProvIncObj(AfDecimal tgaImpbProvIncObj) {
        setTgaImpbProvInc(new AfDecimal(tgaImpbProvIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbProvRicor() {
        throw new FieldNotMappedException("tgaImpbProvRicor");
    }

    @Override
    public void setTgaImpbProvRicor(AfDecimal tgaImpbProvRicor) {
        throw new FieldNotMappedException("tgaImpbProvRicor");
    }

    @Override
    public AfDecimal getTgaImpbProvRicorObj() {
        return getTgaImpbProvRicor();
    }

    @Override
    public void setTgaImpbProvRicorObj(AfDecimal tgaImpbProvRicorObj) {
        setTgaImpbProvRicor(new AfDecimal(tgaImpbProvRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbRemunAss() {
        throw new FieldNotMappedException("tgaImpbRemunAss");
    }

    @Override
    public void setTgaImpbRemunAss(AfDecimal tgaImpbRemunAss) {
        throw new FieldNotMappedException("tgaImpbRemunAss");
    }

    @Override
    public AfDecimal getTgaImpbRemunAssObj() {
        return getTgaImpbRemunAss();
    }

    @Override
    public void setTgaImpbRemunAssObj(AfDecimal tgaImpbRemunAssObj) {
        setTgaImpbRemunAss(new AfDecimal(tgaImpbRemunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000() {
        throw new FieldNotMappedException("tgaImpbVisEnd2000");
    }

    @Override
    public void setTgaImpbVisEnd2000(AfDecimal tgaImpbVisEnd2000) {
        throw new FieldNotMappedException("tgaImpbVisEnd2000");
    }

    @Override
    public AfDecimal getTgaImpbVisEnd2000Obj() {
        return getTgaImpbVisEnd2000();
    }

    @Override
    public void setTgaImpbVisEnd2000Obj(AfDecimal tgaImpbVisEnd2000Obj) {
        setTgaImpbVisEnd2000(new AfDecimal(tgaImpbVisEnd2000Obj, 15, 3));
    }

    @Override
    public AfDecimal getTgaIncrPre() {
        throw new FieldNotMappedException("tgaIncrPre");
    }

    @Override
    public void setTgaIncrPre(AfDecimal tgaIncrPre) {
        throw new FieldNotMappedException("tgaIncrPre");
    }

    @Override
    public AfDecimal getTgaIncrPreObj() {
        return getTgaIncrPre();
    }

    @Override
    public void setTgaIncrPreObj(AfDecimal tgaIncrPreObj) {
        setTgaIncrPre(new AfDecimal(tgaIncrPreObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaIncrPrstz() {
        throw new FieldNotMappedException("tgaIncrPrstz");
    }

    @Override
    public void setTgaIncrPrstz(AfDecimal tgaIncrPrstz) {
        throw new FieldNotMappedException("tgaIncrPrstz");
    }

    @Override
    public AfDecimal getTgaIncrPrstzObj() {
        return getTgaIncrPrstz();
    }

    @Override
    public void setTgaIncrPrstzObj(AfDecimal tgaIncrPrstzObj) {
        setTgaIncrPrstz(new AfDecimal(tgaIncrPrstzObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaIntrMora() {
        throw new FieldNotMappedException("tgaIntrMora");
    }

    @Override
    public void setTgaIntrMora(AfDecimal tgaIntrMora) {
        throw new FieldNotMappedException("tgaIntrMora");
    }

    @Override
    public AfDecimal getTgaIntrMoraObj() {
        return getTgaIntrMora();
    }

    @Override
    public void setTgaIntrMoraObj(AfDecimal tgaIntrMoraObj) {
        setTgaIntrMora(new AfDecimal(tgaIntrMoraObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaManfeeAntic() {
        throw new FieldNotMappedException("tgaManfeeAntic");
    }

    @Override
    public void setTgaManfeeAntic(AfDecimal tgaManfeeAntic) {
        throw new FieldNotMappedException("tgaManfeeAntic");
    }

    @Override
    public AfDecimal getTgaManfeeAnticObj() {
        return getTgaManfeeAntic();
    }

    @Override
    public void setTgaManfeeAnticObj(AfDecimal tgaManfeeAnticObj) {
        setTgaManfeeAntic(new AfDecimal(tgaManfeeAnticObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaManfeeRicor() {
        throw new FieldNotMappedException("tgaManfeeRicor");
    }

    @Override
    public void setTgaManfeeRicor(AfDecimal tgaManfeeRicor) {
        throw new FieldNotMappedException("tgaManfeeRicor");
    }

    @Override
    public AfDecimal getTgaManfeeRicorObj() {
        return getTgaManfeeRicor();
    }

    @Override
    public void setTgaManfeeRicorObj(AfDecimal tgaManfeeRicorObj) {
        setTgaManfeeRicor(new AfDecimal(tgaManfeeRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaMatuEnd2000() {
        throw new FieldNotMappedException("tgaMatuEnd2000");
    }

    @Override
    public void setTgaMatuEnd2000(AfDecimal tgaMatuEnd2000) {
        throw new FieldNotMappedException("tgaMatuEnd2000");
    }

    @Override
    public AfDecimal getTgaMatuEnd2000Obj() {
        return getTgaMatuEnd2000();
    }

    @Override
    public void setTgaMatuEnd2000Obj(AfDecimal tgaMatuEnd2000Obj) {
        setTgaMatuEnd2000(new AfDecimal(tgaMatuEnd2000Obj, 15, 3));
    }

    @Override
    public AfDecimal getTgaMinGarto() {
        throw new FieldNotMappedException("tgaMinGarto");
    }

    @Override
    public void setTgaMinGarto(AfDecimal tgaMinGarto) {
        throw new FieldNotMappedException("tgaMinGarto");
    }

    @Override
    public AfDecimal getTgaMinGartoObj() {
        return getTgaMinGarto();
    }

    @Override
    public void setTgaMinGartoObj(AfDecimal tgaMinGartoObj) {
        setTgaMinGarto(new AfDecimal(tgaMinGartoObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaMinTrnut() {
        throw new FieldNotMappedException("tgaMinTrnut");
    }

    @Override
    public void setTgaMinTrnut(AfDecimal tgaMinTrnut) {
        throw new FieldNotMappedException("tgaMinTrnut");
    }

    @Override
    public AfDecimal getTgaMinTrnutObj() {
        return getTgaMinTrnut();
    }

    @Override
    public void setTgaMinTrnutObj(AfDecimal tgaMinTrnutObj) {
        setTgaMinTrnut(new AfDecimal(tgaMinTrnutObj, 14, 9));
    }

    @Override
    public String getTgaModCalc() {
        throw new FieldNotMappedException("tgaModCalc");
    }

    @Override
    public void setTgaModCalc(String tgaModCalc) {
        throw new FieldNotMappedException("tgaModCalc");
    }

    @Override
    public String getTgaModCalcObj() {
        return getTgaModCalc();
    }

    @Override
    public void setTgaModCalcObj(String tgaModCalcObj) {
        setTgaModCalc(tgaModCalcObj);
    }

    @Override
    public int getTgaNumGgRival() {
        throw new FieldNotMappedException("tgaNumGgRival");
    }

    @Override
    public void setTgaNumGgRival(int tgaNumGgRival) {
        throw new FieldNotMappedException("tgaNumGgRival");
    }

    @Override
    public Integer getTgaNumGgRivalObj() {
        return ((Integer)getTgaNumGgRival());
    }

    @Override
    public void setTgaNumGgRivalObj(Integer tgaNumGgRivalObj) {
        setTgaNumGgRival(((int)tgaNumGgRivalObj));
    }

    @Override
    public AfDecimal getTgaOldTsTec() {
        throw new FieldNotMappedException("tgaOldTsTec");
    }

    @Override
    public void setTgaOldTsTec(AfDecimal tgaOldTsTec) {
        throw new FieldNotMappedException("tgaOldTsTec");
    }

    @Override
    public AfDecimal getTgaOldTsTecObj() {
        return getTgaOldTsTec();
    }

    @Override
    public void setTgaOldTsTecObj(AfDecimal tgaOldTsTecObj) {
        setTgaOldTsTec(new AfDecimal(tgaOldTsTecObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaPcCommisGest() {
        throw new FieldNotMappedException("tgaPcCommisGest");
    }

    @Override
    public void setTgaPcCommisGest(AfDecimal tgaPcCommisGest) {
        throw new FieldNotMappedException("tgaPcCommisGest");
    }

    @Override
    public AfDecimal getTgaPcCommisGestObj() {
        return getTgaPcCommisGest();
    }

    @Override
    public void setTgaPcCommisGestObj(AfDecimal tgaPcCommisGestObj) {
        setTgaPcCommisGest(new AfDecimal(tgaPcCommisGestObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaPcIntrRiat() {
        throw new FieldNotMappedException("tgaPcIntrRiat");
    }

    @Override
    public void setTgaPcIntrRiat(AfDecimal tgaPcIntrRiat) {
        throw new FieldNotMappedException("tgaPcIntrRiat");
    }

    @Override
    public AfDecimal getTgaPcIntrRiatObj() {
        return getTgaPcIntrRiat();
    }

    @Override
    public void setTgaPcIntrRiatObj(AfDecimal tgaPcIntrRiatObj) {
        setTgaPcIntrRiat(new AfDecimal(tgaPcIntrRiatObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaPcRetr() {
        throw new FieldNotMappedException("tgaPcRetr");
    }

    @Override
    public void setTgaPcRetr(AfDecimal tgaPcRetr) {
        throw new FieldNotMappedException("tgaPcRetr");
    }

    @Override
    public AfDecimal getTgaPcRetrObj() {
        return getTgaPcRetr();
    }

    @Override
    public void setTgaPcRetrObj(AfDecimal tgaPcRetrObj) {
        setTgaPcRetr(new AfDecimal(tgaPcRetrObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaPcRipPre() {
        throw new FieldNotMappedException("tgaPcRipPre");
    }

    @Override
    public void setTgaPcRipPre(AfDecimal tgaPcRipPre) {
        throw new FieldNotMappedException("tgaPcRipPre");
    }

    @Override
    public AfDecimal getTgaPcRipPreObj() {
        return getTgaPcRipPre();
    }

    @Override
    public void setTgaPcRipPreObj(AfDecimal tgaPcRipPreObj) {
        setTgaPcRipPre(new AfDecimal(tgaPcRipPreObj, 6, 3));
    }

    @Override
    public AfDecimal getTgaPreAttDiTrch() {
        throw new FieldNotMappedException("tgaPreAttDiTrch");
    }

    @Override
    public void setTgaPreAttDiTrch(AfDecimal tgaPreAttDiTrch) {
        throw new FieldNotMappedException("tgaPreAttDiTrch");
    }

    @Override
    public AfDecimal getTgaPreAttDiTrchObj() {
        return getTgaPreAttDiTrch();
    }

    @Override
    public void setTgaPreAttDiTrchObj(AfDecimal tgaPreAttDiTrchObj) {
        setTgaPreAttDiTrch(new AfDecimal(tgaPreAttDiTrchObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreCasoMor() {
        throw new FieldNotMappedException("tgaPreCasoMor");
    }

    @Override
    public void setTgaPreCasoMor(AfDecimal tgaPreCasoMor) {
        throw new FieldNotMappedException("tgaPreCasoMor");
    }

    @Override
    public AfDecimal getTgaPreCasoMorObj() {
        return getTgaPreCasoMor();
    }

    @Override
    public void setTgaPreCasoMorObj(AfDecimal tgaPreCasoMorObj) {
        setTgaPreCasoMor(new AfDecimal(tgaPreCasoMorObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreIniNet() {
        throw new FieldNotMappedException("tgaPreIniNet");
    }

    @Override
    public void setTgaPreIniNet(AfDecimal tgaPreIniNet) {
        throw new FieldNotMappedException("tgaPreIniNet");
    }

    @Override
    public AfDecimal getTgaPreIniNetObj() {
        return getTgaPreIniNet();
    }

    @Override
    public void setTgaPreIniNetObj(AfDecimal tgaPreIniNetObj) {
        setTgaPreIniNet(new AfDecimal(tgaPreIniNetObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreInvrioIni() {
        throw new FieldNotMappedException("tgaPreInvrioIni");
    }

    @Override
    public void setTgaPreInvrioIni(AfDecimal tgaPreInvrioIni) {
        throw new FieldNotMappedException("tgaPreInvrioIni");
    }

    @Override
    public AfDecimal getTgaPreInvrioIniObj() {
        return getTgaPreInvrioIni();
    }

    @Override
    public void setTgaPreInvrioIniObj(AfDecimal tgaPreInvrioIniObj) {
        setTgaPreInvrioIni(new AfDecimal(tgaPreInvrioIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreInvrioUlt() {
        throw new FieldNotMappedException("tgaPreInvrioUlt");
    }

    @Override
    public void setTgaPreInvrioUlt(AfDecimal tgaPreInvrioUlt) {
        throw new FieldNotMappedException("tgaPreInvrioUlt");
    }

    @Override
    public AfDecimal getTgaPreInvrioUltObj() {
        return getTgaPreInvrioUlt();
    }

    @Override
    public void setTgaPreInvrioUltObj(AfDecimal tgaPreInvrioUltObj) {
        setTgaPreInvrioUlt(new AfDecimal(tgaPreInvrioUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreLrd() {
        throw new FieldNotMappedException("tgaPreLrd");
    }

    @Override
    public void setTgaPreLrd(AfDecimal tgaPreLrd) {
        throw new FieldNotMappedException("tgaPreLrd");
    }

    @Override
    public AfDecimal getTgaPreLrdObj() {
        return getTgaPreLrd();
    }

    @Override
    public void setTgaPreLrdObj(AfDecimal tgaPreLrdObj) {
        setTgaPreLrd(new AfDecimal(tgaPreLrdObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrePattuito() {
        throw new FieldNotMappedException("tgaPrePattuito");
    }

    @Override
    public void setTgaPrePattuito(AfDecimal tgaPrePattuito) {
        throw new FieldNotMappedException("tgaPrePattuito");
    }

    @Override
    public AfDecimal getTgaPrePattuitoObj() {
        return getTgaPrePattuito();
    }

    @Override
    public void setTgaPrePattuitoObj(AfDecimal tgaPrePattuitoObj) {
        setTgaPrePattuito(new AfDecimal(tgaPrePattuitoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrePpIni() {
        throw new FieldNotMappedException("tgaPrePpIni");
    }

    @Override
    public void setTgaPrePpIni(AfDecimal tgaPrePpIni) {
        throw new FieldNotMappedException("tgaPrePpIni");
    }

    @Override
    public AfDecimal getTgaPrePpIniObj() {
        return getTgaPrePpIni();
    }

    @Override
    public void setTgaPrePpIniObj(AfDecimal tgaPrePpIniObj) {
        setTgaPrePpIni(new AfDecimal(tgaPrePpIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrePpUlt() {
        throw new FieldNotMappedException("tgaPrePpUlt");
    }

    @Override
    public void setTgaPrePpUlt(AfDecimal tgaPrePpUlt) {
        throw new FieldNotMappedException("tgaPrePpUlt");
    }

    @Override
    public AfDecimal getTgaPrePpUltObj() {
        return getTgaPrePpUlt();
    }

    @Override
    public void setTgaPrePpUltObj(AfDecimal tgaPrePpUltObj) {
        setTgaPrePpUlt(new AfDecimal(tgaPrePpUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreRivto() {
        throw new FieldNotMappedException("tgaPreRivto");
    }

    @Override
    public void setTgaPreRivto(AfDecimal tgaPreRivto) {
        throw new FieldNotMappedException("tgaPreRivto");
    }

    @Override
    public AfDecimal getTgaPreRivtoObj() {
        return getTgaPreRivto();
    }

    @Override
    public void setTgaPreRivtoObj(AfDecimal tgaPreRivtoObj) {
        setTgaPreRivto(new AfDecimal(tgaPreRivtoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreStab() {
        throw new FieldNotMappedException("tgaPreStab");
    }

    @Override
    public void setTgaPreStab(AfDecimal tgaPreStab) {
        throw new FieldNotMappedException("tgaPreStab");
    }

    @Override
    public AfDecimal getTgaPreStabObj() {
        return getTgaPreStab();
    }

    @Override
    public void setTgaPreStabObj(AfDecimal tgaPreStabObj) {
        setTgaPreStab(new AfDecimal(tgaPreStabObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreTariIni() {
        throw new FieldNotMappedException("tgaPreTariIni");
    }

    @Override
    public void setTgaPreTariIni(AfDecimal tgaPreTariIni) {
        throw new FieldNotMappedException("tgaPreTariIni");
    }

    @Override
    public AfDecimal getTgaPreTariIniObj() {
        return getTgaPreTariIni();
    }

    @Override
    public void setTgaPreTariIniObj(AfDecimal tgaPreTariIniObj) {
        setTgaPreTariIni(new AfDecimal(tgaPreTariIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreTariUlt() {
        throw new FieldNotMappedException("tgaPreTariUlt");
    }

    @Override
    public void setTgaPreTariUlt(AfDecimal tgaPreTariUlt) {
        throw new FieldNotMappedException("tgaPreTariUlt");
    }

    @Override
    public AfDecimal getTgaPreTariUltObj() {
        return getTgaPreTariUlt();
    }

    @Override
    public void setTgaPreTariUltObj(AfDecimal tgaPreTariUltObj) {
        setTgaPreTariUlt(new AfDecimal(tgaPreTariUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPreUniRivto() {
        throw new FieldNotMappedException("tgaPreUniRivto");
    }

    @Override
    public void setTgaPreUniRivto(AfDecimal tgaPreUniRivto) {
        throw new FieldNotMappedException("tgaPreUniRivto");
    }

    @Override
    public AfDecimal getTgaPreUniRivtoObj() {
        return getTgaPreUniRivto();
    }

    @Override
    public void setTgaPreUniRivtoObj(AfDecimal tgaPreUniRivtoObj) {
        setTgaPreUniRivto(new AfDecimal(tgaPreUniRivtoObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaProv1aaAcq() {
        throw new FieldNotMappedException("tgaProv1aaAcq");
    }

    @Override
    public void setTgaProv1aaAcq(AfDecimal tgaProv1aaAcq) {
        throw new FieldNotMappedException("tgaProv1aaAcq");
    }

    @Override
    public AfDecimal getTgaProv1aaAcqObj() {
        return getTgaProv1aaAcq();
    }

    @Override
    public void setTgaProv1aaAcqObj(AfDecimal tgaProv1aaAcqObj) {
        setTgaProv1aaAcq(new AfDecimal(tgaProv1aaAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaProv2aaAcq() {
        throw new FieldNotMappedException("tgaProv2aaAcq");
    }

    @Override
    public void setTgaProv2aaAcq(AfDecimal tgaProv2aaAcq) {
        throw new FieldNotMappedException("tgaProv2aaAcq");
    }

    @Override
    public AfDecimal getTgaProv2aaAcqObj() {
        return getTgaProv2aaAcq();
    }

    @Override
    public void setTgaProv2aaAcqObj(AfDecimal tgaProv2aaAcqObj) {
        setTgaProv2aaAcq(new AfDecimal(tgaProv2aaAcqObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaProvInc() {
        throw new FieldNotMappedException("tgaProvInc");
    }

    @Override
    public void setTgaProvInc(AfDecimal tgaProvInc) {
        throw new FieldNotMappedException("tgaProvInc");
    }

    @Override
    public AfDecimal getTgaProvIncObj() {
        return getTgaProvInc();
    }

    @Override
    public void setTgaProvIncObj(AfDecimal tgaProvIncObj) {
        setTgaProvInc(new AfDecimal(tgaProvIncObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaProvRicor() {
        throw new FieldNotMappedException("tgaProvRicor");
    }

    @Override
    public void setTgaProvRicor(AfDecimal tgaProvRicor) {
        throw new FieldNotMappedException("tgaProvRicor");
    }

    @Override
    public AfDecimal getTgaProvRicorObj() {
        return getTgaProvRicor();
    }

    @Override
    public void setTgaProvRicorObj(AfDecimal tgaProvRicorObj) {
        setTgaProvRicor(new AfDecimal(tgaProvRicorObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzAggIni() {
        throw new FieldNotMappedException("tgaPrstzAggIni");
    }

    @Override
    public void setTgaPrstzAggIni(AfDecimal tgaPrstzAggIni) {
        throw new FieldNotMappedException("tgaPrstzAggIni");
    }

    @Override
    public AfDecimal getTgaPrstzAggIniObj() {
        return getTgaPrstzAggIni();
    }

    @Override
    public void setTgaPrstzAggIniObj(AfDecimal tgaPrstzAggIniObj) {
        setTgaPrstzAggIni(new AfDecimal(tgaPrstzAggIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzAggUlt() {
        throw new FieldNotMappedException("tgaPrstzAggUlt");
    }

    @Override
    public void setTgaPrstzAggUlt(AfDecimal tgaPrstzAggUlt) {
        throw new FieldNotMappedException("tgaPrstzAggUlt");
    }

    @Override
    public AfDecimal getTgaPrstzAggUltObj() {
        return getTgaPrstzAggUlt();
    }

    @Override
    public void setTgaPrstzAggUltObj(AfDecimal tgaPrstzAggUltObj) {
        setTgaPrstzAggUlt(new AfDecimal(tgaPrstzAggUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzIni() {
        throw new FieldNotMappedException("tgaPrstzIni");
    }

    @Override
    public void setTgaPrstzIni(AfDecimal tgaPrstzIni) {
        throw new FieldNotMappedException("tgaPrstzIni");
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfis() {
        throw new FieldNotMappedException("tgaPrstzIniNewfis");
    }

    @Override
    public void setTgaPrstzIniNewfis(AfDecimal tgaPrstzIniNewfis) {
        throw new FieldNotMappedException("tgaPrstzIniNewfis");
    }

    @Override
    public AfDecimal getTgaPrstzIniNewfisObj() {
        return getTgaPrstzIniNewfis();
    }

    @Override
    public void setTgaPrstzIniNewfisObj(AfDecimal tgaPrstzIniNewfisObj) {
        setTgaPrstzIniNewfis(new AfDecimal(tgaPrstzIniNewfisObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzIniNforz() {
        throw new FieldNotMappedException("tgaPrstzIniNforz");
    }

    @Override
    public void setTgaPrstzIniNforz(AfDecimal tgaPrstzIniNforz) {
        throw new FieldNotMappedException("tgaPrstzIniNforz");
    }

    @Override
    public AfDecimal getTgaPrstzIniNforzObj() {
        return getTgaPrstzIniNforz();
    }

    @Override
    public void setTgaPrstzIniNforzObj(AfDecimal tgaPrstzIniNforzObj) {
        setTgaPrstzIniNforz(new AfDecimal(tgaPrstzIniNforzObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzIniObj() {
        return getTgaPrstzIni();
    }

    @Override
    public void setTgaPrstzIniObj(AfDecimal tgaPrstzIniObj) {
        setTgaPrstzIni(new AfDecimal(tgaPrstzIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzIniStab() {
        throw new FieldNotMappedException("tgaPrstzIniStab");
    }

    @Override
    public void setTgaPrstzIniStab(AfDecimal tgaPrstzIniStab) {
        throw new FieldNotMappedException("tgaPrstzIniStab");
    }

    @Override
    public AfDecimal getTgaPrstzIniStabObj() {
        return getTgaPrstzIniStab();
    }

    @Override
    public void setTgaPrstzIniStabObj(AfDecimal tgaPrstzIniStabObj) {
        setTgaPrstzIniStab(new AfDecimal(tgaPrstzIniStabObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzRidIni() {
        throw new FieldNotMappedException("tgaPrstzRidIni");
    }

    @Override
    public void setTgaPrstzRidIni(AfDecimal tgaPrstzRidIni) {
        throw new FieldNotMappedException("tgaPrstzRidIni");
    }

    @Override
    public AfDecimal getTgaPrstzRidIniObj() {
        return getTgaPrstzRidIni();
    }

    @Override
    public void setTgaPrstzRidIniObj(AfDecimal tgaPrstzRidIniObj) {
        setTgaPrstzRidIni(new AfDecimal(tgaPrstzRidIniObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaPrstzUlt() {
        throw new FieldNotMappedException("tgaPrstzUlt");
    }

    @Override
    public void setTgaPrstzUlt(AfDecimal tgaPrstzUlt) {
        throw new FieldNotMappedException("tgaPrstzUlt");
    }

    @Override
    public AfDecimal getTgaPrstzUltObj() {
        return getTgaPrstzUlt();
    }

    @Override
    public void setTgaPrstzUltObj(AfDecimal tgaPrstzUltObj) {
        setTgaPrstzUlt(new AfDecimal(tgaPrstzUltObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaRatLrd() {
        throw new FieldNotMappedException("tgaRatLrd");
    }

    @Override
    public void setTgaRatLrd(AfDecimal tgaRatLrd) {
        throw new FieldNotMappedException("tgaRatLrd");
    }

    @Override
    public AfDecimal getTgaRatLrdObj() {
        return getTgaRatLrd();
    }

    @Override
    public void setTgaRatLrdObj(AfDecimal tgaRatLrdObj) {
        setTgaRatLrd(new AfDecimal(tgaRatLrdObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaRemunAss() {
        throw new FieldNotMappedException("tgaRemunAss");
    }

    @Override
    public void setTgaRemunAss(AfDecimal tgaRemunAss) {
        throw new FieldNotMappedException("tgaRemunAss");
    }

    @Override
    public AfDecimal getTgaRemunAssObj() {
        return getTgaRemunAss();
    }

    @Override
    public void setTgaRemunAssObj(AfDecimal tgaRemunAssObj) {
        setTgaRemunAss(new AfDecimal(tgaRemunAssObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0() {
        throw new FieldNotMappedException("tgaRenIniTsTec0");
    }

    @Override
    public void setTgaRenIniTsTec0(AfDecimal tgaRenIniTsTec0) {
        throw new FieldNotMappedException("tgaRenIniTsTec0");
    }

    @Override
    public AfDecimal getTgaRenIniTsTec0Obj() {
        return getTgaRenIniTsTec0();
    }

    @Override
    public void setTgaRenIniTsTec0Obj(AfDecimal tgaRenIniTsTec0Obj) {
        setTgaRenIniTsTec0(new AfDecimal(tgaRenIniTsTec0Obj, 15, 3));
    }

    @Override
    public AfDecimal getTgaRendtoLrd() {
        throw new FieldNotMappedException("tgaRendtoLrd");
    }

    @Override
    public void setTgaRendtoLrd(AfDecimal tgaRendtoLrd) {
        throw new FieldNotMappedException("tgaRendtoLrd");
    }

    @Override
    public AfDecimal getTgaRendtoLrdObj() {
        return getTgaRendtoLrd();
    }

    @Override
    public void setTgaRendtoLrdObj(AfDecimal tgaRendtoLrdObj) {
        setTgaRendtoLrd(new AfDecimal(tgaRendtoLrdObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaRendtoRetr() {
        throw new FieldNotMappedException("tgaRendtoRetr");
    }

    @Override
    public void setTgaRendtoRetr(AfDecimal tgaRendtoRetr) {
        throw new FieldNotMappedException("tgaRendtoRetr");
    }

    @Override
    public AfDecimal getTgaRendtoRetrObj() {
        return getTgaRendtoRetr();
    }

    @Override
    public void setTgaRendtoRetrObj(AfDecimal tgaRendtoRetrObj) {
        setTgaRendtoRetr(new AfDecimal(tgaRendtoRetrObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaRisMat() {
        throw new FieldNotMappedException("tgaRisMat");
    }

    @Override
    public void setTgaRisMat(AfDecimal tgaRisMat) {
        throw new FieldNotMappedException("tgaRisMat");
    }

    @Override
    public AfDecimal getTgaRisMatObj() {
        return getTgaRisMat();
    }

    @Override
    public void setTgaRisMatObj(AfDecimal tgaRisMatObj) {
        setTgaRisMat(new AfDecimal(tgaRisMatObj, 15, 3));
    }

    @Override
    public char getTgaTpAdegAbb() {
        throw new FieldNotMappedException("tgaTpAdegAbb");
    }

    @Override
    public void setTgaTpAdegAbb(char tgaTpAdegAbb) {
        throw new FieldNotMappedException("tgaTpAdegAbb");
    }

    @Override
    public Character getTgaTpAdegAbbObj() {
        return ((Character)getTgaTpAdegAbb());
    }

    @Override
    public void setTgaTpAdegAbbObj(Character tgaTpAdegAbbObj) {
        setTgaTpAdegAbb(((char)tgaTpAdegAbbObj));
    }

    @Override
    public String getTgaTpManfeeAppl() {
        throw new FieldNotMappedException("tgaTpManfeeAppl");
    }

    @Override
    public void setTgaTpManfeeAppl(String tgaTpManfeeAppl) {
        throw new FieldNotMappedException("tgaTpManfeeAppl");
    }

    @Override
    public String getTgaTpManfeeApplObj() {
        return getTgaTpManfeeAppl();
    }

    @Override
    public void setTgaTpManfeeApplObj(String tgaTpManfeeApplObj) {
        setTgaTpManfeeAppl(tgaTpManfeeApplObj);
    }

    @Override
    public String getTgaTpRgmFisc() {
        throw new FieldNotMappedException("tgaTpRgmFisc");
    }

    @Override
    public void setTgaTpRgmFisc(String tgaTpRgmFisc) {
        throw new FieldNotMappedException("tgaTpRgmFisc");
    }

    @Override
    public String getTgaTpRival() {
        throw new FieldNotMappedException("tgaTpRival");
    }

    @Override
    public void setTgaTpRival(String tgaTpRival) {
        throw new FieldNotMappedException("tgaTpRival");
    }

    @Override
    public String getTgaTpRivalObj() {
        return getTgaTpRival();
    }

    @Override
    public void setTgaTpRivalObj(String tgaTpRivalObj) {
        setTgaTpRival(tgaTpRivalObj);
    }

    @Override
    public String getTgaTpTrch() {
        throw new FieldNotMappedException("tgaTpTrch");
    }

    @Override
    public void setTgaTpTrch(String tgaTpTrch) {
        throw new FieldNotMappedException("tgaTpTrch");
    }

    @Override
    public AfDecimal getTgaTsRivalFis() {
        throw new FieldNotMappedException("tgaTsRivalFis");
    }

    @Override
    public void setTgaTsRivalFis(AfDecimal tgaTsRivalFis) {
        throw new FieldNotMappedException("tgaTsRivalFis");
    }

    @Override
    public AfDecimal getTgaTsRivalFisObj() {
        return getTgaTsRivalFis();
    }

    @Override
    public void setTgaTsRivalFisObj(AfDecimal tgaTsRivalFisObj) {
        setTgaTsRivalFis(new AfDecimal(tgaTsRivalFisObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaTsRivalIndiciz() {
        throw new FieldNotMappedException("tgaTsRivalIndiciz");
    }

    @Override
    public void setTgaTsRivalIndiciz(AfDecimal tgaTsRivalIndiciz) {
        throw new FieldNotMappedException("tgaTsRivalIndiciz");
    }

    @Override
    public AfDecimal getTgaTsRivalIndicizObj() {
        return getTgaTsRivalIndiciz();
    }

    @Override
    public void setTgaTsRivalIndicizObj(AfDecimal tgaTsRivalIndicizObj) {
        setTgaTsRivalIndiciz(new AfDecimal(tgaTsRivalIndicizObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaTsRivalNet() {
        throw new FieldNotMappedException("tgaTsRivalNet");
    }

    @Override
    public void setTgaTsRivalNet(AfDecimal tgaTsRivalNet) {
        throw new FieldNotMappedException("tgaTsRivalNet");
    }

    @Override
    public AfDecimal getTgaTsRivalNetObj() {
        return getTgaTsRivalNet();
    }

    @Override
    public void setTgaTsRivalNetObj(AfDecimal tgaTsRivalNetObj) {
        setTgaTsRivalNet(new AfDecimal(tgaTsRivalNetObj, 14, 9));
    }

    @Override
    public AfDecimal getTgaVisEnd2000() {
        throw new FieldNotMappedException("tgaVisEnd2000");
    }

    @Override
    public void setTgaVisEnd2000(AfDecimal tgaVisEnd2000) {
        throw new FieldNotMappedException("tgaVisEnd2000");
    }

    @Override
    public AfDecimal getTgaVisEnd2000Nforz() {
        throw new FieldNotMappedException("tgaVisEnd2000Nforz");
    }

    @Override
    public void setTgaVisEnd2000Nforz(AfDecimal tgaVisEnd2000Nforz) {
        throw new FieldNotMappedException("tgaVisEnd2000Nforz");
    }

    @Override
    public AfDecimal getTgaVisEnd2000NforzObj() {
        return getTgaVisEnd2000Nforz();
    }

    @Override
    public void setTgaVisEnd2000NforzObj(AfDecimal tgaVisEnd2000NforzObj) {
        setTgaVisEnd2000Nforz(new AfDecimal(tgaVisEnd2000NforzObj, 15, 3));
    }

    @Override
    public AfDecimal getTgaVisEnd2000Obj() {
        return getTgaVisEnd2000();
    }

    @Override
    public void setTgaVisEnd2000Obj(AfDecimal tgaVisEnd2000Obj) {
        setTgaVisEnd2000(new AfDecimal(tgaVisEnd2000Obj, 15, 3));
    }

    @Override
    public int getWkIdAdesA() {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public void setWkIdAdesA(int wkIdAdesA) {
        throw new FieldNotMappedException("wkIdAdesA");
    }

    @Override
    public int getWkIdAdesDa() {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public void setWkIdAdesDa(int wkIdAdesDa) {
        throw new FieldNotMappedException("wkIdAdesDa");
    }

    @Override
    public int getWkIdGarA() {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public void setWkIdGarA(int wkIdGarA) {
        throw new FieldNotMappedException("wkIdGarA");
    }

    @Override
    public int getWkIdGarDa() {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public void setWkIdGarDa(int wkIdGarDa) {
        throw new FieldNotMappedException("wkIdGarDa");
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        throw new FieldNotMappedException("wsDataInizioEffettoDb");
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        throw new FieldNotMappedException("wsTsCompetenza");
    }

    //==== INNER CLASSES ====
    public static class Len {

        //==== PROPERTIES ====
        public static final int WS_ID_MOVI_CRZ = 9;

        //==== CONSTRUCTORS ====
        private Len() {
        }
    }
}
