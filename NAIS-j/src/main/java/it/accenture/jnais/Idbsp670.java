package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.EstPoliCpiPrDao;
import it.accenture.jnais.commons.data.to.IEstPoliCpiPr;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.EstPoliCpiPrIdbsp670;
import it.accenture.jnais.ws.Idbsp670Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.P67Amm1aRat;
import it.accenture.jnais.ws.redefines.P67AmmRatEnd;
import it.accenture.jnais.ws.redefines.P67CptFin;
import it.accenture.jnais.ws.redefines.P67Dt1oUtlzCRev;
import it.accenture.jnais.ws.redefines.P67DtEndFinanz;
import it.accenture.jnais.ws.redefines.P67DtErogFinanz;
import it.accenture.jnais.ws.redefines.P67DtEstFinanz;
import it.accenture.jnais.ws.redefines.P67DtManCop;
import it.accenture.jnais.ws.redefines.P67DtScad1aRat;
import it.accenture.jnais.ws.redefines.P67DtScadCop;
import it.accenture.jnais.ws.redefines.P67DtStipulaFinanz;
import it.accenture.jnais.ws.redefines.P67DurMmFinanz;
import it.accenture.jnais.ws.redefines.P67GgDelMmScadRat;
import it.accenture.jnais.ws.redefines.P67IdMoviChiu;
import it.accenture.jnais.ws.redefines.P67ImpAssto;
import it.accenture.jnais.ws.redefines.P67ImpCanoneAntic;
import it.accenture.jnais.ws.redefines.P67ImpDebRes;
import it.accenture.jnais.ws.redefines.P67ImpFinRevolving;
import it.accenture.jnais.ws.redefines.P67ImpRatFinanz;
import it.accenture.jnais.ws.redefines.P67ImpRatRevolving;
import it.accenture.jnais.ws.redefines.P67ImpUtilCRev;
import it.accenture.jnais.ws.redefines.P67MmPreamm;
import it.accenture.jnais.ws.redefines.P67NumTstFin;
import it.accenture.jnais.ws.redefines.P67PerRatFinanz;
import it.accenture.jnais.ws.redefines.P67PreVers;
import it.accenture.jnais.ws.redefines.P67TsCreRatFinanz;
import it.accenture.jnais.ws.redefines.P67TsFinanz;
import it.accenture.jnais.ws.redefines.P67ValRiscBene;
import it.accenture.jnais.ws.redefines.P67ValRiscEndLeas;

/**Original name: IDBSP670<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  11 MAR 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsp670 extends Program implements IEstPoliCpiPr {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private EstPoliCpiPrDao estPoliCpiPrDao = new EstPoliCpiPrDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsp670Data ws = new Idbsp670Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: EST-POLI-CPI-PR
    private EstPoliCpiPrIdbsp670 estPoliCpiPr;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSP670_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, EstPoliCpiPrIdbsp670 estPoliCpiPr) {
        this.idsv0003 = idsv0003;
        this.estPoliCpiPr = estPoliCpiPr;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsp670 getInstance() {
        return ((Idbsp670)Programs.getInstance(Idbsp670.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSP670'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSP670");
        // COB_CODE: MOVE 'EST_POLI_CPI_PR' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("EST_POLI_CPI_PR");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI_CPI_PR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_PROD_ESTNO
        //                ,CPT_FIN
        //                ,NUM_TST_FIN
        //                ,TS_FINANZ
        //                ,DUR_MM_FINANZ
        //                ,DT_END_FINANZ
        //                ,AMM_1A_RAT
        //                ,VAL_RISC_BENE
        //                ,AMM_RAT_END
        //                ,TS_CRE_RAT_FINANZ
        //                ,IMP_FIN_REVOLVING
        //                ,IMP_UTIL_C_REV
        //                ,IMP_RAT_REVOLVING
        //                ,DT_1O_UTLZ_C_REV
        //                ,IMP_ASSTO
        //                ,PRE_VERS
        //                ,DT_SCAD_COP
        //                ,GG_DEL_MM_SCAD_RAT
        //                ,FL_PRE_FIN
        //                ,DT_SCAD_1A_RAT
        //                ,DT_EROG_FINANZ
        //                ,DT_STIPULA_FINANZ
        //                ,MM_PREAMM
        //                ,IMP_DEB_RES
        //                ,IMP_RAT_FINANZ
        //                ,IMP_CANONE_ANTIC
        //                ,PER_RAT_FINANZ
        //                ,TP_MOD_PAG_RAT
        //                ,TP_FINANZ_ER
        //                ,CAT_FINANZ_ER
        //                ,VAL_RISC_END_LEAS
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,NUM_FINANZ
        //                ,TP_MOD_ACQS
        //             INTO
        //                :P67-ID-EST-POLI-CPI-PR
        //               ,:P67-ID-MOVI-CRZ
        //               ,:P67-ID-MOVI-CHIU
        //                :IND-P67-ID-MOVI-CHIU
        //               ,:P67-DT-INI-EFF-DB
        //               ,:P67-DT-END-EFF-DB
        //               ,:P67-COD-COMP-ANIA
        //               ,:P67-IB-OGG
        //               ,:P67-DS-RIGA
        //               ,:P67-DS-OPER-SQL
        //               ,:P67-DS-VER
        //               ,:P67-DS-TS-INI-CPTZ
        //               ,:P67-DS-TS-END-CPTZ
        //               ,:P67-DS-UTENTE
        //               ,:P67-DS-STATO-ELAB
        //               ,:P67-COD-PROD-ESTNO
        //               ,:P67-CPT-FIN
        //                :IND-P67-CPT-FIN
        //               ,:P67-NUM-TST-FIN
        //                :IND-P67-NUM-TST-FIN
        //               ,:P67-TS-FINANZ
        //                :IND-P67-TS-FINANZ
        //               ,:P67-DUR-MM-FINANZ
        //                :IND-P67-DUR-MM-FINANZ
        //               ,:P67-DT-END-FINANZ-DB
        //                :IND-P67-DT-END-FINANZ
        //               ,:P67-AMM-1A-RAT
        //                :IND-P67-AMM-1A-RAT
        //               ,:P67-VAL-RISC-BENE
        //                :IND-P67-VAL-RISC-BENE
        //               ,:P67-AMM-RAT-END
        //                :IND-P67-AMM-RAT-END
        //               ,:P67-TS-CRE-RAT-FINANZ
        //                :IND-P67-TS-CRE-RAT-FINANZ
        //               ,:P67-IMP-FIN-REVOLVING
        //                :IND-P67-IMP-FIN-REVOLVING
        //               ,:P67-IMP-UTIL-C-REV
        //                :IND-P67-IMP-UTIL-C-REV
        //               ,:P67-IMP-RAT-REVOLVING
        //                :IND-P67-IMP-RAT-REVOLVING
        //               ,:P67-DT-1O-UTLZ-C-REV-DB
        //                :IND-P67-DT-1O-UTLZ-C-REV
        //               ,:P67-IMP-ASSTO
        //                :IND-P67-IMP-ASSTO
        //               ,:P67-PRE-VERS
        //                :IND-P67-PRE-VERS
        //               ,:P67-DT-SCAD-COP-DB
        //                :IND-P67-DT-SCAD-COP
        //               ,:P67-GG-DEL-MM-SCAD-RAT
        //                :IND-P67-GG-DEL-MM-SCAD-RAT
        //               ,:P67-FL-PRE-FIN
        //               ,:P67-DT-SCAD-1A-RAT-DB
        //                :IND-P67-DT-SCAD-1A-RAT
        //               ,:P67-DT-EROG-FINANZ-DB
        //                :IND-P67-DT-EROG-FINANZ
        //               ,:P67-DT-STIPULA-FINANZ-DB
        //                :IND-P67-DT-STIPULA-FINANZ
        //               ,:P67-MM-PREAMM
        //                :IND-P67-MM-PREAMM
        //               ,:P67-IMP-DEB-RES
        //                :IND-P67-IMP-DEB-RES
        //               ,:P67-IMP-RAT-FINANZ
        //                :IND-P67-IMP-RAT-FINANZ
        //               ,:P67-IMP-CANONE-ANTIC
        //                :IND-P67-IMP-CANONE-ANTIC
        //               ,:P67-PER-RAT-FINANZ
        //                :IND-P67-PER-RAT-FINANZ
        //               ,:P67-TP-MOD-PAG-RAT
        //                :IND-P67-TP-MOD-PAG-RAT
        //               ,:P67-TP-FINANZ-ER
        //                :IND-P67-TP-FINANZ-ER
        //               ,:P67-CAT-FINANZ-ER
        //                :IND-P67-CAT-FINANZ-ER
        //               ,:P67-VAL-RISC-END-LEAS
        //                :IND-P67-VAL-RISC-END-LEAS
        //               ,:P67-DT-EST-FINANZ-DB
        //                :IND-P67-DT-EST-FINANZ
        //               ,:P67-DT-MAN-COP-DB
        //                :IND-P67-DT-MAN-COP
        //               ,:P67-NUM-FINANZ
        //                :IND-P67-NUM-FINANZ
        //               ,:P67-TP-MOD-ACQS
        //                :IND-P67-TP-MOD-ACQS
        //             FROM EST_POLI_CPI_PR
        //             WHERE     DS_RIGA = :P67-DS-RIGA
        //           END-EXEC.
        estPoliCpiPrDao.selectByP67DsRiga(estPoliCpiPr.getP67DsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO EST_POLI_CPI_PR
            //                  (
            //                     ID_EST_POLI_CPI_PR
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,IB_OGG
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,COD_PROD_ESTNO
            //                    ,CPT_FIN
            //                    ,NUM_TST_FIN
            //                    ,TS_FINANZ
            //                    ,DUR_MM_FINANZ
            //                    ,DT_END_FINANZ
            //                    ,AMM_1A_RAT
            //                    ,VAL_RISC_BENE
            //                    ,AMM_RAT_END
            //                    ,TS_CRE_RAT_FINANZ
            //                    ,IMP_FIN_REVOLVING
            //                    ,IMP_UTIL_C_REV
            //                    ,IMP_RAT_REVOLVING
            //                    ,DT_1O_UTLZ_C_REV
            //                    ,IMP_ASSTO
            //                    ,PRE_VERS
            //                    ,DT_SCAD_COP
            //                    ,GG_DEL_MM_SCAD_RAT
            //                    ,FL_PRE_FIN
            //                    ,DT_SCAD_1A_RAT
            //                    ,DT_EROG_FINANZ
            //                    ,DT_STIPULA_FINANZ
            //                    ,MM_PREAMM
            //                    ,IMP_DEB_RES
            //                    ,IMP_RAT_FINANZ
            //                    ,IMP_CANONE_ANTIC
            //                    ,PER_RAT_FINANZ
            //                    ,TP_MOD_PAG_RAT
            //                    ,TP_FINANZ_ER
            //                    ,CAT_FINANZ_ER
            //                    ,VAL_RISC_END_LEAS
            //                    ,DT_EST_FINANZ
            //                    ,DT_MAN_COP
            //                    ,NUM_FINANZ
            //                    ,TP_MOD_ACQS
            //                  )
            //              VALUES
            //                  (
            //                    :P67-ID-EST-POLI-CPI-PR
            //                    ,:P67-ID-MOVI-CRZ
            //                    ,:P67-ID-MOVI-CHIU
            //                     :IND-P67-ID-MOVI-CHIU
            //                    ,:P67-DT-INI-EFF-DB
            //                    ,:P67-DT-END-EFF-DB
            //                    ,:P67-COD-COMP-ANIA
            //                    ,:P67-IB-OGG
            //                    ,:P67-DS-RIGA
            //                    ,:P67-DS-OPER-SQL
            //                    ,:P67-DS-VER
            //                    ,:P67-DS-TS-INI-CPTZ
            //                    ,:P67-DS-TS-END-CPTZ
            //                    ,:P67-DS-UTENTE
            //                    ,:P67-DS-STATO-ELAB
            //                    ,:P67-COD-PROD-ESTNO
            //                    ,:P67-CPT-FIN
            //                     :IND-P67-CPT-FIN
            //                    ,:P67-NUM-TST-FIN
            //                     :IND-P67-NUM-TST-FIN
            //                    ,:P67-TS-FINANZ
            //                     :IND-P67-TS-FINANZ
            //                    ,:P67-DUR-MM-FINANZ
            //                     :IND-P67-DUR-MM-FINANZ
            //                    ,:P67-DT-END-FINANZ-DB
            //                     :IND-P67-DT-END-FINANZ
            //                    ,:P67-AMM-1A-RAT
            //                     :IND-P67-AMM-1A-RAT
            //                    ,:P67-VAL-RISC-BENE
            //                     :IND-P67-VAL-RISC-BENE
            //                    ,:P67-AMM-RAT-END
            //                     :IND-P67-AMM-RAT-END
            //                    ,:P67-TS-CRE-RAT-FINANZ
            //                     :IND-P67-TS-CRE-RAT-FINANZ
            //                    ,:P67-IMP-FIN-REVOLVING
            //                     :IND-P67-IMP-FIN-REVOLVING
            //                    ,:P67-IMP-UTIL-C-REV
            //                     :IND-P67-IMP-UTIL-C-REV
            //                    ,:P67-IMP-RAT-REVOLVING
            //                     :IND-P67-IMP-RAT-REVOLVING
            //                    ,:P67-DT-1O-UTLZ-C-REV-DB
            //                     :IND-P67-DT-1O-UTLZ-C-REV
            //                    ,:P67-IMP-ASSTO
            //                     :IND-P67-IMP-ASSTO
            //                    ,:P67-PRE-VERS
            //                     :IND-P67-PRE-VERS
            //                    ,:P67-DT-SCAD-COP-DB
            //                     :IND-P67-DT-SCAD-COP
            //                    ,:P67-GG-DEL-MM-SCAD-RAT
            //                     :IND-P67-GG-DEL-MM-SCAD-RAT
            //                    ,:P67-FL-PRE-FIN
            //                    ,:P67-DT-SCAD-1A-RAT-DB
            //                     :IND-P67-DT-SCAD-1A-RAT
            //                    ,:P67-DT-EROG-FINANZ-DB
            //                     :IND-P67-DT-EROG-FINANZ
            //                    ,:P67-DT-STIPULA-FINANZ-DB
            //                     :IND-P67-DT-STIPULA-FINANZ
            //                    ,:P67-MM-PREAMM
            //                     :IND-P67-MM-PREAMM
            //                    ,:P67-IMP-DEB-RES
            //                     :IND-P67-IMP-DEB-RES
            //                    ,:P67-IMP-RAT-FINANZ
            //                     :IND-P67-IMP-RAT-FINANZ
            //                    ,:P67-IMP-CANONE-ANTIC
            //                     :IND-P67-IMP-CANONE-ANTIC
            //                    ,:P67-PER-RAT-FINANZ
            //                     :IND-P67-PER-RAT-FINANZ
            //                    ,:P67-TP-MOD-PAG-RAT
            //                     :IND-P67-TP-MOD-PAG-RAT
            //                    ,:P67-TP-FINANZ-ER
            //                     :IND-P67-TP-FINANZ-ER
            //                    ,:P67-CAT-FINANZ-ER
            //                     :IND-P67-CAT-FINANZ-ER
            //                    ,:P67-VAL-RISC-END-LEAS
            //                     :IND-P67-VAL-RISC-END-LEAS
            //                    ,:P67-DT-EST-FINANZ-DB
            //                     :IND-P67-DT-EST-FINANZ
            //                    ,:P67-DT-MAN-COP-DB
            //                     :IND-P67-DT-MAN-COP
            //                    ,:P67-NUM-FINANZ
            //                     :IND-P67-NUM-FINANZ
            //                    ,:P67-TP-MOD-ACQS
            //                     :IND-P67-TP-MOD-ACQS
            //                  )
            //           END-EXEC
            estPoliCpiPrDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE EST_POLI_CPI_PR SET
        //                   ID_EST_POLI_CPI_PR     =
        //                :P67-ID-EST-POLI-CPI-PR
        //                  ,ID_MOVI_CRZ            =
        //                :P67-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :P67-ID-MOVI-CHIU
        //                                       :IND-P67-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :P67-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :P67-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :P67-COD-COMP-ANIA
        //                  ,IB_OGG                 =
        //                :P67-IB-OGG
        //                  ,DS_RIGA                =
        //                :P67-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :P67-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :P67-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :P67-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :P67-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :P67-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :P67-DS-STATO-ELAB
        //                  ,COD_PROD_ESTNO         =
        //                :P67-COD-PROD-ESTNO
        //                  ,CPT_FIN                =
        //                :P67-CPT-FIN
        //                                       :IND-P67-CPT-FIN
        //                  ,NUM_TST_FIN            =
        //                :P67-NUM-TST-FIN
        //                                       :IND-P67-NUM-TST-FIN
        //                  ,TS_FINANZ              =
        //                :P67-TS-FINANZ
        //                                       :IND-P67-TS-FINANZ
        //                  ,DUR_MM_FINANZ          =
        //                :P67-DUR-MM-FINANZ
        //                                       :IND-P67-DUR-MM-FINANZ
        //                  ,DT_END_FINANZ          =
        //           :P67-DT-END-FINANZ-DB
        //                                       :IND-P67-DT-END-FINANZ
        //                  ,AMM_1A_RAT             =
        //                :P67-AMM-1A-RAT
        //                                       :IND-P67-AMM-1A-RAT
        //                  ,VAL_RISC_BENE          =
        //                :P67-VAL-RISC-BENE
        //                                       :IND-P67-VAL-RISC-BENE
        //                  ,AMM_RAT_END            =
        //                :P67-AMM-RAT-END
        //                                       :IND-P67-AMM-RAT-END
        //                  ,TS_CRE_RAT_FINANZ      =
        //                :P67-TS-CRE-RAT-FINANZ
        //                                       :IND-P67-TS-CRE-RAT-FINANZ
        //                  ,IMP_FIN_REVOLVING      =
        //                :P67-IMP-FIN-REVOLVING
        //                                       :IND-P67-IMP-FIN-REVOLVING
        //                  ,IMP_UTIL_C_REV         =
        //                :P67-IMP-UTIL-C-REV
        //                                       :IND-P67-IMP-UTIL-C-REV
        //                  ,IMP_RAT_REVOLVING      =
        //                :P67-IMP-RAT-REVOLVING
        //                                       :IND-P67-IMP-RAT-REVOLVING
        //                  ,DT_1O_UTLZ_C_REV       =
        //           :P67-DT-1O-UTLZ-C-REV-DB
        //                                       :IND-P67-DT-1O-UTLZ-C-REV
        //                  ,IMP_ASSTO              =
        //                :P67-IMP-ASSTO
        //                                       :IND-P67-IMP-ASSTO
        //                  ,PRE_VERS               =
        //                :P67-PRE-VERS
        //                                       :IND-P67-PRE-VERS
        //                  ,DT_SCAD_COP            =
        //           :P67-DT-SCAD-COP-DB
        //                                       :IND-P67-DT-SCAD-COP
        //                  ,GG_DEL_MM_SCAD_RAT     =
        //                :P67-GG-DEL-MM-SCAD-RAT
        //                                       :IND-P67-GG-DEL-MM-SCAD-RAT
        //                  ,FL_PRE_FIN             =
        //                :P67-FL-PRE-FIN
        //                  ,DT_SCAD_1A_RAT         =
        //           :P67-DT-SCAD-1A-RAT-DB
        //                                       :IND-P67-DT-SCAD-1A-RAT
        //                  ,DT_EROG_FINANZ         =
        //           :P67-DT-EROG-FINANZ-DB
        //                                       :IND-P67-DT-EROG-FINANZ
        //                  ,DT_STIPULA_FINANZ      =
        //           :P67-DT-STIPULA-FINANZ-DB
        //                                       :IND-P67-DT-STIPULA-FINANZ
        //                  ,MM_PREAMM              =
        //                :P67-MM-PREAMM
        //                                       :IND-P67-MM-PREAMM
        //                  ,IMP_DEB_RES            =
        //                :P67-IMP-DEB-RES
        //                                       :IND-P67-IMP-DEB-RES
        //                  ,IMP_RAT_FINANZ         =
        //                :P67-IMP-RAT-FINANZ
        //                                       :IND-P67-IMP-RAT-FINANZ
        //                  ,IMP_CANONE_ANTIC       =
        //                :P67-IMP-CANONE-ANTIC
        //                                       :IND-P67-IMP-CANONE-ANTIC
        //                  ,PER_RAT_FINANZ         =
        //                :P67-PER-RAT-FINANZ
        //                                       :IND-P67-PER-RAT-FINANZ
        //                  ,TP_MOD_PAG_RAT         =
        //                :P67-TP-MOD-PAG-RAT
        //                                       :IND-P67-TP-MOD-PAG-RAT
        //                  ,TP_FINANZ_ER           =
        //                :P67-TP-FINANZ-ER
        //                                       :IND-P67-TP-FINANZ-ER
        //                  ,CAT_FINANZ_ER          =
        //                :P67-CAT-FINANZ-ER
        //                                       :IND-P67-CAT-FINANZ-ER
        //                  ,VAL_RISC_END_LEAS      =
        //                :P67-VAL-RISC-END-LEAS
        //                                       :IND-P67-VAL-RISC-END-LEAS
        //                  ,DT_EST_FINANZ          =
        //           :P67-DT-EST-FINANZ-DB
        //                                       :IND-P67-DT-EST-FINANZ
        //                  ,DT_MAN_COP             =
        //           :P67-DT-MAN-COP-DB
        //                                       :IND-P67-DT-MAN-COP
        //                  ,NUM_FINANZ             =
        //                :P67-NUM-FINANZ
        //                                       :IND-P67-NUM-FINANZ
        //                  ,TP_MOD_ACQS            =
        //                :P67-TP-MOD-ACQS
        //                                       :IND-P67-TP-MOD-ACQS
        //                WHERE     DS_RIGA = :P67-DS-RIGA
        //           END-EXEC.
        estPoliCpiPrDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM EST_POLI_CPI_PR
        //                WHERE     DS_RIGA = :P67-DS-RIGA
        //           END-EXEC.
        estPoliCpiPrDao.deleteByP67DsRiga(estPoliCpiPr.getP67DsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-P67 CURSOR FOR
        //              SELECT
        //                     ID_EST_POLI_CPI_PR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_PROD_ESTNO
        //                    ,CPT_FIN
        //                    ,NUM_TST_FIN
        //                    ,TS_FINANZ
        //                    ,DUR_MM_FINANZ
        //                    ,DT_END_FINANZ
        //                    ,AMM_1A_RAT
        //                    ,VAL_RISC_BENE
        //                    ,AMM_RAT_END
        //                    ,TS_CRE_RAT_FINANZ
        //                    ,IMP_FIN_REVOLVING
        //                    ,IMP_UTIL_C_REV
        //                    ,IMP_RAT_REVOLVING
        //                    ,DT_1O_UTLZ_C_REV
        //                    ,IMP_ASSTO
        //                    ,PRE_VERS
        //                    ,DT_SCAD_COP
        //                    ,GG_DEL_MM_SCAD_RAT
        //                    ,FL_PRE_FIN
        //                    ,DT_SCAD_1A_RAT
        //                    ,DT_EROG_FINANZ
        //                    ,DT_STIPULA_FINANZ
        //                    ,MM_PREAMM
        //                    ,IMP_DEB_RES
        //                    ,IMP_RAT_FINANZ
        //                    ,IMP_CANONE_ANTIC
        //                    ,PER_RAT_FINANZ
        //                    ,TP_MOD_PAG_RAT
        //                    ,TP_FINANZ_ER
        //                    ,CAT_FINANZ_ER
        //                    ,VAL_RISC_END_LEAS
        //                    ,DT_EST_FINANZ
        //                    ,DT_MAN_COP
        //                    ,NUM_FINANZ
        //                    ,TP_MOD_ACQS
        //              FROM EST_POLI_CPI_PR
        //              WHERE     ID_EST_POLI_CPI_PR = :P67-ID-EST-POLI-CPI-PR
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI_CPI_PR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_PROD_ESTNO
        //                ,CPT_FIN
        //                ,NUM_TST_FIN
        //                ,TS_FINANZ
        //                ,DUR_MM_FINANZ
        //                ,DT_END_FINANZ
        //                ,AMM_1A_RAT
        //                ,VAL_RISC_BENE
        //                ,AMM_RAT_END
        //                ,TS_CRE_RAT_FINANZ
        //                ,IMP_FIN_REVOLVING
        //                ,IMP_UTIL_C_REV
        //                ,IMP_RAT_REVOLVING
        //                ,DT_1O_UTLZ_C_REV
        //                ,IMP_ASSTO
        //                ,PRE_VERS
        //                ,DT_SCAD_COP
        //                ,GG_DEL_MM_SCAD_RAT
        //                ,FL_PRE_FIN
        //                ,DT_SCAD_1A_RAT
        //                ,DT_EROG_FINANZ
        //                ,DT_STIPULA_FINANZ
        //                ,MM_PREAMM
        //                ,IMP_DEB_RES
        //                ,IMP_RAT_FINANZ
        //                ,IMP_CANONE_ANTIC
        //                ,PER_RAT_FINANZ
        //                ,TP_MOD_PAG_RAT
        //                ,TP_FINANZ_ER
        //                ,CAT_FINANZ_ER
        //                ,VAL_RISC_END_LEAS
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,NUM_FINANZ
        //                ,TP_MOD_ACQS
        //             INTO
        //                :P67-ID-EST-POLI-CPI-PR
        //               ,:P67-ID-MOVI-CRZ
        //               ,:P67-ID-MOVI-CHIU
        //                :IND-P67-ID-MOVI-CHIU
        //               ,:P67-DT-INI-EFF-DB
        //               ,:P67-DT-END-EFF-DB
        //               ,:P67-COD-COMP-ANIA
        //               ,:P67-IB-OGG
        //               ,:P67-DS-RIGA
        //               ,:P67-DS-OPER-SQL
        //               ,:P67-DS-VER
        //               ,:P67-DS-TS-INI-CPTZ
        //               ,:P67-DS-TS-END-CPTZ
        //               ,:P67-DS-UTENTE
        //               ,:P67-DS-STATO-ELAB
        //               ,:P67-COD-PROD-ESTNO
        //               ,:P67-CPT-FIN
        //                :IND-P67-CPT-FIN
        //               ,:P67-NUM-TST-FIN
        //                :IND-P67-NUM-TST-FIN
        //               ,:P67-TS-FINANZ
        //                :IND-P67-TS-FINANZ
        //               ,:P67-DUR-MM-FINANZ
        //                :IND-P67-DUR-MM-FINANZ
        //               ,:P67-DT-END-FINANZ-DB
        //                :IND-P67-DT-END-FINANZ
        //               ,:P67-AMM-1A-RAT
        //                :IND-P67-AMM-1A-RAT
        //               ,:P67-VAL-RISC-BENE
        //                :IND-P67-VAL-RISC-BENE
        //               ,:P67-AMM-RAT-END
        //                :IND-P67-AMM-RAT-END
        //               ,:P67-TS-CRE-RAT-FINANZ
        //                :IND-P67-TS-CRE-RAT-FINANZ
        //               ,:P67-IMP-FIN-REVOLVING
        //                :IND-P67-IMP-FIN-REVOLVING
        //               ,:P67-IMP-UTIL-C-REV
        //                :IND-P67-IMP-UTIL-C-REV
        //               ,:P67-IMP-RAT-REVOLVING
        //                :IND-P67-IMP-RAT-REVOLVING
        //               ,:P67-DT-1O-UTLZ-C-REV-DB
        //                :IND-P67-DT-1O-UTLZ-C-REV
        //               ,:P67-IMP-ASSTO
        //                :IND-P67-IMP-ASSTO
        //               ,:P67-PRE-VERS
        //                :IND-P67-PRE-VERS
        //               ,:P67-DT-SCAD-COP-DB
        //                :IND-P67-DT-SCAD-COP
        //               ,:P67-GG-DEL-MM-SCAD-RAT
        //                :IND-P67-GG-DEL-MM-SCAD-RAT
        //               ,:P67-FL-PRE-FIN
        //               ,:P67-DT-SCAD-1A-RAT-DB
        //                :IND-P67-DT-SCAD-1A-RAT
        //               ,:P67-DT-EROG-FINANZ-DB
        //                :IND-P67-DT-EROG-FINANZ
        //               ,:P67-DT-STIPULA-FINANZ-DB
        //                :IND-P67-DT-STIPULA-FINANZ
        //               ,:P67-MM-PREAMM
        //                :IND-P67-MM-PREAMM
        //               ,:P67-IMP-DEB-RES
        //                :IND-P67-IMP-DEB-RES
        //               ,:P67-IMP-RAT-FINANZ
        //                :IND-P67-IMP-RAT-FINANZ
        //               ,:P67-IMP-CANONE-ANTIC
        //                :IND-P67-IMP-CANONE-ANTIC
        //               ,:P67-PER-RAT-FINANZ
        //                :IND-P67-PER-RAT-FINANZ
        //               ,:P67-TP-MOD-PAG-RAT
        //                :IND-P67-TP-MOD-PAG-RAT
        //               ,:P67-TP-FINANZ-ER
        //                :IND-P67-TP-FINANZ-ER
        //               ,:P67-CAT-FINANZ-ER
        //                :IND-P67-CAT-FINANZ-ER
        //               ,:P67-VAL-RISC-END-LEAS
        //                :IND-P67-VAL-RISC-END-LEAS
        //               ,:P67-DT-EST-FINANZ-DB
        //                :IND-P67-DT-EST-FINANZ
        //               ,:P67-DT-MAN-COP-DB
        //                :IND-P67-DT-MAN-COP
        //               ,:P67-NUM-FINANZ
        //                :IND-P67-NUM-FINANZ
        //               ,:P67-TP-MOD-ACQS
        //                :IND-P67-TP-MOD-ACQS
        //             FROM EST_POLI_CPI_PR
        //             WHERE     ID_EST_POLI_CPI_PR = :P67-ID-EST-POLI-CPI-PR
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estPoliCpiPrDao.selectRec(estPoliCpiPr.getP67IdEstPoliCpiPr(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE EST_POLI_CPI_PR SET
        //                   ID_EST_POLI_CPI_PR     =
        //                :P67-ID-EST-POLI-CPI-PR
        //                  ,ID_MOVI_CRZ            =
        //                :P67-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :P67-ID-MOVI-CHIU
        //                                       :IND-P67-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :P67-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :P67-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :P67-COD-COMP-ANIA
        //                  ,IB_OGG                 =
        //                :P67-IB-OGG
        //                  ,DS_RIGA                =
        //                :P67-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :P67-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :P67-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :P67-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :P67-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :P67-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :P67-DS-STATO-ELAB
        //                  ,COD_PROD_ESTNO         =
        //                :P67-COD-PROD-ESTNO
        //                  ,CPT_FIN                =
        //                :P67-CPT-FIN
        //                                       :IND-P67-CPT-FIN
        //                  ,NUM_TST_FIN            =
        //                :P67-NUM-TST-FIN
        //                                       :IND-P67-NUM-TST-FIN
        //                  ,TS_FINANZ              =
        //                :P67-TS-FINANZ
        //                                       :IND-P67-TS-FINANZ
        //                  ,DUR_MM_FINANZ          =
        //                :P67-DUR-MM-FINANZ
        //                                       :IND-P67-DUR-MM-FINANZ
        //                  ,DT_END_FINANZ          =
        //           :P67-DT-END-FINANZ-DB
        //                                       :IND-P67-DT-END-FINANZ
        //                  ,AMM_1A_RAT             =
        //                :P67-AMM-1A-RAT
        //                                       :IND-P67-AMM-1A-RAT
        //                  ,VAL_RISC_BENE          =
        //                :P67-VAL-RISC-BENE
        //                                       :IND-P67-VAL-RISC-BENE
        //                  ,AMM_RAT_END            =
        //                :P67-AMM-RAT-END
        //                                       :IND-P67-AMM-RAT-END
        //                  ,TS_CRE_RAT_FINANZ      =
        //                :P67-TS-CRE-RAT-FINANZ
        //                                       :IND-P67-TS-CRE-RAT-FINANZ
        //                  ,IMP_FIN_REVOLVING      =
        //                :P67-IMP-FIN-REVOLVING
        //                                       :IND-P67-IMP-FIN-REVOLVING
        //                  ,IMP_UTIL_C_REV         =
        //                :P67-IMP-UTIL-C-REV
        //                                       :IND-P67-IMP-UTIL-C-REV
        //                  ,IMP_RAT_REVOLVING      =
        //                :P67-IMP-RAT-REVOLVING
        //                                       :IND-P67-IMP-RAT-REVOLVING
        //                  ,DT_1O_UTLZ_C_REV       =
        //           :P67-DT-1O-UTLZ-C-REV-DB
        //                                       :IND-P67-DT-1O-UTLZ-C-REV
        //                  ,IMP_ASSTO              =
        //                :P67-IMP-ASSTO
        //                                       :IND-P67-IMP-ASSTO
        //                  ,PRE_VERS               =
        //                :P67-PRE-VERS
        //                                       :IND-P67-PRE-VERS
        //                  ,DT_SCAD_COP            =
        //           :P67-DT-SCAD-COP-DB
        //                                       :IND-P67-DT-SCAD-COP
        //                  ,GG_DEL_MM_SCAD_RAT     =
        //                :P67-GG-DEL-MM-SCAD-RAT
        //                                       :IND-P67-GG-DEL-MM-SCAD-RAT
        //                  ,FL_PRE_FIN             =
        //                :P67-FL-PRE-FIN
        //                  ,DT_SCAD_1A_RAT         =
        //           :P67-DT-SCAD-1A-RAT-DB
        //                                       :IND-P67-DT-SCAD-1A-RAT
        //                  ,DT_EROG_FINANZ         =
        //           :P67-DT-EROG-FINANZ-DB
        //                                       :IND-P67-DT-EROG-FINANZ
        //                  ,DT_STIPULA_FINANZ      =
        //           :P67-DT-STIPULA-FINANZ-DB
        //                                       :IND-P67-DT-STIPULA-FINANZ
        //                  ,MM_PREAMM              =
        //                :P67-MM-PREAMM
        //                                       :IND-P67-MM-PREAMM
        //                  ,IMP_DEB_RES            =
        //                :P67-IMP-DEB-RES
        //                                       :IND-P67-IMP-DEB-RES
        //                  ,IMP_RAT_FINANZ         =
        //                :P67-IMP-RAT-FINANZ
        //                                       :IND-P67-IMP-RAT-FINANZ
        //                  ,IMP_CANONE_ANTIC       =
        //                :P67-IMP-CANONE-ANTIC
        //                                       :IND-P67-IMP-CANONE-ANTIC
        //                  ,PER_RAT_FINANZ         =
        //                :P67-PER-RAT-FINANZ
        //                                       :IND-P67-PER-RAT-FINANZ
        //                  ,TP_MOD_PAG_RAT         =
        //                :P67-TP-MOD-PAG-RAT
        //                                       :IND-P67-TP-MOD-PAG-RAT
        //                  ,TP_FINANZ_ER           =
        //                :P67-TP-FINANZ-ER
        //                                       :IND-P67-TP-FINANZ-ER
        //                  ,CAT_FINANZ_ER          =
        //                :P67-CAT-FINANZ-ER
        //                                       :IND-P67-CAT-FINANZ-ER
        //                  ,VAL_RISC_END_LEAS      =
        //                :P67-VAL-RISC-END-LEAS
        //                                       :IND-P67-VAL-RISC-END-LEAS
        //                  ,DT_EST_FINANZ          =
        //           :P67-DT-EST-FINANZ-DB
        //                                       :IND-P67-DT-EST-FINANZ
        //                  ,DT_MAN_COP             =
        //           :P67-DT-MAN-COP-DB
        //                                       :IND-P67-DT-MAN-COP
        //                  ,NUM_FINANZ             =
        //                :P67-NUM-FINANZ
        //                                       :IND-P67-NUM-FINANZ
        //                  ,TP_MOD_ACQS            =
        //                :P67-TP-MOD-ACQS
        //                                       :IND-P67-TP-MOD-ACQS
        //                WHERE     DS_RIGA = :P67-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estPoliCpiPrDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-P67
        //           END-EXEC.
        estPoliCpiPrDao.openCIdUpdEffP67(estPoliCpiPr.getP67IdEstPoliCpiPr(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-P67
        //           END-EXEC.
        estPoliCpiPrDao.closeCIdUpdEffP67();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-P67
        //           INTO
        //                :P67-ID-EST-POLI-CPI-PR
        //               ,:P67-ID-MOVI-CRZ
        //               ,:P67-ID-MOVI-CHIU
        //                :IND-P67-ID-MOVI-CHIU
        //               ,:P67-DT-INI-EFF-DB
        //               ,:P67-DT-END-EFF-DB
        //               ,:P67-COD-COMP-ANIA
        //               ,:P67-IB-OGG
        //               ,:P67-DS-RIGA
        //               ,:P67-DS-OPER-SQL
        //               ,:P67-DS-VER
        //               ,:P67-DS-TS-INI-CPTZ
        //               ,:P67-DS-TS-END-CPTZ
        //               ,:P67-DS-UTENTE
        //               ,:P67-DS-STATO-ELAB
        //               ,:P67-COD-PROD-ESTNO
        //               ,:P67-CPT-FIN
        //                :IND-P67-CPT-FIN
        //               ,:P67-NUM-TST-FIN
        //                :IND-P67-NUM-TST-FIN
        //               ,:P67-TS-FINANZ
        //                :IND-P67-TS-FINANZ
        //               ,:P67-DUR-MM-FINANZ
        //                :IND-P67-DUR-MM-FINANZ
        //               ,:P67-DT-END-FINANZ-DB
        //                :IND-P67-DT-END-FINANZ
        //               ,:P67-AMM-1A-RAT
        //                :IND-P67-AMM-1A-RAT
        //               ,:P67-VAL-RISC-BENE
        //                :IND-P67-VAL-RISC-BENE
        //               ,:P67-AMM-RAT-END
        //                :IND-P67-AMM-RAT-END
        //               ,:P67-TS-CRE-RAT-FINANZ
        //                :IND-P67-TS-CRE-RAT-FINANZ
        //               ,:P67-IMP-FIN-REVOLVING
        //                :IND-P67-IMP-FIN-REVOLVING
        //               ,:P67-IMP-UTIL-C-REV
        //                :IND-P67-IMP-UTIL-C-REV
        //               ,:P67-IMP-RAT-REVOLVING
        //                :IND-P67-IMP-RAT-REVOLVING
        //               ,:P67-DT-1O-UTLZ-C-REV-DB
        //                :IND-P67-DT-1O-UTLZ-C-REV
        //               ,:P67-IMP-ASSTO
        //                :IND-P67-IMP-ASSTO
        //               ,:P67-PRE-VERS
        //                :IND-P67-PRE-VERS
        //               ,:P67-DT-SCAD-COP-DB
        //                :IND-P67-DT-SCAD-COP
        //               ,:P67-GG-DEL-MM-SCAD-RAT
        //                :IND-P67-GG-DEL-MM-SCAD-RAT
        //               ,:P67-FL-PRE-FIN
        //               ,:P67-DT-SCAD-1A-RAT-DB
        //                :IND-P67-DT-SCAD-1A-RAT
        //               ,:P67-DT-EROG-FINANZ-DB
        //                :IND-P67-DT-EROG-FINANZ
        //               ,:P67-DT-STIPULA-FINANZ-DB
        //                :IND-P67-DT-STIPULA-FINANZ
        //               ,:P67-MM-PREAMM
        //                :IND-P67-MM-PREAMM
        //               ,:P67-IMP-DEB-RES
        //                :IND-P67-IMP-DEB-RES
        //               ,:P67-IMP-RAT-FINANZ
        //                :IND-P67-IMP-RAT-FINANZ
        //               ,:P67-IMP-CANONE-ANTIC
        //                :IND-P67-IMP-CANONE-ANTIC
        //               ,:P67-PER-RAT-FINANZ
        //                :IND-P67-PER-RAT-FINANZ
        //               ,:P67-TP-MOD-PAG-RAT
        //                :IND-P67-TP-MOD-PAG-RAT
        //               ,:P67-TP-FINANZ-ER
        //                :IND-P67-TP-FINANZ-ER
        //               ,:P67-CAT-FINANZ-ER
        //                :IND-P67-CAT-FINANZ-ER
        //               ,:P67-VAL-RISC-END-LEAS
        //                :IND-P67-VAL-RISC-END-LEAS
        //               ,:P67-DT-EST-FINANZ-DB
        //                :IND-P67-DT-EST-FINANZ
        //               ,:P67-DT-MAN-COP-DB
        //                :IND-P67-DT-MAN-COP
        //               ,:P67-NUM-FINANZ
        //                :IND-P67-NUM-FINANZ
        //               ,:P67-TP-MOD-ACQS
        //                :IND-P67-TP-MOD-ACQS
        //           END-EXEC.
        estPoliCpiPrDao.fetchCIdUpdEffP67(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-EFF-P67 CURSOR FOR
        //              SELECT
        //                     ID_EST_POLI_CPI_PR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_PROD_ESTNO
        //                    ,CPT_FIN
        //                    ,NUM_TST_FIN
        //                    ,TS_FINANZ
        //                    ,DUR_MM_FINANZ
        //                    ,DT_END_FINANZ
        //                    ,AMM_1A_RAT
        //                    ,VAL_RISC_BENE
        //                    ,AMM_RAT_END
        //                    ,TS_CRE_RAT_FINANZ
        //                    ,IMP_FIN_REVOLVING
        //                    ,IMP_UTIL_C_REV
        //                    ,IMP_RAT_REVOLVING
        //                    ,DT_1O_UTLZ_C_REV
        //                    ,IMP_ASSTO
        //                    ,PRE_VERS
        //                    ,DT_SCAD_COP
        //                    ,GG_DEL_MM_SCAD_RAT
        //                    ,FL_PRE_FIN
        //                    ,DT_SCAD_1A_RAT
        //                    ,DT_EROG_FINANZ
        //                    ,DT_STIPULA_FINANZ
        //                    ,MM_PREAMM
        //                    ,IMP_DEB_RES
        //                    ,IMP_RAT_FINANZ
        //                    ,IMP_CANONE_ANTIC
        //                    ,PER_RAT_FINANZ
        //                    ,TP_MOD_PAG_RAT
        //                    ,TP_FINANZ_ER
        //                    ,CAT_FINANZ_ER
        //                    ,VAL_RISC_END_LEAS
        //                    ,DT_EST_FINANZ
        //                    ,DT_MAN_COP
        //                    ,NUM_FINANZ
        //                    ,TP_MOD_ACQS
        //              FROM EST_POLI_CPI_PR
        //              WHERE     IB_OGG = :P67-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_EST_POLI_CPI_PR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI_CPI_PR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_PROD_ESTNO
        //                ,CPT_FIN
        //                ,NUM_TST_FIN
        //                ,TS_FINANZ
        //                ,DUR_MM_FINANZ
        //                ,DT_END_FINANZ
        //                ,AMM_1A_RAT
        //                ,VAL_RISC_BENE
        //                ,AMM_RAT_END
        //                ,TS_CRE_RAT_FINANZ
        //                ,IMP_FIN_REVOLVING
        //                ,IMP_UTIL_C_REV
        //                ,IMP_RAT_REVOLVING
        //                ,DT_1O_UTLZ_C_REV
        //                ,IMP_ASSTO
        //                ,PRE_VERS
        //                ,DT_SCAD_COP
        //                ,GG_DEL_MM_SCAD_RAT
        //                ,FL_PRE_FIN
        //                ,DT_SCAD_1A_RAT
        //                ,DT_EROG_FINANZ
        //                ,DT_STIPULA_FINANZ
        //                ,MM_PREAMM
        //                ,IMP_DEB_RES
        //                ,IMP_RAT_FINANZ
        //                ,IMP_CANONE_ANTIC
        //                ,PER_RAT_FINANZ
        //                ,TP_MOD_PAG_RAT
        //                ,TP_FINANZ_ER
        //                ,CAT_FINANZ_ER
        //                ,VAL_RISC_END_LEAS
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,NUM_FINANZ
        //                ,TP_MOD_ACQS
        //             INTO
        //                :P67-ID-EST-POLI-CPI-PR
        //               ,:P67-ID-MOVI-CRZ
        //               ,:P67-ID-MOVI-CHIU
        //                :IND-P67-ID-MOVI-CHIU
        //               ,:P67-DT-INI-EFF-DB
        //               ,:P67-DT-END-EFF-DB
        //               ,:P67-COD-COMP-ANIA
        //               ,:P67-IB-OGG
        //               ,:P67-DS-RIGA
        //               ,:P67-DS-OPER-SQL
        //               ,:P67-DS-VER
        //               ,:P67-DS-TS-INI-CPTZ
        //               ,:P67-DS-TS-END-CPTZ
        //               ,:P67-DS-UTENTE
        //               ,:P67-DS-STATO-ELAB
        //               ,:P67-COD-PROD-ESTNO
        //               ,:P67-CPT-FIN
        //                :IND-P67-CPT-FIN
        //               ,:P67-NUM-TST-FIN
        //                :IND-P67-NUM-TST-FIN
        //               ,:P67-TS-FINANZ
        //                :IND-P67-TS-FINANZ
        //               ,:P67-DUR-MM-FINANZ
        //                :IND-P67-DUR-MM-FINANZ
        //               ,:P67-DT-END-FINANZ-DB
        //                :IND-P67-DT-END-FINANZ
        //               ,:P67-AMM-1A-RAT
        //                :IND-P67-AMM-1A-RAT
        //               ,:P67-VAL-RISC-BENE
        //                :IND-P67-VAL-RISC-BENE
        //               ,:P67-AMM-RAT-END
        //                :IND-P67-AMM-RAT-END
        //               ,:P67-TS-CRE-RAT-FINANZ
        //                :IND-P67-TS-CRE-RAT-FINANZ
        //               ,:P67-IMP-FIN-REVOLVING
        //                :IND-P67-IMP-FIN-REVOLVING
        //               ,:P67-IMP-UTIL-C-REV
        //                :IND-P67-IMP-UTIL-C-REV
        //               ,:P67-IMP-RAT-REVOLVING
        //                :IND-P67-IMP-RAT-REVOLVING
        //               ,:P67-DT-1O-UTLZ-C-REV-DB
        //                :IND-P67-DT-1O-UTLZ-C-REV
        //               ,:P67-IMP-ASSTO
        //                :IND-P67-IMP-ASSTO
        //               ,:P67-PRE-VERS
        //                :IND-P67-PRE-VERS
        //               ,:P67-DT-SCAD-COP-DB
        //                :IND-P67-DT-SCAD-COP
        //               ,:P67-GG-DEL-MM-SCAD-RAT
        //                :IND-P67-GG-DEL-MM-SCAD-RAT
        //               ,:P67-FL-PRE-FIN
        //               ,:P67-DT-SCAD-1A-RAT-DB
        //                :IND-P67-DT-SCAD-1A-RAT
        //               ,:P67-DT-EROG-FINANZ-DB
        //                :IND-P67-DT-EROG-FINANZ
        //               ,:P67-DT-STIPULA-FINANZ-DB
        //                :IND-P67-DT-STIPULA-FINANZ
        //               ,:P67-MM-PREAMM
        //                :IND-P67-MM-PREAMM
        //               ,:P67-IMP-DEB-RES
        //                :IND-P67-IMP-DEB-RES
        //               ,:P67-IMP-RAT-FINANZ
        //                :IND-P67-IMP-RAT-FINANZ
        //               ,:P67-IMP-CANONE-ANTIC
        //                :IND-P67-IMP-CANONE-ANTIC
        //               ,:P67-PER-RAT-FINANZ
        //                :IND-P67-PER-RAT-FINANZ
        //               ,:P67-TP-MOD-PAG-RAT
        //                :IND-P67-TP-MOD-PAG-RAT
        //               ,:P67-TP-FINANZ-ER
        //                :IND-P67-TP-FINANZ-ER
        //               ,:P67-CAT-FINANZ-ER
        //                :IND-P67-CAT-FINANZ-ER
        //               ,:P67-VAL-RISC-END-LEAS
        //                :IND-P67-VAL-RISC-END-LEAS
        //               ,:P67-DT-EST-FINANZ-DB
        //                :IND-P67-DT-EST-FINANZ
        //               ,:P67-DT-MAN-COP-DB
        //                :IND-P67-DT-MAN-COP
        //               ,:P67-NUM-FINANZ
        //                :IND-P67-NUM-FINANZ
        //               ,:P67-TP-MOD-ACQS
        //                :IND-P67-TP-MOD-ACQS
        //             FROM EST_POLI_CPI_PR
        //             WHERE     IB_OGG = :P67-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        estPoliCpiPrDao.selectRec1(estPoliCpiPr.getP67IbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-EFF-P67
        //           END-EXEC.
        estPoliCpiPrDao.openCIboEffP67(estPoliCpiPr.getP67IbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-EFF-P67
        //           END-EXEC.
        estPoliCpiPrDao.closeCIboEffP67();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-EFF-P67
        //           INTO
        //                :P67-ID-EST-POLI-CPI-PR
        //               ,:P67-ID-MOVI-CRZ
        //               ,:P67-ID-MOVI-CHIU
        //                :IND-P67-ID-MOVI-CHIU
        //               ,:P67-DT-INI-EFF-DB
        //               ,:P67-DT-END-EFF-DB
        //               ,:P67-COD-COMP-ANIA
        //               ,:P67-IB-OGG
        //               ,:P67-DS-RIGA
        //               ,:P67-DS-OPER-SQL
        //               ,:P67-DS-VER
        //               ,:P67-DS-TS-INI-CPTZ
        //               ,:P67-DS-TS-END-CPTZ
        //               ,:P67-DS-UTENTE
        //               ,:P67-DS-STATO-ELAB
        //               ,:P67-COD-PROD-ESTNO
        //               ,:P67-CPT-FIN
        //                :IND-P67-CPT-FIN
        //               ,:P67-NUM-TST-FIN
        //                :IND-P67-NUM-TST-FIN
        //               ,:P67-TS-FINANZ
        //                :IND-P67-TS-FINANZ
        //               ,:P67-DUR-MM-FINANZ
        //                :IND-P67-DUR-MM-FINANZ
        //               ,:P67-DT-END-FINANZ-DB
        //                :IND-P67-DT-END-FINANZ
        //               ,:P67-AMM-1A-RAT
        //                :IND-P67-AMM-1A-RAT
        //               ,:P67-VAL-RISC-BENE
        //                :IND-P67-VAL-RISC-BENE
        //               ,:P67-AMM-RAT-END
        //                :IND-P67-AMM-RAT-END
        //               ,:P67-TS-CRE-RAT-FINANZ
        //                :IND-P67-TS-CRE-RAT-FINANZ
        //               ,:P67-IMP-FIN-REVOLVING
        //                :IND-P67-IMP-FIN-REVOLVING
        //               ,:P67-IMP-UTIL-C-REV
        //                :IND-P67-IMP-UTIL-C-REV
        //               ,:P67-IMP-RAT-REVOLVING
        //                :IND-P67-IMP-RAT-REVOLVING
        //               ,:P67-DT-1O-UTLZ-C-REV-DB
        //                :IND-P67-DT-1O-UTLZ-C-REV
        //               ,:P67-IMP-ASSTO
        //                :IND-P67-IMP-ASSTO
        //               ,:P67-PRE-VERS
        //                :IND-P67-PRE-VERS
        //               ,:P67-DT-SCAD-COP-DB
        //                :IND-P67-DT-SCAD-COP
        //               ,:P67-GG-DEL-MM-SCAD-RAT
        //                :IND-P67-GG-DEL-MM-SCAD-RAT
        //               ,:P67-FL-PRE-FIN
        //               ,:P67-DT-SCAD-1A-RAT-DB
        //                :IND-P67-DT-SCAD-1A-RAT
        //               ,:P67-DT-EROG-FINANZ-DB
        //                :IND-P67-DT-EROG-FINANZ
        //               ,:P67-DT-STIPULA-FINANZ-DB
        //                :IND-P67-DT-STIPULA-FINANZ
        //               ,:P67-MM-PREAMM
        //                :IND-P67-MM-PREAMM
        //               ,:P67-IMP-DEB-RES
        //                :IND-P67-IMP-DEB-RES
        //               ,:P67-IMP-RAT-FINANZ
        //                :IND-P67-IMP-RAT-FINANZ
        //               ,:P67-IMP-CANONE-ANTIC
        //                :IND-P67-IMP-CANONE-ANTIC
        //               ,:P67-PER-RAT-FINANZ
        //                :IND-P67-PER-RAT-FINANZ
        //               ,:P67-TP-MOD-PAG-RAT
        //                :IND-P67-TP-MOD-PAG-RAT
        //               ,:P67-TP-FINANZ-ER
        //                :IND-P67-TP-FINANZ-ER
        //               ,:P67-CAT-FINANZ-ER
        //                :IND-P67-CAT-FINANZ-ER
        //               ,:P67-VAL-RISC-END-LEAS
        //                :IND-P67-VAL-RISC-END-LEAS
        //               ,:P67-DT-EST-FINANZ-DB
        //                :IND-P67-DT-EST-FINANZ
        //               ,:P67-DT-MAN-COP-DB
        //                :IND-P67-DT-MAN-COP
        //               ,:P67-NUM-FINANZ
        //                :IND-P67-NUM-FINANZ
        //               ,:P67-TP-MOD-ACQS
        //                :IND-P67-TP-MOD-ACQS
        //           END-EXEC.
        estPoliCpiPrDao.fetchCIboEffP67(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX
            a570CloseCursorIbo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI_CPI_PR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_PROD_ESTNO
        //                ,CPT_FIN
        //                ,NUM_TST_FIN
        //                ,TS_FINANZ
        //                ,DUR_MM_FINANZ
        //                ,DT_END_FINANZ
        //                ,AMM_1A_RAT
        //                ,VAL_RISC_BENE
        //                ,AMM_RAT_END
        //                ,TS_CRE_RAT_FINANZ
        //                ,IMP_FIN_REVOLVING
        //                ,IMP_UTIL_C_REV
        //                ,IMP_RAT_REVOLVING
        //                ,DT_1O_UTLZ_C_REV
        //                ,IMP_ASSTO
        //                ,PRE_VERS
        //                ,DT_SCAD_COP
        //                ,GG_DEL_MM_SCAD_RAT
        //                ,FL_PRE_FIN
        //                ,DT_SCAD_1A_RAT
        //                ,DT_EROG_FINANZ
        //                ,DT_STIPULA_FINANZ
        //                ,MM_PREAMM
        //                ,IMP_DEB_RES
        //                ,IMP_RAT_FINANZ
        //                ,IMP_CANONE_ANTIC
        //                ,PER_RAT_FINANZ
        //                ,TP_MOD_PAG_RAT
        //                ,TP_FINANZ_ER
        //                ,CAT_FINANZ_ER
        //                ,VAL_RISC_END_LEAS
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,NUM_FINANZ
        //                ,TP_MOD_ACQS
        //             INTO
        //                :P67-ID-EST-POLI-CPI-PR
        //               ,:P67-ID-MOVI-CRZ
        //               ,:P67-ID-MOVI-CHIU
        //                :IND-P67-ID-MOVI-CHIU
        //               ,:P67-DT-INI-EFF-DB
        //               ,:P67-DT-END-EFF-DB
        //               ,:P67-COD-COMP-ANIA
        //               ,:P67-IB-OGG
        //               ,:P67-DS-RIGA
        //               ,:P67-DS-OPER-SQL
        //               ,:P67-DS-VER
        //               ,:P67-DS-TS-INI-CPTZ
        //               ,:P67-DS-TS-END-CPTZ
        //               ,:P67-DS-UTENTE
        //               ,:P67-DS-STATO-ELAB
        //               ,:P67-COD-PROD-ESTNO
        //               ,:P67-CPT-FIN
        //                :IND-P67-CPT-FIN
        //               ,:P67-NUM-TST-FIN
        //                :IND-P67-NUM-TST-FIN
        //               ,:P67-TS-FINANZ
        //                :IND-P67-TS-FINANZ
        //               ,:P67-DUR-MM-FINANZ
        //                :IND-P67-DUR-MM-FINANZ
        //               ,:P67-DT-END-FINANZ-DB
        //                :IND-P67-DT-END-FINANZ
        //               ,:P67-AMM-1A-RAT
        //                :IND-P67-AMM-1A-RAT
        //               ,:P67-VAL-RISC-BENE
        //                :IND-P67-VAL-RISC-BENE
        //               ,:P67-AMM-RAT-END
        //                :IND-P67-AMM-RAT-END
        //               ,:P67-TS-CRE-RAT-FINANZ
        //                :IND-P67-TS-CRE-RAT-FINANZ
        //               ,:P67-IMP-FIN-REVOLVING
        //                :IND-P67-IMP-FIN-REVOLVING
        //               ,:P67-IMP-UTIL-C-REV
        //                :IND-P67-IMP-UTIL-C-REV
        //               ,:P67-IMP-RAT-REVOLVING
        //                :IND-P67-IMP-RAT-REVOLVING
        //               ,:P67-DT-1O-UTLZ-C-REV-DB
        //                :IND-P67-DT-1O-UTLZ-C-REV
        //               ,:P67-IMP-ASSTO
        //                :IND-P67-IMP-ASSTO
        //               ,:P67-PRE-VERS
        //                :IND-P67-PRE-VERS
        //               ,:P67-DT-SCAD-COP-DB
        //                :IND-P67-DT-SCAD-COP
        //               ,:P67-GG-DEL-MM-SCAD-RAT
        //                :IND-P67-GG-DEL-MM-SCAD-RAT
        //               ,:P67-FL-PRE-FIN
        //               ,:P67-DT-SCAD-1A-RAT-DB
        //                :IND-P67-DT-SCAD-1A-RAT
        //               ,:P67-DT-EROG-FINANZ-DB
        //                :IND-P67-DT-EROG-FINANZ
        //               ,:P67-DT-STIPULA-FINANZ-DB
        //                :IND-P67-DT-STIPULA-FINANZ
        //               ,:P67-MM-PREAMM
        //                :IND-P67-MM-PREAMM
        //               ,:P67-IMP-DEB-RES
        //                :IND-P67-IMP-DEB-RES
        //               ,:P67-IMP-RAT-FINANZ
        //                :IND-P67-IMP-RAT-FINANZ
        //               ,:P67-IMP-CANONE-ANTIC
        //                :IND-P67-IMP-CANONE-ANTIC
        //               ,:P67-PER-RAT-FINANZ
        //                :IND-P67-PER-RAT-FINANZ
        //               ,:P67-TP-MOD-PAG-RAT
        //                :IND-P67-TP-MOD-PAG-RAT
        //               ,:P67-TP-FINANZ-ER
        //                :IND-P67-TP-FINANZ-ER
        //               ,:P67-CAT-FINANZ-ER
        //                :IND-P67-CAT-FINANZ-ER
        //               ,:P67-VAL-RISC-END-LEAS
        //                :IND-P67-VAL-RISC-END-LEAS
        //               ,:P67-DT-EST-FINANZ-DB
        //                :IND-P67-DT-EST-FINANZ
        //               ,:P67-DT-MAN-COP-DB
        //                :IND-P67-DT-MAN-COP
        //               ,:P67-NUM-FINANZ
        //                :IND-P67-NUM-FINANZ
        //               ,:P67-TP-MOD-ACQS
        //                :IND-P67-TP-MOD-ACQS
        //             FROM EST_POLI_CPI_PR
        //             WHERE     ID_EST_POLI_CPI_PR = :P67-ID-EST-POLI-CPI-PR
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        estPoliCpiPrDao.selectRec2(estPoliCpiPr.getP67IdEstPoliCpiPr(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-CPZ-P67 CURSOR FOR
        //              SELECT
        //                     ID_EST_POLI_CPI_PR
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,COD_PROD_ESTNO
        //                    ,CPT_FIN
        //                    ,NUM_TST_FIN
        //                    ,TS_FINANZ
        //                    ,DUR_MM_FINANZ
        //                    ,DT_END_FINANZ
        //                    ,AMM_1A_RAT
        //                    ,VAL_RISC_BENE
        //                    ,AMM_RAT_END
        //                    ,TS_CRE_RAT_FINANZ
        //                    ,IMP_FIN_REVOLVING
        //                    ,IMP_UTIL_C_REV
        //                    ,IMP_RAT_REVOLVING
        //                    ,DT_1O_UTLZ_C_REV
        //                    ,IMP_ASSTO
        //                    ,PRE_VERS
        //                    ,DT_SCAD_COP
        //                    ,GG_DEL_MM_SCAD_RAT
        //                    ,FL_PRE_FIN
        //                    ,DT_SCAD_1A_RAT
        //                    ,DT_EROG_FINANZ
        //                    ,DT_STIPULA_FINANZ
        //                    ,MM_PREAMM
        //                    ,IMP_DEB_RES
        //                    ,IMP_RAT_FINANZ
        //                    ,IMP_CANONE_ANTIC
        //                    ,PER_RAT_FINANZ
        //                    ,TP_MOD_PAG_RAT
        //                    ,TP_FINANZ_ER
        //                    ,CAT_FINANZ_ER
        //                    ,VAL_RISC_END_LEAS
        //                    ,DT_EST_FINANZ
        //                    ,DT_MAN_COP
        //                    ,NUM_FINANZ
        //                    ,TP_MOD_ACQS
        //              FROM EST_POLI_CPI_PR
        //              WHERE     IB_OGG = :P67-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_EST_POLI_CPI_PR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_EST_POLI_CPI_PR
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,COD_PROD_ESTNO
        //                ,CPT_FIN
        //                ,NUM_TST_FIN
        //                ,TS_FINANZ
        //                ,DUR_MM_FINANZ
        //                ,DT_END_FINANZ
        //                ,AMM_1A_RAT
        //                ,VAL_RISC_BENE
        //                ,AMM_RAT_END
        //                ,TS_CRE_RAT_FINANZ
        //                ,IMP_FIN_REVOLVING
        //                ,IMP_UTIL_C_REV
        //                ,IMP_RAT_REVOLVING
        //                ,DT_1O_UTLZ_C_REV
        //                ,IMP_ASSTO
        //                ,PRE_VERS
        //                ,DT_SCAD_COP
        //                ,GG_DEL_MM_SCAD_RAT
        //                ,FL_PRE_FIN
        //                ,DT_SCAD_1A_RAT
        //                ,DT_EROG_FINANZ
        //                ,DT_STIPULA_FINANZ
        //                ,MM_PREAMM
        //                ,IMP_DEB_RES
        //                ,IMP_RAT_FINANZ
        //                ,IMP_CANONE_ANTIC
        //                ,PER_RAT_FINANZ
        //                ,TP_MOD_PAG_RAT
        //                ,TP_FINANZ_ER
        //                ,CAT_FINANZ_ER
        //                ,VAL_RISC_END_LEAS
        //                ,DT_EST_FINANZ
        //                ,DT_MAN_COP
        //                ,NUM_FINANZ
        //                ,TP_MOD_ACQS
        //             INTO
        //                :P67-ID-EST-POLI-CPI-PR
        //               ,:P67-ID-MOVI-CRZ
        //               ,:P67-ID-MOVI-CHIU
        //                :IND-P67-ID-MOVI-CHIU
        //               ,:P67-DT-INI-EFF-DB
        //               ,:P67-DT-END-EFF-DB
        //               ,:P67-COD-COMP-ANIA
        //               ,:P67-IB-OGG
        //               ,:P67-DS-RIGA
        //               ,:P67-DS-OPER-SQL
        //               ,:P67-DS-VER
        //               ,:P67-DS-TS-INI-CPTZ
        //               ,:P67-DS-TS-END-CPTZ
        //               ,:P67-DS-UTENTE
        //               ,:P67-DS-STATO-ELAB
        //               ,:P67-COD-PROD-ESTNO
        //               ,:P67-CPT-FIN
        //                :IND-P67-CPT-FIN
        //               ,:P67-NUM-TST-FIN
        //                :IND-P67-NUM-TST-FIN
        //               ,:P67-TS-FINANZ
        //                :IND-P67-TS-FINANZ
        //               ,:P67-DUR-MM-FINANZ
        //                :IND-P67-DUR-MM-FINANZ
        //               ,:P67-DT-END-FINANZ-DB
        //                :IND-P67-DT-END-FINANZ
        //               ,:P67-AMM-1A-RAT
        //                :IND-P67-AMM-1A-RAT
        //               ,:P67-VAL-RISC-BENE
        //                :IND-P67-VAL-RISC-BENE
        //               ,:P67-AMM-RAT-END
        //                :IND-P67-AMM-RAT-END
        //               ,:P67-TS-CRE-RAT-FINANZ
        //                :IND-P67-TS-CRE-RAT-FINANZ
        //               ,:P67-IMP-FIN-REVOLVING
        //                :IND-P67-IMP-FIN-REVOLVING
        //               ,:P67-IMP-UTIL-C-REV
        //                :IND-P67-IMP-UTIL-C-REV
        //               ,:P67-IMP-RAT-REVOLVING
        //                :IND-P67-IMP-RAT-REVOLVING
        //               ,:P67-DT-1O-UTLZ-C-REV-DB
        //                :IND-P67-DT-1O-UTLZ-C-REV
        //               ,:P67-IMP-ASSTO
        //                :IND-P67-IMP-ASSTO
        //               ,:P67-PRE-VERS
        //                :IND-P67-PRE-VERS
        //               ,:P67-DT-SCAD-COP-DB
        //                :IND-P67-DT-SCAD-COP
        //               ,:P67-GG-DEL-MM-SCAD-RAT
        //                :IND-P67-GG-DEL-MM-SCAD-RAT
        //               ,:P67-FL-PRE-FIN
        //               ,:P67-DT-SCAD-1A-RAT-DB
        //                :IND-P67-DT-SCAD-1A-RAT
        //               ,:P67-DT-EROG-FINANZ-DB
        //                :IND-P67-DT-EROG-FINANZ
        //               ,:P67-DT-STIPULA-FINANZ-DB
        //                :IND-P67-DT-STIPULA-FINANZ
        //               ,:P67-MM-PREAMM
        //                :IND-P67-MM-PREAMM
        //               ,:P67-IMP-DEB-RES
        //                :IND-P67-IMP-DEB-RES
        //               ,:P67-IMP-RAT-FINANZ
        //                :IND-P67-IMP-RAT-FINANZ
        //               ,:P67-IMP-CANONE-ANTIC
        //                :IND-P67-IMP-CANONE-ANTIC
        //               ,:P67-PER-RAT-FINANZ
        //                :IND-P67-PER-RAT-FINANZ
        //               ,:P67-TP-MOD-PAG-RAT
        //                :IND-P67-TP-MOD-PAG-RAT
        //               ,:P67-TP-FINANZ-ER
        //                :IND-P67-TP-FINANZ-ER
        //               ,:P67-CAT-FINANZ-ER
        //                :IND-P67-CAT-FINANZ-ER
        //               ,:P67-VAL-RISC-END-LEAS
        //                :IND-P67-VAL-RISC-END-LEAS
        //               ,:P67-DT-EST-FINANZ-DB
        //                :IND-P67-DT-EST-FINANZ
        //               ,:P67-DT-MAN-COP-DB
        //                :IND-P67-DT-MAN-COP
        //               ,:P67-NUM-FINANZ
        //                :IND-P67-NUM-FINANZ
        //               ,:P67-TP-MOD-ACQS
        //                :IND-P67-TP-MOD-ACQS
        //             FROM EST_POLI_CPI_PR
        //             WHERE     IB_OGG = :P67-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        estPoliCpiPrDao.selectRec3(estPoliCpiPr.getP67IbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-CPZ-P67
        //           END-EXEC.
        estPoliCpiPrDao.openCIboCpzP67(estPoliCpiPr.getP67IbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-CPZ-P67
        //           END-EXEC.
        estPoliCpiPrDao.closeCIboCpzP67();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-CPZ-P67
        //           INTO
        //                :P67-ID-EST-POLI-CPI-PR
        //               ,:P67-ID-MOVI-CRZ
        //               ,:P67-ID-MOVI-CHIU
        //                :IND-P67-ID-MOVI-CHIU
        //               ,:P67-DT-INI-EFF-DB
        //               ,:P67-DT-END-EFF-DB
        //               ,:P67-COD-COMP-ANIA
        //               ,:P67-IB-OGG
        //               ,:P67-DS-RIGA
        //               ,:P67-DS-OPER-SQL
        //               ,:P67-DS-VER
        //               ,:P67-DS-TS-INI-CPTZ
        //               ,:P67-DS-TS-END-CPTZ
        //               ,:P67-DS-UTENTE
        //               ,:P67-DS-STATO-ELAB
        //               ,:P67-COD-PROD-ESTNO
        //               ,:P67-CPT-FIN
        //                :IND-P67-CPT-FIN
        //               ,:P67-NUM-TST-FIN
        //                :IND-P67-NUM-TST-FIN
        //               ,:P67-TS-FINANZ
        //                :IND-P67-TS-FINANZ
        //               ,:P67-DUR-MM-FINANZ
        //                :IND-P67-DUR-MM-FINANZ
        //               ,:P67-DT-END-FINANZ-DB
        //                :IND-P67-DT-END-FINANZ
        //               ,:P67-AMM-1A-RAT
        //                :IND-P67-AMM-1A-RAT
        //               ,:P67-VAL-RISC-BENE
        //                :IND-P67-VAL-RISC-BENE
        //               ,:P67-AMM-RAT-END
        //                :IND-P67-AMM-RAT-END
        //               ,:P67-TS-CRE-RAT-FINANZ
        //                :IND-P67-TS-CRE-RAT-FINANZ
        //               ,:P67-IMP-FIN-REVOLVING
        //                :IND-P67-IMP-FIN-REVOLVING
        //               ,:P67-IMP-UTIL-C-REV
        //                :IND-P67-IMP-UTIL-C-REV
        //               ,:P67-IMP-RAT-REVOLVING
        //                :IND-P67-IMP-RAT-REVOLVING
        //               ,:P67-DT-1O-UTLZ-C-REV-DB
        //                :IND-P67-DT-1O-UTLZ-C-REV
        //               ,:P67-IMP-ASSTO
        //                :IND-P67-IMP-ASSTO
        //               ,:P67-PRE-VERS
        //                :IND-P67-PRE-VERS
        //               ,:P67-DT-SCAD-COP-DB
        //                :IND-P67-DT-SCAD-COP
        //               ,:P67-GG-DEL-MM-SCAD-RAT
        //                :IND-P67-GG-DEL-MM-SCAD-RAT
        //               ,:P67-FL-PRE-FIN
        //               ,:P67-DT-SCAD-1A-RAT-DB
        //                :IND-P67-DT-SCAD-1A-RAT
        //               ,:P67-DT-EROG-FINANZ-DB
        //                :IND-P67-DT-EROG-FINANZ
        //               ,:P67-DT-STIPULA-FINANZ-DB
        //                :IND-P67-DT-STIPULA-FINANZ
        //               ,:P67-MM-PREAMM
        //                :IND-P67-MM-PREAMM
        //               ,:P67-IMP-DEB-RES
        //                :IND-P67-IMP-DEB-RES
        //               ,:P67-IMP-RAT-FINANZ
        //                :IND-P67-IMP-RAT-FINANZ
        //               ,:P67-IMP-CANONE-ANTIC
        //                :IND-P67-IMP-CANONE-ANTIC
        //               ,:P67-PER-RAT-FINANZ
        //                :IND-P67-PER-RAT-FINANZ
        //               ,:P67-TP-MOD-PAG-RAT
        //                :IND-P67-TP-MOD-PAG-RAT
        //               ,:P67-TP-FINANZ-ER
        //                :IND-P67-TP-FINANZ-ER
        //               ,:P67-CAT-FINANZ-ER
        //                :IND-P67-CAT-FINANZ-ER
        //               ,:P67-VAL-RISC-END-LEAS
        //                :IND-P67-VAL-RISC-END-LEAS
        //               ,:P67-DT-EST-FINANZ-DB
        //                :IND-P67-DT-EST-FINANZ
        //               ,:P67-DT-MAN-COP-DB
        //                :IND-P67-DT-MAN-COP
        //               ,:P67-NUM-FINANZ
        //                :IND-P67-NUM-FINANZ
        //               ,:P67-TP-MOD-ACQS
        //                :IND-P67-TP-MOD-ACQS
        //           END-EXEC.
        estPoliCpiPrDao.fetchCIboCpzP67(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX
            b570CloseCursorIboCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-P67-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO P67-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-ID-MOVI-CHIU-NULL
            estPoliCpiPr.getP67IdMoviChiu().setP67IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67IdMoviChiu.Len.P67_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-P67-CPT-FIN = -1
        //              MOVE HIGH-VALUES TO P67-CPT-FIN-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getCptFin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-CPT-FIN-NULL
            estPoliCpiPr.getP67CptFin().setP67CptFinNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67CptFin.Len.P67_CPT_FIN_NULL));
        }
        // COB_CODE: IF IND-P67-NUM-TST-FIN = -1
        //              MOVE HIGH-VALUES TO P67-NUM-TST-FIN-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getNumTstFin() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-NUM-TST-FIN-NULL
            estPoliCpiPr.getP67NumTstFin().setP67NumTstFinNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67NumTstFin.Len.P67_NUM_TST_FIN_NULL));
        }
        // COB_CODE: IF IND-P67-TS-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-TS-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getTsFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-TS-FINANZ-NULL
            estPoliCpiPr.getP67TsFinanz().setP67TsFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67TsFinanz.Len.P67_TS_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P67-DUR-MM-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-DUR-MM-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDurMmFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-DUR-MM-FINANZ-NULL
            estPoliCpiPr.getP67DurMmFinanz().setP67DurMmFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67DurMmFinanz.Len.P67_DUR_MM_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P67-DT-END-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-DT-END-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtEndFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-DT-END-FINANZ-NULL
            estPoliCpiPr.getP67DtEndFinanz().setP67DtEndFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67DtEndFinanz.Len.P67_DT_END_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P67-AMM-1A-RAT = -1
        //              MOVE HIGH-VALUES TO P67-AMM-1A-RAT-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getAmm1aRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-AMM-1A-RAT-NULL
            estPoliCpiPr.getP67Amm1aRat().setP67Amm1aRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67Amm1aRat.Len.P67_AMM1A_RAT_NULL));
        }
        // COB_CODE: IF IND-P67-VAL-RISC-BENE = -1
        //              MOVE HIGH-VALUES TO P67-VAL-RISC-BENE-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getValRiscBene() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-VAL-RISC-BENE-NULL
            estPoliCpiPr.getP67ValRiscBene().setP67ValRiscBeneNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67ValRiscBene.Len.P67_VAL_RISC_BENE_NULL));
        }
        // COB_CODE: IF IND-P67-AMM-RAT-END = -1
        //              MOVE HIGH-VALUES TO P67-AMM-RAT-END-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getAmmRatEnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-AMM-RAT-END-NULL
            estPoliCpiPr.getP67AmmRatEnd().setP67AmmRatEndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67AmmRatEnd.Len.P67_AMM_RAT_END_NULL));
        }
        // COB_CODE: IF IND-P67-TS-CRE-RAT-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-TS-CRE-RAT-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getTsCreRatFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-TS-CRE-RAT-FINANZ-NULL
            estPoliCpiPr.getP67TsCreRatFinanz().setP67TsCreRatFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67TsCreRatFinanz.Len.P67_TS_CRE_RAT_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P67-IMP-FIN-REVOLVING = -1
        //              MOVE HIGH-VALUES TO P67-IMP-FIN-REVOLVING-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getImpFinRevolving() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-IMP-FIN-REVOLVING-NULL
            estPoliCpiPr.getP67ImpFinRevolving().setP67ImpFinRevolvingNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67ImpFinRevolving.Len.P67_IMP_FIN_REVOLVING_NULL));
        }
        // COB_CODE: IF IND-P67-IMP-UTIL-C-REV = -1
        //              MOVE HIGH-VALUES TO P67-IMP-UTIL-C-REV-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getImpUtilCRev() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-IMP-UTIL-C-REV-NULL
            estPoliCpiPr.getP67ImpUtilCRev().setP67ImpUtilCRevNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67ImpUtilCRev.Len.P67_IMP_UTIL_C_REV_NULL));
        }
        // COB_CODE: IF IND-P67-IMP-RAT-REVOLVING = -1
        //              MOVE HIGH-VALUES TO P67-IMP-RAT-REVOLVING-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getImpRatRevolving() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-IMP-RAT-REVOLVING-NULL
            estPoliCpiPr.getP67ImpRatRevolving().setP67ImpRatRevolvingNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67ImpRatRevolving.Len.P67_IMP_RAT_REVOLVING_NULL));
        }
        // COB_CODE: IF IND-P67-DT-1O-UTLZ-C-REV = -1
        //              MOVE HIGH-VALUES TO P67-DT-1O-UTLZ-C-REV-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDt1oUtlzCRev() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-DT-1O-UTLZ-C-REV-NULL
            estPoliCpiPr.getP67Dt1oUtlzCRev().setP67Dt1oUtlzCRevNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67Dt1oUtlzCRev.Len.P67_DT1O_UTLZ_C_REV_NULL));
        }
        // COB_CODE: IF IND-P67-IMP-ASSTO = -1
        //              MOVE HIGH-VALUES TO P67-IMP-ASSTO-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getImpAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-IMP-ASSTO-NULL
            estPoliCpiPr.getP67ImpAssto().setP67ImpAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67ImpAssto.Len.P67_IMP_ASSTO_NULL));
        }
        // COB_CODE: IF IND-P67-PRE-VERS = -1
        //              MOVE HIGH-VALUES TO P67-PRE-VERS-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getPreVers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-PRE-VERS-NULL
            estPoliCpiPr.getP67PreVers().setP67PreVersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67PreVers.Len.P67_PRE_VERS_NULL));
        }
        // COB_CODE: IF IND-P67-DT-SCAD-COP = -1
        //              MOVE HIGH-VALUES TO P67-DT-SCAD-COP-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtScadCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-DT-SCAD-COP-NULL
            estPoliCpiPr.getP67DtScadCop().setP67DtScadCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67DtScadCop.Len.P67_DT_SCAD_COP_NULL));
        }
        // COB_CODE: IF IND-P67-GG-DEL-MM-SCAD-RAT = -1
        //              MOVE HIGH-VALUES TO P67-GG-DEL-MM-SCAD-RAT-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getGgDelMmScadRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-GG-DEL-MM-SCAD-RAT-NULL
            estPoliCpiPr.getP67GgDelMmScadRat().setP67GgDelMmScadRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67GgDelMmScadRat.Len.P67_GG_DEL_MM_SCAD_RAT_NULL));
        }
        // COB_CODE: IF IND-P67-DT-SCAD-1A-RAT = -1
        //              MOVE HIGH-VALUES TO P67-DT-SCAD-1A-RAT-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtScad1aRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-DT-SCAD-1A-RAT-NULL
            estPoliCpiPr.getP67DtScad1aRat().setP67DtScad1aRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67DtScad1aRat.Len.P67_DT_SCAD1A_RAT_NULL));
        }
        // COB_CODE: IF IND-P67-DT-EROG-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-DT-EROG-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtErogFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-DT-EROG-FINANZ-NULL
            estPoliCpiPr.getP67DtErogFinanz().setP67DtErogFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67DtErogFinanz.Len.P67_DT_EROG_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P67-DT-STIPULA-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-DT-STIPULA-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtStipulaFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-DT-STIPULA-FINANZ-NULL
            estPoliCpiPr.getP67DtStipulaFinanz().setP67DtStipulaFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67DtStipulaFinanz.Len.P67_DT_STIPULA_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P67-MM-PREAMM = -1
        //              MOVE HIGH-VALUES TO P67-MM-PREAMM-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getMmPreamm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-MM-PREAMM-NULL
            estPoliCpiPr.getP67MmPreamm().setP67MmPreammNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67MmPreamm.Len.P67_MM_PREAMM_NULL));
        }
        // COB_CODE: IF IND-P67-IMP-DEB-RES = -1
        //              MOVE HIGH-VALUES TO P67-IMP-DEB-RES-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getImpDebRes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-IMP-DEB-RES-NULL
            estPoliCpiPr.getP67ImpDebRes().setP67ImpDebResNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67ImpDebRes.Len.P67_IMP_DEB_RES_NULL));
        }
        // COB_CODE: IF IND-P67-IMP-RAT-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-IMP-RAT-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getImpRatFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-IMP-RAT-FINANZ-NULL
            estPoliCpiPr.getP67ImpRatFinanz().setP67ImpRatFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67ImpRatFinanz.Len.P67_IMP_RAT_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P67-IMP-CANONE-ANTIC = -1
        //              MOVE HIGH-VALUES TO P67-IMP-CANONE-ANTIC-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getImpCanoneAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-IMP-CANONE-ANTIC-NULL
            estPoliCpiPr.getP67ImpCanoneAntic().setP67ImpCanoneAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67ImpCanoneAntic.Len.P67_IMP_CANONE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-P67-PER-RAT-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-PER-RAT-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getPerRatFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-PER-RAT-FINANZ-NULL
            estPoliCpiPr.getP67PerRatFinanz().setP67PerRatFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67PerRatFinanz.Len.P67_PER_RAT_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P67-TP-MOD-PAG-RAT = -1
        //              MOVE HIGH-VALUES TO P67-TP-MOD-PAG-RAT-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getTpModPagRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-TP-MOD-PAG-RAT-NULL
            estPoliCpiPr.setP67TpModPagRat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstPoliCpiPrIdbsp670.Len.P67_TP_MOD_PAG_RAT));
        }
        // COB_CODE: IF IND-P67-TP-FINANZ-ER = -1
        //              MOVE HIGH-VALUES TO P67-TP-FINANZ-ER-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getTpFinanzEr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-TP-FINANZ-ER-NULL
            estPoliCpiPr.setP67TpFinanzEr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstPoliCpiPrIdbsp670.Len.P67_TP_FINANZ_ER));
        }
        // COB_CODE: IF IND-P67-CAT-FINANZ-ER = -1
        //              MOVE HIGH-VALUES TO P67-CAT-FINANZ-ER-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getCatFinanzEr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-CAT-FINANZ-ER-NULL
            estPoliCpiPr.setP67CatFinanzEr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstPoliCpiPrIdbsp670.Len.P67_CAT_FINANZ_ER));
        }
        // COB_CODE: IF IND-P67-VAL-RISC-END-LEAS = -1
        //              MOVE HIGH-VALUES TO P67-VAL-RISC-END-LEAS-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getValRiscEndLeas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-VAL-RISC-END-LEAS-NULL
            estPoliCpiPr.getP67ValRiscEndLeas().setP67ValRiscEndLeasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67ValRiscEndLeas.Len.P67_VAL_RISC_END_LEAS_NULL));
        }
        // COB_CODE: IF IND-P67-DT-EST-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-DT-EST-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtEstFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-DT-EST-FINANZ-NULL
            estPoliCpiPr.getP67DtEstFinanz().setP67DtEstFinanzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67DtEstFinanz.Len.P67_DT_EST_FINANZ_NULL));
        }
        // COB_CODE: IF IND-P67-DT-MAN-COP = -1
        //              MOVE HIGH-VALUES TO P67-DT-MAN-COP-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtManCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-DT-MAN-COP-NULL
            estPoliCpiPr.getP67DtManCop().setP67DtManCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67DtManCop.Len.P67_DT_MAN_COP_NULL));
        }
        // COB_CODE: IF IND-P67-NUM-FINANZ = -1
        //              MOVE HIGH-VALUES TO P67-NUM-FINANZ-NULL
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getNumFinanz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-NUM-FINANZ-NULL
            estPoliCpiPr.setP67NumFinanz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstPoliCpiPrIdbsp670.Len.P67_NUM_FINANZ));
        }
        // COB_CODE: IF IND-P67-TP-MOD-ACQS = -1
        //              MOVE HIGH-VALUES TO P67-TP-MOD-ACQS-NULL
        //           END-IF.
        if (ws.getIndEstPoliCpiPr().getTpModAcqs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P67-TP-MOD-ACQS-NULL
            estPoliCpiPr.setP67TpModAcqs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, EstPoliCpiPrIdbsp670.Len.P67_TP_MOD_ACQS));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO P67-DS-OPER-SQL
        estPoliCpiPr.setP67DsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO P67-DS-VER
        estPoliCpiPr.setP67DsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P67-DS-UTENTE
        estPoliCpiPr.setP67DsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO P67-DS-STATO-ELAB.
        estPoliCpiPr.setP67DsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO P67-DS-OPER-SQL
        estPoliCpiPr.setP67DsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO P67-DS-UTENTE.
        estPoliCpiPr.setP67DsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF P67-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-P67-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67IdMoviChiu().getP67IdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-ID-MOVI-CHIU
            ws.getIndEstPoliCpiPr().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-ID-MOVI-CHIU
            ws.getIndEstPoliCpiPr().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF P67-CPT-FIN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-CPT-FIN
        //           ELSE
        //              MOVE 0 TO IND-P67-CPT-FIN
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67CptFin().getP67CptFinNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-CPT-FIN
            ws.getIndEstPoliCpiPr().setCptFin(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-CPT-FIN
            ws.getIndEstPoliCpiPr().setCptFin(((short)0));
        }
        // COB_CODE: IF P67-NUM-TST-FIN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-NUM-TST-FIN
        //           ELSE
        //              MOVE 0 TO IND-P67-NUM-TST-FIN
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67NumTstFin().getP67NumTstFinNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-NUM-TST-FIN
            ws.getIndEstPoliCpiPr().setNumTstFin(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-NUM-TST-FIN
            ws.getIndEstPoliCpiPr().setNumTstFin(((short)0));
        }
        // COB_CODE: IF P67-TS-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-TS-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-TS-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67TsFinanz().getP67TsFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-TS-FINANZ
            ws.getIndEstPoliCpiPr().setTsFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-TS-FINANZ
            ws.getIndEstPoliCpiPr().setTsFinanz(((short)0));
        }
        // COB_CODE: IF P67-DUR-MM-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-DUR-MM-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-DUR-MM-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67DurMmFinanz().getP67DurMmFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-DUR-MM-FINANZ
            ws.getIndEstPoliCpiPr().setDurMmFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-DUR-MM-FINANZ
            ws.getIndEstPoliCpiPr().setDurMmFinanz(((short)0));
        }
        // COB_CODE: IF P67-DT-END-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-DT-END-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-DT-END-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67DtEndFinanz().getP67DtEndFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-DT-END-FINANZ
            ws.getIndEstPoliCpiPr().setDtEndFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-DT-END-FINANZ
            ws.getIndEstPoliCpiPr().setDtEndFinanz(((short)0));
        }
        // COB_CODE: IF P67-AMM-1A-RAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-AMM-1A-RAT
        //           ELSE
        //              MOVE 0 TO IND-P67-AMM-1A-RAT
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67Amm1aRat().getP67Amm1aRatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-AMM-1A-RAT
            ws.getIndEstPoliCpiPr().setAmm1aRat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-AMM-1A-RAT
            ws.getIndEstPoliCpiPr().setAmm1aRat(((short)0));
        }
        // COB_CODE: IF P67-VAL-RISC-BENE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-VAL-RISC-BENE
        //           ELSE
        //              MOVE 0 TO IND-P67-VAL-RISC-BENE
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67ValRiscBene().getP67ValRiscBeneNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-VAL-RISC-BENE
            ws.getIndEstPoliCpiPr().setValRiscBene(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-VAL-RISC-BENE
            ws.getIndEstPoliCpiPr().setValRiscBene(((short)0));
        }
        // COB_CODE: IF P67-AMM-RAT-END-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-AMM-RAT-END
        //           ELSE
        //              MOVE 0 TO IND-P67-AMM-RAT-END
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67AmmRatEnd().getP67AmmRatEndNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-AMM-RAT-END
            ws.getIndEstPoliCpiPr().setAmmRatEnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-AMM-RAT-END
            ws.getIndEstPoliCpiPr().setAmmRatEnd(((short)0));
        }
        // COB_CODE: IF P67-TS-CRE-RAT-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-TS-CRE-RAT-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-TS-CRE-RAT-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67TsCreRatFinanz().getP67TsCreRatFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-TS-CRE-RAT-FINANZ
            ws.getIndEstPoliCpiPr().setTsCreRatFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-TS-CRE-RAT-FINANZ
            ws.getIndEstPoliCpiPr().setTsCreRatFinanz(((short)0));
        }
        // COB_CODE: IF P67-IMP-FIN-REVOLVING-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-IMP-FIN-REVOLVING
        //           ELSE
        //              MOVE 0 TO IND-P67-IMP-FIN-REVOLVING
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67ImpFinRevolving().getP67ImpFinRevolvingNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-IMP-FIN-REVOLVING
            ws.getIndEstPoliCpiPr().setImpFinRevolving(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-IMP-FIN-REVOLVING
            ws.getIndEstPoliCpiPr().setImpFinRevolving(((short)0));
        }
        // COB_CODE: IF P67-IMP-UTIL-C-REV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-IMP-UTIL-C-REV
        //           ELSE
        //              MOVE 0 TO IND-P67-IMP-UTIL-C-REV
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67ImpUtilCRev().getP67ImpUtilCRevNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-IMP-UTIL-C-REV
            ws.getIndEstPoliCpiPr().setImpUtilCRev(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-IMP-UTIL-C-REV
            ws.getIndEstPoliCpiPr().setImpUtilCRev(((short)0));
        }
        // COB_CODE: IF P67-IMP-RAT-REVOLVING-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-IMP-RAT-REVOLVING
        //           ELSE
        //              MOVE 0 TO IND-P67-IMP-RAT-REVOLVING
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67ImpRatRevolving().getP67ImpRatRevolvingNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-IMP-RAT-REVOLVING
            ws.getIndEstPoliCpiPr().setImpRatRevolving(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-IMP-RAT-REVOLVING
            ws.getIndEstPoliCpiPr().setImpRatRevolving(((short)0));
        }
        // COB_CODE: IF P67-DT-1O-UTLZ-C-REV-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-DT-1O-UTLZ-C-REV
        //           ELSE
        //              MOVE 0 TO IND-P67-DT-1O-UTLZ-C-REV
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67Dt1oUtlzCRev().getP67Dt1oUtlzCRevNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-DT-1O-UTLZ-C-REV
            ws.getIndEstPoliCpiPr().setDt1oUtlzCRev(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-DT-1O-UTLZ-C-REV
            ws.getIndEstPoliCpiPr().setDt1oUtlzCRev(((short)0));
        }
        // COB_CODE: IF P67-IMP-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-IMP-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-P67-IMP-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67ImpAssto().getP67ImpAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-IMP-ASSTO
            ws.getIndEstPoliCpiPr().setImpAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-IMP-ASSTO
            ws.getIndEstPoliCpiPr().setImpAssto(((short)0));
        }
        // COB_CODE: IF P67-PRE-VERS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-PRE-VERS
        //           ELSE
        //              MOVE 0 TO IND-P67-PRE-VERS
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67PreVers().getP67PreVersNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-PRE-VERS
            ws.getIndEstPoliCpiPr().setPreVers(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-PRE-VERS
            ws.getIndEstPoliCpiPr().setPreVers(((short)0));
        }
        // COB_CODE: IF P67-DT-SCAD-COP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-DT-SCAD-COP
        //           ELSE
        //              MOVE 0 TO IND-P67-DT-SCAD-COP
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67DtScadCop().getP67DtScadCopNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-DT-SCAD-COP
            ws.getIndEstPoliCpiPr().setDtScadCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-DT-SCAD-COP
            ws.getIndEstPoliCpiPr().setDtScadCop(((short)0));
        }
        // COB_CODE: IF P67-GG-DEL-MM-SCAD-RAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-GG-DEL-MM-SCAD-RAT
        //           ELSE
        //              MOVE 0 TO IND-P67-GG-DEL-MM-SCAD-RAT
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67GgDelMmScadRat().getP67GgDelMmScadRatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-GG-DEL-MM-SCAD-RAT
            ws.getIndEstPoliCpiPr().setGgDelMmScadRat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-GG-DEL-MM-SCAD-RAT
            ws.getIndEstPoliCpiPr().setGgDelMmScadRat(((short)0));
        }
        // COB_CODE: IF P67-DT-SCAD-1A-RAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-DT-SCAD-1A-RAT
        //           ELSE
        //              MOVE 0 TO IND-P67-DT-SCAD-1A-RAT
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67DtScad1aRat().getP67DtScad1aRatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-DT-SCAD-1A-RAT
            ws.getIndEstPoliCpiPr().setDtScad1aRat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-DT-SCAD-1A-RAT
            ws.getIndEstPoliCpiPr().setDtScad1aRat(((short)0));
        }
        // COB_CODE: IF P67-DT-EROG-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-DT-EROG-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-DT-EROG-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67DtErogFinanz().getP67DtErogFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-DT-EROG-FINANZ
            ws.getIndEstPoliCpiPr().setDtErogFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-DT-EROG-FINANZ
            ws.getIndEstPoliCpiPr().setDtErogFinanz(((short)0));
        }
        // COB_CODE: IF P67-DT-STIPULA-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-DT-STIPULA-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-DT-STIPULA-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67DtStipulaFinanz().getP67DtStipulaFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-DT-STIPULA-FINANZ
            ws.getIndEstPoliCpiPr().setDtStipulaFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-DT-STIPULA-FINANZ
            ws.getIndEstPoliCpiPr().setDtStipulaFinanz(((short)0));
        }
        // COB_CODE: IF P67-MM-PREAMM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-MM-PREAMM
        //           ELSE
        //              MOVE 0 TO IND-P67-MM-PREAMM
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67MmPreamm().getP67MmPreammNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-MM-PREAMM
            ws.getIndEstPoliCpiPr().setMmPreamm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-MM-PREAMM
            ws.getIndEstPoliCpiPr().setMmPreamm(((short)0));
        }
        // COB_CODE: IF P67-IMP-DEB-RES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-IMP-DEB-RES
        //           ELSE
        //              MOVE 0 TO IND-P67-IMP-DEB-RES
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67ImpDebRes().getP67ImpDebResNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-IMP-DEB-RES
            ws.getIndEstPoliCpiPr().setImpDebRes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-IMP-DEB-RES
            ws.getIndEstPoliCpiPr().setImpDebRes(((short)0));
        }
        // COB_CODE: IF P67-IMP-RAT-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-IMP-RAT-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-IMP-RAT-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67ImpRatFinanz().getP67ImpRatFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-IMP-RAT-FINANZ
            ws.getIndEstPoliCpiPr().setImpRatFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-IMP-RAT-FINANZ
            ws.getIndEstPoliCpiPr().setImpRatFinanz(((short)0));
        }
        // COB_CODE: IF P67-IMP-CANONE-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-IMP-CANONE-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-P67-IMP-CANONE-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67ImpCanoneAntic().getP67ImpCanoneAnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-IMP-CANONE-ANTIC
            ws.getIndEstPoliCpiPr().setImpCanoneAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-IMP-CANONE-ANTIC
            ws.getIndEstPoliCpiPr().setImpCanoneAntic(((short)0));
        }
        // COB_CODE: IF P67-PER-RAT-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-PER-RAT-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-PER-RAT-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67PerRatFinanz().getP67PerRatFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-PER-RAT-FINANZ
            ws.getIndEstPoliCpiPr().setPerRatFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-PER-RAT-FINANZ
            ws.getIndEstPoliCpiPr().setPerRatFinanz(((short)0));
        }
        // COB_CODE: IF P67-TP-MOD-PAG-RAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-TP-MOD-PAG-RAT
        //           ELSE
        //              MOVE 0 TO IND-P67-TP-MOD-PAG-RAT
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67TpModPagRatFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-TP-MOD-PAG-RAT
            ws.getIndEstPoliCpiPr().setTpModPagRat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-TP-MOD-PAG-RAT
            ws.getIndEstPoliCpiPr().setTpModPagRat(((short)0));
        }
        // COB_CODE: IF P67-TP-FINANZ-ER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-TP-FINANZ-ER
        //           ELSE
        //              MOVE 0 TO IND-P67-TP-FINANZ-ER
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67TpFinanzErFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-TP-FINANZ-ER
            ws.getIndEstPoliCpiPr().setTpFinanzEr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-TP-FINANZ-ER
            ws.getIndEstPoliCpiPr().setTpFinanzEr(((short)0));
        }
        // COB_CODE: IF P67-CAT-FINANZ-ER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-CAT-FINANZ-ER
        //           ELSE
        //              MOVE 0 TO IND-P67-CAT-FINANZ-ER
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67CatFinanzErFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-CAT-FINANZ-ER
            ws.getIndEstPoliCpiPr().setCatFinanzEr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-CAT-FINANZ-ER
            ws.getIndEstPoliCpiPr().setCatFinanzEr(((short)0));
        }
        // COB_CODE: IF P67-VAL-RISC-END-LEAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-VAL-RISC-END-LEAS
        //           ELSE
        //              MOVE 0 TO IND-P67-VAL-RISC-END-LEAS
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67ValRiscEndLeas().getP67ValRiscEndLeasNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-VAL-RISC-END-LEAS
            ws.getIndEstPoliCpiPr().setValRiscEndLeas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-VAL-RISC-END-LEAS
            ws.getIndEstPoliCpiPr().setValRiscEndLeas(((short)0));
        }
        // COB_CODE: IF P67-DT-EST-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-DT-EST-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-DT-EST-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67DtEstFinanz().getP67DtEstFinanzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-DT-EST-FINANZ
            ws.getIndEstPoliCpiPr().setDtEstFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-DT-EST-FINANZ
            ws.getIndEstPoliCpiPr().setDtEstFinanz(((short)0));
        }
        // COB_CODE: IF P67-DT-MAN-COP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-DT-MAN-COP
        //           ELSE
        //              MOVE 0 TO IND-P67-DT-MAN-COP
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67DtManCop().getP67DtManCopNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-DT-MAN-COP
            ws.getIndEstPoliCpiPr().setDtManCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-DT-MAN-COP
            ws.getIndEstPoliCpiPr().setDtManCop(((short)0));
        }
        // COB_CODE: IF P67-NUM-FINANZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-NUM-FINANZ
        //           ELSE
        //              MOVE 0 TO IND-P67-NUM-FINANZ
        //           END-IF
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67NumFinanz(), EstPoliCpiPrIdbsp670.Len.P67_NUM_FINANZ)) {
            // COB_CODE: MOVE -1 TO IND-P67-NUM-FINANZ
            ws.getIndEstPoliCpiPr().setNumFinanz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-NUM-FINANZ
            ws.getIndEstPoliCpiPr().setNumFinanz(((short)0));
        }
        // COB_CODE: IF P67-TP-MOD-ACQS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-P67-TP-MOD-ACQS
        //           ELSE
        //              MOVE 0 TO IND-P67-TP-MOD-ACQS
        //           END-IF.
        if (Characters.EQ_HIGH.test(estPoliCpiPr.getP67TpModAcqsFormatted())) {
            // COB_CODE: MOVE -1 TO IND-P67-TP-MOD-ACQS
            ws.getIndEstPoliCpiPr().setTpModAcqs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-P67-TP-MOD-ACQS
            ws.getIndEstPoliCpiPr().setTpModAcqs(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : P67-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE EST-POLI-CPI-PR TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(estPoliCpiPr.getEstPoliCpiPrFormatted());
        // COB_CODE: MOVE P67-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(estPoliCpiPr.getP67IdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO P67-ID-MOVI-CHIU
                estPoliCpiPr.getP67IdMoviChiu().setP67IdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO P67-DS-TS-END-CPTZ
                estPoliCpiPr.setP67DsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO P67-ID-MOVI-CRZ
                    estPoliCpiPr.setP67IdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO P67-ID-MOVI-CHIU-NULL
                    estPoliCpiPr.getP67IdMoviChiu().setP67IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67IdMoviChiu.Len.P67_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO P67-DT-END-EFF
                    estPoliCpiPr.setP67DtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO P67-DS-TS-INI-CPTZ
                    estPoliCpiPr.setP67DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO P67-DS-TS-END-CPTZ
                    estPoliCpiPr.setP67DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE EST-POLI-CPI-PR TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(estPoliCpiPr.getEstPoliCpiPrFormatted());
        // COB_CODE: MOVE P67-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(estPoliCpiPr.getP67IdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO EST-POLI-CPI-PR.
        estPoliCpiPr.setEstPoliCpiPrFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO P67-ID-MOVI-CRZ.
        estPoliCpiPr.setP67IdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO P67-ID-MOVI-CHIU-NULL.
        estPoliCpiPr.getP67IdMoviChiu().setP67IdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P67IdMoviChiu.Len.P67_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO P67-DT-INI-EFF.
        estPoliCpiPr.setP67DtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO P67-DT-END-EFF.
        estPoliCpiPr.setP67DtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO P67-DS-TS-INI-CPTZ.
        estPoliCpiPr.setP67DsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO P67-DS-TS-END-CPTZ.
        estPoliCpiPr.setP67DsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO P67-COD-COMP-ANIA.
        estPoliCpiPr.setP67CodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE P67-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67DtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P67-DT-INI-EFF-DB
        ws.getEstPoliCpiPrDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE P67-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67DtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO P67-DT-END-EFF-DB
        ws.getEstPoliCpiPrDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-P67-DT-END-FINANZ = 0
        //               MOVE WS-DATE-X      TO P67-DT-END-FINANZ-DB
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtEndFinanz() == 0) {
            // COB_CODE: MOVE P67-DT-END-FINANZ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67DtEndFinanz().getP67DtEndFinanz(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P67-DT-END-FINANZ-DB
            ws.getEstPoliCpiPrDb().setDecorDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P67-DT-1O-UTLZ-C-REV = 0
        //               MOVE WS-DATE-X      TO P67-DT-1O-UTLZ-C-REV-DB
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDt1oUtlzCRev() == 0) {
            // COB_CODE: MOVE P67-DT-1O-UTLZ-C-REV TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67Dt1oUtlzCRev().getP67Dt1oUtlzCRev(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P67-DT-1O-UTLZ-C-REV-DB
            ws.getEstPoliCpiPrDb().setScadDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P67-DT-SCAD-COP = 0
        //               MOVE WS-DATE-X      TO P67-DT-SCAD-COP-DB
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtScadCop() == 0) {
            // COB_CODE: MOVE P67-DT-SCAD-COP TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67DtScadCop().getP67DtScadCop(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P67-DT-SCAD-COP-DB
            ws.getEstPoliCpiPrDb().setVarzTpIasDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P67-DT-SCAD-1A-RAT = 0
        //               MOVE WS-DATE-X      TO P67-DT-SCAD-1A-RAT-DB
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtScad1aRat() == 0) {
            // COB_CODE: MOVE P67-DT-SCAD-1A-RAT TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67DtScad1aRat().getP67DtScad1aRat(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P67-DT-SCAD-1A-RAT-DB
            ws.getEstPoliCpiPrDb().setNovaRgmFiscDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P67-DT-EROG-FINANZ = 0
        //               MOVE WS-DATE-X      TO P67-DT-EROG-FINANZ-DB
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtErogFinanz() == 0) {
            // COB_CODE: MOVE P67-DT-EROG-FINANZ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67DtErogFinanz().getP67DtErogFinanz(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P67-DT-EROG-FINANZ-DB
            ws.getEstPoliCpiPrDb().setDecorPrestBanDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P67-DT-STIPULA-FINANZ = 0
        //               MOVE WS-DATE-X      TO P67-DT-STIPULA-FINANZ-DB
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtStipulaFinanz() == 0) {
            // COB_CODE: MOVE P67-DT-STIPULA-FINANZ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67DtStipulaFinanz().getP67DtStipulaFinanz(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P67-DT-STIPULA-FINANZ-DB
            ws.getEstPoliCpiPrDb().setEffVarzStatTDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P67-DT-EST-FINANZ = 0
        //               MOVE WS-DATE-X      TO P67-DT-EST-FINANZ-DB
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtEstFinanz() == 0) {
            // COB_CODE: MOVE P67-DT-EST-FINANZ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67DtEstFinanz().getP67DtEstFinanz(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P67-DT-EST-FINANZ-DB
            ws.getEstPoliCpiPrDb().setUltConsCnbtDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-P67-DT-MAN-COP = 0
        //               MOVE WS-DATE-X      TO P67-DT-MAN-COP-DB
        //           END-IF.
        if (ws.getIndEstPoliCpiPr().getDtManCop() == 0) {
            // COB_CODE: MOVE P67-DT-MAN-COP TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(estPoliCpiPr.getP67DtManCop().getP67DtManCop(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO P67-DT-MAN-COP-DB
            ws.getEstPoliCpiPrDb().setPrescDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE P67-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P67-DT-INI-EFF
        estPoliCpiPr.setP67DtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE P67-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P67-DT-END-EFF
        estPoliCpiPr.setP67DtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-P67-DT-END-FINANZ = 0
        //               MOVE WS-DATE-N      TO P67-DT-END-FINANZ
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtEndFinanz() == 0) {
            // COB_CODE: MOVE P67-DT-END-FINANZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getDecorDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P67-DT-END-FINANZ
            estPoliCpiPr.getP67DtEndFinanz().setP67DtEndFinanz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P67-DT-1O-UTLZ-C-REV = 0
        //               MOVE WS-DATE-N      TO P67-DT-1O-UTLZ-C-REV
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDt1oUtlzCRev() == 0) {
            // COB_CODE: MOVE P67-DT-1O-UTLZ-C-REV-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P67-DT-1O-UTLZ-C-REV
            estPoliCpiPr.getP67Dt1oUtlzCRev().setP67Dt1oUtlzCRev(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P67-DT-SCAD-COP = 0
        //               MOVE WS-DATE-N      TO P67-DT-SCAD-COP
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtScadCop() == 0) {
            // COB_CODE: MOVE P67-DT-SCAD-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getVarzTpIasDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P67-DT-SCAD-COP
            estPoliCpiPr.getP67DtScadCop().setP67DtScadCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P67-DT-SCAD-1A-RAT = 0
        //               MOVE WS-DATE-N      TO P67-DT-SCAD-1A-RAT
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtScad1aRat() == 0) {
            // COB_CODE: MOVE P67-DT-SCAD-1A-RAT-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getNovaRgmFiscDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P67-DT-SCAD-1A-RAT
            estPoliCpiPr.getP67DtScad1aRat().setP67DtScad1aRat(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P67-DT-EROG-FINANZ = 0
        //               MOVE WS-DATE-N      TO P67-DT-EROG-FINANZ
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtErogFinanz() == 0) {
            // COB_CODE: MOVE P67-DT-EROG-FINANZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getDecorPrestBanDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P67-DT-EROG-FINANZ
            estPoliCpiPr.getP67DtErogFinanz().setP67DtErogFinanz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P67-DT-STIPULA-FINANZ = 0
        //               MOVE WS-DATE-N      TO P67-DT-STIPULA-FINANZ
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtStipulaFinanz() == 0) {
            // COB_CODE: MOVE P67-DT-STIPULA-FINANZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getEffVarzStatTDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P67-DT-STIPULA-FINANZ
            estPoliCpiPr.getP67DtStipulaFinanz().setP67DtStipulaFinanz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P67-DT-EST-FINANZ = 0
        //               MOVE WS-DATE-N      TO P67-DT-EST-FINANZ
        //           END-IF
        if (ws.getIndEstPoliCpiPr().getDtEstFinanz() == 0) {
            // COB_CODE: MOVE P67-DT-EST-FINANZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getUltConsCnbtDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P67-DT-EST-FINANZ
            estPoliCpiPr.getP67DtEstFinanz().setP67DtEstFinanz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-P67-DT-MAN-COP = 0
        //               MOVE WS-DATE-N      TO P67-DT-MAN-COP
        //           END-IF.
        if (ws.getIndEstPoliCpiPr().getDtManCop() == 0) {
            // COB_CODE: MOVE P67-DT-MAN-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getEstPoliCpiPrDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO P67-DT-MAN-COP
            estPoliCpiPr.getP67DtManCop().setP67DtManCop(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getAmm1aRat() {
        return estPoliCpiPr.getP67Amm1aRat().getP67Amm1aRat();
    }

    @Override
    public void setAmm1aRat(AfDecimal amm1aRat) {
        this.estPoliCpiPr.getP67Amm1aRat().setP67Amm1aRat(amm1aRat.copy());
    }

    @Override
    public AfDecimal getAmm1aRatObj() {
        if (ws.getIndEstPoliCpiPr().getAmm1aRat() >= 0) {
            return getAmm1aRat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAmm1aRatObj(AfDecimal amm1aRatObj) {
        if (amm1aRatObj != null) {
            setAmm1aRat(new AfDecimal(amm1aRatObj, 15, 3));
            ws.getIndEstPoliCpiPr().setAmm1aRat(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setAmm1aRat(((short)-1));
        }
    }

    @Override
    public AfDecimal getAmmRatEnd() {
        return estPoliCpiPr.getP67AmmRatEnd().getP67AmmRatEnd();
    }

    @Override
    public void setAmmRatEnd(AfDecimal ammRatEnd) {
        this.estPoliCpiPr.getP67AmmRatEnd().setP67AmmRatEnd(ammRatEnd.copy());
    }

    @Override
    public AfDecimal getAmmRatEndObj() {
        if (ws.getIndEstPoliCpiPr().getAmmRatEnd() >= 0) {
            return getAmmRatEnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAmmRatEndObj(AfDecimal ammRatEndObj) {
        if (ammRatEndObj != null) {
            setAmmRatEnd(new AfDecimal(ammRatEndObj, 15, 3));
            ws.getIndEstPoliCpiPr().setAmmRatEnd(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setAmmRatEnd(((short)-1));
        }
    }

    @Override
    public String getCatFinanzEr() {
        return estPoliCpiPr.getP67CatFinanzEr();
    }

    @Override
    public void setCatFinanzEr(String catFinanzEr) {
        this.estPoliCpiPr.setP67CatFinanzEr(catFinanzEr);
    }

    @Override
    public String getCatFinanzErObj() {
        if (ws.getIndEstPoliCpiPr().getCatFinanzEr() >= 0) {
            return getCatFinanzEr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCatFinanzErObj(String catFinanzErObj) {
        if (catFinanzErObj != null) {
            setCatFinanzEr(catFinanzErObj);
            ws.getIndEstPoliCpiPr().setCatFinanzEr(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setCatFinanzEr(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return estPoliCpiPr.getP67CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.estPoliCpiPr.setP67CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodProdEstno() {
        return estPoliCpiPr.getP67CodProdEstno();
    }

    @Override
    public void setCodProdEstno(String codProdEstno) {
        this.estPoliCpiPr.setP67CodProdEstno(codProdEstno);
    }

    @Override
    public AfDecimal getCptFin() {
        return estPoliCpiPr.getP67CptFin().getP67CptFin();
    }

    @Override
    public void setCptFin(AfDecimal cptFin) {
        this.estPoliCpiPr.getP67CptFin().setP67CptFin(cptFin.copy());
    }

    @Override
    public AfDecimal getCptFinObj() {
        if (ws.getIndEstPoliCpiPr().getCptFin() >= 0) {
            return getCptFin();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCptFinObj(AfDecimal cptFinObj) {
        if (cptFinObj != null) {
            setCptFin(new AfDecimal(cptFinObj, 15, 3));
            ws.getIndEstPoliCpiPr().setCptFin(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setCptFin(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return estPoliCpiPr.getP67DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.estPoliCpiPr.setP67DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return estPoliCpiPr.getP67DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.estPoliCpiPr.setP67DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return estPoliCpiPr.getP67DsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.estPoliCpiPr.setP67DsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return estPoliCpiPr.getP67DsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.estPoliCpiPr.setP67DsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return estPoliCpiPr.getP67DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.estPoliCpiPr.setP67DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return estPoliCpiPr.getP67DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.estPoliCpiPr.setP67DsVer(dsVer);
    }

    @Override
    public String getDt1oUtlzCRevDb() {
        return ws.getEstPoliCpiPrDb().getScadDb();
    }

    @Override
    public void setDt1oUtlzCRevDb(String dt1oUtlzCRevDb) {
        this.ws.getEstPoliCpiPrDb().setScadDb(dt1oUtlzCRevDb);
    }

    @Override
    public String getDt1oUtlzCRevDbObj() {
        if (ws.getIndEstPoliCpiPr().getDt1oUtlzCRev() >= 0) {
            return getDt1oUtlzCRevDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDt1oUtlzCRevDbObj(String dt1oUtlzCRevDbObj) {
        if (dt1oUtlzCRevDbObj != null) {
            setDt1oUtlzCRevDb(dt1oUtlzCRevDbObj);
            ws.getIndEstPoliCpiPr().setDt1oUtlzCRev(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setDt1oUtlzCRev(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getEstPoliCpiPrDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getEstPoliCpiPrDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtEndFinanzDb() {
        return ws.getEstPoliCpiPrDb().getDecorDb();
    }

    @Override
    public void setDtEndFinanzDb(String dtEndFinanzDb) {
        this.ws.getEstPoliCpiPrDb().setDecorDb(dtEndFinanzDb);
    }

    @Override
    public String getDtEndFinanzDbObj() {
        if (ws.getIndEstPoliCpiPr().getDtEndFinanz() >= 0) {
            return getDtEndFinanzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndFinanzDbObj(String dtEndFinanzDbObj) {
        if (dtEndFinanzDbObj != null) {
            setDtEndFinanzDb(dtEndFinanzDbObj);
            ws.getIndEstPoliCpiPr().setDtEndFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setDtEndFinanz(((short)-1));
        }
    }

    @Override
    public String getDtErogFinanzDb() {
        return ws.getEstPoliCpiPrDb().getDecorPrestBanDb();
    }

    @Override
    public void setDtErogFinanzDb(String dtErogFinanzDb) {
        this.ws.getEstPoliCpiPrDb().setDecorPrestBanDb(dtErogFinanzDb);
    }

    @Override
    public String getDtErogFinanzDbObj() {
        if (ws.getIndEstPoliCpiPr().getDtErogFinanz() >= 0) {
            return getDtErogFinanzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtErogFinanzDbObj(String dtErogFinanzDbObj) {
        if (dtErogFinanzDbObj != null) {
            setDtErogFinanzDb(dtErogFinanzDbObj);
            ws.getIndEstPoliCpiPr().setDtErogFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setDtErogFinanz(((short)-1));
        }
    }

    @Override
    public String getDtEstFinanzDb() {
        return ws.getEstPoliCpiPrDb().getUltConsCnbtDb();
    }

    @Override
    public void setDtEstFinanzDb(String dtEstFinanzDb) {
        this.ws.getEstPoliCpiPrDb().setUltConsCnbtDb(dtEstFinanzDb);
    }

    @Override
    public String getDtEstFinanzDbObj() {
        if (ws.getIndEstPoliCpiPr().getDtEstFinanz() >= 0) {
            return getDtEstFinanzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEstFinanzDbObj(String dtEstFinanzDbObj) {
        if (dtEstFinanzDbObj != null) {
            setDtEstFinanzDb(dtEstFinanzDbObj);
            ws.getIndEstPoliCpiPr().setDtEstFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setDtEstFinanz(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getEstPoliCpiPrDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getEstPoliCpiPrDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtManCopDb() {
        return ws.getEstPoliCpiPrDb().getPrescDb();
    }

    @Override
    public void setDtManCopDb(String dtManCopDb) {
        this.ws.getEstPoliCpiPrDb().setPrescDb(dtManCopDb);
    }

    @Override
    public String getDtManCopDbObj() {
        if (ws.getIndEstPoliCpiPr().getDtManCop() >= 0) {
            return getDtManCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtManCopDbObj(String dtManCopDbObj) {
        if (dtManCopDbObj != null) {
            setDtManCopDb(dtManCopDbObj);
            ws.getIndEstPoliCpiPr().setDtManCop(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setDtManCop(((short)-1));
        }
    }

    @Override
    public String getDtScad1aRatDb() {
        return ws.getEstPoliCpiPrDb().getNovaRgmFiscDb();
    }

    @Override
    public void setDtScad1aRatDb(String dtScad1aRatDb) {
        this.ws.getEstPoliCpiPrDb().setNovaRgmFiscDb(dtScad1aRatDb);
    }

    @Override
    public String getDtScad1aRatDbObj() {
        if (ws.getIndEstPoliCpiPr().getDtScad1aRat() >= 0) {
            return getDtScad1aRatDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScad1aRatDbObj(String dtScad1aRatDbObj) {
        if (dtScad1aRatDbObj != null) {
            setDtScad1aRatDb(dtScad1aRatDbObj);
            ws.getIndEstPoliCpiPr().setDtScad1aRat(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setDtScad1aRat(((short)-1));
        }
    }

    @Override
    public String getDtScadCopDb() {
        return ws.getEstPoliCpiPrDb().getVarzTpIasDb();
    }

    @Override
    public void setDtScadCopDb(String dtScadCopDb) {
        this.ws.getEstPoliCpiPrDb().setVarzTpIasDb(dtScadCopDb);
    }

    @Override
    public String getDtScadCopDbObj() {
        if (ws.getIndEstPoliCpiPr().getDtScadCop() >= 0) {
            return getDtScadCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadCopDbObj(String dtScadCopDbObj) {
        if (dtScadCopDbObj != null) {
            setDtScadCopDb(dtScadCopDbObj);
            ws.getIndEstPoliCpiPr().setDtScadCop(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setDtScadCop(((short)-1));
        }
    }

    @Override
    public String getDtStipulaFinanzDb() {
        return ws.getEstPoliCpiPrDb().getEffVarzStatTDb();
    }

    @Override
    public void setDtStipulaFinanzDb(String dtStipulaFinanzDb) {
        this.ws.getEstPoliCpiPrDb().setEffVarzStatTDb(dtStipulaFinanzDb);
    }

    @Override
    public String getDtStipulaFinanzDbObj() {
        if (ws.getIndEstPoliCpiPr().getDtStipulaFinanz() >= 0) {
            return getDtStipulaFinanzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtStipulaFinanzDbObj(String dtStipulaFinanzDbObj) {
        if (dtStipulaFinanzDbObj != null) {
            setDtStipulaFinanzDb(dtStipulaFinanzDbObj);
            ws.getIndEstPoliCpiPr().setDtStipulaFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setDtStipulaFinanz(((short)-1));
        }
    }

    @Override
    public int getDurMmFinanz() {
        return estPoliCpiPr.getP67DurMmFinanz().getP67DurMmFinanz();
    }

    @Override
    public void setDurMmFinanz(int durMmFinanz) {
        this.estPoliCpiPr.getP67DurMmFinanz().setP67DurMmFinanz(durMmFinanz);
    }

    @Override
    public Integer getDurMmFinanzObj() {
        if (ws.getIndEstPoliCpiPr().getDurMmFinanz() >= 0) {
            return ((Integer)getDurMmFinanz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmFinanzObj(Integer durMmFinanzObj) {
        if (durMmFinanzObj != null) {
            setDurMmFinanz(((int)durMmFinanzObj));
            ws.getIndEstPoliCpiPr().setDurMmFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setDurMmFinanz(((short)-1));
        }
    }

    @Override
    public char getFlPreFin() {
        return estPoliCpiPr.getP67FlPreFin();
    }

    @Override
    public void setFlPreFin(char flPreFin) {
        this.estPoliCpiPr.setP67FlPreFin(flPreFin);
    }

    @Override
    public short getGgDelMmScadRat() {
        return estPoliCpiPr.getP67GgDelMmScadRat().getP67GgDelMmScadRat();
    }

    @Override
    public void setGgDelMmScadRat(short ggDelMmScadRat) {
        this.estPoliCpiPr.getP67GgDelMmScadRat().setP67GgDelMmScadRat(ggDelMmScadRat);
    }

    @Override
    public Short getGgDelMmScadRatObj() {
        if (ws.getIndEstPoliCpiPr().getGgDelMmScadRat() >= 0) {
            return ((Short)getGgDelMmScadRat());
        }
        else {
            return null;
        }
    }

    @Override
    public void setGgDelMmScadRatObj(Short ggDelMmScadRatObj) {
        if (ggDelMmScadRatObj != null) {
            setGgDelMmScadRat(((short)ggDelMmScadRatObj));
            ws.getIndEstPoliCpiPr().setGgDelMmScadRat(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setGgDelMmScadRat(((short)-1));
        }
    }

    @Override
    public String getIbOgg() {
        return estPoliCpiPr.getP67IbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.estPoliCpiPr.setP67IbOgg(ibOgg);
    }

    @Override
    public int getIdEstPoliCpiPr() {
        return estPoliCpiPr.getP67IdEstPoliCpiPr();
    }

    @Override
    public void setIdEstPoliCpiPr(int idEstPoliCpiPr) {
        this.estPoliCpiPr.setP67IdEstPoliCpiPr(idEstPoliCpiPr);
    }

    @Override
    public int getIdMoviChiu() {
        return estPoliCpiPr.getP67IdMoviChiu().getP67IdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.estPoliCpiPr.getP67IdMoviChiu().setP67IdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndEstPoliCpiPr().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndEstPoliCpiPr().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return estPoliCpiPr.getP67IdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.estPoliCpiPr.setP67IdMoviCrz(idMoviCrz);
    }

    @Override
    public AfDecimal getImpAssto() {
        return estPoliCpiPr.getP67ImpAssto().getP67ImpAssto();
    }

    @Override
    public void setImpAssto(AfDecimal impAssto) {
        this.estPoliCpiPr.getP67ImpAssto().setP67ImpAssto(impAssto.copy());
    }

    @Override
    public AfDecimal getImpAsstoObj() {
        if (ws.getIndEstPoliCpiPr().getImpAssto() >= 0) {
            return getImpAssto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAsstoObj(AfDecimal impAsstoObj) {
        if (impAsstoObj != null) {
            setImpAssto(new AfDecimal(impAsstoObj, 15, 3));
            ws.getIndEstPoliCpiPr().setImpAssto(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setImpAssto(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpCanoneAntic() {
        return estPoliCpiPr.getP67ImpCanoneAntic().getP67ImpCanoneAntic();
    }

    @Override
    public void setImpCanoneAntic(AfDecimal impCanoneAntic) {
        this.estPoliCpiPr.getP67ImpCanoneAntic().setP67ImpCanoneAntic(impCanoneAntic.copy());
    }

    @Override
    public AfDecimal getImpCanoneAnticObj() {
        if (ws.getIndEstPoliCpiPr().getImpCanoneAntic() >= 0) {
            return getImpCanoneAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpCanoneAnticObj(AfDecimal impCanoneAnticObj) {
        if (impCanoneAnticObj != null) {
            setImpCanoneAntic(new AfDecimal(impCanoneAnticObj, 15, 3));
            ws.getIndEstPoliCpiPr().setImpCanoneAntic(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setImpCanoneAntic(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpDebRes() {
        return estPoliCpiPr.getP67ImpDebRes().getP67ImpDebRes();
    }

    @Override
    public void setImpDebRes(AfDecimal impDebRes) {
        this.estPoliCpiPr.getP67ImpDebRes().setP67ImpDebRes(impDebRes.copy());
    }

    @Override
    public AfDecimal getImpDebResObj() {
        if (ws.getIndEstPoliCpiPr().getImpDebRes() >= 0) {
            return getImpDebRes();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpDebResObj(AfDecimal impDebResObj) {
        if (impDebResObj != null) {
            setImpDebRes(new AfDecimal(impDebResObj, 15, 3));
            ws.getIndEstPoliCpiPr().setImpDebRes(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setImpDebRes(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpFinRevolving() {
        return estPoliCpiPr.getP67ImpFinRevolving().getP67ImpFinRevolving();
    }

    @Override
    public void setImpFinRevolving(AfDecimal impFinRevolving) {
        this.estPoliCpiPr.getP67ImpFinRevolving().setP67ImpFinRevolving(impFinRevolving.copy());
    }

    @Override
    public AfDecimal getImpFinRevolvingObj() {
        if (ws.getIndEstPoliCpiPr().getImpFinRevolving() >= 0) {
            return getImpFinRevolving();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpFinRevolvingObj(AfDecimal impFinRevolvingObj) {
        if (impFinRevolvingObj != null) {
            setImpFinRevolving(new AfDecimal(impFinRevolvingObj, 15, 3));
            ws.getIndEstPoliCpiPr().setImpFinRevolving(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setImpFinRevolving(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRatFinanz() {
        return estPoliCpiPr.getP67ImpRatFinanz().getP67ImpRatFinanz();
    }

    @Override
    public void setImpRatFinanz(AfDecimal impRatFinanz) {
        this.estPoliCpiPr.getP67ImpRatFinanz().setP67ImpRatFinanz(impRatFinanz.copy());
    }

    @Override
    public AfDecimal getImpRatFinanzObj() {
        if (ws.getIndEstPoliCpiPr().getImpRatFinanz() >= 0) {
            return getImpRatFinanz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRatFinanzObj(AfDecimal impRatFinanzObj) {
        if (impRatFinanzObj != null) {
            setImpRatFinanz(new AfDecimal(impRatFinanzObj, 15, 3));
            ws.getIndEstPoliCpiPr().setImpRatFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setImpRatFinanz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpRatRevolving() {
        return estPoliCpiPr.getP67ImpRatRevolving().getP67ImpRatRevolving();
    }

    @Override
    public void setImpRatRevolving(AfDecimal impRatRevolving) {
        this.estPoliCpiPr.getP67ImpRatRevolving().setP67ImpRatRevolving(impRatRevolving.copy());
    }

    @Override
    public AfDecimal getImpRatRevolvingObj() {
        if (ws.getIndEstPoliCpiPr().getImpRatRevolving() >= 0) {
            return getImpRatRevolving();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpRatRevolvingObj(AfDecimal impRatRevolvingObj) {
        if (impRatRevolvingObj != null) {
            setImpRatRevolving(new AfDecimal(impRatRevolvingObj, 15, 3));
            ws.getIndEstPoliCpiPr().setImpRatRevolving(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setImpRatRevolving(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpUtilCRev() {
        return estPoliCpiPr.getP67ImpUtilCRev().getP67ImpUtilCRev();
    }

    @Override
    public void setImpUtilCRev(AfDecimal impUtilCRev) {
        this.estPoliCpiPr.getP67ImpUtilCRev().setP67ImpUtilCRev(impUtilCRev.copy());
    }

    @Override
    public AfDecimal getImpUtilCRevObj() {
        if (ws.getIndEstPoliCpiPr().getImpUtilCRev() >= 0) {
            return getImpUtilCRev();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpUtilCRevObj(AfDecimal impUtilCRevObj) {
        if (impUtilCRevObj != null) {
            setImpUtilCRev(new AfDecimal(impUtilCRevObj, 15, 3));
            ws.getIndEstPoliCpiPr().setImpUtilCRev(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setImpUtilCRev(((short)-1));
        }
    }

    @Override
    public int getMmPreamm() {
        return estPoliCpiPr.getP67MmPreamm().getP67MmPreamm();
    }

    @Override
    public void setMmPreamm(int mmPreamm) {
        this.estPoliCpiPr.getP67MmPreamm().setP67MmPreamm(mmPreamm);
    }

    @Override
    public Integer getMmPreammObj() {
        if (ws.getIndEstPoliCpiPr().getMmPreamm() >= 0) {
            return ((Integer)getMmPreamm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmPreammObj(Integer mmPreammObj) {
        if (mmPreammObj != null) {
            setMmPreamm(((int)mmPreammObj));
            ws.getIndEstPoliCpiPr().setMmPreamm(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setMmPreamm(((short)-1));
        }
    }

    @Override
    public String getNumFinanz() {
        return estPoliCpiPr.getP67NumFinanz();
    }

    @Override
    public void setNumFinanz(String numFinanz) {
        this.estPoliCpiPr.setP67NumFinanz(numFinanz);
    }

    @Override
    public String getNumFinanzObj() {
        if (ws.getIndEstPoliCpiPr().getNumFinanz() >= 0) {
            return getNumFinanz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumFinanzObj(String numFinanzObj) {
        if (numFinanzObj != null) {
            setNumFinanz(numFinanzObj);
            ws.getIndEstPoliCpiPr().setNumFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setNumFinanz(((short)-1));
        }
    }

    @Override
    public int getNumTstFin() {
        return estPoliCpiPr.getP67NumTstFin().getP67NumTstFin();
    }

    @Override
    public void setNumTstFin(int numTstFin) {
        this.estPoliCpiPr.getP67NumTstFin().setP67NumTstFin(numTstFin);
    }

    @Override
    public Integer getNumTstFinObj() {
        if (ws.getIndEstPoliCpiPr().getNumTstFin() >= 0) {
            return ((Integer)getNumTstFin());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumTstFinObj(Integer numTstFinObj) {
        if (numTstFinObj != null) {
            setNumTstFin(((int)numTstFinObj));
            ws.getIndEstPoliCpiPr().setNumTstFin(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setNumTstFin(((short)-1));
        }
    }

    @Override
    public long getP67DsRiga() {
        return estPoliCpiPr.getP67DsRiga();
    }

    @Override
    public void setP67DsRiga(long p67DsRiga) {
        this.estPoliCpiPr.setP67DsRiga(p67DsRiga);
    }

    @Override
    public int getPerRatFinanz() {
        return estPoliCpiPr.getP67PerRatFinanz().getP67PerRatFinanz();
    }

    @Override
    public void setPerRatFinanz(int perRatFinanz) {
        this.estPoliCpiPr.getP67PerRatFinanz().setP67PerRatFinanz(perRatFinanz);
    }

    @Override
    public Integer getPerRatFinanzObj() {
        if (ws.getIndEstPoliCpiPr().getPerRatFinanz() >= 0) {
            return ((Integer)getPerRatFinanz());
        }
        else {
            return null;
        }
    }

    @Override
    public void setPerRatFinanzObj(Integer perRatFinanzObj) {
        if (perRatFinanzObj != null) {
            setPerRatFinanz(((int)perRatFinanzObj));
            ws.getIndEstPoliCpiPr().setPerRatFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setPerRatFinanz(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreVers() {
        return estPoliCpiPr.getP67PreVers().getP67PreVers();
    }

    @Override
    public void setPreVers(AfDecimal preVers) {
        this.estPoliCpiPr.getP67PreVers().setP67PreVers(preVers.copy());
    }

    @Override
    public AfDecimal getPreVersObj() {
        if (ws.getIndEstPoliCpiPr().getPreVers() >= 0) {
            return getPreVers();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreVersObj(AfDecimal preVersObj) {
        if (preVersObj != null) {
            setPreVers(new AfDecimal(preVersObj, 15, 3));
            ws.getIndEstPoliCpiPr().setPreVers(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setPreVers(((short)-1));
        }
    }

    @Override
    public String getTpFinanzEr() {
        return estPoliCpiPr.getP67TpFinanzEr();
    }

    @Override
    public void setTpFinanzEr(String tpFinanzEr) {
        this.estPoliCpiPr.setP67TpFinanzEr(tpFinanzEr);
    }

    @Override
    public String getTpFinanzErObj() {
        if (ws.getIndEstPoliCpiPr().getTpFinanzEr() >= 0) {
            return getTpFinanzEr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpFinanzErObj(String tpFinanzErObj) {
        if (tpFinanzErObj != null) {
            setTpFinanzEr(tpFinanzErObj);
            ws.getIndEstPoliCpiPr().setTpFinanzEr(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setTpFinanzEr(((short)-1));
        }
    }

    @Override
    public String getTpModAcqs() {
        return estPoliCpiPr.getP67TpModAcqs();
    }

    @Override
    public void setTpModAcqs(String tpModAcqs) {
        this.estPoliCpiPr.setP67TpModAcqs(tpModAcqs);
    }

    @Override
    public String getTpModAcqsObj() {
        if (ws.getIndEstPoliCpiPr().getTpModAcqs() >= 0) {
            return getTpModAcqs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpModAcqsObj(String tpModAcqsObj) {
        if (tpModAcqsObj != null) {
            setTpModAcqs(tpModAcqsObj);
            ws.getIndEstPoliCpiPr().setTpModAcqs(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setTpModAcqs(((short)-1));
        }
    }

    @Override
    public String getTpModPagRat() {
        return estPoliCpiPr.getP67TpModPagRat();
    }

    @Override
    public void setTpModPagRat(String tpModPagRat) {
        this.estPoliCpiPr.setP67TpModPagRat(tpModPagRat);
    }

    @Override
    public String getTpModPagRatObj() {
        if (ws.getIndEstPoliCpiPr().getTpModPagRat() >= 0) {
            return getTpModPagRat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpModPagRatObj(String tpModPagRatObj) {
        if (tpModPagRatObj != null) {
            setTpModPagRat(tpModPagRatObj);
            ws.getIndEstPoliCpiPr().setTpModPagRat(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setTpModPagRat(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsCreRatFinanz() {
        return estPoliCpiPr.getP67TsCreRatFinanz().getP67TsCreRatFinanz();
    }

    @Override
    public void setTsCreRatFinanz(AfDecimal tsCreRatFinanz) {
        this.estPoliCpiPr.getP67TsCreRatFinanz().setP67TsCreRatFinanz(tsCreRatFinanz.copy());
    }

    @Override
    public AfDecimal getTsCreRatFinanzObj() {
        if (ws.getIndEstPoliCpiPr().getTsCreRatFinanz() >= 0) {
            return getTsCreRatFinanz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsCreRatFinanzObj(AfDecimal tsCreRatFinanzObj) {
        if (tsCreRatFinanzObj != null) {
            setTsCreRatFinanz(new AfDecimal(tsCreRatFinanzObj, 14, 9));
            ws.getIndEstPoliCpiPr().setTsCreRatFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setTsCreRatFinanz(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsFinanz() {
        return estPoliCpiPr.getP67TsFinanz().getP67TsFinanz();
    }

    @Override
    public void setTsFinanz(AfDecimal tsFinanz) {
        this.estPoliCpiPr.getP67TsFinanz().setP67TsFinanz(tsFinanz.copy());
    }

    @Override
    public AfDecimal getTsFinanzObj() {
        if (ws.getIndEstPoliCpiPr().getTsFinanz() >= 0) {
            return getTsFinanz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsFinanzObj(AfDecimal tsFinanzObj) {
        if (tsFinanzObj != null) {
            setTsFinanz(new AfDecimal(tsFinanzObj, 14, 9));
            ws.getIndEstPoliCpiPr().setTsFinanz(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setTsFinanz(((short)-1));
        }
    }

    @Override
    public AfDecimal getValRiscBene() {
        return estPoliCpiPr.getP67ValRiscBene().getP67ValRiscBene();
    }

    @Override
    public void setValRiscBene(AfDecimal valRiscBene) {
        this.estPoliCpiPr.getP67ValRiscBene().setP67ValRiscBene(valRiscBene.copy());
    }

    @Override
    public AfDecimal getValRiscBeneObj() {
        if (ws.getIndEstPoliCpiPr().getValRiscBene() >= 0) {
            return getValRiscBene();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValRiscBeneObj(AfDecimal valRiscBeneObj) {
        if (valRiscBeneObj != null) {
            setValRiscBene(new AfDecimal(valRiscBeneObj, 15, 3));
            ws.getIndEstPoliCpiPr().setValRiscBene(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setValRiscBene(((short)-1));
        }
    }

    @Override
    public AfDecimal getValRiscEndLeas() {
        return estPoliCpiPr.getP67ValRiscEndLeas().getP67ValRiscEndLeas();
    }

    @Override
    public void setValRiscEndLeas(AfDecimal valRiscEndLeas) {
        this.estPoliCpiPr.getP67ValRiscEndLeas().setP67ValRiscEndLeas(valRiscEndLeas.copy());
    }

    @Override
    public AfDecimal getValRiscEndLeasObj() {
        if (ws.getIndEstPoliCpiPr().getValRiscEndLeas() >= 0) {
            return getValRiscEndLeas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setValRiscEndLeasObj(AfDecimal valRiscEndLeasObj) {
        if (valRiscEndLeasObj != null) {
            setValRiscEndLeas(new AfDecimal(valRiscEndLeasObj, 15, 3));
            ws.getIndEstPoliCpiPr().setValRiscEndLeas(((short)0));
        }
        else {
            ws.getIndEstPoliCpiPr().setValRiscEndLeas(((short)-1));
        }
    }
}
