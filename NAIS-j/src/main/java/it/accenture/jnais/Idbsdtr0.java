package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.DettTitDiRatDao;
import it.accenture.jnais.commons.data.to.IDettTitDiRat;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.DettTitDiRatIdbsdtr0;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsdtr0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.DtrAcqExp;
import it.accenture.jnais.ws.redefines.DtrCarAcq;
import it.accenture.jnais.ws.redefines.DtrCarGest;
import it.accenture.jnais.ws.redefines.DtrCarIas;
import it.accenture.jnais.ws.redefines.DtrCarInc;
import it.accenture.jnais.ws.redefines.DtrCnbtAntirac;
import it.accenture.jnais.ws.redefines.DtrCommisInter;
import it.accenture.jnais.ws.redefines.DtrDir;
import it.accenture.jnais.ws.redefines.DtrDtEndCop;
import it.accenture.jnais.ws.redefines.DtrDtIniCop;
import it.accenture.jnais.ws.redefines.DtrFrqMovi;
import it.accenture.jnais.ws.redefines.DtrIdMoviChiu;
import it.accenture.jnais.ws.redefines.DtrImpAder;
import it.accenture.jnais.ws.redefines.DtrImpAz;
import it.accenture.jnais.ws.redefines.DtrImpTfr;
import it.accenture.jnais.ws.redefines.DtrImpTfrStrc;
import it.accenture.jnais.ws.redefines.DtrImpTrasfe;
import it.accenture.jnais.ws.redefines.DtrImpVolo;
import it.accenture.jnais.ws.redefines.DtrIntrFraz;
import it.accenture.jnais.ws.redefines.DtrIntrMora;
import it.accenture.jnais.ws.redefines.DtrIntrRetdt;
import it.accenture.jnais.ws.redefines.DtrIntrRiat;
import it.accenture.jnais.ws.redefines.DtrManfeeAntic;
import it.accenture.jnais.ws.redefines.DtrManfeeRicor;
import it.accenture.jnais.ws.redefines.DtrPreNet;
import it.accenture.jnais.ws.redefines.DtrPrePpIas;
import it.accenture.jnais.ws.redefines.DtrPreSoloRsh;
import it.accenture.jnais.ws.redefines.DtrPreTot;
import it.accenture.jnais.ws.redefines.DtrProvAcq1aa;
import it.accenture.jnais.ws.redefines.DtrProvAcq2aa;
import it.accenture.jnais.ws.redefines.DtrProvDaRec;
import it.accenture.jnais.ws.redefines.DtrProvInc;
import it.accenture.jnais.ws.redefines.DtrProvRicor;
import it.accenture.jnais.ws.redefines.DtrRemunAss;
import it.accenture.jnais.ws.redefines.DtrSoprAlt;
import it.accenture.jnais.ws.redefines.DtrSoprProf;
import it.accenture.jnais.ws.redefines.DtrSoprSan;
import it.accenture.jnais.ws.redefines.DtrSoprSpo;
import it.accenture.jnais.ws.redefines.DtrSoprTec;
import it.accenture.jnais.ws.redefines.DtrSpeAge;
import it.accenture.jnais.ws.redefines.DtrSpeMed;
import it.accenture.jnais.ws.redefines.DtrTax;
import it.accenture.jnais.ws.redefines.DtrTotIntrPrest;

/**Original name: IDBSDTR0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  28 NOV 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsdtr0 extends Program implements IDettTitDiRat {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private DettTitDiRatDao dettTitDiRatDao = new DettTitDiRatDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsdtr0Data ws = new Idbsdtr0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: DETT-TIT-DI-RAT
    private DettTitDiRatIdbsdtr0 dettTitDiRat;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSDTR0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, DettTitDiRatIdbsdtr0 dettTitDiRat) {
        this.idsv0003 = idsv0003;
        this.dettTitDiRat = dettTitDiRat;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsdtr0 getInstance() {
        return ((Idbsdtr0)Programs.getInstance(Idbsdtr0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSDTR0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSDTR0");
        // COB_CODE: MOVE 'DETT_TIT_DI_RAT' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("DETT_TIT_DI_RAT");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_TIT_DI_RAT
        //                ,ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,PRE_NET
        //                ,INTR_FRAZ
        //                ,INTR_MORA
        //                ,INTR_RETDT
        //                ,INTR_RIAT
        //                ,DIR
        //                ,SPE_MED
        //                ,SPE_AGE
        //                ,TAX
        //                ,SOPR_SAN
        //                ,SOPR_SPO
        //                ,SOPR_TEC
        //                ,SOPR_PROF
        //                ,SOPR_ALT
        //                ,PRE_TOT
        //                ,PRE_PP_IAS
        //                ,PRE_SOLO_RSH
        //                ,CAR_IAS
        //                ,PROV_ACQ_1AA
        //                ,PROV_ACQ_2AA
        //                ,PROV_RICOR
        //                ,PROV_INC
        //                ,PROV_DA_REC
        //                ,COD_DVS
        //                ,FRQ_MOVI
        //                ,TP_RGM_FISC
        //                ,COD_TARI
        //                ,TP_STAT_TIT
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,CAR_ACQ
        //                ,CAR_GEST
        //                ,CAR_INC
        //                ,MANFEE_ANTIC
        //                ,MANFEE_RICOR
        //                ,MANFEE_REC
        //                ,TOT_INTR_PREST
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,ACQ_EXP
        //                ,REMUN_ASS
        //                ,COMMIS_INTER
        //                ,CNBT_ANTIRAC
        //             INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //             FROM DETT_TIT_DI_RAT
        //             WHERE     DS_RIGA = :DTR-DS-RIGA
        //           END-EXEC.
        dettTitDiRatDao.selectByDtrDsRiga(dettTitDiRat.getDtrDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO DETT_TIT_DI_RAT
            //                  (
            //                     ID_DETT_TIT_DI_RAT
            //                    ,ID_TIT_RAT
            //                    ,ID_OGG
            //                    ,TP_OGG
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,DT_INI_COP
            //                    ,DT_END_COP
            //                    ,PRE_NET
            //                    ,INTR_FRAZ
            //                    ,INTR_MORA
            //                    ,INTR_RETDT
            //                    ,INTR_RIAT
            //                    ,DIR
            //                    ,SPE_MED
            //                    ,SPE_AGE
            //                    ,TAX
            //                    ,SOPR_SAN
            //                    ,SOPR_SPO
            //                    ,SOPR_TEC
            //                    ,SOPR_PROF
            //                    ,SOPR_ALT
            //                    ,PRE_TOT
            //                    ,PRE_PP_IAS
            //                    ,PRE_SOLO_RSH
            //                    ,CAR_IAS
            //                    ,PROV_ACQ_1AA
            //                    ,PROV_ACQ_2AA
            //                    ,PROV_RICOR
            //                    ,PROV_INC
            //                    ,PROV_DA_REC
            //                    ,COD_DVS
            //                    ,FRQ_MOVI
            //                    ,TP_RGM_FISC
            //                    ,COD_TARI
            //                    ,TP_STAT_TIT
            //                    ,IMP_AZ
            //                    ,IMP_ADER
            //                    ,IMP_TFR
            //                    ,IMP_VOLO
            //                    ,FL_VLDT_TIT
            //                    ,CAR_ACQ
            //                    ,CAR_GEST
            //                    ,CAR_INC
            //                    ,MANFEE_ANTIC
            //                    ,MANFEE_RICOR
            //                    ,MANFEE_REC
            //                    ,TOT_INTR_PREST
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,IMP_TRASFE
            //                    ,IMP_TFR_STRC
            //                    ,ACQ_EXP
            //                    ,REMUN_ASS
            //                    ,COMMIS_INTER
            //                    ,CNBT_ANTIRAC
            //                  )
            //              VALUES
            //                  (
            //                    :DTR-ID-DETT-TIT-DI-RAT
            //                    ,:DTR-ID-TIT-RAT
            //                    ,:DTR-ID-OGG
            //                    ,:DTR-TP-OGG
            //                    ,:DTR-ID-MOVI-CRZ
            //                    ,:DTR-ID-MOVI-CHIU
            //                     :IND-DTR-ID-MOVI-CHIU
            //                    ,:DTR-DT-INI-EFF-DB
            //                    ,:DTR-DT-END-EFF-DB
            //                    ,:DTR-COD-COMP-ANIA
            //                    ,:DTR-DT-INI-COP-DB
            //                     :IND-DTR-DT-INI-COP
            //                    ,:DTR-DT-END-COP-DB
            //                     :IND-DTR-DT-END-COP
            //                    ,:DTR-PRE-NET
            //                     :IND-DTR-PRE-NET
            //                    ,:DTR-INTR-FRAZ
            //                     :IND-DTR-INTR-FRAZ
            //                    ,:DTR-INTR-MORA
            //                     :IND-DTR-INTR-MORA
            //                    ,:DTR-INTR-RETDT
            //                     :IND-DTR-INTR-RETDT
            //                    ,:DTR-INTR-RIAT
            //                     :IND-DTR-INTR-RIAT
            //                    ,:DTR-DIR
            //                     :IND-DTR-DIR
            //                    ,:DTR-SPE-MED
            //                     :IND-DTR-SPE-MED
            //                    ,:DTR-SPE-AGE
            //                     :IND-DTR-SPE-AGE
            //                    ,:DTR-TAX
            //                     :IND-DTR-TAX
            //                    ,:DTR-SOPR-SAN
            //                     :IND-DTR-SOPR-SAN
            //                    ,:DTR-SOPR-SPO
            //                     :IND-DTR-SOPR-SPO
            //                    ,:DTR-SOPR-TEC
            //                     :IND-DTR-SOPR-TEC
            //                    ,:DTR-SOPR-PROF
            //                     :IND-DTR-SOPR-PROF
            //                    ,:DTR-SOPR-ALT
            //                     :IND-DTR-SOPR-ALT
            //                    ,:DTR-PRE-TOT
            //                     :IND-DTR-PRE-TOT
            //                    ,:DTR-PRE-PP-IAS
            //                     :IND-DTR-PRE-PP-IAS
            //                    ,:DTR-PRE-SOLO-RSH
            //                     :IND-DTR-PRE-SOLO-RSH
            //                    ,:DTR-CAR-IAS
            //                     :IND-DTR-CAR-IAS
            //                    ,:DTR-PROV-ACQ-1AA
            //                     :IND-DTR-PROV-ACQ-1AA
            //                    ,:DTR-PROV-ACQ-2AA
            //                     :IND-DTR-PROV-ACQ-2AA
            //                    ,:DTR-PROV-RICOR
            //                     :IND-DTR-PROV-RICOR
            //                    ,:DTR-PROV-INC
            //                     :IND-DTR-PROV-INC
            //                    ,:DTR-PROV-DA-REC
            //                     :IND-DTR-PROV-DA-REC
            //                    ,:DTR-COD-DVS
            //                     :IND-DTR-COD-DVS
            //                    ,:DTR-FRQ-MOVI
            //                     :IND-DTR-FRQ-MOVI
            //                    ,:DTR-TP-RGM-FISC
            //                    ,:DTR-COD-TARI
            //                     :IND-DTR-COD-TARI
            //                    ,:DTR-TP-STAT-TIT
            //                    ,:DTR-IMP-AZ
            //                     :IND-DTR-IMP-AZ
            //                    ,:DTR-IMP-ADER
            //                     :IND-DTR-IMP-ADER
            //                    ,:DTR-IMP-TFR
            //                     :IND-DTR-IMP-TFR
            //                    ,:DTR-IMP-VOLO
            //                     :IND-DTR-IMP-VOLO
            //                    ,:DTR-FL-VLDT-TIT
            //                     :IND-DTR-FL-VLDT-TIT
            //                    ,:DTR-CAR-ACQ
            //                     :IND-DTR-CAR-ACQ
            //                    ,:DTR-CAR-GEST
            //                     :IND-DTR-CAR-GEST
            //                    ,:DTR-CAR-INC
            //                     :IND-DTR-CAR-INC
            //                    ,:DTR-MANFEE-ANTIC
            //                     :IND-DTR-MANFEE-ANTIC
            //                    ,:DTR-MANFEE-RICOR
            //                     :IND-DTR-MANFEE-RICOR
            //                    ,:DTR-MANFEE-REC
            //                     :IND-DTR-MANFEE-REC
            //                    ,:DTR-TOT-INTR-PREST
            //                     :IND-DTR-TOT-INTR-PREST
            //                    ,:DTR-DS-RIGA
            //                    ,:DTR-DS-OPER-SQL
            //                    ,:DTR-DS-VER
            //                    ,:DTR-DS-TS-INI-CPTZ
            //                    ,:DTR-DS-TS-END-CPTZ
            //                    ,:DTR-DS-UTENTE
            //                    ,:DTR-DS-STATO-ELAB
            //                    ,:DTR-IMP-TRASFE
            //                     :IND-DTR-IMP-TRASFE
            //                    ,:DTR-IMP-TFR-STRC
            //                     :IND-DTR-IMP-TFR-STRC
            //                    ,:DTR-ACQ-EXP
            //                     :IND-DTR-ACQ-EXP
            //                    ,:DTR-REMUN-ASS
            //                     :IND-DTR-REMUN-ASS
            //                    ,:DTR-COMMIS-INTER
            //                     :IND-DTR-COMMIS-INTER
            //                    ,:DTR-CNBT-ANTIRAC
            //                     :IND-DTR-CNBT-ANTIRAC
            //                  )
            //           END-EXEC
            dettTitDiRatDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE DETT_TIT_DI_RAT SET
        //                   ID_DETT_TIT_DI_RAT     =
        //                :DTR-ID-DETT-TIT-DI-RAT
        //                  ,ID_TIT_RAT             =
        //                :DTR-ID-TIT-RAT
        //                  ,ID_OGG                 =
        //                :DTR-ID-OGG
        //                  ,TP_OGG                 =
        //                :DTR-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :DTR-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DTR-ID-MOVI-CHIU
        //                                       :IND-DTR-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :DTR-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DTR-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :DTR-COD-COMP-ANIA
        //                  ,DT_INI_COP             =
        //           :DTR-DT-INI-COP-DB
        //                                       :IND-DTR-DT-INI-COP
        //                  ,DT_END_COP             =
        //           :DTR-DT-END-COP-DB
        //                                       :IND-DTR-DT-END-COP
        //                  ,PRE_NET                =
        //                :DTR-PRE-NET
        //                                       :IND-DTR-PRE-NET
        //                  ,INTR_FRAZ              =
        //                :DTR-INTR-FRAZ
        //                                       :IND-DTR-INTR-FRAZ
        //                  ,INTR_MORA              =
        //                :DTR-INTR-MORA
        //                                       :IND-DTR-INTR-MORA
        //                  ,INTR_RETDT             =
        //                :DTR-INTR-RETDT
        //                                       :IND-DTR-INTR-RETDT
        //                  ,INTR_RIAT              =
        //                :DTR-INTR-RIAT
        //                                       :IND-DTR-INTR-RIAT
        //                  ,DIR                    =
        //                :DTR-DIR
        //                                       :IND-DTR-DIR
        //                  ,SPE_MED                =
        //                :DTR-SPE-MED
        //                                       :IND-DTR-SPE-MED
        //                  ,SPE_AGE                =
        //                :DTR-SPE-AGE
        //                                       :IND-DTR-SPE-AGE
        //                  ,TAX                    =
        //                :DTR-TAX
        //                                       :IND-DTR-TAX
        //                  ,SOPR_SAN               =
        //                :DTR-SOPR-SAN
        //                                       :IND-DTR-SOPR-SAN
        //                  ,SOPR_SPO               =
        //                :DTR-SOPR-SPO
        //                                       :IND-DTR-SOPR-SPO
        //                  ,SOPR_TEC               =
        //                :DTR-SOPR-TEC
        //                                       :IND-DTR-SOPR-TEC
        //                  ,SOPR_PROF              =
        //                :DTR-SOPR-PROF
        //                                       :IND-DTR-SOPR-PROF
        //                  ,SOPR_ALT               =
        //                :DTR-SOPR-ALT
        //                                       :IND-DTR-SOPR-ALT
        //                  ,PRE_TOT                =
        //                :DTR-PRE-TOT
        //                                       :IND-DTR-PRE-TOT
        //                  ,PRE_PP_IAS             =
        //                :DTR-PRE-PP-IAS
        //                                       :IND-DTR-PRE-PP-IAS
        //                  ,PRE_SOLO_RSH           =
        //                :DTR-PRE-SOLO-RSH
        //                                       :IND-DTR-PRE-SOLO-RSH
        //                  ,CAR_IAS                =
        //                :DTR-CAR-IAS
        //                                       :IND-DTR-CAR-IAS
        //                  ,PROV_ACQ_1AA           =
        //                :DTR-PROV-ACQ-1AA
        //                                       :IND-DTR-PROV-ACQ-1AA
        //                  ,PROV_ACQ_2AA           =
        //                :DTR-PROV-ACQ-2AA
        //                                       :IND-DTR-PROV-ACQ-2AA
        //                  ,PROV_RICOR             =
        //                :DTR-PROV-RICOR
        //                                       :IND-DTR-PROV-RICOR
        //                  ,PROV_INC               =
        //                :DTR-PROV-INC
        //                                       :IND-DTR-PROV-INC
        //                  ,PROV_DA_REC            =
        //                :DTR-PROV-DA-REC
        //                                       :IND-DTR-PROV-DA-REC
        //                  ,COD_DVS                =
        //                :DTR-COD-DVS
        //                                       :IND-DTR-COD-DVS
        //                  ,FRQ_MOVI               =
        //                :DTR-FRQ-MOVI
        //                                       :IND-DTR-FRQ-MOVI
        //                  ,TP_RGM_FISC            =
        //                :DTR-TP-RGM-FISC
        //                  ,COD_TARI               =
        //                :DTR-COD-TARI
        //                                       :IND-DTR-COD-TARI
        //                  ,TP_STAT_TIT            =
        //                :DTR-TP-STAT-TIT
        //                  ,IMP_AZ                 =
        //                :DTR-IMP-AZ
        //                                       :IND-DTR-IMP-AZ
        //                  ,IMP_ADER               =
        //                :DTR-IMP-ADER
        //                                       :IND-DTR-IMP-ADER
        //                  ,IMP_TFR                =
        //                :DTR-IMP-TFR
        //                                       :IND-DTR-IMP-TFR
        //                  ,IMP_VOLO               =
        //                :DTR-IMP-VOLO
        //                                       :IND-DTR-IMP-VOLO
        //                  ,FL_VLDT_TIT            =
        //                :DTR-FL-VLDT-TIT
        //                                       :IND-DTR-FL-VLDT-TIT
        //                  ,CAR_ACQ                =
        //                :DTR-CAR-ACQ
        //                                       :IND-DTR-CAR-ACQ
        //                  ,CAR_GEST               =
        //                :DTR-CAR-GEST
        //                                       :IND-DTR-CAR-GEST
        //                  ,CAR_INC                =
        //                :DTR-CAR-INC
        //                                       :IND-DTR-CAR-INC
        //                  ,MANFEE_ANTIC           =
        //                :DTR-MANFEE-ANTIC
        //                                       :IND-DTR-MANFEE-ANTIC
        //                  ,MANFEE_RICOR           =
        //                :DTR-MANFEE-RICOR
        //                                       :IND-DTR-MANFEE-RICOR
        //                  ,MANFEE_REC             =
        //                :DTR-MANFEE-REC
        //                                       :IND-DTR-MANFEE-REC
        //                  ,TOT_INTR_PREST         =
        //                :DTR-TOT-INTR-PREST
        //                                       :IND-DTR-TOT-INTR-PREST
        //                  ,DS_RIGA                =
        //                :DTR-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DTR-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DTR-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DTR-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DTR-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DTR-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DTR-DS-STATO-ELAB
        //                  ,IMP_TRASFE             =
        //                :DTR-IMP-TRASFE
        //                                       :IND-DTR-IMP-TRASFE
        //                  ,IMP_TFR_STRC           =
        //                :DTR-IMP-TFR-STRC
        //                                       :IND-DTR-IMP-TFR-STRC
        //                  ,ACQ_EXP                =
        //                :DTR-ACQ-EXP
        //                                       :IND-DTR-ACQ-EXP
        //                  ,REMUN_ASS              =
        //                :DTR-REMUN-ASS
        //                                       :IND-DTR-REMUN-ASS
        //                  ,COMMIS_INTER           =
        //                :DTR-COMMIS-INTER
        //                                       :IND-DTR-COMMIS-INTER
        //                  ,CNBT_ANTIRAC           =
        //                :DTR-CNBT-ANTIRAC
        //                                       :IND-DTR-CNBT-ANTIRAC
        //                WHERE     DS_RIGA = :DTR-DS-RIGA
        //           END-EXEC.
        dettTitDiRatDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM DETT_TIT_DI_RAT
        //                WHERE     DS_RIGA = :DTR-DS-RIGA
        //           END-EXEC.
        dettTitDiRatDao.deleteByDtrDsRiga(dettTitDiRat.getDtrDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-DTR CURSOR FOR
        //              SELECT
        //                     ID_DETT_TIT_DI_RAT
        //                    ,ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,SPE_AGE
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_IAS
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_DI_RAT
        //              WHERE     ID_DETT_TIT_DI_RAT = :DTR-ID-DETT-TIT-DI-RAT
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_TIT_DI_RAT
        //                ,ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,PRE_NET
        //                ,INTR_FRAZ
        //                ,INTR_MORA
        //                ,INTR_RETDT
        //                ,INTR_RIAT
        //                ,DIR
        //                ,SPE_MED
        //                ,SPE_AGE
        //                ,TAX
        //                ,SOPR_SAN
        //                ,SOPR_SPO
        //                ,SOPR_TEC
        //                ,SOPR_PROF
        //                ,SOPR_ALT
        //                ,PRE_TOT
        //                ,PRE_PP_IAS
        //                ,PRE_SOLO_RSH
        //                ,CAR_IAS
        //                ,PROV_ACQ_1AA
        //                ,PROV_ACQ_2AA
        //                ,PROV_RICOR
        //                ,PROV_INC
        //                ,PROV_DA_REC
        //                ,COD_DVS
        //                ,FRQ_MOVI
        //                ,TP_RGM_FISC
        //                ,COD_TARI
        //                ,TP_STAT_TIT
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,CAR_ACQ
        //                ,CAR_GEST
        //                ,CAR_INC
        //                ,MANFEE_ANTIC
        //                ,MANFEE_RICOR
        //                ,MANFEE_REC
        //                ,TOT_INTR_PREST
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,ACQ_EXP
        //                ,REMUN_ASS
        //                ,COMMIS_INTER
        //                ,CNBT_ANTIRAC
        //             INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //             FROM DETT_TIT_DI_RAT
        //             WHERE     ID_DETT_TIT_DI_RAT = :DTR-ID-DETT-TIT-DI-RAT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettTitDiRatDao.selectRec(dettTitDiRat.getDtrIdDettTitDiRat(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE DETT_TIT_DI_RAT SET
        //                   ID_DETT_TIT_DI_RAT     =
        //                :DTR-ID-DETT-TIT-DI-RAT
        //                  ,ID_TIT_RAT             =
        //                :DTR-ID-TIT-RAT
        //                  ,ID_OGG                 =
        //                :DTR-ID-OGG
        //                  ,TP_OGG                 =
        //                :DTR-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :DTR-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :DTR-ID-MOVI-CHIU
        //                                       :IND-DTR-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :DTR-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :DTR-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :DTR-COD-COMP-ANIA
        //                  ,DT_INI_COP             =
        //           :DTR-DT-INI-COP-DB
        //                                       :IND-DTR-DT-INI-COP
        //                  ,DT_END_COP             =
        //           :DTR-DT-END-COP-DB
        //                                       :IND-DTR-DT-END-COP
        //                  ,PRE_NET                =
        //                :DTR-PRE-NET
        //                                       :IND-DTR-PRE-NET
        //                  ,INTR_FRAZ              =
        //                :DTR-INTR-FRAZ
        //                                       :IND-DTR-INTR-FRAZ
        //                  ,INTR_MORA              =
        //                :DTR-INTR-MORA
        //                                       :IND-DTR-INTR-MORA
        //                  ,INTR_RETDT             =
        //                :DTR-INTR-RETDT
        //                                       :IND-DTR-INTR-RETDT
        //                  ,INTR_RIAT              =
        //                :DTR-INTR-RIAT
        //                                       :IND-DTR-INTR-RIAT
        //                  ,DIR                    =
        //                :DTR-DIR
        //                                       :IND-DTR-DIR
        //                  ,SPE_MED                =
        //                :DTR-SPE-MED
        //                                       :IND-DTR-SPE-MED
        //                  ,SPE_AGE                =
        //                :DTR-SPE-AGE
        //                                       :IND-DTR-SPE-AGE
        //                  ,TAX                    =
        //                :DTR-TAX
        //                                       :IND-DTR-TAX
        //                  ,SOPR_SAN               =
        //                :DTR-SOPR-SAN
        //                                       :IND-DTR-SOPR-SAN
        //                  ,SOPR_SPO               =
        //                :DTR-SOPR-SPO
        //                                       :IND-DTR-SOPR-SPO
        //                  ,SOPR_TEC               =
        //                :DTR-SOPR-TEC
        //                                       :IND-DTR-SOPR-TEC
        //                  ,SOPR_PROF              =
        //                :DTR-SOPR-PROF
        //                                       :IND-DTR-SOPR-PROF
        //                  ,SOPR_ALT               =
        //                :DTR-SOPR-ALT
        //                                       :IND-DTR-SOPR-ALT
        //                  ,PRE_TOT                =
        //                :DTR-PRE-TOT
        //                                       :IND-DTR-PRE-TOT
        //                  ,PRE_PP_IAS             =
        //                :DTR-PRE-PP-IAS
        //                                       :IND-DTR-PRE-PP-IAS
        //                  ,PRE_SOLO_RSH           =
        //                :DTR-PRE-SOLO-RSH
        //                                       :IND-DTR-PRE-SOLO-RSH
        //                  ,CAR_IAS                =
        //                :DTR-CAR-IAS
        //                                       :IND-DTR-CAR-IAS
        //                  ,PROV_ACQ_1AA           =
        //                :DTR-PROV-ACQ-1AA
        //                                       :IND-DTR-PROV-ACQ-1AA
        //                  ,PROV_ACQ_2AA           =
        //                :DTR-PROV-ACQ-2AA
        //                                       :IND-DTR-PROV-ACQ-2AA
        //                  ,PROV_RICOR             =
        //                :DTR-PROV-RICOR
        //                                       :IND-DTR-PROV-RICOR
        //                  ,PROV_INC               =
        //                :DTR-PROV-INC
        //                                       :IND-DTR-PROV-INC
        //                  ,PROV_DA_REC            =
        //                :DTR-PROV-DA-REC
        //                                       :IND-DTR-PROV-DA-REC
        //                  ,COD_DVS                =
        //                :DTR-COD-DVS
        //                                       :IND-DTR-COD-DVS
        //                  ,FRQ_MOVI               =
        //                :DTR-FRQ-MOVI
        //                                       :IND-DTR-FRQ-MOVI
        //                  ,TP_RGM_FISC            =
        //                :DTR-TP-RGM-FISC
        //                  ,COD_TARI               =
        //                :DTR-COD-TARI
        //                                       :IND-DTR-COD-TARI
        //                  ,TP_STAT_TIT            =
        //                :DTR-TP-STAT-TIT
        //                  ,IMP_AZ                 =
        //                :DTR-IMP-AZ
        //                                       :IND-DTR-IMP-AZ
        //                  ,IMP_ADER               =
        //                :DTR-IMP-ADER
        //                                       :IND-DTR-IMP-ADER
        //                  ,IMP_TFR                =
        //                :DTR-IMP-TFR
        //                                       :IND-DTR-IMP-TFR
        //                  ,IMP_VOLO               =
        //                :DTR-IMP-VOLO
        //                                       :IND-DTR-IMP-VOLO
        //                  ,FL_VLDT_TIT            =
        //                :DTR-FL-VLDT-TIT
        //                                       :IND-DTR-FL-VLDT-TIT
        //                  ,CAR_ACQ                =
        //                :DTR-CAR-ACQ
        //                                       :IND-DTR-CAR-ACQ
        //                  ,CAR_GEST               =
        //                :DTR-CAR-GEST
        //                                       :IND-DTR-CAR-GEST
        //                  ,CAR_INC                =
        //                :DTR-CAR-INC
        //                                       :IND-DTR-CAR-INC
        //                  ,MANFEE_ANTIC           =
        //                :DTR-MANFEE-ANTIC
        //                                       :IND-DTR-MANFEE-ANTIC
        //                  ,MANFEE_RICOR           =
        //                :DTR-MANFEE-RICOR
        //                                       :IND-DTR-MANFEE-RICOR
        //                  ,MANFEE_REC             =
        //                :DTR-MANFEE-REC
        //                                       :IND-DTR-MANFEE-REC
        //                  ,TOT_INTR_PREST         =
        //                :DTR-TOT-INTR-PREST
        //                                       :IND-DTR-TOT-INTR-PREST
        //                  ,DS_RIGA                =
        //                :DTR-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :DTR-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :DTR-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :DTR-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :DTR-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :DTR-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :DTR-DS-STATO-ELAB
        //                  ,IMP_TRASFE             =
        //                :DTR-IMP-TRASFE
        //                                       :IND-DTR-IMP-TRASFE
        //                  ,IMP_TFR_STRC           =
        //                :DTR-IMP-TFR-STRC
        //                                       :IND-DTR-IMP-TFR-STRC
        //                  ,ACQ_EXP                =
        //                :DTR-ACQ-EXP
        //                                       :IND-DTR-ACQ-EXP
        //                  ,REMUN_ASS              =
        //                :DTR-REMUN-ASS
        //                                       :IND-DTR-REMUN-ASS
        //                  ,COMMIS_INTER           =
        //                :DTR-COMMIS-INTER
        //                                       :IND-DTR-COMMIS-INTER
        //                  ,CNBT_ANTIRAC           =
        //                :DTR-CNBT-ANTIRAC
        //                                       :IND-DTR-CNBT-ANTIRAC
        //                WHERE     DS_RIGA = :DTR-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettTitDiRatDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-DTR
        //           END-EXEC.
        dettTitDiRatDao.openCIdUpdEffDtr(dettTitDiRat.getDtrIdDettTitDiRat(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-DTR
        //           END-EXEC.
        dettTitDiRatDao.closeCIdUpdEffDtr();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-DTR
        //           INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //           END-EXEC.
        dettTitDiRatDao.fetchCIdUpdEffDtr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-DTR CURSOR FOR
        //              SELECT
        //                     ID_DETT_TIT_DI_RAT
        //                    ,ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,SPE_AGE
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_IAS
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_DI_RAT
        //              WHERE     ID_TIT_RAT = :DTR-ID-TIT-RAT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_DETT_TIT_DI_RAT ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_TIT_DI_RAT
        //                ,ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,PRE_NET
        //                ,INTR_FRAZ
        //                ,INTR_MORA
        //                ,INTR_RETDT
        //                ,INTR_RIAT
        //                ,DIR
        //                ,SPE_MED
        //                ,SPE_AGE
        //                ,TAX
        //                ,SOPR_SAN
        //                ,SOPR_SPO
        //                ,SOPR_TEC
        //                ,SOPR_PROF
        //                ,SOPR_ALT
        //                ,PRE_TOT
        //                ,PRE_PP_IAS
        //                ,PRE_SOLO_RSH
        //                ,CAR_IAS
        //                ,PROV_ACQ_1AA
        //                ,PROV_ACQ_2AA
        //                ,PROV_RICOR
        //                ,PROV_INC
        //                ,PROV_DA_REC
        //                ,COD_DVS
        //                ,FRQ_MOVI
        //                ,TP_RGM_FISC
        //                ,COD_TARI
        //                ,TP_STAT_TIT
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,CAR_ACQ
        //                ,CAR_GEST
        //                ,CAR_INC
        //                ,MANFEE_ANTIC
        //                ,MANFEE_RICOR
        //                ,MANFEE_REC
        //                ,TOT_INTR_PREST
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,ACQ_EXP
        //                ,REMUN_ASS
        //                ,COMMIS_INTER
        //                ,CNBT_ANTIRAC
        //             INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //             FROM DETT_TIT_DI_RAT
        //             WHERE     ID_TIT_RAT = :DTR-ID-TIT-RAT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettTitDiRatDao.selectRec1(dettTitDiRat.getDtrIdTitRat(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-DTR
        //           END-EXEC.
        dettTitDiRatDao.openCIdpEffDtr(dettTitDiRat.getDtrIdTitRat(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-DTR
        //           END-EXEC.
        dettTitDiRatDao.closeCIdpEffDtr();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-DTR
        //           INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //           END-EXEC.
        dettTitDiRatDao.fetchCIdpEffDtr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-EFF-DTR CURSOR FOR
        //              SELECT
        //                     ID_DETT_TIT_DI_RAT
        //                    ,ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,SPE_AGE
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_IAS
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_DI_RAT
        //              WHERE     ID_OGG = :DTR-ID-OGG
        //                    AND TP_OGG = :DTR-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_DETT_TIT_DI_RAT ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_TIT_DI_RAT
        //                ,ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,PRE_NET
        //                ,INTR_FRAZ
        //                ,INTR_MORA
        //                ,INTR_RETDT
        //                ,INTR_RIAT
        //                ,DIR
        //                ,SPE_MED
        //                ,SPE_AGE
        //                ,TAX
        //                ,SOPR_SAN
        //                ,SOPR_SPO
        //                ,SOPR_TEC
        //                ,SOPR_PROF
        //                ,SOPR_ALT
        //                ,PRE_TOT
        //                ,PRE_PP_IAS
        //                ,PRE_SOLO_RSH
        //                ,CAR_IAS
        //                ,PROV_ACQ_1AA
        //                ,PROV_ACQ_2AA
        //                ,PROV_RICOR
        //                ,PROV_INC
        //                ,PROV_DA_REC
        //                ,COD_DVS
        //                ,FRQ_MOVI
        //                ,TP_RGM_FISC
        //                ,COD_TARI
        //                ,TP_STAT_TIT
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,CAR_ACQ
        //                ,CAR_GEST
        //                ,CAR_INC
        //                ,MANFEE_ANTIC
        //                ,MANFEE_RICOR
        //                ,MANFEE_REC
        //                ,TOT_INTR_PREST
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,ACQ_EXP
        //                ,REMUN_ASS
        //                ,COMMIS_INTER
        //                ,CNBT_ANTIRAC
        //             INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //             FROM DETT_TIT_DI_RAT
        //             WHERE     ID_OGG = :DTR-ID-OGG
        //                    AND TP_OGG = :DTR-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        dettTitDiRatDao.selectRec2(dettTitDiRat.getDtrIdOgg(), dettTitDiRat.getDtrTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-EFF-DTR
        //           END-EXEC.
        dettTitDiRatDao.openCIdoEffDtr(dettTitDiRat.getDtrIdOgg(), dettTitDiRat.getDtrTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-EFF-DTR
        //           END-EXEC.
        dettTitDiRatDao.closeCIdoEffDtr();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-EFF-DTR
        //           INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //           END-EXEC.
        dettTitDiRatDao.fetchCIdoEffDtr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX
            a770CloseCursorIdo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_TIT_DI_RAT
        //                ,ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,PRE_NET
        //                ,INTR_FRAZ
        //                ,INTR_MORA
        //                ,INTR_RETDT
        //                ,INTR_RIAT
        //                ,DIR
        //                ,SPE_MED
        //                ,SPE_AGE
        //                ,TAX
        //                ,SOPR_SAN
        //                ,SOPR_SPO
        //                ,SOPR_TEC
        //                ,SOPR_PROF
        //                ,SOPR_ALT
        //                ,PRE_TOT
        //                ,PRE_PP_IAS
        //                ,PRE_SOLO_RSH
        //                ,CAR_IAS
        //                ,PROV_ACQ_1AA
        //                ,PROV_ACQ_2AA
        //                ,PROV_RICOR
        //                ,PROV_INC
        //                ,PROV_DA_REC
        //                ,COD_DVS
        //                ,FRQ_MOVI
        //                ,TP_RGM_FISC
        //                ,COD_TARI
        //                ,TP_STAT_TIT
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,CAR_ACQ
        //                ,CAR_GEST
        //                ,CAR_INC
        //                ,MANFEE_ANTIC
        //                ,MANFEE_RICOR
        //                ,MANFEE_REC
        //                ,TOT_INTR_PREST
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,ACQ_EXP
        //                ,REMUN_ASS
        //                ,COMMIS_INTER
        //                ,CNBT_ANTIRAC
        //             INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //             FROM DETT_TIT_DI_RAT
        //             WHERE     ID_DETT_TIT_DI_RAT = :DTR-ID-DETT-TIT-DI-RAT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dettTitDiRatDao.selectRec3(dettTitDiRat.getDtrIdDettTitDiRat(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-DTR CURSOR FOR
        //              SELECT
        //                     ID_DETT_TIT_DI_RAT
        //                    ,ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,SPE_AGE
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_IAS
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_DI_RAT
        //              WHERE     ID_TIT_RAT = :DTR-ID-TIT-RAT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_DETT_TIT_DI_RAT ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_TIT_DI_RAT
        //                ,ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,PRE_NET
        //                ,INTR_FRAZ
        //                ,INTR_MORA
        //                ,INTR_RETDT
        //                ,INTR_RIAT
        //                ,DIR
        //                ,SPE_MED
        //                ,SPE_AGE
        //                ,TAX
        //                ,SOPR_SAN
        //                ,SOPR_SPO
        //                ,SOPR_TEC
        //                ,SOPR_PROF
        //                ,SOPR_ALT
        //                ,PRE_TOT
        //                ,PRE_PP_IAS
        //                ,PRE_SOLO_RSH
        //                ,CAR_IAS
        //                ,PROV_ACQ_1AA
        //                ,PROV_ACQ_2AA
        //                ,PROV_RICOR
        //                ,PROV_INC
        //                ,PROV_DA_REC
        //                ,COD_DVS
        //                ,FRQ_MOVI
        //                ,TP_RGM_FISC
        //                ,COD_TARI
        //                ,TP_STAT_TIT
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,CAR_ACQ
        //                ,CAR_GEST
        //                ,CAR_INC
        //                ,MANFEE_ANTIC
        //                ,MANFEE_RICOR
        //                ,MANFEE_REC
        //                ,TOT_INTR_PREST
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,ACQ_EXP
        //                ,REMUN_ASS
        //                ,COMMIS_INTER
        //                ,CNBT_ANTIRAC
        //             INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //             FROM DETT_TIT_DI_RAT
        //             WHERE     ID_TIT_RAT = :DTR-ID-TIT-RAT
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dettTitDiRatDao.selectRec4(dettTitDiRat.getDtrIdTitRat(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-DTR
        //           END-EXEC.
        dettTitDiRatDao.openCIdpCpzDtr(dettTitDiRat.getDtrIdTitRat(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-DTR
        //           END-EXEC.
        dettTitDiRatDao.closeCIdpCpzDtr();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-DTR
        //           INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //           END-EXEC.
        dettTitDiRatDao.fetchCIdpCpzDtr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-CPZ-DTR CURSOR FOR
        //              SELECT
        //                     ID_DETT_TIT_DI_RAT
        //                    ,ID_TIT_RAT
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,DT_INI_COP
        //                    ,DT_END_COP
        //                    ,PRE_NET
        //                    ,INTR_FRAZ
        //                    ,INTR_MORA
        //                    ,INTR_RETDT
        //                    ,INTR_RIAT
        //                    ,DIR
        //                    ,SPE_MED
        //                    ,SPE_AGE
        //                    ,TAX
        //                    ,SOPR_SAN
        //                    ,SOPR_SPO
        //                    ,SOPR_TEC
        //                    ,SOPR_PROF
        //                    ,SOPR_ALT
        //                    ,PRE_TOT
        //                    ,PRE_PP_IAS
        //                    ,PRE_SOLO_RSH
        //                    ,CAR_IAS
        //                    ,PROV_ACQ_1AA
        //                    ,PROV_ACQ_2AA
        //                    ,PROV_RICOR
        //                    ,PROV_INC
        //                    ,PROV_DA_REC
        //                    ,COD_DVS
        //                    ,FRQ_MOVI
        //                    ,TP_RGM_FISC
        //                    ,COD_TARI
        //                    ,TP_STAT_TIT
        //                    ,IMP_AZ
        //                    ,IMP_ADER
        //                    ,IMP_TFR
        //                    ,IMP_VOLO
        //                    ,FL_VLDT_TIT
        //                    ,CAR_ACQ
        //                    ,CAR_GEST
        //                    ,CAR_INC
        //                    ,MANFEE_ANTIC
        //                    ,MANFEE_RICOR
        //                    ,MANFEE_REC
        //                    ,TOT_INTR_PREST
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,IMP_TRASFE
        //                    ,IMP_TFR_STRC
        //                    ,ACQ_EXP
        //                    ,REMUN_ASS
        //                    ,COMMIS_INTER
        //                    ,CNBT_ANTIRAC
        //              FROM DETT_TIT_DI_RAT
        //              WHERE     ID_OGG = :DTR-ID-OGG
        //           AND TP_OGG = :DTR-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_DETT_TIT_DI_RAT ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_DETT_TIT_DI_RAT
        //                ,ID_TIT_RAT
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,DT_INI_COP
        //                ,DT_END_COP
        //                ,PRE_NET
        //                ,INTR_FRAZ
        //                ,INTR_MORA
        //                ,INTR_RETDT
        //                ,INTR_RIAT
        //                ,DIR
        //                ,SPE_MED
        //                ,SPE_AGE
        //                ,TAX
        //                ,SOPR_SAN
        //                ,SOPR_SPO
        //                ,SOPR_TEC
        //                ,SOPR_PROF
        //                ,SOPR_ALT
        //                ,PRE_TOT
        //                ,PRE_PP_IAS
        //                ,PRE_SOLO_RSH
        //                ,CAR_IAS
        //                ,PROV_ACQ_1AA
        //                ,PROV_ACQ_2AA
        //                ,PROV_RICOR
        //                ,PROV_INC
        //                ,PROV_DA_REC
        //                ,COD_DVS
        //                ,FRQ_MOVI
        //                ,TP_RGM_FISC
        //                ,COD_TARI
        //                ,TP_STAT_TIT
        //                ,IMP_AZ
        //                ,IMP_ADER
        //                ,IMP_TFR
        //                ,IMP_VOLO
        //                ,FL_VLDT_TIT
        //                ,CAR_ACQ
        //                ,CAR_GEST
        //                ,CAR_INC
        //                ,MANFEE_ANTIC
        //                ,MANFEE_RICOR
        //                ,MANFEE_REC
        //                ,TOT_INTR_PREST
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,IMP_TRASFE
        //                ,IMP_TFR_STRC
        //                ,ACQ_EXP
        //                ,REMUN_ASS
        //                ,COMMIS_INTER
        //                ,CNBT_ANTIRAC
        //             INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //             FROM DETT_TIT_DI_RAT
        //             WHERE     ID_OGG = :DTR-ID-OGG
        //                    AND TP_OGG = :DTR-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        dettTitDiRatDao.selectRec5(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-CPZ-DTR
        //           END-EXEC.
        dettTitDiRatDao.openCIdoCpzDtr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-CPZ-DTR
        //           END-EXEC.
        dettTitDiRatDao.closeCIdoCpzDtr();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-CPZ-DTR
        //           INTO
        //                :DTR-ID-DETT-TIT-DI-RAT
        //               ,:DTR-ID-TIT-RAT
        //               ,:DTR-ID-OGG
        //               ,:DTR-TP-OGG
        //               ,:DTR-ID-MOVI-CRZ
        //               ,:DTR-ID-MOVI-CHIU
        //                :IND-DTR-ID-MOVI-CHIU
        //               ,:DTR-DT-INI-EFF-DB
        //               ,:DTR-DT-END-EFF-DB
        //               ,:DTR-COD-COMP-ANIA
        //               ,:DTR-DT-INI-COP-DB
        //                :IND-DTR-DT-INI-COP
        //               ,:DTR-DT-END-COP-DB
        //                :IND-DTR-DT-END-COP
        //               ,:DTR-PRE-NET
        //                :IND-DTR-PRE-NET
        //               ,:DTR-INTR-FRAZ
        //                :IND-DTR-INTR-FRAZ
        //               ,:DTR-INTR-MORA
        //                :IND-DTR-INTR-MORA
        //               ,:DTR-INTR-RETDT
        //                :IND-DTR-INTR-RETDT
        //               ,:DTR-INTR-RIAT
        //                :IND-DTR-INTR-RIAT
        //               ,:DTR-DIR
        //                :IND-DTR-DIR
        //               ,:DTR-SPE-MED
        //                :IND-DTR-SPE-MED
        //               ,:DTR-SPE-AGE
        //                :IND-DTR-SPE-AGE
        //               ,:DTR-TAX
        //                :IND-DTR-TAX
        //               ,:DTR-SOPR-SAN
        //                :IND-DTR-SOPR-SAN
        //               ,:DTR-SOPR-SPO
        //                :IND-DTR-SOPR-SPO
        //               ,:DTR-SOPR-TEC
        //                :IND-DTR-SOPR-TEC
        //               ,:DTR-SOPR-PROF
        //                :IND-DTR-SOPR-PROF
        //               ,:DTR-SOPR-ALT
        //                :IND-DTR-SOPR-ALT
        //               ,:DTR-PRE-TOT
        //                :IND-DTR-PRE-TOT
        //               ,:DTR-PRE-PP-IAS
        //                :IND-DTR-PRE-PP-IAS
        //               ,:DTR-PRE-SOLO-RSH
        //                :IND-DTR-PRE-SOLO-RSH
        //               ,:DTR-CAR-IAS
        //                :IND-DTR-CAR-IAS
        //               ,:DTR-PROV-ACQ-1AA
        //                :IND-DTR-PROV-ACQ-1AA
        //               ,:DTR-PROV-ACQ-2AA
        //                :IND-DTR-PROV-ACQ-2AA
        //               ,:DTR-PROV-RICOR
        //                :IND-DTR-PROV-RICOR
        //               ,:DTR-PROV-INC
        //                :IND-DTR-PROV-INC
        //               ,:DTR-PROV-DA-REC
        //                :IND-DTR-PROV-DA-REC
        //               ,:DTR-COD-DVS
        //                :IND-DTR-COD-DVS
        //               ,:DTR-FRQ-MOVI
        //                :IND-DTR-FRQ-MOVI
        //               ,:DTR-TP-RGM-FISC
        //               ,:DTR-COD-TARI
        //                :IND-DTR-COD-TARI
        //               ,:DTR-TP-STAT-TIT
        //               ,:DTR-IMP-AZ
        //                :IND-DTR-IMP-AZ
        //               ,:DTR-IMP-ADER
        //                :IND-DTR-IMP-ADER
        //               ,:DTR-IMP-TFR
        //                :IND-DTR-IMP-TFR
        //               ,:DTR-IMP-VOLO
        //                :IND-DTR-IMP-VOLO
        //               ,:DTR-FL-VLDT-TIT
        //                :IND-DTR-FL-VLDT-TIT
        //               ,:DTR-CAR-ACQ
        //                :IND-DTR-CAR-ACQ
        //               ,:DTR-CAR-GEST
        //                :IND-DTR-CAR-GEST
        //               ,:DTR-CAR-INC
        //                :IND-DTR-CAR-INC
        //               ,:DTR-MANFEE-ANTIC
        //                :IND-DTR-MANFEE-ANTIC
        //               ,:DTR-MANFEE-RICOR
        //                :IND-DTR-MANFEE-RICOR
        //               ,:DTR-MANFEE-REC
        //                :IND-DTR-MANFEE-REC
        //               ,:DTR-TOT-INTR-PREST
        //                :IND-DTR-TOT-INTR-PREST
        //               ,:DTR-DS-RIGA
        //               ,:DTR-DS-OPER-SQL
        //               ,:DTR-DS-VER
        //               ,:DTR-DS-TS-INI-CPTZ
        //               ,:DTR-DS-TS-END-CPTZ
        //               ,:DTR-DS-UTENTE
        //               ,:DTR-DS-STATO-ELAB
        //               ,:DTR-IMP-TRASFE
        //                :IND-DTR-IMP-TRASFE
        //               ,:DTR-IMP-TFR-STRC
        //                :IND-DTR-IMP-TFR-STRC
        //               ,:DTR-ACQ-EXP
        //                :IND-DTR-ACQ-EXP
        //               ,:DTR-REMUN-ASS
        //                :IND-DTR-REMUN-ASS
        //               ,:DTR-COMMIS-INTER
        //                :IND-DTR-COMMIS-INTER
        //               ,:DTR-CNBT-ANTIRAC
        //                :IND-DTR-CNBT-ANTIRAC
        //           END-EXEC.
        dettTitDiRatDao.fetchCIdoCpzDtr(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX
            b770CloseCursorIdoCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-DTR-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO DTR-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-ID-MOVI-CHIU-NULL
            dettTitDiRat.getDtrIdMoviChiu().setDtrIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIdMoviChiu.Len.DTR_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-DTR-DT-INI-COP = -1
        //              MOVE HIGH-VALUES TO DTR-DT-INI-COP-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getDtIniCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-DT-INI-COP-NULL
            dettTitDiRat.getDtrDtIniCop().setDtrDtIniCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrDtIniCop.Len.DTR_DT_INI_COP_NULL));
        }
        // COB_CODE: IF IND-DTR-DT-END-COP = -1
        //              MOVE HIGH-VALUES TO DTR-DT-END-COP-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getDtEndCop() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-DT-END-COP-NULL
            dettTitDiRat.getDtrDtEndCop().setDtrDtEndCopNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrDtEndCop.Len.DTR_DT_END_COP_NULL));
        }
        // COB_CODE: IF IND-DTR-PRE-NET = -1
        //              MOVE HIGH-VALUES TO DTR-PRE-NET-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getPreNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PRE-NET-NULL
            dettTitDiRat.getDtrPreNet().setDtrPreNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrPreNet.Len.DTR_PRE_NET_NULL));
        }
        // COB_CODE: IF IND-DTR-INTR-FRAZ = -1
        //              MOVE HIGH-VALUES TO DTR-INTR-FRAZ-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIntrFraz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-INTR-FRAZ-NULL
            dettTitDiRat.getDtrIntrFraz().setDtrIntrFrazNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIntrFraz.Len.DTR_INTR_FRAZ_NULL));
        }
        // COB_CODE: IF IND-DTR-INTR-MORA = -1
        //              MOVE HIGH-VALUES TO DTR-INTR-MORA-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIntrMora() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-INTR-MORA-NULL
            dettTitDiRat.getDtrIntrMora().setDtrIntrMoraNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIntrMora.Len.DTR_INTR_MORA_NULL));
        }
        // COB_CODE: IF IND-DTR-INTR-RETDT = -1
        //              MOVE HIGH-VALUES TO DTR-INTR-RETDT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIntrRetdt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-INTR-RETDT-NULL
            dettTitDiRat.getDtrIntrRetdt().setDtrIntrRetdtNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIntrRetdt.Len.DTR_INTR_RETDT_NULL));
        }
        // COB_CODE: IF IND-DTR-INTR-RIAT = -1
        //              MOVE HIGH-VALUES TO DTR-INTR-RIAT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getIntrRiat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-INTR-RIAT-NULL
            dettTitDiRat.getDtrIntrRiat().setDtrIntrRiatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIntrRiat.Len.DTR_INTR_RIAT_NULL));
        }
        // COB_CODE: IF IND-DTR-DIR = -1
        //              MOVE HIGH-VALUES TO DTR-DIR-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getDir() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-DIR-NULL
            dettTitDiRat.getDtrDir().setDtrDirNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrDir.Len.DTR_DIR_NULL));
        }
        // COB_CODE: IF IND-DTR-SPE-MED = -1
        //              MOVE HIGH-VALUES TO DTR-SPE-MED-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSpeMed() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SPE-MED-NULL
            dettTitDiRat.getDtrSpeMed().setDtrSpeMedNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSpeMed.Len.DTR_SPE_MED_NULL));
        }
        // COB_CODE: IF IND-DTR-SPE-AGE = -1
        //              MOVE HIGH-VALUES TO DTR-SPE-AGE-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSpeAge() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SPE-AGE-NULL
            dettTitDiRat.getDtrSpeAge().setDtrSpeAgeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSpeAge.Len.DTR_SPE_AGE_NULL));
        }
        // COB_CODE: IF IND-DTR-TAX = -1
        //              MOVE HIGH-VALUES TO DTR-TAX-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-TAX-NULL
            dettTitDiRat.getDtrTax().setDtrTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrTax.Len.DTR_TAX_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-SAN = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-SAN-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprSan() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-SAN-NULL
            dettTitDiRat.getDtrSoprSan().setDtrSoprSanNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprSan.Len.DTR_SOPR_SAN_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-SPO = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-SPO-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprSpo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-SPO-NULL
            dettTitDiRat.getDtrSoprSpo().setDtrSoprSpoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprSpo.Len.DTR_SOPR_SPO_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-TEC = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-TEC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprTec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-TEC-NULL
            dettTitDiRat.getDtrSoprTec().setDtrSoprTecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprTec.Len.DTR_SOPR_TEC_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-PROF = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-PROF-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-PROF-NULL
            dettTitDiRat.getDtrSoprProf().setDtrSoprProfNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprProf.Len.DTR_SOPR_PROF_NULL));
        }
        // COB_CODE: IF IND-DTR-SOPR-ALT = -1
        //              MOVE HIGH-VALUES TO DTR-SOPR-ALT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getSoprAlt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-SOPR-ALT-NULL
            dettTitDiRat.getDtrSoprAlt().setDtrSoprAltNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrSoprAlt.Len.DTR_SOPR_ALT_NULL));
        }
        // COB_CODE: IF IND-DTR-PRE-TOT = -1
        //              MOVE HIGH-VALUES TO DTR-PRE-TOT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getPreTot() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PRE-TOT-NULL
            dettTitDiRat.getDtrPreTot().setDtrPreTotNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrPreTot.Len.DTR_PRE_TOT_NULL));
        }
        // COB_CODE: IF IND-DTR-PRE-PP-IAS = -1
        //              MOVE HIGH-VALUES TO DTR-PRE-PP-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getPrePpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PRE-PP-IAS-NULL
            dettTitDiRat.getDtrPrePpIas().setDtrPrePpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrPrePpIas.Len.DTR_PRE_PP_IAS_NULL));
        }
        // COB_CODE: IF IND-DTR-PRE-SOLO-RSH = -1
        //              MOVE HIGH-VALUES TO DTR-PRE-SOLO-RSH-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getPreSoloRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PRE-SOLO-RSH-NULL
            dettTitDiRat.getDtrPreSoloRsh().setDtrPreSoloRshNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrPreSoloRsh.Len.DTR_PRE_SOLO_RSH_NULL));
        }
        // COB_CODE: IF IND-DTR-CAR-IAS = -1
        //              MOVE HIGH-VALUES TO DTR-CAR-IAS-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCarIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CAR-IAS-NULL
            dettTitDiRat.getDtrCarIas().setDtrCarIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCarIas.Len.DTR_CAR_IAS_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-ACQ-1AA = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-ACQ-1AA-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvAcq1aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-ACQ-1AA-NULL
            dettTitDiRat.getDtrProvAcq1aa().setDtrProvAcq1aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvAcq1aa.Len.DTR_PROV_ACQ1AA_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-ACQ-2AA = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-ACQ-2AA-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvAcq2aa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-ACQ-2AA-NULL
            dettTitDiRat.getDtrProvAcq2aa().setDtrProvAcq2aaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvAcq2aa.Len.DTR_PROV_ACQ2AA_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-RICOR = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-RICOR-NULL
            dettTitDiRat.getDtrProvRicor().setDtrProvRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvRicor.Len.DTR_PROV_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-INC = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-INC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-INC-NULL
            dettTitDiRat.getDtrProvInc().setDtrProvIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvInc.Len.DTR_PROV_INC_NULL));
        }
        // COB_CODE: IF IND-DTR-PROV-DA-REC = -1
        //              MOVE HIGH-VALUES TO DTR-PROV-DA-REC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getProvDaRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-PROV-DA-REC-NULL
            dettTitDiRat.getDtrProvDaRec().setDtrProvDaRecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrProvDaRec.Len.DTR_PROV_DA_REC_NULL));
        }
        // COB_CODE: IF IND-DTR-COD-DVS = -1
        //              MOVE HIGH-VALUES TO DTR-COD-DVS-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCodDvs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-COD-DVS-NULL
            dettTitDiRat.setDtrCodDvs(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitDiRatIdbsdtr0.Len.DTR_COD_DVS));
        }
        // COB_CODE: IF IND-DTR-FRQ-MOVI = -1
        //              MOVE HIGH-VALUES TO DTR-FRQ-MOVI-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getFrqMovi() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-FRQ-MOVI-NULL
            dettTitDiRat.getDtrFrqMovi().setDtrFrqMoviNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrFrqMovi.Len.DTR_FRQ_MOVI_NULL));
        }
        // COB_CODE: IF IND-DTR-COD-TARI = -1
        //              MOVE HIGH-VALUES TO DTR-COD-TARI-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCodTari() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-COD-TARI-NULL
            dettTitDiRat.setDtrCodTari(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitDiRatIdbsdtr0.Len.DTR_COD_TARI));
        }
        // COB_CODE: IF IND-DTR-IMP-AZ = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-AZ-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-AZ-NULL
            dettTitDiRat.getDtrImpAz().setDtrImpAzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpAz.Len.DTR_IMP_AZ_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-ADER = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-ADER-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpAder() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-ADER-NULL
            dettTitDiRat.getDtrImpAder().setDtrImpAderNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpAder.Len.DTR_IMP_ADER_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-TFR = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-TFR-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpTfr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-TFR-NULL
            dettTitDiRat.getDtrImpTfr().setDtrImpTfrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpTfr.Len.DTR_IMP_TFR_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-VOLO = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-VOLO-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpVolo() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-VOLO-NULL
            dettTitDiRat.getDtrImpVolo().setDtrImpVoloNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpVolo.Len.DTR_IMP_VOLO_NULL));
        }
        // COB_CODE: IF IND-DTR-FL-VLDT-TIT = -1
        //              MOVE HIGH-VALUES TO DTR-FL-VLDT-TIT-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getFlVldtTit() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-FL-VLDT-TIT-NULL
            dettTitDiRat.setDtrFlVldtTit(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-DTR-CAR-ACQ = -1
        //              MOVE HIGH-VALUES TO DTR-CAR-ACQ-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCarAcq() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CAR-ACQ-NULL
            dettTitDiRat.getDtrCarAcq().setDtrCarAcqNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCarAcq.Len.DTR_CAR_ACQ_NULL));
        }
        // COB_CODE: IF IND-DTR-CAR-GEST = -1
        //              MOVE HIGH-VALUES TO DTR-CAR-GEST-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCarGest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CAR-GEST-NULL
            dettTitDiRat.getDtrCarGest().setDtrCarGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCarGest.Len.DTR_CAR_GEST_NULL));
        }
        // COB_CODE: IF IND-DTR-CAR-INC = -1
        //              MOVE HIGH-VALUES TO DTR-CAR-INC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCarInc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CAR-INC-NULL
            dettTitDiRat.getDtrCarInc().setDtrCarIncNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCarInc.Len.DTR_CAR_INC_NULL));
        }
        // COB_CODE: IF IND-DTR-MANFEE-ANTIC = -1
        //              MOVE HIGH-VALUES TO DTR-MANFEE-ANTIC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getManfeeAntic() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-MANFEE-ANTIC-NULL
            dettTitDiRat.getDtrManfeeAntic().setDtrManfeeAnticNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrManfeeAntic.Len.DTR_MANFEE_ANTIC_NULL));
        }
        // COB_CODE: IF IND-DTR-MANFEE-RICOR = -1
        //              MOVE HIGH-VALUES TO DTR-MANFEE-RICOR-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getManfeeRicor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-MANFEE-RICOR-NULL
            dettTitDiRat.getDtrManfeeRicor().setDtrManfeeRicorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrManfeeRicor.Len.DTR_MANFEE_RICOR_NULL));
        }
        // COB_CODE: IF IND-DTR-MANFEE-REC = -1
        //              MOVE HIGH-VALUES TO DTR-MANFEE-REC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getManfeeRec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-MANFEE-REC-NULL
            dettTitDiRat.setDtrManfeeRec(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DettTitDiRatIdbsdtr0.Len.DTR_MANFEE_REC));
        }
        // COB_CODE: IF IND-DTR-TOT-INTR-PREST = -1
        //              MOVE HIGH-VALUES TO DTR-TOT-INTR-PREST-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getTotIntrPrest() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-TOT-INTR-PREST-NULL
            dettTitDiRat.getDtrTotIntrPrest().setDtrTotIntrPrestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrTotIntrPrest.Len.DTR_TOT_INTR_PREST_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-TRASFE = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-TRASFE-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpTrasfe() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-TRASFE-NULL
            dettTitDiRat.getDtrImpTrasfe().setDtrImpTrasfeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpTrasfe.Len.DTR_IMP_TRASFE_NULL));
        }
        // COB_CODE: IF IND-DTR-IMP-TFR-STRC = -1
        //              MOVE HIGH-VALUES TO DTR-IMP-TFR-STRC-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getImpTfrStrc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-IMP-TFR-STRC-NULL
            dettTitDiRat.getDtrImpTfrStrc().setDtrImpTfrStrcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrImpTfrStrc.Len.DTR_IMP_TFR_STRC_NULL));
        }
        // COB_CODE: IF IND-DTR-ACQ-EXP = -1
        //              MOVE HIGH-VALUES TO DTR-ACQ-EXP-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getAcqExp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-ACQ-EXP-NULL
            dettTitDiRat.getDtrAcqExp().setDtrAcqExpNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrAcqExp.Len.DTR_ACQ_EXP_NULL));
        }
        // COB_CODE: IF IND-DTR-REMUN-ASS = -1
        //              MOVE HIGH-VALUES TO DTR-REMUN-ASS-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getRemunAss() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-REMUN-ASS-NULL
            dettTitDiRat.getDtrRemunAss().setDtrRemunAssNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrRemunAss.Len.DTR_REMUN_ASS_NULL));
        }
        // COB_CODE: IF IND-DTR-COMMIS-INTER = -1
        //              MOVE HIGH-VALUES TO DTR-COMMIS-INTER-NULL
        //           END-IF
        if (ws.getIndDettTitDiRat().getCommisInter() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-COMMIS-INTER-NULL
            dettTitDiRat.getDtrCommisInter().setDtrCommisInterNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCommisInter.Len.DTR_COMMIS_INTER_NULL));
        }
        // COB_CODE: IF IND-DTR-CNBT-ANTIRAC = -1
        //              MOVE HIGH-VALUES TO DTR-CNBT-ANTIRAC-NULL
        //           END-IF.
        if (ws.getIndDettTitDiRat().getCnbtAntirac() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO DTR-CNBT-ANTIRAC-NULL
            dettTitDiRat.getDtrCnbtAntirac().setDtrCnbtAntiracNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrCnbtAntirac.Len.DTR_CNBT_ANTIRAC_NULL));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO DTR-DS-OPER-SQL
        dettTitDiRat.setDtrDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO DTR-DS-VER
        dettTitDiRat.setDtrDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DTR-DS-UTENTE
        dettTitDiRat.setDtrDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO DTR-DS-STATO-ELAB.
        dettTitDiRat.setDtrDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO DTR-DS-OPER-SQL
        dettTitDiRat.setDtrDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO DTR-DS-UTENTE.
        dettTitDiRat.setDtrDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF DTR-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-DTR-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrIdMoviChiu().getDtrIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-ID-MOVI-CHIU
            ws.getIndDettTitDiRat().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-ID-MOVI-CHIU
            ws.getIndDettTitDiRat().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF DTR-DT-INI-COP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-DT-INI-COP
        //           ELSE
        //              MOVE 0 TO IND-DTR-DT-INI-COP
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrDtIniCop().getDtrDtIniCopNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-DT-INI-COP
            ws.getIndDettTitDiRat().setDtIniCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-DT-INI-COP
            ws.getIndDettTitDiRat().setDtIniCop(((short)0));
        }
        // COB_CODE: IF DTR-DT-END-COP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-DT-END-COP
        //           ELSE
        //              MOVE 0 TO IND-DTR-DT-END-COP
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrDtEndCop().getDtrDtEndCopNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-DT-END-COP
            ws.getIndDettTitDiRat().setDtEndCop(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-DT-END-COP
            ws.getIndDettTitDiRat().setDtEndCop(((short)0));
        }
        // COB_CODE: IF DTR-PRE-NET-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-PRE-NET
        //           ELSE
        //              MOVE 0 TO IND-DTR-PRE-NET
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrPreNet().getDtrPreNetNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-PRE-NET
            ws.getIndDettTitDiRat().setPreNet(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-PRE-NET
            ws.getIndDettTitDiRat().setPreNet(((short)0));
        }
        // COB_CODE: IF DTR-INTR-FRAZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-INTR-FRAZ
        //           ELSE
        //              MOVE 0 TO IND-DTR-INTR-FRAZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrIntrFraz().getDtrIntrFrazNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-INTR-FRAZ
            ws.getIndDettTitDiRat().setIntrFraz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-INTR-FRAZ
            ws.getIndDettTitDiRat().setIntrFraz(((short)0));
        }
        // COB_CODE: IF DTR-INTR-MORA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-INTR-MORA
        //           ELSE
        //              MOVE 0 TO IND-DTR-INTR-MORA
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrIntrMora().getDtrIntrMoraNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-INTR-MORA
            ws.getIndDettTitDiRat().setIntrMora(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-INTR-MORA
            ws.getIndDettTitDiRat().setIntrMora(((short)0));
        }
        // COB_CODE: IF DTR-INTR-RETDT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-INTR-RETDT
        //           ELSE
        //              MOVE 0 TO IND-DTR-INTR-RETDT
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrIntrRetdt().getDtrIntrRetdtNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-INTR-RETDT
            ws.getIndDettTitDiRat().setIntrRetdt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-INTR-RETDT
            ws.getIndDettTitDiRat().setIntrRetdt(((short)0));
        }
        // COB_CODE: IF DTR-INTR-RIAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-INTR-RIAT
        //           ELSE
        //              MOVE 0 TO IND-DTR-INTR-RIAT
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrIntrRiat().getDtrIntrRiatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-INTR-RIAT
            ws.getIndDettTitDiRat().setIntrRiat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-INTR-RIAT
            ws.getIndDettTitDiRat().setIntrRiat(((short)0));
        }
        // COB_CODE: IF DTR-DIR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-DIR
        //           ELSE
        //              MOVE 0 TO IND-DTR-DIR
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrDir().getDtrDirNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-DIR
            ws.getIndDettTitDiRat().setDir(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-DIR
            ws.getIndDettTitDiRat().setDir(((short)0));
        }
        // COB_CODE: IF DTR-SPE-MED-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-SPE-MED
        //           ELSE
        //              MOVE 0 TO IND-DTR-SPE-MED
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrSpeMed().getDtrSpeMedNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-SPE-MED
            ws.getIndDettTitDiRat().setSpeMed(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-SPE-MED
            ws.getIndDettTitDiRat().setSpeMed(((short)0));
        }
        // COB_CODE: IF DTR-SPE-AGE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-SPE-AGE
        //           ELSE
        //              MOVE 0 TO IND-DTR-SPE-AGE
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrSpeAge().getDtrSpeAgeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-SPE-AGE
            ws.getIndDettTitDiRat().setSpeAge(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-SPE-AGE
            ws.getIndDettTitDiRat().setSpeAge(((short)0));
        }
        // COB_CODE: IF DTR-TAX-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-TAX
        //           ELSE
        //              MOVE 0 TO IND-DTR-TAX
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrTax().getDtrTaxNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-TAX
            ws.getIndDettTitDiRat().setTax(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-TAX
            ws.getIndDettTitDiRat().setTax(((short)0));
        }
        // COB_CODE: IF DTR-SOPR-SAN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-SOPR-SAN
        //           ELSE
        //              MOVE 0 TO IND-DTR-SOPR-SAN
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrSoprSan().getDtrSoprSanNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-SOPR-SAN
            ws.getIndDettTitDiRat().setSoprSan(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-SOPR-SAN
            ws.getIndDettTitDiRat().setSoprSan(((short)0));
        }
        // COB_CODE: IF DTR-SOPR-SPO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-SOPR-SPO
        //           ELSE
        //              MOVE 0 TO IND-DTR-SOPR-SPO
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrSoprSpo().getDtrSoprSpoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-SOPR-SPO
            ws.getIndDettTitDiRat().setSoprSpo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-SOPR-SPO
            ws.getIndDettTitDiRat().setSoprSpo(((short)0));
        }
        // COB_CODE: IF DTR-SOPR-TEC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-SOPR-TEC
        //           ELSE
        //              MOVE 0 TO IND-DTR-SOPR-TEC
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrSoprTec().getDtrSoprTecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-SOPR-TEC
            ws.getIndDettTitDiRat().setSoprTec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-SOPR-TEC
            ws.getIndDettTitDiRat().setSoprTec(((short)0));
        }
        // COB_CODE: IF DTR-SOPR-PROF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-SOPR-PROF
        //           ELSE
        //              MOVE 0 TO IND-DTR-SOPR-PROF
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrSoprProf().getDtrSoprProfNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-SOPR-PROF
            ws.getIndDettTitDiRat().setSoprProf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-SOPR-PROF
            ws.getIndDettTitDiRat().setSoprProf(((short)0));
        }
        // COB_CODE: IF DTR-SOPR-ALT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-SOPR-ALT
        //           ELSE
        //              MOVE 0 TO IND-DTR-SOPR-ALT
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrSoprAlt().getDtrSoprAltNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-SOPR-ALT
            ws.getIndDettTitDiRat().setSoprAlt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-SOPR-ALT
            ws.getIndDettTitDiRat().setSoprAlt(((short)0));
        }
        // COB_CODE: IF DTR-PRE-TOT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-PRE-TOT
        //           ELSE
        //              MOVE 0 TO IND-DTR-PRE-TOT
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrPreTot().getDtrPreTotNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-PRE-TOT
            ws.getIndDettTitDiRat().setPreTot(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-PRE-TOT
            ws.getIndDettTitDiRat().setPreTot(((short)0));
        }
        // COB_CODE: IF DTR-PRE-PP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-PRE-PP-IAS
        //           ELSE
        //              MOVE 0 TO IND-DTR-PRE-PP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrPrePpIas().getDtrPrePpIasNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-PRE-PP-IAS
            ws.getIndDettTitDiRat().setPrePpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-PRE-PP-IAS
            ws.getIndDettTitDiRat().setPrePpIas(((short)0));
        }
        // COB_CODE: IF DTR-PRE-SOLO-RSH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-PRE-SOLO-RSH
        //           ELSE
        //              MOVE 0 TO IND-DTR-PRE-SOLO-RSH
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrPreSoloRsh().getDtrPreSoloRshNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-PRE-SOLO-RSH
            ws.getIndDettTitDiRat().setPreSoloRsh(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-PRE-SOLO-RSH
            ws.getIndDettTitDiRat().setPreSoloRsh(((short)0));
        }
        // COB_CODE: IF DTR-CAR-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-CAR-IAS
        //           ELSE
        //              MOVE 0 TO IND-DTR-CAR-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrCarIas().getDtrCarIasNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-CAR-IAS
            ws.getIndDettTitDiRat().setCarIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-CAR-IAS
            ws.getIndDettTitDiRat().setCarIas(((short)0));
        }
        // COB_CODE: IF DTR-PROV-ACQ-1AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-PROV-ACQ-1AA
        //           ELSE
        //              MOVE 0 TO IND-DTR-PROV-ACQ-1AA
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrProvAcq1aa().getDtrProvAcq1aaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-PROV-ACQ-1AA
            ws.getIndDettTitDiRat().setProvAcq1aa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-PROV-ACQ-1AA
            ws.getIndDettTitDiRat().setProvAcq1aa(((short)0));
        }
        // COB_CODE: IF DTR-PROV-ACQ-2AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-PROV-ACQ-2AA
        //           ELSE
        //              MOVE 0 TO IND-DTR-PROV-ACQ-2AA
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrProvAcq2aa().getDtrProvAcq2aaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-PROV-ACQ-2AA
            ws.getIndDettTitDiRat().setProvAcq2aa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-PROV-ACQ-2AA
            ws.getIndDettTitDiRat().setProvAcq2aa(((short)0));
        }
        // COB_CODE: IF DTR-PROV-RICOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-PROV-RICOR
        //           ELSE
        //              MOVE 0 TO IND-DTR-PROV-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrProvRicor().getDtrProvRicorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-PROV-RICOR
            ws.getIndDettTitDiRat().setProvRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-PROV-RICOR
            ws.getIndDettTitDiRat().setProvRicor(((short)0));
        }
        // COB_CODE: IF DTR-PROV-INC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-PROV-INC
        //           ELSE
        //              MOVE 0 TO IND-DTR-PROV-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrProvInc().getDtrProvIncNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-PROV-INC
            ws.getIndDettTitDiRat().setProvInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-PROV-INC
            ws.getIndDettTitDiRat().setProvInc(((short)0));
        }
        // COB_CODE: IF DTR-PROV-DA-REC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-PROV-DA-REC
        //           ELSE
        //              MOVE 0 TO IND-DTR-PROV-DA-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrProvDaRec().getDtrProvDaRecNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-PROV-DA-REC
            ws.getIndDettTitDiRat().setProvDaRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-PROV-DA-REC
            ws.getIndDettTitDiRat().setProvDaRec(((short)0));
        }
        // COB_CODE: IF DTR-COD-DVS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-COD-DVS
        //           ELSE
        //              MOVE 0 TO IND-DTR-COD-DVS
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrCodDvsFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-COD-DVS
            ws.getIndDettTitDiRat().setCodDvs(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-COD-DVS
            ws.getIndDettTitDiRat().setCodDvs(((short)0));
        }
        // COB_CODE: IF DTR-FRQ-MOVI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-FRQ-MOVI
        //           ELSE
        //              MOVE 0 TO IND-DTR-FRQ-MOVI
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrFrqMovi().getDtrFrqMoviNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-FRQ-MOVI
            ws.getIndDettTitDiRat().setFrqMovi(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-FRQ-MOVI
            ws.getIndDettTitDiRat().setFrqMovi(((short)0));
        }
        // COB_CODE: IF DTR-COD-TARI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-COD-TARI
        //           ELSE
        //              MOVE 0 TO IND-DTR-COD-TARI
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrCodTariFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-COD-TARI
            ws.getIndDettTitDiRat().setCodTari(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-COD-TARI
            ws.getIndDettTitDiRat().setCodTari(((short)0));
        }
        // COB_CODE: IF DTR-IMP-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-IMP-AZ
        //           ELSE
        //              MOVE 0 TO IND-DTR-IMP-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrImpAz().getDtrImpAzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-IMP-AZ
            ws.getIndDettTitDiRat().setImpAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-IMP-AZ
            ws.getIndDettTitDiRat().setImpAz(((short)0));
        }
        // COB_CODE: IF DTR-IMP-ADER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-IMP-ADER
        //           ELSE
        //              MOVE 0 TO IND-DTR-IMP-ADER
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrImpAder().getDtrImpAderNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-IMP-ADER
            ws.getIndDettTitDiRat().setImpAder(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-IMP-ADER
            ws.getIndDettTitDiRat().setImpAder(((short)0));
        }
        // COB_CODE: IF DTR-IMP-TFR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-IMP-TFR
        //           ELSE
        //              MOVE 0 TO IND-DTR-IMP-TFR
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrImpTfr().getDtrImpTfrNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-IMP-TFR
            ws.getIndDettTitDiRat().setImpTfr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-IMP-TFR
            ws.getIndDettTitDiRat().setImpTfr(((short)0));
        }
        // COB_CODE: IF DTR-IMP-VOLO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-IMP-VOLO
        //           ELSE
        //              MOVE 0 TO IND-DTR-IMP-VOLO
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrImpVolo().getDtrImpVoloNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-IMP-VOLO
            ws.getIndDettTitDiRat().setImpVolo(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-IMP-VOLO
            ws.getIndDettTitDiRat().setImpVolo(((short)0));
        }
        // COB_CODE: IF DTR-FL-VLDT-TIT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-FL-VLDT-TIT
        //           ELSE
        //              MOVE 0 TO IND-DTR-FL-VLDT-TIT
        //           END-IF
        if (Conditions.eq(dettTitDiRat.getDtrFlVldtTit(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-DTR-FL-VLDT-TIT
            ws.getIndDettTitDiRat().setFlVldtTit(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-FL-VLDT-TIT
            ws.getIndDettTitDiRat().setFlVldtTit(((short)0));
        }
        // COB_CODE: IF DTR-CAR-ACQ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-CAR-ACQ
        //           ELSE
        //              MOVE 0 TO IND-DTR-CAR-ACQ
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrCarAcq().getDtrCarAcqNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-CAR-ACQ
            ws.getIndDettTitDiRat().setCarAcq(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-CAR-ACQ
            ws.getIndDettTitDiRat().setCarAcq(((short)0));
        }
        // COB_CODE: IF DTR-CAR-GEST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-CAR-GEST
        //           ELSE
        //              MOVE 0 TO IND-DTR-CAR-GEST
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrCarGest().getDtrCarGestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-CAR-GEST
            ws.getIndDettTitDiRat().setCarGest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-CAR-GEST
            ws.getIndDettTitDiRat().setCarGest(((short)0));
        }
        // COB_CODE: IF DTR-CAR-INC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-CAR-INC
        //           ELSE
        //              MOVE 0 TO IND-DTR-CAR-INC
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrCarInc().getDtrCarIncNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-CAR-INC
            ws.getIndDettTitDiRat().setCarInc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-CAR-INC
            ws.getIndDettTitDiRat().setCarInc(((short)0));
        }
        // COB_CODE: IF DTR-MANFEE-ANTIC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-MANFEE-ANTIC
        //           ELSE
        //              MOVE 0 TO IND-DTR-MANFEE-ANTIC
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrManfeeAntic().getDtrManfeeAnticNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-MANFEE-ANTIC
            ws.getIndDettTitDiRat().setManfeeAntic(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-MANFEE-ANTIC
            ws.getIndDettTitDiRat().setManfeeAntic(((short)0));
        }
        // COB_CODE: IF DTR-MANFEE-RICOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-MANFEE-RICOR
        //           ELSE
        //              MOVE 0 TO IND-DTR-MANFEE-RICOR
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrManfeeRicor().getDtrManfeeRicorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-MANFEE-RICOR
            ws.getIndDettTitDiRat().setManfeeRicor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-MANFEE-RICOR
            ws.getIndDettTitDiRat().setManfeeRicor(((short)0));
        }
        // COB_CODE: IF DTR-MANFEE-REC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-MANFEE-REC
        //           ELSE
        //              MOVE 0 TO IND-DTR-MANFEE-REC
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrManfeeRecFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-MANFEE-REC
            ws.getIndDettTitDiRat().setManfeeRec(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-MANFEE-REC
            ws.getIndDettTitDiRat().setManfeeRec(((short)0));
        }
        // COB_CODE: IF DTR-TOT-INTR-PREST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-TOT-INTR-PREST
        //           ELSE
        //              MOVE 0 TO IND-DTR-TOT-INTR-PREST
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrTotIntrPrest().getDtrTotIntrPrestNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-TOT-INTR-PREST
            ws.getIndDettTitDiRat().setTotIntrPrest(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-TOT-INTR-PREST
            ws.getIndDettTitDiRat().setTotIntrPrest(((short)0));
        }
        // COB_CODE: IF DTR-IMP-TRASFE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-IMP-TRASFE
        //           ELSE
        //              MOVE 0 TO IND-DTR-IMP-TRASFE
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrImpTrasfe().getDtrImpTrasfeNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-IMP-TRASFE
            ws.getIndDettTitDiRat().setImpTrasfe(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-IMP-TRASFE
            ws.getIndDettTitDiRat().setImpTrasfe(((short)0));
        }
        // COB_CODE: IF DTR-IMP-TFR-STRC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-IMP-TFR-STRC
        //           ELSE
        //              MOVE 0 TO IND-DTR-IMP-TFR-STRC
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrImpTfrStrc().getDtrImpTfrStrcNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-IMP-TFR-STRC
            ws.getIndDettTitDiRat().setImpTfrStrc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-IMP-TFR-STRC
            ws.getIndDettTitDiRat().setImpTfrStrc(((short)0));
        }
        // COB_CODE: IF DTR-ACQ-EXP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-ACQ-EXP
        //           ELSE
        //              MOVE 0 TO IND-DTR-ACQ-EXP
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrAcqExp().getDtrAcqExpNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-ACQ-EXP
            ws.getIndDettTitDiRat().setAcqExp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-ACQ-EXP
            ws.getIndDettTitDiRat().setAcqExp(((short)0));
        }
        // COB_CODE: IF DTR-REMUN-ASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-REMUN-ASS
        //           ELSE
        //              MOVE 0 TO IND-DTR-REMUN-ASS
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrRemunAss().getDtrRemunAssNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-REMUN-ASS
            ws.getIndDettTitDiRat().setRemunAss(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-REMUN-ASS
            ws.getIndDettTitDiRat().setRemunAss(((short)0));
        }
        // COB_CODE: IF DTR-COMMIS-INTER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-COMMIS-INTER
        //           ELSE
        //              MOVE 0 TO IND-DTR-COMMIS-INTER
        //           END-IF
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrCommisInter().getDtrCommisInterNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-COMMIS-INTER
            ws.getIndDettTitDiRat().setCommisInter(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-COMMIS-INTER
            ws.getIndDettTitDiRat().setCommisInter(((short)0));
        }
        // COB_CODE: IF DTR-CNBT-ANTIRAC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-DTR-CNBT-ANTIRAC
        //           ELSE
        //              MOVE 0 TO IND-DTR-CNBT-ANTIRAC
        //           END-IF.
        if (Characters.EQ_HIGH.test(dettTitDiRat.getDtrCnbtAntirac().getDtrCnbtAntiracNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-DTR-CNBT-ANTIRAC
            ws.getIndDettTitDiRat().setCnbtAntirac(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-DTR-CNBT-ANTIRAC
            ws.getIndDettTitDiRat().setCnbtAntirac(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : DTR-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE DETT-TIT-DI-RAT TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dettTitDiRat.getDettTitDiRatFormatted());
        // COB_CODE: MOVE DTR-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dettTitDiRat.getDtrIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO DTR-ID-MOVI-CHIU
                dettTitDiRat.getDtrIdMoviChiu().setDtrIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO DTR-DS-TS-END-CPTZ
                dettTitDiRat.setDtrDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO DTR-ID-MOVI-CRZ
                    dettTitDiRat.setDtrIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO DTR-ID-MOVI-CHIU-NULL
                    dettTitDiRat.getDtrIdMoviChiu().setDtrIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIdMoviChiu.Len.DTR_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO DTR-DT-END-EFF
                    dettTitDiRat.setDtrDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO DTR-DS-TS-INI-CPTZ
                    dettTitDiRat.setDtrDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO DTR-DS-TS-END-CPTZ
                    dettTitDiRat.setDtrDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE DETT-TIT-DI-RAT TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(dettTitDiRat.getDettTitDiRatFormatted());
        // COB_CODE: MOVE DTR-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(dettTitDiRat.getDtrIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO DETT-TIT-DI-RAT.
        dettTitDiRat.setDettTitDiRatFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO DTR-ID-MOVI-CRZ.
        dettTitDiRat.setDtrIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO DTR-ID-MOVI-CHIU-NULL.
        dettTitDiRat.getDtrIdMoviChiu().setDtrIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, DtrIdMoviChiu.Len.DTR_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO DTR-DT-INI-EFF.
        dettTitDiRat.setDtrDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO DTR-DT-END-EFF.
        dettTitDiRat.setDtrDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO DTR-DS-TS-INI-CPTZ.
        dettTitDiRat.setDtrDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO DTR-DS-TS-END-CPTZ.
        dettTitDiRat.setDtrDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO DTR-COD-COMP-ANIA.
        dettTitDiRat.setDtrCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE DTR-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dettTitDiRat.getDtrDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DTR-DT-INI-EFF-DB
        ws.getDettTitDiRatDb().setEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE DTR-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dettTitDiRat.getDtrDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO DTR-DT-END-EFF-DB
        ws.getDettTitDiRatDb().setRgstrzRichDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-DTR-DT-INI-COP = 0
        //               MOVE WS-DATE-X      TO DTR-DT-INI-COP-DB
        //           END-IF
        if (ws.getIndDettTitDiRat().getDtIniCop() == 0) {
            // COB_CODE: MOVE DTR-DT-INI-COP TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dettTitDiRat.getDtrDtIniCop().getDtrDtIniCop(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DTR-DT-INI-COP-DB
            ws.getDettTitDiRatDb().setPervRichDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-DTR-DT-END-COP = 0
        //               MOVE WS-DATE-X      TO DTR-DT-END-COP-DB
        //           END-IF.
        if (ws.getIndDettTitDiRat().getDtEndCop() == 0) {
            // COB_CODE: MOVE DTR-DT-END-COP TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(dettTitDiRat.getDtrDtEndCop().getDtrDtEndCop(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO DTR-DT-END-COP-DB
            ws.getDettTitDiRatDb().setEsecRichDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE DTR-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettTitDiRatDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DTR-DT-INI-EFF
        dettTitDiRat.setDtrDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE DTR-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getDettTitDiRatDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO DTR-DT-END-EFF
        dettTitDiRat.setDtrDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-DTR-DT-INI-COP = 0
        //               MOVE WS-DATE-N      TO DTR-DT-INI-COP
        //           END-IF
        if (ws.getIndDettTitDiRat().getDtIniCop() == 0) {
            // COB_CODE: MOVE DTR-DT-INI-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitDiRatDb().getPervRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTR-DT-INI-COP
            dettTitDiRat.getDtrDtIniCop().setDtrDtIniCop(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-DTR-DT-END-COP = 0
        //               MOVE WS-DATE-N      TO DTR-DT-END-COP
        //           END-IF.
        if (ws.getIndDettTitDiRat().getDtEndCop() == 0) {
            // COB_CODE: MOVE DTR-DT-END-COP-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getDettTitDiRatDb().getEsecRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO DTR-DT-END-COP
            dettTitDiRat.getDtrDtEndCop().setDtrDtEndCop(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getAcqExp() {
        return dettTitDiRat.getDtrAcqExp().getDtrAcqExp();
    }

    @Override
    public void setAcqExp(AfDecimal acqExp) {
        this.dettTitDiRat.getDtrAcqExp().setDtrAcqExp(acqExp.copy());
    }

    @Override
    public AfDecimal getAcqExpObj() {
        if (ws.getIndDettTitDiRat().getAcqExp() >= 0) {
            return getAcqExp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAcqExpObj(AfDecimal acqExpObj) {
        if (acqExpObj != null) {
            setAcqExp(new AfDecimal(acqExpObj, 15, 3));
            ws.getIndDettTitDiRat().setAcqExp(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setAcqExp(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarAcq() {
        return dettTitDiRat.getDtrCarAcq().getDtrCarAcq();
    }

    @Override
    public void setCarAcq(AfDecimal carAcq) {
        this.dettTitDiRat.getDtrCarAcq().setDtrCarAcq(carAcq.copy());
    }

    @Override
    public AfDecimal getCarAcqObj() {
        if (ws.getIndDettTitDiRat().getCarAcq() >= 0) {
            return getCarAcq();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarAcqObj(AfDecimal carAcqObj) {
        if (carAcqObj != null) {
            setCarAcq(new AfDecimal(carAcqObj, 15, 3));
            ws.getIndDettTitDiRat().setCarAcq(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCarAcq(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarGest() {
        return dettTitDiRat.getDtrCarGest().getDtrCarGest();
    }

    @Override
    public void setCarGest(AfDecimal carGest) {
        this.dettTitDiRat.getDtrCarGest().setDtrCarGest(carGest.copy());
    }

    @Override
    public AfDecimal getCarGestObj() {
        if (ws.getIndDettTitDiRat().getCarGest() >= 0) {
            return getCarGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarGestObj(AfDecimal carGestObj) {
        if (carGestObj != null) {
            setCarGest(new AfDecimal(carGestObj, 15, 3));
            ws.getIndDettTitDiRat().setCarGest(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCarGest(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarIas() {
        return dettTitDiRat.getDtrCarIas().getDtrCarIas();
    }

    @Override
    public void setCarIas(AfDecimal carIas) {
        this.dettTitDiRat.getDtrCarIas().setDtrCarIas(carIas.copy());
    }

    @Override
    public AfDecimal getCarIasObj() {
        if (ws.getIndDettTitDiRat().getCarIas() >= 0) {
            return getCarIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarIasObj(AfDecimal carIasObj) {
        if (carIasObj != null) {
            setCarIas(new AfDecimal(carIasObj, 15, 3));
            ws.getIndDettTitDiRat().setCarIas(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCarIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getCarInc() {
        return dettTitDiRat.getDtrCarInc().getDtrCarInc();
    }

    @Override
    public void setCarInc(AfDecimal carInc) {
        this.dettTitDiRat.getDtrCarInc().setDtrCarInc(carInc.copy());
    }

    @Override
    public AfDecimal getCarIncObj() {
        if (ws.getIndDettTitDiRat().getCarInc() >= 0) {
            return getCarInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCarIncObj(AfDecimal carIncObj) {
        if (carIncObj != null) {
            setCarInc(new AfDecimal(carIncObj, 15, 3));
            ws.getIndDettTitDiRat().setCarInc(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCarInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getCnbtAntirac() {
        return dettTitDiRat.getDtrCnbtAntirac().getDtrCnbtAntirac();
    }

    @Override
    public void setCnbtAntirac(AfDecimal cnbtAntirac) {
        this.dettTitDiRat.getDtrCnbtAntirac().setDtrCnbtAntirac(cnbtAntirac.copy());
    }

    @Override
    public AfDecimal getCnbtAntiracObj() {
        if (ws.getIndDettTitDiRat().getCnbtAntirac() >= 0) {
            return getCnbtAntirac();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCnbtAntiracObj(AfDecimal cnbtAntiracObj) {
        if (cnbtAntiracObj != null) {
            setCnbtAntirac(new AfDecimal(cnbtAntiracObj, 15, 3));
            ws.getIndDettTitDiRat().setCnbtAntirac(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCnbtAntirac(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return dettTitDiRat.getDtrCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.dettTitDiRat.setDtrCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodDvs() {
        return dettTitDiRat.getDtrCodDvs();
    }

    @Override
    public void setCodDvs(String codDvs) {
        this.dettTitDiRat.setDtrCodDvs(codDvs);
    }

    @Override
    public String getCodDvsObj() {
        if (ws.getIndDettTitDiRat().getCodDvs() >= 0) {
            return getCodDvs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodDvsObj(String codDvsObj) {
        if (codDvsObj != null) {
            setCodDvs(codDvsObj);
            ws.getIndDettTitDiRat().setCodDvs(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCodDvs(((short)-1));
        }
    }

    @Override
    public String getCodTari() {
        return dettTitDiRat.getDtrCodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.dettTitDiRat.setDtrCodTari(codTari);
    }

    @Override
    public String getCodTariObj() {
        if (ws.getIndDettTitDiRat().getCodTari() >= 0) {
            return getCodTari();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTariObj(String codTariObj) {
        if (codTariObj != null) {
            setCodTari(codTariObj);
            ws.getIndDettTitDiRat().setCodTari(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCodTari(((short)-1));
        }
    }

    @Override
    public AfDecimal getCommisInter() {
        return dettTitDiRat.getDtrCommisInter().getDtrCommisInter();
    }

    @Override
    public void setCommisInter(AfDecimal commisInter) {
        this.dettTitDiRat.getDtrCommisInter().setDtrCommisInter(commisInter.copy());
    }

    @Override
    public AfDecimal getCommisInterObj() {
        if (ws.getIndDettTitDiRat().getCommisInter() >= 0) {
            return getCommisInter();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommisInterObj(AfDecimal commisInterObj) {
        if (commisInterObj != null) {
            setCommisInter(new AfDecimal(commisInterObj, 15, 3));
            ws.getIndDettTitDiRat().setCommisInter(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setCommisInter(((short)-1));
        }
    }

    @Override
    public AfDecimal getDir() {
        return dettTitDiRat.getDtrDir().getDtrDir();
    }

    @Override
    public void setDir(AfDecimal dir) {
        this.dettTitDiRat.getDtrDir().setDtrDir(dir.copy());
    }

    @Override
    public AfDecimal getDirObj() {
        if (ws.getIndDettTitDiRat().getDir() >= 0) {
            return getDir();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDirObj(AfDecimal dirObj) {
        if (dirObj != null) {
            setDir(new AfDecimal(dirObj, 15, 3));
            ws.getIndDettTitDiRat().setDir(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setDir(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return dettTitDiRat.getDtrDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.dettTitDiRat.setDtrDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return dettTitDiRat.getDtrDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.dettTitDiRat.setDtrDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return dettTitDiRat.getDtrDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.dettTitDiRat.setDtrDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return dettTitDiRat.getDtrDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.dettTitDiRat.setDtrDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return dettTitDiRat.getDtrDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.dettTitDiRat.setDtrDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return dettTitDiRat.getDtrDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.dettTitDiRat.setDtrDsVer(dsVer);
    }

    @Override
    public String getDtEndCopDb() {
        return ws.getDettTitDiRatDb().getEsecRichDb();
    }

    @Override
    public void setDtEndCopDb(String dtEndCopDb) {
        this.ws.getDettTitDiRatDb().setEsecRichDb(dtEndCopDb);
    }

    @Override
    public String getDtEndCopDbObj() {
        if (ws.getIndDettTitDiRat().getDtEndCop() >= 0) {
            return getDtEndCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndCopDbObj(String dtEndCopDbObj) {
        if (dtEndCopDbObj != null) {
            setDtEndCopDb(dtEndCopDbObj);
            ws.getIndDettTitDiRat().setDtEndCop(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setDtEndCop(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getDettTitDiRatDb().getRgstrzRichDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getDettTitDiRatDb().setRgstrzRichDb(dtEndEffDb);
    }

    @Override
    public String getDtIniCopDb() {
        return ws.getDettTitDiRatDb().getPervRichDb();
    }

    @Override
    public void setDtIniCopDb(String dtIniCopDb) {
        this.ws.getDettTitDiRatDb().setPervRichDb(dtIniCopDb);
    }

    @Override
    public String getDtIniCopDbObj() {
        if (ws.getIndDettTitDiRat().getDtIniCop() >= 0) {
            return getDtIniCopDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniCopDbObj(String dtIniCopDbObj) {
        if (dtIniCopDbObj != null) {
            setDtIniCopDb(dtIniCopDbObj);
            ws.getIndDettTitDiRat().setDtIniCop(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setDtIniCop(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getDettTitDiRatDb().getEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getDettTitDiRatDb().setEffDb(dtIniEffDb);
    }

    @Override
    public long getDtrDsRiga() {
        return dettTitDiRat.getDtrDsRiga();
    }

    @Override
    public void setDtrDsRiga(long dtrDsRiga) {
        this.dettTitDiRat.setDtrDsRiga(dtrDsRiga);
    }

    @Override
    public char getDtrFlVldtTit() {
        return dettTitDiRat.getDtrFlVldtTit();
    }

    @Override
    public void setDtrFlVldtTit(char dtrFlVldtTit) {
        this.dettTitDiRat.setDtrFlVldtTit(dtrFlVldtTit);
    }

    @Override
    public Character getDtrFlVldtTitObj() {
        if (ws.getIndDettTitDiRat().getFlVldtTit() >= 0) {
            return ((Character)getDtrFlVldtTit());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtrFlVldtTitObj(Character dtrFlVldtTitObj) {
        if (dtrFlVldtTitObj != null) {
            setDtrFlVldtTit(((char)dtrFlVldtTitObj));
            ws.getIndDettTitDiRat().setFlVldtTit(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setFlVldtTit(((short)-1));
        }
    }

    @Override
    public int getDtrIdOgg() {
        return dettTitDiRat.getDtrIdOgg();
    }

    @Override
    public void setDtrIdOgg(int dtrIdOgg) {
        this.dettTitDiRat.setDtrIdOgg(dtrIdOgg);
    }

    @Override
    public int getDtrIdTitRat() {
        return dettTitDiRat.getDtrIdTitRat();
    }

    @Override
    public void setDtrIdTitRat(int dtrIdTitRat) {
        this.dettTitDiRat.setDtrIdTitRat(dtrIdTitRat);
    }

    @Override
    public String getDtrTpOgg() {
        return dettTitDiRat.getDtrTpOgg();
    }

    @Override
    public void setDtrTpOgg(String dtrTpOgg) {
        this.dettTitDiRat.setDtrTpOgg(dtrTpOgg);
    }

    @Override
    public int getFrqMovi() {
        return dettTitDiRat.getDtrFrqMovi().getDtrFrqMovi();
    }

    @Override
    public void setFrqMovi(int frqMovi) {
        this.dettTitDiRat.getDtrFrqMovi().setDtrFrqMovi(frqMovi);
    }

    @Override
    public Integer getFrqMoviObj() {
        if (ws.getIndDettTitDiRat().getFrqMovi() >= 0) {
            return ((Integer)getFrqMovi());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrqMoviObj(Integer frqMoviObj) {
        if (frqMoviObj != null) {
            setFrqMovi(((int)frqMoviObj));
            ws.getIndDettTitDiRat().setFrqMovi(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setFrqMovi(((short)-1));
        }
    }

    @Override
    public int getIdDettTitDiRat() {
        return dettTitDiRat.getDtrIdDettTitDiRat();
    }

    @Override
    public void setIdDettTitDiRat(int idDettTitDiRat) {
        this.dettTitDiRat.setDtrIdDettTitDiRat(idDettTitDiRat);
    }

    @Override
    public int getIdMoviChiu() {
        return dettTitDiRat.getDtrIdMoviChiu().getDtrIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.dettTitDiRat.getDtrIdMoviChiu().setDtrIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndDettTitDiRat().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndDettTitDiRat().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return dettTitDiRat.getDtrIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.dettTitDiRat.setDtrIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpAder() {
        return dettTitDiRat.getDtrImpAder().getDtrImpAder();
    }

    @Override
    public void setImpAder(AfDecimal impAder) {
        this.dettTitDiRat.getDtrImpAder().setDtrImpAder(impAder.copy());
    }

    @Override
    public AfDecimal getImpAderObj() {
        if (ws.getIndDettTitDiRat().getImpAder() >= 0) {
            return getImpAder();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAderObj(AfDecimal impAderObj) {
        if (impAderObj != null) {
            setImpAder(new AfDecimal(impAderObj, 15, 3));
            ws.getIndDettTitDiRat().setImpAder(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpAder(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpAz() {
        return dettTitDiRat.getDtrImpAz().getDtrImpAz();
    }

    @Override
    public void setImpAz(AfDecimal impAz) {
        this.dettTitDiRat.getDtrImpAz().setDtrImpAz(impAz.copy());
    }

    @Override
    public AfDecimal getImpAzObj() {
        if (ws.getIndDettTitDiRat().getImpAz() >= 0) {
            return getImpAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpAzObj(AfDecimal impAzObj) {
        if (impAzObj != null) {
            setImpAz(new AfDecimal(impAzObj, 15, 3));
            ws.getIndDettTitDiRat().setImpAz(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpAz(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfr() {
        return dettTitDiRat.getDtrImpTfr().getDtrImpTfr();
    }

    @Override
    public void setImpTfr(AfDecimal impTfr) {
        this.dettTitDiRat.getDtrImpTfr().setDtrImpTfr(impTfr.copy());
    }

    @Override
    public AfDecimal getImpTfrObj() {
        if (ws.getIndDettTitDiRat().getImpTfr() >= 0) {
            return getImpTfr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrObj(AfDecimal impTfrObj) {
        if (impTfrObj != null) {
            setImpTfr(new AfDecimal(impTfrObj, 15, 3));
            ws.getIndDettTitDiRat().setImpTfr(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpTfr(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTfrStrc() {
        return dettTitDiRat.getDtrImpTfrStrc().getDtrImpTfrStrc();
    }

    @Override
    public void setImpTfrStrc(AfDecimal impTfrStrc) {
        this.dettTitDiRat.getDtrImpTfrStrc().setDtrImpTfrStrc(impTfrStrc.copy());
    }

    @Override
    public AfDecimal getImpTfrStrcObj() {
        if (ws.getIndDettTitDiRat().getImpTfrStrc() >= 0) {
            return getImpTfrStrc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTfrStrcObj(AfDecimal impTfrStrcObj) {
        if (impTfrStrcObj != null) {
            setImpTfrStrc(new AfDecimal(impTfrStrcObj, 15, 3));
            ws.getIndDettTitDiRat().setImpTfrStrc(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpTfrStrc(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpTrasfe() {
        return dettTitDiRat.getDtrImpTrasfe().getDtrImpTrasfe();
    }

    @Override
    public void setImpTrasfe(AfDecimal impTrasfe) {
        this.dettTitDiRat.getDtrImpTrasfe().setDtrImpTrasfe(impTrasfe.copy());
    }

    @Override
    public AfDecimal getImpTrasfeObj() {
        if (ws.getIndDettTitDiRat().getImpTrasfe() >= 0) {
            return getImpTrasfe();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpTrasfeObj(AfDecimal impTrasfeObj) {
        if (impTrasfeObj != null) {
            setImpTrasfe(new AfDecimal(impTrasfeObj, 15, 3));
            ws.getIndDettTitDiRat().setImpTrasfe(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpTrasfe(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpVolo() {
        return dettTitDiRat.getDtrImpVolo().getDtrImpVolo();
    }

    @Override
    public void setImpVolo(AfDecimal impVolo) {
        this.dettTitDiRat.getDtrImpVolo().setDtrImpVolo(impVolo.copy());
    }

    @Override
    public AfDecimal getImpVoloObj() {
        if (ws.getIndDettTitDiRat().getImpVolo() >= 0) {
            return getImpVolo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpVoloObj(AfDecimal impVoloObj) {
        if (impVoloObj != null) {
            setImpVolo(new AfDecimal(impVoloObj, 15, 3));
            ws.getIndDettTitDiRat().setImpVolo(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setImpVolo(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrFraz() {
        return dettTitDiRat.getDtrIntrFraz().getDtrIntrFraz();
    }

    @Override
    public void setIntrFraz(AfDecimal intrFraz) {
        this.dettTitDiRat.getDtrIntrFraz().setDtrIntrFraz(intrFraz.copy());
    }

    @Override
    public AfDecimal getIntrFrazObj() {
        if (ws.getIndDettTitDiRat().getIntrFraz() >= 0) {
            return getIntrFraz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrFrazObj(AfDecimal intrFrazObj) {
        if (intrFrazObj != null) {
            setIntrFraz(new AfDecimal(intrFrazObj, 15, 3));
            ws.getIndDettTitDiRat().setIntrFraz(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIntrFraz(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrMora() {
        return dettTitDiRat.getDtrIntrMora().getDtrIntrMora();
    }

    @Override
    public void setIntrMora(AfDecimal intrMora) {
        this.dettTitDiRat.getDtrIntrMora().setDtrIntrMora(intrMora.copy());
    }

    @Override
    public AfDecimal getIntrMoraObj() {
        if (ws.getIndDettTitDiRat().getIntrMora() >= 0) {
            return getIntrMora();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrMoraObj(AfDecimal intrMoraObj) {
        if (intrMoraObj != null) {
            setIntrMora(new AfDecimal(intrMoraObj, 15, 3));
            ws.getIndDettTitDiRat().setIntrMora(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIntrMora(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrRetdt() {
        return dettTitDiRat.getDtrIntrRetdt().getDtrIntrRetdt();
    }

    @Override
    public void setIntrRetdt(AfDecimal intrRetdt) {
        this.dettTitDiRat.getDtrIntrRetdt().setDtrIntrRetdt(intrRetdt.copy());
    }

    @Override
    public AfDecimal getIntrRetdtObj() {
        if (ws.getIndDettTitDiRat().getIntrRetdt() >= 0) {
            return getIntrRetdt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrRetdtObj(AfDecimal intrRetdtObj) {
        if (intrRetdtObj != null) {
            setIntrRetdt(new AfDecimal(intrRetdtObj, 15, 3));
            ws.getIndDettTitDiRat().setIntrRetdt(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIntrRetdt(((short)-1));
        }
    }

    @Override
    public AfDecimal getIntrRiat() {
        return dettTitDiRat.getDtrIntrRiat().getDtrIntrRiat();
    }

    @Override
    public void setIntrRiat(AfDecimal intrRiat) {
        this.dettTitDiRat.getDtrIntrRiat().setDtrIntrRiat(intrRiat.copy());
    }

    @Override
    public AfDecimal getIntrRiatObj() {
        if (ws.getIndDettTitDiRat().getIntrRiat() >= 0) {
            return getIntrRiat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIntrRiatObj(AfDecimal intrRiatObj) {
        if (intrRiatObj != null) {
            setIntrRiat(new AfDecimal(intrRiatObj, 15, 3));
            ws.getIndDettTitDiRat().setIntrRiat(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setIntrRiat(((short)-1));
        }
    }

    @Override
    public AfDecimal getManfeeAntic() {
        return dettTitDiRat.getDtrManfeeAntic().getDtrManfeeAntic();
    }

    @Override
    public void setManfeeAntic(AfDecimal manfeeAntic) {
        this.dettTitDiRat.getDtrManfeeAntic().setDtrManfeeAntic(manfeeAntic.copy());
    }

    @Override
    public AfDecimal getManfeeAnticObj() {
        if (ws.getIndDettTitDiRat().getManfeeAntic() >= 0) {
            return getManfeeAntic();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeAnticObj(AfDecimal manfeeAnticObj) {
        if (manfeeAnticObj != null) {
            setManfeeAntic(new AfDecimal(manfeeAnticObj, 15, 3));
            ws.getIndDettTitDiRat().setManfeeAntic(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setManfeeAntic(((short)-1));
        }
    }

    @Override
    public String getManfeeRec() {
        return dettTitDiRat.getDtrManfeeRec();
    }

    @Override
    public void setManfeeRec(String manfeeRec) {
        this.dettTitDiRat.setDtrManfeeRec(manfeeRec);
    }

    @Override
    public String getManfeeRecObj() {
        if (ws.getIndDettTitDiRat().getManfeeRec() >= 0) {
            return getManfeeRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeRecObj(String manfeeRecObj) {
        if (manfeeRecObj != null) {
            setManfeeRec(manfeeRecObj);
            ws.getIndDettTitDiRat().setManfeeRec(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setManfeeRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getManfeeRicor() {
        return dettTitDiRat.getDtrManfeeRicor().getDtrManfeeRicor();
    }

    @Override
    public void setManfeeRicor(AfDecimal manfeeRicor) {
        this.dettTitDiRat.getDtrManfeeRicor().setDtrManfeeRicor(manfeeRicor.copy());
    }

    @Override
    public AfDecimal getManfeeRicorObj() {
        if (ws.getIndDettTitDiRat().getManfeeRicor() >= 0) {
            return getManfeeRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setManfeeRicorObj(AfDecimal manfeeRicorObj) {
        if (manfeeRicorObj != null) {
            setManfeeRicor(new AfDecimal(manfeeRicorObj, 15, 3));
            ws.getIndDettTitDiRat().setManfeeRicor(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setManfeeRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreNet() {
        return dettTitDiRat.getDtrPreNet().getDtrPreNet();
    }

    @Override
    public void setPreNet(AfDecimal preNet) {
        this.dettTitDiRat.getDtrPreNet().setDtrPreNet(preNet.copy());
    }

    @Override
    public AfDecimal getPreNetObj() {
        if (ws.getIndDettTitDiRat().getPreNet() >= 0) {
            return getPreNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreNetObj(AfDecimal preNetObj) {
        if (preNetObj != null) {
            setPreNet(new AfDecimal(preNetObj, 15, 3));
            ws.getIndDettTitDiRat().setPreNet(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setPreNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrePpIas() {
        return dettTitDiRat.getDtrPrePpIas().getDtrPrePpIas();
    }

    @Override
    public void setPrePpIas(AfDecimal prePpIas) {
        this.dettTitDiRat.getDtrPrePpIas().setDtrPrePpIas(prePpIas.copy());
    }

    @Override
    public AfDecimal getPrePpIasObj() {
        if (ws.getIndDettTitDiRat().getPrePpIas() >= 0) {
            return getPrePpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrePpIasObj(AfDecimal prePpIasObj) {
        if (prePpIasObj != null) {
            setPrePpIas(new AfDecimal(prePpIasObj, 15, 3));
            ws.getIndDettTitDiRat().setPrePpIas(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setPrePpIas(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreSoloRsh() {
        return dettTitDiRat.getDtrPreSoloRsh().getDtrPreSoloRsh();
    }

    @Override
    public void setPreSoloRsh(AfDecimal preSoloRsh) {
        this.dettTitDiRat.getDtrPreSoloRsh().setDtrPreSoloRsh(preSoloRsh.copy());
    }

    @Override
    public AfDecimal getPreSoloRshObj() {
        if (ws.getIndDettTitDiRat().getPreSoloRsh() >= 0) {
            return getPreSoloRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreSoloRshObj(AfDecimal preSoloRshObj) {
        if (preSoloRshObj != null) {
            setPreSoloRsh(new AfDecimal(preSoloRshObj, 15, 3));
            ws.getIndDettTitDiRat().setPreSoloRsh(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setPreSoloRsh(((short)-1));
        }
    }

    @Override
    public AfDecimal getPreTot() {
        return dettTitDiRat.getDtrPreTot().getDtrPreTot();
    }

    @Override
    public void setPreTot(AfDecimal preTot) {
        this.dettTitDiRat.getDtrPreTot().setDtrPreTot(preTot.copy());
    }

    @Override
    public AfDecimal getPreTotObj() {
        if (ws.getIndDettTitDiRat().getPreTot() >= 0) {
            return getPreTot();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPreTotObj(AfDecimal preTotObj) {
        if (preTotObj != null) {
            setPreTot(new AfDecimal(preTotObj, 15, 3));
            ws.getIndDettTitDiRat().setPreTot(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setPreTot(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvAcq1aa() {
        return dettTitDiRat.getDtrProvAcq1aa().getDtrProvAcq1aa();
    }

    @Override
    public void setProvAcq1aa(AfDecimal provAcq1aa) {
        this.dettTitDiRat.getDtrProvAcq1aa().setDtrProvAcq1aa(provAcq1aa.copy());
    }

    @Override
    public AfDecimal getProvAcq1aaObj() {
        if (ws.getIndDettTitDiRat().getProvAcq1aa() >= 0) {
            return getProvAcq1aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvAcq1aaObj(AfDecimal provAcq1aaObj) {
        if (provAcq1aaObj != null) {
            setProvAcq1aa(new AfDecimal(provAcq1aaObj, 15, 3));
            ws.getIndDettTitDiRat().setProvAcq1aa(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvAcq1aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvAcq2aa() {
        return dettTitDiRat.getDtrProvAcq2aa().getDtrProvAcq2aa();
    }

    @Override
    public void setProvAcq2aa(AfDecimal provAcq2aa) {
        this.dettTitDiRat.getDtrProvAcq2aa().setDtrProvAcq2aa(provAcq2aa.copy());
    }

    @Override
    public AfDecimal getProvAcq2aaObj() {
        if (ws.getIndDettTitDiRat().getProvAcq2aa() >= 0) {
            return getProvAcq2aa();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvAcq2aaObj(AfDecimal provAcq2aaObj) {
        if (provAcq2aaObj != null) {
            setProvAcq2aa(new AfDecimal(provAcq2aaObj, 15, 3));
            ws.getIndDettTitDiRat().setProvAcq2aa(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvAcq2aa(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvDaRec() {
        return dettTitDiRat.getDtrProvDaRec().getDtrProvDaRec();
    }

    @Override
    public void setProvDaRec(AfDecimal provDaRec) {
        this.dettTitDiRat.getDtrProvDaRec().setDtrProvDaRec(provDaRec.copy());
    }

    @Override
    public AfDecimal getProvDaRecObj() {
        if (ws.getIndDettTitDiRat().getProvDaRec() >= 0) {
            return getProvDaRec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvDaRecObj(AfDecimal provDaRecObj) {
        if (provDaRecObj != null) {
            setProvDaRec(new AfDecimal(provDaRecObj, 15, 3));
            ws.getIndDettTitDiRat().setProvDaRec(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvDaRec(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvInc() {
        return dettTitDiRat.getDtrProvInc().getDtrProvInc();
    }

    @Override
    public void setProvInc(AfDecimal provInc) {
        this.dettTitDiRat.getDtrProvInc().setDtrProvInc(provInc.copy());
    }

    @Override
    public AfDecimal getProvIncObj() {
        if (ws.getIndDettTitDiRat().getProvInc() >= 0) {
            return getProvInc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvIncObj(AfDecimal provIncObj) {
        if (provIncObj != null) {
            setProvInc(new AfDecimal(provIncObj, 15, 3));
            ws.getIndDettTitDiRat().setProvInc(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvInc(((short)-1));
        }
    }

    @Override
    public AfDecimal getProvRicor() {
        return dettTitDiRat.getDtrProvRicor().getDtrProvRicor();
    }

    @Override
    public void setProvRicor(AfDecimal provRicor) {
        this.dettTitDiRat.getDtrProvRicor().setDtrProvRicor(provRicor.copy());
    }

    @Override
    public AfDecimal getProvRicorObj() {
        if (ws.getIndDettTitDiRat().getProvRicor() >= 0) {
            return getProvRicor();
        }
        else {
            return null;
        }
    }

    @Override
    public void setProvRicorObj(AfDecimal provRicorObj) {
        if (provRicorObj != null) {
            setProvRicor(new AfDecimal(provRicorObj, 15, 3));
            ws.getIndDettTitDiRat().setProvRicor(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setProvRicor(((short)-1));
        }
    }

    @Override
    public AfDecimal getRemunAss() {
        return dettTitDiRat.getDtrRemunAss().getDtrRemunAss();
    }

    @Override
    public void setRemunAss(AfDecimal remunAss) {
        this.dettTitDiRat.getDtrRemunAss().setDtrRemunAss(remunAss.copy());
    }

    @Override
    public AfDecimal getRemunAssObj() {
        if (ws.getIndDettTitDiRat().getRemunAss() >= 0) {
            return getRemunAss();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRemunAssObj(AfDecimal remunAssObj) {
        if (remunAssObj != null) {
            setRemunAss(new AfDecimal(remunAssObj, 15, 3));
            ws.getIndDettTitDiRat().setRemunAss(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setRemunAss(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprAlt() {
        return dettTitDiRat.getDtrSoprAlt().getDtrSoprAlt();
    }

    @Override
    public void setSoprAlt(AfDecimal soprAlt) {
        this.dettTitDiRat.getDtrSoprAlt().setDtrSoprAlt(soprAlt.copy());
    }

    @Override
    public AfDecimal getSoprAltObj() {
        if (ws.getIndDettTitDiRat().getSoprAlt() >= 0) {
            return getSoprAlt();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprAltObj(AfDecimal soprAltObj) {
        if (soprAltObj != null) {
            setSoprAlt(new AfDecimal(soprAltObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprAlt(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprAlt(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprProf() {
        return dettTitDiRat.getDtrSoprProf().getDtrSoprProf();
    }

    @Override
    public void setSoprProf(AfDecimal soprProf) {
        this.dettTitDiRat.getDtrSoprProf().setDtrSoprProf(soprProf.copy());
    }

    @Override
    public AfDecimal getSoprProfObj() {
        if (ws.getIndDettTitDiRat().getSoprProf() >= 0) {
            return getSoprProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprProfObj(AfDecimal soprProfObj) {
        if (soprProfObj != null) {
            setSoprProf(new AfDecimal(soprProfObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprProf(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprProf(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprSan() {
        return dettTitDiRat.getDtrSoprSan().getDtrSoprSan();
    }

    @Override
    public void setSoprSan(AfDecimal soprSan) {
        this.dettTitDiRat.getDtrSoprSan().setDtrSoprSan(soprSan.copy());
    }

    @Override
    public AfDecimal getSoprSanObj() {
        if (ws.getIndDettTitDiRat().getSoprSan() >= 0) {
            return getSoprSan();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprSanObj(AfDecimal soprSanObj) {
        if (soprSanObj != null) {
            setSoprSan(new AfDecimal(soprSanObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprSan(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprSan(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprSpo() {
        return dettTitDiRat.getDtrSoprSpo().getDtrSoprSpo();
    }

    @Override
    public void setSoprSpo(AfDecimal soprSpo) {
        this.dettTitDiRat.getDtrSoprSpo().setDtrSoprSpo(soprSpo.copy());
    }

    @Override
    public AfDecimal getSoprSpoObj() {
        if (ws.getIndDettTitDiRat().getSoprSpo() >= 0) {
            return getSoprSpo();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprSpoObj(AfDecimal soprSpoObj) {
        if (soprSpoObj != null) {
            setSoprSpo(new AfDecimal(soprSpoObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprSpo(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprSpo(((short)-1));
        }
    }

    @Override
    public AfDecimal getSoprTec() {
        return dettTitDiRat.getDtrSoprTec().getDtrSoprTec();
    }

    @Override
    public void setSoprTec(AfDecimal soprTec) {
        this.dettTitDiRat.getDtrSoprTec().setDtrSoprTec(soprTec.copy());
    }

    @Override
    public AfDecimal getSoprTecObj() {
        if (ws.getIndDettTitDiRat().getSoprTec() >= 0) {
            return getSoprTec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSoprTecObj(AfDecimal soprTecObj) {
        if (soprTecObj != null) {
            setSoprTec(new AfDecimal(soprTecObj, 15, 3));
            ws.getIndDettTitDiRat().setSoprTec(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSoprTec(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpeAge() {
        return dettTitDiRat.getDtrSpeAge().getDtrSpeAge();
    }

    @Override
    public void setSpeAge(AfDecimal speAge) {
        this.dettTitDiRat.getDtrSpeAge().setDtrSpeAge(speAge.copy());
    }

    @Override
    public AfDecimal getSpeAgeObj() {
        if (ws.getIndDettTitDiRat().getSpeAge() >= 0) {
            return getSpeAge();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeAgeObj(AfDecimal speAgeObj) {
        if (speAgeObj != null) {
            setSpeAge(new AfDecimal(speAgeObj, 15, 3));
            ws.getIndDettTitDiRat().setSpeAge(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSpeAge(((short)-1));
        }
    }

    @Override
    public AfDecimal getSpeMed() {
        return dettTitDiRat.getDtrSpeMed().getDtrSpeMed();
    }

    @Override
    public void setSpeMed(AfDecimal speMed) {
        this.dettTitDiRat.getDtrSpeMed().setDtrSpeMed(speMed.copy());
    }

    @Override
    public AfDecimal getSpeMedObj() {
        if (ws.getIndDettTitDiRat().getSpeMed() >= 0) {
            return getSpeMed();
        }
        else {
            return null;
        }
    }

    @Override
    public void setSpeMedObj(AfDecimal speMedObj) {
        if (speMedObj != null) {
            setSpeMed(new AfDecimal(speMedObj, 15, 3));
            ws.getIndDettTitDiRat().setSpeMed(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setSpeMed(((short)-1));
        }
    }

    @Override
    public AfDecimal getTax() {
        return dettTitDiRat.getDtrTax().getDtrTax();
    }

    @Override
    public void setTax(AfDecimal tax) {
        this.dettTitDiRat.getDtrTax().setDtrTax(tax.copy());
    }

    @Override
    public AfDecimal getTaxObj() {
        if (ws.getIndDettTitDiRat().getTax() >= 0) {
            return getTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTaxObj(AfDecimal taxObj) {
        if (taxObj != null) {
            setTax(new AfDecimal(taxObj, 15, 3));
            ws.getIndDettTitDiRat().setTax(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setTax(((short)-1));
        }
    }

    @Override
    public AfDecimal getTotIntrPrest() {
        return dettTitDiRat.getDtrTotIntrPrest().getDtrTotIntrPrest();
    }

    @Override
    public void setTotIntrPrest(AfDecimal totIntrPrest) {
        this.dettTitDiRat.getDtrTotIntrPrest().setDtrTotIntrPrest(totIntrPrest.copy());
    }

    @Override
    public AfDecimal getTotIntrPrestObj() {
        if (ws.getIndDettTitDiRat().getTotIntrPrest() >= 0) {
            return getTotIntrPrest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTotIntrPrestObj(AfDecimal totIntrPrestObj) {
        if (totIntrPrestObj != null) {
            setTotIntrPrest(new AfDecimal(totIntrPrestObj, 15, 3));
            ws.getIndDettTitDiRat().setTotIntrPrest(((short)0));
        }
        else {
            ws.getIndDettTitDiRat().setTotIntrPrest(((short)-1));
        }
    }

    @Override
    public String getTpRgmFisc() {
        return dettTitDiRat.getDtrTpRgmFisc();
    }

    @Override
    public void setTpRgmFisc(String tpRgmFisc) {
        this.dettTitDiRat.setDtrTpRgmFisc(tpRgmFisc);
    }

    @Override
    public String getTpStatTit() {
        return dettTitDiRat.getDtrTpStatTit();
    }

    @Override
    public void setTpStatTit(String tpStatTit) {
        this.dettTitDiRat.setDtrTpStatTit(tpStatTit);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
