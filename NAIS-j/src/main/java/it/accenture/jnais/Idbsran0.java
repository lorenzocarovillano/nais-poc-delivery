package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.RappAnaDao;
import it.accenture.jnais.commons.data.to.IRappAna;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idbsran0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.RappAnaLdbs1240;
import it.accenture.jnais.ws.redefines.RanDtDeces;
import it.accenture.jnais.ws.redefines.RanDtDeliberaCda;
import it.accenture.jnais.ws.redefines.RanDtNasc;
import it.accenture.jnais.ws.redefines.RanIdMoviChiu;
import it.accenture.jnais.ws.redefines.RanIdRappAnaCollg;
import it.accenture.jnais.ws.redefines.RanPcNelRapp;

/**Original name: IDBSRAN0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  24 GEN 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsran0 extends Program implements IRappAna {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private RappAnaDao rappAnaDao = new RappAnaDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsran0Data ws = new Idbsran0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: RAPP-ANA
    private RappAnaLdbs1240 rappAna;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSRAN0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, RappAnaLdbs1240 rappAna) {
        this.idsv0003 = idsv0003;
        this.rappAna = rappAna;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsran0 getInstance() {
        return ((Idbsran0)Programs.getInstance(Idbsran0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSRAN0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSRAN0");
        // COB_CODE: MOVE 'RAPP_ANA' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("RAPP_ANA");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,TP_PERS
        //                ,SEX
        //                ,DT_NASC
        //                ,FL_ESTAS
        //                ,INDIR_1
        //                ,INDIR_2
        //                ,INDIR_3
        //                ,TP_UTLZ_INDIR_1
        //                ,TP_UTLZ_INDIR_2
        //                ,TP_UTLZ_INDIR_3
        //                ,ESTR_CNT_CORR_ACCR
        //                ,ESTR_CNT_CORR_ADD
        //                ,ESTR_DOCTO
        //                ,PC_NEL_RAPP
        //                ,TP_MEZ_PAG_ADD
        //                ,TP_MEZ_PAG_ACCR
        //                ,COD_MATR
        //                ,TP_ADEGZ
        //                ,FL_TST_RSH
        //                ,COD_AZ
        //                ,IND_PRINC
        //                ,DT_DELIBERA_CDA
        //                ,DLG_AL_RISC
        //                ,LEGALE_RAPPR_PRINC
        //                ,TP_LEGALE_RAPPR
        //                ,TP_IND_PRINC
        //                ,TP_STAT_RID
        //                ,NOME_INT_RID
        //                ,COGN_INT_RID
        //                ,COGN_INT_TRATT
        //                ,NOME_INT_TRATT
        //                ,CF_INT_RID
        //                ,FL_COINC_DIP_CNTR
        //                ,FL_COINC_INT_CNTR
        //                ,DT_DECES
        //                ,FL_FUMATORE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_LAV_DIP
        //                ,TP_VARZ_PAGAT
        //                ,COD_RID
        //                ,TP_CAUS_RID
        //                ,IND_MASSA_CORP
        //                ,CAT_RSH_PROF
        //             INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //             FROM RAPP_ANA
        //             WHERE     DS_RIGA = :RAN-DS-RIGA
        //           END-EXEC.
        rappAnaDao.selectByRanDsRiga(rappAna.getRanDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO RAPP_ANA
            //                  (
            //                     ID_RAPP_ANA
            //                    ,ID_RAPP_ANA_COLLG
            //                    ,ID_OGG
            //                    ,TP_OGG
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,COD_SOGG
            //                    ,TP_RAPP_ANA
            //                    ,TP_PERS
            //                    ,SEX
            //                    ,DT_NASC
            //                    ,FL_ESTAS
            //                    ,INDIR_1
            //                    ,INDIR_2
            //                    ,INDIR_3
            //                    ,TP_UTLZ_INDIR_1
            //                    ,TP_UTLZ_INDIR_2
            //                    ,TP_UTLZ_INDIR_3
            //                    ,ESTR_CNT_CORR_ACCR
            //                    ,ESTR_CNT_CORR_ADD
            //                    ,ESTR_DOCTO
            //                    ,PC_NEL_RAPP
            //                    ,TP_MEZ_PAG_ADD
            //                    ,TP_MEZ_PAG_ACCR
            //                    ,COD_MATR
            //                    ,TP_ADEGZ
            //                    ,FL_TST_RSH
            //                    ,COD_AZ
            //                    ,IND_PRINC
            //                    ,DT_DELIBERA_CDA
            //                    ,DLG_AL_RISC
            //                    ,LEGALE_RAPPR_PRINC
            //                    ,TP_LEGALE_RAPPR
            //                    ,TP_IND_PRINC
            //                    ,TP_STAT_RID
            //                    ,NOME_INT_RID
            //                    ,COGN_INT_RID
            //                    ,COGN_INT_TRATT
            //                    ,NOME_INT_TRATT
            //                    ,CF_INT_RID
            //                    ,FL_COINC_DIP_CNTR
            //                    ,FL_COINC_INT_CNTR
            //                    ,DT_DECES
            //                    ,FL_FUMATORE
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,FL_LAV_DIP
            //                    ,TP_VARZ_PAGAT
            //                    ,COD_RID
            //                    ,TP_CAUS_RID
            //                    ,IND_MASSA_CORP
            //                    ,CAT_RSH_PROF
            //                  )
            //              VALUES
            //                  (
            //                    :RAN-ID-RAPP-ANA
            //                    ,:RAN-ID-RAPP-ANA-COLLG
            //                     :IND-RAN-ID-RAPP-ANA-COLLG
            //                    ,:RAN-ID-OGG
            //                    ,:RAN-TP-OGG
            //                    ,:RAN-ID-MOVI-CRZ
            //                    ,:RAN-ID-MOVI-CHIU
            //                     :IND-RAN-ID-MOVI-CHIU
            //                    ,:RAN-DT-INI-EFF-DB
            //                    ,:RAN-DT-END-EFF-DB
            //                    ,:RAN-COD-COMP-ANIA
            //                    ,:RAN-COD-SOGG
            //                     :IND-RAN-COD-SOGG
            //                    ,:RAN-TP-RAPP-ANA
            //                    ,:RAN-TP-PERS
            //                     :IND-RAN-TP-PERS
            //                    ,:RAN-SEX
            //                     :IND-RAN-SEX
            //                    ,:RAN-DT-NASC-DB
            //                     :IND-RAN-DT-NASC
            //                    ,:RAN-FL-ESTAS
            //                     :IND-RAN-FL-ESTAS
            //                    ,:RAN-INDIR-1
            //                     :IND-RAN-INDIR-1
            //                    ,:RAN-INDIR-2
            //                     :IND-RAN-INDIR-2
            //                    ,:RAN-INDIR-3
            //                     :IND-RAN-INDIR-3
            //                    ,:RAN-TP-UTLZ-INDIR-1
            //                     :IND-RAN-TP-UTLZ-INDIR-1
            //                    ,:RAN-TP-UTLZ-INDIR-2
            //                     :IND-RAN-TP-UTLZ-INDIR-2
            //                    ,:RAN-TP-UTLZ-INDIR-3
            //                     :IND-RAN-TP-UTLZ-INDIR-3
            //                    ,:RAN-ESTR-CNT-CORR-ACCR
            //                     :IND-RAN-ESTR-CNT-CORR-ACCR
            //                    ,:RAN-ESTR-CNT-CORR-ADD
            //                     :IND-RAN-ESTR-CNT-CORR-ADD
            //                    ,:RAN-ESTR-DOCTO
            //                     :IND-RAN-ESTR-DOCTO
            //                    ,:RAN-PC-NEL-RAPP
            //                     :IND-RAN-PC-NEL-RAPP
            //                    ,:RAN-TP-MEZ-PAG-ADD
            //                     :IND-RAN-TP-MEZ-PAG-ADD
            //                    ,:RAN-TP-MEZ-PAG-ACCR
            //                     :IND-RAN-TP-MEZ-PAG-ACCR
            //                    ,:RAN-COD-MATR
            //                     :IND-RAN-COD-MATR
            //                    ,:RAN-TP-ADEGZ
            //                     :IND-RAN-TP-ADEGZ
            //                    ,:RAN-FL-TST-RSH
            //                     :IND-RAN-FL-TST-RSH
            //                    ,:RAN-COD-AZ
            //                     :IND-RAN-COD-AZ
            //                    ,:RAN-IND-PRINC
            //                     :IND-RAN-IND-PRINC
            //                    ,:RAN-DT-DELIBERA-CDA-DB
            //                     :IND-RAN-DT-DELIBERA-CDA
            //                    ,:RAN-DLG-AL-RISC
            //                     :IND-RAN-DLG-AL-RISC
            //                    ,:RAN-LEGALE-RAPPR-PRINC
            //                     :IND-RAN-LEGALE-RAPPR-PRINC
            //                    ,:RAN-TP-LEGALE-RAPPR
            //                     :IND-RAN-TP-LEGALE-RAPPR
            //                    ,:RAN-TP-IND-PRINC
            //                     :IND-RAN-TP-IND-PRINC
            //                    ,:RAN-TP-STAT-RID
            //                     :IND-RAN-TP-STAT-RID
            //                    ,:RAN-NOME-INT-RID-VCHAR
            //                     :IND-RAN-NOME-INT-RID
            //                    ,:RAN-COGN-INT-RID-VCHAR
            //                     :IND-RAN-COGN-INT-RID
            //                    ,:RAN-COGN-INT-TRATT-VCHAR
            //                     :IND-RAN-COGN-INT-TRATT
            //                    ,:RAN-NOME-INT-TRATT-VCHAR
            //                     :IND-RAN-NOME-INT-TRATT
            //                    ,:RAN-CF-INT-RID
            //                     :IND-RAN-CF-INT-RID
            //                    ,:RAN-FL-COINC-DIP-CNTR
            //                     :IND-RAN-FL-COINC-DIP-CNTR
            //                    ,:RAN-FL-COINC-INT-CNTR
            //                     :IND-RAN-FL-COINC-INT-CNTR
            //                    ,:RAN-DT-DECES-DB
            //                     :IND-RAN-DT-DECES
            //                    ,:RAN-FL-FUMATORE
            //                     :IND-RAN-FL-FUMATORE
            //                    ,:RAN-DS-RIGA
            //                    ,:RAN-DS-OPER-SQL
            //                    ,:RAN-DS-VER
            //                    ,:RAN-DS-TS-INI-CPTZ
            //                    ,:RAN-DS-TS-END-CPTZ
            //                    ,:RAN-DS-UTENTE
            //                    ,:RAN-DS-STATO-ELAB
            //                    ,:RAN-FL-LAV-DIP
            //                     :IND-RAN-FL-LAV-DIP
            //                    ,:RAN-TP-VARZ-PAGAT
            //                     :IND-RAN-TP-VARZ-PAGAT
            //                    ,:RAN-COD-RID
            //                     :IND-RAN-COD-RID
            //                    ,:RAN-TP-CAUS-RID
            //                     :IND-RAN-TP-CAUS-RID
            //                    ,:RAN-IND-MASSA-CORP
            //                     :IND-RAN-IND-MASSA-CORP
            //                    ,:RAN-CAT-RSH-PROF
            //                     :IND-RAN-CAT-RSH-PROF
            //                  )
            //           END-EXEC
            rappAnaDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE RAPP_ANA SET
        //                   ID_RAPP_ANA            =
        //                :RAN-ID-RAPP-ANA
        //                  ,ID_RAPP_ANA_COLLG      =
        //                :RAN-ID-RAPP-ANA-COLLG
        //                                       :IND-RAN-ID-RAPP-ANA-COLLG
        //                  ,ID_OGG                 =
        //                :RAN-ID-OGG
        //                  ,TP_OGG                 =
        //                :RAN-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :RAN-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :RAN-ID-MOVI-CHIU
        //                                       :IND-RAN-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :RAN-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :RAN-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :RAN-COD-COMP-ANIA
        //                  ,COD_SOGG               =
        //                :RAN-COD-SOGG
        //                                       :IND-RAN-COD-SOGG
        //                  ,TP_RAPP_ANA            =
        //                :RAN-TP-RAPP-ANA
        //                  ,TP_PERS                =
        //                :RAN-TP-PERS
        //                                       :IND-RAN-TP-PERS
        //                  ,SEX                    =
        //                :RAN-SEX
        //                                       :IND-RAN-SEX
        //                  ,DT_NASC                =
        //           :RAN-DT-NASC-DB
        //                                       :IND-RAN-DT-NASC
        //                  ,FL_ESTAS               =
        //                :RAN-FL-ESTAS
        //                                       :IND-RAN-FL-ESTAS
        //                  ,INDIR_1                =
        //                :RAN-INDIR-1
        //                                       :IND-RAN-INDIR-1
        //                  ,INDIR_2                =
        //                :RAN-INDIR-2
        //                                       :IND-RAN-INDIR-2
        //                  ,INDIR_3                =
        //                :RAN-INDIR-3
        //                                       :IND-RAN-INDIR-3
        //                  ,TP_UTLZ_INDIR_1        =
        //                :RAN-TP-UTLZ-INDIR-1
        //                                       :IND-RAN-TP-UTLZ-INDIR-1
        //                  ,TP_UTLZ_INDIR_2        =
        //                :RAN-TP-UTLZ-INDIR-2
        //                                       :IND-RAN-TP-UTLZ-INDIR-2
        //                  ,TP_UTLZ_INDIR_3        =
        //                :RAN-TP-UTLZ-INDIR-3
        //                                       :IND-RAN-TP-UTLZ-INDIR-3
        //                  ,ESTR_CNT_CORR_ACCR     =
        //                :RAN-ESTR-CNT-CORR-ACCR
        //                                       :IND-RAN-ESTR-CNT-CORR-ACCR
        //                  ,ESTR_CNT_CORR_ADD      =
        //                :RAN-ESTR-CNT-CORR-ADD
        //                                       :IND-RAN-ESTR-CNT-CORR-ADD
        //                  ,ESTR_DOCTO             =
        //                :RAN-ESTR-DOCTO
        //                                       :IND-RAN-ESTR-DOCTO
        //                  ,PC_NEL_RAPP            =
        //                :RAN-PC-NEL-RAPP
        //                                       :IND-RAN-PC-NEL-RAPP
        //                  ,TP_MEZ_PAG_ADD         =
        //                :RAN-TP-MEZ-PAG-ADD
        //                                       :IND-RAN-TP-MEZ-PAG-ADD
        //                  ,TP_MEZ_PAG_ACCR        =
        //                :RAN-TP-MEZ-PAG-ACCR
        //                                       :IND-RAN-TP-MEZ-PAG-ACCR
        //                  ,COD_MATR               =
        //                :RAN-COD-MATR
        //                                       :IND-RAN-COD-MATR
        //                  ,TP_ADEGZ               =
        //                :RAN-TP-ADEGZ
        //                                       :IND-RAN-TP-ADEGZ
        //                  ,FL_TST_RSH             =
        //                :RAN-FL-TST-RSH
        //                                       :IND-RAN-FL-TST-RSH
        //                  ,COD_AZ                 =
        //                :RAN-COD-AZ
        //                                       :IND-RAN-COD-AZ
        //                  ,IND_PRINC              =
        //                :RAN-IND-PRINC
        //                                       :IND-RAN-IND-PRINC
        //                  ,DT_DELIBERA_CDA        =
        //           :RAN-DT-DELIBERA-CDA-DB
        //                                       :IND-RAN-DT-DELIBERA-CDA
        //                  ,DLG_AL_RISC            =
        //                :RAN-DLG-AL-RISC
        //                                       :IND-RAN-DLG-AL-RISC
        //                  ,LEGALE_RAPPR_PRINC     =
        //                :RAN-LEGALE-RAPPR-PRINC
        //                                       :IND-RAN-LEGALE-RAPPR-PRINC
        //                  ,TP_LEGALE_RAPPR        =
        //                :RAN-TP-LEGALE-RAPPR
        //                                       :IND-RAN-TP-LEGALE-RAPPR
        //                  ,TP_IND_PRINC           =
        //                :RAN-TP-IND-PRINC
        //                                       :IND-RAN-TP-IND-PRINC
        //                  ,TP_STAT_RID            =
        //                :RAN-TP-STAT-RID
        //                                       :IND-RAN-TP-STAT-RID
        //                  ,NOME_INT_RID           =
        //                :RAN-NOME-INT-RID-VCHAR
        //                                       :IND-RAN-NOME-INT-RID
        //                  ,COGN_INT_RID           =
        //                :RAN-COGN-INT-RID-VCHAR
        //                                       :IND-RAN-COGN-INT-RID
        //                  ,COGN_INT_TRATT         =
        //                :RAN-COGN-INT-TRATT-VCHAR
        //                                       :IND-RAN-COGN-INT-TRATT
        //                  ,NOME_INT_TRATT         =
        //                :RAN-NOME-INT-TRATT-VCHAR
        //                                       :IND-RAN-NOME-INT-TRATT
        //                  ,CF_INT_RID             =
        //                :RAN-CF-INT-RID
        //                                       :IND-RAN-CF-INT-RID
        //                  ,FL_COINC_DIP_CNTR      =
        //                :RAN-FL-COINC-DIP-CNTR
        //                                       :IND-RAN-FL-COINC-DIP-CNTR
        //                  ,FL_COINC_INT_CNTR      =
        //                :RAN-FL-COINC-INT-CNTR
        //                                       :IND-RAN-FL-COINC-INT-CNTR
        //                  ,DT_DECES               =
        //           :RAN-DT-DECES-DB
        //                                       :IND-RAN-DT-DECES
        //                  ,FL_FUMATORE            =
        //                :RAN-FL-FUMATORE
        //                                       :IND-RAN-FL-FUMATORE
        //                  ,DS_RIGA                =
        //                :RAN-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :RAN-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :RAN-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :RAN-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :RAN-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :RAN-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :RAN-DS-STATO-ELAB
        //                  ,FL_LAV_DIP             =
        //                :RAN-FL-LAV-DIP
        //                                       :IND-RAN-FL-LAV-DIP
        //                  ,TP_VARZ_PAGAT          =
        //                :RAN-TP-VARZ-PAGAT
        //                                       :IND-RAN-TP-VARZ-PAGAT
        //                  ,COD_RID                =
        //                :RAN-COD-RID
        //                                       :IND-RAN-COD-RID
        //                  ,TP_CAUS_RID            =
        //                :RAN-TP-CAUS-RID
        //                                       :IND-RAN-TP-CAUS-RID
        //                  ,IND_MASSA_CORP         =
        //                :RAN-IND-MASSA-CORP
        //                                       :IND-RAN-IND-MASSA-CORP
        //                  ,CAT_RSH_PROF           =
        //                :RAN-CAT-RSH-PROF
        //                                       :IND-RAN-CAT-RSH-PROF
        //                WHERE     DS_RIGA = :RAN-DS-RIGA
        //           END-EXEC.
        rappAnaDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM RAPP_ANA
        //                WHERE     DS_RIGA = :RAN-DS-RIGA
        //           END-EXEC.
        rappAnaDao.deleteByRanDsRiga(rappAna.getRanDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-RAN CURSOR FOR
        //              SELECT
        //                     ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,TP_PERS
        //                    ,SEX
        //                    ,DT_NASC
        //                    ,FL_ESTAS
        //                    ,INDIR_1
        //                    ,INDIR_2
        //                    ,INDIR_3
        //                    ,TP_UTLZ_INDIR_1
        //                    ,TP_UTLZ_INDIR_2
        //                    ,TP_UTLZ_INDIR_3
        //                    ,ESTR_CNT_CORR_ACCR
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,ESTR_DOCTO
        //                    ,PC_NEL_RAPP
        //                    ,TP_MEZ_PAG_ADD
        //                    ,TP_MEZ_PAG_ACCR
        //                    ,COD_MATR
        //                    ,TP_ADEGZ
        //                    ,FL_TST_RSH
        //                    ,COD_AZ
        //                    ,IND_PRINC
        //                    ,DT_DELIBERA_CDA
        //                    ,DLG_AL_RISC
        //                    ,LEGALE_RAPPR_PRINC
        //                    ,TP_LEGALE_RAPPR
        //                    ,TP_IND_PRINC
        //                    ,TP_STAT_RID
        //                    ,NOME_INT_RID
        //                    ,COGN_INT_RID
        //                    ,COGN_INT_TRATT
        //                    ,NOME_INT_TRATT
        //                    ,CF_INT_RID
        //                    ,FL_COINC_DIP_CNTR
        //                    ,FL_COINC_INT_CNTR
        //                    ,DT_DECES
        //                    ,FL_FUMATORE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_LAV_DIP
        //                    ,TP_VARZ_PAGAT
        //                    ,COD_RID
        //                    ,TP_CAUS_RID
        //                    ,IND_MASSA_CORP
        //                    ,CAT_RSH_PROF
        //              FROM RAPP_ANA
        //              WHERE     ID_RAPP_ANA = :RAN-ID-RAPP-ANA
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,TP_PERS
        //                ,SEX
        //                ,DT_NASC
        //                ,FL_ESTAS
        //                ,INDIR_1
        //                ,INDIR_2
        //                ,INDIR_3
        //                ,TP_UTLZ_INDIR_1
        //                ,TP_UTLZ_INDIR_2
        //                ,TP_UTLZ_INDIR_3
        //                ,ESTR_CNT_CORR_ACCR
        //                ,ESTR_CNT_CORR_ADD
        //                ,ESTR_DOCTO
        //                ,PC_NEL_RAPP
        //                ,TP_MEZ_PAG_ADD
        //                ,TP_MEZ_PAG_ACCR
        //                ,COD_MATR
        //                ,TP_ADEGZ
        //                ,FL_TST_RSH
        //                ,COD_AZ
        //                ,IND_PRINC
        //                ,DT_DELIBERA_CDA
        //                ,DLG_AL_RISC
        //                ,LEGALE_RAPPR_PRINC
        //                ,TP_LEGALE_RAPPR
        //                ,TP_IND_PRINC
        //                ,TP_STAT_RID
        //                ,NOME_INT_RID
        //                ,COGN_INT_RID
        //                ,COGN_INT_TRATT
        //                ,NOME_INT_TRATT
        //                ,CF_INT_RID
        //                ,FL_COINC_DIP_CNTR
        //                ,FL_COINC_INT_CNTR
        //                ,DT_DECES
        //                ,FL_FUMATORE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_LAV_DIP
        //                ,TP_VARZ_PAGAT
        //                ,COD_RID
        //                ,TP_CAUS_RID
        //                ,IND_MASSA_CORP
        //                ,CAT_RSH_PROF
        //             INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //             FROM RAPP_ANA
        //             WHERE     ID_RAPP_ANA = :RAN-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        rappAnaDao.selectRec(rappAna.getRanIdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE RAPP_ANA SET
        //                   ID_RAPP_ANA            =
        //                :RAN-ID-RAPP-ANA
        //                  ,ID_RAPP_ANA_COLLG      =
        //                :RAN-ID-RAPP-ANA-COLLG
        //                                       :IND-RAN-ID-RAPP-ANA-COLLG
        //                  ,ID_OGG                 =
        //                :RAN-ID-OGG
        //                  ,TP_OGG                 =
        //                :RAN-TP-OGG
        //                  ,ID_MOVI_CRZ            =
        //                :RAN-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :RAN-ID-MOVI-CHIU
        //                                       :IND-RAN-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :RAN-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :RAN-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :RAN-COD-COMP-ANIA
        //                  ,COD_SOGG               =
        //                :RAN-COD-SOGG
        //                                       :IND-RAN-COD-SOGG
        //                  ,TP_RAPP_ANA            =
        //                :RAN-TP-RAPP-ANA
        //                  ,TP_PERS                =
        //                :RAN-TP-PERS
        //                                       :IND-RAN-TP-PERS
        //                  ,SEX                    =
        //                :RAN-SEX
        //                                       :IND-RAN-SEX
        //                  ,DT_NASC                =
        //           :RAN-DT-NASC-DB
        //                                       :IND-RAN-DT-NASC
        //                  ,FL_ESTAS               =
        //                :RAN-FL-ESTAS
        //                                       :IND-RAN-FL-ESTAS
        //                  ,INDIR_1                =
        //                :RAN-INDIR-1
        //                                       :IND-RAN-INDIR-1
        //                  ,INDIR_2                =
        //                :RAN-INDIR-2
        //                                       :IND-RAN-INDIR-2
        //                  ,INDIR_3                =
        //                :RAN-INDIR-3
        //                                       :IND-RAN-INDIR-3
        //                  ,TP_UTLZ_INDIR_1        =
        //                :RAN-TP-UTLZ-INDIR-1
        //                                       :IND-RAN-TP-UTLZ-INDIR-1
        //                  ,TP_UTLZ_INDIR_2        =
        //                :RAN-TP-UTLZ-INDIR-2
        //                                       :IND-RAN-TP-UTLZ-INDIR-2
        //                  ,TP_UTLZ_INDIR_3        =
        //                :RAN-TP-UTLZ-INDIR-3
        //                                       :IND-RAN-TP-UTLZ-INDIR-3
        //                  ,ESTR_CNT_CORR_ACCR     =
        //                :RAN-ESTR-CNT-CORR-ACCR
        //                                       :IND-RAN-ESTR-CNT-CORR-ACCR
        //                  ,ESTR_CNT_CORR_ADD      =
        //                :RAN-ESTR-CNT-CORR-ADD
        //                                       :IND-RAN-ESTR-CNT-CORR-ADD
        //                  ,ESTR_DOCTO             =
        //                :RAN-ESTR-DOCTO
        //                                       :IND-RAN-ESTR-DOCTO
        //                  ,PC_NEL_RAPP            =
        //                :RAN-PC-NEL-RAPP
        //                                       :IND-RAN-PC-NEL-RAPP
        //                  ,TP_MEZ_PAG_ADD         =
        //                :RAN-TP-MEZ-PAG-ADD
        //                                       :IND-RAN-TP-MEZ-PAG-ADD
        //                  ,TP_MEZ_PAG_ACCR        =
        //                :RAN-TP-MEZ-PAG-ACCR
        //                                       :IND-RAN-TP-MEZ-PAG-ACCR
        //                  ,COD_MATR               =
        //                :RAN-COD-MATR
        //                                       :IND-RAN-COD-MATR
        //                  ,TP_ADEGZ               =
        //                :RAN-TP-ADEGZ
        //                                       :IND-RAN-TP-ADEGZ
        //                  ,FL_TST_RSH             =
        //                :RAN-FL-TST-RSH
        //                                       :IND-RAN-FL-TST-RSH
        //                  ,COD_AZ                 =
        //                :RAN-COD-AZ
        //                                       :IND-RAN-COD-AZ
        //                  ,IND_PRINC              =
        //                :RAN-IND-PRINC
        //                                       :IND-RAN-IND-PRINC
        //                  ,DT_DELIBERA_CDA        =
        //           :RAN-DT-DELIBERA-CDA-DB
        //                                       :IND-RAN-DT-DELIBERA-CDA
        //                  ,DLG_AL_RISC            =
        //                :RAN-DLG-AL-RISC
        //                                       :IND-RAN-DLG-AL-RISC
        //                  ,LEGALE_RAPPR_PRINC     =
        //                :RAN-LEGALE-RAPPR-PRINC
        //                                       :IND-RAN-LEGALE-RAPPR-PRINC
        //                  ,TP_LEGALE_RAPPR        =
        //                :RAN-TP-LEGALE-RAPPR
        //                                       :IND-RAN-TP-LEGALE-RAPPR
        //                  ,TP_IND_PRINC           =
        //                :RAN-TP-IND-PRINC
        //                                       :IND-RAN-TP-IND-PRINC
        //                  ,TP_STAT_RID            =
        //                :RAN-TP-STAT-RID
        //                                       :IND-RAN-TP-STAT-RID
        //                  ,NOME_INT_RID           =
        //                :RAN-NOME-INT-RID-VCHAR
        //                                       :IND-RAN-NOME-INT-RID
        //                  ,COGN_INT_RID           =
        //                :RAN-COGN-INT-RID-VCHAR
        //                                       :IND-RAN-COGN-INT-RID
        //                  ,COGN_INT_TRATT         =
        //                :RAN-COGN-INT-TRATT-VCHAR
        //                                       :IND-RAN-COGN-INT-TRATT
        //                  ,NOME_INT_TRATT         =
        //                :RAN-NOME-INT-TRATT-VCHAR
        //                                       :IND-RAN-NOME-INT-TRATT
        //                  ,CF_INT_RID             =
        //                :RAN-CF-INT-RID
        //                                       :IND-RAN-CF-INT-RID
        //                  ,FL_COINC_DIP_CNTR      =
        //                :RAN-FL-COINC-DIP-CNTR
        //                                       :IND-RAN-FL-COINC-DIP-CNTR
        //                  ,FL_COINC_INT_CNTR      =
        //                :RAN-FL-COINC-INT-CNTR
        //                                       :IND-RAN-FL-COINC-INT-CNTR
        //                  ,DT_DECES               =
        //           :RAN-DT-DECES-DB
        //                                       :IND-RAN-DT-DECES
        //                  ,FL_FUMATORE            =
        //                :RAN-FL-FUMATORE
        //                                       :IND-RAN-FL-FUMATORE
        //                  ,DS_RIGA                =
        //                :RAN-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :RAN-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :RAN-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :RAN-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :RAN-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :RAN-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :RAN-DS-STATO-ELAB
        //                  ,FL_LAV_DIP             =
        //                :RAN-FL-LAV-DIP
        //                                       :IND-RAN-FL-LAV-DIP
        //                  ,TP_VARZ_PAGAT          =
        //                :RAN-TP-VARZ-PAGAT
        //                                       :IND-RAN-TP-VARZ-PAGAT
        //                  ,COD_RID                =
        //                :RAN-COD-RID
        //                                       :IND-RAN-COD-RID
        //                  ,TP_CAUS_RID            =
        //                :RAN-TP-CAUS-RID
        //                                       :IND-RAN-TP-CAUS-RID
        //                  ,IND_MASSA_CORP         =
        //                :RAN-IND-MASSA-CORP
        //                                       :IND-RAN-IND-MASSA-CORP
        //                  ,CAT_RSH_PROF           =
        //                :RAN-CAT-RSH-PROF
        //                                       :IND-RAN-CAT-RSH-PROF
        //                WHERE     DS_RIGA = :RAN-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        rappAnaDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-RAN
        //           END-EXEC.
        rappAnaDao.openCIdUpdEffRan(rappAna.getRanIdRappAna(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-RAN
        //           END-EXEC.
        rappAnaDao.closeCIdUpdEffRan();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-RAN
        //           INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //           END-EXEC.
        rappAnaDao.fetchCIdUpdEffRan(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-EFF-RAN CURSOR FOR
        //              SELECT
        //                     ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,TP_PERS
        //                    ,SEX
        //                    ,DT_NASC
        //                    ,FL_ESTAS
        //                    ,INDIR_1
        //                    ,INDIR_2
        //                    ,INDIR_3
        //                    ,TP_UTLZ_INDIR_1
        //                    ,TP_UTLZ_INDIR_2
        //                    ,TP_UTLZ_INDIR_3
        //                    ,ESTR_CNT_CORR_ACCR
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,ESTR_DOCTO
        //                    ,PC_NEL_RAPP
        //                    ,TP_MEZ_PAG_ADD
        //                    ,TP_MEZ_PAG_ACCR
        //                    ,COD_MATR
        //                    ,TP_ADEGZ
        //                    ,FL_TST_RSH
        //                    ,COD_AZ
        //                    ,IND_PRINC
        //                    ,DT_DELIBERA_CDA
        //                    ,DLG_AL_RISC
        //                    ,LEGALE_RAPPR_PRINC
        //                    ,TP_LEGALE_RAPPR
        //                    ,TP_IND_PRINC
        //                    ,TP_STAT_RID
        //                    ,NOME_INT_RID
        //                    ,COGN_INT_RID
        //                    ,COGN_INT_TRATT
        //                    ,NOME_INT_TRATT
        //                    ,CF_INT_RID
        //                    ,FL_COINC_DIP_CNTR
        //                    ,FL_COINC_INT_CNTR
        //                    ,DT_DECES
        //                    ,FL_FUMATORE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_LAV_DIP
        //                    ,TP_VARZ_PAGAT
        //                    ,COD_RID
        //                    ,TP_CAUS_RID
        //                    ,IND_MASSA_CORP
        //                    ,CAT_RSH_PROF
        //              FROM RAPP_ANA
        //              WHERE     ID_OGG = :RAN-ID-OGG
        //                    AND TP_OGG = :RAN-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_RAPP_ANA ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,TP_PERS
        //                ,SEX
        //                ,DT_NASC
        //                ,FL_ESTAS
        //                ,INDIR_1
        //                ,INDIR_2
        //                ,INDIR_3
        //                ,TP_UTLZ_INDIR_1
        //                ,TP_UTLZ_INDIR_2
        //                ,TP_UTLZ_INDIR_3
        //                ,ESTR_CNT_CORR_ACCR
        //                ,ESTR_CNT_CORR_ADD
        //                ,ESTR_DOCTO
        //                ,PC_NEL_RAPP
        //                ,TP_MEZ_PAG_ADD
        //                ,TP_MEZ_PAG_ACCR
        //                ,COD_MATR
        //                ,TP_ADEGZ
        //                ,FL_TST_RSH
        //                ,COD_AZ
        //                ,IND_PRINC
        //                ,DT_DELIBERA_CDA
        //                ,DLG_AL_RISC
        //                ,LEGALE_RAPPR_PRINC
        //                ,TP_LEGALE_RAPPR
        //                ,TP_IND_PRINC
        //                ,TP_STAT_RID
        //                ,NOME_INT_RID
        //                ,COGN_INT_RID
        //                ,COGN_INT_TRATT
        //                ,NOME_INT_TRATT
        //                ,CF_INT_RID
        //                ,FL_COINC_DIP_CNTR
        //                ,FL_COINC_INT_CNTR
        //                ,DT_DECES
        //                ,FL_FUMATORE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_LAV_DIP
        //                ,TP_VARZ_PAGAT
        //                ,COD_RID
        //                ,TP_CAUS_RID
        //                ,IND_MASSA_CORP
        //                ,CAT_RSH_PROF
        //             INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //             FROM RAPP_ANA
        //             WHERE     ID_OGG = :RAN-ID-OGG
        //                    AND TP_OGG = :RAN-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        rappAnaDao.selectRec1(rappAna.getRanIdOgg(), rappAna.getRanTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-EFF-RAN
        //           END-EXEC.
        rappAnaDao.openCIdoEffRan(rappAna.getRanIdOgg(), rappAna.getRanTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-EFF-RAN
        //           END-EXEC.
        rappAnaDao.closeCIdoEffRan();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-EFF-RAN
        //           INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //           END-EXEC.
        rappAnaDao.fetchCIdoEffRan(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO     THRU A770-EX
            a770CloseCursorIdo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,TP_PERS
        //                ,SEX
        //                ,DT_NASC
        //                ,FL_ESTAS
        //                ,INDIR_1
        //                ,INDIR_2
        //                ,INDIR_3
        //                ,TP_UTLZ_INDIR_1
        //                ,TP_UTLZ_INDIR_2
        //                ,TP_UTLZ_INDIR_3
        //                ,ESTR_CNT_CORR_ACCR
        //                ,ESTR_CNT_CORR_ADD
        //                ,ESTR_DOCTO
        //                ,PC_NEL_RAPP
        //                ,TP_MEZ_PAG_ADD
        //                ,TP_MEZ_PAG_ACCR
        //                ,COD_MATR
        //                ,TP_ADEGZ
        //                ,FL_TST_RSH
        //                ,COD_AZ
        //                ,IND_PRINC
        //                ,DT_DELIBERA_CDA
        //                ,DLG_AL_RISC
        //                ,LEGALE_RAPPR_PRINC
        //                ,TP_LEGALE_RAPPR
        //                ,TP_IND_PRINC
        //                ,TP_STAT_RID
        //                ,NOME_INT_RID
        //                ,COGN_INT_RID
        //                ,COGN_INT_TRATT
        //                ,NOME_INT_TRATT
        //                ,CF_INT_RID
        //                ,FL_COINC_DIP_CNTR
        //                ,FL_COINC_INT_CNTR
        //                ,DT_DECES
        //                ,FL_FUMATORE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_LAV_DIP
        //                ,TP_VARZ_PAGAT
        //                ,COD_RID
        //                ,TP_CAUS_RID
        //                ,IND_MASSA_CORP
        //                ,CAT_RSH_PROF
        //             INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //             FROM RAPP_ANA
        //             WHERE     ID_RAPP_ANA = :RAN-ID-RAPP-ANA
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        rappAnaDao.selectRec2(rappAna.getRanIdRappAna(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDO-CPZ-RAN CURSOR FOR
        //              SELECT
        //                     ID_RAPP_ANA
        //                    ,ID_RAPP_ANA_COLLG
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,COD_SOGG
        //                    ,TP_RAPP_ANA
        //                    ,TP_PERS
        //                    ,SEX
        //                    ,DT_NASC
        //                    ,FL_ESTAS
        //                    ,INDIR_1
        //                    ,INDIR_2
        //                    ,INDIR_3
        //                    ,TP_UTLZ_INDIR_1
        //                    ,TP_UTLZ_INDIR_2
        //                    ,TP_UTLZ_INDIR_3
        //                    ,ESTR_CNT_CORR_ACCR
        //                    ,ESTR_CNT_CORR_ADD
        //                    ,ESTR_DOCTO
        //                    ,PC_NEL_RAPP
        //                    ,TP_MEZ_PAG_ADD
        //                    ,TP_MEZ_PAG_ACCR
        //                    ,COD_MATR
        //                    ,TP_ADEGZ
        //                    ,FL_TST_RSH
        //                    ,COD_AZ
        //                    ,IND_PRINC
        //                    ,DT_DELIBERA_CDA
        //                    ,DLG_AL_RISC
        //                    ,LEGALE_RAPPR_PRINC
        //                    ,TP_LEGALE_RAPPR
        //                    ,TP_IND_PRINC
        //                    ,TP_STAT_RID
        //                    ,NOME_INT_RID
        //                    ,COGN_INT_RID
        //                    ,COGN_INT_TRATT
        //                    ,NOME_INT_TRATT
        //                    ,CF_INT_RID
        //                    ,FL_COINC_DIP_CNTR
        //                    ,FL_COINC_INT_CNTR
        //                    ,DT_DECES
        //                    ,FL_FUMATORE
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,FL_LAV_DIP
        //                    ,TP_VARZ_PAGAT
        //                    ,COD_RID
        //                    ,TP_CAUS_RID
        //                    ,IND_MASSA_CORP
        //                    ,CAT_RSH_PROF
        //              FROM RAPP_ANA
        //              WHERE     ID_OGG = :RAN-ID-OGG
        //           AND TP_OGG = :RAN-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_RAPP_ANA ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_RAPP_ANA
        //                ,ID_RAPP_ANA_COLLG
        //                ,ID_OGG
        //                ,TP_OGG
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,COD_SOGG
        //                ,TP_RAPP_ANA
        //                ,TP_PERS
        //                ,SEX
        //                ,DT_NASC
        //                ,FL_ESTAS
        //                ,INDIR_1
        //                ,INDIR_2
        //                ,INDIR_3
        //                ,TP_UTLZ_INDIR_1
        //                ,TP_UTLZ_INDIR_2
        //                ,TP_UTLZ_INDIR_3
        //                ,ESTR_CNT_CORR_ACCR
        //                ,ESTR_CNT_CORR_ADD
        //                ,ESTR_DOCTO
        //                ,PC_NEL_RAPP
        //                ,TP_MEZ_PAG_ADD
        //                ,TP_MEZ_PAG_ACCR
        //                ,COD_MATR
        //                ,TP_ADEGZ
        //                ,FL_TST_RSH
        //                ,COD_AZ
        //                ,IND_PRINC
        //                ,DT_DELIBERA_CDA
        //                ,DLG_AL_RISC
        //                ,LEGALE_RAPPR_PRINC
        //                ,TP_LEGALE_RAPPR
        //                ,TP_IND_PRINC
        //                ,TP_STAT_RID
        //                ,NOME_INT_RID
        //                ,COGN_INT_RID
        //                ,COGN_INT_TRATT
        //                ,NOME_INT_TRATT
        //                ,CF_INT_RID
        //                ,FL_COINC_DIP_CNTR
        //                ,FL_COINC_INT_CNTR
        //                ,DT_DECES
        //                ,FL_FUMATORE
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,FL_LAV_DIP
        //                ,TP_VARZ_PAGAT
        //                ,COD_RID
        //                ,TP_CAUS_RID
        //                ,IND_MASSA_CORP
        //                ,CAT_RSH_PROF
        //             INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //             FROM RAPP_ANA
        //             WHERE     ID_OGG = :RAN-ID-OGG
        //                    AND TP_OGG = :RAN-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        rappAnaDao.selectRec3(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDO-CPZ-RAN
        //           END-EXEC.
        rappAnaDao.openCIdoCpzRan(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDO-CPZ-RAN
        //           END-EXEC.
        rappAnaDao.closeCIdoCpzRan();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDO-CPZ-RAN
        //           INTO
        //                :RAN-ID-RAPP-ANA
        //               ,:RAN-ID-RAPP-ANA-COLLG
        //                :IND-RAN-ID-RAPP-ANA-COLLG
        //               ,:RAN-ID-OGG
        //               ,:RAN-TP-OGG
        //               ,:RAN-ID-MOVI-CRZ
        //               ,:RAN-ID-MOVI-CHIU
        //                :IND-RAN-ID-MOVI-CHIU
        //               ,:RAN-DT-INI-EFF-DB
        //               ,:RAN-DT-END-EFF-DB
        //               ,:RAN-COD-COMP-ANIA
        //               ,:RAN-COD-SOGG
        //                :IND-RAN-COD-SOGG
        //               ,:RAN-TP-RAPP-ANA
        //               ,:RAN-TP-PERS
        //                :IND-RAN-TP-PERS
        //               ,:RAN-SEX
        //                :IND-RAN-SEX
        //               ,:RAN-DT-NASC-DB
        //                :IND-RAN-DT-NASC
        //               ,:RAN-FL-ESTAS
        //                :IND-RAN-FL-ESTAS
        //               ,:RAN-INDIR-1
        //                :IND-RAN-INDIR-1
        //               ,:RAN-INDIR-2
        //                :IND-RAN-INDIR-2
        //               ,:RAN-INDIR-3
        //                :IND-RAN-INDIR-3
        //               ,:RAN-TP-UTLZ-INDIR-1
        //                :IND-RAN-TP-UTLZ-INDIR-1
        //               ,:RAN-TP-UTLZ-INDIR-2
        //                :IND-RAN-TP-UTLZ-INDIR-2
        //               ,:RAN-TP-UTLZ-INDIR-3
        //                :IND-RAN-TP-UTLZ-INDIR-3
        //               ,:RAN-ESTR-CNT-CORR-ACCR
        //                :IND-RAN-ESTR-CNT-CORR-ACCR
        //               ,:RAN-ESTR-CNT-CORR-ADD
        //                :IND-RAN-ESTR-CNT-CORR-ADD
        //               ,:RAN-ESTR-DOCTO
        //                :IND-RAN-ESTR-DOCTO
        //               ,:RAN-PC-NEL-RAPP
        //                :IND-RAN-PC-NEL-RAPP
        //               ,:RAN-TP-MEZ-PAG-ADD
        //                :IND-RAN-TP-MEZ-PAG-ADD
        //               ,:RAN-TP-MEZ-PAG-ACCR
        //                :IND-RAN-TP-MEZ-PAG-ACCR
        //               ,:RAN-COD-MATR
        //                :IND-RAN-COD-MATR
        //               ,:RAN-TP-ADEGZ
        //                :IND-RAN-TP-ADEGZ
        //               ,:RAN-FL-TST-RSH
        //                :IND-RAN-FL-TST-RSH
        //               ,:RAN-COD-AZ
        //                :IND-RAN-COD-AZ
        //               ,:RAN-IND-PRINC
        //                :IND-RAN-IND-PRINC
        //               ,:RAN-DT-DELIBERA-CDA-DB
        //                :IND-RAN-DT-DELIBERA-CDA
        //               ,:RAN-DLG-AL-RISC
        //                :IND-RAN-DLG-AL-RISC
        //               ,:RAN-LEGALE-RAPPR-PRINC
        //                :IND-RAN-LEGALE-RAPPR-PRINC
        //               ,:RAN-TP-LEGALE-RAPPR
        //                :IND-RAN-TP-LEGALE-RAPPR
        //               ,:RAN-TP-IND-PRINC
        //                :IND-RAN-TP-IND-PRINC
        //               ,:RAN-TP-STAT-RID
        //                :IND-RAN-TP-STAT-RID
        //               ,:RAN-NOME-INT-RID-VCHAR
        //                :IND-RAN-NOME-INT-RID
        //               ,:RAN-COGN-INT-RID-VCHAR
        //                :IND-RAN-COGN-INT-RID
        //               ,:RAN-COGN-INT-TRATT-VCHAR
        //                :IND-RAN-COGN-INT-TRATT
        //               ,:RAN-NOME-INT-TRATT-VCHAR
        //                :IND-RAN-NOME-INT-TRATT
        //               ,:RAN-CF-INT-RID
        //                :IND-RAN-CF-INT-RID
        //               ,:RAN-FL-COINC-DIP-CNTR
        //                :IND-RAN-FL-COINC-DIP-CNTR
        //               ,:RAN-FL-COINC-INT-CNTR
        //                :IND-RAN-FL-COINC-INT-CNTR
        //               ,:RAN-DT-DECES-DB
        //                :IND-RAN-DT-DECES
        //               ,:RAN-FL-FUMATORE
        //                :IND-RAN-FL-FUMATORE
        //               ,:RAN-DS-RIGA
        //               ,:RAN-DS-OPER-SQL
        //               ,:RAN-DS-VER
        //               ,:RAN-DS-TS-INI-CPTZ
        //               ,:RAN-DS-TS-END-CPTZ
        //               ,:RAN-DS-UTENTE
        //               ,:RAN-DS-STATO-ELAB
        //               ,:RAN-FL-LAV-DIP
        //                :IND-RAN-FL-LAV-DIP
        //               ,:RAN-TP-VARZ-PAGAT
        //                :IND-RAN-TP-VARZ-PAGAT
        //               ,:RAN-COD-RID
        //                :IND-RAN-COD-RID
        //               ,:RAN-TP-CAUS-RID
        //                :IND-RAN-TP-CAUS-RID
        //               ,:RAN-IND-MASSA-CORP
        //                :IND-RAN-IND-MASSA-CORP
        //               ,:RAN-CAT-RSH-PROF
        //                :IND-RAN-CAT-RSH-PROF
        //           END-EXEC.
        rappAnaDao.fetchCIdoCpzRan(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ     THRU B770-EX
            b770CloseCursorIdoCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-RAN-ID-RAPP-ANA-COLLG = -1
        //              MOVE HIGH-VALUES TO RAN-ID-RAPP-ANA-COLLG-NULL
        //           END-IF
        if (ws.getIndRappAna().getIdRappAnaCollg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ID-RAPP-ANA-COLLG-NULL
            rappAna.getRanIdRappAnaCollg().setRanIdRappAnaCollgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanIdRappAnaCollg.Len.RAN_ID_RAPP_ANA_COLLG_NULL));
        }
        // COB_CODE: IF IND-RAN-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO RAN-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndRappAna().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ID-MOVI-CHIU-NULL
            rappAna.getRanIdMoviChiu().setRanIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanIdMoviChiu.Len.RAN_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-RAN-COD-SOGG = -1
        //              MOVE HIGH-VALUES TO RAN-COD-SOGG-NULL
        //           END-IF
        if (ws.getIndRappAna().getCodSogg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COD-SOGG-NULL
            rappAna.setRanCodSogg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COD_SOGG));
        }
        // COB_CODE: IF IND-RAN-TP-PERS = -1
        //              MOVE HIGH-VALUES TO RAN-TP-PERS-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpPers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-PERS-NULL
            rappAna.setRanTpPers(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-SEX = -1
        //              MOVE HIGH-VALUES TO RAN-SEX-NULL
        //           END-IF
        if (ws.getIndRappAna().getSex() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-SEX-NULL
            rappAna.setRanSex(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-DT-NASC = -1
        //              MOVE HIGH-VALUES TO RAN-DT-NASC-NULL
        //           END-IF
        if (ws.getIndRappAna().getDtNasc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-DT-NASC-NULL
            rappAna.getRanDtNasc().setRanDtNascNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanDtNasc.Len.RAN_DT_NASC_NULL));
        }
        // COB_CODE: IF IND-RAN-FL-ESTAS = -1
        //              MOVE HIGH-VALUES TO RAN-FL-ESTAS-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlEstas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-ESTAS-NULL
            rappAna.setRanFlEstas(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-INDIR-1 = -1
        //              MOVE HIGH-VALUES TO RAN-INDIR-1-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndir1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-INDIR-1-NULL
            rappAna.setRanIndir1(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_INDIR1));
        }
        // COB_CODE: IF IND-RAN-INDIR-2 = -1
        //              MOVE HIGH-VALUES TO RAN-INDIR-2-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndir2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-INDIR-2-NULL
            rappAna.setRanIndir2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_INDIR2));
        }
        // COB_CODE: IF IND-RAN-INDIR-3 = -1
        //              MOVE HIGH-VALUES TO RAN-INDIR-3-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndir3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-INDIR-3-NULL
            rappAna.setRanIndir3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_INDIR3));
        }
        // COB_CODE: IF IND-RAN-TP-UTLZ-INDIR-1 = -1
        //              MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-1-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpUtlzIndir1() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-1-NULL
            rappAna.setRanTpUtlzIndir1(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_UTLZ_INDIR1));
        }
        // COB_CODE: IF IND-RAN-TP-UTLZ-INDIR-2 = -1
        //              MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-2-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpUtlzIndir2() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-2-NULL
            rappAna.setRanTpUtlzIndir2(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_UTLZ_INDIR2));
        }
        // COB_CODE: IF IND-RAN-TP-UTLZ-INDIR-3 = -1
        //              MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-3-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpUtlzIndir3() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-UTLZ-INDIR-3-NULL
            rappAna.setRanTpUtlzIndir3(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_UTLZ_INDIR3));
        }
        // COB_CODE: IF IND-RAN-ESTR-CNT-CORR-ACCR = -1
        //              MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ACCR-NULL
        //           END-IF
        if (ws.getIndRappAna().getEstrCntCorrAccr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ACCR-NULL
            rappAna.setRanEstrCntCorrAccr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_ESTR_CNT_CORR_ACCR));
        }
        // COB_CODE: IF IND-RAN-ESTR-CNT-CORR-ADD = -1
        //              MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ADD-NULL
        //           END-IF
        if (ws.getIndRappAna().getEstrCntCorrAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ESTR-CNT-CORR-ADD-NULL
            rappAna.setRanEstrCntCorrAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_ESTR_CNT_CORR_ADD));
        }
        // COB_CODE: IF IND-RAN-ESTR-DOCTO = -1
        //              MOVE HIGH-VALUES TO RAN-ESTR-DOCTO-NULL
        //           END-IF
        if (ws.getIndRappAna().getEstrDocto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-ESTR-DOCTO-NULL
            rappAna.setRanEstrDocto(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_ESTR_DOCTO));
        }
        // COB_CODE: IF IND-RAN-PC-NEL-RAPP = -1
        //              MOVE HIGH-VALUES TO RAN-PC-NEL-RAPP-NULL
        //           END-IF
        if (ws.getIndRappAna().getPcNelRapp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-PC-NEL-RAPP-NULL
            rappAna.getRanPcNelRapp().setRanPcNelRappNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanPcNelRapp.Len.RAN_PC_NEL_RAPP_NULL));
        }
        // COB_CODE: IF IND-RAN-TP-MEZ-PAG-ADD = -1
        //              MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ADD-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpMezPagAdd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ADD-NULL
            rappAna.setRanTpMezPagAdd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_MEZ_PAG_ADD));
        }
        // COB_CODE: IF IND-RAN-TP-MEZ-PAG-ACCR = -1
        //              MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ACCR-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpMezPagAccr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-MEZ-PAG-ACCR-NULL
            rappAna.setRanTpMezPagAccr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_MEZ_PAG_ACCR));
        }
        // COB_CODE: IF IND-RAN-COD-MATR = -1
        //              MOVE HIGH-VALUES TO RAN-COD-MATR-NULL
        //           END-IF
        if (ws.getIndRappAna().getCodMatr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COD-MATR-NULL
            rappAna.setRanCodMatr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COD_MATR));
        }
        // COB_CODE: IF IND-RAN-TP-ADEGZ = -1
        //              MOVE HIGH-VALUES TO RAN-TP-ADEGZ-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpAdegz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-ADEGZ-NULL
            rappAna.setRanTpAdegz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_ADEGZ));
        }
        // COB_CODE: IF IND-RAN-FL-TST-RSH = -1
        //              MOVE HIGH-VALUES TO RAN-FL-TST-RSH-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlTstRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-TST-RSH-NULL
            rappAna.setRanFlTstRsh(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-COD-AZ = -1
        //              MOVE HIGH-VALUES TO RAN-COD-AZ-NULL
        //           END-IF
        if (ws.getIndRappAna().getCodAz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COD-AZ-NULL
            rappAna.setRanCodAz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COD_AZ));
        }
        // COB_CODE: IF IND-RAN-IND-PRINC = -1
        //              MOVE HIGH-VALUES TO RAN-IND-PRINC-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndPrinc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-IND-PRINC-NULL
            rappAna.setRanIndPrinc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_IND_PRINC));
        }
        // COB_CODE: IF IND-RAN-DT-DELIBERA-CDA = -1
        //              MOVE HIGH-VALUES TO RAN-DT-DELIBERA-CDA-NULL
        //           END-IF
        if (ws.getIndRappAna().getDtDeliberaCda() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-DT-DELIBERA-CDA-NULL
            rappAna.getRanDtDeliberaCda().setRanDtDeliberaCdaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanDtDeliberaCda.Len.RAN_DT_DELIBERA_CDA_NULL));
        }
        // COB_CODE: IF IND-RAN-DLG-AL-RISC = -1
        //              MOVE HIGH-VALUES TO RAN-DLG-AL-RISC-NULL
        //           END-IF
        if (ws.getIndRappAna().getDlgAlRisc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-DLG-AL-RISC-NULL
            rappAna.setRanDlgAlRisc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-LEGALE-RAPPR-PRINC = -1
        //              MOVE HIGH-VALUES TO RAN-LEGALE-RAPPR-PRINC-NULL
        //           END-IF
        if (ws.getIndRappAna().getLegaleRapprPrinc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-LEGALE-RAPPR-PRINC-NULL
            rappAna.setRanLegaleRapprPrinc(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-TP-LEGALE-RAPPR = -1
        //              MOVE HIGH-VALUES TO RAN-TP-LEGALE-RAPPR-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpLegaleRappr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-LEGALE-RAPPR-NULL
            rappAna.setRanTpLegaleRappr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_LEGALE_RAPPR));
        }
        // COB_CODE: IF IND-RAN-TP-IND-PRINC = -1
        //              MOVE HIGH-VALUES TO RAN-TP-IND-PRINC-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpIndPrinc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-IND-PRINC-NULL
            rappAna.setRanTpIndPrinc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_IND_PRINC));
        }
        // COB_CODE: IF IND-RAN-TP-STAT-RID = -1
        //              MOVE HIGH-VALUES TO RAN-TP-STAT-RID-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpStatRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-STAT-RID-NULL
            rappAna.setRanTpStatRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_STAT_RID));
        }
        // COB_CODE: IF IND-RAN-NOME-INT-RID = -1
        //              MOVE HIGH-VALUES TO RAN-NOME-INT-RID
        //           END-IF
        if (ws.getIndRappAna().getNomeIntRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-NOME-INT-RID
            rappAna.setRanNomeIntRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_NOME_INT_RID));
        }
        // COB_CODE: IF IND-RAN-COGN-INT-RID = -1
        //              MOVE HIGH-VALUES TO RAN-COGN-INT-RID
        //           END-IF
        if (ws.getIndRappAna().getCognIntRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COGN-INT-RID
            rappAna.setRanCognIntRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COGN_INT_RID));
        }
        // COB_CODE: IF IND-RAN-COGN-INT-TRATT = -1
        //              MOVE HIGH-VALUES TO RAN-COGN-INT-TRATT
        //           END-IF
        if (ws.getIndRappAna().getCognIntTratt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COGN-INT-TRATT
            rappAna.setRanCognIntTratt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COGN_INT_TRATT));
        }
        // COB_CODE: IF IND-RAN-NOME-INT-TRATT = -1
        //              MOVE HIGH-VALUES TO RAN-NOME-INT-TRATT
        //           END-IF
        if (ws.getIndRappAna().getNomeIntTratt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-NOME-INT-TRATT
            rappAna.setRanNomeIntTratt(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_NOME_INT_TRATT));
        }
        // COB_CODE: IF IND-RAN-CF-INT-RID = -1
        //              MOVE HIGH-VALUES TO RAN-CF-INT-RID-NULL
        //           END-IF
        if (ws.getIndRappAna().getCfIntRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-CF-INT-RID-NULL
            rappAna.setRanCfIntRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_CF_INT_RID));
        }
        // COB_CODE: IF IND-RAN-FL-COINC-DIP-CNTR = -1
        //              MOVE HIGH-VALUES TO RAN-FL-COINC-DIP-CNTR-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlCoincDipCntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-COINC-DIP-CNTR-NULL
            rappAna.setRanFlCoincDipCntr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-FL-COINC-INT-CNTR = -1
        //              MOVE HIGH-VALUES TO RAN-FL-COINC-INT-CNTR-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlCoincIntCntr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-COINC-INT-CNTR-NULL
            rappAna.setRanFlCoincIntCntr(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-DT-DECES = -1
        //              MOVE HIGH-VALUES TO RAN-DT-DECES-NULL
        //           END-IF
        if (ws.getIndRappAna().getDtDeces() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-DT-DECES-NULL
            rappAna.getRanDtDeces().setRanDtDecesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanDtDeces.Len.RAN_DT_DECES_NULL));
        }
        // COB_CODE: IF IND-RAN-FL-FUMATORE = -1
        //              MOVE HIGH-VALUES TO RAN-FL-FUMATORE-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlFumatore() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-FUMATORE-NULL
            rappAna.setRanFlFumatore(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-FL-LAV-DIP = -1
        //              MOVE HIGH-VALUES TO RAN-FL-LAV-DIP-NULL
        //           END-IF
        if (ws.getIndRappAna().getFlLavDip() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-FL-LAV-DIP-NULL
            rappAna.setRanFlLavDip(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-RAN-TP-VARZ-PAGAT = -1
        //              MOVE HIGH-VALUES TO RAN-TP-VARZ-PAGAT-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpVarzPagat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-VARZ-PAGAT-NULL
            rappAna.setRanTpVarzPagat(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_VARZ_PAGAT));
        }
        // COB_CODE: IF IND-RAN-COD-RID = -1
        //              MOVE HIGH-VALUES TO RAN-COD-RID-NULL
        //           END-IF
        if (ws.getIndRappAna().getCodRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-COD-RID-NULL
            rappAna.setRanCodRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_COD_RID));
        }
        // COB_CODE: IF IND-RAN-TP-CAUS-RID = -1
        //              MOVE HIGH-VALUES TO RAN-TP-CAUS-RID-NULL
        //           END-IF
        if (ws.getIndRappAna().getTpCausRid() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-TP-CAUS-RID-NULL
            rappAna.setRanTpCausRid(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_TP_CAUS_RID));
        }
        // COB_CODE: IF IND-RAN-IND-MASSA-CORP = -1
        //              MOVE HIGH-VALUES TO RAN-IND-MASSA-CORP-NULL
        //           END-IF
        if (ws.getIndRappAna().getIndMassaCorp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-IND-MASSA-CORP-NULL
            rappAna.setRanIndMassaCorp(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_IND_MASSA_CORP));
        }
        // COB_CODE: IF IND-RAN-CAT-RSH-PROF = -1
        //              MOVE HIGH-VALUES TO RAN-CAT-RSH-PROF-NULL
        //           END-IF.
        if (ws.getIndRappAna().getCatRshProf() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO RAN-CAT-RSH-PROF-NULL
            rappAna.setRanCatRshProf(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RappAnaLdbs1240.Len.RAN_CAT_RSH_PROF));
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO RAN-DS-OPER-SQL
        rappAna.setRanDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO RAN-DS-VER
        rappAna.setRanDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO RAN-DS-UTENTE
        rappAna.setRanDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO RAN-DS-STATO-ELAB.
        rappAna.setRanDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO RAN-DS-OPER-SQL
        rappAna.setRanDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO RAN-DS-UTENTE.
        rappAna.setRanDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF RAN-ID-RAPP-ANA-COLLG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-ID-RAPP-ANA-COLLG
        //           ELSE
        //              MOVE 0 TO IND-RAN-ID-RAPP-ANA-COLLG
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanIdRappAnaCollg().getRanIdRappAnaCollgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-ID-RAPP-ANA-COLLG
            ws.getIndRappAna().setIdRappAnaCollg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-ID-RAPP-ANA-COLLG
            ws.getIndRappAna().setIdRappAnaCollg(((short)0));
        }
        // COB_CODE: IF RAN-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-RAN-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanIdMoviChiu().getRanIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-ID-MOVI-CHIU
            ws.getIndRappAna().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-ID-MOVI-CHIU
            ws.getIndRappAna().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF RAN-COD-SOGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-COD-SOGG
        //           ELSE
        //              MOVE 0 TO IND-RAN-COD-SOGG
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanCodSoggFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-COD-SOGG
            ws.getIndRappAna().setCodSogg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-COD-SOGG
            ws.getIndRappAna().setCodSogg(((short)0));
        }
        // COB_CODE: IF RAN-TP-PERS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-PERS
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-PERS
        //           END-IF
        if (Conditions.eq(rappAna.getRanTpPers(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-PERS
            ws.getIndRappAna().setTpPers(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-PERS
            ws.getIndRappAna().setTpPers(((short)0));
        }
        // COB_CODE: IF RAN-SEX-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-SEX
        //           ELSE
        //              MOVE 0 TO IND-RAN-SEX
        //           END-IF
        if (Conditions.eq(rappAna.getRanSex(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-SEX
            ws.getIndRappAna().setSex(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-SEX
            ws.getIndRappAna().setSex(((short)0));
        }
        // COB_CODE: IF RAN-DT-NASC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-DT-NASC
        //           ELSE
        //              MOVE 0 TO IND-RAN-DT-NASC
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanDtNasc().getRanDtNascNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-DT-NASC
            ws.getIndRappAna().setDtNasc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-DT-NASC
            ws.getIndRappAna().setDtNasc(((short)0));
        }
        // COB_CODE: IF RAN-FL-ESTAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-FL-ESTAS
        //           ELSE
        //              MOVE 0 TO IND-RAN-FL-ESTAS
        //           END-IF
        if (Conditions.eq(rappAna.getRanFlEstas(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-FL-ESTAS
            ws.getIndRappAna().setFlEstas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-FL-ESTAS
            ws.getIndRappAna().setFlEstas(((short)0));
        }
        // COB_CODE: IF RAN-INDIR-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-INDIR-1
        //           ELSE
        //              MOVE 0 TO IND-RAN-INDIR-1
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanIndir1Formatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-INDIR-1
            ws.getIndRappAna().setIndir1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-INDIR-1
            ws.getIndRappAna().setIndir1(((short)0));
        }
        // COB_CODE: IF RAN-INDIR-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-INDIR-2
        //           ELSE
        //              MOVE 0 TO IND-RAN-INDIR-2
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanIndir2Formatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-INDIR-2
            ws.getIndRappAna().setIndir2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-INDIR-2
            ws.getIndRappAna().setIndir2(((short)0));
        }
        // COB_CODE: IF RAN-INDIR-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-INDIR-3
        //           ELSE
        //              MOVE 0 TO IND-RAN-INDIR-3
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanIndir3Formatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-INDIR-3
            ws.getIndRappAna().setIndir3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-INDIR-3
            ws.getIndRappAna().setIndir3(((short)0));
        }
        // COB_CODE: IF RAN-TP-UTLZ-INDIR-1-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-UTLZ-INDIR-1
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-UTLZ-INDIR-1
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpUtlzIndir1Formatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-UTLZ-INDIR-1
            ws.getIndRappAna().setTpUtlzIndir1(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-UTLZ-INDIR-1
            ws.getIndRappAna().setTpUtlzIndir1(((short)0));
        }
        // COB_CODE: IF RAN-TP-UTLZ-INDIR-2-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-UTLZ-INDIR-2
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-UTLZ-INDIR-2
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpUtlzIndir2Formatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-UTLZ-INDIR-2
            ws.getIndRappAna().setTpUtlzIndir2(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-UTLZ-INDIR-2
            ws.getIndRappAna().setTpUtlzIndir2(((short)0));
        }
        // COB_CODE: IF RAN-TP-UTLZ-INDIR-3-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-UTLZ-INDIR-3
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-UTLZ-INDIR-3
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpUtlzIndir3Formatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-UTLZ-INDIR-3
            ws.getIndRappAna().setTpUtlzIndir3(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-UTLZ-INDIR-3
            ws.getIndRappAna().setTpUtlzIndir3(((short)0));
        }
        // COB_CODE: IF RAN-ESTR-CNT-CORR-ACCR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-ESTR-CNT-CORR-ACCR
        //           ELSE
        //              MOVE 0 TO IND-RAN-ESTR-CNT-CORR-ACCR
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanEstrCntCorrAccrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-ESTR-CNT-CORR-ACCR
            ws.getIndRappAna().setEstrCntCorrAccr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-ESTR-CNT-CORR-ACCR
            ws.getIndRappAna().setEstrCntCorrAccr(((short)0));
        }
        // COB_CODE: IF RAN-ESTR-CNT-CORR-ADD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-ESTR-CNT-CORR-ADD
        //           ELSE
        //              MOVE 0 TO IND-RAN-ESTR-CNT-CORR-ADD
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanEstrCntCorrAddFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-ESTR-CNT-CORR-ADD
            ws.getIndRappAna().setEstrCntCorrAdd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-ESTR-CNT-CORR-ADD
            ws.getIndRappAna().setEstrCntCorrAdd(((short)0));
        }
        // COB_CODE: IF RAN-ESTR-DOCTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-ESTR-DOCTO
        //           ELSE
        //              MOVE 0 TO IND-RAN-ESTR-DOCTO
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanEstrDoctoFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-ESTR-DOCTO
            ws.getIndRappAna().setEstrDocto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-ESTR-DOCTO
            ws.getIndRappAna().setEstrDocto(((short)0));
        }
        // COB_CODE: IF RAN-PC-NEL-RAPP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-PC-NEL-RAPP
        //           ELSE
        //              MOVE 0 TO IND-RAN-PC-NEL-RAPP
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanPcNelRapp().getRanPcNelRappNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-PC-NEL-RAPP
            ws.getIndRappAna().setPcNelRapp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-PC-NEL-RAPP
            ws.getIndRappAna().setPcNelRapp(((short)0));
        }
        // COB_CODE: IF RAN-TP-MEZ-PAG-ADD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-MEZ-PAG-ADD
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-MEZ-PAG-ADD
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpMezPagAddFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-MEZ-PAG-ADD
            ws.getIndRappAna().setTpMezPagAdd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-MEZ-PAG-ADD
            ws.getIndRappAna().setTpMezPagAdd(((short)0));
        }
        // COB_CODE: IF RAN-TP-MEZ-PAG-ACCR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-MEZ-PAG-ACCR
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-MEZ-PAG-ACCR
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpMezPagAccrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-MEZ-PAG-ACCR
            ws.getIndRappAna().setTpMezPagAccr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-MEZ-PAG-ACCR
            ws.getIndRappAna().setTpMezPagAccr(((short)0));
        }
        // COB_CODE: IF RAN-COD-MATR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-COD-MATR
        //           ELSE
        //              MOVE 0 TO IND-RAN-COD-MATR
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanCodMatrFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-COD-MATR
            ws.getIndRappAna().setCodMatr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-COD-MATR
            ws.getIndRappAna().setCodMatr(((short)0));
        }
        // COB_CODE: IF RAN-TP-ADEGZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-ADEGZ
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-ADEGZ
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpAdegzFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-ADEGZ
            ws.getIndRappAna().setTpAdegz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-ADEGZ
            ws.getIndRappAna().setTpAdegz(((short)0));
        }
        // COB_CODE: IF RAN-FL-TST-RSH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-FL-TST-RSH
        //           ELSE
        //              MOVE 0 TO IND-RAN-FL-TST-RSH
        //           END-IF
        if (Conditions.eq(rappAna.getRanFlTstRsh(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-FL-TST-RSH
            ws.getIndRappAna().setFlTstRsh(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-FL-TST-RSH
            ws.getIndRappAna().setFlTstRsh(((short)0));
        }
        // COB_CODE: IF RAN-COD-AZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-COD-AZ
        //           ELSE
        //              MOVE 0 TO IND-RAN-COD-AZ
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanCodAz(), RappAnaLdbs1240.Len.RAN_COD_AZ)) {
            // COB_CODE: MOVE -1 TO IND-RAN-COD-AZ
            ws.getIndRappAna().setCodAz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-COD-AZ
            ws.getIndRappAna().setCodAz(((short)0));
        }
        // COB_CODE: IF RAN-IND-PRINC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-IND-PRINC
        //           ELSE
        //              MOVE 0 TO IND-RAN-IND-PRINC
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanIndPrincFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-IND-PRINC
            ws.getIndRappAna().setIndPrinc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-IND-PRINC
            ws.getIndRappAna().setIndPrinc(((short)0));
        }
        // COB_CODE: IF RAN-DT-DELIBERA-CDA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-DT-DELIBERA-CDA
        //           ELSE
        //              MOVE 0 TO IND-RAN-DT-DELIBERA-CDA
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanDtDeliberaCda().getRanDtDeliberaCdaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-DT-DELIBERA-CDA
            ws.getIndRappAna().setDtDeliberaCda(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-DT-DELIBERA-CDA
            ws.getIndRappAna().setDtDeliberaCda(((short)0));
        }
        // COB_CODE: IF RAN-DLG-AL-RISC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-DLG-AL-RISC
        //           ELSE
        //              MOVE 0 TO IND-RAN-DLG-AL-RISC
        //           END-IF
        if (Conditions.eq(rappAna.getRanDlgAlRisc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-DLG-AL-RISC
            ws.getIndRappAna().setDlgAlRisc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-DLG-AL-RISC
            ws.getIndRappAna().setDlgAlRisc(((short)0));
        }
        // COB_CODE: IF RAN-LEGALE-RAPPR-PRINC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-LEGALE-RAPPR-PRINC
        //           ELSE
        //              MOVE 0 TO IND-RAN-LEGALE-RAPPR-PRINC
        //           END-IF
        if (Conditions.eq(rappAna.getRanLegaleRapprPrinc(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-LEGALE-RAPPR-PRINC
            ws.getIndRappAna().setLegaleRapprPrinc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-LEGALE-RAPPR-PRINC
            ws.getIndRappAna().setLegaleRapprPrinc(((short)0));
        }
        // COB_CODE: IF RAN-TP-LEGALE-RAPPR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-LEGALE-RAPPR
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-LEGALE-RAPPR
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpLegaleRapprFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-LEGALE-RAPPR
            ws.getIndRappAna().setTpLegaleRappr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-LEGALE-RAPPR
            ws.getIndRappAna().setTpLegaleRappr(((short)0));
        }
        // COB_CODE: IF RAN-TP-IND-PRINC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-IND-PRINC
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-IND-PRINC
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpIndPrincFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-IND-PRINC
            ws.getIndRappAna().setTpIndPrinc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-IND-PRINC
            ws.getIndRappAna().setTpIndPrinc(((short)0));
        }
        // COB_CODE: IF RAN-TP-STAT-RID-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-STAT-RID
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-STAT-RID
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpStatRidFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-STAT-RID
            ws.getIndRappAna().setTpStatRid(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-STAT-RID
            ws.getIndRappAna().setTpStatRid(((short)0));
        }
        // COB_CODE: IF RAN-NOME-INT-RID = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-NOME-INT-RID
        //           ELSE
        //              MOVE 0 TO IND-RAN-NOME-INT-RID
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanNomeIntRid(), RappAnaLdbs1240.Len.RAN_NOME_INT_RID)) {
            // COB_CODE: MOVE -1 TO IND-RAN-NOME-INT-RID
            ws.getIndRappAna().setNomeIntRid(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-NOME-INT-RID
            ws.getIndRappAna().setNomeIntRid(((short)0));
        }
        // COB_CODE: IF RAN-COGN-INT-RID = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-COGN-INT-RID
        //           ELSE
        //              MOVE 0 TO IND-RAN-COGN-INT-RID
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanCognIntRid(), RappAnaLdbs1240.Len.RAN_COGN_INT_RID)) {
            // COB_CODE: MOVE -1 TO IND-RAN-COGN-INT-RID
            ws.getIndRappAna().setCognIntRid(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-COGN-INT-RID
            ws.getIndRappAna().setCognIntRid(((short)0));
        }
        // COB_CODE: IF RAN-COGN-INT-TRATT = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-COGN-INT-TRATT
        //           ELSE
        //              MOVE 0 TO IND-RAN-COGN-INT-TRATT
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanCognIntTratt(), RappAnaLdbs1240.Len.RAN_COGN_INT_TRATT)) {
            // COB_CODE: MOVE -1 TO IND-RAN-COGN-INT-TRATT
            ws.getIndRappAna().setCognIntTratt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-COGN-INT-TRATT
            ws.getIndRappAna().setCognIntTratt(((short)0));
        }
        // COB_CODE: IF RAN-NOME-INT-TRATT = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-NOME-INT-TRATT
        //           ELSE
        //              MOVE 0 TO IND-RAN-NOME-INT-TRATT
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanNomeIntTratt(), RappAnaLdbs1240.Len.RAN_NOME_INT_TRATT)) {
            // COB_CODE: MOVE -1 TO IND-RAN-NOME-INT-TRATT
            ws.getIndRappAna().setNomeIntTratt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-NOME-INT-TRATT
            ws.getIndRappAna().setNomeIntTratt(((short)0));
        }
        // COB_CODE: IF RAN-CF-INT-RID-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-CF-INT-RID
        //           ELSE
        //              MOVE 0 TO IND-RAN-CF-INT-RID
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanCfIntRidFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-CF-INT-RID
            ws.getIndRappAna().setCfIntRid(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-CF-INT-RID
            ws.getIndRappAna().setCfIntRid(((short)0));
        }
        // COB_CODE: IF RAN-FL-COINC-DIP-CNTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-FL-COINC-DIP-CNTR
        //           ELSE
        //              MOVE 0 TO IND-RAN-FL-COINC-DIP-CNTR
        //           END-IF
        if (Conditions.eq(rappAna.getRanFlCoincDipCntr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-FL-COINC-DIP-CNTR
            ws.getIndRappAna().setFlCoincDipCntr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-FL-COINC-DIP-CNTR
            ws.getIndRappAna().setFlCoincDipCntr(((short)0));
        }
        // COB_CODE: IF RAN-FL-COINC-INT-CNTR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-FL-COINC-INT-CNTR
        //           ELSE
        //              MOVE 0 TO IND-RAN-FL-COINC-INT-CNTR
        //           END-IF
        if (Conditions.eq(rappAna.getRanFlCoincIntCntr(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-FL-COINC-INT-CNTR
            ws.getIndRappAna().setFlCoincIntCntr(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-FL-COINC-INT-CNTR
            ws.getIndRappAna().setFlCoincIntCntr(((short)0));
        }
        // COB_CODE: IF RAN-DT-DECES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-DT-DECES
        //           ELSE
        //              MOVE 0 TO IND-RAN-DT-DECES
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanDtDeces().getRanDtDecesNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-DT-DECES
            ws.getIndRappAna().setDtDeces(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-DT-DECES
            ws.getIndRappAna().setDtDeces(((short)0));
        }
        // COB_CODE: IF RAN-FL-FUMATORE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-FL-FUMATORE
        //           ELSE
        //              MOVE 0 TO IND-RAN-FL-FUMATORE
        //           END-IF
        if (Conditions.eq(rappAna.getRanFlFumatore(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-FL-FUMATORE
            ws.getIndRappAna().setFlFumatore(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-FL-FUMATORE
            ws.getIndRappAna().setFlFumatore(((short)0));
        }
        // COB_CODE: IF RAN-FL-LAV-DIP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-FL-LAV-DIP
        //           ELSE
        //              MOVE 0 TO IND-RAN-FL-LAV-DIP
        //           END-IF
        if (Conditions.eq(rappAna.getRanFlLavDip(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-RAN-FL-LAV-DIP
            ws.getIndRappAna().setFlLavDip(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-FL-LAV-DIP
            ws.getIndRappAna().setFlLavDip(((short)0));
        }
        // COB_CODE: IF RAN-TP-VARZ-PAGAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-VARZ-PAGAT
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-VARZ-PAGAT
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpVarzPagatFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-VARZ-PAGAT
            ws.getIndRappAna().setTpVarzPagat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-VARZ-PAGAT
            ws.getIndRappAna().setTpVarzPagat(((short)0));
        }
        // COB_CODE: IF RAN-COD-RID-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-COD-RID
        //           ELSE
        //              MOVE 0 TO IND-RAN-COD-RID
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanCodRidFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-COD-RID
            ws.getIndRappAna().setCodRid(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-COD-RID
            ws.getIndRappAna().setCodRid(((short)0));
        }
        // COB_CODE: IF RAN-TP-CAUS-RID-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-TP-CAUS-RID
        //           ELSE
        //              MOVE 0 TO IND-RAN-TP-CAUS-RID
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanTpCausRidFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-TP-CAUS-RID
            ws.getIndRappAna().setTpCausRid(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-TP-CAUS-RID
            ws.getIndRappAna().setTpCausRid(((short)0));
        }
        // COB_CODE: IF RAN-IND-MASSA-CORP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-IND-MASSA-CORP
        //           ELSE
        //              MOVE 0 TO IND-RAN-IND-MASSA-CORP
        //           END-IF
        if (Characters.EQ_HIGH.test(rappAna.getRanIndMassaCorpFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-IND-MASSA-CORP
            ws.getIndRappAna().setIndMassaCorp(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-IND-MASSA-CORP
            ws.getIndRappAna().setIndMassaCorp(((short)0));
        }
        // COB_CODE: IF RAN-CAT-RSH-PROF-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-RAN-CAT-RSH-PROF
        //           ELSE
        //              MOVE 0 TO IND-RAN-CAT-RSH-PROF
        //           END-IF.
        if (Characters.EQ_HIGH.test(rappAna.getRanCatRshProfFormatted())) {
            // COB_CODE: MOVE -1 TO IND-RAN-CAT-RSH-PROF
            ws.getIndRappAna().setCatRshProf(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-RAN-CAT-RSH-PROF
            ws.getIndRappAna().setCatRshProf(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : RAN-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE RAPP-ANA TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(rappAna.getRappAnaFormatted());
        // COB_CODE: MOVE RAN-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(rappAna.getRanIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO RAN-ID-MOVI-CHIU
                rappAna.getRanIdMoviChiu().setRanIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO RAN-DS-TS-END-CPTZ
                rappAna.setRanDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO RAN-ID-MOVI-CRZ
                    rappAna.setRanIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO RAN-ID-MOVI-CHIU-NULL
                    rappAna.getRanIdMoviChiu().setRanIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanIdMoviChiu.Len.RAN_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO RAN-DT-END-EFF
                    rappAna.setRanDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO RAN-DS-TS-INI-CPTZ
                    rappAna.setRanDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO RAN-DS-TS-END-CPTZ
                    rappAna.setRanDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE RAPP-ANA TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(rappAna.getRappAnaFormatted());
        // COB_CODE: MOVE RAN-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(rappAna.getRanIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO RAPP-ANA.
        rappAna.setRappAnaFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO RAN-ID-MOVI-CRZ.
        rappAna.setRanIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO RAN-ID-MOVI-CHIU-NULL.
        rappAna.getRanIdMoviChiu().setRanIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, RanIdMoviChiu.Len.RAN_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO RAN-DT-INI-EFF.
        rappAna.setRanDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO RAN-DT-END-EFF.
        rappAna.setRanDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO RAN-DS-TS-INI-CPTZ.
        rappAna.setRanDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO RAN-DS-TS-END-CPTZ.
        rappAna.setRanDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO RAN-COD-COMP-ANIA.
        rappAna.setRanCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE RAN-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(rappAna.getRanDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RAN-DT-INI-EFF-DB
        ws.getRappAnaDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE RAN-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(rappAna.getRanDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO RAN-DT-END-EFF-DB
        ws.getRappAnaDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-RAN-DT-NASC = 0
        //               MOVE WS-DATE-X      TO RAN-DT-NASC-DB
        //           END-IF
        if (ws.getIndRappAna().getDtNasc() == 0) {
            // COB_CODE: MOVE RAN-DT-NASC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(rappAna.getRanDtNasc().getRanDtNasc(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO RAN-DT-NASC-DB
            ws.getRappAnaDb().setIniCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-RAN-DT-DELIBERA-CDA = 0
        //               MOVE WS-DATE-X      TO RAN-DT-DELIBERA-CDA-DB
        //           END-IF
        if (ws.getIndRappAna().getDtDeliberaCda() == 0) {
            // COB_CODE: MOVE RAN-DT-DELIBERA-CDA TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(rappAna.getRanDtDeliberaCda().getRanDtDeliberaCda(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO RAN-DT-DELIBERA-CDA-DB
            ws.getRappAnaDb().setEndCopDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-RAN-DT-DECES = 0
        //               MOVE WS-DATE-X      TO RAN-DT-DECES-DB
        //           END-IF.
        if (ws.getIndRappAna().getDtDeces() == 0) {
            // COB_CODE: MOVE RAN-DT-DECES TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(rappAna.getRanDtDeces().getRanDtDeces(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO RAN-DT-DECES-DB
            ws.getRappAnaDb().setEsiTitDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE RAN-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-INI-EFF
        rappAna.setRanDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE RAN-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-END-EFF
        rappAna.setRanDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-RAN-DT-NASC = 0
        //               MOVE WS-DATE-N      TO RAN-DT-NASC
        //           END-IF
        if (ws.getIndRappAna().getDtNasc() == 0) {
            // COB_CODE: MOVE RAN-DT-NASC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getIniCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-NASC
            rappAna.getRanDtNasc().setRanDtNasc(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-RAN-DT-DELIBERA-CDA = 0
        //               MOVE WS-DATE-N      TO RAN-DT-DELIBERA-CDA
        //           END-IF
        if (ws.getIndRappAna().getDtDeliberaCda() == 0) {
            // COB_CODE: MOVE RAN-DT-DELIBERA-CDA-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getEndCopDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-DELIBERA-CDA
            rappAna.getRanDtDeliberaCda().setRanDtDeliberaCda(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-RAN-DT-DECES = 0
        //               MOVE WS-DATE-N      TO RAN-DT-DECES
        //           END-IF.
        if (ws.getIndRappAna().getDtDeces() == 0) {
            // COB_CODE: MOVE RAN-DT-DECES-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getRappAnaDb().getEsiTitDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO RAN-DT-DECES
            rappAna.getRanDtDeces().setRanDtDeces(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
        // COB_CODE: MOVE LENGTH OF RAN-NOME-INT-RID
        //                       TO RAN-NOME-INT-RID-LEN
        rappAna.setRanNomeIntRidLen(((short)RappAnaLdbs1240.Len.RAN_NOME_INT_RID));
        // COB_CODE: MOVE LENGTH OF RAN-COGN-INT-RID
        //                       TO RAN-COGN-INT-RID-LEN
        rappAna.setRanCognIntRidLen(((short)RappAnaLdbs1240.Len.RAN_COGN_INT_RID));
        // COB_CODE: MOVE LENGTH OF RAN-COGN-INT-TRATT
        //                       TO RAN-COGN-INT-TRATT-LEN
        rappAna.setRanCognIntTrattLen(((short)RappAnaLdbs1240.Len.RAN_COGN_INT_TRATT));
        // COB_CODE: MOVE LENGTH OF RAN-NOME-INT-TRATT
        //                       TO RAN-NOME-INT-TRATT-LEN.
        rappAna.setRanNomeIntTrattLen(((short)RappAnaLdbs1240.Len.RAN_NOME_INT_TRATT));
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public String getCatRshProf() {
        return rappAna.getRanCatRshProf();
    }

    @Override
    public void setCatRshProf(String catRshProf) {
        this.rappAna.setRanCatRshProf(catRshProf);
    }

    @Override
    public String getCatRshProfObj() {
        if (ws.getIndRappAna().getCatRshProf() >= 0) {
            return getCatRshProf();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCatRshProfObj(String catRshProfObj) {
        if (catRshProfObj != null) {
            setCatRshProf(catRshProfObj);
            ws.getIndRappAna().setCatRshProf(((short)0));
        }
        else {
            ws.getIndRappAna().setCatRshProf(((short)-1));
        }
    }

    @Override
    public String getCfIntRid() {
        return rappAna.getRanCfIntRid();
    }

    @Override
    public void setCfIntRid(String cfIntRid) {
        this.rappAna.setRanCfIntRid(cfIntRid);
    }

    @Override
    public String getCfIntRidObj() {
        if (ws.getIndRappAna().getCfIntRid() >= 0) {
            return getCfIntRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCfIntRidObj(String cfIntRidObj) {
        if (cfIntRidObj != null) {
            setCfIntRid(cfIntRidObj);
            ws.getIndRappAna().setCfIntRid(((short)0));
        }
        else {
            ws.getIndRappAna().setCfIntRid(((short)-1));
        }
    }

    @Override
    public String getCodAz() {
        return rappAna.getRanCodAz();
    }

    @Override
    public void setCodAz(String codAz) {
        this.rappAna.setRanCodAz(codAz);
    }

    @Override
    public String getCodAzObj() {
        if (ws.getIndRappAna().getCodAz() >= 0) {
            return getCodAz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodAzObj(String codAzObj) {
        if (codAzObj != null) {
            setCodAz(codAzObj);
            ws.getIndRappAna().setCodAz(((short)0));
        }
        else {
            ws.getIndRappAna().setCodAz(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return rappAna.getRanCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.rappAna.setRanCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodMatr() {
        return rappAna.getRanCodMatr();
    }

    @Override
    public void setCodMatr(String codMatr) {
        this.rappAna.setRanCodMatr(codMatr);
    }

    @Override
    public String getCodMatrObj() {
        if (ws.getIndRappAna().getCodMatr() >= 0) {
            return getCodMatr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodMatrObj(String codMatrObj) {
        if (codMatrObj != null) {
            setCodMatr(codMatrObj);
            ws.getIndRappAna().setCodMatr(((short)0));
        }
        else {
            ws.getIndRappAna().setCodMatr(((short)-1));
        }
    }

    @Override
    public String getCodRid() {
        return rappAna.getRanCodRid();
    }

    @Override
    public void setCodRid(String codRid) {
        this.rappAna.setRanCodRid(codRid);
    }

    @Override
    public String getCodRidObj() {
        if (ws.getIndRappAna().getCodRid() >= 0) {
            return getCodRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodRidObj(String codRidObj) {
        if (codRidObj != null) {
            setCodRid(codRidObj);
            ws.getIndRappAna().setCodRid(((short)0));
        }
        else {
            ws.getIndRappAna().setCodRid(((short)-1));
        }
    }

    @Override
    public String getCognIntRidVchar() {
        return rappAna.getRanCognIntRidVcharFormatted();
    }

    @Override
    public void setCognIntRidVchar(String cognIntRidVchar) {
        this.rappAna.setRanCognIntRidVcharFormatted(cognIntRidVchar);
    }

    @Override
    public String getCognIntRidVcharObj() {
        if (ws.getIndRappAna().getCognIntRid() >= 0) {
            return getCognIntRidVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCognIntRidVcharObj(String cognIntRidVcharObj) {
        if (cognIntRidVcharObj != null) {
            setCognIntRidVchar(cognIntRidVcharObj);
            ws.getIndRappAna().setCognIntRid(((short)0));
        }
        else {
            ws.getIndRappAna().setCognIntRid(((short)-1));
        }
    }

    @Override
    public String getCognIntTrattVchar() {
        return rappAna.getRanCognIntTrattVcharFormatted();
    }

    @Override
    public void setCognIntTrattVchar(String cognIntTrattVchar) {
        this.rappAna.setRanCognIntTrattVcharFormatted(cognIntTrattVchar);
    }

    @Override
    public String getCognIntTrattVcharObj() {
        if (ws.getIndRappAna().getCognIntTratt() >= 0) {
            return getCognIntTrattVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCognIntTrattVcharObj(String cognIntTrattVcharObj) {
        if (cognIntTrattVcharObj != null) {
            setCognIntTrattVchar(cognIntTrattVcharObj);
            ws.getIndRappAna().setCognIntTratt(((short)0));
        }
        else {
            ws.getIndRappAna().setCognIntTratt(((short)-1));
        }
    }

    @Override
    public char getDlgAlRisc() {
        return rappAna.getRanDlgAlRisc();
    }

    @Override
    public void setDlgAlRisc(char dlgAlRisc) {
        this.rappAna.setRanDlgAlRisc(dlgAlRisc);
    }

    @Override
    public Character getDlgAlRiscObj() {
        if (ws.getIndRappAna().getDlgAlRisc() >= 0) {
            return ((Character)getDlgAlRisc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDlgAlRiscObj(Character dlgAlRiscObj) {
        if (dlgAlRiscObj != null) {
            setDlgAlRisc(((char)dlgAlRiscObj));
            ws.getIndRappAna().setDlgAlRisc(((short)0));
        }
        else {
            ws.getIndRappAna().setDlgAlRisc(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return rappAna.getRanDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.rappAna.setRanDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return rappAna.getRanDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.rappAna.setRanDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return rappAna.getRanDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.rappAna.setRanDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return rappAna.getRanDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.rappAna.setRanDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return rappAna.getRanDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.rappAna.setRanDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return rappAna.getRanDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.rappAna.setRanDsVer(dsVer);
    }

    @Override
    public String getDtDecesDb() {
        return ws.getRappAnaDb().getEsiTitDb();
    }

    @Override
    public void setDtDecesDb(String dtDecesDb) {
        this.ws.getRappAnaDb().setEsiTitDb(dtDecesDb);
    }

    @Override
    public String getDtDecesDbObj() {
        if (ws.getIndRappAna().getDtDeces() >= 0) {
            return getDtDecesDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecesDbObj(String dtDecesDbObj) {
        if (dtDecesDbObj != null) {
            setDtDecesDb(dtDecesDbObj);
            ws.getIndRappAna().setDtDeces(((short)0));
        }
        else {
            ws.getIndRappAna().setDtDeces(((short)-1));
        }
    }

    @Override
    public String getDtDeliberaCdaDb() {
        return ws.getRappAnaDb().getEndCopDb();
    }

    @Override
    public void setDtDeliberaCdaDb(String dtDeliberaCdaDb) {
        this.ws.getRappAnaDb().setEndCopDb(dtDeliberaCdaDb);
    }

    @Override
    public String getDtDeliberaCdaDbObj() {
        if (ws.getIndRappAna().getDtDeliberaCda() >= 0) {
            return getDtDeliberaCdaDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDeliberaCdaDbObj(String dtDeliberaCdaDbObj) {
        if (dtDeliberaCdaDbObj != null) {
            setDtDeliberaCdaDb(dtDeliberaCdaDbObj);
            ws.getIndRappAna().setDtDeliberaCda(((short)0));
        }
        else {
            ws.getIndRappAna().setDtDeliberaCda(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getRappAnaDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getRappAnaDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getRappAnaDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getRappAnaDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtNascDb() {
        return ws.getRappAnaDb().getIniCopDb();
    }

    @Override
    public void setDtNascDb(String dtNascDb) {
        this.ws.getRappAnaDb().setIniCopDb(dtNascDb);
    }

    @Override
    public String getDtNascDbObj() {
        if (ws.getIndRappAna().getDtNasc() >= 0) {
            return getDtNascDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtNascDbObj(String dtNascDbObj) {
        if (dtNascDbObj != null) {
            setDtNascDb(dtNascDbObj);
            ws.getIndRappAna().setDtNasc(((short)0));
        }
        else {
            ws.getIndRappAna().setDtNasc(((short)-1));
        }
    }

    @Override
    public String getEstrCntCorrAccr() {
        return rappAna.getRanEstrCntCorrAccr();
    }

    @Override
    public void setEstrCntCorrAccr(String estrCntCorrAccr) {
        this.rappAna.setRanEstrCntCorrAccr(estrCntCorrAccr);
    }

    @Override
    public String getEstrCntCorrAccrObj() {
        if (ws.getIndRappAna().getEstrCntCorrAccr() >= 0) {
            return getEstrCntCorrAccr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEstrCntCorrAccrObj(String estrCntCorrAccrObj) {
        if (estrCntCorrAccrObj != null) {
            setEstrCntCorrAccr(estrCntCorrAccrObj);
            ws.getIndRappAna().setEstrCntCorrAccr(((short)0));
        }
        else {
            ws.getIndRappAna().setEstrCntCorrAccr(((short)-1));
        }
    }

    @Override
    public String getEstrCntCorrAdd() {
        return rappAna.getRanEstrCntCorrAdd();
    }

    @Override
    public void setEstrCntCorrAdd(String estrCntCorrAdd) {
        this.rappAna.setRanEstrCntCorrAdd(estrCntCorrAdd);
    }

    @Override
    public String getEstrCntCorrAddObj() {
        if (ws.getIndRappAna().getEstrCntCorrAdd() >= 0) {
            return getEstrCntCorrAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEstrCntCorrAddObj(String estrCntCorrAddObj) {
        if (estrCntCorrAddObj != null) {
            setEstrCntCorrAdd(estrCntCorrAddObj);
            ws.getIndRappAna().setEstrCntCorrAdd(((short)0));
        }
        else {
            ws.getIndRappAna().setEstrCntCorrAdd(((short)-1));
        }
    }

    @Override
    public String getEstrDocto() {
        return rappAna.getRanEstrDocto();
    }

    @Override
    public void setEstrDocto(String estrDocto) {
        this.rappAna.setRanEstrDocto(estrDocto);
    }

    @Override
    public String getEstrDoctoObj() {
        if (ws.getIndRappAna().getEstrDocto() >= 0) {
            return getEstrDocto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setEstrDoctoObj(String estrDoctoObj) {
        if (estrDoctoObj != null) {
            setEstrDocto(estrDoctoObj);
            ws.getIndRappAna().setEstrDocto(((short)0));
        }
        else {
            ws.getIndRappAna().setEstrDocto(((short)-1));
        }
    }

    @Override
    public char getFlCoincDipCntr() {
        return rappAna.getRanFlCoincDipCntr();
    }

    @Override
    public void setFlCoincDipCntr(char flCoincDipCntr) {
        this.rappAna.setRanFlCoincDipCntr(flCoincDipCntr);
    }

    @Override
    public Character getFlCoincDipCntrObj() {
        if (ws.getIndRappAna().getFlCoincDipCntr() >= 0) {
            return ((Character)getFlCoincDipCntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCoincDipCntrObj(Character flCoincDipCntrObj) {
        if (flCoincDipCntrObj != null) {
            setFlCoincDipCntr(((char)flCoincDipCntrObj));
            ws.getIndRappAna().setFlCoincDipCntr(((short)0));
        }
        else {
            ws.getIndRappAna().setFlCoincDipCntr(((short)-1));
        }
    }

    @Override
    public char getFlCoincIntCntr() {
        return rappAna.getRanFlCoincIntCntr();
    }

    @Override
    public void setFlCoincIntCntr(char flCoincIntCntr) {
        this.rappAna.setRanFlCoincIntCntr(flCoincIntCntr);
    }

    @Override
    public Character getFlCoincIntCntrObj() {
        if (ws.getIndRappAna().getFlCoincIntCntr() >= 0) {
            return ((Character)getFlCoincIntCntr());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlCoincIntCntrObj(Character flCoincIntCntrObj) {
        if (flCoincIntCntrObj != null) {
            setFlCoincIntCntr(((char)flCoincIntCntrObj));
            ws.getIndRappAna().setFlCoincIntCntr(((short)0));
        }
        else {
            ws.getIndRappAna().setFlCoincIntCntr(((short)-1));
        }
    }

    @Override
    public char getFlEstas() {
        return rappAna.getRanFlEstas();
    }

    @Override
    public void setFlEstas(char flEstas) {
        this.rappAna.setRanFlEstas(flEstas);
    }

    @Override
    public Character getFlEstasObj() {
        if (ws.getIndRappAna().getFlEstas() >= 0) {
            return ((Character)getFlEstas());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlEstasObj(Character flEstasObj) {
        if (flEstasObj != null) {
            setFlEstas(((char)flEstasObj));
            ws.getIndRappAna().setFlEstas(((short)0));
        }
        else {
            ws.getIndRappAna().setFlEstas(((short)-1));
        }
    }

    @Override
    public char getFlFumatore() {
        return rappAna.getRanFlFumatore();
    }

    @Override
    public void setFlFumatore(char flFumatore) {
        this.rappAna.setRanFlFumatore(flFumatore);
    }

    @Override
    public Character getFlFumatoreObj() {
        if (ws.getIndRappAna().getFlFumatore() >= 0) {
            return ((Character)getFlFumatore());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlFumatoreObj(Character flFumatoreObj) {
        if (flFumatoreObj != null) {
            setFlFumatore(((char)flFumatoreObj));
            ws.getIndRappAna().setFlFumatore(((short)0));
        }
        else {
            ws.getIndRappAna().setFlFumatore(((short)-1));
        }
    }

    @Override
    public char getFlLavDip() {
        return rappAna.getRanFlLavDip();
    }

    @Override
    public void setFlLavDip(char flLavDip) {
        this.rappAna.setRanFlLavDip(flLavDip);
    }

    @Override
    public Character getFlLavDipObj() {
        if (ws.getIndRappAna().getFlLavDip() >= 0) {
            return ((Character)getFlLavDip());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlLavDipObj(Character flLavDipObj) {
        if (flLavDipObj != null) {
            setFlLavDip(((char)flLavDipObj));
            ws.getIndRappAna().setFlLavDip(((short)0));
        }
        else {
            ws.getIndRappAna().setFlLavDip(((short)-1));
        }
    }

    @Override
    public char getFlTstRsh() {
        return rappAna.getRanFlTstRsh();
    }

    @Override
    public void setFlTstRsh(char flTstRsh) {
        this.rappAna.setRanFlTstRsh(flTstRsh);
    }

    @Override
    public Character getFlTstRshObj() {
        if (ws.getIndRappAna().getFlTstRsh() >= 0) {
            return ((Character)getFlTstRsh());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFlTstRshObj(Character flTstRshObj) {
        if (flTstRshObj != null) {
            setFlTstRsh(((char)flTstRshObj));
            ws.getIndRappAna().setFlTstRsh(((short)0));
        }
        else {
            ws.getIndRappAna().setFlTstRsh(((short)-1));
        }
    }

    @Override
    public int getIdMoviChiu() {
        return rappAna.getRanIdMoviChiu().getRanIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.rappAna.getRanIdMoviChiu().setRanIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndRappAna().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndRappAna().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndRappAna().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return rappAna.getRanIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.rappAna.setRanIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdRappAna() {
        return rappAna.getRanIdRappAna();
    }

    @Override
    public void setIdRappAna(int idRappAna) {
        this.rappAna.setRanIdRappAna(idRappAna);
    }

    @Override
    public int getIdRappAnaCollg() {
        return rappAna.getRanIdRappAnaCollg().getRanIdRappAnaCollg();
    }

    @Override
    public void setIdRappAnaCollg(int idRappAnaCollg) {
        this.rappAna.getRanIdRappAnaCollg().setRanIdRappAnaCollg(idRappAnaCollg);
    }

    @Override
    public Integer getIdRappAnaCollgObj() {
        if (ws.getIndRappAna().getIdRappAnaCollg() >= 0) {
            return ((Integer)getIdRappAnaCollg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdRappAnaCollgObj(Integer idRappAnaCollgObj) {
        if (idRappAnaCollgObj != null) {
            setIdRappAnaCollg(((int)idRappAnaCollgObj));
            ws.getIndRappAna().setIdRappAnaCollg(((short)0));
        }
        else {
            ws.getIndRappAna().setIdRappAnaCollg(((short)-1));
        }
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getIndMassaCorp() {
        return rappAna.getRanIndMassaCorp();
    }

    @Override
    public void setIndMassaCorp(String indMassaCorp) {
        this.rappAna.setRanIndMassaCorp(indMassaCorp);
    }

    @Override
    public String getIndMassaCorpObj() {
        if (ws.getIndRappAna().getIndMassaCorp() >= 0) {
            return getIndMassaCorp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndMassaCorpObj(String indMassaCorpObj) {
        if (indMassaCorpObj != null) {
            setIndMassaCorp(indMassaCorpObj);
            ws.getIndRappAna().setIndMassaCorp(((short)0));
        }
        else {
            ws.getIndRappAna().setIndMassaCorp(((short)-1));
        }
    }

    @Override
    public String getIndPrinc() {
        return rappAna.getRanIndPrinc();
    }

    @Override
    public void setIndPrinc(String indPrinc) {
        this.rappAna.setRanIndPrinc(indPrinc);
    }

    @Override
    public String getIndPrincObj() {
        if (ws.getIndRappAna().getIndPrinc() >= 0) {
            return getIndPrinc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndPrincObj(String indPrincObj) {
        if (indPrincObj != null) {
            setIndPrinc(indPrincObj);
            ws.getIndRappAna().setIndPrinc(((short)0));
        }
        else {
            ws.getIndRappAna().setIndPrinc(((short)-1));
        }
    }

    @Override
    public String getIndir1() {
        return rappAna.getRanIndir1();
    }

    @Override
    public void setIndir1(String indir1) {
        this.rappAna.setRanIndir1(indir1);
    }

    @Override
    public String getIndir1Obj() {
        if (ws.getIndRappAna().getIndir1() >= 0) {
            return getIndir1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndir1Obj(String indir1Obj) {
        if (indir1Obj != null) {
            setIndir1(indir1Obj);
            ws.getIndRappAna().setIndir1(((short)0));
        }
        else {
            ws.getIndRappAna().setIndir1(((short)-1));
        }
    }

    @Override
    public String getIndir2() {
        return rappAna.getRanIndir2();
    }

    @Override
    public void setIndir2(String indir2) {
        this.rappAna.setRanIndir2(indir2);
    }

    @Override
    public String getIndir2Obj() {
        if (ws.getIndRappAna().getIndir2() >= 0) {
            return getIndir2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndir2Obj(String indir2Obj) {
        if (indir2Obj != null) {
            setIndir2(indir2Obj);
            ws.getIndRappAna().setIndir2(((short)0));
        }
        else {
            ws.getIndRappAna().setIndir2(((short)-1));
        }
    }

    @Override
    public String getIndir3() {
        return rappAna.getRanIndir3();
    }

    @Override
    public void setIndir3(String indir3) {
        this.rappAna.setRanIndir3(indir3);
    }

    @Override
    public String getIndir3Obj() {
        if (ws.getIndRappAna().getIndir3() >= 0) {
            return getIndir3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIndir3Obj(String indir3Obj) {
        if (indir3Obj != null) {
            setIndir3(indir3Obj);
            ws.getIndRappAna().setIndir3(((short)0));
        }
        else {
            ws.getIndRappAna().setIndir3(((short)-1));
        }
    }

    @Override
    public int getLdbv1241IdOgg() {
        throw new FieldNotMappedException("ldbv1241IdOgg");
    }

    @Override
    public void setLdbv1241IdOgg(int ldbv1241IdOgg) {
        throw new FieldNotMappedException("ldbv1241IdOgg");
    }

    @Override
    public String getLdbv1241TpOgg() {
        throw new FieldNotMappedException("ldbv1241TpOgg");
    }

    @Override
    public void setLdbv1241TpOgg(String ldbv1241TpOgg) {
        throw new FieldNotMappedException("ldbv1241TpOgg");
    }

    @Override
    public String getLdbv1241TpRappAna() {
        throw new FieldNotMappedException("ldbv1241TpRappAna");
    }

    @Override
    public void setLdbv1241TpRappAna(String ldbv1241TpRappAna) {
        throw new FieldNotMappedException("ldbv1241TpRappAna");
    }

    @Override
    public int getLdbv1291IdOgg() {
        throw new FieldNotMappedException("ldbv1291IdOgg");
    }

    @Override
    public void setLdbv1291IdOgg(int ldbv1291IdOgg) {
        throw new FieldNotMappedException("ldbv1291IdOgg");
    }

    @Override
    public String getLdbv1291Tp10() {
        throw new FieldNotMappedException("ldbv1291Tp10");
    }

    @Override
    public void setLdbv1291Tp10(String ldbv1291Tp10) {
        throw new FieldNotMappedException("ldbv1291Tp10");
    }

    @Override
    public String getLdbv1291Tp11() {
        throw new FieldNotMappedException("ldbv1291Tp11");
    }

    @Override
    public void setLdbv1291Tp11(String ldbv1291Tp11) {
        throw new FieldNotMappedException("ldbv1291Tp11");
    }

    @Override
    public String getLdbv1291Tp1() {
        throw new FieldNotMappedException("ldbv1291Tp1");
    }

    @Override
    public void setLdbv1291Tp1(String ldbv1291Tp1) {
        throw new FieldNotMappedException("ldbv1291Tp1");
    }

    @Override
    public String getLdbv1291Tp2() {
        throw new FieldNotMappedException("ldbv1291Tp2");
    }

    @Override
    public void setLdbv1291Tp2(String ldbv1291Tp2) {
        throw new FieldNotMappedException("ldbv1291Tp2");
    }

    @Override
    public String getLdbv1291Tp3() {
        throw new FieldNotMappedException("ldbv1291Tp3");
    }

    @Override
    public void setLdbv1291Tp3(String ldbv1291Tp3) {
        throw new FieldNotMappedException("ldbv1291Tp3");
    }

    @Override
    public String getLdbv1291Tp4() {
        throw new FieldNotMappedException("ldbv1291Tp4");
    }

    @Override
    public void setLdbv1291Tp4(String ldbv1291Tp4) {
        throw new FieldNotMappedException("ldbv1291Tp4");
    }

    @Override
    public String getLdbv1291Tp5() {
        throw new FieldNotMappedException("ldbv1291Tp5");
    }

    @Override
    public void setLdbv1291Tp5(String ldbv1291Tp5) {
        throw new FieldNotMappedException("ldbv1291Tp5");
    }

    @Override
    public String getLdbv1291Tp6() {
        throw new FieldNotMappedException("ldbv1291Tp6");
    }

    @Override
    public void setLdbv1291Tp6(String ldbv1291Tp6) {
        throw new FieldNotMappedException("ldbv1291Tp6");
    }

    @Override
    public String getLdbv1291Tp7() {
        throw new FieldNotMappedException("ldbv1291Tp7");
    }

    @Override
    public void setLdbv1291Tp7(String ldbv1291Tp7) {
        throw new FieldNotMappedException("ldbv1291Tp7");
    }

    @Override
    public String getLdbv1291Tp8() {
        throw new FieldNotMappedException("ldbv1291Tp8");
    }

    @Override
    public void setLdbv1291Tp8(String ldbv1291Tp8) {
        throw new FieldNotMappedException("ldbv1291Tp8");
    }

    @Override
    public String getLdbv1291Tp9() {
        throw new FieldNotMappedException("ldbv1291Tp9");
    }

    @Override
    public void setLdbv1291Tp9(String ldbv1291Tp9) {
        throw new FieldNotMappedException("ldbv1291Tp9");
    }

    @Override
    public String getLdbv1291TpOgg() {
        throw new FieldNotMappedException("ldbv1291TpOgg");
    }

    @Override
    public void setLdbv1291TpOgg(String ldbv1291TpOgg) {
        throw new FieldNotMappedException("ldbv1291TpOgg");
    }

    @Override
    public char getLegaleRapprPrinc() {
        return rappAna.getRanLegaleRapprPrinc();
    }

    @Override
    public void setLegaleRapprPrinc(char legaleRapprPrinc) {
        this.rappAna.setRanLegaleRapprPrinc(legaleRapprPrinc);
    }

    @Override
    public Character getLegaleRapprPrincObj() {
        if (ws.getIndRappAna().getLegaleRapprPrinc() >= 0) {
            return ((Character)getLegaleRapprPrinc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setLegaleRapprPrincObj(Character legaleRapprPrincObj) {
        if (legaleRapprPrincObj != null) {
            setLegaleRapprPrinc(((char)legaleRapprPrincObj));
            ws.getIndRappAna().setLegaleRapprPrinc(((short)0));
        }
        else {
            ws.getIndRappAna().setLegaleRapprPrinc(((short)-1));
        }
    }

    @Override
    public String getNomeIntRidVchar() {
        return rappAna.getRanNomeIntRidVcharFormatted();
    }

    @Override
    public void setNomeIntRidVchar(String nomeIntRidVchar) {
        this.rappAna.setRanNomeIntRidVcharFormatted(nomeIntRidVchar);
    }

    @Override
    public String getNomeIntRidVcharObj() {
        if (ws.getIndRappAna().getNomeIntRid() >= 0) {
            return getNomeIntRidVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNomeIntRidVcharObj(String nomeIntRidVcharObj) {
        if (nomeIntRidVcharObj != null) {
            setNomeIntRidVchar(nomeIntRidVcharObj);
            ws.getIndRappAna().setNomeIntRid(((short)0));
        }
        else {
            ws.getIndRappAna().setNomeIntRid(((short)-1));
        }
    }

    @Override
    public String getNomeIntTrattVchar() {
        return rappAna.getRanNomeIntTrattVcharFormatted();
    }

    @Override
    public void setNomeIntTrattVchar(String nomeIntTrattVchar) {
        this.rappAna.setRanNomeIntTrattVcharFormatted(nomeIntTrattVchar);
    }

    @Override
    public String getNomeIntTrattVcharObj() {
        if (ws.getIndRappAna().getNomeIntTratt() >= 0) {
            return getNomeIntTrattVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNomeIntTrattVcharObj(String nomeIntTrattVcharObj) {
        if (nomeIntTrattVcharObj != null) {
            setNomeIntTrattVchar(nomeIntTrattVcharObj);
            ws.getIndRappAna().setNomeIntTratt(((short)0));
        }
        else {
            ws.getIndRappAna().setNomeIntTratt(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcNelRapp() {
        return rappAna.getRanPcNelRapp().getRanPcNelRapp();
    }

    @Override
    public void setPcNelRapp(AfDecimal pcNelRapp) {
        this.rappAna.getRanPcNelRapp().setRanPcNelRapp(pcNelRapp.copy());
    }

    @Override
    public AfDecimal getPcNelRappObj() {
        if (ws.getIndRappAna().getPcNelRapp() >= 0) {
            return getPcNelRapp();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcNelRappObj(AfDecimal pcNelRappObj) {
        if (pcNelRappObj != null) {
            setPcNelRapp(new AfDecimal(pcNelRappObj, 6, 3));
            ws.getIndRappAna().setPcNelRapp(((short)0));
        }
        else {
            ws.getIndRappAna().setPcNelRapp(((short)-1));
        }
    }

    @Override
    public String getRanCodSogg() {
        return rappAna.getRanCodSogg();
    }

    @Override
    public void setRanCodSogg(String ranCodSogg) {
        this.rappAna.setRanCodSogg(ranCodSogg);
    }

    @Override
    public String getRanCodSoggObj() {
        if (ws.getIndRappAna().getCodSogg() >= 0) {
            return getRanCodSogg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRanCodSoggObj(String ranCodSoggObj) {
        if (ranCodSoggObj != null) {
            setRanCodSogg(ranCodSoggObj);
            ws.getIndRappAna().setCodSogg(((short)0));
        }
        else {
            ws.getIndRappAna().setCodSogg(((short)-1));
        }
    }

    @Override
    public long getRanDsRiga() {
        return rappAna.getRanDsRiga();
    }

    @Override
    public void setRanDsRiga(long ranDsRiga) {
        this.rappAna.setRanDsRiga(ranDsRiga);
    }

    @Override
    public int getRanIdOgg() {
        return rappAna.getRanIdOgg();
    }

    @Override
    public void setRanIdOgg(int ranIdOgg) {
        this.rappAna.setRanIdOgg(ranIdOgg);
    }

    @Override
    public String getRanTpOgg() {
        return rappAna.getRanTpOgg();
    }

    @Override
    public void setRanTpOgg(String ranTpOgg) {
        this.rappAna.setRanTpOgg(ranTpOgg);
    }

    @Override
    public String getRanTpRappAna() {
        return rappAna.getRanTpRappAna();
    }

    @Override
    public void setRanTpRappAna(String ranTpRappAna) {
        this.rappAna.setRanTpRappAna(ranTpRappAna);
    }

    @Override
    public char getSex() {
        return rappAna.getRanSex();
    }

    @Override
    public void setSex(char sex) {
        this.rappAna.setRanSex(sex);
    }

    @Override
    public Character getSexObj() {
        if (ws.getIndRappAna().getSex() >= 0) {
            return ((Character)getSex());
        }
        else {
            return null;
        }
    }

    @Override
    public void setSexObj(Character sexObj) {
        if (sexObj != null) {
            setSex(((char)sexObj));
            ws.getIndRappAna().setSex(((short)0));
        }
        else {
            ws.getIndRappAna().setSex(((short)-1));
        }
    }

    @Override
    public String getTpAdegz() {
        return rappAna.getRanTpAdegz();
    }

    @Override
    public void setTpAdegz(String tpAdegz) {
        this.rappAna.setRanTpAdegz(tpAdegz);
    }

    @Override
    public String getTpAdegzObj() {
        if (ws.getIndRappAna().getTpAdegz() >= 0) {
            return getTpAdegz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAdegzObj(String tpAdegzObj) {
        if (tpAdegzObj != null) {
            setTpAdegz(tpAdegzObj);
            ws.getIndRappAna().setTpAdegz(((short)0));
        }
        else {
            ws.getIndRappAna().setTpAdegz(((short)-1));
        }
    }

    @Override
    public String getTpCausRid() {
        return rappAna.getRanTpCausRid();
    }

    @Override
    public void setTpCausRid(String tpCausRid) {
        this.rappAna.setRanTpCausRid(tpCausRid);
    }

    @Override
    public String getTpCausRidObj() {
        if (ws.getIndRappAna().getTpCausRid() >= 0) {
            return getTpCausRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCausRidObj(String tpCausRidObj) {
        if (tpCausRidObj != null) {
            setTpCausRid(tpCausRidObj);
            ws.getIndRappAna().setTpCausRid(((short)0));
        }
        else {
            ws.getIndRappAna().setTpCausRid(((short)-1));
        }
    }

    @Override
    public String getTpIndPrinc() {
        return rappAna.getRanTpIndPrinc();
    }

    @Override
    public void setTpIndPrinc(String tpIndPrinc) {
        this.rappAna.setRanTpIndPrinc(tpIndPrinc);
    }

    @Override
    public String getTpIndPrincObj() {
        if (ws.getIndRappAna().getTpIndPrinc() >= 0) {
            return getTpIndPrinc();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpIndPrincObj(String tpIndPrincObj) {
        if (tpIndPrincObj != null) {
            setTpIndPrinc(tpIndPrincObj);
            ws.getIndRappAna().setTpIndPrinc(((short)0));
        }
        else {
            ws.getIndRappAna().setTpIndPrinc(((short)-1));
        }
    }

    @Override
    public String getTpLegaleRappr() {
        return rappAna.getRanTpLegaleRappr();
    }

    @Override
    public void setTpLegaleRappr(String tpLegaleRappr) {
        this.rappAna.setRanTpLegaleRappr(tpLegaleRappr);
    }

    @Override
    public String getTpLegaleRapprObj() {
        if (ws.getIndRappAna().getTpLegaleRappr() >= 0) {
            return getTpLegaleRappr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpLegaleRapprObj(String tpLegaleRapprObj) {
        if (tpLegaleRapprObj != null) {
            setTpLegaleRappr(tpLegaleRapprObj);
            ws.getIndRappAna().setTpLegaleRappr(((short)0));
        }
        else {
            ws.getIndRappAna().setTpLegaleRappr(((short)-1));
        }
    }

    @Override
    public String getTpMezPagAccr() {
        return rappAna.getRanTpMezPagAccr();
    }

    @Override
    public void setTpMezPagAccr(String tpMezPagAccr) {
        this.rappAna.setRanTpMezPagAccr(tpMezPagAccr);
    }

    @Override
    public String getTpMezPagAccrObj() {
        if (ws.getIndRappAna().getTpMezPagAccr() >= 0) {
            return getTpMezPagAccr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMezPagAccrObj(String tpMezPagAccrObj) {
        if (tpMezPagAccrObj != null) {
            setTpMezPagAccr(tpMezPagAccrObj);
            ws.getIndRappAna().setTpMezPagAccr(((short)0));
        }
        else {
            ws.getIndRappAna().setTpMezPagAccr(((short)-1));
        }
    }

    @Override
    public String getTpMezPagAdd() {
        return rappAna.getRanTpMezPagAdd();
    }

    @Override
    public void setTpMezPagAdd(String tpMezPagAdd) {
        this.rappAna.setRanTpMezPagAdd(tpMezPagAdd);
    }

    @Override
    public String getTpMezPagAddObj() {
        if (ws.getIndRappAna().getTpMezPagAdd() >= 0) {
            return getTpMezPagAdd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpMezPagAddObj(String tpMezPagAddObj) {
        if (tpMezPagAddObj != null) {
            setTpMezPagAdd(tpMezPagAddObj);
            ws.getIndRappAna().setTpMezPagAdd(((short)0));
        }
        else {
            ws.getIndRappAna().setTpMezPagAdd(((short)-1));
        }
    }

    @Override
    public char getTpPers() {
        return rappAna.getRanTpPers();
    }

    @Override
    public void setTpPers(char tpPers) {
        this.rappAna.setRanTpPers(tpPers);
    }

    @Override
    public Character getTpPersObj() {
        if (ws.getIndRappAna().getTpPers() >= 0) {
            return ((Character)getTpPers());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPersObj(Character tpPersObj) {
        if (tpPersObj != null) {
            setTpPers(((char)tpPersObj));
            ws.getIndRappAna().setTpPers(((short)0));
        }
        else {
            ws.getIndRappAna().setTpPers(((short)-1));
        }
    }

    @Override
    public String getTpStatRid() {
        return rappAna.getRanTpStatRid();
    }

    @Override
    public void setTpStatRid(String tpStatRid) {
        this.rappAna.setRanTpStatRid(tpStatRid);
    }

    @Override
    public String getTpStatRidObj() {
        if (ws.getIndRappAna().getTpStatRid() >= 0) {
            return getTpStatRid();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStatRidObj(String tpStatRidObj) {
        if (tpStatRidObj != null) {
            setTpStatRid(tpStatRidObj);
            ws.getIndRappAna().setTpStatRid(((short)0));
        }
        else {
            ws.getIndRappAna().setTpStatRid(((short)-1));
        }
    }

    @Override
    public String getTpUtlzIndir1() {
        return rappAna.getRanTpUtlzIndir1();
    }

    @Override
    public void setTpUtlzIndir1(String tpUtlzIndir1) {
        this.rappAna.setRanTpUtlzIndir1(tpUtlzIndir1);
    }

    @Override
    public String getTpUtlzIndir1Obj() {
        if (ws.getIndRappAna().getTpUtlzIndir1() >= 0) {
            return getTpUtlzIndir1();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpUtlzIndir1Obj(String tpUtlzIndir1Obj) {
        if (tpUtlzIndir1Obj != null) {
            setTpUtlzIndir1(tpUtlzIndir1Obj);
            ws.getIndRappAna().setTpUtlzIndir1(((short)0));
        }
        else {
            ws.getIndRappAna().setTpUtlzIndir1(((short)-1));
        }
    }

    @Override
    public String getTpUtlzIndir2() {
        return rappAna.getRanTpUtlzIndir2();
    }

    @Override
    public void setTpUtlzIndir2(String tpUtlzIndir2) {
        this.rappAna.setRanTpUtlzIndir2(tpUtlzIndir2);
    }

    @Override
    public String getTpUtlzIndir2Obj() {
        if (ws.getIndRappAna().getTpUtlzIndir2() >= 0) {
            return getTpUtlzIndir2();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpUtlzIndir2Obj(String tpUtlzIndir2Obj) {
        if (tpUtlzIndir2Obj != null) {
            setTpUtlzIndir2(tpUtlzIndir2Obj);
            ws.getIndRappAna().setTpUtlzIndir2(((short)0));
        }
        else {
            ws.getIndRappAna().setTpUtlzIndir2(((short)-1));
        }
    }

    @Override
    public String getTpUtlzIndir3() {
        return rappAna.getRanTpUtlzIndir3();
    }

    @Override
    public void setTpUtlzIndir3(String tpUtlzIndir3) {
        this.rappAna.setRanTpUtlzIndir3(tpUtlzIndir3);
    }

    @Override
    public String getTpUtlzIndir3Obj() {
        if (ws.getIndRappAna().getTpUtlzIndir3() >= 0) {
            return getTpUtlzIndir3();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpUtlzIndir3Obj(String tpUtlzIndir3Obj) {
        if (tpUtlzIndir3Obj != null) {
            setTpUtlzIndir3(tpUtlzIndir3Obj);
            ws.getIndRappAna().setTpUtlzIndir3(((short)0));
        }
        else {
            ws.getIndRappAna().setTpUtlzIndir3(((short)-1));
        }
    }

    @Override
    public String getTpVarzPagat() {
        return rappAna.getRanTpVarzPagat();
    }

    @Override
    public void setTpVarzPagat(String tpVarzPagat) {
        this.rappAna.setRanTpVarzPagat(tpVarzPagat);
    }

    @Override
    public String getTpVarzPagatObj() {
        if (ws.getIndRappAna().getTpVarzPagat() >= 0) {
            return getTpVarzPagat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpVarzPagatObj(String tpVarzPagatObj) {
        if (tpVarzPagatObj != null) {
            setTpVarzPagat(tpVarzPagatObj);
            ws.getIndRappAna().setTpVarzPagat(((short)0));
        }
        else {
            ws.getIndRappAna().setTpVarzPagat(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
