package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.VarFunzDiCalcDao;
import it.accenture.jnais.commons.data.to.IVarFunzDiCalc;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbs0270Data;
import it.accenture.jnais.ws.redefines.Ac5Isprecalc;
import it.accenture.jnais.ws.VarFunzDiCalcR;

/**Original name: LDBS0270<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  12 MAR 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULI PER ACCESSO RISORSE DB               *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs0270 extends Program implements IVarFunzDiCalc {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private VarFunzDiCalcDao varFunzDiCalcDao = new VarFunzDiCalcDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs0270Data ws = new Ldbs0270Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: VAR-FUNZ-DI-CALC
    private VarFunzDiCalcR varFunzDiCalc;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS0270_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, VarFunzDiCalcR varFunzDiCalc) {
        this.idsv0003 = idsv0003;
        this.varFunzDiCalc = varFunzDiCalc;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
        //              END-EVALUATE
        //           ELSE
        //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-IF.
        if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
            // COB_CODE: EVALUATE TRUE
            //              WHEN IDSV0003-PRIMARY-KEY
            //                 PERFORM A200-ELABORA-PK       THRU A200-EX
            //              WHEN OTHER
            //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            //           END-EVALUATE
            switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK       THRU A200-EX
                    a200ElaboraPk();
                    break;

                default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                    this.idsv0003.getReturnCode().setInvalidLevelOper();
                    break;
            }
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
            this.idsv0003.getReturnCode().setInvalidLevelOper();
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs0270 getInstance() {
        return ((Ldbs0270)Programs.getInstance(Ldbs0270.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS0270'               TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS0270");
        // COB_CODE: MOVE 'VAR_FUNZ_DI_CALC'       TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("VAR_FUNZ_DI_CALC");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: MOVE ZEROES                  TO WS-TIMESTAMP-NUM.
        ws.getWsTimestamp().setWsTimestampNum(0);
        // COB_CODE: ACCEPT WS-TIMESTAMP(1:8)     FROM DATE YYYYMMDD.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getDateYYYYMMDD(), 1, 8));
        // COB_CODE: ACCEPT WS-TIMESTAMP(9:6)     FROM TIME.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getTimeHHMMSSMM(), 9, 6));
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-SELECT
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isSelect()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM Z300-LENGTH-VCHAR       THRU Z300-EX
            z300LengthVchar();
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 IDCOMP
        //                ,CODPROD
        //                ,CODTARI
        //                ,DINIZ
        //                ,NOMEFUNZ
        //                ,DEND
        //                ,ISPRECALC
        //                ,NOMEPROGR
        //                ,MODCALC
        //                ,GLOVARLIST
        //                ,STEP_ELAB
        //             INTO
        //                  :VFC-IDCOMP
        //                 ,:VFC-CODPROD-VCHAR
        //                 ,:VFC-CODTARI-VCHAR
        //                 ,:VFC-DINIZ
        //                 ,:VFC-NOMEFUNZ-VCHAR
        //                 ,:VFC-DEND
        //                 ,:VFC-ISPRECALC
        //                  :IND-VFC-ISPRECALC
        //                 ,:VFC-NOMEPROGR-VCHAR
        //                  :IND-VFC-NOMEPROGR
        //                 ,:VFC-MODCALC-VCHAR
        //                  :IND-VFC-MODCALC
        //                 ,:VFC-GLOVARLIST-VCHAR
        //                  :IND-VFC-GLOVARLIST
        //                 ,:VFC-STEP-ELAB-VCHAR
        //             FROM VAR_FUNZ_DI_CALC
        //             WHERE     IDCOMP    = :VFC-IDCOMP
        //                   AND CODPROD   = :VFC-CODPROD-VCHAR
        //                   AND CODTARI   = :VFC-CODTARI-VCHAR
        //                   AND DINIZ    <= :VFC-DINIZ
        //                   AND DEND     >= :VFC-DINIZ
        //                   AND NOMEFUNZ  = :VFC-NOMEFUNZ-VCHAR
        //                   AND STEP_ELAB = :VFC-STEP-ELAB-VCHAR
        //           END-EXEC.
        varFunzDiCalcDao.selectRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-VFC-ISPRECALC = -1
        //              MOVE HIGH-VALUES TO VFC-ISPRECALC-NULL
        //           END-IF
        if (ws.getIndVarFunzDiCalc().getCodBlocco() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VFC-ISPRECALC-NULL
            varFunzDiCalc.getAc5Isprecalc().setAc5IsprecalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Ac5Isprecalc.Len.AC5_ISPRECALC_NULL));
        }
        // COB_CODE: IF IND-VFC-NOMEPROGR = -1
        //              MOVE HIGH-VALUES TO VFC-NOMEPROGR
        //           END-IF
        if (ws.getIndVarFunzDiCalc().getSrvzVerAnn() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VFC-NOMEPROGR
            varFunzDiCalc.setAc5Nomeprogr(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VarFunzDiCalcR.Len.AC5_NOMEPROGR));
        }
        // COB_CODE: IF IND-VFC-MODCALC = -1
        //              MOVE HIGH-VALUES TO VFC-MODCALC
        //           END-IF
        if (ws.getIndVarFunzDiCalc().getWhereCondition() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VFC-MODCALC
            varFunzDiCalc.setAc5Modcalc(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VarFunzDiCalcR.Len.AC5_MODCALC));
        }
        // COB_CODE: IF IND-VFC-GLOVARLIST = -1
        //              MOVE HIGH-VALUES TO VFC-GLOVARLIST
        //           END-IF.
        if (ws.getIndVarFunzDiCalc().getFlPoliIfp() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO VFC-GLOVARLIST
            varFunzDiCalc.setAc5Glovarlist(LiteralGenerator.create(Types.HIGH_CHAR_VAL, VarFunzDiCalcR.Len.AC5_GLOVARLIST));
        }
    }

    /**Original name: Z300-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z300LengthVchar() {
        // COB_CODE: MOVE LENGTH OF VFC-CODPROD
        //                       TO VFC-CODPROD-LEN
        varFunzDiCalc.setAc5CodprodLen(((short)VarFunzDiCalcR.Len.AC5_CODPROD));
        // COB_CODE: MOVE LENGTH OF VFC-NOMEFUNZ
        //                       TO VFC-NOMEFUNZ-LEN
        varFunzDiCalc.setAc5NomefunzLen(((short)VarFunzDiCalcR.Len.AC5_NOMEFUNZ));
        // COB_CODE: MOVE LENGTH OF VFC-NOMEPROGR
        //                       TO VFC-NOMEPROGR-LEN
        varFunzDiCalc.setAc5NomeprogrLen(((short)VarFunzDiCalcR.Len.AC5_NOMEPROGR));
        // COB_CODE: MOVE LENGTH OF VFC-MODCALC
        //                       TO VFC-MODCALC-LEN
        varFunzDiCalc.setAc5ModcalcLen(((short)VarFunzDiCalcR.Len.AC5_MODCALC));
        // COB_CODE: MOVE LENGTH OF VFC-STEP-ELAB
        //                       TO VFC-STEP-ELAB-LEN
        varFunzDiCalc.setAc5StepElabLen(((short)VarFunzDiCalcR.Len.AC5_STEP_ELAB));
        // COB_CODE: MOVE LENGTH OF VFC-GLOVARLIST
        //                       TO VFC-GLOVARLIST-LEN
        varFunzDiCalc.setAc5GlovarlistLen(((short)VarFunzDiCalcR.Len.AC5_GLOVARLIST));
        // COB_CODE: MOVE LENGTH OF VFC-CODTARI
        //                       TO VFC-CODTARI-LEN.
        varFunzDiCalc.setAc5CodtariLen(((short)VarFunzDiCalcR.Len.AC5_CODTARI));
    }

    @Override
    public String getCodprodVchar() {
        return varFunzDiCalc.getAc5CodprodVcharFormatted();
    }

    @Override
    public void setCodprodVchar(String codprodVchar) {
        this.varFunzDiCalc.setAc5CodprodVcharFormatted(codprodVchar);
    }

    @Override
    public String getCodtariVchar() {
        return varFunzDiCalc.getAc5CodtariVcharFormatted();
    }

    @Override
    public void setCodtariVchar(String codtariVchar) {
        this.varFunzDiCalc.setAc5CodtariVcharFormatted(codtariVchar);
    }

    @Override
    public int getDend() {
        return varFunzDiCalc.getAc5Dend();
    }

    @Override
    public void setDend(int dend) {
        this.varFunzDiCalc.setAc5Dend(dend);
    }

    @Override
    public int getDiniz() {
        return varFunzDiCalc.getAc5Diniz();
    }

    @Override
    public void setDiniz(int diniz) {
        this.varFunzDiCalc.setAc5Diniz(diniz);
    }

    @Override
    public String getGlovarlistVchar() {
        return varFunzDiCalc.getAc5GlovarlistVcharFormatted();
    }

    @Override
    public void setGlovarlistVchar(String glovarlistVchar) {
        this.varFunzDiCalc.setAc5GlovarlistVcharFormatted(glovarlistVchar);
    }

    @Override
    public String getGlovarlistVcharObj() {
        if (ws.getIndVarFunzDiCalc().getFlPoliIfp() >= 0) {
            return getGlovarlistVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setGlovarlistVcharObj(String glovarlistVcharObj) {
        if (glovarlistVcharObj != null) {
            setGlovarlistVchar(glovarlistVcharObj);
            ws.getIndVarFunzDiCalc().setFlPoliIfp(((short)0));
        }
        else {
            ws.getIndVarFunzDiCalc().setFlPoliIfp(((short)-1));
        }
    }

    @Override
    public short getIdcomp() {
        return varFunzDiCalc.getAc5Idcomp();
    }

    @Override
    public void setIdcomp(short idcomp) {
        this.varFunzDiCalc.setAc5Idcomp(idcomp);
    }

    @Override
    public short getIsprecalc() {
        return varFunzDiCalc.getAc5Isprecalc().getAc5Isprecalc();
    }

    @Override
    public void setIsprecalc(short isprecalc) {
        this.varFunzDiCalc.getAc5Isprecalc().setAc5Isprecalc(isprecalc);
    }

    @Override
    public Short getIsprecalcObj() {
        if (ws.getIndVarFunzDiCalc().getCodBlocco() >= 0) {
            return ((Short)getIsprecalc());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIsprecalcObj(Short isprecalcObj) {
        if (isprecalcObj != null) {
            setIsprecalc(((short)isprecalcObj));
            ws.getIndVarFunzDiCalc().setCodBlocco(((short)0));
        }
        else {
            ws.getIndVarFunzDiCalc().setCodBlocco(((short)-1));
        }
    }

    @Override
    public String getModcalcVchar() {
        return varFunzDiCalc.getAc5ModcalcVcharFormatted();
    }

    @Override
    public void setModcalcVchar(String modcalcVchar) {
        this.varFunzDiCalc.setAc5ModcalcVcharFormatted(modcalcVchar);
    }

    @Override
    public String getModcalcVcharObj() {
        if (ws.getIndVarFunzDiCalc().getWhereCondition() >= 0) {
            return getModcalcVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModcalcVcharObj(String modcalcVcharObj) {
        if (modcalcVcharObj != null) {
            setModcalcVchar(modcalcVcharObj);
            ws.getIndVarFunzDiCalc().setWhereCondition(((short)0));
        }
        else {
            ws.getIndVarFunzDiCalc().setWhereCondition(((short)-1));
        }
    }

    @Override
    public String getNomefunzVchar() {
        return varFunzDiCalc.getAc5NomefunzVcharFormatted();
    }

    @Override
    public void setNomefunzVchar(String nomefunzVchar) {
        this.varFunzDiCalc.setAc5NomefunzVcharFormatted(nomefunzVchar);
    }

    @Override
    public String getNomeprogrVchar() {
        return varFunzDiCalc.getAc5NomeprogrVcharFormatted();
    }

    @Override
    public void setNomeprogrVchar(String nomeprogrVchar) {
        this.varFunzDiCalc.setAc5NomeprogrVcharFormatted(nomeprogrVchar);
    }

    @Override
    public String getNomeprogrVcharObj() {
        if (ws.getIndVarFunzDiCalc().getSrvzVerAnn() >= 0) {
            return getNomeprogrVchar();
        }
        else {
            return null;
        }
    }

    @Override
    public void setNomeprogrVcharObj(String nomeprogrVcharObj) {
        if (nomeprogrVcharObj != null) {
            setNomeprogrVchar(nomeprogrVcharObj);
            ws.getIndVarFunzDiCalc().setSrvzVerAnn(((short)0));
        }
        else {
            ws.getIndVarFunzDiCalc().setSrvzVerAnn(((short)-1));
        }
    }

    @Override
    public String getStepElabVchar() {
        return varFunzDiCalc.getAc5StepElabVcharFormatted();
    }

    @Override
    public void setStepElabVchar(String stepElabVchar) {
        this.varFunzDiCalc.setAc5StepElabVcharFormatted(stepElabVchar);
    }
}
