package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs3260Data;

/**Original name: LVVS3260<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2017.
 * DATE-COMPILED.
 * **------------------------------------------------------------***</pre>*/
public class Lvvs3260 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3260Data ws = new Lvvs3260Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS3260
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3260_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3260 getInstance() {
        return ((Lvvs3260)Programs.getInstance(Lvvs3260.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             WK-VARIABILI.
        initIxIndici();
        initTabOutput();
        initWkVariabili();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //                AND IDSV0003-SUCCESSFUL-SQL
        //           * --     LETTURA MOVI PER VERIFCA PRESENZA SWITCH DI LINEA
        //                      TO IVVC0213-VAL-IMP-O
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // --     LETTURA MOVI PER VERIFCA PRESENZA SWITCH DI LINEA
            // COB_CODE: PERFORM A000-LEGGI-E06
            //              THRU A000-LEGGI-E06-EX
            a000LeggiE06();
            // COB_CODE: MOVE WK-CAP-PROT
            //             TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getWkCapProt(), 18, 7));
        }
    }

    /**Original name: A000-LEGGI-E06<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA ESTENSIONE POLIZZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a000LeggiE06() {
        Idbse060 idbse060 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE EST-POLI.
        initEstPoli();
        // COB_CODE: MOVE DPOL-ID-POLI             TO E06-ID-POLI
        ws.getEstPoli().setE06IdPoli(ws.getLccvpol1().getDati().getWpolIdPoli());
        // COB_CODE: MOVE SPACES                   TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond("");
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        // COB_CODE: SET IDSV0003-ID-PADRE           TO TRUE.
        idsv0003.getLivelloOperazione().setIdsi0011IdPadre();
        // COB_CODE: SET IDSV0003-SELECT             TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC      TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL     TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: CALL PGM-IDBSE060  USING  IDSV0003 EST-POLI
        //           ON EXCEPTION
        //              SET IDSV0003-INVALID-OPER TO TRUE
        //           END-CALL.
        try {
            idbse060 = Idbse060.getInstance();
            idbse060.run(idsv0003, ws.getEstPoli());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE PGM-IDBSE060
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getPgmIdbse060());
            // COB_CODE: MOVE 'CALL IDBSE060 ERRORE CHIAMATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("CALL IDBSE060 ERRORE CHIAMATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           ELSE
        //            END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: IF E06-CPT-PROTETTO-NULL NOT = HIGH-VALUE
            //                                      AND LOW-VALUE AND SPACES
            //                TO WK-CAP-PROT
            //           ELSE
            //                TO WK-CAP-PROT
            //           END-IF
            if (!Characters.EQ_HIGH.test(ws.getEstPoli().getE06CptProtetto().getE06CptProtettoNullFormatted()) && !Characters.EQ_LOW.test(ws.getEstPoli().getE06CptProtetto().getE06CptProtettoNullFormatted()) && !Characters.EQ_SPACE.test(ws.getEstPoli().getE06CptProtetto().getE06CptProtettoNull())) {
                // COB_CODE: MOVE E06-CPT-PROTETTO
                //             TO WK-CAP-PROT
                ws.setWkCapProt(Trunc.toDecimal(ws.getEstPoli().getE06CptProtetto().getE06CptProtetto(), 18, 7));
            }
            else {
                // COB_CODE: MOVE ZEROES
                //             TO WK-CAP-PROT
                ws.setWkCapProt(new AfDecimal(0, 18, 7));
            }
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF  IDSV0003-NOT-FOUND
            //            END-IF
            //           ELSE
            //             END-STRING
            //           END-IF
            // COB_CODE: IF DPOL-FL-POLI-IFP = 'G'
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           ELSE
            //              TO  IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ws.getLccvpol1().getDati().getWpolFlPoliIfp() == 'G') {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE PGM-IDBSE060             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getPgmIdbse060());
                // COB_CODE: MOVE   'MANCA LA VARIABILE CAPPROTETTO'
                //             TO  IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("MANCA LA VARIABILE CAPPROTETTO");
            }
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER     TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE PGM-IDBSE060             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getPgmIdbse060());
            // COB_CODE: STRING 'CHIAMATA IDBSE060 ;'
            //                  IDSV0003-RETURN-CODE ';'
            //                  IDSV0003-SQLCODE
            //                  DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
            //           END-STRING
            concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA IDBSE060 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
            idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLI
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLI
            ws.setDpolAreaPoliFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initWkVariabili() {
        ws.setWkCapProt(new AfDecimal(0, 18, 7));
    }

    public void initEstPoli() {
        ws.getEstPoli().setE06IdEstPoli("");
        ws.getEstPoli().setE06IdPoli(0);
        ws.getEstPoli().setE06IbOgg("");
        ws.getEstPoli().setE06IbProp("");
        ws.getEstPoli().setE06IdMoviCrz(0);
        ws.getEstPoli().getE06IdMoviChiu().setE06IdMoviChiu(0);
        ws.getEstPoli().setE06DtIniEff(0);
        ws.getEstPoli().setE06DtEndEff(0);
        ws.getEstPoli().setE06CodCompAnia(0);
        ws.getEstPoli().setE06DsRiga(0);
        ws.getEstPoli().setE06DsOperSql(Types.SPACE_CHAR);
        ws.getEstPoli().setE06DsVer(0);
        ws.getEstPoli().setE06DsTsIniCptz(0);
        ws.getEstPoli().setE06DsTsEndCptz(0);
        ws.getEstPoli().setE06DsUtente("");
        ws.getEstPoli().setE06DsStatoElab(Types.SPACE_CHAR);
        ws.getEstPoli().getE06DtUltPerd().setE06DtUltPerd(0);
        ws.getEstPoli().getE06PcUltPerd().setE06PcUltPerd(new AfDecimal(0, 6, 3));
        ws.getEstPoli().setE06FlEsclSwitchMax(Types.SPACE_CHAR);
        ws.getEstPoli().setE06EsiAdegzIsvap("");
        ws.getEstPoli().setE06NumIna("");
        ws.getEstPoli().setE06TpModProv("");
        ws.getEstPoli().getE06CptProtetto().setE06CptProtetto(new AfDecimal(0, 15, 3));
        ws.getEstPoli().getE06CumPreAttTakeP().setE06CumPreAttTakeP(new AfDecimal(0, 15, 3));
        ws.getEstPoli().getE06DtEmisPartner().setE06DtEmisPartner(0);
    }
}
