package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.util.date.CalendarUtil;
import com.bphx.ctu.af.util.Functions;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.BtcExeMessageDao;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.BtcExeMessage;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Iabs0090Data;
import it.accenture.jnais.ws.Idsv0003;

/**Original name: IABS0090<br>
 * <pre>AUTHOR.        ATS NAPOLI.
 * DATE-WRITTEN.  MAGGIO 2007.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULI PER ACCESSO RISORSE DB               *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Iabs0090 extends Program {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private BtcExeMessageDao btcExeMessageDao = new BtcExeMessageDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Iabs0090Data ws = new Iabs0090Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: BTC-EXE-MESSAGE
    private BtcExeMessage btcExeMessage;

    //==== METHODS ====
    /**Original name: PROGRAM_IABS0090_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, BtcExeMessage btcExeMessage) {
        this.idsv0003 = idsv0003;
        this.btcExeMessage = btcExeMessage;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-PRIMARY-KEY
        //                 PERFORM A200-ELABORA-PK       THRU A200-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
        //           END-EVALUATE
        switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

            case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK       THRU A200-EX
                a200ElaboraPk();
                break;

            default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
                break;
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Iabs0090 getInstance() {
        return ((Iabs0090)Programs.getInstance(Iabs0090.class));
    }

    /**Original name: A000-INIZIO<br>
	 * <pre>*****************************************************************</pre>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IABS0090'             TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IABS0090");
        // COB_CODE: MOVE 'BTC_EXE_MESSAGE'           TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("BTC_EXE_MESSAGE");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: MOVE ZEROES                  TO WS-TIMESTAMP-NUM.
        ws.getWsTimestamp().setWsTimestampNum(0);
        // COB_CODE: ACCEPT WS-TIMESTAMP(1:8)     FROM DATE YYYYMMDD.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getDateYYYYMMDD(), 1, 8));
        // COB_CODE: ACCEPT WS-TIMESTAMP(9:6)     FROM TIME.
        ws.getWsTimestamp().setWsTimestamp(Functions.setSubstring(ws.getWsTimestamp().getWsTimestamp(), CalendarUtil.getTimeHHMMSSMM(), 9, 6));
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>
	 * <pre>*****************************************************************</pre>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a210SelectPk() {
        // COB_CODE: EXEC SQL
        //             SELECT
        //                 ID_BATCH
        //                ,ID_JOB
        //                ,ID_EXECUTION
        //                ,ID_MESSAGE
        //                ,COD_MESSAGE
        //                ,MESSAGE
        //             INTO
        //                  :BEM-ID-BATCH
        //                 ,:BEM-ID-JOB
        //                 ,:BEM-ID-EXECUTION
        //                 ,:BEM-ID-MESSAGE
        //                 ,:BEM-COD-MESSAGE
        //                 ,:BEM-MESSAGE-VCHAR
        //             FROM BTC_EXE_MESSAGE
        //             WHERE     ID_BATCH     = :BEM-ID-BATCH
        //                   AND ID_JOB       = :BEM-ID-JOB
        //                   AND ID_EXECUTION = :BEM-ID-EXECUTION
        //                   AND ID_MESSAGE   = :BEM-ID-MESSAGE
        //           END-EXEC.
        btcExeMessageDao.selectRec(btcExeMessage.getIdBatch(), btcExeMessage.getIdJob(), btcExeMessage.getIdExecution(), btcExeMessage.getIdMessage(), btcExeMessage);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
        }
    }

    /**Original name: A220-INSERT-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z080-ESTRAI-ID-MESSAGE     THRU Z080-EX.
        z080EstraiIdMessage();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EXEC
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO BTC_EXE_MESSAGE
            //                  (
            //                     ID_BATCH
            //                    ,ID_JOB
            //                    ,ID_EXECUTION
            //                    ,ID_MESSAGE
            //                    ,COD_MESSAGE
            //                    ,MESSAGE
            //                  )
            //              VALUES
            //                  (
            //                     :BEM-ID-BATCH
            //                    ,:BEM-ID-JOB
            //                    ,:BEM-ID-EXECUTION
            //                    ,:BEM-ID-MESSAGE
            //                    ,:BEM-COD-MESSAGE
            //                    ,:BEM-MESSAGE-VCHAR
            //                  )
            //           END-EXEC
            btcExeMessageDao.insertRec(btcExeMessage);
        }
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A230-UPDATE-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: EXEC SQL
        //                UPDATE BTC_EXE_MESSAGE SET
        //                   ID_BATCH               = :BEM-ID-BATCH
        //                  ,ID_JOB                 = :BEM-ID-JOB
        //                  ,ID_EXECUTION           = :BEM-ID-EXECUTION
        //                  ,ID_MESSAGE             = :BEM-ID-MESSAGE
        //                  ,COD_MESSAGE            = :BEM-COD-MESSAGE
        //                  ,MESSAGE                = :BEM-MESSAGE-VCHAR
        //                WHERE  ID_BATCH     = :BEM-ID-BATCH
        //                   AND ID_JOB       = :BEM-ID-JOB
        //                   AND ID_EXECUTION = :BEM-ID-EXECUTION
        //                   AND ID_MESSAGE   = :BEM-ID-MESSAGE
        //           END-EXEC.
        btcExeMessageDao.updateRec(btcExeMessage);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>
	 * <pre>*****************************************************************</pre>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM BTC_EXE_MESSAGE
        //                WHERE  ID_BATCH     = :BEM-ID-BATCH
        //                   AND ID_JOB       = :BEM-ID-JOB
        //                   AND ID_EXECUTION = :BEM-ID-EXECUTION
        //                   AND ID_MESSAGE   = :BEM-ID-MESSAGE
        //           END-EXEC.
        btcExeMessageDao.deleteRec(btcExeMessage.getIdBatch(), btcExeMessage.getIdJob(), btcExeMessage.getIdExecution(), btcExeMessage.getIdMessage());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z080-ESTRAI-ID-MESSAGE<br>
	 * <pre>*****************************************************************
	 * **************************************************************
	 *  STACCO DI SEQUENCE
	 * **************************************************************</pre>*/
    private void z080EstraiIdMessage() {
        Lccs0090 lccs0090 = null;
        // COB_CODE: MOVE SPACES             TO LCCC0090
        ws.getLccc0090().initLccc0090Spaces();
        // COB_CODE: MOVE 'SEQ_ID_MESSAGE'   TO LINK-NOME-TABELLA
        ws.getLccc0090().setNomeTabella("SEQ_ID_MESSAGE");
        // COB_CODE: CALL PGM-LCCS0090       USING LCCC0090
        lccs0090 = Lccs0090.getInstance();
        lccs0090.run(ws.getLccc0090());
        // COB_CODE: MOVE LINK-RETURN-CODE
        //                            TO IDSV0003-RETURN-CODE
        idsv0003.getReturnCode().setReturnCode(ws.getLccc0090().getReturnCode().getReturnCode());
        // COB_CODE: MOVE LINK-SQLCODE
        //                            TO IDSV0003-SQLCODE
        idsv0003.getSqlcode().setSqlcode(ws.getLccc0090().getSqlcode().getSqlcodeSigned());
        // COB_CODE: MOVE LINK-DESCRIZ-ERR-DB2
        //                            TO IDSV0003-DESCRIZ-ERR-DB2
        idsv0003.getCampiEsito().setDescrizErrDb2(ws.getLccc0090().getDescrizErrDb2());
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              MOVE LINK-SEQ-TABELLA TO BEM-ID-MESSAGE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE LINK-SEQ-TABELLA TO BEM-ID-MESSAGE
            btcExeMessage.setIdMessage(ws.getLccc0090().getSeqTabella());
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>
	 * <pre>*****************************************************************</pre>*/
    private void z200SetIndicatoriNull() {
    // COB_CODE: CONTINUE.
    //continue
    }
}
