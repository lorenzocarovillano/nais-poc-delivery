package it.accenture.jnais;

import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.A2kInput;
import it.accenture.jnais.copy.A2kOugbmba;
import it.accenture.jnais.copy.A2kOurid;
import it.accenture.jnais.copy.A2kOusta;
import it.accenture.jnais.copy.A2kOutput;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.copy.WvasDati;
import it.accenture.jnais.ws.A2kOuamgX;
import it.accenture.jnais.ws.A2kOugl07X;
import it.accenture.jnais.ws.A2kOugmaX;
import it.accenture.jnais.ws.A2kOujulX;
import it.accenture.jnais.ws.enums.Idsv0003Sqlcode;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0135Data;
import it.accenture.jnais.ws.redefines.WkDtRicorTrancheLvvs0135;
import it.accenture.jnais.ws.redefines.Wl19DtRilevazioneNav;
import it.accenture.jnais.ws.redefines.Wl19ValQuo;
import it.accenture.jnais.ws.redefines.Wl19ValQuoManfee;
import it.accenture.jnais.ws.redefines.WvasDtValzz;
import it.accenture.jnais.ws.redefines.WvasDtValzzCalc;
import it.accenture.jnais.ws.redefines.WvasIdMoviChiu;
import it.accenture.jnais.ws.redefines.WvasIdRichDisFnd;
import it.accenture.jnais.ws.redefines.WvasIdRichInvstFnd;
import it.accenture.jnais.ws.redefines.WvasIdTrchDiGar;
import it.accenture.jnais.ws.redefines.WvasImpMovto;
import it.accenture.jnais.ws.redefines.WvasMinusValenza;
import it.accenture.jnais.ws.redefines.WvasNumQuo;
import it.accenture.jnais.ws.redefines.WvasNumQuoLorde;
import it.accenture.jnais.ws.redefines.WvasPcInvDis;
import it.accenture.jnais.ws.redefines.WvasPlusValenza;
import it.accenture.jnais.ws.redefines.WvasPreMovto;
import it.accenture.jnais.ws.redefines.WvasValQuo;
import it.accenture.jnais.ws.WkVariabiliLvvs0135;
import static java.lang.Math.abs;

/**Original name: LVVS0135<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2009.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *     MODULO DI CALCOLO PER VARIABILE CTT
 * **------------------------------------------------------------***
 *     MODALITA' VALORIZZAZIONE:
 *     Reperire il valore del fondo alla data di ricorrenza
 *     anniversaria per ogni fondo della tranche.
 *     Se il valore non h presente per la data ricorrenza
 *     anniversaria, reperire valore presente alla data piy vicina e
 *     precedente la ricorrenza anniversaria.
 *     Reperire il numero quote di ogni fondo sulla Valore Asset.
 *     Determinare il controvalore per ogni fondo:
 *     Valore quote (L19) * Numero Quote (VAS).
 *     Sommare il controvalore dei fondi della tranche.
 *     Se il valore della variabile h zero, oppure non h possibile
 *     valorizzarla (perchi ad es. non h Ramo III),
 *     non fornire la variabile ad Actuator.
 * **------------------------------------------------------------***
 *     TABELLE:   VALORE_ASSET, QUOTAZIONE_FONDI_UNIT
 *     ATTRIBUTO: NUMERO_QUOTE
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0135 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0135Data ws = new Lvvs0135Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0007
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0135_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        //
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        //
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0135 getInstance() {
        return ((Lvvs0135)Programs.getInstance(Lvvs0135.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT
        //                                             WK-VARIABILI.
        initIxIndici();
        initTabOutput();
        initWkVariabili();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET WK-NUM-QUO-OK                 TO TRUE.
        ws.getWkNumQuo().setOk();
        // COB_CODE: INITIALIZE VAL-AST
        //                      AREA-IO-TGA
        //                      AREA-IO-GAR
        //                      AREA-IO-L19.
        initValAst();
        initAreaIoTga();
        initAreaIoGar();
        initAreaIoL19();
        // COB_CODE: MOVE ZEROES
        //             TO DL19-ELE-FND-MAX
        //                WVAS-ELE-VAL-AST-MAX
        ws.setDl19EleFndMax(((short)0));
        ws.setWvasEleValAstMax(((short)0));
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*
	 * --> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 * --> RISPETTIVE AREE DCLGEN IN WORKING</pre>*/
    private void s1000Elaborazione() {
        ConcatUtil concatUtil = null;
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.getIxIndici().setDclgen(((short)1));
        while (!(ws.getIxIndici().getDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.getIxIndici().setDclgen(Trunc.toShort(ws.getIxIndici().getDclgen() + 1, 4));
        }
        //
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTROLLO-DATI
            //              THRU S1200-CONTROLLO-DATI-EX
            s1200ControlloDati();
            //
            // COB_CODE:          IF  IDSV0003-SUCCESSFUL-RC
            //                    AND IDSV0003-SUCCESSFUL-SQL
            //           *            La variabile deve essere valorizzata
            //           *            solo per prodotti Ramo III
            //                        END-IF
            //                    END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
                //            La variabile deve essere valorizzata
                //            solo per prodotti Ramo III
                // COB_CODE:              IF DGRZ-TP-INVST(IX-GUIDA-GRZ) = 7 OR 8
                //           *               Calcolo prima la Data Ricorrenza
                //           *               Anniversaria di Tranche
                //                           END-IF
                //                        ELSE
                //           *            Scarta la variabile
                //                        END-STRING
                //                        END-IF
                if (ws.getDgrzTabGar(ws.getIxIndici().getGuidaGrz()).getLccvgrz1().getDati().getWgrzTpInvst().getWgrzTpInvst() == 7 || ws.getDgrzTabGar(ws.getIxIndici().getGuidaGrz()).getLccvgrz1().getDati().getWgrzTpInvst().getWgrzTpInvst() == 8) {
                    //               Calcolo prima la Data Ricorrenza
                    //               Anniversaria di Tranche
                    // COB_CODE: PERFORM A191-CALCOLA-DT-RICOR-TRANCHE
                    //              THRU A191-EX
                    a191CalcolaDtRicorTranche();
                    //               Lettura Fondi
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
                    //                 THRU A140-EX
                    //           END-IF
                    if (idsv0003.getReturnCode().isSuccessfulRc()) {
                        // COB_CODE: PERFORM A140-RECUP-VALORE-ASSET
                        //              THRU A140-EX
                        a140RecupValoreAsset();
                    }
                    //               Lettura Quotazioni
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
                    //                   OR NOT IDSV0003-SUCCESSFUL-RC
                    //           END-IF
                    if (idsv0003.getReturnCode().isSuccessfulRc()) {
                        // COB_CODE: PERFORM A160-RECUP-QUOTAZ-FONDO
                        //              THRU A160-EX
                        //           VARYING IX-TAB-VAS FROM 1 BY 1
                        //             UNTIL IX-TAB-VAS > WVAS-ELE-VAL-AST-MAX
                        //                OR NOT IDSV0003-SUCCESSFUL-RC
                        ws.getIxIndici().setTabVas(((short)1));
                        while (!(ws.getIxIndici().getTabVas() > ws.getWvasEleValAstMax() || !idsv0003.getReturnCode().isSuccessfulRc())) {
                            a160RecupQuotazFondo();
                            ws.getIxIndici().setTabVas(Trunc.toShort(ws.getIxIndici().getTabVas() + 1, 4));
                        }
                    }
                    //               Calcolo della variabile
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
                    //                   OR NOT IDSV0003-SUCCESSFUL-SQL
                    //           END-IF
                    if (idsv0003.getReturnCode().isSuccessfulRc()) {
                        // COB_CODE: PERFORM S1250-CALCOLA-VARIABILE
                        //              THRU S1250-EX
                        //           VARYING IX-TAB-VAS FROM 1 BY 1
                        //             UNTIL IX-TAB-VAS > WVAS-ELE-VAL-AST-MAX
                        //                OR NOT IDSV0003-SUCCESSFUL-SQL
                        ws.getIxIndici().setTabVas(((short)1));
                        while (!(ws.getIxIndici().getTabVas() > ws.getWvasEleValAstMax() || !idsv0003.getSqlcode().isSuccessfulSql())) {
                            s1250CalcolaVariabile();
                            ws.getIxIndici().setTabVas(Trunc.toShort(ws.getIxIndici().getTabVas() + 1, 4));
                        }
                    }
                }
                else {
                    //            Scarta la variabile
                    // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED      TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                    // COB_CODE: MOVE WK-PGM              TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'VARIABILE NON VALORIZZATA PERCHE'' IL '
                    //                  'PRODOTTO NON E'' DI RAMO III'
                    //           DELIMITED BY SIZE   INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "VARIABILE NON VALORIZZATA PERCHE' IL ", "PRODOTTO NON E' DI RAMO III");
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                }
            }
        }
        // COB_CODE: IF IVVC0213-VAL-IMP-O EQUAL ZERO
        //              MOVE WK-PGM              TO IDSV0003-COD-SERVIZIO-BE
        //           END-IF.
        if (ivvc0213.getTabOutput().getValImpO().compareTo(0) == 0) {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED      TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM              TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*
	 *     AREA TRANCHE</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-TRCH-GAR
        //                TO DTGA-AREA-TGA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasTrchGar())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DTGA-AREA-TGA
            ws.setDtgaAreaTgaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
        //    AREA GARANZIA
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-GARANZIA
        //                TO DGRZ-AREA-GRA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getTabAlias(), ws.getIvvc0218().getAliasGaranzia())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DGRZ-AREA-GRA
            ws.setDgrzAreaGraFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxIndici().getDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTROLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *    CONTROLLO DATI DCLGEN
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ControlloDati() {
        // COB_CODE: SET WK-ID-COD-NON-TROVATO              TO TRUE.
        ws.getWkIdCodLivFlag().setNonTrovato();
        //
        // COB_CODE:      IF DGRZ-ELE-GAR-MAX GREATER ZERO
        //           *
        //                   END-IF
        //           *
        //                ELSE
        //                     TO IDSV0003-DESCRIZ-ERR-DB2
        //                END-IF.
        if (ws.getDgrzEleGarMax() > 0) {
            //
            // COB_CODE: PERFORM VARYING IX-TAB-GRZ FROM 1 BY 1
            //             UNTIL IX-TAB-GRZ > DGRZ-ELE-GAR-MAX
            //                OR WK-ID-COD-TROVATO
            //                END-IF
            //           END-PERFORM
            ws.getIxIndici().setTabGrz(((short)1));
            while (!(ws.getIxIndici().getTabGrz() > ws.getDgrzEleGarMax() || ws.getWkIdCodLivFlag().isTrovato())) {
                // COB_CODE: IF DTGA-ID-GAR(IVVC0213-IX-TABB) =
                //              DGRZ-ID-GAR(IX-TAB-GRZ)
                //              MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                //           END-IF
                if (ws.getDtgaTabTran(ivvc0213.getIxTabb()).getLccvtga1().getDati().getWtgaIdGar() == ws.getDgrzTabGar(ws.getIxIndici().getTabGrz()).getLccvgrz1().getDati().getWgrzIdGar()) {
                    // COB_CODE: SET WK-ID-COD-TROVATO     TO TRUE
                    ws.getWkIdCodLivFlag().setTrovato();
                    // COB_CODE: MOVE IX-TAB-GRZ           TO IX-GUIDA-GRZ
                    ws.getIxIndici().setGuidaGrz(ws.getIxIndici().getTabGrz());
                }
                ws.getIxIndici().setTabGrz(Trunc.toShort(ws.getIxIndici().getTabGrz() + 1, 4));
            }
            //
            // COB_CODE: IF WK-ID-COD-NON-TROVATO
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            if (ws.getWkIdCodLivFlag().isNonTrovato()) {
                // COB_CODE: SET  IDSV0003-INVALID-OPER               TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: MOVE 'GARANZIA NON VALORIZZATA PER CALCOLO QUOTE'
                //             TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("GARANZIA NON VALORIZZATA PER CALCOLO QUOTE");
            }
            //
        }
        else {
            // COB_CODE: SET  IDSV0003-INVALID-OPER                  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'GARANZIA NON VALORIZZATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("GARANZIA NON VALORIZZATA");
        }
    }

    /**Original name: S1250-CALCOLA-VARIABILE<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA LA VARIABILE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1250CalcolaVariabile() {
        // COB_CODE: SET WK-ID-COD-NON-TROVATO         TO TRUE.
        ws.getWkIdCodLivFlag().setNonTrovato();
        //
        // COB_CODE: PERFORM VARYING IX-TAB-L19 FROM 1 BY 1
        //             UNTIL IX-TAB-L19 > DL19-ELE-FND-MAX
        //                OR WK-ID-COD-TROVATO
        //                OR WK-NUM-QUO-KO
        //                END-IF
        //           END-PERFORM.
        ws.getIxIndici().setTabL19(((short)1));
        while (!(ws.getIxIndici().getTabL19() > ws.getDl19EleFndMax() || ws.getWkIdCodLivFlag().isTrovato() || ws.getWkNumQuo().isKo())) {
            // COB_CODE: IF WVAS-COD-FND(IX-TAB-VAS) = DL19-COD-FND(IX-TAB-L19)
            //              END-IF
            //           END-IF
            if (Conditions.eq(ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasCodFnd(), ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19CodFnd())) {
                // COB_CODE: SET WK-ID-COD-TROVATO        TO TRUE
                ws.getWkIdCodLivFlag().setTrovato();
                //
                // COB_CODE:              IF DL19-VAL-QUO(IX-TAB-L19) IS NUMERIC
                //           *--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                //                            END-IF
                //                        ELSE
                //                            SET IDSV0003-INVALID-OPER  TO TRUE
                //                        END-IF
                if (Functions.isNumber(ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().getWl19ValQuo())) {
                    //--> PER IL PRIMO FONDO CHE HA NUM QUOTE A NULL ESCE DAL CICLO
                    // COB_CODE: IF WVAS-NUM-QUO(IX-TAB-VAS) IS NUMERIC
                    //                       = (IVVC0213-VAL-IMP-O + WK-VAL-FONDO)
                    //            ELSE
                    //              SET WK-NUM-QUO-KO      TO TRUE
                    //            END-IF
                    if (Functions.isNumber(ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasNumQuo().getWvasNumQuo())) {
                        // COB_CODE: MOVE ZERO              TO WK-VAL-FONDO
                        ws.setWkValFondo(new AfDecimal(0, 15, 3));
                        // COB_CODE: COMPUTE WK-VAL-FONDO =
                        //           (WVAS-NUM-QUO(IX-TAB-VAS) *
                        //            DL19-VAL-QUO(IX-TAB-L19))
                        ws.setWkValFondo(Trunc.toDecimal(ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasNumQuo().getWvasNumQuo().multiply(ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().getWl19ValQuo()), 15, 3));
                        // COB_CODE: COMPUTE IVVC0213-VAL-IMP-O
                        //                    = (IVVC0213-VAL-IMP-O + WK-VAL-FONDO)
                        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ivvc0213.getTabOutput().getValImpO().add(ws.getWkValFondo()), 18, 7));
                    }
                    else {
                        // COB_CODE: SET WK-NUM-QUO-KO      TO TRUE
                        ws.getWkNumQuo().setKo();
                    }
                }
                else {
                    // COB_CODE: MOVE LDBS4910
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
                    // COB_CODE: MOVE 'NUM-QUO / VAL-QUO NON NUMERICI'
                    //             TO IDSV0003-DESCRIZ-ERR-DB2
                    idsv0003.getCampiEsito().setDescrizErrDb2("NUM-QUO / VAL-QUO NON NUMERICI");
                    // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
            ws.getIxIndici().setTabL19(Trunc.toShort(ws.getIxIndici().getTabL19() + 1, 4));
        }
        // COB_CODE: PERFORM S1253-ARROTONDA-IMP
        //              THRU S1253-EX.
        s1253ArrotondaImp();
    }

    /**Original name: S1253-ARROTONDA-IMP<br>
	 * <pre>----------------------------------------------------------------*
	 *    ARROTONDAMENTO IMPORTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1253ArrotondaImp() {
        // COB_CODE: IF IVVC0213-VAL-IMP-O GREATER ZERO
        //              MOVE WK-AREA-ARRO                 TO IVVC0213-VAL-IMP-O
        //           ELSE
        //              SET  IDSV0003-FIELD-NOT-VALUED    TO TRUE
        //           END-IF.
        if (ivvc0213.getTabOutput().getValImpO().compareTo(0) > 0) {
            // COB_CODE: MOVE IVVC0213-VAL-IMP-O           TO WK-AREA-ARRO
            ws.setWkAreaArroFormatted(ivvc0213.getTabOutput().getIvvc0213ValImpOFormatted());
            // COB_CODE: MOVE WK-IMP-ARRO(13)              TO WK-APP-ARRO
            ws.setWkAppArroFormatted(String.valueOf(ws.getWkTabArro(13).getWkImpArro()));
            // COB_CODE: MOVE WK-IMP-ARRO(14)              TO WK-APP-ARRO-1
            ws.setWkAppArro1Formatted(String.valueOf(ws.getWkTabArro(14).getWkImpArro()));
            // COB_CODE: IF WK-APP-ARRO-1 NOT LESS 5
            //              MOVE ZEROES                    TO WK-IMP-ARRO(14)
            //           ELSE
            //              MOVE ZEROES                    TO WK-IMP-ARRO(14)
            //           END-IF
            if (ws.getWkAppArro1() >= 5) {
                // COB_CODE: ADD 1                          TO WK-APP-ARRO
                ws.setWkAppArro(Trunc.toShort(1 + ws.getWkAppArro(), 1));
                // COB_CODE: MOVE WK-APP-ARRO               TO WK-IMP-ARRO(13)
                ws.getWkTabArro(13).setWkImpArroFormatted(ws.getWkAppArroFormatted());
                // COB_CODE: MOVE ZEROES                    TO WK-IMP-ARRO(14)
                ws.getWkTabArro(14).setWkImpArro('0');
            }
            else {
                // COB_CODE: MOVE ZEROES                    TO WK-IMP-ARRO(14)
                ws.getWkTabArro(14).setWkImpArro('0');
            }
            //
            // COB_CODE: MOVE WK-AREA-ARRO                 TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setIvvc0213ValImpOFromBuffer(ws.getWkAreaArroBytes());
        }
        else {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED    TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: A191-CALCOLA-DT-RICOR-TRANCHE<br>
	 * <pre>----------------------------------------------------------------*
	 *   CONVERTI VALORE DA STRINGA IN NUMERICO
	 * ----------------------------------------------------------------*</pre>*/
    private void a191CalcolaDtRicorTranche() {
        // COB_CODE: INITIALIZE           IO-A2K-LCCC0003
        //                                IN-RCODE
        initIoA2kLccc0003();
        ws.setInRcodeFormatted("00");
        //    somma DELTA e conversione data
        // COB_CODE: MOVE '02'                        TO A2K-FUNZ
        ws.getIoA2kLccc0003().getInput().setA2kFunz("02");
        //    FORMATO AAAAMMGG
        // COB_CODE: MOVE '03'                        TO A2K-INFO
        ws.getIoA2kLccc0003().getInput().setA2kInfo("03");
        //    1 ANNO
        // COB_CODE: MOVE 1                           TO A2K-DELTA
        ws.getIoA2kLccc0003().getInput().setA2kDelta(((short)1));
        //    ANNI
        // COB_CODE: MOVE 'A'                         TO A2K-TDELTA
        ws.getIoA2kLccc0003().getInput().setA2kTdeltaFormatted("A");
        //    GIORNI FISSI
        // COB_CODE: MOVE '0'                         TO A2K-FISLAV
        ws.getIoA2kLccc0003().getInput().setA2kFislavFormatted("0");
        //    INIZIO CONTEGGIO DA STESSO GIORNO
        // COB_CODE: MOVE 0                           TO A2K-INICON
        ws.getIoA2kLccc0003().getInput().setA2kIniconFormatted("0");
        //    DATA INPUT
        //    Data decorrenza di tranche
        // COB_CODE: MOVE IVVC0213-DT-DECOR
        //             TO A2K-INDATA
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata(ivvc0213.getDatiLivello().getDtDecor());
        // COB_CODE: PERFORM CALL-ROUTINE-DATE
        //              THRU CALL-ROUTINE-DATE-EX
        callRoutineDate();
        //    Se Data decorrenza di tranche + 1 anno h > della Data
        //    calcolo Riserva allora la data di ricorrenza
        //    h la Data decorrenza di tranche
        // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
        //           *       DATA OUTPUT (CALCOLATA)
        //                   END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            //       DATA OUTPUT (CALCOLATA)
            // COB_CODE:         IF A2K-OUAMG > IVVC0213-DATA-EFFETTO
            //                        TO WK-DT-RICOR-TRANCHE
            //                   ELSE
            //           *          Altrimenti calcolare data ricorrenza con gg e mm
            //           *          di decorrenza tranche e anno = anno di calcolo
            //           *          riserva
            //                      END-IF
            //                   END-IF
            if (ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().getA2kOuamg() > ivvc0213.getDataEffetto()) {
                // COB_CODE: MOVE IVVC0213-DT-DECOR
                //             TO WK-DT-RICOR-TRANCHE
                ws.getWkVariabili().getWkDtRicorTranche().setWkDtRicorTrancheFormatted(ivvc0213.getDatiLivello().getIvvc0213DtDecorFormatted());
            }
            else {
                //          Altrimenti calcolare data ricorrenza con gg e mm
                //          di decorrenza tranche e anno = anno di calcolo
                //          riserva
                // COB_CODE: MOVE ZEROES
                //             TO WK-DT-RICOR-TRANCHE
                ws.getWkVariabili().getWkDtRicorTranche().initWkDtRicorTrancheZeroes();
                // COB_CODE: MOVE IVVC0213-DT-DECOR
                //             TO WK-DATA-APPO
                ws.getWkVariabili().setWkDataAppoFormatted(ivvc0213.getDatiLivello().getIvvc0213DtDecorFormatted());
                // COB_CODE: MOVE WK-DATA-APPO-GG
                //             TO WK-DT-RICOR-TRANCHE-GG
                ws.getWkVariabili().getWkDtRicorTranche().setRicorTrancheGgFormatted(ws.getWkVariabili().getWkDataAppoGgFormatted());
                // COB_CODE: MOVE WK-DATA-APPO-MM
                //             TO WK-DT-RICOR-TRANCHE-MM
                ws.getWkVariabili().getWkDtRicorTranche().setRicorTrancheMmFormatted(ws.getWkVariabili().getWkDataAppoMmFormatted());
                // COB_CODE: MOVE WK-DATA-APPO-AA
                //             TO WK-DT-RICOR-TRANCHE-AA
                ws.getWkVariabili().getWkDtRicorTranche().setRicorTrancheAaFormatted(ws.getWkVariabili().getWkDataAppoAaFormatted());
                //          Se la data cosl calcolata h < = alla data di calcolo
                //          riserva allora impostare la data ricorrenza
                //          con tale data
                // COB_CODE:            IF WK-DT-RICOR-TRANCHE <= IVVC0213-DATA-EFFETTO
                //                         CONTINUE
                //                      ELSE
                //           *             Altrimenti (se > di data calcolo riserva)
                //           *             sottrarre 1 anno e valorizzare la data
                //           *             ricorrenza con tale data
                //                           TO WK-DT-RICOR-TRANCHE-AA
                //                      END-IF
                if (Conditions.lte(ws.getWkVariabili().getWkDtRicorTranche().getWkDtRicorTrancheBytes(), MarshalByteExt.strToBuffer(ivvc0213.getDataEffettoFormatted(), Ivvc0213.Len.DATA_EFFETTO))) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    //             Altrimenti (se > di data calcolo riserva)
                    //             sottrarre 1 anno e valorizzare la data
                    //             ricorrenza con tale data
                    // COB_CODE: MOVE WK-DT-RICOR-TRANCHE-AA
                    //             TO WK-ANNO
                    ws.getWkVariabili().setWkAnnoFormatted(ws.getWkVariabili().getWkDtRicorTranche().getRicorTrancheAaFormatted());
                    // COB_CODE: SUBTRACT 1
                    //               FROM WK-ANNO
                    ws.getWkVariabili().setWkAnno(Trunc.toShort(abs(ws.getWkVariabili().getWkAnno() - 1), 4));
                    // COB_CODE: MOVE WK-ANNO
                    //             TO WK-DT-RICOR-TRANCHE-AA
                    ws.getWkVariabili().getWkDtRicorTranche().setRicorTrancheAaFormatted(ws.getWkVariabili().getWkAnnoFormatted());
                }
            }
        }
    }

    /**Original name: CALL-ROUTINE-DATE<br>
	 * <pre>----------------------------------------------------------------*
	 *   CHIAMATA AL SERVIZIO CALCOLA DATA
	 * ----------------------------------------------------------------*</pre>*/
    private void callRoutineDate() {
        Lccs0003 lccs0003 = null;
        GenericParam inRcode = null;
        // COB_CODE: CALL LCCS0003      USING IO-A2K-LCCC0003
        //                                    IN-RCODE
        //           ON EXCEPTION
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           END-CALL.
        try {
            lccs0003 = Lccs0003.getInstance();
            inRcode = new GenericParam(MarshalByteExt.strToBuffer(ws.getInRcodeFormatted(), Lvvs0135Data.Len.IN_RCODE));
            lccs0003.run(ws.getIoA2kLccc0003(), inRcode);
            ws.setInRcodeFromBuffer(inRcode.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: SET  IDSV0003-INVALID-OPER                  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ROUTINE CALCOLO DATA (LCCS0003)'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ROUTINE CALCOLO DATA (LCCS0003)");
        }
        // COB_CODE: IF IN-RCODE  = '00'
        //              CONTINUE
        //           ELSE
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           END-IF.
        if (ws.getInRcodeFormatted().equals("00")) {
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: SET  IDSV0003-INVALID-OPER                  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'ROUTINE CALCOLO DATA (LCCS0003)'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ROUTINE CALCOLO DATA (LCCS0003)");
        }
    }

    /**Original name: A140-RECUP-VALORE-ASSET<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA DATI TABELLA VALORE ASSET
	 * ----------------------------------------------------------------*</pre>*/
    private void a140RecupValoreAsset() {
        Ldbs4910 ldbs4910 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A140-RECUP-VALORE-ASSET'
        //             TO WK-LABEL
        ws.setWkLabel("A140-RECUP-VALORE-ASSET");
        // COB_CODE: MOVE ZEROES
        //             TO IX-TAB-VAS
        ws.getIxIndici().setTabVas(((short)0));
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-FETCH-FIRST       TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        //
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR NOT IDSV0003-SUCCESSFUL-SQL
        //                      OR WK-NUM-QUO-KO
        //                    END-EVALUATE
        //           END-PERFORM
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql() || ws.getWkNumQuo().isKo())) {
            // COB_CODE: INITIALIZE LDBV4911
            initLdbv4911();
            // COB_CODE: MOVE SPACES                TO LDBV4911-TP-VAL-AST-1
            ws.getLdbv4911().setTpValAst1("");
            // COB_CODE: MOVE SPACES                TO LDBV4911-TP-VAL-AST-2
            ws.getLdbv4911().setTpValAst2("");
            // COB_CODE: MOVE IVVC0213-ID-TRANCHE   TO LDBV4911-ID-TRCH-DI-GAR
            ws.getLdbv4911().setIdTrchDiGar(ivvc0213.getIdTranche());
            // COB_CODE: MOVE LDBV4911              TO IDSV0003-BUFFER-WHERE-COND
            idsv0003.setBufferWhereCond(ws.getLdbv4911().getLdbv4911Formatted());
            //--> LIVELLO OPERAZIONE
            // COB_CODE: SET IDSV0003-WHERE-CONDITION TO TRUE
            idsv0003.getLivelloOperazione().setWhereCondition();
            // COB_CODE: CALL LDBS4910  USING  IDSV0003 VAL-AST
            //           ON EXCEPTION
            //                SET IDSV0003-INVALID-OPER  TO TRUE
            //           END-CALL
            try {
                ldbs4910 = Ldbs4910.getInstance();
                ldbs4910.run(idsv0003, ws.getValAst());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE LDBS4910
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
                // COB_CODE: MOVE 'ERRORE CALL LDBS4910'
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS4910");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            // COB_CODE:               EVALUATE TRUE
            //                             WHEN IDSV0003-NOT-FOUND
            //           *-->    NESSUN DATO IN TABELLA
            //                                  CONTINUE
            //                             WHEN IDSV0003-SUCCESSFUL-SQL
            //           *-->       OPERAZIONE ESEGUITA CORRETTAMENTE
            //                                  SET IDSV0003-FETCH-NEXT   TO TRUE
            //                             WHEN OTHER
            //           *--->   ERRORE DI ACCESSO AL DB
            //                         END-STRING
            //                         END-EVALUATE
            switch (idsv0003.getSqlcode().getSqlcode()) {

                case Idsv0003Sqlcode.NOT_FOUND://-->    NESSUN DATO IN TABELLA
                // COB_CODE: CONTINUE
                //continue
                    break;

                case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->       OPERAZIONE ESEGUITA CORRETTAMENTE
                    // COB_CODE: ADD 1
                    //            TO IX-TAB-VAS
                    ws.getIxIndici().setTabVas(Trunc.toShort(1 + ws.getIxIndici().getTabVas(), 4));
                    // COB_CODE: PERFORM INIZIA-TOT-VAS
                    iniziaTotVas();
                    // COB_CODE: PERFORM INIZIA-TOT-VAS-EX
                    iniziaTotVasEx();
                    // COB_CODE: PERFORM VALORIZZA-OUTPUT-VAS
                    //              THRU VALORIZZA-OUTPUT-VAS-EX
                    valorizzaOutputVas();
                    // COB_CODE: SET WVAS-ST-INV(IX-TAB-VAS)
                    //            TO TRUE
                    ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getStatus().setInv();
                    // COB_CODE: MOVE IX-TAB-VAS
                    //             TO WVAS-ELE-VAL-AST-MAX
                    ws.setWvasEleValAstMax(ws.getIxIndici().getTabVas());
                    // COB_CODE: SET IDSV0003-FETCH-NEXT   TO TRUE
                    idsv0003.getOperazione().setFetchNext();
                    break;

                default://--->   ERRORE DI ACCESSO AL DB
                    // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-PGM
                    //             TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                    // COB_CODE: STRING 'ERRORE LETTURA TABELLA VALORE ASSET ;'
                    //                 IDSV0003-RETURN-CODE ';'
                    //                 IDSV0003-SQLCODE
                    //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA VALORE ASSET ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                    break;
            }
        }
        // COB_CODE: IF WK-NUM-QUO-KO
        //              PERFORM S1252-CHIUDE-CURVAS         THRU S1252-EX
        //           END-IF.
        if (ws.getWkNumQuo().isKo()) {
            // COB_CODE: PERFORM S1252-CHIUDE-CURVAS         THRU S1252-EX
            s1252ChiudeCurvas();
        }
    }

    /**Original name: S1252-CHIUDE-CURVAS<br>
	 * <pre>----------------------------------------------------------------*
	 *    CHIUSURA CURSORE VALORE ASSET VALORE ASSETT
	 * ----------------------------------------------------------------*</pre>*/
    private void s1252ChiudeCurvas() {
        Ldbs4910 ldbs4910 = null;
        // COB_CODE: SET IDSV0003-CLOSE-CURSOR            TO TRUE.
        idsv0003.getOperazione().setIdsv0003CloseCursor();
        //
        // COB_CODE: CALL LDBS4910 USING  IDSV0003 VAL-AST
        //             ON EXCEPTION
        //                 SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL.
        try {
            ldbs4910 = Ldbs4910.getInstance();
            ldbs4910.run(idsv0003, ws.getValAst());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS4910
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
            // COB_CODE: MOVE 'ERRORE CALL LDBS4910'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS4910");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF NOT IDSV0003-SUCCESSFUL-RC
        //           OR NOT IDSV0003-SUCCESSFUL-SQL
        //                TO TRUE
        //           END-IF.
        if (!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: MOVE LDBS4910
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
            // COB_CODE: MOVE 'ERRORE CALL LDBS4910'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS4910");
            // COB_CODE: SET IDSV0003-INVALID-OPER
            //            TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A160-RECUP-QUOTAZ-FONDO<br>
	 * <pre>----------------------------------------------------------------*
	 *     LETTURA QUOTAZIONI FONDO
	 * ----------------------------------------------------------------*</pre>*/
    private void a160RecupQuotazFondo() {
        Ldbs2080 ldbs2080 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: MOVE 'A160-RECUP-QUOTAZ-FONDO'
        //             TO WK-LABEL
        ws.setWkLabel("A160-RECUP-QUOTAZ-FONDO");
        // COB_CODE: INITIALIZE QUOTZ-FND-UNIT
        initQuotzFndUnit();
        //--> INIZIALIZZA CODICE DI RITORNO
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-RC    TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: SET  IDSV0003-SUCCESSFUL-SQL   TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: MOVE ZEROES
        //             TO IX-TAB-L19
        ws.getIxIndici().setTabL19(((short)0));
        // COB_CODE: SET IDSV0003-TRATT-SENZA-STOR  TO TRUE.
        idsv0003.getTrattamentoStoricita().setTrattSenzaStor();
        //--> LIVELLO OPERAZIONE
        // COB_CODE: SET IDSV0003-WHERE-CONDITION   TO TRUE.
        idsv0003.getLivelloOperazione().setWhereCondition();
        //--> TIPO OPERAZIONE
        // COB_CODE: SET IDSV0003-SELECT            TO TRUE.
        idsv0003.getOperazione().setSelect();
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                          TO L19-COD-COMP-ANIA.
        ws.getQuotzFndUnit().setL19CodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: MOVE WVAS-COD-FND(IX-TAB-VAS)  TO L19-COD-FND.
        ws.getQuotzFndUnit().setL19CodFnd(ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasCodFnd());
        // COB_CODE: MOVE WK-DT-DECOR-TRCH-RED      TO L19-DT-QTZ.
        ws.getQuotzFndUnit().setL19DtQtz(ws.getWkVariabili().getWkDtRicorTranche().getWkDtDecorTrchRed());
        // COB_CODE: CALL LDBS2080  USING  IDSV0003 QUOTZ-FND-UNIT
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL
        try {
            ldbs2080 = Ldbs2080.getInstance();
            ldbs2080.run(idsv0003, ws.getQuotzFndUnit());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE LDBS4910
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getLdbs4910());
            // COB_CODE: MOVE 'ERRORE CALL LDBS4910'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL LDBS4910");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        // COB_CODE:            EVALUATE TRUE
        //                          WHEN IDSV0003-SUCCESSFUL-SQL
        //           *-->    OPERAZIONE ESEGUITA CORRETTAMENTE
        //                                   TO DL19-ELE-FND-MAX
        //                          WHEN OTHER
        //           *--->   ERRORE DI ACCESSO AL DB
        //                         END-STRING
        //                      END-EVALUATE.
        switch (idsv0003.getSqlcode().getSqlcode()) {

            case Idsv0003Sqlcode.SUCCESSFUL_SQL://-->    OPERAZIONE ESEGUITA CORRETTAMENTE
                // COB_CODE: ADD 1
                //            TO IX-TAB-L19
                ws.getIxIndici().setTabL19(Trunc.toShort(1 + ws.getIxIndici().getTabL19(), 4));
                // COB_CODE: PERFORM INIZIA-TOT-L19
                iniziaTotL19();
                // COB_CODE: PERFORM INIZIA-TOT-L19-EX
                iniziaTotL19Ex();
                // COB_CODE: PERFORM VALORIZZA-OUTPUT-L19
                //              THRU VALORIZZA-OUTPUT-L19-EX
                valorizzaOutputL19();
                // COB_CODE: SET DL19-ST-INV(IX-TAB-L19)
                //            TO TRUE
                ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getStatus().setInv();
                // COB_CODE: MOVE IX-TAB-L19
                //             TO DL19-ELE-FND-MAX
                ws.setDl19EleFndMax(ws.getIxIndici().getTabL19());
                break;

            default://--->   ERRORE DI ACCESSO AL DB
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
                // COB_CODE: MOVE WK-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
                // COB_CODE: STRING 'ERRORE LETTURA TABELLA QUOTZ-FND-UNIT ;'
                //                 IDSV0003-RETURN-CODE ';'
                //                 IDSV0003-SQLCODE
                //             DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "ERRORE LETTURA TABELLA QUOTZ-FND-UNIT ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                break;
        }
    }

    /**Original name: VALORIZZA-OUTPUT-L19<br>
	 * <pre>----------------------------------------------------------------*
	 *    VALORIZZA OUTPUT
	 * ----------------------------------------------------------------*</pre>*/
    private void valorizzaOutputL19() {
        // COB_CODE: MOVE L19-COD-COMP-ANIA
        //             TO DL19-COD-COMP-ANIA(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19CodCompAnia(ws.getQuotzFndUnit().getL19CodCompAnia());
        // COB_CODE: MOVE L19-COD-FND
        //             TO DL19-COD-FND(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19CodFnd(ws.getQuotzFndUnit().getL19CodFnd());
        // COB_CODE: MOVE L19-DT-QTZ
        //             TO DL19-DT-QTZ(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19DtQtz(ws.getQuotzFndUnit().getL19DtQtz());
        // COB_CODE: IF L19-VAL-QUO-NULL = HIGH-VALUES
        //                TO DL19-VAL-QUO-NULL(IX-TAB-L19)
        //           ELSE
        //                TO DL19-VAL-QUO(IX-TAB-L19)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getQuotzFndUnit().getL19ValQuo().getL19ValQuoNullFormatted())) {
            // COB_CODE: MOVE L19-VAL-QUO-NULL
            //             TO DL19-VAL-QUO-NULL(IX-TAB-L19)
            ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().setWl19ValQuoNull(ws.getQuotzFndUnit().getL19ValQuo().getL19ValQuoNull());
        }
        else {
            // COB_CODE: MOVE L19-VAL-QUO
            //             TO DL19-VAL-QUO(IX-TAB-L19)
            ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().setWl19ValQuo(Trunc.toDecimal(ws.getQuotzFndUnit().getL19ValQuo().getL19ValQuo(), 12, 5));
        }
        // COB_CODE: IF L19-VAL-QUO-MANFEE-NULL = HIGH-VALUES
        //                TO DL19-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
        //           ELSE
        //                TO DL19-VAL-QUO-MANFEE(IX-TAB-L19)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getQuotzFndUnit().getL19ValQuoManfee().getL19ValQuoManfeeNullFormatted())) {
            // COB_CODE: MOVE L19-VAL-QUO-MANFEE-NULL
            //             TO DL19-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
            ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuoManfee().setWl19ValQuoManfeeNull(ws.getQuotzFndUnit().getL19ValQuoManfee().getL19ValQuoManfeeNull());
        }
        else {
            // COB_CODE: MOVE L19-VAL-QUO-MANFEE
            //             TO DL19-VAL-QUO-MANFEE(IX-TAB-L19)
            ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuoManfee().setDl19ValQuoManfee(Trunc.toDecimal(ws.getQuotzFndUnit().getL19ValQuoManfee().getL19ValQuoManfee(), 12, 5));
        }
        // COB_CODE: MOVE L19-DS-OPER-SQL
        //             TO DL19-DS-OPER-SQL(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19DsOperSql(ws.getQuotzFndUnit().getL19DsOperSql());
        // COB_CODE: MOVE L19-DS-VER
        //             TO DL19-DS-VER(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19DsVer(ws.getQuotzFndUnit().getL19DsVer());
        // COB_CODE: MOVE L19-DS-TS-CPTZ
        //             TO DL19-DS-TS-CPTZ(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19DsTsCptz(ws.getQuotzFndUnit().getL19DsTsCptz());
        // COB_CODE: MOVE L19-DS-UTENTE
        //             TO DL19-DS-UTENTE(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19DsUtente(ws.getQuotzFndUnit().getL19DsUtente());
        // COB_CODE: MOVE L19-DS-STATO-ELAB
        //             TO DL19-DS-STATO-ELAB(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19DsStatoElab(ws.getQuotzFndUnit().getL19DsStatoElab());
        // COB_CODE: MOVE L19-TP-FND
        //             TO DL19-TP-FND(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19TpFnd(ws.getQuotzFndUnit().getL19TpFnd());
        // COB_CODE: IF L19-DT-RILEVAZIONE-NAV-NULL = HIGH-VALUES
        //                TO DL19-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19)
        //           ELSE
        //                TO DL19-DT-RILEVAZIONE-NAV(IX-TAB-L19)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getQuotzFndUnit().getL19DtRilevazioneNav().getL19DtRilevazioneNavNullFormatted())) {
            // COB_CODE: MOVE L19-DT-RILEVAZIONE-NAV-NULL
            //             TO DL19-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19)
            ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19DtRilevazioneNav().setWl19DtRilevazioneNavNull(ws.getQuotzFndUnit().getL19DtRilevazioneNav().getL19DtRilevazioneNavNull());
        }
        else {
            // COB_CODE: MOVE L19-DT-RILEVAZIONE-NAV
            //             TO DL19-DT-RILEVAZIONE-NAV(IX-TAB-L19)
            ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19DtRilevazioneNav().setDl19DtRilevazioneNav(ws.getQuotzFndUnit().getL19DtRilevazioneNav().getL19DtRilevazioneNav());
        }
    }

    /**Original name: INIZIA-TOT-L19<br>*/
    private void iniziaTotL19() {
        // COB_CODE: PERFORM INIZIA-ZEROES-L19 THRU INIZIA-ZEROES-L19-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LVVS0135.cbl:line=746, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-SPACES-L19 THRU INIZIA-SPACES-L19-EX
        iniziaSpacesL19();
        // COB_CODE: PERFORM INIZIA-NULL-L19 THRU INIZIA-NULL-L19-EX.
        iniziaNullL19();
    }

    /**Original name: INIZIA-TOT-L19-EX<br>*/
    private void iniziaTotL19Ex() {
    }

    /**Original name: INIZIA-NULL-L19<br>*/
    private void iniziaNullL19() {
        // COB_CODE: MOVE HIGH-VALUES TO DL19-VAL-QUO-NULL(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuo().setWl19ValQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl19ValQuo.Len.WL19_VAL_QUO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO DL19-VAL-QUO-MANFEE-NULL(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19ValQuoManfee().setWl19ValQuoManfeeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl19ValQuoManfee.Len.WL19_VAL_QUO_MANFEE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO DL19-DT-RILEVAZIONE-NAV-NULL(IX-TAB-L19).
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().getWl19DtRilevazioneNav().setWl19DtRilevazioneNavNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Wl19DtRilevazioneNav.Len.WL19_DT_RILEVAZIONE_NAV_NULL));
    }

    /**Original name: INIZIA-SPACES-L19<br>*/
    private void iniziaSpacesL19() {
        // COB_CODE: MOVE SPACES TO DL19-COD-FND(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19CodFnd("");
        // COB_CODE: MOVE SPACES TO DL19-DS-OPER-SQL(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19DsOperSql(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO DL19-DS-UTENTE(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19DsUtente("");
        // COB_CODE: MOVE SPACES TO DL19-DS-STATO-ELAB(IX-TAB-L19)
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19DsStatoElab(Types.SPACE_CHAR);
        // COB_CODE: MOVE SPACES TO DL19-TP-FND(IX-TAB-L19).
        ws.getDl19TabFnd(ws.getIxIndici().getTabL19()).getLccvl191().getDati().setWl19TpFnd(Types.SPACE_CHAR);
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    /**Original name: VALORIZZA-OUTPUT-VAS<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY VALORIZZA OUTPUT TABELLE
	 * ----------------------------------------------------------------*
	 * --> VALORE ASSET
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    VALORIZZA OUTPUT COPY LCCVVAS3
	 *    ULTIMO AGG. 09 LUG 2010
	 * ------------------------------------------------------------</pre>*/
    private void valorizzaOutputVas() {
        // COB_CODE: MOVE VAS-ID-VAL-AST
        //             TO (SF)-ID-PTF(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().setIdPtf(ws.getValAst().getVasIdValAst());
        // COB_CODE: MOVE VAS-ID-VAL-AST
        //             TO (SF)-ID-VAL-AST(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasIdValAst(ws.getValAst().getVasIdValAst());
        // COB_CODE: IF VAS-ID-TRCH-DI-GAR-NULL = HIGH-VALUES
        //                TO (SF)-ID-TRCH-DI-GAR-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-ID-TRCH-DI-GAR(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasIdTrchDiGar().getVasIdTrchDiGarNullFormatted())) {
            // COB_CODE: MOVE VAS-ID-TRCH-DI-GAR-NULL
            //             TO (SF)-ID-TRCH-DI-GAR-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdTrchDiGar().setWvasIdTrchDiGarNull(ws.getValAst().getVasIdTrchDiGar().getVasIdTrchDiGarNull());
        }
        else {
            // COB_CODE: MOVE VAS-ID-TRCH-DI-GAR
            //             TO (SF)-ID-TRCH-DI-GAR(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdTrchDiGar().setWvasIdTrchDiGar(ws.getValAst().getVasIdTrchDiGar().getVasIdTrchDiGar());
        }
        // COB_CODE: MOVE VAS-ID-MOVI-FINRIO
        //             TO (SF)-ID-MOVI-FINRIO(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasIdMoviFinrio(ws.getValAst().getVasIdMoviFinrio());
        // COB_CODE: MOVE VAS-ID-MOVI-CRZ
        //             TO (SF)-ID-MOVI-CRZ(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasIdMoviCrz(ws.getValAst().getVasIdMoviCrz());
        // COB_CODE: IF VAS-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //                TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-ID-MOVI-CHIU(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasIdMoviChiu().getVasIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE VAS-ID-MOVI-CHIU-NULL
            //             TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdMoviChiu().setWvasIdMoviChiuNull(ws.getValAst().getVasIdMoviChiu().getVasIdMoviChiuNull());
        }
        else {
            // COB_CODE: MOVE VAS-ID-MOVI-CHIU
            //             TO (SF)-ID-MOVI-CHIU(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdMoviChiu().setWvasIdMoviChiu(ws.getValAst().getVasIdMoviChiu().getVasIdMoviChiu());
        }
        // COB_CODE: MOVE VAS-DT-INI-EFF
        //             TO (SF)-DT-INI-EFF(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasDtIniEff(ws.getValAst().getVasDtIniEff());
        // COB_CODE: MOVE VAS-DT-END-EFF
        //             TO (SF)-DT-END-EFF(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasDtEndEff(ws.getValAst().getVasDtEndEff());
        // COB_CODE: MOVE VAS-COD-COMP-ANIA
        //             TO (SF)-COD-COMP-ANIA(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasCodCompAnia(ws.getValAst().getVasCodCompAnia());
        // COB_CODE: IF VAS-COD-FND-NULL = HIGH-VALUES
        //                TO (SF)-COD-FND-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-COD-FND(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasCodFndFormatted())) {
            // COB_CODE: MOVE VAS-COD-FND-NULL
            //             TO (SF)-COD-FND-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasCodFnd(ws.getValAst().getVasCodFnd());
        }
        else {
            // COB_CODE: MOVE VAS-COD-FND
            //             TO (SF)-COD-FND(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasCodFnd(ws.getValAst().getVasCodFnd());
        }
        // COB_CODE: IF VAS-NUM-QUO-NULL = HIGH-VALUES
        //                TO (SF)-NUM-QUO-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-NUM-QUO(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuo().getVasNumQuoNullFormatted())) {
            // COB_CODE: MOVE VAS-NUM-QUO-NULL
            //             TO (SF)-NUM-QUO-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasNumQuo().setWvasNumQuoNull(ws.getValAst().getVasNumQuo().getVasNumQuoNull());
        }
        else {
            // COB_CODE: MOVE VAS-NUM-QUO
            //             TO (SF)-NUM-QUO(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasNumQuo().setWvasNumQuo(Trunc.toDecimal(ws.getValAst().getVasNumQuo().getVasNumQuo(), 12, 5));
        }
        // COB_CODE: IF VAS-VAL-QUO-NULL = HIGH-VALUES
        //                TO (SF)-VAL-QUO-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-VAL-QUO(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasValQuo().getVasValQuoNullFormatted())) {
            // COB_CODE: MOVE VAS-VAL-QUO-NULL
            //             TO (SF)-VAL-QUO-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasValQuo().setWvasValQuoNull(ws.getValAst().getVasValQuo().getVasValQuoNull());
        }
        else {
            // COB_CODE: MOVE VAS-VAL-QUO
            //             TO (SF)-VAL-QUO(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasValQuo().setWvasValQuo(Trunc.toDecimal(ws.getValAst().getVasValQuo().getVasValQuo(), 12, 5));
        }
        // COB_CODE: IF VAS-DT-VALZZ-NULL = HIGH-VALUES
        //                TO (SF)-DT-VALZZ-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-DT-VALZZ(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasDtValzz().getVasDtValzzNullFormatted())) {
            // COB_CODE: MOVE VAS-DT-VALZZ-NULL
            //             TO (SF)-DT-VALZZ-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasDtValzz().setWvasDtValzzNull(ws.getValAst().getVasDtValzz().getVasDtValzzNull());
        }
        else {
            // COB_CODE: MOVE VAS-DT-VALZZ
            //             TO (SF)-DT-VALZZ(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasDtValzz().setWvasDtValzz(ws.getValAst().getVasDtValzz().getVasDtValzz());
        }
        // COB_CODE: MOVE VAS-TP-VAL-AST
        //             TO (SF)-TP-VAL-AST(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasTpValAst(ws.getValAst().getVasTpValAst());
        // COB_CODE: IF VAS-ID-RICH-INVST-FND-NULL = HIGH-VALUES
        //                TO (SF)-ID-RICH-INVST-FND-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-ID-RICH-INVST-FND(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFndNullFormatted())) {
            // COB_CODE: MOVE VAS-ID-RICH-INVST-FND-NULL
            //             TO (SF)-ID-RICH-INVST-FND-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdRichInvstFnd().setWvasIdRichInvstFndNull(ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFndNull());
        }
        else {
            // COB_CODE: MOVE VAS-ID-RICH-INVST-FND
            //             TO (SF)-ID-RICH-INVST-FND(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdRichInvstFnd().setWvasIdRichInvstFnd(ws.getValAst().getVasIdRichInvstFnd().getVasIdRichInvstFnd());
        }
        // COB_CODE: IF VAS-ID-RICH-DIS-FND-NULL = HIGH-VALUES
        //                TO (SF)-ID-RICH-DIS-FND-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-ID-RICH-DIS-FND(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasIdRichDisFnd().getVasIdRichDisFndNullFormatted())) {
            // COB_CODE: MOVE VAS-ID-RICH-DIS-FND-NULL
            //             TO (SF)-ID-RICH-DIS-FND-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdRichDisFnd().setWvasIdRichDisFndNull(ws.getValAst().getVasIdRichDisFnd().getVasIdRichDisFndNull());
        }
        else {
            // COB_CODE: MOVE VAS-ID-RICH-DIS-FND
            //             TO (SF)-ID-RICH-DIS-FND(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdRichDisFnd().setWvasIdRichDisFnd(ws.getValAst().getVasIdRichDisFnd().getVasIdRichDisFnd());
        }
        // COB_CODE: MOVE VAS-DS-RIGA
        //             TO (SF)-DS-RIGA(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasDsRiga(ws.getValAst().getVasDsRiga());
        // COB_CODE: MOVE VAS-DS-OPER-SQL
        //             TO (SF)-DS-OPER-SQL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasDsOperSql(ws.getValAst().getVasDsOperSql());
        // COB_CODE: MOVE VAS-DS-VER
        //             TO (SF)-DS-VER(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasDsVer(ws.getValAst().getVasDsVer());
        // COB_CODE: MOVE VAS-DS-TS-INI-CPTZ
        //             TO (SF)-DS-TS-INI-CPTZ(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasDsTsIniCptz(ws.getValAst().getVasDsTsIniCptz());
        // COB_CODE: MOVE VAS-DS-TS-END-CPTZ
        //             TO (SF)-DS-TS-END-CPTZ(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasDsTsEndCptz(ws.getValAst().getVasDsTsEndCptz());
        // COB_CODE: MOVE VAS-DS-UTENTE
        //             TO (SF)-DS-UTENTE(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasDsUtente(ws.getValAst().getVasDsUtente());
        // COB_CODE: MOVE VAS-DS-STATO-ELAB
        //             TO (SF)-DS-STATO-ELAB(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasDsStatoElab(ws.getValAst().getVasDsStatoElab());
        // COB_CODE: IF VAS-PRE-MOVTO-NULL = HIGH-VALUES
        //                TO (SF)-PRE-MOVTO-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-PRE-MOVTO(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasPreMovto().getVasPreMovtoNullFormatted())) {
            // COB_CODE: MOVE VAS-PRE-MOVTO-NULL
            //             TO (SF)-PRE-MOVTO-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasPreMovto().setWvasPreMovtoNull(ws.getValAst().getVasPreMovto().getVasPreMovtoNull());
        }
        else {
            // COB_CODE: MOVE VAS-PRE-MOVTO
            //             TO (SF)-PRE-MOVTO(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasPreMovto().setWvasPreMovto(Trunc.toDecimal(ws.getValAst().getVasPreMovto().getVasPreMovto(), 15, 3));
        }
        // COB_CODE: IF VAS-IMP-MOVTO-NULL = HIGH-VALUES
        //                TO (SF)-IMP-MOVTO-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-IMP-MOVTO(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasImpMovto().getVasImpMovtoNullFormatted())) {
            // COB_CODE: MOVE VAS-IMP-MOVTO-NULL
            //             TO (SF)-IMP-MOVTO-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasImpMovto().setWvasImpMovtoNull(ws.getValAst().getVasImpMovto().getVasImpMovtoNull());
        }
        else {
            // COB_CODE: MOVE VAS-IMP-MOVTO
            //             TO (SF)-IMP-MOVTO(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasImpMovto().setWvasImpMovto(Trunc.toDecimal(ws.getValAst().getVasImpMovto().getVasImpMovto(), 15, 3));
        }
        // COB_CODE: IF VAS-PC-INV-DIS-NULL = HIGH-VALUES
        //                TO (SF)-PC-INV-DIS-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-PC-INV-DIS(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasPcInvDis().getVasPcInvDisNullFormatted())) {
            // COB_CODE: MOVE VAS-PC-INV-DIS-NULL
            //             TO (SF)-PC-INV-DIS-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasPcInvDis().setWvasPcInvDisNull(ws.getValAst().getVasPcInvDis().getVasPcInvDisNull());
        }
        else {
            // COB_CODE: MOVE VAS-PC-INV-DIS
            //             TO (SF)-PC-INV-DIS(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasPcInvDis().setWvasPcInvDis(Trunc.toDecimal(ws.getValAst().getVasPcInvDis().getVasPcInvDis(), 14, 9));
        }
        // COB_CODE: IF VAS-NUM-QUO-LORDE-NULL = HIGH-VALUES
        //                TO (SF)-NUM-QUO-LORDE-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-NUM-QUO-LORDE(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasNumQuoLorde().getVasNumQuoLordeNullFormatted())) {
            // COB_CODE: MOVE VAS-NUM-QUO-LORDE-NULL
            //             TO (SF)-NUM-QUO-LORDE-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasNumQuoLorde().setWvasNumQuoLordeNull(ws.getValAst().getVasNumQuoLorde().getVasNumQuoLordeNull());
        }
        else {
            // COB_CODE: MOVE VAS-NUM-QUO-LORDE
            //             TO (SF)-NUM-QUO-LORDE(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasNumQuoLorde().setWvasNumQuoLorde(Trunc.toDecimal(ws.getValAst().getVasNumQuoLorde().getVasNumQuoLorde(), 12, 5));
        }
        // COB_CODE: IF VAS-DT-VALZZ-CALC-NULL = HIGH-VALUES
        //                TO (SF)-DT-VALZZ-CALC-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-DT-VALZZ-CALC(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasDtValzzCalc().getVasDtValzzCalcNullFormatted())) {
            // COB_CODE: MOVE VAS-DT-VALZZ-CALC-NULL
            //             TO (SF)-DT-VALZZ-CALC-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasDtValzzCalc().setWvasDtValzzCalcNull(ws.getValAst().getVasDtValzzCalc().getVasDtValzzCalcNull());
        }
        else {
            // COB_CODE: MOVE VAS-DT-VALZZ-CALC
            //             TO (SF)-DT-VALZZ-CALC(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasDtValzzCalc().setWvasDtValzzCalc(ws.getValAst().getVasDtValzzCalc().getVasDtValzzCalc());
        }
        // COB_CODE: IF VAS-MINUS-VALENZA-NULL = HIGH-VALUES
        //                TO (SF)-MINUS-VALENZA-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-MINUS-VALENZA(IX-TAB-VAS)
        //           END-IF
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasMinusValenza().getVasMinusValenzaNullFormatted())) {
            // COB_CODE: MOVE VAS-MINUS-VALENZA-NULL
            //             TO (SF)-MINUS-VALENZA-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasMinusValenza().setWvasMinusValenzaNull(ws.getValAst().getVasMinusValenza().getVasMinusValenzaNull());
        }
        else {
            // COB_CODE: MOVE VAS-MINUS-VALENZA
            //             TO (SF)-MINUS-VALENZA(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasMinusValenza().setWvasMinusValenza(Trunc.toDecimal(ws.getValAst().getVasMinusValenza().getVasMinusValenza(), 15, 3));
        }
        // COB_CODE: IF VAS-PLUS-VALENZA-NULL = HIGH-VALUES
        //                TO (SF)-PLUS-VALENZA-NULL(IX-TAB-VAS)
        //           ELSE
        //                TO (SF)-PLUS-VALENZA(IX-TAB-VAS)
        //           END-IF.
        if (Characters.EQ_HIGH.test(ws.getValAst().getVasPlusValenza().getVasPlusValenzaNullFormatted())) {
            // COB_CODE: MOVE VAS-PLUS-VALENZA-NULL
            //             TO (SF)-PLUS-VALENZA-NULL(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasPlusValenza().setWvasPlusValenzaNull(ws.getValAst().getVasPlusValenza().getVasPlusValenzaNull());
        }
        else {
            // COB_CODE: MOVE VAS-PLUS-VALENZA
            //             TO (SF)-PLUS-VALENZA(IX-TAB-VAS)
            ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasPlusValenza().setWvasPlusValenza(Trunc.toDecimal(ws.getValAst().getVasPlusValenza().getVasPlusValenza(), 15, 3));
        }
    }

    /**Original name: INIZIA-TOT-VAS<br>
	 * <pre>----------------------------------------------------------------*
	 *     COPY INIZIALIZZAZIONE AREE OUTPUT TABELLE
	 * ----------------------------------------------------------------*
	 * --> VALORE ASSET
	 * ------------------------------------------------------------
	 *    PORTAFOGLIO VITA
	 *    INIZIALIZZAZIONE CAMPI NULL COPY LCCVVAS4
	 *    ULTIMO AGG. 09 LUG 2010
	 * ------------------------------------------------------------</pre>*/
    private void iniziaTotVas() {
        // COB_CODE: PERFORM INIZIA-ZEROES-VAS THRU INIZIA-ZEROES-VAS-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVVAS4:line=10, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-SPACES-VAS THRU INIZIA-SPACES-VAS-EX
        //Nothing to translate for statement com.bphx.model.cobol.statement.impl.CblPerformStmtImpl @source=LCCVVAS4:line=12, because the code is unreachable.
        // COB_CODE: PERFORM INIZIA-NULL-VAS THRU INIZIA-NULL-VAS-EX.
        iniziaNullVas();
    }

    /**Original name: INIZIA-TOT-VAS-EX<br>*/
    private void iniziaTotVasEx() {
    }

    /**Original name: INIZIA-NULL-VAS<br>*/
    private void iniziaNullVas() {
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-TRCH-DI-GAR-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdTrchDiGar().setWvasIdTrchDiGarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasIdTrchDiGar.Len.WVAS_ID_TRCH_DI_GAR_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-MOVI-CHIU-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdMoviChiu().setWvasIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasIdMoviChiu.Len.WVAS_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-COD-FND-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().setWvasCodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasDati.Len.WVAS_COD_FND));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-NUM-QUO-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasNumQuo().setWvasNumQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasNumQuo.Len.WVAS_NUM_QUO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-VAL-QUO-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasValQuo().setWvasValQuoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasValQuo.Len.WVAS_VAL_QUO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-VALZZ-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasDtValzz().setWvasDtValzzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasDtValzz.Len.WVAS_DT_VALZZ_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-RICH-INVST-FND-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdRichInvstFnd().setWvasIdRichInvstFndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasIdRichInvstFnd.Len.WVAS_ID_RICH_INVST_FND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-ID-RICH-DIS-FND-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasIdRichDisFnd().setWvasIdRichDisFndNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasIdRichDisFnd.Len.WVAS_ID_RICH_DIS_FND_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PRE-MOVTO-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasPreMovto().setWvasPreMovtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasPreMovto.Len.WVAS_PRE_MOVTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-IMP-MOVTO-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasImpMovto().setWvasImpMovtoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasImpMovto.Len.WVAS_IMP_MOVTO_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PC-INV-DIS-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasPcInvDis().setWvasPcInvDisNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasPcInvDis.Len.WVAS_PC_INV_DIS_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-NUM-QUO-LORDE-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasNumQuoLorde().setWvasNumQuoLordeNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasNumQuoLorde.Len.WVAS_NUM_QUO_LORDE_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-DT-VALZZ-CALC-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasDtValzzCalc().setWvasDtValzzCalcNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasDtValzzCalc.Len.WVAS_DT_VALZZ_CALC_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-MINUS-VALENZA-NULL(IX-TAB-VAS)
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasMinusValenza().setWvasMinusValenzaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasMinusValenza.Len.WVAS_MINUS_VALENZA_NULL));
        // COB_CODE: MOVE HIGH-VALUES TO (SF)-PLUS-VALENZA-NULL(IX-TAB-VAS).
        ws.getWvasTabValAst(ws.getIxIndici().getTabVas()).getLccvvas1().getDati().getWvasPlusValenza().setWvasPlusValenzaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, WvasPlusValenza.Len.WVAS_PLUS_VALENZA_NULL));
    }

    public void initIxIndici() {
        ws.getIxIndici().setDclgen(((short)0));
        ws.getIxIndici().setTabGrz(((short)0));
        ws.getIxIndici().setTabL19(((short)0));
        ws.getIxIndici().setTabTga(((short)0));
        ws.getIxIndici().setTabVas(((short)0));
        ws.getIxIndici().setGuidaGrz(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initWkVariabili() {
        ws.getWkVariabili().getWkDtRicorTranche().setRicorTrancheAaFormatted("0000");
        ws.getWkVariabili().getWkDtRicorTranche().setRicorTrancheMmFormatted("00");
        ws.getWkVariabili().getWkDtRicorTranche().setRicorTrancheGgFormatted("00");
        ws.getWkVariabili().setWkAnnoFormatted("0000");
        ws.getWkVariabili().setWkDataAppoAaFormatted("0000");
        ws.getWkVariabili().setWkDataAppoMmFormatted("00");
        ws.getWkVariabili().setWkDataAppoGgFormatted("00");
    }

    public void initValAst() {
        ws.getValAst().setVasIdValAst(0);
        ws.getValAst().getVasIdTrchDiGar().setVasIdTrchDiGar(0);
        ws.getValAst().setVasIdMoviFinrio(0);
        ws.getValAst().setVasIdMoviCrz(0);
        ws.getValAst().getVasIdMoviChiu().setVasIdMoviChiu(0);
        ws.getValAst().setVasDtIniEff(0);
        ws.getValAst().setVasDtEndEff(0);
        ws.getValAst().setVasCodCompAnia(0);
        ws.getValAst().setVasCodFnd("");
        ws.getValAst().getVasNumQuo().setVasNumQuo(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasValQuo().setVasValQuo(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasDtValzz().setVasDtValzz(0);
        ws.getValAst().setVasTpValAst("");
        ws.getValAst().getVasIdRichInvstFnd().setVasIdRichInvstFnd(0);
        ws.getValAst().getVasIdRichDisFnd().setVasIdRichDisFnd(0);
        ws.getValAst().setVasDsRiga(0);
        ws.getValAst().setVasDsOperSql(Types.SPACE_CHAR);
        ws.getValAst().setVasDsVer(0);
        ws.getValAst().setVasDsTsIniCptz(0);
        ws.getValAst().setVasDsTsEndCptz(0);
        ws.getValAst().setVasDsUtente("");
        ws.getValAst().setVasDsStatoElab(Types.SPACE_CHAR);
        ws.getValAst().getVasPreMovto().setVasPreMovto(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasImpMovto().setVasImpMovto(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasPcInvDis().setVasPcInvDis(new AfDecimal(0, 14, 9));
        ws.getValAst().getVasNumQuoLorde().setVasNumQuoLorde(new AfDecimal(0, 12, 5));
        ws.getValAst().getVasDtValzzCalc().setVasDtValzzCalc(0);
        ws.getValAst().getVasMinusValenza().setVasMinusValenza(new AfDecimal(0, 15, 3));
        ws.getValAst().getVasPlusValenza().setVasPlusValenza(new AfDecimal(0, 15, 3));
    }

    public void initAreaIoTga() {
        ws.setDtgaEleTgaMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0135Data.DTGA_TAB_TRAN_MAXOCCURS; idx0++) {
            ws.getDtgaTabTran(idx0).getLccvtga1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().setIdPtf(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdTrchDiGar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdGar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdAdes(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdPoli(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIdMoviCrz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIdMoviChiu().setWtgaIdMoviChiu(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtIniEff(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtEndEff(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodCompAnia(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDtDecor(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtScad().setWtgaDtScad(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaIbOgg("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRgmFisc("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEmis().setWtgaDtEmis(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpTrch("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAa().setWtgaDurAa(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurMm().setWtgaDurMm(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurGg().setWtgaDurGg(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreCasoMor().setWtgaPreCasoMor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcIntrRiat().setWtgaPcIntrRiat(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBnsAntic().setWtgaImpBnsAntic(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreIniNet().setWtgaPreIniNet(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpIni().setWtgaPrePpIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePpUlt().setWtgaPrePpUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariIni().setWtgaPreTariIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreTariUlt().setWtgaPreTariUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioIni().setWtgaPreInvrioIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreInvrioUlt().setWtgaPreInvrioUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreRivto().setWtgaPreRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprProf().setWtgaImpSoprProf(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSan().setWtgaImpSoprSan(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprSpo().setWtgaImpSoprSpo(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpSoprTec().setWtgaImpSoprTec(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAltSopr().setWtgaImpAltSopr(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreStab().setWtgaPreStab(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtEffStab().setWtgaDtEffStab(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalFis().setWtgaTsRivalFis(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalIndiciz().setWtgaTsRivalIndiciz(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaOldTsTec().setWtgaOldTsTec(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRatLrd().setWtgaRatLrd(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreLrd().setWtgaPreLrd(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIni().setWtgaPrstzIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzUlt().setWtgaPrstzUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptInOpzRivto().setWtgaCptInOpzRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniStab().setWtgaPrstzIniStab(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptRshMor().setWtgaCptRshMor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzRidIni().setWtgaPrstzRidIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlCarCont(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaBnsGiaLiqto().setWtgaBnsGiaLiqto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpBns().setWtgaImpBns(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaCodDvs("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNewfis().setWtgaPrstzIniNewfis(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpScon().setWtgaImpScon(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqScon().setWtgaAlqScon(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarAcq().setWtgaImpCarAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarInc().setWtgaImpCarInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpCarGest().setWtgaImpCarGest(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa1oAssto().setWtgaEtaAa1oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm1oAssto().setWtgaEtaMm1oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa2oAssto().setWtgaEtaAa2oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm2oAssto().setWtgaEtaMm2oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaAa3oAssto().setWtgaEtaAa3oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaEtaMm3oAssto().setWtgaEtaMm3oAssto(((short)0));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoLrd().setWtgaRendtoLrd(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRetr().setWtgaPcRetr(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRendtoRetr().setWtgaRendtoRetr(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinGarto().setWtgaMinGarto(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMinTrnut().setWtgaMinTrnut(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreAttDiTrch().setWtgaPreAttDiTrch(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaMatuEnd2000().setWtgaMatuEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotIni().setWtgaAbbTotIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbTotUlt().setWtgaAbbTotUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAbbAnnuUlt().setWtgaAbbAnnuUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDurAbb().setWtgaDurAbb(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpAdegAbb(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaModCalc("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAz().setWtgaImpAz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpAder().setWtgaImpAder(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfr().setWtgaImpTfr(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpVolo().setWtgaImpVolo(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000().setWtgaVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtVldtProd().setWtgaDtVldtProd(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtIniValTar().setWtgaDtIniValTar(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbVisEnd2000().setWtgaImpbVisEnd2000(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRenIniTsTec0().setWtgaRenIniTsTec0(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcRipPre().setWtgaPcRipPre(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlImportiForz(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzIniNforz().setWtgaPrstzIniNforz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaVisEnd2000Nforz().setWtgaVisEnd2000Nforz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIntrMora().setWtgaIntrMora(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeAntic().setWtgaManfeeAntic(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaManfeeRicor().setWtgaManfeeRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPreUniRivto().setWtgaPreUniRivto(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv1aaAcq().setWtgaProv1aaAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProv2aaAcq().setWtgaProv2aaAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvRicor().setWtgaProvRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaProvInc().setWtgaProvInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvAcq().setWtgaAlqProvAcq(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvInc().setWtgaAlqProvInc(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqProvRicor().setWtgaAlqProvRicor(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvAcq().setWtgaImpbProvAcq(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvInc().setWtgaImpbProvInc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbProvRicor().setWtgaImpbProvRicor(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaFlProvForz(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggIni().setWtgaPrstzAggIni(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPre().setWtgaIncrPre(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaIncrPrstz().setWtgaIncrPrstz(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaDtUltAdegPrePr().setWtgaDtUltAdegPrePr(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrstzAggUlt().setWtgaPrstzAggUlt(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaTsRivalNet().setWtgaTsRivalNet(new AfDecimal(0, 14, 9));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPrePattuito().setWtgaPrePattuito(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpRival("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRisMat().setWtgaRisMat(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCptMinScad().setWtgaCptMinScad(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisGest().setWtgaCommisGest(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaTpManfeeAppl("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsRiga(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsOperSql(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsVer(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsIniCptz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsTsEndCptz(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsUtente("");
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().setWtgaDsStatoElab(Types.SPACE_CHAR);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaPcCommisGest().setWtgaPcCommisGest(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaNumGgRival().setWtgaNumGgRival(0);
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTrasfe().setWtgaImpTrasfe(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpTfrStrc().setWtgaImpTfrStrc(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAcqExp().setWtgaAcqExp(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaRemunAss().setWtgaRemunAss(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCommisInter().setWtgaCommisInter(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqRemunAss().setWtgaAlqRemunAss(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaAlqCommisInter().setWtgaAlqCommisInter(new AfDecimal(0, 6, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbRemunAss().setWtgaImpbRemunAss(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaImpbCommisInter().setWtgaImpbCommisInter(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssva().setWtgaCosRunAssva(new AfDecimal(0, 15, 3));
            ws.getDtgaTabTran(idx0).getLccvtga1().getDati().getWtgaCosRunAssvaIdc().setWtgaCosRunAssvaIdc(new AfDecimal(0, 15, 3));
        }
    }

    public void initAreaIoGar() {
        ws.setDgrzEleGarMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0135Data.DGRZ_TAB_GAR_MAXOCCURS; idx0++) {
            ws.getDgrzTabGar(idx0).getLccvgrz1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().setIdPtf(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdGar(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdAdes().setWgrzIdAdes(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdPoli(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIdMoviCrz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzIdMoviChiu().setWgrzIdMoviChiu(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtIniEff(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDtEndEff(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodCompAnia(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzIbOgg("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtDecor().setWgrzDtDecor(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtScad().setWgrzDtScad(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodSez("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTari("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRamoBila("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtIniValTar().setWgrzDtIniValTar(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId1oAssto().setWgrzId1oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId2oAssto().setWgrzId2oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzId3oAssto().setWgrzId3oAssto(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpGar().setWgrzTpGar(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRsh("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTpInvst().setWgrzTpInvst(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzModPagGarcol("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPerPre("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa1oAssto().setWgrzEtaAa1oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm1oAssto().setWgrzEtaMm1oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa2oAssto().setWgrzEtaAa2oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm2oAssto().setWgrzEtaMm2oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAa3oAssto().setWgrzEtaAa3oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaMm3oAssto().setWgrzEtaMm3oAssto(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpEmisPur(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzEtaAScad().setWgrzEtaAScad(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCalcPrePrstz("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPre(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDur("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurAa().setWgrzDurAa(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurMm().setWgrzDurMm(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDurGg().setWgrzDurGg(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzNumAaPagPre().setWgrzNumAaPagPre(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaPagPreUni().setWgrzAaPagPreUni(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMmPagPreUni().setWgrzMmPagPreUni(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazIniErogRen().setWgrzFrazIniErogRen(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzMm1oRat().setWgrzMm1oRat(((short)0));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPc1oRat().setWgrzPc1oRat(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPrstzAssta("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtEndCarz().setWgrzDtEndCarz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRipPre().setWgrzPcRipPre(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodFnd("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzAaRenCer("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcRevrsb().setWgrzPcRevrsb(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpPcRip("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzPcOpz().setWgrzPcOpz(new AfDecimal(0, 6, 3));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpIas("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpStab("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpAdegPre(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtVarzTpIas().setWgrzDtVarzTpIas(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzFrazDecrCpt().setWgrzFrazDecrCpt(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzCodTratRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpDtEmisRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpCessRiass("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsRiga(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsOperSql(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsVer(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsIniCptz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsTsEndCptz(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsUtente("");
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzDsStatoElab(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzAaStab().setWgrzAaStab(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzTsStabLimitata().setWgrzTsStabLimitata(new AfDecimal(0, 14, 9));
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().getWgrzDtPresc().setWgrzDtPresc(0);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzRshInvst(Types.SPACE_CHAR);
            ws.getDgrzTabGar(idx0).getLccvgrz1().getDati().setWgrzTpRamoBila("");
        }
    }

    public void initAreaIoL19() {
        ws.setDl19EleFndMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0135Data.DL19_TAB_FND_MAXOCCURS; idx0++) {
            ws.getDl19TabFnd(idx0).getLccvl191().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19CodCompAnia(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19CodFnd("");
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DtQtz(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19ValQuo().setWl19ValQuo(new AfDecimal(0, 12, 5));
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19ValQuoManfee().setDl19ValQuoManfee(new AfDecimal(0, 12, 5));
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsOperSql(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsVer(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsTsCptz(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsUtente("");
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19DsStatoElab(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19TpFnd(Types.SPACE_CHAR);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19DtRilevazioneNav().setDl19DtRilevazioneNav(0);
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().getWl19ValQuoAcq().setDl19ValQuoAcq(new AfDecimal(0, 12, 5));
            ws.getDl19TabFnd(idx0).getLccvl191().getDati().setWl19FlNoNav(Types.SPACE_CHAR);
        }
    }

    public void initIoA2kLccc0003() {
        ws.getIoA2kLccc0003().getInput().setA2kFunz("");
        ws.getIoA2kLccc0003().getInput().setA2kInfo("");
        ws.getIoA2kLccc0003().getInput().setA2kDeltaFormatted("000");
        ws.getIoA2kLccc0003().getInput().setA2kTdelta(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kFislav(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().setA2kInicon(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getInput().getA2kIndata().setA2kIndata("");
        ws.getIoA2kLccc0003().getOutput().getA2kOugmaX().setA2kOugmaFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOugg02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus102(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOumm02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOus202(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuss02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugbmba().setOuaa02Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOuamgX().setA2kOuamgFormatted("00000000");
        ws.getIoA2kLccc0003().getOutput().setA2kOuamgp(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuamg0p(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog9(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprogc(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuprog(0);
        ws.getIoA2kLccc0003().getOutput().setA2kOuproco(0);
        ws.getIoA2kLccc0003().getOutput().getA2kOujulX().setA2kOujulFormatted("0000000");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOugg04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOumm04("");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuss04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOusta().setOuaa04Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOugg05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOumm05("");
        ws.getIoA2kLccc0003().getOutput().getA2kOurid().setOuaa05Formatted("00");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg06("");
        ws.getIoA2kLccc0003().getOutput().setA2kOung06Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOutg06(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg07Formatted("00");
        ws.getIoA2kLccc0003().getOutput().getA2kOugl07X().setA2kOufa07Formatted("000");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg08Formatted("0");
        ws.getIoA2kLccc0003().getOutput().setA2kOugg09(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().getOutput().setA2kOugg10(Types.SPACE_CHAR);
        ws.getIoA2kLccc0003().setRcode("");
    }

    public void initLdbv4911() {
        ws.getLdbv4911().setTpValAst1("");
        ws.getLdbv4911().setTpValAst2("");
        ws.getLdbv4911().setTpValAst3("");
        ws.getLdbv4911().setTpValAst4("");
        ws.getLdbv4911().setTpValAst5("");
        ws.getLdbv4911().setTpValAst6("");
        ws.getLdbv4911().setTpValAst7("");
        ws.getLdbv4911().setTpValAst8("");
        ws.getLdbv4911().setTpValAst9("");
        ws.getLdbv4911().setTpValAst10("");
        ws.getLdbv4911().setIdTrchDiGar(0);
    }

    public void initQuotzFndUnit() {
        ws.getQuotzFndUnit().setL19CodCompAnia(0);
        ws.getQuotzFndUnit().setL19CodFnd("");
        ws.getQuotzFndUnit().setL19DtQtz(0);
        ws.getQuotzFndUnit().getL19ValQuo().setL19ValQuo(new AfDecimal(0, 12, 5));
        ws.getQuotzFndUnit().getL19ValQuoManfee().setL19ValQuoManfee(new AfDecimal(0, 12, 5));
        ws.getQuotzFndUnit().setL19DsOperSql(Types.SPACE_CHAR);
        ws.getQuotzFndUnit().setL19DsVer(0);
        ws.getQuotzFndUnit().setL19DsTsCptz(0);
        ws.getQuotzFndUnit().setL19DsUtente("");
        ws.getQuotzFndUnit().setL19DsStatoElab(Types.SPACE_CHAR);
        ws.getQuotzFndUnit().setL19TpFnd(Types.SPACE_CHAR);
        ws.getQuotzFndUnit().getL19DtRilevazioneNav().setL19DtRilevazioneNav(0);
        ws.getQuotzFndUnit().getL19ValQuoAcq().setL19ValQuoAcq(new AfDecimal(0, 12, 5));
        ws.getQuotzFndUnit().setL19FlNoNav(Types.SPACE_CHAR);
    }
}
