package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.EstrCntDiagnRivDao;
import it.accenture.jnais.commons.data.to.IEstrCntDiagnRiv;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.EstrCntDiagnRiv;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ldbsf320Data;
import it.accenture.jnais.ws.redefines.P85CommisGest;
import it.accenture.jnais.ws.redefines.P85MinGarto;
import it.accenture.jnais.ws.redefines.P85MinTrnut;
import it.accenture.jnais.ws.redefines.P85PcRetr;
import it.accenture.jnais.ws.redefines.P85RendtoLrd;
import it.accenture.jnais.ws.redefines.P85RendtoRetr;
import it.accenture.jnais.ws.redefines.P85TsRivalNet;

/**Original name: LDBSF320<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  01 OTT 2014.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbsf320 extends Program implements IEstrCntDiagnRiv {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private EstrCntDiagnRivDao estrCntDiagnRivDao = new EstrCntDiagnRivDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbsf320Data ws = new Ldbsf320Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: ESTR-CNT-DIAGN-RIV
    private EstrCntDiagnRiv estrCntDiagnRiv;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBSF320_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, EstrCntDiagnRiv estrCntDiagnRiv) {
        this.idsv0003 = idsv0003;
        this.estrCntDiagnRiv = estrCntDiagnRiv;
        // COB_CODE: MOVE IDSV0003-BUFFER-WHERE-COND TO LDBVF321.
        ws.getLdbvf321().setLdbvf321Formatted(this.idsv0003.getBufferWhereCondFormatted());
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: MOVE LDBVF321 TO IDSV0003-BUFFER-WHERE-COND.
        this.idsv0003.setBufferWhereCond(ws.getLdbvf321().getLdbvf321Formatted());
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbsf320 getInstance() {
        return ((Ldbsf320)Programs.getInstance(Ldbsf320.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBSF320'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBSF320");
        // COB_CODE: MOVE 'ESTR-CNT-DIAGN-RIV' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("ESTR-CNT-DIAGN-RIV");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //              DECLARE C-NST CURSOR FOR
        //              SELECT
        //                     COD_COMP_ANIA
        //                    ,ID_POLI
        //                    ,DT_RIVAL
        //                    ,COD_TARI
        //                    ,RENDTO_LRD
        //                    ,PC_RETR
        //                    ,RENDTO_RETR
        //                    ,COMMIS_GEST
        //                    ,TS_RIVAL_NET
        //                    ,MIN_GARTO
        //                    ,MIN_TRNUT
        //                    ,IB_POLI
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //              FROM ESTR_CNT_DIAGN_RIV
        //              WHERE               ID_POLI = :LDBVF321-ID-POLI
        //                        AND DT_RIVAL >  :LDBVF321-DT-RIV-DA-DB
        //                        AND DT_RIVAL <=:LDBVF321-DT-RIV-A-DB
        //                        AND COD_TARI = :LDBVF321-COD-TARI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_RIVAL DESC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     COD_COMP_ANIA
        //                    ,ID_POLI
        //                    ,DT_RIVAL
        //                    ,COD_TARI
        //                    ,RENDTO_LRD
        //                    ,PC_RETR
        //                    ,RENDTO_RETR
        //                    ,COMMIS_GEST
        //                    ,TS_RIVAL_NET
        //                    ,MIN_GARTO
        //                    ,MIN_TRNUT
        //                    ,IB_POLI
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //             INTO
        //                :P85-COD-COMP-ANIA
        //               ,:P85-ID-POLI
        //               ,:P85-DT-RIVAL-DB
        //               ,:P85-COD-TARI
        //               ,:P85-RENDTO-LRD
        //                :IND-P85-RENDTO-LRD
        //               ,:P85-PC-RETR
        //                :IND-P85-PC-RETR
        //               ,:P85-RENDTO-RETR
        //                :IND-P85-RENDTO-RETR
        //               ,:P85-COMMIS-GEST
        //                :IND-P85-COMMIS-GEST
        //               ,:P85-TS-RIVAL-NET
        //                :IND-P85-TS-RIVAL-NET
        //               ,:P85-MIN-GARTO
        //                :IND-P85-MIN-GARTO
        //               ,:P85-MIN-TRNUT
        //                :IND-P85-MIN-TRNUT
        //               ,:P85-IB-POLI
        //               ,:P85-DS-OPER-SQL
        //               ,:P85-DS-VER
        //               ,:P85-DS-TS-CPTZ
        //               ,:P85-DS-UTENTE
        //               ,:P85-DS-STATO-ELAB
        //             FROM ESTR_CNT_DIAGN_RIV
        //             WHERE               ID_POLI = :LDBVF321-ID-POLI
        //                    AND DT_RIVAL >  :LDBVF321-DT-RIV-DA-DB
        //                    AND DT_RIVAL <= :LDBVF321-DT-RIV-A-DB
        //                    AND COD_TARI = :LDBVF321-COD-TARI
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_RIVAL DESC
        //           END-EXEC.
        estrCntDiagnRivDao.selectRec2(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: EXEC SQL
        //                OPEN C-NST
        //           END-EXEC.
        estrCntDiagnRivDao.openCNst26(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-NST
        //           END-EXEC.
        estrCntDiagnRivDao.closeCNst26();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: EXEC SQL
        //                FETCH C-NST
        //           INTO
        //                :P85-COD-COMP-ANIA
        //               ,:P85-ID-POLI
        //               ,:P85-DT-RIVAL-DB
        //               ,:P85-COD-TARI
        //               ,:P85-RENDTO-LRD
        //                :IND-P85-RENDTO-LRD
        //               ,:P85-PC-RETR
        //                :IND-P85-PC-RETR
        //               ,:P85-RENDTO-RETR
        //                :IND-P85-RENDTO-RETR
        //               ,:P85-COMMIS-GEST
        //                :IND-P85-COMMIS-GEST
        //               ,:P85-TS-RIVAL-NET
        //                :IND-P85-TS-RIVAL-NET
        //               ,:P85-MIN-GARTO
        //                :IND-P85-MIN-GARTO
        //               ,:P85-MIN-TRNUT
        //                :IND-P85-MIN-TRNUT
        //               ,:P85-IB-POLI
        //               ,:P85-DS-OPER-SQL
        //               ,:P85-DS-VER
        //               ,:P85-DS-TS-CPTZ
        //               ,:P85-DS-UTENTE
        //               ,:P85-DS-STATO-ELAB
        //           END-EXEC.
        estrCntDiagnRivDao.fetchCNst26(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST THRU C270-EX
            c270CloseCursorWcNst();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-P85-RENDTO-LRD = -1
        //              MOVE HIGH-VALUES TO P85-RENDTO-LRD-NULL
        //           END-IF
        if (ws.getIndEstrCntDiagnRiv().getLabelErr() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P85-RENDTO-LRD-NULL
            estrCntDiagnRiv.getP85RendtoLrd().setP85RendtoLrdNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P85RendtoLrd.Len.P85_RENDTO_LRD_NULL));
        }
        // COB_CODE: IF IND-P85-PC-RETR = -1
        //              MOVE HIGH-VALUES TO P85-PC-RETR-NULL
        //           END-IF
        if (ws.getIndEstrCntDiagnRiv().getOperTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P85-PC-RETR-NULL
            estrCntDiagnRiv.getP85PcRetr().setP85PcRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P85PcRetr.Len.P85_PC_RETR_NULL));
        }
        // COB_CODE: IF IND-P85-RENDTO-RETR = -1
        //              MOVE HIGH-VALUES TO P85-RENDTO-RETR-NULL
        //           END-IF
        if (ws.getIndEstrCntDiagnRiv().getNomeTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P85-RENDTO-RETR-NULL
            estrCntDiagnRiv.getP85RendtoRetr().setP85RendtoRetrNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P85RendtoRetr.Len.P85_RENDTO_RETR_NULL));
        }
        // COB_CODE: IF IND-P85-COMMIS-GEST = -1
        //              MOVE HIGH-VALUES TO P85-COMMIS-GEST-NULL
        //           END-IF
        if (ws.getIndEstrCntDiagnRiv().getStatusTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P85-COMMIS-GEST-NULL
            estrCntDiagnRiv.getP85CommisGest().setP85CommisGestNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P85CommisGest.Len.P85_COMMIS_GEST_NULL));
        }
        // COB_CODE: IF IND-P85-TS-RIVAL-NET = -1
        //              MOVE HIGH-VALUES TO P85-TS-RIVAL-NET-NULL
        //           END-IF
        if (ws.getIndEstrCntDiagnRiv().getKeyTabella() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P85-TS-RIVAL-NET-NULL
            estrCntDiagnRiv.getP85TsRivalNet().setP85TsRivalNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P85TsRivalNet.Len.P85_TS_RIVAL_NET_NULL));
        }
        // COB_CODE: IF IND-P85-MIN-GARTO = -1
        //              MOVE HIGH-VALUES TO P85-MIN-GARTO-NULL
        //           END-IF
        if (ws.getIndEstrCntDiagnRiv().getTipoOggetto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P85-MIN-GARTO-NULL
            estrCntDiagnRiv.getP85MinGarto().setP85MinGartoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P85MinGarto.Len.P85_MIN_GARTO_NULL));
        }
        // COB_CODE: IF IND-P85-MIN-TRNUT = -1
        //              MOVE HIGH-VALUES TO P85-MIN-TRNUT-NULL
        //           END-IF.
        if (ws.getIndEstrCntDiagnRiv().getIbOggetto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO P85-MIN-TRNUT-NULL
            estrCntDiagnRiv.getP85MinTrnut().setP85MinTrnutNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, P85MinTrnut.Len.P85_MIN_TRNUT_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE P85-DT-RIVAL-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getP85DtRivalDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO P85-DT-RIVAL.
        estrCntDiagnRiv.setP85DtRival(ws.getIdsv0010().getWsDateN());
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
        // COB_CODE: MOVE LDBVF321-DT-RIV-DA TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ws.getLdbvf321().getDtRivDa(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO LDBVF321-DT-RIV-DA-DB
        ws.getLdbvf321().setDtRivDaDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE LDBVF321-DT-RIV-A TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(ws.getLdbvf321().getDtRivA(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO LDBVF321-DT-RIV-A-DB.
        ws.getLdbvf321().setDtRivADb(ws.getIdsv0010().getWsDateX());
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getCodCompAnia() {
        return estrCntDiagnRiv.getP85CodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.estrCntDiagnRiv.setP85CodCompAnia(codCompAnia);
    }

    @Override
    public String getCodTari() {
        return estrCntDiagnRiv.getP85CodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.estrCntDiagnRiv.setP85CodTari(codTari);
    }

    @Override
    public AfDecimal getCommisGest() {
        return estrCntDiagnRiv.getP85CommisGest().getP85CommisGest();
    }

    @Override
    public void setCommisGest(AfDecimal commisGest) {
        this.estrCntDiagnRiv.getP85CommisGest().setP85CommisGest(commisGest.copy());
    }

    @Override
    public AfDecimal getCommisGestObj() {
        if (ws.getIndEstrCntDiagnRiv().getStatusTabella() >= 0) {
            return getCommisGest();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCommisGestObj(AfDecimal commisGestObj) {
        if (commisGestObj != null) {
            setCommisGest(new AfDecimal(commisGestObj, 15, 3));
            ws.getIndEstrCntDiagnRiv().setStatusTabella(((short)0));
        }
        else {
            ws.getIndEstrCntDiagnRiv().setStatusTabella(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return estrCntDiagnRiv.getP85DsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.estrCntDiagnRiv.setP85DsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return estrCntDiagnRiv.getP85DsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.estrCntDiagnRiv.setP85DsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsCptz() {
        return estrCntDiagnRiv.getP85DsTsCptz();
    }

    @Override
    public void setDsTsCptz(long dsTsCptz) {
        this.estrCntDiagnRiv.setP85DsTsCptz(dsTsCptz);
    }

    @Override
    public String getDsUtente() {
        return estrCntDiagnRiv.getP85DsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.estrCntDiagnRiv.setP85DsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return estrCntDiagnRiv.getP85DsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.estrCntDiagnRiv.setP85DsVer(dsVer);
    }

    @Override
    public String getDtRivalDb() {
        return ws.getP85DtRivalDb();
    }

    @Override
    public void setDtRivalDb(String dtRivalDb) {
        this.ws.setP85DtRivalDb(dtRivalDb);
    }

    @Override
    public String getIbPoli() {
        return estrCntDiagnRiv.getP85IbPoli();
    }

    @Override
    public void setIbPoli(String ibPoli) {
        this.estrCntDiagnRiv.setP85IbPoli(ibPoli);
    }

    @Override
    public int getIdPoli() {
        return estrCntDiagnRiv.getP85IdPoli();
    }

    @Override
    public void setIdPoli(int idPoli) {
        this.estrCntDiagnRiv.setP85IdPoli(idPoli);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public String getLdbvf321CodTari() {
        return ws.getLdbvf321().getCodTari();
    }

    @Override
    public void setLdbvf321CodTari(String ldbvf321CodTari) {
        this.ws.getLdbvf321().setCodTari(ldbvf321CodTari);
    }

    @Override
    public String getLdbvf321DtRivADb() {
        return ws.getLdbvf321().getDtRivADb();
    }

    @Override
    public void setLdbvf321DtRivADb(String ldbvf321DtRivADb) {
        this.ws.getLdbvf321().setDtRivADb(ldbvf321DtRivADb);
    }

    @Override
    public String getLdbvf321DtRivDaDb() {
        return ws.getLdbvf321().getDtRivDaDb();
    }

    @Override
    public void setLdbvf321DtRivDaDb(String ldbvf321DtRivDaDb) {
        this.ws.getLdbvf321().setDtRivDaDb(ldbvf321DtRivDaDb);
    }

    @Override
    public int getLdbvf321IdPoli() {
        return ws.getLdbvf321().getIdPoli();
    }

    @Override
    public void setLdbvf321IdPoli(int ldbvf321IdPoli) {
        this.ws.getLdbvf321().setIdPoli(ldbvf321IdPoli);
    }

    @Override
    public AfDecimal getMinGarto() {
        return estrCntDiagnRiv.getP85MinGarto().getP85MinGarto();
    }

    @Override
    public void setMinGarto(AfDecimal minGarto) {
        this.estrCntDiagnRiv.getP85MinGarto().setP85MinGarto(minGarto.copy());
    }

    @Override
    public AfDecimal getMinGartoObj() {
        if (ws.getIndEstrCntDiagnRiv().getTipoOggetto() >= 0) {
            return getMinGarto();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMinGartoObj(AfDecimal minGartoObj) {
        if (minGartoObj != null) {
            setMinGarto(new AfDecimal(minGartoObj, 14, 9));
            ws.getIndEstrCntDiagnRiv().setTipoOggetto(((short)0));
        }
        else {
            ws.getIndEstrCntDiagnRiv().setTipoOggetto(((short)-1));
        }
    }

    @Override
    public AfDecimal getMinTrnut() {
        return estrCntDiagnRiv.getP85MinTrnut().getP85MinTrnut();
    }

    @Override
    public void setMinTrnut(AfDecimal minTrnut) {
        this.estrCntDiagnRiv.getP85MinTrnut().setP85MinTrnut(minTrnut.copy());
    }

    @Override
    public AfDecimal getMinTrnutObj() {
        if (ws.getIndEstrCntDiagnRiv().getIbOggetto() >= 0) {
            return getMinTrnut();
        }
        else {
            return null;
        }
    }

    @Override
    public void setMinTrnutObj(AfDecimal minTrnutObj) {
        if (minTrnutObj != null) {
            setMinTrnut(new AfDecimal(minTrnutObj, 14, 9));
            ws.getIndEstrCntDiagnRiv().setIbOggetto(((short)0));
        }
        else {
            ws.getIndEstrCntDiagnRiv().setIbOggetto(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRetr() {
        return estrCntDiagnRiv.getP85PcRetr().getP85PcRetr();
    }

    @Override
    public void setPcRetr(AfDecimal pcRetr) {
        this.estrCntDiagnRiv.getP85PcRetr().setP85PcRetr(pcRetr.copy());
    }

    @Override
    public AfDecimal getPcRetrObj() {
        if (ws.getIndEstrCntDiagnRiv().getOperTabella() >= 0) {
            return getPcRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRetrObj(AfDecimal pcRetrObj) {
        if (pcRetrObj != null) {
            setPcRetr(new AfDecimal(pcRetrObj, 6, 3));
            ws.getIndEstrCntDiagnRiv().setOperTabella(((short)0));
        }
        else {
            ws.getIndEstrCntDiagnRiv().setOperTabella(((short)-1));
        }
    }

    @Override
    public AfDecimal getRendtoLrd() {
        return estrCntDiagnRiv.getP85RendtoLrd().getP85RendtoLrd();
    }

    @Override
    public void setRendtoLrd(AfDecimal rendtoLrd) {
        this.estrCntDiagnRiv.getP85RendtoLrd().setP85RendtoLrd(rendtoLrd.copy());
    }

    @Override
    public AfDecimal getRendtoLrdObj() {
        if (ws.getIndEstrCntDiagnRiv().getLabelErr() >= 0) {
            return getRendtoLrd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRendtoLrdObj(AfDecimal rendtoLrdObj) {
        if (rendtoLrdObj != null) {
            setRendtoLrd(new AfDecimal(rendtoLrdObj, 14, 9));
            ws.getIndEstrCntDiagnRiv().setLabelErr(((short)0));
        }
        else {
            ws.getIndEstrCntDiagnRiv().setLabelErr(((short)-1));
        }
    }

    @Override
    public AfDecimal getRendtoRetr() {
        return estrCntDiagnRiv.getP85RendtoRetr().getP85RendtoRetr();
    }

    @Override
    public void setRendtoRetr(AfDecimal rendtoRetr) {
        this.estrCntDiagnRiv.getP85RendtoRetr().setP85RendtoRetr(rendtoRetr.copy());
    }

    @Override
    public AfDecimal getRendtoRetrObj() {
        if (ws.getIndEstrCntDiagnRiv().getNomeTabella() >= 0) {
            return getRendtoRetr();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRendtoRetrObj(AfDecimal rendtoRetrObj) {
        if (rendtoRetrObj != null) {
            setRendtoRetr(new AfDecimal(rendtoRetrObj, 14, 9));
            ws.getIndEstrCntDiagnRiv().setNomeTabella(((short)0));
        }
        else {
            ws.getIndEstrCntDiagnRiv().setNomeTabella(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsRivalNet() {
        return estrCntDiagnRiv.getP85TsRivalNet().getP85TsRivalNet();
    }

    @Override
    public void setTsRivalNet(AfDecimal tsRivalNet) {
        this.estrCntDiagnRiv.getP85TsRivalNet().setP85TsRivalNet(tsRivalNet.copy());
    }

    @Override
    public AfDecimal getTsRivalNetObj() {
        if (ws.getIndEstrCntDiagnRiv().getKeyTabella() >= 0) {
            return getTsRivalNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsRivalNetObj(AfDecimal tsRivalNetObj) {
        if (tsRivalNetObj != null) {
            setTsRivalNet(new AfDecimal(tsRivalNetObj, 14, 9));
            ws.getIndEstrCntDiagnRiv().setKeyTabella(((short)0));
        }
        else {
            ws.getIndEstrCntDiagnRiv().setKeyTabella(((short)-1));
        }
    }
}
