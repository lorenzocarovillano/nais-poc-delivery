package it.accenture.jnais;

import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs0109Data;

/**Original name: LVVS0109<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2007.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS0109
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... CONVERSIONE DATA DECORRENZA POLIZZA
 * **------------------------------------------------------------***</pre>*/
public class Lvvs0109 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs0109Data ws = new Lvvs0109Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS0109
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS0109_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: PERFORM S1000-ELABORAZIONE
        //              THRU EX-S1000
        s1000Elaborazione();
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs0109 getInstance() {
        return ((Lvvs0109)Programs.getInstance(Lvvs0109.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-RAN
        //                      WK-DATA-OUTPUT.
        initAreaIoRan();
        ws.setWkDataOutput(new AfDecimal(0, 11, 7));
        //
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE.
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1200-CONTRLLO-DATI         THRU S1200-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-CONTRLLO-DATI         THRU S1200-EX
            s1200ContrlloDati();
        }
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //               PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1260-CALCOLA-DIFF          THRU S1260-EX
            s1260CalcolaDiff();
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-RAPP-ANAG
        //                TO DRAN-AREA-RAN
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasRappAnag())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DRAN-AREA-RAN
            ws.setDranAreaRanFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-CONTRLLO-DATI<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA VDA
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200ContrlloDati() {
        // COB_CODE: IF DRAN-DT-NASC(IVVC0213-IX-TABB) NOT NUMERIC
        //                TO IDSV0003-DESCRIZ-ERR-DB2
        //           ELSE
        //              END-IF
        //           END-IF.
        if (!Functions.isNumber(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranDtNasc().getWranDtNasc())) {
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED     TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA DI NASCITA 1  ASS NON VALORIZZATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA DI NASCITA 1  ASS NON VALORIZZATA");
        }
        else if (ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranDtNasc().getWranDtNasc() == 0) {
            // COB_CODE: IF DRAN-DT-NASC(IVVC0213-IX-TABB) = ZEROES
            //                TO IDSV0003-DESCRIZ-ERR-DB2
            //           END-IF
            // COB_CODE: SET  IDSV0003-FIELD-NOT-VALUED  TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
            // COB_CODE: MOVE WK-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkPgm());
            // COB_CODE: MOVE 'DATA DI NASCITA 1  ASS NON VALORIZZATA'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("DATA DI NASCITA 1  ASS NON VALORIZZATA");
        }
    }

    /**Original name: S1260-CALCOLA-DIFF<br>
	 * <pre>----------------------------------------------------------------*
	 *   CALCOLA DATA 1
	 * ----------------------------------------------------------------*</pre>*/
    private void s1260CalcolaDiff() {
        Lccs1750 lccs1750 = null;
        // COB_CODE: MOVE  DRAN-DT-NASC(IVVC0213-IX-TABB)
        //                                            TO LCCC1751-DATA-INFERIORE.
        ws.getLccc1751().setDataInferiore(TruncAbs.toInt(ws.getDranTabRappAnag(ivvc0213.getIxTabb()).getLccvran1().getDati().getWranDtNasc().getWranDtNasc(), 8));
        // COB_CODE: MOVE  IVVC0213-DT-DECOR          TO LCCC1751-DATA-SUPERIORE.
        ws.getLccc1751().setDataSuperioreFormatted(ivvc0213.getDatiLivello().getIvvc0213DtDecorFormatted());
        // COB_CODE: CALL LCCS1750 USING LCCC1751
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL.
        try {
            lccs1750 = Lccs1750.getInstance();
            lccs1750.run(ws.getLccc1751());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'ERRORE CHIAMATA LCCS1751'
            //             TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CHIAMATA LCCS1751");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF LCCC1751-RETURN-CODE = SPACE
        //              MOVE LCCC1751-ETA-AA-ASSICURATO TO IVVC0213-VAL-IMP-O
        //           ELSE
        //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           END-IF.
        if (Characters.EQ_SPACE.test(ws.getLccc1751().getReturnCode())) {
            // COB_CODE: IF LCCC1751-ETA-MM-ASSICURATO GREATER 5
            //              ADD 1                     TO LCCC1751-ETA-AA-ASSICURATO
            //           END-IF
            if (ws.getLccc1751().getEtaMmAssicurato() > 5) {
                // COB_CODE: ADD 1                     TO LCCC1751-ETA-AA-ASSICURATO
                ws.getLccc1751().setEtaAaAssicurato(Trunc.toInt(1 + ws.getLccc1751().getEtaAaAssicurato(), 5));
            }
            // COB_CODE: MOVE LCCC1751-ETA-AA-ASSICURATO TO IVVC0213-VAL-IMP-O
            ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(ws.getLccc1751().getEtaAaAssicurato(), 18, 7));
        }
        else {
            // COB_CODE: MOVE WK-CALL-PGM            TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE LCCC1751-DESCRIZ-ERR   TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getLccc1751().getDescrizErr());
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            idsv0003.getReturnCode().setFieldNotValued();
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
        ws.setIxTabRan(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoRan() {
        ws.setDranEleRanMax(((short)0));
        for (int idx0 = 1; idx0 <= Lvvs0109Data.DRAN_TAB_RAPP_ANAG_MAXOCCURS; idx0++) {
            ws.getDranTabRappAnag(idx0).getLccvran1().getStatus().setStatus(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().setIdPtf(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIdRappAna(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranIdRappAnaCollg().setWranIdRappAnaCollg(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIdOgg(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpOgg("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIdMoviCrz(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranIdMoviChiu().setWranIdMoviChiu(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDtIniEff(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDtEndEff(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodCompAnia(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodSogg("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpRappAna("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpPers(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranSex(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranDtNasc().setWranDtNasc(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlEstas(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir1("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir2("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndir3("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir1("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir2("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpUtlzIndir3("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrCntCorrAccr("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrCntCorrAdd("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranEstrDocto("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranPcNelRapp().setWranPcNelRapp(new AfDecimal(0, 6, 3));
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpMezPagAdd("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpMezPagAccr("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodMatr("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpAdegz("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlTstRsh(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodAz("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndPrinc("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranDtDeliberaCda().setWranDtDeliberaCda(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDlgAlRisc(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranLegaleRapprPrinc(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpLegaleRappr("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpIndPrinc("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpStatRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranNomeIntRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCognIntRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCognIntTratt("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranNomeIntTratt("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCfIntRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlCoincDipCntr(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlCoincIntCntr(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().getWranDtDeces().setWranDtDeces(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlFumatore(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsRiga(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsOperSql(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsVer(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsTsIniCptz(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsTsEndCptz(0);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsUtente("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranDsStatoElab(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranFlLavDip(Types.SPACE_CHAR);
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpVarzPagat("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCodRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranTpCausRid("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranIndMassaCorp("");
            ws.getDranTabRappAnag(idx0).getLccvran1().getDati().setWranCatRshProf("");
        }
    }
}
