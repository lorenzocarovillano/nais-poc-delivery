package it.accenture.jnais;

import com.bphx.ctu.af.core.marshal.MarshalByteExt;
import com.bphx.ctu.af.core.program.GenericParam;
import com.bphx.ctu.af.core.ReturnException;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.lang.types.RoundingMode;
import com.bphx.ctu.af.lang.util.MathUtil;
import com.bphx.ctu.af.util.ConcatUtil;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.Trunc;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.core.ProgramExecutionException;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.copy.Idsv0003CampiEsito;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.Ivvc0213;
import it.accenture.jnais.ws.Lvvs3150Data;

/**Original name: LVVS3150<br>
 * <pre>*****************************************************************
 * *                                                              **
 * *    PORTAFOGLIO VITA ITALIA                                   **
 * *                                                              **
 * *****************************************************************
 * AUTHOR.             ATS.
 * DATE-WRITTEN.       2014.
 * DATE-COMPILED.
 * **------------------------------------------------------------***
 *   PROGRAMMA...... LVVS3150
 *   TIPOLOGIA...... SERVIZIO
 *   PROCESSO....... XXX
 *   FUNZIONE....... XXX
 *   DESCRIZIONE.... NUOVA FISCALITA' 2014
 *                   CALCOLO DELLA VARIABILE RATEO_CED_2014
 * **------------------------------------------------------------***</pre>*/
public class Lvvs3150 extends Program {

    //==== PROPERTIES ====
    //Original name: WORKING-STORAGE
    private Lvvs3150Data ws = new Lvvs3150Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: INPUT-LVVS3150
    private Ivvc0213 ivvc0213;

    //==== METHODS ====
    /**Original name: PROGRAM_LVVS3150_FIRST_SENTENCES<br>
	 * <pre>----------------------------------------------------------------*</pre>*/
    public long execute(Idsv0003 idsv0003, Ivvc0213 ivvc0213) {
        this.idsv0003 = idsv0003;
        this.ivvc0213 = ivvc0213;
        // COB_CODE: PERFORM S0000-OPERAZIONI-INIZIALI
        //              THRU EX-S0000.
        s0000OperazioniIniziali();
        // COB_CODE: MOVE IVVC0213-TIPO-MOVIMENTO  TO WS-MOVIMENTO
        ws.getWsMovimento().setWsMovimentoFormatted(this.ivvc0213.getTipoMovimentoFormatted());
        // COB_CODE: IF GENER-CEDOLA
        //                THRU EX-S1000
        //           ELSE
        //             SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
        //           END-IF.
        if (ws.getWsMovimento().isGenerCedola()) {
            // COB_CODE: PERFORM S1000-ELABORAZIONE
            //              THRU EX-S1000
            s1000Elaborazione();
        }
        else {
            // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            this.idsv0003.getReturnCode().setFieldNotValued();
        }
        // COB_CODE: PERFORM S9000-OPERAZIONI-FINALI
        //              THRU EX-S9000.
        s9000OperazioniFinali();
        return 0;
    }

    public static Lvvs3150 getInstance() {
        return ((Lvvs3150)Programs.getInstance(Lvvs3150.class));
    }

    /**Original name: S0000-OPERAZIONI-INIZIALI<br>
	 * <pre>----------------------------------------------------------------*
	 *   OPERAZIONI INIZIALI                                           *
	 * ----------------------------------------------------------------*</pre>*/
    private void s0000OperazioniIniziali() {
        // COB_CODE: INITIALIZE                        IX-INDICI
        //                                             IVVC0213-TAB-OUTPUT.
        initIxIndici();
        initTabOutput();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL       TO TRUE.
        idsv0003.getSqlcode().setSuccessfulSql();
        // COB_CODE: SET IDSV0003-SUCCESSFUL-RC        TO TRUE.
        idsv0003.getReturnCode().setIdsv0003SuccessfulRc();
        // COB_CODE: MOVE IVVC0213-AREA-VARIABILE
        //             TO IVVC0213-TAB-OUTPUT.
        ivvc0213.getTabOutput().setTabOutputBytes(ivvc0213.getDatiLivello().getIvvc0213AreaVariabileBytes());
    }

    /**Original name: S1000-ELABORAZIONE<br>
	 * <pre>----------------------------------------------------------------*
	 *     ELABORAZIONE
	 * ----------------------------------------------------------------*</pre>*/
    private void s1000Elaborazione() {
        // COB_CODE: INITIALIZE AREA-IO-POL.
        initAreaIoPol();
        //--> ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
        //--> RISPETTIVE AREE DCLGEN IN WORKING
        // COB_CODE: PERFORM S1100-VALORIZZA-DCLGEN
        //              THRU S1100-VALORIZZA-DCLGEN-EX
        //           VARYING IX-DCLGEN FROM 1 BY 1
        //             UNTIL IX-DCLGEN > IVVC0213-ELE-INFO-MAX
        //                OR IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //                   SPACES OR LOW-VALUE OR HIGH-VALUE
        ws.setIxDclgen(((short)1));
        while (!(ws.getIxDclgen() > ivvc0213.getEleInfoMax() || Characters.EQ_SPACE.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias()) || Characters.EQ_LOW.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()) || Characters.EQ_HIGH.test(ivvc0213.getTabInfo(ws.getIxDclgen()).getIvvc0213TabAliasFormatted()))) {
            s1100ValorizzaDclgen();
            ws.setIxDclgen(Trunc.toShort(ws.getIxDclgen() + 1, 4));
        }
        //--> RECUPERA PARAMETRO MOVIMENTO
        // COB_CODE: IF  IDSV0003-SUCCESSFUL-RC
        //           AND IDSV0003-SUCCESSFUL-SQL
        //                  THRU S1200-RECUP-PARAM-MOVI-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM S1200-RECUP-PARAM-MOVI
            //              THRU S1200-RECUP-PARAM-MOVI-EX
            s1200RecupParamMovi();
        }
        //--> CALCOLO RATEOCED2014
        //    AND IDSV0003-SUCCESSFUL-SQL
        // COB_CODE:      IF  IDSV0003-SUCCESSFUL-RC
        //           *    AND IDSV0003-SUCCESSFUL-SQL
        //                AND IDSV0003-NOT-FOUND
        //                AND WK-PARAM-MOVI-OK
        //                 END-IF
        //                END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc() && idsv0003.getSqlcode().isNotFound() && ws.getWkParamMovi().isOk()) {
            // COB_CODE: IF  WK-DT-RICOR-SUCC >= 20140701
            //           AND WK-DT-RICOR-SUCC <= 20150629
            //                 THRU S1300-CALCOLO-RATEOCED2014-EX
            //           ELSE
            //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
            //           END-IF
            if (ws.getWkDtRicorSucc() >= 20140701 && ws.getWkDtRicorSucc() <= 20150629) {
                // COB_CODE: PERFORM S1300-CALCOLO-RATEOCED2014
                //              THRU S1300-CALCOLO-RATEOCED2014-EX
                s1300CalcoloRateoced2014();
            }
            else {
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
        }
    }

    /**Original name: S1100-VALORIZZA-DCLGEN<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINE PER DISTRIBUIRE LE DCLGEN DI INPUT NELLE
	 *     RISPETTIVE AREE DCLGEN IN WORKING
	 * ----------------------------------------------------------------*</pre>*/
    private void s1100ValorizzaDclgen() {
        // COB_CODE: IF IVVC0213-TAB-ALIAS(IX-DCLGEN) =
        //              IVVC0218-ALIAS-POLI
        //                TO DPOL-AREA-POLIZZA
        //           END-IF.
        if (Conditions.eq(ivvc0213.getTabInfo(ws.getIxDclgen()).getTabAlias(), ws.getIvvc0218().getAliasPoli())) {
            // COB_CODE: MOVE IVVC0213-BUFFER-DATI
            //               (IVVC0213-POSIZ-INI(IX-DCLGEN) :
            //                IVVC0213-LUNGHEZZA(IX-DCLGEN))
            //             TO DPOL-AREA-POLIZZA
            ws.setDpolAreaPolizzaFormatted(ivvc0213.getBufferDatiFormatted().substring((ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni()) - 1, ivvc0213.getTabInfo(ws.getIxDclgen()).getPosizIni() + ivvc0213.getTabInfo(ws.getIxDclgen()).getLunghezza() - 1));
        }
    }

    /**Original name: S1200-RECUP-PARAM-MOVI<br>
	 * <pre>----------------------------------------------------------------*
	 *    RECUPERA PARAMETRO MOVIMENTO
	 * ----------------------------------------------------------------*</pre>*/
    private void s1200RecupParamMovi() {
        Ldbs5060 ldbs5060 = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: SET WK-PARAM-MOVI-KO             TO TRUE
        ws.getWkParamMovi().setKo();
        //    SET IDSV0003-SELECT              TO TRUE
        // COB_CODE: SET IDSV0003-FETCH-FIRST         TO TRUE
        idsv0003.getOperazione().setFetchFirst();
        // COB_CODE: SET IDSV0003-WHERE-CONDITION     TO TRUE
        idsv0003.getLivelloOperazione().setWhereCondition();
        // COB_CODE: SET IDSV0003-TRATT-X-COMPETENZA  TO TRUE
        idsv0003.getTrattamentoStoricita().setTrattXCompetenza();
        //
        // COB_CODE: INITIALIZE PARAM-MOVI
        //                      LDBV5061
        //                      IDSV0003-BUFFER-WHERE-COND
        //                      WK-DT-RICOR-PREC
        //                      WK-DT-RICOR-SUCC.
        initParamMovi();
        initLdbv5061();
        idsv0003.setBufferWhereCond("");
        ws.setWkDtRicorPrecFormatted("00000000");
        ws.setWkDtRicorSuccFormatted("00000000");
        //
        // COB_CODE: MOVE DPOL-ID-POLI             TO LDBV5061-ID-POLI.
        ws.getLdbv5061().setIdPoli(ws.getLccvpol1().getDati().getWpolIdPoli());
        // COB_CODE: SET GENER-CEDOLA              TO TRUE.
        ws.getWsMovimento().setGenerCedola();
        // COB_CODE: MOVE WS-MOVIMENTO             TO LDBV5061-TP-MOVI-01.
        ws.getLdbv5061().setTpMovi01(ws.getWsMovimento().getWsMovimento());
        // COB_CODE: MOVE LDBV5061                 TO IDSV0003-BUFFER-WHERE-COND.
        idsv0003.setBufferWhereCond(ws.getLdbv5061().getLdbv5061Formatted());
        // COB_CODE: MOVE 'LDBS5060'               TO WK-CALL-PGM
        ws.setWkCallPgm("LDBS5060");
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-RC
        //                      OR NOT IDSV0003-SUCCESSFUL-SQL
        //           END-IF
        //           END-PERFORM.
        while (!(!idsv0003.getReturnCode().isSuccessfulRc() || !idsv0003.getSqlcode().isSuccessfulSql())) {
            //
            // COB_CODE:      CALL WK-CALL-PGM  USING  IDSV0003 PARAM-MOVI
            //           *
            //                ON EXCEPTION
            //                     SET IDSV0003-INVALID-OPER  TO TRUE
            //                END-CALL
            try {
                ldbs5060 = Ldbs5060.getInstance();
                ldbs5060.run(idsv0003, ws.getParamMovi());
            }
            catch (ProgramExecutionException __ex) {
                // COB_CODE: MOVE WK-CALL-PGM
                //             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: MOVE 'CALL-LDBS5060 ERRORE CHIAMATA '
                //              TO IDSV0003-DESCRIZ-ERR-DB2
                idsv0003.getCampiEsito().setDescrizErrDb2("CALL-LDBS5060 ERRORE CHIAMATA ");
                // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
                idsv0003.getReturnCode().setInvalidOper();
            }
            //
            // COB_CODE:      IF IDSV0003-SUCCESSFUL-RC
            //           *    AND IDSV0003-SUCCESSFUL-SQL
            //                 END-IF
            //                ELSE
            //                   END-IF
            //                END-IF
            if (idsv0003.getReturnCode().isSuccessfulRc()) {
                //    AND IDSV0003-SUCCESSFUL-SQL
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //             SET IDSV0003-FETCH-NEXT      TO TRUE
                //           ELSE
                //            END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE:         IF  PMO-DT-RICOR-PREC-NULL NOT EQUAL HIGH-VALUE
                    //                   AND PMO-DT-RICOR-PREC-NULL NOT EQUAL LOW-VALUE
                    //                   AND PMO-DT-RICOR-PREC-NULL NOT EQUAL SPACES
                    //           *          INITIALIZE WK-DT-RICOR-PREC
                    //                    END-IF
                    //           *       ELSE
                    //           *          INITIALIZE WK-DT-RICOR-PREC
                    //           *          MOVE DPOL-DT-DECOR
                    //           *            TO WK-DT-RICOR-PREC
                    //           *          SET WK-PARAM-MOVI-OK
                    //           *           TO TRUE
                    //                   END-IF
                    if (!Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrecNullFormatted()) && !Characters.EQ_LOW.test(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrecNullFormatted()) && !Characters.EQ_SPACE.test(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrecNull())) {
                        //          INITIALIZE WK-DT-RICOR-PREC
                        // COB_CODE:          IF PMO-DT-RICOR-PREC > WK-DT-RICOR-PREC
                        //                        TO WK-DT-RICOR-PREC
                        //           *          SET WK-PARAM-MOVI-OK
                        //           *           TO TRUE
                        //                    END-IF
                        if (ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrec() > ws.getWkDtRicorPrec()) {
                            // COB_CODE: MOVE PMO-DT-RICOR-PREC
                            //             TO WK-DT-RICOR-PREC
                            ws.setWkDtRicorPrec(TruncAbs.toInt(ws.getParamMovi().getPmoDtRicorPrec().getPmoDtRicorPrec(), 8));
                            //          SET WK-PARAM-MOVI-OK
                            //           TO TRUE
                        }
                        //       ELSE
                        //          INITIALIZE WK-DT-RICOR-PREC
                        //          MOVE DPOL-DT-DECOR
                        //            TO WK-DT-RICOR-PREC
                        //          SET WK-PARAM-MOVI-OK
                        //           TO TRUE
                    }
                    // COB_CODE: IF  PMO-DT-RICOR-SUCC-NULL NOT EQUAL HIGH-VALUE
                    //           AND PMO-DT-RICOR-SUCC-NULL NOT EQUAL LOW-VALUE
                    //           AND PMO-DT-RICOR-SUCC-NULL NOT EQUAL SPACES
                    //            END-IF
                    //           END-IF
                    if (!Characters.EQ_HIGH.test(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNullFormatted()) && !Characters.EQ_LOW.test(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNullFormatted()) && !Characters.EQ_SPACE.test(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSuccNull())) {
                        // COB_CODE: IF PMO-DT-RICOR-SUCC > WK-DT-RICOR-SUCC
                        //                TO WK-DT-RICOR-SUCC
                        //           END-IF
                        if (ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSucc() > ws.getWkDtRicorSucc()) {
                            // COB_CODE: MOVE PMO-DT-RICOR-SUCC
                            //             TO WK-DT-RICOR-SUCC
                            ws.setWkDtRicorSucc(TruncAbs.toInt(ws.getParamMovi().getPmoDtRicorSucc().getPmoDtRicorSucc(), 8));
                        }
                    }
                    // COB_CODE: SET IDSV0003-FETCH-NEXT      TO TRUE
                    idsv0003.getOperazione().setFetchNext();
                }
                else if (idsv0003.getSqlcode().isNotFound()) {
                    // COB_CODE: IF IDSV0003-NOT-FOUND
                    //            END-IF
                    //           ELSE
                    //            END-IF
                    //           END-IF
                    // COB_CODE: IF IDSV0003-FETCH-FIRST
                    //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    //           ELSE
                    //            END-IF
                    //           END-IF
                    if (idsv0003.getOperazione().isFetchFirst()) {
                        // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                        idsv0003.getReturnCode().setFieldNotValued();
                    }
                    else {
                        // COB_CODE: SET WK-PARAM-MOVI-OK TO TRUE
                        ws.getWkParamMovi().setOk();
                        // COB_CODE: IF WK-DT-RICOR-PREC = ZERO
                        //               TO WK-DT-RICOR-PREC
                        //           END-IF
                        if (Characters.EQ_ZERO.test(ws.getWkDtRicorPrecFormatted())) {
                            // COB_CODE: MOVE DPOL-DT-DECOR
                            //             TO WK-DT-RICOR-PREC
                            ws.setWkDtRicorPrec(TruncAbs.toInt(ws.getLccvpol1().getDati().getWpolDtDecor(), 8));
                        }
                    }
                }
                else if (idsv0003.getSqlcode().getSqlcode() == -305) {
                    // COB_CODE: IF IDSV0003-SQLCODE = -305
                    //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    //           ELSE
                    //              END-STRING
                    //           END-IF
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                    // COB_CODE: MOVE WK-CALL-PGM          TO IDSV0003-COD-SERVIZIO-BE
                    idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                    // COB_CODE: STRING 'CHIAMATA LDBS5060 ;'
                    //                  IDSV0003-RETURN-CODE ';'
                    //                  IDSV0003-SQLCODE
                    //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                    //           END-STRING
                    concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS5060 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                    idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                }
            }
            else {
                // COB_CODE: MOVE WK-CALL-PGM             TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CHIAMATA LDBS5060 ;'
                //                  IDSV0003-RETURN-CODE ';'
                //                  IDSV0003-SQLCODE
                //           DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LDBS5060 ;", idsv0003.getReturnCode().getReturnCodeFormatted(), ";", idsv0003.getSqlcode().getSqlcodeAsString());
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: IF IDSV0003-NOT-FOUND
                //           OR IDSV0003-SQLCODE = -305
                //              SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                //           ELSE
                //              SET IDSV0003-INVALID-OPER            TO TRUE
                //           END-IF
                if (idsv0003.getSqlcode().isNotFound() || idsv0003.getSqlcode().getSqlcode() == -305) {
                    // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED        TO TRUE
                    idsv0003.getReturnCode().setFieldNotValued();
                }
                else {
                    // COB_CODE: SET IDSV0003-INVALID-OPER            TO TRUE
                    idsv0003.getReturnCode().setInvalidOper();
                }
            }
        }
    }

    /**Original name: S1300-CALCOLO-RATEOCED2014<br>
	 * <pre>----------------------------------------------------------------*
	 *    CALCOLO RATEOCED2014
	 * ----------------------------------------------------------------*</pre>*/
    private void s1300CalcoloRateoced2014() {
        Lccs0010 lccs0010 = null;
        GenericParam formato = null;
        GenericParam ggDiff = null;
        GenericParam codiceRitorno = null;
        ConcatUtil concatUtil = null;
        // COB_CODE: INITIALIZE WK-APPO-DT
        ws.setWkAppoDtFormatted("00000000");
        // COB_CODE: MOVE 'A'                       TO FORMATO.
        ws.setFormatoFormatted("A");
        // COB_CODE: MOVE 20140630                  TO DATA-SUPERIORE.
        ws.getDataSuperiore().setDataSuperioreFormatted("20140630");
        // COB_CODE: MOVE WK-DT-RICOR-PREC          TO WK-APPO-DT.
        ws.setWkAppoDtFormatted(ws.getWkDtRicorPrecFormatted());
        // COB_CODE: MOVE WK-APPO-DT                TO DATA-INFERIORE.
        ws.getDataInferiore().setDataInferioreFormatted(ws.getWkAppoDtFormatted());
        // COB_CODE: MOVE 'LCCS0010'                TO WK-CALL-PGM.
        ws.setWkCallPgm("LCCS0010");
        //
        // COB_CODE: CALL WK-CALL-PGM  USING FORMATO,
        //                                   DATA-INFERIORE,
        //                                   DATA-SUPERIORE,
        //                                   GG-DIFF,
        //                                   CODICE-RITORNO
        //           ON EXCEPTION
        //                SET IDSV0003-INVALID-OPER  TO TRUE
        //           END-CALL.
        try {
            lccs0010 = Lccs0010.getInstance();
            formato = new GenericParam(MarshalByteExt.chToBuffer(ws.getFormato()));
            ggDiff = new GenericParam(MarshalByteExt.strToBuffer(ws.getGgDiffFormatted(), Lvvs3150Data.Len.GG_DIFF));
            codiceRitorno = new GenericParam(MarshalByteExt.chToBuffer(ws.getCodiceRitorno()));
            lccs0010.run(formato, ws.getDataInferiore(), ws.getDataSuperiore(), ggDiff, codiceRitorno);
            ws.setFormatoFromBuffer(formato.getByteData());
            ws.setGgDiffFromBuffer(ggDiff.getByteData());
            ws.setCodiceRitornoFromBuffer(codiceRitorno.getByteData());
        }
        catch (ProgramExecutionException __ex) {
            // COB_CODE: MOVE WK-CALL-PGM
            //             TO IDSV0003-COD-SERVIZIO-BE
            idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
            // COB_CODE: MOVE 'ERRORE CALL-LCCS0010 - CALCOLA DATA'
            //              TO IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2("ERRORE CALL-LCCS0010 - CALCOLA DATA");
            // COB_CODE: SET IDSV0003-INVALID-OPER  TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
        //
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF CODICE-RITORNO EQUAL ZERO
            //                                                    * 100
            //           ELSE
            //              SET IDSV0003-FIELD-NOT-VALUED TO TRUE
            //           END-IF
            if (Conditions.eq(ws.getCodiceRitorno(), '0')) {
                // COB_CODE: COMPUTE IVVC0213-VAL-PERC-O ROUNDED = GG-DIFF / 360
                //                                                 * 100
                ivvc0213.getTabOutput().setValPercO(Trunc.toDecimal(MathUtil.convertRoundDecimal(((new AfDecimal(((((double)(ws.getGgDiff()))) / 360), 16, 10)).multiply(100)), 9, RoundingMode.ROUND_UP, 31, 9), 14, 9));
            }
            else {
                // COB_CODE: MOVE WK-CALL-PGM         TO IDSV0003-COD-SERVIZIO-BE
                idsv0003.getCampiEsito().setCodServizioBe(ws.getWkCallPgm());
                // COB_CODE: STRING 'CHIAMATA LCCS0010 COD-RIT:'
                //                   CODICE-RITORNO ';'
                //              DELIMITED BY SIZE INTO IDSV0003-DESCRIZ-ERR-DB2
                //           END-STRING
                concatUtil = ConcatUtil.buildString(Idsv0003CampiEsito.Len.DESCRIZ_ERR_DB2, "CHIAMATA LCCS0010 COD-RIT:", String.valueOf(ws.getCodiceRitorno()), ";");
                idsv0003.getCampiEsito().setDescrizErrDb2(concatUtil.replaceInString(idsv0003.getCampiEsito().getDescrizErrDb2Formatted()));
                // COB_CODE: SET IDSV0003-FIELD-NOT-VALUED TO TRUE
                idsv0003.getReturnCode().setFieldNotValued();
            }
        }
    }

    /**Original name: S9000-OPERAZIONI-FINALI<br>
	 * <pre>----------------------------------------------------------------*
	 *    OPERAZIONI FINALI
	 * ----------------------------------------------------------------*</pre>*/
    private void s9000OperazioniFinali() {
        // COB_CODE: MOVE SPACES                     TO IVVC0213-VAL-STR-O.
        ivvc0213.getTabOutput().setValStrO("");
        // COB_CODE: MOVE 0                          TO IVVC0213-VAL-IMP-O.
        ivvc0213.getTabOutput().setValImpO(Trunc.toDecimal(0, 18, 7));
        //
        // COB_CODE: GOBACK.
        throw new ReturnException();
    }

    public void initIxIndici() {
        ws.setIxDclgen(((short)0));
    }

    public void initTabOutput() {
        ivvc0213.getTabOutput().setCodVariabileO("");
        ivvc0213.getTabOutput().setTpDatoO(Types.SPACE_CHAR);
        ivvc0213.getTabOutput().setValImpO(new AfDecimal(0, 18, 7));
        ivvc0213.getTabOutput().setValPercO(new AfDecimal(0, 14, 9));
        ivvc0213.getTabOutput().setValStrO("");
    }

    public void initAreaIoPol() {
        ws.setDpolElePoliMax(((short)0));
        ws.getLccvpol1().getStatus().setStatus(Types.SPACE_CHAR);
        ws.getLccvpol1().setIdPtf(0);
        ws.getLccvpol1().getDati().setWpolIdPoli(0);
        ws.getLccvpol1().getDati().setWpolIdMoviCrz(0);
        ws.getLccvpol1().getDati().getWpolIdMoviChiu().setWpolIdMoviChiu(0);
        ws.getLccvpol1().getDati().setWpolIbOgg("");
        ws.getLccvpol1().getDati().setWpolIbProp("");
        ws.getLccvpol1().getDati().getWpolDtProp().setWpolDtProp(0);
        ws.getLccvpol1().getDati().setWpolDtIniEff(0);
        ws.getLccvpol1().getDati().setWpolDtEndEff(0);
        ws.getLccvpol1().getDati().setWpolCodCompAnia(0);
        ws.getLccvpol1().getDati().setWpolDtDecor(0);
        ws.getLccvpol1().getDati().setWpolDtEmis(0);
        ws.getLccvpol1().getDati().setWpolTpPoli("");
        ws.getLccvpol1().getDati().getWpolDurAa().setWpolDurAa(0);
        ws.getLccvpol1().getDati().getWpolDurMm().setWpolDurMm(0);
        ws.getLccvpol1().getDati().getWpolDtScad().setWpolDtScad(0);
        ws.getLccvpol1().getDati().setWpolCodProd("");
        ws.getLccvpol1().getDati().setWpolDtIniVldtProd(0);
        ws.getLccvpol1().getDati().setWpolCodConv("");
        ws.getLccvpol1().getDati().setWpolCodRamo("");
        ws.getLccvpol1().getDati().getWpolDtIniVldtConv().setWpolDtIniVldtConv(0);
        ws.getLccvpol1().getDati().getWpolDtApplzConv().setWpolDtApplzConv(0);
        ws.getLccvpol1().getDati().setWpolTpFrmAssva("");
        ws.getLccvpol1().getDati().setWpolTpRgmFisc("");
        ws.getLccvpol1().getDati().setWpolFlEstas(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComun(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlRshComunCond(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpLivGenzTit("");
        ws.getLccvpol1().getDati().setWpolFlCopFinanz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpApplzDir("");
        ws.getLccvpol1().getDati().getWpolSpeMed().setWpolSpeMed(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirEmis().setWpolDirEmis(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDir1oVers().setWpolDir1oVers(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().getWpolDirVersAgg().setWpolDirVersAgg(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolCodDvs("");
        ws.getLccvpol1().getDati().setWpolFlFntAz(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntAder(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntTfr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlFntVolo(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolTpOpzAScad("");
        ws.getLccvpol1().getDati().getWpolAaDiffProrDflt().setWpolAaDiffProrDflt(0);
        ws.getLccvpol1().getDati().setWpolFlVerProd("");
        ws.getLccvpol1().getDati().getWpolDurGg().setWpolDurGg(0);
        ws.getLccvpol1().getDati().getWpolDirQuiet().setWpolDirQuiet(new AfDecimal(0, 15, 3));
        ws.getLccvpol1().getDati().setWpolTpPtfEstno("");
        ws.getLccvpol1().getDati().setWpolFlCumPreCntr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlAmmbMovi(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolConvGeco("");
        ws.getLccvpol1().getDati().setWpolDsRiga(0);
        ws.getLccvpol1().getDati().setWpolDsOperSql(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolDsVer(0);
        ws.getLccvpol1().getDati().setWpolDsTsIniCptz(0);
        ws.getLccvpol1().getDati().setWpolDsTsEndCptz(0);
        ws.getLccvpol1().getDati().setWpolDsUtente("");
        ws.getLccvpol1().getDati().setWpolDsStatoElab(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlScudoFisc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTrasfe(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlTfrStrc(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().getWpolDtPresc().setWpolDtPresc(0);
        ws.getLccvpol1().getDati().setWpolCodConvAgg("");
        ws.getLccvpol1().getDati().setWpolSubcatProd("");
        ws.getLccvpol1().getDati().setWpolFlQuestAdegzAss(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolCodTpa("");
        ws.getLccvpol1().getDati().getWpolIdAccComm().setWpolIdAccComm(0);
        ws.getLccvpol1().getDati().setWpolFlPoliCpiPr(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlPoliBundling(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIndPoliPrinColl(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolFlVndBundle(Types.SPACE_CHAR);
        ws.getLccvpol1().getDati().setWpolIbBs("");
        ws.getLccvpol1().getDati().setWpolFlPoliIfp(Types.SPACE_CHAR);
    }

    public void initParamMovi() {
        ws.getParamMovi().setPmoIdParamMovi(0);
        ws.getParamMovi().setPmoIdOgg(0);
        ws.getParamMovi().setPmoTpOgg("");
        ws.getParamMovi().setPmoIdMoviCrz(0);
        ws.getParamMovi().getPmoIdMoviChiu().setPmoIdMoviChiu(0);
        ws.getParamMovi().setPmoDtIniEff(0);
        ws.getParamMovi().setPmoDtEndEff(0);
        ws.getParamMovi().setPmoCodCompAnia(0);
        ws.getParamMovi().getPmoTpMovi().setPmoTpMovi(0);
        ws.getParamMovi().getPmoFrqMovi().setPmoFrqMovi(0);
        ws.getParamMovi().getPmoDurAa().setPmoDurAa(0);
        ws.getParamMovi().getPmoDurMm().setPmoDurMm(0);
        ws.getParamMovi().getPmoDurGg().setPmoDurGg(0);
        ws.getParamMovi().getPmoDtRicorPrec().setPmoDtRicorPrec(0);
        ws.getParamMovi().getPmoDtRicorSucc().setPmoDtRicorSucc(0);
        ws.getParamMovi().getPmoPcIntrFraz().setPmoPcIntrFraz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpBnsDaScoTot().setPmoImpBnsDaScoTot(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpBnsDaSco().setPmoImpBnsDaSco(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcAnticBns().setPmoPcAnticBns(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoTpRinnColl("");
        ws.getParamMovi().setPmoTpRivalPre("");
        ws.getParamMovi().setPmoTpRivalPrstz("");
        ws.getParamMovi().setPmoFlEvidRival(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoUltPcPerd().setPmoUltPcPerd(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoTotAaGiaPror().setPmoTotAaGiaPror(0);
        ws.getParamMovi().setPmoTpOpz("");
        ws.getParamMovi().getPmoAaRenCer().setPmoAaRenCer(0);
        ws.getParamMovi().getPmoPcRevrsb().setPmoPcRevrsb(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoImpRiscParzPrgt().setPmoImpRiscParzPrgt(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoImpLrdDiRat().setPmoImpLrdDiRat(new AfDecimal(0, 15, 3));
        ws.getParamMovi().setPmoIbOgg("");
        ws.getParamMovi().getPmoCosOner().setPmoCosOner(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoSpePc().setPmoSpePc(new AfDecimal(0, 6, 3));
        ws.getParamMovi().setPmoFlAttivGar(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCambioVerProd(Types.SPACE_CHAR);
        ws.getParamMovi().getPmoMmDiff().setPmoMmDiff(((short)0));
        ws.getParamMovi().getPmoImpRatManfee().setPmoImpRatManfee(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoDtUltErogManfee().setPmoDtUltErogManfee(0);
        ws.getParamMovi().setPmoTpOggRival("");
        ws.getParamMovi().getPmoSomAsstaGarac().setPmoSomAsstaGarac(new AfDecimal(0, 15, 3));
        ws.getParamMovi().getPmoPcApplzOpz().setPmoPcApplzOpz(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoIdAdes().setPmoIdAdes(0);
        ws.getParamMovi().setPmoIdPoli(0);
        ws.getParamMovi().setPmoTpFrmAssva("");
        ws.getParamMovi().setPmoDsRiga(0);
        ws.getParamMovi().setPmoDsOperSql(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoDsVer(0);
        ws.getParamMovi().setPmoDsTsIniCptz(0);
        ws.getParamMovi().setPmoDsTsEndCptz(0);
        ws.getParamMovi().setPmoDsUtente("");
        ws.getParamMovi().setPmoDsStatoElab(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoTpEstrCnt("");
        ws.getParamMovi().setPmoCodRamo("");
        ws.getParamMovi().setPmoGenDaSin(Types.SPACE_CHAR);
        ws.getParamMovi().setPmoCodTari("");
        ws.getParamMovi().getPmoNumRatPagPre().setPmoNumRatPagPre(0);
        ws.getParamMovi().getPmoPcServVal().setPmoPcServVal(new AfDecimal(0, 6, 3));
        ws.getParamMovi().getPmoEtaAaSoglBnficr().setPmoEtaAaSoglBnficr(((short)0));
    }

    public void initLdbv5061() {
        ws.getLdbv5061().setIdPoli(0);
        ws.getLdbv5061().setTpMovi01(0);
        ws.getLdbv5061().setTpMovi02(0);
        ws.getLdbv5061().setTpMovi03(0);
        ws.getLdbv5061().setTpMovi04(0);
        ws.getLdbv5061().setTpMovi05(0);
        ws.getLdbv5061().setTpMovi06(0);
        ws.getLdbv5061().setTpMovi07(0);
        ws.getLdbv5061().setTpMovi08(0);
        ws.getLdbv5061().setTpMovi09(0);
        ws.getLdbv5061().setTpMovi10(0);
    }
}
