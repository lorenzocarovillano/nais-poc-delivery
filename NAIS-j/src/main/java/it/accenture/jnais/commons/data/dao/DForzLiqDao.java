package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDForzLiq;

/**
 * Data Access Object(DAO) for table [D_FORZ_LIQ]
 * 
 */
public class DForzLiqDao extends BaseSqlDao<IDForzLiq> {

    private Cursor cIdUpdEffDfl;
    private final IRowMapper<IDForzLiq> selectByDflDsRigaRm = buildNamedRowMapper(IDForzLiq.class, "idDForzLiq", "idLiq", "idMoviCrz", "idMoviChiuObj", "codCompAnia", "dtIniEffDb", "dtEndEffDb", "impLrdCalcObj", "impLrdDfzObj", "impLrdEfflqObj", "impNetCalcObj", "impNetDfzObj", "impNetEfflqObj", "impstPrvrCalcObj", "impstPrvrDfzObj", "impstPrvrEfflqObj", "impstVisCalcObj", "impstVisDfzObj", "impstVisEfflqObj", "ritAccCalcObj", "ritAccDfzObj", "ritAccEfflqObj", "ritIrpefCalcObj", "ritIrpefDfzObj", "ritIrpefEfflqObj", "impstSostCalcObj", "impstSostDfzObj", "impstSostEfflqObj", "taxSepCalcObj", "taxSepDfzObj", "taxSepEfflqObj", "intrPrestCalcObj", "intrPrestDfzObj", "intrPrestEfflqObj", "accpreSostCalcObj", "accpreSostDfzObj", "accpreSostEfflqObj", "accpreVisCalcObj", "accpreVisDfzObj", "accpreVisEfflqObj", "accpreAccCalcObj", "accpreAccDfzObj", "accpreAccEfflqObj", "resPrstzCalcObj", "resPrstzDfzObj", "resPrstzEfflqObj", "resPreAttCalcObj", "resPreAttDfzObj", "resPreAttEfflqObj", "impExcontrEffObj", "impbVisCalcObj", "impbVisEfflqObj", "impbVisDfzObj", "impbRitAccCalcObj", "impbRitAccEfflqObj", "impbRitAccDfzObj", "impbTfrCalcObj", "impbTfrEfflqObj", "impbTfrDfzObj", "impbIsCalcObj", "impbIsEfflqObj", "impbIsDfzObj", "impbTaxSepCalcObj", "impbTaxSepEfflqObj", "impbTaxSepDfzObj", "iintPrestCalcObj", "iintPrestEfflqObj", "iintPrestDfzObj", "montEnd2000CalcObj", "montEnd2000EfflqObj", "montEnd2000DfzObj", "montEnd2006CalcObj", "montEnd2006EfflqObj", "montEnd2006DfzObj", "montDal2007CalcObj", "montDal2007EfflqObj", "montDal2007DfzObj", "iimpstPrvrCalcObj", "iimpstPrvrEfflqObj", "iimpstPrvrDfzObj", "iimpst252CalcObj", "iimpst252EfflqObj", "iimpst252DfzObj", "impst252CalcObj", "impst252EfflqObj", "ritTfrCalcObj", "ritTfrEfflqObj", "ritTfrDfzObj", "cnbtInpstfmCalcObj", "cnbtInpstfmEfflqObj", "cnbtInpstfmDfzObj", "icnbInpstfmCalcObj", "icnbInpstfmEfflqObj", "icnbInpstfmDfzObj", "cndeEnd2000CalcObj", "cndeEnd2000EfflqObj", "cndeEnd2000DfzObj", "cndeEnd2006CalcObj", "cndeEnd2006EfflqObj", "cndeEnd2006DfzObj", "cndeDal2007CalcObj", "cndeDal2007EfflqObj", "cndeDal2007DfzObj", "aaCnbzEnd2000EfObj", "aaCnbzEnd2006EfObj", "aaCnbzDal2007EfObj", "mmCnbzEnd2000EfObj", "mmCnbzEnd2006EfObj", "mmCnbzDal2007EfObj", "impstDaRimbEffObj", "alqTaxSepCalcObj", "alqTaxSepEfflqObj", "alqTaxSepDfzObj", "alqCnbtInpstfmCObj", "alqCnbtInpstfmEObj", "alqCnbtInpstfmDObj", "dflDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "impbVis1382011cObj", "impbVis1382011dObj", "impbVis1382011lObj", "impstVis1382011cObj", "impstVis1382011dObj", "impstVis1382011lObj", "impbIs1382011cObj", "impbIs1382011dObj", "impbIs1382011lObj", "is1382011cObj", "is1382011dObj", "is1382011lObj", "impIntrRitPagCObj", "impIntrRitPagDObj", "impIntrRitPagLObj", "impbBolloDettCObj", "impbBolloDettDObj", "impbBolloDettLObj", "impstBolloDettCObj", "impstBolloDettDObj", "impstBolloDettLObj", "impstBolloTotVcObj", "impstBolloTotVdObj", "impstBolloTotVlObj", "impbVis662014cObj", "impbVis662014dObj", "impbVis662014lObj", "impstVis662014cObj", "impstVis662014dObj", "impstVis662014lObj", "impbIs662014cObj", "impbIs662014dObj", "impbIs662014lObj", "is662014cObj", "is662014dObj", "is662014lObj");

    public DForzLiqDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDForzLiq> getToClass() {
        return IDForzLiq.class;
    }

    public IDForzLiq selectByDflDsRiga(long dflDsRiga, IDForzLiq iDForzLiq) {
        return buildQuery("selectByDflDsRiga").bind("dflDsRiga", dflDsRiga).rowMapper(selectByDflDsRigaRm).singleResult(iDForzLiq);
    }

    public DbAccessStatus insertRec(IDForzLiq iDForzLiq) {
        return buildQuery("insertRec").bind(iDForzLiq).executeInsert();
    }

    public DbAccessStatus updateRec(IDForzLiq iDForzLiq) {
        return buildQuery("updateRec").bind(iDForzLiq).executeUpdate();
    }

    public DbAccessStatus deleteByDflDsRiga(long dflDsRiga) {
        return buildQuery("deleteByDflDsRiga").bind("dflDsRiga", dflDsRiga).executeDelete();
    }

    public IDForzLiq selectRec(int dflIdDForzLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDForzLiq iDForzLiq) {
        return buildQuery("selectRec").bind("dflIdDForzLiq", dflIdDForzLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDflDsRigaRm).singleResult(iDForzLiq);
    }

    public DbAccessStatus updateRec1(IDForzLiq iDForzLiq) {
        return buildQuery("updateRec1").bind(iDForzLiq).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffDfl(int dflIdDForzLiq, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffDfl = buildQuery("openCIdUpdEffDfl").bind("dflIdDForzLiq", dflIdDForzLiq).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffDfl() {
        return closeCursor(cIdUpdEffDfl);
    }

    public IDForzLiq fetchCIdUpdEffDfl(IDForzLiq iDForzLiq) {
        return fetch(cIdUpdEffDfl, iDForzLiq, selectByDflDsRigaRm);
    }

    public IDForzLiq selectRec1(int dflIdDForzLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDForzLiq iDForzLiq) {
        return buildQuery("selectRec1").bind("dflIdDForzLiq", dflIdDForzLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDflDsRigaRm).singleResult(iDForzLiq);
    }
}
