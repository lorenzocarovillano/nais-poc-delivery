package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IGarStatOggBus;

/**
 * Data Access Object(DAO) for tables [GAR, STAT_OGG_BUS]
 * 
 */
public class GarStatOggBusDao extends BaseSqlDao<IGarStatOggBus> {

    private Cursor cEffGrz;
    private Cursor cCpzGrz;
    private Cursor cEffGrzNs;
    private Cursor cCpzGrzNs;
    private Cursor cEffGrzNc;
    private Cursor cCpzGrzNc;
    private Cursor cEffGrzNsNc;
    private Cursor cCpzGrzNsNc;
    private Cursor cEff6;
    private Cursor cCpz6;
    private Cursor cEff18;
    private Cursor cCpz18;
    private Cursor cEff19;
    private Cursor cCpz19;
    private Cursor cEff39;
    private Cursor cCpz39;
    private final IRowMapper<IGarStatOggBus> selectRecRm = buildNamedRowMapper(IGarStatOggBus.class, "idGar", "idAdesObj", "grzIdPoli", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "ibOggObj", "dtDecorDbObj", "dtScadDbObj", "codSezObj", "grzCodTari", "ramoBilaObj", "dtIniValTarDbObj", "id1oAsstoObj", "id2oAsstoObj", "id3oAsstoObj", "tpGarObj", "tpRshObj", "tpInvstObj", "modPagGarcolObj", "tpPerPreObj", "etaAa1oAsstoObj", "etaMm1oAsstoObj", "etaAa2oAsstoObj", "etaMm2oAsstoObj", "etaAa3oAsstoObj", "etaMm3oAsstoObj", "tpEmisPurObj", "etaAScadObj", "tpCalcPrePrstzObj", "tpPreObj", "tpDurObj", "durAaObj", "durMmObj", "durGgObj", "numAaPagPreObj", "aaPagPreUniObj", "mmPagPreUniObj", "frazIniErogRenObj", "mm1oRatObj", "pc1oRatObj", "tpPrstzAsstaObj", "dtEndCarzDbObj", "pcRipPreObj", "codFndObj", "aaRenCerObj", "pcRevrsbObj", "tpPcRipObj", "pcOpzObj", "tpIasObj", "tpStabObj", "tpAdegPreObj", "dtVarzTpIasDbObj", "frazDecrCptObj", "codTratRiassObj", "tpDtEmisRiassObj", "tpCessRiassObj", "dsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "aaStabObj", "tsStabLimitataObj", "dtPrescDbObj", "rshInvstObj", "tpRamoBila");
    private final IRowMapper<IGarStatOggBus> selectRec10Rm = buildNamedRowMapper(IGarStatOggBus.class, "idGar", "idAdesObj", "grzIdPoli", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "ibOggObj", "dtDecorDbObj", "dtScadDbObj", "codSezObj", "grzCodTari", "ramoBilaObj", "dtIniValTarDbObj", "id1oAsstoObj", "id2oAsstoObj", "id3oAsstoObj", "tpGarObj", "tpRshObj", "tpInvstObj", "modPagGarcolObj", "tpPerPreObj", "etaAa1oAsstoObj", "etaMm1oAsstoObj", "etaAa2oAsstoObj", "etaMm2oAsstoObj", "etaAa3oAsstoObj", "etaMm3oAsstoObj", "tpEmisPurObj", "etaAScadObj", "tpCalcPrePrstzObj", "tpPreObj", "tpDurObj", "durAaObj", "durMmObj", "durGgObj", "numAaPagPreObj", "aaPagPreUniObj", "mmPagPreUniObj", "frazIniErogRenObj", "mm1oRatObj", "pc1oRatObj", "tpPrstzAsstaObj", "dtEndCarzDbObj", "pcRipPreObj", "codFndObj", "aaRenCerObj", "pcRevrsbObj", "tpPcRipObj", "pcOpzObj", "tpIasObj", "tpStabObj", "tpAdegPreObj", "dtVarzTpIasDbObj", "frazDecrCptObj", "codTratRiassObj", "tpDtEmisRiassObj", "tpCessRiassObj", "dsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "aaStabObj", "tsStabLimitataObj", "dtPrescDbObj", "rshInvstObj", "tpRamoBila", "stbIdStatOggBus", "stbIdOgg", "stbTpOgg", "stbIdMoviCrz", "stbIdMoviChiuObj", "stbDtIniEffDb", "stbDtEndEffDb", "stbCodCompAnia", "stbTpStatBus", "stbTpCaus", "stbDsRiga", "stbDsOperSql", "stbDsVer", "stbDsTsIniCptz", "stbDsTsEndCptz", "stbDsUtente", "stbDsStatoElab");

    public GarStatOggBusDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IGarStatOggBus> getToClass() {
        return IGarStatOggBus.class;
    }

    public IGarStatOggBus selectRec(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCEffGrz(IGarStatOggBus iGarStatOggBus) {
        cEffGrz = buildQuery("openCEffGrz").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffGrz() {
        return closeCursor(cEffGrz);
    }

    public IGarStatOggBus fetchCEffGrz(IGarStatOggBus iGarStatOggBus) {
        return fetch(cEffGrz, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec1(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec1").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCCpzGrz(IGarStatOggBus iGarStatOggBus) {
        cCpzGrz = buildQuery("openCCpzGrz").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzGrz() {
        return closeCursor(cCpzGrz);
    }

    public IGarStatOggBus fetchCCpzGrz(IGarStatOggBus iGarStatOggBus) {
        return fetch(cCpzGrz, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec2(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec2").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCEffGrzNs(IGarStatOggBus iGarStatOggBus) {
        cEffGrzNs = buildQuery("openCEffGrzNs").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffGrzNs() {
        return closeCursor(cEffGrzNs);
    }

    public IGarStatOggBus fetchCEffGrzNs(IGarStatOggBus iGarStatOggBus) {
        return fetch(cEffGrzNs, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec3(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec3").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCCpzGrzNs(IGarStatOggBus iGarStatOggBus) {
        cCpzGrzNs = buildQuery("openCCpzGrzNs").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzGrzNs() {
        return closeCursor(cCpzGrzNs);
    }

    public IGarStatOggBus fetchCCpzGrzNs(IGarStatOggBus iGarStatOggBus) {
        return fetch(cCpzGrzNs, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec4(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec4").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCEffGrzNc(IGarStatOggBus iGarStatOggBus) {
        cEffGrzNc = buildQuery("openCEffGrzNc").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffGrzNc() {
        return closeCursor(cEffGrzNc);
    }

    public IGarStatOggBus fetchCEffGrzNc(IGarStatOggBus iGarStatOggBus) {
        return fetch(cEffGrzNc, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec5(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec5").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCCpzGrzNc(IGarStatOggBus iGarStatOggBus) {
        cCpzGrzNc = buildQuery("openCCpzGrzNc").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzGrzNc() {
        return closeCursor(cCpzGrzNc);
    }

    public IGarStatOggBus fetchCCpzGrzNc(IGarStatOggBus iGarStatOggBus) {
        return fetch(cCpzGrzNc, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec6(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec6").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCEffGrzNsNc(IGarStatOggBus iGarStatOggBus) {
        cEffGrzNsNc = buildQuery("openCEffGrzNsNc").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffGrzNsNc() {
        return closeCursor(cEffGrzNsNc);
    }

    public IGarStatOggBus fetchCEffGrzNsNc(IGarStatOggBus iGarStatOggBus) {
        return fetch(cEffGrzNsNc, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec7(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec7").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCCpzGrzNsNc(IGarStatOggBus iGarStatOggBus) {
        cCpzGrzNsNc = buildQuery("openCCpzGrzNsNc").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzGrzNsNc() {
        return closeCursor(cCpzGrzNsNc);
    }

    public IGarStatOggBus fetchCCpzGrzNsNc(IGarStatOggBus iGarStatOggBus) {
        return fetch(cCpzGrzNsNc, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec8(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec8").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCEff6(IGarStatOggBus iGarStatOggBus) {
        cEff6 = buildQuery("openCEff6").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff6() {
        return closeCursor(cEff6);
    }

    public IGarStatOggBus fetchCEff6(IGarStatOggBus iGarStatOggBus) {
        return fetch(cEff6, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec9(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec9").bind(iGarStatOggBus).rowMapper(selectRecRm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCCpz6(IGarStatOggBus iGarStatOggBus) {
        cCpz6 = buildQuery("openCCpz6").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz6() {
        return closeCursor(cCpz6);
    }

    public IGarStatOggBus fetchCCpz6(IGarStatOggBus iGarStatOggBus) {
        return fetch(cCpz6, iGarStatOggBus, selectRecRm);
    }

    public IGarStatOggBus selectRec10(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec10").bind(iGarStatOggBus).rowMapper(selectRec10Rm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCEff18(IGarStatOggBus iGarStatOggBus) {
        cEff18 = buildQuery("openCEff18").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff18() {
        return closeCursor(cEff18);
    }

    public IGarStatOggBus fetchCEff18(IGarStatOggBus iGarStatOggBus) {
        return fetch(cEff18, iGarStatOggBus, selectRec10Rm);
    }

    public IGarStatOggBus selectRec11(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec11").bind(iGarStatOggBus).rowMapper(selectRec10Rm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCCpz18(IGarStatOggBus iGarStatOggBus) {
        cCpz18 = buildQuery("openCCpz18").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz18() {
        return closeCursor(cCpz18);
    }

    public IGarStatOggBus fetchCCpz18(IGarStatOggBus iGarStatOggBus) {
        return fetch(cCpz18, iGarStatOggBus, selectRec10Rm);
    }

    public IGarStatOggBus selectRec12(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec12").bind(iGarStatOggBus).rowMapper(selectRec10Rm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCEff19(IGarStatOggBus iGarStatOggBus) {
        cEff19 = buildQuery("openCEff19").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff19() {
        return closeCursor(cEff19);
    }

    public IGarStatOggBus fetchCEff19(IGarStatOggBus iGarStatOggBus) {
        return fetch(cEff19, iGarStatOggBus, selectRec10Rm);
    }

    public IGarStatOggBus selectRec13(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec13").bind(iGarStatOggBus).rowMapper(selectRec10Rm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCCpz19(IGarStatOggBus iGarStatOggBus) {
        cCpz19 = buildQuery("openCCpz19").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz19() {
        return closeCursor(cCpz19);
    }

    public IGarStatOggBus fetchCCpz19(IGarStatOggBus iGarStatOggBus) {
        return fetch(cCpz19, iGarStatOggBus, selectRec10Rm);
    }

    public IGarStatOggBus selectRec14(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec14").bind(iGarStatOggBus).rowMapper(selectRec10Rm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCEff39(IGarStatOggBus iGarStatOggBus) {
        cEff39 = buildQuery("openCEff39").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff39() {
        return closeCursor(cEff39);
    }

    public IGarStatOggBus fetchCEff39(IGarStatOggBus iGarStatOggBus) {
        return fetch(cEff39, iGarStatOggBus, selectRec10Rm);
    }

    public IGarStatOggBus selectRec15(IGarStatOggBus iGarStatOggBus) {
        return buildQuery("selectRec15").bind(iGarStatOggBus).rowMapper(selectRec10Rm).singleResult(iGarStatOggBus);
    }

    public DbAccessStatus openCCpz39(IGarStatOggBus iGarStatOggBus) {
        cCpz39 = buildQuery("openCCpz39").bind(iGarStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz39() {
        return closeCursor(cCpz39);
    }

    public IGarStatOggBus fetchCCpz39(IGarStatOggBus iGarStatOggBus) {
        return fetch(cCpz39, iGarStatOggBus, selectRec10Rm);
    }
}
