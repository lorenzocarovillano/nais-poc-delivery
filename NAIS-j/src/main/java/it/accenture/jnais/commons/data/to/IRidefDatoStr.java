package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [RIDEF_DATO_STR]
 * 
 */
public interface IRidefDatoStr extends BaseSqlTo {

};
