package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [GAR_LIQ]
 * 
 */
public interface IGarLiq extends BaseSqlTo {

    /**
     * Host Variable GRL-ID-GAR-LIQ
     * 
     */
    int getIdGarLiq();

    void setIdGarLiq(int idGarLiq);

    /**
     * Host Variable GRL-ID-LIQ
     * 
     */
    int getIdLiq();

    void setIdLiq(int idLiq);

    /**
     * Host Variable GRL-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Host Variable GRL-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable GRL-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for GRL-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable GRL-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable GRL-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable GRL-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable GRL-DT-SIN-1O-ASSTO-DB
     * 
     */
    String getDtSin1oAsstoDb();

    void setDtSin1oAsstoDb(String dtSin1oAsstoDb);

    /**
     * Nullable property for GRL-DT-SIN-1O-ASSTO-DB
     * 
     */
    String getDtSin1oAsstoDbObj();

    void setDtSin1oAsstoDbObj(String dtSin1oAsstoDbObj);

    /**
     * Host Variable GRL-CAU-SIN-1O-ASSTO
     * 
     */
    String getCauSin1oAssto();

    void setCauSin1oAssto(String cauSin1oAssto);

    /**
     * Nullable property for GRL-CAU-SIN-1O-ASSTO
     * 
     */
    String getCauSin1oAsstoObj();

    void setCauSin1oAsstoObj(String cauSin1oAsstoObj);

    /**
     * Host Variable GRL-TP-SIN-1O-ASSTO
     * 
     */
    String getTpSin1oAssto();

    void setTpSin1oAssto(String tpSin1oAssto);

    /**
     * Nullable property for GRL-TP-SIN-1O-ASSTO
     * 
     */
    String getTpSin1oAsstoObj();

    void setTpSin1oAsstoObj(String tpSin1oAsstoObj);

    /**
     * Host Variable GRL-DT-SIN-2O-ASSTO-DB
     * 
     */
    String getDtSin2oAsstoDb();

    void setDtSin2oAsstoDb(String dtSin2oAsstoDb);

    /**
     * Nullable property for GRL-DT-SIN-2O-ASSTO-DB
     * 
     */
    String getDtSin2oAsstoDbObj();

    void setDtSin2oAsstoDbObj(String dtSin2oAsstoDbObj);

    /**
     * Host Variable GRL-CAU-SIN-2O-ASSTO
     * 
     */
    String getCauSin2oAssto();

    void setCauSin2oAssto(String cauSin2oAssto);

    /**
     * Nullable property for GRL-CAU-SIN-2O-ASSTO
     * 
     */
    String getCauSin2oAsstoObj();

    void setCauSin2oAsstoObj(String cauSin2oAsstoObj);

    /**
     * Host Variable GRL-TP-SIN-2O-ASSTO
     * 
     */
    String getTpSin2oAssto();

    void setTpSin2oAssto(String tpSin2oAssto);

    /**
     * Nullable property for GRL-TP-SIN-2O-ASSTO
     * 
     */
    String getTpSin2oAsstoObj();

    void setTpSin2oAsstoObj(String tpSin2oAsstoObj);

    /**
     * Host Variable GRL-DT-SIN-3O-ASSTO-DB
     * 
     */
    String getDtSin3oAsstoDb();

    void setDtSin3oAsstoDb(String dtSin3oAsstoDb);

    /**
     * Nullable property for GRL-DT-SIN-3O-ASSTO-DB
     * 
     */
    String getDtSin3oAsstoDbObj();

    void setDtSin3oAsstoDbObj(String dtSin3oAsstoDbObj);

    /**
     * Host Variable GRL-CAU-SIN-3O-ASSTO
     * 
     */
    String getCauSin3oAssto();

    void setCauSin3oAssto(String cauSin3oAssto);

    /**
     * Nullable property for GRL-CAU-SIN-3O-ASSTO
     * 
     */
    String getCauSin3oAsstoObj();

    void setCauSin3oAsstoObj(String cauSin3oAsstoObj);

    /**
     * Host Variable GRL-TP-SIN-3O-ASSTO
     * 
     */
    String getTpSin3oAssto();

    void setTpSin3oAssto(String tpSin3oAssto);

    /**
     * Nullable property for GRL-TP-SIN-3O-ASSTO
     * 
     */
    String getTpSin3oAsstoObj();

    void setTpSin3oAsstoObj(String tpSin3oAsstoObj);

    /**
     * Host Variable GRL-DS-RIGA
     * 
     */
    long getGrlDsRiga();

    void setGrlDsRiga(long grlDsRiga);

    /**
     * Host Variable GRL-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable GRL-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable GRL-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable GRL-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable GRL-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable GRL-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
