package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BTC_BATCH_STATE]
 * 
 */
public interface IBtcBatchState extends BaseSqlTo {

    /**
     * Host Variable BBS-COD-BATCH-STATE
     * 
     */
    char getCodBatchState();

    void setCodBatchState(char codBatchState);

    /**
     * Host Variable BBS-DES
     * 
     */
    String getDes();

    void setDes(String des);

    /**
     * Nullable property for BBS-DES
     * 
     */
    String getDesObj();

    void setDesObj(String desObj);

    /**
     * Host Variable BBS-FLAG-TO-EXECUTE
     * 
     */
    char getFlagToExecute();

    void setFlagToExecute(char flagToExecute);

    /**
     * Nullable property for BBS-FLAG-TO-EXECUTE
     * 
     */
    Character getFlagToExecuteObj();

    void setFlagToExecuteObj(Character flagToExecuteObj);
};
