package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IRichInvstFnd;

/**
 * Data Access Object(DAO) for table [RICH_INVST_FND]
 * 
 */
public class RichInvstFndDao extends BaseSqlDao<IRichInvstFnd> {

    private Cursor cIdUpdEffRif;
    private Cursor cIdpEffRif;
    private Cursor cIdpCpzRif;
    private final IRowMapper<IRichInvstFnd> selectByRifDsRigaRm = buildNamedRowMapper(IRichInvstFnd.class, "idRichInvstFnd", "idMoviFinrio", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codFndObj", "numQuoObj", "pcObj", "impMovtoObj", "dtInvstDbObj", "codTariObj", "tpStatObj", "tpModInvstObj", "codDiv", "dtCambioVltDbObj", "tpFnd", "rifDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "dtInvstCalcDbObj", "flCalcInvtoObj", "impGapEventObj", "flSwmBp2sObj");

    public RichInvstFndDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IRichInvstFnd> getToClass() {
        return IRichInvstFnd.class;
    }

    public IRichInvstFnd selectByRifDsRiga(long rifDsRiga, IRichInvstFnd iRichInvstFnd) {
        return buildQuery("selectByRifDsRiga").bind("rifDsRiga", rifDsRiga).rowMapper(selectByRifDsRigaRm).singleResult(iRichInvstFnd);
    }

    public DbAccessStatus insertRec(IRichInvstFnd iRichInvstFnd) {
        return buildQuery("insertRec").bind(iRichInvstFnd).executeInsert();
    }

    public DbAccessStatus updateRec(IRichInvstFnd iRichInvstFnd) {
        return buildQuery("updateRec").bind(iRichInvstFnd).executeUpdate();
    }

    public DbAccessStatus deleteByRifDsRiga(long rifDsRiga) {
        return buildQuery("deleteByRifDsRiga").bind("rifDsRiga", rifDsRiga).executeDelete();
    }

    public IRichInvstFnd selectRec(int rifIdRichInvstFnd, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRichInvstFnd iRichInvstFnd) {
        return buildQuery("selectRec").bind("rifIdRichInvstFnd", rifIdRichInvstFnd).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRifDsRigaRm).singleResult(iRichInvstFnd);
    }

    public DbAccessStatus updateRec1(IRichInvstFnd iRichInvstFnd) {
        return buildQuery("updateRec1").bind(iRichInvstFnd).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffRif(int rifIdRichInvstFnd, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffRif = buildQuery("openCIdUpdEffRif").bind("rifIdRichInvstFnd", rifIdRichInvstFnd).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffRif() {
        return closeCursor(cIdUpdEffRif);
    }

    public IRichInvstFnd fetchCIdUpdEffRif(IRichInvstFnd iRichInvstFnd) {
        return fetch(cIdUpdEffRif, iRichInvstFnd, selectByRifDsRigaRm);
    }

    public IRichInvstFnd selectRec1(int rifIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRichInvstFnd iRichInvstFnd) {
        return buildQuery("selectRec1").bind("rifIdMoviFinrio", rifIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRifDsRigaRm).singleResult(iRichInvstFnd);
    }

    public DbAccessStatus openCIdpEffRif(int rifIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffRif = buildQuery("openCIdpEffRif").bind("rifIdMoviFinrio", rifIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffRif() {
        return closeCursor(cIdpEffRif);
    }

    public IRichInvstFnd fetchCIdpEffRif(IRichInvstFnd iRichInvstFnd) {
        return fetch(cIdpEffRif, iRichInvstFnd, selectByRifDsRigaRm);
    }

    public IRichInvstFnd selectRec2(int rifIdRichInvstFnd, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IRichInvstFnd iRichInvstFnd) {
        return buildQuery("selectRec2").bind("rifIdRichInvstFnd", rifIdRichInvstFnd).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByRifDsRigaRm).singleResult(iRichInvstFnd);
    }

    public IRichInvstFnd selectRec3(int rifIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IRichInvstFnd iRichInvstFnd) {
        return buildQuery("selectRec3").bind("rifIdMoviFinrio", rifIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByRifDsRigaRm).singleResult(iRichInvstFnd);
    }

    public DbAccessStatus openCIdpCpzRif(int rifIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzRif = buildQuery("openCIdpCpzRif").bind("rifIdMoviFinrio", rifIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzRif() {
        return closeCursor(cIdpCpzRif);
    }

    public IRichInvstFnd fetchCIdpCpzRif(IRichInvstFnd iRichInvstFnd) {
        return fetch(cIdpCpzRif, iRichInvstFnd, selectByRifDsRigaRm);
    }
}
