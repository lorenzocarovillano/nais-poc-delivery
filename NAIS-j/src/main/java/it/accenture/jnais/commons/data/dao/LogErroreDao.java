package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ILogErrore;

/**
 * Data Access Object(DAO) for table [LOG_ERRORE]
 * 
 */
public class LogErroreDao extends BaseSqlDao<ILogErrore> {

    private final IRowMapper<ILogErrore> selectRecRm = buildNamedRowMapper(ILogErrore.class, "idLogErrore", "progLogErrore", "idGravitaErrore", "descErroreEstesaVchar", "codMainBatch", "codServizioBe", "labelErrObj", "operTabellaObj", "nomeTabellaObj", "statusTabellaObj", "keyTabellaVcharObj", "timestampReg", "tipoOggettoObj", "ibOggettoObj");

    public LogErroreDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ILogErrore> getToClass() {
        return ILogErrore.class;
    }

    public ILogErrore selectRec(int idLogErrore, int progLogErrore, ILogErrore iLogErrore) {
        return buildQuery("selectRec").bind("idLogErrore", idLogErrore).bind("progLogErrore", progLogErrore).rowMapper(selectRecRm).singleResult(iLogErrore);
    }

    public DbAccessStatus updateRec(ILogErrore iLogErrore) {
        return buildQuery("updateRec").bind(iLogErrore).executeUpdate();
    }

    public DbAccessStatus deleteRec(int idLogErrore, int progLogErrore) {
        return buildQuery("deleteRec").bind("idLogErrore", idLogErrore).bind("progLogErrore", progLogErrore).executeDelete();
    }

    public int selectByLorIdLogErrore(int lorIdLogErrore, int dft) {
        return buildQuery("selectByLorIdLogErrore").bind("lorIdLogErrore", lorIdLogErrore).scalarResultInt(dft);
    }

    public int selectByLorIdLogErrore1(int lorIdLogErrore, int dft) {
        return buildQuery("selectByLorIdLogErrore1").bind("lorIdLogErrore", lorIdLogErrore).scalarResultInt(dft);
    }

    public DbAccessStatus insertRec(ILogErrore iLogErrore) {
        return buildQuery("insertRec").bind(iLogErrore).executeInsert();
    }
}
