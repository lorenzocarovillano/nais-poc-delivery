package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IParamComp;

/**
 * Data Access Object(DAO) for table [PARAM_COMP]
 * 
 */
public class ParamCompDao extends BaseSqlDao<IParamComp> {

    private final IRowMapper<IParamComp> selectByPcoCodCompAniaRm = buildNamedRowMapper(IParamComp.class, "pcoCodCompAnia", "codTratCirtObj", "limVltrObj", "tpRatPerfObj", "tpLivGenzTit", "arrotPreObj", "dtContDbObj", "dtUltRivalInDbObj", "dtUltQtzoInDbObj", "dtUltRiclRiassDbObj", "dtUltTabulRiassDbObj", "dtUltBollEmesDbObj", "dtUltBollStorDbObj", "dtUltBollLiqDbObj", "dtUltBollRiatDbObj", "dtUltelriscparPrDbObj", "dtUltcIsInDbObj", "dtUltRiclPreDbObj", "dtUltcMarsolDbObj", "dtUltcRbInDbObj", "pcProv1aaAcqObj", "modIntrPrestObj", "ggMaxRecProvObj", "dtUltgzTrchEInDbObj", "dtUltBollSndenDbObj", "dtUltBollSndnlqDbObj", "dtUltscElabInDbObj", "dtUltscOpzInDbObj", "dtUltcBnsricInDbObj", "dtUltcBnsfdtInDbObj", "dtUltRinnGaracDbObj", "dtUltgzCedDbObj", "dtUltElabPrlcosDbObj", "dtUltRinnCollDbObj", "flRvcPerfObj", "flRcsPoliNoperfObj", "flGestPlusvObj", "dtUltRivalClDbObj", "dtUltQtzoClDbObj", "dtUltcBnsricClDbObj", "dtUltcBnsfdtClDbObj", "dtUltcIsClDbObj", "dtUltcRbClDbObj", "dtUltgzTrchEClDbObj", "dtUltscElabClDbObj", "dtUltscOpzClDbObj", "ststXRegioneDbObj", "dtUltgzCedCollDbObj", "tpModRival", "numMmCalcMoraObj", "dtUltEcRivCollDbObj", "dtUltEcRivIndDbObj", "dtUltEcIlCollDbObj", "dtUltEcIlIndDbObj", "dtUltEcUlCollDbObj", "dtUltEcUlIndDbObj", "aaUtiObj", "calcRshComun", "flLivDebug", "dtUltBollPerfCDbObj", "dtUltBollRspInDbObj", "dtUltBollRspClDbObj", "dtUltBollEmesIDbObj", "dtUltBollStorIDbObj", "dtUltBollRiatIDbObj", "dtUltBollSdIDbObj", "dtUltBollSdnlIDbObj", "dtUltBollPerfIDbObj", "dtRiclRiriasComDbObj", "dtUltElabAt92CDbObj", "dtUltElabAt92IDbObj", "dtUltElabAt93CDbObj", "dtUltElabAt93IDbObj", "dtUltElabSpeInDbObj", "dtUltElabPrConDbObj", "dtUltBollRpClDbObj", "dtUltBollRpInDbObj", "dtUltBollPreIDbObj", "dtUltBollPreCDbObj", "dtUltcPildiMmCDbObj", "dtUltcPildiAaCDbObj", "dtUltcPildiMmIDbObj", "dtUltcPildiTrIDbObj", "dtUltcPildiAaIDbObj", "dtUltBollQuieCDbObj", "dtUltBollQuieIDbObj", "dtUltBollCotrIDbObj", "dtUltBollCotrCDbObj", "dtUltBollCoriCDbObj", "dtUltBollCoriIDbObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "tpValzzDtVltObj", "flFrazProvAcqObj", "dtUltAggErogReDbObj", "pcRmMarsolObj", "pcCSubrshMarsolObj", "codCompIsvapObj", "lmRisConIntObj", "lmCSubrshConInObj", "pcGarNoriskMarsObj", "crz1aRatIntrPrObj", "numGgArrIntrPrObj", "flVisualVinpgObj", "dtUltEstrazFugDbObj", "dtUltElabPrAutDbObj", "dtUltElabCommefDbObj", "dtUltElabLiqmefDbObj", "codFiscMefObj", "impAssSocialeObj", "modComnzInvstSwObj", "dtRiatRiassRshDbObj", "dtRiatRiassCommDbObj", "ggIntrRitPagObj", "dtUltRinnTacDbObj", "descCompVcharObj", "dtUltEcTcmIndDbObj", "dtUltEcTcmCollDbObj", "dtUltEcMrmIndDbObj", "dtUltEcMrmCollDbObj", "codCompLdapObj", "pcRidImp1382011Obj", "pcRidImp662014Obj", "soglAmlPreUniObj", "soglAmlPrePerObj", "codSoggFtzAsstoObj", "dtUltElabRedproDbObj", "dtUltElabTakePDbObj", "dtUltElabPaspasDbObj", "soglAmlPreSavRObj", "dtUltEstrDecCoDbObj", "dtUltElabCosAtDbObj", "frqCostiAttObj", "dtUltElabCosStDbObj", "frqCostiStornatiObj", "dtEstrAssMin70aDbObj", "dtEstrAssMag70aDbObj");
    private final IRowMapper<IParamComp> selectByPcoCodCompAnia1Rm = buildNamedRowMapper(IParamComp.class, "dtContDbObj", "flLivDebug");

    public ParamCompDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IParamComp> getToClass() {
        return IParamComp.class;
    }

    public IParamComp selectByPcoCodCompAnia(int pcoCodCompAnia, IParamComp iParamComp) {
        return buildQuery("selectByPcoCodCompAnia").bind("pcoCodCompAnia", pcoCodCompAnia).rowMapper(selectByPcoCodCompAniaRm).singleResult(iParamComp);
    }

    public DbAccessStatus insertRec(IParamComp iParamComp) {
        return buildQuery("insertRec").bind(iParamComp).executeInsert();
    }

    public DbAccessStatus updateRec(IParamComp iParamComp) {
        return buildQuery("updateRec").bind(iParamComp).executeUpdate();
    }

    public DbAccessStatus deleteByPcoCodCompAnia(int pcoCodCompAnia) {
        return buildQuery("deleteByPcoCodCompAnia").bind("pcoCodCompAnia", pcoCodCompAnia).executeDelete();
    }

    public IParamComp selectByPcoCodCompAnia1(int pcoCodCompAnia, IParamComp iParamComp) {
        return buildQuery("selectByPcoCodCompAnia1").bind("pcoCodCompAnia", pcoCodCompAnia).rowMapper(selectByPcoCodCompAnia1Rm).singleResult(iParamComp);
    }
}
