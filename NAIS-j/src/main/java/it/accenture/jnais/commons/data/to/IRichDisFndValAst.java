package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [RICH_DIS_FND, VAL_AST]
 * 
 */
public interface IRichDisFndValAst extends BaseSqlTo {

    /**
     * Host Variable RDF-ID-RICH-DIS-FND
     * 
     */
    int getIdRichDisFnd();

    void setIdRichDisFnd(int idRichDisFnd);

    /**
     * Host Variable RDF-ID-MOVI-FINRIO
     * 
     */
    int getIdMoviFinrio();

    void setIdMoviFinrio(int idMoviFinrio);

    /**
     * Host Variable RDF-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable RDF-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for RDF-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable RDF-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable RDF-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable RDF-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable RDF-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Nullable property for RDF-COD-FND
     * 
     */
    String getCodFndObj();

    void setCodFndObj(String codFndObj);

    /**
     * Host Variable RDF-NUM-QUO
     * 
     */
    AfDecimal getNumQuo();

    void setNumQuo(AfDecimal numQuo);

    /**
     * Nullable property for RDF-NUM-QUO
     * 
     */
    AfDecimal getNumQuoObj();

    void setNumQuoObj(AfDecimal numQuoObj);

    /**
     * Host Variable RDF-PC
     * 
     */
    AfDecimal getPc();

    void setPc(AfDecimal pc);

    /**
     * Nullable property for RDF-PC
     * 
     */
    AfDecimal getPcObj();

    void setPcObj(AfDecimal pcObj);

    /**
     * Host Variable RDF-IMP-MOVTO
     * 
     */
    AfDecimal getImpMovto();

    void setImpMovto(AfDecimal impMovto);

    /**
     * Nullable property for RDF-IMP-MOVTO
     * 
     */
    AfDecimal getImpMovtoObj();

    void setImpMovtoObj(AfDecimal impMovtoObj);

    /**
     * Host Variable RDF-DT-DIS-DB
     * 
     */
    String getDtDisDb();

    void setDtDisDb(String dtDisDb);

    /**
     * Nullable property for RDF-DT-DIS-DB
     * 
     */
    String getDtDisDbObj();

    void setDtDisDbObj(String dtDisDbObj);

    /**
     * Host Variable RDF-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Nullable property for RDF-COD-TARI
     * 
     */
    String getCodTariObj();

    void setCodTariObj(String codTariObj);

    /**
     * Host Variable RDF-TP-STAT
     * 
     */
    String getTpStat();

    void setTpStat(String tpStat);

    /**
     * Nullable property for RDF-TP-STAT
     * 
     */
    String getTpStatObj();

    void setTpStatObj(String tpStatObj);

    /**
     * Host Variable RDF-TP-MOD-DIS
     * 
     */
    String getTpModDis();

    void setTpModDis(String tpModDis);

    /**
     * Nullable property for RDF-TP-MOD-DIS
     * 
     */
    String getTpModDisObj();

    void setTpModDisObj(String tpModDisObj);

    /**
     * Host Variable RDF-COD-DIV
     * 
     */
    String getCodDiv();

    void setCodDiv(String codDiv);

    /**
     * Host Variable RDF-DT-CAMBIO-VLT-DB
     * 
     */
    String getDtCambioVltDb();

    void setDtCambioVltDb(String dtCambioVltDb);

    /**
     * Nullable property for RDF-DT-CAMBIO-VLT-DB
     * 
     */
    String getDtCambioVltDbObj();

    void setDtCambioVltDbObj(String dtCambioVltDbObj);

    /**
     * Host Variable RDF-TP-FND
     * 
     */
    char getTpFnd();

    void setTpFnd(char tpFnd);

    /**
     * Host Variable RDF-DS-RIGA
     * 
     */
    long getDsRiga();

    void setDsRiga(long dsRiga);

    /**
     * Host Variable RDF-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable RDF-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable RDF-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable RDF-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable RDF-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable RDF-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable RDF-DT-DIS-CALC-DB
     * 
     */
    String getDtDisCalcDb();

    void setDtDisCalcDb(String dtDisCalcDb);

    /**
     * Nullable property for RDF-DT-DIS-CALC-DB
     * 
     */
    String getDtDisCalcDbObj();

    void setDtDisCalcDbObj(String dtDisCalcDbObj);

    /**
     * Host Variable RDF-FL-CALC-DIS
     * 
     */
    char getFlCalcDis();

    void setFlCalcDis(char flCalcDis);

    /**
     * Nullable property for RDF-FL-CALC-DIS
     * 
     */
    Character getFlCalcDisObj();

    void setFlCalcDisObj(Character flCalcDisObj);

    /**
     * Host Variable RDF-COMMIS-GEST
     * 
     */
    AfDecimal getCommisGest();

    void setCommisGest(AfDecimal commisGest);

    /**
     * Nullable property for RDF-COMMIS-GEST
     * 
     */
    AfDecimal getCommisGestObj();

    void setCommisGestObj(AfDecimal commisGestObj);

    /**
     * Host Variable RDF-NUM-QUO-CDG-FNZ
     * 
     */
    AfDecimal getNumQuoCdgFnz();

    void setNumQuoCdgFnz(AfDecimal numQuoCdgFnz);

    /**
     * Nullable property for RDF-NUM-QUO-CDG-FNZ
     * 
     */
    AfDecimal getNumQuoCdgFnzObj();

    void setNumQuoCdgFnzObj(AfDecimal numQuoCdgFnzObj);

    /**
     * Host Variable RDF-NUM-QUO-CDGTOT-FNZ
     * 
     */
    AfDecimal getNumQuoCdgtotFnz();

    void setNumQuoCdgtotFnz(AfDecimal numQuoCdgtotFnz);

    /**
     * Nullable property for RDF-NUM-QUO-CDGTOT-FNZ
     * 
     */
    AfDecimal getNumQuoCdgtotFnzObj();

    void setNumQuoCdgtotFnzObj(AfDecimal numQuoCdgtotFnzObj);

    /**
     * Host Variable RDF-COS-RUN-ASSVA-IDC
     * 
     */
    AfDecimal getCosRunAssvaIdc();

    void setCosRunAssvaIdc(AfDecimal cosRunAssvaIdc);

    /**
     * Nullable property for RDF-COS-RUN-ASSVA-IDC
     * 
     */
    AfDecimal getCosRunAssvaIdcObj();

    void setCosRunAssvaIdcObj(AfDecimal cosRunAssvaIdcObj);

    /**
     * Host Variable RDF-FL-SWM-BP2S
     * 
     */
    char getFlSwmBp2s();

    void setFlSwmBp2s(char flSwmBp2s);

    /**
     * Nullable property for RDF-FL-SWM-BP2S
     * 
     */
    Character getFlSwmBp2sObj();

    void setFlSwmBp2sObj(Character flSwmBp2sObj);
};
