package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAmmbFunz;

/**
 * Data Access Object(DAO) for table [AMMB_FUNZ_FUNZ]
 * 
 */
public class AmmbFunzDao extends BaseSqlDao<IAmmbFunz> {

    private Cursor cNst6;
    private Cursor cNst28;
    private final IRowMapper<IAmmbFunz> selectRecRm = buildNamedRowMapper(IAmmbFunz.class, "codCompAnia", "l05TpMoviEsec", "l05TpMoviRifto", "gravFunzFunz", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "codBloccoObj", "srvzVerAnnObj", "whereConditionVcharObj", "flPoliIfpObj");

    public AmmbFunzDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAmmbFunz> getToClass() {
        return IAmmbFunz.class;
    }

    public IAmmbFunz selectRec(int codCompAnia, int tpMoviEsec, int tpMoviRifto, IAmmbFunz iAmmbFunz) {
        return buildQuery("selectRec").bind("codCompAnia", codCompAnia).bind("tpMoviEsec", tpMoviEsec).bind("tpMoviRifto", tpMoviRifto).rowMapper(selectRecRm).singleResult(iAmmbFunz);
    }

    public DbAccessStatus insertRec(IAmmbFunz iAmmbFunz) {
        return buildQuery("insertRec").bind(iAmmbFunz).executeInsert();
    }

    public DbAccessStatus updateRec(IAmmbFunz iAmmbFunz) {
        return buildQuery("updateRec").bind(iAmmbFunz).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompAnia, int tpMoviEsec, int tpMoviRifto) {
        return buildQuery("deleteRec").bind("codCompAnia", codCompAnia).bind("tpMoviEsec", tpMoviEsec).bind("tpMoviRifto", tpMoviRifto).executeDelete();
    }

    public IAmmbFunz selectRec1(IAmmbFunz iAmmbFunz) {
        return buildQuery("selectRec1").bind(iAmmbFunz).rowMapper(selectRecRm).singleResult(iAmmbFunz);
    }

    public DbAccessStatus openCNst6(IAmmbFunz iAmmbFunz) {
        cNst6 = buildQuery("openCNst6").bind(iAmmbFunz).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst6() {
        return closeCursor(cNst6);
    }

    public IAmmbFunz fetchCNst6(IAmmbFunz iAmmbFunz) {
        return fetch(cNst6, iAmmbFunz, selectRecRm);
    }

    public IAmmbFunz selectRec2(IAmmbFunz iAmmbFunz) {
        return buildQuery("selectRec2").bind(iAmmbFunz).rowMapper(selectRecRm).singleResult(iAmmbFunz);
    }

    public DbAccessStatus openCNst28(IAmmbFunz iAmmbFunz) {
        cNst28 = buildQuery("openCNst28").bind(iAmmbFunz).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst28() {
        return closeCursor(cNst28);
    }

    public IAmmbFunz fetchCNst28(IAmmbFunz iAmmbFunz) {
        return fetch(cNst28, iAmmbFunz, selectRecRm);
    }
}
