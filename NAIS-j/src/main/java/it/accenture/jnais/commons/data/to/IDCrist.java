package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [D_CRIST]
 * 
 */
public interface IDCrist extends BaseSqlTo {

    /**
     * Host Variable P61-ID-D-CRIST
     * 
     */
    int getIdDCrist();

    void setIdDCrist(int idDCrist);

    /**
     * Host Variable P61-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable P61-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P61-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable P61-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for P61-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable P61-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable P61-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable P61-COD-PROD
     * 
     */
    String getCodProd();

    void setCodProd(String codProd);

    /**
     * Host Variable P61-DT-DECOR-DB
     * 
     */
    String getDtDecorDb();

    void setDtDecorDb(String dtDecorDb);

    /**
     * Host Variable P61-DS-RIGA
     * 
     */
    long getP61DsRiga();

    void setP61DsRiga(long p61DsRiga);

    /**
     * Host Variable P61-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P61-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P61-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable P61-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable P61-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P61-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable P61-RIS-MAT-31122011
     * 
     */
    AfDecimal getRisMat31122011();

    void setRisMat31122011(AfDecimal risMat31122011);

    /**
     * Nullable property for P61-RIS-MAT-31122011
     * 
     */
    AfDecimal getRisMat31122011Obj();

    void setRisMat31122011Obj(AfDecimal risMat31122011Obj);

    /**
     * Host Variable P61-PRE-V-31122011
     * 
     */
    AfDecimal getPreV31122011();

    void setPreV31122011(AfDecimal preV31122011);

    /**
     * Nullable property for P61-PRE-V-31122011
     * 
     */
    AfDecimal getPreV31122011Obj();

    void setPreV31122011Obj(AfDecimal preV31122011Obj);

    /**
     * Host Variable P61-PRE-RSH-V-31122011
     * 
     */
    AfDecimal getPreRshV31122011();

    void setPreRshV31122011(AfDecimal preRshV31122011);

    /**
     * Nullable property for P61-PRE-RSH-V-31122011
     * 
     */
    AfDecimal getPreRshV31122011Obj();

    void setPreRshV31122011Obj(AfDecimal preRshV31122011Obj);

    /**
     * Host Variable P61-CPT-RIVTO-31122011
     * 
     */
    AfDecimal getCptRivto31122011();

    void setCptRivto31122011(AfDecimal cptRivto31122011);

    /**
     * Nullable property for P61-CPT-RIVTO-31122011
     * 
     */
    AfDecimal getCptRivto31122011Obj();

    void setCptRivto31122011Obj(AfDecimal cptRivto31122011Obj);

    /**
     * Host Variable P61-IMPB-VIS-31122011
     * 
     */
    AfDecimal getImpbVis31122011();

    void setImpbVis31122011(AfDecimal impbVis31122011);

    /**
     * Nullable property for P61-IMPB-VIS-31122011
     * 
     */
    AfDecimal getImpbVis31122011Obj();

    void setImpbVis31122011Obj(AfDecimal impbVis31122011Obj);

    /**
     * Host Variable P61-IMPB-IS-31122011
     * 
     */
    AfDecimal getImpbIs31122011();

    void setImpbIs31122011(AfDecimal impbIs31122011);

    /**
     * Nullable property for P61-IMPB-IS-31122011
     * 
     */
    AfDecimal getImpbIs31122011Obj();

    void setImpbIs31122011Obj(AfDecimal impbIs31122011Obj);

    /**
     * Host Variable P61-IMPB-VIS-RP-P2011
     * 
     */
    AfDecimal getImpbVisRpP2011();

    void setImpbVisRpP2011(AfDecimal impbVisRpP2011);

    /**
     * Nullable property for P61-IMPB-VIS-RP-P2011
     * 
     */
    AfDecimal getImpbVisRpP2011Obj();

    void setImpbVisRpP2011Obj(AfDecimal impbVisRpP2011Obj);

    /**
     * Host Variable P61-IMPB-IS-RP-P2011
     * 
     */
    AfDecimal getImpbIsRpP2011();

    void setImpbIsRpP2011(AfDecimal impbIsRpP2011);

    /**
     * Nullable property for P61-IMPB-IS-RP-P2011
     * 
     */
    AfDecimal getImpbIsRpP2011Obj();

    void setImpbIsRpP2011Obj(AfDecimal impbIsRpP2011Obj);

    /**
     * Host Variable P61-PRE-V-30062014
     * 
     */
    AfDecimal getPreV30062014();

    void setPreV30062014(AfDecimal preV30062014);

    /**
     * Nullable property for P61-PRE-V-30062014
     * 
     */
    AfDecimal getPreV30062014Obj();

    void setPreV30062014Obj(AfDecimal preV30062014Obj);

    /**
     * Host Variable P61-PRE-RSH-V-30062014
     * 
     */
    AfDecimal getPreRshV30062014();

    void setPreRshV30062014(AfDecimal preRshV30062014);

    /**
     * Nullable property for P61-PRE-RSH-V-30062014
     * 
     */
    AfDecimal getPreRshV30062014Obj();

    void setPreRshV30062014Obj(AfDecimal preRshV30062014Obj);

    /**
     * Host Variable P61-CPT-INI-30062014
     * 
     */
    AfDecimal getCptIni30062014();

    void setCptIni30062014(AfDecimal cptIni30062014);

    /**
     * Nullable property for P61-CPT-INI-30062014
     * 
     */
    AfDecimal getCptIni30062014Obj();

    void setCptIni30062014Obj(AfDecimal cptIni30062014Obj);

    /**
     * Host Variable P61-IMPB-VIS-30062014
     * 
     */
    AfDecimal getImpbVis30062014();

    void setImpbVis30062014(AfDecimal impbVis30062014);

    /**
     * Nullable property for P61-IMPB-VIS-30062014
     * 
     */
    AfDecimal getImpbVis30062014Obj();

    void setImpbVis30062014Obj(AfDecimal impbVis30062014Obj);

    /**
     * Host Variable P61-IMPB-IS-30062014
     * 
     */
    AfDecimal getImpbIs30062014();

    void setImpbIs30062014(AfDecimal impbIs30062014);

    /**
     * Nullable property for P61-IMPB-IS-30062014
     * 
     */
    AfDecimal getImpbIs30062014Obj();

    void setImpbIs30062014Obj(AfDecimal impbIs30062014Obj);

    /**
     * Host Variable P61-IMPB-VIS-RP-P62014
     * 
     */
    AfDecimal getImpbVisRpP62014();

    void setImpbVisRpP62014(AfDecimal impbVisRpP62014);

    /**
     * Nullable property for P61-IMPB-VIS-RP-P62014
     * 
     */
    AfDecimal getImpbVisRpP62014Obj();

    void setImpbVisRpP62014Obj(AfDecimal impbVisRpP62014Obj);

    /**
     * Host Variable P61-IMPB-IS-RP-P62014
     * 
     */
    AfDecimal getImpbIsRpP62014();

    void setImpbIsRpP62014(AfDecimal impbIsRpP62014);

    /**
     * Nullable property for P61-IMPB-IS-RP-P62014
     * 
     */
    AfDecimal getImpbIsRpP62014Obj();

    void setImpbIsRpP62014Obj(AfDecimal impbIsRpP62014Obj);

    /**
     * Host Variable P61-RIS-MAT-30062014
     * 
     */
    AfDecimal getRisMat30062014();

    void setRisMat30062014(AfDecimal risMat30062014);

    /**
     * Nullable property for P61-RIS-MAT-30062014
     * 
     */
    AfDecimal getRisMat30062014Obj();

    void setRisMat30062014Obj(AfDecimal risMat30062014Obj);

    /**
     * Host Variable P61-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Nullable property for P61-ID-ADES
     * 
     */
    Integer getIdAdesObj();

    void setIdAdesObj(Integer idAdesObj);

    /**
     * Host Variable P61-MONT-LRD-END2000
     * 
     */
    AfDecimal getMontLrdEnd2000();

    void setMontLrdEnd2000(AfDecimal montLrdEnd2000);

    /**
     * Nullable property for P61-MONT-LRD-END2000
     * 
     */
    AfDecimal getMontLrdEnd2000Obj();

    void setMontLrdEnd2000Obj(AfDecimal montLrdEnd2000Obj);

    /**
     * Host Variable P61-PRE-LRD-END2000
     * 
     */
    AfDecimal getPreLrdEnd2000();

    void setPreLrdEnd2000(AfDecimal preLrdEnd2000);

    /**
     * Nullable property for P61-PRE-LRD-END2000
     * 
     */
    AfDecimal getPreLrdEnd2000Obj();

    void setPreLrdEnd2000Obj(AfDecimal preLrdEnd2000Obj);

    /**
     * Host Variable P61-RENDTO-LRD-END2000
     * 
     */
    AfDecimal getRendtoLrdEnd2000();

    void setRendtoLrdEnd2000(AfDecimal rendtoLrdEnd2000);

    /**
     * Nullable property for P61-RENDTO-LRD-END2000
     * 
     */
    AfDecimal getRendtoLrdEnd2000Obj();

    void setRendtoLrdEnd2000Obj(AfDecimal rendtoLrdEnd2000Obj);

    /**
     * Host Variable P61-MONT-LRD-END2006
     * 
     */
    AfDecimal getMontLrdEnd2006();

    void setMontLrdEnd2006(AfDecimal montLrdEnd2006);

    /**
     * Nullable property for P61-MONT-LRD-END2006
     * 
     */
    AfDecimal getMontLrdEnd2006Obj();

    void setMontLrdEnd2006Obj(AfDecimal montLrdEnd2006Obj);

    /**
     * Host Variable P61-PRE-LRD-END2006
     * 
     */
    AfDecimal getPreLrdEnd2006();

    void setPreLrdEnd2006(AfDecimal preLrdEnd2006);

    /**
     * Nullable property for P61-PRE-LRD-END2006
     * 
     */
    AfDecimal getPreLrdEnd2006Obj();

    void setPreLrdEnd2006Obj(AfDecimal preLrdEnd2006Obj);

    /**
     * Host Variable P61-RENDTO-LRD-END2006
     * 
     */
    AfDecimal getRendtoLrdEnd2006();

    void setRendtoLrdEnd2006(AfDecimal rendtoLrdEnd2006);

    /**
     * Nullable property for P61-RENDTO-LRD-END2006
     * 
     */
    AfDecimal getRendtoLrdEnd2006Obj();

    void setRendtoLrdEnd2006Obj(AfDecimal rendtoLrdEnd2006Obj);

    /**
     * Host Variable P61-MONT-LRD-DAL2007
     * 
     */
    AfDecimal getMontLrdDal2007();

    void setMontLrdDal2007(AfDecimal montLrdDal2007);

    /**
     * Nullable property for P61-MONT-LRD-DAL2007
     * 
     */
    AfDecimal getMontLrdDal2007Obj();

    void setMontLrdDal2007Obj(AfDecimal montLrdDal2007Obj);

    /**
     * Host Variable P61-PRE-LRD-DAL2007
     * 
     */
    AfDecimal getPreLrdDal2007();

    void setPreLrdDal2007(AfDecimal preLrdDal2007);

    /**
     * Nullable property for P61-PRE-LRD-DAL2007
     * 
     */
    AfDecimal getPreLrdDal2007Obj();

    void setPreLrdDal2007Obj(AfDecimal preLrdDal2007Obj);

    /**
     * Host Variable P61-RENDTO-LRD-DAL2007
     * 
     */
    AfDecimal getRendtoLrdDal2007();

    void setRendtoLrdDal2007(AfDecimal rendtoLrdDal2007);

    /**
     * Nullable property for P61-RENDTO-LRD-DAL2007
     * 
     */
    AfDecimal getRendtoLrdDal2007Obj();

    void setRendtoLrdDal2007Obj(AfDecimal rendtoLrdDal2007Obj);

    /**
     * Host Variable P61-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Nullable property for P61-ID-TRCH-DI-GAR
     * 
     */
    Integer getIdTrchDiGarObj();

    void setIdTrchDiGarObj(Integer idTrchDiGarObj);
};
