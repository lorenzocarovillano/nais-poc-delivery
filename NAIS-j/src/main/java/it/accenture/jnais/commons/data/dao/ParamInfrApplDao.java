package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IParamInfrAppl;

/**
 * Data Access Object(DAO) for table [PARAM_INFR_APPL]
 * 
 */
public class ParamInfrApplDao extends BaseSqlDao<IParamInfrAppl> {

    private final IRowMapper<IParamInfrAppl> selectByD09CodCompAniaRm = buildNamedRowMapper(IParamInfrAppl.class, "d09CodCompAnia", "ambiente", "piattaforma", "tpComCobolJava", "mqTpUtilizzoApiObj", "mqQueueManagerObj", "mqCodaPutObj", "mqCodaGetObj", "mqOpzPersistenzaObj", "mqOpzWaitObj", "mqOpzSyncpointObj", "mqAttesaRispostaObj", "mqTempoAttesa1Obj", "mqTempoAttesa2Obj", "mqTempoExpiryObj", "csocketIpAddressObj", "csocketPortNumObj", "flCompressoreCObj");

    public ParamInfrApplDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IParamInfrAppl> getToClass() {
        return IParamInfrAppl.class;
    }

    public IParamInfrAppl selectByD09CodCompAnia(int d09CodCompAnia, IParamInfrAppl iParamInfrAppl) {
        return buildQuery("selectByD09CodCompAnia").bind("d09CodCompAnia", d09CodCompAnia).rowMapper(selectByD09CodCompAniaRm).singleResult(iParamInfrAppl);
    }

    public DbAccessStatus insertRec(IParamInfrAppl iParamInfrAppl) {
        return buildQuery("insertRec").bind(iParamInfrAppl).executeInsert();
    }

    public DbAccessStatus updateRec(IParamInfrAppl iParamInfrAppl) {
        return buildQuery("updateRec").bind(iParamInfrAppl).executeUpdate();
    }

    public DbAccessStatus deleteByD09CodCompAnia(int d09CodCompAnia) {
        return buildQuery("deleteByD09CodCompAnia").bind("d09CodCompAnia", d09CodCompAnia).executeDelete();
    }

    public IParamInfrAppl selectByIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia, IParamInfrAppl iParamInfrAppl) {
        return buildQuery("selectByIdsv0003CodiceCompagniaAnia").bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByD09CodCompAniaRm).singleResult(iParamInfrAppl);
    }
}
