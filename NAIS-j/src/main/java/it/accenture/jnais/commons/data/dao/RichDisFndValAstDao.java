package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IRichDisFndValAst;

/**
 * Data Access Object(DAO) for tables [RICH_DIS_FND, VAL_AST]
 * 
 */
public class RichDisFndValAstDao extends BaseSqlDao<IRichDisFndValAst> {

    private Cursor cEff27;
    private Cursor cCpz27;
    private final IRowMapper<IRichDisFndValAst> selectRecRm = buildNamedRowMapper(IRichDisFndValAst.class, "idRichDisFnd", "idMoviFinrio", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codFndObj", "numQuoObj", "pcObj", "impMovtoObj", "dtDisDbObj", "codTariObj", "tpStatObj", "tpModDisObj", "codDiv", "dtCambioVltDbObj", "tpFnd", "dsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "dtDisCalcDbObj", "flCalcDisObj", "commisGestObj", "numQuoCdgFnzObj", "numQuoCdgtotFnzObj", "cosRunAssvaIdcObj", "flSwmBp2sObj");

    public RichDisFndValAstDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IRichDisFndValAst> getToClass() {
        return IRichDisFndValAst.class;
    }

    public IRichDisFndValAst selectRec(int ldbv5471IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRichDisFndValAst iRichDisFndValAst) {
        return buildQuery("selectRec").bind("ldbv5471IdTrchDiGar", ldbv5471IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectRecRm).singleResult(iRichDisFndValAst);
    }

    public DbAccessStatus openCEff27(int ldbv5471IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff27 = buildQuery("openCEff27").bind("ldbv5471IdTrchDiGar", ldbv5471IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff27() {
        return closeCursor(cEff27);
    }

    public IRichDisFndValAst fetchCEff27(IRichDisFndValAst iRichDisFndValAst) {
        return fetch(cEff27, iRichDisFndValAst, selectRecRm);
    }

    public IRichDisFndValAst selectRec1(int ldbv5471IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IRichDisFndValAst iRichDisFndValAst) {
        return buildQuery("selectRec1").bind("ldbv5471IdTrchDiGar", ldbv5471IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectRecRm).singleResult(iRichDisFndValAst);
    }

    public DbAccessStatus openCCpz27(int ldbv5471IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cCpz27 = buildQuery("openCCpz27").bind("ldbv5471IdTrchDiGar", ldbv5471IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz27() {
        return closeCursor(cCpz27);
    }

    public IRichDisFndValAst fetchCCpz27(IRichDisFndValAst iRichDisFndValAst) {
        return fetch(cCpz27, iRichDisFndValAst, selectRecRm);
    }
}
