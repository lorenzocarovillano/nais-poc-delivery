package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDColl;

/**
 * Data Access Object(DAO) for table [D_COLL]
 * 
 */
public class DCollDao extends BaseSqlDao<IDColl> {

    private Cursor cIdUpdEffDco;
    private Cursor cIdpEffDco;
    private Cursor cIdpCpzDco;
    private final IRowMapper<IDColl> selectByDcoDsRigaRm = buildNamedRowMapper(IDColl.class, "idDColl", "idPoli", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "impArrotPreObj", "pcSconObj", "flAdesSingObj", "tpImpObj", "flRiclPreDaCptObj", "tpAdesObj", "dtUltRinnTacDbObj", "impSconObj", "frazDfltObj", "etaScadMascDfltObj", "etaScadFemmDfltObj", "tpDfltDurObj", "durAaAdesDfltObj", "durMmAdesDfltObj", "durGgAdesDfltObj", "dtScadAdesDfltDbObj", "codFndDfltObj", "tpDurObj", "tpCalcDurObj", "flNoAderentiObj", "flDistintaCnbtvaObj", "flCnbtAutesObj", "flQtzPostEmisObj", "flComnzFndIsObj", "dcoDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public DCollDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDColl> getToClass() {
        return IDColl.class;
    }

    public IDColl selectByDcoDsRiga(long dcoDsRiga, IDColl iDColl) {
        return buildQuery("selectByDcoDsRiga").bind("dcoDsRiga", dcoDsRiga).rowMapper(selectByDcoDsRigaRm).singleResult(iDColl);
    }

    public DbAccessStatus insertRec(IDColl iDColl) {
        return buildQuery("insertRec").bind(iDColl).executeInsert();
    }

    public DbAccessStatus updateRec(IDColl iDColl) {
        return buildQuery("updateRec").bind(iDColl).executeUpdate();
    }

    public DbAccessStatus deleteByDcoDsRiga(long dcoDsRiga) {
        return buildQuery("deleteByDcoDsRiga").bind("dcoDsRiga", dcoDsRiga).executeDelete();
    }

    public IDColl selectRec(int dcoIdDColl, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDColl iDColl) {
        return buildQuery("selectRec").bind("dcoIdDColl", dcoIdDColl).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDcoDsRigaRm).singleResult(iDColl);
    }

    public DbAccessStatus updateRec1(IDColl iDColl) {
        return buildQuery("updateRec1").bind(iDColl).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffDco(int dcoIdDColl, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffDco = buildQuery("openCIdUpdEffDco").bind("dcoIdDColl", dcoIdDColl).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffDco() {
        return closeCursor(cIdUpdEffDco);
    }

    public IDColl fetchCIdUpdEffDco(IDColl iDColl) {
        return fetch(cIdUpdEffDco, iDColl, selectByDcoDsRigaRm);
    }

    public IDColl selectRec1(int dcoIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDColl iDColl) {
        return buildQuery("selectRec1").bind("dcoIdPoli", dcoIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDcoDsRigaRm).singleResult(iDColl);
    }

    public DbAccessStatus openCIdpEffDco(int dcoIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffDco = buildQuery("openCIdpEffDco").bind("dcoIdPoli", dcoIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffDco() {
        return closeCursor(cIdpEffDco);
    }

    public IDColl fetchCIdpEffDco(IDColl iDColl) {
        return fetch(cIdpEffDco, iDColl, selectByDcoDsRigaRm);
    }

    public IDColl selectRec2(int dcoIdDColl, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDColl iDColl) {
        return buildQuery("selectRec2").bind("dcoIdDColl", dcoIdDColl).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDcoDsRigaRm).singleResult(iDColl);
    }

    public IDColl selectRec3(int dcoIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDColl iDColl) {
        return buildQuery("selectRec3").bind("dcoIdPoli", dcoIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDcoDsRigaRm).singleResult(iDColl);
    }

    public DbAccessStatus openCIdpCpzDco(int dcoIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzDco = buildQuery("openCIdpCpzDco").bind("dcoIdPoli", dcoIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzDco() {
        return closeCursor(cIdpCpzDco);
    }

    public IDColl fetchCIdpCpzDco(IDColl iDColl) {
        return fetch(cIdpCpzDco, iDColl, selectByDcoDsRigaRm);
    }
}
