package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [STAT_OGG_WF]
 * 
 */
public interface IStatOggWf extends BaseSqlTo {

    /**
     * Host Variable STW-ID-OGG
     * 
     */
    int getStwIdOgg();

    void setStwIdOgg(int stwIdOgg);

    /**
     * Host Variable STW-TP-OGG
     * 
     */
    String getStwTpOgg();

    void setStwTpOgg(String stwTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable STW-ID-STAT-OGG-WF
     * 
     */
    int getIdStatOggWf();

    void setIdStatOggWf(int idStatOggWf);

    /**
     * Host Variable STW-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable STW-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for STW-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable STW-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable STW-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable STW-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable STW-COD-PRCS
     * 
     */
    String getCodPrcs();

    void setCodPrcs(String codPrcs);

    /**
     * Host Variable STW-COD-ATTVT
     * 
     */
    String getCodAttvt();

    void setCodAttvt(String codAttvt);

    /**
     * Host Variable STW-STAT-OGG-WF
     * 
     */
    String getStatOggWf();

    void setStatOggWf(String statOggWf);

    /**
     * Host Variable STW-FL-STAT-END
     * 
     */
    char getFlStatEnd();

    void setFlStatEnd(char flStatEnd);

    /**
     * Nullable property for STW-FL-STAT-END
     * 
     */
    Character getFlStatEndObj();

    void setFlStatEndObj(Character flStatEndObj);

    /**
     * Host Variable STW-DS-RIGA
     * 
     */
    long getStwDsRiga();

    void setStwDsRiga(long stwDsRiga);

    /**
     * Host Variable STW-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable STW-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable STW-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable STW-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable STW-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable STW-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
