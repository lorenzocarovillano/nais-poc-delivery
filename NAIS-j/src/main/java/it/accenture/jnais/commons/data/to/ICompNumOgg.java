package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [COMP_NUM_OGG]
 * 
 */
public interface ICompNumOgg extends BaseSqlTo {

    /**
     * Host Variable CNO-COD-COMPAGNIA-ANIA
     * 
     */
    int getCodCompagniaAnia();

    void setCodCompagniaAnia(int codCompagniaAnia);

    /**
     * Host Variable CNO-FORMA-ASSICURATIVA
     * 
     */
    String getFormaAssicurativa();

    void setFormaAssicurativa(String formaAssicurativa);

    /**
     * Host Variable CNO-COD-OGGETTO
     * 
     */
    String getCodOggetto();

    void setCodOggetto(String codOggetto);

    /**
     * Host Variable CNO-POSIZIONE
     * 
     */
    int getPosizione();

    void setPosizione(int posizione);

    /**
     * Host Variable CNO-COD-STR-DATO
     * 
     */
    String getCodStrDato();

    void setCodStrDato(String codStrDato);

    /**
     * Nullable property for CNO-COD-STR-DATO
     * 
     */
    String getCodStrDatoObj();

    void setCodStrDatoObj(String codStrDatoObj);

    /**
     * Host Variable CNO-COD-DATO
     * 
     */
    String getCodDato();

    void setCodDato(String codDato);

    /**
     * Nullable property for CNO-COD-DATO
     * 
     */
    String getCodDatoObj();

    void setCodDatoObj(String codDatoObj);

    /**
     * Host Variable CNO-VALORE-DEFAULT
     * 
     */
    String getValoreDefault();

    void setValoreDefault(String valoreDefault);

    /**
     * Nullable property for CNO-VALORE-DEFAULT
     * 
     */
    String getValoreDefaultObj();

    void setValoreDefaultObj(String valoreDefaultObj);

    /**
     * Host Variable CNO-LUNGHEZZA-DATO
     * 
     */
    int getLunghezzaDato();

    void setLunghezzaDato(int lunghezzaDato);

    /**
     * Nullable property for CNO-LUNGHEZZA-DATO
     * 
     */
    Integer getLunghezzaDatoObj();

    void setLunghezzaDatoObj(Integer lunghezzaDatoObj);

    /**
     * Host Variable CNO-FLAG-KEY-ULT-PROGR
     * 
     */
    char getFlagKeyUltProgr();

    void setFlagKeyUltProgr(char flagKeyUltProgr);

    /**
     * Nullable property for CNO-FLAG-KEY-ULT-PROGR
     * 
     */
    Character getFlagKeyUltProgrObj();

    void setFlagKeyUltProgrObj(Character flagKeyUltProgrObj);
};
