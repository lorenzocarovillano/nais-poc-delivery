package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ANAG_DATO]
 * 
 */
public interface IAnagDato extends BaseSqlTo {

    /**
     * Host Variable ADA-COD-COMPAGNIA-ANIA
     * 
     */
    int getCodCompagniaAnia();

    void setCodCompagniaAnia(int codCompagniaAnia);

    /**
     * Host Variable ADA-COD-DATO
     * 
     */
    String getCodDato();

    void setCodDato(String codDato);

    /**
     * Host Variable ADA-DESC-DATO-VCHAR
     * 
     */
    String getDescDatoVchar();

    void setDescDatoVchar(String descDatoVchar);

    /**
     * Nullable property for ADA-DESC-DATO-VCHAR
     * 
     */
    String getDescDatoVcharObj();

    void setDescDatoVcharObj(String descDatoVcharObj);

    /**
     * Host Variable ADA-TIPO-DATO
     * 
     */
    String getTipoDato();

    void setTipoDato(String tipoDato);

    /**
     * Nullable property for ADA-TIPO-DATO
     * 
     */
    String getTipoDatoObj();

    void setTipoDatoObj(String tipoDatoObj);

    /**
     * Host Variable ADA-LUNGHEZZA-DATO
     * 
     */
    int getLunghezzaDato();

    void setLunghezzaDato(int lunghezzaDato);

    /**
     * Nullable property for ADA-LUNGHEZZA-DATO
     * 
     */
    Integer getLunghezzaDatoObj();

    void setLunghezzaDatoObj(Integer lunghezzaDatoObj);

    /**
     * Host Variable ADA-PRECISIONE-DATO
     * 
     */
    short getPrecisioneDato();

    void setPrecisioneDato(short precisioneDato);

    /**
     * Nullable property for ADA-PRECISIONE-DATO
     * 
     */
    Short getPrecisioneDatoObj();

    void setPrecisioneDatoObj(Short precisioneDatoObj);

    /**
     * Host Variable ADA-COD-DOMINIO
     * 
     */
    String getCodDominio();

    void setCodDominio(String codDominio);

    /**
     * Nullable property for ADA-COD-DOMINIO
     * 
     */
    String getCodDominioObj();

    void setCodDominioObj(String codDominioObj);

    /**
     * Host Variable ADA-FORMATTAZIONE-DATO
     * 
     */
    String getFormattazioneDato();

    void setFormattazioneDato(String formattazioneDato);

    /**
     * Nullable property for ADA-FORMATTAZIONE-DATO
     * 
     */
    String getFormattazioneDatoObj();

    void setFormattazioneDatoObj(String formattazioneDatoObj);

    /**
     * Host Variable ADA-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);
};
