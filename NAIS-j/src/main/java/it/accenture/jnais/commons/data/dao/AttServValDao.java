package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAttServVal;

/**
 * Data Access Object(DAO) for table [ATT_SERV_VAL]
 * 
 */
public class AttServValDao extends BaseSqlDao<IAttServVal> {

    private Cursor cIdUpdEffP88;
    private Cursor cIdoEffP88;
    private Cursor cIdoCpzP88;
    private final IRowMapper<IAttServVal> selectByP88DsRigaRm = buildNamedRowMapper(IAttServVal.class, "idAttServVal", "codCompAnia", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "tpServVal", "p88IdOgg", "p88TpOgg", "p88DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "flAllFndObj");

    public AttServValDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAttServVal> getToClass() {
        return IAttServVal.class;
    }

    public IAttServVal selectByP88DsRiga(long p88DsRiga, IAttServVal iAttServVal) {
        return buildQuery("selectByP88DsRiga").bind("p88DsRiga", p88DsRiga).rowMapper(selectByP88DsRigaRm).singleResult(iAttServVal);
    }

    public DbAccessStatus insertRec(IAttServVal iAttServVal) {
        return buildQuery("insertRec").bind(iAttServVal).executeInsert();
    }

    public DbAccessStatus updateRec(IAttServVal iAttServVal) {
        return buildQuery("updateRec").bind(iAttServVal).executeUpdate();
    }

    public DbAccessStatus deleteByP88DsRiga(long p88DsRiga) {
        return buildQuery("deleteByP88DsRiga").bind("p88DsRiga", p88DsRiga).executeDelete();
    }

    public IAttServVal selectRec(int p88IdAttServVal, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IAttServVal iAttServVal) {
        return buildQuery("selectRec").bind("p88IdAttServVal", p88IdAttServVal).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP88DsRigaRm).singleResult(iAttServVal);
    }

    public DbAccessStatus updateRec1(IAttServVal iAttServVal) {
        return buildQuery("updateRec1").bind(iAttServVal).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffP88(int p88IdAttServVal, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffP88 = buildQuery("openCIdUpdEffP88").bind("p88IdAttServVal", p88IdAttServVal).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffP88() {
        return closeCursor(cIdUpdEffP88);
    }

    public IAttServVal fetchCIdUpdEffP88(IAttServVal iAttServVal) {
        return fetch(cIdUpdEffP88, iAttServVal, selectByP88DsRigaRm);
    }

    public IAttServVal selectRec1(int p88IdOgg, String p88TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IAttServVal iAttServVal) {
        return buildQuery("selectRec1").bind("p88IdOgg", p88IdOgg).bind("p88TpOgg", p88TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP88DsRigaRm).singleResult(iAttServVal);
    }

    public DbAccessStatus openCIdoEffP88(int p88IdOgg, String p88TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffP88 = buildQuery("openCIdoEffP88").bind("p88IdOgg", p88IdOgg).bind("p88TpOgg", p88TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffP88() {
        return closeCursor(cIdoEffP88);
    }

    public IAttServVal fetchCIdoEffP88(IAttServVal iAttServVal) {
        return fetch(cIdoEffP88, iAttServVal, selectByP88DsRigaRm);
    }

    public IAttServVal selectRec2(int p88IdAttServVal, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IAttServVal iAttServVal) {
        return buildQuery("selectRec2").bind("p88IdAttServVal", p88IdAttServVal).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP88DsRigaRm).singleResult(iAttServVal);
    }

    public IAttServVal selectRec3(IAttServVal iAttServVal) {
        return buildQuery("selectRec3").bind(iAttServVal).rowMapper(selectByP88DsRigaRm).singleResult(iAttServVal);
    }

    public DbAccessStatus openCIdoCpzP88(IAttServVal iAttServVal) {
        cIdoCpzP88 = buildQuery("openCIdoCpzP88").bind(iAttServVal).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzP88() {
        return closeCursor(cIdoCpzP88);
    }

    public IAttServVal fetchCIdoCpzP88(IAttServVal iAttServVal) {
        return fetch(cIdoCpzP88, iAttServVal, selectByP88DsRigaRm);
    }
}
