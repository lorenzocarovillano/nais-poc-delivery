package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IMatrMovimento;

/**
 * Data Access Object(DAO) for table [MATR_MOVIMENTO]
 * 
 */
public class MatrMovimentoDao extends BaseSqlDao<IMatrMovimento> {

    private Cursor curWcMmo01;
    private Cursor curWcMmo02;
    private final IRowMapper<IMatrMovimento> fetchCurWcMmo01Rm = buildNamedRowMapper(IMatrMovimento.class, "idMatrMovimento", "codCompAnia", "tpMoviPtf", "tpOgg", "tpFrmAssvaObj", "tpMoviAct", "ammissibilitaMoviObj", "servizioControlloObj", "codProcessoWfObj");

    public MatrMovimentoDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IMatrMovimento> getToClass() {
        return IMatrMovimento.class;
    }

    public IMatrMovimento selectRec(int idsv0003CodiceCompagniaAnia, int ldbi0260CodMoviNais, IMatrMovimento iMatrMovimento) {
        return buildQuery("selectRec").bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("ldbi0260CodMoviNais", ldbi0260CodMoviNais).singleResult(iMatrMovimento);
    }

    public DbAccessStatus openCurWcMmo01(int idsv0003CodiceCompagniaAnia, int ldbi0260CodMoviNais) {
        curWcMmo01 = buildQuery("openCurWcMmo01").bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("ldbi0260CodMoviNais", ldbi0260CodMoviNais).open();
        return dbStatus;
    }

    public DbAccessStatus openCurWcMmo02(int idsv0003CodiceCompagniaAnia, String ldbi0260CodMoviActuator, String ldbi0260OggNais, String ldbi0260FrmAssva) {
        curWcMmo02 = buildQuery("openCurWcMmo02").bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("ldbi0260CodMoviActuator", ldbi0260CodMoviActuator).bind("ldbi0260OggNais", ldbi0260OggNais).bind("ldbi0260FrmAssva", ldbi0260FrmAssva).open();
        return dbStatus;
    }

    public DbAccessStatus closeCurWcMmo01() {
        return closeCursor(curWcMmo01);
    }

    public DbAccessStatus closeCurWcMmo02() {
        return closeCursor(curWcMmo02);
    }

    public IMatrMovimento fetchCurWcMmo01(IMatrMovimento iMatrMovimento) {
        return fetch(curWcMmo01, iMatrMovimento, fetchCurWcMmo01Rm);
    }

    public IMatrMovimento fetchCurWcMmo02(IMatrMovimento iMatrMovimento) {
        return fetch(curWcMmo02, iMatrMovimento, fetchCurWcMmo01Rm);
    }
}
