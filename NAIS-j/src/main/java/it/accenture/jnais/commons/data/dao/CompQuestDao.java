package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ICompQuest;

/**
 * Data Access Object(DAO) for table [COMP_QUEST]
 * 
 */
public class CompQuestDao extends BaseSqlDao<ICompQuest> {

    private Cursor cNst14;
    private Cursor cNst15;
    private final IRowMapper<ICompQuest> fetchCNst14Rm = buildNamedRowMapper(ICompQuest.class, "idCompQuest", "dtIniEffDb", "codCompAnia", "q04CodCanObj", "dtEndEffDb", "q04CodQuest", "q04CodDomanda", "ordineDomanda", "codRisp", "obblRispObj");

    public CompQuestDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ICompQuest> getToClass() {
        return ICompQuest.class;
    }

    public DbAccessStatus openCNst14(ICompQuest iCompQuest) {
        cNst14 = buildQuery("openCNst14").bind(iCompQuest).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst14() {
        return closeCursor(cNst14);
    }

    public ICompQuest fetchCNst14(ICompQuest iCompQuest) {
        return fetch(cNst14, iCompQuest, fetchCNst14Rm);
    }

    public DbAccessStatus openCNst15(int idsv0003CodiceCompagniaAnia, String q04CodQuest, String q04CodDomanda, String wsDataInizioEffettoDb) {
        cNst15 = buildQuery("openCNst15").bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("q04CodQuest", q04CodQuest).bind("q04CodDomanda", q04CodDomanda).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst15() {
        return closeCursor(cNst15);
    }

    public ICompQuest fetchCNst15(ICompQuest iCompQuest) {
        return fetch(cNst15, iCompQuest, fetchCNst14Rm);
    }

    public ICompQuest selectRec(ICompQuest iCompQuest) {
        return buildQuery("selectRec").bind(iCompQuest).rowMapper(fetchCNst14Rm).singleResult(iCompQuest);
    }
}
