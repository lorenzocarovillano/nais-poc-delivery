package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDettTitCont1;

/**
 * Data Access Object(DAO) for tables [DETT_TIT_CONT, TIT_CONT]
 * 
 */
public class DettTitCont1Dao extends BaseSqlDao<IDettTitCont1> {

    private Cursor cEff1;
    private Cursor cCpz1;
    private Cursor cEff16;
    private Cursor cCpz16;
    private Cursor cEff35;
    private Cursor cCpz35;
    private Cursor cEff42;
    private Cursor cCpz42;
    private final IRowMapper<IDettTitCont1> selectRecRm = buildNamedRowMapper(IDettTitCont1.class, "dtcIdDettTitCont", "dtcIdTitCont", "dtcIdOgg", "dtcTpOgg", "dtcIdMoviCrz", "dtcIdMoviChiuObj", "dtcDtIniEffDb", "dtcDtEndEffDb", "dtcCodCompAnia", "dtcDtIniCopDbObj", "dtcDtEndCopDbObj", "dtcPreNetObj", "dtcIntrFrazObj", "dtcIntrMoraObj", "dtcIntrRetdtObj", "dtcIntrRiatObj", "dtcDirObj", "dtcSpeMedObj", "dtcTaxObj", "dtcSoprSanObj", "dtcSoprSpoObj", "dtcSoprTecObj", "dtcSoprProfObj", "dtcSoprAltObj", "dtcPreTotObj", "dtcPrePpIasObj", "dtcPreSoloRshObj", "dtcCarAcqObj", "dtcCarGestObj", "dtcCarIncObj", "dtcProvAcq1aaObj", "dtcProvAcq2aaObj", "dtcProvRicorObj", "dtcProvIncObj", "dtcProvDaRecObj", "dtcCodDvsObj", "dtcFrqMoviObj", "dtcTpRgmFisc", "dtcCodTariObj", "dtcTpStatTit", "dtcImpAzObj", "dtcImpAderObj", "dtcImpTfrObj", "dtcImpVoloObj", "dtcManfeeAnticObj", "dtcManfeeRicorObj", "dtcManfeeRecObj", "dtcDtEsiTitDbObj", "dtcSpeAgeObj", "dtcCarIasObj", "dtcTotIntrPrestObj", "dtcDsRiga", "dtcDsOperSql", "dtcDsVer", "dtcDsTsIniCptz", "dtcDsTsEndCptz", "dtcDsUtente", "dtcDsStatoElab", "dtcImpTrasfeObj", "dtcImpTfrStrcObj", "dtcNumGgRitardoPagObj", "dtcNumGgRivalObj", "dtcAcqExpObj", "dtcRemunAssObj", "dtcCommisInterObj", "dtcCnbtAntiracObj", "titIdTitCont", "titIdOgg", "titTpOgg", "titIbRichObj", "titIdMoviCrz", "titIdMoviChiuObj", "titDtIniEffDb", "titDtEndEffDb", "titCodCompAnia", "titTpTit", "titProgTitObj", "titTpPreTit", "titTpStatTit", "titDtIniCopDbObj", "titDtEndCopDbObj", "titImpPagObj", "titFlSollObj", "titFrazObj", "titDtApplzMoraDbObj", "titFlMoraObj", "titIdRappReteObj", "titIdRappAnaObj", "titCodDvsObj", "titDtEmisTitDbObj", "titDtEsiTitDbObj", "titTotPreNetObj", "titTotIntrFrazObj", "titTotIntrMoraObj", "titTotIntrPrestObj", "titTotIntrRetdtObj", "titTotIntrRiatObj", "titTotDirObj", "titTotSpeMedObj", "titTotTaxObj", "titTotSoprSanObj", "titTotSoprTecObj", "titTotSoprSpoObj", "titTotSoprProfObj", "titTotSoprAltObj", "titTotPreTotObj", "titTotPrePpIasObj", "titTotCarAcqObj", "titTotCarGestObj", "titTotCarIncObj", "titTotPreSoloRshObj", "titTotProvAcq1aaObj", "titTotProvAcq2aaObj", "titTotProvRicorObj", "titTotProvIncObj", "titTotProvDaRecObj", "titImpAzObj", "titImpAderObj", "titImpTfrObj", "titImpVoloObj", "titTotManfeeAnticObj", "titTotManfeeRicorObj", "titTotManfeeRecObj", "titTpMezPagAddObj", "titEstrCntCorrAddObj", "titDtVltDbObj", "titFlForzDtVltObj", "titDtCambioVltDbObj", "titTotSpeAgeObj", "titTotCarIasObj", "titNumRatAccorpateObj", "titDsRiga", "titDsOperSql", "titDsVer", "titDsTsIniCptz", "titDsTsEndCptz", "titDsUtente", "titDsStatoElab", "titFlTitDaReinvstObj", "titDtRichAddRidDbObj", "titTpEsiRidObj", "titCodIbanObj", "titImpTrasfeObj", "titImpTfrStrcObj", "titDtCertFiscDbObj", "titTpCausStorObj", "titTpCausDispStorObj", "titTpTitMigrazObj", "titTotAcqExpObj", "titTotRemunAssObj", "titTotCommisInterObj", "titTpCausRimbObj", "titTotCnbtAntiracObj", "titFlIncAutogenObj");
    private final IRowMapper<IDettTitCont1> selectRec2Rm = buildNamedRowMapper(IDettTitCont1.class, "titDtIniCopDbObj", "titNumRatAccorpateObj", "titFrazObj");
    private final IRowMapper<IDettTitCont1> selectRec4Rm = buildNamedRowMapper(IDettTitCont1.class, "titIdTitCont", "titIdOgg", "titTpOgg", "titIbRichObj", "titIdMoviCrz", "titIdMoviChiuObj", "titDtIniEffDb", "titDtEndEffDb", "titCodCompAnia", "titTpTit", "titProgTitObj", "titTpPreTit", "titTpStatTit", "titDtIniCopDbObj", "titDtEndCopDbObj", "titImpPagObj", "titFlSollObj", "titFrazObj", "titDtApplzMoraDbObj", "titFlMoraObj", "titIdRappReteObj", "titIdRappAnaObj", "titCodDvsObj", "titDtEmisTitDbObj", "titDtEsiTitDbObj", "titTotPreNetObj", "titTotIntrFrazObj", "titTotIntrMoraObj", "titTotIntrPrestObj", "titTotIntrRetdtObj", "titTotIntrRiatObj", "titTotDirObj", "titTotSpeMedObj", "titTotTaxObj", "titTotSoprSanObj", "titTotSoprTecObj", "titTotSoprSpoObj", "titTotSoprProfObj", "titTotSoprAltObj", "titTotPreTotObj", "titTotPrePpIasObj", "titTotCarAcqObj", "titTotCarGestObj", "titTotCarIncObj", "titTotPreSoloRshObj", "titTotProvAcq1aaObj", "titTotProvAcq2aaObj", "titTotProvRicorObj", "titTotProvIncObj", "titTotProvDaRecObj", "titImpAzObj", "titImpAderObj", "titImpTfrObj", "titImpVoloObj", "titTotManfeeAnticObj", "titTotManfeeRicorObj", "titTotManfeeRecObj", "titTpMezPagAddObj", "titEstrCntCorrAddObj", "titDtVltDbObj", "titFlForzDtVltObj", "titDtCambioVltDbObj", "titTotSpeAgeObj", "titTotCarIasObj", "titNumRatAccorpateObj", "titDsRiga", "titDsOperSql", "titDsVer", "titDsTsIniCptz", "titDsTsEndCptz", "titDsUtente", "titDsStatoElab", "titFlTitDaReinvstObj", "titDtRichAddRidDbObj", "titTpEsiRidObj", "titCodIbanObj", "titImpTrasfeObj", "titImpTfrStrcObj", "titDtCertFiscDbObj", "titTpCausStorObj", "titTpCausDispStorObj", "titTpTitMigrazObj", "titTotAcqExpObj", "titTotRemunAssObj", "titTotCommisInterObj", "titTpCausRimbObj", "titTotCnbtAntiracObj", "titFlIncAutogenObj");

    public DettTitCont1Dao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDettTitCont1> getToClass() {
        return IDettTitCont1.class;
    }

    public IDettTitCont1 selectRec(IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec").bind(iDettTitCont1).rowMapper(selectRecRm).singleResult(iDettTitCont1);
    }

    public DbAccessStatus openCEff1(IDettTitCont1 iDettTitCont1) {
        cEff1 = buildQuery("openCEff1").bind(iDettTitCont1).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff1() {
        return closeCursor(cEff1);
    }

    public IDettTitCont1 fetchCEff1(IDettTitCont1 iDettTitCont1) {
        return fetch(cEff1, iDettTitCont1, selectRecRm);
    }

    public IDettTitCont1 selectRec1(IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec1").bind(iDettTitCont1).rowMapper(selectRecRm).singleResult(iDettTitCont1);
    }

    public DbAccessStatus openCCpz1(IDettTitCont1 iDettTitCont1) {
        cCpz1 = buildQuery("openCCpz1").bind(iDettTitCont1).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz1() {
        return closeCursor(cCpz1);
    }

    public IDettTitCont1 fetchCCpz1(IDettTitCont1 iDettTitCont1) {
        return fetch(cCpz1, iDettTitCont1, selectRecRm);
    }

    public IDettTitCont1 selectRec2(int ldbv2971IdOgg, String ldbv2971TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec2").bind("ldbv2971IdOgg", ldbv2971IdOgg).bind("ldbv2971TpOgg", ldbv2971TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectRec2Rm).singleResult(iDettTitCont1);
    }

    public IDettTitCont1 selectRec3(IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec3").bind(iDettTitCont1).rowMapper(selectRec2Rm).singleResult(iDettTitCont1);
    }

    public IDettTitCont1 selectRec4(IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec4").bind(iDettTitCont1).rowMapper(selectRec4Rm).singleResult(iDettTitCont1);
    }

    public DbAccessStatus openCEff16(IDettTitCont1 iDettTitCont1) {
        cEff16 = buildQuery("openCEff16").bind(iDettTitCont1).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff16() {
        return closeCursor(cEff16);
    }

    public IDettTitCont1 fetchCEff16(IDettTitCont1 iDettTitCont1) {
        return fetch(cEff16, iDettTitCont1, selectRec4Rm);
    }

    public IDettTitCont1 selectRec5(IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec5").bind(iDettTitCont1).rowMapper(selectRec4Rm).singleResult(iDettTitCont1);
    }

    public DbAccessStatus openCCpz16(IDettTitCont1 iDettTitCont1) {
        cCpz16 = buildQuery("openCCpz16").bind(iDettTitCont1).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz16() {
        return closeCursor(cCpz16);
    }

    public IDettTitCont1 fetchCCpz16(IDettTitCont1 iDettTitCont1) {
        return fetch(cCpz16, iDettTitCont1, selectRec4Rm);
    }

    public IDettTitCont1 selectRec6(IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec6").bind(iDettTitCont1).rowMapper(selectRec4Rm).singleResult(iDettTitCont1);
    }

    public DbAccessStatus openCEff35(IDettTitCont1 iDettTitCont1) {
        cEff35 = buildQuery("openCEff35").bind(iDettTitCont1).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff35() {
        return closeCursor(cEff35);
    }

    public IDettTitCont1 fetchCEff35(IDettTitCont1 iDettTitCont1) {
        return fetch(cEff35, iDettTitCont1, selectRec4Rm);
    }

    public IDettTitCont1 selectRec7(IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec7").bind(iDettTitCont1).rowMapper(selectRec4Rm).singleResult(iDettTitCont1);
    }

    public DbAccessStatus openCCpz35(IDettTitCont1 iDettTitCont1) {
        cCpz35 = buildQuery("openCCpz35").bind(iDettTitCont1).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz35() {
        return closeCursor(cCpz35);
    }

    public IDettTitCont1 fetchCCpz35(IDettTitCont1 iDettTitCont1) {
        return fetch(cCpz35, iDettTitCont1, selectRec4Rm);
    }

    public IDettTitCont1 selectRec8(IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec8").bind(iDettTitCont1).rowMapper(selectRec4Rm).singleResult(iDettTitCont1);
    }

    public DbAccessStatus openCEff42(IDettTitCont1 iDettTitCont1) {
        cEff42 = buildQuery("openCEff42").bind(iDettTitCont1).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff42() {
        return closeCursor(cEff42);
    }

    public IDettTitCont1 fetchCEff42(IDettTitCont1 iDettTitCont1) {
        return fetch(cEff42, iDettTitCont1, selectRec4Rm);
    }

    public IDettTitCont1 selectRec9(IDettTitCont1 iDettTitCont1) {
        return buildQuery("selectRec9").bind(iDettTitCont1).rowMapper(selectRec4Rm).singleResult(iDettTitCont1);
    }

    public DbAccessStatus openCCpz42(IDettTitCont1 iDettTitCont1) {
        cCpz42 = buildQuery("openCCpz42").bind(iDettTitCont1).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz42() {
        return closeCursor(cCpz42);
    }

    public IDettTitCont1 fetchCCpz42(IDettTitCont1 iDettTitCont1) {
        return fetch(cCpz42, iDettTitCont1, selectRec4Rm);
    }
}
