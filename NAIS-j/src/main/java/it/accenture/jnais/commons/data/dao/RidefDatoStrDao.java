package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import it.accenture.jnais.commons.data.to.IRidefDatoStr;

/**
 * Data Access Object(DAO) for table [RIDEF_DATO_STR]
 * 
 */
public class RidefDatoStrDao extends BaseSqlDao<IRidefDatoStr> {

    public RidefDatoStrDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IRidefDatoStr> getToClass() {
        return IRidefDatoStr.class;
    }

    public String selectRec(int codCompagniaAnia, int tipoMovimento, String rdsCodDato, String dft) {
        return buildQuery("selectRec").bind("codCompagniaAnia", codCompagniaAnia).bind("tipoMovimento", tipoMovimento).bind("rdsCodDato", rdsCodDato).scalarResultString(dft);
    }
}
