package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IQuotzFndUnit;

/**
 * Data Access Object(DAO) for table [QUOTZ_FND_UNIT]
 * 
 */
public class QuotzFndUnitDao extends BaseSqlDao<IQuotzFndUnit> {

    private Cursor cNst2;
    private final IRowMapper<IQuotzFndUnit> selectRecRm = buildNamedRowMapper(IQuotzFndUnit.class, "codCompAnia", "codFnd", "dtQtzDb", "valQuoObj", "valQuoManfeeObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "tpFnd", "dtRilevazioneNavDbObj", "valQuoAcqObj", "flNoNavObj");

    public QuotzFndUnitDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IQuotzFndUnit> getToClass() {
        return IQuotzFndUnit.class;
    }

    public IQuotzFndUnit selectRec(int codCompAnia, String codFnd, String dtQtzDb, IQuotzFndUnit iQuotzFndUnit) {
        return buildQuery("selectRec").bind("codCompAnia", codCompAnia).bind("codFnd", codFnd).bind("dtQtzDb", dtQtzDb).rowMapper(selectRecRm).singleResult(iQuotzFndUnit);
    }

    public DbAccessStatus insertRec(IQuotzFndUnit iQuotzFndUnit) {
        return buildQuery("insertRec").bind(iQuotzFndUnit).executeInsert();
    }

    public DbAccessStatus updateRec(IQuotzFndUnit iQuotzFndUnit) {
        return buildQuery("updateRec").bind(iQuotzFndUnit).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompAnia, String codFnd, String dtQtzDb) {
        return buildQuery("deleteRec").bind("codCompAnia", codCompAnia).bind("codFnd", codFnd).bind("dtQtzDb", dtQtzDb).executeDelete();
    }

    public IQuotzFndUnit selectRec1(String l19CodFnd, String l19DtQtzDb, int idsv0003CodiceCompagniaAnia, IQuotzFndUnit iQuotzFndUnit) {
        return buildQuery("selectRec1").bind("l19CodFnd", l19CodFnd).bind("l19DtQtzDb", l19DtQtzDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectRecRm).singleResult(iQuotzFndUnit);
    }

    public DbAccessStatus openCNst2(String l19CodFnd, String l19DtQtzDb, int idsv0003CodiceCompagniaAnia) {
        cNst2 = buildQuery("openCNst2").bind("l19CodFnd", l19CodFnd).bind("l19DtQtzDb", l19DtQtzDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst2() {
        return closeCursor(cNst2);
    }

    public IQuotzFndUnit fetchCNst2(IQuotzFndUnit iQuotzFndUnit) {
        return fetch(cNst2, iQuotzFndUnit, selectRecRm);
    }
}
