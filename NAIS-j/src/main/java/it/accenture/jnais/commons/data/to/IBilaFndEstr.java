package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BILA_FND_ESTR]
 * 
 */
public interface IBilaFndEstr extends BaseSqlTo {

    /**
     * Host Variable B01-ID-BILA-FND-ESTR
     * 
     */
    int getB01IdBilaFndEstr();

    void setB01IdBilaFndEstr(int b01IdBilaFndEstr);

    /**
     * Host Variable B01-ID-BILA-TRCH-ESTR
     * 
     */
    int getIdBilaTrchEstr();

    void setIdBilaTrchEstr(int idBilaTrchEstr);

    /**
     * Host Variable B01-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable B01-ID-RICH-ESTRAZ-MAS
     * 
     */
    int getIdRichEstrazMas();

    void setIdRichEstrazMas(int idRichEstrazMas);

    /**
     * Host Variable B01-ID-RICH-ESTRAZ-AGG
     * 
     */
    int getIdRichEstrazAgg();

    void setIdRichEstrazAgg(int idRichEstrazAgg);

    /**
     * Nullable property for B01-ID-RICH-ESTRAZ-AGG
     * 
     */
    Integer getIdRichEstrazAggObj();

    void setIdRichEstrazAggObj(Integer idRichEstrazAggObj);

    /**
     * Host Variable B01-DT-RIS-DB
     * 
     */
    String getDtRisDb();

    void setDtRisDb(String dtRisDb);

    /**
     * Host Variable B01-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable B01-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Host Variable B01-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Host Variable B01-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Host Variable B01-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Host Variable B01-DT-QTZ-INI-DB
     * 
     */
    String getDtQtzIniDb();

    void setDtQtzIniDb(String dtQtzIniDb);

    /**
     * Nullable property for B01-DT-QTZ-INI-DB
     * 
     */
    String getDtQtzIniDbObj();

    void setDtQtzIniDbObj(String dtQtzIniDbObj);

    /**
     * Host Variable B01-NUM-QUO-INI
     * 
     */
    AfDecimal getNumQuoIni();

    void setNumQuoIni(AfDecimal numQuoIni);

    /**
     * Nullable property for B01-NUM-QUO-INI
     * 
     */
    AfDecimal getNumQuoIniObj();

    void setNumQuoIniObj(AfDecimal numQuoIniObj);

    /**
     * Host Variable B01-NUM-QUO-DT-CALC
     * 
     */
    AfDecimal getNumQuoDtCalc();

    void setNumQuoDtCalc(AfDecimal numQuoDtCalc);

    /**
     * Nullable property for B01-NUM-QUO-DT-CALC
     * 
     */
    AfDecimal getNumQuoDtCalcObj();

    void setNumQuoDtCalcObj(AfDecimal numQuoDtCalcObj);

    /**
     * Host Variable B01-VAL-QUO-INI
     * 
     */
    AfDecimal getValQuoIni();

    void setValQuoIni(AfDecimal valQuoIni);

    /**
     * Nullable property for B01-VAL-QUO-INI
     * 
     */
    AfDecimal getValQuoIniObj();

    void setValQuoIniObj(AfDecimal valQuoIniObj);

    /**
     * Host Variable B01-DT-VALZZ-QUO-DT-CA-DB
     * 
     */
    String getDtValzzQuoDtCaDb();

    void setDtValzzQuoDtCaDb(String dtValzzQuoDtCaDb);

    /**
     * Nullable property for B01-DT-VALZZ-QUO-DT-CA-DB
     * 
     */
    String getDtValzzQuoDtCaDbObj();

    void setDtValzzQuoDtCaDbObj(String dtValzzQuoDtCaDbObj);

    /**
     * Host Variable B01-VAL-QUO-DT-CALC
     * 
     */
    AfDecimal getValQuoDtCalc();

    void setValQuoDtCalc(AfDecimal valQuoDtCalc);

    /**
     * Nullable property for B01-VAL-QUO-DT-CALC
     * 
     */
    AfDecimal getValQuoDtCalcObj();

    void setValQuoDtCalcObj(AfDecimal valQuoDtCalcObj);

    /**
     * Host Variable B01-VAL-QUO-T
     * 
     */
    AfDecimal getValQuoT();

    void setValQuoT(AfDecimal valQuoT);

    /**
     * Nullable property for B01-VAL-QUO-T
     * 
     */
    AfDecimal getValQuoTObj();

    void setValQuoTObj(AfDecimal valQuoTObj);

    /**
     * Host Variable B01-PC-INVST
     * 
     */
    AfDecimal getPcInvst();

    void setPcInvst(AfDecimal pcInvst);

    /**
     * Nullable property for B01-PC-INVST
     * 
     */
    AfDecimal getPcInvstObj();

    void setPcInvstObj(AfDecimal pcInvstObj);

    /**
     * Host Variable B01-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable B01-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable B01-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable B01-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable B01-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
