package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ITrchLiq;

/**
 * Data Access Object(DAO) for table [TRCH_LIQ]
 * 
 */
public class TrchLiqDao extends BaseSqlDao<ITrchLiq> {

    private Cursor cIdUpdEffTli;
    private Cursor cEff22;
    private Cursor cCpz22;
    private final IRowMapper<ITrchLiq> selectByTliDsRigaRm = buildNamedRowMapper(ITrchLiq.class, "idTrchLiq", "idGarLiqObj", "idLiq", "idTrchDiGar", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "impLrdCalc", "impLrdDfzObj", "impLrdEfflq", "impNetCalc", "impNetDfzObj", "impNetEfflq", "impUtiObj", "codTariObj", "codDvsObj", "iasOnerPrvntFinObj", "iasMggSinObj", "iasRstDpstObj", "impRimbObj", "componTaxRimbObj", "risMatObj", "risSpeObj", "tliDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "iasPnlObj");

    public TrchLiqDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ITrchLiq> getToClass() {
        return ITrchLiq.class;
    }

    public ITrchLiq selectByTliDsRiga(long tliDsRiga, ITrchLiq iTrchLiq) {
        return buildQuery("selectByTliDsRiga").bind("tliDsRiga", tliDsRiga).rowMapper(selectByTliDsRigaRm).singleResult(iTrchLiq);
    }

    public DbAccessStatus insertRec(ITrchLiq iTrchLiq) {
        return buildQuery("insertRec").bind(iTrchLiq).executeInsert();
    }

    public DbAccessStatus updateRec(ITrchLiq iTrchLiq) {
        return buildQuery("updateRec").bind(iTrchLiq).executeUpdate();
    }

    public DbAccessStatus deleteByTliDsRiga(long tliDsRiga) {
        return buildQuery("deleteByTliDsRiga").bind("tliDsRiga", tliDsRiga).executeDelete();
    }

    public ITrchLiq selectRec(int tliIdTrchLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITrchLiq iTrchLiq) {
        return buildQuery("selectRec").bind("tliIdTrchLiq", tliIdTrchLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTliDsRigaRm).singleResult(iTrchLiq);
    }

    public DbAccessStatus updateRec1(ITrchLiq iTrchLiq) {
        return buildQuery("updateRec1").bind(iTrchLiq).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffTli(int tliIdTrchLiq, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffTli = buildQuery("openCIdUpdEffTli").bind("tliIdTrchLiq", tliIdTrchLiq).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffTli() {
        return closeCursor(cIdUpdEffTli);
    }

    public ITrchLiq fetchCIdUpdEffTli(ITrchLiq iTrchLiq) {
        return fetch(cIdUpdEffTli, iTrchLiq, selectByTliDsRigaRm);
    }

    public ITrchLiq selectRec1(int tliIdTrchLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITrchLiq iTrchLiq) {
        return buildQuery("selectRec1").bind("tliIdTrchLiq", tliIdTrchLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTliDsRigaRm).singleResult(iTrchLiq);
    }

    public ITrchLiq selectRec2(int ldbv3621IdMoviCrz, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITrchLiq iTrchLiq) {
        return buildQuery("selectRec2").bind("ldbv3621IdMoviCrz", ldbv3621IdMoviCrz).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTliDsRigaRm).singleResult(iTrchLiq);
    }

    public DbAccessStatus openCEff22(int ldbv3621IdMoviCrz, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff22 = buildQuery("openCEff22").bind("ldbv3621IdMoviCrz", ldbv3621IdMoviCrz).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff22() {
        return closeCursor(cEff22);
    }

    public ITrchLiq fetchCEff22(ITrchLiq iTrchLiq) {
        return fetch(cEff22, iTrchLiq, selectByTliDsRigaRm);
    }

    public ITrchLiq selectRec3(int ldbv3621IdMoviCrz, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITrchLiq iTrchLiq) {
        return buildQuery("selectRec3").bind("ldbv3621IdMoviCrz", ldbv3621IdMoviCrz).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTliDsRigaRm).singleResult(iTrchLiq);
    }

    public DbAccessStatus openCCpz22(int ldbv3621IdMoviCrz, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cCpz22 = buildQuery("openCCpz22").bind("ldbv3621IdMoviCrz", ldbv3621IdMoviCrz).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz22() {
        return closeCursor(cCpz22);
    }

    public ITrchLiq fetchCCpz22(ITrchLiq iTrchLiq) {
        return fetch(cCpz22, iTrchLiq, selectByTliDsRigaRm);
    }
}
