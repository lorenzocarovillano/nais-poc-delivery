package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IMoviFinrio;

/**
 * Data Access Object(DAO) for table [MOVI_FINRIO]
 * 
 */
public class MoviFinrioDao extends BaseSqlDao<IMoviFinrio> {

    private Cursor cIdUpdEffMfz;
    private Cursor cIdpEffMfz;
    private Cursor cIdpCpzMfz;
    private Cursor cEff32;
    private Cursor cCpz32;
    private final IRowMapper<IMoviFinrio> selectByMfzDsRigaRm = buildNamedRowMapper(IMoviFinrio.class, "idMoviFinrio", "idAdesObj", "idLiqObj", "idTitContObj", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpMoviFinrio", "dtEffMoviFinrioDbObj", "dtElabDbObj", "dtRichMoviDbObj", "cosOprzObj", "statMovi", "mfzDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "tpCausRettificaObj");

    public MoviFinrioDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IMoviFinrio> getToClass() {
        return IMoviFinrio.class;
    }

    public IMoviFinrio selectByMfzDsRiga(long mfzDsRiga, IMoviFinrio iMoviFinrio) {
        return buildQuery("selectByMfzDsRiga").bind("mfzDsRiga", mfzDsRiga).rowMapper(selectByMfzDsRigaRm).singleResult(iMoviFinrio);
    }

    public DbAccessStatus insertRec(IMoviFinrio iMoviFinrio) {
        return buildQuery("insertRec").bind(iMoviFinrio).executeInsert();
    }

    public DbAccessStatus updateRec(IMoviFinrio iMoviFinrio) {
        return buildQuery("updateRec").bind(iMoviFinrio).executeUpdate();
    }

    public DbAccessStatus deleteByMfzDsRiga(long mfzDsRiga) {
        return buildQuery("deleteByMfzDsRiga").bind("mfzDsRiga", mfzDsRiga).executeDelete();
    }

    public IMoviFinrio selectRec(int mfzIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IMoviFinrio iMoviFinrio) {
        return buildQuery("selectRec").bind("mfzIdMoviFinrio", mfzIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByMfzDsRigaRm).singleResult(iMoviFinrio);
    }

    public DbAccessStatus updateRec1(IMoviFinrio iMoviFinrio) {
        return buildQuery("updateRec1").bind(iMoviFinrio).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffMfz(int mfzIdMoviFinrio, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffMfz = buildQuery("openCIdUpdEffMfz").bind("mfzIdMoviFinrio", mfzIdMoviFinrio).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffMfz() {
        return closeCursor(cIdUpdEffMfz);
    }

    public IMoviFinrio fetchCIdUpdEffMfz(IMoviFinrio iMoviFinrio) {
        return fetch(cIdUpdEffMfz, iMoviFinrio, selectByMfzDsRigaRm);
    }

    public IMoviFinrio selectRec1(int mfzIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IMoviFinrio iMoviFinrio) {
        return buildQuery("selectRec1").bind("mfzIdAdes", mfzIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByMfzDsRigaRm).singleResult(iMoviFinrio);
    }

    public DbAccessStatus openCIdpEffMfz(int mfzIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffMfz = buildQuery("openCIdpEffMfz").bind("mfzIdAdes", mfzIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffMfz() {
        return closeCursor(cIdpEffMfz);
    }

    public IMoviFinrio fetchCIdpEffMfz(IMoviFinrio iMoviFinrio) {
        return fetch(cIdpEffMfz, iMoviFinrio, selectByMfzDsRigaRm);
    }

    public IMoviFinrio selectRec2(int mfzIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IMoviFinrio iMoviFinrio) {
        return buildQuery("selectRec2").bind("mfzIdMoviFinrio", mfzIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByMfzDsRigaRm).singleResult(iMoviFinrio);
    }

    public IMoviFinrio selectRec3(int mfzIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IMoviFinrio iMoviFinrio) {
        return buildQuery("selectRec3").bind("mfzIdAdes", mfzIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByMfzDsRigaRm).singleResult(iMoviFinrio);
    }

    public DbAccessStatus openCIdpCpzMfz(int mfzIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzMfz = buildQuery("openCIdpCpzMfz").bind("mfzIdAdes", mfzIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzMfz() {
        return closeCursor(cIdpCpzMfz);
    }

    public IMoviFinrio fetchCIdpCpzMfz(IMoviFinrio iMoviFinrio) {
        return fetch(cIdpCpzMfz, iMoviFinrio, selectByMfzDsRigaRm);
    }

    public IMoviFinrio selectRec4(int mfzIdMoviCrz, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IMoviFinrio iMoviFinrio) {
        return buildQuery("selectRec4").bind("mfzIdMoviCrz", mfzIdMoviCrz).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByMfzDsRigaRm).singleResult(iMoviFinrio);
    }

    public DbAccessStatus openCEff32(int mfzIdMoviCrz, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff32 = buildQuery("openCEff32").bind("mfzIdMoviCrz", mfzIdMoviCrz).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff32() {
        return closeCursor(cEff32);
    }

    public IMoviFinrio fetchCEff32(IMoviFinrio iMoviFinrio) {
        return fetch(cEff32, iMoviFinrio, selectByMfzDsRigaRm);
    }

    public IMoviFinrio selectRec5(int mfzIdMoviCrz, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IMoviFinrio iMoviFinrio) {
        return buildQuery("selectRec5").bind("mfzIdMoviCrz", mfzIdMoviCrz).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByMfzDsRigaRm).singleResult(iMoviFinrio);
    }

    public DbAccessStatus openCCpz32(int mfzIdMoviCrz, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cCpz32 = buildQuery("openCCpz32").bind("mfzIdMoviCrz", mfzIdMoviCrz).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz32() {
        return closeCursor(cCpz32);
    }

    public IMoviFinrio fetchCCpz32(IMoviFinrio iMoviFinrio) {
        return fetch(cCpz32, iMoviFinrio, selectByMfzDsRigaRm);
    }
}
