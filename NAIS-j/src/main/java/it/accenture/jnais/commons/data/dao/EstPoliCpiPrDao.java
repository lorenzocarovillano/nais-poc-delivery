package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IEstPoliCpiPr;

/**
 * Data Access Object(DAO) for table [EST_POLI_CPI_PR]
 * 
 */
public class EstPoliCpiPrDao extends BaseSqlDao<IEstPoliCpiPr> {

    private Cursor cIdUpdEffP67;
    private Cursor cIboEffP67;
    private Cursor cIboCpzP67;
    private final IRowMapper<IEstPoliCpiPr> selectByP67DsRigaRm = buildNamedRowMapper(IEstPoliCpiPr.class, "idEstPoliCpiPr", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "ibOgg", "p67DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "codProdEstno", "cptFinObj", "numTstFinObj", "tsFinanzObj", "durMmFinanzObj", "dtEndFinanzDbObj", "amm1aRatObj", "valRiscBeneObj", "ammRatEndObj", "tsCreRatFinanzObj", "impFinRevolvingObj", "impUtilCRevObj", "impRatRevolvingObj", "dt1oUtlzCRevDbObj", "impAsstoObj", "preVersObj", "dtScadCopDbObj", "ggDelMmScadRatObj", "flPreFin", "dtScad1aRatDbObj", "dtErogFinanzDbObj", "dtStipulaFinanzDbObj", "mmPreammObj", "impDebResObj", "impRatFinanzObj", "impCanoneAnticObj", "perRatFinanzObj", "tpModPagRatObj", "tpFinanzErObj", "catFinanzErObj", "valRiscEndLeasObj", "dtEstFinanzDbObj", "dtManCopDbObj", "numFinanzObj", "tpModAcqsObj");

    public EstPoliCpiPrDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IEstPoliCpiPr> getToClass() {
        return IEstPoliCpiPr.class;
    }

    public IEstPoliCpiPr selectByP67DsRiga(long p67DsRiga, IEstPoliCpiPr iEstPoliCpiPr) {
        return buildQuery("selectByP67DsRiga").bind("p67DsRiga", p67DsRiga).rowMapper(selectByP67DsRigaRm).singleResult(iEstPoliCpiPr);
    }

    public DbAccessStatus insertRec(IEstPoliCpiPr iEstPoliCpiPr) {
        return buildQuery("insertRec").bind(iEstPoliCpiPr).executeInsert();
    }

    public DbAccessStatus updateRec(IEstPoliCpiPr iEstPoliCpiPr) {
        return buildQuery("updateRec").bind(iEstPoliCpiPr).executeUpdate();
    }

    public DbAccessStatus deleteByP67DsRiga(long p67DsRiga) {
        return buildQuery("deleteByP67DsRiga").bind("p67DsRiga", p67DsRiga).executeDelete();
    }

    public IEstPoliCpiPr selectRec(int p67IdEstPoliCpiPr, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstPoliCpiPr iEstPoliCpiPr) {
        return buildQuery("selectRec").bind("p67IdEstPoliCpiPr", p67IdEstPoliCpiPr).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP67DsRigaRm).singleResult(iEstPoliCpiPr);
    }

    public DbAccessStatus updateRec1(IEstPoliCpiPr iEstPoliCpiPr) {
        return buildQuery("updateRec1").bind(iEstPoliCpiPr).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffP67(int p67IdEstPoliCpiPr, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffP67 = buildQuery("openCIdUpdEffP67").bind("p67IdEstPoliCpiPr", p67IdEstPoliCpiPr).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffP67() {
        return closeCursor(cIdUpdEffP67);
    }

    public IEstPoliCpiPr fetchCIdUpdEffP67(IEstPoliCpiPr iEstPoliCpiPr) {
        return fetch(cIdUpdEffP67, iEstPoliCpiPr, selectByP67DsRigaRm);
    }

    public IEstPoliCpiPr selectRec1(String p67IbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstPoliCpiPr iEstPoliCpiPr) {
        return buildQuery("selectRec1").bind("p67IbOgg", p67IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP67DsRigaRm).singleResult(iEstPoliCpiPr);
    }

    public DbAccessStatus openCIboEffP67(String p67IbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIboEffP67 = buildQuery("openCIboEffP67").bind("p67IbOgg", p67IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboEffP67() {
        return closeCursor(cIboEffP67);
    }

    public IEstPoliCpiPr fetchCIboEffP67(IEstPoliCpiPr iEstPoliCpiPr) {
        return fetch(cIboEffP67, iEstPoliCpiPr, selectByP67DsRigaRm);
    }

    public IEstPoliCpiPr selectRec2(int p67IdEstPoliCpiPr, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstPoliCpiPr iEstPoliCpiPr) {
        return buildQuery("selectRec2").bind("p67IdEstPoliCpiPr", p67IdEstPoliCpiPr).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP67DsRigaRm).singleResult(iEstPoliCpiPr);
    }

    public IEstPoliCpiPr selectRec3(String p67IbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstPoliCpiPr iEstPoliCpiPr) {
        return buildQuery("selectRec3").bind("p67IbOgg", p67IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP67DsRigaRm).singleResult(iEstPoliCpiPr);
    }

    public DbAccessStatus openCIboCpzP67(String p67IbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIboCpzP67 = buildQuery("openCIboCpzP67").bind("p67IbOgg", p67IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboCpzP67() {
        return closeCursor(cIboCpzP67);
    }

    public IEstPoliCpiPr fetchCIboCpzP67(IEstPoliCpiPr iEstPoliCpiPr) {
        return fetch(cIboCpzP67, iEstPoliCpiPr, selectByP67DsRigaRm);
    }
}
