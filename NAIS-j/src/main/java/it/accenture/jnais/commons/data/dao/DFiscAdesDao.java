package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDFiscAdes;

/**
 * Data Access Object(DAO) for table [D_FISC_ADES]
 * 
 */
public class DFiscAdesDao extends BaseSqlDao<IDFiscAdes> {

    private Cursor cIdUpdEffDfa;
    private Cursor cIdpEffDfa;
    private Cursor cIdpCpzDfa;
    private final IRowMapper<IDFiscAdes> selectByDfaDsRigaRm = buildNamedRowMapper(IDFiscAdes.class, "idDFiscAdes", "idAdes", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpIscFndObj", "dtAccnsRappFndDbObj", "impCnbtAzK1Obj", "impCnbtIscK1Obj", "impCnbtTfrK1Obj", "impCnbtVolK1Obj", "impCnbtAzK2Obj", "impCnbtIscK2Obj", "impCnbtTfrK2Obj", "impCnbtVolK2Obj", "matuK1Obj", "matuResK1Obj", "matuK2Obj", "impbVisObj", "impstVisObj", "impbIsK2Obj", "impstSostK2Obj", "ridzTfrObj", "pcTfrObj", "alqTfrObj", "totAnticObj", "impbTfrAnticObj", "impstTfrAnticObj", "ridzTfrSuAnticObj", "impbVisAnticObj", "impstVisAnticObj", "impbIsK2AnticObj", "impstSostK2AntiObj", "ultCommisTrasfeObj", "codDvsObj", "alqPrvrObj", "pcEseImpstTfrObj", "impEseImpstTfrObj", "anzCnbtvaCarassObj", "anzCnbtvaCaraziObj", "anzSrvzObj", "impCnbtNdedK1Obj", "impCnbtNdedK2Obj", "impCnbtNdedK3Obj", "impCnbtAzK3Obj", "impCnbtIscK3Obj", "impCnbtTfrK3Obj", "impCnbtVolK3Obj", "matuK3Obj", "impb252AnticObj", "impst252AnticObj", "dt1aCnbzDbObj", "commisDiTrasfeObj", "aaCnbzK1Obj", "aaCnbzK2Obj", "aaCnbzK3Obj", "mmCnbzK1Obj", "mmCnbzK2Obj", "mmCnbzK3Obj", "flApplzNewfisObj", "redtTassAbbatK3Obj", "cnbtEcc4x100K1Obj", "dfaDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "creditoIsObj", "redtTassAbbatK2Obj", "impbIsK3Obj", "impstSostK3Obj", "impb252K3Obj", "impst252K3Obj", "impbIsK3AnticObj", "impstSostK3AntiObj", "impbIrpefK1AntiObj", "impstIrpefK1AntObj", "impbIrpefK2AntiObj", "impstIrpefK2AntObj", "dtCessazioneDbObj", "totImpstObj", "onerTrasfeObj", "impNetTrasferitoObj");

    public DFiscAdesDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDFiscAdes> getToClass() {
        return IDFiscAdes.class;
    }

    public IDFiscAdes selectByDfaDsRiga(long dfaDsRiga, IDFiscAdes iDFiscAdes) {
        return buildQuery("selectByDfaDsRiga").bind("dfaDsRiga", dfaDsRiga).rowMapper(selectByDfaDsRigaRm).singleResult(iDFiscAdes);
    }

    public DbAccessStatus insertRec(IDFiscAdes iDFiscAdes) {
        return buildQuery("insertRec").bind(iDFiscAdes).executeInsert();
    }

    public DbAccessStatus updateRec(IDFiscAdes iDFiscAdes) {
        return buildQuery("updateRec").bind(iDFiscAdes).executeUpdate();
    }

    public DbAccessStatus deleteByDfaDsRiga(long dfaDsRiga) {
        return buildQuery("deleteByDfaDsRiga").bind("dfaDsRiga", dfaDsRiga).executeDelete();
    }

    public IDFiscAdes selectRec(int dfaIdDFiscAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDFiscAdes iDFiscAdes) {
        return buildQuery("selectRec").bind("dfaIdDFiscAdes", dfaIdDFiscAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDfaDsRigaRm).singleResult(iDFiscAdes);
    }

    public DbAccessStatus updateRec1(IDFiscAdes iDFiscAdes) {
        return buildQuery("updateRec1").bind(iDFiscAdes).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffDfa(int dfaIdDFiscAdes, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffDfa = buildQuery("openCIdUpdEffDfa").bind("dfaIdDFiscAdes", dfaIdDFiscAdes).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffDfa() {
        return closeCursor(cIdUpdEffDfa);
    }

    public IDFiscAdes fetchCIdUpdEffDfa(IDFiscAdes iDFiscAdes) {
        return fetch(cIdUpdEffDfa, iDFiscAdes, selectByDfaDsRigaRm);
    }

    public IDFiscAdes selectRec1(int dfaIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDFiscAdes iDFiscAdes) {
        return buildQuery("selectRec1").bind("dfaIdAdes", dfaIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDfaDsRigaRm).singleResult(iDFiscAdes);
    }

    public DbAccessStatus openCIdpEffDfa(int dfaIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffDfa = buildQuery("openCIdpEffDfa").bind("dfaIdAdes", dfaIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffDfa() {
        return closeCursor(cIdpEffDfa);
    }

    public IDFiscAdes fetchCIdpEffDfa(IDFiscAdes iDFiscAdes) {
        return fetch(cIdpEffDfa, iDFiscAdes, selectByDfaDsRigaRm);
    }

    public IDFiscAdes selectRec2(int dfaIdDFiscAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDFiscAdes iDFiscAdes) {
        return buildQuery("selectRec2").bind("dfaIdDFiscAdes", dfaIdDFiscAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDfaDsRigaRm).singleResult(iDFiscAdes);
    }

    public IDFiscAdes selectRec3(int dfaIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDFiscAdes iDFiscAdes) {
        return buildQuery("selectRec3").bind("dfaIdAdes", dfaIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDfaDsRigaRm).singleResult(iDFiscAdes);
    }

    public DbAccessStatus openCIdpCpzDfa(int dfaIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzDfa = buildQuery("openCIdpCpzDfa").bind("dfaIdAdes", dfaIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzDfa() {
        return closeCursor(cIdpCpzDfa);
    }

    public IDFiscAdes fetchCIdpCpzDfa(IDFiscAdes iDFiscAdes) {
        return fetch(cIdpCpzDfa, iDFiscAdes, selectByDfaDsRigaRm);
    }
}
