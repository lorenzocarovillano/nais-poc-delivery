package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [RIS_DI_TRCH]
 * 
 */
public interface IRisDiTrch extends BaseSqlTo {

    /**
     * Host Variable RST-ID-RIS-DI-TRCH
     * 
     */
    int getIdRisDiTrch();

    void setIdRisDiTrch(int idRisDiTrch);

    /**
     * Host Variable RST-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable RST-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for RST-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable RST-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable RST-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable RST-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable RST-TP-CALC-RIS
     * 
     */
    String getTpCalcRis();

    void setTpCalcRis(String tpCalcRis);

    /**
     * Nullable property for RST-TP-CALC-RIS
     * 
     */
    String getTpCalcRisObj();

    void setTpCalcRisObj(String tpCalcRisObj);

    /**
     * Host Variable RST-ULT-RM
     * 
     */
    AfDecimal getUltRm();

    void setUltRm(AfDecimal ultRm);

    /**
     * Nullable property for RST-ULT-RM
     * 
     */
    AfDecimal getUltRmObj();

    void setUltRmObj(AfDecimal ultRmObj);

    /**
     * Host Variable RST-DT-CALC-DB
     * 
     */
    String getDtCalcDb();

    void setDtCalcDb(String dtCalcDb);

    /**
     * Nullable property for RST-DT-CALC-DB
     * 
     */
    String getDtCalcDbObj();

    void setDtCalcDbObj(String dtCalcDbObj);

    /**
     * Host Variable RST-DT-ELAB-DB
     * 
     */
    String getDtElabDb();

    void setDtElabDb(String dtElabDb);

    /**
     * Nullable property for RST-DT-ELAB-DB
     * 
     */
    String getDtElabDbObj();

    void setDtElabDbObj(String dtElabDbObj);

    /**
     * Host Variable RST-RIS-BILA
     * 
     */
    AfDecimal getRisBila();

    void setRisBila(AfDecimal risBila);

    /**
     * Nullable property for RST-RIS-BILA
     * 
     */
    AfDecimal getRisBilaObj();

    void setRisBilaObj(AfDecimal risBilaObj);

    /**
     * Host Variable RST-RIS-MAT
     * 
     */
    AfDecimal getRisMat();

    void setRisMat(AfDecimal risMat);

    /**
     * Nullable property for RST-RIS-MAT
     * 
     */
    AfDecimal getRisMatObj();

    void setRisMatObj(AfDecimal risMatObj);

    /**
     * Host Variable RST-INCR-X-RIVAL
     * 
     */
    AfDecimal getIncrXRival();

    void setIncrXRival(AfDecimal incrXRival);

    /**
     * Nullable property for RST-INCR-X-RIVAL
     * 
     */
    AfDecimal getIncrXRivalObj();

    void setIncrXRivalObj(AfDecimal incrXRivalObj);

    /**
     * Host Variable RST-RPTO-PRE
     * 
     */
    AfDecimal getRptoPre();

    void setRptoPre(AfDecimal rptoPre);

    /**
     * Nullable property for RST-RPTO-PRE
     * 
     */
    AfDecimal getRptoPreObj();

    void setRptoPreObj(AfDecimal rptoPreObj);

    /**
     * Host Variable RST-FRAZ-PRE-PP
     * 
     */
    AfDecimal getFrazPrePp();

    void setFrazPrePp(AfDecimal frazPrePp);

    /**
     * Nullable property for RST-FRAZ-PRE-PP
     * 
     */
    AfDecimal getFrazPrePpObj();

    void setFrazPrePpObj(AfDecimal frazPrePpObj);

    /**
     * Host Variable RST-RIS-TOT
     * 
     */
    AfDecimal getRisTot();

    void setRisTot(AfDecimal risTot);

    /**
     * Nullable property for RST-RIS-TOT
     * 
     */
    AfDecimal getRisTotObj();

    void setRisTotObj(AfDecimal risTotObj);

    /**
     * Host Variable RST-RIS-SPE
     * 
     */
    AfDecimal getRisSpe();

    void setRisSpe(AfDecimal risSpe);

    /**
     * Nullable property for RST-RIS-SPE
     * 
     */
    AfDecimal getRisSpeObj();

    void setRisSpeObj(AfDecimal risSpeObj);

    /**
     * Host Variable RST-RIS-ABB
     * 
     */
    AfDecimal getRisAbb();

    void setRisAbb(AfDecimal risAbb);

    /**
     * Nullable property for RST-RIS-ABB
     * 
     */
    AfDecimal getRisAbbObj();

    void setRisAbbObj(AfDecimal risAbbObj);

    /**
     * Host Variable RST-RIS-BNSFDT
     * 
     */
    AfDecimal getRisBnsfdt();

    void setRisBnsfdt(AfDecimal risBnsfdt);

    /**
     * Nullable property for RST-RIS-BNSFDT
     * 
     */
    AfDecimal getRisBnsfdtObj();

    void setRisBnsfdtObj(AfDecimal risBnsfdtObj);

    /**
     * Host Variable RST-RIS-SOPR
     * 
     */
    AfDecimal getRisSopr();

    void setRisSopr(AfDecimal risSopr);

    /**
     * Nullable property for RST-RIS-SOPR
     * 
     */
    AfDecimal getRisSoprObj();

    void setRisSoprObj(AfDecimal risSoprObj);

    /**
     * Host Variable RST-RIS-INTEG-BAS-TEC
     * 
     */
    AfDecimal getRisIntegBasTec();

    void setRisIntegBasTec(AfDecimal risIntegBasTec);

    /**
     * Nullable property for RST-RIS-INTEG-BAS-TEC
     * 
     */
    AfDecimal getRisIntegBasTecObj();

    void setRisIntegBasTecObj(AfDecimal risIntegBasTecObj);

    /**
     * Host Variable RST-RIS-INTEG-DECR-TS
     * 
     */
    AfDecimal getRisIntegDecrTs();

    void setRisIntegDecrTs(AfDecimal risIntegDecrTs);

    /**
     * Nullable property for RST-RIS-INTEG-DECR-TS
     * 
     */
    AfDecimal getRisIntegDecrTsObj();

    void setRisIntegDecrTsObj(AfDecimal risIntegDecrTsObj);

    /**
     * Host Variable RST-RIS-GAR-CASO-MOR
     * 
     */
    AfDecimal getRisGarCasoMor();

    void setRisGarCasoMor(AfDecimal risGarCasoMor);

    /**
     * Nullable property for RST-RIS-GAR-CASO-MOR
     * 
     */
    AfDecimal getRisGarCasoMorObj();

    void setRisGarCasoMorObj(AfDecimal risGarCasoMorObj);

    /**
     * Host Variable RST-RIS-ZIL
     * 
     */
    AfDecimal getRisZil();

    void setRisZil(AfDecimal risZil);

    /**
     * Nullable property for RST-RIS-ZIL
     * 
     */
    AfDecimal getRisZilObj();

    void setRisZilObj(AfDecimal risZilObj);

    /**
     * Host Variable RST-RIS-FAIVL
     * 
     */
    AfDecimal getRisFaivl();

    void setRisFaivl(AfDecimal risFaivl);

    /**
     * Nullable property for RST-RIS-FAIVL
     * 
     */
    AfDecimal getRisFaivlObj();

    void setRisFaivlObj(AfDecimal risFaivlObj);

    /**
     * Host Variable RST-RIS-COS-AMMTZ
     * 
     */
    AfDecimal getRisCosAmmtz();

    void setRisCosAmmtz(AfDecimal risCosAmmtz);

    /**
     * Nullable property for RST-RIS-COS-AMMTZ
     * 
     */
    AfDecimal getRisCosAmmtzObj();

    void setRisCosAmmtzObj(AfDecimal risCosAmmtzObj);

    /**
     * Host Variable RST-RIS-SPE-FAIVL
     * 
     */
    AfDecimal getRisSpeFaivl();

    void setRisSpeFaivl(AfDecimal risSpeFaivl);

    /**
     * Nullable property for RST-RIS-SPE-FAIVL
     * 
     */
    AfDecimal getRisSpeFaivlObj();

    void setRisSpeFaivlObj(AfDecimal risSpeFaivlObj);

    /**
     * Host Variable RST-RIS-PREST-FAIVL
     * 
     */
    AfDecimal getRisPrestFaivl();

    void setRisPrestFaivl(AfDecimal risPrestFaivl);

    /**
     * Nullable property for RST-RIS-PREST-FAIVL
     * 
     */
    AfDecimal getRisPrestFaivlObj();

    void setRisPrestFaivlObj(AfDecimal risPrestFaivlObj);

    /**
     * Host Variable RST-RIS-COMPON-ASSVA
     * 
     */
    AfDecimal getRisComponAssva();

    void setRisComponAssva(AfDecimal risComponAssva);

    /**
     * Nullable property for RST-RIS-COMPON-ASSVA
     * 
     */
    AfDecimal getRisComponAssvaObj();

    void setRisComponAssvaObj(AfDecimal risComponAssvaObj);

    /**
     * Host Variable RST-ULT-COEFF-RIS
     * 
     */
    AfDecimal getUltCoeffRis();

    void setUltCoeffRis(AfDecimal ultCoeffRis);

    /**
     * Nullable property for RST-ULT-COEFF-RIS
     * 
     */
    AfDecimal getUltCoeffRisObj();

    void setUltCoeffRisObj(AfDecimal ultCoeffRisObj);

    /**
     * Host Variable RST-ULT-COEFF-AGG-RIS
     * 
     */
    AfDecimal getUltCoeffAggRis();

    void setUltCoeffAggRis(AfDecimal ultCoeffAggRis);

    /**
     * Nullable property for RST-ULT-COEFF-AGG-RIS
     * 
     */
    AfDecimal getUltCoeffAggRisObj();

    void setUltCoeffAggRisObj(AfDecimal ultCoeffAggRisObj);

    /**
     * Host Variable RST-RIS-ACQ
     * 
     */
    AfDecimal getRisAcq();

    void setRisAcq(AfDecimal risAcq);

    /**
     * Nullable property for RST-RIS-ACQ
     * 
     */
    AfDecimal getRisAcqObj();

    void setRisAcqObj(AfDecimal risAcqObj);

    /**
     * Host Variable RST-RIS-UTI
     * 
     */
    AfDecimal getRisUti();

    void setRisUti(AfDecimal risUti);

    /**
     * Nullable property for RST-RIS-UTI
     * 
     */
    AfDecimal getRisUtiObj();

    void setRisUtiObj(AfDecimal risUtiObj);

    /**
     * Host Variable RST-RIS-MAT-EFF
     * 
     */
    AfDecimal getRisMatEff();

    void setRisMatEff(AfDecimal risMatEff);

    /**
     * Nullable property for RST-RIS-MAT-EFF
     * 
     */
    AfDecimal getRisMatEffObj();

    void setRisMatEffObj(AfDecimal risMatEffObj);

    /**
     * Host Variable RST-RIS-RISTORNI-CAP
     * 
     */
    AfDecimal getRisRistorniCap();

    void setRisRistorniCap(AfDecimal risRistorniCap);

    /**
     * Nullable property for RST-RIS-RISTORNI-CAP
     * 
     */
    AfDecimal getRisRistorniCapObj();

    void setRisRistorniCapObj(AfDecimal risRistorniCapObj);

    /**
     * Host Variable RST-RIS-TRM-BNS
     * 
     */
    AfDecimal getRisTrmBns();

    void setRisTrmBns(AfDecimal risTrmBns);

    /**
     * Nullable property for RST-RIS-TRM-BNS
     * 
     */
    AfDecimal getRisTrmBnsObj();

    void setRisTrmBnsObj(AfDecimal risTrmBnsObj);

    /**
     * Host Variable RST-RIS-BNSRIC
     * 
     */
    AfDecimal getRisBnsric();

    void setRisBnsric(AfDecimal risBnsric);

    /**
     * Nullable property for RST-RIS-BNSRIC
     * 
     */
    AfDecimal getRisBnsricObj();

    void setRisBnsricObj(AfDecimal risBnsricObj);

    /**
     * Host Variable RST-DS-RIGA
     * 
     */
    long getRstDsRiga();

    void setRstDsRiga(long rstDsRiga);

    /**
     * Host Variable RST-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable RST-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable RST-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable RST-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable RST-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable RST-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable RST-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Host Variable RST-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Nullable property for RST-COD-FND
     * 
     */
    String getCodFndObj();

    void setCodFndObj(String codFndObj);

    /**
     * Host Variable RST-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable RST-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Host Variable RST-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Host Variable RST-RIS-MIN-GARTO
     * 
     */
    AfDecimal getRisMinGarto();

    void setRisMinGarto(AfDecimal risMinGarto);

    /**
     * Nullable property for RST-RIS-MIN-GARTO
     * 
     */
    AfDecimal getRisMinGartoObj();

    void setRisMinGartoObj(AfDecimal risMinGartoObj);

    /**
     * Host Variable RST-RIS-RSH-DFLT
     * 
     */
    AfDecimal getRisRshDflt();

    void setRisRshDflt(AfDecimal risRshDflt);

    /**
     * Nullable property for RST-RIS-RSH-DFLT
     * 
     */
    AfDecimal getRisRshDfltObj();

    void setRisRshDfltObj(AfDecimal risRshDfltObj);

    /**
     * Host Variable RST-RIS-MOVI-NON-INVES
     * 
     */
    AfDecimal getRisMoviNonInves();

    void setRisMoviNonInves(AfDecimal risMoviNonInves);

    /**
     * Nullable property for RST-RIS-MOVI-NON-INVES
     * 
     */
    AfDecimal getRisMoviNonInvesObj();

    void setRisMoviNonInvesObj(AfDecimal risMoviNonInvesObj);
};
