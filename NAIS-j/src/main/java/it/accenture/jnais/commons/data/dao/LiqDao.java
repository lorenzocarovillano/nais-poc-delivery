package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ILiq;

/**
 * Data Access Object(DAO) for table [LIQ]
 * 
 */
public class LiqDao extends BaseSqlDao<ILiq> {

    private Cursor cIdUpdEffLqu;
    private Cursor cIboEffLqu;
    private Cursor cIbsEffLqu0;
    private Cursor cIdoEffLqu;
    private Cursor cIboCpzLqu;
    private Cursor cIbsCpzLqu0;
    private Cursor cIdoCpzLqu;
    private Cursor cEff34;
    private Cursor cCpz34;
    private Cursor cEff40;
    private Cursor cCpz40;
    private final IRowMapper<ILiq> selectByLquDsRigaRm = buildNamedRowMapper(ILiq.class, "idLiq", "lquIdOgg", "lquTpOgg", "lquIdMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "ibOggObj", "tpLiq", "descCauEveSinVcharObj", "codCauSinObj", "codSinCatstrfObj", "dtMorDbObj", "dtDenDbObj", "dtPervDenDbObj", "dtRichDbObj", "tpSinObj", "tpRiscObj", "tpMetRiscObj", "dtLiqDbObj", "codDvsObj", "totImpLrdLiqtoObj", "totImpPrestObj", "totImpIntrPrestObj", "totImpUtiObj", "totImpRitTfrObj", "totImpRitAccObj", "totImpRitVisObj", "totImpbTfrObj", "totImpbAccObj", "totImpbVisObj", "totImpRimbObj", "impbImpstPrvrObj", "impstPrvrObj", "impbImpst252Obj", "impst252Obj", "totImpIsObj", "impDirLiqObj", "totImpNetLiqtoObj", "montEnd2000Obj", "montEnd2006Obj", "pcRenObj", "impPnlObj", "impbIrpefObj", "impstIrpefObj", "dtVltDbObj", "dtEndIstrDbObj", "tpRimb", "speRcsObj", "ibLiqObj", "totIasOnerPrvntObj", "totIasMggSinObj", "totIasRstDpstObj", "impOnerLiqObj", "componTaxRimbObj", "tpMezPagObj", "impExcontrObj", "impIntrRitPagObj", "bnsNonGodutoObj", "cnbtInpstfmObj", "impstDaRimbObj", "impbIsObj", "taxSepObj", "impbTaxSepObj", "impbIntrSuPrestObj", "addizComunObj", "impbAddizComunObj", "addizRegionObj", "impbAddizRegionObj", "montDal2007Obj", "impbCnbtInpstfmObj", "impLrdDaRimbObj", "impDirDaRimbObj", "risMatObj", "risSpeObj", "lquDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "totIasPnlObj", "flEveGartoObj", "impRenK1Obj", "impRenK2Obj", "impRenK3Obj", "pcRenK1Obj", "pcRenK2Obj", "pcRenK3Obj", "tpCausAnticObj", "impLrdLiqtoRiltObj", "impstApplRiltObj", "pcRiscParzObj", "impstBolloTotVObj", "impstBolloDettCObj", "impstBolloTotSwObj", "impstBolloTotAaObj", "impbVis1382011Obj", "impstVis1382011Obj", "impbIs1382011Obj", "impstSost1382011Obj", "pcAbbTitStatObj", "impbBolloDettCObj", "flPreCompObj", "impbVis662014Obj", "impstVis662014Obj", "impbIs662014Obj", "impstSost662014Obj", "pcAbbTs662014Obj", "impLrdCalcCpObj", "cosTunnelUscitaObj");
    private final IRowMapper<ILiq> selectRec8Rm = buildNamedRowMapper(ILiq.class, "ldbv2271ImpLrdLiqtoObj");

    public LiqDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ILiq> getToClass() {
        return ILiq.class;
    }

    public ILiq selectByLquDsRiga(long lquDsRiga, ILiq iLiq) {
        return buildQuery("selectByLquDsRiga").bind("lquDsRiga", lquDsRiga).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus insertRec(ILiq iLiq) {
        return buildQuery("insertRec").bind(iLiq).executeInsert();
    }

    public DbAccessStatus updateRec(ILiq iLiq) {
        return buildQuery("updateRec").bind(iLiq).executeUpdate();
    }

    public DbAccessStatus deleteByLquDsRiga(long lquDsRiga) {
        return buildQuery("deleteByLquDsRiga").bind("lquDsRiga", lquDsRiga).executeDelete();
    }

    public ILiq selectRec(int lquIdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ILiq iLiq) {
        return buildQuery("selectRec").bind("lquIdLiq", lquIdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus updateRec1(ILiq iLiq) {
        return buildQuery("updateRec1").bind(iLiq).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffLqu(int lquIdLiq, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffLqu = buildQuery("openCIdUpdEffLqu").bind("lquIdLiq", lquIdLiq).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffLqu() {
        return closeCursor(cIdUpdEffLqu);
    }

    public ILiq fetchCIdUpdEffLqu(ILiq iLiq) {
        return fetch(cIdUpdEffLqu, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec1(String lquIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ILiq iLiq) {
        return buildQuery("selectRec1").bind("lquIbOgg", lquIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCIboEffLqu(String lquIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIboEffLqu = buildQuery("openCIboEffLqu").bind("lquIbOgg", lquIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboEffLqu() {
        return closeCursor(cIboEffLqu);
    }

    public ILiq fetchCIboEffLqu(ILiq iLiq) {
        return fetch(cIboEffLqu, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec2(String lquIbLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ILiq iLiq) {
        return buildQuery("selectRec2").bind("lquIbLiq", lquIbLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCIbsEffLqu0(String lquIbLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffLqu0 = buildQuery("openCIbsEffLqu0").bind("lquIbLiq", lquIbLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsEffLqu0() {
        return closeCursor(cIbsEffLqu0);
    }

    public ILiq fetchCIbsEffLqu0(ILiq iLiq) {
        return fetch(cIbsEffLqu0, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec3(int lquIdOgg, String lquTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ILiq iLiq) {
        return buildQuery("selectRec3").bind("lquIdOgg", lquIdOgg).bind("lquTpOgg", lquTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCIdoEffLqu(int lquIdOgg, String lquTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffLqu = buildQuery("openCIdoEffLqu").bind("lquIdOgg", lquIdOgg).bind("lquTpOgg", lquTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffLqu() {
        return closeCursor(cIdoEffLqu);
    }

    public ILiq fetchCIdoEffLqu(ILiq iLiq) {
        return fetch(cIdoEffLqu, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec4(int lquIdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ILiq iLiq) {
        return buildQuery("selectRec4").bind("lquIdLiq", lquIdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public ILiq selectRec5(String lquIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ILiq iLiq) {
        return buildQuery("selectRec5").bind("lquIbOgg", lquIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCIboCpzLqu(String lquIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIboCpzLqu = buildQuery("openCIboCpzLqu").bind("lquIbOgg", lquIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboCpzLqu() {
        return closeCursor(cIboCpzLqu);
    }

    public ILiq fetchCIboCpzLqu(ILiq iLiq) {
        return fetch(cIboCpzLqu, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec6(String lquIbLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ILiq iLiq) {
        return buildQuery("selectRec6").bind("lquIbLiq", lquIbLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCIbsCpzLqu0(String lquIbLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzLqu0 = buildQuery("openCIbsCpzLqu0").bind("lquIbLiq", lquIbLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsCpzLqu0() {
        return closeCursor(cIbsCpzLqu0);
    }

    public ILiq fetchCIbsCpzLqu0(ILiq iLiq) {
        return fetch(cIbsCpzLqu0, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec7(ILiq iLiq) {
        return buildQuery("selectRec7").bind(iLiq).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCIdoCpzLqu(ILiq iLiq) {
        cIdoCpzLqu = buildQuery("openCIdoCpzLqu").bind(iLiq).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzLqu() {
        return closeCursor(cIdoCpzLqu);
    }

    public ILiq fetchCIdoCpzLqu(ILiq iLiq) {
        return fetch(cIdoCpzLqu, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec8(ILiq iLiq) {
        return buildQuery("selectRec8").bind(iLiq).rowMapper(selectRec8Rm).singleResult(iLiq);
    }

    public ILiq selectRec9(ILiq iLiq) {
        return buildQuery("selectRec9").bind(iLiq).rowMapper(selectRec8Rm).singleResult(iLiq);
    }

    public ILiq selectRec10(ILiq iLiq) {
        return buildQuery("selectRec10").bind(iLiq).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCEff34(ILiq iLiq) {
        cEff34 = buildQuery("openCEff34").bind(iLiq).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff34() {
        return closeCursor(cEff34);
    }

    public ILiq fetchCEff34(ILiq iLiq) {
        return fetch(cEff34, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec11(ILiq iLiq) {
        return buildQuery("selectRec11").bind(iLiq).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCCpz34(ILiq iLiq) {
        cCpz34 = buildQuery("openCCpz34").bind(iLiq).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz34() {
        return closeCursor(cCpz34);
    }

    public ILiq fetchCCpz34(ILiq iLiq) {
        return fetch(cCpz34, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec12(int lquIdOgg, String lquTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ILiq iLiq) {
        return buildQuery("selectRec12").bind("lquIdOgg", lquIdOgg).bind("lquTpOgg", lquTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCEff40(int lquIdOgg, String lquTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff40 = buildQuery("openCEff40").bind("lquIdOgg", lquIdOgg).bind("lquTpOgg", lquTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff40() {
        return closeCursor(cEff40);
    }

    public ILiq fetchCEff40(ILiq iLiq) {
        return fetch(cEff40, iLiq, selectByLquDsRigaRm);
    }

    public ILiq selectRec13(ILiq iLiq) {
        return buildQuery("selectRec13").bind(iLiq).rowMapper(selectByLquDsRigaRm).singleResult(iLiq);
    }

    public DbAccessStatus openCCpz40(ILiq iLiq) {
        cCpz40 = buildQuery("openCCpz40").bind(iLiq).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz40() {
        return closeCursor(cCpz40);
    }

    public ILiq fetchCCpz40(ILiq iLiq) {
        return fetch(cCpz40, iLiq, selectByLquDsRigaRm);
    }
}
