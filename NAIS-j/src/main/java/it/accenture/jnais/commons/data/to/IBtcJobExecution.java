package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BTC_JOB_EXECUTION]
 * 
 */
public interface IBtcJobExecution extends BaseSqlTo {

    /**
     * Host Variable BJE-ID-BATCH
     * 
     */
    int getIdBatch();

    void setIdBatch(int idBatch);

    /**
     * Host Variable BJE-ID-JOB
     * 
     */
    int getIdJob();

    void setIdJob(int idJob);

    /**
     * Host Variable BJE-ID-EXECUTION
     * 
     */
    int getIdExecution();

    void setIdExecution(int idExecution);

    /**
     * Host Variable BJE-COD-ELAB-STATE
     * 
     */
    char getCodElabState();

    void setCodElabState(char codElabState);

    /**
     * Host Variable BJE-DATA-VCHAR
     * 
     */
    String getDataVchar();

    void setDataVchar(String dataVchar);

    /**
     * Nullable property for BJE-DATA-VCHAR
     * 
     */
    String getDataVcharObj();

    void setDataVcharObj(String dataVcharObj);

    /**
     * Host Variable BJE-FLAG-WARNINGS
     * 
     */
    char getFlagWarnings();

    void setFlagWarnings(char flagWarnings);

    /**
     * Nullable property for BJE-FLAG-WARNINGS
     * 
     */
    Character getFlagWarningsObj();

    void setFlagWarningsObj(Character flagWarningsObj);

    /**
     * Host Variable BJE-DT-START-DB
     * 
     */
    String getDtStartDb();

    void setDtStartDb(String dtStartDb);

    /**
     * Host Variable BJE-DT-END-DB
     * 
     */
    String getDtEndDb();

    void setDtEndDb(String dtEndDb);

    /**
     * Nullable property for BJE-DT-END-DB
     * 
     */
    String getDtEndDbObj();

    void setDtEndDbObj(String dtEndDbObj);

    /**
     * Host Variable BJE-USER-START
     * 
     */
    String getUserStart();

    void setUserStart(String userStart);
};
