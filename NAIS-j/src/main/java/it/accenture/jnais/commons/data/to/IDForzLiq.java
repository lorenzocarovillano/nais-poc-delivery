package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [D_FORZ_LIQ]
 * 
 */
public interface IDForzLiq extends BaseSqlTo {

    /**
     * Host Variable DFL-ID-D-FORZ-LIQ
     * 
     */
    int getIdDForzLiq();

    void setIdDForzLiq(int idDForzLiq);

    /**
     * Host Variable DFL-ID-LIQ
     * 
     */
    int getIdLiq();

    void setIdLiq(int idLiq);

    /**
     * Host Variable DFL-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable DFL-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for DFL-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable DFL-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable DFL-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable DFL-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable DFL-IMP-LRD-CALC
     * 
     */
    AfDecimal getImpLrdCalc();

    void setImpLrdCalc(AfDecimal impLrdCalc);

    /**
     * Nullable property for DFL-IMP-LRD-CALC
     * 
     */
    AfDecimal getImpLrdCalcObj();

    void setImpLrdCalcObj(AfDecimal impLrdCalcObj);

    /**
     * Host Variable DFL-IMP-LRD-DFZ
     * 
     */
    AfDecimal getImpLrdDfz();

    void setImpLrdDfz(AfDecimal impLrdDfz);

    /**
     * Nullable property for DFL-IMP-LRD-DFZ
     * 
     */
    AfDecimal getImpLrdDfzObj();

    void setImpLrdDfzObj(AfDecimal impLrdDfzObj);

    /**
     * Host Variable DFL-IMP-LRD-EFFLQ
     * 
     */
    AfDecimal getImpLrdEfflq();

    void setImpLrdEfflq(AfDecimal impLrdEfflq);

    /**
     * Nullable property for DFL-IMP-LRD-EFFLQ
     * 
     */
    AfDecimal getImpLrdEfflqObj();

    void setImpLrdEfflqObj(AfDecimal impLrdEfflqObj);

    /**
     * Host Variable DFL-IMP-NET-CALC
     * 
     */
    AfDecimal getImpNetCalc();

    void setImpNetCalc(AfDecimal impNetCalc);

    /**
     * Nullable property for DFL-IMP-NET-CALC
     * 
     */
    AfDecimal getImpNetCalcObj();

    void setImpNetCalcObj(AfDecimal impNetCalcObj);

    /**
     * Host Variable DFL-IMP-NET-DFZ
     * 
     */
    AfDecimal getImpNetDfz();

    void setImpNetDfz(AfDecimal impNetDfz);

    /**
     * Nullable property for DFL-IMP-NET-DFZ
     * 
     */
    AfDecimal getImpNetDfzObj();

    void setImpNetDfzObj(AfDecimal impNetDfzObj);

    /**
     * Host Variable DFL-IMP-NET-EFFLQ
     * 
     */
    AfDecimal getImpNetEfflq();

    void setImpNetEfflq(AfDecimal impNetEfflq);

    /**
     * Nullable property for DFL-IMP-NET-EFFLQ
     * 
     */
    AfDecimal getImpNetEfflqObj();

    void setImpNetEfflqObj(AfDecimal impNetEfflqObj);

    /**
     * Host Variable DFL-IMPST-PRVR-CALC
     * 
     */
    AfDecimal getImpstPrvrCalc();

    void setImpstPrvrCalc(AfDecimal impstPrvrCalc);

    /**
     * Nullable property for DFL-IMPST-PRVR-CALC
     * 
     */
    AfDecimal getImpstPrvrCalcObj();

    void setImpstPrvrCalcObj(AfDecimal impstPrvrCalcObj);

    /**
     * Host Variable DFL-IMPST-PRVR-DFZ
     * 
     */
    AfDecimal getImpstPrvrDfz();

    void setImpstPrvrDfz(AfDecimal impstPrvrDfz);

    /**
     * Nullable property for DFL-IMPST-PRVR-DFZ
     * 
     */
    AfDecimal getImpstPrvrDfzObj();

    void setImpstPrvrDfzObj(AfDecimal impstPrvrDfzObj);

    /**
     * Host Variable DFL-IMPST-PRVR-EFFLQ
     * 
     */
    AfDecimal getImpstPrvrEfflq();

    void setImpstPrvrEfflq(AfDecimal impstPrvrEfflq);

    /**
     * Nullable property for DFL-IMPST-PRVR-EFFLQ
     * 
     */
    AfDecimal getImpstPrvrEfflqObj();

    void setImpstPrvrEfflqObj(AfDecimal impstPrvrEfflqObj);

    /**
     * Host Variable DFL-IMPST-VIS-CALC
     * 
     */
    AfDecimal getImpstVisCalc();

    void setImpstVisCalc(AfDecimal impstVisCalc);

    /**
     * Nullable property for DFL-IMPST-VIS-CALC
     * 
     */
    AfDecimal getImpstVisCalcObj();

    void setImpstVisCalcObj(AfDecimal impstVisCalcObj);

    /**
     * Host Variable DFL-IMPST-VIS-DFZ
     * 
     */
    AfDecimal getImpstVisDfz();

    void setImpstVisDfz(AfDecimal impstVisDfz);

    /**
     * Nullable property for DFL-IMPST-VIS-DFZ
     * 
     */
    AfDecimal getImpstVisDfzObj();

    void setImpstVisDfzObj(AfDecimal impstVisDfzObj);

    /**
     * Host Variable DFL-IMPST-VIS-EFFLQ
     * 
     */
    AfDecimal getImpstVisEfflq();

    void setImpstVisEfflq(AfDecimal impstVisEfflq);

    /**
     * Nullable property for DFL-IMPST-VIS-EFFLQ
     * 
     */
    AfDecimal getImpstVisEfflqObj();

    void setImpstVisEfflqObj(AfDecimal impstVisEfflqObj);

    /**
     * Host Variable DFL-RIT-ACC-CALC
     * 
     */
    AfDecimal getRitAccCalc();

    void setRitAccCalc(AfDecimal ritAccCalc);

    /**
     * Nullable property for DFL-RIT-ACC-CALC
     * 
     */
    AfDecimal getRitAccCalcObj();

    void setRitAccCalcObj(AfDecimal ritAccCalcObj);

    /**
     * Host Variable DFL-RIT-ACC-DFZ
     * 
     */
    AfDecimal getRitAccDfz();

    void setRitAccDfz(AfDecimal ritAccDfz);

    /**
     * Nullable property for DFL-RIT-ACC-DFZ
     * 
     */
    AfDecimal getRitAccDfzObj();

    void setRitAccDfzObj(AfDecimal ritAccDfzObj);

    /**
     * Host Variable DFL-RIT-ACC-EFFLQ
     * 
     */
    AfDecimal getRitAccEfflq();

    void setRitAccEfflq(AfDecimal ritAccEfflq);

    /**
     * Nullable property for DFL-RIT-ACC-EFFLQ
     * 
     */
    AfDecimal getRitAccEfflqObj();

    void setRitAccEfflqObj(AfDecimal ritAccEfflqObj);

    /**
     * Host Variable DFL-RIT-IRPEF-CALC
     * 
     */
    AfDecimal getRitIrpefCalc();

    void setRitIrpefCalc(AfDecimal ritIrpefCalc);

    /**
     * Nullable property for DFL-RIT-IRPEF-CALC
     * 
     */
    AfDecimal getRitIrpefCalcObj();

    void setRitIrpefCalcObj(AfDecimal ritIrpefCalcObj);

    /**
     * Host Variable DFL-RIT-IRPEF-DFZ
     * 
     */
    AfDecimal getRitIrpefDfz();

    void setRitIrpefDfz(AfDecimal ritIrpefDfz);

    /**
     * Nullable property for DFL-RIT-IRPEF-DFZ
     * 
     */
    AfDecimal getRitIrpefDfzObj();

    void setRitIrpefDfzObj(AfDecimal ritIrpefDfzObj);

    /**
     * Host Variable DFL-RIT-IRPEF-EFFLQ
     * 
     */
    AfDecimal getRitIrpefEfflq();

    void setRitIrpefEfflq(AfDecimal ritIrpefEfflq);

    /**
     * Nullable property for DFL-RIT-IRPEF-EFFLQ
     * 
     */
    AfDecimal getRitIrpefEfflqObj();

    void setRitIrpefEfflqObj(AfDecimal ritIrpefEfflqObj);

    /**
     * Host Variable DFL-IMPST-SOST-CALC
     * 
     */
    AfDecimal getImpstSostCalc();

    void setImpstSostCalc(AfDecimal impstSostCalc);

    /**
     * Nullable property for DFL-IMPST-SOST-CALC
     * 
     */
    AfDecimal getImpstSostCalcObj();

    void setImpstSostCalcObj(AfDecimal impstSostCalcObj);

    /**
     * Host Variable DFL-IMPST-SOST-DFZ
     * 
     */
    AfDecimal getImpstSostDfz();

    void setImpstSostDfz(AfDecimal impstSostDfz);

    /**
     * Nullable property for DFL-IMPST-SOST-DFZ
     * 
     */
    AfDecimal getImpstSostDfzObj();

    void setImpstSostDfzObj(AfDecimal impstSostDfzObj);

    /**
     * Host Variable DFL-IMPST-SOST-EFFLQ
     * 
     */
    AfDecimal getImpstSostEfflq();

    void setImpstSostEfflq(AfDecimal impstSostEfflq);

    /**
     * Nullable property for DFL-IMPST-SOST-EFFLQ
     * 
     */
    AfDecimal getImpstSostEfflqObj();

    void setImpstSostEfflqObj(AfDecimal impstSostEfflqObj);

    /**
     * Host Variable DFL-TAX-SEP-CALC
     * 
     */
    AfDecimal getTaxSepCalc();

    void setTaxSepCalc(AfDecimal taxSepCalc);

    /**
     * Nullable property for DFL-TAX-SEP-CALC
     * 
     */
    AfDecimal getTaxSepCalcObj();

    void setTaxSepCalcObj(AfDecimal taxSepCalcObj);

    /**
     * Host Variable DFL-TAX-SEP-DFZ
     * 
     */
    AfDecimal getTaxSepDfz();

    void setTaxSepDfz(AfDecimal taxSepDfz);

    /**
     * Nullable property for DFL-TAX-SEP-DFZ
     * 
     */
    AfDecimal getTaxSepDfzObj();

    void setTaxSepDfzObj(AfDecimal taxSepDfzObj);

    /**
     * Host Variable DFL-TAX-SEP-EFFLQ
     * 
     */
    AfDecimal getTaxSepEfflq();

    void setTaxSepEfflq(AfDecimal taxSepEfflq);

    /**
     * Nullable property for DFL-TAX-SEP-EFFLQ
     * 
     */
    AfDecimal getTaxSepEfflqObj();

    void setTaxSepEfflqObj(AfDecimal taxSepEfflqObj);

    /**
     * Host Variable DFL-INTR-PREST-CALC
     * 
     */
    AfDecimal getIntrPrestCalc();

    void setIntrPrestCalc(AfDecimal intrPrestCalc);

    /**
     * Nullable property for DFL-INTR-PREST-CALC
     * 
     */
    AfDecimal getIntrPrestCalcObj();

    void setIntrPrestCalcObj(AfDecimal intrPrestCalcObj);

    /**
     * Host Variable DFL-INTR-PREST-DFZ
     * 
     */
    AfDecimal getIntrPrestDfz();

    void setIntrPrestDfz(AfDecimal intrPrestDfz);

    /**
     * Nullable property for DFL-INTR-PREST-DFZ
     * 
     */
    AfDecimal getIntrPrestDfzObj();

    void setIntrPrestDfzObj(AfDecimal intrPrestDfzObj);

    /**
     * Host Variable DFL-INTR-PREST-EFFLQ
     * 
     */
    AfDecimal getIntrPrestEfflq();

    void setIntrPrestEfflq(AfDecimal intrPrestEfflq);

    /**
     * Nullable property for DFL-INTR-PREST-EFFLQ
     * 
     */
    AfDecimal getIntrPrestEfflqObj();

    void setIntrPrestEfflqObj(AfDecimal intrPrestEfflqObj);

    /**
     * Host Variable DFL-ACCPRE-SOST-CALC
     * 
     */
    AfDecimal getAccpreSostCalc();

    void setAccpreSostCalc(AfDecimal accpreSostCalc);

    /**
     * Nullable property for DFL-ACCPRE-SOST-CALC
     * 
     */
    AfDecimal getAccpreSostCalcObj();

    void setAccpreSostCalcObj(AfDecimal accpreSostCalcObj);

    /**
     * Host Variable DFL-ACCPRE-SOST-DFZ
     * 
     */
    AfDecimal getAccpreSostDfz();

    void setAccpreSostDfz(AfDecimal accpreSostDfz);

    /**
     * Nullable property for DFL-ACCPRE-SOST-DFZ
     * 
     */
    AfDecimal getAccpreSostDfzObj();

    void setAccpreSostDfzObj(AfDecimal accpreSostDfzObj);

    /**
     * Host Variable DFL-ACCPRE-SOST-EFFLQ
     * 
     */
    AfDecimal getAccpreSostEfflq();

    void setAccpreSostEfflq(AfDecimal accpreSostEfflq);

    /**
     * Nullable property for DFL-ACCPRE-SOST-EFFLQ
     * 
     */
    AfDecimal getAccpreSostEfflqObj();

    void setAccpreSostEfflqObj(AfDecimal accpreSostEfflqObj);

    /**
     * Host Variable DFL-ACCPRE-VIS-CALC
     * 
     */
    AfDecimal getAccpreVisCalc();

    void setAccpreVisCalc(AfDecimal accpreVisCalc);

    /**
     * Nullable property for DFL-ACCPRE-VIS-CALC
     * 
     */
    AfDecimal getAccpreVisCalcObj();

    void setAccpreVisCalcObj(AfDecimal accpreVisCalcObj);

    /**
     * Host Variable DFL-ACCPRE-VIS-DFZ
     * 
     */
    AfDecimal getAccpreVisDfz();

    void setAccpreVisDfz(AfDecimal accpreVisDfz);

    /**
     * Nullable property for DFL-ACCPRE-VIS-DFZ
     * 
     */
    AfDecimal getAccpreVisDfzObj();

    void setAccpreVisDfzObj(AfDecimal accpreVisDfzObj);

    /**
     * Host Variable DFL-ACCPRE-VIS-EFFLQ
     * 
     */
    AfDecimal getAccpreVisEfflq();

    void setAccpreVisEfflq(AfDecimal accpreVisEfflq);

    /**
     * Nullable property for DFL-ACCPRE-VIS-EFFLQ
     * 
     */
    AfDecimal getAccpreVisEfflqObj();

    void setAccpreVisEfflqObj(AfDecimal accpreVisEfflqObj);

    /**
     * Host Variable DFL-ACCPRE-ACC-CALC
     * 
     */
    AfDecimal getAccpreAccCalc();

    void setAccpreAccCalc(AfDecimal accpreAccCalc);

    /**
     * Nullable property for DFL-ACCPRE-ACC-CALC
     * 
     */
    AfDecimal getAccpreAccCalcObj();

    void setAccpreAccCalcObj(AfDecimal accpreAccCalcObj);

    /**
     * Host Variable DFL-ACCPRE-ACC-DFZ
     * 
     */
    AfDecimal getAccpreAccDfz();

    void setAccpreAccDfz(AfDecimal accpreAccDfz);

    /**
     * Nullable property for DFL-ACCPRE-ACC-DFZ
     * 
     */
    AfDecimal getAccpreAccDfzObj();

    void setAccpreAccDfzObj(AfDecimal accpreAccDfzObj);

    /**
     * Host Variable DFL-ACCPRE-ACC-EFFLQ
     * 
     */
    AfDecimal getAccpreAccEfflq();

    void setAccpreAccEfflq(AfDecimal accpreAccEfflq);

    /**
     * Nullable property for DFL-ACCPRE-ACC-EFFLQ
     * 
     */
    AfDecimal getAccpreAccEfflqObj();

    void setAccpreAccEfflqObj(AfDecimal accpreAccEfflqObj);

    /**
     * Host Variable DFL-RES-PRSTZ-CALC
     * 
     */
    AfDecimal getResPrstzCalc();

    void setResPrstzCalc(AfDecimal resPrstzCalc);

    /**
     * Nullable property for DFL-RES-PRSTZ-CALC
     * 
     */
    AfDecimal getResPrstzCalcObj();

    void setResPrstzCalcObj(AfDecimal resPrstzCalcObj);

    /**
     * Host Variable DFL-RES-PRSTZ-DFZ
     * 
     */
    AfDecimal getResPrstzDfz();

    void setResPrstzDfz(AfDecimal resPrstzDfz);

    /**
     * Nullable property for DFL-RES-PRSTZ-DFZ
     * 
     */
    AfDecimal getResPrstzDfzObj();

    void setResPrstzDfzObj(AfDecimal resPrstzDfzObj);

    /**
     * Host Variable DFL-RES-PRSTZ-EFFLQ
     * 
     */
    AfDecimal getResPrstzEfflq();

    void setResPrstzEfflq(AfDecimal resPrstzEfflq);

    /**
     * Nullable property for DFL-RES-PRSTZ-EFFLQ
     * 
     */
    AfDecimal getResPrstzEfflqObj();

    void setResPrstzEfflqObj(AfDecimal resPrstzEfflqObj);

    /**
     * Host Variable DFL-RES-PRE-ATT-CALC
     * 
     */
    AfDecimal getResPreAttCalc();

    void setResPreAttCalc(AfDecimal resPreAttCalc);

    /**
     * Nullable property for DFL-RES-PRE-ATT-CALC
     * 
     */
    AfDecimal getResPreAttCalcObj();

    void setResPreAttCalcObj(AfDecimal resPreAttCalcObj);

    /**
     * Host Variable DFL-RES-PRE-ATT-DFZ
     * 
     */
    AfDecimal getResPreAttDfz();

    void setResPreAttDfz(AfDecimal resPreAttDfz);

    /**
     * Nullable property for DFL-RES-PRE-ATT-DFZ
     * 
     */
    AfDecimal getResPreAttDfzObj();

    void setResPreAttDfzObj(AfDecimal resPreAttDfzObj);

    /**
     * Host Variable DFL-RES-PRE-ATT-EFFLQ
     * 
     */
    AfDecimal getResPreAttEfflq();

    void setResPreAttEfflq(AfDecimal resPreAttEfflq);

    /**
     * Nullable property for DFL-RES-PRE-ATT-EFFLQ
     * 
     */
    AfDecimal getResPreAttEfflqObj();

    void setResPreAttEfflqObj(AfDecimal resPreAttEfflqObj);

    /**
     * Host Variable DFL-IMP-EXCONTR-EFF
     * 
     */
    AfDecimal getImpExcontrEff();

    void setImpExcontrEff(AfDecimal impExcontrEff);

    /**
     * Nullable property for DFL-IMP-EXCONTR-EFF
     * 
     */
    AfDecimal getImpExcontrEffObj();

    void setImpExcontrEffObj(AfDecimal impExcontrEffObj);

    /**
     * Host Variable DFL-IMPB-VIS-CALC
     * 
     */
    AfDecimal getImpbVisCalc();

    void setImpbVisCalc(AfDecimal impbVisCalc);

    /**
     * Nullable property for DFL-IMPB-VIS-CALC
     * 
     */
    AfDecimal getImpbVisCalcObj();

    void setImpbVisCalcObj(AfDecimal impbVisCalcObj);

    /**
     * Host Variable DFL-IMPB-VIS-EFFLQ
     * 
     */
    AfDecimal getImpbVisEfflq();

    void setImpbVisEfflq(AfDecimal impbVisEfflq);

    /**
     * Nullable property for DFL-IMPB-VIS-EFFLQ
     * 
     */
    AfDecimal getImpbVisEfflqObj();

    void setImpbVisEfflqObj(AfDecimal impbVisEfflqObj);

    /**
     * Host Variable DFL-IMPB-VIS-DFZ
     * 
     */
    AfDecimal getImpbVisDfz();

    void setImpbVisDfz(AfDecimal impbVisDfz);

    /**
     * Nullable property for DFL-IMPB-VIS-DFZ
     * 
     */
    AfDecimal getImpbVisDfzObj();

    void setImpbVisDfzObj(AfDecimal impbVisDfzObj);

    /**
     * Host Variable DFL-IMPB-RIT-ACC-CALC
     * 
     */
    AfDecimal getImpbRitAccCalc();

    void setImpbRitAccCalc(AfDecimal impbRitAccCalc);

    /**
     * Nullable property for DFL-IMPB-RIT-ACC-CALC
     * 
     */
    AfDecimal getImpbRitAccCalcObj();

    void setImpbRitAccCalcObj(AfDecimal impbRitAccCalcObj);

    /**
     * Host Variable DFL-IMPB-RIT-ACC-EFFLQ
     * 
     */
    AfDecimal getImpbRitAccEfflq();

    void setImpbRitAccEfflq(AfDecimal impbRitAccEfflq);

    /**
     * Nullable property for DFL-IMPB-RIT-ACC-EFFLQ
     * 
     */
    AfDecimal getImpbRitAccEfflqObj();

    void setImpbRitAccEfflqObj(AfDecimal impbRitAccEfflqObj);

    /**
     * Host Variable DFL-IMPB-RIT-ACC-DFZ
     * 
     */
    AfDecimal getImpbRitAccDfz();

    void setImpbRitAccDfz(AfDecimal impbRitAccDfz);

    /**
     * Nullable property for DFL-IMPB-RIT-ACC-DFZ
     * 
     */
    AfDecimal getImpbRitAccDfzObj();

    void setImpbRitAccDfzObj(AfDecimal impbRitAccDfzObj);

    /**
     * Host Variable DFL-IMPB-TFR-CALC
     * 
     */
    AfDecimal getImpbTfrCalc();

    void setImpbTfrCalc(AfDecimal impbTfrCalc);

    /**
     * Nullable property for DFL-IMPB-TFR-CALC
     * 
     */
    AfDecimal getImpbTfrCalcObj();

    void setImpbTfrCalcObj(AfDecimal impbTfrCalcObj);

    /**
     * Host Variable DFL-IMPB-TFR-EFFLQ
     * 
     */
    AfDecimal getImpbTfrEfflq();

    void setImpbTfrEfflq(AfDecimal impbTfrEfflq);

    /**
     * Nullable property for DFL-IMPB-TFR-EFFLQ
     * 
     */
    AfDecimal getImpbTfrEfflqObj();

    void setImpbTfrEfflqObj(AfDecimal impbTfrEfflqObj);

    /**
     * Host Variable DFL-IMPB-TFR-DFZ
     * 
     */
    AfDecimal getImpbTfrDfz();

    void setImpbTfrDfz(AfDecimal impbTfrDfz);

    /**
     * Nullable property for DFL-IMPB-TFR-DFZ
     * 
     */
    AfDecimal getImpbTfrDfzObj();

    void setImpbTfrDfzObj(AfDecimal impbTfrDfzObj);

    /**
     * Host Variable DFL-IMPB-IS-CALC
     * 
     */
    AfDecimal getImpbIsCalc();

    void setImpbIsCalc(AfDecimal impbIsCalc);

    /**
     * Nullable property for DFL-IMPB-IS-CALC
     * 
     */
    AfDecimal getImpbIsCalcObj();

    void setImpbIsCalcObj(AfDecimal impbIsCalcObj);

    /**
     * Host Variable DFL-IMPB-IS-EFFLQ
     * 
     */
    AfDecimal getImpbIsEfflq();

    void setImpbIsEfflq(AfDecimal impbIsEfflq);

    /**
     * Nullable property for DFL-IMPB-IS-EFFLQ
     * 
     */
    AfDecimal getImpbIsEfflqObj();

    void setImpbIsEfflqObj(AfDecimal impbIsEfflqObj);

    /**
     * Host Variable DFL-IMPB-IS-DFZ
     * 
     */
    AfDecimal getImpbIsDfz();

    void setImpbIsDfz(AfDecimal impbIsDfz);

    /**
     * Nullable property for DFL-IMPB-IS-DFZ
     * 
     */
    AfDecimal getImpbIsDfzObj();

    void setImpbIsDfzObj(AfDecimal impbIsDfzObj);

    /**
     * Host Variable DFL-IMPB-TAX-SEP-CALC
     * 
     */
    AfDecimal getImpbTaxSepCalc();

    void setImpbTaxSepCalc(AfDecimal impbTaxSepCalc);

    /**
     * Nullable property for DFL-IMPB-TAX-SEP-CALC
     * 
     */
    AfDecimal getImpbTaxSepCalcObj();

    void setImpbTaxSepCalcObj(AfDecimal impbTaxSepCalcObj);

    /**
     * Host Variable DFL-IMPB-TAX-SEP-EFFLQ
     * 
     */
    AfDecimal getImpbTaxSepEfflq();

    void setImpbTaxSepEfflq(AfDecimal impbTaxSepEfflq);

    /**
     * Nullable property for DFL-IMPB-TAX-SEP-EFFLQ
     * 
     */
    AfDecimal getImpbTaxSepEfflqObj();

    void setImpbTaxSepEfflqObj(AfDecimal impbTaxSepEfflqObj);

    /**
     * Host Variable DFL-IMPB-TAX-SEP-DFZ
     * 
     */
    AfDecimal getImpbTaxSepDfz();

    void setImpbTaxSepDfz(AfDecimal impbTaxSepDfz);

    /**
     * Nullable property for DFL-IMPB-TAX-SEP-DFZ
     * 
     */
    AfDecimal getImpbTaxSepDfzObj();

    void setImpbTaxSepDfzObj(AfDecimal impbTaxSepDfzObj);

    /**
     * Host Variable DFL-IINT-PREST-CALC
     * 
     */
    AfDecimal getIintPrestCalc();

    void setIintPrestCalc(AfDecimal iintPrestCalc);

    /**
     * Nullable property for DFL-IINT-PREST-CALC
     * 
     */
    AfDecimal getIintPrestCalcObj();

    void setIintPrestCalcObj(AfDecimal iintPrestCalcObj);

    /**
     * Host Variable DFL-IINT-PREST-EFFLQ
     * 
     */
    AfDecimal getIintPrestEfflq();

    void setIintPrestEfflq(AfDecimal iintPrestEfflq);

    /**
     * Nullable property for DFL-IINT-PREST-EFFLQ
     * 
     */
    AfDecimal getIintPrestEfflqObj();

    void setIintPrestEfflqObj(AfDecimal iintPrestEfflqObj);

    /**
     * Host Variable DFL-IINT-PREST-DFZ
     * 
     */
    AfDecimal getIintPrestDfz();

    void setIintPrestDfz(AfDecimal iintPrestDfz);

    /**
     * Nullable property for DFL-IINT-PREST-DFZ
     * 
     */
    AfDecimal getIintPrestDfzObj();

    void setIintPrestDfzObj(AfDecimal iintPrestDfzObj);

    /**
     * Host Variable DFL-MONT-END2000-CALC
     * 
     */
    AfDecimal getMontEnd2000Calc();

    void setMontEnd2000Calc(AfDecimal montEnd2000Calc);

    /**
     * Nullable property for DFL-MONT-END2000-CALC
     * 
     */
    AfDecimal getMontEnd2000CalcObj();

    void setMontEnd2000CalcObj(AfDecimal montEnd2000CalcObj);

    /**
     * Host Variable DFL-MONT-END2000-EFFLQ
     * 
     */
    AfDecimal getMontEnd2000Efflq();

    void setMontEnd2000Efflq(AfDecimal montEnd2000Efflq);

    /**
     * Nullable property for DFL-MONT-END2000-EFFLQ
     * 
     */
    AfDecimal getMontEnd2000EfflqObj();

    void setMontEnd2000EfflqObj(AfDecimal montEnd2000EfflqObj);

    /**
     * Host Variable DFL-MONT-END2000-DFZ
     * 
     */
    AfDecimal getMontEnd2000Dfz();

    void setMontEnd2000Dfz(AfDecimal montEnd2000Dfz);

    /**
     * Nullable property for DFL-MONT-END2000-DFZ
     * 
     */
    AfDecimal getMontEnd2000DfzObj();

    void setMontEnd2000DfzObj(AfDecimal montEnd2000DfzObj);

    /**
     * Host Variable DFL-MONT-END2006-CALC
     * 
     */
    AfDecimal getMontEnd2006Calc();

    void setMontEnd2006Calc(AfDecimal montEnd2006Calc);

    /**
     * Nullable property for DFL-MONT-END2006-CALC
     * 
     */
    AfDecimal getMontEnd2006CalcObj();

    void setMontEnd2006CalcObj(AfDecimal montEnd2006CalcObj);

    /**
     * Host Variable DFL-MONT-END2006-EFFLQ
     * 
     */
    AfDecimal getMontEnd2006Efflq();

    void setMontEnd2006Efflq(AfDecimal montEnd2006Efflq);

    /**
     * Nullable property for DFL-MONT-END2006-EFFLQ
     * 
     */
    AfDecimal getMontEnd2006EfflqObj();

    void setMontEnd2006EfflqObj(AfDecimal montEnd2006EfflqObj);

    /**
     * Host Variable DFL-MONT-END2006-DFZ
     * 
     */
    AfDecimal getMontEnd2006Dfz();

    void setMontEnd2006Dfz(AfDecimal montEnd2006Dfz);

    /**
     * Nullable property for DFL-MONT-END2006-DFZ
     * 
     */
    AfDecimal getMontEnd2006DfzObj();

    void setMontEnd2006DfzObj(AfDecimal montEnd2006DfzObj);

    /**
     * Host Variable DFL-MONT-DAL2007-CALC
     * 
     */
    AfDecimal getMontDal2007Calc();

    void setMontDal2007Calc(AfDecimal montDal2007Calc);

    /**
     * Nullable property for DFL-MONT-DAL2007-CALC
     * 
     */
    AfDecimal getMontDal2007CalcObj();

    void setMontDal2007CalcObj(AfDecimal montDal2007CalcObj);

    /**
     * Host Variable DFL-MONT-DAL2007-EFFLQ
     * 
     */
    AfDecimal getMontDal2007Efflq();

    void setMontDal2007Efflq(AfDecimal montDal2007Efflq);

    /**
     * Nullable property for DFL-MONT-DAL2007-EFFLQ
     * 
     */
    AfDecimal getMontDal2007EfflqObj();

    void setMontDal2007EfflqObj(AfDecimal montDal2007EfflqObj);

    /**
     * Host Variable DFL-MONT-DAL2007-DFZ
     * 
     */
    AfDecimal getMontDal2007Dfz();

    void setMontDal2007Dfz(AfDecimal montDal2007Dfz);

    /**
     * Nullable property for DFL-MONT-DAL2007-DFZ
     * 
     */
    AfDecimal getMontDal2007DfzObj();

    void setMontDal2007DfzObj(AfDecimal montDal2007DfzObj);

    /**
     * Host Variable DFL-IIMPST-PRVR-CALC
     * 
     */
    AfDecimal getIimpstPrvrCalc();

    void setIimpstPrvrCalc(AfDecimal iimpstPrvrCalc);

    /**
     * Nullable property for DFL-IIMPST-PRVR-CALC
     * 
     */
    AfDecimal getIimpstPrvrCalcObj();

    void setIimpstPrvrCalcObj(AfDecimal iimpstPrvrCalcObj);

    /**
     * Host Variable DFL-IIMPST-PRVR-EFFLQ
     * 
     */
    AfDecimal getIimpstPrvrEfflq();

    void setIimpstPrvrEfflq(AfDecimal iimpstPrvrEfflq);

    /**
     * Nullable property for DFL-IIMPST-PRVR-EFFLQ
     * 
     */
    AfDecimal getIimpstPrvrEfflqObj();

    void setIimpstPrvrEfflqObj(AfDecimal iimpstPrvrEfflqObj);

    /**
     * Host Variable DFL-IIMPST-PRVR-DFZ
     * 
     */
    AfDecimal getIimpstPrvrDfz();

    void setIimpstPrvrDfz(AfDecimal iimpstPrvrDfz);

    /**
     * Nullable property for DFL-IIMPST-PRVR-DFZ
     * 
     */
    AfDecimal getIimpstPrvrDfzObj();

    void setIimpstPrvrDfzObj(AfDecimal iimpstPrvrDfzObj);

    /**
     * Host Variable DFL-IIMPST-252-CALC
     * 
     */
    AfDecimal getIimpst252Calc();

    void setIimpst252Calc(AfDecimal iimpst252Calc);

    /**
     * Nullable property for DFL-IIMPST-252-CALC
     * 
     */
    AfDecimal getIimpst252CalcObj();

    void setIimpst252CalcObj(AfDecimal iimpst252CalcObj);

    /**
     * Host Variable DFL-IIMPST-252-EFFLQ
     * 
     */
    AfDecimal getIimpst252Efflq();

    void setIimpst252Efflq(AfDecimal iimpst252Efflq);

    /**
     * Nullable property for DFL-IIMPST-252-EFFLQ
     * 
     */
    AfDecimal getIimpst252EfflqObj();

    void setIimpst252EfflqObj(AfDecimal iimpst252EfflqObj);

    /**
     * Host Variable DFL-IIMPST-252-DFZ
     * 
     */
    AfDecimal getIimpst252Dfz();

    void setIimpst252Dfz(AfDecimal iimpst252Dfz);

    /**
     * Nullable property for DFL-IIMPST-252-DFZ
     * 
     */
    AfDecimal getIimpst252DfzObj();

    void setIimpst252DfzObj(AfDecimal iimpst252DfzObj);

    /**
     * Host Variable DFL-IMPST-252-CALC
     * 
     */
    AfDecimal getImpst252Calc();

    void setImpst252Calc(AfDecimal impst252Calc);

    /**
     * Nullable property for DFL-IMPST-252-CALC
     * 
     */
    AfDecimal getImpst252CalcObj();

    void setImpst252CalcObj(AfDecimal impst252CalcObj);

    /**
     * Host Variable DFL-IMPST-252-EFFLQ
     * 
     */
    AfDecimal getImpst252Efflq();

    void setImpst252Efflq(AfDecimal impst252Efflq);

    /**
     * Nullable property for DFL-IMPST-252-EFFLQ
     * 
     */
    AfDecimal getImpst252EfflqObj();

    void setImpst252EfflqObj(AfDecimal impst252EfflqObj);

    /**
     * Host Variable DFL-RIT-TFR-CALC
     * 
     */
    AfDecimal getRitTfrCalc();

    void setRitTfrCalc(AfDecimal ritTfrCalc);

    /**
     * Nullable property for DFL-RIT-TFR-CALC
     * 
     */
    AfDecimal getRitTfrCalcObj();

    void setRitTfrCalcObj(AfDecimal ritTfrCalcObj);

    /**
     * Host Variable DFL-RIT-TFR-EFFLQ
     * 
     */
    AfDecimal getRitTfrEfflq();

    void setRitTfrEfflq(AfDecimal ritTfrEfflq);

    /**
     * Nullable property for DFL-RIT-TFR-EFFLQ
     * 
     */
    AfDecimal getRitTfrEfflqObj();

    void setRitTfrEfflqObj(AfDecimal ritTfrEfflqObj);

    /**
     * Host Variable DFL-RIT-TFR-DFZ
     * 
     */
    AfDecimal getRitTfrDfz();

    void setRitTfrDfz(AfDecimal ritTfrDfz);

    /**
     * Nullable property for DFL-RIT-TFR-DFZ
     * 
     */
    AfDecimal getRitTfrDfzObj();

    void setRitTfrDfzObj(AfDecimal ritTfrDfzObj);

    /**
     * Host Variable DFL-CNBT-INPSTFM-CALC
     * 
     */
    AfDecimal getCnbtInpstfmCalc();

    void setCnbtInpstfmCalc(AfDecimal cnbtInpstfmCalc);

    /**
     * Nullable property for DFL-CNBT-INPSTFM-CALC
     * 
     */
    AfDecimal getCnbtInpstfmCalcObj();

    void setCnbtInpstfmCalcObj(AfDecimal cnbtInpstfmCalcObj);

    /**
     * Host Variable DFL-CNBT-INPSTFM-EFFLQ
     * 
     */
    AfDecimal getCnbtInpstfmEfflq();

    void setCnbtInpstfmEfflq(AfDecimal cnbtInpstfmEfflq);

    /**
     * Nullable property for DFL-CNBT-INPSTFM-EFFLQ
     * 
     */
    AfDecimal getCnbtInpstfmEfflqObj();

    void setCnbtInpstfmEfflqObj(AfDecimal cnbtInpstfmEfflqObj);

    /**
     * Host Variable DFL-CNBT-INPSTFM-DFZ
     * 
     */
    AfDecimal getCnbtInpstfmDfz();

    void setCnbtInpstfmDfz(AfDecimal cnbtInpstfmDfz);

    /**
     * Nullable property for DFL-CNBT-INPSTFM-DFZ
     * 
     */
    AfDecimal getCnbtInpstfmDfzObj();

    void setCnbtInpstfmDfzObj(AfDecimal cnbtInpstfmDfzObj);

    /**
     * Host Variable DFL-ICNB-INPSTFM-CALC
     * 
     */
    AfDecimal getIcnbInpstfmCalc();

    void setIcnbInpstfmCalc(AfDecimal icnbInpstfmCalc);

    /**
     * Nullable property for DFL-ICNB-INPSTFM-CALC
     * 
     */
    AfDecimal getIcnbInpstfmCalcObj();

    void setIcnbInpstfmCalcObj(AfDecimal icnbInpstfmCalcObj);

    /**
     * Host Variable DFL-ICNB-INPSTFM-EFFLQ
     * 
     */
    AfDecimal getIcnbInpstfmEfflq();

    void setIcnbInpstfmEfflq(AfDecimal icnbInpstfmEfflq);

    /**
     * Nullable property for DFL-ICNB-INPSTFM-EFFLQ
     * 
     */
    AfDecimal getIcnbInpstfmEfflqObj();

    void setIcnbInpstfmEfflqObj(AfDecimal icnbInpstfmEfflqObj);

    /**
     * Host Variable DFL-ICNB-INPSTFM-DFZ
     * 
     */
    AfDecimal getIcnbInpstfmDfz();

    void setIcnbInpstfmDfz(AfDecimal icnbInpstfmDfz);

    /**
     * Nullable property for DFL-ICNB-INPSTFM-DFZ
     * 
     */
    AfDecimal getIcnbInpstfmDfzObj();

    void setIcnbInpstfmDfzObj(AfDecimal icnbInpstfmDfzObj);

    /**
     * Host Variable DFL-CNDE-END2000-CALC
     * 
     */
    AfDecimal getCndeEnd2000Calc();

    void setCndeEnd2000Calc(AfDecimal cndeEnd2000Calc);

    /**
     * Nullable property for DFL-CNDE-END2000-CALC
     * 
     */
    AfDecimal getCndeEnd2000CalcObj();

    void setCndeEnd2000CalcObj(AfDecimal cndeEnd2000CalcObj);

    /**
     * Host Variable DFL-CNDE-END2000-EFFLQ
     * 
     */
    AfDecimal getCndeEnd2000Efflq();

    void setCndeEnd2000Efflq(AfDecimal cndeEnd2000Efflq);

    /**
     * Nullable property for DFL-CNDE-END2000-EFFLQ
     * 
     */
    AfDecimal getCndeEnd2000EfflqObj();

    void setCndeEnd2000EfflqObj(AfDecimal cndeEnd2000EfflqObj);

    /**
     * Host Variable DFL-CNDE-END2000-DFZ
     * 
     */
    AfDecimal getCndeEnd2000Dfz();

    void setCndeEnd2000Dfz(AfDecimal cndeEnd2000Dfz);

    /**
     * Nullable property for DFL-CNDE-END2000-DFZ
     * 
     */
    AfDecimal getCndeEnd2000DfzObj();

    void setCndeEnd2000DfzObj(AfDecimal cndeEnd2000DfzObj);

    /**
     * Host Variable DFL-CNDE-END2006-CALC
     * 
     */
    AfDecimal getCndeEnd2006Calc();

    void setCndeEnd2006Calc(AfDecimal cndeEnd2006Calc);

    /**
     * Nullable property for DFL-CNDE-END2006-CALC
     * 
     */
    AfDecimal getCndeEnd2006CalcObj();

    void setCndeEnd2006CalcObj(AfDecimal cndeEnd2006CalcObj);

    /**
     * Host Variable DFL-CNDE-END2006-EFFLQ
     * 
     */
    AfDecimal getCndeEnd2006Efflq();

    void setCndeEnd2006Efflq(AfDecimal cndeEnd2006Efflq);

    /**
     * Nullable property for DFL-CNDE-END2006-EFFLQ
     * 
     */
    AfDecimal getCndeEnd2006EfflqObj();

    void setCndeEnd2006EfflqObj(AfDecimal cndeEnd2006EfflqObj);

    /**
     * Host Variable DFL-CNDE-END2006-DFZ
     * 
     */
    AfDecimal getCndeEnd2006Dfz();

    void setCndeEnd2006Dfz(AfDecimal cndeEnd2006Dfz);

    /**
     * Nullable property for DFL-CNDE-END2006-DFZ
     * 
     */
    AfDecimal getCndeEnd2006DfzObj();

    void setCndeEnd2006DfzObj(AfDecimal cndeEnd2006DfzObj);

    /**
     * Host Variable DFL-CNDE-DAL2007-CALC
     * 
     */
    AfDecimal getCndeDal2007Calc();

    void setCndeDal2007Calc(AfDecimal cndeDal2007Calc);

    /**
     * Nullable property for DFL-CNDE-DAL2007-CALC
     * 
     */
    AfDecimal getCndeDal2007CalcObj();

    void setCndeDal2007CalcObj(AfDecimal cndeDal2007CalcObj);

    /**
     * Host Variable DFL-CNDE-DAL2007-EFFLQ
     * 
     */
    AfDecimal getCndeDal2007Efflq();

    void setCndeDal2007Efflq(AfDecimal cndeDal2007Efflq);

    /**
     * Nullable property for DFL-CNDE-DAL2007-EFFLQ
     * 
     */
    AfDecimal getCndeDal2007EfflqObj();

    void setCndeDal2007EfflqObj(AfDecimal cndeDal2007EfflqObj);

    /**
     * Host Variable DFL-CNDE-DAL2007-DFZ
     * 
     */
    AfDecimal getCndeDal2007Dfz();

    void setCndeDal2007Dfz(AfDecimal cndeDal2007Dfz);

    /**
     * Nullable property for DFL-CNDE-DAL2007-DFZ
     * 
     */
    AfDecimal getCndeDal2007DfzObj();

    void setCndeDal2007DfzObj(AfDecimal cndeDal2007DfzObj);

    /**
     * Host Variable DFL-AA-CNBZ-END2000-EF
     * 
     */
    int getAaCnbzEnd2000Ef();

    void setAaCnbzEnd2000Ef(int aaCnbzEnd2000Ef);

    /**
     * Nullable property for DFL-AA-CNBZ-END2000-EF
     * 
     */
    Integer getAaCnbzEnd2000EfObj();

    void setAaCnbzEnd2000EfObj(Integer aaCnbzEnd2000EfObj);

    /**
     * Host Variable DFL-AA-CNBZ-END2006-EF
     * 
     */
    int getAaCnbzEnd2006Ef();

    void setAaCnbzEnd2006Ef(int aaCnbzEnd2006Ef);

    /**
     * Nullable property for DFL-AA-CNBZ-END2006-EF
     * 
     */
    Integer getAaCnbzEnd2006EfObj();

    void setAaCnbzEnd2006EfObj(Integer aaCnbzEnd2006EfObj);

    /**
     * Host Variable DFL-AA-CNBZ-DAL2007-EF
     * 
     */
    int getAaCnbzDal2007Ef();

    void setAaCnbzDal2007Ef(int aaCnbzDal2007Ef);

    /**
     * Nullable property for DFL-AA-CNBZ-DAL2007-EF
     * 
     */
    Integer getAaCnbzDal2007EfObj();

    void setAaCnbzDal2007EfObj(Integer aaCnbzDal2007EfObj);

    /**
     * Host Variable DFL-MM-CNBZ-END2000-EF
     * 
     */
    int getMmCnbzEnd2000Ef();

    void setMmCnbzEnd2000Ef(int mmCnbzEnd2000Ef);

    /**
     * Nullable property for DFL-MM-CNBZ-END2000-EF
     * 
     */
    Integer getMmCnbzEnd2000EfObj();

    void setMmCnbzEnd2000EfObj(Integer mmCnbzEnd2000EfObj);

    /**
     * Host Variable DFL-MM-CNBZ-END2006-EF
     * 
     */
    int getMmCnbzEnd2006Ef();

    void setMmCnbzEnd2006Ef(int mmCnbzEnd2006Ef);

    /**
     * Nullable property for DFL-MM-CNBZ-END2006-EF
     * 
     */
    Integer getMmCnbzEnd2006EfObj();

    void setMmCnbzEnd2006EfObj(Integer mmCnbzEnd2006EfObj);

    /**
     * Host Variable DFL-MM-CNBZ-DAL2007-EF
     * 
     */
    int getMmCnbzDal2007Ef();

    void setMmCnbzDal2007Ef(int mmCnbzDal2007Ef);

    /**
     * Nullable property for DFL-MM-CNBZ-DAL2007-EF
     * 
     */
    Integer getMmCnbzDal2007EfObj();

    void setMmCnbzDal2007EfObj(Integer mmCnbzDal2007EfObj);

    /**
     * Host Variable DFL-IMPST-DA-RIMB-EFF
     * 
     */
    AfDecimal getImpstDaRimbEff();

    void setImpstDaRimbEff(AfDecimal impstDaRimbEff);

    /**
     * Nullable property for DFL-IMPST-DA-RIMB-EFF
     * 
     */
    AfDecimal getImpstDaRimbEffObj();

    void setImpstDaRimbEffObj(AfDecimal impstDaRimbEffObj);

    /**
     * Host Variable DFL-ALQ-TAX-SEP-CALC
     * 
     */
    AfDecimal getAlqTaxSepCalc();

    void setAlqTaxSepCalc(AfDecimal alqTaxSepCalc);

    /**
     * Nullable property for DFL-ALQ-TAX-SEP-CALC
     * 
     */
    AfDecimal getAlqTaxSepCalcObj();

    void setAlqTaxSepCalcObj(AfDecimal alqTaxSepCalcObj);

    /**
     * Host Variable DFL-ALQ-TAX-SEP-EFFLQ
     * 
     */
    AfDecimal getAlqTaxSepEfflq();

    void setAlqTaxSepEfflq(AfDecimal alqTaxSepEfflq);

    /**
     * Nullable property for DFL-ALQ-TAX-SEP-EFFLQ
     * 
     */
    AfDecimal getAlqTaxSepEfflqObj();

    void setAlqTaxSepEfflqObj(AfDecimal alqTaxSepEfflqObj);

    /**
     * Host Variable DFL-ALQ-TAX-SEP-DFZ
     * 
     */
    AfDecimal getAlqTaxSepDfz();

    void setAlqTaxSepDfz(AfDecimal alqTaxSepDfz);

    /**
     * Nullable property for DFL-ALQ-TAX-SEP-DFZ
     * 
     */
    AfDecimal getAlqTaxSepDfzObj();

    void setAlqTaxSepDfzObj(AfDecimal alqTaxSepDfzObj);

    /**
     * Host Variable DFL-ALQ-CNBT-INPSTFM-C
     * 
     */
    AfDecimal getAlqCnbtInpstfmC();

    void setAlqCnbtInpstfmC(AfDecimal alqCnbtInpstfmC);

    /**
     * Nullable property for DFL-ALQ-CNBT-INPSTFM-C
     * 
     */
    AfDecimal getAlqCnbtInpstfmCObj();

    void setAlqCnbtInpstfmCObj(AfDecimal alqCnbtInpstfmCObj);

    /**
     * Host Variable DFL-ALQ-CNBT-INPSTFM-E
     * 
     */
    AfDecimal getAlqCnbtInpstfmE();

    void setAlqCnbtInpstfmE(AfDecimal alqCnbtInpstfmE);

    /**
     * Nullable property for DFL-ALQ-CNBT-INPSTFM-E
     * 
     */
    AfDecimal getAlqCnbtInpstfmEObj();

    void setAlqCnbtInpstfmEObj(AfDecimal alqCnbtInpstfmEObj);

    /**
     * Host Variable DFL-ALQ-CNBT-INPSTFM-D
     * 
     */
    AfDecimal getAlqCnbtInpstfmD();

    void setAlqCnbtInpstfmD(AfDecimal alqCnbtInpstfmD);

    /**
     * Nullable property for DFL-ALQ-CNBT-INPSTFM-D
     * 
     */
    AfDecimal getAlqCnbtInpstfmDObj();

    void setAlqCnbtInpstfmDObj(AfDecimal alqCnbtInpstfmDObj);

    /**
     * Host Variable DFL-DS-RIGA
     * 
     */
    long getDflDsRiga();

    void setDflDsRiga(long dflDsRiga);

    /**
     * Host Variable DFL-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable DFL-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable DFL-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable DFL-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable DFL-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable DFL-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable DFL-IMPB-VIS-1382011C
     * 
     */
    AfDecimal getImpbVis1382011c();

    void setImpbVis1382011c(AfDecimal impbVis1382011c);

    /**
     * Nullable property for DFL-IMPB-VIS-1382011C
     * 
     */
    AfDecimal getImpbVis1382011cObj();

    void setImpbVis1382011cObj(AfDecimal impbVis1382011cObj);

    /**
     * Host Variable DFL-IMPB-VIS-1382011D
     * 
     */
    AfDecimal getImpbVis1382011d();

    void setImpbVis1382011d(AfDecimal impbVis1382011d);

    /**
     * Nullable property for DFL-IMPB-VIS-1382011D
     * 
     */
    AfDecimal getImpbVis1382011dObj();

    void setImpbVis1382011dObj(AfDecimal impbVis1382011dObj);

    /**
     * Host Variable DFL-IMPB-VIS-1382011L
     * 
     */
    AfDecimal getImpbVis1382011l();

    void setImpbVis1382011l(AfDecimal impbVis1382011l);

    /**
     * Nullable property for DFL-IMPB-VIS-1382011L
     * 
     */
    AfDecimal getImpbVis1382011lObj();

    void setImpbVis1382011lObj(AfDecimal impbVis1382011lObj);

    /**
     * Host Variable DFL-IMPST-VIS-1382011C
     * 
     */
    AfDecimal getImpstVis1382011c();

    void setImpstVis1382011c(AfDecimal impstVis1382011c);

    /**
     * Nullable property for DFL-IMPST-VIS-1382011C
     * 
     */
    AfDecimal getImpstVis1382011cObj();

    void setImpstVis1382011cObj(AfDecimal impstVis1382011cObj);

    /**
     * Host Variable DFL-IMPST-VIS-1382011D
     * 
     */
    AfDecimal getImpstVis1382011d();

    void setImpstVis1382011d(AfDecimal impstVis1382011d);

    /**
     * Nullable property for DFL-IMPST-VIS-1382011D
     * 
     */
    AfDecimal getImpstVis1382011dObj();

    void setImpstVis1382011dObj(AfDecimal impstVis1382011dObj);

    /**
     * Host Variable DFL-IMPST-VIS-1382011L
     * 
     */
    AfDecimal getImpstVis1382011l();

    void setImpstVis1382011l(AfDecimal impstVis1382011l);

    /**
     * Nullable property for DFL-IMPST-VIS-1382011L
     * 
     */
    AfDecimal getImpstVis1382011lObj();

    void setImpstVis1382011lObj(AfDecimal impstVis1382011lObj);

    /**
     * Host Variable DFL-IMPB-IS-1382011C
     * 
     */
    AfDecimal getImpbIs1382011c();

    void setImpbIs1382011c(AfDecimal impbIs1382011c);

    /**
     * Nullable property for DFL-IMPB-IS-1382011C
     * 
     */
    AfDecimal getImpbIs1382011cObj();

    void setImpbIs1382011cObj(AfDecimal impbIs1382011cObj);

    /**
     * Host Variable DFL-IMPB-IS-1382011D
     * 
     */
    AfDecimal getImpbIs1382011d();

    void setImpbIs1382011d(AfDecimal impbIs1382011d);

    /**
     * Nullable property for DFL-IMPB-IS-1382011D
     * 
     */
    AfDecimal getImpbIs1382011dObj();

    void setImpbIs1382011dObj(AfDecimal impbIs1382011dObj);

    /**
     * Host Variable DFL-IMPB-IS-1382011L
     * 
     */
    AfDecimal getImpbIs1382011l();

    void setImpbIs1382011l(AfDecimal impbIs1382011l);

    /**
     * Nullable property for DFL-IMPB-IS-1382011L
     * 
     */
    AfDecimal getImpbIs1382011lObj();

    void setImpbIs1382011lObj(AfDecimal impbIs1382011lObj);

    /**
     * Host Variable DFL-IS-1382011C
     * 
     */
    AfDecimal getIs1382011c();

    void setIs1382011c(AfDecimal is1382011c);

    /**
     * Nullable property for DFL-IS-1382011C
     * 
     */
    AfDecimal getIs1382011cObj();

    void setIs1382011cObj(AfDecimal is1382011cObj);

    /**
     * Host Variable DFL-IS-1382011D
     * 
     */
    AfDecimal getIs1382011d();

    void setIs1382011d(AfDecimal is1382011d);

    /**
     * Nullable property for DFL-IS-1382011D
     * 
     */
    AfDecimal getIs1382011dObj();

    void setIs1382011dObj(AfDecimal is1382011dObj);

    /**
     * Host Variable DFL-IS-1382011L
     * 
     */
    AfDecimal getIs1382011l();

    void setIs1382011l(AfDecimal is1382011l);

    /**
     * Nullable property for DFL-IS-1382011L
     * 
     */
    AfDecimal getIs1382011lObj();

    void setIs1382011lObj(AfDecimal is1382011lObj);

    /**
     * Host Variable DFL-IMP-INTR-RIT-PAG-C
     * 
     */
    AfDecimal getImpIntrRitPagC();

    void setImpIntrRitPagC(AfDecimal impIntrRitPagC);

    /**
     * Nullable property for DFL-IMP-INTR-RIT-PAG-C
     * 
     */
    AfDecimal getImpIntrRitPagCObj();

    void setImpIntrRitPagCObj(AfDecimal impIntrRitPagCObj);

    /**
     * Host Variable DFL-IMP-INTR-RIT-PAG-D
     * 
     */
    AfDecimal getImpIntrRitPagD();

    void setImpIntrRitPagD(AfDecimal impIntrRitPagD);

    /**
     * Nullable property for DFL-IMP-INTR-RIT-PAG-D
     * 
     */
    AfDecimal getImpIntrRitPagDObj();

    void setImpIntrRitPagDObj(AfDecimal impIntrRitPagDObj);

    /**
     * Host Variable DFL-IMP-INTR-RIT-PAG-L
     * 
     */
    AfDecimal getImpIntrRitPagL();

    void setImpIntrRitPagL(AfDecimal impIntrRitPagL);

    /**
     * Nullable property for DFL-IMP-INTR-RIT-PAG-L
     * 
     */
    AfDecimal getImpIntrRitPagLObj();

    void setImpIntrRitPagLObj(AfDecimal impIntrRitPagLObj);

    /**
     * Host Variable DFL-IMPB-BOLLO-DETT-C
     * 
     */
    AfDecimal getImpbBolloDettC();

    void setImpbBolloDettC(AfDecimal impbBolloDettC);

    /**
     * Nullable property for DFL-IMPB-BOLLO-DETT-C
     * 
     */
    AfDecimal getImpbBolloDettCObj();

    void setImpbBolloDettCObj(AfDecimal impbBolloDettCObj);

    /**
     * Host Variable DFL-IMPB-BOLLO-DETT-D
     * 
     */
    AfDecimal getImpbBolloDettD();

    void setImpbBolloDettD(AfDecimal impbBolloDettD);

    /**
     * Nullable property for DFL-IMPB-BOLLO-DETT-D
     * 
     */
    AfDecimal getImpbBolloDettDObj();

    void setImpbBolloDettDObj(AfDecimal impbBolloDettDObj);

    /**
     * Host Variable DFL-IMPB-BOLLO-DETT-L
     * 
     */
    AfDecimal getImpbBolloDettL();

    void setImpbBolloDettL(AfDecimal impbBolloDettL);

    /**
     * Nullable property for DFL-IMPB-BOLLO-DETT-L
     * 
     */
    AfDecimal getImpbBolloDettLObj();

    void setImpbBolloDettLObj(AfDecimal impbBolloDettLObj);

    /**
     * Host Variable DFL-IMPST-BOLLO-DETT-C
     * 
     */
    AfDecimal getImpstBolloDettC();

    void setImpstBolloDettC(AfDecimal impstBolloDettC);

    /**
     * Nullable property for DFL-IMPST-BOLLO-DETT-C
     * 
     */
    AfDecimal getImpstBolloDettCObj();

    void setImpstBolloDettCObj(AfDecimal impstBolloDettCObj);

    /**
     * Host Variable DFL-IMPST-BOLLO-DETT-D
     * 
     */
    AfDecimal getImpstBolloDettD();

    void setImpstBolloDettD(AfDecimal impstBolloDettD);

    /**
     * Nullable property for DFL-IMPST-BOLLO-DETT-D
     * 
     */
    AfDecimal getImpstBolloDettDObj();

    void setImpstBolloDettDObj(AfDecimal impstBolloDettDObj);

    /**
     * Host Variable DFL-IMPST-BOLLO-DETT-L
     * 
     */
    AfDecimal getImpstBolloDettL();

    void setImpstBolloDettL(AfDecimal impstBolloDettL);

    /**
     * Nullable property for DFL-IMPST-BOLLO-DETT-L
     * 
     */
    AfDecimal getImpstBolloDettLObj();

    void setImpstBolloDettLObj(AfDecimal impstBolloDettLObj);

    /**
     * Host Variable DFL-IMPST-BOLLO-TOT-VC
     * 
     */
    AfDecimal getImpstBolloTotVc();

    void setImpstBolloTotVc(AfDecimal impstBolloTotVc);

    /**
     * Nullable property for DFL-IMPST-BOLLO-TOT-VC
     * 
     */
    AfDecimal getImpstBolloTotVcObj();

    void setImpstBolloTotVcObj(AfDecimal impstBolloTotVcObj);

    /**
     * Host Variable DFL-IMPST-BOLLO-TOT-VD
     * 
     */
    AfDecimal getImpstBolloTotVd();

    void setImpstBolloTotVd(AfDecimal impstBolloTotVd);

    /**
     * Nullable property for DFL-IMPST-BOLLO-TOT-VD
     * 
     */
    AfDecimal getImpstBolloTotVdObj();

    void setImpstBolloTotVdObj(AfDecimal impstBolloTotVdObj);

    /**
     * Host Variable DFL-IMPST-BOLLO-TOT-VL
     * 
     */
    AfDecimal getImpstBolloTotVl();

    void setImpstBolloTotVl(AfDecimal impstBolloTotVl);

    /**
     * Nullable property for DFL-IMPST-BOLLO-TOT-VL
     * 
     */
    AfDecimal getImpstBolloTotVlObj();

    void setImpstBolloTotVlObj(AfDecimal impstBolloTotVlObj);

    /**
     * Host Variable DFL-IMPB-VIS-662014C
     * 
     */
    AfDecimal getImpbVis662014c();

    void setImpbVis662014c(AfDecimal impbVis662014c);

    /**
     * Nullable property for DFL-IMPB-VIS-662014C
     * 
     */
    AfDecimal getImpbVis662014cObj();

    void setImpbVis662014cObj(AfDecimal impbVis662014cObj);

    /**
     * Host Variable DFL-IMPB-VIS-662014D
     * 
     */
    AfDecimal getImpbVis662014d();

    void setImpbVis662014d(AfDecimal impbVis662014d);

    /**
     * Nullable property for DFL-IMPB-VIS-662014D
     * 
     */
    AfDecimal getImpbVis662014dObj();

    void setImpbVis662014dObj(AfDecimal impbVis662014dObj);

    /**
     * Host Variable DFL-IMPB-VIS-662014L
     * 
     */
    AfDecimal getImpbVis662014l();

    void setImpbVis662014l(AfDecimal impbVis662014l);

    /**
     * Nullable property for DFL-IMPB-VIS-662014L
     * 
     */
    AfDecimal getImpbVis662014lObj();

    void setImpbVis662014lObj(AfDecimal impbVis662014lObj);

    /**
     * Host Variable DFL-IMPST-VIS-662014C
     * 
     */
    AfDecimal getImpstVis662014c();

    void setImpstVis662014c(AfDecimal impstVis662014c);

    /**
     * Nullable property for DFL-IMPST-VIS-662014C
     * 
     */
    AfDecimal getImpstVis662014cObj();

    void setImpstVis662014cObj(AfDecimal impstVis662014cObj);

    /**
     * Host Variable DFL-IMPST-VIS-662014D
     * 
     */
    AfDecimal getImpstVis662014d();

    void setImpstVis662014d(AfDecimal impstVis662014d);

    /**
     * Nullable property for DFL-IMPST-VIS-662014D
     * 
     */
    AfDecimal getImpstVis662014dObj();

    void setImpstVis662014dObj(AfDecimal impstVis662014dObj);

    /**
     * Host Variable DFL-IMPST-VIS-662014L
     * 
     */
    AfDecimal getImpstVis662014l();

    void setImpstVis662014l(AfDecimal impstVis662014l);

    /**
     * Nullable property for DFL-IMPST-VIS-662014L
     * 
     */
    AfDecimal getImpstVis662014lObj();

    void setImpstVis662014lObj(AfDecimal impstVis662014lObj);

    /**
     * Host Variable DFL-IMPB-IS-662014C
     * 
     */
    AfDecimal getImpbIs662014c();

    void setImpbIs662014c(AfDecimal impbIs662014c);

    /**
     * Nullable property for DFL-IMPB-IS-662014C
     * 
     */
    AfDecimal getImpbIs662014cObj();

    void setImpbIs662014cObj(AfDecimal impbIs662014cObj);

    /**
     * Host Variable DFL-IMPB-IS-662014D
     * 
     */
    AfDecimal getImpbIs662014d();

    void setImpbIs662014d(AfDecimal impbIs662014d);

    /**
     * Nullable property for DFL-IMPB-IS-662014D
     * 
     */
    AfDecimal getImpbIs662014dObj();

    void setImpbIs662014dObj(AfDecimal impbIs662014dObj);

    /**
     * Host Variable DFL-IMPB-IS-662014L
     * 
     */
    AfDecimal getImpbIs662014l();

    void setImpbIs662014l(AfDecimal impbIs662014l);

    /**
     * Nullable property for DFL-IMPB-IS-662014L
     * 
     */
    AfDecimal getImpbIs662014lObj();

    void setImpbIs662014lObj(AfDecimal impbIs662014lObj);

    /**
     * Host Variable DFL-IS-662014C
     * 
     */
    AfDecimal getIs662014c();

    void setIs662014c(AfDecimal is662014c);

    /**
     * Nullable property for DFL-IS-662014C
     * 
     */
    AfDecimal getIs662014cObj();

    void setIs662014cObj(AfDecimal is662014cObj);

    /**
     * Host Variable DFL-IS-662014D
     * 
     */
    AfDecimal getIs662014d();

    void setIs662014d(AfDecimal is662014d);

    /**
     * Nullable property for DFL-IS-662014D
     * 
     */
    AfDecimal getIs662014dObj();

    void setIs662014dObj(AfDecimal is662014dObj);

    /**
     * Host Variable DFL-IS-662014L
     * 
     */
    AfDecimal getIs662014l();

    void setIs662014l(AfDecimal is662014l);

    /**
     * Nullable property for DFL-IS-662014L
     * 
     */
    AfDecimal getIs662014lObj();

    void setIs662014lObj(AfDecimal is662014lObj);
};
