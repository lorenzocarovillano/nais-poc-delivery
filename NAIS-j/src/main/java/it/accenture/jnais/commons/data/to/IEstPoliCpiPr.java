package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [EST_POLI_CPI_PR]
 * 
 */
public interface IEstPoliCpiPr extends BaseSqlTo {

    /**
     * Host Variable P67-ID-EST-POLI-CPI-PR
     * 
     */
    int getIdEstPoliCpiPr();

    void setIdEstPoliCpiPr(int idEstPoliCpiPr);

    /**
     * Host Variable P67-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable P67-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for P67-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable P67-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable P67-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable P67-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P67-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Host Variable P67-DS-RIGA
     * 
     */
    long getP67DsRiga();

    void setP67DsRiga(long p67DsRiga);

    /**
     * Host Variable P67-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P67-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P67-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable P67-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable P67-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P67-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable P67-COD-PROD-ESTNO
     * 
     */
    String getCodProdEstno();

    void setCodProdEstno(String codProdEstno);

    /**
     * Host Variable P67-CPT-FIN
     * 
     */
    AfDecimal getCptFin();

    void setCptFin(AfDecimal cptFin);

    /**
     * Nullable property for P67-CPT-FIN
     * 
     */
    AfDecimal getCptFinObj();

    void setCptFinObj(AfDecimal cptFinObj);

    /**
     * Host Variable P67-NUM-TST-FIN
     * 
     */
    int getNumTstFin();

    void setNumTstFin(int numTstFin);

    /**
     * Nullable property for P67-NUM-TST-FIN
     * 
     */
    Integer getNumTstFinObj();

    void setNumTstFinObj(Integer numTstFinObj);

    /**
     * Host Variable P67-TS-FINANZ
     * 
     */
    AfDecimal getTsFinanz();

    void setTsFinanz(AfDecimal tsFinanz);

    /**
     * Nullable property for P67-TS-FINANZ
     * 
     */
    AfDecimal getTsFinanzObj();

    void setTsFinanzObj(AfDecimal tsFinanzObj);

    /**
     * Host Variable P67-DUR-MM-FINANZ
     * 
     */
    int getDurMmFinanz();

    void setDurMmFinanz(int durMmFinanz);

    /**
     * Nullable property for P67-DUR-MM-FINANZ
     * 
     */
    Integer getDurMmFinanzObj();

    void setDurMmFinanzObj(Integer durMmFinanzObj);

    /**
     * Host Variable P67-DT-END-FINANZ-DB
     * 
     */
    String getDtEndFinanzDb();

    void setDtEndFinanzDb(String dtEndFinanzDb);

    /**
     * Nullable property for P67-DT-END-FINANZ-DB
     * 
     */
    String getDtEndFinanzDbObj();

    void setDtEndFinanzDbObj(String dtEndFinanzDbObj);

    /**
     * Host Variable P67-AMM-1A-RAT
     * 
     */
    AfDecimal getAmm1aRat();

    void setAmm1aRat(AfDecimal amm1aRat);

    /**
     * Nullable property for P67-AMM-1A-RAT
     * 
     */
    AfDecimal getAmm1aRatObj();

    void setAmm1aRatObj(AfDecimal amm1aRatObj);

    /**
     * Host Variable P67-VAL-RISC-BENE
     * 
     */
    AfDecimal getValRiscBene();

    void setValRiscBene(AfDecimal valRiscBene);

    /**
     * Nullable property for P67-VAL-RISC-BENE
     * 
     */
    AfDecimal getValRiscBeneObj();

    void setValRiscBeneObj(AfDecimal valRiscBeneObj);

    /**
     * Host Variable P67-AMM-RAT-END
     * 
     */
    AfDecimal getAmmRatEnd();

    void setAmmRatEnd(AfDecimal ammRatEnd);

    /**
     * Nullable property for P67-AMM-RAT-END
     * 
     */
    AfDecimal getAmmRatEndObj();

    void setAmmRatEndObj(AfDecimal ammRatEndObj);

    /**
     * Host Variable P67-TS-CRE-RAT-FINANZ
     * 
     */
    AfDecimal getTsCreRatFinanz();

    void setTsCreRatFinanz(AfDecimal tsCreRatFinanz);

    /**
     * Nullable property for P67-TS-CRE-RAT-FINANZ
     * 
     */
    AfDecimal getTsCreRatFinanzObj();

    void setTsCreRatFinanzObj(AfDecimal tsCreRatFinanzObj);

    /**
     * Host Variable P67-IMP-FIN-REVOLVING
     * 
     */
    AfDecimal getImpFinRevolving();

    void setImpFinRevolving(AfDecimal impFinRevolving);

    /**
     * Nullable property for P67-IMP-FIN-REVOLVING
     * 
     */
    AfDecimal getImpFinRevolvingObj();

    void setImpFinRevolvingObj(AfDecimal impFinRevolvingObj);

    /**
     * Host Variable P67-IMP-UTIL-C-REV
     * 
     */
    AfDecimal getImpUtilCRev();

    void setImpUtilCRev(AfDecimal impUtilCRev);

    /**
     * Nullable property for P67-IMP-UTIL-C-REV
     * 
     */
    AfDecimal getImpUtilCRevObj();

    void setImpUtilCRevObj(AfDecimal impUtilCRevObj);

    /**
     * Host Variable P67-IMP-RAT-REVOLVING
     * 
     */
    AfDecimal getImpRatRevolving();

    void setImpRatRevolving(AfDecimal impRatRevolving);

    /**
     * Nullable property for P67-IMP-RAT-REVOLVING
     * 
     */
    AfDecimal getImpRatRevolvingObj();

    void setImpRatRevolvingObj(AfDecimal impRatRevolvingObj);

    /**
     * Host Variable P67-DT-1O-UTLZ-C-REV-DB
     * 
     */
    String getDt1oUtlzCRevDb();

    void setDt1oUtlzCRevDb(String dt1oUtlzCRevDb);

    /**
     * Nullable property for P67-DT-1O-UTLZ-C-REV-DB
     * 
     */
    String getDt1oUtlzCRevDbObj();

    void setDt1oUtlzCRevDbObj(String dt1oUtlzCRevDbObj);

    /**
     * Host Variable P67-IMP-ASSTO
     * 
     */
    AfDecimal getImpAssto();

    void setImpAssto(AfDecimal impAssto);

    /**
     * Nullable property for P67-IMP-ASSTO
     * 
     */
    AfDecimal getImpAsstoObj();

    void setImpAsstoObj(AfDecimal impAsstoObj);

    /**
     * Host Variable P67-PRE-VERS
     * 
     */
    AfDecimal getPreVers();

    void setPreVers(AfDecimal preVers);

    /**
     * Nullable property for P67-PRE-VERS
     * 
     */
    AfDecimal getPreVersObj();

    void setPreVersObj(AfDecimal preVersObj);

    /**
     * Host Variable P67-DT-SCAD-COP-DB
     * 
     */
    String getDtScadCopDb();

    void setDtScadCopDb(String dtScadCopDb);

    /**
     * Nullable property for P67-DT-SCAD-COP-DB
     * 
     */
    String getDtScadCopDbObj();

    void setDtScadCopDbObj(String dtScadCopDbObj);

    /**
     * Host Variable P67-GG-DEL-MM-SCAD-RAT
     * 
     */
    short getGgDelMmScadRat();

    void setGgDelMmScadRat(short ggDelMmScadRat);

    /**
     * Nullable property for P67-GG-DEL-MM-SCAD-RAT
     * 
     */
    Short getGgDelMmScadRatObj();

    void setGgDelMmScadRatObj(Short ggDelMmScadRatObj);

    /**
     * Host Variable P67-FL-PRE-FIN
     * 
     */
    char getFlPreFin();

    void setFlPreFin(char flPreFin);

    /**
     * Host Variable P67-DT-SCAD-1A-RAT-DB
     * 
     */
    String getDtScad1aRatDb();

    void setDtScad1aRatDb(String dtScad1aRatDb);

    /**
     * Nullable property for P67-DT-SCAD-1A-RAT-DB
     * 
     */
    String getDtScad1aRatDbObj();

    void setDtScad1aRatDbObj(String dtScad1aRatDbObj);

    /**
     * Host Variable P67-DT-EROG-FINANZ-DB
     * 
     */
    String getDtErogFinanzDb();

    void setDtErogFinanzDb(String dtErogFinanzDb);

    /**
     * Nullable property for P67-DT-EROG-FINANZ-DB
     * 
     */
    String getDtErogFinanzDbObj();

    void setDtErogFinanzDbObj(String dtErogFinanzDbObj);

    /**
     * Host Variable P67-DT-STIPULA-FINANZ-DB
     * 
     */
    String getDtStipulaFinanzDb();

    void setDtStipulaFinanzDb(String dtStipulaFinanzDb);

    /**
     * Nullable property for P67-DT-STIPULA-FINANZ-DB
     * 
     */
    String getDtStipulaFinanzDbObj();

    void setDtStipulaFinanzDbObj(String dtStipulaFinanzDbObj);

    /**
     * Host Variable P67-MM-PREAMM
     * 
     */
    int getMmPreamm();

    void setMmPreamm(int mmPreamm);

    /**
     * Nullable property for P67-MM-PREAMM
     * 
     */
    Integer getMmPreammObj();

    void setMmPreammObj(Integer mmPreammObj);

    /**
     * Host Variable P67-IMP-DEB-RES
     * 
     */
    AfDecimal getImpDebRes();

    void setImpDebRes(AfDecimal impDebRes);

    /**
     * Nullable property for P67-IMP-DEB-RES
     * 
     */
    AfDecimal getImpDebResObj();

    void setImpDebResObj(AfDecimal impDebResObj);

    /**
     * Host Variable P67-IMP-RAT-FINANZ
     * 
     */
    AfDecimal getImpRatFinanz();

    void setImpRatFinanz(AfDecimal impRatFinanz);

    /**
     * Nullable property for P67-IMP-RAT-FINANZ
     * 
     */
    AfDecimal getImpRatFinanzObj();

    void setImpRatFinanzObj(AfDecimal impRatFinanzObj);

    /**
     * Host Variable P67-IMP-CANONE-ANTIC
     * 
     */
    AfDecimal getImpCanoneAntic();

    void setImpCanoneAntic(AfDecimal impCanoneAntic);

    /**
     * Nullable property for P67-IMP-CANONE-ANTIC
     * 
     */
    AfDecimal getImpCanoneAnticObj();

    void setImpCanoneAnticObj(AfDecimal impCanoneAnticObj);

    /**
     * Host Variable P67-PER-RAT-FINANZ
     * 
     */
    int getPerRatFinanz();

    void setPerRatFinanz(int perRatFinanz);

    /**
     * Nullable property for P67-PER-RAT-FINANZ
     * 
     */
    Integer getPerRatFinanzObj();

    void setPerRatFinanzObj(Integer perRatFinanzObj);

    /**
     * Host Variable P67-TP-MOD-PAG-RAT
     * 
     */
    String getTpModPagRat();

    void setTpModPagRat(String tpModPagRat);

    /**
     * Nullable property for P67-TP-MOD-PAG-RAT
     * 
     */
    String getTpModPagRatObj();

    void setTpModPagRatObj(String tpModPagRatObj);

    /**
     * Host Variable P67-TP-FINANZ-ER
     * 
     */
    String getTpFinanzEr();

    void setTpFinanzEr(String tpFinanzEr);

    /**
     * Nullable property for P67-TP-FINANZ-ER
     * 
     */
    String getTpFinanzErObj();

    void setTpFinanzErObj(String tpFinanzErObj);

    /**
     * Host Variable P67-CAT-FINANZ-ER
     * 
     */
    String getCatFinanzEr();

    void setCatFinanzEr(String catFinanzEr);

    /**
     * Nullable property for P67-CAT-FINANZ-ER
     * 
     */
    String getCatFinanzErObj();

    void setCatFinanzErObj(String catFinanzErObj);

    /**
     * Host Variable P67-VAL-RISC-END-LEAS
     * 
     */
    AfDecimal getValRiscEndLeas();

    void setValRiscEndLeas(AfDecimal valRiscEndLeas);

    /**
     * Nullable property for P67-VAL-RISC-END-LEAS
     * 
     */
    AfDecimal getValRiscEndLeasObj();

    void setValRiscEndLeasObj(AfDecimal valRiscEndLeasObj);

    /**
     * Host Variable P67-DT-EST-FINANZ-DB
     * 
     */
    String getDtEstFinanzDb();

    void setDtEstFinanzDb(String dtEstFinanzDb);

    /**
     * Nullable property for P67-DT-EST-FINANZ-DB
     * 
     */
    String getDtEstFinanzDbObj();

    void setDtEstFinanzDbObj(String dtEstFinanzDbObj);

    /**
     * Host Variable P67-DT-MAN-COP-DB
     * 
     */
    String getDtManCopDb();

    void setDtManCopDb(String dtManCopDb);

    /**
     * Nullable property for P67-DT-MAN-COP-DB
     * 
     */
    String getDtManCopDbObj();

    void setDtManCopDbObj(String dtManCopDbObj);

    /**
     * Host Variable P67-NUM-FINANZ
     * 
     */
    String getNumFinanz();

    void setNumFinanz(String numFinanz);

    /**
     * Nullable property for P67-NUM-FINANZ
     * 
     */
    String getNumFinanzObj();

    void setNumFinanzObj(String numFinanzObj);

    /**
     * Host Variable P67-TP-MOD-ACQS
     * 
     */
    String getTpModAcqs();

    void setTpModAcqs(String tpModAcqs);

    /**
     * Nullable property for P67-TP-MOD-ACQS
     * 
     */
    String getTpModAcqsObj();

    void setTpModAcqsObj(String tpModAcqsObj);
};
