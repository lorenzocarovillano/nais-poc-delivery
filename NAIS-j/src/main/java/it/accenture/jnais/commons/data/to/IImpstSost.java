package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [IMPST_SOST]
 * 
 */
public interface IImpstSost extends BaseSqlTo {

    /**
     * Host Variable ISO-ID-OGG
     * 
     */
    int getIsoIdOgg();

    void setIsoIdOgg(int isoIdOgg);

    /**
     * Host Variable ISO-TP-OGG
     * 
     */
    String getIsoTpOgg();

    void setIsoTpOgg(String isoTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable ISO-ID-IMPST-SOST
     * 
     */
    int getIdImpstSost();

    void setIdImpstSost(int idImpstSost);

    /**
     * Nullable property for ISO-ID-OGG
     * 
     */
    Integer getIsoIdOggObj();

    void setIsoIdOggObj(Integer isoIdOggObj);

    /**
     * Host Variable ISO-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable ISO-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for ISO-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable ISO-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable ISO-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable ISO-DT-INI-PER-DB
     * 
     */
    String getDtIniPerDb();

    void setDtIniPerDb(String dtIniPerDb);

    /**
     * Nullable property for ISO-DT-INI-PER-DB
     * 
     */
    String getDtIniPerDbObj();

    void setDtIniPerDbObj(String dtIniPerDbObj);

    /**
     * Host Variable ISO-DT-END-PER-DB
     * 
     */
    String getDtEndPerDb();

    void setDtEndPerDb(String dtEndPerDb);

    /**
     * Nullable property for ISO-DT-END-PER-DB
     * 
     */
    String getDtEndPerDbObj();

    void setDtEndPerDbObj(String dtEndPerDbObj);

    /**
     * Host Variable ISO-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable ISO-IMPST-SOST
     * 
     */
    AfDecimal getImpstSost();

    void setImpstSost(AfDecimal impstSost);

    /**
     * Nullable property for ISO-IMPST-SOST
     * 
     */
    AfDecimal getImpstSostObj();

    void setImpstSostObj(AfDecimal impstSostObj);

    /**
     * Host Variable ISO-IMPB-IS
     * 
     */
    AfDecimal getImpbIs();

    void setImpbIs(AfDecimal impbIs);

    /**
     * Nullable property for ISO-IMPB-IS
     * 
     */
    AfDecimal getImpbIsObj();

    void setImpbIsObj(AfDecimal impbIsObj);

    /**
     * Host Variable ISO-ALQ-IS
     * 
     */
    AfDecimal getAlqIs();

    void setAlqIs(AfDecimal alqIs);

    /**
     * Nullable property for ISO-ALQ-IS
     * 
     */
    AfDecimal getAlqIsObj();

    void setAlqIsObj(AfDecimal alqIsObj);

    /**
     * Host Variable ISO-COD-TRB
     * 
     */
    String getCodTrb();

    void setCodTrb(String codTrb);

    /**
     * Nullable property for ISO-COD-TRB
     * 
     */
    String getCodTrbObj();

    void setCodTrbObj(String codTrbObj);

    /**
     * Host Variable ISO-PRSTZ-LRD-ANTE-IS
     * 
     */
    AfDecimal getPrstzLrdAnteIs();

    void setPrstzLrdAnteIs(AfDecimal prstzLrdAnteIs);

    /**
     * Nullable property for ISO-PRSTZ-LRD-ANTE-IS
     * 
     */
    AfDecimal getPrstzLrdAnteIsObj();

    void setPrstzLrdAnteIsObj(AfDecimal prstzLrdAnteIsObj);

    /**
     * Host Variable ISO-RIS-MAT-NET-PREC
     * 
     */
    AfDecimal getRisMatNetPrec();

    void setRisMatNetPrec(AfDecimal risMatNetPrec);

    /**
     * Nullable property for ISO-RIS-MAT-NET-PREC
     * 
     */
    AfDecimal getRisMatNetPrecObj();

    void setRisMatNetPrecObj(AfDecimal risMatNetPrecObj);

    /**
     * Host Variable ISO-RIS-MAT-ANTE-TAX
     * 
     */
    AfDecimal getRisMatAnteTax();

    void setRisMatAnteTax(AfDecimal risMatAnteTax);

    /**
     * Nullable property for ISO-RIS-MAT-ANTE-TAX
     * 
     */
    AfDecimal getRisMatAnteTaxObj();

    void setRisMatAnteTaxObj(AfDecimal risMatAnteTaxObj);

    /**
     * Host Variable ISO-RIS-MAT-POST-TAX
     * 
     */
    AfDecimal getRisMatPostTax();

    void setRisMatPostTax(AfDecimal risMatPostTax);

    /**
     * Nullable property for ISO-RIS-MAT-POST-TAX
     * 
     */
    AfDecimal getRisMatPostTaxObj();

    void setRisMatPostTaxObj(AfDecimal risMatPostTaxObj);

    /**
     * Host Variable ISO-PRSTZ-NET
     * 
     */
    AfDecimal getPrstzNet();

    void setPrstzNet(AfDecimal prstzNet);

    /**
     * Nullable property for ISO-PRSTZ-NET
     * 
     */
    AfDecimal getPrstzNetObj();

    void setPrstzNetObj(AfDecimal prstzNetObj);

    /**
     * Host Variable ISO-PRSTZ-PREC
     * 
     */
    AfDecimal getPrstzPrec();

    void setPrstzPrec(AfDecimal prstzPrec);

    /**
     * Nullable property for ISO-PRSTZ-PREC
     * 
     */
    AfDecimal getPrstzPrecObj();

    void setPrstzPrecObj(AfDecimal prstzPrecObj);

    /**
     * Host Variable ISO-CUM-PRE-VERS
     * 
     */
    AfDecimal getCumPreVers();

    void setCumPreVers(AfDecimal cumPreVers);

    /**
     * Nullable property for ISO-CUM-PRE-VERS
     * 
     */
    AfDecimal getCumPreVersObj();

    void setCumPreVersObj(AfDecimal cumPreVersObj);

    /**
     * Host Variable ISO-TP-CALC-IMPST
     * 
     */
    String getTpCalcImpst();

    void setTpCalcImpst(String tpCalcImpst);

    /**
     * Host Variable ISO-IMP-GIA-TASSATO
     * 
     */
    AfDecimal getImpGiaTassato();

    void setImpGiaTassato(AfDecimal impGiaTassato);

    /**
     * Host Variable ISO-DS-RIGA
     * 
     */
    long getIsoDsRiga();

    void setIsoDsRiga(long isoDsRiga);

    /**
     * Host Variable ISO-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable ISO-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable ISO-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable ISO-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable ISO-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable ISO-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable LDBV2901-IMPB-IS
     * 
     */
    AfDecimal getMpbIs();

    void setMpbIs(AfDecimal mpbIs);

    /**
     * Host Variable LDBV2901-ID-ADES
     * 
     */
    int getLdbv2901IdAdes();

    void setLdbv2901IdAdes(int ldbv2901IdAdes);

    /**
     * Host Variable LDBV6191-DT-INF-DB
     * 
     */
    String getLdbv6191DtInfDb();

    void setLdbv6191DtInfDb(String ldbv6191DtInfDb);

    /**
     * Host Variable LDBV6191-DT-SUP-DB
     * 
     */
    String getLdbv6191DtSupDb();

    void setLdbv6191DtSupDb(String ldbv6191DtSupDb);

    /**
     * Host Variable LDBV1591-ID-ADES
     * 
     */
    int getLdbv1591IdAdes();

    void setLdbv1591IdAdes(int ldbv1591IdAdes);

    /**
     * Host Variable WS-DT-INFINITO-1
     * 
     */
    String getWsDtInfinito1();

    void setWsDtInfinito1(String wsDtInfinito1);

    /**
     * Host Variable WS-TS-INFINITO-1
     * 
     */
    long getWsTsInfinito1();

    void setWsTsInfinito1(long wsTsInfinito1);
};
