package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BTC_PARALLELISM]
 * 
 */
public interface IBtcParallelism extends BaseSqlTo {

    /**
     * Host Variable BPA-COD-COMP-ANIA
     * 
     */
    int getBpaCodCompAnia();

    void setBpaCodCompAnia(int bpaCodCompAnia);

    /**
     * Host Variable BPA-PROTOCOL
     * 
     */
    String getBpaProtocol();

    void setBpaProtocol(String bpaProtocol);

    /**
     * Host Variable BPA-PROG-PROTOCOL
     * 
     */
    int getBpaProgProtocol();

    void setBpaProgProtocol(int bpaProgProtocol);

    /**
     * Host Variable BPA-COD-BATCH-STATE
     * 
     */
    char getCodBatchState();

    void setCodBatchState(char codBatchState);

    /**
     * Host Variable BPA-DT-INS-DB
     * 
     */
    String getDtInsDb();

    void setDtInsDb(String dtInsDb);

    /**
     * Host Variable BPA-USER-INS
     * 
     */
    String getUserIns();

    void setUserIns(String userIns);

    /**
     * Host Variable BPA-DT-START-DB
     * 
     */
    String getDtStartDb();

    void setDtStartDb(String dtStartDb);

    /**
     * Nullable property for BPA-DT-START-DB
     * 
     */
    String getDtStartDbObj();

    void setDtStartDbObj(String dtStartDbObj);

    /**
     * Host Variable BPA-DT-END-DB
     * 
     */
    String getDtEndDb();

    void setDtEndDb(String dtEndDb);

    /**
     * Nullable property for BPA-DT-END-DB
     * 
     */
    String getDtEndDbObj();

    void setDtEndDbObj(String dtEndDbObj);

    /**
     * Host Variable BPA-USER-START
     * 
     */
    String getUserStart();

    void setUserStart(String userStart);

    /**
     * Nullable property for BPA-USER-START
     * 
     */
    String getUserStartObj();

    void setUserStartObj(String userStartObj);

    /**
     * Host Variable BPA-DESC-PARALLELISM-VCHAR
     * 
     */
    String getDescParallelismVchar();

    void setDescParallelismVchar(String descParallelismVchar);

    /**
     * Nullable property for BPA-DESC-PARALLELISM-VCHAR
     * 
     */
    String getDescParallelismVcharObj();

    void setDescParallelismVcharObj(String descParallelismVcharObj);

    /**
     * Host Variable BPA-ID-OGG-DA
     * 
     */
    int getIdOggDa();

    void setIdOggDa(int idOggDa);

    /**
     * Nullable property for BPA-ID-OGG-DA
     * 
     */
    Integer getIdOggDaObj();

    void setIdOggDaObj(Integer idOggDaObj);

    /**
     * Host Variable BPA-ID-OGG-A
     * 
     */
    int getIdOggA();

    void setIdOggA(int idOggA);

    /**
     * Nullable property for BPA-ID-OGG-A
     * 
     */
    Integer getIdOggAObj();

    void setIdOggAObj(Integer idOggAObj);

    /**
     * Host Variable BPA-TP-OGG
     * 
     */
    String getTpOgg();

    void setTpOgg(String tpOgg);

    /**
     * Nullable property for BPA-TP-OGG
     * 
     */
    String getTpOggObj();

    void setTpOggObj(String tpOggObj);

    /**
     * Host Variable BPA-NUM-ROW-SCHEDULE
     * 
     */
    int getNumRowSchedule();

    void setNumRowSchedule(int numRowSchedule);

    /**
     * Nullable property for BPA-NUM-ROW-SCHEDULE
     * 
     */
    Integer getNumRowScheduleObj();

    void setNumRowScheduleObj(Integer numRowScheduleObj);

    /**
     * Host Variable IABV0002-STATE-01
     * 
     */
    char getIabv0002State01();

    void setIabv0002State01(char iabv0002State01);

    /**
     * Host Variable IABV0002-STATE-02
     * 
     */
    char getIabv0002State02();

    void setIabv0002State02(char iabv0002State02);

    /**
     * Host Variable IABV0002-STATE-03
     * 
     */
    char getIabv0002State03();

    void setIabv0002State03(char iabv0002State03);

    /**
     * Host Variable IABV0002-STATE-04
     * 
     */
    char getIabv0002State04();

    void setIabv0002State04(char iabv0002State04);

    /**
     * Host Variable IABV0002-STATE-05
     * 
     */
    char getIabv0002State05();

    void setIabv0002State05(char iabv0002State05);

    /**
     * Host Variable IABV0002-STATE-06
     * 
     */
    char getIabv0002State06();

    void setIabv0002State06(char iabv0002State06);

    /**
     * Host Variable IABV0002-STATE-07
     * 
     */
    char getIabv0002State07();

    void setIabv0002State07(char iabv0002State07);

    /**
     * Host Variable IABV0002-STATE-08
     * 
     */
    char getIabv0002State08();

    void setIabv0002State08(char iabv0002State08);

    /**
     * Host Variable IABV0002-STATE-09
     * 
     */
    char getIabv0002State09();

    void setIabv0002State09(char iabv0002State09);

    /**
     * Host Variable IABV0002-STATE-10
     * 
     */
    char getIabv0002State10();

    void setIabv0002State10(char iabv0002State10);
};
