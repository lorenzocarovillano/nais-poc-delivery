package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PARAM_DI_CALC]
 * 
 */
public interface IParamDiCalc extends BaseSqlTo {

    /**
     * Host Variable PCA-ID-OGG
     * 
     */
    int getPcaIdOgg();

    void setPcaIdOgg(int pcaIdOgg);

    /**
     * Host Variable PCA-TP-OGG
     * 
     */
    String getPcaTpOgg();

    void setPcaTpOgg(String pcaTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable PCA-COD-PARAM
     * 
     */
    String getPcaCodParam();

    void setPcaCodParam(String pcaCodParam);

    /**
     * Host Variable PCA-ID-PARAM-DI-CALC
     * 
     */
    int getIdParamDiCalc();

    void setIdParamDiCalc(int idParamDiCalc);

    /**
     * Nullable property for PCA-ID-OGG
     * 
     */
    Integer getPcaIdOggObj();

    void setPcaIdOggObj(Integer pcaIdOggObj);

    /**
     * Host Variable PCA-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable PCA-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for PCA-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable PCA-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable PCA-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable PCA-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Nullable property for PCA-COD-PARAM
     * 
     */
    String getPcaCodParamObj();

    void setPcaCodParamObj(String pcaCodParamObj);

    /**
     * Host Variable PCA-TP-D
     * 
     */
    String getTpD();

    void setTpD(String tpD);

    /**
     * Nullable property for PCA-TP-D
     * 
     */
    String getTpDObj();

    void setTpDObj(String tpDObj);

    /**
     * Host Variable PCA-VAL-DT-DB
     * 
     */
    String getValDtDb();

    void setValDtDb(String valDtDb);

    /**
     * Nullable property for PCA-VAL-DT-DB
     * 
     */
    String getValDtDbObj();

    void setValDtDbObj(String valDtDbObj);

    /**
     * Host Variable PCA-VAL-IMP
     * 
     */
    AfDecimal getValImp();

    void setValImp(AfDecimal valImp);

    /**
     * Nullable property for PCA-VAL-IMP
     * 
     */
    AfDecimal getValImpObj();

    void setValImpObj(AfDecimal valImpObj);

    /**
     * Host Variable PCA-VAL-TS
     * 
     */
    AfDecimal getValTs();

    void setValTs(AfDecimal valTs);

    /**
     * Nullable property for PCA-VAL-TS
     * 
     */
    AfDecimal getValTsObj();

    void setValTsObj(AfDecimal valTsObj);

    /**
     * Host Variable PCA-VAL-NUM
     * 
     */
    long getValNum();

    void setValNum(long valNum);

    /**
     * Nullable property for PCA-VAL-NUM
     * 
     */
    Long getValNumObj();

    void setValNumObj(Long valNumObj);

    /**
     * Host Variable PCA-VAL-PC
     * 
     */
    AfDecimal getValPc();

    void setValPc(AfDecimal valPc);

    /**
     * Nullable property for PCA-VAL-PC
     * 
     */
    AfDecimal getValPcObj();

    void setValPcObj(AfDecimal valPcObj);

    /**
     * Host Variable PCA-VAL-TXT-VCHAR
     * 
     */
    String getValTxtVchar();

    void setValTxtVchar(String valTxtVchar);

    /**
     * Nullable property for PCA-VAL-TXT-VCHAR
     * 
     */
    String getValTxtVcharObj();

    void setValTxtVcharObj(String valTxtVcharObj);

    /**
     * Host Variable PCA-VAL-FL
     * 
     */
    char getValFl();

    void setValFl(char valFl);

    /**
     * Nullable property for PCA-VAL-FL
     * 
     */
    Character getValFlObj();

    void setValFlObj(Character valFlObj);

    /**
     * Host Variable PCA-DS-RIGA
     * 
     */
    long getPcaDsRiga();

    void setPcaDsRiga(long pcaDsRiga);

    /**
     * Host Variable PCA-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable PCA-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable PCA-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable PCA-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable PCA-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable PCA-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
