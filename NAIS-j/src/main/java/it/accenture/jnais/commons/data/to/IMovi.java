package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [MOVI]
 * 
 */
public interface IMovi extends BaseSqlTo {

    /**
     * Host Variable LDBV2651-ID-OGG
     * 
     */
    int getLdbv2651IdOgg();

    void setLdbv2651IdOgg(int ldbv2651IdOgg);

    /**
     * Host Variable LDBV2651-TP-OGG
     * 
     */
    String getLdbv2651TpOgg();

    void setLdbv2651TpOgg(String ldbv2651TpOgg);

    /**
     * Host Variable LDBV2651-TP-MOV1
     * 
     */
    int getLdbv2651TpMov1();

    void setLdbv2651TpMov1(int ldbv2651TpMov1);

    /**
     * Host Variable LDBV2651-TP-MOV2
     * 
     */
    int getLdbv2651TpMov2();

    void setLdbv2651TpMov2(int ldbv2651TpMov2);

    /**
     * Host Variable LDBV2651-TP-MOV3
     * 
     */
    int getLdbv2651TpMov3();

    void setLdbv2651TpMov3(int ldbv2651TpMov3);

    /**
     * Host Variable LDBV2651-DT-INIZ-DB
     * 
     */
    String getLdbv2651DtInizDb();

    void setLdbv2651DtInizDb(String ldbv2651DtInizDb);

    /**
     * Host Variable LDBV2651-DT-FINE-DB
     * 
     */
    String getLdbv2651DtFineDb();

    void setLdbv2651DtFineDb(String ldbv2651DtFineDb);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable LDBV3611-TP-OGG
     * 
     */
    String getLdbv3611TpOgg();

    void setLdbv3611TpOgg(String ldbv3611TpOgg);

    /**
     * Host Variable LDBV3611-ID-OGG
     * 
     */
    int getLdbv3611IdOgg();

    void setLdbv3611IdOgg(int ldbv3611IdOgg);

    /**
     * Host Variable LDBV3611-TP-MOVI-1
     * 
     */
    int getLdbv3611TpMovi1();

    void setLdbv3611TpMovi1(int ldbv3611TpMovi1);

    /**
     * Host Variable LDBV3611-TP-MOVI-2
     * 
     */
    int getLdbv3611TpMovi2();

    void setLdbv3611TpMovi2(int ldbv3611TpMovi2);

    /**
     * Host Variable LDBV3611-TP-MOVI-3
     * 
     */
    int getLdbv3611TpMovi3();

    void setLdbv3611TpMovi3(int ldbv3611TpMovi3);

    /**
     * Host Variable LDBV3611-TP-MOVI-4
     * 
     */
    int getLdbv3611TpMovi4();

    void setLdbv3611TpMovi4(int ldbv3611TpMovi4);

    /**
     * Host Variable LDBV3611-TP-MOVI-5
     * 
     */
    int getLdbv3611TpMovi5();

    void setLdbv3611TpMovi5(int ldbv3611TpMovi5);

    /**
     * Host Variable LDBV3611-TP-MOVI-6
     * 
     */
    int getLdbv3611TpMovi6();

    void setLdbv3611TpMovi6(int ldbv3611TpMovi6);

    /**
     * Host Variable LDBV3611-TP-MOVI-7
     * 
     */
    int getLdbv3611TpMovi7();

    void setLdbv3611TpMovi7(int ldbv3611TpMovi7);

    /**
     * Host Variable LDBV3611-TP-MOVI-8
     * 
     */
    int getLdbv3611TpMovi8();

    void setLdbv3611TpMovi8(int ldbv3611TpMovi8);

    /**
     * Host Variable LDBV3611-TP-MOVI-9
     * 
     */
    int getLdbv3611TpMovi9();

    void setLdbv3611TpMovi9(int ldbv3611TpMovi9);

    /**
     * Host Variable LDBV3611-TP-MOVI-10
     * 
     */
    int getLdbv3611TpMovi10();

    void setLdbv3611TpMovi10(int ldbv3611TpMovi10);

    /**
     * Host Variable LDBV3611-TP-MOVI-11
     * 
     */
    int getLdbv3611TpMovi11();

    void setLdbv3611TpMovi11(int ldbv3611TpMovi11);

    /**
     * Host Variable LDBV3611-TP-MOVI-12
     * 
     */
    int getLdbv3611TpMovi12();

    void setLdbv3611TpMovi12(int ldbv3611TpMovi12);

    /**
     * Host Variable LDBV3611-TP-MOVI-13
     * 
     */
    int getLdbv3611TpMovi13();

    void setLdbv3611TpMovi13(int ldbv3611TpMovi13);

    /**
     * Host Variable LDBV3611-TP-MOVI-14
     * 
     */
    int getLdbv3611TpMovi14();

    void setLdbv3611TpMovi14(int ldbv3611TpMovi14);

    /**
     * Host Variable LDBV3611-TP-MOVI-15
     * 
     */
    int getLdbv3611TpMovi15();

    void setLdbv3611TpMovi15(int ldbv3611TpMovi15);

    /**
     * Host Variable LDBV3611-DT-DA-DB
     * 
     */
    String getLdbv3611DtDaDb();

    void setLdbv3611DtDaDb(String ldbv3611DtDaDb);

    /**
     * Host Variable LDBV3611-DT-A-DB
     * 
     */
    String getLdbv3611DtADb();

    void setLdbv3611DtADb(String ldbv3611DtADb);

    /**
     * Host Variable MOV-ID-OGG
     * 
     */
    int getMovIdOgg();

    void setMovIdOgg(int movIdOgg);

    /**
     * Host Variable MOV-TP-OGG
     * 
     */
    String getMovTpOgg();

    void setMovTpOgg(String movTpOgg);

    /**
     * Host Variable LDBV5991-TP-MOV1
     * 
     */
    int getLdbv5991TpMov1();

    void setLdbv5991TpMov1(int ldbv5991TpMov1);

    /**
     * Host Variable LDBV5991-TP-MOV2
     * 
     */
    int getLdbv5991TpMov2();

    void setLdbv5991TpMov2(int ldbv5991TpMov2);

    /**
     * Host Variable LDBV5991-TP-MOV3
     * 
     */
    int getLdbv5991TpMov3();

    void setLdbv5991TpMov3(int ldbv5991TpMov3);

    /**
     * Host Variable LDBV5991-TP-MOV4
     * 
     */
    int getLdbv5991TpMov4();

    void setLdbv5991TpMov4(int ldbv5991TpMov4);

    /**
     * Host Variable LDBV5991-TP-MOV5
     * 
     */
    int getLdbv5991TpMov5();

    void setLdbv5991TpMov5(int ldbv5991TpMov5);

    /**
     * Host Variable LDBV5991-TP-MOV6
     * 
     */
    int getLdbv5991TpMov6();

    void setLdbv5991TpMov6(int ldbv5991TpMov6);

    /**
     * Host Variable LDBV5991-TP-MOV7
     * 
     */
    int getLdbv5991TpMov7();

    void setLdbv5991TpMov7(int ldbv5991TpMov7);

    /**
     * Host Variable LDBV5991-TP-MOV8
     * 
     */
    int getLdbv5991TpMov8();

    void setLdbv5991TpMov8(int ldbv5991TpMov8);

    /**
     * Host Variable LDBV5991-TP-MOV9
     * 
     */
    int getLdbv5991TpMov9();

    void setLdbv5991TpMov9(int ldbv5991TpMov9);

    /**
     * Host Variable LDBV5991-TP-MOV10
     * 
     */
    int getLdbv5991TpMov10();

    void setLdbv5991TpMov10(int ldbv5991TpMov10);

    /**
     * Host Variable LDBVC591-TP-MOVI-1
     * 
     */
    int getLdbvc591TpMovi1();

    void setLdbvc591TpMovi1(int ldbvc591TpMovi1);

    /**
     * Host Variable LDBVC591-TP-MOVI-2
     * 
     */
    int getLdbvc591TpMovi2();

    void setLdbvc591TpMovi2(int ldbvc591TpMovi2);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBVE061-TP-MOVI
     * 
     */
    int getLdbve061TpMovi();

    void setLdbve061TpMovi(int ldbve061TpMovi);

    /**
     * Host Variable LDBVE061-TP-OGG
     * 
     */
    String getLdbve061TpOgg();

    void setLdbve061TpOgg(String ldbve061TpOgg);

    /**
     * Host Variable LDBVE061-ID-OGG
     * 
     */
    int getLdbve061IdOgg();

    void setLdbve061IdOgg(int ldbve061IdOgg);

    /**
     * Host Variable MOV-DT-EFF-DB
     * 
     */
    String getMovDtEffDb();

    void setMovDtEffDb(String movDtEffDb);

    /**
     * Host Variable MOV-ID-MOVI
     * 
     */
    int getMovIdMovi();

    void setMovIdMovi(int movIdMovi);

    /**
     * Host Variable MOV-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Nullable property for MOV-ID-OGG
     * 
     */
    Integer getMovIdOggObj();

    void setMovIdOggObj(Integer movIdOggObj);

    /**
     * Host Variable MOV-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for MOV-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable MOV-IB-MOVI
     * 
     */
    String getIbMovi();

    void setIbMovi(String ibMovi);

    /**
     * Nullable property for MOV-IB-MOVI
     * 
     */
    String getIbMoviObj();

    void setIbMoviObj(String ibMoviObj);

    /**
     * Nullable property for MOV-TP-OGG
     * 
     */
    String getMovTpOggObj();

    void setMovTpOggObj(String movTpOggObj);

    /**
     * Host Variable MOV-ID-RICH
     * 
     */
    int getIdRich();

    void setIdRich(int idRich);

    /**
     * Nullable property for MOV-ID-RICH
     * 
     */
    Integer getIdRichObj();

    void setIdRichObj(Integer idRichObj);

    /**
     * Host Variable MOV-TP-MOVI
     * 
     */
    int getTpMovi();

    void setTpMovi(int tpMovi);

    /**
     * Nullable property for MOV-TP-MOVI
     * 
     */
    Integer getTpMoviObj();

    void setTpMoviObj(Integer tpMoviObj);

    /**
     * Host Variable MOV-ID-MOVI-ANN
     * 
     */
    int getIdMoviAnn();

    void setIdMoviAnn(int idMoviAnn);

    /**
     * Nullable property for MOV-ID-MOVI-ANN
     * 
     */
    Integer getIdMoviAnnObj();

    void setIdMoviAnnObj(Integer idMoviAnnObj);

    /**
     * Host Variable MOV-ID-MOVI-COLLG
     * 
     */
    int getIdMoviCollg();

    void setIdMoviCollg(int idMoviCollg);

    /**
     * Nullable property for MOV-ID-MOVI-COLLG
     * 
     */
    Integer getIdMoviCollgObj();

    void setIdMoviCollgObj(Integer idMoviCollgObj);

    /**
     * Host Variable MOV-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable MOV-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable MOV-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable MOV-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable MOV-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable POL-DT-INI-EFF-DB
     * 
     */
    String getPolDtIniEffDb();

    void setPolDtIniEffDb(String polDtIniEffDb);

    /**
     * Host Variable LDBV2921-ID-POL
     * 
     */
    int getLdbv2921IdPol();

    void setLdbv2921IdPol(int ldbv2921IdPol);

    /**
     * Host Variable POL-DT-EMIS-DB
     * 
     */
    String getPolDtEmisDb();

    void setPolDtEmisDb(String polDtEmisDb);
};
