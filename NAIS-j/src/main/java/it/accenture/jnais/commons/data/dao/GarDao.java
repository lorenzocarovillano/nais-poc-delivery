package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IGar;

/**
 * Data Access Object(DAO) for table [GAR]
 * 
 */
public class GarDao extends BaseSqlDao<IGar> {

    private Cursor cIdUpdEffGrz;
    private Cursor cIdpEffGrz;
    private Cursor cIboEffGrz;
    private Cursor cIdpCpzGrz;
    private Cursor cIboCpzGrz;
    private final IRowMapper<IGar> selectByGrzDsRigaRm = buildNamedRowMapper(IGar.class, "idGar", "grzIdAdesObj", "grzIdPoli", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "ibOggObj", "dtDecorDbObj", "dtScadDbObj", "codSezObj", "codTari", "ramoBilaObj", "dtIniValTarDbObj", "id1oAsstoObj", "id2oAsstoObj", "id3oAsstoObj", "tpGarObj", "tpRshObj", "tpInvstObj", "modPagGarcolObj", "tpPerPreObj", "etaAa1oAsstoObj", "etaMm1oAsstoObj", "etaAa2oAsstoObj", "etaMm2oAsstoObj", "etaAa3oAsstoObj", "etaMm3oAsstoObj", "tpEmisPurObj", "etaAScadObj", "tpCalcPrePrstzObj", "tpPreObj", "tpDurObj", "durAaObj", "durMmObj", "durGgObj", "numAaPagPreObj", "aaPagPreUniObj", "mmPagPreUniObj", "frazIniErogRenObj", "mm1oRatObj", "pc1oRatObj", "tpPrstzAsstaObj", "dtEndCarzDbObj", "pcRipPreObj", "codFndObj", "aaRenCerObj", "pcRevrsbObj", "tpPcRipObj", "pcOpzObj", "tpIasObj", "tpStabObj", "tpAdegPreObj", "dtVarzTpIasDbObj", "frazDecrCptObj", "codTratRiassObj", "tpDtEmisRiassObj", "tpCessRiassObj", "grzDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "aaStabObj", "tsStabLimitataObj", "dtPrescDbObj", "rshInvstObj", "tpRamoBila");

    public GarDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IGar> getToClass() {
        return IGar.class;
    }

    public IGar selectByGrzDsRiga(long grzDsRiga, IGar iGar) {
        return buildQuery("selectByGrzDsRiga").bind("grzDsRiga", grzDsRiga).rowMapper(selectByGrzDsRigaRm).singleResult(iGar);
    }

    public DbAccessStatus insertRec(IGar iGar) {
        return buildQuery("insertRec").bind(iGar).executeInsert();
    }

    public DbAccessStatus updateRec(IGar iGar) {
        return buildQuery("updateRec").bind(iGar).executeUpdate();
    }

    public DbAccessStatus deleteByGrzDsRiga(long grzDsRiga) {
        return buildQuery("deleteByGrzDsRiga").bind("grzDsRiga", grzDsRiga).executeDelete();
    }

    public IGar selectRec(int grzIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IGar iGar) {
        return buildQuery("selectRec").bind("grzIdGar", grzIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByGrzDsRigaRm).singleResult(iGar);
    }

    public DbAccessStatus updateRec1(IGar iGar) {
        return buildQuery("updateRec1").bind(iGar).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffGrz(int grzIdGar, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffGrz = buildQuery("openCIdUpdEffGrz").bind("grzIdGar", grzIdGar).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffGrz() {
        return closeCursor(cIdUpdEffGrz);
    }

    public IGar fetchCIdUpdEffGrz(IGar iGar) {
        return fetch(cIdUpdEffGrz, iGar, selectByGrzDsRigaRm);
    }

    public IGar selectRec1(int grzIdPoli, int grzIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IGar iGar) {
        return buildQuery("selectRec1").bind("grzIdPoli", grzIdPoli).bind("grzIdAdes", grzIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByGrzDsRigaRm).singleResult(iGar);
    }

    public DbAccessStatus openCIdpEffGrz(int grzIdPoli, int grzIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffGrz = buildQuery("openCIdpEffGrz").bind("grzIdPoli", grzIdPoli).bind("grzIdAdes", grzIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffGrz() {
        return closeCursor(cIdpEffGrz);
    }

    public IGar fetchCIdpEffGrz(IGar iGar) {
        return fetch(cIdpEffGrz, iGar, selectByGrzDsRigaRm);
    }

    public IGar selectRec2(String grzIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IGar iGar) {
        return buildQuery("selectRec2").bind("grzIbOgg", grzIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByGrzDsRigaRm).singleResult(iGar);
    }

    public DbAccessStatus openCIboEffGrz(String grzIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIboEffGrz = buildQuery("openCIboEffGrz").bind("grzIbOgg", grzIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboEffGrz() {
        return closeCursor(cIboEffGrz);
    }

    public IGar fetchCIboEffGrz(IGar iGar) {
        return fetch(cIboEffGrz, iGar, selectByGrzDsRigaRm);
    }

    public IGar selectRec3(int grzIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IGar iGar) {
        return buildQuery("selectRec3").bind("grzIdGar", grzIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByGrzDsRigaRm).singleResult(iGar);
    }

    public IGar selectRec4(IGar iGar) {
        return buildQuery("selectRec4").bind(iGar).rowMapper(selectByGrzDsRigaRm).singleResult(iGar);
    }

    public DbAccessStatus openCIdpCpzGrz(IGar iGar) {
        cIdpCpzGrz = buildQuery("openCIdpCpzGrz").bind(iGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzGrz() {
        return closeCursor(cIdpCpzGrz);
    }

    public IGar fetchCIdpCpzGrz(IGar iGar) {
        return fetch(cIdpCpzGrz, iGar, selectByGrzDsRigaRm);
    }

    public IGar selectRec5(String grzIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IGar iGar) {
        return buildQuery("selectRec5").bind("grzIbOgg", grzIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByGrzDsRigaRm).singleResult(iGar);
    }

    public DbAccessStatus openCIboCpzGrz(String grzIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIboCpzGrz = buildQuery("openCIboCpzGrz").bind("grzIbOgg", grzIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboCpzGrz() {
        return closeCursor(cIboCpzGrz);
    }

    public IGar fetchCIboCpzGrz(IGar iGar) {
        return fetch(cIboCpzGrz, iGar, selectByGrzDsRigaRm);
    }
}
