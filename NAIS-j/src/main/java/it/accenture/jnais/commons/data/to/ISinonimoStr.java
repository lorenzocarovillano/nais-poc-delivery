package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [SINONIMO_STR]
 * 
 */
public interface ISinonimoStr extends BaseSqlTo {

};
