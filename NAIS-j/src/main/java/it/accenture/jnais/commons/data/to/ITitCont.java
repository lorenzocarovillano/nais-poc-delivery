package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [TIT_CONT]
 * 
 */
public interface ITitCont extends BaseSqlTo {

    /**
     * Host Variable TIT-ID-OGG
     * 
     */
    int getTitIdOgg();

    void setTitIdOgg(int titIdOgg);

    /**
     * Host Variable TIT-TP-OGG
     * 
     */
    String getTitTpOgg();

    void setTitTpOgg(String titTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable TIT-TP-TIT
     * 
     */
    String getTitTpTit();

    void setTitTpTit(String titTpTit);

    /**
     * Host Variable TIT-TP-STAT-TIT
     * 
     */
    String getTitTpStatTit();

    void setTitTpStatTit(String titTpStatTit);

    /**
     * Host Variable TIT-DT-INI-COP-DB
     * 
     */
    String getTitDtIniCopDb();

    void setTitDtIniCopDb(String titDtIniCopDb);

    /**
     * Host Variable TIT-TP-PRE-TIT
     * 
     */
    String getTitTpPreTit();

    void setTitTpPreTit(String titTpPreTit);

    /**
     * Host Variable WS-DT-INFINITO-1
     * 
     */
    String getWsDtInfinito1();

    void setWsDtInfinito1(String wsDtInfinito1);

    /**
     * Host Variable WS-TS-INFINITO-1
     * 
     */
    long getWsTsInfinito1();

    void setWsTsInfinito1(long wsTsInfinito1);

    /**
     * Host Variable TIT-ID-TIT-CONT
     * 
     */
    int getIdTitCont();

    void setIdTitCont(int idTitCont);

    /**
     * Host Variable TIT-IB-RICH
     * 
     */
    String getIbRich();

    void setIbRich(String ibRich);

    /**
     * Nullable property for TIT-IB-RICH
     * 
     */
    String getIbRichObj();

    void setIbRichObj(String ibRichObj);

    /**
     * Host Variable TIT-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable TIT-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for TIT-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable TIT-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable TIT-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable TIT-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable TIT-PROG-TIT
     * 
     */
    int getProgTit();

    void setProgTit(int progTit);

    /**
     * Nullable property for TIT-PROG-TIT
     * 
     */
    Integer getProgTitObj();

    void setProgTitObj(Integer progTitObj);

    /**
     * Nullable property for TIT-DT-INI-COP-DB
     * 
     */
    String getTitDtIniCopDbObj();

    void setTitDtIniCopDbObj(String titDtIniCopDbObj);

    /**
     * Host Variable TIT-DT-END-COP-DB
     * 
     */
    String getDtEndCopDb();

    void setDtEndCopDb(String dtEndCopDb);

    /**
     * Nullable property for TIT-DT-END-COP-DB
     * 
     */
    String getDtEndCopDbObj();

    void setDtEndCopDbObj(String dtEndCopDbObj);

    /**
     * Host Variable TIT-IMP-PAG
     * 
     */
    AfDecimal getImpPag();

    void setImpPag(AfDecimal impPag);

    /**
     * Nullable property for TIT-IMP-PAG
     * 
     */
    AfDecimal getImpPagObj();

    void setImpPagObj(AfDecimal impPagObj);

    /**
     * Host Variable TIT-FL-SOLL
     * 
     */
    char getFlSoll();

    void setFlSoll(char flSoll);

    /**
     * Nullable property for TIT-FL-SOLL
     * 
     */
    Character getFlSollObj();

    void setFlSollObj(Character flSollObj);

    /**
     * Host Variable TIT-FRAZ
     * 
     */
    int getFraz();

    void setFraz(int fraz);

    /**
     * Nullable property for TIT-FRAZ
     * 
     */
    Integer getFrazObj();

    void setFrazObj(Integer frazObj);

    /**
     * Host Variable TIT-DT-APPLZ-MORA-DB
     * 
     */
    String getDtApplzMoraDb();

    void setDtApplzMoraDb(String dtApplzMoraDb);

    /**
     * Nullable property for TIT-DT-APPLZ-MORA-DB
     * 
     */
    String getDtApplzMoraDbObj();

    void setDtApplzMoraDbObj(String dtApplzMoraDbObj);

    /**
     * Host Variable TIT-FL-MORA
     * 
     */
    char getFlMora();

    void setFlMora(char flMora);

    /**
     * Nullable property for TIT-FL-MORA
     * 
     */
    Character getFlMoraObj();

    void setFlMoraObj(Character flMoraObj);

    /**
     * Host Variable TIT-ID-RAPP-RETE
     * 
     */
    int getIdRappRete();

    void setIdRappRete(int idRappRete);

    /**
     * Nullable property for TIT-ID-RAPP-RETE
     * 
     */
    Integer getIdRappReteObj();

    void setIdRappReteObj(Integer idRappReteObj);

    /**
     * Host Variable TIT-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Nullable property for TIT-ID-RAPP-ANA
     * 
     */
    Integer getIdRappAnaObj();

    void setIdRappAnaObj(Integer idRappAnaObj);

    /**
     * Host Variable TIT-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for TIT-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable TIT-DT-EMIS-TIT-DB
     * 
     */
    String getDtEmisTitDb();

    void setDtEmisTitDb(String dtEmisTitDb);

    /**
     * Nullable property for TIT-DT-EMIS-TIT-DB
     * 
     */
    String getDtEmisTitDbObj();

    void setDtEmisTitDbObj(String dtEmisTitDbObj);

    /**
     * Host Variable TIT-DT-ESI-TIT-DB
     * 
     */
    String getDtEsiTitDb();

    void setDtEsiTitDb(String dtEsiTitDb);

    /**
     * Nullable property for TIT-DT-ESI-TIT-DB
     * 
     */
    String getDtEsiTitDbObj();

    void setDtEsiTitDbObj(String dtEsiTitDbObj);

    /**
     * Host Variable TIT-TOT-PRE-NET
     * 
     */
    AfDecimal getTotPreNet();

    void setTotPreNet(AfDecimal totPreNet);

    /**
     * Nullable property for TIT-TOT-PRE-NET
     * 
     */
    AfDecimal getTotPreNetObj();

    void setTotPreNetObj(AfDecimal totPreNetObj);

    /**
     * Host Variable TIT-TOT-INTR-FRAZ
     * 
     */
    AfDecimal getTotIntrFraz();

    void setTotIntrFraz(AfDecimal totIntrFraz);

    /**
     * Nullable property for TIT-TOT-INTR-FRAZ
     * 
     */
    AfDecimal getTotIntrFrazObj();

    void setTotIntrFrazObj(AfDecimal totIntrFrazObj);

    /**
     * Host Variable TIT-TOT-INTR-MORA
     * 
     */
    AfDecimal getTotIntrMora();

    void setTotIntrMora(AfDecimal totIntrMora);

    /**
     * Nullable property for TIT-TOT-INTR-MORA
     * 
     */
    AfDecimal getTotIntrMoraObj();

    void setTotIntrMoraObj(AfDecimal totIntrMoraObj);

    /**
     * Host Variable TIT-TOT-INTR-PREST
     * 
     */
    AfDecimal getTotIntrPrest();

    void setTotIntrPrest(AfDecimal totIntrPrest);

    /**
     * Nullable property for TIT-TOT-INTR-PREST
     * 
     */
    AfDecimal getTotIntrPrestObj();

    void setTotIntrPrestObj(AfDecimal totIntrPrestObj);

    /**
     * Host Variable TIT-TOT-INTR-RETDT
     * 
     */
    AfDecimal getTotIntrRetdt();

    void setTotIntrRetdt(AfDecimal totIntrRetdt);

    /**
     * Nullable property for TIT-TOT-INTR-RETDT
     * 
     */
    AfDecimal getTotIntrRetdtObj();

    void setTotIntrRetdtObj(AfDecimal totIntrRetdtObj);

    /**
     * Host Variable TIT-TOT-INTR-RIAT
     * 
     */
    AfDecimal getTotIntrRiat();

    void setTotIntrRiat(AfDecimal totIntrRiat);

    /**
     * Nullable property for TIT-TOT-INTR-RIAT
     * 
     */
    AfDecimal getTotIntrRiatObj();

    void setTotIntrRiatObj(AfDecimal totIntrRiatObj);

    /**
     * Host Variable TIT-TOT-DIR
     * 
     */
    AfDecimal getTotDir();

    void setTotDir(AfDecimal totDir);

    /**
     * Nullable property for TIT-TOT-DIR
     * 
     */
    AfDecimal getTotDirObj();

    void setTotDirObj(AfDecimal totDirObj);

    /**
     * Host Variable TIT-TOT-SPE-MED
     * 
     */
    AfDecimal getTotSpeMed();

    void setTotSpeMed(AfDecimal totSpeMed);

    /**
     * Nullable property for TIT-TOT-SPE-MED
     * 
     */
    AfDecimal getTotSpeMedObj();

    void setTotSpeMedObj(AfDecimal totSpeMedObj);

    /**
     * Host Variable TIT-TOT-TAX
     * 
     */
    AfDecimal getTotTax();

    void setTotTax(AfDecimal totTax);

    /**
     * Nullable property for TIT-TOT-TAX
     * 
     */
    AfDecimal getTotTaxObj();

    void setTotTaxObj(AfDecimal totTaxObj);

    /**
     * Host Variable TIT-TOT-SOPR-SAN
     * 
     */
    AfDecimal getTotSoprSan();

    void setTotSoprSan(AfDecimal totSoprSan);

    /**
     * Nullable property for TIT-TOT-SOPR-SAN
     * 
     */
    AfDecimal getTotSoprSanObj();

    void setTotSoprSanObj(AfDecimal totSoprSanObj);

    /**
     * Host Variable TIT-TOT-SOPR-TEC
     * 
     */
    AfDecimal getTotSoprTec();

    void setTotSoprTec(AfDecimal totSoprTec);

    /**
     * Nullable property for TIT-TOT-SOPR-TEC
     * 
     */
    AfDecimal getTotSoprTecObj();

    void setTotSoprTecObj(AfDecimal totSoprTecObj);

    /**
     * Host Variable TIT-TOT-SOPR-SPO
     * 
     */
    AfDecimal getTotSoprSpo();

    void setTotSoprSpo(AfDecimal totSoprSpo);

    /**
     * Nullable property for TIT-TOT-SOPR-SPO
     * 
     */
    AfDecimal getTotSoprSpoObj();

    void setTotSoprSpoObj(AfDecimal totSoprSpoObj);

    /**
     * Host Variable TIT-TOT-SOPR-PROF
     * 
     */
    AfDecimal getTotSoprProf();

    void setTotSoprProf(AfDecimal totSoprProf);

    /**
     * Nullable property for TIT-TOT-SOPR-PROF
     * 
     */
    AfDecimal getTotSoprProfObj();

    void setTotSoprProfObj(AfDecimal totSoprProfObj);

    /**
     * Host Variable TIT-TOT-SOPR-ALT
     * 
     */
    AfDecimal getTotSoprAlt();

    void setTotSoprAlt(AfDecimal totSoprAlt);

    /**
     * Nullable property for TIT-TOT-SOPR-ALT
     * 
     */
    AfDecimal getTotSoprAltObj();

    void setTotSoprAltObj(AfDecimal totSoprAltObj);

    /**
     * Host Variable TIT-TOT-PRE-TOT
     * 
     */
    AfDecimal getTotPreTot();

    void setTotPreTot(AfDecimal totPreTot);

    /**
     * Nullable property for TIT-TOT-PRE-TOT
     * 
     */
    AfDecimal getTotPreTotObj();

    void setTotPreTotObj(AfDecimal totPreTotObj);

    /**
     * Host Variable TIT-TOT-PRE-PP-IAS
     * 
     */
    AfDecimal getTotPrePpIas();

    void setTotPrePpIas(AfDecimal totPrePpIas);

    /**
     * Nullable property for TIT-TOT-PRE-PP-IAS
     * 
     */
    AfDecimal getTotPrePpIasObj();

    void setTotPrePpIasObj(AfDecimal totPrePpIasObj);

    /**
     * Host Variable TIT-TOT-CAR-ACQ
     * 
     */
    AfDecimal getTotCarAcq();

    void setTotCarAcq(AfDecimal totCarAcq);

    /**
     * Nullable property for TIT-TOT-CAR-ACQ
     * 
     */
    AfDecimal getTotCarAcqObj();

    void setTotCarAcqObj(AfDecimal totCarAcqObj);

    /**
     * Host Variable TIT-TOT-CAR-GEST
     * 
     */
    AfDecimal getTotCarGest();

    void setTotCarGest(AfDecimal totCarGest);

    /**
     * Nullable property for TIT-TOT-CAR-GEST
     * 
     */
    AfDecimal getTotCarGestObj();

    void setTotCarGestObj(AfDecimal totCarGestObj);

    /**
     * Host Variable TIT-TOT-CAR-INC
     * 
     */
    AfDecimal getTotCarInc();

    void setTotCarInc(AfDecimal totCarInc);

    /**
     * Nullable property for TIT-TOT-CAR-INC
     * 
     */
    AfDecimal getTotCarIncObj();

    void setTotCarIncObj(AfDecimal totCarIncObj);

    /**
     * Host Variable TIT-TOT-PRE-SOLO-RSH
     * 
     */
    AfDecimal getTotPreSoloRsh();

    void setTotPreSoloRsh(AfDecimal totPreSoloRsh);

    /**
     * Nullable property for TIT-TOT-PRE-SOLO-RSH
     * 
     */
    AfDecimal getTotPreSoloRshObj();

    void setTotPreSoloRshObj(AfDecimal totPreSoloRshObj);

    /**
     * Host Variable TIT-TOT-PROV-ACQ-1AA
     * 
     */
    AfDecimal getTotProvAcq1aa();

    void setTotProvAcq1aa(AfDecimal totProvAcq1aa);

    /**
     * Nullable property for TIT-TOT-PROV-ACQ-1AA
     * 
     */
    AfDecimal getTotProvAcq1aaObj();

    void setTotProvAcq1aaObj(AfDecimal totProvAcq1aaObj);

    /**
     * Host Variable TIT-TOT-PROV-ACQ-2AA
     * 
     */
    AfDecimal getTotProvAcq2aa();

    void setTotProvAcq2aa(AfDecimal totProvAcq2aa);

    /**
     * Nullable property for TIT-TOT-PROV-ACQ-2AA
     * 
     */
    AfDecimal getTotProvAcq2aaObj();

    void setTotProvAcq2aaObj(AfDecimal totProvAcq2aaObj);

    /**
     * Host Variable TIT-TOT-PROV-RICOR
     * 
     */
    AfDecimal getTotProvRicor();

    void setTotProvRicor(AfDecimal totProvRicor);

    /**
     * Nullable property for TIT-TOT-PROV-RICOR
     * 
     */
    AfDecimal getTotProvRicorObj();

    void setTotProvRicorObj(AfDecimal totProvRicorObj);

    /**
     * Host Variable TIT-TOT-PROV-INC
     * 
     */
    AfDecimal getTotProvInc();

    void setTotProvInc(AfDecimal totProvInc);

    /**
     * Nullable property for TIT-TOT-PROV-INC
     * 
     */
    AfDecimal getTotProvIncObj();

    void setTotProvIncObj(AfDecimal totProvIncObj);

    /**
     * Host Variable TIT-TOT-PROV-DA-REC
     * 
     */
    AfDecimal getTotProvDaRec();

    void setTotProvDaRec(AfDecimal totProvDaRec);

    /**
     * Nullable property for TIT-TOT-PROV-DA-REC
     * 
     */
    AfDecimal getTotProvDaRecObj();

    void setTotProvDaRecObj(AfDecimal totProvDaRecObj);

    /**
     * Host Variable TIT-IMP-AZ
     * 
     */
    AfDecimal getImpAz();

    void setImpAz(AfDecimal impAz);

    /**
     * Nullable property for TIT-IMP-AZ
     * 
     */
    AfDecimal getImpAzObj();

    void setImpAzObj(AfDecimal impAzObj);

    /**
     * Host Variable TIT-IMP-ADER
     * 
     */
    AfDecimal getImpAder();

    void setImpAder(AfDecimal impAder);

    /**
     * Nullable property for TIT-IMP-ADER
     * 
     */
    AfDecimal getImpAderObj();

    void setImpAderObj(AfDecimal impAderObj);

    /**
     * Host Variable TIT-IMP-TFR
     * 
     */
    AfDecimal getImpTfr();

    void setImpTfr(AfDecimal impTfr);

    /**
     * Nullable property for TIT-IMP-TFR
     * 
     */
    AfDecimal getImpTfrObj();

    void setImpTfrObj(AfDecimal impTfrObj);

    /**
     * Host Variable TIT-IMP-VOLO
     * 
     */
    AfDecimal getImpVolo();

    void setImpVolo(AfDecimal impVolo);

    /**
     * Nullable property for TIT-IMP-VOLO
     * 
     */
    AfDecimal getImpVoloObj();

    void setImpVoloObj(AfDecimal impVoloObj);

    /**
     * Host Variable TIT-TOT-MANFEE-ANTIC
     * 
     */
    AfDecimal getTotManfeeAntic();

    void setTotManfeeAntic(AfDecimal totManfeeAntic);

    /**
     * Nullable property for TIT-TOT-MANFEE-ANTIC
     * 
     */
    AfDecimal getTotManfeeAnticObj();

    void setTotManfeeAnticObj(AfDecimal totManfeeAnticObj);

    /**
     * Host Variable TIT-TOT-MANFEE-RICOR
     * 
     */
    AfDecimal getTotManfeeRicor();

    void setTotManfeeRicor(AfDecimal totManfeeRicor);

    /**
     * Nullable property for TIT-TOT-MANFEE-RICOR
     * 
     */
    AfDecimal getTotManfeeRicorObj();

    void setTotManfeeRicorObj(AfDecimal totManfeeRicorObj);

    /**
     * Host Variable TIT-TOT-MANFEE-REC
     * 
     */
    AfDecimal getTotManfeeRec();

    void setTotManfeeRec(AfDecimal totManfeeRec);

    /**
     * Nullable property for TIT-TOT-MANFEE-REC
     * 
     */
    AfDecimal getTotManfeeRecObj();

    void setTotManfeeRecObj(AfDecimal totManfeeRecObj);

    /**
     * Host Variable TIT-TP-MEZ-PAG-ADD
     * 
     */
    String getTpMezPagAdd();

    void setTpMezPagAdd(String tpMezPagAdd);

    /**
     * Nullable property for TIT-TP-MEZ-PAG-ADD
     * 
     */
    String getTpMezPagAddObj();

    void setTpMezPagAddObj(String tpMezPagAddObj);

    /**
     * Host Variable TIT-ESTR-CNT-CORR-ADD
     * 
     */
    String getEstrCntCorrAdd();

    void setEstrCntCorrAdd(String estrCntCorrAdd);

    /**
     * Nullable property for TIT-ESTR-CNT-CORR-ADD
     * 
     */
    String getEstrCntCorrAddObj();

    void setEstrCntCorrAddObj(String estrCntCorrAddObj);

    /**
     * Host Variable TIT-DT-VLT-DB
     * 
     */
    String getDtVltDb();

    void setDtVltDb(String dtVltDb);

    /**
     * Nullable property for TIT-DT-VLT-DB
     * 
     */
    String getDtVltDbObj();

    void setDtVltDbObj(String dtVltDbObj);

    /**
     * Host Variable TIT-FL-FORZ-DT-VLT
     * 
     */
    char getFlForzDtVlt();

    void setFlForzDtVlt(char flForzDtVlt);

    /**
     * Nullable property for TIT-FL-FORZ-DT-VLT
     * 
     */
    Character getFlForzDtVltObj();

    void setFlForzDtVltObj(Character flForzDtVltObj);

    /**
     * Host Variable TIT-DT-CAMBIO-VLT-DB
     * 
     */
    String getDtCambioVltDb();

    void setDtCambioVltDb(String dtCambioVltDb);

    /**
     * Nullable property for TIT-DT-CAMBIO-VLT-DB
     * 
     */
    String getDtCambioVltDbObj();

    void setDtCambioVltDbObj(String dtCambioVltDbObj);

    /**
     * Host Variable TIT-TOT-SPE-AGE
     * 
     */
    AfDecimal getTotSpeAge();

    void setTotSpeAge(AfDecimal totSpeAge);

    /**
     * Nullable property for TIT-TOT-SPE-AGE
     * 
     */
    AfDecimal getTotSpeAgeObj();

    void setTotSpeAgeObj(AfDecimal totSpeAgeObj);

    /**
     * Host Variable TIT-TOT-CAR-IAS
     * 
     */
    AfDecimal getTotCarIas();

    void setTotCarIas(AfDecimal totCarIas);

    /**
     * Nullable property for TIT-TOT-CAR-IAS
     * 
     */
    AfDecimal getTotCarIasObj();

    void setTotCarIasObj(AfDecimal totCarIasObj);

    /**
     * Host Variable TIT-NUM-RAT-ACCORPATE
     * 
     */
    int getNumRatAccorpate();

    void setNumRatAccorpate(int numRatAccorpate);

    /**
     * Nullable property for TIT-NUM-RAT-ACCORPATE
     * 
     */
    Integer getNumRatAccorpateObj();

    void setNumRatAccorpateObj(Integer numRatAccorpateObj);

    /**
     * Host Variable TIT-DS-RIGA
     * 
     */
    long getTitDsRiga();

    void setTitDsRiga(long titDsRiga);

    /**
     * Host Variable TIT-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable TIT-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable TIT-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable TIT-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable TIT-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable TIT-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable TIT-FL-TIT-DA-REINVST
     * 
     */
    char getFlTitDaReinvst();

    void setFlTitDaReinvst(char flTitDaReinvst);

    /**
     * Nullable property for TIT-FL-TIT-DA-REINVST
     * 
     */
    Character getFlTitDaReinvstObj();

    void setFlTitDaReinvstObj(Character flTitDaReinvstObj);

    /**
     * Host Variable TIT-DT-RICH-ADD-RID-DB
     * 
     */
    String getDtRichAddRidDb();

    void setDtRichAddRidDb(String dtRichAddRidDb);

    /**
     * Nullable property for TIT-DT-RICH-ADD-RID-DB
     * 
     */
    String getDtRichAddRidDbObj();

    void setDtRichAddRidDbObj(String dtRichAddRidDbObj);

    /**
     * Host Variable TIT-TP-ESI-RID
     * 
     */
    String getTpEsiRid();

    void setTpEsiRid(String tpEsiRid);

    /**
     * Nullable property for TIT-TP-ESI-RID
     * 
     */
    String getTpEsiRidObj();

    void setTpEsiRidObj(String tpEsiRidObj);

    /**
     * Host Variable TIT-COD-IBAN
     * 
     */
    String getCodIban();

    void setCodIban(String codIban);

    /**
     * Nullable property for TIT-COD-IBAN
     * 
     */
    String getCodIbanObj();

    void setCodIbanObj(String codIbanObj);

    /**
     * Host Variable TIT-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfe();

    void setImpTrasfe(AfDecimal impTrasfe);

    /**
     * Nullable property for TIT-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfeObj();

    void setImpTrasfeObj(AfDecimal impTrasfeObj);

    /**
     * Host Variable TIT-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrc();

    void setImpTfrStrc(AfDecimal impTfrStrc);

    /**
     * Nullable property for TIT-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrcObj();

    void setImpTfrStrcObj(AfDecimal impTfrStrcObj);

    /**
     * Host Variable TIT-DT-CERT-FISC-DB
     * 
     */
    String getDtCertFiscDb();

    void setDtCertFiscDb(String dtCertFiscDb);

    /**
     * Nullable property for TIT-DT-CERT-FISC-DB
     * 
     */
    String getDtCertFiscDbObj();

    void setDtCertFiscDbObj(String dtCertFiscDbObj);

    /**
     * Host Variable TIT-TP-CAUS-STOR
     * 
     */
    int getTpCausStor();

    void setTpCausStor(int tpCausStor);

    /**
     * Nullable property for TIT-TP-CAUS-STOR
     * 
     */
    Integer getTpCausStorObj();

    void setTpCausStorObj(Integer tpCausStorObj);

    /**
     * Host Variable TIT-TP-CAUS-DISP-STOR
     * 
     */
    String getTpCausDispStor();

    void setTpCausDispStor(String tpCausDispStor);

    /**
     * Nullable property for TIT-TP-CAUS-DISP-STOR
     * 
     */
    String getTpCausDispStorObj();

    void setTpCausDispStorObj(String tpCausDispStorObj);

    /**
     * Host Variable TIT-TP-TIT-MIGRAZ
     * 
     */
    String getTpTitMigraz();

    void setTpTitMigraz(String tpTitMigraz);

    /**
     * Nullable property for TIT-TP-TIT-MIGRAZ
     * 
     */
    String getTpTitMigrazObj();

    void setTpTitMigrazObj(String tpTitMigrazObj);

    /**
     * Host Variable TIT-TOT-ACQ-EXP
     * 
     */
    AfDecimal getTotAcqExp();

    void setTotAcqExp(AfDecimal totAcqExp);

    /**
     * Nullable property for TIT-TOT-ACQ-EXP
     * 
     */
    AfDecimal getTotAcqExpObj();

    void setTotAcqExpObj(AfDecimal totAcqExpObj);

    /**
     * Host Variable TIT-TOT-REMUN-ASS
     * 
     */
    AfDecimal getTotRemunAss();

    void setTotRemunAss(AfDecimal totRemunAss);

    /**
     * Nullable property for TIT-TOT-REMUN-ASS
     * 
     */
    AfDecimal getTotRemunAssObj();

    void setTotRemunAssObj(AfDecimal totRemunAssObj);

    /**
     * Host Variable TIT-TOT-COMMIS-INTER
     * 
     */
    AfDecimal getTotCommisInter();

    void setTotCommisInter(AfDecimal totCommisInter);

    /**
     * Nullable property for TIT-TOT-COMMIS-INTER
     * 
     */
    AfDecimal getTotCommisInterObj();

    void setTotCommisInterObj(AfDecimal totCommisInterObj);

    /**
     * Host Variable TIT-TP-CAUS-RIMB
     * 
     */
    String getTpCausRimb();

    void setTpCausRimb(String tpCausRimb);

    /**
     * Nullable property for TIT-TP-CAUS-RIMB
     * 
     */
    String getTpCausRimbObj();

    void setTpCausRimbObj(String tpCausRimbObj);

    /**
     * Host Variable TIT-TOT-CNBT-ANTIRAC
     * 
     */
    AfDecimal getTotCnbtAntirac();

    void setTotCnbtAntirac(AfDecimal totCnbtAntirac);

    /**
     * Nullable property for TIT-TOT-CNBT-ANTIRAC
     * 
     */
    AfDecimal getTotCnbtAntiracObj();

    void setTotCnbtAntiracObj(AfDecimal totCnbtAntiracObj);

    /**
     * Host Variable TIT-FL-INC-AUTOGEN
     * 
     */
    char getFlIncAutogen();

    void setFlIncAutogen(char flIncAutogen);

    /**
     * Nullable property for TIT-FL-INC-AUTOGEN
     * 
     */
    Character getFlIncAutogenObj();

    void setFlIncAutogenObj(Character flIncAutogenObj);

    /**
     * Host Variable LDBV1591-IMP-TOT
     * 
     */
    AfDecimal getLdbv1591ImpTot();

    void setLdbv1591ImpTot(AfDecimal ldbv1591ImpTot);

    /**
     * Host Variable LDBV1591-ID-OGG
     * 
     */
    int getLdbv1591IdOgg();

    void setLdbv1591IdOgg(int ldbv1591IdOgg);

    /**
     * Host Variable LDBV1591-TP-OGG
     * 
     */
    String getLdbv1591TpOgg();

    void setLdbv1591TpOgg(String ldbv1591TpOgg);

    /**
     * Host Variable ISO-DT-INI-PER-DB
     * 
     */
    String getIsoDtIniPerDb();

    void setIsoDtIniPerDb(String isoDtIniPerDb);

    /**
     * Host Variable LDBV2091-TOT-PREMI
     * 
     */
    AfDecimal getLdbv2091TotPremi();

    void setLdbv2091TotPremi(AfDecimal ldbv2091TotPremi);

    /**
     * Host Variable LDBV2091-ID-OGG
     * 
     */
    int getLdbv2091IdOgg();

    void setLdbv2091IdOgg(int ldbv2091IdOgg);

    /**
     * Host Variable LDBV2091-TP-OGG
     * 
     */
    String getLdbv2091TpOgg();

    void setLdbv2091TpOgg(String ldbv2091TpOgg);

    /**
     * Host Variable LDBV2091-TP-STAT-TIT-01
     * 
     */
    String getLdbv2091TpStatTit01();

    void setLdbv2091TpStatTit01(String ldbv2091TpStatTit01);

    /**
     * Host Variable LDBV2091-TP-STAT-TIT-02
     * 
     */
    String getLdbv2091TpStatTit02();

    void setLdbv2091TpStatTit02(String ldbv2091TpStatTit02);

    /**
     * Host Variable LDBV2091-TP-STAT-TIT-03
     * 
     */
    String getLdbv2091TpStatTit03();

    void setLdbv2091TpStatTit03(String ldbv2091TpStatTit03);

    /**
     * Host Variable LDBV2091-TP-STAT-TIT-04
     * 
     */
    String getLdbv2091TpStatTit04();

    void setLdbv2091TpStatTit04(String ldbv2091TpStatTit04);

    /**
     * Host Variable LDBV2091-TP-STAT-TIT-05
     * 
     */
    String getLdbv2091TpStatTit05();

    void setLdbv2091TpStatTit05(String ldbv2091TpStatTit05);

    /**
     * Host Variable LDBV6151-DT-MAX-DB
     * 
     */
    String getLdbv6151DtMaxDb();

    void setLdbv6151DtMaxDb(String ldbv6151DtMaxDb);

    /**
     * Nullable property for LDBV6151-DT-MAX-DB
     * 
     */
    String getLdbv6151DtMaxDbObj();

    void setLdbv6151DtMaxDbObj(String ldbv6151DtMaxDbObj);

    /**
     * Host Variable LDBV6151-ID-OGG
     * 
     */
    int getLdbv6151IdOgg();

    void setLdbv6151IdOgg(int ldbv6151IdOgg);

    /**
     * Host Variable LDBV6151-TP-OGG
     * 
     */
    String getLdbv6151TpOgg();

    void setLdbv6151TpOgg(String ldbv6151TpOgg);

    /**
     * Host Variable LDBV6151-TP-TIT-01
     * 
     */
    String getLdbv6151TpTit01();

    void setLdbv6151TpTit01(String ldbv6151TpTit01);

    /**
     * Host Variable LDBV6151-TP-TIT-02
     * 
     */
    String getLdbv6151TpTit02();

    void setLdbv6151TpTit02(String ldbv6151TpTit02);

    /**
     * Host Variable LDBV6151-DT-DECOR-PREST-DB
     * 
     */
    String getLdbv6151DtDecorPrestDb();

    void setLdbv6151DtDecorPrestDb(String ldbv6151DtDecorPrestDb);

    /**
     * Host Variable LDBV6151-TP-STAT-TIT
     * 
     */
    String getLdbv6151TpStatTit();

    void setLdbv6151TpStatTit(String ldbv6151TpStatTit);

    /**
     * Host Variable LDBVB441-DT-MAX-DB
     * 
     */
    String getLdbvb441DtMaxDb();

    void setLdbvb441DtMaxDb(String ldbvb441DtMaxDb);

    /**
     * Nullable property for LDBVB441-DT-MAX-DB
     * 
     */
    String getLdbvb441DtMaxDbObj();

    void setLdbvb441DtMaxDbObj(String ldbvb441DtMaxDbObj);

    /**
     * Host Variable LDBVB441-TP-TIT-01
     * 
     */
    String getLdbvb441TpTit01();

    void setLdbvb441TpTit01(String ldbvb441TpTit01);

    /**
     * Host Variable LDBVB441-TP-TIT-02
     * 
     */
    String getLdbvb441TpTit02();

    void setLdbvb441TpTit02(String ldbvb441TpTit02);

    /**
     * Host Variable LDBVB441-TP-STAT-TIT-1
     * 
     */
    String getLdbvb441TpStatTit1();

    void setLdbvb441TpStatTit1(String ldbvb441TpStatTit1);

    /**
     * Host Variable LDBVB441-TP-STAT-TIT-2
     * 
     */
    String getLdbvb441TpStatTit2();

    void setLdbvb441TpStatTit2(String ldbvb441TpStatTit2);

    /**
     * Host Variable LDBVB441-TP-STAT-TIT-3
     * 
     */
    String getLdbvb441TpStatTit3();

    void setLdbvb441TpStatTit3(String ldbvb441TpStatTit3);

    /**
     * Host Variable LDBVB441-TP-OGG
     * 
     */
    String getLdbvb441TpOgg();

    void setLdbvb441TpOgg(String ldbvb441TpOgg);

    /**
     * Host Variable LDBVB441-ID-OGG
     * 
     */
    int getLdbvb441IdOgg();

    void setLdbvb441IdOgg(int ldbvb441IdOgg);

    /**
     * Host Variable LDBVB471-TP-TIT-01
     * 
     */
    String getLdbvb471TpTit01();

    void setLdbvb471TpTit01(String ldbvb471TpTit01);

    /**
     * Host Variable LDBVB471-TP-TIT-02
     * 
     */
    String getLdbvb471TpTit02();

    void setLdbvb471TpTit02(String ldbvb471TpTit02);

    /**
     * Host Variable LDBVB471-TP-STAT-TIT-1
     * 
     */
    String getLdbvb471TpStatTit1();

    void setLdbvb471TpStatTit1(String ldbvb471TpStatTit1);

    /**
     * Host Variable LDBVB471-TP-STAT-TIT-2
     * 
     */
    String getLdbvb471TpStatTit2();

    void setLdbvb471TpStatTit2(String ldbvb471TpStatTit2);

    /**
     * Host Variable LDBVB471-TP-STAT-TIT-3
     * 
     */
    String getLdbvb471TpStatTit3();

    void setLdbvb471TpStatTit3(String ldbvb471TpStatTit3);

    /**
     * Host Variable LDBVB471-DT-MAX-DB
     * 
     */
    String getLdbvb471DtMaxDb();

    void setLdbvb471DtMaxDb(String ldbvb471DtMaxDb);

    /**
     * Host Variable LDBVB471-ID-OGG
     * 
     */
    int getLdbvb471IdOgg();

    void setLdbvb471IdOgg(int ldbvb471IdOgg);

    /**
     * Host Variable LDBVB471-TP-OGG
     * 
     */
    String getLdbvb471TpOgg();

    void setLdbvb471TpOgg(String ldbvb471TpOgg);

    /**
     * Host Variable LDBVF111-CUM-PRE-VERS
     * 
     */
    AfDecimal getLdbvf111CumPreVers();

    void setLdbvf111CumPreVers(AfDecimal ldbvf111CumPreVers);

    /**
     * Host Variable LDBVF111-ID-OGG
     * 
     */
    int getLdbvf111IdOgg();

    void setLdbvf111IdOgg(int ldbvf111IdOgg);

    /**
     * Host Variable LDBVF111-TP-STAT-TIT1
     * 
     */
    String getLdbvf111TpStatTit1();

    void setLdbvf111TpStatTit1(String ldbvf111TpStatTit1);

    /**
     * Host Variable LDBVF111-TP-STAT-TIT2
     * 
     */
    String getLdbvf111TpStatTit2();

    void setLdbvf111TpStatTit2(String ldbvf111TpStatTit2);

    /**
     * Host Variable LDBVF111-TP-STAT-TIT3
     * 
     */
    String getLdbvf111TpStatTit3();

    void setLdbvf111TpStatTit3(String ldbvf111TpStatTit3);

    /**
     * Host Variable LDBVF111-TP-STAT-TIT4
     * 
     */
    String getLdbvf111TpStatTit4();

    void setLdbvf111TpStatTit4(String ldbvf111TpStatTit4);

    /**
     * Host Variable LDBVF111-TP-STAT-TIT5
     * 
     */
    String getLdbvf111TpStatTit5();

    void setLdbvf111TpStatTit5(String ldbvf111TpStatTit5);
};
