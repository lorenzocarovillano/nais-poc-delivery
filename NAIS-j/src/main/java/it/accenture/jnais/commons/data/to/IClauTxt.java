package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [CLAU_TXT]
 * 
 */
public interface IClauTxt extends BaseSqlTo {

    /**
     * Host Variable CLT-ID-OGG
     * 
     */
    int getCltIdOgg();

    void setCltIdOgg(int cltIdOgg);

    /**
     * Host Variable CLT-TP-OGG
     * 
     */
    String getCltTpOgg();

    void setCltTpOgg(String cltTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable CLT-ID-CLAU-TXT
     * 
     */
    int getIdClauTxt();

    void setIdClauTxt(int idClauTxt);

    /**
     * Host Variable CLT-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable CLT-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for CLT-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable CLT-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable CLT-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable CLT-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable CLT-TP-CLAU
     * 
     */
    String getTpClau();

    void setTpClau(String tpClau);

    /**
     * Nullable property for CLT-TP-CLAU
     * 
     */
    String getTpClauObj();

    void setTpClauObj(String tpClauObj);

    /**
     * Host Variable CLT-COD-CLAU
     * 
     */
    String getCodClau();

    void setCodClau(String codClau);

    /**
     * Nullable property for CLT-COD-CLAU
     * 
     */
    String getCodClauObj();

    void setCodClauObj(String codClauObj);

    /**
     * Host Variable CLT-DESC-BREVE
     * 
     */
    String getDescBreve();

    void setDescBreve(String descBreve);

    /**
     * Nullable property for CLT-DESC-BREVE
     * 
     */
    String getDescBreveObj();

    void setDescBreveObj(String descBreveObj);

    /**
     * Host Variable CLT-DESC-LNG-VCHAR
     * 
     */
    String getDescLngVchar();

    void setDescLngVchar(String descLngVchar);

    /**
     * Nullable property for CLT-DESC-LNG-VCHAR
     * 
     */
    String getDescLngVcharObj();

    void setDescLngVcharObj(String descLngVcharObj);

    /**
     * Host Variable CLT-DS-RIGA
     * 
     */
    long getCltDsRiga();

    void setCltDsRiga(long cltDsRiga);

    /**
     * Host Variable CLT-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable CLT-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable CLT-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable CLT-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable CLT-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable CLT-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
