package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [RICH_EST]
 * 
 */
public interface IRichEst extends BaseSqlTo {

    /**
     * Host Variable P01-ID-RICH-EST
     * 
     */
    int getP01IdRichEst();

    void setP01IdRichEst(int p01IdRichEst);

    /**
     * Host Variable P01-ID-RICH-EST-COLLG
     * 
     */
    int getIdRichEstCollg();

    void setIdRichEstCollg(int idRichEstCollg);

    /**
     * Nullable property for P01-ID-RICH-EST-COLLG
     * 
     */
    Integer getIdRichEstCollgObj();

    void setIdRichEstCollgObj(Integer idRichEstCollgObj);

    /**
     * Host Variable P01-ID-LIQ
     * 
     */
    int getIdLiq();

    void setIdLiq(int idLiq);

    /**
     * Nullable property for P01-ID-LIQ
     * 
     */
    Integer getIdLiqObj();

    void setIdLiqObj(Integer idLiqObj);

    /**
     * Host Variable P01-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P01-IB-RICH-EST
     * 
     */
    String getIbRichEst();

    void setIbRichEst(String ibRichEst);

    /**
     * Nullable property for P01-IB-RICH-EST
     * 
     */
    String getIbRichEstObj();

    void setIbRichEstObj(String ibRichEstObj);

    /**
     * Host Variable P01-TP-MOVI
     * 
     */
    int getTpMovi();

    void setTpMovi(int tpMovi);

    /**
     * Host Variable P01-DT-FORM-RICH-DB
     * 
     */
    String getDtFormRichDb();

    void setDtFormRichDb(String dtFormRichDb);

    /**
     * Host Variable P01-DT-INVIO-RICH-DB
     * 
     */
    String getDtInvioRichDb();

    void setDtInvioRichDb(String dtInvioRichDb);

    /**
     * Host Variable P01-DT-PERV-RICH-DB
     * 
     */
    String getDtPervRichDb();

    void setDtPervRichDb(String dtPervRichDb);

    /**
     * Host Variable P01-DT-RGSTRZ-RICH-DB
     * 
     */
    String getDtRgstrzRichDb();

    void setDtRgstrzRichDb(String dtRgstrzRichDb);

    /**
     * Host Variable P01-DT-EFF-DB
     * 
     */
    String getDtEffDb();

    void setDtEffDb(String dtEffDb);

    /**
     * Nullable property for P01-DT-EFF-DB
     * 
     */
    String getDtEffDbObj();

    void setDtEffDbObj(String dtEffDbObj);

    /**
     * Host Variable P01-DT-SIN-DB
     * 
     */
    String getDtSinDb();

    void setDtSinDb(String dtSinDb);

    /**
     * Nullable property for P01-DT-SIN-DB
     * 
     */
    String getDtSinDbObj();

    void setDtSinDbObj(String dtSinDbObj);

    /**
     * Host Variable P01-TP-OGG
     * 
     */
    String getTpOgg();

    void setTpOgg(String tpOgg);

    /**
     * Host Variable P01-ID-OGG
     * 
     */
    int getIdOgg();

    void setIdOgg(int idOgg);

    /**
     * Host Variable P01-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Host Variable P01-FL-MOD-EXEC
     * 
     */
    char getFlModExec();

    void setFlModExec(char flModExec);

    /**
     * Host Variable P01-ID-RICHIEDENTE
     * 
     */
    int getIdRichiedente();

    void setIdRichiedente(int idRichiedente);

    /**
     * Nullable property for P01-ID-RICHIEDENTE
     * 
     */
    Integer getIdRichiedenteObj();

    void setIdRichiedenteObj(Integer idRichiedenteObj);

    /**
     * Host Variable P01-COD-PROD
     * 
     */
    String getCodProd();

    void setCodProd(String codProd);

    /**
     * Host Variable P01-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P01-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P01-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable P01-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P01-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable P01-COD-CAN
     * 
     */
    int getCodCan();

    void setCodCan(int codCan);

    /**
     * Host Variable P01-IB-OGG-ORIG
     * 
     */
    String getIbOggOrig();

    void setIbOggOrig(String ibOggOrig);

    /**
     * Nullable property for P01-IB-OGG-ORIG
     * 
     */
    String getIbOggOrigObj();

    void setIbOggOrigObj(String ibOggOrigObj);

    /**
     * Host Variable P01-DT-EST-FINANZ-DB
     * 
     */
    String getDtEstFinanzDb();

    void setDtEstFinanzDb(String dtEstFinanzDb);

    /**
     * Nullable property for P01-DT-EST-FINANZ-DB
     * 
     */
    String getDtEstFinanzDbObj();

    void setDtEstFinanzDbObj(String dtEstFinanzDbObj);

    /**
     * Host Variable P01-DT-MAN-COP-DB
     * 
     */
    String getDtManCopDb();

    void setDtManCopDb(String dtManCopDb);

    /**
     * Nullable property for P01-DT-MAN-COP-DB
     * 
     */
    String getDtManCopDbObj();

    void setDtManCopDbObj(String dtManCopDbObj);

    /**
     * Host Variable P01-FL-GEST-PROTEZIONE
     * 
     */
    char getFlGestProtezione();

    void setFlGestProtezione(char flGestProtezione);

    /**
     * Nullable property for P01-FL-GEST-PROTEZIONE
     * 
     */
    Character getFlGestProtezioneObj();

    void setFlGestProtezioneObj(Character flGestProtezioneObj);
};
