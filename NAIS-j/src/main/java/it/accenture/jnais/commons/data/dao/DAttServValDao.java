package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDAttServVal;

/**
 * Data Access Object(DAO) for table [D_ATT_SERV_VAL]
 * 
 */
public class DAttServValDao extends BaseSqlDao<IDAttServVal> {

    private Cursor cIdUpdEffP89;
    private Cursor cIdoEffP89;
    private Cursor cIdoCpzP89;
    private final IRowMapper<IDAttServVal> selectByP89DsRigaRm = buildNamedRowMapper(IDAttServVal.class, "idDAttServVal", "codCompAnia", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "idAttServVal", "tpServVal", "codFnd", "dtIniCntrlFndDbObj", "p89IdOgg", "p89TpOgg", "p89DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public DAttServValDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDAttServVal> getToClass() {
        return IDAttServVal.class;
    }

    public IDAttServVal selectByP89DsRiga(long p89DsRiga, IDAttServVal iDAttServVal) {
        return buildQuery("selectByP89DsRiga").bind("p89DsRiga", p89DsRiga).rowMapper(selectByP89DsRigaRm).singleResult(iDAttServVal);
    }

    public DbAccessStatus insertRec(IDAttServVal iDAttServVal) {
        return buildQuery("insertRec").bind(iDAttServVal).executeInsert();
    }

    public DbAccessStatus updateRec(IDAttServVal iDAttServVal) {
        return buildQuery("updateRec").bind(iDAttServVal).executeUpdate();
    }

    public DbAccessStatus deleteByP89DsRiga(long p89DsRiga) {
        return buildQuery("deleteByP89DsRiga").bind("p89DsRiga", p89DsRiga).executeDelete();
    }

    public IDAttServVal selectRec(int p89IdDAttServVal, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDAttServVal iDAttServVal) {
        return buildQuery("selectRec").bind("p89IdDAttServVal", p89IdDAttServVal).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP89DsRigaRm).singleResult(iDAttServVal);
    }

    public DbAccessStatus updateRec1(IDAttServVal iDAttServVal) {
        return buildQuery("updateRec1").bind(iDAttServVal).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffP89(int p89IdDAttServVal, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffP89 = buildQuery("openCIdUpdEffP89").bind("p89IdDAttServVal", p89IdDAttServVal).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffP89() {
        return closeCursor(cIdUpdEffP89);
    }

    public IDAttServVal fetchCIdUpdEffP89(IDAttServVal iDAttServVal) {
        return fetch(cIdUpdEffP89, iDAttServVal, selectByP89DsRigaRm);
    }

    public IDAttServVal selectRec1(int p89IdOgg, String p89TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDAttServVal iDAttServVal) {
        return buildQuery("selectRec1").bind("p89IdOgg", p89IdOgg).bind("p89TpOgg", p89TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP89DsRigaRm).singleResult(iDAttServVal);
    }

    public DbAccessStatus openCIdoEffP89(int p89IdOgg, String p89TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffP89 = buildQuery("openCIdoEffP89").bind("p89IdOgg", p89IdOgg).bind("p89TpOgg", p89TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffP89() {
        return closeCursor(cIdoEffP89);
    }

    public IDAttServVal fetchCIdoEffP89(IDAttServVal iDAttServVal) {
        return fetch(cIdoEffP89, iDAttServVal, selectByP89DsRigaRm);
    }

    public IDAttServVal selectRec2(int p89IdDAttServVal, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDAttServVal iDAttServVal) {
        return buildQuery("selectRec2").bind("p89IdDAttServVal", p89IdDAttServVal).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP89DsRigaRm).singleResult(iDAttServVal);
    }

    public IDAttServVal selectRec3(IDAttServVal iDAttServVal) {
        return buildQuery("selectRec3").bind(iDAttServVal).rowMapper(selectByP89DsRigaRm).singleResult(iDAttServVal);
    }

    public DbAccessStatus openCIdoCpzP89(IDAttServVal iDAttServVal) {
        cIdoCpzP89 = buildQuery("openCIdoCpzP89").bind(iDAttServVal).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzP89() {
        return closeCursor(cIdoCpzP89);
    }

    public IDAttServVal fetchCIdoCpzP89(IDAttServVal iDAttServVal) {
        return fetch(cIdoCpzP89, iDAttServVal, selectByP89DsRigaRm);
    }
}
