package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [STAT_RICH_EST]
 * 
 */
public interface IStatRichEst extends BaseSqlTo {

    /**
     * Host Variable P04-ID-STAT-RICH-EST
     * 
     */
    int getP04IdStatRichEst();

    void setP04IdStatRichEst(int p04IdStatRichEst);

    /**
     * Host Variable P04-ID-RICH-EST
     * 
     */
    int getIdRichEst();

    void setIdRichEst(int idRichEst);

    /**
     * Host Variable P04-TS-INI-VLDT
     * 
     */
    long getTsIniVldt();

    void setTsIniVldt(long tsIniVldt);

    /**
     * Host Variable P04-TS-END-VLDT
     * 
     */
    long getTsEndVldt();

    void setTsEndVldt(long tsEndVldt);

    /**
     * Host Variable P04-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P04-COD-PRCS
     * 
     */
    String getCodPrcs();

    void setCodPrcs(String codPrcs);

    /**
     * Host Variable P04-COD-ATTVT
     * 
     */
    String getCodAttvt();

    void setCodAttvt(String codAttvt);

    /**
     * Host Variable P04-STAT-RICH-EST
     * 
     */
    String getStatRichEst();

    void setStatRichEst(String statRichEst);

    /**
     * Host Variable P04-FL-STAT-END
     * 
     */
    char getFlStatEnd();

    void setFlStatEnd(char flStatEnd);

    /**
     * Nullable property for P04-FL-STAT-END
     * 
     */
    Character getFlStatEndObj();

    void setFlStatEndObj(Character flStatEndObj);

    /**
     * Host Variable P04-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P04-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P04-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable P04-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P04-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable P04-TP-CAUS-SCARTO
     * 
     */
    String getTpCausScarto();

    void setTpCausScarto(String tpCausScarto);

    /**
     * Nullable property for P04-TP-CAUS-SCARTO
     * 
     */
    String getTpCausScartoObj();

    void setTpCausScartoObj(String tpCausScartoObj);

    /**
     * Host Variable P04-DESC-ERR-VCHAR
     * 
     */
    String getDescErrVchar();

    void setDescErrVchar(String descErrVchar);

    /**
     * Nullable property for P04-DESC-ERR-VCHAR
     * 
     */
    String getDescErrVcharObj();

    void setDescErrVcharObj(String descErrVcharObj);

    /**
     * Host Variable P04-UTENTE-INS-AGG
     * 
     */
    String getUtenteInsAgg();

    void setUtenteInsAgg(String utenteInsAgg);

    /**
     * Host Variable P04-COD-ERR-SCARTO
     * 
     */
    String getCodErrScarto();

    void setCodErrScarto(String codErrScarto);

    /**
     * Nullable property for P04-COD-ERR-SCARTO
     * 
     */
    String getCodErrScartoObj();

    void setCodErrScartoObj(String codErrScartoObj);

    /**
     * Host Variable P04-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Nullable property for P04-ID-MOVI-CRZ
     * 
     */
    Integer getIdMoviCrzObj();

    void setIdMoviCrzObj(Integer idMoviCrzObj);
};
