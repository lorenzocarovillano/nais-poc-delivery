package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ADES]
 * 
 */
public interface IAdes extends BaseSqlTo {

    /**
     * Host Variable ADE-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Host Variable ADE-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable ADE-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable ADE-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for ADE-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable ADE-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable ADE-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable ADE-IB-PREV
     * 
     */
    String getIbPrev();

    void setIbPrev(String ibPrev);

    /**
     * Nullable property for ADE-IB-PREV
     * 
     */
    String getIbPrevObj();

    void setIbPrevObj(String ibPrevObj);

    /**
     * Host Variable ADE-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for ADE-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable ADE-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable ADE-DT-DECOR-DB
     * 
     */
    String getDtDecorDb();

    void setDtDecorDb(String dtDecorDb);

    /**
     * Nullable property for ADE-DT-DECOR-DB
     * 
     */
    String getDtDecorDbObj();

    void setDtDecorDbObj(String dtDecorDbObj);

    /**
     * Host Variable ADE-DT-SCAD-DB
     * 
     */
    String getDtScadDb();

    void setDtScadDb(String dtScadDb);

    /**
     * Nullable property for ADE-DT-SCAD-DB
     * 
     */
    String getDtScadDbObj();

    void setDtScadDbObj(String dtScadDbObj);

    /**
     * Host Variable ADE-ETA-A-SCAD
     * 
     */
    int getEtaAScad();

    void setEtaAScad(int etaAScad);

    /**
     * Nullable property for ADE-ETA-A-SCAD
     * 
     */
    Integer getEtaAScadObj();

    void setEtaAScadObj(Integer etaAScadObj);

    /**
     * Host Variable ADE-DUR-AA
     * 
     */
    int getDurAa();

    void setDurAa(int durAa);

    /**
     * Nullable property for ADE-DUR-AA
     * 
     */
    Integer getDurAaObj();

    void setDurAaObj(Integer durAaObj);

    /**
     * Host Variable ADE-DUR-MM
     * 
     */
    int getDurMm();

    void setDurMm(int durMm);

    /**
     * Nullable property for ADE-DUR-MM
     * 
     */
    Integer getDurMmObj();

    void setDurMmObj(Integer durMmObj);

    /**
     * Host Variable ADE-DUR-GG
     * 
     */
    int getDurGg();

    void setDurGg(int durGg);

    /**
     * Nullable property for ADE-DUR-GG
     * 
     */
    Integer getDurGgObj();

    void setDurGgObj(Integer durGgObj);

    /**
     * Host Variable ADE-TP-RGM-FISC
     * 
     */
    String getTpRgmFisc();

    void setTpRgmFisc(String tpRgmFisc);

    /**
     * Host Variable ADE-TP-RIAT
     * 
     */
    String getTpRiat();

    void setTpRiat(String tpRiat);

    /**
     * Nullable property for ADE-TP-RIAT
     * 
     */
    String getTpRiatObj();

    void setTpRiatObj(String tpRiatObj);

    /**
     * Host Variable ADE-TP-MOD-PAG-TIT
     * 
     */
    String getTpModPagTit();

    void setTpModPagTit(String tpModPagTit);

    /**
     * Host Variable ADE-TP-IAS
     * 
     */
    String getTpIas();

    void setTpIas(String tpIas);

    /**
     * Nullable property for ADE-TP-IAS
     * 
     */
    String getTpIasObj();

    void setTpIasObj(String tpIasObj);

    /**
     * Host Variable ADE-DT-VARZ-TP-IAS-DB
     * 
     */
    String getDtVarzTpIasDb();

    void setDtVarzTpIasDb(String dtVarzTpIasDb);

    /**
     * Nullable property for ADE-DT-VARZ-TP-IAS-DB
     * 
     */
    String getDtVarzTpIasDbObj();

    void setDtVarzTpIasDbObj(String dtVarzTpIasDbObj);

    /**
     * Host Variable ADE-PRE-NET-IND
     * 
     */
    AfDecimal getPreNetInd();

    void setPreNetInd(AfDecimal preNetInd);

    /**
     * Nullable property for ADE-PRE-NET-IND
     * 
     */
    AfDecimal getPreNetIndObj();

    void setPreNetIndObj(AfDecimal preNetIndObj);

    /**
     * Host Variable ADE-PRE-LRD-IND
     * 
     */
    AfDecimal getPreLrdInd();

    void setPreLrdInd(AfDecimal preLrdInd);

    /**
     * Nullable property for ADE-PRE-LRD-IND
     * 
     */
    AfDecimal getPreLrdIndObj();

    void setPreLrdIndObj(AfDecimal preLrdIndObj);

    /**
     * Host Variable ADE-RAT-LRD-IND
     * 
     */
    AfDecimal getRatLrdInd();

    void setRatLrdInd(AfDecimal ratLrdInd);

    /**
     * Nullable property for ADE-RAT-LRD-IND
     * 
     */
    AfDecimal getRatLrdIndObj();

    void setRatLrdIndObj(AfDecimal ratLrdIndObj);

    /**
     * Host Variable ADE-PRSTZ-INI-IND
     * 
     */
    AfDecimal getPrstzIniInd();

    void setPrstzIniInd(AfDecimal prstzIniInd);

    /**
     * Nullable property for ADE-PRSTZ-INI-IND
     * 
     */
    AfDecimal getPrstzIniIndObj();

    void setPrstzIniIndObj(AfDecimal prstzIniIndObj);

    /**
     * Host Variable ADE-FL-COINC-ASSTO
     * 
     */
    char getFlCoincAssto();

    void setFlCoincAssto(char flCoincAssto);

    /**
     * Nullable property for ADE-FL-COINC-ASSTO
     * 
     */
    Character getFlCoincAsstoObj();

    void setFlCoincAsstoObj(Character flCoincAsstoObj);

    /**
     * Host Variable ADE-IB-DFLT
     * 
     */
    String getIbDflt();

    void setIbDflt(String ibDflt);

    /**
     * Nullable property for ADE-IB-DFLT
     * 
     */
    String getIbDfltObj();

    void setIbDfltObj(String ibDfltObj);

    /**
     * Host Variable ADE-MOD-CALC
     * 
     */
    String getModCalc();

    void setModCalc(String modCalc);

    /**
     * Nullable property for ADE-MOD-CALC
     * 
     */
    String getModCalcObj();

    void setModCalcObj(String modCalcObj);

    /**
     * Host Variable ADE-TP-FNT-CNBTVA
     * 
     */
    String getTpFntCnbtva();

    void setTpFntCnbtva(String tpFntCnbtva);

    /**
     * Nullable property for ADE-TP-FNT-CNBTVA
     * 
     */
    String getTpFntCnbtvaObj();

    void setTpFntCnbtvaObj(String tpFntCnbtvaObj);

    /**
     * Host Variable ADE-IMP-AZ
     * 
     */
    AfDecimal getImpAz();

    void setImpAz(AfDecimal impAz);

    /**
     * Nullable property for ADE-IMP-AZ
     * 
     */
    AfDecimal getImpAzObj();

    void setImpAzObj(AfDecimal impAzObj);

    /**
     * Host Variable ADE-IMP-ADER
     * 
     */
    AfDecimal getImpAder();

    void setImpAder(AfDecimal impAder);

    /**
     * Nullable property for ADE-IMP-ADER
     * 
     */
    AfDecimal getImpAderObj();

    void setImpAderObj(AfDecimal impAderObj);

    /**
     * Host Variable ADE-IMP-TFR
     * 
     */
    AfDecimal getImpTfr();

    void setImpTfr(AfDecimal impTfr);

    /**
     * Nullable property for ADE-IMP-TFR
     * 
     */
    AfDecimal getImpTfrObj();

    void setImpTfrObj(AfDecimal impTfrObj);

    /**
     * Host Variable ADE-IMP-VOLO
     * 
     */
    AfDecimal getImpVolo();

    void setImpVolo(AfDecimal impVolo);

    /**
     * Nullable property for ADE-IMP-VOLO
     * 
     */
    AfDecimal getImpVoloObj();

    void setImpVoloObj(AfDecimal impVoloObj);

    /**
     * Host Variable ADE-PC-AZ
     * 
     */
    AfDecimal getPcAz();

    void setPcAz(AfDecimal pcAz);

    /**
     * Nullable property for ADE-PC-AZ
     * 
     */
    AfDecimal getPcAzObj();

    void setPcAzObj(AfDecimal pcAzObj);

    /**
     * Host Variable ADE-PC-ADER
     * 
     */
    AfDecimal getPcAder();

    void setPcAder(AfDecimal pcAder);

    /**
     * Nullable property for ADE-PC-ADER
     * 
     */
    AfDecimal getPcAderObj();

    void setPcAderObj(AfDecimal pcAderObj);

    /**
     * Host Variable ADE-PC-TFR
     * 
     */
    AfDecimal getPcTfr();

    void setPcTfr(AfDecimal pcTfr);

    /**
     * Nullable property for ADE-PC-TFR
     * 
     */
    AfDecimal getPcTfrObj();

    void setPcTfrObj(AfDecimal pcTfrObj);

    /**
     * Host Variable ADE-PC-VOLO
     * 
     */
    AfDecimal getPcVolo();

    void setPcVolo(AfDecimal pcVolo);

    /**
     * Nullable property for ADE-PC-VOLO
     * 
     */
    AfDecimal getPcVoloObj();

    void setPcVoloObj(AfDecimal pcVoloObj);

    /**
     * Host Variable ADE-DT-NOVA-RGM-FISC-DB
     * 
     */
    String getDtNovaRgmFiscDb();

    void setDtNovaRgmFiscDb(String dtNovaRgmFiscDb);

    /**
     * Nullable property for ADE-DT-NOVA-RGM-FISC-DB
     * 
     */
    String getDtNovaRgmFiscDbObj();

    void setDtNovaRgmFiscDbObj(String dtNovaRgmFiscDbObj);

    /**
     * Host Variable ADE-FL-ATTIV
     * 
     */
    char getFlAttiv();

    void setFlAttiv(char flAttiv);

    /**
     * Nullable property for ADE-FL-ATTIV
     * 
     */
    Character getFlAttivObj();

    void setFlAttivObj(Character flAttivObj);

    /**
     * Host Variable ADE-IMP-REC-RIT-VIS
     * 
     */
    AfDecimal getImpRecRitVis();

    void setImpRecRitVis(AfDecimal impRecRitVis);

    /**
     * Nullable property for ADE-IMP-REC-RIT-VIS
     * 
     */
    AfDecimal getImpRecRitVisObj();

    void setImpRecRitVisObj(AfDecimal impRecRitVisObj);

    /**
     * Host Variable ADE-IMP-REC-RIT-ACC
     * 
     */
    AfDecimal getImpRecRitAcc();

    void setImpRecRitAcc(AfDecimal impRecRitAcc);

    /**
     * Nullable property for ADE-IMP-REC-RIT-ACC
     * 
     */
    AfDecimal getImpRecRitAccObj();

    void setImpRecRitAccObj(AfDecimal impRecRitAccObj);

    /**
     * Host Variable ADE-FL-VARZ-STAT-TBGC
     * 
     */
    char getFlVarzStatTbgc();

    void setFlVarzStatTbgc(char flVarzStatTbgc);

    /**
     * Nullable property for ADE-FL-VARZ-STAT-TBGC
     * 
     */
    Character getFlVarzStatTbgcObj();

    void setFlVarzStatTbgcObj(Character flVarzStatTbgcObj);

    /**
     * Host Variable ADE-FL-PROVZA-MIGRAZ
     * 
     */
    char getFlProvzaMigraz();

    void setFlProvzaMigraz(char flProvzaMigraz);

    /**
     * Nullable property for ADE-FL-PROVZA-MIGRAZ
     * 
     */
    Character getFlProvzaMigrazObj();

    void setFlProvzaMigrazObj(Character flProvzaMigrazObj);

    /**
     * Host Variable ADE-IMPB-VIS-DA-REC
     * 
     */
    AfDecimal getImpbVisDaRec();

    void setImpbVisDaRec(AfDecimal impbVisDaRec);

    /**
     * Nullable property for ADE-IMPB-VIS-DA-REC
     * 
     */
    AfDecimal getImpbVisDaRecObj();

    void setImpbVisDaRecObj(AfDecimal impbVisDaRecObj);

    /**
     * Host Variable ADE-DT-DECOR-PREST-BAN-DB
     * 
     */
    String getDtDecorPrestBanDb();

    void setDtDecorPrestBanDb(String dtDecorPrestBanDb);

    /**
     * Nullable property for ADE-DT-DECOR-PREST-BAN-DB
     * 
     */
    String getDtDecorPrestBanDbObj();

    void setDtDecorPrestBanDbObj(String dtDecorPrestBanDbObj);

    /**
     * Host Variable ADE-DT-EFF-VARZ-STAT-T-DB
     * 
     */
    String getDtEffVarzStatTDb();

    void setDtEffVarzStatTDb(String dtEffVarzStatTDb);

    /**
     * Nullable property for ADE-DT-EFF-VARZ-STAT-T-DB
     * 
     */
    String getDtEffVarzStatTDbObj();

    void setDtEffVarzStatTDbObj(String dtEffVarzStatTDbObj);

    /**
     * Host Variable ADE-DS-RIGA
     * 
     */
    long getAdeDsRiga();

    void setAdeDsRiga(long adeDsRiga);

    /**
     * Host Variable ADE-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable ADE-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable ADE-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable ADE-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable ADE-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable ADE-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable ADE-CUM-CNBT-CAP
     * 
     */
    AfDecimal getCumCnbtCap();

    void setCumCnbtCap(AfDecimal cumCnbtCap);

    /**
     * Nullable property for ADE-CUM-CNBT-CAP
     * 
     */
    AfDecimal getCumCnbtCapObj();

    void setCumCnbtCapObj(AfDecimal cumCnbtCapObj);

    /**
     * Host Variable ADE-IMP-GAR-CNBT
     * 
     */
    AfDecimal getImpGarCnbt();

    void setImpGarCnbt(AfDecimal impGarCnbt);

    /**
     * Nullable property for ADE-IMP-GAR-CNBT
     * 
     */
    AfDecimal getImpGarCnbtObj();

    void setImpGarCnbtObj(AfDecimal impGarCnbtObj);

    /**
     * Host Variable ADE-DT-ULT-CONS-CNBT-DB
     * 
     */
    String getDtUltConsCnbtDb();

    void setDtUltConsCnbtDb(String dtUltConsCnbtDb);

    /**
     * Nullable property for ADE-DT-ULT-CONS-CNBT-DB
     * 
     */
    String getDtUltConsCnbtDbObj();

    void setDtUltConsCnbtDbObj(String dtUltConsCnbtDbObj);

    /**
     * Host Variable ADE-IDEN-ISC-FND
     * 
     */
    String getIdenIscFnd();

    void setIdenIscFnd(String idenIscFnd);

    /**
     * Nullable property for ADE-IDEN-ISC-FND
     * 
     */
    String getIdenIscFndObj();

    void setIdenIscFndObj(String idenIscFndObj);

    /**
     * Host Variable ADE-NUM-RAT-PIAN
     * 
     */
    AfDecimal getNumRatPian();

    void setNumRatPian(AfDecimal numRatPian);

    /**
     * Nullable property for ADE-NUM-RAT-PIAN
     * 
     */
    AfDecimal getNumRatPianObj();

    void setNumRatPianObj(AfDecimal numRatPianObj);

    /**
     * Host Variable ADE-DT-PRESC-DB
     * 
     */
    String getDtPrescDb();

    void setDtPrescDb(String dtPrescDb);

    /**
     * Nullable property for ADE-DT-PRESC-DB
     * 
     */
    String getDtPrescDbObj();

    void setDtPrescDbObj(String dtPrescDbObj);

    /**
     * Host Variable ADE-CONCS-PREST
     * 
     */
    char getConcsPrest();

    void setConcsPrest(char concsPrest);

    /**
     * Nullable property for ADE-CONCS-PREST
     * 
     */
    Character getConcsPrestObj();

    void setConcsPrestObj(Character concsPrestObj);

    /**
     * Host Variable IABV0009-VERSIONING
     * 
     */
    int getIabv0009Versioning();

    void setIabv0009Versioning(int iabv0009Versioning);

    /**
     * Host Variable IABV0002-STATE-CURRENT
     * 
     */
    char getIabv0002StateCurrent();

    void setIabv0002StateCurrent(char iabv0002StateCurrent);
};
