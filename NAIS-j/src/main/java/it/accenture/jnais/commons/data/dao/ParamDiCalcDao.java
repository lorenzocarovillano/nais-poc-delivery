package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IParamDiCalc;

/**
 * Data Access Object(DAO) for table [PARAM_DI_CALC]
 * 
 */
public class ParamDiCalcDao extends BaseSqlDao<IParamDiCalc> {

    private Cursor cIdUpdEffPca;
    private Cursor cIdoEffPca;
    private Cursor cIdoCpzPca;
    private Cursor cEff9;
    private Cursor cCpz9;
    private final IRowMapper<IParamDiCalc> selectByPcaDsRigaRm = buildNamedRowMapper(IParamDiCalc.class, "idParamDiCalc", "pcaIdOggObj", "pcaTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "pcaCodParamObj", "tpDObj", "valDtDbObj", "valImpObj", "valTsObj", "valNumObj", "valPcObj", "valTxtVcharObj", "valFlObj", "pcaDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public ParamDiCalcDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IParamDiCalc> getToClass() {
        return IParamDiCalc.class;
    }

    public IParamDiCalc selectByPcaDsRiga(long pcaDsRiga, IParamDiCalc iParamDiCalc) {
        return buildQuery("selectByPcaDsRiga").bind("pcaDsRiga", pcaDsRiga).rowMapper(selectByPcaDsRigaRm).singleResult(iParamDiCalc);
    }

    public DbAccessStatus insertRec(IParamDiCalc iParamDiCalc) {
        return buildQuery("insertRec").bind(iParamDiCalc).executeInsert();
    }

    public DbAccessStatus updateRec(IParamDiCalc iParamDiCalc) {
        return buildQuery("updateRec").bind(iParamDiCalc).executeUpdate();
    }

    public DbAccessStatus deleteByPcaDsRiga(long pcaDsRiga) {
        return buildQuery("deleteByPcaDsRiga").bind("pcaDsRiga", pcaDsRiga).executeDelete();
    }

    public IParamDiCalc selectRec(int pcaIdParamDiCalc, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IParamDiCalc iParamDiCalc) {
        return buildQuery("selectRec").bind("pcaIdParamDiCalc", pcaIdParamDiCalc).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPcaDsRigaRm).singleResult(iParamDiCalc);
    }

    public DbAccessStatus updateRec1(IParamDiCalc iParamDiCalc) {
        return buildQuery("updateRec1").bind(iParamDiCalc).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffPca(int pcaIdParamDiCalc, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffPca = buildQuery("openCIdUpdEffPca").bind("pcaIdParamDiCalc", pcaIdParamDiCalc).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffPca() {
        return closeCursor(cIdUpdEffPca);
    }

    public IParamDiCalc fetchCIdUpdEffPca(IParamDiCalc iParamDiCalc) {
        return fetch(cIdUpdEffPca, iParamDiCalc, selectByPcaDsRigaRm);
    }

    public IParamDiCalc selectRec1(int pcaIdOgg, String pcaTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IParamDiCalc iParamDiCalc) {
        return buildQuery("selectRec1").bind("pcaIdOgg", pcaIdOgg).bind("pcaTpOgg", pcaTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPcaDsRigaRm).singleResult(iParamDiCalc);
    }

    public DbAccessStatus openCIdoEffPca(int pcaIdOgg, String pcaTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffPca = buildQuery("openCIdoEffPca").bind("pcaIdOgg", pcaIdOgg).bind("pcaTpOgg", pcaTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffPca() {
        return closeCursor(cIdoEffPca);
    }

    public IParamDiCalc fetchCIdoEffPca(IParamDiCalc iParamDiCalc) {
        return fetch(cIdoEffPca, iParamDiCalc, selectByPcaDsRigaRm);
    }

    public IParamDiCalc selectRec2(int pcaIdParamDiCalc, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IParamDiCalc iParamDiCalc) {
        return buildQuery("selectRec2").bind("pcaIdParamDiCalc", pcaIdParamDiCalc).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPcaDsRigaRm).singleResult(iParamDiCalc);
    }

    public IParamDiCalc selectRec3(IParamDiCalc iParamDiCalc) {
        return buildQuery("selectRec3").bind(iParamDiCalc).rowMapper(selectByPcaDsRigaRm).singleResult(iParamDiCalc);
    }

    public DbAccessStatus openCIdoCpzPca(IParamDiCalc iParamDiCalc) {
        cIdoCpzPca = buildQuery("openCIdoCpzPca").bind(iParamDiCalc).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzPca() {
        return closeCursor(cIdoCpzPca);
    }

    public IParamDiCalc fetchCIdoCpzPca(IParamDiCalc iParamDiCalc) {
        return fetch(cIdoCpzPca, iParamDiCalc, selectByPcaDsRigaRm);
    }

    public IParamDiCalc selectRec4(IParamDiCalc iParamDiCalc) {
        return buildQuery("selectRec4").bind(iParamDiCalc).rowMapper(selectByPcaDsRigaRm).singleResult(iParamDiCalc);
    }

    public DbAccessStatus openCEff9(IParamDiCalc iParamDiCalc) {
        cEff9 = buildQuery("openCEff9").bind(iParamDiCalc).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff9() {
        return closeCursor(cEff9);
    }

    public IParamDiCalc fetchCEff9(IParamDiCalc iParamDiCalc) {
        return fetch(cEff9, iParamDiCalc, selectByPcaDsRigaRm);
    }

    public IParamDiCalc selectRec5(IParamDiCalc iParamDiCalc) {
        return buildQuery("selectRec5").bind(iParamDiCalc).rowMapper(selectByPcaDsRigaRm).singleResult(iParamDiCalc);
    }

    public DbAccessStatus openCCpz9(IParamDiCalc iParamDiCalc) {
        cCpz9 = buildQuery("openCCpz9").bind(iParamDiCalc).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz9() {
        return closeCursor(cCpz9);
    }

    public IParamDiCalc fetchCCpz9(IParamDiCalc iParamDiCalc) {
        return fetch(cCpz9, iParamDiCalc, selectByPcaDsRigaRm);
    }
}
