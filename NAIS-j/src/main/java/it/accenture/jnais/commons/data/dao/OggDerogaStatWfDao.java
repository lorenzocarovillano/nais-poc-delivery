package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IOggDerogaStatWf;

/**
 * Data Access Object(DAO) for tables [OGG_DEROGA, STAT_OGG_WF]
 * 
 */
public class OggDerogaStatWfDao extends BaseSqlDao<IOggDerogaStatWf> {

    private Cursor cEff17;
    private Cursor cCpz17;
    private final IRowMapper<IOggDerogaStatWf> selectRecRm = buildNamedRowMapper(IOggDerogaStatWf.class, "idOggDeroga", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "idOgg", "tpOgg", "ibOggObj", "tpDeroga", "codGrAutApprt", "codLivAutApprtObj", "codGrAutSup", "codLivAutSup", "dsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public OggDerogaStatWfDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IOggDerogaStatWf> getToClass() {
        return IOggDerogaStatWf.class;
    }

    public IOggDerogaStatWf selectRec(int ldbv3361IdOgg, String ldbv3361TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggDerogaStatWf iOggDerogaStatWf) {
        return buildQuery("selectRec").bind("ldbv3361IdOgg", ldbv3361IdOgg).bind("ldbv3361TpOgg", ldbv3361TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectRecRm).singleResult(iOggDerogaStatWf);
    }

    public DbAccessStatus openCEff17(int ldbv3361IdOgg, String ldbv3361TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff17 = buildQuery("openCEff17").bind("ldbv3361IdOgg", ldbv3361IdOgg).bind("ldbv3361TpOgg", ldbv3361TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff17() {
        return closeCursor(cEff17);
    }

    public IOggDerogaStatWf fetchCEff17(IOggDerogaStatWf iOggDerogaStatWf) {
        return fetch(cEff17, iOggDerogaStatWf, selectRecRm);
    }

    public IOggDerogaStatWf selectRec1(IOggDerogaStatWf iOggDerogaStatWf) {
        return buildQuery("selectRec1").bind(iOggDerogaStatWf).rowMapper(selectRecRm).singleResult(iOggDerogaStatWf);
    }

    public DbAccessStatus openCCpz17(IOggDerogaStatWf iOggDerogaStatWf) {
        cCpz17 = buildQuery("openCCpz17").bind(iOggDerogaStatWf).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz17() {
        return closeCursor(cCpz17);
    }

    public IOggDerogaStatWf fetchCCpz17(IOggDerogaStatWf iOggDerogaStatWf) {
        return fetch(cCpz17, iOggDerogaStatWf, selectRecRm);
    }
}
