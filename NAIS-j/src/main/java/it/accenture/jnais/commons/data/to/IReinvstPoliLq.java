package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [REINVST_POLI_LQ]
 * 
 */
public interface IReinvstPoliLq extends BaseSqlTo {

    /**
     * Host Variable L30-ID-REINVST-POLI-LQ
     * 
     */
    int getIdReinvstPoliLq();

    void setIdReinvstPoliLq(int idReinvstPoliLq);

    /**
     * Host Variable L30-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable L30-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for L30-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable L30-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable L30-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable L30-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L30-COD-RAMO
     * 
     */
    String getCodRamo();

    void setCodRamo(String codRamo);

    /**
     * Host Variable L30-IB-POLI
     * 
     */
    String getIbPoli();

    void setIbPoli(String ibPoli);

    /**
     * Host Variable L30-PR-KEY-SIST-ESTNO
     * 
     */
    String getPrKeySistEstno();

    void setPrKeySistEstno(String prKeySistEstno);

    /**
     * Nullable property for L30-PR-KEY-SIST-ESTNO
     * 
     */
    String getPrKeySistEstnoObj();

    void setPrKeySistEstnoObj(String prKeySistEstnoObj);

    /**
     * Host Variable L30-SEC-KEY-SIST-ESTNO
     * 
     */
    String getSecKeySistEstno();

    void setSecKeySistEstno(String secKeySistEstno);

    /**
     * Nullable property for L30-SEC-KEY-SIST-ESTNO
     * 
     */
    String getSecKeySistEstnoObj();

    void setSecKeySistEstnoObj(String secKeySistEstnoObj);

    /**
     * Host Variable L30-COD-CAN
     * 
     */
    int getCodCan();

    void setCodCan(int codCan);

    /**
     * Host Variable L30-COD-AGE
     * 
     */
    int getCodAge();

    void setCodAge(int codAge);

    /**
     * Host Variable L30-COD-SUB-AGE
     * 
     */
    int getCodSubAge();

    void setCodSubAge(int codSubAge);

    /**
     * Nullable property for L30-COD-SUB-AGE
     * 
     */
    Integer getCodSubAgeObj();

    void setCodSubAgeObj(Integer codSubAgeObj);

    /**
     * Host Variable L30-IMP-RES
     * 
     */
    AfDecimal getImpRes();

    void setImpRes(AfDecimal impRes);

    /**
     * Host Variable L30-TP-LIQ
     * 
     */
    String getTpLiq();

    void setTpLiq(String tpLiq);

    /**
     * Host Variable L30-TP-SIST-ESTNO
     * 
     */
    String getTpSistEstno();

    void setTpSistEstno(String tpSistEstno);

    /**
     * Host Variable L30-COD-FISC-PART-IVA
     * 
     */
    String getCodFiscPartIva();

    void setCodFiscPartIva(String codFiscPartIva);

    /**
     * Host Variable L30-COD-MOVI-LIQ
     * 
     */
    int getCodMoviLiq();

    void setCodMoviLiq(int codMoviLiq);

    /**
     * Host Variable L30-TP-INVST-LIQ
     * 
     */
    short getTpInvstLiq();

    void setTpInvstLiq(short tpInvstLiq);

    /**
     * Nullable property for L30-TP-INVST-LIQ
     * 
     */
    Short getTpInvstLiqObj();

    void setTpInvstLiqObj(Short tpInvstLiqObj);

    /**
     * Host Variable L30-IMP-TOT-LIQ
     * 
     */
    AfDecimal getImpTotLiq();

    void setImpTotLiq(AfDecimal impTotLiq);

    /**
     * Host Variable L30-DS-RIGA
     * 
     */
    long getL30DsRiga();

    void setL30DsRiga(long l30DsRiga);

    /**
     * Host Variable L30-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L30-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L30-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable L30-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable L30-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable L30-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable L30-DT-DECOR-POLI-LQ-DB
     * 
     */
    String getDtDecorPoliLqDb();

    void setDtDecorPoliLqDb(String dtDecorPoliLqDb);

    /**
     * Nullable property for L30-DT-DECOR-POLI-LQ-DB
     * 
     */
    String getDtDecorPoliLqDbObj();

    void setDtDecorPoliLqDbObj(String dtDecorPoliLqDbObj);
};
