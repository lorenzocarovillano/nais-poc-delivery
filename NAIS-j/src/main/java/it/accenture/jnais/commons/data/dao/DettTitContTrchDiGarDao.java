package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDettTitContTrchDiGar;

/**
 * Data Access Object(DAO) for tables [DETT_TIT_CONT, TRCH_DI_GAR]
 * 
 */
public class DettTitContTrchDiGarDao extends BaseSqlDao<IDettTitContTrchDiGar> {

    private Cursor cEff46;
    private Cursor cCpz46;
    private final IRowMapper<IDettTitContTrchDiGar> selectRec2Rm = buildNamedRowMapper(IDettTitContTrchDiGar.class, "ldbve091PreLrdObj");
    private final IRowMapper<IDettTitContTrchDiGar> selectRec4Rm = buildNamedRowMapper(IDettTitContTrchDiGar.class, "idDettTitCont", "idTitCont", "idOgg", "tpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtcDtIniCopDbObj", "dtEndCopDbObj", "preNetObj", "intrFrazObj", "intrMoraObj", "intrRetdtObj", "intrRiatObj", "dirObj", "speMedObj", "taxObj", "soprSanObj", "soprSpoObj", "soprTecObj", "soprProfObj", "soprAltObj", "preTotObj", "prePpIasObj", "preSoloRshObj", "carAcqObj", "carGestObj", "carIncObj", "provAcq1aaObj", "provAcq2aaObj", "provRicorObj", "provIncObj", "provDaRecObj", "codDvsObj", "frqMoviObj", "tpRgmFisc", "codTariObj", "tpStatTit", "impAzObj", "impAderObj", "impTfrObj", "impVoloObj", "manfeeAnticObj", "manfeeRicorObj", "manfeeRecObj", "dtEsiTitDbObj", "speAgeObj", "carIasObj", "totIntrPrestObj", "dsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "impTrasfeObj", "impTfrStrcObj", "numGgRitardoPagObj", "numGgRivalObj", "acqExpObj", "remunAssObj", "commisInterObj", "cnbtAntiracObj");

    public DettTitContTrchDiGarDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDettTitContTrchDiGar> getToClass() {
        return IDettTitContTrchDiGar.class;
    }

    public AfDecimal selectRec(IDettTitContTrchDiGar iDettTitContTrchDiGar, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec").bind(iDettTitContTrchDiGar).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec1(IDettTitContTrchDiGar iDettTitContTrchDiGar, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec1").bind(iDettTitContTrchDiGar).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public IDettTitContTrchDiGar selectRec2(IDettTitContTrchDiGar iDettTitContTrchDiGar) {
        return buildQuery("selectRec2").bind(iDettTitContTrchDiGar).rowMapper(selectRec2Rm).singleResult(iDettTitContTrchDiGar);
    }

    public IDettTitContTrchDiGar selectRec3(IDettTitContTrchDiGar iDettTitContTrchDiGar) {
        return buildQuery("selectRec3").bind(iDettTitContTrchDiGar).rowMapper(selectRec2Rm).singleResult(iDettTitContTrchDiGar);
    }

    public IDettTitContTrchDiGar selectRec4(IDettTitContTrchDiGar iDettTitContTrchDiGar) {
        return buildQuery("selectRec4").bind(iDettTitContTrchDiGar).rowMapper(selectRec4Rm).singleResult(iDettTitContTrchDiGar);
    }

    public DbAccessStatus openCEff46(IDettTitContTrchDiGar iDettTitContTrchDiGar) {
        cEff46 = buildQuery("openCEff46").bind(iDettTitContTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff46() {
        return closeCursor(cEff46);
    }

    public IDettTitContTrchDiGar fetchCEff46(IDettTitContTrchDiGar iDettTitContTrchDiGar) {
        return fetch(cEff46, iDettTitContTrchDiGar, selectRec4Rm);
    }

    public IDettTitContTrchDiGar selectRec5(IDettTitContTrchDiGar iDettTitContTrchDiGar) {
        return buildQuery("selectRec5").bind(iDettTitContTrchDiGar).rowMapper(selectRec4Rm).singleResult(iDettTitContTrchDiGar);
    }

    public DbAccessStatus openCCpz46(IDettTitContTrchDiGar iDettTitContTrchDiGar) {
        cCpz46 = buildQuery("openCCpz46").bind(iDettTitContTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz46() {
        return closeCursor(cCpz46);
    }

    public IDettTitContTrchDiGar fetchCCpz46(IDettTitContTrchDiGar iDettTitContTrchDiGar) {
        return fetch(cCpz46, iDettTitContTrchDiGar, selectRec4Rm);
    }
}
