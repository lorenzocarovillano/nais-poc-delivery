package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [LOG_ERRORE]
 * 
 */
public interface ILogErrore extends BaseSqlTo {

    /**
     * Host Variable LOR-ID-LOG-ERRORE
     * 
     */
    int getIdLogErrore();

    void setIdLogErrore(int idLogErrore);

    /**
     * Host Variable LOR-PROG-LOG-ERRORE
     * 
     */
    int getProgLogErrore();

    void setProgLogErrore(int progLogErrore);

    /**
     * Host Variable LOR-ID-GRAVITA-ERRORE
     * 
     */
    int getIdGravitaErrore();

    void setIdGravitaErrore(int idGravitaErrore);

    /**
     * Host Variable LOR-DESC-ERRORE-ESTESA-VCHAR
     * 
     */
    String getDescErroreEstesaVchar();

    void setDescErroreEstesaVchar(String descErroreEstesaVchar);

    /**
     * Host Variable LOR-COD-MAIN-BATCH
     * 
     */
    String getCodMainBatch();

    void setCodMainBatch(String codMainBatch);

    /**
     * Host Variable LOR-COD-SERVIZIO-BE
     * 
     */
    String getCodServizioBe();

    void setCodServizioBe(String codServizioBe);

    /**
     * Host Variable LOR-LABEL-ERR
     * 
     */
    String getLabelErr();

    void setLabelErr(String labelErr);

    /**
     * Nullable property for LOR-LABEL-ERR
     * 
     */
    String getLabelErrObj();

    void setLabelErrObj(String labelErrObj);

    /**
     * Host Variable LOR-OPER-TABELLA
     * 
     */
    String getOperTabella();

    void setOperTabella(String operTabella);

    /**
     * Nullable property for LOR-OPER-TABELLA
     * 
     */
    String getOperTabellaObj();

    void setOperTabellaObj(String operTabellaObj);

    /**
     * Host Variable LOR-NOME-TABELLA
     * 
     */
    String getNomeTabella();

    void setNomeTabella(String nomeTabella);

    /**
     * Nullable property for LOR-NOME-TABELLA
     * 
     */
    String getNomeTabellaObj();

    void setNomeTabellaObj(String nomeTabellaObj);

    /**
     * Host Variable LOR-STATUS-TABELLA
     * 
     */
    String getStatusTabella();

    void setStatusTabella(String statusTabella);

    /**
     * Nullable property for LOR-STATUS-TABELLA
     * 
     */
    String getStatusTabellaObj();

    void setStatusTabellaObj(String statusTabellaObj);

    /**
     * Host Variable LOR-KEY-TABELLA-VCHAR
     * 
     */
    String getKeyTabellaVchar();

    void setKeyTabellaVchar(String keyTabellaVchar);

    /**
     * Nullable property for LOR-KEY-TABELLA-VCHAR
     * 
     */
    String getKeyTabellaVcharObj();

    void setKeyTabellaVcharObj(String keyTabellaVcharObj);

    /**
     * Host Variable LOR-TIMESTAMP-REG
     * 
     */
    long getTimestampReg();

    void setTimestampReg(long timestampReg);

    /**
     * Host Variable LOR-TIPO-OGGETTO
     * 
     */
    short getTipoOggetto();

    void setTipoOggetto(short tipoOggetto);

    /**
     * Nullable property for LOR-TIPO-OGGETTO
     * 
     */
    Short getTipoOggettoObj();

    void setTipoOggettoObj(Short tipoOggettoObj);

    /**
     * Host Variable LOR-IB-OGGETTO
     * 
     */
    String getIbOggetto();

    void setIbOggetto(String ibOggetto);

    /**
     * Nullable property for LOR-IB-OGGETTO
     * 
     */
    String getIbOggettoObj();

    void setIbOggettoObj(String ibOggettoObj);
};
