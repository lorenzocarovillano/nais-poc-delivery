package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PARAM_MOVI]
 * 
 */
public interface IParamMovi extends BaseSqlTo {

    /**
     * Host Variable PMO-ID-OGG
     * 
     */
    int getPmoIdOgg();

    void setPmoIdOgg(int pmoIdOgg);

    /**
     * Host Variable PMO-TP-OGG
     * 
     */
    String getPmoTpOgg();

    void setPmoTpOgg(String pmoTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable WS-TP-MOVI
     * 
     */
    int getWsTpMovi();

    void setWsTpMovi(int wsTpMovi);

    /**
     * Host Variable WS-GARANZIA
     * 
     */
    String getWsGaranzia();

    void setWsGaranzia(String wsGaranzia);

    /**
     * Host Variable IABV0009-ID-OGG-DA
     * 
     */
    int getIabv0009IdOggDa();

    void setIabv0009IdOggDa(int iabv0009IdOggDa);

    /**
     * Host Variable IABV0009-ID-OGG-A
     * 
     */
    int getIabv0009IdOggA();

    void setIabv0009IdOggA(int iabv0009IdOggA);

    /**
     * Host Variable WS-FORMA1
     * 
     */
    String getWsForma1();

    void setWsForma1(String wsForma1);

    /**
     * Host Variable WS-DATA-EFF
     * 
     */
    String getWsDataEff();

    void setWsDataEff(String wsDataEff);

    /**
     * Host Variable IABV0002-STATE-01
     * 
     */
    char getIabv0002State01();

    void setIabv0002State01(char iabv0002State01);

    /**
     * Host Variable IABV0002-STATE-02
     * 
     */
    char getIabv0002State02();

    void setIabv0002State02(char iabv0002State02);

    /**
     * Host Variable IABV0002-STATE-03
     * 
     */
    char getIabv0002State03();

    void setIabv0002State03(char iabv0002State03);

    /**
     * Host Variable IABV0002-STATE-04
     * 
     */
    char getIabv0002State04();

    void setIabv0002State04(char iabv0002State04);

    /**
     * Host Variable IABV0002-STATE-05
     * 
     */
    char getIabv0002State05();

    void setIabv0002State05(char iabv0002State05);

    /**
     * Host Variable IABV0002-STATE-06
     * 
     */
    char getIabv0002State06();

    void setIabv0002State06(char iabv0002State06);

    /**
     * Host Variable IABV0002-STATE-07
     * 
     */
    char getIabv0002State07();

    void setIabv0002State07(char iabv0002State07);

    /**
     * Host Variable IABV0002-STATE-08
     * 
     */
    char getIabv0002State08();

    void setIabv0002State08(char iabv0002State08);

    /**
     * Host Variable IABV0002-STATE-09
     * 
     */
    char getIabv0002State09();

    void setIabv0002State09(char iabv0002State09);

    /**
     * Host Variable IABV0002-STATE-10
     * 
     */
    char getIabv0002State10();

    void setIabv0002State10(char iabv0002State10);

    /**
     * Host Variable WS-COD-RAMO
     * 
     */
    String getWsCodRamo();

    void setWsCodRamo(String wsCodRamo);

    /**
     * Host Variable WS-FORMA2
     * 
     */
    String getWsForma2();

    void setWsForma2(String wsForma2);

    /**
     * Host Variable WS-DT-ELAB-DA-DB
     * 
     */
    String getWsDtElabDaDb();

    void setWsDtElabDaDb(String wsDtElabDaDb);

    /**
     * Host Variable WS-DT-ELAB-A-DB
     * 
     */
    String getWsDtElabADb();

    void setWsDtElabADb(String wsDtElabADb);

    /**
     * Host Variable WS-TS-INFINITO-1
     * 
     */
    long getWsTsInfinito1();

    void setWsTsInfinito1(long wsTsInfinito1);

    /**
     * Host Variable WS-ID-POLI
     * 
     */
    int getWsIdPoli();

    void setWsIdPoli(int wsIdPoli);

    /**
     * Host Variable WS-ID-ADES
     * 
     */
    int getWsIdAdes();

    void setWsIdAdes(int wsIdAdes);

    /**
     * Host Variable LDBV0641-ID-POLI
     * 
     */
    int getLdbv0641IdPoli();

    void setLdbv0641IdPoli(int ldbv0641IdPoli);

    /**
     * Host Variable LDBV0641-TP-MOVI-1
     * 
     */
    int getLdbv0641TpMovi1();

    void setLdbv0641TpMovi1(int ldbv0641TpMovi1);

    /**
     * Host Variable LDBV0641-TP-MOVI-2
     * 
     */
    int getLdbv0641TpMovi2();

    void setLdbv0641TpMovi2(int ldbv0641TpMovi2);

    /**
     * Host Variable LDBV0641-TP-MOVI-3
     * 
     */
    int getLdbv0641TpMovi3();

    void setLdbv0641TpMovi3(int ldbv0641TpMovi3);

    /**
     * Host Variable LDBV0641-TP-MOVI-4
     * 
     */
    int getLdbv0641TpMovi4();

    void setLdbv0641TpMovi4(int ldbv0641TpMovi4);

    /**
     * Host Variable LDBV0641-TP-MOVI-5
     * 
     */
    int getLdbv0641TpMovi5();

    void setLdbv0641TpMovi5(int ldbv0641TpMovi5);

    /**
     * Host Variable LDBV0641-TP-MOVI-6
     * 
     */
    int getLdbv0641TpMovi6();

    void setLdbv0641TpMovi6(int ldbv0641TpMovi6);

    /**
     * Host Variable LDBV0641-TP-MOVI-7
     * 
     */
    int getLdbv0641TpMovi7();

    void setLdbv0641TpMovi7(int ldbv0641TpMovi7);

    /**
     * Host Variable LDBV0641-TP-MOVI-8
     * 
     */
    int getLdbv0641TpMovi8();

    void setLdbv0641TpMovi8(int ldbv0641TpMovi8);

    /**
     * Host Variable LDBV0641-TP-MOVI-9
     * 
     */
    int getLdbv0641TpMovi9();

    void setLdbv0641TpMovi9(int ldbv0641TpMovi9);

    /**
     * Host Variable LDBV0641-TP-MOVI-10
     * 
     */
    int getLdbv0641TpMovi10();

    void setLdbv0641TpMovi10(int ldbv0641TpMovi10);

    /**
     * Host Variable LDBV0641-TP-MOVI-11
     * 
     */
    int getLdbv0641TpMovi11();

    void setLdbv0641TpMovi11(int ldbv0641TpMovi11);

    /**
     * Host Variable LDBV0641-TP-MOVI-12
     * 
     */
    int getLdbv0641TpMovi12();

    void setLdbv0641TpMovi12(int ldbv0641TpMovi12);

    /**
     * Host Variable LDBV0641-TP-MOVI-13
     * 
     */
    int getLdbv0641TpMovi13();

    void setLdbv0641TpMovi13(int ldbv0641TpMovi13);

    /**
     * Host Variable LDBV0641-TP-MOVI-14
     * 
     */
    int getLdbv0641TpMovi14();

    void setLdbv0641TpMovi14(int ldbv0641TpMovi14);

    /**
     * Host Variable LDBV0641-TP-MOVI-15
     * 
     */
    int getLdbv0641TpMovi15();

    void setLdbv0641TpMovi15(int ldbv0641TpMovi15);

    /**
     * Host Variable LDBV1471-TP-MOVI-01
     * 
     */
    int getLdbv1471TpMovi01();

    void setLdbv1471TpMovi01(int ldbv1471TpMovi01);

    /**
     * Host Variable LDBV1471-TP-MOVI-02
     * 
     */
    int getLdbv1471TpMovi02();

    void setLdbv1471TpMovi02(int ldbv1471TpMovi02);

    /**
     * Host Variable LDBV1471-TP-MOVI-03
     * 
     */
    int getLdbv1471TpMovi03();

    void setLdbv1471TpMovi03(int ldbv1471TpMovi03);

    /**
     * Host Variable LDBV1471-TP-MOVI-04
     * 
     */
    int getLdbv1471TpMovi04();

    void setLdbv1471TpMovi04(int ldbv1471TpMovi04);

    /**
     * Host Variable LDBV1471-TP-MOVI-05
     * 
     */
    int getLdbv1471TpMovi05();

    void setLdbv1471TpMovi05(int ldbv1471TpMovi05);

    /**
     * Host Variable LDBV1471-TP-MOVI-06
     * 
     */
    int getLdbv1471TpMovi06();

    void setLdbv1471TpMovi06(int ldbv1471TpMovi06);

    /**
     * Host Variable LDBV1471-TP-MOVI-07
     * 
     */
    int getLdbv1471TpMovi07();

    void setLdbv1471TpMovi07(int ldbv1471TpMovi07);

    /**
     * Host Variable LDBV1471-TP-MOVI-08
     * 
     */
    int getLdbv1471TpMovi08();

    void setLdbv1471TpMovi08(int ldbv1471TpMovi08);

    /**
     * Host Variable LDBV1471-TP-MOVI-09
     * 
     */
    int getLdbv1471TpMovi09();

    void setLdbv1471TpMovi09(int ldbv1471TpMovi09);

    /**
     * Host Variable LDBV1471-TP-MOVI-10
     * 
     */
    int getLdbv1471TpMovi10();

    void setLdbv1471TpMovi10(int ldbv1471TpMovi10);

    /**
     * Host Variable PMO-COD-COMP-ANIA
     * 
     */
    int getPmoCodCompAnia();

    void setPmoCodCompAnia(int pmoCodCompAnia);

    /**
     * Host Variable PMO-TP-MOVI
     * 
     */
    int getPmoTpMovi();

    void setPmoTpMovi(int pmoTpMovi);

    /**
     * Host Variable PMO-TP-FRM-ASSVA
     * 
     */
    String getPmoTpFrmAssva();

    void setPmoTpFrmAssva(String pmoTpFrmAssva);

    /**
     * Host Variable PMO-ID-POLI
     * 
     */
    int getPmoIdPoli();

    void setPmoIdPoli(int pmoIdPoli);

    /**
     * Host Variable PMO-ID-ADES
     * 
     */
    int getPmoIdAdes();

    void setPmoIdAdes(int pmoIdAdes);

    /**
     * Host Variable PMO-DT-RICOR-SUCC-DB
     * 
     */
    String getPmoDtRicorSuccDb();

    void setPmoDtRicorSuccDb(String pmoDtRicorSuccDb);

    /**
     * Host Variable PMO-DT-INI-EFF-DB
     * 
     */
    String getPmoDtIniEffDb();

    void setPmoDtIniEffDb(String pmoDtIniEffDb);

    /**
     * Host Variable PMO-DT-END-EFF-DB
     * 
     */
    String getPmoDtEndEffDb();

    void setPmoDtEndEffDb(String pmoDtEndEffDb);

    /**
     * Host Variable PMO-DS-TS-INI-CPTZ
     * 
     */
    long getPmoDsTsIniCptz();

    void setPmoDsTsIniCptz(long pmoDsTsIniCptz);

    /**
     * Host Variable PMO-DS-TS-END-CPTZ
     * 
     */
    long getPmoDsTsEndCptz();

    void setPmoDsTsEndCptz(long pmoDsTsEndCptz);

    /**
     * Host Variable LDBV5061-ID-POLI
     * 
     */
    int getLdbv5061IdPoli();

    void setLdbv5061IdPoli(int ldbv5061IdPoli);

    /**
     * Host Variable LDBV5061-TP-MOVI-01
     * 
     */
    int getLdbv5061TpMovi01();

    void setLdbv5061TpMovi01(int ldbv5061TpMovi01);

    /**
     * Host Variable LDBV5061-TP-MOVI-02
     * 
     */
    int getLdbv5061TpMovi02();

    void setLdbv5061TpMovi02(int ldbv5061TpMovi02);

    /**
     * Host Variable LDBV5061-TP-MOVI-03
     * 
     */
    int getLdbv5061TpMovi03();

    void setLdbv5061TpMovi03(int ldbv5061TpMovi03);

    /**
     * Host Variable LDBV5061-TP-MOVI-04
     * 
     */
    int getLdbv5061TpMovi04();

    void setLdbv5061TpMovi04(int ldbv5061TpMovi04);

    /**
     * Host Variable LDBV5061-TP-MOVI-05
     * 
     */
    int getLdbv5061TpMovi05();

    void setLdbv5061TpMovi05(int ldbv5061TpMovi05);

    /**
     * Host Variable LDBV5061-TP-MOVI-06
     * 
     */
    int getLdbv5061TpMovi06();

    void setLdbv5061TpMovi06(int ldbv5061TpMovi06);

    /**
     * Host Variable LDBV5061-TP-MOVI-07
     * 
     */
    int getLdbv5061TpMovi07();

    void setLdbv5061TpMovi07(int ldbv5061TpMovi07);

    /**
     * Host Variable LDBV5061-TP-MOVI-08
     * 
     */
    int getLdbv5061TpMovi08();

    void setLdbv5061TpMovi08(int ldbv5061TpMovi08);

    /**
     * Host Variable LDBV5061-TP-MOVI-09
     * 
     */
    int getLdbv5061TpMovi09();

    void setLdbv5061TpMovi09(int ldbv5061TpMovi09);

    /**
     * Host Variable LDBV5061-TP-MOVI-10
     * 
     */
    int getLdbv5061TpMovi10();

    void setLdbv5061TpMovi10(int ldbv5061TpMovi10);

    /**
     * Host Variable LDBV7851-ID-OGG
     * 
     */
    int getLdbv7851IdOgg();

    void setLdbv7851IdOgg(int ldbv7851IdOgg);

    /**
     * Host Variable LDBV7851-TP-OGG
     * 
     */
    String getLdbv7851TpOgg();

    void setLdbv7851TpOgg(String ldbv7851TpOgg);

    /**
     * Host Variable LDBV7851-TP-MOVI-01
     * 
     */
    int getLdbv7851TpMovi01();

    void setLdbv7851TpMovi01(int ldbv7851TpMovi01);

    /**
     * Host Variable LDBV7851-TP-MOVI-02
     * 
     */
    int getLdbv7851TpMovi02();

    void setLdbv7851TpMovi02(int ldbv7851TpMovi02);

    /**
     * Host Variable LDBV7851-TP-MOVI-03
     * 
     */
    int getLdbv7851TpMovi03();

    void setLdbv7851TpMovi03(int ldbv7851TpMovi03);

    /**
     * Host Variable LDBVD961-TP-MOVI-01
     * 
     */
    int getLdbvd961TpMovi01();

    void setLdbvd961TpMovi01(int ldbvd961TpMovi01);

    /**
     * Host Variable LDBVD961-TP-MOVI-02
     * 
     */
    int getLdbvd961TpMovi02();

    void setLdbvd961TpMovi02(int ldbvd961TpMovi02);

    /**
     * Host Variable LDBVD961-TP-MOVI-03
     * 
     */
    int getLdbvd961TpMovi03();

    void setLdbvd961TpMovi03(int ldbvd961TpMovi03);

    /**
     * Host Variable LDBVD961-TP-MOVI-04
     * 
     */
    int getLdbvd961TpMovi04();

    void setLdbvd961TpMovi04(int ldbvd961TpMovi04);

    /**
     * Host Variable LDBVD961-TP-MOVI-05
     * 
     */
    int getLdbvd961TpMovi05();

    void setLdbvd961TpMovi05(int ldbvd961TpMovi05);

    /**
     * Host Variable LDBVD961-TP-MOVI-06
     * 
     */
    int getLdbvd961TpMovi06();

    void setLdbvd961TpMovi06(int ldbvd961TpMovi06);

    /**
     * Host Variable LDBVD961-TP-MOVI-07
     * 
     */
    int getLdbvd961TpMovi07();

    void setLdbvd961TpMovi07(int ldbvd961TpMovi07);

    /**
     * Host Variable LDBVD961-TP-MOVI-08
     * 
     */
    int getLdbvd961TpMovi08();

    void setLdbvd961TpMovi08(int ldbvd961TpMovi08);

    /**
     * Host Variable LDBVD961-TP-MOVI-09
     * 
     */
    int getLdbvd961TpMovi09();

    void setLdbvd961TpMovi09(int ldbvd961TpMovi09);

    /**
     * Host Variable LDBVD961-TP-MOVI-10
     * 
     */
    int getLdbvd961TpMovi10();

    void setLdbvd961TpMovi10(int ldbvd961TpMovi10);

    /**
     * Host Variable LDBVH601-ID-ADES
     * 
     */
    int getLdbvh601IdAdes();

    void setLdbvh601IdAdes(int ldbvh601IdAdes);

    /**
     * Host Variable LDBVH601-TP-MOVI-1
     * 
     */
    int getLdbvh601TpMovi1();

    void setLdbvh601TpMovi1(int ldbvh601TpMovi1);

    /**
     * Host Variable LDBVH601-TP-MOVI-2
     * 
     */
    int getLdbvh601TpMovi2();

    void setLdbvh601TpMovi2(int ldbvh601TpMovi2);

    /**
     * Host Variable LDBVH601-TP-MOVI-3
     * 
     */
    int getLdbvh601TpMovi3();

    void setLdbvh601TpMovi3(int ldbvh601TpMovi3);

    /**
     * Host Variable LDBVH601-TP-MOVI-4
     * 
     */
    int getLdbvh601TpMovi4();

    void setLdbvh601TpMovi4(int ldbvh601TpMovi4);

    /**
     * Host Variable LDBVH601-TP-MOVI-5
     * 
     */
    int getLdbvh601TpMovi5();

    void setLdbvh601TpMovi5(int ldbvh601TpMovi5);

    /**
     * Host Variable LDBVH601-TP-MOVI-6
     * 
     */
    int getLdbvh601TpMovi6();

    void setLdbvh601TpMovi6(int ldbvh601TpMovi6);

    /**
     * Host Variable LDBVH601-TP-MOVI-7
     * 
     */
    int getLdbvh601TpMovi7();

    void setLdbvh601TpMovi7(int ldbvh601TpMovi7);

    /**
     * Host Variable LDBVH601-TP-MOVI-8
     * 
     */
    int getLdbvh601TpMovi8();

    void setLdbvh601TpMovi8(int ldbvh601TpMovi8);

    /**
     * Host Variable LDBVH601-TP-MOVI-9
     * 
     */
    int getLdbvh601TpMovi9();

    void setLdbvh601TpMovi9(int ldbvh601TpMovi9);

    /**
     * Host Variable LDBVH601-TP-MOVI-10
     * 
     */
    int getLdbvh601TpMovi10();

    void setLdbvh601TpMovi10(int ldbvh601TpMovi10);

    /**
     * Host Variable LDBVH601-TP-MOVI-11
     * 
     */
    int getLdbvh601TpMovi11();

    void setLdbvh601TpMovi11(int ldbvh601TpMovi11);

    /**
     * Host Variable LDBVH601-TP-MOVI-12
     * 
     */
    int getLdbvh601TpMovi12();

    void setLdbvh601TpMovi12(int ldbvh601TpMovi12);

    /**
     * Host Variable LDBVH601-TP-MOVI-13
     * 
     */
    int getLdbvh601TpMovi13();

    void setLdbvh601TpMovi13(int ldbvh601TpMovi13);

    /**
     * Host Variable LDBVH601-TP-MOVI-14
     * 
     */
    int getLdbvh601TpMovi14();

    void setLdbvh601TpMovi14(int ldbvh601TpMovi14);

    /**
     * Host Variable LDBVH601-TP-MOVI-15
     * 
     */
    int getLdbvh601TpMovi15();

    void setLdbvh601TpMovi15(int ldbvh601TpMovi15);

    /**
     * Host Variable LDBV2681-TP-OGG
     * 
     */
    String getLdbv2681TpOgg();

    void setLdbv2681TpOgg(String ldbv2681TpOgg);

    /**
     * Host Variable LDBV2681-ID-OGG
     * 
     */
    int getLdbv2681IdOgg();

    void setLdbv2681IdOgg(int ldbv2681IdOgg);

    /**
     * Host Variable LDBV2681-TP-MOVI-01
     * 
     */
    int getLdbv2681TpMovi01();

    void setLdbv2681TpMovi01(int ldbv2681TpMovi01);

    /**
     * Host Variable LDBV2681-TP-MOVI-02
     * 
     */
    int getLdbv2681TpMovi02();

    void setLdbv2681TpMovi02(int ldbv2681TpMovi02);

    /**
     * Host Variable LDBV2681-TP-MOVI-03
     * 
     */
    int getLdbv2681TpMovi03();

    void setLdbv2681TpMovi03(int ldbv2681TpMovi03);

    /**
     * Host Variable LDBV2681-TP-MOVI-04
     * 
     */
    int getLdbv2681TpMovi04();

    void setLdbv2681TpMovi04(int ldbv2681TpMovi04);

    /**
     * Host Variable LDBV2681-TP-MOVI-05
     * 
     */
    int getLdbv2681TpMovi05();

    void setLdbv2681TpMovi05(int ldbv2681TpMovi05);

    /**
     * Host Variable LDBV2681-TP-MOVI-06
     * 
     */
    int getLdbv2681TpMovi06();

    void setLdbv2681TpMovi06(int ldbv2681TpMovi06);

    /**
     * Host Variable LDBVF971-TP-MOVI-1
     * 
     */
    int getLdbvf971TpMovi1();

    void setLdbvf971TpMovi1(int ldbvf971TpMovi1);

    /**
     * Host Variable LDBVF971-TP-MOVI-2
     * 
     */
    int getLdbvf971TpMovi2();

    void setLdbvf971TpMovi2(int ldbvf971TpMovi2);

    /**
     * Host Variable LDBVF971-TP-MOVI-3
     * 
     */
    int getLdbvf971TpMovi3();

    void setLdbvf971TpMovi3(int ldbvf971TpMovi3);

    /**
     * Host Variable LDBVG351-TP-OGG
     * 
     */
    String getLdbvg351TpOgg();

    void setLdbvg351TpOgg(String ldbvg351TpOgg);

    /**
     * Host Variable LDBVG351-ID-OGG
     * 
     */
    int getLdbvg351IdOgg();

    void setLdbvg351IdOgg(int ldbvg351IdOgg);

    /**
     * Host Variable LDBVG351-TP-MOVI
     * 
     */
    int getLdbvg351TpMovi();

    void setLdbvg351TpMovi(int ldbvg351TpMovi);

    /**
     * Host Variable PMO-ID-PARAM-MOVI
     * 
     */
    int getIdParamMovi();

    void setIdParamMovi(int idParamMovi);

    /**
     * Host Variable PMO-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable PMO-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for PMO-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Nullable property for PMO-TP-MOVI
     * 
     */
    Integer getPmoTpMoviObj();

    void setPmoTpMoviObj(Integer pmoTpMoviObj);

    /**
     * Host Variable PMO-FRQ-MOVI
     * 
     */
    int getFrqMovi();

    void setFrqMovi(int frqMovi);

    /**
     * Nullable property for PMO-FRQ-MOVI
     * 
     */
    Integer getFrqMoviObj();

    void setFrqMoviObj(Integer frqMoviObj);

    /**
     * Host Variable PMO-DUR-AA
     * 
     */
    int getDurAa();

    void setDurAa(int durAa);

    /**
     * Nullable property for PMO-DUR-AA
     * 
     */
    Integer getDurAaObj();

    void setDurAaObj(Integer durAaObj);

    /**
     * Host Variable PMO-DUR-MM
     * 
     */
    int getDurMm();

    void setDurMm(int durMm);

    /**
     * Nullable property for PMO-DUR-MM
     * 
     */
    Integer getDurMmObj();

    void setDurMmObj(Integer durMmObj);

    /**
     * Host Variable PMO-DUR-GG
     * 
     */
    int getDurGg();

    void setDurGg(int durGg);

    /**
     * Nullable property for PMO-DUR-GG
     * 
     */
    Integer getDurGgObj();

    void setDurGgObj(Integer durGgObj);

    /**
     * Host Variable PMO-DT-RICOR-PREC-DB
     * 
     */
    String getDtRicorPrecDb();

    void setDtRicorPrecDb(String dtRicorPrecDb);

    /**
     * Nullable property for PMO-DT-RICOR-PREC-DB
     * 
     */
    String getDtRicorPrecDbObj();

    void setDtRicorPrecDbObj(String dtRicorPrecDbObj);

    /**
     * Nullable property for PMO-DT-RICOR-SUCC-DB
     * 
     */
    String getPmoDtRicorSuccDbObj();

    void setPmoDtRicorSuccDbObj(String pmoDtRicorSuccDbObj);

    /**
     * Host Variable PMO-PC-INTR-FRAZ
     * 
     */
    AfDecimal getPcIntrFraz();

    void setPcIntrFraz(AfDecimal pcIntrFraz);

    /**
     * Nullable property for PMO-PC-INTR-FRAZ
     * 
     */
    AfDecimal getPcIntrFrazObj();

    void setPcIntrFrazObj(AfDecimal pcIntrFrazObj);

    /**
     * Host Variable PMO-IMP-BNS-DA-SCO-TOT
     * 
     */
    AfDecimal getImpBnsDaScoTot();

    void setImpBnsDaScoTot(AfDecimal impBnsDaScoTot);

    /**
     * Nullable property for PMO-IMP-BNS-DA-SCO-TOT
     * 
     */
    AfDecimal getImpBnsDaScoTotObj();

    void setImpBnsDaScoTotObj(AfDecimal impBnsDaScoTotObj);

    /**
     * Host Variable PMO-IMP-BNS-DA-SCO
     * 
     */
    AfDecimal getImpBnsDaSco();

    void setImpBnsDaSco(AfDecimal impBnsDaSco);

    /**
     * Nullable property for PMO-IMP-BNS-DA-SCO
     * 
     */
    AfDecimal getImpBnsDaScoObj();

    void setImpBnsDaScoObj(AfDecimal impBnsDaScoObj);

    /**
     * Host Variable PMO-PC-ANTIC-BNS
     * 
     */
    AfDecimal getPcAnticBns();

    void setPcAnticBns(AfDecimal pcAnticBns);

    /**
     * Nullable property for PMO-PC-ANTIC-BNS
     * 
     */
    AfDecimal getPcAnticBnsObj();

    void setPcAnticBnsObj(AfDecimal pcAnticBnsObj);

    /**
     * Host Variable PMO-TP-RINN-COLL
     * 
     */
    String getTpRinnColl();

    void setTpRinnColl(String tpRinnColl);

    /**
     * Nullable property for PMO-TP-RINN-COLL
     * 
     */
    String getTpRinnCollObj();

    void setTpRinnCollObj(String tpRinnCollObj);

    /**
     * Host Variable PMO-TP-RIVAL-PRE
     * 
     */
    String getTpRivalPre();

    void setTpRivalPre(String tpRivalPre);

    /**
     * Nullable property for PMO-TP-RIVAL-PRE
     * 
     */
    String getTpRivalPreObj();

    void setTpRivalPreObj(String tpRivalPreObj);

    /**
     * Host Variable PMO-TP-RIVAL-PRSTZ
     * 
     */
    String getTpRivalPrstz();

    void setTpRivalPrstz(String tpRivalPrstz);

    /**
     * Nullable property for PMO-TP-RIVAL-PRSTZ
     * 
     */
    String getTpRivalPrstzObj();

    void setTpRivalPrstzObj(String tpRivalPrstzObj);

    /**
     * Host Variable PMO-FL-EVID-RIVAL
     * 
     */
    char getFlEvidRival();

    void setFlEvidRival(char flEvidRival);

    /**
     * Nullable property for PMO-FL-EVID-RIVAL
     * 
     */
    Character getFlEvidRivalObj();

    void setFlEvidRivalObj(Character flEvidRivalObj);

    /**
     * Host Variable PMO-ULT-PC-PERD
     * 
     */
    AfDecimal getUltPcPerd();

    void setUltPcPerd(AfDecimal ultPcPerd);

    /**
     * Nullable property for PMO-ULT-PC-PERD
     * 
     */
    AfDecimal getUltPcPerdObj();

    void setUltPcPerdObj(AfDecimal ultPcPerdObj);

    /**
     * Host Variable PMO-TOT-AA-GIA-PROR
     * 
     */
    int getTotAaGiaPror();

    void setTotAaGiaPror(int totAaGiaPror);

    /**
     * Nullable property for PMO-TOT-AA-GIA-PROR
     * 
     */
    Integer getTotAaGiaProrObj();

    void setTotAaGiaProrObj(Integer totAaGiaProrObj);

    /**
     * Host Variable PMO-TP-OPZ
     * 
     */
    String getTpOpz();

    void setTpOpz(String tpOpz);

    /**
     * Nullable property for PMO-TP-OPZ
     * 
     */
    String getTpOpzObj();

    void setTpOpzObj(String tpOpzObj);

    /**
     * Host Variable PMO-AA-REN-CER
     * 
     */
    int getAaRenCer();

    void setAaRenCer(int aaRenCer);

    /**
     * Nullable property for PMO-AA-REN-CER
     * 
     */
    Integer getAaRenCerObj();

    void setAaRenCerObj(Integer aaRenCerObj);

    /**
     * Host Variable PMO-PC-REVRSB
     * 
     */
    AfDecimal getPcRevrsb();

    void setPcRevrsb(AfDecimal pcRevrsb);

    /**
     * Nullable property for PMO-PC-REVRSB
     * 
     */
    AfDecimal getPcRevrsbObj();

    void setPcRevrsbObj(AfDecimal pcRevrsbObj);

    /**
     * Host Variable PMO-IMP-RISC-PARZ-PRGT
     * 
     */
    AfDecimal getImpRiscParzPrgt();

    void setImpRiscParzPrgt(AfDecimal impRiscParzPrgt);

    /**
     * Nullable property for PMO-IMP-RISC-PARZ-PRGT
     * 
     */
    AfDecimal getImpRiscParzPrgtObj();

    void setImpRiscParzPrgtObj(AfDecimal impRiscParzPrgtObj);

    /**
     * Host Variable PMO-IMP-LRD-DI-RAT
     * 
     */
    AfDecimal getImpLrdDiRat();

    void setImpLrdDiRat(AfDecimal impLrdDiRat);

    /**
     * Nullable property for PMO-IMP-LRD-DI-RAT
     * 
     */
    AfDecimal getImpLrdDiRatObj();

    void setImpLrdDiRatObj(AfDecimal impLrdDiRatObj);

    /**
     * Host Variable PMO-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for PMO-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable PMO-COS-ONER
     * 
     */
    AfDecimal getCosOner();

    void setCosOner(AfDecimal cosOner);

    /**
     * Nullable property for PMO-COS-ONER
     * 
     */
    AfDecimal getCosOnerObj();

    void setCosOnerObj(AfDecimal cosOnerObj);

    /**
     * Host Variable PMO-SPE-PC
     * 
     */
    AfDecimal getSpePc();

    void setSpePc(AfDecimal spePc);

    /**
     * Nullable property for PMO-SPE-PC
     * 
     */
    AfDecimal getSpePcObj();

    void setSpePcObj(AfDecimal spePcObj);

    /**
     * Host Variable PMO-FL-ATTIV-GAR
     * 
     */
    char getFlAttivGar();

    void setFlAttivGar(char flAttivGar);

    /**
     * Nullable property for PMO-FL-ATTIV-GAR
     * 
     */
    Character getFlAttivGarObj();

    void setFlAttivGarObj(Character flAttivGarObj);

    /**
     * Host Variable PMO-CAMBIO-VER-PROD
     * 
     */
    char getCambioVerProd();

    void setCambioVerProd(char cambioVerProd);

    /**
     * Nullable property for PMO-CAMBIO-VER-PROD
     * 
     */
    Character getCambioVerProdObj();

    void setCambioVerProdObj(Character cambioVerProdObj);

    /**
     * Host Variable PMO-MM-DIFF
     * 
     */
    short getMmDiff();

    void setMmDiff(short mmDiff);

    /**
     * Nullable property for PMO-MM-DIFF
     * 
     */
    Short getMmDiffObj();

    void setMmDiffObj(Short mmDiffObj);

    /**
     * Host Variable PMO-IMP-RAT-MANFEE
     * 
     */
    AfDecimal getImpRatManfee();

    void setImpRatManfee(AfDecimal impRatManfee);

    /**
     * Nullable property for PMO-IMP-RAT-MANFEE
     * 
     */
    AfDecimal getImpRatManfeeObj();

    void setImpRatManfeeObj(AfDecimal impRatManfeeObj);

    /**
     * Host Variable PMO-DT-ULT-EROG-MANFEE-DB
     * 
     */
    String getDtUltErogManfeeDb();

    void setDtUltErogManfeeDb(String dtUltErogManfeeDb);

    /**
     * Nullable property for PMO-DT-ULT-EROG-MANFEE-DB
     * 
     */
    String getDtUltErogManfeeDbObj();

    void setDtUltErogManfeeDbObj(String dtUltErogManfeeDbObj);

    /**
     * Host Variable PMO-TP-OGG-RIVAL
     * 
     */
    String getTpOggRival();

    void setTpOggRival(String tpOggRival);

    /**
     * Nullable property for PMO-TP-OGG-RIVAL
     * 
     */
    String getTpOggRivalObj();

    void setTpOggRivalObj(String tpOggRivalObj);

    /**
     * Host Variable PMO-SOM-ASSTA-GARAC
     * 
     */
    AfDecimal getSomAsstaGarac();

    void setSomAsstaGarac(AfDecimal somAsstaGarac);

    /**
     * Nullable property for PMO-SOM-ASSTA-GARAC
     * 
     */
    AfDecimal getSomAsstaGaracObj();

    void setSomAsstaGaracObj(AfDecimal somAsstaGaracObj);

    /**
     * Host Variable PMO-PC-APPLZ-OPZ
     * 
     */
    AfDecimal getPcApplzOpz();

    void setPcApplzOpz(AfDecimal pcApplzOpz);

    /**
     * Nullable property for PMO-PC-APPLZ-OPZ
     * 
     */
    AfDecimal getPcApplzOpzObj();

    void setPcApplzOpzObj(AfDecimal pcApplzOpzObj);

    /**
     * Nullable property for PMO-ID-ADES
     * 
     */
    Integer getPmoIdAdesObj();

    void setPmoIdAdesObj(Integer pmoIdAdesObj);

    /**
     * Host Variable PMO-DS-RIGA
     * 
     */
    long getPmoDsRiga();

    void setPmoDsRiga(long pmoDsRiga);

    /**
     * Host Variable PMO-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable PMO-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable PMO-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable PMO-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable PMO-TP-ESTR-CNT
     * 
     */
    String getTpEstrCnt();

    void setTpEstrCnt(String tpEstrCnt);

    /**
     * Nullable property for PMO-TP-ESTR-CNT
     * 
     */
    String getTpEstrCntObj();

    void setTpEstrCntObj(String tpEstrCntObj);

    /**
     * Host Variable PMO-COD-RAMO
     * 
     */
    String getCodRamo();

    void setCodRamo(String codRamo);

    /**
     * Nullable property for PMO-COD-RAMO
     * 
     */
    String getCodRamoObj();

    void setCodRamoObj(String codRamoObj);

    /**
     * Host Variable PMO-GEN-DA-SIN
     * 
     */
    char getGenDaSin();

    void setGenDaSin(char genDaSin);

    /**
     * Nullable property for PMO-GEN-DA-SIN
     * 
     */
    Character getGenDaSinObj();

    void setGenDaSinObj(Character genDaSinObj);

    /**
     * Host Variable PMO-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Nullable property for PMO-COD-TARI
     * 
     */
    String getCodTariObj();

    void setCodTariObj(String codTariObj);

    /**
     * Host Variable PMO-NUM-RAT-PAG-PRE
     * 
     */
    int getNumRatPagPre();

    void setNumRatPagPre(int numRatPagPre);

    /**
     * Nullable property for PMO-NUM-RAT-PAG-PRE
     * 
     */
    Integer getNumRatPagPreObj();

    void setNumRatPagPreObj(Integer numRatPagPreObj);

    /**
     * Host Variable PMO-PC-SERV-VAL
     * 
     */
    AfDecimal getPcServVal();

    void setPcServVal(AfDecimal pcServVal);

    /**
     * Nullable property for PMO-PC-SERV-VAL
     * 
     */
    AfDecimal getPcServValObj();

    void setPcServValObj(AfDecimal pcServValObj);

    /**
     * Host Variable PMO-ETA-AA-SOGL-BNFICR
     * 
     */
    short getEtaAaSoglBnficr();

    void setEtaAaSoglBnficr(short etaAaSoglBnficr);

    /**
     * Nullable property for PMO-ETA-AA-SOGL-BNFICR
     * 
     */
    Short getEtaAaSoglBnficrObj();

    void setEtaAaSoglBnficrObj(Short etaAaSoglBnficrObj);
};
