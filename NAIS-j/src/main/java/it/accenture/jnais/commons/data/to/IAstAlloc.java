package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [AST_ALLOC]
 * 
 */
public interface IAstAlloc extends BaseSqlTo {

    /**
     * Host Variable ALL-ID-AST-ALLOC
     * 
     */
    int getIdAstAlloc();

    void setIdAstAlloc(int idAstAlloc);

    /**
     * Host Variable ALL-ID-STRA-DI-INVST
     * 
     */
    int getIdStraDiInvst();

    void setIdStraDiInvst(int idStraDiInvst);

    /**
     * Host Variable ALL-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable ALL-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for ALL-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable ALL-DT-INI-VLDT-DB
     * 
     */
    String getDtIniVldtDb();

    void setDtIniVldtDb(String dtIniVldtDb);

    /**
     * Host Variable ALL-DT-END-VLDT-DB
     * 
     */
    String getDtEndVldtDb();

    void setDtEndVldtDb(String dtEndVldtDb);

    /**
     * Nullable property for ALL-DT-END-VLDT-DB
     * 
     */
    String getDtEndVldtDbObj();

    void setDtEndVldtDbObj(String dtEndVldtDbObj);

    /**
     * Host Variable ALL-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable ALL-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable ALL-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable ALL-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Nullable property for ALL-COD-FND
     * 
     */
    String getCodFndObj();

    void setCodFndObj(String codFndObj);

    /**
     * Host Variable ALL-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Nullable property for ALL-COD-TARI
     * 
     */
    String getCodTariObj();

    void setCodTariObj(String codTariObj);

    /**
     * Host Variable ALL-TP-APPLZ-AST
     * 
     */
    String getTpApplzAst();

    void setTpApplzAst(String tpApplzAst);

    /**
     * Nullable property for ALL-TP-APPLZ-AST
     * 
     */
    String getTpApplzAstObj();

    void setTpApplzAstObj(String tpApplzAstObj);

    /**
     * Host Variable ALL-PC-RIP-AST
     * 
     */
    AfDecimal getPcRipAst();

    void setPcRipAst(AfDecimal pcRipAst);

    /**
     * Nullable property for ALL-PC-RIP-AST
     * 
     */
    AfDecimal getPcRipAstObj();

    void setPcRipAstObj(AfDecimal pcRipAstObj);

    /**
     * Host Variable ALL-TP-FND
     * 
     */
    char getTpFnd();

    void setTpFnd(char tpFnd);

    /**
     * Host Variable ALL-DS-RIGA
     * 
     */
    long getAllDsRiga();

    void setAllDsRiga(long allDsRiga);

    /**
     * Host Variable ALL-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable ALL-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable ALL-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable ALL-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable ALL-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable ALL-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable ALL-PERIODO
     * 
     */
    short getPeriodo();

    void setPeriodo(short periodo);

    /**
     * Nullable property for ALL-PERIODO
     * 
     */
    Short getPeriodoObj();

    void setPeriodoObj(Short periodoObj);

    /**
     * Host Variable ALL-TP-RIBIL-FND
     * 
     */
    String getTpRibilFnd();

    void setTpRibilFnd(String tpRibilFnd);

    /**
     * Nullable property for ALL-TP-RIBIL-FND
     * 
     */
    String getTpRibilFndObj();

    void setTpRibilFndObj(String tpRibilFndObj);
};
