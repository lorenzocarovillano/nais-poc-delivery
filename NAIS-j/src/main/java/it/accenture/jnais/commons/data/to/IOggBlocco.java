package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [OGG_BLOCCO]
 * 
 */
public interface IOggBlocco extends BaseSqlTo {

    /**
     * Host Variable L11-ID-OGG
     * 
     */
    int getL11IdOgg();

    void setL11IdOgg(int l11IdOgg);

    /**
     * Host Variable L11-TP-OGG
     * 
     */
    String getL11TpOgg();

    void setL11TpOgg(String l11TpOgg);

    /**
     * Host Variable L11-TP-STAT-BLOCCO
     * 
     */
    String getL11TpStatBlocco();

    void setL11TpStatBlocco(String l11TpStatBlocco);

    /**
     * Host Variable L11-COD-BLOCCO
     * 
     */
    String getL11CodBlocco();

    void setL11CodBlocco(String l11CodBlocco);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable L11-ID-OGG-BLOCCO
     * 
     */
    int getL11IdOggBlocco();

    void setL11IdOggBlocco(int l11IdOggBlocco);

    /**
     * Host Variable L11-ID-OGG-1RIO
     * 
     */
    int getIdOgg1rio();

    void setIdOgg1rio(int idOgg1rio);

    /**
     * Nullable property for L11-ID-OGG-1RIO
     * 
     */
    Integer getIdOgg1rioObj();

    void setIdOgg1rioObj(Integer idOgg1rioObj);

    /**
     * Host Variable L11-TP-OGG-1RIO
     * 
     */
    String getTpOgg1rio();

    void setTpOgg1rio(String tpOgg1rio);

    /**
     * Nullable property for L11-TP-OGG-1RIO
     * 
     */
    String getTpOgg1rioObj();

    void setTpOgg1rioObj(String tpOgg1rioObj);

    /**
     * Host Variable L11-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L11-TP-MOVI
     * 
     */
    int getTpMovi();

    void setTpMovi(int tpMovi);

    /**
     * Host Variable L11-DT-EFF-DB
     * 
     */
    String getDtEffDb();

    void setDtEffDb(String dtEffDb);

    /**
     * Host Variable L11-ID-RICH
     * 
     */
    int getIdRich();

    void setIdRich(int idRich);

    /**
     * Nullable property for L11-ID-RICH
     * 
     */
    Integer getIdRichObj();

    void setIdRichObj(Integer idRichObj);

    /**
     * Host Variable L11-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L11-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L11-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable L11-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable L11-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
