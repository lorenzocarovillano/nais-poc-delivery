package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBilaTrchEstr;

/**
 * Data Access Object(DAO) for table [BILA_TRCH_ESTR]
 * 
 */
public class BilaTrchEstrDao extends BaseSqlDao<IBilaTrchEstr> {

    private Cursor cNst9;
    private final IRowMapper<IBilaTrchEstr> selectByB03IdBilaTrchEstrRm = buildNamedRowMapper(IBilaTrchEstr.class, "b03IdBilaTrchEstr", "codCompAnia", "b03IdRichEstrazMas", "idRichEstrazAggObj", "flSimulazioneObj", "dtRisDb", "dtProduzioneDb", "b03IdPoli", "b03IdAdes", "idGar", "b03IdTrchDiGar", "tpFrmAssva", "tpRamoBila", "tpCalcRis", "codRamo", "codTari", "dtIniValTarDbObj", "codProdObj", "dtIniVldtProdDb", "codTariOrgnObj", "minGartoTObj", "tpTariObj", "tpPreObj", "tpAdegPreObj", "tpRivalObj", "flDaTrasfObj", "flCarContObj", "flPreDaRisObj", "flPreAggObj", "tpTrchObj", "tpTstObj", "codConvObj", "dtDecorPoliDb", "dtDecorAdesDbObj", "dtDecorTrchDb", "dtEmisPoliDb", "dtEmisTrchDbObj", "dtScadTrchDbObj", "dtScadIntmdDbObj", "dtScadPagPreDbObj", "dtUltPrePagDbObj", "dtNasc1oAsstoDbObj", "sex1oAsstoObj", "etaAa1oAsstoObj", "etaMm1oAsstoObj", "etaRaggnDtCalcObj", "durAaObj", "durMmObj", "durGgObj", "dur1oPerAaObj", "dur1oPerMmObj", "dur1oPerGgObj", "antidurRicorPrecObj", "antidurDtCalcObj", "durResDtCalcObj", "tpStatBusPoli", "tpCausPoli", "tpStatBusAdes", "tpCausAdes", "tpStatBusTrch", "tpCausTrch", "dtEffCambStatDbObj", "dtEmisCambStatDbObj", "dtEffStabDbObj", "cptDtStabObj", "dtEffRidzDbObj", "dtEmisRidzDbObj", "cptDtRidzObj", "frazObj", "durPagPreObj", "numPrePattObj", "frazIniErogRenObj", "aaRenCerObj", "ratRenObj", "codDivObj", "riscparObj", "cumRiscparObj", "ultRmObj", "tsRendtoTObj", "alqRetrTObj", "minTrnutTObj", "tsNetTObj", "dtUltRivalDbObj", "prstzIniObj", "prstzAggIniObj", "prstzAggUltObj", "rappelObj", "prePattuitoIniObj", "preDovIniObj", "preDovRivtoTObj", "preAnnualizRicorObj", "preContObj", "prePpIniObj", "risPuraTObj", "provAcqObj", "provAcqRicorObj", "provIncObj", "carAcqNonSconObj", "overCommObj", "carAcqPrecontatoObj", "risAcqTObj", "risZilTObj", "carGestNonSconObj", "carGestObj", "risSpeTObj", "carIncNonSconObj", "carIncObj", "risRistorniCapObj", "intrTecnObj", "cptRshMorObj", "cSubrshTObj", "preRshTObj", "alqMargRisObj", "alqMargCSubrshObj", "tsRendtoSpprObj", "tpIasObj", "nsQuoObj", "tsMedioObj", "cptRiastoObj", "preRiastoObj", "risRiastaObj", "cptRiastoEccObj", "preRiastoEccObj", "risRiastaEccObj", "codAgeObj", "codSubageObj", "codCanObj", "ibPoliObj", "ibAdesObj", "ibTrchDiGarObj", "tpPrstzObj", "tpTrasfObj", "ppInvrioTariObj", "coeffOpzRenObj", "coeffOpzCptObj", "durPagRenObj", "vltObj", "risMatChiuPrecObj", "codFndObj", "prstzTObj", "tsTariDovObj", "tsTariSconObj", "tsPpObj", "coeffRis1TObj", "coeffRis2TObj", "abbObj", "tpCoassObj", "tratRiassObj", "tratRiassEccObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "tpRgmFiscObj", "durGarAaObj", "durGarMmObj", "durGarGgObj", "antidurCalc365Obj", "codFiscCntrObj", "codFiscAssto1Obj", "codFiscAssto2Obj", "codFiscAssto3Obj", "causSconVcharObj", "emitTitOpzVcharObj", "qtzSpZCoupEmisObj", "qtzSpZOpzEmisObj", "qtzSpZCoupDtCObj", "qtzSpZOpzDtCaObj", "qtzTotEmisObj", "qtzTotDtCalcObj", "qtzTotDtUltBilObj", "dtQtzEmisDbObj", "pcCarGestObj", "pcCarAcqObj", "impCarCasoMorObj", "pcCarMorObj", "tpVersObj", "flSwitchObj", "flIasObj", "dirObj", "tpCopCasoMorObj", "metRiscSpclObj", "tpStatInvstObj", "codPrdtObj", "statAssto1Obj", "statAssto2Obj", "statAssto3Obj", "cptAsstoIniMorObj", "tsStabPreObj", "dirEmisObj", "dtIncUltPreDbObj", "statTbgcAssto1Obj", "statTbgcAssto2Obj", "statTbgcAssto3Obj", "frazDecrCptObj", "prePpUltObj", "acqExpObj", "remunAssObj", "commisInterObj", "numFinanzObj", "tpAccCommObj", "ibAccCommObj", "ramoBila", "carzObj");

    public BilaTrchEstrDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBilaTrchEstr> getToClass() {
        return IBilaTrchEstr.class;
    }

    public IBilaTrchEstr selectByB03IdBilaTrchEstr(int b03IdBilaTrchEstr, IBilaTrchEstr iBilaTrchEstr) {
        return buildQuery("selectByB03IdBilaTrchEstr").bind("b03IdBilaTrchEstr", b03IdBilaTrchEstr).rowMapper(selectByB03IdBilaTrchEstrRm).singleResult(iBilaTrchEstr);
    }

    public DbAccessStatus insertRec(IBilaTrchEstr iBilaTrchEstr) {
        return buildQuery("insertRec").bind(iBilaTrchEstr).executeInsert();
    }

    public DbAccessStatus updateRec(IBilaTrchEstr iBilaTrchEstr) {
        return buildQuery("updateRec").bind(iBilaTrchEstr).executeUpdate();
    }

    public DbAccessStatus deleteByB03IdBilaTrchEstr(int b03IdBilaTrchEstr) {
        return buildQuery("deleteByB03IdBilaTrchEstr").bind("b03IdBilaTrchEstr", b03IdBilaTrchEstr).executeDelete();
    }

    public IBilaTrchEstr selectRec(IBilaTrchEstr iBilaTrchEstr) {
        return buildQuery("selectRec").bind(iBilaTrchEstr).rowMapper(selectByB03IdBilaTrchEstrRm).singleResult(iBilaTrchEstr);
    }

    public DbAccessStatus openCNst9(IBilaTrchEstr iBilaTrchEstr) {
        cNst9 = buildQuery("openCNst9").bind(iBilaTrchEstr).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst9() {
        return closeCursor(cNst9);
    }

    public IBilaTrchEstr fetchCNst9(IBilaTrchEstr iBilaTrchEstr) {
        return fetch(cNst9, iBilaTrchEstr, selectByB03IdBilaTrchEstrRm);
    }

    public DbAccessStatus updateRec1(IBilaTrchEstr iBilaTrchEstr) {
        return buildQuery("updateRec1").bind(iBilaTrchEstr).executeUpdate();
    }

    public DbAccessStatus updateRec2(IBilaTrchEstr iBilaTrchEstr) {
        return buildQuery("updateRec2").bind(iBilaTrchEstr).executeUpdate();
    }

    public DbAccessStatus updateRec3(IBilaTrchEstr iBilaTrchEstr) {
        return buildQuery("updateRec3").bind(iBilaTrchEstr).executeUpdate();
    }
}
