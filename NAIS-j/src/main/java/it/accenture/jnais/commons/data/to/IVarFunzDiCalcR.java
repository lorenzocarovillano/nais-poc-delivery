package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [VAR_FUNZ_DI_CALC_R]
 * 
 */
public interface IVarFunzDiCalcR extends BaseSqlTo {

    /**
     * Host Variable AC5-IDCOMP
     * 
     */
    short getIdcomp();

    void setIdcomp(short idcomp);

    /**
     * Host Variable AC5-CODPROD-VCHAR
     * 
     */
    String getCodprodVchar();

    void setCodprodVchar(String codprodVchar);

    /**
     * Host Variable AC5-CODTARI-VCHAR
     * 
     */
    String getCodtariVchar();

    void setCodtariVchar(String codtariVchar);

    /**
     * Host Variable AC5-DINIZ
     * 
     */
    int getDiniz();

    void setDiniz(int diniz);

    /**
     * Host Variable AC5-NOMEFUNZ-VCHAR
     * 
     */
    String getNomefunzVchar();

    void setNomefunzVchar(String nomefunzVchar);

    /**
     * Host Variable AC5-STEP-ELAB-VCHAR
     * 
     */
    String getStepElabVchar();

    void setStepElabVchar(String stepElabVchar);

    /**
     * Host Variable AC5-DEND
     * 
     */
    int getDend();

    void setDend(int dend);

    /**
     * Host Variable AC5-ISPRECALC
     * 
     */
    short getIsprecalc();

    void setIsprecalc(short isprecalc);

    /**
     * Nullable property for AC5-ISPRECALC
     * 
     */
    Short getIsprecalcObj();

    void setIsprecalcObj(Short isprecalcObj);

    /**
     * Host Variable AC5-NOMEPROGR-VCHAR
     * 
     */
    String getNomeprogrVchar();

    void setNomeprogrVchar(String nomeprogrVchar);

    /**
     * Nullable property for AC5-NOMEPROGR-VCHAR
     * 
     */
    String getNomeprogrVcharObj();

    void setNomeprogrVcharObj(String nomeprogrVcharObj);

    /**
     * Host Variable AC5-MODCALC-VCHAR
     * 
     */
    String getModcalcVchar();

    void setModcalcVchar(String modcalcVchar);

    /**
     * Nullable property for AC5-MODCALC-VCHAR
     * 
     */
    String getModcalcVcharObj();

    void setModcalcVcharObj(String modcalcVcharObj);

    /**
     * Host Variable AC5-GLOVARLIST-VCHAR
     * 
     */
    String getGlovarlistVchar();

    void setGlovarlistVchar(String glovarlistVchar);

    /**
     * Nullable property for AC5-GLOVARLIST-VCHAR
     * 
     */
    String getGlovarlistVcharObj();

    void setGlovarlistVcharObj(String glovarlistVcharObj);
};
