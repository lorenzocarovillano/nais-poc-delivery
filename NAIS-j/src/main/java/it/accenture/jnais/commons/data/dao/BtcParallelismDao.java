package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBtcParallelism;

/**
 * Data Access Object(DAO) for table [BTC_PARALLELISM]
 * 
 */
public class BtcParallelismDao extends BaseSqlDao<IBtcParallelism> {

    private Cursor cursorBpa;
    private final IRowMapper<IBtcParallelism> selectRecRm = buildNamedRowMapper(IBtcParallelism.class, "bpaCodCompAnia", "bpaProtocol", "bpaProgProtocol", "codBatchState", "dtInsDb", "userIns", "dtStartDbObj", "dtEndDbObj", "userStartObj", "descParallelismVcharObj", "idOggDaObj", "idOggAObj", "tpOggObj", "numRowScheduleObj");

    public BtcParallelismDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBtcParallelism> getToClass() {
        return IBtcParallelism.class;
    }

    public IBtcParallelism selectRec(IBtcParallelism iBtcParallelism) {
        return buildQuery("selectRec").bind(iBtcParallelism).rowMapper(selectRecRm).singleResult(iBtcParallelism);
    }

    public DbAccessStatus updateRec(IBtcParallelism iBtcParallelism) {
        return buildQuery("updateRec").bind(iBtcParallelism).executeUpdate();
    }

    public DbAccessStatus openCursorBpa(int codCompAnia, String protocol) {
        cursorBpa = buildQuery("openCursorBpa").bind("codCompAnia", codCompAnia).bind("protocol", protocol).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCursorBpa() {
        return closeCursor(cursorBpa);
    }

    public IBtcParallelism fetchCursorBpa(IBtcParallelism iBtcParallelism) {
        return fetch(cursorBpa, iBtcParallelism, selectRecRm);
    }

    public IBtcParallelism selectRec1(int codCompAnia, String protocol, int progProtocol, IBtcParallelism iBtcParallelism) {
        return buildQuery("selectRec1").bind("codCompAnia", codCompAnia).bind("protocol", protocol).bind("progProtocol", progProtocol).rowMapper(selectRecRm).singleResult(iBtcParallelism);
    }

    public DbAccessStatus insertRec(IBtcParallelism iBtcParallelism) {
        return buildQuery("insertRec").bind(iBtcParallelism).executeInsert();
    }

    public DbAccessStatus updateRec1(IBtcParallelism iBtcParallelism) {
        return buildQuery("updateRec1").bind(iBtcParallelism).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompAnia, String protocol, int progProtocol) {
        return buildQuery("deleteRec").bind("codCompAnia", codCompAnia).bind("protocol", protocol).bind("progProtocol", progProtocol).executeDelete();
    }
}
