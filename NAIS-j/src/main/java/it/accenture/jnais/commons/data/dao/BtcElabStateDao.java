package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBtcElabState;

/**
 * Data Access Object(DAO) for table [BTC_ELAB_STATE]
 * 
 */
public class BtcElabStateDao extends BaseSqlDao<IBtcElabState> {

    private Cursor curBes;
    private final IRowMapper<IBtcElabState> fetchCurBesRm = buildNamedRowMapper(IBtcElabState.class, "codElabState", "des", "flagToExecuteObj");

    public BtcElabStateDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBtcElabState> getToClass() {
        return IBtcElabState.class;
    }

    public IBtcElabState selectByIabv0002StateCurrent(char iabv0002StateCurrent, IBtcElabState iBtcElabState) {
        return buildQuery("selectByIabv0002StateCurrent").bind("iabv0002StateCurrent", String.valueOf(iabv0002StateCurrent)).singleResult(iBtcElabState);
    }

    public DbAccessStatus updateRec(IBtcElabState iBtcElabState) {
        return buildQuery("updateRec").bind(iBtcElabState).executeUpdate();
    }

    public DbAccessStatus openCurBes() {
        curBes = buildQuery("openCurBes").withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurBes() {
        return closeCursor(curBes);
    }

    public IBtcElabState fetchCurBes(IBtcElabState iBtcElabState) {
        return fetch(curBes, iBtcElabState, fetchCurBesRm);
    }
}
