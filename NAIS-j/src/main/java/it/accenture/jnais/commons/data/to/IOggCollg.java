package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [OGG_COLLG]
 * 
 */
public interface IOggCollg extends BaseSqlTo {

    /**
     * Host Variable LDBV0721-ID-OGG-COLLG
     * 
     */
    int getLdbv0721IdOggCollg();

    void setLdbv0721IdOggCollg(int ldbv0721IdOggCollg);

    /**
     * Host Variable LDBV0721-TP-OGG-COLLG
     * 
     */
    String getLdbv0721TpOggCollg();

    void setLdbv0721TpOggCollg(String ldbv0721TpOggCollg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable OCO-ID-OGG-COLLG
     * 
     */
    int getIdOggCollg();

    void setIdOggCollg(int idOggCollg);

    /**
     * Host Variable OCO-ID-OGG-COINV
     * 
     */
    int getIdOggCoinv();

    void setIdOggCoinv(int idOggCoinv);

    /**
     * Host Variable OCO-TP-OGG-COINV
     * 
     */
    String getTpOggCoinv();

    void setTpOggCoinv(String tpOggCoinv);

    /**
     * Host Variable OCO-ID-OGG-DER
     * 
     */
    int getIdOggDer();

    void setIdOggDer(int idOggDer);

    /**
     * Host Variable OCO-TP-OGG-DER
     * 
     */
    String getTpOggDer();

    void setTpOggDer(String tpOggDer);

    /**
     * Host Variable OCO-IB-RIFTO-ESTNO
     * 
     */
    String getIbRiftoEstno();

    void setIbRiftoEstno(String ibRiftoEstno);

    /**
     * Nullable property for OCO-IB-RIFTO-ESTNO
     * 
     */
    String getIbRiftoEstnoObj();

    void setIbRiftoEstnoObj(String ibRiftoEstnoObj);

    /**
     * Host Variable OCO-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable OCO-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for OCO-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable OCO-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable OCO-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable OCO-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable OCO-COD-PROD
     * 
     */
    String getCodProd();

    void setCodProd(String codProd);

    /**
     * Host Variable OCO-DT-SCAD-DB
     * 
     */
    String getDtScadDb();

    void setDtScadDb(String dtScadDb);

    /**
     * Nullable property for OCO-DT-SCAD-DB
     * 
     */
    String getDtScadDbObj();

    void setDtScadDbObj(String dtScadDbObj);

    /**
     * Host Variable OCO-TP-COLLGM
     * 
     */
    String getTpCollgm();

    void setTpCollgm(String tpCollgm);

    /**
     * Host Variable OCO-TP-TRASF
     * 
     */
    String getTpTrasf();

    void setTpTrasf(String tpTrasf);

    /**
     * Nullable property for OCO-TP-TRASF
     * 
     */
    String getTpTrasfObj();

    void setTpTrasfObj(String tpTrasfObj);

    /**
     * Host Variable OCO-REC-PROV
     * 
     */
    AfDecimal getRecProv();

    void setRecProv(AfDecimal recProv);

    /**
     * Nullable property for OCO-REC-PROV
     * 
     */
    AfDecimal getRecProvObj();

    void setRecProvObj(AfDecimal recProvObj);

    /**
     * Host Variable OCO-DT-DECOR-DB
     * 
     */
    String getDtDecorDb();

    void setDtDecorDb(String dtDecorDb);

    /**
     * Nullable property for OCO-DT-DECOR-DB
     * 
     */
    String getDtDecorDbObj();

    void setDtDecorDbObj(String dtDecorDbObj);

    /**
     * Host Variable OCO-DT-ULT-PRE-PAG-DB
     * 
     */
    String getDtUltPrePagDb();

    void setDtUltPrePagDb(String dtUltPrePagDb);

    /**
     * Nullable property for OCO-DT-ULT-PRE-PAG-DB
     * 
     */
    String getDtUltPrePagDbObj();

    void setDtUltPrePagDbObj(String dtUltPrePagDbObj);

    /**
     * Host Variable OCO-TOT-PRE
     * 
     */
    AfDecimal getTotPre();

    void setTotPre(AfDecimal totPre);

    /**
     * Nullable property for OCO-TOT-PRE
     * 
     */
    AfDecimal getTotPreObj();

    void setTotPreObj(AfDecimal totPreObj);

    /**
     * Host Variable OCO-RIS-MAT
     * 
     */
    AfDecimal getRisMat();

    void setRisMat(AfDecimal risMat);

    /**
     * Nullable property for OCO-RIS-MAT
     * 
     */
    AfDecimal getRisMatObj();

    void setRisMatObj(AfDecimal risMatObj);

    /**
     * Host Variable OCO-RIS-ZIL
     * 
     */
    AfDecimal getRisZil();

    void setRisZil(AfDecimal risZil);

    /**
     * Nullable property for OCO-RIS-ZIL
     * 
     */
    AfDecimal getRisZilObj();

    void setRisZilObj(AfDecimal risZilObj);

    /**
     * Host Variable OCO-IMP-TRASF
     * 
     */
    AfDecimal getImpTrasf();

    void setImpTrasf(AfDecimal impTrasf);

    /**
     * Nullable property for OCO-IMP-TRASF
     * 
     */
    AfDecimal getImpTrasfObj();

    void setImpTrasfObj(AfDecimal impTrasfObj);

    /**
     * Host Variable OCO-IMP-REINVST
     * 
     */
    AfDecimal getImpReinvst();

    void setImpReinvst(AfDecimal impReinvst);

    /**
     * Nullable property for OCO-IMP-REINVST
     * 
     */
    AfDecimal getImpReinvstObj();

    void setImpReinvstObj(AfDecimal impReinvstObj);

    /**
     * Host Variable OCO-PC-REINVST-RILIEVI
     * 
     */
    AfDecimal getPcReinvstRilievi();

    void setPcReinvstRilievi(AfDecimal pcReinvstRilievi);

    /**
     * Nullable property for OCO-PC-REINVST-RILIEVI
     * 
     */
    AfDecimal getPcReinvstRilieviObj();

    void setPcReinvstRilieviObj(AfDecimal pcReinvstRilieviObj);

    /**
     * Host Variable OCO-IB-2O-RIFTO-ESTNO
     * 
     */
    String getIb2oRiftoEstno();

    void setIb2oRiftoEstno(String ib2oRiftoEstno);

    /**
     * Nullable property for OCO-IB-2O-RIFTO-ESTNO
     * 
     */
    String getIb2oRiftoEstnoObj();

    void setIb2oRiftoEstnoObj(String ib2oRiftoEstnoObj);

    /**
     * Host Variable OCO-IND-LIQ-AGG-MAN
     * 
     */
    char getIndLiqAggMan();

    void setIndLiqAggMan(char indLiqAggMan);

    /**
     * Nullable property for OCO-IND-LIQ-AGG-MAN
     * 
     */
    Character getIndLiqAggManObj();

    void setIndLiqAggManObj(Character indLiqAggManObj);

    /**
     * Host Variable OCO-DS-RIGA
     * 
     */
    long getOcoDsRiga();

    void setOcoDsRiga(long ocoDsRiga);

    /**
     * Host Variable OCO-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable OCO-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable OCO-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable OCO-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable OCO-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable OCO-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable OCO-IMP-COLLG
     * 
     */
    AfDecimal getImpCollg();

    void setImpCollg(AfDecimal impCollg);

    /**
     * Nullable property for OCO-IMP-COLLG
     * 
     */
    AfDecimal getImpCollgObj();

    void setImpCollgObj(AfDecimal impCollgObj);

    /**
     * Host Variable OCO-PRE-PER-TRASF
     * 
     */
    AfDecimal getPrePerTrasf();

    void setPrePerTrasf(AfDecimal prePerTrasf);

    /**
     * Nullable property for OCO-PRE-PER-TRASF
     * 
     */
    AfDecimal getPrePerTrasfObj();

    void setPrePerTrasfObj(AfDecimal prePerTrasfObj);

    /**
     * Host Variable OCO-CAR-ACQ
     * 
     */
    AfDecimal getCarAcq();

    void setCarAcq(AfDecimal carAcq);

    /**
     * Nullable property for OCO-CAR-ACQ
     * 
     */
    AfDecimal getCarAcqObj();

    void setCarAcqObj(AfDecimal carAcqObj);

    /**
     * Host Variable OCO-PRE-1A-ANNUALITA
     * 
     */
    AfDecimal getPre1aAnnualita();

    void setPre1aAnnualita(AfDecimal pre1aAnnualita);

    /**
     * Nullable property for OCO-PRE-1A-ANNUALITA
     * 
     */
    AfDecimal getPre1aAnnualitaObj();

    void setPre1aAnnualitaObj(AfDecimal pre1aAnnualitaObj);

    /**
     * Host Variable OCO-IMP-TRASFERITO
     * 
     */
    AfDecimal getImpTrasferito();

    void setImpTrasferito(AfDecimal impTrasferito);

    /**
     * Nullable property for OCO-IMP-TRASFERITO
     * 
     */
    AfDecimal getImpTrasferitoObj();

    void setImpTrasferitoObj(AfDecimal impTrasferitoObj);

    /**
     * Host Variable OCO-TP-MOD-ABBINAMENTO
     * 
     */
    String getTpModAbbinamento();

    void setTpModAbbinamento(String tpModAbbinamento);

    /**
     * Nullable property for OCO-TP-MOD-ABBINAMENTO
     * 
     */
    String getTpModAbbinamentoObj();

    void setTpModAbbinamentoObj(String tpModAbbinamentoObj);

    /**
     * Host Variable OCO-PC-PRE-TRASFERITO
     * 
     */
    AfDecimal getPcPreTrasferito();

    void setPcPreTrasferito(AfDecimal pcPreTrasferito);

    /**
     * Nullable property for OCO-PC-PRE-TRASFERITO
     * 
     */
    AfDecimal getPcPreTrasferitoObj();

    void setPcPreTrasferitoObj(AfDecimal pcPreTrasferitoObj);

    /**
     * Host Variable LDBV5571-TOT-IMP-COLL
     * 
     */
    AfDecimal getLdbv5571TotImpColl();

    void setLdbv5571TotImpColl(AfDecimal ldbv5571TotImpColl);

    /**
     * Nullable property for LDBV5571-TOT-IMP-COLL
     * 
     */
    AfDecimal getLdbv5571TotImpCollObj();

    void setLdbv5571TotImpCollObj(AfDecimal ldbv5571TotImpCollObj);

    /**
     * Host Variable LDBV5571-ID-OGG-DER
     * 
     */
    int getLdbv5571IdOggDer();

    void setLdbv5571IdOggDer(int ldbv5571IdOggDer);

    /**
     * Host Variable LDBV5571-TP-OGG-DER
     * 
     */
    String getLdbv5571TpOggDer();

    void setLdbv5571TpOggDer(String ldbv5571TpOggDer);

    /**
     * Host Variable LDBV5571-TP-COLL-1
     * 
     */
    String getLdbv5571TpColl1();

    void setLdbv5571TpColl1(String ldbv5571TpColl1);

    /**
     * Host Variable LDBV5571-TP-COLL-2
     * 
     */
    String getLdbv5571TpColl2();

    void setLdbv5571TpColl2(String ldbv5571TpColl2);

    /**
     * Host Variable LDBV5571-TP-COLL-3
     * 
     */
    String getLdbv5571TpColl3();

    void setLdbv5571TpColl3(String ldbv5571TpColl3);
};
