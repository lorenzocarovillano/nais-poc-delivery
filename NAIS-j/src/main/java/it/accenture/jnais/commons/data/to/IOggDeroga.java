package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [OGG_DEROGA]
 * 
 */
public interface IOggDeroga extends BaseSqlTo {

    /**
     * Host Variable ODE-ID-OGG
     * 
     */
    int getOdeIdOgg();

    void setOdeIdOgg(int odeIdOgg);

    /**
     * Host Variable ODE-TP-OGG
     * 
     */
    String getOdeTpOgg();

    void setOdeTpOgg(String odeTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable ODE-ID-OGG-DEROGA
     * 
     */
    int getIdOggDeroga();

    void setIdOggDeroga(int idOggDeroga);

    /**
     * Host Variable ODE-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable ODE-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for ODE-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable ODE-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable ODE-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable ODE-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable ODE-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for ODE-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable ODE-TP-DEROGA
     * 
     */
    String getTpDeroga();

    void setTpDeroga(String tpDeroga);

    /**
     * Host Variable ODE-COD-GR-AUT-APPRT
     * 
     */
    long getCodGrAutApprt();

    void setCodGrAutApprt(long codGrAutApprt);

    /**
     * Host Variable ODE-COD-LIV-AUT-APPRT
     * 
     */
    int getCodLivAutApprt();

    void setCodLivAutApprt(int codLivAutApprt);

    /**
     * Nullable property for ODE-COD-LIV-AUT-APPRT
     * 
     */
    Integer getCodLivAutApprtObj();

    void setCodLivAutApprtObj(Integer codLivAutApprtObj);

    /**
     * Host Variable ODE-COD-GR-AUT-SUP
     * 
     */
    long getCodGrAutSup();

    void setCodGrAutSup(long codGrAutSup);

    /**
     * Host Variable ODE-COD-LIV-AUT-SUP
     * 
     */
    int getCodLivAutSup();

    void setCodLivAutSup(int codLivAutSup);

    /**
     * Host Variable ODE-DS-RIGA
     * 
     */
    long getOdeDsRiga();

    void setOdeDsRiga(long odeDsRiga);

    /**
     * Host Variable ODE-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable ODE-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable ODE-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable ODE-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable ODE-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable ODE-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
