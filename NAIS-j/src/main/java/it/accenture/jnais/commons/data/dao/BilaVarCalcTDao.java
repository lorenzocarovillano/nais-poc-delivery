package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBilaVarCalcT;

/**
 * Data Access Object(DAO) for table [BILA_VAR_CALC_T]
 * 
 */
public class BilaVarCalcTDao extends BaseSqlDao<IBilaVarCalcT> {

    private final IRowMapper<IBilaVarCalcT> selectByB05IdBilaVarCalcTRm = buildNamedRowMapper(IBilaVarCalcT.class, "b05IdBilaVarCalcT", "codCompAnia", "idBilaTrchEstr", "idRichEstrazMas", "idRichEstrazAggObj", "dtRisDbObj", "idPoli", "idAdes", "idTrchDiGar", "progSchedaValorObj", "dtIniVldtTariDb", "tpRgmFisc", "dtIniVldtProdDb", "dtDecorTrchDb", "codVar", "tpD", "valImpObj", "valPcObj", "valStringaObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "areaDValorVarVcharObj");

    public BilaVarCalcTDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBilaVarCalcT> getToClass() {
        return IBilaVarCalcT.class;
    }

    public IBilaVarCalcT selectByB05IdBilaVarCalcT(int b05IdBilaVarCalcT, IBilaVarCalcT iBilaVarCalcT) {
        return buildQuery("selectByB05IdBilaVarCalcT").bind("b05IdBilaVarCalcT", b05IdBilaVarCalcT).rowMapper(selectByB05IdBilaVarCalcTRm).singleResult(iBilaVarCalcT);
    }

    public DbAccessStatus insertRec(IBilaVarCalcT iBilaVarCalcT) {
        return buildQuery("insertRec").bind(iBilaVarCalcT).executeInsert();
    }

    public DbAccessStatus updateRec(IBilaVarCalcT iBilaVarCalcT) {
        return buildQuery("updateRec").bind(iBilaVarCalcT).executeUpdate();
    }

    public DbAccessStatus deleteByB05IdBilaVarCalcT(int b05IdBilaVarCalcT) {
        return buildQuery("deleteByB05IdBilaVarCalcT").bind("b05IdBilaVarCalcT", b05IdBilaVarCalcT).executeDelete();
    }

    public DbAccessStatus deleteRec(int richEstrazMas, int poli, int ades) {
        return buildQuery("deleteRec").bind("richEstrazMas", richEstrazMas).bind("poli", poli).bind("ades", ades).executeDelete();
    }
}
