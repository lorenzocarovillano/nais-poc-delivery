package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import it.accenture.jnais.commons.data.to.IAdesRappAnaStatOggBus;

/**
 * Data Access Object(DAO) for tables [ADES, RAPP_ANA, STAT_OGG_BUS]
 * 
 */
public class AdesRappAnaStatOggBusDao extends BaseSqlDao<IAdesRappAnaStatOggBus> {

    public AdesRappAnaStatOggBusDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAdesRappAnaStatOggBus> getToClass() {
        return IAdesRappAnaStatOggBus.class;
    }

    public int selectRec(int ldbv1691IdPolizza, String ldbv1691TpStatBus, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, int dft) {
        return buildQuery("selectRec").bind("ldbv1691IdPolizza", ldbv1691IdPolizza).bind("ldbv1691TpStatBus", ldbv1691TpStatBus).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).scalarResultInt(dft);
    }

    public int selectRec1(IAdesRappAnaStatOggBus iAdesRappAnaStatOggBus, int dft) {
        return buildQuery("selectRec1").bind(iAdesRappAnaStatOggBus).scalarResultInt(dft);
    }
}
