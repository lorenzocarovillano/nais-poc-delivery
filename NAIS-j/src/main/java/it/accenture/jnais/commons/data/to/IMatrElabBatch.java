package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [MATR_ELAB_BATCH]
 * 
 */
public interface IMatrElabBatch extends BaseSqlTo {

    /**
     * Host Variable L71-ID-MATR-ELAB-BATCH
     * 
     */
    int getL71IdMatrElabBatch();

    void setL71IdMatrElabBatch(int l71IdMatrElabBatch);

    /**
     * Host Variable L71-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L71-TP-FRM-ASSVA
     * 
     */
    String getTpFrmAssva();

    void setTpFrmAssva(String tpFrmAssva);

    /**
     * Host Variable L71-TP-MOVI
     * 
     */
    int getTpMovi();

    void setTpMovi(int tpMovi);

    /**
     * Host Variable L71-COD-RAMO
     * 
     */
    String getCodRamo();

    void setCodRamo(String codRamo);

    /**
     * Nullable property for L71-COD-RAMO
     * 
     */
    String getCodRamoObj();

    void setCodRamoObj(String codRamoObj);

    /**
     * Host Variable L71-DT-ULT-ELAB-DB
     * 
     */
    String getDtUltElabDb();

    void setDtUltElabDb(String dtUltElabDb);

    /**
     * Host Variable L71-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L71-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L71-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable L71-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable L71-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
