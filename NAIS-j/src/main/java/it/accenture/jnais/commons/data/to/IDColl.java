package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [D_COLL]
 * 
 */
public interface IDColl extends BaseSqlTo {

    /**
     * Host Variable DCO-ID-D-COLL
     * 
     */
    int getIdDColl();

    void setIdDColl(int idDColl);

    /**
     * Host Variable DCO-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable DCO-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable DCO-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for DCO-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable DCO-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable DCO-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable DCO-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable DCO-IMP-ARROT-PRE
     * 
     */
    AfDecimal getImpArrotPre();

    void setImpArrotPre(AfDecimal impArrotPre);

    /**
     * Nullable property for DCO-IMP-ARROT-PRE
     * 
     */
    AfDecimal getImpArrotPreObj();

    void setImpArrotPreObj(AfDecimal impArrotPreObj);

    /**
     * Host Variable DCO-PC-SCON
     * 
     */
    AfDecimal getPcScon();

    void setPcScon(AfDecimal pcScon);

    /**
     * Nullable property for DCO-PC-SCON
     * 
     */
    AfDecimal getPcSconObj();

    void setPcSconObj(AfDecimal pcSconObj);

    /**
     * Host Variable DCO-FL-ADES-SING
     * 
     */
    char getFlAdesSing();

    void setFlAdesSing(char flAdesSing);

    /**
     * Nullable property for DCO-FL-ADES-SING
     * 
     */
    Character getFlAdesSingObj();

    void setFlAdesSingObj(Character flAdesSingObj);

    /**
     * Host Variable DCO-TP-IMP
     * 
     */
    String getTpImp();

    void setTpImp(String tpImp);

    /**
     * Nullable property for DCO-TP-IMP
     * 
     */
    String getTpImpObj();

    void setTpImpObj(String tpImpObj);

    /**
     * Host Variable DCO-FL-RICL-PRE-DA-CPT
     * 
     */
    char getFlRiclPreDaCpt();

    void setFlRiclPreDaCpt(char flRiclPreDaCpt);

    /**
     * Nullable property for DCO-FL-RICL-PRE-DA-CPT
     * 
     */
    Character getFlRiclPreDaCptObj();

    void setFlRiclPreDaCptObj(Character flRiclPreDaCptObj);

    /**
     * Host Variable DCO-TP-ADES
     * 
     */
    String getTpAdes();

    void setTpAdes(String tpAdes);

    /**
     * Nullable property for DCO-TP-ADES
     * 
     */
    String getTpAdesObj();

    void setTpAdesObj(String tpAdesObj);

    /**
     * Host Variable DCO-DT-ULT-RINN-TAC-DB
     * 
     */
    String getDtUltRinnTacDb();

    void setDtUltRinnTacDb(String dtUltRinnTacDb);

    /**
     * Nullable property for DCO-DT-ULT-RINN-TAC-DB
     * 
     */
    String getDtUltRinnTacDbObj();

    void setDtUltRinnTacDbObj(String dtUltRinnTacDbObj);

    /**
     * Host Variable DCO-IMP-SCON
     * 
     */
    AfDecimal getImpScon();

    void setImpScon(AfDecimal impScon);

    /**
     * Nullable property for DCO-IMP-SCON
     * 
     */
    AfDecimal getImpSconObj();

    void setImpSconObj(AfDecimal impSconObj);

    /**
     * Host Variable DCO-FRAZ-DFLT
     * 
     */
    int getFrazDflt();

    void setFrazDflt(int frazDflt);

    /**
     * Nullable property for DCO-FRAZ-DFLT
     * 
     */
    Integer getFrazDfltObj();

    void setFrazDfltObj(Integer frazDfltObj);

    /**
     * Host Variable DCO-ETA-SCAD-MASC-DFLT
     * 
     */
    int getEtaScadMascDflt();

    void setEtaScadMascDflt(int etaScadMascDflt);

    /**
     * Nullable property for DCO-ETA-SCAD-MASC-DFLT
     * 
     */
    Integer getEtaScadMascDfltObj();

    void setEtaScadMascDfltObj(Integer etaScadMascDfltObj);

    /**
     * Host Variable DCO-ETA-SCAD-FEMM-DFLT
     * 
     */
    int getEtaScadFemmDflt();

    void setEtaScadFemmDflt(int etaScadFemmDflt);

    /**
     * Nullable property for DCO-ETA-SCAD-FEMM-DFLT
     * 
     */
    Integer getEtaScadFemmDfltObj();

    void setEtaScadFemmDfltObj(Integer etaScadFemmDfltObj);

    /**
     * Host Variable DCO-TP-DFLT-DUR
     * 
     */
    String getTpDfltDur();

    void setTpDfltDur(String tpDfltDur);

    /**
     * Nullable property for DCO-TP-DFLT-DUR
     * 
     */
    String getTpDfltDurObj();

    void setTpDfltDurObj(String tpDfltDurObj);

    /**
     * Host Variable DCO-DUR-AA-ADES-DFLT
     * 
     */
    int getDurAaAdesDflt();

    void setDurAaAdesDflt(int durAaAdesDflt);

    /**
     * Nullable property for DCO-DUR-AA-ADES-DFLT
     * 
     */
    Integer getDurAaAdesDfltObj();

    void setDurAaAdesDfltObj(Integer durAaAdesDfltObj);

    /**
     * Host Variable DCO-DUR-MM-ADES-DFLT
     * 
     */
    int getDurMmAdesDflt();

    void setDurMmAdesDflt(int durMmAdesDflt);

    /**
     * Nullable property for DCO-DUR-MM-ADES-DFLT
     * 
     */
    Integer getDurMmAdesDfltObj();

    void setDurMmAdesDfltObj(Integer durMmAdesDfltObj);

    /**
     * Host Variable DCO-DUR-GG-ADES-DFLT
     * 
     */
    int getDurGgAdesDflt();

    void setDurGgAdesDflt(int durGgAdesDflt);

    /**
     * Nullable property for DCO-DUR-GG-ADES-DFLT
     * 
     */
    Integer getDurGgAdesDfltObj();

    void setDurGgAdesDfltObj(Integer durGgAdesDfltObj);

    /**
     * Host Variable DCO-DT-SCAD-ADES-DFLT-DB
     * 
     */
    String getDtScadAdesDfltDb();

    void setDtScadAdesDfltDb(String dtScadAdesDfltDb);

    /**
     * Nullable property for DCO-DT-SCAD-ADES-DFLT-DB
     * 
     */
    String getDtScadAdesDfltDbObj();

    void setDtScadAdesDfltDbObj(String dtScadAdesDfltDbObj);

    /**
     * Host Variable DCO-COD-FND-DFLT
     * 
     */
    String getCodFndDflt();

    void setCodFndDflt(String codFndDflt);

    /**
     * Nullable property for DCO-COD-FND-DFLT
     * 
     */
    String getCodFndDfltObj();

    void setCodFndDfltObj(String codFndDfltObj);

    /**
     * Host Variable DCO-TP-DUR
     * 
     */
    String getTpDur();

    void setTpDur(String tpDur);

    /**
     * Nullable property for DCO-TP-DUR
     * 
     */
    String getTpDurObj();

    void setTpDurObj(String tpDurObj);

    /**
     * Host Variable DCO-TP-CALC-DUR
     * 
     */
    String getTpCalcDur();

    void setTpCalcDur(String tpCalcDur);

    /**
     * Nullable property for DCO-TP-CALC-DUR
     * 
     */
    String getTpCalcDurObj();

    void setTpCalcDurObj(String tpCalcDurObj);

    /**
     * Host Variable DCO-FL-NO-ADERENTI
     * 
     */
    char getFlNoAderenti();

    void setFlNoAderenti(char flNoAderenti);

    /**
     * Nullable property for DCO-FL-NO-ADERENTI
     * 
     */
    Character getFlNoAderentiObj();

    void setFlNoAderentiObj(Character flNoAderentiObj);

    /**
     * Host Variable DCO-FL-DISTINTA-CNBTVA
     * 
     */
    char getFlDistintaCnbtva();

    void setFlDistintaCnbtva(char flDistintaCnbtva);

    /**
     * Nullable property for DCO-FL-DISTINTA-CNBTVA
     * 
     */
    Character getFlDistintaCnbtvaObj();

    void setFlDistintaCnbtvaObj(Character flDistintaCnbtvaObj);

    /**
     * Host Variable DCO-FL-CNBT-AUTES
     * 
     */
    char getFlCnbtAutes();

    void setFlCnbtAutes(char flCnbtAutes);

    /**
     * Nullable property for DCO-FL-CNBT-AUTES
     * 
     */
    Character getFlCnbtAutesObj();

    void setFlCnbtAutesObj(Character flCnbtAutesObj);

    /**
     * Host Variable DCO-FL-QTZ-POST-EMIS
     * 
     */
    char getFlQtzPostEmis();

    void setFlQtzPostEmis(char flQtzPostEmis);

    /**
     * Nullable property for DCO-FL-QTZ-POST-EMIS
     * 
     */
    Character getFlQtzPostEmisObj();

    void setFlQtzPostEmisObj(Character flQtzPostEmisObj);

    /**
     * Host Variable DCO-FL-COMNZ-FND-IS
     * 
     */
    char getFlComnzFndIs();

    void setFlComnzFndIs(char flComnzFndIs);

    /**
     * Nullable property for DCO-FL-COMNZ-FND-IS
     * 
     */
    Character getFlComnzFndIsObj();

    void setFlComnzFndIsObj(Character flComnzFndIsObj);

    /**
     * Host Variable DCO-DS-RIGA
     * 
     */
    long getDcoDsRiga();

    void setDcoDsRiga(long dcoDsRiga);

    /**
     * Host Variable DCO-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable DCO-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable DCO-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable DCO-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable DCO-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable DCO-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
