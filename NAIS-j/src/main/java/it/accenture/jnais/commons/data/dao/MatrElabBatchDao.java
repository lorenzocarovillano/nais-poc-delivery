package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IMatrElabBatch;

/**
 * Data Access Object(DAO) for table [MATR_ELAB_BATCH]
 * 
 */
public class MatrElabBatchDao extends BaseSqlDao<IMatrElabBatch> {

    private final IRowMapper<IMatrElabBatch> selectByL71IdMatrElabBatchRm = buildNamedRowMapper(IMatrElabBatch.class, "l71IdMatrElabBatch", "codCompAnia", "tpFrmAssva", "tpMovi", "codRamoObj", "dtUltElabDb", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public MatrElabBatchDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IMatrElabBatch> getToClass() {
        return IMatrElabBatch.class;
    }

    public IMatrElabBatch selectByL71IdMatrElabBatch(int l71IdMatrElabBatch, IMatrElabBatch iMatrElabBatch) {
        return buildQuery("selectByL71IdMatrElabBatch").bind("l71IdMatrElabBatch", l71IdMatrElabBatch).rowMapper(selectByL71IdMatrElabBatchRm).singleResult(iMatrElabBatch);
    }

    public DbAccessStatus insertRec(IMatrElabBatch iMatrElabBatch) {
        return buildQuery("insertRec").bind(iMatrElabBatch).executeInsert();
    }

    public DbAccessStatus updateRec(IMatrElabBatch iMatrElabBatch) {
        return buildQuery("updateRec").bind(iMatrElabBatch).executeUpdate();
    }

    public DbAccessStatus deleteByL71IdMatrElabBatch(int l71IdMatrElabBatch) {
        return buildQuery("deleteByL71IdMatrElabBatch").bind("l71IdMatrElabBatch", l71IdMatrElabBatch).executeDelete();
    }
}
