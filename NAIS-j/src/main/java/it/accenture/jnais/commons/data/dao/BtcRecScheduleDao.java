package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBtcRecSchedule;

/**
 * Data Access Object(DAO) for table [BTC_REC_SCHEDULE]
 * 
 */
public class BtcRecScheduleDao extends BaseSqlDao<IBtcRecSchedule> {

    private Cursor curBrs;
    private final IRowMapper<IBtcRecSchedule> fetchCurBrsRm = buildNamedRowMapper(IBtcRecSchedule.class, "idBatch", "idJob", "idRecord", "typeRecordObj", "dataRecordVchar");

    public BtcRecScheduleDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBtcRecSchedule> getToClass() {
        return IBtcRecSchedule.class;
    }

    public DbAccessStatus insertRec(IBtcRecSchedule iBtcRecSchedule) {
        return buildQuery("insertRec").bind(iBtcRecSchedule).executeInsert();
    }

    public DbAccessStatus openCurBrs(int batch, int job) {
        curBrs = buildQuery("openCurBrs").bind("batch", batch).bind("job", job).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurBrs() {
        return closeCursor(curBrs);
    }

    public IBtcRecSchedule fetchCurBrs(IBtcRecSchedule iBtcRecSchedule) {
        return fetch(curBrs, iBtcRecSchedule, fetchCurBrsRm);
    }

    public DbAccessStatus deleteRec(int batch, int job) {
        return buildQuery("deleteRec").bind("batch", batch).bind("job", job).executeDelete();
    }
}
