package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IRichDisFnd;

/**
 * Data Access Object(DAO) for table [RICH_DIS_FND]
 * 
 */
public class RichDisFndDao extends BaseSqlDao<IRichDisFnd> {

    private Cursor cIdUpdEffRdf;
    private Cursor cIdpEffRdf;
    private Cursor cIdpCpzRdf;
    private final IRowMapper<IRichDisFnd> selectByRdfDsRigaRm = buildNamedRowMapper(IRichDisFnd.class, "idRichDisFnd", "idMoviFinrio", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codFndObj", "numQuoObj", "pcObj", "impMovtoObj", "dtDisDbObj", "codTariObj", "tpStatObj", "tpModDisObj", "codDiv", "dtCambioVltDbObj", "tpFnd", "rdfDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "dtDisCalcDbObj", "flCalcDisObj", "commisGestObj", "numQuoCdgFnzObj", "numQuoCdgtotFnzObj", "cosRunAssvaIdcObj", "flSwmBp2sObj");

    public RichDisFndDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IRichDisFnd> getToClass() {
        return IRichDisFnd.class;
    }

    public IRichDisFnd selectByRdfDsRiga(long rdfDsRiga, IRichDisFnd iRichDisFnd) {
        return buildQuery("selectByRdfDsRiga").bind("rdfDsRiga", rdfDsRiga).rowMapper(selectByRdfDsRigaRm).singleResult(iRichDisFnd);
    }

    public DbAccessStatus insertRec(IRichDisFnd iRichDisFnd) {
        return buildQuery("insertRec").bind(iRichDisFnd).executeInsert();
    }

    public DbAccessStatus updateRec(IRichDisFnd iRichDisFnd) {
        return buildQuery("updateRec").bind(iRichDisFnd).executeUpdate();
    }

    public DbAccessStatus deleteByRdfDsRiga(long rdfDsRiga) {
        return buildQuery("deleteByRdfDsRiga").bind("rdfDsRiga", rdfDsRiga).executeDelete();
    }

    public IRichDisFnd selectRec(int rdfIdRichDisFnd, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRichDisFnd iRichDisFnd) {
        return buildQuery("selectRec").bind("rdfIdRichDisFnd", rdfIdRichDisFnd).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRdfDsRigaRm).singleResult(iRichDisFnd);
    }

    public DbAccessStatus updateRec1(IRichDisFnd iRichDisFnd) {
        return buildQuery("updateRec1").bind(iRichDisFnd).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffRdf(int rdfIdRichDisFnd, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffRdf = buildQuery("openCIdUpdEffRdf").bind("rdfIdRichDisFnd", rdfIdRichDisFnd).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffRdf() {
        return closeCursor(cIdUpdEffRdf);
    }

    public IRichDisFnd fetchCIdUpdEffRdf(IRichDisFnd iRichDisFnd) {
        return fetch(cIdUpdEffRdf, iRichDisFnd, selectByRdfDsRigaRm);
    }

    public IRichDisFnd selectRec1(int rdfIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRichDisFnd iRichDisFnd) {
        return buildQuery("selectRec1").bind("rdfIdMoviFinrio", rdfIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRdfDsRigaRm).singleResult(iRichDisFnd);
    }

    public DbAccessStatus openCIdpEffRdf(int rdfIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffRdf = buildQuery("openCIdpEffRdf").bind("rdfIdMoviFinrio", rdfIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffRdf() {
        return closeCursor(cIdpEffRdf);
    }

    public IRichDisFnd fetchCIdpEffRdf(IRichDisFnd iRichDisFnd) {
        return fetch(cIdpEffRdf, iRichDisFnd, selectByRdfDsRigaRm);
    }

    public IRichDisFnd selectRec2(int rdfIdRichDisFnd, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IRichDisFnd iRichDisFnd) {
        return buildQuery("selectRec2").bind("rdfIdRichDisFnd", rdfIdRichDisFnd).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByRdfDsRigaRm).singleResult(iRichDisFnd);
    }

    public IRichDisFnd selectRec3(int rdfIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IRichDisFnd iRichDisFnd) {
        return buildQuery("selectRec3").bind("rdfIdMoviFinrio", rdfIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByRdfDsRigaRm).singleResult(iRichDisFnd);
    }

    public DbAccessStatus openCIdpCpzRdf(int rdfIdMoviFinrio, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzRdf = buildQuery("openCIdpCpzRdf").bind("rdfIdMoviFinrio", rdfIdMoviFinrio).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzRdf() {
        return closeCursor(cIdpCpzRdf);
    }

    public IRichDisFnd fetchCIdpCpzRdf(IRichDisFnd iRichDisFnd) {
        return fetch(cIdpCpzRdf, iRichDisFnd, selectByRdfDsRigaRm);
    }
}
