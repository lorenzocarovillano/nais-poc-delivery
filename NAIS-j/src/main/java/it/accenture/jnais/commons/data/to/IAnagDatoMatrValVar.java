package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ANAG_DATO, MATR_VAL_VAR]
 * 
 */
public interface IAnagDatoMatrValVar extends BaseSqlTo {

    /**
     * Host Variable ADA-TIPO-DATO
     * 
     */
    String getAdaTipoDato();

    void setAdaTipoDato(String adaTipoDato);

    /**
     * Nullable property for ADA-TIPO-DATO
     * 
     */
    String getAdaTipoDatoObj();

    void setAdaTipoDatoObj(String adaTipoDatoObj);

    /**
     * Host Variable ADA-LUNGHEZZA-DATO
     * 
     */
    int getAdaLunghezzaDato();

    void setAdaLunghezzaDato(int adaLunghezzaDato);

    /**
     * Nullable property for ADA-LUNGHEZZA-DATO
     * 
     */
    Integer getAdaLunghezzaDatoObj();

    void setAdaLunghezzaDatoObj(Integer adaLunghezzaDatoObj);

    /**
     * Host Variable ADA-PRECISIONE-DATO
     * 
     */
    short getAdaPrecisioneDato();

    void setAdaPrecisioneDato(short adaPrecisioneDato);

    /**
     * Nullable property for ADA-PRECISIONE-DATO
     * 
     */
    Short getAdaPrecisioneDatoObj();

    void setAdaPrecisioneDatoObj(Short adaPrecisioneDatoObj);

    /**
     * Host Variable ADA-FORMATTAZIONE-DATO
     * 
     */
    String getAdaFormattazioneDato();

    void setAdaFormattazioneDato(String adaFormattazioneDato);

    /**
     * Nullable property for ADA-FORMATTAZIONE-DATO
     * 
     */
    String getAdaFormattazioneDatoObj();

    void setAdaFormattazioneDatoObj(String adaFormattazioneDatoObj);

    /**
     * Host Variable MVV-ID-MATR-VAL-VAR
     * 
     */
    int getMvvIdMatrValVar();

    void setMvvIdMatrValVar(int mvvIdMatrValVar);

    /**
     * Host Variable MVV-IDP-MATR-VAL-VAR
     * 
     */
    int getMvvIdpMatrValVar();

    void setMvvIdpMatrValVar(int mvvIdpMatrValVar);

    /**
     * Nullable property for MVV-IDP-MATR-VAL-VAR
     * 
     */
    Integer getMvvIdpMatrValVarObj();

    void setMvvIdpMatrValVarObj(Integer mvvIdpMatrValVarObj);

    /**
     * Host Variable MVV-TIPO-MOVIMENTO
     * 
     */
    int getMvvTipoMovimento();

    void setMvvTipoMovimento(int mvvTipoMovimento);

    /**
     * Nullable property for MVV-TIPO-MOVIMENTO
     * 
     */
    Integer getMvvTipoMovimentoObj();

    void setMvvTipoMovimentoObj(Integer mvvTipoMovimentoObj);

    /**
     * Host Variable MVV-COD-DATO-EXT
     * 
     */
    String getMvvCodDatoExt();

    void setMvvCodDatoExt(String mvvCodDatoExt);

    /**
     * Nullable property for MVV-COD-DATO-EXT
     * 
     */
    String getMvvCodDatoExtObj();

    void setMvvCodDatoExtObj(String mvvCodDatoExtObj);

    /**
     * Host Variable MVV-OBBLIGATORIETA
     * 
     */
    char getMvvObbligatorieta();

    void setMvvObbligatorieta(char mvvObbligatorieta);

    /**
     * Nullable property for MVV-OBBLIGATORIETA
     * 
     */
    Character getMvvObbligatorietaObj();

    void setMvvObbligatorietaObj(Character mvvObbligatorietaObj);

    /**
     * Host Variable MVV-VALORE-DEFAULT
     * 
     */
    String getMvvValoreDefault();

    void setMvvValoreDefault(String mvvValoreDefault);

    /**
     * Nullable property for MVV-VALORE-DEFAULT
     * 
     */
    String getMvvValoreDefaultObj();

    void setMvvValoreDefaultObj(String mvvValoreDefaultObj);

    /**
     * Host Variable MVV-COD-STR-DATO-PTF
     * 
     */
    String getMvvCodStrDatoPtf();

    void setMvvCodStrDatoPtf(String mvvCodStrDatoPtf);

    /**
     * Nullable property for MVV-COD-STR-DATO-PTF
     * 
     */
    String getMvvCodStrDatoPtfObj();

    void setMvvCodStrDatoPtfObj(String mvvCodStrDatoPtfObj);

    /**
     * Host Variable MVV-COD-DATO-PTF
     * 
     */
    String getMvvCodDatoPtf();

    void setMvvCodDatoPtf(String mvvCodDatoPtf);

    /**
     * Nullable property for MVV-COD-DATO-PTF
     * 
     */
    String getMvvCodDatoPtfObj();

    void setMvvCodDatoPtfObj(String mvvCodDatoPtfObj);

    /**
     * Host Variable MVV-COD-PARAMETRO
     * 
     */
    String getMvvCodParametro();

    void setMvvCodParametro(String mvvCodParametro);

    /**
     * Nullable property for MVV-COD-PARAMETRO
     * 
     */
    String getMvvCodParametroObj();

    void setMvvCodParametroObj(String mvvCodParametroObj);

    /**
     * Host Variable MVV-OPERAZIONE
     * 
     */
    String getMvvOperazione();

    void setMvvOperazione(String mvvOperazione);

    /**
     * Nullable property for MVV-OPERAZIONE
     * 
     */
    String getMvvOperazioneObj();

    void setMvvOperazioneObj(String mvvOperazioneObj);

    /**
     * Host Variable MVV-LIVELLO-OPERAZIONE
     * 
     */
    String getMvvLivelloOperazione();

    void setMvvLivelloOperazione(String mvvLivelloOperazione);

    /**
     * Nullable property for MVV-LIVELLO-OPERAZIONE
     * 
     */
    String getMvvLivelloOperazioneObj();

    void setMvvLivelloOperazioneObj(String mvvLivelloOperazioneObj);

    /**
     * Host Variable MVV-TIPO-OGGETTO
     * 
     */
    String getMvvTipoOggetto();

    void setMvvTipoOggetto(String mvvTipoOggetto);

    /**
     * Nullable property for MVV-TIPO-OGGETTO
     * 
     */
    String getMvvTipoOggettoObj();

    void setMvvTipoOggettoObj(String mvvTipoOggettoObj);

    /**
     * Host Variable MVV-WHERE-CONDITION-VCHAR
     * 
     */
    String getMvvWhereConditionVchar();

    void setMvvWhereConditionVchar(String mvvWhereConditionVchar);

    /**
     * Nullable property for MVV-WHERE-CONDITION-VCHAR
     * 
     */
    String getMvvWhereConditionVcharObj();

    void setMvvWhereConditionVcharObj(String mvvWhereConditionVcharObj);

    /**
     * Host Variable MVV-SERVIZIO-LETTURA
     * 
     */
    String getMvvServizioLettura();

    void setMvvServizioLettura(String mvvServizioLettura);

    /**
     * Nullable property for MVV-SERVIZIO-LETTURA
     * 
     */
    String getMvvServizioLetturaObj();

    void setMvvServizioLetturaObj(String mvvServizioLetturaObj);

    /**
     * Host Variable MVV-MODULO-CALCOLO
     * 
     */
    String getMvvModuloCalcolo();

    void setMvvModuloCalcolo(String mvvModuloCalcolo);

    /**
     * Nullable property for MVV-MODULO-CALCOLO
     * 
     */
    String getMvvModuloCalcoloObj();

    void setMvvModuloCalcoloObj(String mvvModuloCalcoloObj);

    /**
     * Host Variable MVV-COD-DATO-INTERNO
     * 
     */
    String getMvvCodDatoInterno();

    void setMvvCodDatoInterno(String mvvCodDatoInterno);

    /**
     * Nullable property for MVV-COD-DATO-INTERNO
     * 
     */
    String getMvvCodDatoInternoObj();

    void setMvvCodDatoInternoObj(String mvvCodDatoInternoObj);
};
