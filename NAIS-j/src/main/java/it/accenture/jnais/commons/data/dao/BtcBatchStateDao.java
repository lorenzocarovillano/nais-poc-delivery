package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBtcBatchState;

/**
 * Data Access Object(DAO) for table [BTC_BATCH_STATE]
 * 
 */
public class BtcBatchStateDao extends BaseSqlDao<IBtcBatchState> {

    private Cursor curBbs;
    private final IRowMapper<IBtcBatchState> fetchCurBbsRm = buildNamedRowMapper(IBtcBatchState.class, "codBatchState", "desObj", "flagToExecuteObj");

    public BtcBatchStateDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBtcBatchState> getToClass() {
        return IBtcBatchState.class;
    }

    public IBtcBatchState selectByIabv0002StateCurrent(char iabv0002StateCurrent, IBtcBatchState iBtcBatchState) {
        return buildQuery("selectByIabv0002StateCurrent").bind("iabv0002StateCurrent", String.valueOf(iabv0002StateCurrent)).singleResult(iBtcBatchState);
    }

    public DbAccessStatus openCurBbs() {
        curBbs = buildQuery("openCurBbs").withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurBbs() {
        return closeCursor(curBbs);
    }

    public IBtcBatchState fetchCurBbs(IBtcBatchState iBtcBatchState) {
        return fetch(curBbs, iBtcBatchState, fetchCurBbsRm);
    }
}
