package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [VAR_FUNZ_DI_CALC]
 * 
 */
public interface IVarFunzDiCalc extends BaseSqlTo {

    /**
     * Host Variable VFC-IDCOMP
     * 
     */
    short getIdcomp();

    void setIdcomp(short idcomp);

    /**
     * Host Variable VFC-CODPROD-VCHAR
     * 
     */
    String getCodprodVchar();

    void setCodprodVchar(String codprodVchar);

    /**
     * Host Variable VFC-CODTARI-VCHAR
     * 
     */
    String getCodtariVchar();

    void setCodtariVchar(String codtariVchar);

    /**
     * Host Variable VFC-DINIZ
     * 
     */
    int getDiniz();

    void setDiniz(int diniz);

    /**
     * Host Variable VFC-NOMEFUNZ-VCHAR
     * 
     */
    String getNomefunzVchar();

    void setNomefunzVchar(String nomefunzVchar);

    /**
     * Host Variable VFC-DEND
     * 
     */
    int getDend();

    void setDend(int dend);

    /**
     * Host Variable VFC-ISPRECALC
     * 
     */
    short getIsprecalc();

    void setIsprecalc(short isprecalc);

    /**
     * Nullable property for VFC-ISPRECALC
     * 
     */
    Short getIsprecalcObj();

    void setIsprecalcObj(Short isprecalcObj);

    /**
     * Host Variable VFC-NOMEPROGR-VCHAR
     * 
     */
    String getNomeprogrVchar();

    void setNomeprogrVchar(String nomeprogrVchar);

    /**
     * Nullable property for VFC-NOMEPROGR-VCHAR
     * 
     */
    String getNomeprogrVcharObj();

    void setNomeprogrVcharObj(String nomeprogrVcharObj);

    /**
     * Host Variable VFC-MODCALC-VCHAR
     * 
     */
    String getModcalcVchar();

    void setModcalcVchar(String modcalcVchar);

    /**
     * Nullable property for VFC-MODCALC-VCHAR
     * 
     */
    String getModcalcVcharObj();

    void setModcalcVcharObj(String modcalcVcharObj);

    /**
     * Host Variable VFC-GLOVARLIST-VCHAR
     * 
     */
    String getGlovarlistVchar();

    void setGlovarlistVchar(String glovarlistVchar);

    /**
     * Nullable property for VFC-GLOVARLIST-VCHAR
     * 
     */
    String getGlovarlistVcharObj();

    void setGlovarlistVcharObj(String glovarlistVcharObj);

    /**
     * Host Variable VFC-STEP-ELAB-VCHAR
     * 
     */
    String getStepElabVchar();

    void setStepElabVchar(String stepElabVchar);
};
