package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDomandaCollg;

/**
 * Data Access Object(DAO) for table [DOMANDA_COLLG]
 * 
 */
public class DomandaCollgDao extends BaseSqlDao<IDomandaCollg> {

    private Cursor cNst16;
    private final IRowMapper<IDomandaCollg> selectRecRm = buildNamedRowMapper(IDomandaCollg.class, "q05IdCompQuestPad", "idCompQuest", "dtIniEffDb", "codCompAnia", "dtEndEffDb", "valRispVchar", "tpGerarchia", "dfltVisualObj");

    public DomandaCollgDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDomandaCollg> getToClass() {
        return IDomandaCollg.class;
    }

    public IDomandaCollg selectRec(int q05IdCompQuestPad, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia, IDomandaCollg iDomandaCollg) {
        return buildQuery("selectRec").bind("q05IdCompQuestPad", q05IdCompQuestPad).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectRecRm).singleResult(iDomandaCollg);
    }

    public DbAccessStatus openCNst16(int q05IdCompQuestPad, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cNst16 = buildQuery("openCNst16").bind("q05IdCompQuestPad", q05IdCompQuestPad).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst16() {
        return closeCursor(cNst16);
    }

    public IDomandaCollg fetchCNst16(IDomandaCollg iDomandaCollg) {
        return fetch(cNst16, iDomandaCollg, selectRecRm);
    }
}
