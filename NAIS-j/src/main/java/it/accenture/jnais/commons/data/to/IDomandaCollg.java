package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [DOMANDA_COLLG]
 * 
 */
public interface IDomandaCollg extends BaseSqlTo {

    /**
     * Host Variable Q05-ID-COMP-QUEST-PAD
     * 
     */
    int getQ05IdCompQuestPad();

    void setQ05IdCompQuestPad(int q05IdCompQuestPad);

    /**
     * Host Variable Q05-ID-COMP-QUEST
     * 
     */
    int getIdCompQuest();

    void setIdCompQuest(int idCompQuest);

    /**
     * Host Variable Q05-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable Q05-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable Q05-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable Q05-VAL-RISP-VCHAR
     * 
     */
    String getValRispVchar();

    void setValRispVchar(String valRispVchar);

    /**
     * Host Variable Q05-TP-GERARCHIA
     * 
     */
    String getTpGerarchia();

    void setTpGerarchia(String tpGerarchia);

    /**
     * Host Variable Q05-DFLT-VISUAL
     * 
     */
    char getDfltVisual();

    void setDfltVisual(char dfltVisual);

    /**
     * Nullable property for Q05-DFLT-VISUAL
     * 
     */
    Character getDfltVisualObj();

    void setDfltVisualObj(Character dfltVisualObj);
};
