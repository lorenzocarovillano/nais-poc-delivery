package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAstAlloc;

/**
 * Data Access Object(DAO) for table [AST_ALLOC]
 * 
 */
public class AstAllocDao extends BaseSqlDao<IAstAlloc> {

    private Cursor cIdUpdEffAll;
    private Cursor cIdpEffAll;
    private Cursor cIdpCpzAll;
    private final IRowMapper<IAstAlloc> selectByAllDsRigaRm = buildNamedRowMapper(IAstAlloc.class, "idAstAlloc", "idStraDiInvst", "idMoviCrz", "idMoviChiuObj", "dtIniVldtDb", "dtEndVldtDbObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codFndObj", "codTariObj", "tpApplzAstObj", "pcRipAstObj", "tpFnd", "allDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "periodoObj", "tpRibilFndObj");

    public AstAllocDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAstAlloc> getToClass() {
        return IAstAlloc.class;
    }

    public IAstAlloc selectByAllDsRiga(long allDsRiga, IAstAlloc iAstAlloc) {
        return buildQuery("selectByAllDsRiga").bind("allDsRiga", allDsRiga).rowMapper(selectByAllDsRigaRm).singleResult(iAstAlloc);
    }

    public DbAccessStatus insertRec(IAstAlloc iAstAlloc) {
        return buildQuery("insertRec").bind(iAstAlloc).executeInsert();
    }

    public DbAccessStatus updateRec(IAstAlloc iAstAlloc) {
        return buildQuery("updateRec").bind(iAstAlloc).executeUpdate();
    }

    public DbAccessStatus deleteByAllDsRiga(long allDsRiga) {
        return buildQuery("deleteByAllDsRiga").bind("allDsRiga", allDsRiga).executeDelete();
    }

    public IAstAlloc selectRec(int allIdAstAlloc, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IAstAlloc iAstAlloc) {
        return buildQuery("selectRec").bind("allIdAstAlloc", allIdAstAlloc).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByAllDsRigaRm).singleResult(iAstAlloc);
    }

    public DbAccessStatus updateRec1(IAstAlloc iAstAlloc) {
        return buildQuery("updateRec1").bind(iAstAlloc).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffAll(int allIdAstAlloc, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffAll = buildQuery("openCIdUpdEffAll").bind("allIdAstAlloc", allIdAstAlloc).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffAll() {
        return closeCursor(cIdUpdEffAll);
    }

    public IAstAlloc fetchCIdUpdEffAll(IAstAlloc iAstAlloc) {
        return fetch(cIdUpdEffAll, iAstAlloc, selectByAllDsRigaRm);
    }

    public IAstAlloc selectRec1(int allIdStraDiInvst, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IAstAlloc iAstAlloc) {
        return buildQuery("selectRec1").bind("allIdStraDiInvst", allIdStraDiInvst).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByAllDsRigaRm).singleResult(iAstAlloc);
    }

    public DbAccessStatus openCIdpEffAll(int allIdStraDiInvst, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffAll = buildQuery("openCIdpEffAll").bind("allIdStraDiInvst", allIdStraDiInvst).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffAll() {
        return closeCursor(cIdpEffAll);
    }

    public IAstAlloc fetchCIdpEffAll(IAstAlloc iAstAlloc) {
        return fetch(cIdpEffAll, iAstAlloc, selectByAllDsRigaRm);
    }

    public IAstAlloc selectRec2(int allIdAstAlloc, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IAstAlloc iAstAlloc) {
        return buildQuery("selectRec2").bind("allIdAstAlloc", allIdAstAlloc).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByAllDsRigaRm).singleResult(iAstAlloc);
    }

    public IAstAlloc selectRec3(int allIdStraDiInvst, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IAstAlloc iAstAlloc) {
        return buildQuery("selectRec3").bind("allIdStraDiInvst", allIdStraDiInvst).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByAllDsRigaRm).singleResult(iAstAlloc);
    }

    public DbAccessStatus openCIdpCpzAll(int allIdStraDiInvst, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzAll = buildQuery("openCIdpCpzAll").bind("allIdStraDiInvst", allIdStraDiInvst).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzAll() {
        return closeCursor(cIdpCpzAll);
    }

    public IAstAlloc fetchCIdpCpzAll(IAstAlloc iAstAlloc) {
        return fetch(cIdpCpzAll, iAstAlloc, selectByAllDsRigaRm);
    }
}
