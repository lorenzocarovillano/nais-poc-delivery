package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDettRich;

/**
 * Data Access Object(DAO) for table [DETT_RICH]
 * 
 */
public class DettRichDao extends BaseSqlDao<IDettRich> {

    private Cursor cDer;
    private final IRowMapper<IDettRich> selectByDerIdRichRm = buildNamedRowMapper(IDettRich.class, "tpAreaDRichObj", "areaDRichVcharObj");

    public DettRichDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDettRich> getToClass() {
        return IDettRich.class;
    }

    public IDettRich selectByDerIdRich(int derIdRich, IDettRich iDettRich) {
        return buildQuery("selectByDerIdRich").bind("derIdRich", derIdRich).rowMapper(selectByDerIdRichRm).singleResult(iDettRich);
    }

    public DbAccessStatus openCDer(int derIdRich, int idsv0003CodiceCompagniaAnia) {
        cDer = buildQuery("openCDer").bind("derIdRich", derIdRich).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public IDettRich fetchCDer(IDettRich iDettRich) {
        return fetch(cDer, iDettRich, selectByDerIdRichRm);
    }

    public DbAccessStatus closeCDer() {
        return closeCursor(cDer);
    }
}
