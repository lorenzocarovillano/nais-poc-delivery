package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IReinvstPoliLq;

/**
 * Data Access Object(DAO) for table [REINVST_POLI_LQ]
 * 
 */
public class ReinvstPoliLqDao extends BaseSqlDao<IReinvstPoliLq> {

    private Cursor cIdUpdEffL30;
    private Cursor cIbsEffL300;
    private Cursor cIbsCpzL300;
    private final IRowMapper<IReinvstPoliLq> selectByL30DsRigaRm = buildNamedRowMapper(IReinvstPoliLq.class, "idReinvstPoliLq", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codRamo", "ibPoli", "prKeySistEstnoObj", "secKeySistEstnoObj", "codCan", "codAge", "codSubAgeObj", "impRes", "tpLiq", "tpSistEstno", "codFiscPartIva", "codMoviLiq", "tpInvstLiqObj", "impTotLiq", "l30DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "dtDecorPoliLqDbObj");

    public ReinvstPoliLqDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IReinvstPoliLq> getToClass() {
        return IReinvstPoliLq.class;
    }

    public IReinvstPoliLq selectByL30DsRiga(long l30DsRiga, IReinvstPoliLq iReinvstPoliLq) {
        return buildQuery("selectByL30DsRiga").bind("l30DsRiga", l30DsRiga).rowMapper(selectByL30DsRigaRm).singleResult(iReinvstPoliLq);
    }

    public DbAccessStatus insertRec(IReinvstPoliLq iReinvstPoliLq) {
        return buildQuery("insertRec").bind(iReinvstPoliLq).executeInsert();
    }

    public DbAccessStatus updateRec(IReinvstPoliLq iReinvstPoliLq) {
        return buildQuery("updateRec").bind(iReinvstPoliLq).executeUpdate();
    }

    public DbAccessStatus deleteByL30DsRiga(long l30DsRiga) {
        return buildQuery("deleteByL30DsRiga").bind("l30DsRiga", l30DsRiga).executeDelete();
    }

    public IReinvstPoliLq selectRec(int l30IdReinvstPoliLq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IReinvstPoliLq iReinvstPoliLq) {
        return buildQuery("selectRec").bind("l30IdReinvstPoliLq", l30IdReinvstPoliLq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByL30DsRigaRm).singleResult(iReinvstPoliLq);
    }

    public DbAccessStatus updateRec1(IReinvstPoliLq iReinvstPoliLq) {
        return buildQuery("updateRec1").bind(iReinvstPoliLq).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffL30(int l30IdReinvstPoliLq, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffL30 = buildQuery("openCIdUpdEffL30").bind("l30IdReinvstPoliLq", l30IdReinvstPoliLq).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffL30() {
        return closeCursor(cIdUpdEffL30);
    }

    public IReinvstPoliLq fetchCIdUpdEffL30(IReinvstPoliLq iReinvstPoliLq) {
        return fetch(cIdUpdEffL30, iReinvstPoliLq, selectByL30DsRigaRm);
    }

    public IReinvstPoliLq selectRec1(String l30IbPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IReinvstPoliLq iReinvstPoliLq) {
        return buildQuery("selectRec1").bind("l30IbPoli", l30IbPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByL30DsRigaRm).singleResult(iReinvstPoliLq);
    }

    public DbAccessStatus openCIbsEffL300(String l30IbPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffL300 = buildQuery("openCIbsEffL300").bind("l30IbPoli", l30IbPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsEffL300() {
        return closeCursor(cIbsEffL300);
    }

    public IReinvstPoliLq fetchCIbsEffL300(IReinvstPoliLq iReinvstPoliLq) {
        return fetch(cIbsEffL300, iReinvstPoliLq, selectByL30DsRigaRm);
    }

    public IReinvstPoliLq selectRec2(int l30IdReinvstPoliLq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IReinvstPoliLq iReinvstPoliLq) {
        return buildQuery("selectRec2").bind("l30IdReinvstPoliLq", l30IdReinvstPoliLq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByL30DsRigaRm).singleResult(iReinvstPoliLq);
    }

    public IReinvstPoliLq selectRec3(String l30IbPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IReinvstPoliLq iReinvstPoliLq) {
        return buildQuery("selectRec3").bind("l30IbPoli", l30IbPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByL30DsRigaRm).singleResult(iReinvstPoliLq);
    }

    public DbAccessStatus openCIbsCpzL300(String l30IbPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzL300 = buildQuery("openCIbsCpzL300").bind("l30IbPoli", l30IbPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsCpzL300() {
        return closeCursor(cIbsCpzL300);
    }

    public IReinvstPoliLq fetchCIbsCpzL300(IReinvstPoliLq iReinvstPoliLq) {
        return fetch(cIbsCpzL300, iReinvstPoliLq, selectByL30DsRigaRm);
    }
}
