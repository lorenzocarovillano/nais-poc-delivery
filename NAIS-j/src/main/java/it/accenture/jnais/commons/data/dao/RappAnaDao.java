package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IRappAna;

/**
 * Data Access Object(DAO) for table [RAPP_ANA]
 * 
 */
public class RappAnaDao extends BaseSqlDao<IRappAna> {

    private Cursor cIdUpdEffRan;
    private Cursor cIdoEffRan;
    private Cursor cIdoCpzRan;
    private Cursor cEff4;
    private Cursor cCpz4;
    private Cursor cEff5;
    private Cursor cCpz5;
    private Cursor cEff31;
    private Cursor cCpz31;
    private Cursor cEff37;
    private Cursor cCpz37;
    private final IRowMapper<IRappAna> selectByRanDsRigaRm = buildNamedRowMapper(IRappAna.class, "idRappAna", "idRappAnaCollgObj", "ranIdOgg", "ranTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "ranCodSoggObj", "ranTpRappAna", "tpPersObj", "sexObj", "dtNascDbObj", "flEstasObj", "indir1Obj", "indir2Obj", "indir3Obj", "tpUtlzIndir1Obj", "tpUtlzIndir2Obj", "tpUtlzIndir3Obj", "estrCntCorrAccrObj", "estrCntCorrAddObj", "estrDoctoObj", "pcNelRappObj", "tpMezPagAddObj", "tpMezPagAccrObj", "codMatrObj", "tpAdegzObj", "flTstRshObj", "codAzObj", "indPrincObj", "dtDeliberaCdaDbObj", "dlgAlRiscObj", "legaleRapprPrincObj", "tpLegaleRapprObj", "tpIndPrincObj", "tpStatRidObj", "nomeIntRidVcharObj", "cognIntRidVcharObj", "cognIntTrattVcharObj", "nomeIntTrattVcharObj", "cfIntRidObj", "flCoincDipCntrObj", "flCoincIntCntrObj", "dtDecesDbObj", "flFumatoreObj", "ranDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "flLavDipObj", "tpVarzPagatObj", "codRidObj", "tpCausRidObj", "indMassaCorpObj", "catRshProfObj");

    public RappAnaDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IRappAna> getToClass() {
        return IRappAna.class;
    }

    public IRappAna selectByRanDsRiga(long ranDsRiga, IRappAna iRappAna) {
        return buildQuery("selectByRanDsRiga").bind("ranDsRiga", ranDsRiga).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus insertRec(IRappAna iRappAna) {
        return buildQuery("insertRec").bind(iRappAna).executeInsert();
    }

    public DbAccessStatus updateRec(IRappAna iRappAna) {
        return buildQuery("updateRec").bind(iRappAna).executeUpdate();
    }

    public DbAccessStatus deleteByRanDsRiga(long ranDsRiga) {
        return buildQuery("deleteByRanDsRiga").bind("ranDsRiga", ranDsRiga).executeDelete();
    }

    public IRappAna selectRec(int ranIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRappAna iRappAna) {
        return buildQuery("selectRec").bind("ranIdRappAna", ranIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus updateRec1(IRappAna iRappAna) {
        return buildQuery("updateRec1").bind(iRappAna).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffRan(int ranIdRappAna, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffRan = buildQuery("openCIdUpdEffRan").bind("ranIdRappAna", ranIdRappAna).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffRan() {
        return closeCursor(cIdUpdEffRan);
    }

    public IRappAna fetchCIdUpdEffRan(IRappAna iRappAna) {
        return fetch(cIdUpdEffRan, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec1(int ranIdOgg, String ranTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRappAna iRappAna) {
        return buildQuery("selectRec1").bind("ranIdOgg", ranIdOgg).bind("ranTpOgg", ranTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCIdoEffRan(int ranIdOgg, String ranTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffRan = buildQuery("openCIdoEffRan").bind("ranIdOgg", ranIdOgg).bind("ranTpOgg", ranTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffRan() {
        return closeCursor(cIdoEffRan);
    }

    public IRappAna fetchCIdoEffRan(IRappAna iRappAna) {
        return fetch(cIdoEffRan, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec2(int ranIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IRappAna iRappAna) {
        return buildQuery("selectRec2").bind("ranIdRappAna", ranIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public IRappAna selectRec3(IRappAna iRappAna) {
        return buildQuery("selectRec3").bind(iRappAna).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCIdoCpzRan(IRappAna iRappAna) {
        cIdoCpzRan = buildQuery("openCIdoCpzRan").bind(iRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzRan() {
        return closeCursor(cIdoCpzRan);
    }

    public IRappAna fetchCIdoCpzRan(IRappAna iRappAna) {
        return fetch(cIdoCpzRan, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec4(IRappAna iRappAna) {
        return buildQuery("selectRec4").bind(iRappAna).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCEff4(IRappAna iRappAna) {
        cEff4 = buildQuery("openCEff4").bind(iRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff4() {
        return closeCursor(cEff4);
    }

    public IRappAna fetchCEff4(IRappAna iRappAna) {
        return fetch(cEff4, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec5(IRappAna iRappAna) {
        return buildQuery("selectRec5").bind(iRappAna).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCCpz4(IRappAna iRappAna) {
        cCpz4 = buildQuery("openCCpz4").bind(iRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz4() {
        return closeCursor(cCpz4);
    }

    public IRappAna fetchCCpz4(IRappAna iRappAna) {
        return fetch(cCpz4, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec6(IRappAna iRappAna) {
        return buildQuery("selectRec6").bind(iRappAna).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCEff5(IRappAna iRappAna) {
        cEff5 = buildQuery("openCEff5").bind(iRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff5() {
        return closeCursor(cEff5);
    }

    public IRappAna fetchCEff5(IRappAna iRappAna) {
        return fetch(cEff5, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec7(IRappAna iRappAna) {
        return buildQuery("selectRec7").bind(iRappAna).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCCpz5(IRappAna iRappAna) {
        cCpz5 = buildQuery("openCCpz5").bind(iRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz5() {
        return closeCursor(cCpz5);
    }

    public IRappAna fetchCCpz5(IRappAna iRappAna) {
        return fetch(cCpz5, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec8(IRappAna iRappAna) {
        return buildQuery("selectRec8").bind(iRappAna).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCEff31(IRappAna iRappAna) {
        cEff31 = buildQuery("openCEff31").bind(iRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff31() {
        return closeCursor(cEff31);
    }

    public IRappAna fetchCEff31(IRappAna iRappAna) {
        return fetch(cEff31, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec9(IRappAna iRappAna) {
        return buildQuery("selectRec9").bind(iRappAna).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCCpz31(IRappAna iRappAna) {
        cCpz31 = buildQuery("openCCpz31").bind(iRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz31() {
        return closeCursor(cCpz31);
    }

    public IRappAna fetchCCpz31(IRappAna iRappAna) {
        return fetch(cCpz31, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec10(IRappAna iRappAna) {
        return buildQuery("selectRec10").bind(iRappAna).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCEff37(IRappAna iRappAna) {
        cEff37 = buildQuery("openCEff37").bind(iRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff37() {
        return closeCursor(cEff37);
    }

    public IRappAna fetchCEff37(IRappAna iRappAna) {
        return fetch(cEff37, iRappAna, selectByRanDsRigaRm);
    }

    public IRappAna selectRec11(IRappAna iRappAna) {
        return buildQuery("selectRec11").bind(iRappAna).rowMapper(selectByRanDsRigaRm).singleResult(iRappAna);
    }

    public DbAccessStatus openCCpz37(IRappAna iRappAna) {
        cCpz37 = buildQuery("openCCpz37").bind(iRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz37() {
        return closeCursor(cCpz37);
    }

    public IRappAna fetchCCpz37(IRappAna iRappAna) {
        return fetch(cCpz37, iRappAna, selectByRanDsRigaRm);
    }
}
