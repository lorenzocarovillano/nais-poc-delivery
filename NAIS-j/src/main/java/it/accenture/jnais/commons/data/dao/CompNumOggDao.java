package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ICompNumOgg;

/**
 * Data Access Object(DAO) for table [COMP_NUM_OGG]
 * 
 */
public class CompNumOggDao extends BaseSqlDao<ICompNumOgg> {

    private Cursor cNst12;
    private final IRowMapper<ICompNumOgg> fetchCNst12Rm = buildNamedRowMapper(ICompNumOgg.class, "codCompagniaAnia", "formaAssicurativa", "codOggetto", "posizione", "codStrDatoObj", "codDatoObj", "valoreDefaultObj", "lunghezzaDatoObj", "flagKeyUltProgrObj");

    public CompNumOggDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ICompNumOgg> getToClass() {
        return ICompNumOgg.class;
    }

    public ICompNumOgg selectRec(int codCompagniaAnia, String formaAssicurativa, String codOggetto, int posizione, ICompNumOgg iCompNumOgg) {
        return buildQuery("selectRec").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).bind("posizione", posizione).singleResult(iCompNumOgg);
    }

    public DbAccessStatus insertRec(ICompNumOgg iCompNumOgg) {
        return buildQuery("insertRec").bind(iCompNumOgg).executeInsert();
    }

    public DbAccessStatus updateRec(ICompNumOgg iCompNumOgg) {
        return buildQuery("updateRec").bind(iCompNumOgg).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompagniaAnia, String formaAssicurativa, String codOggetto, int posizione) {
        return buildQuery("deleteRec").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).bind("posizione", posizione).executeDelete();
    }

    public DbAccessStatus openCNst12(int codCompagniaAnia, String formaAssicurativa, String codOggetto) {
        cNst12 = buildQuery("openCNst12").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst12() {
        return closeCursor(cNst12);
    }

    public ICompNumOgg fetchCNst12(ICompNumOgg iCompNumOgg) {
        return fetch(cNst12, iCompNumOgg, fetchCNst12Rm);
    }
}
