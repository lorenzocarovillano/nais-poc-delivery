package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IEstTrchDiGar;

/**
 * Data Access Object(DAO) for table [EST_TRCH_DI_GAR]
 * 
 */
public class EstTrchDiGarDao extends BaseSqlDao<IEstTrchDiGar> {

    private Cursor cIdUpdEffE12;
    private Cursor cIdpEffE12;
    private Cursor cIdpCpzE12;
    private final IRowMapper<IEstTrchDiGar> selectByE12DsRigaRm = buildNamedRowMapper(IEstTrchDiGar.class, "idEstTrchDiGar", "idTrchDiGar", "idGar", "idAdes", "idPoli", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtEmisDbObj", "cumPreAttObj", "accprePagObj", "cumPrstzObj", "e12DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "tpTrch", "causSconObj");

    public EstTrchDiGarDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IEstTrchDiGar> getToClass() {
        return IEstTrchDiGar.class;
    }

    public IEstTrchDiGar selectByE12DsRiga(long e12DsRiga, IEstTrchDiGar iEstTrchDiGar) {
        return buildQuery("selectByE12DsRiga").bind("e12DsRiga", e12DsRiga).rowMapper(selectByE12DsRigaRm).singleResult(iEstTrchDiGar);
    }

    public DbAccessStatus insertRec(IEstTrchDiGar iEstTrchDiGar) {
        return buildQuery("insertRec").bind(iEstTrchDiGar).executeInsert();
    }

    public DbAccessStatus updateRec(IEstTrchDiGar iEstTrchDiGar) {
        return buildQuery("updateRec").bind(iEstTrchDiGar).executeUpdate();
    }

    public DbAccessStatus deleteByE12DsRiga(long e12DsRiga) {
        return buildQuery("deleteByE12DsRiga").bind("e12DsRiga", e12DsRiga).executeDelete();
    }

    public IEstTrchDiGar selectRec(int e12IdEstTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstTrchDiGar iEstTrchDiGar) {
        return buildQuery("selectRec").bind("e12IdEstTrchDiGar", e12IdEstTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByE12DsRigaRm).singleResult(iEstTrchDiGar);
    }

    public DbAccessStatus updateRec1(IEstTrchDiGar iEstTrchDiGar) {
        return buildQuery("updateRec1").bind(iEstTrchDiGar).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffE12(int e12IdEstTrchDiGar, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffE12 = buildQuery("openCIdUpdEffE12").bind("e12IdEstTrchDiGar", e12IdEstTrchDiGar).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffE12() {
        return closeCursor(cIdUpdEffE12);
    }

    public IEstTrchDiGar fetchCIdUpdEffE12(IEstTrchDiGar iEstTrchDiGar) {
        return fetch(cIdUpdEffE12, iEstTrchDiGar, selectByE12DsRigaRm);
    }

    public IEstTrchDiGar selectRec1(int e12IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstTrchDiGar iEstTrchDiGar) {
        return buildQuery("selectRec1").bind("e12IdTrchDiGar", e12IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByE12DsRigaRm).singleResult(iEstTrchDiGar);
    }

    public DbAccessStatus openCIdpEffE12(int e12IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffE12 = buildQuery("openCIdpEffE12").bind("e12IdTrchDiGar", e12IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffE12() {
        return closeCursor(cIdpEffE12);
    }

    public IEstTrchDiGar fetchCIdpEffE12(IEstTrchDiGar iEstTrchDiGar) {
        return fetch(cIdpEffE12, iEstTrchDiGar, selectByE12DsRigaRm);
    }

    public IEstTrchDiGar selectRec2(int e12IdEstTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstTrchDiGar iEstTrchDiGar) {
        return buildQuery("selectRec2").bind("e12IdEstTrchDiGar", e12IdEstTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByE12DsRigaRm).singleResult(iEstTrchDiGar);
    }

    public IEstTrchDiGar selectRec3(int e12IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstTrchDiGar iEstTrchDiGar) {
        return buildQuery("selectRec3").bind("e12IdTrchDiGar", e12IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByE12DsRigaRm).singleResult(iEstTrchDiGar);
    }

    public DbAccessStatus openCIdpCpzE12(int e12IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzE12 = buildQuery("openCIdpCpzE12").bind("e12IdTrchDiGar", e12IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzE12() {
        return closeCursor(cIdpCpzE12);
    }

    public IEstTrchDiGar fetchCIdpCpzE12(IEstTrchDiGar iEstTrchDiGar) {
        return fetch(cIdpCpzE12, iEstTrchDiGar, selectByE12DsRigaRm);
    }
}
