package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BTC_EXE_MESSAGE]
 * 
 */
public interface IBtcExeMessage extends BaseSqlTo {

    /**
     * Host Variable BEM-ID-BATCH
     * 
     */
    int getIdBatch();

    void setIdBatch(int idBatch);

    /**
     * Host Variable BEM-ID-JOB
     * 
     */
    int getIdJob();

    void setIdJob(int idJob);

    /**
     * Host Variable BEM-ID-EXECUTION
     * 
     */
    int getIdExecution();

    void setIdExecution(int idExecution);

    /**
     * Host Variable BEM-ID-MESSAGE
     * 
     */
    int getIdMessage();

    void setIdMessage(int idMessage);

    /**
     * Host Variable BEM-COD-MESSAGE
     * 
     */
    int getCodMessage();

    void setCodMessage(int codMessage);

    /**
     * Host Variable BEM-MESSAGE-VCHAR
     * 
     */
    String getMessageVchar();

    void setMessageVchar(String messageVchar);
};
