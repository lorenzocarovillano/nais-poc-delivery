package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IMoviStatOggBus;

/**
 * Data Access Object(DAO) for tables [MOVI, STAT_OGG_BUS]
 * 
 */
public class MoviStatOggBusDao extends BaseSqlDao<IMoviStatOggBus> {

    private Cursor cEff12;
    private Cursor cCpz12;
    private final IRowMapper<IMoviStatOggBus> selectRecRm = buildNamedRowMapper(IMoviStatOggBus.class, "idStatOggBus", "idOgg", "tpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpStatBus", "tpCaus", "dsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public MoviStatOggBusDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IMoviStatOggBus> getToClass() {
        return IMoviStatOggBus.class;
    }

    public IMoviStatOggBus selectRec(IMoviStatOggBus iMoviStatOggBus) {
        return buildQuery("selectRec").bind(iMoviStatOggBus).rowMapper(selectRecRm).singleResult(iMoviStatOggBus);
    }

    public DbAccessStatus openCEff12(IMoviStatOggBus iMoviStatOggBus) {
        cEff12 = buildQuery("openCEff12").bind(iMoviStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff12() {
        return closeCursor(cEff12);
    }

    public IMoviStatOggBus fetchCEff12(IMoviStatOggBus iMoviStatOggBus) {
        return fetch(cEff12, iMoviStatOggBus, selectRecRm);
    }

    public IMoviStatOggBus selectRec1(IMoviStatOggBus iMoviStatOggBus) {
        return buildQuery("selectRec1").bind(iMoviStatOggBus).rowMapper(selectRecRm).singleResult(iMoviStatOggBus);
    }

    public DbAccessStatus openCCpz12(IMoviStatOggBus iMoviStatOggBus) {
        cCpz12 = buildQuery("openCCpz12").bind(iMoviStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz12() {
        return closeCursor(cCpz12);
    }

    public IMoviStatOggBus fetchCCpz12(IMoviStatOggBus iMoviStatOggBus) {
        return fetch(cCpz12, iMoviStatOggBus, selectRecRm);
    }
}
