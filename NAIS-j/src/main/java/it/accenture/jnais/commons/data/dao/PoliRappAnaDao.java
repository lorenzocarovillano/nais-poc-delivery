package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IPoliRappAna;

/**
 * Data Access Object(DAO) for tables [POLI, RAPP_ANA]
 * 
 */
public class PoliRappAnaDao extends BaseSqlDao<IPoliRappAna> {

    private Cursor cEff28;
    private Cursor cCpz28;
    private final IRowMapper<IPoliRappAna> selectRecRm = buildNamedRowMapper(IPoliRappAna.class, "idPoli", "idMoviCrz", "idMoviChiuObj", "ibOggObj", "ibProp", "dtPropDbObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtDecorDb", "dtEmisDb", "tpPoli", "durAaObj", "durMmObj", "dtScadDbObj", "codProd", "dtIniVldtProdDb", "codConvObj", "codRamoObj", "dtIniVldtConvDbObj", "dtApplzConvDbObj", "tpFrmAssva", "tpRgmFiscObj", "flEstasObj", "flRshComunObj", "flRshComunCondObj", "tpLivGenzTit", "flCopFinanzObj", "tpApplzDirObj", "speMedObj", "dirEmisObj", "dir1oVersObj", "dirVersAggObj", "codDvsObj", "flFntAzObj", "flFntAderObj", "flFntTfrObj", "flFntVoloObj", "tpOpzAScadObj", "aaDiffProrDfltObj", "flVerProdObj", "durGgObj", "dirQuietObj", "tpPtfEstnoObj", "flCumPreCntrObj", "flAmmbMoviObj", "convGecoObj", "dsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "flScudoFiscObj", "flTrasfeObj", "flTfrStrcObj", "dtPrescDbObj", "codConvAggObj", "subcatProdObj", "flQuestAdegzAssObj", "codTpaObj", "idAccCommObj", "flPoliCpiPrObj", "flPoliBundlingObj", "indPoliPrinCollObj", "flVndBundleObj", "ibBsObj", "flPoliIfpObj");

    public PoliRappAnaDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IPoliRappAna> getToClass() {
        return IPoliRappAna.class;
    }

    public IPoliRappAna selectRec(String ldbv5561CodSogg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPoliRappAna iPoliRappAna) {
        return buildQuery("selectRec").bind("ldbv5561CodSogg", ldbv5561CodSogg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectRecRm).singleResult(iPoliRappAna);
    }

    public DbAccessStatus openCEff28(String ldbv5561CodSogg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff28 = buildQuery("openCEff28").bind("ldbv5561CodSogg", ldbv5561CodSogg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff28() {
        return closeCursor(cEff28);
    }

    public IPoliRappAna fetchCEff28(IPoliRappAna iPoliRappAna) {
        return fetch(cEff28, iPoliRappAna, selectRecRm);
    }

    public IPoliRappAna selectRec1(String ldbv5561CodSogg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IPoliRappAna iPoliRappAna) {
        return buildQuery("selectRec1").bind("ldbv5561CodSogg", ldbv5561CodSogg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectRecRm).singleResult(iPoliRappAna);
    }

    public DbAccessStatus openCCpz28(String ldbv5561CodSogg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cCpz28 = buildQuery("openCCpz28").bind("ldbv5561CodSogg", ldbv5561CodSogg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz28() {
        return closeCursor(cCpz28);
    }

    public IPoliRappAna fetchCCpz28(IPoliRappAna iPoliRappAna) {
        return fetch(cCpz28, iPoliRappAna, selectRecRm);
    }
}
