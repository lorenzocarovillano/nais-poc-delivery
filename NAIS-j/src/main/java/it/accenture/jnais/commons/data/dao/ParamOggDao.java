package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IParamOgg;

/**
 * Data Access Object(DAO) for table [PARAM_OGG]
 * 
 */
public class ParamOggDao extends BaseSqlDao<IParamOgg> {

    private Cursor cIdUpdEffPog;
    private Cursor cIdoEffPog;
    private Cursor cIdoCpzPog;
    private final IRowMapper<IParamOgg> selectByPogDsRigaRm = buildNamedRowMapper(IParamOgg.class, "idParamOgg", "pogIdOgg", "pogTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codParamObj", "tpParamObj", "tpDObj", "valImpObj", "valDtDbObj", "valTsObj", "valTxtVcharObj", "valFlObj", "valNumObj", "valPcObj", "pogDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public ParamOggDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IParamOgg> getToClass() {
        return IParamOgg.class;
    }

    public IParamOgg selectByPogDsRiga(long pogDsRiga, IParamOgg iParamOgg) {
        return buildQuery("selectByPogDsRiga").bind("pogDsRiga", pogDsRiga).rowMapper(selectByPogDsRigaRm).singleResult(iParamOgg);
    }

    public DbAccessStatus insertRec(IParamOgg iParamOgg) {
        return buildQuery("insertRec").bind(iParamOgg).executeInsert();
    }

    public DbAccessStatus updateRec(IParamOgg iParamOgg) {
        return buildQuery("updateRec").bind(iParamOgg).executeUpdate();
    }

    public DbAccessStatus deleteByPogDsRiga(long pogDsRiga) {
        return buildQuery("deleteByPogDsRiga").bind("pogDsRiga", pogDsRiga).executeDelete();
    }

    public IParamOgg selectRec(int pogIdParamOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IParamOgg iParamOgg) {
        return buildQuery("selectRec").bind("pogIdParamOgg", pogIdParamOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPogDsRigaRm).singleResult(iParamOgg);
    }

    public DbAccessStatus updateRec1(IParamOgg iParamOgg) {
        return buildQuery("updateRec1").bind(iParamOgg).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffPog(int pogIdParamOgg, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffPog = buildQuery("openCIdUpdEffPog").bind("pogIdParamOgg", pogIdParamOgg).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffPog() {
        return closeCursor(cIdUpdEffPog);
    }

    public IParamOgg fetchCIdUpdEffPog(IParamOgg iParamOgg) {
        return fetch(cIdUpdEffPog, iParamOgg, selectByPogDsRigaRm);
    }

    public IParamOgg selectRec1(int pogIdOgg, String pogTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IParamOgg iParamOgg) {
        return buildQuery("selectRec1").bind("pogIdOgg", pogIdOgg).bind("pogTpOgg", pogTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPogDsRigaRm).singleResult(iParamOgg);
    }

    public DbAccessStatus openCIdoEffPog(int pogIdOgg, String pogTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffPog = buildQuery("openCIdoEffPog").bind("pogIdOgg", pogIdOgg).bind("pogTpOgg", pogTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffPog() {
        return closeCursor(cIdoEffPog);
    }

    public IParamOgg fetchCIdoEffPog(IParamOgg iParamOgg) {
        return fetch(cIdoEffPog, iParamOgg, selectByPogDsRigaRm);
    }

    public IParamOgg selectRec2(int pogIdParamOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IParamOgg iParamOgg) {
        return buildQuery("selectRec2").bind("pogIdParamOgg", pogIdParamOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPogDsRigaRm).singleResult(iParamOgg);
    }

    public IParamOgg selectRec3(IParamOgg iParamOgg) {
        return buildQuery("selectRec3").bind(iParamOgg).rowMapper(selectByPogDsRigaRm).singleResult(iParamOgg);
    }

    public DbAccessStatus openCIdoCpzPog(IParamOgg iParamOgg) {
        cIdoCpzPog = buildQuery("openCIdoCpzPog").bind(iParamOgg).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzPog() {
        return closeCursor(cIdoCpzPog);
    }

    public IParamOgg fetchCIdoCpzPog(IParamOgg iParamOgg) {
        return fetch(cIdoCpzPog, iParamOgg, selectByPogDsRigaRm);
    }

    public IParamOgg selectRec4(IParamOgg iParamOgg) {
        return buildQuery("selectRec4").bind(iParamOgg).rowMapper(selectByPogDsRigaRm).singleResult(iParamOgg);
    }
}
