package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDfltAdes;

/**
 * Data Access Object(DAO) for table [DFLT_ADES]
 * 
 */
public class DfltAdesDao extends BaseSqlDao<IDfltAdes> {

    private final IRowMapper<IDfltAdes> selectByDadIdDfltAdesRm = buildNamedRowMapper(IDfltAdes.class, "dadIdDfltAdes", "idPoli", "ibPoliObj", "ibDfltObj", "codGar1Obj", "codGar2Obj", "codGar3Obj", "codGar4Obj", "codGar5Obj", "codGar6Obj", "dtDecorDfltDbObj", "etaScadMascDfltObj", "etaScadFemmDfltObj", "durAaAdesDfltObj", "durMmAdesDfltObj", "durGgAdesDfltObj", "dtScadAdesDfltDbObj", "frazDfltObj", "pcProvIncDfltObj", "impProvIncDfltObj", "impAzObj", "impAderObj", "impTfrObj", "impVoloObj", "pcAzObj", "pcAderObj", "pcTfrObj", "pcVoloObj", "tpFntCnbtvaObj", "impPreDfltObj", "codCompAnia", "tpPreObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public DfltAdesDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDfltAdes> getToClass() {
        return IDfltAdes.class;
    }

    public IDfltAdes selectByDadIdDfltAdes(int dadIdDfltAdes, IDfltAdes iDfltAdes) {
        return buildQuery("selectByDadIdDfltAdes").bind("dadIdDfltAdes", dadIdDfltAdes).rowMapper(selectByDadIdDfltAdesRm).singleResult(iDfltAdes);
    }

    public DbAccessStatus insertRec(IDfltAdes iDfltAdes) {
        return buildQuery("insertRec").bind(iDfltAdes).executeInsert();
    }

    public DbAccessStatus updateRec(IDfltAdes iDfltAdes) {
        return buildQuery("updateRec").bind(iDfltAdes).executeUpdate();
    }

    public DbAccessStatus deleteByDadIdDfltAdes(int dadIdDfltAdes) {
        return buildQuery("deleteByDadIdDfltAdes").bind("dadIdDfltAdes", dadIdDfltAdes).executeDelete();
    }
}
