package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [EST_RAPP_ANA]
 * 
 */
public interface IEstRappAna extends BaseSqlTo {

    /**
     * Host Variable E15-ID-OGG
     * 
     */
    int getE15IdOgg();

    void setE15IdOgg(int e15IdOgg);

    /**
     * Host Variable E15-TP-OGG
     * 
     */
    String getE15TpOgg();

    void setE15TpOgg(String e15TpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable E15-ID-EST-RAPP-ANA
     * 
     */
    int getIdEstRappAna();

    void setIdEstRappAna(int idEstRappAna);

    /**
     * Host Variable E15-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Host Variable E15-ID-RAPP-ANA-COLLG
     * 
     */
    int getIdRappAnaCollg();

    void setIdRappAnaCollg(int idRappAnaCollg);

    /**
     * Nullable property for E15-ID-RAPP-ANA-COLLG
     * 
     */
    Integer getIdRappAnaCollgObj();

    void setIdRappAnaCollgObj(Integer idRappAnaCollgObj);

    /**
     * Host Variable E15-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable E15-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for E15-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable E15-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable E15-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable E15-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable E15-COD-SOGG
     * 
     */
    String getCodSogg();

    void setCodSogg(String codSogg);

    /**
     * Nullable property for E15-COD-SOGG
     * 
     */
    String getCodSoggObj();

    void setCodSoggObj(String codSoggObj);

    /**
     * Host Variable E15-TP-RAPP-ANA
     * 
     */
    String getTpRappAna();

    void setTpRappAna(String tpRappAna);

    /**
     * Host Variable E15-DS-RIGA
     * 
     */
    long getE15DsRiga();

    void setE15DsRiga(long e15DsRiga);

    /**
     * Host Variable E15-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable E15-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable E15-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable E15-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable E15-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable E15-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable E15-ID-SEGMENTAZ-CLI
     * 
     */
    int getIdSegmentazCli();

    void setIdSegmentazCli(int idSegmentazCli);

    /**
     * Nullable property for E15-ID-SEGMENTAZ-CLI
     * 
     */
    Integer getIdSegmentazCliObj();

    void setIdSegmentazCliObj(Integer idSegmentazCliObj);

    /**
     * Host Variable E15-FL-COINC-TIT-EFF
     * 
     */
    char getFlCoincTitEff();

    void setFlCoincTitEff(char flCoincTitEff);

    /**
     * Nullable property for E15-FL-COINC-TIT-EFF
     * 
     */
    Character getFlCoincTitEffObj();

    void setFlCoincTitEffObj(Character flCoincTitEffObj);

    /**
     * Host Variable E15-FL-PERS-ESP-POL
     * 
     */
    char getFlPersEspPol();

    void setFlPersEspPol(char flPersEspPol);

    /**
     * Nullable property for E15-FL-PERS-ESP-POL
     * 
     */
    Character getFlPersEspPolObj();

    void setFlPersEspPolObj(Character flPersEspPolObj);

    /**
     * Host Variable E15-DESC-PERS-ESP-POL-VCHAR
     * 
     */
    String getDescPersEspPolVchar();

    void setDescPersEspPolVchar(String descPersEspPolVchar);

    /**
     * Nullable property for E15-DESC-PERS-ESP-POL-VCHAR
     * 
     */
    String getDescPersEspPolVcharObj();

    void setDescPersEspPolVcharObj(String descPersEspPolVcharObj);

    /**
     * Host Variable E15-TP-LEG-CNTR
     * 
     */
    String getTpLegCntr();

    void setTpLegCntr(String tpLegCntr);

    /**
     * Nullable property for E15-TP-LEG-CNTR
     * 
     */
    String getTpLegCntrObj();

    void setTpLegCntrObj(String tpLegCntrObj);

    /**
     * Host Variable E15-DESC-LEG-CNTR-VCHAR
     * 
     */
    String getDescLegCntrVchar();

    void setDescLegCntrVchar(String descLegCntrVchar);

    /**
     * Nullable property for E15-DESC-LEG-CNTR-VCHAR
     * 
     */
    String getDescLegCntrVcharObj();

    void setDescLegCntrVcharObj(String descLegCntrVcharObj);

    /**
     * Host Variable E15-TP-LEG-PERC-BNFICR
     * 
     */
    String getTpLegPercBnficr();

    void setTpLegPercBnficr(String tpLegPercBnficr);

    /**
     * Nullable property for E15-TP-LEG-PERC-BNFICR
     * 
     */
    String getTpLegPercBnficrObj();

    void setTpLegPercBnficrObj(String tpLegPercBnficrObj);

    /**
     * Host Variable E15-D-LEG-PERC-BNFICR-VCHAR
     * 
     */
    String getdLegPercBnficrVchar();

    void setdLegPercBnficrVchar(String dLegPercBnficrVchar);

    /**
     * Nullable property for E15-D-LEG-PERC-BNFICR-VCHAR
     * 
     */
    String getdLegPercBnficrVcharObj();

    void setdLegPercBnficrVcharObj(String dLegPercBnficrVcharObj);

    /**
     * Host Variable E15-TP-CNT-CORR
     * 
     */
    String getTpCntCorr();

    void setTpCntCorr(String tpCntCorr);

    /**
     * Nullable property for E15-TP-CNT-CORR
     * 
     */
    String getTpCntCorrObj();

    void setTpCntCorrObj(String tpCntCorrObj);

    /**
     * Host Variable E15-TP-COINC-PIC-PAC
     * 
     */
    String getTpCoincPicPac();

    void setTpCoincPicPac(String tpCoincPicPac);

    /**
     * Nullable property for E15-TP-COINC-PIC-PAC
     * 
     */
    String getTpCoincPicPacObj();

    void setTpCoincPicPacObj(String tpCoincPicPacObj);
};
