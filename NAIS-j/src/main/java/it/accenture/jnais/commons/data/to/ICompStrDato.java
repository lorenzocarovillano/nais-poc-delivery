package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [COMP_STR_DATO]
 * 
 */
public interface ICompStrDato extends BaseSqlTo {

    /**
     * Host Variable CSD-COD-COMPAGNIA-ANIA
     * 
     */
    int getCodCompagniaAnia();

    void setCodCompagniaAnia(int codCompagniaAnia);

    /**
     * Host Variable CSD-COD-STR-DATO
     * 
     */
    String getCodStrDato();

    void setCodStrDato(String codStrDato);

    /**
     * Host Variable CSD-POSIZIONE
     * 
     */
    int getPosizione();

    void setPosizione(int posizione);

    /**
     * Host Variable CSD-COD-STR-DATO2
     * 
     */
    String getCodStrDato2();

    void setCodStrDato2(String codStrDato2);

    /**
     * Nullable property for CSD-COD-STR-DATO2
     * 
     */
    String getCodStrDato2Obj();

    void setCodStrDato2Obj(String codStrDato2Obj);

    /**
     * Host Variable CSD-COD-DATO
     * 
     */
    String getCodDato();

    void setCodDato(String codDato);

    /**
     * Nullable property for CSD-COD-DATO
     * 
     */
    String getCodDatoObj();

    void setCodDatoObj(String codDatoObj);

    /**
     * Host Variable CSD-FLAG-KEY
     * 
     */
    char getFlagKey();

    void setFlagKey(char flagKey);

    /**
     * Nullable property for CSD-FLAG-KEY
     * 
     */
    Character getFlagKeyObj();

    void setFlagKeyObj(Character flagKeyObj);

    /**
     * Host Variable CSD-FLAG-RETURN-CODE
     * 
     */
    char getFlagReturnCode();

    void setFlagReturnCode(char flagReturnCode);

    /**
     * Nullable property for CSD-FLAG-RETURN-CODE
     * 
     */
    Character getFlagReturnCodeObj();

    void setFlagReturnCodeObj(Character flagReturnCodeObj);

    /**
     * Host Variable CSD-FLAG-CALL-USING
     * 
     */
    char getFlagCallUsing();

    void setFlagCallUsing(char flagCallUsing);

    /**
     * Nullable property for CSD-FLAG-CALL-USING
     * 
     */
    Character getFlagCallUsingObj();

    void setFlagCallUsingObj(Character flagCallUsingObj);

    /**
     * Host Variable CSD-FLAG-WHERE-COND
     * 
     */
    char getFlagWhereCond();

    void setFlagWhereCond(char flagWhereCond);

    /**
     * Nullable property for CSD-FLAG-WHERE-COND
     * 
     */
    Character getFlagWhereCondObj();

    void setFlagWhereCondObj(Character flagWhereCondObj);

    /**
     * Host Variable CSD-FLAG-REDEFINES
     * 
     */
    char getFlagRedefines();

    void setFlagRedefines(char flagRedefines);

    /**
     * Nullable property for CSD-FLAG-REDEFINES
     * 
     */
    Character getFlagRedefinesObj();

    void setFlagRedefinesObj(Character flagRedefinesObj);

    /**
     * Host Variable CSD-TIPO-DATO
     * 
     */
    String getTipoDato();

    void setTipoDato(String tipoDato);

    /**
     * Nullable property for CSD-TIPO-DATO
     * 
     */
    String getTipoDatoObj();

    void setTipoDatoObj(String tipoDatoObj);

    /**
     * Host Variable CSD-LUNGHEZZA-DATO
     * 
     */
    int getLunghezzaDato();

    void setLunghezzaDato(int lunghezzaDato);

    /**
     * Nullable property for CSD-LUNGHEZZA-DATO
     * 
     */
    Integer getLunghezzaDatoObj();

    void setLunghezzaDatoObj(Integer lunghezzaDatoObj);

    /**
     * Host Variable CSD-PRECISIONE-DATO
     * 
     */
    short getPrecisioneDato();

    void setPrecisioneDato(short precisioneDato);

    /**
     * Nullable property for CSD-PRECISIONE-DATO
     * 
     */
    Short getPrecisioneDatoObj();

    void setPrecisioneDatoObj(Short precisioneDatoObj);

    /**
     * Host Variable CSD-COD-DOMINIO
     * 
     */
    String getCodDominio();

    void setCodDominio(String codDominio);

    /**
     * Nullable property for CSD-COD-DOMINIO
     * 
     */
    String getCodDominioObj();

    void setCodDominioObj(String codDominioObj);

    /**
     * Host Variable CSD-FORMATTAZIONE-DATO
     * 
     */
    String getFormattazioneDato();

    void setFormattazioneDato(String formattazioneDato);

    /**
     * Nullable property for CSD-FORMATTAZIONE-DATO
     * 
     */
    String getFormattazioneDatoObj();

    void setFormattazioneDatoObj(String formattazioneDatoObj);

    /**
     * Host Variable CSD-SERVIZIO-CONVERS
     * 
     */
    String getServizioConvers();

    void setServizioConvers(String servizioConvers);

    /**
     * Nullable property for CSD-SERVIZIO-CONVERS
     * 
     */
    String getServizioConversObj();

    void setServizioConversObj(String servizioConversObj);

    /**
     * Host Variable CSD-AREA-CONV-STANDARD
     * 
     */
    char getAreaConvStandard();

    void setAreaConvStandard(char areaConvStandard);

    /**
     * Nullable property for CSD-AREA-CONV-STANDARD
     * 
     */
    Character getAreaConvStandardObj();

    void setAreaConvStandardObj(Character areaConvStandardObj);

    /**
     * Host Variable CSD-RICORRENZA
     * 
     */
    int getRicorrenza();

    void setRicorrenza(int ricorrenza);

    /**
     * Nullable property for CSD-RICORRENZA
     * 
     */
    Integer getRicorrenzaObj();

    void setRicorrenzaObj(Integer ricorrenzaObj);

    /**
     * Host Variable CSD-OBBLIGATORIETA
     * 
     */
    char getObbligatorieta();

    void setObbligatorieta(char obbligatorieta);

    /**
     * Nullable property for CSD-OBBLIGATORIETA
     * 
     */
    Character getObbligatorietaObj();

    void setObbligatorietaObj(Character obbligatorietaObj);

    /**
     * Host Variable CSD-VALORE-DEFAULT
     * 
     */
    String getValoreDefault();

    void setValoreDefault(String valoreDefault);

    /**
     * Nullable property for CSD-VALORE-DEFAULT
     * 
     */
    String getValoreDefaultObj();

    void setValoreDefaultObj(String valoreDefaultObj);

    /**
     * Host Variable CSD-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);
};
