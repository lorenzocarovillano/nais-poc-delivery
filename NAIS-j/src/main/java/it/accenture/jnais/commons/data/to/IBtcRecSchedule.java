package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BTC_REC_SCHEDULE]
 * 
 */
public interface IBtcRecSchedule extends BaseSqlTo {

    /**
     * Host Variable BRS-ID-BATCH
     * 
     */
    int getIdBatch();

    void setIdBatch(int idBatch);

    /**
     * Host Variable BRS-ID-JOB
     * 
     */
    int getIdJob();

    void setIdJob(int idJob);

    /**
     * Host Variable BRS-ID-RECORD
     * 
     */
    int getIdRecord();

    void setIdRecord(int idRecord);

    /**
     * Host Variable BRS-TYPE-RECORD
     * 
     */
    String getTypeRecord();

    void setTypeRecord(String typeRecord);

    /**
     * Nullable property for BRS-TYPE-RECORD
     * 
     */
    String getTypeRecordObj();

    void setTypeRecordObj(String typeRecordObj);

    /**
     * Host Variable BRS-DATA-RECORD-VCHAR
     * 
     */
    String getDataRecordVchar();

    void setDataRecordVchar(String dataRecordVchar);
};
