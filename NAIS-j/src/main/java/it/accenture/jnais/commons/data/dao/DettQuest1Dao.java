package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDettQuest1;

/**
 * Data Access Object(DAO) for tables [DETT_QUEST, QUEST]
 * 
 */
public class DettQuest1Dao extends BaseSqlDao<IDettQuest1> {

    private Cursor cEff25;
    private Cursor cCpz25;
    private final IRowMapper<IDettQuest1> selectRecRm = buildNamedRowMapper(IDettQuest1.class, "idDettQuest", "idQuest", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codQuestObj", "codDomObj", "codDomCollgObj", "rispNumObj", "rispFlObj", "rispTxtVcharObj", "rispTsObj", "rispImpObj", "rispPcObj", "rispDtDbObj", "rispKeyObj", "tpRispObj", "idCompQuestObj", "valRispVcharObj", "dsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public DettQuest1Dao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDettQuest1> getToClass() {
        return IDettQuest1.class;
    }

    public IDettQuest1 selectRec(int ldbv5001IdRappAna, int ldbv5001IdCompQuest, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDettQuest1 iDettQuest1) {
        return buildQuery("selectRec").bind("ldbv5001IdRappAna", ldbv5001IdRappAna).bind("ldbv5001IdCompQuest", ldbv5001IdCompQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectRecRm).singleResult(iDettQuest1);
    }

    public DbAccessStatus openCEff25(int ldbv5001IdRappAna, int ldbv5001IdCompQuest, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff25 = buildQuery("openCEff25").bind("ldbv5001IdRappAna", ldbv5001IdRappAna).bind("ldbv5001IdCompQuest", ldbv5001IdCompQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff25() {
        return closeCursor(cEff25);
    }

    public IDettQuest1 fetchCEff25(IDettQuest1 iDettQuest1) {
        return fetch(cEff25, iDettQuest1, selectRecRm);
    }

    public IDettQuest1 selectRec1(IDettQuest1 iDettQuest1) {
        return buildQuery("selectRec1").bind(iDettQuest1).rowMapper(selectRecRm).singleResult(iDettQuest1);
    }

    public DbAccessStatus openCCpz25(IDettQuest1 iDettQuest1) {
        cCpz25 = buildQuery("openCCpz25").bind(iDettQuest1).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz25() {
        return closeCursor(cCpz25);
    }

    public IDettQuest1 fetchCCpz25(IDettQuest1 iDettQuest1) {
        return fetch(cCpz25, iDettQuest1, selectRecRm);
    }
}
