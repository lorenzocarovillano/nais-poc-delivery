package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [GRAVITA_ERRORE]
 * 
 */
public interface IGravitaErrore extends BaseSqlTo {

    /**
     * Host Variable GER-ID-GRAVITA-ERRORE
     * 
     */
    int getIdGravitaErrore();

    void setIdGravitaErrore(int idGravitaErrore);

    /**
     * Host Variable GER-LIVELLO-GRAVITA
     * 
     */
    short getLivelloGravita();

    void setLivelloGravita(short livelloGravita);

    /**
     * Host Variable GER-TIPO-TRATT-FE
     * 
     */
    char getTipoTrattFe();

    void setTipoTrattFe(char tipoTrattFe);
};
