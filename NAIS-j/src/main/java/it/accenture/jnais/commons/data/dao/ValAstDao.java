package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IValAst;

/**
 * Data Access Object(DAO) for table [VAL_AST]
 * 
 */
public class ValAstDao extends BaseSqlDao<IValAst> {

    private Cursor cEff24;
    private Cursor cCpz24;
    private Cursor cEff33;
    private Cursor cCpz33;
    private final IRowMapper<IValAst> selectRecRm = buildNamedRowMapper(IValAst.class, "idValAst", "idTrchDiGarObj", "idMoviFinrio", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codFndObj", "numQuoObj", "valQuoObj", "dtValzzDbObj", "tpValAst", "idRichInvstFndObj", "idRichDisFndObj", "dsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "preMovtoObj", "impMovtoObj", "pcInvDisObj", "numQuoLordeObj", "dtValzzCalcDbObj", "minusValenzaObj", "plusValenzaObj");

    public ValAstDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IValAst> getToClass() {
        return IValAst.class;
    }

    public IValAst selectRec(IValAst iValAst) {
        return buildQuery("selectRec").bind(iValAst).rowMapper(selectRecRm).singleResult(iValAst);
    }

    public DbAccessStatus openCEff24(IValAst iValAst) {
        cEff24 = buildQuery("openCEff24").bind(iValAst).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff24() {
        return closeCursor(cEff24);
    }

    public IValAst fetchCEff24(IValAst iValAst) {
        return fetch(cEff24, iValAst, selectRecRm);
    }

    public IValAst selectRec1(IValAst iValAst) {
        return buildQuery("selectRec1").bind(iValAst).rowMapper(selectRecRm).singleResult(iValAst);
    }

    public DbAccessStatus openCCpz24(IValAst iValAst) {
        cCpz24 = buildQuery("openCCpz24").bind(iValAst).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz24() {
        return closeCursor(cCpz24);
    }

    public IValAst fetchCCpz24(IValAst iValAst) {
        return fetch(cCpz24, iValAst, selectRecRm);
    }

    public DbAccessStatus openCEff33(IValAst iValAst) {
        cEff33 = buildQuery("openCEff33").bind(iValAst).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff33() {
        return closeCursor(cEff33);
    }

    public IValAst fetchCEff33(IValAst iValAst) {
        return fetch(cEff33, iValAst, selectRecRm);
    }

    public DbAccessStatus openCCpz33(IValAst iValAst) {
        cCpz33 = buildQuery("openCCpz33").bind(iValAst).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz33() {
        return closeCursor(cCpz33);
    }

    public IValAst fetchCCpz33(IValAst iValAst) {
        return fetch(cCpz33, iValAst, selectRecRm);
    }
}
