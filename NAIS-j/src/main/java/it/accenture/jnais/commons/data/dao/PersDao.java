package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IPers;

/**
 * Data Access Object(DAO) for table [PERS]
 * 
 */
public class PersDao extends BaseSqlDao<IPers> {

    private final IRowMapper<IPers> selectRecRm = buildNamedRowMapper(IPers.class, "idPers", "tstamIniVldt", "tstamEndVldtObj", "codPers", "riftoReteObj", "codPrtIvaObj", "indPvcyPrsnlObj", "indPvcyCmmrcObj", "indPvcyIndstObj", "dtNascCliDbObj", "dtAcqsPersDbObj", "indCliObj", "codCmnObj", "codFrmGiurdObj", "codEntePubbObj", "denRgnSocVcharObj", "denSigRgnSocVcharObj", "indEseFiscObj", "codStatCvlObj", "denNomeVcharObj", "denCognVcharObj", "codFiscObj", "indSexObj", "indCpctGiurdObj", "indPortHdcpObj", "codUserIns", "tstamInsRiga", "codUserAggmObj", "tstamAggmRigaObj", "denCmnNascStrnVcharObj", "codRamoStgrObj", "codStgrAtvtUicObj", "codRamoAtvtUicObj", "dtEndVldtPersDbObj", "dtDeadPersDbObj", "tpStatCliObj", "dtBlocCliDbObj", "codPersSecondObj", "idSegmentazCliObj", "dt1aAtvtDbObj", "dtSegnalPartnerDbObj");

    public PersDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IPers> getToClass() {
        return IPers.class;
    }

    public IPers selectRec(int idPers, long tstamIniVldt, IPers iPers) {
        return buildQuery("selectRec").bind("idPers", idPers).bind("tstamIniVldt", tstamIniVldt).rowMapper(selectRecRm).singleResult(iPers);
    }

    public DbAccessStatus insertRec(IPers iPers) {
        return buildQuery("insertRec").bind(iPers).executeInsert();
    }

    public DbAccessStatus updateRec(IPers iPers) {
        return buildQuery("updateRec").bind(iPers).executeUpdate();
    }

    public DbAccessStatus deleteRec(int idPers, long tstamIniVldt) {
        return buildQuery("deleteRec").bind("idPers", idPers).bind("tstamIniVldt", tstamIniVldt).executeDelete();
    }

    public IPers selectRec1(int idTab, long timestampFld, IPers iPers) {
        return buildQuery("selectRec1").bind("idTab", idTab).bind("timestampFld", timestampFld).rowMapper(selectRecRm).singleResult(iPers);
    }
}
