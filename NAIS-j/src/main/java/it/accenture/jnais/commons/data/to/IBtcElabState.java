package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BTC_ELAB_STATE]
 * 
 */
public interface IBtcElabState extends BaseSqlTo {

    /**
     * Host Variable BES-COD-ELAB-STATE
     * 
     */
    char getCodElabState();

    void setCodElabState(char codElabState);

    /**
     * Host Variable BES-DES
     * 
     */
    String getDes();

    void setDes(String des);

    /**
     * Host Variable BES-FLAG-TO-EXECUTE
     * 
     */
    char getFlagToExecute();

    void setFlagToExecute(char flagToExecute);

    /**
     * Nullable property for BES-FLAG-TO-EXECUTE
     * 
     */
    Character getFlagToExecuteObj();

    void setFlagToExecuteObj(Character flagToExecuteObj);

    /**
     * Host Variable IABV0002-STATE-CURRENT
     * 
     */
    char getIabv0002StateCurrent();

    void setIabv0002StateCurrent(char iabv0002StateCurrent);

    /**
     * Host Variable IABV0002-STATE-01
     * 
     */
    char getIabv0002State01();

    void setIabv0002State01(char iabv0002State01);

    /**
     * Host Variable IABV0002-STATE-02
     * 
     */
    char getIabv0002State02();

    void setIabv0002State02(char iabv0002State02);

    /**
     * Host Variable IABV0002-STATE-03
     * 
     */
    char getIabv0002State03();

    void setIabv0002State03(char iabv0002State03);

    /**
     * Host Variable IABV0002-STATE-04
     * 
     */
    char getIabv0002State04();

    void setIabv0002State04(char iabv0002State04);

    /**
     * Host Variable IABV0002-STATE-05
     * 
     */
    char getIabv0002State05();

    void setIabv0002State05(char iabv0002State05);

    /**
     * Host Variable IABV0002-STATE-06
     * 
     */
    char getIabv0002State06();

    void setIabv0002State06(char iabv0002State06);

    /**
     * Host Variable IABV0002-STATE-07
     * 
     */
    char getIabv0002State07();

    void setIabv0002State07(char iabv0002State07);

    /**
     * Host Variable IABV0002-STATE-08
     * 
     */
    char getIabv0002State08();

    void setIabv0002State08(char iabv0002State08);

    /**
     * Host Variable IABV0002-STATE-09
     * 
     */
    char getIabv0002State09();

    void setIabv0002State09(char iabv0002State09);

    /**
     * Host Variable IABV0002-STATE-10
     * 
     */
    char getIabv0002State10();

    void setIabv0002State10(char iabv0002State10);
};
