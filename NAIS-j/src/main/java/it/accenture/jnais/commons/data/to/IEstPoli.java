package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [EST_POLI]
 * 
 */
public interface IEstPoli extends BaseSqlTo {

    /**
     * Host Variable E06-ID-EST-POLI
     * 
     */
    String getIdEstPoli();

    void setIdEstPoli(String idEstPoli);

    /**
     * Host Variable E06-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable E06-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for E06-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable E06-IB-PROP
     * 
     */
    String getIbProp();

    void setIbProp(String ibProp);

    /**
     * Host Variable E06-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable E06-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for E06-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable E06-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable E06-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable E06-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable E06-DS-RIGA
     * 
     */
    long getE06DsRiga();

    void setE06DsRiga(long e06DsRiga);

    /**
     * Host Variable E06-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable E06-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable E06-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable E06-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable E06-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable E06-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable E06-DT-ULT-PERD-DB
     * 
     */
    String getDtUltPerdDb();

    void setDtUltPerdDb(String dtUltPerdDb);

    /**
     * Nullable property for E06-DT-ULT-PERD-DB
     * 
     */
    String getDtUltPerdDbObj();

    void setDtUltPerdDbObj(String dtUltPerdDbObj);

    /**
     * Host Variable E06-PC-ULT-PERD
     * 
     */
    AfDecimal getPcUltPerd();

    void setPcUltPerd(AfDecimal pcUltPerd);

    /**
     * Nullable property for E06-PC-ULT-PERD
     * 
     */
    AfDecimal getPcUltPerdObj();

    void setPcUltPerdObj(AfDecimal pcUltPerdObj);

    /**
     * Host Variable E06-FL-ESCL-SWITCH-MAX
     * 
     */
    char getFlEsclSwitchMax();

    void setFlEsclSwitchMax(char flEsclSwitchMax);

    /**
     * Nullable property for E06-FL-ESCL-SWITCH-MAX
     * 
     */
    Character getFlEsclSwitchMaxObj();

    void setFlEsclSwitchMaxObj(Character flEsclSwitchMaxObj);

    /**
     * Host Variable E06-ESI-ADEGZ-ISVAP
     * 
     */
    String getEsiAdegzIsvap();

    void setEsiAdegzIsvap(String esiAdegzIsvap);

    /**
     * Nullable property for E06-ESI-ADEGZ-ISVAP
     * 
     */
    String getEsiAdegzIsvapObj();

    void setEsiAdegzIsvapObj(String esiAdegzIsvapObj);

    /**
     * Host Variable E06-NUM-INA
     * 
     */
    String getNumIna();

    void setNumIna(String numIna);

    /**
     * Nullable property for E06-NUM-INA
     * 
     */
    String getNumInaObj();

    void setNumInaObj(String numInaObj);

    /**
     * Host Variable E06-TP-MOD-PROV
     * 
     */
    String getTpModProv();

    void setTpModProv(String tpModProv);

    /**
     * Nullable property for E06-TP-MOD-PROV
     * 
     */
    String getTpModProvObj();

    void setTpModProvObj(String tpModProvObj);

    /**
     * Host Variable E06-CPT-PROTETTO
     * 
     */
    AfDecimal getCptProtetto();

    void setCptProtetto(AfDecimal cptProtetto);

    /**
     * Nullable property for E06-CPT-PROTETTO
     * 
     */
    AfDecimal getCptProtettoObj();

    void setCptProtettoObj(AfDecimal cptProtettoObj);

    /**
     * Host Variable E06-CUM-PRE-ATT-TAKE-P
     * 
     */
    AfDecimal getCumPreAttTakeP();

    void setCumPreAttTakeP(AfDecimal cumPreAttTakeP);

    /**
     * Nullable property for E06-CUM-PRE-ATT-TAKE-P
     * 
     */
    AfDecimal getCumPreAttTakePObj();

    void setCumPreAttTakePObj(AfDecimal cumPreAttTakePObj);

    /**
     * Host Variable E06-DT-EMIS-PARTNER-DB
     * 
     */
    String getDtEmisPartnerDb();

    void setDtEmisPartnerDb(String dtEmisPartnerDb);

    /**
     * Nullable property for E06-DT-EMIS-PARTNER-DB
     * 
     */
    String getDtEmisPartnerDbObj();

    void setDtEmisPartnerDbObj(String dtEmisPartnerDbObj);
};
