package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ESTR_CNT_DIAGN_CED]
 * 
 */
public interface IEstrCntDiagnCed extends BaseSqlTo {

    /**
     * Host Variable P84-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P84-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable P84-DT-LIQ-CED-DB
     * 
     */
    String getDtLiqCedDb();

    void setDtLiqCedDb(String dtLiqCedDb);

    /**
     * Host Variable P84-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Host Variable P84-IMP-LRD-CEDOLE-LIQ
     * 
     */
    AfDecimal getImpLrdCedoleLiq();

    void setImpLrdCedoleLiq(AfDecimal impLrdCedoleLiq);

    /**
     * Nullable property for P84-IMP-LRD-CEDOLE-LIQ
     * 
     */
    AfDecimal getImpLrdCedoleLiqObj();

    void setImpLrdCedoleLiqObj(AfDecimal impLrdCedoleLiqObj);

    /**
     * Host Variable P84-IB-POLI
     * 
     */
    String getIbPoli();

    void setIbPoli(String ibPoli);

    /**
     * Host Variable P84-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P84-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P84-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable P84-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P84-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
