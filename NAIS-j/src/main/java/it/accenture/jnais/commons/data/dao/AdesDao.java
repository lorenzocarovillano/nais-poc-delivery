package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAdes;

/**
 * Data Access Object(DAO) for table [ADES]
 * 
 */
public class AdesDao extends BaseSqlDao<IAdes> {

    private Cursor cIdUpdEffAde;
    private Cursor cIdpEffAde;
    private Cursor cIboEffAde;
    private Cursor cIbsEffAde0;
    private Cursor cIbsEffAde1;
    private Cursor cIdpCpzAde;
    private Cursor cIboCpzAde;
    private Cursor cIbsCpzAde0;
    private Cursor cIbsCpzAde1;
    private final IRowMapper<IAdes> selectByAdeDsRigaRm = buildNamedRowMapper(IAdes.class, "idAdes", "idPoli", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "ibPrevObj", "ibOggObj", "codCompAnia", "dtDecorDbObj", "dtScadDbObj", "etaAScadObj", "durAaObj", "durMmObj", "durGgObj", "tpRgmFisc", "tpRiatObj", "tpModPagTit", "tpIasObj", "dtVarzTpIasDbObj", "preNetIndObj", "preLrdIndObj", "ratLrdIndObj", "prstzIniIndObj", "flCoincAsstoObj", "ibDfltObj", "modCalcObj", "tpFntCnbtvaObj", "impAzObj", "impAderObj", "impTfrObj", "impVoloObj", "pcAzObj", "pcAderObj", "pcTfrObj", "pcVoloObj", "dtNovaRgmFiscDbObj", "flAttivObj", "impRecRitVisObj", "impRecRitAccObj", "flVarzStatTbgcObj", "flProvzaMigrazObj", "impbVisDaRecObj", "dtDecorPrestBanDbObj", "dtEffVarzStatTDbObj", "adeDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "cumCnbtCapObj", "impGarCnbtObj", "dtUltConsCnbtDbObj", "idenIscFndObj", "numRatPianObj", "dtPrescDbObj", "concsPrestObj");

    public AdesDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAdes> getToClass() {
        return IAdes.class;
    }

    public IAdes selectByAdeDsRiga(long adeDsRiga, IAdes iAdes) {
        return buildQuery("selectByAdeDsRiga").bind("adeDsRiga", adeDsRiga).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public DbAccessStatus insertRec(IAdes iAdes) {
        return buildQuery("insertRec").bind(iAdes).executeInsert();
    }

    public DbAccessStatus updateRec(IAdes iAdes) {
        return buildQuery("updateRec").bind(iAdes).executeUpdate();
    }

    public DbAccessStatus deleteByAdeDsRiga(long adeDsRiga) {
        return buildQuery("deleteByAdeDsRiga").bind("adeDsRiga", adeDsRiga).executeDelete();
    }

    public IAdes selectRec(int adeIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IAdes iAdes) {
        return buildQuery("selectRec").bind("adeIdAdes", adeIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public DbAccessStatus updateRec1(IAdes iAdes) {
        return buildQuery("updateRec1").bind(iAdes).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffAde(int adeIdAdes, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffAde = buildQuery("openCIdUpdEffAde").bind("adeIdAdes", adeIdAdes).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffAde() {
        return closeCursor(cIdUpdEffAde);
    }

    public IAdes fetchCIdUpdEffAde(IAdes iAdes) {
        return fetch(cIdUpdEffAde, iAdes, selectByAdeDsRigaRm);
    }

    public IAdes selectRec1(int adeIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IAdes iAdes) {
        return buildQuery("selectRec1").bind("adeIdPoli", adeIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public DbAccessStatus openCIdpEffAde(int adeIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffAde = buildQuery("openCIdpEffAde").bind("adeIdPoli", adeIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffAde() {
        return closeCursor(cIdpEffAde);
    }

    public IAdes fetchCIdpEffAde(IAdes iAdes) {
        return fetch(cIdpEffAde, iAdes, selectByAdeDsRigaRm);
    }

    public IAdes selectRec2(String adeIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IAdes iAdes) {
        return buildQuery("selectRec2").bind("adeIbOgg", adeIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public DbAccessStatus openCIboEffAde(String adeIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIboEffAde = buildQuery("openCIboEffAde").bind("adeIbOgg", adeIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboEffAde() {
        return closeCursor(cIboEffAde);
    }

    public IAdes fetchCIboEffAde(IAdes iAdes) {
        return fetch(cIboEffAde, iAdes, selectByAdeDsRigaRm);
    }

    public IAdes selectRec3(String adeIbPrev, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IAdes iAdes) {
        return buildQuery("selectRec3").bind("adeIbPrev", adeIbPrev).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public IAdes selectRec4(String adeIbDflt, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IAdes iAdes) {
        return buildQuery("selectRec4").bind("adeIbDflt", adeIbDflt).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public DbAccessStatus openCIbsEffAde0(String adeIbPrev, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffAde0 = buildQuery("openCIbsEffAde0").bind("adeIbPrev", adeIbPrev).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus openCIbsEffAde1(String adeIbDflt, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffAde1 = buildQuery("openCIbsEffAde1").bind("adeIbDflt", adeIbDflt).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsEffAde0() {
        return closeCursor(cIbsEffAde0);
    }

    public DbAccessStatus closeCIbsEffAde1() {
        return closeCursor(cIbsEffAde1);
    }

    public IAdes fetchCIbsEffAde0(IAdes iAdes) {
        return fetch(cIbsEffAde0, iAdes, selectByAdeDsRigaRm);
    }

    public IAdes fetchCIbsEffAde1(IAdes iAdes) {
        return fetch(cIbsEffAde1, iAdes, selectByAdeDsRigaRm);
    }

    public IAdes selectRec5(int adeIdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IAdes iAdes) {
        return buildQuery("selectRec5").bind("adeIdAdes", adeIdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public IAdes selectRec6(int adeIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IAdes iAdes) {
        return buildQuery("selectRec6").bind("adeIdPoli", adeIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public DbAccessStatus openCIdpCpzAde(int adeIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzAde = buildQuery("openCIdpCpzAde").bind("adeIdPoli", adeIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzAde() {
        return closeCursor(cIdpCpzAde);
    }

    public IAdes fetchCIdpCpzAde(IAdes iAdes) {
        return fetch(cIdpCpzAde, iAdes, selectByAdeDsRigaRm);
    }

    public IAdes selectRec7(String adeIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IAdes iAdes) {
        return buildQuery("selectRec7").bind("adeIbOgg", adeIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public DbAccessStatus openCIboCpzAde(String adeIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIboCpzAde = buildQuery("openCIboCpzAde").bind("adeIbOgg", adeIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboCpzAde() {
        return closeCursor(cIboCpzAde);
    }

    public IAdes fetchCIboCpzAde(IAdes iAdes) {
        return fetch(cIboCpzAde, iAdes, selectByAdeDsRigaRm);
    }

    public IAdes selectRec8(String adeIbPrev, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IAdes iAdes) {
        return buildQuery("selectRec8").bind("adeIbPrev", adeIbPrev).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public IAdes selectRec9(String adeIbDflt, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IAdes iAdes) {
        return buildQuery("selectRec9").bind("adeIbDflt", adeIbDflt).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByAdeDsRigaRm).singleResult(iAdes);
    }

    public DbAccessStatus openCIbsCpzAde0(String adeIbPrev, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzAde0 = buildQuery("openCIbsCpzAde0").bind("adeIbPrev", adeIbPrev).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus openCIbsCpzAde1(String adeIbDflt, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzAde1 = buildQuery("openCIbsCpzAde1").bind("adeIbDflt", adeIbDflt).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsCpzAde0() {
        return closeCursor(cIbsCpzAde0);
    }

    public DbAccessStatus closeCIbsCpzAde1() {
        return closeCursor(cIbsCpzAde1);
    }

    public IAdes fetchCIbsCpzAde0(IAdes iAdes) {
        return fetch(cIbsCpzAde0, iAdes, selectByAdeDsRigaRm);
    }

    public IAdes fetchCIbsCpzAde1(IAdes iAdes) {
        return fetch(cIbsCpzAde1, iAdes, selectByAdeDsRigaRm);
    }

    public DbAccessStatus updateRec2(char adeDsOperSql, String adeDsUtente, char iabv0002StateCurrent, long adeDsRiga) {
        return buildQuery("updateRec2").bind("adeDsOperSql", String.valueOf(adeDsOperSql)).bind("adeDsUtente", adeDsUtente).bind("iabv0002StateCurrent", String.valueOf(iabv0002StateCurrent)).bind("adeDsRiga", adeDsRiga).executeUpdate();
    }

    public DbAccessStatus updateRec3(IAdes iAdes) {
        return buildQuery("updateRec3").bind(iAdes).executeUpdate();
    }
}
