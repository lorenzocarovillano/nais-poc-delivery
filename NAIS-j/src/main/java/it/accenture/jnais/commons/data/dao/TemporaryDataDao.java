package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ITemporaryData;

/**
 * Data Access Object(DAO) for table [TEMPORARY_DATA]
 * 
 */
public class TemporaryDataDao extends BaseSqlDao<ITemporaryData> {

    private Cursor curPackageSessio;
    private final IRowMapper<ITemporaryData> fetchCurPackageSessioRm = buildNamedRowMapper(ITemporaryData.class, "idTemporaryData", "aliasStrDato", "numFrame", "codCompAnia", "idSession", "flContiguousDataObj", "totalRecurrenceObj", "partialRecurrenceObj", "actualRecurrenceObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "typeRecordObj", "bufferDataVcharObj");

    public TemporaryDataDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ITemporaryData> getToClass() {
        return ITemporaryData.class;
    }

    public DbAccessStatus deleteRec(int codCompAnia, long idTemporaryData, String aliasStrDato) {
        return buildQuery("deleteRec").bind("codCompAnia", codCompAnia).bind("idTemporaryData", idTemporaryData).bind("aliasStrDato", aliasStrDato).executeDelete();
    }

    public DbAccessStatus openCurPackageSessio(int codCompAnia, long idTemporaryData, String aliasStrDato) {
        curPackageSessio = buildQuery("openCurPackageSessio").bind("codCompAnia", codCompAnia).bind("idTemporaryData", idTemporaryData).bind("aliasStrDato", aliasStrDato).open();
        return dbStatus;
    }

    public ITemporaryData fetchCurPackageSessio(ITemporaryData iTemporaryData) {
        return fetch(curPackageSessio, iTemporaryData, fetchCurPackageSessioRm);
    }

    public DbAccessStatus closeCurPackageSessio() {
        return closeCursor(curPackageSessio);
    }

    public int selectRec(int codCompAnia, long idTemporaryData, String aliasStrDato, int dft) {
        return buildQuery("selectRec").bind("codCompAnia", codCompAnia).bind("idTemporaryData", idTemporaryData).bind("aliasStrDato", aliasStrDato).scalarResultInt(dft);
    }

    public DbAccessStatus insertRec(ITemporaryData iTemporaryData) {
        return buildQuery("insertRec").bind(iTemporaryData).executeInsert();
    }
}
