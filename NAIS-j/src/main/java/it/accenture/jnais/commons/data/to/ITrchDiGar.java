package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [TRCH_DI_GAR]
 * 
 */
public interface ITrchDiGar extends BaseSqlTo {

    /**
     * Host Variable TGA-ID-ADES
     * 
     */
    int getTgaIdAdes();

    void setTgaIdAdes(int tgaIdAdes);

    /**
     * Host Variable TGA-ID-POLI
     * 
     */
    int getTgaIdPoli();

    void setTgaIdPoli(int tgaIdPoli);

    /**
     * Host Variable TGA-ID-MOVI-CRZ
     * 
     */
    int getTgaIdMoviCrz();

    void setTgaIdMoviCrz(int tgaIdMoviCrz);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable TGA-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Host Variable TGA-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Host Variable TGA-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for TGA-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable TGA-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable TGA-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable TGA-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable TGA-DT-DECOR-DB
     * 
     */
    String getDtDecorDb();

    void setDtDecorDb(String dtDecorDb);

    /**
     * Host Variable TGA-DT-SCAD-DB
     * 
     */
    String getDtScadDb();

    void setDtScadDb(String dtScadDb);

    /**
     * Nullable property for TGA-DT-SCAD-DB
     * 
     */
    String getDtScadDbObj();

    void setDtScadDbObj(String dtScadDbObj);

    /**
     * Host Variable TGA-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for TGA-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable TGA-TP-RGM-FISC
     * 
     */
    String getTpRgmFisc();

    void setTpRgmFisc(String tpRgmFisc);

    /**
     * Host Variable TGA-DT-EMIS-DB
     * 
     */
    String getDtEmisDb();

    void setDtEmisDb(String dtEmisDb);

    /**
     * Nullable property for TGA-DT-EMIS-DB
     * 
     */
    String getDtEmisDbObj();

    void setDtEmisDbObj(String dtEmisDbObj);

    /**
     * Host Variable TGA-TP-TRCH
     * 
     */
    String getTpTrch();

    void setTpTrch(String tpTrch);

    /**
     * Host Variable TGA-DUR-AA
     * 
     */
    int getDurAa();

    void setDurAa(int durAa);

    /**
     * Nullable property for TGA-DUR-AA
     * 
     */
    Integer getDurAaObj();

    void setDurAaObj(Integer durAaObj);

    /**
     * Host Variable TGA-DUR-MM
     * 
     */
    int getDurMm();

    void setDurMm(int durMm);

    /**
     * Nullable property for TGA-DUR-MM
     * 
     */
    Integer getDurMmObj();

    void setDurMmObj(Integer durMmObj);

    /**
     * Host Variable TGA-DUR-GG
     * 
     */
    int getDurGg();

    void setDurGg(int durGg);

    /**
     * Nullable property for TGA-DUR-GG
     * 
     */
    Integer getDurGgObj();

    void setDurGgObj(Integer durGgObj);

    /**
     * Host Variable TGA-PRE-CASO-MOR
     * 
     */
    AfDecimal getPreCasoMor();

    void setPreCasoMor(AfDecimal preCasoMor);

    /**
     * Nullable property for TGA-PRE-CASO-MOR
     * 
     */
    AfDecimal getPreCasoMorObj();

    void setPreCasoMorObj(AfDecimal preCasoMorObj);

    /**
     * Host Variable TGA-PC-INTR-RIAT
     * 
     */
    AfDecimal getPcIntrRiat();

    void setPcIntrRiat(AfDecimal pcIntrRiat);

    /**
     * Nullable property for TGA-PC-INTR-RIAT
     * 
     */
    AfDecimal getPcIntrRiatObj();

    void setPcIntrRiatObj(AfDecimal pcIntrRiatObj);

    /**
     * Host Variable TGA-IMP-BNS-ANTIC
     * 
     */
    AfDecimal getImpBnsAntic();

    void setImpBnsAntic(AfDecimal impBnsAntic);

    /**
     * Nullable property for TGA-IMP-BNS-ANTIC
     * 
     */
    AfDecimal getImpBnsAnticObj();

    void setImpBnsAnticObj(AfDecimal impBnsAnticObj);

    /**
     * Host Variable TGA-PRE-INI-NET
     * 
     */
    AfDecimal getPreIniNet();

    void setPreIniNet(AfDecimal preIniNet);

    /**
     * Nullable property for TGA-PRE-INI-NET
     * 
     */
    AfDecimal getPreIniNetObj();

    void setPreIniNetObj(AfDecimal preIniNetObj);

    /**
     * Host Variable TGA-PRE-PP-INI
     * 
     */
    AfDecimal getPrePpIni();

    void setPrePpIni(AfDecimal prePpIni);

    /**
     * Nullable property for TGA-PRE-PP-INI
     * 
     */
    AfDecimal getPrePpIniObj();

    void setPrePpIniObj(AfDecimal prePpIniObj);

    /**
     * Host Variable TGA-PRE-PP-ULT
     * 
     */
    AfDecimal getPrePpUlt();

    void setPrePpUlt(AfDecimal prePpUlt);

    /**
     * Nullable property for TGA-PRE-PP-ULT
     * 
     */
    AfDecimal getPrePpUltObj();

    void setPrePpUltObj(AfDecimal prePpUltObj);

    /**
     * Host Variable TGA-PRE-TARI-INI
     * 
     */
    AfDecimal getPreTariIni();

    void setPreTariIni(AfDecimal preTariIni);

    /**
     * Nullable property for TGA-PRE-TARI-INI
     * 
     */
    AfDecimal getPreTariIniObj();

    void setPreTariIniObj(AfDecimal preTariIniObj);

    /**
     * Host Variable TGA-PRE-TARI-ULT
     * 
     */
    AfDecimal getPreTariUlt();

    void setPreTariUlt(AfDecimal preTariUlt);

    /**
     * Nullable property for TGA-PRE-TARI-ULT
     * 
     */
    AfDecimal getPreTariUltObj();

    void setPreTariUltObj(AfDecimal preTariUltObj);

    /**
     * Host Variable TGA-PRE-INVRIO-INI
     * 
     */
    AfDecimal getPreInvrioIni();

    void setPreInvrioIni(AfDecimal preInvrioIni);

    /**
     * Nullable property for TGA-PRE-INVRIO-INI
     * 
     */
    AfDecimal getPreInvrioIniObj();

    void setPreInvrioIniObj(AfDecimal preInvrioIniObj);

    /**
     * Host Variable TGA-PRE-INVRIO-ULT
     * 
     */
    AfDecimal getPreInvrioUlt();

    void setPreInvrioUlt(AfDecimal preInvrioUlt);

    /**
     * Nullable property for TGA-PRE-INVRIO-ULT
     * 
     */
    AfDecimal getPreInvrioUltObj();

    void setPreInvrioUltObj(AfDecimal preInvrioUltObj);

    /**
     * Host Variable TGA-PRE-RIVTO
     * 
     */
    AfDecimal getPreRivto();

    void setPreRivto(AfDecimal preRivto);

    /**
     * Nullable property for TGA-PRE-RIVTO
     * 
     */
    AfDecimal getPreRivtoObj();

    void setPreRivtoObj(AfDecimal preRivtoObj);

    /**
     * Host Variable TGA-IMP-SOPR-PROF
     * 
     */
    AfDecimal getImpSoprProf();

    void setImpSoprProf(AfDecimal impSoprProf);

    /**
     * Nullable property for TGA-IMP-SOPR-PROF
     * 
     */
    AfDecimal getImpSoprProfObj();

    void setImpSoprProfObj(AfDecimal impSoprProfObj);

    /**
     * Host Variable TGA-IMP-SOPR-SAN
     * 
     */
    AfDecimal getImpSoprSan();

    void setImpSoprSan(AfDecimal impSoprSan);

    /**
     * Nullable property for TGA-IMP-SOPR-SAN
     * 
     */
    AfDecimal getImpSoprSanObj();

    void setImpSoprSanObj(AfDecimal impSoprSanObj);

    /**
     * Host Variable TGA-IMP-SOPR-SPO
     * 
     */
    AfDecimal getImpSoprSpo();

    void setImpSoprSpo(AfDecimal impSoprSpo);

    /**
     * Nullable property for TGA-IMP-SOPR-SPO
     * 
     */
    AfDecimal getImpSoprSpoObj();

    void setImpSoprSpoObj(AfDecimal impSoprSpoObj);

    /**
     * Host Variable TGA-IMP-SOPR-TEC
     * 
     */
    AfDecimal getImpSoprTec();

    void setImpSoprTec(AfDecimal impSoprTec);

    /**
     * Nullable property for TGA-IMP-SOPR-TEC
     * 
     */
    AfDecimal getImpSoprTecObj();

    void setImpSoprTecObj(AfDecimal impSoprTecObj);

    /**
     * Host Variable TGA-IMP-ALT-SOPR
     * 
     */
    AfDecimal getImpAltSopr();

    void setImpAltSopr(AfDecimal impAltSopr);

    /**
     * Nullable property for TGA-IMP-ALT-SOPR
     * 
     */
    AfDecimal getImpAltSoprObj();

    void setImpAltSoprObj(AfDecimal impAltSoprObj);

    /**
     * Host Variable TGA-PRE-STAB
     * 
     */
    AfDecimal getPreStab();

    void setPreStab(AfDecimal preStab);

    /**
     * Nullable property for TGA-PRE-STAB
     * 
     */
    AfDecimal getPreStabObj();

    void setPreStabObj(AfDecimal preStabObj);

    /**
     * Host Variable TGA-DT-EFF-STAB-DB
     * 
     */
    String getDtEffStabDb();

    void setDtEffStabDb(String dtEffStabDb);

    /**
     * Nullable property for TGA-DT-EFF-STAB-DB
     * 
     */
    String getDtEffStabDbObj();

    void setDtEffStabDbObj(String dtEffStabDbObj);

    /**
     * Host Variable TGA-TS-RIVAL-FIS
     * 
     */
    AfDecimal getTsRivalFis();

    void setTsRivalFis(AfDecimal tsRivalFis);

    /**
     * Nullable property for TGA-TS-RIVAL-FIS
     * 
     */
    AfDecimal getTsRivalFisObj();

    void setTsRivalFisObj(AfDecimal tsRivalFisObj);

    /**
     * Host Variable TGA-TS-RIVAL-INDICIZ
     * 
     */
    AfDecimal getTsRivalIndiciz();

    void setTsRivalIndiciz(AfDecimal tsRivalIndiciz);

    /**
     * Nullable property for TGA-TS-RIVAL-INDICIZ
     * 
     */
    AfDecimal getTsRivalIndicizObj();

    void setTsRivalIndicizObj(AfDecimal tsRivalIndicizObj);

    /**
     * Host Variable TGA-OLD-TS-TEC
     * 
     */
    AfDecimal getOldTsTec();

    void setOldTsTec(AfDecimal oldTsTec);

    /**
     * Nullable property for TGA-OLD-TS-TEC
     * 
     */
    AfDecimal getOldTsTecObj();

    void setOldTsTecObj(AfDecimal oldTsTecObj);

    /**
     * Host Variable TGA-RAT-LRD
     * 
     */
    AfDecimal getRatLrd();

    void setRatLrd(AfDecimal ratLrd);

    /**
     * Nullable property for TGA-RAT-LRD
     * 
     */
    AfDecimal getRatLrdObj();

    void setRatLrdObj(AfDecimal ratLrdObj);

    /**
     * Host Variable TGA-PRE-LRD
     * 
     */
    AfDecimal getPreLrd();

    void setPreLrd(AfDecimal preLrd);

    /**
     * Nullable property for TGA-PRE-LRD
     * 
     */
    AfDecimal getPreLrdObj();

    void setPreLrdObj(AfDecimal preLrdObj);

    /**
     * Host Variable TGA-PRSTZ-INI
     * 
     */
    AfDecimal getPrstzIni();

    void setPrstzIni(AfDecimal prstzIni);

    /**
     * Nullable property for TGA-PRSTZ-INI
     * 
     */
    AfDecimal getPrstzIniObj();

    void setPrstzIniObj(AfDecimal prstzIniObj);

    /**
     * Host Variable TGA-PRSTZ-ULT
     * 
     */
    AfDecimal getPrstzUlt();

    void setPrstzUlt(AfDecimal prstzUlt);

    /**
     * Nullable property for TGA-PRSTZ-ULT
     * 
     */
    AfDecimal getPrstzUltObj();

    void setPrstzUltObj(AfDecimal prstzUltObj);

    /**
     * Host Variable TGA-CPT-IN-OPZ-RIVTO
     * 
     */
    AfDecimal getCptInOpzRivto();

    void setCptInOpzRivto(AfDecimal cptInOpzRivto);

    /**
     * Nullable property for TGA-CPT-IN-OPZ-RIVTO
     * 
     */
    AfDecimal getCptInOpzRivtoObj();

    void setCptInOpzRivtoObj(AfDecimal cptInOpzRivtoObj);

    /**
     * Host Variable TGA-PRSTZ-INI-STAB
     * 
     */
    AfDecimal getPrstzIniStab();

    void setPrstzIniStab(AfDecimal prstzIniStab);

    /**
     * Nullable property for TGA-PRSTZ-INI-STAB
     * 
     */
    AfDecimal getPrstzIniStabObj();

    void setPrstzIniStabObj(AfDecimal prstzIniStabObj);

    /**
     * Host Variable TGA-CPT-RSH-MOR
     * 
     */
    AfDecimal getCptRshMor();

    void setCptRshMor(AfDecimal cptRshMor);

    /**
     * Nullable property for TGA-CPT-RSH-MOR
     * 
     */
    AfDecimal getCptRshMorObj();

    void setCptRshMorObj(AfDecimal cptRshMorObj);

    /**
     * Host Variable TGA-PRSTZ-RID-INI
     * 
     */
    AfDecimal getPrstzRidIni();

    void setPrstzRidIni(AfDecimal prstzRidIni);

    /**
     * Nullable property for TGA-PRSTZ-RID-INI
     * 
     */
    AfDecimal getPrstzRidIniObj();

    void setPrstzRidIniObj(AfDecimal prstzRidIniObj);

    /**
     * Host Variable TGA-FL-CAR-CONT
     * 
     */
    char getFlCarCont();

    void setFlCarCont(char flCarCont);

    /**
     * Nullable property for TGA-FL-CAR-CONT
     * 
     */
    Character getFlCarContObj();

    void setFlCarContObj(Character flCarContObj);

    /**
     * Host Variable TGA-BNS-GIA-LIQTO
     * 
     */
    AfDecimal getBnsGiaLiqto();

    void setBnsGiaLiqto(AfDecimal bnsGiaLiqto);

    /**
     * Nullable property for TGA-BNS-GIA-LIQTO
     * 
     */
    AfDecimal getBnsGiaLiqtoObj();

    void setBnsGiaLiqtoObj(AfDecimal bnsGiaLiqtoObj);

    /**
     * Host Variable TGA-IMP-BNS
     * 
     */
    AfDecimal getImpBns();

    void setImpBns(AfDecimal impBns);

    /**
     * Nullable property for TGA-IMP-BNS
     * 
     */
    AfDecimal getImpBnsObj();

    void setImpBnsObj(AfDecimal impBnsObj);

    /**
     * Host Variable TGA-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Host Variable TGA-PRSTZ-INI-NEWFIS
     * 
     */
    AfDecimal getPrstzIniNewfis();

    void setPrstzIniNewfis(AfDecimal prstzIniNewfis);

    /**
     * Nullable property for TGA-PRSTZ-INI-NEWFIS
     * 
     */
    AfDecimal getPrstzIniNewfisObj();

    void setPrstzIniNewfisObj(AfDecimal prstzIniNewfisObj);

    /**
     * Host Variable TGA-IMP-SCON
     * 
     */
    AfDecimal getImpScon();

    void setImpScon(AfDecimal impScon);

    /**
     * Nullable property for TGA-IMP-SCON
     * 
     */
    AfDecimal getImpSconObj();

    void setImpSconObj(AfDecimal impSconObj);

    /**
     * Host Variable TGA-ALQ-SCON
     * 
     */
    AfDecimal getAlqScon();

    void setAlqScon(AfDecimal alqScon);

    /**
     * Nullable property for TGA-ALQ-SCON
     * 
     */
    AfDecimal getAlqSconObj();

    void setAlqSconObj(AfDecimal alqSconObj);

    /**
     * Host Variable TGA-IMP-CAR-ACQ
     * 
     */
    AfDecimal getImpCarAcq();

    void setImpCarAcq(AfDecimal impCarAcq);

    /**
     * Nullable property for TGA-IMP-CAR-ACQ
     * 
     */
    AfDecimal getImpCarAcqObj();

    void setImpCarAcqObj(AfDecimal impCarAcqObj);

    /**
     * Host Variable TGA-IMP-CAR-INC
     * 
     */
    AfDecimal getImpCarInc();

    void setImpCarInc(AfDecimal impCarInc);

    /**
     * Nullable property for TGA-IMP-CAR-INC
     * 
     */
    AfDecimal getImpCarIncObj();

    void setImpCarIncObj(AfDecimal impCarIncObj);

    /**
     * Host Variable TGA-IMP-CAR-GEST
     * 
     */
    AfDecimal getImpCarGest();

    void setImpCarGest(AfDecimal impCarGest);

    /**
     * Nullable property for TGA-IMP-CAR-GEST
     * 
     */
    AfDecimal getImpCarGestObj();

    void setImpCarGestObj(AfDecimal impCarGestObj);

    /**
     * Host Variable TGA-ETA-AA-1O-ASSTO
     * 
     */
    short getEtaAa1oAssto();

    void setEtaAa1oAssto(short etaAa1oAssto);

    /**
     * Nullable property for TGA-ETA-AA-1O-ASSTO
     * 
     */
    Short getEtaAa1oAsstoObj();

    void setEtaAa1oAsstoObj(Short etaAa1oAsstoObj);

    /**
     * Host Variable TGA-ETA-MM-1O-ASSTO
     * 
     */
    short getEtaMm1oAssto();

    void setEtaMm1oAssto(short etaMm1oAssto);

    /**
     * Nullable property for TGA-ETA-MM-1O-ASSTO
     * 
     */
    Short getEtaMm1oAsstoObj();

    void setEtaMm1oAsstoObj(Short etaMm1oAsstoObj);

    /**
     * Host Variable TGA-ETA-AA-2O-ASSTO
     * 
     */
    short getEtaAa2oAssto();

    void setEtaAa2oAssto(short etaAa2oAssto);

    /**
     * Nullable property for TGA-ETA-AA-2O-ASSTO
     * 
     */
    Short getEtaAa2oAsstoObj();

    void setEtaAa2oAsstoObj(Short etaAa2oAsstoObj);

    /**
     * Host Variable TGA-ETA-MM-2O-ASSTO
     * 
     */
    short getEtaMm2oAssto();

    void setEtaMm2oAssto(short etaMm2oAssto);

    /**
     * Nullable property for TGA-ETA-MM-2O-ASSTO
     * 
     */
    Short getEtaMm2oAsstoObj();

    void setEtaMm2oAsstoObj(Short etaMm2oAsstoObj);

    /**
     * Host Variable TGA-ETA-AA-3O-ASSTO
     * 
     */
    short getEtaAa3oAssto();

    void setEtaAa3oAssto(short etaAa3oAssto);

    /**
     * Nullable property for TGA-ETA-AA-3O-ASSTO
     * 
     */
    Short getEtaAa3oAsstoObj();

    void setEtaAa3oAsstoObj(Short etaAa3oAsstoObj);

    /**
     * Host Variable TGA-ETA-MM-3O-ASSTO
     * 
     */
    short getEtaMm3oAssto();

    void setEtaMm3oAssto(short etaMm3oAssto);

    /**
     * Nullable property for TGA-ETA-MM-3O-ASSTO
     * 
     */
    Short getEtaMm3oAsstoObj();

    void setEtaMm3oAsstoObj(Short etaMm3oAsstoObj);

    /**
     * Host Variable TGA-RENDTO-LRD
     * 
     */
    AfDecimal getRendtoLrd();

    void setRendtoLrd(AfDecimal rendtoLrd);

    /**
     * Nullable property for TGA-RENDTO-LRD
     * 
     */
    AfDecimal getRendtoLrdObj();

    void setRendtoLrdObj(AfDecimal rendtoLrdObj);

    /**
     * Host Variable TGA-PC-RETR
     * 
     */
    AfDecimal getPcRetr();

    void setPcRetr(AfDecimal pcRetr);

    /**
     * Nullable property for TGA-PC-RETR
     * 
     */
    AfDecimal getPcRetrObj();

    void setPcRetrObj(AfDecimal pcRetrObj);

    /**
     * Host Variable TGA-RENDTO-RETR
     * 
     */
    AfDecimal getRendtoRetr();

    void setRendtoRetr(AfDecimal rendtoRetr);

    /**
     * Nullable property for TGA-RENDTO-RETR
     * 
     */
    AfDecimal getRendtoRetrObj();

    void setRendtoRetrObj(AfDecimal rendtoRetrObj);

    /**
     * Host Variable TGA-MIN-GARTO
     * 
     */
    AfDecimal getMinGarto();

    void setMinGarto(AfDecimal minGarto);

    /**
     * Nullable property for TGA-MIN-GARTO
     * 
     */
    AfDecimal getMinGartoObj();

    void setMinGartoObj(AfDecimal minGartoObj);

    /**
     * Host Variable TGA-MIN-TRNUT
     * 
     */
    AfDecimal getMinTrnut();

    void setMinTrnut(AfDecimal minTrnut);

    /**
     * Nullable property for TGA-MIN-TRNUT
     * 
     */
    AfDecimal getMinTrnutObj();

    void setMinTrnutObj(AfDecimal minTrnutObj);

    /**
     * Host Variable TGA-PRE-ATT-DI-TRCH
     * 
     */
    AfDecimal getPreAttDiTrch();

    void setPreAttDiTrch(AfDecimal preAttDiTrch);

    /**
     * Nullable property for TGA-PRE-ATT-DI-TRCH
     * 
     */
    AfDecimal getPreAttDiTrchObj();

    void setPreAttDiTrchObj(AfDecimal preAttDiTrchObj);

    /**
     * Host Variable TGA-MATU-END2000
     * 
     */
    AfDecimal getMatuEnd2000();

    void setMatuEnd2000(AfDecimal matuEnd2000);

    /**
     * Nullable property for TGA-MATU-END2000
     * 
     */
    AfDecimal getMatuEnd2000Obj();

    void setMatuEnd2000Obj(AfDecimal matuEnd2000Obj);

    /**
     * Host Variable TGA-ABB-TOT-INI
     * 
     */
    AfDecimal getAbbTotIni();

    void setAbbTotIni(AfDecimal abbTotIni);

    /**
     * Nullable property for TGA-ABB-TOT-INI
     * 
     */
    AfDecimal getAbbTotIniObj();

    void setAbbTotIniObj(AfDecimal abbTotIniObj);

    /**
     * Host Variable TGA-ABB-TOT-ULT
     * 
     */
    AfDecimal getAbbTotUlt();

    void setAbbTotUlt(AfDecimal abbTotUlt);

    /**
     * Nullable property for TGA-ABB-TOT-ULT
     * 
     */
    AfDecimal getAbbTotUltObj();

    void setAbbTotUltObj(AfDecimal abbTotUltObj);

    /**
     * Host Variable TGA-ABB-ANNU-ULT
     * 
     */
    AfDecimal getAbbAnnuUlt();

    void setAbbAnnuUlt(AfDecimal abbAnnuUlt);

    /**
     * Nullable property for TGA-ABB-ANNU-ULT
     * 
     */
    AfDecimal getAbbAnnuUltObj();

    void setAbbAnnuUltObj(AfDecimal abbAnnuUltObj);

    /**
     * Host Variable TGA-DUR-ABB
     * 
     */
    int getDurAbb();

    void setDurAbb(int durAbb);

    /**
     * Nullable property for TGA-DUR-ABB
     * 
     */
    Integer getDurAbbObj();

    void setDurAbbObj(Integer durAbbObj);

    /**
     * Host Variable TGA-TP-ADEG-ABB
     * 
     */
    char getTpAdegAbb();

    void setTpAdegAbb(char tpAdegAbb);

    /**
     * Nullable property for TGA-TP-ADEG-ABB
     * 
     */
    Character getTpAdegAbbObj();

    void setTpAdegAbbObj(Character tpAdegAbbObj);

    /**
     * Host Variable TGA-MOD-CALC
     * 
     */
    String getModCalc();

    void setModCalc(String modCalc);

    /**
     * Nullable property for TGA-MOD-CALC
     * 
     */
    String getModCalcObj();

    void setModCalcObj(String modCalcObj);

    /**
     * Host Variable TGA-IMP-AZ
     * 
     */
    AfDecimal getImpAz();

    void setImpAz(AfDecimal impAz);

    /**
     * Nullable property for TGA-IMP-AZ
     * 
     */
    AfDecimal getImpAzObj();

    void setImpAzObj(AfDecimal impAzObj);

    /**
     * Host Variable TGA-IMP-ADER
     * 
     */
    AfDecimal getImpAder();

    void setImpAder(AfDecimal impAder);

    /**
     * Nullable property for TGA-IMP-ADER
     * 
     */
    AfDecimal getImpAderObj();

    void setImpAderObj(AfDecimal impAderObj);

    /**
     * Host Variable TGA-IMP-TFR
     * 
     */
    AfDecimal getImpTfr();

    void setImpTfr(AfDecimal impTfr);

    /**
     * Nullable property for TGA-IMP-TFR
     * 
     */
    AfDecimal getImpTfrObj();

    void setImpTfrObj(AfDecimal impTfrObj);

    /**
     * Host Variable TGA-IMP-VOLO
     * 
     */
    AfDecimal getImpVolo();

    void setImpVolo(AfDecimal impVolo);

    /**
     * Nullable property for TGA-IMP-VOLO
     * 
     */
    AfDecimal getImpVoloObj();

    void setImpVoloObj(AfDecimal impVoloObj);

    /**
     * Host Variable TGA-VIS-END2000
     * 
     */
    AfDecimal getVisEnd2000();

    void setVisEnd2000(AfDecimal visEnd2000);

    /**
     * Nullable property for TGA-VIS-END2000
     * 
     */
    AfDecimal getVisEnd2000Obj();

    void setVisEnd2000Obj(AfDecimal visEnd2000Obj);

    /**
     * Host Variable TGA-DT-VLDT-PROD-DB
     * 
     */
    String getDtVldtProdDb();

    void setDtVldtProdDb(String dtVldtProdDb);

    /**
     * Nullable property for TGA-DT-VLDT-PROD-DB
     * 
     */
    String getDtVldtProdDbObj();

    void setDtVldtProdDbObj(String dtVldtProdDbObj);

    /**
     * Host Variable TGA-DT-INI-VAL-TAR-DB
     * 
     */
    String getDtIniValTarDb();

    void setDtIniValTarDb(String dtIniValTarDb);

    /**
     * Nullable property for TGA-DT-INI-VAL-TAR-DB
     * 
     */
    String getDtIniValTarDbObj();

    void setDtIniValTarDbObj(String dtIniValTarDbObj);

    /**
     * Host Variable TGA-IMPB-VIS-END2000
     * 
     */
    AfDecimal getImpbVisEnd2000();

    void setImpbVisEnd2000(AfDecimal impbVisEnd2000);

    /**
     * Nullable property for TGA-IMPB-VIS-END2000
     * 
     */
    AfDecimal getImpbVisEnd2000Obj();

    void setImpbVisEnd2000Obj(AfDecimal impbVisEnd2000Obj);

    /**
     * Host Variable TGA-REN-INI-TS-TEC-0
     * 
     */
    AfDecimal getRenIniTsTec0();

    void setRenIniTsTec0(AfDecimal renIniTsTec0);

    /**
     * Nullable property for TGA-REN-INI-TS-TEC-0
     * 
     */
    AfDecimal getRenIniTsTec0Obj();

    void setRenIniTsTec0Obj(AfDecimal renIniTsTec0Obj);

    /**
     * Host Variable TGA-PC-RIP-PRE
     * 
     */
    AfDecimal getPcRipPre();

    void setPcRipPre(AfDecimal pcRipPre);

    /**
     * Nullable property for TGA-PC-RIP-PRE
     * 
     */
    AfDecimal getPcRipPreObj();

    void setPcRipPreObj(AfDecimal pcRipPreObj);

    /**
     * Host Variable TGA-FL-IMPORTI-FORZ
     * 
     */
    char getFlImportiForz();

    void setFlImportiForz(char flImportiForz);

    /**
     * Nullable property for TGA-FL-IMPORTI-FORZ
     * 
     */
    Character getFlImportiForzObj();

    void setFlImportiForzObj(Character flImportiForzObj);

    /**
     * Host Variable TGA-PRSTZ-INI-NFORZ
     * 
     */
    AfDecimal getPrstzIniNforz();

    void setPrstzIniNforz(AfDecimal prstzIniNforz);

    /**
     * Nullable property for TGA-PRSTZ-INI-NFORZ
     * 
     */
    AfDecimal getPrstzIniNforzObj();

    void setPrstzIniNforzObj(AfDecimal prstzIniNforzObj);

    /**
     * Host Variable TGA-VIS-END2000-NFORZ
     * 
     */
    AfDecimal getVisEnd2000Nforz();

    void setVisEnd2000Nforz(AfDecimal visEnd2000Nforz);

    /**
     * Nullable property for TGA-VIS-END2000-NFORZ
     * 
     */
    AfDecimal getVisEnd2000NforzObj();

    void setVisEnd2000NforzObj(AfDecimal visEnd2000NforzObj);

    /**
     * Host Variable TGA-INTR-MORA
     * 
     */
    AfDecimal getIntrMora();

    void setIntrMora(AfDecimal intrMora);

    /**
     * Nullable property for TGA-INTR-MORA
     * 
     */
    AfDecimal getIntrMoraObj();

    void setIntrMoraObj(AfDecimal intrMoraObj);

    /**
     * Host Variable TGA-MANFEE-ANTIC
     * 
     */
    AfDecimal getManfeeAntic();

    void setManfeeAntic(AfDecimal manfeeAntic);

    /**
     * Nullable property for TGA-MANFEE-ANTIC
     * 
     */
    AfDecimal getManfeeAnticObj();

    void setManfeeAnticObj(AfDecimal manfeeAnticObj);

    /**
     * Host Variable TGA-MANFEE-RICOR
     * 
     */
    AfDecimal getManfeeRicor();

    void setManfeeRicor(AfDecimal manfeeRicor);

    /**
     * Nullable property for TGA-MANFEE-RICOR
     * 
     */
    AfDecimal getManfeeRicorObj();

    void setManfeeRicorObj(AfDecimal manfeeRicorObj);

    /**
     * Host Variable TGA-PRE-UNI-RIVTO
     * 
     */
    AfDecimal getPreUniRivto();

    void setPreUniRivto(AfDecimal preUniRivto);

    /**
     * Nullable property for TGA-PRE-UNI-RIVTO
     * 
     */
    AfDecimal getPreUniRivtoObj();

    void setPreUniRivtoObj(AfDecimal preUniRivtoObj);

    /**
     * Host Variable TGA-PROV-1AA-ACQ
     * 
     */
    AfDecimal getProv1aaAcq();

    void setProv1aaAcq(AfDecimal prov1aaAcq);

    /**
     * Nullable property for TGA-PROV-1AA-ACQ
     * 
     */
    AfDecimal getProv1aaAcqObj();

    void setProv1aaAcqObj(AfDecimal prov1aaAcqObj);

    /**
     * Host Variable TGA-PROV-2AA-ACQ
     * 
     */
    AfDecimal getProv2aaAcq();

    void setProv2aaAcq(AfDecimal prov2aaAcq);

    /**
     * Nullable property for TGA-PROV-2AA-ACQ
     * 
     */
    AfDecimal getProv2aaAcqObj();

    void setProv2aaAcqObj(AfDecimal prov2aaAcqObj);

    /**
     * Host Variable TGA-PROV-RICOR
     * 
     */
    AfDecimal getProvRicor();

    void setProvRicor(AfDecimal provRicor);

    /**
     * Nullable property for TGA-PROV-RICOR
     * 
     */
    AfDecimal getProvRicorObj();

    void setProvRicorObj(AfDecimal provRicorObj);

    /**
     * Host Variable TGA-PROV-INC
     * 
     */
    AfDecimal getProvInc();

    void setProvInc(AfDecimal provInc);

    /**
     * Nullable property for TGA-PROV-INC
     * 
     */
    AfDecimal getProvIncObj();

    void setProvIncObj(AfDecimal provIncObj);

    /**
     * Host Variable TGA-ALQ-PROV-ACQ
     * 
     */
    AfDecimal getAlqProvAcq();

    void setAlqProvAcq(AfDecimal alqProvAcq);

    /**
     * Nullable property for TGA-ALQ-PROV-ACQ
     * 
     */
    AfDecimal getAlqProvAcqObj();

    void setAlqProvAcqObj(AfDecimal alqProvAcqObj);

    /**
     * Host Variable TGA-ALQ-PROV-INC
     * 
     */
    AfDecimal getAlqProvInc();

    void setAlqProvInc(AfDecimal alqProvInc);

    /**
     * Nullable property for TGA-ALQ-PROV-INC
     * 
     */
    AfDecimal getAlqProvIncObj();

    void setAlqProvIncObj(AfDecimal alqProvIncObj);

    /**
     * Host Variable TGA-ALQ-PROV-RICOR
     * 
     */
    AfDecimal getAlqProvRicor();

    void setAlqProvRicor(AfDecimal alqProvRicor);

    /**
     * Nullable property for TGA-ALQ-PROV-RICOR
     * 
     */
    AfDecimal getAlqProvRicorObj();

    void setAlqProvRicorObj(AfDecimal alqProvRicorObj);

    /**
     * Host Variable TGA-IMPB-PROV-ACQ
     * 
     */
    AfDecimal getImpbProvAcq();

    void setImpbProvAcq(AfDecimal impbProvAcq);

    /**
     * Nullable property for TGA-IMPB-PROV-ACQ
     * 
     */
    AfDecimal getImpbProvAcqObj();

    void setImpbProvAcqObj(AfDecimal impbProvAcqObj);

    /**
     * Host Variable TGA-IMPB-PROV-INC
     * 
     */
    AfDecimal getImpbProvInc();

    void setImpbProvInc(AfDecimal impbProvInc);

    /**
     * Nullable property for TGA-IMPB-PROV-INC
     * 
     */
    AfDecimal getImpbProvIncObj();

    void setImpbProvIncObj(AfDecimal impbProvIncObj);

    /**
     * Host Variable TGA-IMPB-PROV-RICOR
     * 
     */
    AfDecimal getImpbProvRicor();

    void setImpbProvRicor(AfDecimal impbProvRicor);

    /**
     * Nullable property for TGA-IMPB-PROV-RICOR
     * 
     */
    AfDecimal getImpbProvRicorObj();

    void setImpbProvRicorObj(AfDecimal impbProvRicorObj);

    /**
     * Host Variable TGA-FL-PROV-FORZ
     * 
     */
    char getFlProvForz();

    void setFlProvForz(char flProvForz);

    /**
     * Nullable property for TGA-FL-PROV-FORZ
     * 
     */
    Character getFlProvForzObj();

    void setFlProvForzObj(Character flProvForzObj);

    /**
     * Host Variable TGA-PRSTZ-AGG-INI
     * 
     */
    AfDecimal getPrstzAggIni();

    void setPrstzAggIni(AfDecimal prstzAggIni);

    /**
     * Nullable property for TGA-PRSTZ-AGG-INI
     * 
     */
    AfDecimal getPrstzAggIniObj();

    void setPrstzAggIniObj(AfDecimal prstzAggIniObj);

    /**
     * Host Variable TGA-INCR-PRE
     * 
     */
    AfDecimal getIncrPre();

    void setIncrPre(AfDecimal incrPre);

    /**
     * Nullable property for TGA-INCR-PRE
     * 
     */
    AfDecimal getIncrPreObj();

    void setIncrPreObj(AfDecimal incrPreObj);

    /**
     * Host Variable TGA-INCR-PRSTZ
     * 
     */
    AfDecimal getIncrPrstz();

    void setIncrPrstz(AfDecimal incrPrstz);

    /**
     * Nullable property for TGA-INCR-PRSTZ
     * 
     */
    AfDecimal getIncrPrstzObj();

    void setIncrPrstzObj(AfDecimal incrPrstzObj);

    /**
     * Host Variable TGA-DT-ULT-ADEG-PRE-PR-DB
     * 
     */
    String getDtUltAdegPrePrDb();

    void setDtUltAdegPrePrDb(String dtUltAdegPrePrDb);

    /**
     * Nullable property for TGA-DT-ULT-ADEG-PRE-PR-DB
     * 
     */
    String getDtUltAdegPrePrDbObj();

    void setDtUltAdegPrePrDbObj(String dtUltAdegPrePrDbObj);

    /**
     * Host Variable TGA-PRSTZ-AGG-ULT
     * 
     */
    AfDecimal getPrstzAggUlt();

    void setPrstzAggUlt(AfDecimal prstzAggUlt);

    /**
     * Nullable property for TGA-PRSTZ-AGG-ULT
     * 
     */
    AfDecimal getPrstzAggUltObj();

    void setPrstzAggUltObj(AfDecimal prstzAggUltObj);

    /**
     * Host Variable TGA-TS-RIVAL-NET
     * 
     */
    AfDecimal getTsRivalNet();

    void setTsRivalNet(AfDecimal tsRivalNet);

    /**
     * Nullable property for TGA-TS-RIVAL-NET
     * 
     */
    AfDecimal getTsRivalNetObj();

    void setTsRivalNetObj(AfDecimal tsRivalNetObj);

    /**
     * Host Variable TGA-PRE-PATTUITO
     * 
     */
    AfDecimal getPrePattuito();

    void setPrePattuito(AfDecimal prePattuito);

    /**
     * Nullable property for TGA-PRE-PATTUITO
     * 
     */
    AfDecimal getPrePattuitoObj();

    void setPrePattuitoObj(AfDecimal prePattuitoObj);

    /**
     * Host Variable TGA-TP-RIVAL
     * 
     */
    String getTpRival();

    void setTpRival(String tpRival);

    /**
     * Nullable property for TGA-TP-RIVAL
     * 
     */
    String getTpRivalObj();

    void setTpRivalObj(String tpRivalObj);

    /**
     * Host Variable TGA-RIS-MAT
     * 
     */
    AfDecimal getRisMat();

    void setRisMat(AfDecimal risMat);

    /**
     * Nullable property for TGA-RIS-MAT
     * 
     */
    AfDecimal getRisMatObj();

    void setRisMatObj(AfDecimal risMatObj);

    /**
     * Host Variable TGA-CPT-MIN-SCAD
     * 
     */
    AfDecimal getCptMinScad();

    void setCptMinScad(AfDecimal cptMinScad);

    /**
     * Nullable property for TGA-CPT-MIN-SCAD
     * 
     */
    AfDecimal getCptMinScadObj();

    void setCptMinScadObj(AfDecimal cptMinScadObj);

    /**
     * Host Variable TGA-COMMIS-GEST
     * 
     */
    AfDecimal getCommisGest();

    void setCommisGest(AfDecimal commisGest);

    /**
     * Nullable property for TGA-COMMIS-GEST
     * 
     */
    AfDecimal getCommisGestObj();

    void setCommisGestObj(AfDecimal commisGestObj);

    /**
     * Host Variable TGA-TP-MANFEE-APPL
     * 
     */
    String getTpManfeeAppl();

    void setTpManfeeAppl(String tpManfeeAppl);

    /**
     * Nullable property for TGA-TP-MANFEE-APPL
     * 
     */
    String getTpManfeeApplObj();

    void setTpManfeeApplObj(String tpManfeeApplObj);

    /**
     * Host Variable TGA-DS-RIGA
     * 
     */
    long getTgaDsRiga();

    void setTgaDsRiga(long tgaDsRiga);

    /**
     * Host Variable TGA-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable TGA-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable TGA-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable TGA-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable TGA-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable TGA-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable TGA-PC-COMMIS-GEST
     * 
     */
    AfDecimal getPcCommisGest();

    void setPcCommisGest(AfDecimal pcCommisGest);

    /**
     * Nullable property for TGA-PC-COMMIS-GEST
     * 
     */
    AfDecimal getPcCommisGestObj();

    void setPcCommisGestObj(AfDecimal pcCommisGestObj);

    /**
     * Host Variable TGA-NUM-GG-RIVAL
     * 
     */
    int getNumGgRival();

    void setNumGgRival(int numGgRival);

    /**
     * Nullable property for TGA-NUM-GG-RIVAL
     * 
     */
    Integer getNumGgRivalObj();

    void setNumGgRivalObj(Integer numGgRivalObj);

    /**
     * Host Variable TGA-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfe();

    void setImpTrasfe(AfDecimal impTrasfe);

    /**
     * Nullable property for TGA-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfeObj();

    void setImpTrasfeObj(AfDecimal impTrasfeObj);

    /**
     * Host Variable TGA-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrc();

    void setImpTfrStrc(AfDecimal impTfrStrc);

    /**
     * Nullable property for TGA-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrcObj();

    void setImpTfrStrcObj(AfDecimal impTfrStrcObj);

    /**
     * Host Variable TGA-ACQ-EXP
     * 
     */
    AfDecimal getAcqExp();

    void setAcqExp(AfDecimal acqExp);

    /**
     * Nullable property for TGA-ACQ-EXP
     * 
     */
    AfDecimal getAcqExpObj();

    void setAcqExpObj(AfDecimal acqExpObj);

    /**
     * Host Variable TGA-REMUN-ASS
     * 
     */
    AfDecimal getRemunAss();

    void setRemunAss(AfDecimal remunAss);

    /**
     * Nullable property for TGA-REMUN-ASS
     * 
     */
    AfDecimal getRemunAssObj();

    void setRemunAssObj(AfDecimal remunAssObj);

    /**
     * Host Variable TGA-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInter();

    void setCommisInter(AfDecimal commisInter);

    /**
     * Nullable property for TGA-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInterObj();

    void setCommisInterObj(AfDecimal commisInterObj);

    /**
     * Host Variable TGA-ALQ-REMUN-ASS
     * 
     */
    AfDecimal getAlqRemunAss();

    void setAlqRemunAss(AfDecimal alqRemunAss);

    /**
     * Nullable property for TGA-ALQ-REMUN-ASS
     * 
     */
    AfDecimal getAlqRemunAssObj();

    void setAlqRemunAssObj(AfDecimal alqRemunAssObj);

    /**
     * Host Variable TGA-ALQ-COMMIS-INTER
     * 
     */
    AfDecimal getAlqCommisInter();

    void setAlqCommisInter(AfDecimal alqCommisInter);

    /**
     * Nullable property for TGA-ALQ-COMMIS-INTER
     * 
     */
    AfDecimal getAlqCommisInterObj();

    void setAlqCommisInterObj(AfDecimal alqCommisInterObj);

    /**
     * Host Variable TGA-IMPB-REMUN-ASS
     * 
     */
    AfDecimal getImpbRemunAss();

    void setImpbRemunAss(AfDecimal impbRemunAss);

    /**
     * Nullable property for TGA-IMPB-REMUN-ASS
     * 
     */
    AfDecimal getImpbRemunAssObj();

    void setImpbRemunAssObj(AfDecimal impbRemunAssObj);

    /**
     * Host Variable TGA-IMPB-COMMIS-INTER
     * 
     */
    AfDecimal getImpbCommisInter();

    void setImpbCommisInter(AfDecimal impbCommisInter);

    /**
     * Nullable property for TGA-IMPB-COMMIS-INTER
     * 
     */
    AfDecimal getImpbCommisInterObj();

    void setImpbCommisInterObj(AfDecimal impbCommisInterObj);

    /**
     * Host Variable TGA-COS-RUN-ASSVA
     * 
     */
    AfDecimal getCosRunAssva();

    void setCosRunAssva(AfDecimal cosRunAssva);

    /**
     * Nullable property for TGA-COS-RUN-ASSVA
     * 
     */
    AfDecimal getCosRunAssvaObj();

    void setCosRunAssvaObj(AfDecimal cosRunAssvaObj);

    /**
     * Host Variable TGA-COS-RUN-ASSVA-IDC
     * 
     */
    AfDecimal getCosRunAssvaIdc();

    void setCosRunAssvaIdc(AfDecimal cosRunAssvaIdc);

    /**
     * Nullable property for TGA-COS-RUN-ASSVA-IDC
     * 
     */
    AfDecimal getCosRunAssvaIdcObj();

    void setCosRunAssvaIdcObj(AfDecimal cosRunAssvaIdcObj);
};
