package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IStatRichEst;

/**
 * Data Access Object(DAO) for table [STAT_RICH_EST]
 * 
 */
public class StatRichEstDao extends BaseSqlDao<IStatRichEst> {

    private Cursor cNst19;
    private final IRowMapper<IStatRichEst> selectByP04IdStatRichEstRm = buildNamedRowMapper(IStatRichEst.class, "p04IdStatRichEst", "idRichEst", "tsIniVldt", "tsEndVldt", "codCompAnia", "codPrcs", "codAttvt", "statRichEst", "flStatEndObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "tpCausScartoObj", "descErrVcharObj", "utenteInsAgg", "codErrScartoObj", "idMoviCrzObj");

    public StatRichEstDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IStatRichEst> getToClass() {
        return IStatRichEst.class;
    }

    public IStatRichEst selectByP04IdStatRichEst(int p04IdStatRichEst, IStatRichEst iStatRichEst) {
        return buildQuery("selectByP04IdStatRichEst").bind("p04IdStatRichEst", p04IdStatRichEst).rowMapper(selectByP04IdStatRichEstRm).singleResult(iStatRichEst);
    }

    public DbAccessStatus insertRec(IStatRichEst iStatRichEst) {
        return buildQuery("insertRec").bind(iStatRichEst).executeInsert();
    }

    public DbAccessStatus updateRec(IStatRichEst iStatRichEst) {
        return buildQuery("updateRec").bind(iStatRichEst).executeUpdate();
    }

    public DbAccessStatus deleteByP04IdStatRichEst(int p04IdStatRichEst) {
        return buildQuery("deleteByP04IdStatRichEst").bind("p04IdStatRichEst", p04IdStatRichEst).executeDelete();
    }

    public IStatRichEst selectRec(int p04IdRichEst, long wsTsCompetenza, int idsv0003CodiceCompagniaAnia, IStatRichEst iStatRichEst) {
        return buildQuery("selectRec").bind("p04IdRichEst", p04IdRichEst).bind("wsTsCompetenza", wsTsCompetenza).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByP04IdStatRichEstRm).singleResult(iStatRichEst);
    }

    public DbAccessStatus openCNst19(int p04IdRichEst, long wsTsCompetenza, int idsv0003CodiceCompagniaAnia) {
        cNst19 = buildQuery("openCNst19").bind("p04IdRichEst", p04IdRichEst).bind("wsTsCompetenza", wsTsCompetenza).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst19() {
        return closeCursor(cNst19);
    }

    public IStatRichEst fetchCNst19(IStatRichEst iStatRichEst) {
        return fetch(cNst19, iStatRichEst, selectByP04IdStatRichEstRm);
    }
}
