package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ITitRat;

/**
 * Data Access Object(DAO) for table [TIT_RAT]
 * 
 */
public class TitRatDao extends BaseSqlDao<ITitRat> {

    private Cursor cIdUpdEffTdr;
    private Cursor cIdoEffTdr;
    private Cursor cIdoCpzTdr;
    private final IRowMapper<ITitRat> selectByTdrDsRigaRm = buildNamedRowMapper(ITitRat.class, "idTitRat", "tdrIdOgg", "tdrTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpTit", "progTitObj", "tpPreTit", "tpStatTit", "dtIniCopDbObj", "dtEndCopDbObj", "impPagObj", "flSollObj", "frazObj", "dtApplzMoraDbObj", "flMoraObj", "idRappReteObj", "idRappAnaObj", "codDvsObj", "dtEmisTitDb", "dtEsiTitDbObj", "totPreNetObj", "totIntrFrazObj", "totIntrMoraObj", "totIntrPrestObj", "totIntrRetdtObj", "totIntrRiatObj", "totDirObj", "totSpeMedObj", "totSpeAgeObj", "totTaxObj", "totSoprSanObj", "totSoprTecObj", "totSoprSpoObj", "totSoprProfObj", "totSoprAltObj", "totPreTotObj", "totPrePpIasObj", "totCarIasObj", "totPreSoloRshObj", "totProvAcq1aaObj", "totProvAcq2aaObj", "totProvRicorObj", "totProvIncObj", "totProvDaRecObj", "impAzObj", "impAderObj", "impTfrObj", "impVoloObj", "flVldtTitObj", "totCarAcqObj", "totCarGestObj", "totCarIncObj", "totManfeeAnticObj", "totManfeeRicorObj", "totManfeeRecObj", "tdrDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "impTrasfeObj", "impTfrStrcObj", "totAcqExpObj", "totRemunAssObj", "totCommisInterObj", "totCnbtAntiracObj", "flIncAutogenObj");

    public TitRatDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ITitRat> getToClass() {
        return ITitRat.class;
    }

    public ITitRat selectByTdrDsRiga(long tdrDsRiga, ITitRat iTitRat) {
        return buildQuery("selectByTdrDsRiga").bind("tdrDsRiga", tdrDsRiga).rowMapper(selectByTdrDsRigaRm).singleResult(iTitRat);
    }

    public DbAccessStatus insertRec(ITitRat iTitRat) {
        return buildQuery("insertRec").bind(iTitRat).executeInsert();
    }

    public DbAccessStatus updateRec(ITitRat iTitRat) {
        return buildQuery("updateRec").bind(iTitRat).executeUpdate();
    }

    public DbAccessStatus deleteByTdrDsRiga(long tdrDsRiga) {
        return buildQuery("deleteByTdrDsRiga").bind("tdrDsRiga", tdrDsRiga).executeDelete();
    }

    public ITitRat selectRec(int tdrIdTitRat, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITitRat iTitRat) {
        return buildQuery("selectRec").bind("tdrIdTitRat", tdrIdTitRat).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTdrDsRigaRm).singleResult(iTitRat);
    }

    public DbAccessStatus updateRec1(ITitRat iTitRat) {
        return buildQuery("updateRec1").bind(iTitRat).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffTdr(int tdrIdTitRat, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffTdr = buildQuery("openCIdUpdEffTdr").bind("tdrIdTitRat", tdrIdTitRat).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffTdr() {
        return closeCursor(cIdUpdEffTdr);
    }

    public ITitRat fetchCIdUpdEffTdr(ITitRat iTitRat) {
        return fetch(cIdUpdEffTdr, iTitRat, selectByTdrDsRigaRm);
    }

    public ITitRat selectRec1(int tdrIdOgg, String tdrTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITitRat iTitRat) {
        return buildQuery("selectRec1").bind("tdrIdOgg", tdrIdOgg).bind("tdrTpOgg", tdrTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTdrDsRigaRm).singleResult(iTitRat);
    }

    public DbAccessStatus openCIdoEffTdr(int tdrIdOgg, String tdrTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffTdr = buildQuery("openCIdoEffTdr").bind("tdrIdOgg", tdrIdOgg).bind("tdrTpOgg", tdrTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffTdr() {
        return closeCursor(cIdoEffTdr);
    }

    public ITitRat fetchCIdoEffTdr(ITitRat iTitRat) {
        return fetch(cIdoEffTdr, iTitRat, selectByTdrDsRigaRm);
    }

    public ITitRat selectRec2(int tdrIdTitRat, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITitRat iTitRat) {
        return buildQuery("selectRec2").bind("tdrIdTitRat", tdrIdTitRat).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTdrDsRigaRm).singleResult(iTitRat);
    }

    public ITitRat selectRec3(ITitRat iTitRat) {
        return buildQuery("selectRec3").bind(iTitRat).rowMapper(selectByTdrDsRigaRm).singleResult(iTitRat);
    }

    public DbAccessStatus openCIdoCpzTdr(ITitRat iTitRat) {
        cIdoCpzTdr = buildQuery("openCIdoCpzTdr").bind(iTitRat).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzTdr() {
        return closeCursor(cIdoCpzTdr);
    }

    public ITitRat fetchCIdoCpzTdr(ITitRat iTitRat) {
        return fetch(cIdoCpzTdr, iTitRat, selectByTdrDsRigaRm);
    }
}
