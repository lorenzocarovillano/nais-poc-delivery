package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BNFIC]
 * 
 */
public interface IBnfic extends BaseSqlTo {

    /**
     * Host Variable BEP-ID-BNFIC
     * 
     */
    int getIdBnfic();

    void setIdBnfic(int idBnfic);

    /**
     * Host Variable BEP-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Host Variable BEP-ID-BNFICR
     * 
     */
    int getIdBnficr();

    void setIdBnficr(int idBnficr);

    /**
     * Nullable property for BEP-ID-BNFICR
     * 
     */
    Integer getIdBnficrObj();

    void setIdBnficrObj(Integer idBnficrObj);

    /**
     * Host Variable BEP-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable BEP-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for BEP-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable BEP-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable BEP-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable BEP-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable BEP-COD-BNFIC
     * 
     */
    short getCodBnfic();

    void setCodBnfic(short codBnfic);

    /**
     * Nullable property for BEP-COD-BNFIC
     * 
     */
    Short getCodBnficObj();

    void setCodBnficObj(Short codBnficObj);

    /**
     * Host Variable BEP-TP-IND-BNFICR
     * 
     */
    String getTpIndBnficr();

    void setTpIndBnficr(String tpIndBnficr);

    /**
     * Host Variable BEP-COD-BNFICR
     * 
     */
    String getCodBnficr();

    void setCodBnficr(String codBnficr);

    /**
     * Nullable property for BEP-COD-BNFICR
     * 
     */
    String getCodBnficrObj();

    void setCodBnficrObj(String codBnficrObj);

    /**
     * Host Variable BEP-DESC-BNFICR-VCHAR
     * 
     */
    String getDescBnficrVchar();

    void setDescBnficrVchar(String descBnficrVchar);

    /**
     * Nullable property for BEP-DESC-BNFICR-VCHAR
     * 
     */
    String getDescBnficrVcharObj();

    void setDescBnficrVcharObj(String descBnficrVcharObj);

    /**
     * Host Variable BEP-PC-DEL-BNFICR
     * 
     */
    AfDecimal getPcDelBnficr();

    void setPcDelBnficr(AfDecimal pcDelBnficr);

    /**
     * Nullable property for BEP-PC-DEL-BNFICR
     * 
     */
    AfDecimal getPcDelBnficrObj();

    void setPcDelBnficrObj(AfDecimal pcDelBnficrObj);

    /**
     * Host Variable BEP-FL-ESE
     * 
     */
    char getFlEse();

    void setFlEse(char flEse);

    /**
     * Nullable property for BEP-FL-ESE
     * 
     */
    Character getFlEseObj();

    void setFlEseObj(Character flEseObj);

    /**
     * Host Variable BEP-FL-IRREV
     * 
     */
    char getFlIrrev();

    void setFlIrrev(char flIrrev);

    /**
     * Nullable property for BEP-FL-IRREV
     * 
     */
    Character getFlIrrevObj();

    void setFlIrrevObj(Character flIrrevObj);

    /**
     * Host Variable BEP-FL-DFLT
     * 
     */
    char getFlDflt();

    void setFlDflt(char flDflt);

    /**
     * Nullable property for BEP-FL-DFLT
     * 
     */
    Character getFlDfltObj();

    void setFlDfltObj(Character flDfltObj);

    /**
     * Host Variable BEP-ESRCN-ATTVT-IMPRS
     * 
     */
    char getEsrcnAttvtImprs();

    void setEsrcnAttvtImprs(char esrcnAttvtImprs);

    /**
     * Nullable property for BEP-ESRCN-ATTVT-IMPRS
     * 
     */
    Character getEsrcnAttvtImprsObj();

    void setEsrcnAttvtImprsObj(Character esrcnAttvtImprsObj);

    /**
     * Host Variable BEP-FL-BNFICR-COLL
     * 
     */
    char getFlBnficrColl();

    void setFlBnficrColl(char flBnficrColl);

    /**
     * Host Variable BEP-DS-RIGA
     * 
     */
    long getBepDsRiga();

    void setBepDsRiga(long bepDsRiga);

    /**
     * Host Variable BEP-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable BEP-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable BEP-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable BEP-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable BEP-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable BEP-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable BEP-TP-NORMAL-BNFIC
     * 
     */
    String getTpNormalBnfic();

    void setTpNormalBnfic(String tpNormalBnfic);

    /**
     * Nullable property for BEP-TP-NORMAL-BNFIC
     * 
     */
    String getTpNormalBnficObj();

    void setTpNormalBnficObj(String tpNormalBnficObj);
};
