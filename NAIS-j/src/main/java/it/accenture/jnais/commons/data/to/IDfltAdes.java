package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [DFLT_ADES]
 * 
 */
public interface IDfltAdes extends BaseSqlTo {

    /**
     * Host Variable DAD-ID-DFLT-ADES
     * 
     */
    int getDadIdDfltAdes();

    void setDadIdDfltAdes(int dadIdDfltAdes);

    /**
     * Host Variable DAD-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable DAD-IB-POLI
     * 
     */
    String getIbPoli();

    void setIbPoli(String ibPoli);

    /**
     * Nullable property for DAD-IB-POLI
     * 
     */
    String getIbPoliObj();

    void setIbPoliObj(String ibPoliObj);

    /**
     * Host Variable DAD-IB-DFLT
     * 
     */
    String getIbDflt();

    void setIbDflt(String ibDflt);

    /**
     * Nullable property for DAD-IB-DFLT
     * 
     */
    String getIbDfltObj();

    void setIbDfltObj(String ibDfltObj);

    /**
     * Host Variable DAD-COD-GAR-1
     * 
     */
    String getCodGar1();

    void setCodGar1(String codGar1);

    /**
     * Nullable property for DAD-COD-GAR-1
     * 
     */
    String getCodGar1Obj();

    void setCodGar1Obj(String codGar1Obj);

    /**
     * Host Variable DAD-COD-GAR-2
     * 
     */
    String getCodGar2();

    void setCodGar2(String codGar2);

    /**
     * Nullable property for DAD-COD-GAR-2
     * 
     */
    String getCodGar2Obj();

    void setCodGar2Obj(String codGar2Obj);

    /**
     * Host Variable DAD-COD-GAR-3
     * 
     */
    String getCodGar3();

    void setCodGar3(String codGar3);

    /**
     * Nullable property for DAD-COD-GAR-3
     * 
     */
    String getCodGar3Obj();

    void setCodGar3Obj(String codGar3Obj);

    /**
     * Host Variable DAD-COD-GAR-4
     * 
     */
    String getCodGar4();

    void setCodGar4(String codGar4);

    /**
     * Nullable property for DAD-COD-GAR-4
     * 
     */
    String getCodGar4Obj();

    void setCodGar4Obj(String codGar4Obj);

    /**
     * Host Variable DAD-COD-GAR-5
     * 
     */
    String getCodGar5();

    void setCodGar5(String codGar5);

    /**
     * Nullable property for DAD-COD-GAR-5
     * 
     */
    String getCodGar5Obj();

    void setCodGar5Obj(String codGar5Obj);

    /**
     * Host Variable DAD-COD-GAR-6
     * 
     */
    String getCodGar6();

    void setCodGar6(String codGar6);

    /**
     * Nullable property for DAD-COD-GAR-6
     * 
     */
    String getCodGar6Obj();

    void setCodGar6Obj(String codGar6Obj);

    /**
     * Host Variable DAD-DT-DECOR-DFLT-DB
     * 
     */
    String getDtDecorDfltDb();

    void setDtDecorDfltDb(String dtDecorDfltDb);

    /**
     * Nullable property for DAD-DT-DECOR-DFLT-DB
     * 
     */
    String getDtDecorDfltDbObj();

    void setDtDecorDfltDbObj(String dtDecorDfltDbObj);

    /**
     * Host Variable DAD-ETA-SCAD-MASC-DFLT
     * 
     */
    int getEtaScadMascDflt();

    void setEtaScadMascDflt(int etaScadMascDflt);

    /**
     * Nullable property for DAD-ETA-SCAD-MASC-DFLT
     * 
     */
    Integer getEtaScadMascDfltObj();

    void setEtaScadMascDfltObj(Integer etaScadMascDfltObj);

    /**
     * Host Variable DAD-ETA-SCAD-FEMM-DFLT
     * 
     */
    int getEtaScadFemmDflt();

    void setEtaScadFemmDflt(int etaScadFemmDflt);

    /**
     * Nullable property for DAD-ETA-SCAD-FEMM-DFLT
     * 
     */
    Integer getEtaScadFemmDfltObj();

    void setEtaScadFemmDfltObj(Integer etaScadFemmDfltObj);

    /**
     * Host Variable DAD-DUR-AA-ADES-DFLT
     * 
     */
    int getDurAaAdesDflt();

    void setDurAaAdesDflt(int durAaAdesDflt);

    /**
     * Nullable property for DAD-DUR-AA-ADES-DFLT
     * 
     */
    Integer getDurAaAdesDfltObj();

    void setDurAaAdesDfltObj(Integer durAaAdesDfltObj);

    /**
     * Host Variable DAD-DUR-MM-ADES-DFLT
     * 
     */
    int getDurMmAdesDflt();

    void setDurMmAdesDflt(int durMmAdesDflt);

    /**
     * Nullable property for DAD-DUR-MM-ADES-DFLT
     * 
     */
    Integer getDurMmAdesDfltObj();

    void setDurMmAdesDfltObj(Integer durMmAdesDfltObj);

    /**
     * Host Variable DAD-DUR-GG-ADES-DFLT
     * 
     */
    int getDurGgAdesDflt();

    void setDurGgAdesDflt(int durGgAdesDflt);

    /**
     * Nullable property for DAD-DUR-GG-ADES-DFLT
     * 
     */
    Integer getDurGgAdesDfltObj();

    void setDurGgAdesDfltObj(Integer durGgAdesDfltObj);

    /**
     * Host Variable DAD-DT-SCAD-ADES-DFLT-DB
     * 
     */
    String getDtScadAdesDfltDb();

    void setDtScadAdesDfltDb(String dtScadAdesDfltDb);

    /**
     * Nullable property for DAD-DT-SCAD-ADES-DFLT-DB
     * 
     */
    String getDtScadAdesDfltDbObj();

    void setDtScadAdesDfltDbObj(String dtScadAdesDfltDbObj);

    /**
     * Host Variable DAD-FRAZ-DFLT
     * 
     */
    int getFrazDflt();

    void setFrazDflt(int frazDflt);

    /**
     * Nullable property for DAD-FRAZ-DFLT
     * 
     */
    Integer getFrazDfltObj();

    void setFrazDfltObj(Integer frazDfltObj);

    /**
     * Host Variable DAD-PC-PROV-INC-DFLT
     * 
     */
    AfDecimal getPcProvIncDflt();

    void setPcProvIncDflt(AfDecimal pcProvIncDflt);

    /**
     * Nullable property for DAD-PC-PROV-INC-DFLT
     * 
     */
    AfDecimal getPcProvIncDfltObj();

    void setPcProvIncDfltObj(AfDecimal pcProvIncDfltObj);

    /**
     * Host Variable DAD-IMP-PROV-INC-DFLT
     * 
     */
    AfDecimal getImpProvIncDflt();

    void setImpProvIncDflt(AfDecimal impProvIncDflt);

    /**
     * Nullable property for DAD-IMP-PROV-INC-DFLT
     * 
     */
    AfDecimal getImpProvIncDfltObj();

    void setImpProvIncDfltObj(AfDecimal impProvIncDfltObj);

    /**
     * Host Variable DAD-IMP-AZ
     * 
     */
    AfDecimal getImpAz();

    void setImpAz(AfDecimal impAz);

    /**
     * Nullable property for DAD-IMP-AZ
     * 
     */
    AfDecimal getImpAzObj();

    void setImpAzObj(AfDecimal impAzObj);

    /**
     * Host Variable DAD-IMP-ADER
     * 
     */
    AfDecimal getImpAder();

    void setImpAder(AfDecimal impAder);

    /**
     * Nullable property for DAD-IMP-ADER
     * 
     */
    AfDecimal getImpAderObj();

    void setImpAderObj(AfDecimal impAderObj);

    /**
     * Host Variable DAD-IMP-TFR
     * 
     */
    AfDecimal getImpTfr();

    void setImpTfr(AfDecimal impTfr);

    /**
     * Nullable property for DAD-IMP-TFR
     * 
     */
    AfDecimal getImpTfrObj();

    void setImpTfrObj(AfDecimal impTfrObj);

    /**
     * Host Variable DAD-IMP-VOLO
     * 
     */
    AfDecimal getImpVolo();

    void setImpVolo(AfDecimal impVolo);

    /**
     * Nullable property for DAD-IMP-VOLO
     * 
     */
    AfDecimal getImpVoloObj();

    void setImpVoloObj(AfDecimal impVoloObj);

    /**
     * Host Variable DAD-PC-AZ
     * 
     */
    AfDecimal getPcAz();

    void setPcAz(AfDecimal pcAz);

    /**
     * Nullable property for DAD-PC-AZ
     * 
     */
    AfDecimal getPcAzObj();

    void setPcAzObj(AfDecimal pcAzObj);

    /**
     * Host Variable DAD-PC-ADER
     * 
     */
    AfDecimal getPcAder();

    void setPcAder(AfDecimal pcAder);

    /**
     * Nullable property for DAD-PC-ADER
     * 
     */
    AfDecimal getPcAderObj();

    void setPcAderObj(AfDecimal pcAderObj);

    /**
     * Host Variable DAD-PC-TFR
     * 
     */
    AfDecimal getPcTfr();

    void setPcTfr(AfDecimal pcTfr);

    /**
     * Nullable property for DAD-PC-TFR
     * 
     */
    AfDecimal getPcTfrObj();

    void setPcTfrObj(AfDecimal pcTfrObj);

    /**
     * Host Variable DAD-PC-VOLO
     * 
     */
    AfDecimal getPcVolo();

    void setPcVolo(AfDecimal pcVolo);

    /**
     * Nullable property for DAD-PC-VOLO
     * 
     */
    AfDecimal getPcVoloObj();

    void setPcVoloObj(AfDecimal pcVoloObj);

    /**
     * Host Variable DAD-TP-FNT-CNBTVA
     * 
     */
    String getTpFntCnbtva();

    void setTpFntCnbtva(String tpFntCnbtva);

    /**
     * Nullable property for DAD-TP-FNT-CNBTVA
     * 
     */
    String getTpFntCnbtvaObj();

    void setTpFntCnbtvaObj(String tpFntCnbtvaObj);

    /**
     * Host Variable DAD-IMP-PRE-DFLT
     * 
     */
    AfDecimal getImpPreDflt();

    void setImpPreDflt(AfDecimal impPreDflt);

    /**
     * Nullable property for DAD-IMP-PRE-DFLT
     * 
     */
    AfDecimal getImpPreDfltObj();

    void setImpPreDfltObj(AfDecimal impPreDfltObj);

    /**
     * Host Variable DAD-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable DAD-TP-PRE
     * 
     */
    char getTpPre();

    void setTpPre(char tpPre);

    /**
     * Nullable property for DAD-TP-PRE
     * 
     */
    Character getTpPreObj();

    void setTpPreObj(Character tpPreObj);

    /**
     * Host Variable DAD-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable DAD-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable DAD-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable DAD-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable DAD-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
