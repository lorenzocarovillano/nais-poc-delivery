package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IPrest;

/**
 * Data Access Object(DAO) for table [PREST]
 * 
 */
public class PrestDao extends BaseSqlDao<IPrest> {

    private Cursor cIdUpdEffPre;
    private Cursor cIdoEffPre;
    private Cursor cIdoCpzPre;
    private final IRowMapper<IPrest> selectByPreDsRigaRm = buildNamedRowMapper(IPrest.class, "idPrest", "preIdOgg", "preTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtConcsPrestDbObj", "dtDecorPrestDbObj", "impPrestObj", "intrPrestObj", "tpPrestObj", "frazPagIntrObj", "dtRimbDbObj", "impRimbObj", "codDvsObj", "dtRichPrestDb", "modIntrPrestObj", "spePrestObj", "impPrestLiqtoObj", "sdoIntrObj", "rimbEffObj", "prestResEffObj", "preDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");
    private final IRowMapper<IPrest> selectRec5Rm = buildNamedRowMapper(IPrest.class, "ldbv2821ImpPrestObj");

    public PrestDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IPrest> getToClass() {
        return IPrest.class;
    }

    public IPrest selectByPreDsRiga(long preDsRiga, IPrest iPrest) {
        return buildQuery("selectByPreDsRiga").bind("preDsRiga", preDsRiga).rowMapper(selectByPreDsRigaRm).singleResult(iPrest);
    }

    public DbAccessStatus insertRec(IPrest iPrest) {
        return buildQuery("insertRec").bind(iPrest).executeInsert();
    }

    public DbAccessStatus updateRec(IPrest iPrest) {
        return buildQuery("updateRec").bind(iPrest).executeUpdate();
    }

    public DbAccessStatus deleteByPreDsRiga(long preDsRiga) {
        return buildQuery("deleteByPreDsRiga").bind("preDsRiga", preDsRiga).executeDelete();
    }

    public IPrest selectRec(int preIdPrest, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPrest iPrest) {
        return buildQuery("selectRec").bind("preIdPrest", preIdPrest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPreDsRigaRm).singleResult(iPrest);
    }

    public DbAccessStatus updateRec1(IPrest iPrest) {
        return buildQuery("updateRec1").bind(iPrest).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffPre(int preIdPrest, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffPre = buildQuery("openCIdUpdEffPre").bind("preIdPrest", preIdPrest).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffPre() {
        return closeCursor(cIdUpdEffPre);
    }

    public IPrest fetchCIdUpdEffPre(IPrest iPrest) {
        return fetch(cIdUpdEffPre, iPrest, selectByPreDsRigaRm);
    }

    public IPrest selectRec1(int preIdOgg, String preTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPrest iPrest) {
        return buildQuery("selectRec1").bind("preIdOgg", preIdOgg).bind("preTpOgg", preTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPreDsRigaRm).singleResult(iPrest);
    }

    public DbAccessStatus openCIdoEffPre(int preIdOgg, String preTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffPre = buildQuery("openCIdoEffPre").bind("preIdOgg", preIdOgg).bind("preTpOgg", preTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffPre() {
        return closeCursor(cIdoEffPre);
    }

    public IPrest fetchCIdoEffPre(IPrest iPrest) {
        return fetch(cIdoEffPre, iPrest, selectByPreDsRigaRm);
    }

    public IPrest selectRec2(int preIdPrest, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IPrest iPrest) {
        return buildQuery("selectRec2").bind("preIdPrest", preIdPrest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPreDsRigaRm).singleResult(iPrest);
    }

    public IPrest selectRec3(IPrest iPrest) {
        return buildQuery("selectRec3").bind(iPrest).rowMapper(selectByPreDsRigaRm).singleResult(iPrest);
    }

    public DbAccessStatus openCIdoCpzPre(IPrest iPrest) {
        cIdoCpzPre = buildQuery("openCIdoCpzPre").bind(iPrest).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzPre() {
        return closeCursor(cIdoCpzPre);
    }

    public IPrest fetchCIdoCpzPre(IPrest iPrest) {
        return fetch(cIdoCpzPre, iPrest, selectByPreDsRigaRm);
    }

    public IPrest selectRec4(int ldbv2821IdOgg, String ldbv2821TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
    	IPrest iprest = null;
    	buildQuery("selectRec").bind("ldbv2821IdOgg", ldbv2821IdOgg).bind("ldbv2821TpOgg", ldbv2821TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPreDsRigaRm).singleResult(iprest);
    	return iprest;
    }

    public IPrest selectRec5(IPrest iPrest) {
        return buildQuery("selectRec5").bind(iPrest).rowMapper(selectRec5Rm).singleResult(iPrest);
    }

    public IPrest selectRec6(int preIdOgg, String preTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPrest iPrest) {
        return buildQuery("selectRec6").bind("preIdOgg", preIdOgg).bind("preTpOgg", preTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPreDsRigaRm).singleResult(iPrest);
    }

    public IPrest selectRec7(IPrest iPrest) {
        return buildQuery("selectRec7").bind(iPrest).rowMapper(selectByPreDsRigaRm).singleResult(iPrest);
    }
}
