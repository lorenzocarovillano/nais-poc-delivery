package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IPoli;

/**
 * Data Access Object(DAO) for table [POLI]
 * 
 */
public class PoliDao extends BaseSqlDao<IPoli> {

    private Cursor cIdUpdEffPol;
    private Cursor cIboEffPol;
    private Cursor cIbsEffPol0;
    private Cursor cIbsEffPol1;
    private Cursor cIboCpzPol;
    private Cursor cIbsCpzPol0;
    private Cursor cIbsCpzPol1;
    private Cursor cEff57;
    private final IRowMapper<IPoli> selectByPolDsRigaRm = buildNamedRowMapper(IPoli.class, "idPoli", "idMoviCrz", "idMoviChiuObj", "ibOggObj", "ibProp", "dtPropDbObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtDecorDb", "dtEmisDb", "tpPoli", "durAaObj", "durMmObj", "dtScadDbObj", "codProd", "dtIniVldtProdDb", "codConvObj", "codRamoObj", "dtIniVldtConvDbObj", "dtApplzConvDbObj", "tpFrmAssva", "tpRgmFiscObj", "flEstasObj", "flRshComunObj", "flRshComunCondObj", "tpLivGenzTit", "flCopFinanzObj", "tpApplzDirObj", "speMedObj", "dirEmisObj", "dir1oVersObj", "dirVersAggObj", "codDvsObj", "flFntAzObj", "flFntAderObj", "flFntTfrObj", "flFntVoloObj", "tpOpzAScadObj", "aaDiffProrDfltObj", "flVerProdObj", "durGgObj", "dirQuietObj", "tpPtfEstnoObj", "flCumPreCntrObj", "flAmmbMoviObj", "convGecoObj", "polDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "flScudoFiscObj", "flTrasfeObj", "flTfrStrcObj", "dtPrescDbObj", "codConvAggObj", "subcatProdObj", "flQuestAdegzAssObj", "codTpaObj", "idAccCommObj", "flPoliCpiPrObj", "flPoliBundlingObj", "indPoliPrinCollObj", "flVndBundleObj", "ibBsObj", "flPoliIfpObj");

    public PoliDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IPoli> getToClass() {
        return IPoli.class;
    }

    public IPoli selectByPolDsRiga(long polDsRiga, IPoli iPoli) {
        return buildQuery("selectByPolDsRiga").bind("polDsRiga", polDsRiga).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public DbAccessStatus insertRec(IPoli iPoli) {
        return buildQuery("insertRec").bind(iPoli).executeInsert();
    }

    public DbAccessStatus updateRec(IPoli iPoli) {
        return buildQuery("updateRec").bind(iPoli).executeUpdate();
    }

    public DbAccessStatus deleteByPolDsRiga(long polDsRiga) {
        return buildQuery("deleteByPolDsRiga").bind("polDsRiga", polDsRiga).executeDelete();
    }

    public IPoli selectRec(int polIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPoli iPoli) {
        return buildQuery("selectRec").bind("polIdPoli", polIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public DbAccessStatus updateRec1(IPoli iPoli) {
        return buildQuery("updateRec1").bind(iPoli).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffPol(int polIdPoli, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffPol = buildQuery("openCIdUpdEffPol").bind("polIdPoli", polIdPoli).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffPol() {
        return closeCursor(cIdUpdEffPol);
    }

    public IPoli fetchCIdUpdEffPol(IPoli iPoli) {
        return fetch(cIdUpdEffPol, iPoli, selectByPolDsRigaRm);
    }

    public IPoli selectRec1(String polIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPoli iPoli) {
        return buildQuery("selectRec1").bind("polIbOgg", polIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public DbAccessStatus openCIboEffPol(String polIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIboEffPol = buildQuery("openCIboEffPol").bind("polIbOgg", polIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboEffPol() {
        return closeCursor(cIboEffPol);
    }

    public IPoli fetchCIboEffPol(IPoli iPoli) {
        return fetch(cIboEffPol, iPoli, selectByPolDsRigaRm);
    }

    public IPoli selectRec2(String polIbProp, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPoli iPoli) {
        return buildQuery("selectRec2").bind("polIbProp", polIbProp).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public IPoli selectRec3(String polIbBs, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPoli iPoli) {
        return buildQuery("selectRec3").bind("polIbBs", polIbBs).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public DbAccessStatus openCIbsEffPol0(String polIbProp, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffPol0 = buildQuery("openCIbsEffPol0").bind("polIbProp", polIbProp).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus openCIbsEffPol1(String polIbBs, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffPol1 = buildQuery("openCIbsEffPol1").bind("polIbBs", polIbBs).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsEffPol0() {
        return closeCursor(cIbsEffPol0);
    }

    public DbAccessStatus closeCIbsEffPol1() {
        return closeCursor(cIbsEffPol1);
    }

    public IPoli fetchCIbsEffPol0(IPoli iPoli) {
        return fetch(cIbsEffPol0, iPoli, selectByPolDsRigaRm);
    }

    public IPoli fetchCIbsEffPol1(IPoli iPoli) {
        return fetch(cIbsEffPol1, iPoli, selectByPolDsRigaRm);
    }

    public IPoli selectRec4(int polIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IPoli iPoli) {
        return buildQuery("selectRec4").bind("polIdPoli", polIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public IPoli selectRec5(String polIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IPoli iPoli) {
        return buildQuery("selectRec5").bind("polIbOgg", polIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public DbAccessStatus openCIboCpzPol(String polIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIboCpzPol = buildQuery("openCIboCpzPol").bind("polIbOgg", polIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboCpzPol() {
        return closeCursor(cIboCpzPol);
    }

    public IPoli fetchCIboCpzPol(IPoli iPoli) {
        return fetch(cIboCpzPol, iPoli, selectByPolDsRigaRm);
    }

    public IPoli selectRec6(String polIbProp, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IPoli iPoli) {
        return buildQuery("selectRec6").bind("polIbProp", polIbProp).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public IPoli selectRec7(String polIbBs, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IPoli iPoli) {
        return buildQuery("selectRec7").bind("polIbBs", polIbBs).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public DbAccessStatus openCIbsCpzPol0(String polIbProp, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzPol0 = buildQuery("openCIbsCpzPol0").bind("polIbProp", polIbProp).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus openCIbsCpzPol1(String polIbBs, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzPol1 = buildQuery("openCIbsCpzPol1").bind("polIbBs", polIbBs).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsCpzPol0() {
        return closeCursor(cIbsCpzPol0);
    }

    public DbAccessStatus closeCIbsCpzPol1() {
        return closeCursor(cIbsCpzPol1);
    }

    public IPoli fetchCIbsCpzPol0(IPoli iPoli) {
        return fetch(cIbsCpzPol0, iPoli, selectByPolDsRigaRm);
    }

    public IPoli fetchCIbsCpzPol1(IPoli iPoli) {
        return fetch(cIbsCpzPol1, iPoli, selectByPolDsRigaRm);
    }

    public IPoli selectRec8(int polIdPoli, int idsv0003CodiceCompagniaAnia, IPoli iPoli) {
        return buildQuery("selectRec8").bind("polIdPoli", polIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByPolDsRigaRm).singleResult(iPoli);
    }

    public DbAccessStatus openCEff57(int polIdPoli, int idsv0003CodiceCompagniaAnia) {
        cEff57 = buildQuery("openCEff57").bind("polIdPoli", polIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff57() {
        return closeCursor(cEff57);
    }

    public IPoli fetchCEff57(IPoli iPoli) {
        return fetch(cEff57, iPoli, selectByPolDsRigaRm);
    }
}
