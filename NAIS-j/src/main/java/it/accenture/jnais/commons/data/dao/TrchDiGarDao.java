package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ITrchDiGar;

/**
 * Data Access Object(DAO) for table [TRCH_DI_GAR]
 * 
 */
public class TrchDiGarDao extends BaseSqlDao<ITrchDiGar> {

    private Cursor cIdUpdEffTga;
    private Cursor cIdpEffTga;
    private Cursor cIboEffTga;
    private Cursor cIdpCpzTga;
    private Cursor cIboCpzTga;
    private Cursor cEff;
    private Cursor cCpz;
    private Cursor cEff44;
    private Cursor cCpz44;
    private final IRowMapper<ITrchDiGar> selectByTgaDsRigaRm = buildNamedRowMapper(ITrchDiGar.class, "idTrchDiGar", "idGar", "tgaIdAdes", "tgaIdPoli", "tgaIdMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtDecorDb", "dtScadDbObj", "ibOggObj", "tpRgmFisc", "dtEmisDbObj", "tpTrch", "durAaObj", "durMmObj", "durGgObj", "preCasoMorObj", "pcIntrRiatObj", "impBnsAnticObj", "preIniNetObj", "prePpIniObj", "prePpUltObj", "preTariIniObj", "preTariUltObj", "preInvrioIniObj", "preInvrioUltObj", "preRivtoObj", "impSoprProfObj", "impSoprSanObj", "impSoprSpoObj", "impSoprTecObj", "impAltSoprObj", "preStabObj", "dtEffStabDbObj", "tsRivalFisObj", "tsRivalIndicizObj", "oldTsTecObj", "ratLrdObj", "preLrdObj", "prstzIniObj", "prstzUltObj", "cptInOpzRivtoObj", "prstzIniStabObj", "cptRshMorObj", "prstzRidIniObj", "flCarContObj", "bnsGiaLiqtoObj", "impBnsObj", "codDvs", "prstzIniNewfisObj", "impSconObj", "alqSconObj", "impCarAcqObj", "impCarIncObj", "impCarGestObj", "etaAa1oAsstoObj", "etaMm1oAsstoObj", "etaAa2oAsstoObj", "etaMm2oAsstoObj", "etaAa3oAsstoObj", "etaMm3oAsstoObj", "rendtoLrdObj", "pcRetrObj", "rendtoRetrObj", "minGartoObj", "minTrnutObj", "preAttDiTrchObj", "matuEnd2000Obj", "abbTotIniObj", "abbTotUltObj", "abbAnnuUltObj", "durAbbObj", "tpAdegAbbObj", "modCalcObj", "impAzObj", "impAderObj", "impTfrObj", "impVoloObj", "visEnd2000Obj", "dtVldtProdDbObj", "dtIniValTarDbObj", "impbVisEnd2000Obj", "renIniTsTec0Obj", "pcRipPreObj", "flImportiForzObj", "prstzIniNforzObj", "visEnd2000NforzObj", "intrMoraObj", "manfeeAnticObj", "manfeeRicorObj", "preUniRivtoObj", "prov1aaAcqObj", "prov2aaAcqObj", "provRicorObj", "provIncObj", "alqProvAcqObj", "alqProvIncObj", "alqProvRicorObj", "impbProvAcqObj", "impbProvIncObj", "impbProvRicorObj", "flProvForzObj", "prstzAggIniObj", "incrPreObj", "incrPrstzObj", "dtUltAdegPrePrDbObj", "prstzAggUltObj", "tsRivalNetObj", "prePattuitoObj", "tpRivalObj", "risMatObj", "cptMinScadObj", "commisGestObj", "tpManfeeApplObj", "tgaDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "pcCommisGestObj", "numGgRivalObj", "impTrasfeObj", "impTfrStrcObj", "acqExpObj", "remunAssObj", "commisInterObj", "alqRemunAssObj", "alqCommisInterObj", "impbRemunAssObj", "impbCommisInterObj", "cosRunAssvaObj", "cosRunAssvaIdcObj");

    public TrchDiGarDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ITrchDiGar> getToClass() {
        return ITrchDiGar.class;
    }

    public ITrchDiGar selectByTgaDsRiga(long tgaDsRiga, ITrchDiGar iTrchDiGar) {
        return buildQuery("selectByTgaDsRiga").bind("tgaDsRiga", tgaDsRiga).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus insertRec(ITrchDiGar iTrchDiGar) {
        return buildQuery("insertRec").bind(iTrchDiGar).executeInsert();
    }

    public DbAccessStatus updateRec(ITrchDiGar iTrchDiGar) {
        return buildQuery("updateRec").bind(iTrchDiGar).executeUpdate();
    }

    public DbAccessStatus deleteByTgaDsRiga(long tgaDsRiga) {
        return buildQuery("deleteByTgaDsRiga").bind("tgaDsRiga", tgaDsRiga).executeDelete();
    }

    public ITrchDiGar selectRec(int tgaIdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec").bind("tgaIdTrchDiGar", tgaIdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus updateRec1(ITrchDiGar iTrchDiGar) {
        return buildQuery("updateRec1").bind(iTrchDiGar).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffTga(int tgaIdTrchDiGar, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffTga = buildQuery("openCIdUpdEffTga").bind("tgaIdTrchDiGar", tgaIdTrchDiGar).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffTga() {
        return closeCursor(cIdUpdEffTga);
    }

    public ITrchDiGar fetchCIdUpdEffTga(ITrchDiGar iTrchDiGar) {
        return fetch(cIdUpdEffTga, iTrchDiGar, selectByTgaDsRigaRm);
    }

    public ITrchDiGar selectRec1(int tgaIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec1").bind("tgaIdPoli", tgaIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus openCIdpEffTga(int tgaIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffTga = buildQuery("openCIdpEffTga").bind("tgaIdPoli", tgaIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffTga() {
        return closeCursor(cIdpEffTga);
    }

    public ITrchDiGar fetchCIdpEffTga(ITrchDiGar iTrchDiGar) {
        return fetch(cIdpEffTga, iTrchDiGar, selectByTgaDsRigaRm);
    }

    public ITrchDiGar selectRec2(String tgaIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec2").bind("tgaIbOgg", tgaIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus openCIboEffTga(String tgaIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIboEffTga = buildQuery("openCIboEffTga").bind("tgaIbOgg", tgaIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboEffTga() {
        return closeCursor(cIboEffTga);
    }

    public ITrchDiGar fetchCIboEffTga(ITrchDiGar iTrchDiGar) {
        return fetch(cIboEffTga, iTrchDiGar, selectByTgaDsRigaRm);
    }

    public ITrchDiGar selectRec3(int tgaIdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec3").bind("tgaIdTrchDiGar", tgaIdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public ITrchDiGar selectRec4(int tgaIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec4").bind("tgaIdPoli", tgaIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus openCIdpCpzTga(int tgaIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzTga = buildQuery("openCIdpCpzTga").bind("tgaIdPoli", tgaIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzTga() {
        return closeCursor(cIdpCpzTga);
    }

    public ITrchDiGar fetchCIdpCpzTga(ITrchDiGar iTrchDiGar) {
        return fetch(cIdpCpzTga, iTrchDiGar, selectByTgaDsRigaRm);
    }

    public ITrchDiGar selectRec5(String tgaIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec5").bind("tgaIbOgg", tgaIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus openCIboCpzTga(String tgaIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIboCpzTga = buildQuery("openCIboCpzTga").bind("tgaIbOgg", tgaIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboCpzTga() {
        return closeCursor(cIboCpzTga);
    }

    public ITrchDiGar fetchCIboCpzTga(ITrchDiGar iTrchDiGar) {
        return fetch(cIboCpzTga, iTrchDiGar, selectByTgaDsRigaRm);
    }

    public ITrchDiGar selectRec6(int ldbv0011IdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec6").bind("ldbv0011IdGar", ldbv0011IdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus openCEff(int ldbv0011IdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff = buildQuery("openCEff").bind("ldbv0011IdGar", ldbv0011IdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff() {
        return closeCursor(cEff);
    }

    public ITrchDiGar fetchCEff(ITrchDiGar iTrchDiGar) {
        return fetch(cEff, iTrchDiGar, selectByTgaDsRigaRm);
    }

    public ITrchDiGar selectRec7(int ldbv0011IdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec7").bind("ldbv0011IdGar", ldbv0011IdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus openCCpz(int ldbv0011IdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cCpz = buildQuery("openCCpz").bind("ldbv0011IdGar", ldbv0011IdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz() {
        return closeCursor(cCpz);
    }

    public ITrchDiGar fetchCCpz(ITrchDiGar iTrchDiGar) {
        return fetch(cCpz, iTrchDiGar, selectByTgaDsRigaRm);
    }

    public ITrchDiGar selectRec8(ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec8").bind(iTrchDiGar).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus openCEff44(ITrchDiGar iTrchDiGar) {
        cEff44 = buildQuery("openCEff44").bind(iTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff44() {
        return closeCursor(cEff44);
    }

    public ITrchDiGar fetchCEff44(ITrchDiGar iTrchDiGar) {
        return fetch(cEff44, iTrchDiGar, selectByTgaDsRigaRm);
    }

    public ITrchDiGar selectRec9(ITrchDiGar iTrchDiGar) {
        return buildQuery("selectRec9").bind(iTrchDiGar).rowMapper(selectByTgaDsRigaRm).singleResult(iTrchDiGar);
    }

    public DbAccessStatus openCCpz44(ITrchDiGar iTrchDiGar) {
        cCpz44 = buildQuery("openCCpz44").bind(iTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz44() {
        return closeCursor(cCpz44);
    }

    public ITrchDiGar fetchCCpz44(ITrchDiGar iTrchDiGar) {
        return fetch(cCpz44, iTrchDiGar, selectByTgaDsRigaRm);
    }
}
