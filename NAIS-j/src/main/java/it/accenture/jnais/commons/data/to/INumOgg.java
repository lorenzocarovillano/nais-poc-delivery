package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [NUM_OGG]
 * 
 */
public interface INumOgg extends BaseSqlTo {

    /**
     * Host Variable NOG-COD-COMPAGNIA-ANIA
     * 
     */
    int getCodCompagniaAnia();

    void setCodCompagniaAnia(int codCompagniaAnia);

    /**
     * Host Variable NOG-FORMA-ASSICURATIVA
     * 
     */
    String getFormaAssicurativa();

    void setFormaAssicurativa(String formaAssicurativa);

    /**
     * Host Variable NOG-COD-OGGETTO
     * 
     */
    String getCodOggetto();

    void setCodOggetto(String codOggetto);

    /**
     * Host Variable NOG-TIPO-OGGETTO
     * 
     */
    String getTipoOggetto();

    void setTipoOggetto(String tipoOggetto);

    /**
     * Host Variable NOG-ULT-PROGR
     * 
     */
    long getUltProgr();

    void setUltProgr(long ultProgr);

    /**
     * Nullable property for NOG-ULT-PROGR
     * 
     */
    Long getUltProgrObj();

    void setUltProgrObj(Long ultProgrObj);

    /**
     * Host Variable NOG-DESC-OGGETTO-VCHAR
     * 
     */
    String getDescOggettoVchar();

    void setDescOggettoVchar(String descOggettoVchar);
};
