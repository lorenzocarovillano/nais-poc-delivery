package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IPrev;

/**
 * Data Access Object(DAO) for table [PREV]
 * 
 */
public class PrevDao extends BaseSqlDao<IPrev> {

    private Cursor cIboNstL27;
    private Cursor cIdoNstL27;
    private final IRowMapper<IPrev> selectByL27IdPrevRm = buildNamedRowMapper(IPrev.class, "l27IdPrev", "codCompAnia", "statPrev", "idOggObj", "tpOggObj", "ibOgg", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public PrevDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IPrev> getToClass() {
        return IPrev.class;
    }

    public IPrev selectByL27IdPrev(int l27IdPrev, IPrev iPrev) {
        return buildQuery("selectByL27IdPrev").bind("l27IdPrev", l27IdPrev).rowMapper(selectByL27IdPrevRm).singleResult(iPrev);
    }

    public DbAccessStatus insertRec(IPrev iPrev) {
        return buildQuery("insertRec").bind(iPrev).executeInsert();
    }

    public DbAccessStatus updateRec(IPrev iPrev) {
        return buildQuery("updateRec").bind(iPrev).executeUpdate();
    }

    public DbAccessStatus deleteByL27IdPrev(int l27IdPrev) {
        return buildQuery("deleteByL27IdPrev").bind("l27IdPrev", l27IdPrev).executeDelete();
    }

    public IPrev selectRec(String l27IbOgg, int idsv0003CodiceCompagniaAnia, IPrev iPrev) {
        return buildQuery("selectRec").bind("l27IbOgg", l27IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByL27IdPrevRm).singleResult(iPrev);
    }

    public DbAccessStatus openCIboNstL27(String l27IbOgg, int idsv0003CodiceCompagniaAnia) {
        cIboNstL27 = buildQuery("openCIboNstL27").bind("l27IbOgg", l27IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboNstL27() {
        return closeCursor(cIboNstL27);
    }

    public IPrev fetchCIboNstL27(IPrev iPrev) {
        return fetch(cIboNstL27, iPrev, selectByL27IdPrevRm);
    }

    public IPrev selectRec1(int l27IdOgg, String l27TpOgg, int idsv0003CodiceCompagniaAnia, IPrev iPrev) {
        return buildQuery("selectRec1").bind("l27IdOgg", l27IdOgg).bind("l27TpOgg", l27TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByL27IdPrevRm).singleResult(iPrev);
    }

    public DbAccessStatus openCIdoNstL27(int l27IdOgg, String l27TpOgg, int idsv0003CodiceCompagniaAnia) {
        cIdoNstL27 = buildQuery("openCIdoNstL27").bind("l27IdOgg", l27IdOgg).bind("l27TpOgg", l27TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoNstL27() {
        return closeCursor(cIdoNstL27);
    }

    public IPrev fetchCIdoNstL27(IPrev iPrev) {
        return fetch(cIdoNstL27, iPrev, selectByL27IdPrevRm);
    }
}
