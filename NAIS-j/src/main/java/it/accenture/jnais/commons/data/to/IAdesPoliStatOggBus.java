package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ADES, POLI, STAT_OGG_BUS]
 * 
 */
public interface IAdesPoliStatOggBus extends BaseSqlTo {

    /**
     * Host Variable ADE-ID-ADES
     * 
     */
    int getAdeIdAdes();

    void setAdeIdAdes(int adeIdAdes);

    /**
     * Host Variable ADE-ID-POLI
     * 
     */
    int getAdeIdPoli();

    void setAdeIdPoli(int adeIdPoli);

    /**
     * Host Variable ADE-ID-MOVI-CRZ
     * 
     */
    int getAdeIdMoviCrz();

    void setAdeIdMoviCrz(int adeIdMoviCrz);

    /**
     * Host Variable ADE-ID-MOVI-CHIU
     * 
     */
    int getAdeIdMoviChiu();

    void setAdeIdMoviChiu(int adeIdMoviChiu);

    /**
     * Nullable property for ADE-ID-MOVI-CHIU
     * 
     */
    Integer getAdeIdMoviChiuObj();

    void setAdeIdMoviChiuObj(Integer adeIdMoviChiuObj);

    /**
     * Host Variable ADE-DT-INI-EFF-DB
     * 
     */
    String getAdeDtIniEffDb();

    void setAdeDtIniEffDb(String adeDtIniEffDb);

    /**
     * Host Variable ADE-DT-END-EFF-DB
     * 
     */
    String getAdeDtEndEffDb();

    void setAdeDtEndEffDb(String adeDtEndEffDb);

    /**
     * Host Variable ADE-IB-PREV
     * 
     */
    String getAdeIbPrev();

    void setAdeIbPrev(String adeIbPrev);

    /**
     * Nullable property for ADE-IB-PREV
     * 
     */
    String getAdeIbPrevObj();

    void setAdeIbPrevObj(String adeIbPrevObj);

    /**
     * Host Variable ADE-IB-OGG
     * 
     */
    String getAdeIbOgg();

    void setAdeIbOgg(String adeIbOgg);

    /**
     * Nullable property for ADE-IB-OGG
     * 
     */
    String getAdeIbOggObj();

    void setAdeIbOggObj(String adeIbOggObj);

    /**
     * Host Variable ADE-COD-COMP-ANIA
     * 
     */
    int getAdeCodCompAnia();

    void setAdeCodCompAnia(int adeCodCompAnia);

    /**
     * Host Variable ADE-DT-DECOR-DB
     * 
     */
    String getAdeDtDecorDb();

    void setAdeDtDecorDb(String adeDtDecorDb);

    /**
     * Nullable property for ADE-DT-DECOR-DB
     * 
     */
    String getAdeDtDecorDbObj();

    void setAdeDtDecorDbObj(String adeDtDecorDbObj);

    /**
     * Host Variable ADE-DT-SCAD-DB
     * 
     */
    String getAdeDtScadDb();

    void setAdeDtScadDb(String adeDtScadDb);

    /**
     * Nullable property for ADE-DT-SCAD-DB
     * 
     */
    String getAdeDtScadDbObj();

    void setAdeDtScadDbObj(String adeDtScadDbObj);

    /**
     * Host Variable ADE-ETA-A-SCAD
     * 
     */
    int getAdeEtaAScad();

    void setAdeEtaAScad(int adeEtaAScad);

    /**
     * Nullable property for ADE-ETA-A-SCAD
     * 
     */
    Integer getAdeEtaAScadObj();

    void setAdeEtaAScadObj(Integer adeEtaAScadObj);

    /**
     * Host Variable ADE-DUR-AA
     * 
     */
    int getAdeDurAa();

    void setAdeDurAa(int adeDurAa);

    /**
     * Nullable property for ADE-DUR-AA
     * 
     */
    Integer getAdeDurAaObj();

    void setAdeDurAaObj(Integer adeDurAaObj);

    /**
     * Host Variable ADE-DUR-MM
     * 
     */
    int getAdeDurMm();

    void setAdeDurMm(int adeDurMm);

    /**
     * Nullable property for ADE-DUR-MM
     * 
     */
    Integer getAdeDurMmObj();

    void setAdeDurMmObj(Integer adeDurMmObj);

    /**
     * Host Variable ADE-DUR-GG
     * 
     */
    int getAdeDurGg();

    void setAdeDurGg(int adeDurGg);

    /**
     * Nullable property for ADE-DUR-GG
     * 
     */
    Integer getAdeDurGgObj();

    void setAdeDurGgObj(Integer adeDurGgObj);

    /**
     * Host Variable ADE-TP-RGM-FISC
     * 
     */
    String getAdeTpRgmFisc();

    void setAdeTpRgmFisc(String adeTpRgmFisc);

    /**
     * Host Variable ADE-TP-RIAT
     * 
     */
    String getAdeTpRiat();

    void setAdeTpRiat(String adeTpRiat);

    /**
     * Nullable property for ADE-TP-RIAT
     * 
     */
    String getAdeTpRiatObj();

    void setAdeTpRiatObj(String adeTpRiatObj);

    /**
     * Host Variable ADE-TP-MOD-PAG-TIT
     * 
     */
    String getAdeTpModPagTit();

    void setAdeTpModPagTit(String adeTpModPagTit);

    /**
     * Host Variable ADE-TP-IAS
     * 
     */
    String getAdeTpIas();

    void setAdeTpIas(String adeTpIas);

    /**
     * Nullable property for ADE-TP-IAS
     * 
     */
    String getAdeTpIasObj();

    void setAdeTpIasObj(String adeTpIasObj);

    /**
     * Host Variable ADE-DT-VARZ-TP-IAS-DB
     * 
     */
    String getAdeDtVarzTpIasDb();

    void setAdeDtVarzTpIasDb(String adeDtVarzTpIasDb);

    /**
     * Nullable property for ADE-DT-VARZ-TP-IAS-DB
     * 
     */
    String getAdeDtVarzTpIasDbObj();

    void setAdeDtVarzTpIasDbObj(String adeDtVarzTpIasDbObj);

    /**
     * Host Variable ADE-PRE-NET-IND
     * 
     */
    AfDecimal getAdePreNetInd();

    void setAdePreNetInd(AfDecimal adePreNetInd);

    /**
     * Nullable property for ADE-PRE-NET-IND
     * 
     */
    AfDecimal getAdePreNetIndObj();

    void setAdePreNetIndObj(AfDecimal adePreNetIndObj);

    /**
     * Host Variable ADE-PRE-LRD-IND
     * 
     */
    AfDecimal getAdePreLrdInd();

    void setAdePreLrdInd(AfDecimal adePreLrdInd);

    /**
     * Nullable property for ADE-PRE-LRD-IND
     * 
     */
    AfDecimal getAdePreLrdIndObj();

    void setAdePreLrdIndObj(AfDecimal adePreLrdIndObj);

    /**
     * Host Variable ADE-RAT-LRD-IND
     * 
     */
    AfDecimal getAdeRatLrdInd();

    void setAdeRatLrdInd(AfDecimal adeRatLrdInd);

    /**
     * Nullable property for ADE-RAT-LRD-IND
     * 
     */
    AfDecimal getAdeRatLrdIndObj();

    void setAdeRatLrdIndObj(AfDecimal adeRatLrdIndObj);

    /**
     * Host Variable ADE-PRSTZ-INI-IND
     * 
     */
    AfDecimal getAdePrstzIniInd();

    void setAdePrstzIniInd(AfDecimal adePrstzIniInd);

    /**
     * Nullable property for ADE-PRSTZ-INI-IND
     * 
     */
    AfDecimal getAdePrstzIniIndObj();

    void setAdePrstzIniIndObj(AfDecimal adePrstzIniIndObj);

    /**
     * Host Variable ADE-FL-COINC-ASSTO
     * 
     */
    char getAdeFlCoincAssto();

    void setAdeFlCoincAssto(char adeFlCoincAssto);

    /**
     * Nullable property for ADE-FL-COINC-ASSTO
     * 
     */
    Character getAdeFlCoincAsstoObj();

    void setAdeFlCoincAsstoObj(Character adeFlCoincAsstoObj);

    /**
     * Host Variable ADE-IB-DFLT
     * 
     */
    String getAdeIbDflt();

    void setAdeIbDflt(String adeIbDflt);

    /**
     * Nullable property for ADE-IB-DFLT
     * 
     */
    String getAdeIbDfltObj();

    void setAdeIbDfltObj(String adeIbDfltObj);

    /**
     * Host Variable ADE-MOD-CALC
     * 
     */
    String getAdeModCalc();

    void setAdeModCalc(String adeModCalc);

    /**
     * Nullable property for ADE-MOD-CALC
     * 
     */
    String getAdeModCalcObj();

    void setAdeModCalcObj(String adeModCalcObj);

    /**
     * Host Variable ADE-TP-FNT-CNBTVA
     * 
     */
    String getAdeTpFntCnbtva();

    void setAdeTpFntCnbtva(String adeTpFntCnbtva);

    /**
     * Nullable property for ADE-TP-FNT-CNBTVA
     * 
     */
    String getAdeTpFntCnbtvaObj();

    void setAdeTpFntCnbtvaObj(String adeTpFntCnbtvaObj);

    /**
     * Host Variable ADE-IMP-AZ
     * 
     */
    AfDecimal getAdeImpAz();

    void setAdeImpAz(AfDecimal adeImpAz);

    /**
     * Nullable property for ADE-IMP-AZ
     * 
     */
    AfDecimal getAdeImpAzObj();

    void setAdeImpAzObj(AfDecimal adeImpAzObj);

    /**
     * Host Variable ADE-IMP-ADER
     * 
     */
    AfDecimal getAdeImpAder();

    void setAdeImpAder(AfDecimal adeImpAder);

    /**
     * Nullable property for ADE-IMP-ADER
     * 
     */
    AfDecimal getAdeImpAderObj();

    void setAdeImpAderObj(AfDecimal adeImpAderObj);

    /**
     * Host Variable ADE-IMP-TFR
     * 
     */
    AfDecimal getAdeImpTfr();

    void setAdeImpTfr(AfDecimal adeImpTfr);

    /**
     * Nullable property for ADE-IMP-TFR
     * 
     */
    AfDecimal getAdeImpTfrObj();

    void setAdeImpTfrObj(AfDecimal adeImpTfrObj);

    /**
     * Host Variable ADE-IMP-VOLO
     * 
     */
    AfDecimal getAdeImpVolo();

    void setAdeImpVolo(AfDecimal adeImpVolo);

    /**
     * Nullable property for ADE-IMP-VOLO
     * 
     */
    AfDecimal getAdeImpVoloObj();

    void setAdeImpVoloObj(AfDecimal adeImpVoloObj);

    /**
     * Host Variable ADE-PC-AZ
     * 
     */
    AfDecimal getAdePcAz();

    void setAdePcAz(AfDecimal adePcAz);

    /**
     * Nullable property for ADE-PC-AZ
     * 
     */
    AfDecimal getAdePcAzObj();

    void setAdePcAzObj(AfDecimal adePcAzObj);

    /**
     * Host Variable ADE-PC-ADER
     * 
     */
    AfDecimal getAdePcAder();

    void setAdePcAder(AfDecimal adePcAder);

    /**
     * Nullable property for ADE-PC-ADER
     * 
     */
    AfDecimal getAdePcAderObj();

    void setAdePcAderObj(AfDecimal adePcAderObj);

    /**
     * Host Variable ADE-PC-TFR
     * 
     */
    AfDecimal getAdePcTfr();

    void setAdePcTfr(AfDecimal adePcTfr);

    /**
     * Nullable property for ADE-PC-TFR
     * 
     */
    AfDecimal getAdePcTfrObj();

    void setAdePcTfrObj(AfDecimal adePcTfrObj);

    /**
     * Host Variable ADE-PC-VOLO
     * 
     */
    AfDecimal getAdePcVolo();

    void setAdePcVolo(AfDecimal adePcVolo);

    /**
     * Nullable property for ADE-PC-VOLO
     * 
     */
    AfDecimal getAdePcVoloObj();

    void setAdePcVoloObj(AfDecimal adePcVoloObj);

    /**
     * Host Variable ADE-DT-NOVA-RGM-FISC-DB
     * 
     */
    String getAdeDtNovaRgmFiscDb();

    void setAdeDtNovaRgmFiscDb(String adeDtNovaRgmFiscDb);

    /**
     * Nullable property for ADE-DT-NOVA-RGM-FISC-DB
     * 
     */
    String getAdeDtNovaRgmFiscDbObj();

    void setAdeDtNovaRgmFiscDbObj(String adeDtNovaRgmFiscDbObj);

    /**
     * Host Variable ADE-FL-ATTIV
     * 
     */
    char getAdeFlAttiv();

    void setAdeFlAttiv(char adeFlAttiv);

    /**
     * Nullable property for ADE-FL-ATTIV
     * 
     */
    Character getAdeFlAttivObj();

    void setAdeFlAttivObj(Character adeFlAttivObj);

    /**
     * Host Variable ADE-IMP-REC-RIT-VIS
     * 
     */
    AfDecimal getAdeImpRecRitVis();

    void setAdeImpRecRitVis(AfDecimal adeImpRecRitVis);

    /**
     * Nullable property for ADE-IMP-REC-RIT-VIS
     * 
     */
    AfDecimal getAdeImpRecRitVisObj();

    void setAdeImpRecRitVisObj(AfDecimal adeImpRecRitVisObj);

    /**
     * Host Variable ADE-IMP-REC-RIT-ACC
     * 
     */
    AfDecimal getAdeImpRecRitAcc();

    void setAdeImpRecRitAcc(AfDecimal adeImpRecRitAcc);

    /**
     * Nullable property for ADE-IMP-REC-RIT-ACC
     * 
     */
    AfDecimal getAdeImpRecRitAccObj();

    void setAdeImpRecRitAccObj(AfDecimal adeImpRecRitAccObj);

    /**
     * Host Variable ADE-FL-VARZ-STAT-TBGC
     * 
     */
    char getAdeFlVarzStatTbgc();

    void setAdeFlVarzStatTbgc(char adeFlVarzStatTbgc);

    /**
     * Nullable property for ADE-FL-VARZ-STAT-TBGC
     * 
     */
    Character getAdeFlVarzStatTbgcObj();

    void setAdeFlVarzStatTbgcObj(Character adeFlVarzStatTbgcObj);

    /**
     * Host Variable ADE-FL-PROVZA-MIGRAZ
     * 
     */
    char getAdeFlProvzaMigraz();

    void setAdeFlProvzaMigraz(char adeFlProvzaMigraz);

    /**
     * Nullable property for ADE-FL-PROVZA-MIGRAZ
     * 
     */
    Character getAdeFlProvzaMigrazObj();

    void setAdeFlProvzaMigrazObj(Character adeFlProvzaMigrazObj);

    /**
     * Host Variable ADE-IMPB-VIS-DA-REC
     * 
     */
    AfDecimal getAdeImpbVisDaRec();

    void setAdeImpbVisDaRec(AfDecimal adeImpbVisDaRec);

    /**
     * Nullable property for ADE-IMPB-VIS-DA-REC
     * 
     */
    AfDecimal getAdeImpbVisDaRecObj();

    void setAdeImpbVisDaRecObj(AfDecimal adeImpbVisDaRecObj);

    /**
     * Host Variable ADE-DT-DECOR-PREST-BAN-DB
     * 
     */
    String getAdeDtDecorPrestBanDb();

    void setAdeDtDecorPrestBanDb(String adeDtDecorPrestBanDb);

    /**
     * Nullable property for ADE-DT-DECOR-PREST-BAN-DB
     * 
     */
    String getAdeDtDecorPrestBanDbObj();

    void setAdeDtDecorPrestBanDbObj(String adeDtDecorPrestBanDbObj);

    /**
     * Host Variable ADE-DT-EFF-VARZ-STAT-T-DB
     * 
     */
    String getAdeDtEffVarzStatTDb();

    void setAdeDtEffVarzStatTDb(String adeDtEffVarzStatTDb);

    /**
     * Nullable property for ADE-DT-EFF-VARZ-STAT-T-DB
     * 
     */
    String getAdeDtEffVarzStatTDbObj();

    void setAdeDtEffVarzStatTDbObj(String adeDtEffVarzStatTDbObj);

    /**
     * Host Variable ADE-DS-RIGA
     * 
     */
    long getAdeDsRiga();

    void setAdeDsRiga(long adeDsRiga);

    /**
     * Host Variable ADE-DS-OPER-SQL
     * 
     */
    char getAdeDsOperSql();

    void setAdeDsOperSql(char adeDsOperSql);

    /**
     * Host Variable ADE-DS-VER
     * 
     */
    int getAdeDsVer();

    void setAdeDsVer(int adeDsVer);

    /**
     * Host Variable ADE-DS-TS-INI-CPTZ
     * 
     */
    long getAdeDsTsIniCptz();

    void setAdeDsTsIniCptz(long adeDsTsIniCptz);

    /**
     * Host Variable ADE-DS-TS-END-CPTZ
     * 
     */
    long getAdeDsTsEndCptz();

    void setAdeDsTsEndCptz(long adeDsTsEndCptz);

    /**
     * Host Variable ADE-DS-UTENTE
     * 
     */
    String getAdeDsUtente();

    void setAdeDsUtente(String adeDsUtente);

    /**
     * Host Variable ADE-DS-STATO-ELAB
     * 
     */
    char getAdeDsStatoElab();

    void setAdeDsStatoElab(char adeDsStatoElab);

    /**
     * Host Variable ADE-CUM-CNBT-CAP
     * 
     */
    AfDecimal getAdeCumCnbtCap();

    void setAdeCumCnbtCap(AfDecimal adeCumCnbtCap);

    /**
     * Nullable property for ADE-CUM-CNBT-CAP
     * 
     */
    AfDecimal getAdeCumCnbtCapObj();

    void setAdeCumCnbtCapObj(AfDecimal adeCumCnbtCapObj);

    /**
     * Host Variable ADE-IMP-GAR-CNBT
     * 
     */
    AfDecimal getAdeImpGarCnbt();

    void setAdeImpGarCnbt(AfDecimal adeImpGarCnbt);

    /**
     * Nullable property for ADE-IMP-GAR-CNBT
     * 
     */
    AfDecimal getAdeImpGarCnbtObj();

    void setAdeImpGarCnbtObj(AfDecimal adeImpGarCnbtObj);

    /**
     * Host Variable ADE-DT-ULT-CONS-CNBT-DB
     * 
     */
    String getAdeDtUltConsCnbtDb();

    void setAdeDtUltConsCnbtDb(String adeDtUltConsCnbtDb);

    /**
     * Nullable property for ADE-DT-ULT-CONS-CNBT-DB
     * 
     */
    String getAdeDtUltConsCnbtDbObj();

    void setAdeDtUltConsCnbtDbObj(String adeDtUltConsCnbtDbObj);

    /**
     * Host Variable ADE-IDEN-ISC-FND
     * 
     */
    String getAdeIdenIscFnd();

    void setAdeIdenIscFnd(String adeIdenIscFnd);

    /**
     * Nullable property for ADE-IDEN-ISC-FND
     * 
     */
    String getAdeIdenIscFndObj();

    void setAdeIdenIscFndObj(String adeIdenIscFndObj);

    /**
     * Host Variable ADE-NUM-RAT-PIAN
     * 
     */
    AfDecimal getAdeNumRatPian();

    void setAdeNumRatPian(AfDecimal adeNumRatPian);

    /**
     * Nullable property for ADE-NUM-RAT-PIAN
     * 
     */
    AfDecimal getAdeNumRatPianObj();

    void setAdeNumRatPianObj(AfDecimal adeNumRatPianObj);

    /**
     * Host Variable ADE-DT-PRESC-DB
     * 
     */
    String getAdeDtPrescDb();

    void setAdeDtPrescDb(String adeDtPrescDb);

    /**
     * Nullable property for ADE-DT-PRESC-DB
     * 
     */
    String getAdeDtPrescDbObj();

    void setAdeDtPrescDbObj(String adeDtPrescDbObj);

    /**
     * Host Variable ADE-CONCS-PREST
     * 
     */
    char getAdeConcsPrest();

    void setAdeConcsPrest(char adeConcsPrest);

    /**
     * Nullable property for ADE-CONCS-PREST
     * 
     */
    Character getAdeConcsPrestObj();

    void setAdeConcsPrestObj(Character adeConcsPrestObj);

    /**
     * Host Variable POL-ID-POLI
     * 
     */
    int getPolIdPoli();

    void setPolIdPoli(int polIdPoli);

    /**
     * Host Variable POL-ID-MOVI-CRZ
     * 
     */
    int getPolIdMoviCrz();

    void setPolIdMoviCrz(int polIdMoviCrz);

    /**
     * Host Variable POL-ID-MOVI-CHIU
     * 
     */
    int getPolIdMoviChiu();

    void setPolIdMoviChiu(int polIdMoviChiu);

    /**
     * Nullable property for POL-ID-MOVI-CHIU
     * 
     */
    Integer getPolIdMoviChiuObj();

    void setPolIdMoviChiuObj(Integer polIdMoviChiuObj);

    /**
     * Host Variable POL-IB-OGG
     * 
     */
    String getPolIbOgg();

    void setPolIbOgg(String polIbOgg);

    /**
     * Nullable property for POL-IB-OGG
     * 
     */
    String getPolIbOggObj();

    void setPolIbOggObj(String polIbOggObj);

    /**
     * Host Variable POL-IB-PROP
     * 
     */
    String getPolIbProp();

    void setPolIbProp(String polIbProp);

    /**
     * Host Variable POL-DT-PROP-DB
     * 
     */
    String getPolDtPropDb();

    void setPolDtPropDb(String polDtPropDb);

    /**
     * Nullable property for POL-DT-PROP-DB
     * 
     */
    String getPolDtPropDbObj();

    void setPolDtPropDbObj(String polDtPropDbObj);

    /**
     * Host Variable POL-DT-INI-EFF-DB
     * 
     */
    String getPolDtIniEffDb();

    void setPolDtIniEffDb(String polDtIniEffDb);

    /**
     * Host Variable POL-DT-END-EFF-DB
     * 
     */
    String getPolDtEndEffDb();

    void setPolDtEndEffDb(String polDtEndEffDb);

    /**
     * Host Variable POL-COD-COMP-ANIA
     * 
     */
    int getPolCodCompAnia();

    void setPolCodCompAnia(int polCodCompAnia);

    /**
     * Host Variable POL-DT-DECOR-DB
     * 
     */
    String getPolDtDecorDb();

    void setPolDtDecorDb(String polDtDecorDb);

    /**
     * Host Variable POL-DT-EMIS-DB
     * 
     */
    String getPolDtEmisDb();

    void setPolDtEmisDb(String polDtEmisDb);

    /**
     * Host Variable POL-TP-POLI
     * 
     */
    String getPolTpPoli();

    void setPolTpPoli(String polTpPoli);

    /**
     * Host Variable POL-DUR-AA
     * 
     */
    int getPolDurAa();

    void setPolDurAa(int polDurAa);

    /**
     * Nullable property for POL-DUR-AA
     * 
     */
    Integer getPolDurAaObj();

    void setPolDurAaObj(Integer polDurAaObj);

    /**
     * Host Variable POL-DUR-MM
     * 
     */
    int getPolDurMm();

    void setPolDurMm(int polDurMm);

    /**
     * Nullable property for POL-DUR-MM
     * 
     */
    Integer getPolDurMmObj();

    void setPolDurMmObj(Integer polDurMmObj);

    /**
     * Host Variable POL-DT-SCAD-DB
     * 
     */
    String getPolDtScadDb();

    void setPolDtScadDb(String polDtScadDb);

    /**
     * Nullable property for POL-DT-SCAD-DB
     * 
     */
    String getPolDtScadDbObj();

    void setPolDtScadDbObj(String polDtScadDbObj);

    /**
     * Host Variable POL-COD-PROD
     * 
     */
    String getPolCodProd();

    void setPolCodProd(String polCodProd);

    /**
     * Host Variable POL-DT-INI-VLDT-PROD-DB
     * 
     */
    String getPolDtIniVldtProdDb();

    void setPolDtIniVldtProdDb(String polDtIniVldtProdDb);

    /**
     * Host Variable POL-COD-CONV
     * 
     */
    String getPolCodConv();

    void setPolCodConv(String polCodConv);

    /**
     * Nullable property for POL-COD-CONV
     * 
     */
    String getPolCodConvObj();

    void setPolCodConvObj(String polCodConvObj);

    /**
     * Host Variable POL-COD-RAMO
     * 
     */
    String getPolCodRamo();

    void setPolCodRamo(String polCodRamo);

    /**
     * Nullable property for POL-COD-RAMO
     * 
     */
    String getPolCodRamoObj();

    void setPolCodRamoObj(String polCodRamoObj);

    /**
     * Host Variable POL-DT-INI-VLDT-CONV-DB
     * 
     */
    String getPolDtIniVldtConvDb();

    void setPolDtIniVldtConvDb(String polDtIniVldtConvDb);

    /**
     * Nullable property for POL-DT-INI-VLDT-CONV-DB
     * 
     */
    String getPolDtIniVldtConvDbObj();

    void setPolDtIniVldtConvDbObj(String polDtIniVldtConvDbObj);

    /**
     * Host Variable POL-DT-APPLZ-CONV-DB
     * 
     */
    String getPolDtApplzConvDb();

    void setPolDtApplzConvDb(String polDtApplzConvDb);

    /**
     * Nullable property for POL-DT-APPLZ-CONV-DB
     * 
     */
    String getPolDtApplzConvDbObj();

    void setPolDtApplzConvDbObj(String polDtApplzConvDbObj);

    /**
     * Host Variable POL-TP-FRM-ASSVA
     * 
     */
    String getPolTpFrmAssva();

    void setPolTpFrmAssva(String polTpFrmAssva);

    /**
     * Host Variable POL-TP-RGM-FISC
     * 
     */
    String getPolTpRgmFisc();

    void setPolTpRgmFisc(String polTpRgmFisc);

    /**
     * Nullable property for POL-TP-RGM-FISC
     * 
     */
    String getPolTpRgmFiscObj();

    void setPolTpRgmFiscObj(String polTpRgmFiscObj);

    /**
     * Host Variable POL-FL-ESTAS
     * 
     */
    char getPolFlEstas();

    void setPolFlEstas(char polFlEstas);

    /**
     * Nullable property for POL-FL-ESTAS
     * 
     */
    Character getPolFlEstasObj();

    void setPolFlEstasObj(Character polFlEstasObj);

    /**
     * Host Variable POL-FL-RSH-COMUN
     * 
     */
    char getPolFlRshComun();

    void setPolFlRshComun(char polFlRshComun);

    /**
     * Nullable property for POL-FL-RSH-COMUN
     * 
     */
    Character getPolFlRshComunObj();

    void setPolFlRshComunObj(Character polFlRshComunObj);

    /**
     * Host Variable POL-FL-RSH-COMUN-COND
     * 
     */
    char getPolFlRshComunCond();

    void setPolFlRshComunCond(char polFlRshComunCond);

    /**
     * Nullable property for POL-FL-RSH-COMUN-COND
     * 
     */
    Character getPolFlRshComunCondObj();

    void setPolFlRshComunCondObj(Character polFlRshComunCondObj);

    /**
     * Host Variable POL-TP-LIV-GENZ-TIT
     * 
     */
    String getPolTpLivGenzTit();

    void setPolTpLivGenzTit(String polTpLivGenzTit);

    /**
     * Host Variable POL-FL-COP-FINANZ
     * 
     */
    char getPolFlCopFinanz();

    void setPolFlCopFinanz(char polFlCopFinanz);

    /**
     * Nullable property for POL-FL-COP-FINANZ
     * 
     */
    Character getPolFlCopFinanzObj();

    void setPolFlCopFinanzObj(Character polFlCopFinanzObj);

    /**
     * Host Variable POL-TP-APPLZ-DIR
     * 
     */
    String getPolTpApplzDir();

    void setPolTpApplzDir(String polTpApplzDir);

    /**
     * Nullable property for POL-TP-APPLZ-DIR
     * 
     */
    String getPolTpApplzDirObj();

    void setPolTpApplzDirObj(String polTpApplzDirObj);

    /**
     * Host Variable POL-SPE-MED
     * 
     */
    AfDecimal getPolSpeMed();

    void setPolSpeMed(AfDecimal polSpeMed);

    /**
     * Nullable property for POL-SPE-MED
     * 
     */
    AfDecimal getPolSpeMedObj();

    void setPolSpeMedObj(AfDecimal polSpeMedObj);

    /**
     * Host Variable POL-DIR-EMIS
     * 
     */
    AfDecimal getPolDirEmis();

    void setPolDirEmis(AfDecimal polDirEmis);

    /**
     * Nullable property for POL-DIR-EMIS
     * 
     */
    AfDecimal getPolDirEmisObj();

    void setPolDirEmisObj(AfDecimal polDirEmisObj);

    /**
     * Host Variable POL-DIR-1O-VERS
     * 
     */
    AfDecimal getPolDir1oVers();

    void setPolDir1oVers(AfDecimal polDir1oVers);

    /**
     * Nullable property for POL-DIR-1O-VERS
     * 
     */
    AfDecimal getPolDir1oVersObj();

    void setPolDir1oVersObj(AfDecimal polDir1oVersObj);

    /**
     * Host Variable POL-DIR-VERS-AGG
     * 
     */
    AfDecimal getPolDirVersAgg();

    void setPolDirVersAgg(AfDecimal polDirVersAgg);

    /**
     * Nullable property for POL-DIR-VERS-AGG
     * 
     */
    AfDecimal getPolDirVersAggObj();

    void setPolDirVersAggObj(AfDecimal polDirVersAggObj);

    /**
     * Host Variable POL-COD-DVS
     * 
     */
    String getPolCodDvs();

    void setPolCodDvs(String polCodDvs);

    /**
     * Nullable property for POL-COD-DVS
     * 
     */
    String getPolCodDvsObj();

    void setPolCodDvsObj(String polCodDvsObj);

    /**
     * Host Variable POL-FL-FNT-AZ
     * 
     */
    char getPolFlFntAz();

    void setPolFlFntAz(char polFlFntAz);

    /**
     * Nullable property for POL-FL-FNT-AZ
     * 
     */
    Character getPolFlFntAzObj();

    void setPolFlFntAzObj(Character polFlFntAzObj);

    /**
     * Host Variable POL-FL-FNT-ADER
     * 
     */
    char getPolFlFntAder();

    void setPolFlFntAder(char polFlFntAder);

    /**
     * Nullable property for POL-FL-FNT-ADER
     * 
     */
    Character getPolFlFntAderObj();

    void setPolFlFntAderObj(Character polFlFntAderObj);

    /**
     * Host Variable POL-FL-FNT-TFR
     * 
     */
    char getPolFlFntTfr();

    void setPolFlFntTfr(char polFlFntTfr);

    /**
     * Nullable property for POL-FL-FNT-TFR
     * 
     */
    Character getPolFlFntTfrObj();

    void setPolFlFntTfrObj(Character polFlFntTfrObj);

    /**
     * Host Variable POL-FL-FNT-VOLO
     * 
     */
    char getPolFlFntVolo();

    void setPolFlFntVolo(char polFlFntVolo);

    /**
     * Nullable property for POL-FL-FNT-VOLO
     * 
     */
    Character getPolFlFntVoloObj();

    void setPolFlFntVoloObj(Character polFlFntVoloObj);

    /**
     * Host Variable POL-TP-OPZ-A-SCAD
     * 
     */
    String getPolTpOpzAScad();

    void setPolTpOpzAScad(String polTpOpzAScad);

    /**
     * Nullable property for POL-TP-OPZ-A-SCAD
     * 
     */
    String getPolTpOpzAScadObj();

    void setPolTpOpzAScadObj(String polTpOpzAScadObj);

    /**
     * Host Variable POL-AA-DIFF-PROR-DFLT
     * 
     */
    int getPolAaDiffProrDflt();

    void setPolAaDiffProrDflt(int polAaDiffProrDflt);

    /**
     * Nullable property for POL-AA-DIFF-PROR-DFLT
     * 
     */
    Integer getPolAaDiffProrDfltObj();

    void setPolAaDiffProrDfltObj(Integer polAaDiffProrDfltObj);

    /**
     * Host Variable POL-FL-VER-PROD
     * 
     */
    String getPolFlVerProd();

    void setPolFlVerProd(String polFlVerProd);

    /**
     * Nullable property for POL-FL-VER-PROD
     * 
     */
    String getPolFlVerProdObj();

    void setPolFlVerProdObj(String polFlVerProdObj);

    /**
     * Host Variable POL-DUR-GG
     * 
     */
    int getPolDurGg();

    void setPolDurGg(int polDurGg);

    /**
     * Nullable property for POL-DUR-GG
     * 
     */
    Integer getPolDurGgObj();

    void setPolDurGgObj(Integer polDurGgObj);

    /**
     * Host Variable POL-DIR-QUIET
     * 
     */
    AfDecimal getPolDirQuiet();

    void setPolDirQuiet(AfDecimal polDirQuiet);

    /**
     * Nullable property for POL-DIR-QUIET
     * 
     */
    AfDecimal getPolDirQuietObj();

    void setPolDirQuietObj(AfDecimal polDirQuietObj);

    /**
     * Host Variable POL-TP-PTF-ESTNO
     * 
     */
    String getPolTpPtfEstno();

    void setPolTpPtfEstno(String polTpPtfEstno);

    /**
     * Nullable property for POL-TP-PTF-ESTNO
     * 
     */
    String getPolTpPtfEstnoObj();

    void setPolTpPtfEstnoObj(String polTpPtfEstnoObj);

    /**
     * Host Variable POL-FL-CUM-PRE-CNTR
     * 
     */
    char getPolFlCumPreCntr();

    void setPolFlCumPreCntr(char polFlCumPreCntr);

    /**
     * Nullable property for POL-FL-CUM-PRE-CNTR
     * 
     */
    Character getPolFlCumPreCntrObj();

    void setPolFlCumPreCntrObj(Character polFlCumPreCntrObj);

    /**
     * Host Variable POL-FL-AMMB-MOVI
     * 
     */
    char getPolFlAmmbMovi();

    void setPolFlAmmbMovi(char polFlAmmbMovi);

    /**
     * Nullable property for POL-FL-AMMB-MOVI
     * 
     */
    Character getPolFlAmmbMoviObj();

    void setPolFlAmmbMoviObj(Character polFlAmmbMoviObj);

    /**
     * Host Variable POL-CONV-GECO
     * 
     */
    String getPolConvGeco();

    void setPolConvGeco(String polConvGeco);

    /**
     * Nullable property for POL-CONV-GECO
     * 
     */
    String getPolConvGecoObj();

    void setPolConvGecoObj(String polConvGecoObj);

    /**
     * Host Variable POL-DS-RIGA
     * 
     */
    long getPolDsRiga();

    void setPolDsRiga(long polDsRiga);

    /**
     * Host Variable POL-DS-OPER-SQL
     * 
     */
    char getPolDsOperSql();

    void setPolDsOperSql(char polDsOperSql);

    /**
     * Host Variable POL-DS-VER
     * 
     */
    int getPolDsVer();

    void setPolDsVer(int polDsVer);

    /**
     * Host Variable POL-DS-TS-INI-CPTZ
     * 
     */
    long getPolDsTsIniCptz();

    void setPolDsTsIniCptz(long polDsTsIniCptz);

    /**
     * Host Variable POL-DS-TS-END-CPTZ
     * 
     */
    long getPolDsTsEndCptz();

    void setPolDsTsEndCptz(long polDsTsEndCptz);

    /**
     * Host Variable POL-DS-UTENTE
     * 
     */
    String getPolDsUtente();

    void setPolDsUtente(String polDsUtente);

    /**
     * Host Variable POL-DS-STATO-ELAB
     * 
     */
    char getPolDsStatoElab();

    void setPolDsStatoElab(char polDsStatoElab);

    /**
     * Host Variable POL-FL-SCUDO-FISC
     * 
     */
    char getPolFlScudoFisc();

    void setPolFlScudoFisc(char polFlScudoFisc);

    /**
     * Nullable property for POL-FL-SCUDO-FISC
     * 
     */
    Character getPolFlScudoFiscObj();

    void setPolFlScudoFiscObj(Character polFlScudoFiscObj);

    /**
     * Host Variable POL-FL-TRASFE
     * 
     */
    char getPolFlTrasfe();

    void setPolFlTrasfe(char polFlTrasfe);

    /**
     * Nullable property for POL-FL-TRASFE
     * 
     */
    Character getPolFlTrasfeObj();

    void setPolFlTrasfeObj(Character polFlTrasfeObj);

    /**
     * Host Variable POL-FL-TFR-STRC
     * 
     */
    char getPolFlTfrStrc();

    void setPolFlTfrStrc(char polFlTfrStrc);

    /**
     * Nullable property for POL-FL-TFR-STRC
     * 
     */
    Character getPolFlTfrStrcObj();

    void setPolFlTfrStrcObj(Character polFlTfrStrcObj);

    /**
     * Host Variable POL-DT-PRESC-DB
     * 
     */
    String getPolDtPrescDb();

    void setPolDtPrescDb(String polDtPrescDb);

    /**
     * Nullable property for POL-DT-PRESC-DB
     * 
     */
    String getPolDtPrescDbObj();

    void setPolDtPrescDbObj(String polDtPrescDbObj);

    /**
     * Host Variable POL-COD-CONV-AGG
     * 
     */
    String getPolCodConvAgg();

    void setPolCodConvAgg(String polCodConvAgg);

    /**
     * Nullable property for POL-COD-CONV-AGG
     * 
     */
    String getPolCodConvAggObj();

    void setPolCodConvAggObj(String polCodConvAggObj);

    /**
     * Host Variable POL-SUBCAT-PROD
     * 
     */
    String getPolSubcatProd();

    void setPolSubcatProd(String polSubcatProd);

    /**
     * Nullable property for POL-SUBCAT-PROD
     * 
     */
    String getPolSubcatProdObj();

    void setPolSubcatProdObj(String polSubcatProdObj);

    /**
     * Host Variable POL-FL-QUEST-ADEGZ-ASS
     * 
     */
    char getPolFlQuestAdegzAss();

    void setPolFlQuestAdegzAss(char polFlQuestAdegzAss);

    /**
     * Nullable property for POL-FL-QUEST-ADEGZ-ASS
     * 
     */
    Character getPolFlQuestAdegzAssObj();

    void setPolFlQuestAdegzAssObj(Character polFlQuestAdegzAssObj);

    /**
     * Host Variable POL-COD-TPA
     * 
     */
    String getPolCodTpa();

    void setPolCodTpa(String polCodTpa);

    /**
     * Nullable property for POL-COD-TPA
     * 
     */
    String getPolCodTpaObj();

    void setPolCodTpaObj(String polCodTpaObj);

    /**
     * Host Variable POL-ID-ACC-COMM
     * 
     */
    int getPolIdAccComm();

    void setPolIdAccComm(int polIdAccComm);

    /**
     * Nullable property for POL-ID-ACC-COMM
     * 
     */
    Integer getPolIdAccCommObj();

    void setPolIdAccCommObj(Integer polIdAccCommObj);

    /**
     * Host Variable POL-FL-POLI-CPI-PR
     * 
     */
    char getPolFlPoliCpiPr();

    void setPolFlPoliCpiPr(char polFlPoliCpiPr);

    /**
     * Nullable property for POL-FL-POLI-CPI-PR
     * 
     */
    Character getPolFlPoliCpiPrObj();

    void setPolFlPoliCpiPrObj(Character polFlPoliCpiPrObj);

    /**
     * Host Variable POL-FL-POLI-BUNDLING
     * 
     */
    char getPolFlPoliBundling();

    void setPolFlPoliBundling(char polFlPoliBundling);

    /**
     * Nullable property for POL-FL-POLI-BUNDLING
     * 
     */
    Character getPolFlPoliBundlingObj();

    void setPolFlPoliBundlingObj(Character polFlPoliBundlingObj);

    /**
     * Host Variable POL-IND-POLI-PRIN-COLL
     * 
     */
    char getPolIndPoliPrinColl();

    void setPolIndPoliPrinColl(char polIndPoliPrinColl);

    /**
     * Nullable property for POL-IND-POLI-PRIN-COLL
     * 
     */
    Character getPolIndPoliPrinCollObj();

    void setPolIndPoliPrinCollObj(Character polIndPoliPrinCollObj);

    /**
     * Host Variable POL-FL-VND-BUNDLE
     * 
     */
    char getPolFlVndBundle();

    void setPolFlVndBundle(char polFlVndBundle);

    /**
     * Nullable property for POL-FL-VND-BUNDLE
     * 
     */
    Character getPolFlVndBundleObj();

    void setPolFlVndBundleObj(Character polFlVndBundleObj);

    /**
     * Host Variable POL-IB-BS
     * 
     */
    String getPolIbBs();

    void setPolIbBs(String polIbBs);

    /**
     * Nullable property for POL-IB-BS
     * 
     */
    String getPolIbBsObj();

    void setPolIbBsObj(String polIbBsObj);

    /**
     * Host Variable POL-FL-POLI-IFP
     * 
     */
    char getPolFlPoliIfp();

    void setPolFlPoliIfp(char polFlPoliIfp);

    /**
     * Nullable property for POL-FL-POLI-IFP
     * 
     */
    Character getPolFlPoliIfpObj();

    void setPolFlPoliIfpObj(Character polFlPoliIfpObj);

    /**
     * Host Variable STB-ID-STAT-OGG-BUS
     * 
     */
    int getStbIdStatOggBus();

    void setStbIdStatOggBus(int stbIdStatOggBus);

    /**
     * Host Variable STB-ID-OGG
     * 
     */
    int getStbIdOgg();

    void setStbIdOgg(int stbIdOgg);

    /**
     * Host Variable STB-TP-OGG
     * 
     */
    String getStbTpOgg();

    void setStbTpOgg(String stbTpOgg);

    /**
     * Host Variable STB-ID-MOVI-CRZ
     * 
     */
    int getStbIdMoviCrz();

    void setStbIdMoviCrz(int stbIdMoviCrz);

    /**
     * Host Variable STB-ID-MOVI-CHIU
     * 
     */
    int getStbIdMoviChiu();

    void setStbIdMoviChiu(int stbIdMoviChiu);

    /**
     * Nullable property for STB-ID-MOVI-CHIU
     * 
     */
    Integer getStbIdMoviChiuObj();

    void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj);

    /**
     * Host Variable STB-DT-INI-EFF-DB
     * 
     */
    String getStbDtIniEffDb();

    void setStbDtIniEffDb(String stbDtIniEffDb);

    /**
     * Host Variable STB-DT-END-EFF-DB
     * 
     */
    String getStbDtEndEffDb();

    void setStbDtEndEffDb(String stbDtEndEffDb);

    /**
     * Host Variable STB-COD-COMP-ANIA
     * 
     */
    int getStbCodCompAnia();

    void setStbCodCompAnia(int stbCodCompAnia);

    /**
     * Host Variable STB-TP-STAT-BUS
     * 
     */
    String getStbTpStatBus();

    void setStbTpStatBus(String stbTpStatBus);

    /**
     * Host Variable STB-TP-CAUS
     * 
     */
    String getStbTpCaus();

    void setStbTpCaus(String stbTpCaus);

    /**
     * Host Variable STB-DS-RIGA
     * 
     */
    long getStbDsRiga();

    void setStbDsRiga(long stbDsRiga);

    /**
     * Host Variable STB-DS-OPER-SQL
     * 
     */
    char getStbDsOperSql();

    void setStbDsOperSql(char stbDsOperSql);

    /**
     * Host Variable STB-DS-VER
     * 
     */
    int getStbDsVer();

    void setStbDsVer(int stbDsVer);

    /**
     * Host Variable STB-DS-TS-INI-CPTZ
     * 
     */
    long getStbDsTsIniCptz();

    void setStbDsTsIniCptz(long stbDsTsIniCptz);

    /**
     * Host Variable STB-DS-TS-END-CPTZ
     * 
     */
    long getStbDsTsEndCptz();

    void setStbDsTsEndCptz(long stbDsTsEndCptz);

    /**
     * Host Variable STB-DS-UTENTE
     * 
     */
    String getStbDsUtente();

    void setStbDsUtente(String stbDsUtente);

    /**
     * Host Variable STB-DS-STATO-ELAB
     * 
     */
    char getStbDsStatoElab();

    void setStbDsStatoElab(char stbDsStatoElab);

    /**
     * Host Variable LC821-TP-FRM-ASSVA1
     * 
     */
    String getLc821TpFrmAssva1();

    void setLc821TpFrmAssva1(String lc821TpFrmAssva1);

    /**
     * Host Variable LC821-TP-FRM-ASSVA2
     * 
     */
    String getLc821TpFrmAssva2();

    void setLc821TpFrmAssva2(String lc821TpFrmAssva2);

    /**
     * Host Variable LC821-ID-POLI
     * 
     */
    int getLc821IdPoli();

    void setLc821IdPoli(int lc821IdPoli);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);
};
