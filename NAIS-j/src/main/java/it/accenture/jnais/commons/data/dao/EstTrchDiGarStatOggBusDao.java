package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IEstTrchDiGarStatOggBus;

/**
 * Data Access Object(DAO) for tables [EST_TRCH_DI_GAR, STAT_OGG_BUS]
 * 
 */
public class EstTrchDiGarStatOggBusDao extends BaseSqlDao<IEstTrchDiGarStatOggBus> {

    private final IRowMapper<IEstTrchDiGarStatOggBus> selectRec2Rm = buildNamedRowMapper(IEstTrchDiGarStatOggBus.class, "ldbv7121CumPreAttObj");

    public EstTrchDiGarStatOggBusDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IEstTrchDiGarStatOggBus> getToClass() {
        return IEstTrchDiGarStatOggBus.class;
    }

    public AfDecimal selectRec(IEstTrchDiGarStatOggBus iEstTrchDiGarStatOggBus, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec").bind(iEstTrchDiGarStatOggBus).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec1(IEstTrchDiGarStatOggBus iEstTrchDiGarStatOggBus, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec1").bind(iEstTrchDiGarStatOggBus).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public IEstTrchDiGarStatOggBus selectRec2(IEstTrchDiGarStatOggBus iEstTrchDiGarStatOggBus) {
        return buildQuery("selectRec2").bind(iEstTrchDiGarStatOggBus).rowMapper(selectRec2Rm).singleResult(iEstTrchDiGarStatOggBus);
    }

    public IEstTrchDiGarStatOggBus selectRec3(IEstTrchDiGarStatOggBus iEstTrchDiGarStatOggBus) {
        return buildQuery("selectRec3").bind(iEstTrchDiGarStatOggBus).rowMapper(selectRec2Rm).singleResult(iEstTrchDiGarStatOggBus);
    }
}
