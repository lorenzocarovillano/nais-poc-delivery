package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [QUEST]
 * 
 */
public interface IQuest extends BaseSqlTo {

    /**
     * Host Variable QUE-ID-QUEST
     * 
     */
    int getIdQuest();

    void setIdQuest(int idQuest);

    /**
     * Host Variable QUE-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Host Variable QUE-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable QUE-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for QUE-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable QUE-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable QUE-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable QUE-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable QUE-COD-QUEST
     * 
     */
    String getCodQuest();

    void setCodQuest(String codQuest);

    /**
     * Host Variable QUE-TP-QUEST
     * 
     */
    String getTpQuest();

    void setTpQuest(String tpQuest);

    /**
     * Host Variable QUE-FL-VST-MED
     * 
     */
    char getFlVstMed();

    void setFlVstMed(char flVstMed);

    /**
     * Nullable property for QUE-FL-VST-MED
     * 
     */
    Character getFlVstMedObj();

    void setFlVstMedObj(Character flVstMedObj);

    /**
     * Host Variable QUE-FL-STAT-BUON-SAL
     * 
     */
    char getFlStatBuonSal();

    void setFlStatBuonSal(char flStatBuonSal);

    /**
     * Nullable property for QUE-FL-STAT-BUON-SAL
     * 
     */
    Character getFlStatBuonSalObj();

    void setFlStatBuonSalObj(Character flStatBuonSalObj);

    /**
     * Host Variable QUE-DS-RIGA
     * 
     */
    long getQueDsRiga();

    void setQueDsRiga(long queDsRiga);

    /**
     * Host Variable QUE-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable QUE-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable QUE-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable QUE-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable QUE-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable QUE-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable QUE-TP-ADEGZ
     * 
     */
    String getTpAdegz();

    void setTpAdegz(String tpAdegz);

    /**
     * Nullable property for QUE-TP-ADEGZ
     * 
     */
    String getTpAdegzObj();

    void setTpAdegzObj(String tpAdegzObj);
};
