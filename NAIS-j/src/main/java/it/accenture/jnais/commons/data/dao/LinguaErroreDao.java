package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ILinguaErrore;

/**
 * Data Access Object(DAO) for table [LINGUA_ERRORE]
 * 
 */
public class LinguaErroreDao extends BaseSqlDao<ILinguaErrore> {

    private final IRowMapper<ILinguaErrore> selectRecRm = buildNamedRowMapper(ILinguaErrore.class, "breveVchar", "estesaVchar");

    public LinguaErroreDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ILinguaErrore> getToClass() {
        return ILinguaErrore.class;
    }

    public ILinguaErrore selectRec(int gerCodErrore, int gerCodCompagniaAnia, String lerLingua, String lerPaese, ILinguaErrore iLinguaErrore) {
        return buildQuery("selectRec").bind("gerCodErrore", gerCodErrore).bind("gerCodCompagniaAnia", gerCodCompagniaAnia).bind("lerLingua", lerLingua).bind("lerPaese", lerPaese).rowMapper(selectRecRm).singleResult(iLinguaErrore);
    }
}
