package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [TCONT_LIQ]
 * 
 */
public interface ITcontLiq extends BaseSqlTo {

    /**
     * Host Variable TCL-ID-TCONT-LIQ
     * 
     */
    int getIdTcontLiq();

    void setIdTcontLiq(int idTcontLiq);

    /**
     * Host Variable TCL-ID-PERC-LIQ
     * 
     */
    int getIdPercLiq();

    void setIdPercLiq(int idPercLiq);

    /**
     * Host Variable TCL-ID-BNFICR-LIQ
     * 
     */
    int getIdBnficrLiq();

    void setIdBnficrLiq(int idBnficrLiq);

    /**
     * Host Variable TCL-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable TCL-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for TCL-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable TCL-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable TCL-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable TCL-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable TCL-DT-VLT-DB
     * 
     */
    String getDtVltDb();

    void setDtVltDb(String dtVltDb);

    /**
     * Nullable property for TCL-DT-VLT-DB
     * 
     */
    String getDtVltDbObj();

    void setDtVltDbObj(String dtVltDbObj);

    /**
     * Host Variable TCL-IMP-LRD-LIQTO
     * 
     */
    AfDecimal getImpLrdLiqto();

    void setImpLrdLiqto(AfDecimal impLrdLiqto);

    /**
     * Nullable property for TCL-IMP-LRD-LIQTO
     * 
     */
    AfDecimal getImpLrdLiqtoObj();

    void setImpLrdLiqtoObj(AfDecimal impLrdLiqtoObj);

    /**
     * Host Variable TCL-IMP-PREST
     * 
     */
    AfDecimal getImpPrest();

    void setImpPrest(AfDecimal impPrest);

    /**
     * Nullable property for TCL-IMP-PREST
     * 
     */
    AfDecimal getImpPrestObj();

    void setImpPrestObj(AfDecimal impPrestObj);

    /**
     * Host Variable TCL-IMP-INTR-PREST
     * 
     */
    AfDecimal getImpIntrPrest();

    void setImpIntrPrest(AfDecimal impIntrPrest);

    /**
     * Nullable property for TCL-IMP-INTR-PREST
     * 
     */
    AfDecimal getImpIntrPrestObj();

    void setImpIntrPrestObj(AfDecimal impIntrPrestObj);

    /**
     * Host Variable TCL-IMP-RAT
     * 
     */
    AfDecimal getImpRat();

    void setImpRat(AfDecimal impRat);

    /**
     * Nullable property for TCL-IMP-RAT
     * 
     */
    AfDecimal getImpRatObj();

    void setImpRatObj(AfDecimal impRatObj);

    /**
     * Host Variable TCL-IMP-UTI
     * 
     */
    AfDecimal getImpUti();

    void setImpUti(AfDecimal impUti);

    /**
     * Nullable property for TCL-IMP-UTI
     * 
     */
    AfDecimal getImpUtiObj();

    void setImpUtiObj(AfDecimal impUtiObj);

    /**
     * Host Variable TCL-IMP-RIT-TFR
     * 
     */
    AfDecimal getImpRitTfr();

    void setImpRitTfr(AfDecimal impRitTfr);

    /**
     * Nullable property for TCL-IMP-RIT-TFR
     * 
     */
    AfDecimal getImpRitTfrObj();

    void setImpRitTfrObj(AfDecimal impRitTfrObj);

    /**
     * Host Variable TCL-IMP-RIT-ACC
     * 
     */
    AfDecimal getImpRitAcc();

    void setImpRitAcc(AfDecimal impRitAcc);

    /**
     * Nullable property for TCL-IMP-RIT-ACC
     * 
     */
    AfDecimal getImpRitAccObj();

    void setImpRitAccObj(AfDecimal impRitAccObj);

    /**
     * Host Variable TCL-IMP-RIT-VIS
     * 
     */
    AfDecimal getImpRitVis();

    void setImpRitVis(AfDecimal impRitVis);

    /**
     * Nullable property for TCL-IMP-RIT-VIS
     * 
     */
    AfDecimal getImpRitVisObj();

    void setImpRitVisObj(AfDecimal impRitVisObj);

    /**
     * Host Variable TCL-IMPB-TFR
     * 
     */
    AfDecimal getImpbTfr();

    void setImpbTfr(AfDecimal impbTfr);

    /**
     * Nullable property for TCL-IMPB-TFR
     * 
     */
    AfDecimal getImpbTfrObj();

    void setImpbTfrObj(AfDecimal impbTfrObj);

    /**
     * Host Variable TCL-IMPB-ACC
     * 
     */
    AfDecimal getImpbAcc();

    void setImpbAcc(AfDecimal impbAcc);

    /**
     * Nullable property for TCL-IMPB-ACC
     * 
     */
    AfDecimal getImpbAccObj();

    void setImpbAccObj(AfDecimal impbAccObj);

    /**
     * Host Variable TCL-IMPB-VIS
     * 
     */
    AfDecimal getImpbVis();

    void setImpbVis(AfDecimal impbVis);

    /**
     * Nullable property for TCL-IMPB-VIS
     * 
     */
    AfDecimal getImpbVisObj();

    void setImpbVisObj(AfDecimal impbVisObj);

    /**
     * Host Variable TCL-IMP-RIMB
     * 
     */
    AfDecimal getImpRimb();

    void setImpRimb(AfDecimal impRimb);

    /**
     * Nullable property for TCL-IMP-RIMB
     * 
     */
    AfDecimal getImpRimbObj();

    void setImpRimbObj(AfDecimal impRimbObj);

    /**
     * Host Variable TCL-IMP-CORTVO
     * 
     */
    AfDecimal getImpCortvo();

    void setImpCortvo(AfDecimal impCortvo);

    /**
     * Nullable property for TCL-IMP-CORTVO
     * 
     */
    AfDecimal getImpCortvoObj();

    void setImpCortvoObj(AfDecimal impCortvoObj);

    /**
     * Host Variable TCL-IMPB-IMPST-PRVR
     * 
     */
    AfDecimal getImpbImpstPrvr();

    void setImpbImpstPrvr(AfDecimal impbImpstPrvr);

    /**
     * Nullable property for TCL-IMPB-IMPST-PRVR
     * 
     */
    AfDecimal getImpbImpstPrvrObj();

    void setImpbImpstPrvrObj(AfDecimal impbImpstPrvrObj);

    /**
     * Host Variable TCL-IMPST-PRVR
     * 
     */
    AfDecimal getImpstPrvr();

    void setImpstPrvr(AfDecimal impstPrvr);

    /**
     * Nullable property for TCL-IMPST-PRVR
     * 
     */
    AfDecimal getImpstPrvrObj();

    void setImpstPrvrObj(AfDecimal impstPrvrObj);

    /**
     * Host Variable TCL-IMPB-IMPST-252
     * 
     */
    AfDecimal getImpbImpst252();

    void setImpbImpst252(AfDecimal impbImpst252);

    /**
     * Nullable property for TCL-IMPB-IMPST-252
     * 
     */
    AfDecimal getImpbImpst252Obj();

    void setImpbImpst252Obj(AfDecimal impbImpst252Obj);

    /**
     * Host Variable TCL-IMPST-252
     * 
     */
    AfDecimal getImpst252();

    void setImpst252(AfDecimal impst252);

    /**
     * Nullable property for TCL-IMPST-252
     * 
     */
    AfDecimal getImpst252Obj();

    void setImpst252Obj(AfDecimal impst252Obj);

    /**
     * Host Variable TCL-IMP-IS
     * 
     */
    AfDecimal getImpIs();

    void setImpIs(AfDecimal impIs);

    /**
     * Nullable property for TCL-IMP-IS
     * 
     */
    AfDecimal getImpIsObj();

    void setImpIsObj(AfDecimal impIsObj);

    /**
     * Host Variable TCL-IMP-DIR-LIQ
     * 
     */
    AfDecimal getImpDirLiq();

    void setImpDirLiq(AfDecimal impDirLiq);

    /**
     * Nullable property for TCL-IMP-DIR-LIQ
     * 
     */
    AfDecimal getImpDirLiqObj();

    void setImpDirLiqObj(AfDecimal impDirLiqObj);

    /**
     * Host Variable TCL-IMP-NET-LIQTO
     * 
     */
    AfDecimal getImpNetLiqto();

    void setImpNetLiqto(AfDecimal impNetLiqto);

    /**
     * Nullable property for TCL-IMP-NET-LIQTO
     * 
     */
    AfDecimal getImpNetLiqtoObj();

    void setImpNetLiqtoObj(AfDecimal impNetLiqtoObj);

    /**
     * Host Variable TCL-IMP-EFFLQ
     * 
     */
    AfDecimal getImpEfflq();

    void setImpEfflq(AfDecimal impEfflq);

    /**
     * Nullable property for TCL-IMP-EFFLQ
     * 
     */
    AfDecimal getImpEfflqObj();

    void setImpEfflqObj(AfDecimal impEfflqObj);

    /**
     * Host Variable TCL-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for TCL-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable TCL-TP-MEZ-PAG-ACCR
     * 
     */
    String getTpMezPagAccr();

    void setTpMezPagAccr(String tpMezPagAccr);

    /**
     * Nullable property for TCL-TP-MEZ-PAG-ACCR
     * 
     */
    String getTpMezPagAccrObj();

    void setTpMezPagAccrObj(String tpMezPagAccrObj);

    /**
     * Host Variable TCL-ESTR-CNT-CORR-ACCR
     * 
     */
    String getEstrCntCorrAccr();

    void setEstrCntCorrAccr(String estrCntCorrAccr);

    /**
     * Nullable property for TCL-ESTR-CNT-CORR-ACCR
     * 
     */
    String getEstrCntCorrAccrObj();

    void setEstrCntCorrAccrObj(String estrCntCorrAccrObj);

    /**
     * Host Variable TCL-DS-RIGA
     * 
     */
    long getTclDsRiga();

    void setTclDsRiga(long tclDsRiga);

    /**
     * Host Variable TCL-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable TCL-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable TCL-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable TCL-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable TCL-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable TCL-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable TCL-TP-STAT-TIT
     * 
     */
    String getTpStatTit();

    void setTpStatTit(String tpStatTit);

    /**
     * Nullable property for TCL-TP-STAT-TIT
     * 
     */
    String getTpStatTitObj();

    void setTpStatTitObj(String tpStatTitObj);

    /**
     * Host Variable TCL-IMPB-VIS-1382011
     * 
     */
    AfDecimal getImpbVis1382011();

    void setImpbVis1382011(AfDecimal impbVis1382011);

    /**
     * Nullable property for TCL-IMPB-VIS-1382011
     * 
     */
    AfDecimal getImpbVis1382011Obj();

    void setImpbVis1382011Obj(AfDecimal impbVis1382011Obj);

    /**
     * Host Variable TCL-IMPST-VIS-1382011
     * 
     */
    AfDecimal getImpstVis1382011();

    void setImpstVis1382011(AfDecimal impstVis1382011);

    /**
     * Nullable property for TCL-IMPST-VIS-1382011
     * 
     */
    AfDecimal getImpstVis1382011Obj();

    void setImpstVis1382011Obj(AfDecimal impstVis1382011Obj);

    /**
     * Host Variable TCL-IMPST-SOST-1382011
     * 
     */
    AfDecimal getImpstSost1382011();

    void setImpstSost1382011(AfDecimal impstSost1382011);

    /**
     * Nullable property for TCL-IMPST-SOST-1382011
     * 
     */
    AfDecimal getImpstSost1382011Obj();

    void setImpstSost1382011Obj(AfDecimal impstSost1382011Obj);

    /**
     * Host Variable TCL-IMP-INTR-RIT-PAG
     * 
     */
    AfDecimal getImpIntrRitPag();

    void setImpIntrRitPag(AfDecimal impIntrRitPag);

    /**
     * Nullable property for TCL-IMP-INTR-RIT-PAG
     * 
     */
    AfDecimal getImpIntrRitPagObj();

    void setImpIntrRitPagObj(AfDecimal impIntrRitPagObj);

    /**
     * Host Variable TCL-IMPB-IS
     * 
     */
    AfDecimal getImpbIs();

    void setImpbIs(AfDecimal impbIs);

    /**
     * Nullable property for TCL-IMPB-IS
     * 
     */
    AfDecimal getImpbIsObj();

    void setImpbIsObj(AfDecimal impbIsObj);

    /**
     * Host Variable TCL-IMPB-IS-1382011
     * 
     */
    AfDecimal getImpbIs1382011();

    void setImpbIs1382011(AfDecimal impbIs1382011);

    /**
     * Nullable property for TCL-IMPB-IS-1382011
     * 
     */
    AfDecimal getImpbIs1382011Obj();

    void setImpbIs1382011Obj(AfDecimal impbIs1382011Obj);

    /**
     * Host Variable TCL-IMPB-VIS-662014
     * 
     */
    AfDecimal getImpbVis662014();

    void setImpbVis662014(AfDecimal impbVis662014);

    /**
     * Nullable property for TCL-IMPB-VIS-662014
     * 
     */
    AfDecimal getImpbVis662014Obj();

    void setImpbVis662014Obj(AfDecimal impbVis662014Obj);

    /**
     * Host Variable TCL-IMPST-VIS-662014
     * 
     */
    AfDecimal getImpstVis662014();

    void setImpstVis662014(AfDecimal impstVis662014);

    /**
     * Nullable property for TCL-IMPST-VIS-662014
     * 
     */
    AfDecimal getImpstVis662014Obj();

    void setImpstVis662014Obj(AfDecimal impstVis662014Obj);

    /**
     * Host Variable TCL-IMPB-IS-662014
     * 
     */
    AfDecimal getImpbIs662014();

    void setImpbIs662014(AfDecimal impbIs662014);

    /**
     * Nullable property for TCL-IMPB-IS-662014
     * 
     */
    AfDecimal getImpbIs662014Obj();

    void setImpbIs662014Obj(AfDecimal impbIs662014Obj);

    /**
     * Host Variable TCL-IMPST-SOST-662014
     * 
     */
    AfDecimal getImpstSost662014();

    void setImpstSost662014(AfDecimal impstSost662014);

    /**
     * Nullable property for TCL-IMPST-SOST-662014
     * 
     */
    AfDecimal getImpstSost662014Obj();

    void setImpstSost662014Obj(AfDecimal impstSost662014Obj);
};
