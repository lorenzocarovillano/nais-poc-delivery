package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ADES, STAT_OGG_BUS]
 * 
 */
public interface IAdesStatOggBus extends BaseSqlTo {

    /**
     * Host Variable LDBI0731-ID-POLI
     * 
     */
    int getLdbi0731IdPoli();

    void setLdbi0731IdPoli(int ldbi0731IdPoli);

    /**
     * Host Variable LDBI0731-TP-STAT-BUS
     * 
     */
    String getLdbi0731TpStatBus();

    void setLdbi0731TpStatBus(String ldbi0731TpStatBus);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable ADE-ID-POLI
     * 
     */
    int getAdeIdPoli();

    void setAdeIdPoli(int adeIdPoli);

    /**
     * Host Variable STB-ID-STAT-OGG-BUS
     * 
     */
    int getStbIdStatOggBus();

    void setStbIdStatOggBus(int stbIdStatOggBus);

    /**
     * Host Variable STB-ID-OGG
     * 
     */
    int getStbIdOgg();

    void setStbIdOgg(int stbIdOgg);

    /**
     * Host Variable STB-TP-OGG
     * 
     */
    String getStbTpOgg();

    void setStbTpOgg(String stbTpOgg);

    /**
     * Host Variable STB-ID-MOVI-CRZ
     * 
     */
    int getStbIdMoviCrz();

    void setStbIdMoviCrz(int stbIdMoviCrz);

    /**
     * Host Variable STB-ID-MOVI-CHIU
     * 
     */
    int getStbIdMoviChiu();

    void setStbIdMoviChiu(int stbIdMoviChiu);

    /**
     * Nullable property for STB-ID-MOVI-CHIU
     * 
     */
    Integer getStbIdMoviChiuObj();

    void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj);

    /**
     * Host Variable STB-DT-INI-EFF-DB
     * 
     */
    String getStbDtIniEffDb();

    void setStbDtIniEffDb(String stbDtIniEffDb);

    /**
     * Host Variable STB-DT-END-EFF-DB
     * 
     */
    String getStbDtEndEffDb();

    void setStbDtEndEffDb(String stbDtEndEffDb);

    /**
     * Host Variable STB-COD-COMP-ANIA
     * 
     */
    int getStbCodCompAnia();

    void setStbCodCompAnia(int stbCodCompAnia);

    /**
     * Host Variable STB-TP-STAT-BUS
     * 
     */
    String getStbTpStatBus();

    void setStbTpStatBus(String stbTpStatBus);

    /**
     * Host Variable STB-TP-CAUS
     * 
     */
    String getStbTpCaus();

    void setStbTpCaus(String stbTpCaus);

    /**
     * Host Variable STB-DS-RIGA
     * 
     */
    long getStbDsRiga();

    void setStbDsRiga(long stbDsRiga);

    /**
     * Host Variable STB-DS-OPER-SQL
     * 
     */
    char getStbDsOperSql();

    void setStbDsOperSql(char stbDsOperSql);

    /**
     * Host Variable STB-DS-VER
     * 
     */
    int getStbDsVer();

    void setStbDsVer(int stbDsVer);

    /**
     * Host Variable STB-DS-TS-INI-CPTZ
     * 
     */
    long getStbDsTsIniCptz();

    void setStbDsTsIniCptz(long stbDsTsIniCptz);

    /**
     * Host Variable STB-DS-TS-END-CPTZ
     * 
     */
    long getStbDsTsEndCptz();

    void setStbDsTsEndCptz(long stbDsTsEndCptz);

    /**
     * Host Variable STB-DS-UTENTE
     * 
     */
    String getStbDsUtente();

    void setStbDsUtente(String stbDsUtente);

    /**
     * Host Variable STB-DS-STATO-ELAB
     * 
     */
    char getStbDsStatoElab();

    void setStbDsStatoElab(char stbDsStatoElab);

    /**
     * Host Variable LDBI0731-ID-ADES
     * 
     */
    int getLdbi0731IdAdes();

    void setLdbi0731IdAdes(int ldbi0731IdAdes);
};
