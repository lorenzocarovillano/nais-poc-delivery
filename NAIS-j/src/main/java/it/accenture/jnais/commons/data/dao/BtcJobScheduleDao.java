package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBtcJobSchedule;

/**
 * Data Access Object(DAO) for table [BTC_JOB_SCHEDULE]
 * 
 */
public class BtcJobScheduleDao extends BaseSqlDao<IBtcJobSchedule> {

    private Cursor curBjsIdjStd;
    private Cursor curBjsIdjMf;
    private Cursor curBjsIdjTm;
    private Cursor curBjsIdjMfTm;
    private Cursor curBjsIboStd;
    private Cursor curBjsIboMf;
    private Cursor curBjsIboTm;
    private Cursor curBjsIboMfTm;
    private Cursor curIdjStdXRng;
    private final IRowMapper<IBtcJobSchedule> selectRecRm = buildNamedRowMapper(IBtcJobSchedule.class, "bjsIdBatch", "idJob", "codElabState", "flagWarningsObj", "dtStartDbObj", "dtEndDbObj", "userStartObj", "flagAlwaysExeObj", "executionsCountObj", "dataContentTypeObj", "bjsCodMacrofunctObj", "bjsTpMoviObj", "ibOggObj", "dataVcharObj");

    public BtcJobScheduleDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBtcJobSchedule> getToClass() {
        return IBtcJobSchedule.class;
    }

    public IBtcJobSchedule selectRec(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("selectRec").bind(iBtcJobSchedule).rowMapper(selectRecRm).singleResult(iBtcJobSchedule);
    }

    public IBtcJobSchedule selectRec1(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("selectRec1").bind(iBtcJobSchedule).rowMapper(selectRecRm).singleResult(iBtcJobSchedule);
    }

    public IBtcJobSchedule selectRec2(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("selectRec2").bind(iBtcJobSchedule).rowMapper(selectRecRm).singleResult(iBtcJobSchedule);
    }

    public IBtcJobSchedule selectRec3(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("selectRec3").bind(iBtcJobSchedule).rowMapper(selectRecRm).singleResult(iBtcJobSchedule);
    }

    public IBtcJobSchedule selectRec4(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("selectRec4").bind(iBtcJobSchedule).rowMapper(selectRecRm).singleResult(iBtcJobSchedule);
    }

    public DbAccessStatus insertRec(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("insertRec").bind(iBtcJobSchedule).executeInsert();
    }

    public DbAccessStatus updateRec(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec").bind(iBtcJobSchedule).executeUpdate();
    }

    public DbAccessStatus updateRec1(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec1").bind(iBtcJobSchedule).executeUpdate();
    }

    public DbAccessStatus updateRec2(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec2").bind(iBtcJobSchedule).executeUpdate();
    }

    public DbAccessStatus updateRec3(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec3").bind(iBtcJobSchedule).executeUpdate();
    }

    public DbAccessStatus updateRec4(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec4").bind(iBtcJobSchedule).executeUpdate();
    }

    public DbAccessStatus updateRec5(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec5").bind(iBtcJobSchedule).executeUpdate();
    }

    public DbAccessStatus updateRec6(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec6").bind(iBtcJobSchedule).executeUpdate();
    }

    public DbAccessStatus updateRec7(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec7").bind(iBtcJobSchedule).executeUpdate();
    }

    public DbAccessStatus updateRec8(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec8").bind(iBtcJobSchedule).executeUpdate();
    }

    public IBtcJobSchedule fetchCurBjsIdjStd(IBtcJobSchedule iBtcJobSchedule) {
        return fetch(curBjsIdjStd, iBtcJobSchedule, selectRecRm);
    }

    public IBtcJobSchedule fetchCurBjsIdjMf(IBtcJobSchedule iBtcJobSchedule) {
        return fetch(curBjsIdjMf, iBtcJobSchedule, selectRecRm);
    }

    public IBtcJobSchedule fetchCurBjsIdjTm(IBtcJobSchedule iBtcJobSchedule) {
        return fetch(curBjsIdjTm, iBtcJobSchedule, selectRecRm);
    }

    public IBtcJobSchedule fetchCurBjsIdjMfTm(IBtcJobSchedule iBtcJobSchedule) {
        return fetch(curBjsIdjMfTm, iBtcJobSchedule, selectRecRm);
    }

    public IBtcJobSchedule fetchCurBjsIboStd(IBtcJobSchedule iBtcJobSchedule) {
        return fetch(curBjsIboStd, iBtcJobSchedule, selectRecRm);
    }

    public IBtcJobSchedule fetchCurBjsIboMf(IBtcJobSchedule iBtcJobSchedule) {
        return fetch(curBjsIboMf, iBtcJobSchedule, selectRecRm);
    }

    public IBtcJobSchedule fetchCurBjsIboTm(IBtcJobSchedule iBtcJobSchedule) {
        return fetch(curBjsIboTm, iBtcJobSchedule, selectRecRm);
    }

    public IBtcJobSchedule fetchCurBjsIboMfTm(IBtcJobSchedule iBtcJobSchedule) {
        return fetch(curBjsIboMfTm, iBtcJobSchedule, selectRecRm);
    }

    public IBtcJobSchedule selectRec5(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("selectRec5").bind(iBtcJobSchedule).rowMapper(selectRecRm).singleResult(iBtcJobSchedule);
    }

    public IBtcJobSchedule fetchCurIdjStdXRng(IBtcJobSchedule iBtcJobSchedule) {
        return fetch(curIdjStdXRng, iBtcJobSchedule, selectRecRm);
    }

    public DbAccessStatus updateRec9(IBtcJobSchedule iBtcJobSchedule) {
        return buildQuery("updateRec9").bind(iBtcJobSchedule).executeUpdate();
    }

    public DbAccessStatus closeCurBjsIdjStd() {
        return closeCursor(curBjsIdjStd);
    }

    public DbAccessStatus closeCurBjsIdjMf() {
        return closeCursor(curBjsIdjMf);
    }

    public DbAccessStatus closeCurBjsIdjTm() {
        return closeCursor(curBjsIdjTm);
    }

    public DbAccessStatus closeCurBjsIdjMfTm() {
        return closeCursor(curBjsIdjMfTm);
    }

    public DbAccessStatus closeCurIdjStdXRng() {
        return closeCursor(curIdjStdXRng);
    }

    public DbAccessStatus closeCurBjsIboStd() {
        return closeCursor(curBjsIboStd);
    }

    public DbAccessStatus closeCurBjsIboMf() {
        return closeCursor(curBjsIboMf);
    }

    public DbAccessStatus closeCurBjsIboTm() {
        return closeCursor(curBjsIboTm);
    }

    public DbAccessStatus closeCurBjsIboMfTm() {
        return closeCursor(curBjsIboMfTm);
    }

    public DbAccessStatus openCurBjsIdjStd(IBtcJobSchedule iBtcJobSchedule) {
        curBjsIdjStd = buildQuery("openCurBjsIdjStd").bind(iBtcJobSchedule).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurBjsIdjMf(IBtcJobSchedule iBtcJobSchedule) {
        curBjsIdjMf = buildQuery("openCurBjsIdjMf").bind(iBtcJobSchedule).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurBjsIdjTm(IBtcJobSchedule iBtcJobSchedule) {
        curBjsIdjTm = buildQuery("openCurBjsIdjTm").bind(iBtcJobSchedule).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurBjsIdjMfTm(IBtcJobSchedule iBtcJobSchedule) {
        curBjsIdjMfTm = buildQuery("openCurBjsIdjMfTm").bind(iBtcJobSchedule).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurIdjStdXRng(IBtcJobSchedule iBtcJobSchedule) {
        curIdjStdXRng = buildQuery("openCurIdjStdXRng").bind(iBtcJobSchedule).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurBjsIboStd(IBtcJobSchedule iBtcJobSchedule) {
        curBjsIboStd = buildQuery("openCurBjsIboStd").bind(iBtcJobSchedule).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurBjsIboMf(IBtcJobSchedule iBtcJobSchedule) {
        curBjsIboMf = buildQuery("openCurBjsIboMf").bind(iBtcJobSchedule).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurBjsIboTm(IBtcJobSchedule iBtcJobSchedule) {
        curBjsIboTm = buildQuery("openCurBjsIboTm").bind(iBtcJobSchedule).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurBjsIboMfTm(IBtcJobSchedule iBtcJobSchedule) {
        curBjsIboMfTm = buildQuery("openCurBjsIboMfTm").bind(iBtcJobSchedule).withHoldCursorsOverCommit().open();
        return dbStatus;
    }
}
