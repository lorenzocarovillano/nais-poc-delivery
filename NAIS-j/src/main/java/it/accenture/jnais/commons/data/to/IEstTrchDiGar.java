package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [EST_TRCH_DI_GAR]
 * 
 */
public interface IEstTrchDiGar extends BaseSqlTo {

    /**
     * Host Variable E12-ID-EST-TRCH-DI-GAR
     * 
     */
    int getIdEstTrchDiGar();

    void setIdEstTrchDiGar(int idEstTrchDiGar);

    /**
     * Host Variable E12-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Host Variable E12-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Host Variable E12-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Host Variable E12-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable E12-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable E12-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for E12-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable E12-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable E12-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable E12-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable E12-DT-EMIS-DB
     * 
     */
    String getDtEmisDb();

    void setDtEmisDb(String dtEmisDb);

    /**
     * Nullable property for E12-DT-EMIS-DB
     * 
     */
    String getDtEmisDbObj();

    void setDtEmisDbObj(String dtEmisDbObj);

    /**
     * Host Variable E12-CUM-PRE-ATT
     * 
     */
    AfDecimal getCumPreAtt();

    void setCumPreAtt(AfDecimal cumPreAtt);

    /**
     * Nullable property for E12-CUM-PRE-ATT
     * 
     */
    AfDecimal getCumPreAttObj();

    void setCumPreAttObj(AfDecimal cumPreAttObj);

    /**
     * Host Variable E12-ACCPRE-PAG
     * 
     */
    AfDecimal getAccprePag();

    void setAccprePag(AfDecimal accprePag);

    /**
     * Nullable property for E12-ACCPRE-PAG
     * 
     */
    AfDecimal getAccprePagObj();

    void setAccprePagObj(AfDecimal accprePagObj);

    /**
     * Host Variable E12-CUM-PRSTZ
     * 
     */
    AfDecimal getCumPrstz();

    void setCumPrstz(AfDecimal cumPrstz);

    /**
     * Nullable property for E12-CUM-PRSTZ
     * 
     */
    AfDecimal getCumPrstzObj();

    void setCumPrstzObj(AfDecimal cumPrstzObj);

    /**
     * Host Variable E12-DS-RIGA
     * 
     */
    long getE12DsRiga();

    void setE12DsRiga(long e12DsRiga);

    /**
     * Host Variable E12-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable E12-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable E12-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable E12-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable E12-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable E12-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable E12-TP-TRCH
     * 
     */
    String getTpTrch();

    void setTpTrch(String tpTrch);

    /**
     * Host Variable E12-CAUS-SCON
     * 
     */
    String getCausScon();

    void setCausScon(String causScon);

    /**
     * Nullable property for E12-CAUS-SCON
     * 
     */
    String getCausSconObj();

    void setCausSconObj(String causSconObj);
};
