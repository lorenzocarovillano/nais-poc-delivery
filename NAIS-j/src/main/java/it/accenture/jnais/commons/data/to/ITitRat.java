package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [TIT_RAT]
 * 
 */
public interface ITitRat extends BaseSqlTo {

    /**
     * Host Variable TDR-ID-OGG
     * 
     */
    int getTdrIdOgg();

    void setTdrIdOgg(int tdrIdOgg);

    /**
     * Host Variable TDR-TP-OGG
     * 
     */
    String getTdrTpOgg();

    void setTdrTpOgg(String tdrTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable TDR-ID-TIT-RAT
     * 
     */
    int getIdTitRat();

    void setIdTitRat(int idTitRat);

    /**
     * Host Variable TDR-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable TDR-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for TDR-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable TDR-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable TDR-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable TDR-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable TDR-TP-TIT
     * 
     */
    String getTpTit();

    void setTpTit(String tpTit);

    /**
     * Host Variable TDR-PROG-TIT
     * 
     */
    int getProgTit();

    void setProgTit(int progTit);

    /**
     * Nullable property for TDR-PROG-TIT
     * 
     */
    Integer getProgTitObj();

    void setProgTitObj(Integer progTitObj);

    /**
     * Host Variable TDR-TP-PRE-TIT
     * 
     */
    String getTpPreTit();

    void setTpPreTit(String tpPreTit);

    /**
     * Host Variable TDR-TP-STAT-TIT
     * 
     */
    String getTpStatTit();

    void setTpStatTit(String tpStatTit);

    /**
     * Host Variable TDR-DT-INI-COP-DB
     * 
     */
    String getDtIniCopDb();

    void setDtIniCopDb(String dtIniCopDb);

    /**
     * Nullable property for TDR-DT-INI-COP-DB
     * 
     */
    String getDtIniCopDbObj();

    void setDtIniCopDbObj(String dtIniCopDbObj);

    /**
     * Host Variable TDR-DT-END-COP-DB
     * 
     */
    String getDtEndCopDb();

    void setDtEndCopDb(String dtEndCopDb);

    /**
     * Nullable property for TDR-DT-END-COP-DB
     * 
     */
    String getDtEndCopDbObj();

    void setDtEndCopDbObj(String dtEndCopDbObj);

    /**
     * Host Variable TDR-IMP-PAG
     * 
     */
    AfDecimal getImpPag();

    void setImpPag(AfDecimal impPag);

    /**
     * Nullable property for TDR-IMP-PAG
     * 
     */
    AfDecimal getImpPagObj();

    void setImpPagObj(AfDecimal impPagObj);

    /**
     * Host Variable TDR-FL-SOLL
     * 
     */
    char getFlSoll();

    void setFlSoll(char flSoll);

    /**
     * Nullable property for TDR-FL-SOLL
     * 
     */
    Character getFlSollObj();

    void setFlSollObj(Character flSollObj);

    /**
     * Host Variable TDR-FRAZ
     * 
     */
    int getFraz();

    void setFraz(int fraz);

    /**
     * Nullable property for TDR-FRAZ
     * 
     */
    Integer getFrazObj();

    void setFrazObj(Integer frazObj);

    /**
     * Host Variable TDR-DT-APPLZ-MORA-DB
     * 
     */
    String getDtApplzMoraDb();

    void setDtApplzMoraDb(String dtApplzMoraDb);

    /**
     * Nullable property for TDR-DT-APPLZ-MORA-DB
     * 
     */
    String getDtApplzMoraDbObj();

    void setDtApplzMoraDbObj(String dtApplzMoraDbObj);

    /**
     * Host Variable TDR-FL-MORA
     * 
     */
    char getFlMora();

    void setFlMora(char flMora);

    /**
     * Nullable property for TDR-FL-MORA
     * 
     */
    Character getFlMoraObj();

    void setFlMoraObj(Character flMoraObj);

    /**
     * Host Variable TDR-ID-RAPP-RETE
     * 
     */
    int getIdRappRete();

    void setIdRappRete(int idRappRete);

    /**
     * Nullable property for TDR-ID-RAPP-RETE
     * 
     */
    Integer getIdRappReteObj();

    void setIdRappReteObj(Integer idRappReteObj);

    /**
     * Host Variable TDR-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Nullable property for TDR-ID-RAPP-ANA
     * 
     */
    Integer getIdRappAnaObj();

    void setIdRappAnaObj(Integer idRappAnaObj);

    /**
     * Host Variable TDR-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for TDR-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable TDR-DT-EMIS-TIT-DB
     * 
     */
    String getDtEmisTitDb();

    void setDtEmisTitDb(String dtEmisTitDb);

    /**
     * Host Variable TDR-DT-ESI-TIT-DB
     * 
     */
    String getDtEsiTitDb();

    void setDtEsiTitDb(String dtEsiTitDb);

    /**
     * Nullable property for TDR-DT-ESI-TIT-DB
     * 
     */
    String getDtEsiTitDbObj();

    void setDtEsiTitDbObj(String dtEsiTitDbObj);

    /**
     * Host Variable TDR-TOT-PRE-NET
     * 
     */
    AfDecimal getTotPreNet();

    void setTotPreNet(AfDecimal totPreNet);

    /**
     * Nullable property for TDR-TOT-PRE-NET
     * 
     */
    AfDecimal getTotPreNetObj();

    void setTotPreNetObj(AfDecimal totPreNetObj);

    /**
     * Host Variable TDR-TOT-INTR-FRAZ
     * 
     */
    AfDecimal getTotIntrFraz();

    void setTotIntrFraz(AfDecimal totIntrFraz);

    /**
     * Nullable property for TDR-TOT-INTR-FRAZ
     * 
     */
    AfDecimal getTotIntrFrazObj();

    void setTotIntrFrazObj(AfDecimal totIntrFrazObj);

    /**
     * Host Variable TDR-TOT-INTR-MORA
     * 
     */
    AfDecimal getTotIntrMora();

    void setTotIntrMora(AfDecimal totIntrMora);

    /**
     * Nullable property for TDR-TOT-INTR-MORA
     * 
     */
    AfDecimal getTotIntrMoraObj();

    void setTotIntrMoraObj(AfDecimal totIntrMoraObj);

    /**
     * Host Variable TDR-TOT-INTR-PREST
     * 
     */
    AfDecimal getTotIntrPrest();

    void setTotIntrPrest(AfDecimal totIntrPrest);

    /**
     * Nullable property for TDR-TOT-INTR-PREST
     * 
     */
    AfDecimal getTotIntrPrestObj();

    void setTotIntrPrestObj(AfDecimal totIntrPrestObj);

    /**
     * Host Variable TDR-TOT-INTR-RETDT
     * 
     */
    AfDecimal getTotIntrRetdt();

    void setTotIntrRetdt(AfDecimal totIntrRetdt);

    /**
     * Nullable property for TDR-TOT-INTR-RETDT
     * 
     */
    AfDecimal getTotIntrRetdtObj();

    void setTotIntrRetdtObj(AfDecimal totIntrRetdtObj);

    /**
     * Host Variable TDR-TOT-INTR-RIAT
     * 
     */
    AfDecimal getTotIntrRiat();

    void setTotIntrRiat(AfDecimal totIntrRiat);

    /**
     * Nullable property for TDR-TOT-INTR-RIAT
     * 
     */
    AfDecimal getTotIntrRiatObj();

    void setTotIntrRiatObj(AfDecimal totIntrRiatObj);

    /**
     * Host Variable TDR-TOT-DIR
     * 
     */
    AfDecimal getTotDir();

    void setTotDir(AfDecimal totDir);

    /**
     * Nullable property for TDR-TOT-DIR
     * 
     */
    AfDecimal getTotDirObj();

    void setTotDirObj(AfDecimal totDirObj);

    /**
     * Host Variable TDR-TOT-SPE-MED
     * 
     */
    AfDecimal getTotSpeMed();

    void setTotSpeMed(AfDecimal totSpeMed);

    /**
     * Nullable property for TDR-TOT-SPE-MED
     * 
     */
    AfDecimal getTotSpeMedObj();

    void setTotSpeMedObj(AfDecimal totSpeMedObj);

    /**
     * Host Variable TDR-TOT-SPE-AGE
     * 
     */
    AfDecimal getTotSpeAge();

    void setTotSpeAge(AfDecimal totSpeAge);

    /**
     * Nullable property for TDR-TOT-SPE-AGE
     * 
     */
    AfDecimal getTotSpeAgeObj();

    void setTotSpeAgeObj(AfDecimal totSpeAgeObj);

    /**
     * Host Variable TDR-TOT-TAX
     * 
     */
    AfDecimal getTotTax();

    void setTotTax(AfDecimal totTax);

    /**
     * Nullable property for TDR-TOT-TAX
     * 
     */
    AfDecimal getTotTaxObj();

    void setTotTaxObj(AfDecimal totTaxObj);

    /**
     * Host Variable TDR-TOT-SOPR-SAN
     * 
     */
    AfDecimal getTotSoprSan();

    void setTotSoprSan(AfDecimal totSoprSan);

    /**
     * Nullable property for TDR-TOT-SOPR-SAN
     * 
     */
    AfDecimal getTotSoprSanObj();

    void setTotSoprSanObj(AfDecimal totSoprSanObj);

    /**
     * Host Variable TDR-TOT-SOPR-TEC
     * 
     */
    AfDecimal getTotSoprTec();

    void setTotSoprTec(AfDecimal totSoprTec);

    /**
     * Nullable property for TDR-TOT-SOPR-TEC
     * 
     */
    AfDecimal getTotSoprTecObj();

    void setTotSoprTecObj(AfDecimal totSoprTecObj);

    /**
     * Host Variable TDR-TOT-SOPR-SPO
     * 
     */
    AfDecimal getTotSoprSpo();

    void setTotSoprSpo(AfDecimal totSoprSpo);

    /**
     * Nullable property for TDR-TOT-SOPR-SPO
     * 
     */
    AfDecimal getTotSoprSpoObj();

    void setTotSoprSpoObj(AfDecimal totSoprSpoObj);

    /**
     * Host Variable TDR-TOT-SOPR-PROF
     * 
     */
    AfDecimal getTotSoprProf();

    void setTotSoprProf(AfDecimal totSoprProf);

    /**
     * Nullable property for TDR-TOT-SOPR-PROF
     * 
     */
    AfDecimal getTotSoprProfObj();

    void setTotSoprProfObj(AfDecimal totSoprProfObj);

    /**
     * Host Variable TDR-TOT-SOPR-ALT
     * 
     */
    AfDecimal getTotSoprAlt();

    void setTotSoprAlt(AfDecimal totSoprAlt);

    /**
     * Nullable property for TDR-TOT-SOPR-ALT
     * 
     */
    AfDecimal getTotSoprAltObj();

    void setTotSoprAltObj(AfDecimal totSoprAltObj);

    /**
     * Host Variable TDR-TOT-PRE-TOT
     * 
     */
    AfDecimal getTotPreTot();

    void setTotPreTot(AfDecimal totPreTot);

    /**
     * Nullable property for TDR-TOT-PRE-TOT
     * 
     */
    AfDecimal getTotPreTotObj();

    void setTotPreTotObj(AfDecimal totPreTotObj);

    /**
     * Host Variable TDR-TOT-PRE-PP-IAS
     * 
     */
    AfDecimal getTotPrePpIas();

    void setTotPrePpIas(AfDecimal totPrePpIas);

    /**
     * Nullable property for TDR-TOT-PRE-PP-IAS
     * 
     */
    AfDecimal getTotPrePpIasObj();

    void setTotPrePpIasObj(AfDecimal totPrePpIasObj);

    /**
     * Host Variable TDR-TOT-CAR-IAS
     * 
     */
    AfDecimal getTotCarIas();

    void setTotCarIas(AfDecimal totCarIas);

    /**
     * Nullable property for TDR-TOT-CAR-IAS
     * 
     */
    AfDecimal getTotCarIasObj();

    void setTotCarIasObj(AfDecimal totCarIasObj);

    /**
     * Host Variable TDR-TOT-PRE-SOLO-RSH
     * 
     */
    AfDecimal getTotPreSoloRsh();

    void setTotPreSoloRsh(AfDecimal totPreSoloRsh);

    /**
     * Nullable property for TDR-TOT-PRE-SOLO-RSH
     * 
     */
    AfDecimal getTotPreSoloRshObj();

    void setTotPreSoloRshObj(AfDecimal totPreSoloRshObj);

    /**
     * Host Variable TDR-TOT-PROV-ACQ-1AA
     * 
     */
    AfDecimal getTotProvAcq1aa();

    void setTotProvAcq1aa(AfDecimal totProvAcq1aa);

    /**
     * Nullable property for TDR-TOT-PROV-ACQ-1AA
     * 
     */
    AfDecimal getTotProvAcq1aaObj();

    void setTotProvAcq1aaObj(AfDecimal totProvAcq1aaObj);

    /**
     * Host Variable TDR-TOT-PROV-ACQ-2AA
     * 
     */
    AfDecimal getTotProvAcq2aa();

    void setTotProvAcq2aa(AfDecimal totProvAcq2aa);

    /**
     * Nullable property for TDR-TOT-PROV-ACQ-2AA
     * 
     */
    AfDecimal getTotProvAcq2aaObj();

    void setTotProvAcq2aaObj(AfDecimal totProvAcq2aaObj);

    /**
     * Host Variable TDR-TOT-PROV-RICOR
     * 
     */
    AfDecimal getTotProvRicor();

    void setTotProvRicor(AfDecimal totProvRicor);

    /**
     * Nullable property for TDR-TOT-PROV-RICOR
     * 
     */
    AfDecimal getTotProvRicorObj();

    void setTotProvRicorObj(AfDecimal totProvRicorObj);

    /**
     * Host Variable TDR-TOT-PROV-INC
     * 
     */
    AfDecimal getTotProvInc();

    void setTotProvInc(AfDecimal totProvInc);

    /**
     * Nullable property for TDR-TOT-PROV-INC
     * 
     */
    AfDecimal getTotProvIncObj();

    void setTotProvIncObj(AfDecimal totProvIncObj);

    /**
     * Host Variable TDR-TOT-PROV-DA-REC
     * 
     */
    AfDecimal getTotProvDaRec();

    void setTotProvDaRec(AfDecimal totProvDaRec);

    /**
     * Nullable property for TDR-TOT-PROV-DA-REC
     * 
     */
    AfDecimal getTotProvDaRecObj();

    void setTotProvDaRecObj(AfDecimal totProvDaRecObj);

    /**
     * Host Variable TDR-IMP-AZ
     * 
     */
    AfDecimal getImpAz();

    void setImpAz(AfDecimal impAz);

    /**
     * Nullable property for TDR-IMP-AZ
     * 
     */
    AfDecimal getImpAzObj();

    void setImpAzObj(AfDecimal impAzObj);

    /**
     * Host Variable TDR-IMP-ADER
     * 
     */
    AfDecimal getImpAder();

    void setImpAder(AfDecimal impAder);

    /**
     * Nullable property for TDR-IMP-ADER
     * 
     */
    AfDecimal getImpAderObj();

    void setImpAderObj(AfDecimal impAderObj);

    /**
     * Host Variable TDR-IMP-TFR
     * 
     */
    AfDecimal getImpTfr();

    void setImpTfr(AfDecimal impTfr);

    /**
     * Nullable property for TDR-IMP-TFR
     * 
     */
    AfDecimal getImpTfrObj();

    void setImpTfrObj(AfDecimal impTfrObj);

    /**
     * Host Variable TDR-IMP-VOLO
     * 
     */
    AfDecimal getImpVolo();

    void setImpVolo(AfDecimal impVolo);

    /**
     * Nullable property for TDR-IMP-VOLO
     * 
     */
    AfDecimal getImpVoloObj();

    void setImpVoloObj(AfDecimal impVoloObj);

    /**
     * Host Variable TDR-FL-VLDT-TIT
     * 
     */
    char getFlVldtTit();

    void setFlVldtTit(char flVldtTit);

    /**
     * Nullable property for TDR-FL-VLDT-TIT
     * 
     */
    Character getFlVldtTitObj();

    void setFlVldtTitObj(Character flVldtTitObj);

    /**
     * Host Variable TDR-TOT-CAR-ACQ
     * 
     */
    AfDecimal getTotCarAcq();

    void setTotCarAcq(AfDecimal totCarAcq);

    /**
     * Nullable property for TDR-TOT-CAR-ACQ
     * 
     */
    AfDecimal getTotCarAcqObj();

    void setTotCarAcqObj(AfDecimal totCarAcqObj);

    /**
     * Host Variable TDR-TOT-CAR-GEST
     * 
     */
    AfDecimal getTotCarGest();

    void setTotCarGest(AfDecimal totCarGest);

    /**
     * Nullable property for TDR-TOT-CAR-GEST
     * 
     */
    AfDecimal getTotCarGestObj();

    void setTotCarGestObj(AfDecimal totCarGestObj);

    /**
     * Host Variable TDR-TOT-CAR-INC
     * 
     */
    AfDecimal getTotCarInc();

    void setTotCarInc(AfDecimal totCarInc);

    /**
     * Nullable property for TDR-TOT-CAR-INC
     * 
     */
    AfDecimal getTotCarIncObj();

    void setTotCarIncObj(AfDecimal totCarIncObj);

    /**
     * Host Variable TDR-TOT-MANFEE-ANTIC
     * 
     */
    AfDecimal getTotManfeeAntic();

    void setTotManfeeAntic(AfDecimal totManfeeAntic);

    /**
     * Nullable property for TDR-TOT-MANFEE-ANTIC
     * 
     */
    AfDecimal getTotManfeeAnticObj();

    void setTotManfeeAnticObj(AfDecimal totManfeeAnticObj);

    /**
     * Host Variable TDR-TOT-MANFEE-RICOR
     * 
     */
    AfDecimal getTotManfeeRicor();

    void setTotManfeeRicor(AfDecimal totManfeeRicor);

    /**
     * Nullable property for TDR-TOT-MANFEE-RICOR
     * 
     */
    AfDecimal getTotManfeeRicorObj();

    void setTotManfeeRicorObj(AfDecimal totManfeeRicorObj);

    /**
     * Host Variable TDR-TOT-MANFEE-REC
     * 
     */
    AfDecimal getTotManfeeRec();

    void setTotManfeeRec(AfDecimal totManfeeRec);

    /**
     * Nullable property for TDR-TOT-MANFEE-REC
     * 
     */
    AfDecimal getTotManfeeRecObj();

    void setTotManfeeRecObj(AfDecimal totManfeeRecObj);

    /**
     * Host Variable TDR-DS-RIGA
     * 
     */
    long getTdrDsRiga();

    void setTdrDsRiga(long tdrDsRiga);

    /**
     * Host Variable TDR-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable TDR-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable TDR-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable TDR-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable TDR-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable TDR-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable TDR-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfe();

    void setImpTrasfe(AfDecimal impTrasfe);

    /**
     * Nullable property for TDR-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfeObj();

    void setImpTrasfeObj(AfDecimal impTrasfeObj);

    /**
     * Host Variable TDR-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrc();

    void setImpTfrStrc(AfDecimal impTfrStrc);

    /**
     * Nullable property for TDR-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrcObj();

    void setImpTfrStrcObj(AfDecimal impTfrStrcObj);

    /**
     * Host Variable TDR-TOT-ACQ-EXP
     * 
     */
    AfDecimal getTotAcqExp();

    void setTotAcqExp(AfDecimal totAcqExp);

    /**
     * Nullable property for TDR-TOT-ACQ-EXP
     * 
     */
    AfDecimal getTotAcqExpObj();

    void setTotAcqExpObj(AfDecimal totAcqExpObj);

    /**
     * Host Variable TDR-TOT-REMUN-ASS
     * 
     */
    AfDecimal getTotRemunAss();

    void setTotRemunAss(AfDecimal totRemunAss);

    /**
     * Nullable property for TDR-TOT-REMUN-ASS
     * 
     */
    AfDecimal getTotRemunAssObj();

    void setTotRemunAssObj(AfDecimal totRemunAssObj);

    /**
     * Host Variable TDR-TOT-COMMIS-INTER
     * 
     */
    AfDecimal getTotCommisInter();

    void setTotCommisInter(AfDecimal totCommisInter);

    /**
     * Nullable property for TDR-TOT-COMMIS-INTER
     * 
     */
    AfDecimal getTotCommisInterObj();

    void setTotCommisInterObj(AfDecimal totCommisInterObj);

    /**
     * Host Variable TDR-TOT-CNBT-ANTIRAC
     * 
     */
    AfDecimal getTotCnbtAntirac();

    void setTotCnbtAntirac(AfDecimal totCnbtAntirac);

    /**
     * Nullable property for TDR-TOT-CNBT-ANTIRAC
     * 
     */
    AfDecimal getTotCnbtAntiracObj();

    void setTotCnbtAntiracObj(AfDecimal totCnbtAntiracObj);

    /**
     * Host Variable TDR-FL-INC-AUTOGEN
     * 
     */
    char getFlIncAutogen();

    void setFlIncAutogen(char flIncAutogen);

    /**
     * Nullable property for TDR-FL-INC-AUTOGEN
     * 
     */
    Character getFlIncAutogenObj();

    void setFlIncAutogenObj(Character flIncAutogenObj);
};
