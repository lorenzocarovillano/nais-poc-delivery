package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IEstrCntDiagnCed;

/**
 * Data Access Object(DAO) for table [ESTR_CNT_DIAGN_CED]
 * 
 */
public class EstrCntDiagnCedDao extends BaseSqlDao<IEstrCntDiagnCed> {

    private Cursor cNst24;
    private final IRowMapper<IEstrCntDiagnCed> selectRecRm = buildNamedRowMapper(IEstrCntDiagnCed.class, "codCompAnia", "idPoli", "dtLiqCedDb", "codTari", "impLrdCedoleLiqObj", "ibPoli", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public EstrCntDiagnCedDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IEstrCntDiagnCed> getToClass() {
        return IEstrCntDiagnCed.class;
    }

    public IEstrCntDiagnCed selectRec(int codCompAnia, int idPoli, String dtLiqCedDb, String codTari, IEstrCntDiagnCed iEstrCntDiagnCed) {
        return buildQuery("selectRec").bind("codCompAnia", codCompAnia).bind("idPoli", idPoli).bind("dtLiqCedDb", dtLiqCedDb).bind("codTari", codTari).rowMapper(selectRecRm).singleResult(iEstrCntDiagnCed);
    }

    public DbAccessStatus insertRec(IEstrCntDiagnCed iEstrCntDiagnCed) {
        return buildQuery("insertRec").bind(iEstrCntDiagnCed).executeInsert();
    }

    public DbAccessStatus updateRec(IEstrCntDiagnCed iEstrCntDiagnCed) {
        return buildQuery("updateRec").bind(iEstrCntDiagnCed).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompAnia, int idPoli, String dtLiqCedDb, String codTari) {
        return buildQuery("deleteRec").bind("codCompAnia", codCompAnia).bind("idPoli", idPoli).bind("dtLiqCedDb", dtLiqCedDb).bind("codTari", codTari).executeDelete();
    }

    public IEstrCntDiagnCed selectRec1(int ldbvf301IdPoli, String ldbvf301DtLiqCedDaDb, String ldbvf301DtLiqCedADb, int idsv0003CodiceCompagniaAnia, IEstrCntDiagnCed iEstrCntDiagnCed) {
        return buildQuery("selectRec1").bind("ldbvf301IdPoli", ldbvf301IdPoli).bind("ldbvf301DtLiqCedDaDb", ldbvf301DtLiqCedDaDb).bind("ldbvf301DtLiqCedADb", ldbvf301DtLiqCedADb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectRecRm).singleResult(iEstrCntDiagnCed);
    }

    public DbAccessStatus openCNst24(int ldbvf301IdPoli, String ldbvf301DtLiqCedDaDb, String ldbvf301DtLiqCedADb, int idsv0003CodiceCompagniaAnia) {
        cNst24 = buildQuery("openCNst24").bind("ldbvf301IdPoli", ldbvf301IdPoli).bind("ldbvf301DtLiqCedDaDb", ldbvf301DtLiqCedDaDb).bind("ldbvf301DtLiqCedADb", ldbvf301DtLiqCedADb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst24() {
        return closeCursor(cNst24);
    }

    public IEstrCntDiagnCed fetchCNst24(IEstrCntDiagnCed iEstrCntDiagnCed) {
        return fetch(cNst24, iEstrCntDiagnCed, selectRecRm);
    }
}
