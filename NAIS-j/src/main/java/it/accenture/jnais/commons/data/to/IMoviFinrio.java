package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [MOVI_FINRIO]
 * 
 */
public interface IMoviFinrio extends BaseSqlTo {

    /**
     * Host Variable MFZ-ID-MOVI-FINRIO
     * 
     */
    int getIdMoviFinrio();

    void setIdMoviFinrio(int idMoviFinrio);

    /**
     * Host Variable MFZ-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Nullable property for MFZ-ID-ADES
     * 
     */
    Integer getIdAdesObj();

    void setIdAdesObj(Integer idAdesObj);

    /**
     * Host Variable MFZ-ID-LIQ
     * 
     */
    int getIdLiq();

    void setIdLiq(int idLiq);

    /**
     * Nullable property for MFZ-ID-LIQ
     * 
     */
    Integer getIdLiqObj();

    void setIdLiqObj(Integer idLiqObj);

    /**
     * Host Variable MFZ-ID-TIT-CONT
     * 
     */
    int getIdTitCont();

    void setIdTitCont(int idTitCont);

    /**
     * Nullable property for MFZ-ID-TIT-CONT
     * 
     */
    Integer getIdTitContObj();

    void setIdTitContObj(Integer idTitContObj);

    /**
     * Host Variable MFZ-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable MFZ-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for MFZ-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable MFZ-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable MFZ-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable MFZ-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable MFZ-TP-MOVI-FINRIO
     * 
     */
    String getTpMoviFinrio();

    void setTpMoviFinrio(String tpMoviFinrio);

    /**
     * Host Variable MFZ-DT-EFF-MOVI-FINRIO-DB
     * 
     */
    String getDtEffMoviFinrioDb();

    void setDtEffMoviFinrioDb(String dtEffMoviFinrioDb);

    /**
     * Nullable property for MFZ-DT-EFF-MOVI-FINRIO-DB
     * 
     */
    String getDtEffMoviFinrioDbObj();

    void setDtEffMoviFinrioDbObj(String dtEffMoviFinrioDbObj);

    /**
     * Host Variable MFZ-DT-ELAB-DB
     * 
     */
    String getDtElabDb();

    void setDtElabDb(String dtElabDb);

    /**
     * Nullable property for MFZ-DT-ELAB-DB
     * 
     */
    String getDtElabDbObj();

    void setDtElabDbObj(String dtElabDbObj);

    /**
     * Host Variable MFZ-DT-RICH-MOVI-DB
     * 
     */
    String getDtRichMoviDb();

    void setDtRichMoviDb(String dtRichMoviDb);

    /**
     * Nullable property for MFZ-DT-RICH-MOVI-DB
     * 
     */
    String getDtRichMoviDbObj();

    void setDtRichMoviDbObj(String dtRichMoviDbObj);

    /**
     * Host Variable MFZ-COS-OPRZ
     * 
     */
    AfDecimal getCosOprz();

    void setCosOprz(AfDecimal cosOprz);

    /**
     * Nullable property for MFZ-COS-OPRZ
     * 
     */
    AfDecimal getCosOprzObj();

    void setCosOprzObj(AfDecimal cosOprzObj);

    /**
     * Host Variable MFZ-STAT-MOVI
     * 
     */
    String getStatMovi();

    void setStatMovi(String statMovi);

    /**
     * Host Variable MFZ-DS-RIGA
     * 
     */
    long getMfzDsRiga();

    void setMfzDsRiga(long mfzDsRiga);

    /**
     * Host Variable MFZ-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable MFZ-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable MFZ-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable MFZ-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable MFZ-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable MFZ-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable MFZ-TP-CAUS-RETTIFICA
     * 
     */
    String getTpCausRettifica();

    void setTpCausRettifica(String tpCausRettifica);

    /**
     * Nullable property for MFZ-TP-CAUS-RETTIFICA
     * 
     */
    String getTpCausRettificaObj();

    void setTpCausRettificaObj(String tpCausRettificaObj);
};
