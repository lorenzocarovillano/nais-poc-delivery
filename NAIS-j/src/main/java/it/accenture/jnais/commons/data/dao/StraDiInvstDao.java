package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IStraDiInvst;

/**
 * Data Access Object(DAO) for table [STRA_DI_INVST]
 * 
 */
public class StraDiInvstDao extends BaseSqlDao<IStraDiInvst> {

    private Cursor cIdUpdEffSdi;
    private Cursor cIdoEffSdi;
    private Cursor cIdoCpzSdi;
    private final IRowMapper<IStraDiInvst> selectBySdiDsRigaRm = buildNamedRowMapper(IStraDiInvst.class, "idStraDiInvst", "sdiIdOgg", "sdiTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codStraObj", "modGestObj", "varRiftoObj", "valVarRiftoObj", "dtFisObj", "frqValutObj", "flIniEndPerObj", "sdiDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "tpStraObj", "tpProvzaStraObj", "pcProtezioneObj");

    public StraDiInvstDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IStraDiInvst> getToClass() {
        return IStraDiInvst.class;
    }

    public IStraDiInvst selectBySdiDsRiga(long sdiDsRiga, IStraDiInvst iStraDiInvst) {
        return buildQuery("selectBySdiDsRiga").bind("sdiDsRiga", sdiDsRiga).rowMapper(selectBySdiDsRigaRm).singleResult(iStraDiInvst);
    }

    public DbAccessStatus insertRec(IStraDiInvst iStraDiInvst) {
        return buildQuery("insertRec").bind(iStraDiInvst).executeInsert();
    }

    public DbAccessStatus updateRec(IStraDiInvst iStraDiInvst) {
        return buildQuery("updateRec").bind(iStraDiInvst).executeUpdate();
    }

    public DbAccessStatus deleteBySdiDsRiga(long sdiDsRiga) {
        return buildQuery("deleteBySdiDsRiga").bind("sdiDsRiga", sdiDsRiga).executeDelete();
    }

    public IStraDiInvst selectRec(int sdiIdStraDiInvst, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IStraDiInvst iStraDiInvst) {
        return buildQuery("selectRec").bind("sdiIdStraDiInvst", sdiIdStraDiInvst).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectBySdiDsRigaRm).singleResult(iStraDiInvst);
    }

    public DbAccessStatus updateRec1(IStraDiInvst iStraDiInvst) {
        return buildQuery("updateRec1").bind(iStraDiInvst).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffSdi(int sdiIdStraDiInvst, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffSdi = buildQuery("openCIdUpdEffSdi").bind("sdiIdStraDiInvst", sdiIdStraDiInvst).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffSdi() {
        return closeCursor(cIdUpdEffSdi);
    }

    public IStraDiInvst fetchCIdUpdEffSdi(IStraDiInvst iStraDiInvst) {
        return fetch(cIdUpdEffSdi, iStraDiInvst, selectBySdiDsRigaRm);
    }

    public IStraDiInvst selectRec1(int sdiIdOgg, String sdiTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IStraDiInvst iStraDiInvst) {
        return buildQuery("selectRec1").bind("sdiIdOgg", sdiIdOgg).bind("sdiTpOgg", sdiTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectBySdiDsRigaRm).singleResult(iStraDiInvst);
    }

    public DbAccessStatus openCIdoEffSdi(int sdiIdOgg, String sdiTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffSdi = buildQuery("openCIdoEffSdi").bind("sdiIdOgg", sdiIdOgg).bind("sdiTpOgg", sdiTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffSdi() {
        return closeCursor(cIdoEffSdi);
    }

    public IStraDiInvst fetchCIdoEffSdi(IStraDiInvst iStraDiInvst) {
        return fetch(cIdoEffSdi, iStraDiInvst, selectBySdiDsRigaRm);
    }

    public IStraDiInvst selectRec2(int sdiIdStraDiInvst, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IStraDiInvst iStraDiInvst) {
        return buildQuery("selectRec2").bind("sdiIdStraDiInvst", sdiIdStraDiInvst).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectBySdiDsRigaRm).singleResult(iStraDiInvst);
    }

    public IStraDiInvst selectRec3(IStraDiInvst iStraDiInvst) {
        return buildQuery("selectRec3").bind(iStraDiInvst).rowMapper(selectBySdiDsRigaRm).singleResult(iStraDiInvst);
    }

    public DbAccessStatus openCIdoCpzSdi(IStraDiInvst iStraDiInvst) {
        cIdoCpzSdi = buildQuery("openCIdoCpzSdi").bind(iStraDiInvst).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzSdi() {
        return closeCursor(cIdoCpzSdi);
    }

    public IStraDiInvst fetchCIdoCpzSdi(IStraDiInvst iStraDiInvst) {
        return fetch(cIdoCpzSdi, iStraDiInvst, selectBySdiDsRigaRm);
    }
}
