package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [DETT_QUEST, QUEST]
 * 
 */
public interface IDettQuest1 extends BaseSqlTo {

    /**
     * Host Variable LDBV5001-ID-RAPP-ANA
     * 
     */
    int getLdbv5001IdRappAna();

    void setLdbv5001IdRappAna(int ldbv5001IdRappAna);

    /**
     * Host Variable LDBV5001-ID-COMP-QUEST
     * 
     */
    int getLdbv5001IdCompQuest();

    void setLdbv5001IdCompQuest(int ldbv5001IdCompQuest);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable DEQ-ID-DETT-QUEST
     * 
     */
    int getIdDettQuest();

    void setIdDettQuest(int idDettQuest);

    /**
     * Host Variable DEQ-ID-QUEST
     * 
     */
    int getIdQuest();

    void setIdQuest(int idQuest);

    /**
     * Host Variable DEQ-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable DEQ-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for DEQ-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable DEQ-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable DEQ-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable DEQ-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable DEQ-COD-QUEST
     * 
     */
    String getCodQuest();

    void setCodQuest(String codQuest);

    /**
     * Nullable property for DEQ-COD-QUEST
     * 
     */
    String getCodQuestObj();

    void setCodQuestObj(String codQuestObj);

    /**
     * Host Variable DEQ-COD-DOM
     * 
     */
    String getCodDom();

    void setCodDom(String codDom);

    /**
     * Nullable property for DEQ-COD-DOM
     * 
     */
    String getCodDomObj();

    void setCodDomObj(String codDomObj);

    /**
     * Host Variable DEQ-COD-DOM-COLLG
     * 
     */
    String getCodDomCollg();

    void setCodDomCollg(String codDomCollg);

    /**
     * Nullable property for DEQ-COD-DOM-COLLG
     * 
     */
    String getCodDomCollgObj();

    void setCodDomCollgObj(String codDomCollgObj);

    /**
     * Host Variable DEQ-RISP-NUM
     * 
     */
    int getRispNum();

    void setRispNum(int rispNum);

    /**
     * Nullable property for DEQ-RISP-NUM
     * 
     */
    Integer getRispNumObj();

    void setRispNumObj(Integer rispNumObj);

    /**
     * Host Variable DEQ-RISP-FL
     * 
     */
    char getRispFl();

    void setRispFl(char rispFl);

    /**
     * Nullable property for DEQ-RISP-FL
     * 
     */
    Character getRispFlObj();

    void setRispFlObj(Character rispFlObj);

    /**
     * Host Variable DEQ-RISP-TXT-VCHAR
     * 
     */
    String getRispTxtVchar();

    void setRispTxtVchar(String rispTxtVchar);

    /**
     * Nullable property for DEQ-RISP-TXT-VCHAR
     * 
     */
    String getRispTxtVcharObj();

    void setRispTxtVcharObj(String rispTxtVcharObj);

    /**
     * Host Variable DEQ-RISP-TS
     * 
     */
    AfDecimal getRispTs();

    void setRispTs(AfDecimal rispTs);

    /**
     * Nullable property for DEQ-RISP-TS
     * 
     */
    AfDecimal getRispTsObj();

    void setRispTsObj(AfDecimal rispTsObj);

    /**
     * Host Variable DEQ-RISP-IMP
     * 
     */
    AfDecimal getRispImp();

    void setRispImp(AfDecimal rispImp);

    /**
     * Nullable property for DEQ-RISP-IMP
     * 
     */
    AfDecimal getRispImpObj();

    void setRispImpObj(AfDecimal rispImpObj);

    /**
     * Host Variable DEQ-RISP-PC
     * 
     */
    AfDecimal getRispPc();

    void setRispPc(AfDecimal rispPc);

    /**
     * Nullable property for DEQ-RISP-PC
     * 
     */
    AfDecimal getRispPcObj();

    void setRispPcObj(AfDecimal rispPcObj);

    /**
     * Host Variable DEQ-RISP-DT-DB
     * 
     */
    String getRispDtDb();

    void setRispDtDb(String rispDtDb);

    /**
     * Nullable property for DEQ-RISP-DT-DB
     * 
     */
    String getRispDtDbObj();

    void setRispDtDbObj(String rispDtDbObj);

    /**
     * Host Variable DEQ-RISP-KEY
     * 
     */
    String getRispKey();

    void setRispKey(String rispKey);

    /**
     * Nullable property for DEQ-RISP-KEY
     * 
     */
    String getRispKeyObj();

    void setRispKeyObj(String rispKeyObj);

    /**
     * Host Variable DEQ-TP-RISP
     * 
     */
    String getTpRisp();

    void setTpRisp(String tpRisp);

    /**
     * Nullable property for DEQ-TP-RISP
     * 
     */
    String getTpRispObj();

    void setTpRispObj(String tpRispObj);

    /**
     * Host Variable DEQ-ID-COMP-QUEST
     * 
     */
    int getIdCompQuest();

    void setIdCompQuest(int idCompQuest);

    /**
     * Nullable property for DEQ-ID-COMP-QUEST
     * 
     */
    Integer getIdCompQuestObj();

    void setIdCompQuestObj(Integer idCompQuestObj);

    /**
     * Host Variable DEQ-VAL-RISP-VCHAR
     * 
     */
    String getValRispVchar();

    void setValRispVchar(String valRispVchar);

    /**
     * Nullable property for DEQ-VAL-RISP-VCHAR
     * 
     */
    String getValRispVcharObj();

    void setValRispVcharObj(String valRispVcharObj);

    /**
     * Host Variable DEQ-DS-RIGA
     * 
     */
    long getDsRiga();

    void setDsRiga(long dsRiga);

    /**
     * Host Variable DEQ-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable DEQ-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable DEQ-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable DEQ-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable DEQ-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable DEQ-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
