package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [RAPP_RETE]
 * 
 */
public interface IRappRete extends BaseSqlTo {

    /**
     * Host Variable RRE-ID-OGG
     * 
     */
    int getRreIdOgg();

    void setRreIdOgg(int rreIdOgg);

    /**
     * Host Variable RRE-TP-OGG
     * 
     */
    String getRreTpOgg();

    void setRreTpOgg(String rreTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable RRE-TP-RETE
     * 
     */
    String getRreTpRete();

    void setRreTpRete(String rreTpRete);

    /**
     * Host Variable RRE-ID-RAPP-RETE
     * 
     */
    int getIdRappRete();

    void setIdRappRete(int idRappRete);

    /**
     * Nullable property for RRE-ID-OGG
     * 
     */
    Integer getRreIdOggObj();

    void setRreIdOggObj(Integer rreIdOggObj);

    /**
     * Host Variable RRE-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable RRE-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for RRE-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable RRE-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable RRE-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable RRE-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable RRE-TP-ACQS-CNTRT
     * 
     */
    int getTpAcqsCntrt();

    void setTpAcqsCntrt(int tpAcqsCntrt);

    /**
     * Nullable property for RRE-TP-ACQS-CNTRT
     * 
     */
    Integer getTpAcqsCntrtObj();

    void setTpAcqsCntrtObj(Integer tpAcqsCntrtObj);

    /**
     * Host Variable RRE-COD-ACQS-CNTRT
     * 
     */
    int getCodAcqsCntrt();

    void setCodAcqsCntrt(int codAcqsCntrt);

    /**
     * Nullable property for RRE-COD-ACQS-CNTRT
     * 
     */
    Integer getCodAcqsCntrtObj();

    void setCodAcqsCntrtObj(Integer codAcqsCntrtObj);

    /**
     * Host Variable RRE-COD-PNT-RETE-INI
     * 
     */
    long getCodPntReteIni();

    void setCodPntReteIni(long codPntReteIni);

    /**
     * Nullable property for RRE-COD-PNT-RETE-INI
     * 
     */
    Long getCodPntReteIniObj();

    void setCodPntReteIniObj(Long codPntReteIniObj);

    /**
     * Host Variable RRE-COD-PNT-RETE-END
     * 
     */
    long getCodPntReteEnd();

    void setCodPntReteEnd(long codPntReteEnd);

    /**
     * Nullable property for RRE-COD-PNT-RETE-END
     * 
     */
    Long getCodPntReteEndObj();

    void setCodPntReteEndObj(Long codPntReteEndObj);

    /**
     * Host Variable RRE-FL-PNT-RETE-1RIO
     * 
     */
    char getFlPntRete1rio();

    void setFlPntRete1rio(char flPntRete1rio);

    /**
     * Nullable property for RRE-FL-PNT-RETE-1RIO
     * 
     */
    Character getFlPntRete1rioObj();

    void setFlPntRete1rioObj(Character flPntRete1rioObj);

    /**
     * Host Variable RRE-COD-CAN
     * 
     */
    int getCodCan();

    void setCodCan(int codCan);

    /**
     * Nullable property for RRE-COD-CAN
     * 
     */
    Integer getCodCanObj();

    void setCodCanObj(Integer codCanObj);

    /**
     * Host Variable RRE-DS-RIGA
     * 
     */
    long getRreDsRiga();

    void setRreDsRiga(long rreDsRiga);

    /**
     * Host Variable RRE-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable RRE-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable RRE-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable RRE-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable RRE-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable RRE-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable RRE-COD-PNT-RETE-INI-C
     * 
     */
    long getCodPntReteIniC();

    void setCodPntReteIniC(long codPntReteIniC);

    /**
     * Nullable property for RRE-COD-PNT-RETE-INI-C
     * 
     */
    Long getCodPntReteIniCObj();

    void setCodPntReteIniCObj(Long codPntReteIniCObj);

    /**
     * Host Variable RRE-MATR-OPRT
     * 
     */
    String getMatrOprt();

    void setMatrOprt(String matrOprt);

    /**
     * Nullable property for RRE-MATR-OPRT
     * 
     */
    String getMatrOprtObj();

    void setMatrOprtObj(String matrOprtObj);
};
