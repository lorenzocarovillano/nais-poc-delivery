package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [QUEST_ADEG_VER]
 * 
 */
public interface IQuestAdegVer extends BaseSqlTo {

    /**
     * Host Variable P56-ID-QUEST-ADEG-VER
     * 
     */
    int getP56IdQuestAdegVer();

    void setP56IdQuestAdegVer(int p56IdQuestAdegVer);

    /**
     * Host Variable P56-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P56-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable P56-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Nullable property for P56-ID-RAPP-ANA
     * 
     */
    Integer getIdRappAnaObj();

    void setIdRappAnaObj(Integer idRappAnaObj);

    /**
     * Host Variable P56-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable P56-NATURA-OPRZ
     * 
     */
    String getNaturaOprz();

    void setNaturaOprz(String naturaOprz);

    /**
     * Nullable property for P56-NATURA-OPRZ
     * 
     */
    String getNaturaOprzObj();

    void setNaturaOprzObj(String naturaOprzObj);

    /**
     * Host Variable P56-ORGN-FND
     * 
     */
    String getOrgnFnd();

    void setOrgnFnd(String orgnFnd);

    /**
     * Nullable property for P56-ORGN-FND
     * 
     */
    String getOrgnFndObj();

    void setOrgnFndObj(String orgnFndObj);

    /**
     * Host Variable P56-COD-QLFC-PROF
     * 
     */
    String getCodQlfcProf();

    void setCodQlfcProf(String codQlfcProf);

    /**
     * Nullable property for P56-COD-QLFC-PROF
     * 
     */
    String getCodQlfcProfObj();

    void setCodQlfcProfObj(String codQlfcProfObj);

    /**
     * Host Variable P56-COD-NAZ-QLFC-PROF
     * 
     */
    String getCodNazQlfcProf();

    void setCodNazQlfcProf(String codNazQlfcProf);

    /**
     * Nullable property for P56-COD-NAZ-QLFC-PROF
     * 
     */
    String getCodNazQlfcProfObj();

    void setCodNazQlfcProfObj(String codNazQlfcProfObj);

    /**
     * Host Variable P56-COD-PRV-QLFC-PROF
     * 
     */
    String getCodPrvQlfcProf();

    void setCodPrvQlfcProf(String codPrvQlfcProf);

    /**
     * Nullable property for P56-COD-PRV-QLFC-PROF
     * 
     */
    String getCodPrvQlfcProfObj();

    void setCodPrvQlfcProfObj(String codPrvQlfcProfObj);

    /**
     * Host Variable P56-FNT-REDD
     * 
     */
    String getFntRedd();

    void setFntRedd(String fntRedd);

    /**
     * Nullable property for P56-FNT-REDD
     * 
     */
    String getFntReddObj();

    void setFntReddObj(String fntReddObj);

    /**
     * Host Variable P56-REDD-FATT-ANNU
     * 
     */
    AfDecimal getReddFattAnnu();

    void setReddFattAnnu(AfDecimal reddFattAnnu);

    /**
     * Nullable property for P56-REDD-FATT-ANNU
     * 
     */
    AfDecimal getReddFattAnnuObj();

    void setReddFattAnnuObj(AfDecimal reddFattAnnuObj);

    /**
     * Host Variable P56-COD-ATECO
     * 
     */
    String getCodAteco();

    void setCodAteco(String codAteco);

    /**
     * Nullable property for P56-COD-ATECO
     * 
     */
    String getCodAtecoObj();

    void setCodAtecoObj(String codAtecoObj);

    /**
     * Host Variable P56-VALUT-COLL
     * 
     */
    String getValutColl();

    void setValutColl(String valutColl);

    /**
     * Nullable property for P56-VALUT-COLL
     * 
     */
    String getValutCollObj();

    void setValutCollObj(String valutCollObj);

    /**
     * Host Variable P56-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P56-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P56-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable P56-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P56-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable P56-LUOGO-COSTITUZIONE-VCHAR
     * 
     */
    String getLuogoCostituzioneVchar();

    void setLuogoCostituzioneVchar(String luogoCostituzioneVchar);

    /**
     * Nullable property for P56-LUOGO-COSTITUZIONE-VCHAR
     * 
     */
    String getLuogoCostituzioneVcharObj();

    void setLuogoCostituzioneVcharObj(String luogoCostituzioneVcharObj);

    /**
     * Host Variable P56-TP-MOVI
     * 
     */
    int getTpMovi();

    void setTpMovi(int tpMovi);

    /**
     * Nullable property for P56-TP-MOVI
     * 
     */
    Integer getTpMoviObj();

    void setTpMoviObj(Integer tpMoviObj);

    /**
     * Host Variable P56-FL-RAG-RAPP
     * 
     */
    char getFlRagRapp();

    void setFlRagRapp(char flRagRapp);

    /**
     * Nullable property for P56-FL-RAG-RAPP
     * 
     */
    Character getFlRagRappObj();

    void setFlRagRappObj(Character flRagRappObj);

    /**
     * Host Variable P56-FL-PRSZ-TIT-EFF
     * 
     */
    char getFlPrszTitEff();

    void setFlPrszTitEff(char flPrszTitEff);

    /**
     * Nullable property for P56-FL-PRSZ-TIT-EFF
     * 
     */
    Character getFlPrszTitEffObj();

    void setFlPrszTitEffObj(Character flPrszTitEffObj);

    /**
     * Host Variable P56-TP-MOT-RISC
     * 
     */
    String getTpMotRisc();

    void setTpMotRisc(String tpMotRisc);

    /**
     * Nullable property for P56-TP-MOT-RISC
     * 
     */
    String getTpMotRiscObj();

    void setTpMotRiscObj(String tpMotRiscObj);

    /**
     * Host Variable P56-TP-PNT-VND
     * 
     */
    String getTpPntVnd();

    void setTpPntVnd(String tpPntVnd);

    /**
     * Nullable property for P56-TP-PNT-VND
     * 
     */
    String getTpPntVndObj();

    void setTpPntVndObj(String tpPntVndObj);

    /**
     * Host Variable P56-TP-ADEG-VER
     * 
     */
    String getTpAdegVer();

    void setTpAdegVer(String tpAdegVer);

    /**
     * Nullable property for P56-TP-ADEG-VER
     * 
     */
    String getTpAdegVerObj();

    void setTpAdegVerObj(String tpAdegVerObj);

    /**
     * Host Variable P56-TP-RELA-ESEC
     * 
     */
    String getTpRelaEsec();

    void setTpRelaEsec(String tpRelaEsec);

    /**
     * Nullable property for P56-TP-RELA-ESEC
     * 
     */
    String getTpRelaEsecObj();

    void setTpRelaEsecObj(String tpRelaEsecObj);

    /**
     * Host Variable P56-TP-SCO-FIN-RAPP
     * 
     */
    String getTpScoFinRapp();

    void setTpScoFinRapp(String tpScoFinRapp);

    /**
     * Nullable property for P56-TP-SCO-FIN-RAPP
     * 
     */
    String getTpScoFinRappObj();

    void setTpScoFinRappObj(String tpScoFinRappObj);

    /**
     * Host Variable P56-FL-PRSZ-3O-PAGAT
     * 
     */
    char getFlPrsz3oPagat();

    void setFlPrsz3oPagat(char flPrsz3oPagat);

    /**
     * Nullable property for P56-FL-PRSZ-3O-PAGAT
     * 
     */
    Character getFlPrsz3oPagatObj();

    void setFlPrsz3oPagatObj(Character flPrsz3oPagatObj);

    /**
     * Host Variable P56-AREA-GEO-PROV-FND
     * 
     */
    String getAreaGeoProvFnd();

    void setAreaGeoProvFnd(String areaGeoProvFnd);

    /**
     * Nullable property for P56-AREA-GEO-PROV-FND
     * 
     */
    String getAreaGeoProvFndObj();

    void setAreaGeoProvFndObj(String areaGeoProvFndObj);

    /**
     * Host Variable P56-TP-DEST-FND
     * 
     */
    String getTpDestFnd();

    void setTpDestFnd(String tpDestFnd);

    /**
     * Nullable property for P56-TP-DEST-FND
     * 
     */
    String getTpDestFndObj();

    void setTpDestFndObj(String tpDestFndObj);

    /**
     * Host Variable P56-FL-PAESE-RESID-AUT
     * 
     */
    char getFlPaeseResidAut();

    void setFlPaeseResidAut(char flPaeseResidAut);

    /**
     * Nullable property for P56-FL-PAESE-RESID-AUT
     * 
     */
    Character getFlPaeseResidAutObj();

    void setFlPaeseResidAutObj(Character flPaeseResidAutObj);

    /**
     * Host Variable P56-FL-PAESE-CIT-AUT
     * 
     */
    char getFlPaeseCitAut();

    void setFlPaeseCitAut(char flPaeseCitAut);

    /**
     * Nullable property for P56-FL-PAESE-CIT-AUT
     * 
     */
    Character getFlPaeseCitAutObj();

    void setFlPaeseCitAutObj(Character flPaeseCitAutObj);

    /**
     * Host Variable P56-FL-PAESE-NAZ-AUT
     * 
     */
    char getFlPaeseNazAut();

    void setFlPaeseNazAut(char flPaeseNazAut);

    /**
     * Nullable property for P56-FL-PAESE-NAZ-AUT
     * 
     */
    Character getFlPaeseNazAutObj();

    void setFlPaeseNazAutObj(Character flPaeseNazAutObj);

    /**
     * Host Variable P56-COD-PROF-PREC
     * 
     */
    String getCodProfPrec();

    void setCodProfPrec(String codProfPrec);

    /**
     * Nullable property for P56-COD-PROF-PREC
     * 
     */
    String getCodProfPrecObj();

    void setCodProfPrecObj(String codProfPrecObj);

    /**
     * Host Variable P56-FL-AUT-PEP
     * 
     */
    char getFlAutPep();

    void setFlAutPep(char flAutPep);

    /**
     * Nullable property for P56-FL-AUT-PEP
     * 
     */
    Character getFlAutPepObj();

    void setFlAutPepObj(Character flAutPepObj);

    /**
     * Host Variable P56-FL-IMP-CAR-PUB
     * 
     */
    char getFlImpCarPub();

    void setFlImpCarPub(char flImpCarPub);

    /**
     * Nullable property for P56-FL-IMP-CAR-PUB
     * 
     */
    Character getFlImpCarPubObj();

    void setFlImpCarPubObj(Character flImpCarPubObj);

    /**
     * Host Variable P56-FL-LIS-TERR-SORV
     * 
     */
    char getFlLisTerrSorv();

    void setFlLisTerrSorv(char flLisTerrSorv);

    /**
     * Nullable property for P56-FL-LIS-TERR-SORV
     * 
     */
    Character getFlLisTerrSorvObj();

    void setFlLisTerrSorvObj(Character flLisTerrSorvObj);

    /**
     * Host Variable P56-TP-SIT-FIN-PAT
     * 
     */
    String getTpSitFinPat();

    void setTpSitFinPat(String tpSitFinPat);

    /**
     * Nullable property for P56-TP-SIT-FIN-PAT
     * 
     */
    String getTpSitFinPatObj();

    void setTpSitFinPatObj(String tpSitFinPatObj);

    /**
     * Host Variable P56-TP-SIT-FIN-PAT-CON
     * 
     */
    String getTpSitFinPatCon();

    void setTpSitFinPatCon(String tpSitFinPatCon);

    /**
     * Nullable property for P56-TP-SIT-FIN-PAT-CON
     * 
     */
    String getTpSitFinPatConObj();

    void setTpSitFinPatConObj(String tpSitFinPatConObj);

    /**
     * Host Variable P56-IMP-TOT-AFF-UTIL
     * 
     */
    AfDecimal getImpTotAffUtil();

    void setImpTotAffUtil(AfDecimal impTotAffUtil);

    /**
     * Nullable property for P56-IMP-TOT-AFF-UTIL
     * 
     */
    AfDecimal getImpTotAffUtilObj();

    void setImpTotAffUtilObj(AfDecimal impTotAffUtilObj);

    /**
     * Host Variable P56-IMP-TOT-FIN-UTIL
     * 
     */
    AfDecimal getImpTotFinUtil();

    void setImpTotFinUtil(AfDecimal impTotFinUtil);

    /**
     * Nullable property for P56-IMP-TOT-FIN-UTIL
     * 
     */
    AfDecimal getImpTotFinUtilObj();

    void setImpTotFinUtilObj(AfDecimal impTotFinUtilObj);

    /**
     * Host Variable P56-IMP-TOT-AFF-ACC
     * 
     */
    AfDecimal getImpTotAffAcc();

    void setImpTotAffAcc(AfDecimal impTotAffAcc);

    /**
     * Nullable property for P56-IMP-TOT-AFF-ACC
     * 
     */
    AfDecimal getImpTotAffAccObj();

    void setImpTotAffAccObj(AfDecimal impTotAffAccObj);

    /**
     * Host Variable P56-IMP-TOT-FIN-ACC
     * 
     */
    AfDecimal getImpTotFinAcc();

    void setImpTotFinAcc(AfDecimal impTotFinAcc);

    /**
     * Nullable property for P56-IMP-TOT-FIN-ACC
     * 
     */
    AfDecimal getImpTotFinAccObj();

    void setImpTotFinAccObj(AfDecimal impTotFinAccObj);

    /**
     * Host Variable P56-TP-FRM-GIUR-SAV
     * 
     */
    String getTpFrmGiurSav();

    void setTpFrmGiurSav(String tpFrmGiurSav);

    /**
     * Nullable property for P56-TP-FRM-GIUR-SAV
     * 
     */
    String getTpFrmGiurSavObj();

    void setTpFrmGiurSavObj(String tpFrmGiurSavObj);

    /**
     * Host Variable P56-REG-COLL-POLI
     * 
     */
    String getRegCollPoli();

    void setRegCollPoli(String regCollPoli);

    /**
     * Nullable property for P56-REG-COLL-POLI
     * 
     */
    String getRegCollPoliObj();

    void setRegCollPoliObj(String regCollPoliObj);

    /**
     * Host Variable P56-NUM-TEL
     * 
     */
    String getNumTel();

    void setNumTel(String numTel);

    /**
     * Nullable property for P56-NUM-TEL
     * 
     */
    String getNumTelObj();

    void setNumTelObj(String numTelObj);

    /**
     * Host Variable P56-NUM-DIP
     * 
     */
    int getNumDip();

    void setNumDip(int numDip);

    /**
     * Nullable property for P56-NUM-DIP
     * 
     */
    Integer getNumDipObj();

    void setNumDipObj(Integer numDipObj);

    /**
     * Host Variable P56-TP-SIT-FAM-CONV
     * 
     */
    String getTpSitFamConv();

    void setTpSitFamConv(String tpSitFamConv);

    /**
     * Nullable property for P56-TP-SIT-FAM-CONV
     * 
     */
    String getTpSitFamConvObj();

    void setTpSitFamConvObj(String tpSitFamConvObj);

    /**
     * Host Variable P56-COD-PROF-CON
     * 
     */
    String getCodProfCon();

    void setCodProfCon(String codProfCon);

    /**
     * Nullable property for P56-COD-PROF-CON
     * 
     */
    String getCodProfConObj();

    void setCodProfConObj(String codProfConObj);

    /**
     * Host Variable P56-FL-ES-PROC-PEN
     * 
     */
    char getFlEsProcPen();

    void setFlEsProcPen(char flEsProcPen);

    /**
     * Nullable property for P56-FL-ES-PROC-PEN
     * 
     */
    Character getFlEsProcPenObj();

    void setFlEsProcPenObj(Character flEsProcPenObj);

    /**
     * Host Variable P56-TP-COND-CLIENTE
     * 
     */
    String getTpCondCliente();

    void setTpCondCliente(String tpCondCliente);

    /**
     * Nullable property for P56-TP-COND-CLIENTE
     * 
     */
    String getTpCondClienteObj();

    void setTpCondClienteObj(String tpCondClienteObj);

    /**
     * Host Variable P56-COD-SAE
     * 
     */
    String getCodSae();

    void setCodSae(String codSae);

    /**
     * Nullable property for P56-COD-SAE
     * 
     */
    String getCodSaeObj();

    void setCodSaeObj(String codSaeObj);

    /**
     * Host Variable P56-TP-OPER-ESTERO
     * 
     */
    String getTpOperEstero();

    void setTpOperEstero(String tpOperEstero);

    /**
     * Nullable property for P56-TP-OPER-ESTERO
     * 
     */
    String getTpOperEsteroObj();

    void setTpOperEsteroObj(String tpOperEsteroObj);

    /**
     * Host Variable P56-STAT-OPER-ESTERO
     * 
     */
    String getStatOperEstero();

    void setStatOperEstero(String statOperEstero);

    /**
     * Nullable property for P56-STAT-OPER-ESTERO
     * 
     */
    String getStatOperEsteroObj();

    void setStatOperEsteroObj(String statOperEsteroObj);

    /**
     * Host Variable P56-COD-PRV-SVOL-ATT
     * 
     */
    String getCodPrvSvolAtt();

    void setCodPrvSvolAtt(String codPrvSvolAtt);

    /**
     * Nullable property for P56-COD-PRV-SVOL-ATT
     * 
     */
    String getCodPrvSvolAttObj();

    void setCodPrvSvolAttObj(String codPrvSvolAttObj);

    /**
     * Host Variable P56-COD-STAT-SVOL-ATT
     * 
     */
    String getCodStatSvolAtt();

    void setCodStatSvolAtt(String codStatSvolAtt);

    /**
     * Nullable property for P56-COD-STAT-SVOL-ATT
     * 
     */
    String getCodStatSvolAttObj();

    void setCodStatSvolAttObj(String codStatSvolAttObj);

    /**
     * Host Variable P56-TP-SOC
     * 
     */
    String getTpSoc();

    void setTpSoc(String tpSoc);

    /**
     * Nullable property for P56-TP-SOC
     * 
     */
    String getTpSocObj();

    void setTpSocObj(String tpSocObj);

    /**
     * Host Variable P56-FL-IND-SOC-QUOT
     * 
     */
    char getFlIndSocQuot();

    void setFlIndSocQuot(char flIndSocQuot);

    /**
     * Nullable property for P56-FL-IND-SOC-QUOT
     * 
     */
    Character getFlIndSocQuotObj();

    void setFlIndSocQuotObj(Character flIndSocQuotObj);

    /**
     * Host Variable P56-TP-SIT-GIUR
     * 
     */
    String getTpSitGiur();

    void setTpSitGiur(String tpSitGiur);

    /**
     * Nullable property for P56-TP-SIT-GIUR
     * 
     */
    String getTpSitGiurObj();

    void setTpSitGiurObj(String tpSitGiurObj);

    /**
     * Host Variable P56-PC-QUO-DET-TIT-EFF
     * 
     */
    AfDecimal getPcQuoDetTitEff();

    void setPcQuoDetTitEff(AfDecimal pcQuoDetTitEff);

    /**
     * Nullable property for P56-PC-QUO-DET-TIT-EFF
     * 
     */
    AfDecimal getPcQuoDetTitEffObj();

    void setPcQuoDetTitEffObj(AfDecimal pcQuoDetTitEffObj);

    /**
     * Host Variable P56-TP-PRFL-RSH-PEP
     * 
     */
    String getTpPrflRshPep();

    void setTpPrflRshPep(String tpPrflRshPep);

    /**
     * Nullable property for P56-TP-PRFL-RSH-PEP
     * 
     */
    String getTpPrflRshPepObj();

    void setTpPrflRshPepObj(String tpPrflRshPepObj);

    /**
     * Host Variable P56-TP-PEP
     * 
     */
    String getTpPep();

    void setTpPep(String tpPep);

    /**
     * Nullable property for P56-TP-PEP
     * 
     */
    String getTpPepObj();

    void setTpPepObj(String tpPepObj);

    /**
     * Host Variable P56-FL-NOT-PREG
     * 
     */
    char getFlNotPreg();

    void setFlNotPreg(char flNotPreg);

    /**
     * Nullable property for P56-FL-NOT-PREG
     * 
     */
    Character getFlNotPregObj();

    void setFlNotPregObj(Character flNotPregObj);

    /**
     * Host Variable P56-DT-INI-FNT-REDD-DB
     * 
     */
    String getDtIniFntReddDb();

    void setDtIniFntReddDb(String dtIniFntReddDb);

    /**
     * Nullable property for P56-DT-INI-FNT-REDD-DB
     * 
     */
    String getDtIniFntReddDbObj();

    void setDtIniFntReddDbObj(String dtIniFntReddDbObj);

    /**
     * Host Variable P56-FNT-REDD-2
     * 
     */
    String getFntRedd2();

    void setFntRedd2(String fntRedd2);

    /**
     * Nullable property for P56-FNT-REDD-2
     * 
     */
    String getFntRedd2Obj();

    void setFntRedd2Obj(String fntRedd2Obj);

    /**
     * Host Variable P56-DT-INI-FNT-REDD-2-DB
     * 
     */
    String getDtIniFntRedd2Db();

    void setDtIniFntRedd2Db(String dtIniFntRedd2Db);

    /**
     * Nullable property for P56-DT-INI-FNT-REDD-2-DB
     * 
     */
    String getDtIniFntRedd2DbObj();

    void setDtIniFntRedd2DbObj(String dtIniFntRedd2DbObj);

    /**
     * Host Variable P56-FNT-REDD-3
     * 
     */
    String getFntRedd3();

    void setFntRedd3(String fntRedd3);

    /**
     * Nullable property for P56-FNT-REDD-3
     * 
     */
    String getFntRedd3Obj();

    void setFntRedd3Obj(String fntRedd3Obj);

    /**
     * Host Variable P56-DT-INI-FNT-REDD-3-DB
     * 
     */
    String getDtIniFntRedd3Db();

    void setDtIniFntRedd3Db(String dtIniFntRedd3Db);

    /**
     * Nullable property for P56-DT-INI-FNT-REDD-3-DB
     * 
     */
    String getDtIniFntRedd3DbObj();

    void setDtIniFntRedd3DbObj(String dtIniFntRedd3DbObj);

    /**
     * Host Variable P56-MOT-ASS-TIT-EFF-VCHAR
     * 
     */
    String getMotAssTitEffVchar();

    void setMotAssTitEffVchar(String motAssTitEffVchar);

    /**
     * Nullable property for P56-MOT-ASS-TIT-EFF-VCHAR
     * 
     */
    String getMotAssTitEffVcharObj();

    void setMotAssTitEffVcharObj(String motAssTitEffVcharObj);

    /**
     * Host Variable P56-FIN-COSTITUZIONE-VCHAR
     * 
     */
    String getFinCostituzioneVchar();

    void setFinCostituzioneVchar(String finCostituzioneVchar);

    /**
     * Nullable property for P56-FIN-COSTITUZIONE-VCHAR
     * 
     */
    String getFinCostituzioneVcharObj();

    void setFinCostituzioneVcharObj(String finCostituzioneVcharObj);

    /**
     * Host Variable P56-DESC-IMP-CAR-PUB-VCHAR
     * 
     */
    String getDescImpCarPubVchar();

    void setDescImpCarPubVchar(String descImpCarPubVchar);

    /**
     * Nullable property for P56-DESC-IMP-CAR-PUB-VCHAR
     * 
     */
    String getDescImpCarPubVcharObj();

    void setDescImpCarPubVcharObj(String descImpCarPubVcharObj);

    /**
     * Host Variable P56-DESC-SCO-FIN-RAPP-VCHAR
     * 
     */
    String getDescScoFinRappVchar();

    void setDescScoFinRappVchar(String descScoFinRappVchar);

    /**
     * Nullable property for P56-DESC-SCO-FIN-RAPP-VCHAR
     * 
     */
    String getDescScoFinRappVcharObj();

    void setDescScoFinRappVcharObj(String descScoFinRappVcharObj);

    /**
     * Host Variable P56-DESC-PROC-PNL-VCHAR
     * 
     */
    String getDescProcPnlVchar();

    void setDescProcPnlVchar(String descProcPnlVchar);

    /**
     * Nullable property for P56-DESC-PROC-PNL-VCHAR
     * 
     */
    String getDescProcPnlVcharObj();

    void setDescProcPnlVcharObj(String descProcPnlVcharObj);

    /**
     * Host Variable P56-DESC-NOT-PREG-VCHAR
     * 
     */
    String getDescNotPregVchar();

    void setDescNotPregVchar(String descNotPregVchar);

    /**
     * Nullable property for P56-DESC-NOT-PREG-VCHAR
     * 
     */
    String getDescNotPregVcharObj();

    void setDescNotPregVcharObj(String descNotPregVcharObj);

    /**
     * Host Variable P56-ID-ASSICURATI
     * 
     */
    int getIdAssicurati();

    void setIdAssicurati(int idAssicurati);

    /**
     * Nullable property for P56-ID-ASSICURATI
     * 
     */
    Integer getIdAssicuratiObj();

    void setIdAssicuratiObj(Integer idAssicuratiObj);

    /**
     * Host Variable P56-REDD-CON
     * 
     */
    AfDecimal getReddCon();

    void setReddCon(AfDecimal reddCon);

    /**
     * Nullable property for P56-REDD-CON
     * 
     */
    AfDecimal getReddConObj();

    void setReddConObj(AfDecimal reddConObj);

    /**
     * Host Variable P56-DESC-LIB-MOT-RISC-VCHAR
     * 
     */
    String getDescLibMotRiscVchar();

    void setDescLibMotRiscVchar(String descLibMotRiscVchar);

    /**
     * Nullable property for P56-DESC-LIB-MOT-RISC-VCHAR
     * 
     */
    String getDescLibMotRiscVcharObj();

    void setDescLibMotRiscVcharObj(String descLibMotRiscVcharObj);

    /**
     * Host Variable P56-TP-MOT-ASS-TIT-EFF
     * 
     */
    String getTpMotAssTitEff();

    void setTpMotAssTitEff(String tpMotAssTitEff);

    /**
     * Nullable property for P56-TP-MOT-ASS-TIT-EFF
     * 
     */
    String getTpMotAssTitEffObj();

    void setTpMotAssTitEffObj(String tpMotAssTitEffObj);

    /**
     * Host Variable P56-TP-RAG-RAPP
     * 
     */
    String getTpRagRapp();

    void setTpRagRapp(String tpRagRapp);

    /**
     * Nullable property for P56-TP-RAG-RAPP
     * 
     */
    String getTpRagRappObj();

    void setTpRagRappObj(String tpRagRappObj);

    /**
     * Host Variable P56-COD-CAN
     * 
     */
    int getCodCan();

    void setCodCan(int codCan);

    /**
     * Nullable property for P56-COD-CAN
     * 
     */
    Integer getCodCanObj();

    void setCodCanObj(Integer codCanObj);

    /**
     * Host Variable P56-TP-FIN-COST
     * 
     */
    String getTpFinCost();

    void setTpFinCost(String tpFinCost);

    /**
     * Nullable property for P56-TP-FIN-COST
     * 
     */
    String getTpFinCostObj();

    void setTpFinCostObj(String tpFinCostObj);

    /**
     * Host Variable P56-NAZ-DEST-FND
     * 
     */
    String getNazDestFnd();

    void setNazDestFnd(String nazDestFnd);

    /**
     * Nullable property for P56-NAZ-DEST-FND
     * 
     */
    String getNazDestFndObj();

    void setNazDestFndObj(String nazDestFndObj);

    /**
     * Host Variable P56-FL-AU-FATCA-AEOI
     * 
     */
    char getFlAuFatcaAeoi();

    void setFlAuFatcaAeoi(char flAuFatcaAeoi);

    /**
     * Nullable property for P56-FL-AU-FATCA-AEOI
     * 
     */
    Character getFlAuFatcaAeoiObj();

    void setFlAuFatcaAeoiObj(Character flAuFatcaAeoiObj);

    /**
     * Host Variable P56-TP-CAR-FIN-GIUR
     * 
     */
    String getTpCarFinGiur();

    void setTpCarFinGiur(String tpCarFinGiur);

    /**
     * Nullable property for P56-TP-CAR-FIN-GIUR
     * 
     */
    String getTpCarFinGiurObj();

    void setTpCarFinGiurObj(String tpCarFinGiurObj);

    /**
     * Host Variable P56-TP-CAR-FIN-GIUR-AT
     * 
     */
    String getTpCarFinGiurAt();

    void setTpCarFinGiurAt(String tpCarFinGiurAt);

    /**
     * Nullable property for P56-TP-CAR-FIN-GIUR-AT
     * 
     */
    String getTpCarFinGiurAtObj();

    void setTpCarFinGiurAtObj(String tpCarFinGiurAtObj);

    /**
     * Host Variable P56-TP-CAR-FIN-GIUR-PA
     * 
     */
    String getTpCarFinGiurPa();

    void setTpCarFinGiurPa(String tpCarFinGiurPa);

    /**
     * Nullable property for P56-TP-CAR-FIN-GIUR-PA
     * 
     */
    String getTpCarFinGiurPaObj();

    void setTpCarFinGiurPaObj(String tpCarFinGiurPaObj);

    /**
     * Host Variable P56-FL-ISTITUZ-FIN
     * 
     */
    char getFlIstituzFin();

    void setFlIstituzFin(char flIstituzFin);

    /**
     * Nullable property for P56-FL-ISTITUZ-FIN
     * 
     */
    Character getFlIstituzFinObj();

    void setFlIstituzFinObj(Character flIstituzFinObj);

    /**
     * Host Variable P56-TP-ORI-FND-TIT-EFF
     * 
     */
    String getTpOriFndTitEff();

    void setTpOriFndTitEff(String tpOriFndTitEff);

    /**
     * Nullable property for P56-TP-ORI-FND-TIT-EFF
     * 
     */
    String getTpOriFndTitEffObj();

    void setTpOriFndTitEffObj(String tpOriFndTitEffObj);

    /**
     * Host Variable P56-PC-ESP-AG-PA-MSC
     * 
     */
    AfDecimal getPcEspAgPaMsc();

    void setPcEspAgPaMsc(AfDecimal pcEspAgPaMsc);

    /**
     * Nullable property for P56-PC-ESP-AG-PA-MSC
     * 
     */
    AfDecimal getPcEspAgPaMscObj();

    void setPcEspAgPaMscObj(AfDecimal pcEspAgPaMscObj);

    /**
     * Host Variable P56-FL-PR-TR-USA
     * 
     */
    char getFlPrTrUsa();

    void setFlPrTrUsa(char flPrTrUsa);

    /**
     * Nullable property for P56-FL-PR-TR-USA
     * 
     */
    Character getFlPrTrUsaObj();

    void setFlPrTrUsaObj(Character flPrTrUsaObj);

    /**
     * Host Variable P56-FL-PR-TR-NO-USA
     * 
     */
    char getFlPrTrNoUsa();

    void setFlPrTrNoUsa(char flPrTrNoUsa);

    /**
     * Nullable property for P56-FL-PR-TR-NO-USA
     * 
     */
    Character getFlPrTrNoUsaObj();

    void setFlPrTrNoUsaObj(Character flPrTrNoUsaObj);

    /**
     * Host Variable P56-PC-RIP-PAT-AS-VITA
     * 
     */
    AfDecimal getPcRipPatAsVita();

    void setPcRipPatAsVita(AfDecimal pcRipPatAsVita);

    /**
     * Nullable property for P56-PC-RIP-PAT-AS-VITA
     * 
     */
    AfDecimal getPcRipPatAsVitaObj();

    void setPcRipPatAsVitaObj(AfDecimal pcRipPatAsVitaObj);

    /**
     * Host Variable P56-PC-RIP-PAT-IM
     * 
     */
    AfDecimal getPcRipPatIm();

    void setPcRipPatIm(AfDecimal pcRipPatIm);

    /**
     * Nullable property for P56-PC-RIP-PAT-IM
     * 
     */
    AfDecimal getPcRipPatImObj();

    void setPcRipPatImObj(AfDecimal pcRipPatImObj);

    /**
     * Host Variable P56-PC-RIP-PAT-SET-IM
     * 
     */
    AfDecimal getPcRipPatSetIm();

    void setPcRipPatSetIm(AfDecimal pcRipPatSetIm);

    /**
     * Nullable property for P56-PC-RIP-PAT-SET-IM
     * 
     */
    AfDecimal getPcRipPatSetImObj();

    void setPcRipPatSetImObj(AfDecimal pcRipPatSetImObj);

    /**
     * Host Variable P56-TP-STATUS-AEOI
     * 
     */
    String getTpStatusAeoi();

    void setTpStatusAeoi(String tpStatusAeoi);

    /**
     * Nullable property for P56-TP-STATUS-AEOI
     * 
     */
    String getTpStatusAeoiObj();

    void setTpStatusAeoiObj(String tpStatusAeoiObj);

    /**
     * Host Variable P56-TP-STATUS-FATCA
     * 
     */
    String getTpStatusFatca();

    void setTpStatusFatca(String tpStatusFatca);

    /**
     * Nullable property for P56-TP-STATUS-FATCA
     * 
     */
    String getTpStatusFatcaObj();

    void setTpStatusFatcaObj(String tpStatusFatcaObj);

    /**
     * Host Variable P56-FL-RAPP-PA-MSC
     * 
     */
    char getFlRappPaMsc();

    void setFlRappPaMsc(char flRappPaMsc);

    /**
     * Nullable property for P56-FL-RAPP-PA-MSC
     * 
     */
    Character getFlRappPaMscObj();

    void setFlRappPaMscObj(Character flRappPaMscObj);

    /**
     * Host Variable P56-COD-COMUN-SVOL-ATT
     * 
     */
    String getCodComunSvolAtt();

    void setCodComunSvolAtt(String codComunSvolAtt);

    /**
     * Nullable property for P56-COD-COMUN-SVOL-ATT
     * 
     */
    String getCodComunSvolAttObj();

    void setCodComunSvolAttObj(String codComunSvolAttObj);

    /**
     * Host Variable P56-TP-DT-1O-CON-CLI
     * 
     */
    String getTpDt1oConCli();

    void setTpDt1oConCli(String tpDt1oConCli);

    /**
     * Nullable property for P56-TP-DT-1O-CON-CLI
     * 
     */
    String getTpDt1oConCliObj();

    void setTpDt1oConCliObj(String tpDt1oConCliObj);

    /**
     * Host Variable P56-TP-MOD-EN-RELA-INT
     * 
     */
    String getTpModEnRelaInt();

    void setTpModEnRelaInt(String tpModEnRelaInt);

    /**
     * Nullable property for P56-TP-MOD-EN-RELA-INT
     * 
     */
    String getTpModEnRelaIntObj();

    void setTpModEnRelaIntObj(String tpModEnRelaIntObj);

    /**
     * Host Variable P56-TP-REDD-ANNU-LRD
     * 
     */
    String getTpReddAnnuLrd();

    void setTpReddAnnuLrd(String tpReddAnnuLrd);

    /**
     * Nullable property for P56-TP-REDD-ANNU-LRD
     * 
     */
    String getTpReddAnnuLrdObj();

    void setTpReddAnnuLrdObj(String tpReddAnnuLrdObj);

    /**
     * Host Variable P56-TP-REDD-CON
     * 
     */
    String getTpReddCon();

    void setTpReddCon(String tpReddCon);

    /**
     * Nullable property for P56-TP-REDD-CON
     * 
     */
    String getTpReddConObj();

    void setTpReddConObj(String tpReddConObj);

    /**
     * Host Variable P56-TP-OPER-SOC-FID
     * 
     */
    String getTpOperSocFid();

    void setTpOperSocFid(String tpOperSocFid);

    /**
     * Nullable property for P56-TP-OPER-SOC-FID
     * 
     */
    String getTpOperSocFidObj();

    void setTpOperSocFidObj(String tpOperSocFidObj);

    /**
     * Host Variable P56-COD-PA-ESP-MSC-1
     * 
     */
    String getCodPaEspMsc1();

    void setCodPaEspMsc1(String codPaEspMsc1);

    /**
     * Nullable property for P56-COD-PA-ESP-MSC-1
     * 
     */
    String getCodPaEspMsc1Obj();

    void setCodPaEspMsc1Obj(String codPaEspMsc1Obj);

    /**
     * Host Variable P56-IMP-PA-ESP-MSC-1
     * 
     */
    AfDecimal getImpPaEspMsc1();

    void setImpPaEspMsc1(AfDecimal impPaEspMsc1);

    /**
     * Nullable property for P56-IMP-PA-ESP-MSC-1
     * 
     */
    AfDecimal getImpPaEspMsc1Obj();

    void setImpPaEspMsc1Obj(AfDecimal impPaEspMsc1Obj);

    /**
     * Host Variable P56-COD-PA-ESP-MSC-2
     * 
     */
    String getCodPaEspMsc2();

    void setCodPaEspMsc2(String codPaEspMsc2);

    /**
     * Nullable property for P56-COD-PA-ESP-MSC-2
     * 
     */
    String getCodPaEspMsc2Obj();

    void setCodPaEspMsc2Obj(String codPaEspMsc2Obj);

    /**
     * Host Variable P56-IMP-PA-ESP-MSC-2
     * 
     */
    AfDecimal getImpPaEspMsc2();

    void setImpPaEspMsc2(AfDecimal impPaEspMsc2);

    /**
     * Nullable property for P56-IMP-PA-ESP-MSC-2
     * 
     */
    AfDecimal getImpPaEspMsc2Obj();

    void setImpPaEspMsc2Obj(AfDecimal impPaEspMsc2Obj);

    /**
     * Host Variable P56-COD-PA-ESP-MSC-3
     * 
     */
    String getCodPaEspMsc3();

    void setCodPaEspMsc3(String codPaEspMsc3);

    /**
     * Nullable property for P56-COD-PA-ESP-MSC-3
     * 
     */
    String getCodPaEspMsc3Obj();

    void setCodPaEspMsc3Obj(String codPaEspMsc3Obj);

    /**
     * Host Variable P56-IMP-PA-ESP-MSC-3
     * 
     */
    AfDecimal getImpPaEspMsc3();

    void setImpPaEspMsc3(AfDecimal impPaEspMsc3);

    /**
     * Nullable property for P56-IMP-PA-ESP-MSC-3
     * 
     */
    AfDecimal getImpPaEspMsc3Obj();

    void setImpPaEspMsc3Obj(AfDecimal impPaEspMsc3Obj);

    /**
     * Host Variable P56-COD-PA-ESP-MSC-4
     * 
     */
    String getCodPaEspMsc4();

    void setCodPaEspMsc4(String codPaEspMsc4);

    /**
     * Nullable property for P56-COD-PA-ESP-MSC-4
     * 
     */
    String getCodPaEspMsc4Obj();

    void setCodPaEspMsc4Obj(String codPaEspMsc4Obj);

    /**
     * Host Variable P56-IMP-PA-ESP-MSC-4
     * 
     */
    AfDecimal getImpPaEspMsc4();

    void setImpPaEspMsc4(AfDecimal impPaEspMsc4);

    /**
     * Nullable property for P56-IMP-PA-ESP-MSC-4
     * 
     */
    AfDecimal getImpPaEspMsc4Obj();

    void setImpPaEspMsc4Obj(AfDecimal impPaEspMsc4Obj);

    /**
     * Host Variable P56-COD-PA-ESP-MSC-5
     * 
     */
    String getCodPaEspMsc5();

    void setCodPaEspMsc5(String codPaEspMsc5);

    /**
     * Nullable property for P56-COD-PA-ESP-MSC-5
     * 
     */
    String getCodPaEspMsc5Obj();

    void setCodPaEspMsc5Obj(String codPaEspMsc5Obj);

    /**
     * Host Variable P56-IMP-PA-ESP-MSC-5
     * 
     */
    AfDecimal getImpPaEspMsc5();

    void setImpPaEspMsc5(AfDecimal impPaEspMsc5);

    /**
     * Nullable property for P56-IMP-PA-ESP-MSC-5
     * 
     */
    AfDecimal getImpPaEspMsc5Obj();

    void setImpPaEspMsc5Obj(AfDecimal impPaEspMsc5Obj);

    /**
     * Host Variable P56-DESC-ORGN-FND-VCHAR
     * 
     */
    String getDescOrgnFndVchar();

    void setDescOrgnFndVchar(String descOrgnFndVchar);

    /**
     * Nullable property for P56-DESC-ORGN-FND-VCHAR
     * 
     */
    String getDescOrgnFndVcharObj();

    void setDescOrgnFndVcharObj(String descOrgnFndVcharObj);

    /**
     * Host Variable P56-COD-AUT-DUE-DIL
     * 
     */
    String getCodAutDueDil();

    void setCodAutDueDil(String codAutDueDil);

    /**
     * Nullable property for P56-COD-AUT-DUE-DIL
     * 
     */
    String getCodAutDueDilObj();

    void setCodAutDueDilObj(String codAutDueDilObj);

    /**
     * Host Variable P56-FL-PR-QUEST-FATCA
     * 
     */
    char getFlPrQuestFatca();

    void setFlPrQuestFatca(char flPrQuestFatca);

    /**
     * Nullable property for P56-FL-PR-QUEST-FATCA
     * 
     */
    Character getFlPrQuestFatcaObj();

    void setFlPrQuestFatcaObj(Character flPrQuestFatcaObj);

    /**
     * Host Variable P56-FL-PR-QUEST-AEOI
     * 
     */
    char getFlPrQuestAeoi();

    void setFlPrQuestAeoi(char flPrQuestAeoi);

    /**
     * Nullable property for P56-FL-PR-QUEST-AEOI
     * 
     */
    Character getFlPrQuestAeoiObj();

    void setFlPrQuestAeoiObj(Character flPrQuestAeoiObj);

    /**
     * Host Variable P56-FL-PR-QUEST-OFAC
     * 
     */
    char getFlPrQuestOfac();

    void setFlPrQuestOfac(char flPrQuestOfac);

    /**
     * Nullable property for P56-FL-PR-QUEST-OFAC
     * 
     */
    Character getFlPrQuestOfacObj();

    void setFlPrQuestOfacObj(Character flPrQuestOfacObj);

    /**
     * Host Variable P56-FL-PR-QUEST-KYC
     * 
     */
    char getFlPrQuestKyc();

    void setFlPrQuestKyc(char flPrQuestKyc);

    /**
     * Nullable property for P56-FL-PR-QUEST-KYC
     * 
     */
    Character getFlPrQuestKycObj();

    void setFlPrQuestKycObj(Character flPrQuestKycObj);

    /**
     * Host Variable P56-FL-PR-QUEST-MSCQ
     * 
     */
    char getFlPrQuestMscq();

    void setFlPrQuestMscq(char flPrQuestMscq);

    /**
     * Nullable property for P56-FL-PR-QUEST-MSCQ
     * 
     */
    Character getFlPrQuestMscqObj();

    void setFlPrQuestMscqObj(Character flPrQuestMscqObj);

    /**
     * Host Variable P56-TP-NOT-PREG
     * 
     */
    String getTpNotPreg();

    void setTpNotPreg(String tpNotPreg);

    /**
     * Nullable property for P56-TP-NOT-PREG
     * 
     */
    String getTpNotPregObj();

    void setTpNotPregObj(String tpNotPregObj);

    /**
     * Host Variable P56-TP-PROC-PNL
     * 
     */
    String getTpProcPnl();

    void setTpProcPnl(String tpProcPnl);

    /**
     * Nullable property for P56-TP-PROC-PNL
     * 
     */
    String getTpProcPnlObj();

    void setTpProcPnlObj(String tpProcPnlObj);

    /**
     * Host Variable P56-COD-IMP-CAR-PUB
     * 
     */
    String getCodImpCarPub();

    void setCodImpCarPub(String codImpCarPub);

    /**
     * Nullable property for P56-COD-IMP-CAR-PUB
     * 
     */
    String getCodImpCarPubObj();

    void setCodImpCarPubObj(String codImpCarPubObj);

    /**
     * Host Variable P56-OPRZ-SOSPETTE
     * 
     */
    char getOprzSospette();

    void setOprzSospette(char oprzSospette);

    /**
     * Nullable property for P56-OPRZ-SOSPETTE
     * 
     */
    Character getOprzSospetteObj();

    void setOprzSospetteObj(Character oprzSospetteObj);

    /**
     * Host Variable P56-ULT-FATT-ANNU
     * 
     */
    AfDecimal getUltFattAnnu();

    void setUltFattAnnu(AfDecimal ultFattAnnu);

    /**
     * Nullable property for P56-ULT-FATT-ANNU
     * 
     */
    AfDecimal getUltFattAnnuObj();

    void setUltFattAnnuObj(AfDecimal ultFattAnnuObj);

    /**
     * Host Variable P56-DESC-PEP-VCHAR
     * 
     */
    String getDescPepVchar();

    void setDescPepVchar(String descPepVchar);

    /**
     * Nullable property for P56-DESC-PEP-VCHAR
     * 
     */
    String getDescPepVcharObj();

    void setDescPepVcharObj(String descPepVcharObj);

    /**
     * Host Variable P56-NUM-TEL-2
     * 
     */
    String getNumTel2();

    void setNumTel2(String numTel2);

    /**
     * Nullable property for P56-NUM-TEL-2
     * 
     */
    String getNumTel2Obj();

    void setNumTel2Obj(String numTel2Obj);

    /**
     * Host Variable P56-IMP-AFI
     * 
     */
    AfDecimal getImpAfi();

    void setImpAfi(AfDecimal impAfi);

    /**
     * Nullable property for P56-IMP-AFI
     * 
     */
    AfDecimal getImpAfiObj();

    void setImpAfiObj(AfDecimal impAfiObj);

    /**
     * Host Variable P56-FL-NEW-PRO
     * 
     */
    char getFlNewPro();

    void setFlNewPro(char flNewPro);

    /**
     * Nullable property for P56-FL-NEW-PRO
     * 
     */
    Character getFlNewProObj();

    void setFlNewProObj(Character flNewProObj);

    /**
     * Host Variable P56-TP-MOT-CAMBIO-CNTR
     * 
     */
    String getTpMotCambioCntr();

    void setTpMotCambioCntr(String tpMotCambioCntr);

    /**
     * Nullable property for P56-TP-MOT-CAMBIO-CNTR
     * 
     */
    String getTpMotCambioCntrObj();

    void setTpMotCambioCntrObj(String tpMotCambioCntrObj);

    /**
     * Host Variable P56-DESC-MOT-CAMBIO-CN-VCHAR
     * 
     */
    String getDescMotCambioCnVchar();

    void setDescMotCambioCnVchar(String descMotCambioCnVchar);

    /**
     * Nullable property for P56-DESC-MOT-CAMBIO-CN-VCHAR
     * 
     */
    String getDescMotCambioCnVcharObj();

    void setDescMotCambioCnVcharObj(String descMotCambioCnVcharObj);

    /**
     * Host Variable P56-COD-SOGG
     * 
     */
    String getCodSogg();

    void setCodSogg(String codSogg);

    /**
     * Nullable property for P56-COD-SOGG
     * 
     */
    String getCodSoggObj();

    void setCodSoggObj(String codSoggObj);
};
