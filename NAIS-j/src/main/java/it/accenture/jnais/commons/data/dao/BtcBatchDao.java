package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBtcBatch;

/**
 * Data Access Object(DAO) for table [BTC_BATCH]
 * 
 */
public class BtcBatchDao extends BaseSqlDao<IBtcBatch> {

    private Cursor curBtc;
    private final IRowMapper<IBtcBatch> selectRecRm = buildNamedRowMapper(IBtcBatch.class, "idBatch", "btcProtocol", "dtInsDb", "userIns", "btcCodBatchType", "btcCodCompAnia", "codBatchState", "dtStartDbObj", "dtEndDbObj", "userStartObj", "idRichObj", "dtEffDbObj", "dataBatchVcharObj");

    public BtcBatchDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBtcBatch> getToClass() {
        return IBtcBatch.class;
    }

    public IBtcBatch selectRec(IBtcBatch iBtcBatch) {
        return buildQuery("selectRec").bind(iBtcBatch).rowMapper(selectRecRm).singleResult(iBtcBatch);
    }

    public DbAccessStatus insertRec(IBtcBatch iBtcBatch) {
        return buildQuery("insertRec").bind(iBtcBatch).executeInsert();
    }

    public DbAccessStatus updateRec(IBtcBatch iBtcBatch) {
        return buildQuery("updateRec").bind(iBtcBatch).executeUpdate();
    }

    public DbAccessStatus openCurBtc(IBtcBatch iBtcBatch) {
        curBtc = buildQuery("openCurBtc").bind(iBtcBatch).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurBtc() {
        return closeCursor(curBtc);
    }

    public IBtcBatch fetchCurBtc(IBtcBatch iBtcBatch) {
        return fetch(curBtc, iBtcBatch, selectRecRm);
    }

    public DbAccessStatus updateRec1(char iabv0002StateCurrent, int btcIdRich) {
        return buildQuery("updateRec1").bind("iabv0002StateCurrent", String.valueOf(iabv0002StateCurrent)).bind("btcIdRich", btcIdRich).executeUpdate();
    }

    public IBtcBatch selectRec1(IBtcBatch iBtcBatch) {
        return buildQuery("selectRec1").bind(iBtcBatch).rowMapper(selectRecRm).singleResult(iBtcBatch);
    }

    public IBtcBatch selectRec2(IBtcBatch iBtcBatch) {
        return buildQuery("selectRec2").bind(iBtcBatch).rowMapper(selectRecRm).singleResult(iBtcBatch);
    }

    public IBtcBatch selectRec3(IBtcBatch iBtcBatch) {
        return buildQuery("selectRec3").bind(iBtcBatch).rowMapper(selectRecRm).singleResult(iBtcBatch);
    }

    public IBtcBatch selectRec4(IBtcBatch iBtcBatch) {
        return buildQuery("selectRec4").bind(iBtcBatch).rowMapper(selectRecRm).singleResult(iBtcBatch);
    }

    public IBtcBatch selectRec5(IBtcBatch iBtcBatch) {
        return buildQuery("selectRec5").bind(iBtcBatch).rowMapper(selectRecRm).singleResult(iBtcBatch);
    }
}
