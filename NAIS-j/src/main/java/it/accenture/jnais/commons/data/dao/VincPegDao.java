package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IVincPeg;

/**
 * Data Access Object(DAO) for table [VINC_PEG]
 * 
 */
public class VincPegDao extends BaseSqlDao<IVincPeg> {

    private Cursor cIdUpdEffL23;
    private Cursor cIdpEffL23;
    private Cursor cIdpCpzL23;
    private final IRowMapper<IVincPeg> selectByL23DsRigaRm = buildNamedRowMapper(IVincPeg.class, "idVincPeg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "idRappAna", "tpVincObj", "flDelegaAlRiscObj", "descVcharObj", "dtAttivVinpgDbObj", "cptVinctoPignObj", "dtChiuVinpgDbObj", "valRiscIniVinpgObj", "valRiscEndVinpgObj", "somPreVinpgObj", "flVinpgIntPrstzObj", "descAggVincVcharObj", "l23DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "tpAutSeqObj", "codUffSeqObj", "numProvvSeqObj", "tpProvvSeqObj", "dtProvvSeqDbObj", "dtNotificaBloccoDbObj", "notaProvvVcharObj");

    public VincPegDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IVincPeg> getToClass() {
        return IVincPeg.class;
    }

    public IVincPeg selectByL23DsRiga(long l23DsRiga, IVincPeg iVincPeg) {
        return buildQuery("selectByL23DsRiga").bind("l23DsRiga", l23DsRiga).rowMapper(selectByL23DsRigaRm).singleResult(iVincPeg);
    }

    public DbAccessStatus insertRec(IVincPeg iVincPeg) {
        return buildQuery("insertRec").bind(iVincPeg).executeInsert();
    }

    public DbAccessStatus updateRec(IVincPeg iVincPeg) {
        return buildQuery("updateRec").bind(iVincPeg).executeUpdate();
    }

    public DbAccessStatus deleteByL23DsRiga(long l23DsRiga) {
        return buildQuery("deleteByL23DsRiga").bind("l23DsRiga", l23DsRiga).executeDelete();
    }

    public IVincPeg selectRec(int l23IdVincPeg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IVincPeg iVincPeg) {
        return buildQuery("selectRec").bind("l23IdVincPeg", l23IdVincPeg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByL23DsRigaRm).singleResult(iVincPeg);
    }

    public DbAccessStatus updateRec1(IVincPeg iVincPeg) {
        return buildQuery("updateRec1").bind(iVincPeg).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffL23(int l23IdVincPeg, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffL23 = buildQuery("openCIdUpdEffL23").bind("l23IdVincPeg", l23IdVincPeg).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffL23() {
        return closeCursor(cIdUpdEffL23);
    }

    public IVincPeg fetchCIdUpdEffL23(IVincPeg iVincPeg) {
        return fetch(cIdUpdEffL23, iVincPeg, selectByL23DsRigaRm);
    }

    public IVincPeg selectRec1(int l23IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IVincPeg iVincPeg) {
        return buildQuery("selectRec1").bind("l23IdRappAna", l23IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByL23DsRigaRm).singleResult(iVincPeg);
    }

    public DbAccessStatus openCIdpEffL23(int l23IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffL23 = buildQuery("openCIdpEffL23").bind("l23IdRappAna", l23IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffL23() {
        return closeCursor(cIdpEffL23);
    }

    public IVincPeg fetchCIdpEffL23(IVincPeg iVincPeg) {
        return fetch(cIdpEffL23, iVincPeg, selectByL23DsRigaRm);
    }

    public IVincPeg selectRec2(int l23IdVincPeg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IVincPeg iVincPeg) {
        return buildQuery("selectRec2").bind("l23IdVincPeg", l23IdVincPeg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByL23DsRigaRm).singleResult(iVincPeg);
    }

    public IVincPeg selectRec3(int l23IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IVincPeg iVincPeg) {
        return buildQuery("selectRec3").bind("l23IdRappAna", l23IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByL23DsRigaRm).singleResult(iVincPeg);
    }

    public DbAccessStatus openCIdpCpzL23(int l23IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzL23 = buildQuery("openCIdpCpzL23").bind("l23IdRappAna", l23IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzL23() {
        return closeCursor(cIdpCpzL23);
    }

    public IVincPeg fetchCIdpCpzL23(IVincPeg iVincPeg) {
        return fetch(cIdpCpzL23, iVincPeg, selectByL23DsRigaRm);
    }
}
