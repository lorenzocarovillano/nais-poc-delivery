package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IEstrCntDiagnRiv;

/**
 * Data Access Object(DAO) for table [ESTR_CNT_DIAGN_RIV]
 * 
 */
public class EstrCntDiagnRivDao extends BaseSqlDao<IEstrCntDiagnRiv> {

    private Cursor cNst25;
    private Cursor cNst26;
    private final IRowMapper<IEstrCntDiagnRiv> selectRecRm = buildNamedRowMapper(IEstrCntDiagnRiv.class, "codCompAnia", "idPoli", "dtRivalDb", "codTari", "rendtoLrdObj", "pcRetrObj", "rendtoRetrObj", "commisGestObj", "tsRivalNetObj", "minGartoObj", "minTrnutObj", "ibPoli", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");
    private final IRowMapper<IEstrCntDiagnRiv> selectRec1Rm = buildNamedRowMapper(IEstrCntDiagnRiv.class, "codCompAnia", "idPoli", "dtRivalDb", "rendtoLrdObj", "pcRetrObj", "rendtoRetrObj", "commisGestObj", "tsRivalNetObj", "minGartoObj", "minTrnutObj", "ibPoli", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public EstrCntDiagnRivDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IEstrCntDiagnRiv> getToClass() {
        return IEstrCntDiagnRiv.class;
    }

    public IEstrCntDiagnRiv selectRec(int codCompAnia, int idPoli, String dtRivalDb, String codTari, IEstrCntDiagnRiv iEstrCntDiagnRiv) {
        return buildQuery("selectRec").bind("codCompAnia", codCompAnia).bind("idPoli", idPoli).bind("dtRivalDb", dtRivalDb).bind("codTari", codTari).rowMapper(selectRecRm).singleResult(iEstrCntDiagnRiv);
    }

    public DbAccessStatus insertRec(IEstrCntDiagnRiv iEstrCntDiagnRiv) {
        return buildQuery("insertRec").bind(iEstrCntDiagnRiv).executeInsert();
    }

    public DbAccessStatus updateRec(IEstrCntDiagnRiv iEstrCntDiagnRiv) {
        return buildQuery("updateRec").bind(iEstrCntDiagnRiv).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompAnia, int idPoli, String dtRivalDb, String codTari) {
        return buildQuery("deleteRec").bind("codCompAnia", codCompAnia).bind("idPoli", idPoli).bind("dtRivalDb", dtRivalDb).bind("codTari", codTari).executeDelete();
    }

    public IEstrCntDiagnRiv selectRec1(int ldbvf311IdPoli, short ldbvf311Aa, short ldbvf311Mm, int idsv0003CodiceCompagniaAnia, IEstrCntDiagnRiv iEstrCntDiagnRiv) {
        return buildQuery("selectRec1").bind("ldbvf311IdPoli", ldbvf311IdPoli).bind("ldbvf311Aa", ldbvf311Aa).bind("ldbvf311Mm", ldbvf311Mm).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectRec1Rm).singleResult(iEstrCntDiagnRiv);
    }

    public DbAccessStatus openCNst25(int ldbvf311IdPoli, short ldbvf311Aa, short ldbvf311Mm, int idsv0003CodiceCompagniaAnia) {
        cNst25 = buildQuery("openCNst25").bind("ldbvf311IdPoli", ldbvf311IdPoli).bind("ldbvf311Aa", ldbvf311Aa).bind("ldbvf311Mm", ldbvf311Mm).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst25() {
        return closeCursor(cNst25);
    }

    public IEstrCntDiagnRiv fetchCNst25(IEstrCntDiagnRiv iEstrCntDiagnRiv) {
        return fetch(cNst25, iEstrCntDiagnRiv, selectRec1Rm);
    }

    public IEstrCntDiagnRiv selectRec2(IEstrCntDiagnRiv iEstrCntDiagnRiv) {
        return buildQuery("selectRec2").bind(iEstrCntDiagnRiv).rowMapper(selectRecRm).singleResult(iEstrCntDiagnRiv);
    }

    public DbAccessStatus openCNst26(IEstrCntDiagnRiv iEstrCntDiagnRiv) {
        cNst26 = buildQuery("openCNst26").bind(iEstrCntDiagnRiv).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst26() {
        return closeCursor(cNst26);
    }

    public IEstrCntDiagnRiv fetchCNst26(IEstrCntDiagnRiv iEstrCntDiagnRiv) {
        return fetch(cNst26, iEstrCntDiagnRiv, selectRecRm);
    }
}
