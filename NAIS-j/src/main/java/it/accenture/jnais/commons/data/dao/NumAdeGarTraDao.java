package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import it.accenture.jnais.commons.data.to.INumAdeGarTra;

/**
 * Data Access Object(DAO) for table [NUM_ADE_GAR_TRA]
 * 
 */
public class NumAdeGarTraDao extends BaseSqlDao<INumAdeGarTra> {

    public NumAdeGarTraDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<INumAdeGarTra> getToClass() {
        return INumAdeGarTra.class;
    }

    public INumAdeGarTra selectRec(int codCompAnia, int idOgg, String tpOgg, INumAdeGarTra iNumAdeGarTra) {
        return buildQuery("selectRec").bind("codCompAnia", codCompAnia).bind("idOgg", idOgg).bind("tpOgg", tpOgg).singleResult(iNumAdeGarTra);
    }

    public DbAccessStatus insertRec(INumAdeGarTra iNumAdeGarTra) {
        return buildQuery("insertRec").bind(iNumAdeGarTra).executeInsert();
    }

    public DbAccessStatus updateRec(INumAdeGarTra iNumAdeGarTra) {
        return buildQuery("updateRec").bind(iNumAdeGarTra).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompAnia, int idOgg, String tpOgg) {
        return buildQuery("deleteRec").bind("codCompAnia", codCompAnia).bind("idOgg", idOgg).bind("tpOgg", tpOgg).executeDelete();
    }
}
