package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ICompStrDato;

/**
 * Data Access Object(DAO) for table [COMP_STR_DATO]
 * 
 */
public class CompStrDatoDao extends BaseSqlDao<ICompStrDato> {

    private Cursor cur1;
    private final IRowMapper<ICompStrDato> fetchCur1Rm = buildNamedRowMapper(ICompStrDato.class, "codCompagniaAnia", "codStrDato", "posizione", "codStrDato2Obj", "codDatoObj", "flagKeyObj", "flagReturnCodeObj", "flagCallUsingObj", "flagWhereCondObj", "flagRedefinesObj", "tipoDatoObj", "lunghezzaDatoObj", "precisioneDatoObj", "codDominioObj", "formattazioneDatoObj", "servizioConversObj", "areaConvStandardObj", "ricorrenzaObj", "obbligatorietaObj", "valoreDefaultObj", "dsUtente");

    public CompStrDatoDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ICompStrDato> getToClass() {
        return ICompStrDato.class;
    }

    public DbAccessStatus openCur1(int compagniaAnia, String strDato) {
        cur1 = buildQuery("openCur1").bind("compagniaAnia", compagniaAnia).bind("strDato", strDato).open();
        return dbStatus;
    }

    public ICompStrDato fetchCur1(ICompStrDato iCompStrDato) {
        return fetch(cur1, iCompStrDato, fetchCur1Rm);
    }

    public DbAccessStatus closeCur1() {
        return closeCursor(cur1);
    }
}
