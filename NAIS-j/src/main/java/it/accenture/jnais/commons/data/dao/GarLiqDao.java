package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IGarLiq;

/**
 * Data Access Object(DAO) for table [GAR_LIQ]
 * 
 */
public class GarLiqDao extends BaseSqlDao<IGarLiq> {

    private Cursor cIdUpdEffGrl;
    private Cursor cIdpEffGrl;
    private Cursor cIdpCpzGrl;
    private final IRowMapper<IGarLiq> selectByGrlDsRigaRm = buildNamedRowMapper(IGarLiq.class, "idGarLiq", "idLiq", "idGar", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtSin1oAsstoDbObj", "cauSin1oAsstoObj", "tpSin1oAsstoObj", "dtSin2oAsstoDbObj", "cauSin2oAsstoObj", "tpSin2oAsstoObj", "dtSin3oAsstoDbObj", "cauSin3oAsstoObj", "tpSin3oAsstoObj", "grlDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public GarLiqDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IGarLiq> getToClass() {
        return IGarLiq.class;
    }

    public IGarLiq selectByGrlDsRiga(long grlDsRiga, IGarLiq iGarLiq) {
        return buildQuery("selectByGrlDsRiga").bind("grlDsRiga", grlDsRiga).rowMapper(selectByGrlDsRigaRm).singleResult(iGarLiq);
    }

    public DbAccessStatus insertRec(IGarLiq iGarLiq) {
        return buildQuery("insertRec").bind(iGarLiq).executeInsert();
    }

    public DbAccessStatus updateRec(IGarLiq iGarLiq) {
        return buildQuery("updateRec").bind(iGarLiq).executeUpdate();
    }

    public DbAccessStatus deleteByGrlDsRiga(long grlDsRiga) {
        return buildQuery("deleteByGrlDsRiga").bind("grlDsRiga", grlDsRiga).executeDelete();
    }

    public IGarLiq selectRec(int grlIdGarLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IGarLiq iGarLiq) {
        return buildQuery("selectRec").bind("grlIdGarLiq", grlIdGarLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByGrlDsRigaRm).singleResult(iGarLiq);
    }

    public DbAccessStatus updateRec1(IGarLiq iGarLiq) {
        return buildQuery("updateRec1").bind(iGarLiq).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffGrl(int grlIdGarLiq, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffGrl = buildQuery("openCIdUpdEffGrl").bind("grlIdGarLiq", grlIdGarLiq).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffGrl() {
        return closeCursor(cIdUpdEffGrl);
    }

    public IGarLiq fetchCIdUpdEffGrl(IGarLiq iGarLiq) {
        return fetch(cIdUpdEffGrl, iGarLiq, selectByGrlDsRigaRm);
    }

    public IGarLiq selectRec1(int grlIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IGarLiq iGarLiq) {
        return buildQuery("selectRec1").bind("grlIdGar", grlIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByGrlDsRigaRm).singleResult(iGarLiq);
    }

    public DbAccessStatus openCIdpEffGrl(int grlIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffGrl = buildQuery("openCIdpEffGrl").bind("grlIdGar", grlIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffGrl() {
        return closeCursor(cIdpEffGrl);
    }

    public IGarLiq fetchCIdpEffGrl(IGarLiq iGarLiq) {
        return fetch(cIdpEffGrl, iGarLiq, selectByGrlDsRigaRm);
    }

    public IGarLiq selectRec2(int grlIdGarLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IGarLiq iGarLiq) {
        return buildQuery("selectRec2").bind("grlIdGarLiq", grlIdGarLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByGrlDsRigaRm).singleResult(iGarLiq);
    }

    public IGarLiq selectRec3(int grlIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IGarLiq iGarLiq) {
        return buildQuery("selectRec3").bind("grlIdGar", grlIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByGrlDsRigaRm).singleResult(iGarLiq);
    }

    public DbAccessStatus openCIdpCpzGrl(int grlIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzGrl = buildQuery("openCIdpCpzGrl").bind("grlIdGar", grlIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzGrl() {
        return closeCursor(cIdpCpzGrl);
    }

    public IGarLiq fetchCIdpCpzGrl(IGarLiq iGarLiq) {
        return fetch(cIdpCpzGrl, iGarLiq, selectByGrlDsRigaRm);
    }
}
