package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IImpstSost;

/**
 * Data Access Object(DAO) for table [IMPST_SOST]
 * 
 */
public class ImpstSostDao extends BaseSqlDao<IImpstSost> {

    private Cursor cIdUpdEffIso;
    private Cursor cIdoEffIso;
    private Cursor cIdoCpzIso;
    private final IRowMapper<IImpstSost> selectByIsoDsRigaRm = buildNamedRowMapper(IImpstSost.class, "idImpstSost", "isoIdOggObj", "isoTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "dtIniPerDbObj", "dtEndPerDbObj", "codCompAnia", "impstSostObj", "impbIsObj", "alqIsObj", "codTrbObj", "prstzLrdAnteIsObj", "risMatNetPrecObj", "risMatAnteTaxObj", "risMatPostTaxObj", "prstzNetObj", "prstzPrecObj", "cumPreVersObj", "tpCalcImpst", "impGiaTassato", "isoDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");
    private final IRowMapper<IImpstSost> selectRec4Rm = buildNamedRowMapper(IImpstSost.class, "mpbIs", "impstSost");
    private final IRowMapper<IImpstSost> selectRec10Rm = buildNamedRowMapper(IImpstSost.class, "dtIniPerDbObj");

    public ImpstSostDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IImpstSost> getToClass() {
        return IImpstSost.class;
    }

    public IImpstSost selectByIsoDsRiga(long isoDsRiga, IImpstSost iImpstSost) {
        return buildQuery("selectByIsoDsRiga").bind("isoDsRiga", isoDsRiga).rowMapper(selectByIsoDsRigaRm).singleResult(iImpstSost);
    }

    public DbAccessStatus insertRec(IImpstSost iImpstSost) {
        return buildQuery("insertRec").bind(iImpstSost).executeInsert();
    }

    public DbAccessStatus updateRec(IImpstSost iImpstSost) {
        return buildQuery("updateRec").bind(iImpstSost).executeUpdate();
    }

    public DbAccessStatus deleteByIsoDsRiga(long isoDsRiga) {
        return buildQuery("deleteByIsoDsRiga").bind("isoDsRiga", isoDsRiga).executeDelete();
    }

    public IImpstSost selectRec(int isoIdImpstSost, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IImpstSost iImpstSost) {
        return buildQuery("selectRec").bind("isoIdImpstSost", isoIdImpstSost).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByIsoDsRigaRm).singleResult(iImpstSost);
    }

    public DbAccessStatus updateRec1(IImpstSost iImpstSost) {
        return buildQuery("updateRec1").bind(iImpstSost).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffIso(int isoIdImpstSost, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffIso = buildQuery("openCIdUpdEffIso").bind("isoIdImpstSost", isoIdImpstSost).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffIso() {
        return closeCursor(cIdUpdEffIso);
    }

    public IImpstSost fetchCIdUpdEffIso(IImpstSost iImpstSost) {
        return fetch(cIdUpdEffIso, iImpstSost, selectByIsoDsRigaRm);
    }

    public IImpstSost selectRec1(int isoIdOgg, String isoTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IImpstSost iImpstSost) {
        return buildQuery("selectRec1").bind("isoIdOgg", isoIdOgg).bind("isoTpOgg", isoTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByIsoDsRigaRm).singleResult(iImpstSost);
    }

    public DbAccessStatus openCIdoEffIso(int isoIdOgg, String isoTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffIso = buildQuery("openCIdoEffIso").bind("isoIdOgg", isoIdOgg).bind("isoTpOgg", isoTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffIso() {
        return closeCursor(cIdoEffIso);
    }

    public IImpstSost fetchCIdoEffIso(IImpstSost iImpstSost) {
        return fetch(cIdoEffIso, iImpstSost, selectByIsoDsRigaRm);
    }

    public IImpstSost selectRec2(int isoIdImpstSost, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IImpstSost iImpstSost) {
        return buildQuery("selectRec2").bind("isoIdImpstSost", isoIdImpstSost).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByIsoDsRigaRm).singleResult(iImpstSost);
    }

    public IImpstSost selectRec3(IImpstSost iImpstSost) {
        return buildQuery("selectRec3").bind(iImpstSost).rowMapper(selectByIsoDsRigaRm).singleResult(iImpstSost);
    }

    public DbAccessStatus openCIdoCpzIso(IImpstSost iImpstSost) {
        cIdoCpzIso = buildQuery("openCIdoCpzIso").bind(iImpstSost).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzIso() {
        return closeCursor(cIdoCpzIso);
    }

    public IImpstSost fetchCIdoCpzIso(IImpstSost iImpstSost) {
        return fetch(cIdoCpzIso, iImpstSost, selectByIsoDsRigaRm);
    }

    public IImpstSost selectRec4(IImpstSost iImpstSost) {
        return buildQuery("selectRec4").bind(iImpstSost).rowMapper(selectRec4Rm).singleResult(iImpstSost);
    }

    public IImpstSost selectRec5(IImpstSost iImpstSost) {
        return buildQuery("selectRec5").bind(iImpstSost).rowMapper(selectRec4Rm).singleResult(iImpstSost);
    }

    public IImpstSost selectRec6(int isoIdOgg, String isoTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IImpstSost iImpstSost) {
        return buildQuery("selectRec6").bind("isoIdOgg", isoIdOgg).bind("isoTpOgg", isoTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByIsoDsRigaRm).singleResult(iImpstSost);
    }

    public IImpstSost selectRec7(IImpstSost iImpstSost) {
        return buildQuery("selectRec7").bind(iImpstSost).rowMapper(selectByIsoDsRigaRm).singleResult(iImpstSost);
    }

    public IImpstSost selectRec8(IImpstSost iImpstSost) {
        return buildQuery("selectRec8").bind(iImpstSost).rowMapper(selectByIsoDsRigaRm).singleResult(iImpstSost);
    }

    public IImpstSost selectRec9(IImpstSost iImpstSost) {
        return buildQuery("selectRec9").bind(iImpstSost).rowMapper(selectByIsoDsRigaRm).singleResult(iImpstSost);
    }

    public IImpstSost selectRec10(IImpstSost iImpstSost) {
        return buildQuery("selectRec10").bind(iImpstSost).rowMapper(selectRec10Rm).singleResult(iImpstSost);
    }
}
