package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [DETT_RICH]
 * 
 */
public interface IDettRich extends BaseSqlTo {

    /**
     * Host Variable DER-TP-AREA-D-RICH
     * 
     */
    String getTpAreaDRich();

    void setTpAreaDRich(String tpAreaDRich);

    /**
     * Nullable property for DER-TP-AREA-D-RICH
     * 
     */
    String getTpAreaDRichObj();

    void setTpAreaDRichObj(String tpAreaDRichObj);

    /**
     * Host Variable DER-AREA-D-RICH-VCHAR
     * 
     */
    String getAreaDRichVchar();

    void setAreaDRichVchar(String areaDRichVchar);

    /**
     * Nullable property for DER-AREA-D-RICH-VCHAR
     * 
     */
    String getAreaDRichVcharObj();

    void setAreaDRichVcharObj(String areaDRichVcharObj);
};
