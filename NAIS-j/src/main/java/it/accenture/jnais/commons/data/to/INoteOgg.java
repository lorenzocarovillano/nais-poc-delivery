package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [NOTE_OGG]
 * 
 */
public interface INoteOgg extends BaseSqlTo {

    /**
     * Host Variable NOT-ID-NOTE-OGG
     * 
     */
    int getNotIdNoteOgg();

    void setNotIdNoteOgg(int notIdNoteOgg);

    /**
     * Host Variable NOT-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable NOT-ID-OGG
     * 
     */
    int getIdOgg();

    void setIdOgg(int idOgg);

    /**
     * Host Variable NOT-TP-OGG
     * 
     */
    String getTpOgg();

    void setTpOgg(String tpOgg);

    /**
     * Host Variable NOT-NOTA-OGG-VCHAR
     * 
     */
    String getNotaOggVchar();

    void setNotaOggVchar(String notaOggVchar);

    /**
     * Nullable property for NOT-NOTA-OGG-VCHAR
     * 
     */
    String getNotaOggVcharObj();

    void setNotaOggVcharObj(String notaOggVcharObj);

    /**
     * Host Variable NOT-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable NOT-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable NOT-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable NOT-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable NOT-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
