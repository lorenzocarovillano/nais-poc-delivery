package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [OGG_IN_COASS]
 * 
 */
public interface IOggInCoass extends BaseSqlTo {

    /**
     * Host Variable OCS-ID-OGG-IN-COASS
     * 
     */
    int getIdOggInCoass();

    void setIdOggInCoass(int idOggInCoass);

    /**
     * Host Variable OCS-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Nullable property for OCS-ID-POLI
     * 
     */
    Integer getIdPoliObj();

    void setIdPoliObj(Integer idPoliObj);

    /**
     * Host Variable OCS-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable OCS-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for OCS-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable OCS-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable OCS-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable OCS-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable OCS-COD-COASS
     * 
     */
    String getCodCoass();

    void setCodCoass(String codCoass);

    /**
     * Nullable property for OCS-COD-COASS
     * 
     */
    String getCodCoassObj();

    void setCodCoassObj(String codCoassObj);

    /**
     * Host Variable OCS-TP-DLG
     * 
     */
    String getTpDlg();

    void setTpDlg(String tpDlg);

    /**
     * Host Variable OCS-COD-COMP-ANIA-ACQS
     * 
     */
    int getCodCompAniaAcqs();

    void setCodCompAniaAcqs(int codCompAniaAcqs);

    /**
     * Nullable property for OCS-COD-COMP-ANIA-ACQS
     * 
     */
    Integer getCodCompAniaAcqsObj();

    void setCodCompAniaAcqsObj(Integer codCompAniaAcqsObj);

    /**
     * Host Variable OCS-DS-RIGA
     * 
     */
    long getOcsDsRiga();

    void setOcsDsRiga(long ocsDsRiga);

    /**
     * Host Variable OCS-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable OCS-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable OCS-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable OCS-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable OCS-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable OCS-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
