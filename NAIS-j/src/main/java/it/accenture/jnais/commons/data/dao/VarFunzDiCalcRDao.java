package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IVarFunzDiCalcR;

/**
 * Data Access Object(DAO) for table [VAR_FUNZ_DI_CALC_R]
 * 
 */
public class VarFunzDiCalcRDao extends BaseSqlDao<IVarFunzDiCalcR> {

    private Cursor cNst20;
    private final IRowMapper<IVarFunzDiCalcR> selectRecRm = buildNamedRowMapper(IVarFunzDiCalcR.class, "idcomp", "codprodVchar", "codtariVchar", "diniz", "nomefunzVchar", "dend", "isprecalcObj", "nomeprogrVcharObj", "modcalcVcharObj", "glovarlistVcharObj", "stepElabVchar");

    public VarFunzDiCalcRDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IVarFunzDiCalcR> getToClass() {
        return IVarFunzDiCalcR.class;
    }

    public IVarFunzDiCalcR selectRec(IVarFunzDiCalcR iVarFunzDiCalcR) {
        return buildQuery("selectRec").bind(iVarFunzDiCalcR).rowMapper(selectRecRm).singleResult(iVarFunzDiCalcR);
    }

    public DbAccessStatus insertRec(IVarFunzDiCalcR iVarFunzDiCalcR) {
        return buildQuery("insertRec").bind(iVarFunzDiCalcR).executeInsert();
    }

    public DbAccessStatus updateRec(IVarFunzDiCalcR iVarFunzDiCalcR) {
        return buildQuery("updateRec").bind(iVarFunzDiCalcR).executeUpdate();
    }

    public DbAccessStatus deleteRec(IVarFunzDiCalcR iVarFunzDiCalcR) {
        return buildQuery("deleteRec").bind(iVarFunzDiCalcR).executeDelete();
    }

    public IVarFunzDiCalcR selectRec1(IVarFunzDiCalcR iVarFunzDiCalcR) {
        return buildQuery("selectRec1").bind(iVarFunzDiCalcR).rowMapper(selectRecRm).singleResult(iVarFunzDiCalcR);
    }

    public DbAccessStatus openCNst20(IVarFunzDiCalcR iVarFunzDiCalcR) {
        cNst20 = buildQuery("openCNst20").bind(iVarFunzDiCalcR).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst20() {
        return closeCursor(cNst20);
    }

    public IVarFunzDiCalcR fetchCNst20(IVarFunzDiCalcR iVarFunzDiCalcR) {
        return fetch(cNst20, iVarFunzDiCalcR, selectRecRm);
    }
}
