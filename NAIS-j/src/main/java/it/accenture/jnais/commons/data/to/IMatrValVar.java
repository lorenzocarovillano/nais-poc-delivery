package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [MATR_VAL_VAR]
 * 
 */
public interface IMatrValVar extends BaseSqlTo {

    /**
     * Host Variable MVV-ID-MATR-VAL-VAR
     * 
     */
    int getIdMatrValVar();

    void setIdMatrValVar(int idMatrValVar);

    /**
     * Host Variable MVV-IDP-MATR-VAL-VAR
     * 
     */
    int getIdpMatrValVar();

    void setIdpMatrValVar(int idpMatrValVar);

    /**
     * Nullable property for MVV-IDP-MATR-VAL-VAR
     * 
     */
    Integer getIdpMatrValVarObj();

    void setIdpMatrValVarObj(Integer idpMatrValVarObj);

    /**
     * Host Variable MVV-COD-COMPAGNIA-ANIA
     * 
     */
    int getCodCompagniaAnia();

    void setCodCompagniaAnia(int codCompagniaAnia);

    /**
     * Host Variable MVV-TIPO-MOVIMENTO
     * 
     */
    int getTipoMovimento();

    void setTipoMovimento(int tipoMovimento);

    /**
     * Nullable property for MVV-TIPO-MOVIMENTO
     * 
     */
    Integer getTipoMovimentoObj();

    void setTipoMovimentoObj(Integer tipoMovimentoObj);

    /**
     * Host Variable MVV-COD-DATO-EXT
     * 
     */
    String getCodDatoExt();

    void setCodDatoExt(String codDatoExt);

    /**
     * Nullable property for MVV-COD-DATO-EXT
     * 
     */
    String getCodDatoExtObj();

    void setCodDatoExtObj(String codDatoExtObj);

    /**
     * Host Variable MVV-OBBLIGATORIETA
     * 
     */
    char getObbligatorieta();

    void setObbligatorieta(char obbligatorieta);

    /**
     * Nullable property for MVV-OBBLIGATORIETA
     * 
     */
    Character getObbligatorietaObj();

    void setObbligatorietaObj(Character obbligatorietaObj);

    /**
     * Host Variable MVV-VALORE-DEFAULT
     * 
     */
    String getValoreDefault();

    void setValoreDefault(String valoreDefault);

    /**
     * Nullable property for MVV-VALORE-DEFAULT
     * 
     */
    String getValoreDefaultObj();

    void setValoreDefaultObj(String valoreDefaultObj);

    /**
     * Host Variable MVV-COD-STR-DATO-PTF
     * 
     */
    String getCodStrDatoPtf();

    void setCodStrDatoPtf(String codStrDatoPtf);

    /**
     * Nullable property for MVV-COD-STR-DATO-PTF
     * 
     */
    String getCodStrDatoPtfObj();

    void setCodStrDatoPtfObj(String codStrDatoPtfObj);

    /**
     * Host Variable MVV-COD-DATO-PTF
     * 
     */
    String getCodDatoPtf();

    void setCodDatoPtf(String codDatoPtf);

    /**
     * Nullable property for MVV-COD-DATO-PTF
     * 
     */
    String getCodDatoPtfObj();

    void setCodDatoPtfObj(String codDatoPtfObj);

    /**
     * Host Variable MVV-COD-PARAMETRO
     * 
     */
    String getCodParametro();

    void setCodParametro(String codParametro);

    /**
     * Nullable property for MVV-COD-PARAMETRO
     * 
     */
    String getCodParametroObj();

    void setCodParametroObj(String codParametroObj);

    /**
     * Host Variable MVV-OPERAZIONE
     * 
     */
    String getOperazione();

    void setOperazione(String operazione);

    /**
     * Nullable property for MVV-OPERAZIONE
     * 
     */
    String getOperazioneObj();

    void setOperazioneObj(String operazioneObj);

    /**
     * Host Variable MVV-LIVELLO-OPERAZIONE
     * 
     */
    String getLivelloOperazione();

    void setLivelloOperazione(String livelloOperazione);

    /**
     * Nullable property for MVV-LIVELLO-OPERAZIONE
     * 
     */
    String getLivelloOperazioneObj();

    void setLivelloOperazioneObj(String livelloOperazioneObj);

    /**
     * Host Variable MVV-TIPO-OGGETTO
     * 
     */
    String getTipoOggetto();

    void setTipoOggetto(String tipoOggetto);

    /**
     * Nullable property for MVV-TIPO-OGGETTO
     * 
     */
    String getTipoOggettoObj();

    void setTipoOggettoObj(String tipoOggettoObj);

    /**
     * Host Variable MVV-WHERE-CONDITION-VCHAR
     * 
     */
    String getWhereConditionVchar();

    void setWhereConditionVchar(String whereConditionVchar);

    /**
     * Nullable property for MVV-WHERE-CONDITION-VCHAR
     * 
     */
    String getWhereConditionVcharObj();

    void setWhereConditionVcharObj(String whereConditionVcharObj);

    /**
     * Host Variable MVV-SERVIZIO-LETTURA
     * 
     */
    String getServizioLettura();

    void setServizioLettura(String servizioLettura);

    /**
     * Nullable property for MVV-SERVIZIO-LETTURA
     * 
     */
    String getServizioLetturaObj();

    void setServizioLetturaObj(String servizioLetturaObj);

    /**
     * Host Variable MVV-MODULO-CALCOLO
     * 
     */
    String getModuloCalcolo();

    void setModuloCalcolo(String moduloCalcolo);

    /**
     * Nullable property for MVV-MODULO-CALCOLO
     * 
     */
    String getModuloCalcoloObj();

    void setModuloCalcoloObj(String moduloCalcoloObj);

    /**
     * Host Variable MVV-STEP-VALORIZZATORE
     * 
     */
    char getStepValorizzatore();

    void setStepValorizzatore(char stepValorizzatore);

    /**
     * Nullable property for MVV-STEP-VALORIZZATORE
     * 
     */
    Character getStepValorizzatoreObj();

    void setStepValorizzatoreObj(Character stepValorizzatoreObj);

    /**
     * Host Variable MVV-STEP-CONVERSAZIONE
     * 
     */
    char getStepConversazione();

    void setStepConversazione(char stepConversazione);

    /**
     * Nullable property for MVV-STEP-CONVERSAZIONE
     * 
     */
    Character getStepConversazioneObj();

    void setStepConversazioneObj(Character stepConversazioneObj);

    /**
     * Host Variable MVV-OPER-LOG-STATO-BUS
     * 
     */
    String getOperLogStatoBus();

    void setOperLogStatoBus(String operLogStatoBus);

    /**
     * Nullable property for MVV-OPER-LOG-STATO-BUS
     * 
     */
    String getOperLogStatoBusObj();

    void setOperLogStatoBusObj(String operLogStatoBusObj);

    /**
     * Host Variable MVV-ARRAY-STATO-BUS
     * 
     */
    String getArrayStatoBus();

    void setArrayStatoBus(String arrayStatoBus);

    /**
     * Nullable property for MVV-ARRAY-STATO-BUS
     * 
     */
    String getArrayStatoBusObj();

    void setArrayStatoBusObj(String arrayStatoBusObj);

    /**
     * Host Variable MVV-OPER-LOG-CAUSALE
     * 
     */
    String getOperLogCausale();

    void setOperLogCausale(String operLogCausale);

    /**
     * Nullable property for MVV-OPER-LOG-CAUSALE
     * 
     */
    String getOperLogCausaleObj();

    void setOperLogCausaleObj(String operLogCausaleObj);

    /**
     * Host Variable MVV-ARRAY-CAUSALE
     * 
     */
    String getArrayCausale();

    void setArrayCausale(String arrayCausale);

    /**
     * Nullable property for MVV-ARRAY-CAUSALE
     * 
     */
    String getArrayCausaleObj();

    void setArrayCausaleObj(String arrayCausaleObj);
};
