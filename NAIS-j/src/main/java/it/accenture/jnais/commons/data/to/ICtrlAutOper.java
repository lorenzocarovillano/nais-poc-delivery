package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [CTRL_AUT_OPER]
 * 
 */
public interface ICtrlAutOper extends BaseSqlTo {

    /**
     * Host Variable CAO-COD-COMPAGNIA-ANIA
     * 
     */
    int getCodCompagniaAnia();

    void setCodCompagniaAnia(int codCompagniaAnia);

    /**
     * Host Variable CAO-TP-MOVI
     * 
     */
    int getTpMovi();

    void setTpMovi(int tpMovi);

    /**
     * Host Variable CAO-KEY-AUT-OPER1
     * 
     */
    String getKeyAutOper1();

    void setKeyAutOper1(String keyAutOper1);

    /**
     * Host Variable CAO-KEY-AUT-OPER2
     * 
     */
    String getKeyAutOper2();

    void setKeyAutOper2(String keyAutOper2);

    /**
     * Host Variable CAO-KEY-AUT-OPER3
     * 
     */
    String getKeyAutOper3();

    void setKeyAutOper3(String keyAutOper3);

    /**
     * Host Variable CAO-KEY-AUT-OPER4
     * 
     */
    String getKeyAutOper4();

    void setKeyAutOper4(String keyAutOper4);

    /**
     * Host Variable CAO-KEY-AUT-OPER5
     * 
     */
    String getKeyAutOper5();

    void setKeyAutOper5(String keyAutOper5);

    /**
     * Host Variable CAO-ID-CTRL-AUT-OPER
     * 
     */
    int getIdCtrlAutOper();

    void setIdCtrlAutOper(int idCtrlAutOper);

    /**
     * Nullable property for CAO-TP-MOVI
     * 
     */
    Integer getTpMoviObj();

    void setTpMoviObj(Integer tpMoviObj);

    /**
     * Host Variable CAO-COD-ERRORE
     * 
     */
    int getCodErrore();

    void setCodErrore(int codErrore);

    /**
     * Nullable property for CAO-KEY-AUT-OPER1
     * 
     */
    String getKeyAutOper1Obj();

    void setKeyAutOper1Obj(String keyAutOper1Obj);

    /**
     * Nullable property for CAO-KEY-AUT-OPER2
     * 
     */
    String getKeyAutOper2Obj();

    void setKeyAutOper2Obj(String keyAutOper2Obj);

    /**
     * Nullable property for CAO-KEY-AUT-OPER3
     * 
     */
    String getKeyAutOper3Obj();

    void setKeyAutOper3Obj(String keyAutOper3Obj);

    /**
     * Nullable property for CAO-KEY-AUT-OPER4
     * 
     */
    String getKeyAutOper4Obj();

    void setKeyAutOper4Obj(String keyAutOper4Obj);

    /**
     * Nullable property for CAO-KEY-AUT-OPER5
     * 
     */
    String getKeyAutOper5Obj();

    void setKeyAutOper5Obj(String keyAutOper5Obj);

    /**
     * Host Variable CAO-COD-LIV-AUT
     * 
     */
    int getCodLivAut();

    void setCodLivAut(int codLivAut);

    /**
     * Host Variable CAO-TP-MOT-DEROGA
     * 
     */
    String getTpMotDeroga();

    void setTpMotDeroga(String tpMotDeroga);

    /**
     * Host Variable CAO-MODULO-VERIFICA
     * 
     */
    String getModuloVerifica();

    void setModuloVerifica(String moduloVerifica);

    /**
     * Nullable property for CAO-MODULO-VERIFICA
     * 
     */
    String getModuloVerificaObj();

    void setModuloVerificaObj(String moduloVerificaObj);

    /**
     * Host Variable CAO-COD-COND
     * 
     */
    String getCodCond();

    void setCodCond(String codCond);

    /**
     * Nullable property for CAO-COD-COND
     * 
     */
    String getCodCondObj();

    void setCodCondObj(String codCondObj);

    /**
     * Host Variable CAO-PROG-COND
     * 
     */
    short getProgCond();

    void setProgCond(short progCond);

    /**
     * Nullable property for CAO-PROG-COND
     * 
     */
    Short getProgCondObj();

    void setProgCondObj(Short progCondObj);

    /**
     * Host Variable CAO-RISULT-COND
     * 
     */
    char getRisultCond();

    void setRisultCond(char risultCond);

    /**
     * Nullable property for CAO-RISULT-COND
     * 
     */
    Character getRisultCondObj();

    void setRisultCondObj(Character risultCondObj);

    /**
     * Host Variable CAO-PARAM-AUT-OPER-VCHAR
     * 
     */
    String getParamAutOperVchar();

    void setParamAutOperVchar(String paramAutOperVchar);

    /**
     * Nullable property for CAO-PARAM-AUT-OPER-VCHAR
     * 
     */
    String getParamAutOperVcharObj();

    void setParamAutOperVcharObj(String paramAutOperVcharObj);

    /**
     * Host Variable CAO-STATO-ATTIVAZIONE
     * 
     */
    char getStatoAttivazione();

    void setStatoAttivazione(char statoAttivazione);

    /**
     * Nullable property for CAO-STATO-ATTIVAZIONE
     * 
     */
    Character getStatoAttivazioneObj();

    void setStatoAttivazioneObj(Character statoAttivazioneObj);
};
