package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [TEMPORARY_DATA]
 * 
 */
public interface ITemporaryData extends BaseSqlTo {

    /**
     * Host Variable D08-ID-TEMPORARY-DATA
     * 
     */
    long getIdTemporaryData();

    void setIdTemporaryData(long idTemporaryData);

    /**
     * Host Variable D08-ALIAS-STR-DATO
     * 
     */
    String getAliasStrDato();

    void setAliasStrDato(String aliasStrDato);

    /**
     * Host Variable D08-NUM-FRAME
     * 
     */
    int getNumFrame();

    void setNumFrame(int numFrame);

    /**
     * Host Variable D08-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable D08-ID-SESSION
     * 
     */
    String getIdSession();

    void setIdSession(String idSession);

    /**
     * Host Variable D08-FL-CONTIGUOUS-DATA
     * 
     */
    char getFlContiguousData();

    void setFlContiguousData(char flContiguousData);

    /**
     * Nullable property for D08-FL-CONTIGUOUS-DATA
     * 
     */
    Character getFlContiguousDataObj();

    void setFlContiguousDataObj(Character flContiguousDataObj);

    /**
     * Host Variable D08-TOTAL-RECURRENCE
     * 
     */
    int getTotalRecurrence();

    void setTotalRecurrence(int totalRecurrence);

    /**
     * Nullable property for D08-TOTAL-RECURRENCE
     * 
     */
    Integer getTotalRecurrenceObj();

    void setTotalRecurrenceObj(Integer totalRecurrenceObj);

    /**
     * Host Variable D08-PARTIAL-RECURRENCE
     * 
     */
    int getPartialRecurrence();

    void setPartialRecurrence(int partialRecurrence);

    /**
     * Nullable property for D08-PARTIAL-RECURRENCE
     * 
     */
    Integer getPartialRecurrenceObj();

    void setPartialRecurrenceObj(Integer partialRecurrenceObj);

    /**
     * Host Variable D08-ACTUAL-RECURRENCE
     * 
     */
    int getActualRecurrence();

    void setActualRecurrence(int actualRecurrence);

    /**
     * Nullable property for D08-ACTUAL-RECURRENCE
     * 
     */
    Integer getActualRecurrenceObj();

    void setActualRecurrenceObj(Integer actualRecurrenceObj);

    /**
     * Host Variable D08-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable D08-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable D08-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable D08-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable D08-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable D08-TYPE-RECORD
     * 
     */
    String getTypeRecord();

    void setTypeRecord(String typeRecord);

    /**
     * Nullable property for D08-TYPE-RECORD
     * 
     */
    String getTypeRecordObj();

    void setTypeRecordObj(String typeRecordObj);

    /**
     * Host Variable D08-BUFFER-DATA-VCHAR
     * 
     */
    String getBufferDataVchar();

    void setBufferDataVchar(String bufferDataVchar);

    /**
     * Nullable property for D08-BUFFER-DATA-VCHAR
     * 
     */
    String getBufferDataVcharObj();

    void setBufferDataVcharObj(String bufferDataVcharObj);
};
