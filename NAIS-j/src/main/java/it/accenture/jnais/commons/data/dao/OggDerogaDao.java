package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IOggDeroga;

/**
 * Data Access Object(DAO) for table [OGG_DEROGA]
 * 
 */
public class OggDerogaDao extends BaseSqlDao<IOggDeroga> {

    private Cursor cIdUpdEffOde;
    private Cursor cIboEffOde;
    private Cursor cIdoEffOde;
    private Cursor cIboCpzOde;
    private Cursor cIdoCpzOde;
    private final IRowMapper<IOggDeroga> selectByOdeDsRigaRm = buildNamedRowMapper(IOggDeroga.class, "idOggDeroga", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "odeIdOgg", "odeTpOgg", "ibOggObj", "tpDeroga", "codGrAutApprt", "codLivAutApprtObj", "codGrAutSup", "codLivAutSup", "odeDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public OggDerogaDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IOggDeroga> getToClass() {
        return IOggDeroga.class;
    }

    public IOggDeroga selectByOdeDsRiga(long odeDsRiga, IOggDeroga iOggDeroga) {
        return buildQuery("selectByOdeDsRiga").bind("odeDsRiga", odeDsRiga).rowMapper(selectByOdeDsRigaRm).singleResult(iOggDeroga);
    }

    public DbAccessStatus insertRec(IOggDeroga iOggDeroga) {
        return buildQuery("insertRec").bind(iOggDeroga).executeInsert();
    }

    public DbAccessStatus updateRec(IOggDeroga iOggDeroga) {
        return buildQuery("updateRec").bind(iOggDeroga).executeUpdate();
    }

    public DbAccessStatus deleteByOdeDsRiga(long odeDsRiga) {
        return buildQuery("deleteByOdeDsRiga").bind("odeDsRiga", odeDsRiga).executeDelete();
    }

    public IOggDeroga selectRec(int odeIdOggDeroga, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggDeroga iOggDeroga) {
        return buildQuery("selectRec").bind("odeIdOggDeroga", odeIdOggDeroga).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByOdeDsRigaRm).singleResult(iOggDeroga);
    }

    public DbAccessStatus updateRec1(IOggDeroga iOggDeroga) {
        return buildQuery("updateRec1").bind(iOggDeroga).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffOde(int odeIdOggDeroga, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffOde = buildQuery("openCIdUpdEffOde").bind("odeIdOggDeroga", odeIdOggDeroga).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffOde() {
        return closeCursor(cIdUpdEffOde);
    }

    public IOggDeroga fetchCIdUpdEffOde(IOggDeroga iOggDeroga) {
        return fetch(cIdUpdEffOde, iOggDeroga, selectByOdeDsRigaRm);
    }

    public IOggDeroga selectRec1(String odeIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggDeroga iOggDeroga) {
        return buildQuery("selectRec1").bind("odeIbOgg", odeIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByOdeDsRigaRm).singleResult(iOggDeroga);
    }

    public DbAccessStatus openCIboEffOde(String odeIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIboEffOde = buildQuery("openCIboEffOde").bind("odeIbOgg", odeIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboEffOde() {
        return closeCursor(cIboEffOde);
    }

    public IOggDeroga fetchCIboEffOde(IOggDeroga iOggDeroga) {
        return fetch(cIboEffOde, iOggDeroga, selectByOdeDsRigaRm);
    }

    public IOggDeroga selectRec2(int odeIdOgg, String odeTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggDeroga iOggDeroga) {
        return buildQuery("selectRec2").bind("odeIdOgg", odeIdOgg).bind("odeTpOgg", odeTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByOdeDsRigaRm).singleResult(iOggDeroga);
    }

    public DbAccessStatus openCIdoEffOde(int odeIdOgg, String odeTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffOde = buildQuery("openCIdoEffOde").bind("odeIdOgg", odeIdOgg).bind("odeTpOgg", odeTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffOde() {
        return closeCursor(cIdoEffOde);
    }

    public IOggDeroga fetchCIdoEffOde(IOggDeroga iOggDeroga) {
        return fetch(cIdoEffOde, iOggDeroga, selectByOdeDsRigaRm);
    }

    public IOggDeroga selectRec3(int odeIdOggDeroga, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IOggDeroga iOggDeroga) {
        return buildQuery("selectRec3").bind("odeIdOggDeroga", odeIdOggDeroga).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByOdeDsRigaRm).singleResult(iOggDeroga);
    }

    public IOggDeroga selectRec4(String odeIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IOggDeroga iOggDeroga) {
        return buildQuery("selectRec4").bind("odeIbOgg", odeIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByOdeDsRigaRm).singleResult(iOggDeroga);
    }

    public DbAccessStatus openCIboCpzOde(String odeIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIboCpzOde = buildQuery("openCIboCpzOde").bind("odeIbOgg", odeIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboCpzOde() {
        return closeCursor(cIboCpzOde);
    }

    public IOggDeroga fetchCIboCpzOde(IOggDeroga iOggDeroga) {
        return fetch(cIboCpzOde, iOggDeroga, selectByOdeDsRigaRm);
    }

    public IOggDeroga selectRec5(IOggDeroga iOggDeroga) {
        return buildQuery("selectRec5").bind(iOggDeroga).rowMapper(selectByOdeDsRigaRm).singleResult(iOggDeroga);
    }

    public DbAccessStatus openCIdoCpzOde(IOggDeroga iOggDeroga) {
        cIdoCpzOde = buildQuery("openCIdoCpzOde").bind(iOggDeroga).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzOde() {
        return closeCursor(cIdoCpzOde);
    }

    public IOggDeroga fetchCIdoCpzOde(IOggDeroga iOggDeroga) {
        return fetch(cIdoCpzOde, iOggDeroga, selectByOdeDsRigaRm);
    }
}
