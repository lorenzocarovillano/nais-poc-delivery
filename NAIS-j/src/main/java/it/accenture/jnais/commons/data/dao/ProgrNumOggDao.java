package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IProgrNumOgg;

/**
 * Data Access Object(DAO) for table [PROGR_NUM_OGG]
 * 
 */
public class ProgrNumOggDao extends BaseSqlDao<IProgrNumOgg> {

    private Cursor curP;
    private final IRowMapper<IProgrNumOgg> fetchCurPRm = buildNamedRowMapper(IProgrNumOgg.class, "ultProgr");

    public ProgrNumOggDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IProgrNumOgg> getToClass() {
        return IProgrNumOgg.class;
    }

    public IProgrNumOgg selectRec(int codCompagniaAnia, String formaAssicurativa, String codOggetto, String keyBusiness, IProgrNumOgg iProgrNumOgg) {
        return buildQuery("selectRec").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).bind("keyBusiness", keyBusiness).singleResult(iProgrNumOgg);
    }

    public DbAccessStatus insertRec(IProgrNumOgg iProgrNumOgg) {
        return buildQuery("insertRec").bind(iProgrNumOgg).executeInsert();
    }

    public DbAccessStatus updateRec(IProgrNumOgg iProgrNumOgg) {
        return buildQuery("updateRec").bind(iProgrNumOgg).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompagniaAnia, String formaAssicurativa, String codOggetto, String keyBusiness) {
        return buildQuery("deleteRec").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).bind("keyBusiness", keyBusiness).executeDelete();
    }

    public DbAccessStatus updateRec1(IProgrNumOgg iProgrNumOgg) {
        return buildQuery("updateRec1").bind(iProgrNumOgg).executeUpdate();
    }

    public DbAccessStatus openCurP(int codCompagniaAnia, String formaAssicurativa, String codOggetto, String keyBusiness) {
        curP = buildQuery("openCurP").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).bind("keyBusiness", keyBusiness).open();
        return dbStatus;
    }

    public DbAccessStatus closeCurP() {
        return closeCursor(curP);
    }

    public IProgrNumOgg fetchCurP(IProgrNumOgg iProgrNumOgg) {
        return fetch(curP, iProgrNumOgg, fetchCurPRm);
    }
}
