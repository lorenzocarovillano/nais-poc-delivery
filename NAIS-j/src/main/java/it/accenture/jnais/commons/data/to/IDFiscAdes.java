package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [D_FISC_ADES]
 * 
 */
public interface IDFiscAdes extends BaseSqlTo {

    /**
     * Host Variable DFA-ID-D-FISC-ADES
     * 
     */
    int getIdDFiscAdes();

    void setIdDFiscAdes(int idDFiscAdes);

    /**
     * Host Variable DFA-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Host Variable DFA-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable DFA-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for DFA-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable DFA-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable DFA-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable DFA-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable DFA-TP-ISC-FND
     * 
     */
    String getTpIscFnd();

    void setTpIscFnd(String tpIscFnd);

    /**
     * Nullable property for DFA-TP-ISC-FND
     * 
     */
    String getTpIscFndObj();

    void setTpIscFndObj(String tpIscFndObj);

    /**
     * Host Variable DFA-DT-ACCNS-RAPP-FND-DB
     * 
     */
    String getDtAccnsRappFndDb();

    void setDtAccnsRappFndDb(String dtAccnsRappFndDb);

    /**
     * Nullable property for DFA-DT-ACCNS-RAPP-FND-DB
     * 
     */
    String getDtAccnsRappFndDbObj();

    void setDtAccnsRappFndDbObj(String dtAccnsRappFndDbObj);

    /**
     * Host Variable DFA-IMP-CNBT-AZ-K1
     * 
     */
    AfDecimal getImpCnbtAzK1();

    void setImpCnbtAzK1(AfDecimal impCnbtAzK1);

    /**
     * Nullable property for DFA-IMP-CNBT-AZ-K1
     * 
     */
    AfDecimal getImpCnbtAzK1Obj();

    void setImpCnbtAzK1Obj(AfDecimal impCnbtAzK1Obj);

    /**
     * Host Variable DFA-IMP-CNBT-ISC-K1
     * 
     */
    AfDecimal getImpCnbtIscK1();

    void setImpCnbtIscK1(AfDecimal impCnbtIscK1);

    /**
     * Nullable property for DFA-IMP-CNBT-ISC-K1
     * 
     */
    AfDecimal getImpCnbtIscK1Obj();

    void setImpCnbtIscK1Obj(AfDecimal impCnbtIscK1Obj);

    /**
     * Host Variable DFA-IMP-CNBT-TFR-K1
     * 
     */
    AfDecimal getImpCnbtTfrK1();

    void setImpCnbtTfrK1(AfDecimal impCnbtTfrK1);

    /**
     * Nullable property for DFA-IMP-CNBT-TFR-K1
     * 
     */
    AfDecimal getImpCnbtTfrK1Obj();

    void setImpCnbtTfrK1Obj(AfDecimal impCnbtTfrK1Obj);

    /**
     * Host Variable DFA-IMP-CNBT-VOL-K1
     * 
     */
    AfDecimal getImpCnbtVolK1();

    void setImpCnbtVolK1(AfDecimal impCnbtVolK1);

    /**
     * Nullable property for DFA-IMP-CNBT-VOL-K1
     * 
     */
    AfDecimal getImpCnbtVolK1Obj();

    void setImpCnbtVolK1Obj(AfDecimal impCnbtVolK1Obj);

    /**
     * Host Variable DFA-IMP-CNBT-AZ-K2
     * 
     */
    AfDecimal getImpCnbtAzK2();

    void setImpCnbtAzK2(AfDecimal impCnbtAzK2);

    /**
     * Nullable property for DFA-IMP-CNBT-AZ-K2
     * 
     */
    AfDecimal getImpCnbtAzK2Obj();

    void setImpCnbtAzK2Obj(AfDecimal impCnbtAzK2Obj);

    /**
     * Host Variable DFA-IMP-CNBT-ISC-K2
     * 
     */
    AfDecimal getImpCnbtIscK2();

    void setImpCnbtIscK2(AfDecimal impCnbtIscK2);

    /**
     * Nullable property for DFA-IMP-CNBT-ISC-K2
     * 
     */
    AfDecimal getImpCnbtIscK2Obj();

    void setImpCnbtIscK2Obj(AfDecimal impCnbtIscK2Obj);

    /**
     * Host Variable DFA-IMP-CNBT-TFR-K2
     * 
     */
    AfDecimal getImpCnbtTfrK2();

    void setImpCnbtTfrK2(AfDecimal impCnbtTfrK2);

    /**
     * Nullable property for DFA-IMP-CNBT-TFR-K2
     * 
     */
    AfDecimal getImpCnbtTfrK2Obj();

    void setImpCnbtTfrK2Obj(AfDecimal impCnbtTfrK2Obj);

    /**
     * Host Variable DFA-IMP-CNBT-VOL-K2
     * 
     */
    AfDecimal getImpCnbtVolK2();

    void setImpCnbtVolK2(AfDecimal impCnbtVolK2);

    /**
     * Nullable property for DFA-IMP-CNBT-VOL-K2
     * 
     */
    AfDecimal getImpCnbtVolK2Obj();

    void setImpCnbtVolK2Obj(AfDecimal impCnbtVolK2Obj);

    /**
     * Host Variable DFA-MATU-K1
     * 
     */
    AfDecimal getMatuK1();

    void setMatuK1(AfDecimal matuK1);

    /**
     * Nullable property for DFA-MATU-K1
     * 
     */
    AfDecimal getMatuK1Obj();

    void setMatuK1Obj(AfDecimal matuK1Obj);

    /**
     * Host Variable DFA-MATU-RES-K1
     * 
     */
    AfDecimal getMatuResK1();

    void setMatuResK1(AfDecimal matuResK1);

    /**
     * Nullable property for DFA-MATU-RES-K1
     * 
     */
    AfDecimal getMatuResK1Obj();

    void setMatuResK1Obj(AfDecimal matuResK1Obj);

    /**
     * Host Variable DFA-MATU-K2
     * 
     */
    AfDecimal getMatuK2();

    void setMatuK2(AfDecimal matuK2);

    /**
     * Nullable property for DFA-MATU-K2
     * 
     */
    AfDecimal getMatuK2Obj();

    void setMatuK2Obj(AfDecimal matuK2Obj);

    /**
     * Host Variable DFA-IMPB-VIS
     * 
     */
    AfDecimal getImpbVis();

    void setImpbVis(AfDecimal impbVis);

    /**
     * Nullable property for DFA-IMPB-VIS
     * 
     */
    AfDecimal getImpbVisObj();

    void setImpbVisObj(AfDecimal impbVisObj);

    /**
     * Host Variable DFA-IMPST-VIS
     * 
     */
    AfDecimal getImpstVis();

    void setImpstVis(AfDecimal impstVis);

    /**
     * Nullable property for DFA-IMPST-VIS
     * 
     */
    AfDecimal getImpstVisObj();

    void setImpstVisObj(AfDecimal impstVisObj);

    /**
     * Host Variable DFA-IMPB-IS-K2
     * 
     */
    AfDecimal getImpbIsK2();

    void setImpbIsK2(AfDecimal impbIsK2);

    /**
     * Nullable property for DFA-IMPB-IS-K2
     * 
     */
    AfDecimal getImpbIsK2Obj();

    void setImpbIsK2Obj(AfDecimal impbIsK2Obj);

    /**
     * Host Variable DFA-IMPST-SOST-K2
     * 
     */
    AfDecimal getImpstSostK2();

    void setImpstSostK2(AfDecimal impstSostK2);

    /**
     * Nullable property for DFA-IMPST-SOST-K2
     * 
     */
    AfDecimal getImpstSostK2Obj();

    void setImpstSostK2Obj(AfDecimal impstSostK2Obj);

    /**
     * Host Variable DFA-RIDZ-TFR
     * 
     */
    AfDecimal getRidzTfr();

    void setRidzTfr(AfDecimal ridzTfr);

    /**
     * Nullable property for DFA-RIDZ-TFR
     * 
     */
    AfDecimal getRidzTfrObj();

    void setRidzTfrObj(AfDecimal ridzTfrObj);

    /**
     * Host Variable DFA-PC-TFR
     * 
     */
    AfDecimal getPcTfr();

    void setPcTfr(AfDecimal pcTfr);

    /**
     * Nullable property for DFA-PC-TFR
     * 
     */
    AfDecimal getPcTfrObj();

    void setPcTfrObj(AfDecimal pcTfrObj);

    /**
     * Host Variable DFA-ALQ-TFR
     * 
     */
    AfDecimal getAlqTfr();

    void setAlqTfr(AfDecimal alqTfr);

    /**
     * Nullable property for DFA-ALQ-TFR
     * 
     */
    AfDecimal getAlqTfrObj();

    void setAlqTfrObj(AfDecimal alqTfrObj);

    /**
     * Host Variable DFA-TOT-ANTIC
     * 
     */
    AfDecimal getTotAntic();

    void setTotAntic(AfDecimal totAntic);

    /**
     * Nullable property for DFA-TOT-ANTIC
     * 
     */
    AfDecimal getTotAnticObj();

    void setTotAnticObj(AfDecimal totAnticObj);

    /**
     * Host Variable DFA-IMPB-TFR-ANTIC
     * 
     */
    AfDecimal getImpbTfrAntic();

    void setImpbTfrAntic(AfDecimal impbTfrAntic);

    /**
     * Nullable property for DFA-IMPB-TFR-ANTIC
     * 
     */
    AfDecimal getImpbTfrAnticObj();

    void setImpbTfrAnticObj(AfDecimal impbTfrAnticObj);

    /**
     * Host Variable DFA-IMPST-TFR-ANTIC
     * 
     */
    AfDecimal getImpstTfrAntic();

    void setImpstTfrAntic(AfDecimal impstTfrAntic);

    /**
     * Nullable property for DFA-IMPST-TFR-ANTIC
     * 
     */
    AfDecimal getImpstTfrAnticObj();

    void setImpstTfrAnticObj(AfDecimal impstTfrAnticObj);

    /**
     * Host Variable DFA-RIDZ-TFR-SU-ANTIC
     * 
     */
    AfDecimal getRidzTfrSuAntic();

    void setRidzTfrSuAntic(AfDecimal ridzTfrSuAntic);

    /**
     * Nullable property for DFA-RIDZ-TFR-SU-ANTIC
     * 
     */
    AfDecimal getRidzTfrSuAnticObj();

    void setRidzTfrSuAnticObj(AfDecimal ridzTfrSuAnticObj);

    /**
     * Host Variable DFA-IMPB-VIS-ANTIC
     * 
     */
    AfDecimal getImpbVisAntic();

    void setImpbVisAntic(AfDecimal impbVisAntic);

    /**
     * Nullable property for DFA-IMPB-VIS-ANTIC
     * 
     */
    AfDecimal getImpbVisAnticObj();

    void setImpbVisAnticObj(AfDecimal impbVisAnticObj);

    /**
     * Host Variable DFA-IMPST-VIS-ANTIC
     * 
     */
    AfDecimal getImpstVisAntic();

    void setImpstVisAntic(AfDecimal impstVisAntic);

    /**
     * Nullable property for DFA-IMPST-VIS-ANTIC
     * 
     */
    AfDecimal getImpstVisAnticObj();

    void setImpstVisAnticObj(AfDecimal impstVisAnticObj);

    /**
     * Host Variable DFA-IMPB-IS-K2-ANTIC
     * 
     */
    AfDecimal getImpbIsK2Antic();

    void setImpbIsK2Antic(AfDecimal impbIsK2Antic);

    /**
     * Nullable property for DFA-IMPB-IS-K2-ANTIC
     * 
     */
    AfDecimal getImpbIsK2AnticObj();

    void setImpbIsK2AnticObj(AfDecimal impbIsK2AnticObj);

    /**
     * Host Variable DFA-IMPST-SOST-K2-ANTI
     * 
     */
    AfDecimal getImpstSostK2Anti();

    void setImpstSostK2Anti(AfDecimal impstSostK2Anti);

    /**
     * Nullable property for DFA-IMPST-SOST-K2-ANTI
     * 
     */
    AfDecimal getImpstSostK2AntiObj();

    void setImpstSostK2AntiObj(AfDecimal impstSostK2AntiObj);

    /**
     * Host Variable DFA-ULT-COMMIS-TRASFE
     * 
     */
    AfDecimal getUltCommisTrasfe();

    void setUltCommisTrasfe(AfDecimal ultCommisTrasfe);

    /**
     * Nullable property for DFA-ULT-COMMIS-TRASFE
     * 
     */
    AfDecimal getUltCommisTrasfeObj();

    void setUltCommisTrasfeObj(AfDecimal ultCommisTrasfeObj);

    /**
     * Host Variable DFA-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for DFA-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable DFA-ALQ-PRVR
     * 
     */
    AfDecimal getAlqPrvr();

    void setAlqPrvr(AfDecimal alqPrvr);

    /**
     * Nullable property for DFA-ALQ-PRVR
     * 
     */
    AfDecimal getAlqPrvrObj();

    void setAlqPrvrObj(AfDecimal alqPrvrObj);

    /**
     * Host Variable DFA-PC-ESE-IMPST-TFR
     * 
     */
    AfDecimal getPcEseImpstTfr();

    void setPcEseImpstTfr(AfDecimal pcEseImpstTfr);

    /**
     * Nullable property for DFA-PC-ESE-IMPST-TFR
     * 
     */
    AfDecimal getPcEseImpstTfrObj();

    void setPcEseImpstTfrObj(AfDecimal pcEseImpstTfrObj);

    /**
     * Host Variable DFA-IMP-ESE-IMPST-TFR
     * 
     */
    AfDecimal getImpEseImpstTfr();

    void setImpEseImpstTfr(AfDecimal impEseImpstTfr);

    /**
     * Nullable property for DFA-IMP-ESE-IMPST-TFR
     * 
     */
    AfDecimal getImpEseImpstTfrObj();

    void setImpEseImpstTfrObj(AfDecimal impEseImpstTfrObj);

    /**
     * Host Variable DFA-ANZ-CNBTVA-CARASS
     * 
     */
    short getAnzCnbtvaCarass();

    void setAnzCnbtvaCarass(short anzCnbtvaCarass);

    /**
     * Nullable property for DFA-ANZ-CNBTVA-CARASS
     * 
     */
    Short getAnzCnbtvaCarassObj();

    void setAnzCnbtvaCarassObj(Short anzCnbtvaCarassObj);

    /**
     * Host Variable DFA-ANZ-CNBTVA-CARAZI
     * 
     */
    short getAnzCnbtvaCarazi();

    void setAnzCnbtvaCarazi(short anzCnbtvaCarazi);

    /**
     * Nullable property for DFA-ANZ-CNBTVA-CARAZI
     * 
     */
    Short getAnzCnbtvaCaraziObj();

    void setAnzCnbtvaCaraziObj(Short anzCnbtvaCaraziObj);

    /**
     * Host Variable DFA-ANZ-SRVZ
     * 
     */
    short getAnzSrvz();

    void setAnzSrvz(short anzSrvz);

    /**
     * Nullable property for DFA-ANZ-SRVZ
     * 
     */
    Short getAnzSrvzObj();

    void setAnzSrvzObj(Short anzSrvzObj);

    /**
     * Host Variable DFA-IMP-CNBT-NDED-K1
     * 
     */
    AfDecimal getImpCnbtNdedK1();

    void setImpCnbtNdedK1(AfDecimal impCnbtNdedK1);

    /**
     * Nullable property for DFA-IMP-CNBT-NDED-K1
     * 
     */
    AfDecimal getImpCnbtNdedK1Obj();

    void setImpCnbtNdedK1Obj(AfDecimal impCnbtNdedK1Obj);

    /**
     * Host Variable DFA-IMP-CNBT-NDED-K2
     * 
     */
    AfDecimal getImpCnbtNdedK2();

    void setImpCnbtNdedK2(AfDecimal impCnbtNdedK2);

    /**
     * Nullable property for DFA-IMP-CNBT-NDED-K2
     * 
     */
    AfDecimal getImpCnbtNdedK2Obj();

    void setImpCnbtNdedK2Obj(AfDecimal impCnbtNdedK2Obj);

    /**
     * Host Variable DFA-IMP-CNBT-NDED-K3
     * 
     */
    AfDecimal getImpCnbtNdedK3();

    void setImpCnbtNdedK3(AfDecimal impCnbtNdedK3);

    /**
     * Nullable property for DFA-IMP-CNBT-NDED-K3
     * 
     */
    AfDecimal getImpCnbtNdedK3Obj();

    void setImpCnbtNdedK3Obj(AfDecimal impCnbtNdedK3Obj);

    /**
     * Host Variable DFA-IMP-CNBT-AZ-K3
     * 
     */
    AfDecimal getImpCnbtAzK3();

    void setImpCnbtAzK3(AfDecimal impCnbtAzK3);

    /**
     * Nullable property for DFA-IMP-CNBT-AZ-K3
     * 
     */
    AfDecimal getImpCnbtAzK3Obj();

    void setImpCnbtAzK3Obj(AfDecimal impCnbtAzK3Obj);

    /**
     * Host Variable DFA-IMP-CNBT-ISC-K3
     * 
     */
    AfDecimal getImpCnbtIscK3();

    void setImpCnbtIscK3(AfDecimal impCnbtIscK3);

    /**
     * Nullable property for DFA-IMP-CNBT-ISC-K3
     * 
     */
    AfDecimal getImpCnbtIscK3Obj();

    void setImpCnbtIscK3Obj(AfDecimal impCnbtIscK3Obj);

    /**
     * Host Variable DFA-IMP-CNBT-TFR-K3
     * 
     */
    AfDecimal getImpCnbtTfrK3();

    void setImpCnbtTfrK3(AfDecimal impCnbtTfrK3);

    /**
     * Nullable property for DFA-IMP-CNBT-TFR-K3
     * 
     */
    AfDecimal getImpCnbtTfrK3Obj();

    void setImpCnbtTfrK3Obj(AfDecimal impCnbtTfrK3Obj);

    /**
     * Host Variable DFA-IMP-CNBT-VOL-K3
     * 
     */
    AfDecimal getImpCnbtVolK3();

    void setImpCnbtVolK3(AfDecimal impCnbtVolK3);

    /**
     * Nullable property for DFA-IMP-CNBT-VOL-K3
     * 
     */
    AfDecimal getImpCnbtVolK3Obj();

    void setImpCnbtVolK3Obj(AfDecimal impCnbtVolK3Obj);

    /**
     * Host Variable DFA-MATU-K3
     * 
     */
    AfDecimal getMatuK3();

    void setMatuK3(AfDecimal matuK3);

    /**
     * Nullable property for DFA-MATU-K3
     * 
     */
    AfDecimal getMatuK3Obj();

    void setMatuK3Obj(AfDecimal matuK3Obj);

    /**
     * Host Variable DFA-IMPB-252-ANTIC
     * 
     */
    AfDecimal getImpb252Antic();

    void setImpb252Antic(AfDecimal impb252Antic);

    /**
     * Nullable property for DFA-IMPB-252-ANTIC
     * 
     */
    AfDecimal getImpb252AnticObj();

    void setImpb252AnticObj(AfDecimal impb252AnticObj);

    /**
     * Host Variable DFA-IMPST-252-ANTIC
     * 
     */
    AfDecimal getImpst252Antic();

    void setImpst252Antic(AfDecimal impst252Antic);

    /**
     * Nullable property for DFA-IMPST-252-ANTIC
     * 
     */
    AfDecimal getImpst252AnticObj();

    void setImpst252AnticObj(AfDecimal impst252AnticObj);

    /**
     * Host Variable DFA-DT-1A-CNBZ-DB
     * 
     */
    String getDt1aCnbzDb();

    void setDt1aCnbzDb(String dt1aCnbzDb);

    /**
     * Nullable property for DFA-DT-1A-CNBZ-DB
     * 
     */
    String getDt1aCnbzDbObj();

    void setDt1aCnbzDbObj(String dt1aCnbzDbObj);

    /**
     * Host Variable DFA-COMMIS-DI-TRASFE
     * 
     */
    AfDecimal getCommisDiTrasfe();

    void setCommisDiTrasfe(AfDecimal commisDiTrasfe);

    /**
     * Nullable property for DFA-COMMIS-DI-TRASFE
     * 
     */
    AfDecimal getCommisDiTrasfeObj();

    void setCommisDiTrasfeObj(AfDecimal commisDiTrasfeObj);

    /**
     * Host Variable DFA-AA-CNBZ-K1
     * 
     */
    short getAaCnbzK1();

    void setAaCnbzK1(short aaCnbzK1);

    /**
     * Nullable property for DFA-AA-CNBZ-K1
     * 
     */
    Short getAaCnbzK1Obj();

    void setAaCnbzK1Obj(Short aaCnbzK1Obj);

    /**
     * Host Variable DFA-AA-CNBZ-K2
     * 
     */
    short getAaCnbzK2();

    void setAaCnbzK2(short aaCnbzK2);

    /**
     * Nullable property for DFA-AA-CNBZ-K2
     * 
     */
    Short getAaCnbzK2Obj();

    void setAaCnbzK2Obj(Short aaCnbzK2Obj);

    /**
     * Host Variable DFA-AA-CNBZ-K3
     * 
     */
    short getAaCnbzK3();

    void setAaCnbzK3(short aaCnbzK3);

    /**
     * Nullable property for DFA-AA-CNBZ-K3
     * 
     */
    Short getAaCnbzK3Obj();

    void setAaCnbzK3Obj(Short aaCnbzK3Obj);

    /**
     * Host Variable DFA-MM-CNBZ-K1
     * 
     */
    short getMmCnbzK1();

    void setMmCnbzK1(short mmCnbzK1);

    /**
     * Nullable property for DFA-MM-CNBZ-K1
     * 
     */
    Short getMmCnbzK1Obj();

    void setMmCnbzK1Obj(Short mmCnbzK1Obj);

    /**
     * Host Variable DFA-MM-CNBZ-K2
     * 
     */
    short getMmCnbzK2();

    void setMmCnbzK2(short mmCnbzK2);

    /**
     * Nullable property for DFA-MM-CNBZ-K2
     * 
     */
    Short getMmCnbzK2Obj();

    void setMmCnbzK2Obj(Short mmCnbzK2Obj);

    /**
     * Host Variable DFA-MM-CNBZ-K3
     * 
     */
    short getMmCnbzK3();

    void setMmCnbzK3(short mmCnbzK3);

    /**
     * Nullable property for DFA-MM-CNBZ-K3
     * 
     */
    Short getMmCnbzK3Obj();

    void setMmCnbzK3Obj(Short mmCnbzK3Obj);

    /**
     * Host Variable DFA-FL-APPLZ-NEWFIS
     * 
     */
    char getFlApplzNewfis();

    void setFlApplzNewfis(char flApplzNewfis);

    /**
     * Nullable property for DFA-FL-APPLZ-NEWFIS
     * 
     */
    Character getFlApplzNewfisObj();

    void setFlApplzNewfisObj(Character flApplzNewfisObj);

    /**
     * Host Variable DFA-REDT-TASS-ABBAT-K3
     * 
     */
    AfDecimal getRedtTassAbbatK3();

    void setRedtTassAbbatK3(AfDecimal redtTassAbbatK3);

    /**
     * Nullable property for DFA-REDT-TASS-ABBAT-K3
     * 
     */
    AfDecimal getRedtTassAbbatK3Obj();

    void setRedtTassAbbatK3Obj(AfDecimal redtTassAbbatK3Obj);

    /**
     * Host Variable DFA-CNBT-ECC-4X100-K1
     * 
     */
    AfDecimal getCnbtEcc4x100K1();

    void setCnbtEcc4x100K1(AfDecimal cnbtEcc4x100K1);

    /**
     * Nullable property for DFA-CNBT-ECC-4X100-K1
     * 
     */
    AfDecimal getCnbtEcc4x100K1Obj();

    void setCnbtEcc4x100K1Obj(AfDecimal cnbtEcc4x100K1Obj);

    /**
     * Host Variable DFA-DS-RIGA
     * 
     */
    long getDfaDsRiga();

    void setDfaDsRiga(long dfaDsRiga);

    /**
     * Host Variable DFA-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable DFA-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable DFA-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable DFA-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable DFA-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable DFA-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable DFA-CREDITO-IS
     * 
     */
    AfDecimal getCreditoIs();

    void setCreditoIs(AfDecimal creditoIs);

    /**
     * Nullable property for DFA-CREDITO-IS
     * 
     */
    AfDecimal getCreditoIsObj();

    void setCreditoIsObj(AfDecimal creditoIsObj);

    /**
     * Host Variable DFA-REDT-TASS-ABBAT-K2
     * 
     */
    AfDecimal getRedtTassAbbatK2();

    void setRedtTassAbbatK2(AfDecimal redtTassAbbatK2);

    /**
     * Nullable property for DFA-REDT-TASS-ABBAT-K2
     * 
     */
    AfDecimal getRedtTassAbbatK2Obj();

    void setRedtTassAbbatK2Obj(AfDecimal redtTassAbbatK2Obj);

    /**
     * Host Variable DFA-IMPB-IS-K3
     * 
     */
    AfDecimal getImpbIsK3();

    void setImpbIsK3(AfDecimal impbIsK3);

    /**
     * Nullable property for DFA-IMPB-IS-K3
     * 
     */
    AfDecimal getImpbIsK3Obj();

    void setImpbIsK3Obj(AfDecimal impbIsK3Obj);

    /**
     * Host Variable DFA-IMPST-SOST-K3
     * 
     */
    AfDecimal getImpstSostK3();

    void setImpstSostK3(AfDecimal impstSostK3);

    /**
     * Nullable property for DFA-IMPST-SOST-K3
     * 
     */
    AfDecimal getImpstSostK3Obj();

    void setImpstSostK3Obj(AfDecimal impstSostK3Obj);

    /**
     * Host Variable DFA-IMPB-252-K3
     * 
     */
    AfDecimal getImpb252K3();

    void setImpb252K3(AfDecimal impb252K3);

    /**
     * Nullable property for DFA-IMPB-252-K3
     * 
     */
    AfDecimal getImpb252K3Obj();

    void setImpb252K3Obj(AfDecimal impb252K3Obj);

    /**
     * Host Variable DFA-IMPST-252-K3
     * 
     */
    AfDecimal getImpst252K3();

    void setImpst252K3(AfDecimal impst252K3);

    /**
     * Nullable property for DFA-IMPST-252-K3
     * 
     */
    AfDecimal getImpst252K3Obj();

    void setImpst252K3Obj(AfDecimal impst252K3Obj);

    /**
     * Host Variable DFA-IMPB-IS-K3-ANTIC
     * 
     */
    AfDecimal getImpbIsK3Antic();

    void setImpbIsK3Antic(AfDecimal impbIsK3Antic);

    /**
     * Nullable property for DFA-IMPB-IS-K3-ANTIC
     * 
     */
    AfDecimal getImpbIsK3AnticObj();

    void setImpbIsK3AnticObj(AfDecimal impbIsK3AnticObj);

    /**
     * Host Variable DFA-IMPST-SOST-K3-ANTI
     * 
     */
    AfDecimal getImpstSostK3Anti();

    void setImpstSostK3Anti(AfDecimal impstSostK3Anti);

    /**
     * Nullable property for DFA-IMPST-SOST-K3-ANTI
     * 
     */
    AfDecimal getImpstSostK3AntiObj();

    void setImpstSostK3AntiObj(AfDecimal impstSostK3AntiObj);

    /**
     * Host Variable DFA-IMPB-IRPEF-K1-ANTI
     * 
     */
    AfDecimal getImpbIrpefK1Anti();

    void setImpbIrpefK1Anti(AfDecimal impbIrpefK1Anti);

    /**
     * Nullable property for DFA-IMPB-IRPEF-K1-ANTI
     * 
     */
    AfDecimal getImpbIrpefK1AntiObj();

    void setImpbIrpefK1AntiObj(AfDecimal impbIrpefK1AntiObj);

    /**
     * Host Variable DFA-IMPST-IRPEF-K1-ANT
     * 
     */
    AfDecimal getImpstIrpefK1Ant();

    void setImpstIrpefK1Ant(AfDecimal impstIrpefK1Ant);

    /**
     * Nullable property for DFA-IMPST-IRPEF-K1-ANT
     * 
     */
    AfDecimal getImpstIrpefK1AntObj();

    void setImpstIrpefK1AntObj(AfDecimal impstIrpefK1AntObj);

    /**
     * Host Variable DFA-IMPB-IRPEF-K2-ANTI
     * 
     */
    AfDecimal getImpbIrpefK2Anti();

    void setImpbIrpefK2Anti(AfDecimal impbIrpefK2Anti);

    /**
     * Nullable property for DFA-IMPB-IRPEF-K2-ANTI
     * 
     */
    AfDecimal getImpbIrpefK2AntiObj();

    void setImpbIrpefK2AntiObj(AfDecimal impbIrpefK2AntiObj);

    /**
     * Host Variable DFA-IMPST-IRPEF-K2-ANT
     * 
     */
    AfDecimal getImpstIrpefK2Ant();

    void setImpstIrpefK2Ant(AfDecimal impstIrpefK2Ant);

    /**
     * Nullable property for DFA-IMPST-IRPEF-K2-ANT
     * 
     */
    AfDecimal getImpstIrpefK2AntObj();

    void setImpstIrpefK2AntObj(AfDecimal impstIrpefK2AntObj);

    /**
     * Host Variable DFA-DT-CESSAZIONE-DB
     * 
     */
    String getDtCessazioneDb();

    void setDtCessazioneDb(String dtCessazioneDb);

    /**
     * Nullable property for DFA-DT-CESSAZIONE-DB
     * 
     */
    String getDtCessazioneDbObj();

    void setDtCessazioneDbObj(String dtCessazioneDbObj);

    /**
     * Host Variable DFA-TOT-IMPST
     * 
     */
    AfDecimal getTotImpst();

    void setTotImpst(AfDecimal totImpst);

    /**
     * Nullable property for DFA-TOT-IMPST
     * 
     */
    AfDecimal getTotImpstObj();

    void setTotImpstObj(AfDecimal totImpstObj);

    /**
     * Host Variable DFA-ONER-TRASFE
     * 
     */
    AfDecimal getOnerTrasfe();

    void setOnerTrasfe(AfDecimal onerTrasfe);

    /**
     * Nullable property for DFA-ONER-TRASFE
     * 
     */
    AfDecimal getOnerTrasfeObj();

    void setOnerTrasfeObj(AfDecimal onerTrasfeObj);

    /**
     * Host Variable DFA-IMP-NET-TRASFERITO
     * 
     */
    AfDecimal getImpNetTrasferito();

    void setImpNetTrasferito(AfDecimal impNetTrasferito);

    /**
     * Nullable property for DFA-IMP-NET-TRASFERITO
     * 
     */
    AfDecimal getImpNetTrasferitoObj();

    void setImpNetTrasferitoObj(AfDecimal impNetTrasferitoObj);
};
