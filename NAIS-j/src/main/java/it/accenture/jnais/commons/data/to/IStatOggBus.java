package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [STAT_OGG_BUS]
 * 
 */
public interface IStatOggBus extends BaseSqlTo {

    /**
     * Host Variable STB-ID-OGG
     * 
     */
    int getStbIdOgg();

    void setStbIdOgg(int stbIdOgg);

    /**
     * Host Variable STB-TP-OGG
     * 
     */
    String getStbTpOgg();

    void setStbTpOgg(String stbTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable WS-DT-INFINITO-1
     * 
     */
    String getWsDtInfinito1();

    void setWsDtInfinito1(String wsDtInfinito1);

    /**
     * Host Variable STB-ID-STAT-OGG-BUS
     * 
     */
    int getIdStatOggBus();

    void setIdStatOggBus(int idStatOggBus);

    /**
     * Host Variable STB-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable STB-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for STB-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable STB-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable STB-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable STB-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable STB-TP-STAT-BUS
     * 
     */
    String getTpStatBus();

    void setTpStatBus(String tpStatBus);

    /**
     * Host Variable STB-TP-CAUS
     * 
     */
    String getTpCaus();

    void setTpCaus(String tpCaus);

    /**
     * Host Variable STB-DS-RIGA
     * 
     */
    long getStbDsRiga();

    void setStbDsRiga(long stbDsRiga);

    /**
     * Host Variable STB-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable STB-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable STB-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable STB-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable STB-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable STB-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable LDBV1701-TP-CAUS
     * 
     */
    String getCaus();

    void setCaus(String caus);

    /**
     * Host Variable LDBV1701-TP-STAT
     * 
     */
    String getStat();

    void setStat(String stat);

    /**
     * Host Variable LDBV1701-TP-OGG
     * 
     */
    String getLdbv1701TpOgg();

    void setLdbv1701TpOgg(String ldbv1701TpOgg);

    /**
     * Host Variable LDBV1701-ID-OGG
     * 
     */
    int getLdbv1701IdOgg();

    void setLdbv1701IdOgg(int ldbv1701IdOgg);
};
