package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ITcontLiq;

/**
 * Data Access Object(DAO) for table [TCONT_LIQ]
 * 
 */
public class TcontLiqDao extends BaseSqlDao<ITcontLiq> {

    private Cursor cIdUpdEffTcl;
    private final IRowMapper<ITcontLiq> selectByTclDsRigaRm = buildNamedRowMapper(ITcontLiq.class, "idTcontLiq", "idPercLiq", "idBnficrLiq", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtVltDbObj", "impLrdLiqtoObj", "impPrestObj", "impIntrPrestObj", "impRatObj", "impUtiObj", "impRitTfrObj", "impRitAccObj", "impRitVisObj", "impbTfrObj", "impbAccObj", "impbVisObj", "impRimbObj", "impCortvoObj", "impbImpstPrvrObj", "impstPrvrObj", "impbImpst252Obj", "impst252Obj", "impIsObj", "impDirLiqObj", "impNetLiqtoObj", "impEfflqObj", "codDvsObj", "tpMezPagAccrObj", "estrCntCorrAccrObj", "tclDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "tpStatTitObj", "impbVis1382011Obj", "impstVis1382011Obj", "impstSost1382011Obj", "impIntrRitPagObj", "impbIsObj", "impbIs1382011Obj", "impbVis662014Obj", "impstVis662014Obj", "impbIs662014Obj", "impstSost662014Obj");

    public TcontLiqDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ITcontLiq> getToClass() {
        return ITcontLiq.class;
    }

    public ITcontLiq selectByTclDsRiga(long tclDsRiga, ITcontLiq iTcontLiq) {
        return buildQuery("selectByTclDsRiga").bind("tclDsRiga", tclDsRiga).rowMapper(selectByTclDsRigaRm).singleResult(iTcontLiq);
    }

    public DbAccessStatus insertRec(ITcontLiq iTcontLiq) {
        return buildQuery("insertRec").bind(iTcontLiq).executeInsert();
    }

    public DbAccessStatus updateRec(ITcontLiq iTcontLiq) {
        return buildQuery("updateRec").bind(iTcontLiq).executeUpdate();
    }

    public DbAccessStatus deleteByTclDsRiga(long tclDsRiga) {
        return buildQuery("deleteByTclDsRiga").bind("tclDsRiga", tclDsRiga).executeDelete();
    }

    public ITcontLiq selectRec(int tclIdTcontLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITcontLiq iTcontLiq) {
        return buildQuery("selectRec").bind("tclIdTcontLiq", tclIdTcontLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTclDsRigaRm).singleResult(iTcontLiq);
    }

    public DbAccessStatus updateRec1(ITcontLiq iTcontLiq) {
        return buildQuery("updateRec1").bind(iTcontLiq).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffTcl(int tclIdTcontLiq, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffTcl = buildQuery("openCIdUpdEffTcl").bind("tclIdTcontLiq", tclIdTcontLiq).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffTcl() {
        return closeCursor(cIdUpdEffTcl);
    }

    public ITcontLiq fetchCIdUpdEffTcl(ITcontLiq iTcontLiq) {
        return fetch(cIdUpdEffTcl, iTcontLiq, selectByTclDsRigaRm);
    }

    public ITcontLiq selectRec1(int tclIdTcontLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITcontLiq iTcontLiq) {
        return buildQuery("selectRec1").bind("tclIdTcontLiq", tclIdTcontLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTclDsRigaRm).singleResult(iTcontLiq);
    }
}
