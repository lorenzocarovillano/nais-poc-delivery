package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [POLI, RAPP_ANA]
 * 
 */
public interface IPoliRappAna extends BaseSqlTo {

    /**
     * Host Variable POL-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable POL-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable POL-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for POL-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable POL-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for POL-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable POL-IB-PROP
     * 
     */
    String getIbProp();

    void setIbProp(String ibProp);

    /**
     * Host Variable POL-DT-PROP-DB
     * 
     */
    String getDtPropDb();

    void setDtPropDb(String dtPropDb);

    /**
     * Nullable property for POL-DT-PROP-DB
     * 
     */
    String getDtPropDbObj();

    void setDtPropDbObj(String dtPropDbObj);

    /**
     * Host Variable POL-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable POL-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable POL-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable POL-DT-DECOR-DB
     * 
     */
    String getDtDecorDb();

    void setDtDecorDb(String dtDecorDb);

    /**
     * Host Variable POL-DT-EMIS-DB
     * 
     */
    String getDtEmisDb();

    void setDtEmisDb(String dtEmisDb);

    /**
     * Host Variable POL-TP-POLI
     * 
     */
    String getTpPoli();

    void setTpPoli(String tpPoli);

    /**
     * Host Variable POL-DUR-AA
     * 
     */
    int getDurAa();

    void setDurAa(int durAa);

    /**
     * Nullable property for POL-DUR-AA
     * 
     */
    Integer getDurAaObj();

    void setDurAaObj(Integer durAaObj);

    /**
     * Host Variable POL-DUR-MM
     * 
     */
    int getDurMm();

    void setDurMm(int durMm);

    /**
     * Nullable property for POL-DUR-MM
     * 
     */
    Integer getDurMmObj();

    void setDurMmObj(Integer durMmObj);

    /**
     * Host Variable POL-DT-SCAD-DB
     * 
     */
    String getDtScadDb();

    void setDtScadDb(String dtScadDb);

    /**
     * Nullable property for POL-DT-SCAD-DB
     * 
     */
    String getDtScadDbObj();

    void setDtScadDbObj(String dtScadDbObj);

    /**
     * Host Variable POL-COD-PROD
     * 
     */
    String getCodProd();

    void setCodProd(String codProd);

    /**
     * Host Variable POL-DT-INI-VLDT-PROD-DB
     * 
     */
    String getDtIniVldtProdDb();

    void setDtIniVldtProdDb(String dtIniVldtProdDb);

    /**
     * Host Variable POL-COD-CONV
     * 
     */
    String getCodConv();

    void setCodConv(String codConv);

    /**
     * Nullable property for POL-COD-CONV
     * 
     */
    String getCodConvObj();

    void setCodConvObj(String codConvObj);

    /**
     * Host Variable POL-COD-RAMO
     * 
     */
    String getCodRamo();

    void setCodRamo(String codRamo);

    /**
     * Nullable property for POL-COD-RAMO
     * 
     */
    String getCodRamoObj();

    void setCodRamoObj(String codRamoObj);

    /**
     * Host Variable POL-DT-INI-VLDT-CONV-DB
     * 
     */
    String getDtIniVldtConvDb();

    void setDtIniVldtConvDb(String dtIniVldtConvDb);

    /**
     * Nullable property for POL-DT-INI-VLDT-CONV-DB
     * 
     */
    String getDtIniVldtConvDbObj();

    void setDtIniVldtConvDbObj(String dtIniVldtConvDbObj);

    /**
     * Host Variable POL-DT-APPLZ-CONV-DB
     * 
     */
    String getDtApplzConvDb();

    void setDtApplzConvDb(String dtApplzConvDb);

    /**
     * Nullable property for POL-DT-APPLZ-CONV-DB
     * 
     */
    String getDtApplzConvDbObj();

    void setDtApplzConvDbObj(String dtApplzConvDbObj);

    /**
     * Host Variable POL-TP-FRM-ASSVA
     * 
     */
    String getTpFrmAssva();

    void setTpFrmAssva(String tpFrmAssva);

    /**
     * Host Variable POL-TP-RGM-FISC
     * 
     */
    String getTpRgmFisc();

    void setTpRgmFisc(String tpRgmFisc);

    /**
     * Nullable property for POL-TP-RGM-FISC
     * 
     */
    String getTpRgmFiscObj();

    void setTpRgmFiscObj(String tpRgmFiscObj);

    /**
     * Host Variable POL-FL-ESTAS
     * 
     */
    char getFlEstas();

    void setFlEstas(char flEstas);

    /**
     * Nullable property for POL-FL-ESTAS
     * 
     */
    Character getFlEstasObj();

    void setFlEstasObj(Character flEstasObj);

    /**
     * Host Variable POL-FL-RSH-COMUN
     * 
     */
    char getFlRshComun();

    void setFlRshComun(char flRshComun);

    /**
     * Nullable property for POL-FL-RSH-COMUN
     * 
     */
    Character getFlRshComunObj();

    void setFlRshComunObj(Character flRshComunObj);

    /**
     * Host Variable POL-FL-RSH-COMUN-COND
     * 
     */
    char getFlRshComunCond();

    void setFlRshComunCond(char flRshComunCond);

    /**
     * Nullable property for POL-FL-RSH-COMUN-COND
     * 
     */
    Character getFlRshComunCondObj();

    void setFlRshComunCondObj(Character flRshComunCondObj);

    /**
     * Host Variable POL-TP-LIV-GENZ-TIT
     * 
     */
    String getTpLivGenzTit();

    void setTpLivGenzTit(String tpLivGenzTit);

    /**
     * Host Variable POL-FL-COP-FINANZ
     * 
     */
    char getFlCopFinanz();

    void setFlCopFinanz(char flCopFinanz);

    /**
     * Nullable property for POL-FL-COP-FINANZ
     * 
     */
    Character getFlCopFinanzObj();

    void setFlCopFinanzObj(Character flCopFinanzObj);

    /**
     * Host Variable POL-TP-APPLZ-DIR
     * 
     */
    String getTpApplzDir();

    void setTpApplzDir(String tpApplzDir);

    /**
     * Nullable property for POL-TP-APPLZ-DIR
     * 
     */
    String getTpApplzDirObj();

    void setTpApplzDirObj(String tpApplzDirObj);

    /**
     * Host Variable POL-SPE-MED
     * 
     */
    AfDecimal getSpeMed();

    void setSpeMed(AfDecimal speMed);

    /**
     * Nullable property for POL-SPE-MED
     * 
     */
    AfDecimal getSpeMedObj();

    void setSpeMedObj(AfDecimal speMedObj);

    /**
     * Host Variable POL-DIR-EMIS
     * 
     */
    AfDecimal getDirEmis();

    void setDirEmis(AfDecimal dirEmis);

    /**
     * Nullable property for POL-DIR-EMIS
     * 
     */
    AfDecimal getDirEmisObj();

    void setDirEmisObj(AfDecimal dirEmisObj);

    /**
     * Host Variable POL-DIR-1O-VERS
     * 
     */
    AfDecimal getDir1oVers();

    void setDir1oVers(AfDecimal dir1oVers);

    /**
     * Nullable property for POL-DIR-1O-VERS
     * 
     */
    AfDecimal getDir1oVersObj();

    void setDir1oVersObj(AfDecimal dir1oVersObj);

    /**
     * Host Variable POL-DIR-VERS-AGG
     * 
     */
    AfDecimal getDirVersAgg();

    void setDirVersAgg(AfDecimal dirVersAgg);

    /**
     * Nullable property for POL-DIR-VERS-AGG
     * 
     */
    AfDecimal getDirVersAggObj();

    void setDirVersAggObj(AfDecimal dirVersAggObj);

    /**
     * Host Variable POL-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for POL-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable POL-FL-FNT-AZ
     * 
     */
    char getFlFntAz();

    void setFlFntAz(char flFntAz);

    /**
     * Nullable property for POL-FL-FNT-AZ
     * 
     */
    Character getFlFntAzObj();

    void setFlFntAzObj(Character flFntAzObj);

    /**
     * Host Variable POL-FL-FNT-ADER
     * 
     */
    char getFlFntAder();

    void setFlFntAder(char flFntAder);

    /**
     * Nullable property for POL-FL-FNT-ADER
     * 
     */
    Character getFlFntAderObj();

    void setFlFntAderObj(Character flFntAderObj);

    /**
     * Host Variable POL-FL-FNT-TFR
     * 
     */
    char getFlFntTfr();

    void setFlFntTfr(char flFntTfr);

    /**
     * Nullable property for POL-FL-FNT-TFR
     * 
     */
    Character getFlFntTfrObj();

    void setFlFntTfrObj(Character flFntTfrObj);

    /**
     * Host Variable POL-FL-FNT-VOLO
     * 
     */
    char getFlFntVolo();

    void setFlFntVolo(char flFntVolo);

    /**
     * Nullable property for POL-FL-FNT-VOLO
     * 
     */
    Character getFlFntVoloObj();

    void setFlFntVoloObj(Character flFntVoloObj);

    /**
     * Host Variable POL-TP-OPZ-A-SCAD
     * 
     */
    String getTpOpzAScad();

    void setTpOpzAScad(String tpOpzAScad);

    /**
     * Nullable property for POL-TP-OPZ-A-SCAD
     * 
     */
    String getTpOpzAScadObj();

    void setTpOpzAScadObj(String tpOpzAScadObj);

    /**
     * Host Variable POL-AA-DIFF-PROR-DFLT
     * 
     */
    int getAaDiffProrDflt();

    void setAaDiffProrDflt(int aaDiffProrDflt);

    /**
     * Nullable property for POL-AA-DIFF-PROR-DFLT
     * 
     */
    Integer getAaDiffProrDfltObj();

    void setAaDiffProrDfltObj(Integer aaDiffProrDfltObj);

    /**
     * Host Variable POL-FL-VER-PROD
     * 
     */
    String getFlVerProd();

    void setFlVerProd(String flVerProd);

    /**
     * Nullable property for POL-FL-VER-PROD
     * 
     */
    String getFlVerProdObj();

    void setFlVerProdObj(String flVerProdObj);

    /**
     * Host Variable POL-DUR-GG
     * 
     */
    int getDurGg();

    void setDurGg(int durGg);

    /**
     * Nullable property for POL-DUR-GG
     * 
     */
    Integer getDurGgObj();

    void setDurGgObj(Integer durGgObj);

    /**
     * Host Variable POL-DIR-QUIET
     * 
     */
    AfDecimal getDirQuiet();

    void setDirQuiet(AfDecimal dirQuiet);

    /**
     * Nullable property for POL-DIR-QUIET
     * 
     */
    AfDecimal getDirQuietObj();

    void setDirQuietObj(AfDecimal dirQuietObj);

    /**
     * Host Variable POL-TP-PTF-ESTNO
     * 
     */
    String getTpPtfEstno();

    void setTpPtfEstno(String tpPtfEstno);

    /**
     * Nullable property for POL-TP-PTF-ESTNO
     * 
     */
    String getTpPtfEstnoObj();

    void setTpPtfEstnoObj(String tpPtfEstnoObj);

    /**
     * Host Variable POL-FL-CUM-PRE-CNTR
     * 
     */
    char getFlCumPreCntr();

    void setFlCumPreCntr(char flCumPreCntr);

    /**
     * Nullable property for POL-FL-CUM-PRE-CNTR
     * 
     */
    Character getFlCumPreCntrObj();

    void setFlCumPreCntrObj(Character flCumPreCntrObj);

    /**
     * Host Variable POL-FL-AMMB-MOVI
     * 
     */
    char getFlAmmbMovi();

    void setFlAmmbMovi(char flAmmbMovi);

    /**
     * Nullable property for POL-FL-AMMB-MOVI
     * 
     */
    Character getFlAmmbMoviObj();

    void setFlAmmbMoviObj(Character flAmmbMoviObj);

    /**
     * Host Variable POL-CONV-GECO
     * 
     */
    String getConvGeco();

    void setConvGeco(String convGeco);

    /**
     * Nullable property for POL-CONV-GECO
     * 
     */
    String getConvGecoObj();

    void setConvGecoObj(String convGecoObj);

    /**
     * Host Variable POL-DS-RIGA
     * 
     */
    long getDsRiga();

    void setDsRiga(long dsRiga);

    /**
     * Host Variable POL-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable POL-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable POL-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable POL-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable POL-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable POL-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable POL-FL-SCUDO-FISC
     * 
     */
    char getFlScudoFisc();

    void setFlScudoFisc(char flScudoFisc);

    /**
     * Nullable property for POL-FL-SCUDO-FISC
     * 
     */
    Character getFlScudoFiscObj();

    void setFlScudoFiscObj(Character flScudoFiscObj);

    /**
     * Host Variable POL-FL-TRASFE
     * 
     */
    char getFlTrasfe();

    void setFlTrasfe(char flTrasfe);

    /**
     * Nullable property for POL-FL-TRASFE
     * 
     */
    Character getFlTrasfeObj();

    void setFlTrasfeObj(Character flTrasfeObj);

    /**
     * Host Variable POL-FL-TFR-STRC
     * 
     */
    char getFlTfrStrc();

    void setFlTfrStrc(char flTfrStrc);

    /**
     * Nullable property for POL-FL-TFR-STRC
     * 
     */
    Character getFlTfrStrcObj();

    void setFlTfrStrcObj(Character flTfrStrcObj);

    /**
     * Host Variable POL-DT-PRESC-DB
     * 
     */
    String getDtPrescDb();

    void setDtPrescDb(String dtPrescDb);

    /**
     * Nullable property for POL-DT-PRESC-DB
     * 
     */
    String getDtPrescDbObj();

    void setDtPrescDbObj(String dtPrescDbObj);

    /**
     * Host Variable POL-COD-CONV-AGG
     * 
     */
    String getCodConvAgg();

    void setCodConvAgg(String codConvAgg);

    /**
     * Nullable property for POL-COD-CONV-AGG
     * 
     */
    String getCodConvAggObj();

    void setCodConvAggObj(String codConvAggObj);

    /**
     * Host Variable POL-SUBCAT-PROD
     * 
     */
    String getSubcatProd();

    void setSubcatProd(String subcatProd);

    /**
     * Nullable property for POL-SUBCAT-PROD
     * 
     */
    String getSubcatProdObj();

    void setSubcatProdObj(String subcatProdObj);

    /**
     * Host Variable POL-FL-QUEST-ADEGZ-ASS
     * 
     */
    char getFlQuestAdegzAss();

    void setFlQuestAdegzAss(char flQuestAdegzAss);

    /**
     * Nullable property for POL-FL-QUEST-ADEGZ-ASS
     * 
     */
    Character getFlQuestAdegzAssObj();

    void setFlQuestAdegzAssObj(Character flQuestAdegzAssObj);

    /**
     * Host Variable POL-COD-TPA
     * 
     */
    String getCodTpa();

    void setCodTpa(String codTpa);

    /**
     * Nullable property for POL-COD-TPA
     * 
     */
    String getCodTpaObj();

    void setCodTpaObj(String codTpaObj);

    /**
     * Host Variable POL-ID-ACC-COMM
     * 
     */
    int getIdAccComm();

    void setIdAccComm(int idAccComm);

    /**
     * Nullable property for POL-ID-ACC-COMM
     * 
     */
    Integer getIdAccCommObj();

    void setIdAccCommObj(Integer idAccCommObj);

    /**
     * Host Variable POL-FL-POLI-CPI-PR
     * 
     */
    char getFlPoliCpiPr();

    void setFlPoliCpiPr(char flPoliCpiPr);

    /**
     * Nullable property for POL-FL-POLI-CPI-PR
     * 
     */
    Character getFlPoliCpiPrObj();

    void setFlPoliCpiPrObj(Character flPoliCpiPrObj);

    /**
     * Host Variable POL-FL-POLI-BUNDLING
     * 
     */
    char getFlPoliBundling();

    void setFlPoliBundling(char flPoliBundling);

    /**
     * Nullable property for POL-FL-POLI-BUNDLING
     * 
     */
    Character getFlPoliBundlingObj();

    void setFlPoliBundlingObj(Character flPoliBundlingObj);

    /**
     * Host Variable POL-IND-POLI-PRIN-COLL
     * 
     */
    char getIndPoliPrinColl();

    void setIndPoliPrinColl(char indPoliPrinColl);

    /**
     * Nullable property for POL-IND-POLI-PRIN-COLL
     * 
     */
    Character getIndPoliPrinCollObj();

    void setIndPoliPrinCollObj(Character indPoliPrinCollObj);

    /**
     * Host Variable POL-FL-VND-BUNDLE
     * 
     */
    char getFlVndBundle();

    void setFlVndBundle(char flVndBundle);

    /**
     * Nullable property for POL-FL-VND-BUNDLE
     * 
     */
    Character getFlVndBundleObj();

    void setFlVndBundleObj(Character flVndBundleObj);

    /**
     * Host Variable POL-IB-BS
     * 
     */
    String getIbBs();

    void setIbBs(String ibBs);

    /**
     * Nullable property for POL-IB-BS
     * 
     */
    String getIbBsObj();

    void setIbBsObj(String ibBsObj);

    /**
     * Host Variable POL-FL-POLI-IFP
     * 
     */
    char getFlPoliIfp();

    void setFlPoliIfp(char flPoliIfp);

    /**
     * Nullable property for POL-FL-POLI-IFP
     * 
     */
    Character getFlPoliIfpObj();

    void setFlPoliIfpObj(Character flPoliIfpObj);
};
