package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BTC_BATCH]
 * 
 */
public interface IBtcBatch extends BaseSqlTo {

    /**
     * Host Variable BTC-COD-BATCH-TYPE
     * 
     */
    int getBtcCodBatchType();

    void setBtcCodBatchType(int btcCodBatchType);

    /**
     * Host Variable BTC-COD-COMP-ANIA
     * 
     */
    int getBtcCodCompAnia();

    void setBtcCodCompAnia(int btcCodCompAnia);

    /**
     * Host Variable IABV0002-STATE-01
     * 
     */
    char getIabv0002State01();

    void setIabv0002State01(char iabv0002State01);

    /**
     * Host Variable IABV0002-STATE-02
     * 
     */
    char getIabv0002State02();

    void setIabv0002State02(char iabv0002State02);

    /**
     * Host Variable IABV0002-STATE-03
     * 
     */
    char getIabv0002State03();

    void setIabv0002State03(char iabv0002State03);

    /**
     * Host Variable IABV0002-STATE-04
     * 
     */
    char getIabv0002State04();

    void setIabv0002State04(char iabv0002State04);

    /**
     * Host Variable IABV0002-STATE-05
     * 
     */
    char getIabv0002State05();

    void setIabv0002State05(char iabv0002State05);

    /**
     * Host Variable IABV0002-STATE-06
     * 
     */
    char getIabv0002State06();

    void setIabv0002State06(char iabv0002State06);

    /**
     * Host Variable IABV0002-STATE-07
     * 
     */
    char getIabv0002State07();

    void setIabv0002State07(char iabv0002State07);

    /**
     * Host Variable IABV0002-STATE-08
     * 
     */
    char getIabv0002State08();

    void setIabv0002State08(char iabv0002State08);

    /**
     * Host Variable IABV0002-STATE-09
     * 
     */
    char getIabv0002State09();

    void setIabv0002State09(char iabv0002State09);

    /**
     * Host Variable IABV0002-STATE-10
     * 
     */
    char getIabv0002State10();

    void setIabv0002State10(char iabv0002State10);

    /**
     * Host Variable BTC-ID-BATCH
     * 
     */
    int getIdBatch();

    void setIdBatch(int idBatch);

    /**
     * Host Variable BTC-PROTOCOL
     * 
     */
    String getBtcProtocol();

    void setBtcProtocol(String btcProtocol);

    /**
     * Host Variable BTC-DT-INS-DB
     * 
     */
    String getDtInsDb();

    void setDtInsDb(String dtInsDb);

    /**
     * Host Variable BTC-USER-INS
     * 
     */
    String getUserIns();

    void setUserIns(String userIns);

    /**
     * Host Variable BTC-COD-BATCH-STATE
     * 
     */
    char getCodBatchState();

    void setCodBatchState(char codBatchState);

    /**
     * Host Variable BTC-DT-START-DB
     * 
     */
    String getDtStartDb();

    void setDtStartDb(String dtStartDb);

    /**
     * Nullable property for BTC-DT-START-DB
     * 
     */
    String getDtStartDbObj();

    void setDtStartDbObj(String dtStartDbObj);

    /**
     * Host Variable BTC-DT-END-DB
     * 
     */
    String getDtEndDb();

    void setDtEndDb(String dtEndDb);

    /**
     * Nullable property for BTC-DT-END-DB
     * 
     */
    String getDtEndDbObj();

    void setDtEndDbObj(String dtEndDbObj);

    /**
     * Host Variable BTC-USER-START
     * 
     */
    String getUserStart();

    void setUserStart(String userStart);

    /**
     * Nullable property for BTC-USER-START
     * 
     */
    String getUserStartObj();

    void setUserStartObj(String userStartObj);

    /**
     * Host Variable BTC-ID-RICH
     * 
     */
    int getIdRich();

    void setIdRich(int idRich);

    /**
     * Nullable property for BTC-ID-RICH
     * 
     */
    Integer getIdRichObj();

    void setIdRichObj(Integer idRichObj);

    /**
     * Host Variable BTC-DT-EFF-DB
     * 
     */
    String getDtEffDb();

    void setDtEffDb(String dtEffDb);

    /**
     * Nullable property for BTC-DT-EFF-DB
     * 
     */
    String getDtEffDbObj();

    void setDtEffDbObj(String dtEffDbObj);

    /**
     * Host Variable BTC-DATA-BATCH-VCHAR
     * 
     */
    String getDataBatchVchar();

    void setDataBatchVchar(String dataBatchVchar);

    /**
     * Nullable property for BTC-DATA-BATCH-VCHAR
     * 
     */
    String getDataBatchVcharObj();

    void setDataBatchVcharObj(String dataBatchVcharObj);

    /**
     * Host Variable IABV0002-STATE-CURRENT
     * 
     */
    char getIabv0002StateCurrent();

    void setIabv0002StateCurrent(char iabv0002StateCurrent);
};
