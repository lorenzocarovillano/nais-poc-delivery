package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [IMPST_BOLLO]
 * 
 */
public interface IImpstBollo extends BaseSqlTo {

    /**
     * Host Variable P58-ID-IMPST-BOLLO
     * 
     */
    int getIdImpstBollo();

    void setIdImpstBollo(int idImpstBollo);

    /**
     * Host Variable P58-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P58-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable P58-IB-POLI
     * 
     */
    String getIbPoli();

    void setIbPoli(String ibPoli);

    /**
     * Host Variable P58-COD-FISC
     * 
     */
    String getCodFisc();

    void setCodFisc(String codFisc);

    /**
     * Nullable property for P58-COD-FISC
     * 
     */
    String getCodFiscObj();

    void setCodFiscObj(String codFiscObj);

    /**
     * Host Variable P58-COD-PART-IVA
     * 
     */
    String getCodPartIva();

    void setCodPartIva(String codPartIva);

    /**
     * Nullable property for P58-COD-PART-IVA
     * 
     */
    String getCodPartIvaObj();

    void setCodPartIvaObj(String codPartIvaObj);

    /**
     * Host Variable P58-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Host Variable P58-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable P58-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for P58-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable P58-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable P58-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable P58-DT-INI-CALC-DB
     * 
     */
    String getDtIniCalcDb();

    void setDtIniCalcDb(String dtIniCalcDb);

    /**
     * Host Variable P58-DT-END-CALC-DB
     * 
     */
    String getDtEndCalcDb();

    void setDtEndCalcDb(String dtEndCalcDb);

    /**
     * Host Variable P58-TP-CAUS-BOLLO
     * 
     */
    String getTpCausBollo();

    void setTpCausBollo(String tpCausBollo);

    /**
     * Host Variable P58-IMPST-BOLLO-DETT-C
     * 
     */
    AfDecimal getImpstBolloDettC();

    void setImpstBolloDettC(AfDecimal impstBolloDettC);

    /**
     * Host Variable P58-IMPST-BOLLO-DETT-V
     * 
     */
    AfDecimal getImpstBolloDettV();

    void setImpstBolloDettV(AfDecimal impstBolloDettV);

    /**
     * Nullable property for P58-IMPST-BOLLO-DETT-V
     * 
     */
    AfDecimal getImpstBolloDettVObj();

    void setImpstBolloDettVObj(AfDecimal impstBolloDettVObj);

    /**
     * Host Variable P58-IMPST-BOLLO-TOT-V
     * 
     */
    AfDecimal getImpstBolloTotV();

    void setImpstBolloTotV(AfDecimal impstBolloTotV);

    /**
     * Nullable property for P58-IMPST-BOLLO-TOT-V
     * 
     */
    AfDecimal getImpstBolloTotVObj();

    void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj);

    /**
     * Host Variable P58-IMPST-BOLLO-TOT-R
     * 
     */
    AfDecimal getImpstBolloTotR();

    void setImpstBolloTotR(AfDecimal impstBolloTotR);

    /**
     * Host Variable P58-DS-RIGA
     * 
     */
    long getP58DsRiga();

    void setP58DsRiga(long p58DsRiga);

    /**
     * Host Variable P58-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P58-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P58-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable P58-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable P58-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P58-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable LDBVE391-IMP-BOLLO-DETT-C
     * 
     */
    AfDecimal getDettC();

    void setDettC(AfDecimal dettC);

    /**
     * Host Variable LDBVE391-IMP-BOLLO-DETT-V
     * 
     */
    AfDecimal getDettV();

    void setDettV(AfDecimal dettV);

    /**
     * Nullable property for LDBVE391-IMP-BOLLO-DETT-V
     * 
     */
    AfDecimal getDettVObj();

    void setDettVObj(AfDecimal dettVObj);

    /**
     * Host Variable LDBVE391-IMP-BOLLO-TOT-V
     * 
     */
    AfDecimal getTotV();

    void setTotV(AfDecimal totV);

    /**
     * Nullable property for LDBVE391-IMP-BOLLO-TOT-V
     * 
     */
    AfDecimal getTotVObj();

    void setTotVObj(AfDecimal totVObj);

    /**
     * Host Variable LDBVE391-IMP-BOLLO-TOT-R
     * 
     */
    AfDecimal getTotR();

    void setTotR(AfDecimal totR);

    /**
     * Host Variable LDBVE391-COD-FISC
     * 
     */
    String getLdbve391CodFisc();

    void setLdbve391CodFisc(String ldbve391CodFisc);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-1
     * 
     */
    String getLdbve391TpCausBollo1();

    void setLdbve391TpCausBollo1(String ldbve391TpCausBollo1);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-2
     * 
     */
    String getLdbve391TpCausBollo2();

    void setLdbve391TpCausBollo2(String ldbve391TpCausBollo2);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-3
     * 
     */
    String getLdbve391TpCausBollo3();

    void setLdbve391TpCausBollo3(String ldbve391TpCausBollo3);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-4
     * 
     */
    String getLdbve391TpCausBollo4();

    void setLdbve391TpCausBollo4(String ldbve391TpCausBollo4);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-5
     * 
     */
    String getLdbve391TpCausBollo5();

    void setLdbve391TpCausBollo5(String ldbve391TpCausBollo5);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-6
     * 
     */
    String getLdbve391TpCausBollo6();

    void setLdbve391TpCausBollo6(String ldbve391TpCausBollo6);

    /**
     * Host Variable LDBVE391-TP-CAUS-BOLLO-7
     * 
     */
    String getLdbve391TpCausBollo7();

    void setLdbve391TpCausBollo7(String ldbve391TpCausBollo7);

    /**
     * Host Variable LDBVE391-DT-INI-CALC-DB
     * 
     */
    String getLdbve391DtIniCalcDb();

    void setLdbve391DtIniCalcDb(String ldbve391DtIniCalcDb);

    /**
     * Host Variable LDBVE391-DT-END-CALC-DB
     * 
     */
    String getLdbve391DtEndCalcDb();

    void setLdbve391DtEndCalcDb(String ldbve391DtEndCalcDb);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBVE421-COD-PART-IVA
     * 
     */
    String getLdbve421CodPartIva();

    void setLdbve421CodPartIva(String ldbve421CodPartIva);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-1
     * 
     */
    String getLdbve421TpCausBollo1();

    void setLdbve421TpCausBollo1(String ldbve421TpCausBollo1);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-2
     * 
     */
    String getLdbve421TpCausBollo2();

    void setLdbve421TpCausBollo2(String ldbve421TpCausBollo2);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-3
     * 
     */
    String getLdbve421TpCausBollo3();

    void setLdbve421TpCausBollo3(String ldbve421TpCausBollo3);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-4
     * 
     */
    String getLdbve421TpCausBollo4();

    void setLdbve421TpCausBollo4(String ldbve421TpCausBollo4);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-5
     * 
     */
    String getLdbve421TpCausBollo5();

    void setLdbve421TpCausBollo5(String ldbve421TpCausBollo5);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-6
     * 
     */
    String getLdbve421TpCausBollo6();

    void setLdbve421TpCausBollo6(String ldbve421TpCausBollo6);

    /**
     * Host Variable LDBVE421-TP-CAUS-BOLLO-7
     * 
     */
    String getLdbve421TpCausBollo7();

    void setLdbve421TpCausBollo7(String ldbve421TpCausBollo7);

    /**
     * Host Variable LDBVE421-DT-INI-CALC-DB
     * 
     */
    String getLdbve421DtIniCalcDb();

    void setLdbve421DtIniCalcDb(String ldbve421DtIniCalcDb);

    /**
     * Host Variable LDBVE421-DT-END-CALC-DB
     * 
     */
    String getLdbve421DtEndCalcDb();

    void setLdbve421DtEndCalcDb(String ldbve421DtEndCalcDb);
};
