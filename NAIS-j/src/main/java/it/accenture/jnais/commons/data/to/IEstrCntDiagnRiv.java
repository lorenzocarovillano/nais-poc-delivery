package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ESTR_CNT_DIAGN_RIV]
 * 
 */
public interface IEstrCntDiagnRiv extends BaseSqlTo {

    /**
     * Host Variable LDBVF321-ID-POLI
     * 
     */
    int getLdbvf321IdPoli();

    void setLdbvf321IdPoli(int ldbvf321IdPoli);

    /**
     * Host Variable LDBVF321-DT-RIV-DA-DB
     * 
     */
    String getLdbvf321DtRivDaDb();

    void setLdbvf321DtRivDaDb(String ldbvf321DtRivDaDb);

    /**
     * Host Variable LDBVF321-DT-RIV-A-DB
     * 
     */
    String getLdbvf321DtRivADb();

    void setLdbvf321DtRivADb(String ldbvf321DtRivADb);

    /**
     * Host Variable LDBVF321-COD-TARI
     * 
     */
    String getLdbvf321CodTari();

    void setLdbvf321CodTari(String ldbvf321CodTari);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable P85-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P85-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable P85-DT-RIVAL-DB
     * 
     */
    String getDtRivalDb();

    void setDtRivalDb(String dtRivalDb);

    /**
     * Host Variable P85-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Host Variable P85-RENDTO-LRD
     * 
     */
    AfDecimal getRendtoLrd();

    void setRendtoLrd(AfDecimal rendtoLrd);

    /**
     * Nullable property for P85-RENDTO-LRD
     * 
     */
    AfDecimal getRendtoLrdObj();

    void setRendtoLrdObj(AfDecimal rendtoLrdObj);

    /**
     * Host Variable P85-PC-RETR
     * 
     */
    AfDecimal getPcRetr();

    void setPcRetr(AfDecimal pcRetr);

    /**
     * Nullable property for P85-PC-RETR
     * 
     */
    AfDecimal getPcRetrObj();

    void setPcRetrObj(AfDecimal pcRetrObj);

    /**
     * Host Variable P85-RENDTO-RETR
     * 
     */
    AfDecimal getRendtoRetr();

    void setRendtoRetr(AfDecimal rendtoRetr);

    /**
     * Nullable property for P85-RENDTO-RETR
     * 
     */
    AfDecimal getRendtoRetrObj();

    void setRendtoRetrObj(AfDecimal rendtoRetrObj);

    /**
     * Host Variable P85-COMMIS-GEST
     * 
     */
    AfDecimal getCommisGest();

    void setCommisGest(AfDecimal commisGest);

    /**
     * Nullable property for P85-COMMIS-GEST
     * 
     */
    AfDecimal getCommisGestObj();

    void setCommisGestObj(AfDecimal commisGestObj);

    /**
     * Host Variable P85-TS-RIVAL-NET
     * 
     */
    AfDecimal getTsRivalNet();

    void setTsRivalNet(AfDecimal tsRivalNet);

    /**
     * Nullable property for P85-TS-RIVAL-NET
     * 
     */
    AfDecimal getTsRivalNetObj();

    void setTsRivalNetObj(AfDecimal tsRivalNetObj);

    /**
     * Host Variable P85-MIN-GARTO
     * 
     */
    AfDecimal getMinGarto();

    void setMinGarto(AfDecimal minGarto);

    /**
     * Nullable property for P85-MIN-GARTO
     * 
     */
    AfDecimal getMinGartoObj();

    void setMinGartoObj(AfDecimal minGartoObj);

    /**
     * Host Variable P85-MIN-TRNUT
     * 
     */
    AfDecimal getMinTrnut();

    void setMinTrnut(AfDecimal minTrnut);

    /**
     * Nullable property for P85-MIN-TRNUT
     * 
     */
    AfDecimal getMinTrnutObj();

    void setMinTrnutObj(AfDecimal minTrnutObj);

    /**
     * Host Variable P85-IB-POLI
     * 
     */
    String getIbPoli();

    void setIbPoli(String ibPoli);

    /**
     * Host Variable P85-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P85-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P85-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable P85-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P85-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
