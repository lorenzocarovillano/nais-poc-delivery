package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IMoviStatOggWf;

/**
 * Data Access Object(DAO) for tables [MOVI, STAT_OGG_WF]
 * 
 */
public class MoviStatOggWfDao extends BaseSqlDao<IMoviStatOggWf> {

    private Cursor cEff51;
    private Cursor cEff54;
    private final IRowMapper<IMoviStatOggWf> selectRecRm = buildNamedRowMapper(IMoviStatOggWf.class, "idMovi", "codCompAnia", "movIdOggObj", "ibOggObj", "ibMoviObj", "movTpOggObj", "idRichObj", "tpMoviObj", "movDtEffDb", "idMoviAnnObj", "idMoviCollgObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public MoviStatOggWfDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IMoviStatOggWf> getToClass() {
        return IMoviStatOggWf.class;
    }

    public IMoviStatOggWf selectRec(IMoviStatOggWf iMoviStatOggWf) {
        return buildQuery("selectRec").bind(iMoviStatOggWf).rowMapper(selectRecRm).singleResult(iMoviStatOggWf);
    }

    public DbAccessStatus openCEff51(IMoviStatOggWf iMoviStatOggWf) {
        cEff51 = buildQuery("openCEff51").bind(iMoviStatOggWf).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff51() {
        return closeCursor(cEff51);
    }

    public IMoviStatOggWf fetchCEff51(IMoviStatOggWf iMoviStatOggWf) {
        return fetch(cEff51, iMoviStatOggWf, selectRecRm);
    }

    public IMoviStatOggWf selectRec1(IMoviStatOggWf iMoviStatOggWf) {
        return buildQuery("selectRec1").bind(iMoviStatOggWf).rowMapper(selectRecRm).singleResult(iMoviStatOggWf);
    }

    public DbAccessStatus openCEff54(IMoviStatOggWf iMoviStatOggWf) {
        cEff54 = buildQuery("openCEff54").bind(iMoviStatOggWf).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff54() {
        return closeCursor(cEff54);
    }

    public IMoviStatOggWf fetchCEff54(IMoviStatOggWf iMoviStatOggWf) {
        return fetch(cEff54, iMoviStatOggWf, selectRecRm);
    }
}
