package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BTC_BATCH_TYPE]
 * 
 */
public interface IBtcBatchType extends BaseSqlTo {

    /**
     * Host Variable BBT-COD-BATCH-TYPE
     * 
     */
    int getBbtCodBatchType();

    void setBbtCodBatchType(int bbtCodBatchType);

    /**
     * Host Variable BBT-DES-VCHAR
     * 
     */
    String getDesVchar();

    void setDesVchar(String desVchar);

    /**
     * Host Variable BBT-DEF-CONTENT-TYPE
     * 
     */
    String getDefContentType();

    void setDefContentType(String defContentType);

    /**
     * Nullable property for BBT-DEF-CONTENT-TYPE
     * 
     */
    String getDefContentTypeObj();

    void setDefContentTypeObj(String defContentTypeObj);

    /**
     * Host Variable BBT-COMMIT-FREQUENCY
     * 
     */
    int getCommitFrequency();

    void setCommitFrequency(int commitFrequency);

    /**
     * Nullable property for BBT-COMMIT-FREQUENCY
     * 
     */
    Integer getCommitFrequencyObj();

    void setCommitFrequencyObj(Integer commitFrequencyObj);

    /**
     * Host Variable BBT-SERVICE-NAME
     * 
     */
    String getServiceName();

    void setServiceName(String serviceName);

    /**
     * Nullable property for BBT-SERVICE-NAME
     * 
     */
    String getServiceNameObj();

    void setServiceNameObj(String serviceNameObj);

    /**
     * Host Variable BBT-SERVICE-GUIDE-NAME
     * 
     */
    String getServiceGuideName();

    void setServiceGuideName(String serviceGuideName);

    /**
     * Nullable property for BBT-SERVICE-GUIDE-NAME
     * 
     */
    String getServiceGuideNameObj();

    void setServiceGuideNameObj(String serviceGuideNameObj);

    /**
     * Host Variable BBT-SERVICE-ALPO-NAME
     * 
     */
    String getServiceAlpoName();

    void setServiceAlpoName(String serviceAlpoName);

    /**
     * Nullable property for BBT-SERVICE-ALPO-NAME
     * 
     */
    String getServiceAlpoNameObj();

    void setServiceAlpoNameObj(String serviceAlpoNameObj);

    /**
     * Host Variable BBT-COD-MACROFUNCT
     * 
     */
    String getCodMacrofunct();

    void setCodMacrofunct(String codMacrofunct);

    /**
     * Nullable property for BBT-COD-MACROFUNCT
     * 
     */
    String getCodMacrofunctObj();

    void setCodMacrofunctObj(String codMacrofunctObj);

    /**
     * Host Variable BBT-TP-MOVI
     * 
     */
    int getTpMovi();

    void setTpMovi(int tpMovi);

    /**
     * Nullable property for BBT-TP-MOVI
     * 
     */
    Integer getTpMoviObj();

    void setTpMoviObj(Integer tpMoviObj);
};
