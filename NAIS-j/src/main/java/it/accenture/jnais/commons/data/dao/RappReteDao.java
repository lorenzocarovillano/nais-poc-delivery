package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IRappRete;

/**
 * Data Access Object(DAO) for table [RAPP_RETE]
 * 
 */
public class RappReteDao extends BaseSqlDao<IRappRete> {

    private Cursor cIdUpdEffRre;
    private Cursor cIdoEffRre;
    private Cursor cIdoCpzRre;
    private Cursor cEff23;
    private Cursor cCpz23;
    private final IRowMapper<IRappRete> selectByRreDsRigaRm = buildNamedRowMapper(IRappRete.class, "idRappRete", "rreIdOggObj", "rreTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "rreTpRete", "tpAcqsCntrtObj", "codAcqsCntrtObj", "codPntReteIniObj", "codPntReteEndObj", "flPntRete1rioObj", "codCanObj", "rreDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "codPntReteIniCObj", "matrOprtObj");

    public RappReteDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IRappRete> getToClass() {
        return IRappRete.class;
    }

    public IRappRete selectByRreDsRiga(long rreDsRiga, IRappRete iRappRete) {
        return buildQuery("selectByRreDsRiga").bind("rreDsRiga", rreDsRiga).rowMapper(selectByRreDsRigaRm).singleResult(iRappRete);
    }

    public DbAccessStatus insertRec(IRappRete iRappRete) {
        return buildQuery("insertRec").bind(iRappRete).executeInsert();
    }

    public DbAccessStatus updateRec(IRappRete iRappRete) {
        return buildQuery("updateRec").bind(iRappRete).executeUpdate();
    }

    public DbAccessStatus deleteByRreDsRiga(long rreDsRiga) {
        return buildQuery("deleteByRreDsRiga").bind("rreDsRiga", rreDsRiga).executeDelete();
    }

    public IRappRete selectRec(int rreIdRappRete, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRappRete iRappRete) {
        return buildQuery("selectRec").bind("rreIdRappRete", rreIdRappRete).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRreDsRigaRm).singleResult(iRappRete);
    }

    public DbAccessStatus updateRec1(IRappRete iRappRete) {
        return buildQuery("updateRec1").bind(iRappRete).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffRre(int rreIdRappRete, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffRre = buildQuery("openCIdUpdEffRre").bind("rreIdRappRete", rreIdRappRete).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffRre() {
        return closeCursor(cIdUpdEffRre);
    }

    public IRappRete fetchCIdUpdEffRre(IRappRete iRappRete) {
        return fetch(cIdUpdEffRre, iRappRete, selectByRreDsRigaRm);
    }

    public IRappRete selectRec1(int rreIdOgg, String rreTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRappRete iRappRete) {
        return buildQuery("selectRec1").bind("rreIdOgg", rreIdOgg).bind("rreTpOgg", rreTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRreDsRigaRm).singleResult(iRappRete);
    }

    public DbAccessStatus openCIdoEffRre(int rreIdOgg, String rreTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffRre = buildQuery("openCIdoEffRre").bind("rreIdOgg", rreIdOgg).bind("rreTpOgg", rreTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffRre() {
        return closeCursor(cIdoEffRre);
    }

    public IRappRete fetchCIdoEffRre(IRappRete iRappRete) {
        return fetch(cIdoEffRre, iRappRete, selectByRreDsRigaRm);
    }

    public IRappRete selectRec2(int rreIdRappRete, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IRappRete iRappRete) {
        return buildQuery("selectRec2").bind("rreIdRappRete", rreIdRappRete).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByRreDsRigaRm).singleResult(iRappRete);
    }

    public IRappRete selectRec3(IRappRete iRappRete) {
        return buildQuery("selectRec3").bind(iRappRete).rowMapper(selectByRreDsRigaRm).singleResult(iRappRete);
    }

    public DbAccessStatus openCIdoCpzRre(IRappRete iRappRete) {
        cIdoCpzRre = buildQuery("openCIdoCpzRre").bind(iRappRete).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzRre() {
        return closeCursor(cIdoCpzRre);
    }

    public IRappRete fetchCIdoCpzRre(IRappRete iRappRete) {
        return fetch(cIdoCpzRre, iRappRete, selectByRreDsRigaRm);
    }

    public IRappRete selectRec4(IRappRete iRappRete) {
        return buildQuery("selectRec4").bind(iRappRete).rowMapper(selectByRreDsRigaRm).singleResult(iRappRete);
    }

    public DbAccessStatus openCEff23(IRappRete iRappRete) {
        cEff23 = buildQuery("openCEff23").bind(iRappRete).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff23() {
        return closeCursor(cEff23);
    }

    public IRappRete fetchCEff23(IRappRete iRappRete) {
        return fetch(cEff23, iRappRete, selectByRreDsRigaRm);
    }

    public IRappRete selectRec5(IRappRete iRappRete) {
        return buildQuery("selectRec5").bind(iRappRete).rowMapper(selectByRreDsRigaRm).singleResult(iRappRete);
    }

    public DbAccessStatus openCCpz23(IRappRete iRappRete) {
        cCpz23 = buildQuery("openCCpz23").bind(iRappRete).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz23() {
        return closeCursor(cCpz23);
    }

    public IRappRete fetchCCpz23(IRappRete iRappRete) {
        return fetch(cCpz23, iRappRete, selectByRreDsRigaRm);
    }
}
