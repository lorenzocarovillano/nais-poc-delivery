package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IMotLiq;

/**
 * Data Access Object(DAO) for table [MOT_LIQ]
 * 
 */
public class MotLiqDao extends BaseSqlDao<IMotLiq> {

    private Cursor cIdUpdEffP86;
    private Cursor cIdpEffP86;
    private Cursor cIdpCpzP86;
    private final IRowMapper<IMotLiq> selectByP86DsRigaRm = buildNamedRowMapper(IMotLiq.class, "idMotLiq", "idLiq", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpLiq", "tpMotLiq", "p86DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "descLibMotLiqVcharObj");

    public MotLiqDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IMotLiq> getToClass() {
        return IMotLiq.class;
    }

    public IMotLiq selectByP86DsRiga(long p86DsRiga, IMotLiq iMotLiq) {
        return buildQuery("selectByP86DsRiga").bind("p86DsRiga", p86DsRiga).rowMapper(selectByP86DsRigaRm).singleResult(iMotLiq);
    }

    public DbAccessStatus insertRec(IMotLiq iMotLiq) {
        return buildQuery("insertRec").bind(iMotLiq).executeInsert();
    }

    public DbAccessStatus updateRec(IMotLiq iMotLiq) {
        return buildQuery("updateRec").bind(iMotLiq).executeUpdate();
    }

    public DbAccessStatus deleteByP86DsRiga(long p86DsRiga) {
        return buildQuery("deleteByP86DsRiga").bind("p86DsRiga", p86DsRiga).executeDelete();
    }

    public IMotLiq selectRec(int p86IdMotLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IMotLiq iMotLiq) {
        return buildQuery("selectRec").bind("p86IdMotLiq", p86IdMotLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP86DsRigaRm).singleResult(iMotLiq);
    }

    public DbAccessStatus updateRec1(IMotLiq iMotLiq) {
        return buildQuery("updateRec1").bind(iMotLiq).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffP86(int p86IdMotLiq, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffP86 = buildQuery("openCIdUpdEffP86").bind("p86IdMotLiq", p86IdMotLiq).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffP86() {
        return closeCursor(cIdUpdEffP86);
    }

    public IMotLiq fetchCIdUpdEffP86(IMotLiq iMotLiq) {
        return fetch(cIdUpdEffP86, iMotLiq, selectByP86DsRigaRm);
    }

    public IMotLiq selectRec1(int p86IdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IMotLiq iMotLiq) {
        return buildQuery("selectRec1").bind("p86IdLiq", p86IdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP86DsRigaRm).singleResult(iMotLiq);
    }

    public DbAccessStatus openCIdpEffP86(int p86IdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffP86 = buildQuery("openCIdpEffP86").bind("p86IdLiq", p86IdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffP86() {
        return closeCursor(cIdpEffP86);
    }

    public IMotLiq fetchCIdpEffP86(IMotLiq iMotLiq) {
        return fetch(cIdpEffP86, iMotLiq, selectByP86DsRigaRm);
    }

    public IMotLiq selectRec2(int p86IdMotLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IMotLiq iMotLiq) {
        return buildQuery("selectRec2").bind("p86IdMotLiq", p86IdMotLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP86DsRigaRm).singleResult(iMotLiq);
    }

    public IMotLiq selectRec3(int p86IdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IMotLiq iMotLiq) {
        return buildQuery("selectRec3").bind("p86IdLiq", p86IdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP86DsRigaRm).singleResult(iMotLiq);
    }

    public DbAccessStatus openCIdpCpzP86(int p86IdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzP86 = buildQuery("openCIdpCpzP86").bind("p86IdLiq", p86IdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzP86() {
        return closeCursor(cIdpCpzP86);
    }

    public IMotLiq fetchCIdpCpzP86(IMotLiq iMotLiq) {
        return fetch(cIdpCpzP86, iMotLiq, selectByP86DsRigaRm);
    }
}
