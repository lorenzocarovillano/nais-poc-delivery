package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IOggInCoass;

/**
 * Data Access Object(DAO) for table [OGG_IN_COASS]
 * 
 */
public class OggInCoassDao extends BaseSqlDao<IOggInCoass> {

    private Cursor cIdUpdEffOcs;
    private Cursor cIdpEffOcs;
    private Cursor cIdpCpzOcs;
    private final IRowMapper<IOggInCoass> selectByOcsDsRigaRm = buildNamedRowMapper(IOggInCoass.class, "idOggInCoass", "idPoliObj", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codCoassObj", "tpDlg", "codCompAniaAcqsObj", "ocsDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public OggInCoassDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IOggInCoass> getToClass() {
        return IOggInCoass.class;
    }

    public IOggInCoass selectByOcsDsRiga(long ocsDsRiga, IOggInCoass iOggInCoass) {
        return buildQuery("selectByOcsDsRiga").bind("ocsDsRiga", ocsDsRiga).rowMapper(selectByOcsDsRigaRm).singleResult(iOggInCoass);
    }

    public DbAccessStatus insertRec(IOggInCoass iOggInCoass) {
        return buildQuery("insertRec").bind(iOggInCoass).executeInsert();
    }

    public DbAccessStatus updateRec(IOggInCoass iOggInCoass) {
        return buildQuery("updateRec").bind(iOggInCoass).executeUpdate();
    }

    public DbAccessStatus deleteByOcsDsRiga(long ocsDsRiga) {
        return buildQuery("deleteByOcsDsRiga").bind("ocsDsRiga", ocsDsRiga).executeDelete();
    }

    public IOggInCoass selectRec(int ocsIdOggInCoass, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggInCoass iOggInCoass) {
        return buildQuery("selectRec").bind("ocsIdOggInCoass", ocsIdOggInCoass).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByOcsDsRigaRm).singleResult(iOggInCoass);
    }

    public DbAccessStatus updateRec1(IOggInCoass iOggInCoass) {
        return buildQuery("updateRec1").bind(iOggInCoass).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffOcs(int ocsIdOggInCoass, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffOcs = buildQuery("openCIdUpdEffOcs").bind("ocsIdOggInCoass", ocsIdOggInCoass).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffOcs() {
        return closeCursor(cIdUpdEffOcs);
    }

    public IOggInCoass fetchCIdUpdEffOcs(IOggInCoass iOggInCoass) {
        return fetch(cIdUpdEffOcs, iOggInCoass, selectByOcsDsRigaRm);
    }

    public IOggInCoass selectRec1(int ocsIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggInCoass iOggInCoass) {
        return buildQuery("selectRec1").bind("ocsIdPoli", ocsIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByOcsDsRigaRm).singleResult(iOggInCoass);
    }

    public DbAccessStatus openCIdpEffOcs(int ocsIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffOcs = buildQuery("openCIdpEffOcs").bind("ocsIdPoli", ocsIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffOcs() {
        return closeCursor(cIdpEffOcs);
    }

    public IOggInCoass fetchCIdpEffOcs(IOggInCoass iOggInCoass) {
        return fetch(cIdpEffOcs, iOggInCoass, selectByOcsDsRigaRm);
    }

    public IOggInCoass selectRec2(int ocsIdOggInCoass, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IOggInCoass iOggInCoass) {
        return buildQuery("selectRec2").bind("ocsIdOggInCoass", ocsIdOggInCoass).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByOcsDsRigaRm).singleResult(iOggInCoass);
    }

    public IOggInCoass selectRec3(int ocsIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IOggInCoass iOggInCoass) {
        return buildQuery("selectRec3").bind("ocsIdPoli", ocsIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByOcsDsRigaRm).singleResult(iOggInCoass);
    }

    public DbAccessStatus openCIdpCpzOcs(int ocsIdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzOcs = buildQuery("openCIdpCpzOcs").bind("ocsIdPoli", ocsIdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzOcs() {
        return closeCursor(cIdpCpzOcs);
    }

    public IOggInCoass fetchCIdpCpzOcs(IOggInCoass iOggInCoass) {
        return fetch(cIdpCpzOcs, iOggInCoass, selectByOcsDsRigaRm);
    }
}
