package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IPercLiq;

/**
 * Data Access Object(DAO) for table [PERC_LIQ]
 * 
 */
public class PercLiqDao extends BaseSqlDao<IPercLiq> {

    private Cursor cIdUpdEffPli;
    private Cursor cIdpEffPli;
    private Cursor cIdpCpzPli;
    private final IRowMapper<IPercLiq> selectByPliDsRigaRm = buildNamedRowMapper(IPercLiq.class, "idPercLiq", "idBnficrLiq", "idMoviCrz", "idMoviChiuObj", "idRappAnaObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "pcLiqObj", "impLiqObj", "tpMezPagObj", "iterPagAvvObj", "pliDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "dtVltDbObj", "intCntCorrAccrVcharObj", "codIbanRitConObj");

    public PercLiqDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IPercLiq> getToClass() {
        return IPercLiq.class;
    }

    public IPercLiq selectByPliDsRiga(long pliDsRiga, IPercLiq iPercLiq) {
        return buildQuery("selectByPliDsRiga").bind("pliDsRiga", pliDsRiga).rowMapper(selectByPliDsRigaRm).singleResult(iPercLiq);
    }

    public DbAccessStatus insertRec(IPercLiq iPercLiq) {
        return buildQuery("insertRec").bind(iPercLiq).executeInsert();
    }

    public DbAccessStatus updateRec(IPercLiq iPercLiq) {
        return buildQuery("updateRec").bind(iPercLiq).executeUpdate();
    }

    public DbAccessStatus deleteByPliDsRiga(long pliDsRiga) {
        return buildQuery("deleteByPliDsRiga").bind("pliDsRiga", pliDsRiga).executeDelete();
    }

    public IPercLiq selectRec(int pliIdPercLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPercLiq iPercLiq) {
        return buildQuery("selectRec").bind("pliIdPercLiq", pliIdPercLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPliDsRigaRm).singleResult(iPercLiq);
    }

    public DbAccessStatus updateRec1(IPercLiq iPercLiq) {
        return buildQuery("updateRec1").bind(iPercLiq).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffPli(int pliIdPercLiq, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffPli = buildQuery("openCIdUpdEffPli").bind("pliIdPercLiq", pliIdPercLiq).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffPli() {
        return closeCursor(cIdUpdEffPli);
    }

    public IPercLiq fetchCIdUpdEffPli(IPercLiq iPercLiq) {
        return fetch(cIdUpdEffPli, iPercLiq, selectByPliDsRigaRm);
    }

    public IPercLiq selectRec1(int pliIdBnficrLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IPercLiq iPercLiq) {
        return buildQuery("selectRec1").bind("pliIdBnficrLiq", pliIdBnficrLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPliDsRigaRm).singleResult(iPercLiq);
    }

    public DbAccessStatus openCIdpEffPli(int pliIdBnficrLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffPli = buildQuery("openCIdpEffPli").bind("pliIdBnficrLiq", pliIdBnficrLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffPli() {
        return closeCursor(cIdpEffPli);
    }

    public IPercLiq fetchCIdpEffPli(IPercLiq iPercLiq) {
        return fetch(cIdpEffPli, iPercLiq, selectByPliDsRigaRm);
    }

    public IPercLiq selectRec2(int pliIdPercLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IPercLiq iPercLiq) {
        return buildQuery("selectRec2").bind("pliIdPercLiq", pliIdPercLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPliDsRigaRm).singleResult(iPercLiq);
    }

    public IPercLiq selectRec3(int pliIdBnficrLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IPercLiq iPercLiq) {
        return buildQuery("selectRec3").bind("pliIdBnficrLiq", pliIdBnficrLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPliDsRigaRm).singleResult(iPercLiq);
    }

    public DbAccessStatus openCIdpCpzPli(int pliIdBnficrLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzPli = buildQuery("openCIdpCpzPli").bind("pliIdBnficrLiq", pliIdBnficrLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzPli() {
        return closeCursor(cIdpCpzPli);
    }

    public IPercLiq fetchCIdpCpzPli(IPercLiq iPercLiq) {
        return fetch(cIdpCpzPli, iPercLiq, selectByPliDsRigaRm);
    }
}
