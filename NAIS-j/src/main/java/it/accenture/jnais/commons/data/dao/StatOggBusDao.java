package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IStatOggBus;

/**
 * Data Access Object(DAO) for table [STAT_OGG_BUS]
 * 
 */
public class StatOggBusDao extends BaseSqlDao<IStatOggBus> {

    private Cursor cIdUpdEffStb;
    private Cursor cIdoEffStb;
    private Cursor cIdoCpzStb;
    private Cursor cEff14;
    private Cursor cCpz14;
    private Cursor cEff55;
    private final IRowMapper<IStatOggBus> selectByStbDsRigaRm = buildNamedRowMapper(IStatOggBus.class, "idStatOggBus", "stbIdOgg", "stbTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpStatBus", "tpCaus", "stbDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");
    private final IRowMapper<IStatOggBus> selectRec4Rm = buildNamedRowMapper(IStatOggBus.class, "caus", "stat");

    public StatOggBusDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IStatOggBus> getToClass() {
        return IStatOggBus.class;
    }

    public IStatOggBus selectByStbDsRiga(long stbDsRiga, IStatOggBus iStatOggBus) {
        return buildQuery("selectByStbDsRiga").bind("stbDsRiga", stbDsRiga).rowMapper(selectByStbDsRigaRm).singleResult(iStatOggBus);
    }

    public DbAccessStatus insertRec(IStatOggBus iStatOggBus) {
        return buildQuery("insertRec").bind(iStatOggBus).executeInsert();
    }

    public DbAccessStatus updateRec(IStatOggBus iStatOggBus) {
        return buildQuery("updateRec").bind(iStatOggBus).executeUpdate();
    }

    public DbAccessStatus deleteByStbDsRiga(long stbDsRiga) {
        return buildQuery("deleteByStbDsRiga").bind("stbDsRiga", stbDsRiga).executeDelete();
    }

    public IStatOggBus selectRec(int stbIdStatOggBus, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IStatOggBus iStatOggBus) {
        return buildQuery("selectRec").bind("stbIdStatOggBus", stbIdStatOggBus).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByStbDsRigaRm).singleResult(iStatOggBus);
    }

    public DbAccessStatus updateRec1(IStatOggBus iStatOggBus) {
        return buildQuery("updateRec1").bind(iStatOggBus).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffStb(int stbIdStatOggBus, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffStb = buildQuery("openCIdUpdEffStb").bind("stbIdStatOggBus", stbIdStatOggBus).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffStb() {
        return closeCursor(cIdUpdEffStb);
    }

    public IStatOggBus fetchCIdUpdEffStb(IStatOggBus iStatOggBus) {
        return fetch(cIdUpdEffStb, iStatOggBus, selectByStbDsRigaRm);
    }

    public IStatOggBus selectRec1(int stbIdOgg, String stbTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IStatOggBus iStatOggBus) {
        return buildQuery("selectRec1").bind("stbIdOgg", stbIdOgg).bind("stbTpOgg", stbTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByStbDsRigaRm).singleResult(iStatOggBus);
    }

    public DbAccessStatus openCIdoEffStb(int stbIdOgg, String stbTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffStb = buildQuery("openCIdoEffStb").bind("stbIdOgg", stbIdOgg).bind("stbTpOgg", stbTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffStb() {
        return closeCursor(cIdoEffStb);
    }

    public IStatOggBus fetchCIdoEffStb(IStatOggBus iStatOggBus) {
        return fetch(cIdoEffStb, iStatOggBus, selectByStbDsRigaRm);
    }

    public IStatOggBus selectRec2(int stbIdStatOggBus, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IStatOggBus iStatOggBus) {
        return buildQuery("selectRec2").bind("stbIdStatOggBus", stbIdStatOggBus).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByStbDsRigaRm).singleResult(iStatOggBus);
    }

    public IStatOggBus selectRec3(IStatOggBus iStatOggBus) {
        return buildQuery("selectRec3").bind(iStatOggBus).rowMapper(selectByStbDsRigaRm).singleResult(iStatOggBus);
    }

    public DbAccessStatus openCIdoCpzStb(IStatOggBus iStatOggBus) {
        cIdoCpzStb = buildQuery("openCIdoCpzStb").bind(iStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzStb() {
        return closeCursor(cIdoCpzStb);
    }

    public IStatOggBus fetchCIdoCpzStb(IStatOggBus iStatOggBus) {
        return fetch(cIdoCpzStb, iStatOggBus, selectByStbDsRigaRm);
    }

    public IStatOggBus selectRec4(String ldbv1701TpOgg, int ldbv1701IdOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IStatOggBus iStatOggBus) {
        return buildQuery("selectRec4").bind("ldbv1701TpOgg", ldbv1701TpOgg).bind("ldbv1701IdOgg", ldbv1701IdOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectRec4Rm).singleResult(iStatOggBus);
    }

    public IStatOggBus selectRec5(IStatOggBus iStatOggBus) {
        return buildQuery("selectRec5").bind(iStatOggBus).rowMapper(selectRec4Rm).singleResult(iStatOggBus);
    }

    public IStatOggBus selectRec6(int stbIdOgg, String stbTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IStatOggBus iStatOggBus) {
        return buildQuery("selectRec6").bind("stbIdOgg", stbIdOgg).bind("stbTpOgg", stbTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByStbDsRigaRm).singleResult(iStatOggBus);
    }

    public DbAccessStatus openCEff14(int stbIdOgg, String stbTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff14 = buildQuery("openCEff14").bind("stbIdOgg", stbIdOgg).bind("stbTpOgg", stbTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff14() {
        return closeCursor(cEff14);
    }

    public IStatOggBus fetchCEff14(IStatOggBus iStatOggBus) {
        return fetch(cEff14, iStatOggBus, selectByStbDsRigaRm);
    }

    public IStatOggBus selectRec7(IStatOggBus iStatOggBus) {
        return buildQuery("selectRec7").bind(iStatOggBus).rowMapper(selectByStbDsRigaRm).singleResult(iStatOggBus);
    }

    public DbAccessStatus openCCpz14(IStatOggBus iStatOggBus) {
        cCpz14 = buildQuery("openCCpz14").bind(iStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz14() {
        return closeCursor(cCpz14);
    }

    public IStatOggBus fetchCCpz14(IStatOggBus iStatOggBus) {
        return fetch(cCpz14, iStatOggBus, selectByStbDsRigaRm);
    }

    public IStatOggBus selectRec8(IStatOggBus iStatOggBus) {
        return buildQuery("selectRec8").bind(iStatOggBus).rowMapper(selectByStbDsRigaRm).singleResult(iStatOggBus);
    }

    public DbAccessStatus openCEff55(IStatOggBus iStatOggBus) {
        cEff55 = buildQuery("openCEff55").bind(iStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff55() {
        return closeCursor(cEff55);
    }

    public IStatOggBus fetchCEff55(IStatOggBus iStatOggBus) {
        return fetch(cEff55, iStatOggBus, selectByStbDsRigaRm);
    }
}
