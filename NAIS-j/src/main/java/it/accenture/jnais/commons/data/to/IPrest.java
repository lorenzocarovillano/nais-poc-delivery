package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PREST]
 * 
 */
public interface IPrest extends BaseSqlTo {

    /**
     * Host Variable PRE-ID-OGG
     * 
     */
    int getPreIdOgg();

    void setPreIdOgg(int preIdOgg);

    /**
     * Host Variable PRE-TP-OGG
     * 
     */
    String getPreTpOgg();

    void setPreTpOgg(String preTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable PRE-ID-PREST
     * 
     */
    int getIdPrest();

    void setIdPrest(int idPrest);

    /**
     * Host Variable PRE-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable PRE-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for PRE-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable PRE-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable PRE-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable PRE-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable PRE-DT-CONCS-PREST-DB
     * 
     */
    String getDtConcsPrestDb();

    void setDtConcsPrestDb(String dtConcsPrestDb);

    /**
     * Nullable property for PRE-DT-CONCS-PREST-DB
     * 
     */
    String getDtConcsPrestDbObj();

    void setDtConcsPrestDbObj(String dtConcsPrestDbObj);

    /**
     * Host Variable PRE-DT-DECOR-PREST-DB
     * 
     */
    String getDtDecorPrestDb();

    void setDtDecorPrestDb(String dtDecorPrestDb);

    /**
     * Nullable property for PRE-DT-DECOR-PREST-DB
     * 
     */
    String getDtDecorPrestDbObj();

    void setDtDecorPrestDbObj(String dtDecorPrestDbObj);

    /**
     * Host Variable PRE-IMP-PREST
     * 
     */
    AfDecimal getImpPrest();

    void setImpPrest(AfDecimal impPrest);

    /**
     * Nullable property for PRE-IMP-PREST
     * 
     */
    AfDecimal getImpPrestObj();

    void setImpPrestObj(AfDecimal impPrestObj);

    /**
     * Host Variable PRE-INTR-PREST
     * 
     */
    AfDecimal getIntrPrest();

    void setIntrPrest(AfDecimal intrPrest);

    /**
     * Nullable property for PRE-INTR-PREST
     * 
     */
    AfDecimal getIntrPrestObj();

    void setIntrPrestObj(AfDecimal intrPrestObj);

    /**
     * Host Variable PRE-TP-PREST
     * 
     */
    String getTpPrest();

    void setTpPrest(String tpPrest);

    /**
     * Nullable property for PRE-TP-PREST
     * 
     */
    String getTpPrestObj();

    void setTpPrestObj(String tpPrestObj);

    /**
     * Host Variable PRE-FRAZ-PAG-INTR
     * 
     */
    int getFrazPagIntr();

    void setFrazPagIntr(int frazPagIntr);

    /**
     * Nullable property for PRE-FRAZ-PAG-INTR
     * 
     */
    Integer getFrazPagIntrObj();

    void setFrazPagIntrObj(Integer frazPagIntrObj);

    /**
     * Host Variable PRE-DT-RIMB-DB
     * 
     */
    String getDtRimbDb();

    void setDtRimbDb(String dtRimbDb);

    /**
     * Nullable property for PRE-DT-RIMB-DB
     * 
     */
    String getDtRimbDbObj();

    void setDtRimbDbObj(String dtRimbDbObj);

    /**
     * Host Variable PRE-IMP-RIMB
     * 
     */
    AfDecimal getImpRimb();

    void setImpRimb(AfDecimal impRimb);

    /**
     * Nullable property for PRE-IMP-RIMB
     * 
     */
    AfDecimal getImpRimbObj();

    void setImpRimbObj(AfDecimal impRimbObj);

    /**
     * Host Variable PRE-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for PRE-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable PRE-DT-RICH-PREST-DB
     * 
     */
    String getDtRichPrestDb();

    void setDtRichPrestDb(String dtRichPrestDb);

    /**
     * Host Variable PRE-MOD-INTR-PREST
     * 
     */
    String getModIntrPrest();

    void setModIntrPrest(String modIntrPrest);

    /**
     * Nullable property for PRE-MOD-INTR-PREST
     * 
     */
    String getModIntrPrestObj();

    void setModIntrPrestObj(String modIntrPrestObj);

    /**
     * Host Variable PRE-SPE-PREST
     * 
     */
    AfDecimal getSpePrest();

    void setSpePrest(AfDecimal spePrest);

    /**
     * Nullable property for PRE-SPE-PREST
     * 
     */
    AfDecimal getSpePrestObj();

    void setSpePrestObj(AfDecimal spePrestObj);

    /**
     * Host Variable PRE-IMP-PREST-LIQTO
     * 
     */
    AfDecimal getImpPrestLiqto();

    void setImpPrestLiqto(AfDecimal impPrestLiqto);

    /**
     * Nullable property for PRE-IMP-PREST-LIQTO
     * 
     */
    AfDecimal getImpPrestLiqtoObj();

    void setImpPrestLiqtoObj(AfDecimal impPrestLiqtoObj);

    /**
     * Host Variable PRE-SDO-INTR
     * 
     */
    AfDecimal getSdoIntr();

    void setSdoIntr(AfDecimal sdoIntr);

    /**
     * Nullable property for PRE-SDO-INTR
     * 
     */
    AfDecimal getSdoIntrObj();

    void setSdoIntrObj(AfDecimal sdoIntrObj);

    /**
     * Host Variable PRE-RIMB-EFF
     * 
     */
    AfDecimal getRimbEff();

    void setRimbEff(AfDecimal rimbEff);

    /**
     * Nullable property for PRE-RIMB-EFF
     * 
     */
    AfDecimal getRimbEffObj();

    void setRimbEffObj(AfDecimal rimbEffObj);

    /**
     * Host Variable PRE-PREST-RES-EFF
     * 
     */
    AfDecimal getPrestResEff();

    void setPrestResEff(AfDecimal prestResEff);

    /**
     * Nullable property for PRE-PREST-RES-EFF
     * 
     */
    AfDecimal getPrestResEffObj();

    void setPrestResEffObj(AfDecimal prestResEffObj);

    /**
     * Host Variable PRE-DS-RIGA
     * 
     */
    long getPreDsRiga();

    void setPreDsRiga(long preDsRiga);

    /**
     * Host Variable PRE-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable PRE-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable PRE-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable PRE-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable PRE-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable PRE-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable LDBV2821-IMP-PREST
     * 
     */
    AfDecimal getLdbv2821ImpPrest();

    void setLdbv2821ImpPrest(AfDecimal ldbv2821ImpPrest);

    /**
     * Nullable property for LDBV2821-IMP-PREST
     * 
     */
    AfDecimal getLdbv2821ImpPrestObj();

    void setLdbv2821ImpPrestObj(AfDecimal ldbv2821ImpPrestObj);

    /**
     * Host Variable LDBV2821-ID-OGG
     * 
     */
    int getLdbv2821IdOgg();

    void setLdbv2821IdOgg(int ldbv2821IdOgg);

    /**
     * Host Variable LDBV2821-TP-OGG
     * 
     */
    String getLdbv2821TpOgg();

    void setLdbv2821TpOgg(String ldbv2821TpOgg);
};
