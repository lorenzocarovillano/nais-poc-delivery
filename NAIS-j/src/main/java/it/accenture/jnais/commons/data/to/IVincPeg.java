package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [VINC_PEG]
 * 
 */
public interface IVincPeg extends BaseSqlTo {

    /**
     * Host Variable L23-ID-VINC-PEG
     * 
     */
    int getIdVincPeg();

    void setIdVincPeg(int idVincPeg);

    /**
     * Host Variable L23-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable L23-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for L23-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable L23-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable L23-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable L23-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L23-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Host Variable L23-TP-VINC
     * 
     */
    String getTpVinc();

    void setTpVinc(String tpVinc);

    /**
     * Nullable property for L23-TP-VINC
     * 
     */
    String getTpVincObj();

    void setTpVincObj(String tpVincObj);

    /**
     * Host Variable L23-FL-DELEGA-AL-RISC
     * 
     */
    char getFlDelegaAlRisc();

    void setFlDelegaAlRisc(char flDelegaAlRisc);

    /**
     * Nullable property for L23-FL-DELEGA-AL-RISC
     * 
     */
    Character getFlDelegaAlRiscObj();

    void setFlDelegaAlRiscObj(Character flDelegaAlRiscObj);

    /**
     * Host Variable L23-DESC-VCHAR
     * 
     */
    String getDescVchar();

    void setDescVchar(String descVchar);

    /**
     * Nullable property for L23-DESC-VCHAR
     * 
     */
    String getDescVcharObj();

    void setDescVcharObj(String descVcharObj);

    /**
     * Host Variable L23-DT-ATTIV-VINPG-DB
     * 
     */
    String getDtAttivVinpgDb();

    void setDtAttivVinpgDb(String dtAttivVinpgDb);

    /**
     * Nullable property for L23-DT-ATTIV-VINPG-DB
     * 
     */
    String getDtAttivVinpgDbObj();

    void setDtAttivVinpgDbObj(String dtAttivVinpgDbObj);

    /**
     * Host Variable L23-CPT-VINCTO-PIGN
     * 
     */
    AfDecimal getCptVinctoPign();

    void setCptVinctoPign(AfDecimal cptVinctoPign);

    /**
     * Nullable property for L23-CPT-VINCTO-PIGN
     * 
     */
    AfDecimal getCptVinctoPignObj();

    void setCptVinctoPignObj(AfDecimal cptVinctoPignObj);

    /**
     * Host Variable L23-DT-CHIU-VINPG-DB
     * 
     */
    String getDtChiuVinpgDb();

    void setDtChiuVinpgDb(String dtChiuVinpgDb);

    /**
     * Nullable property for L23-DT-CHIU-VINPG-DB
     * 
     */
    String getDtChiuVinpgDbObj();

    void setDtChiuVinpgDbObj(String dtChiuVinpgDbObj);

    /**
     * Host Variable L23-VAL-RISC-INI-VINPG
     * 
     */
    AfDecimal getValRiscIniVinpg();

    void setValRiscIniVinpg(AfDecimal valRiscIniVinpg);

    /**
     * Nullable property for L23-VAL-RISC-INI-VINPG
     * 
     */
    AfDecimal getValRiscIniVinpgObj();

    void setValRiscIniVinpgObj(AfDecimal valRiscIniVinpgObj);

    /**
     * Host Variable L23-VAL-RISC-END-VINPG
     * 
     */
    AfDecimal getValRiscEndVinpg();

    void setValRiscEndVinpg(AfDecimal valRiscEndVinpg);

    /**
     * Nullable property for L23-VAL-RISC-END-VINPG
     * 
     */
    AfDecimal getValRiscEndVinpgObj();

    void setValRiscEndVinpgObj(AfDecimal valRiscEndVinpgObj);

    /**
     * Host Variable L23-SOM-PRE-VINPG
     * 
     */
    AfDecimal getSomPreVinpg();

    void setSomPreVinpg(AfDecimal somPreVinpg);

    /**
     * Nullable property for L23-SOM-PRE-VINPG
     * 
     */
    AfDecimal getSomPreVinpgObj();

    void setSomPreVinpgObj(AfDecimal somPreVinpgObj);

    /**
     * Host Variable L23-FL-VINPG-INT-PRSTZ
     * 
     */
    char getFlVinpgIntPrstz();

    void setFlVinpgIntPrstz(char flVinpgIntPrstz);

    /**
     * Nullable property for L23-FL-VINPG-INT-PRSTZ
     * 
     */
    Character getFlVinpgIntPrstzObj();

    void setFlVinpgIntPrstzObj(Character flVinpgIntPrstzObj);

    /**
     * Host Variable L23-DESC-AGG-VINC-VCHAR
     * 
     */
    String getDescAggVincVchar();

    void setDescAggVincVchar(String descAggVincVchar);

    /**
     * Nullable property for L23-DESC-AGG-VINC-VCHAR
     * 
     */
    String getDescAggVincVcharObj();

    void setDescAggVincVcharObj(String descAggVincVcharObj);

    /**
     * Host Variable L23-DS-RIGA
     * 
     */
    long getL23DsRiga();

    void setL23DsRiga(long l23DsRiga);

    /**
     * Host Variable L23-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L23-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L23-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable L23-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable L23-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable L23-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable L23-TP-AUT-SEQ
     * 
     */
    String getTpAutSeq();

    void setTpAutSeq(String tpAutSeq);

    /**
     * Nullable property for L23-TP-AUT-SEQ
     * 
     */
    String getTpAutSeqObj();

    void setTpAutSeqObj(String tpAutSeqObj);

    /**
     * Host Variable L23-COD-UFF-SEQ
     * 
     */
    String getCodUffSeq();

    void setCodUffSeq(String codUffSeq);

    /**
     * Nullable property for L23-COD-UFF-SEQ
     * 
     */
    String getCodUffSeqObj();

    void setCodUffSeqObj(String codUffSeqObj);

    /**
     * Host Variable L23-NUM-PROVV-SEQ
     * 
     */
    int getNumProvvSeq();

    void setNumProvvSeq(int numProvvSeq);

    /**
     * Nullable property for L23-NUM-PROVV-SEQ
     * 
     */
    Integer getNumProvvSeqObj();

    void setNumProvvSeqObj(Integer numProvvSeqObj);

    /**
     * Host Variable L23-TP-PROVV-SEQ
     * 
     */
    String getTpProvvSeq();

    void setTpProvvSeq(String tpProvvSeq);

    /**
     * Nullable property for L23-TP-PROVV-SEQ
     * 
     */
    String getTpProvvSeqObj();

    void setTpProvvSeqObj(String tpProvvSeqObj);

    /**
     * Host Variable L23-DT-PROVV-SEQ-DB
     * 
     */
    String getDtProvvSeqDb();

    void setDtProvvSeqDb(String dtProvvSeqDb);

    /**
     * Nullable property for L23-DT-PROVV-SEQ-DB
     * 
     */
    String getDtProvvSeqDbObj();

    void setDtProvvSeqDbObj(String dtProvvSeqDbObj);

    /**
     * Host Variable L23-DT-NOTIFICA-BLOCCO-DB
     * 
     */
    String getDtNotificaBloccoDb();

    void setDtNotificaBloccoDb(String dtNotificaBloccoDb);

    /**
     * Nullable property for L23-DT-NOTIFICA-BLOCCO-DB
     * 
     */
    String getDtNotificaBloccoDbObj();

    void setDtNotificaBloccoDbObj(String dtNotificaBloccoDbObj);

    /**
     * Host Variable L23-NOTA-PROVV-VCHAR
     * 
     */
    String getNotaProvvVchar();

    void setNotaProvvVchar(String notaProvvVchar);

    /**
     * Nullable property for L23-NOTA-PROVV-VCHAR
     * 
     */
    String getNotaProvvVcharObj();

    void setNotaProvvVcharObj(String notaProvvVcharObj);
};
