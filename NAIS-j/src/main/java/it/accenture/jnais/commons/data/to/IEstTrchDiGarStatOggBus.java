package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [EST_TRCH_DI_GAR, STAT_OGG_BUS]
 * 
 */
public interface IEstTrchDiGarStatOggBus extends BaseSqlTo {

    /**
     * Host Variable LDBV5141-CUM-PRE-ATT
     * 
     */
    AfDecimal getLdbv5141CumPreAtt();

    void setLdbv5141CumPreAtt(AfDecimal ldbv5141CumPreAtt);

    /**
     * Host Variable LDBV5141-ID-ADES
     * 
     */
    int getLdbv5141IdAdes();

    void setLdbv5141IdAdes(int ldbv5141IdAdes);

    /**
     * Host Variable LDBV5141-TP-TRCH-1
     * 
     */
    String getLdbv5141TpTrch1();

    void setLdbv5141TpTrch1(String ldbv5141TpTrch1);

    /**
     * Host Variable LDBV5141-TP-TRCH-2
     * 
     */
    String getLdbv5141TpTrch2();

    void setLdbv5141TpTrch2(String ldbv5141TpTrch2);

    /**
     * Host Variable LDBV5141-TP-TRCH-3
     * 
     */
    String getLdbv5141TpTrch3();

    void setLdbv5141TpTrch3(String ldbv5141TpTrch3);

    /**
     * Host Variable LDBV5141-TP-TRCH-4
     * 
     */
    String getLdbv5141TpTrch4();

    void setLdbv5141TpTrch4(String ldbv5141TpTrch4);

    /**
     * Host Variable LDBV5141-TP-TRCH-5
     * 
     */
    String getLdbv5141TpTrch5();

    void setLdbv5141TpTrch5(String ldbv5141TpTrch5);

    /**
     * Host Variable LDBV5141-TP-TRCH-6
     * 
     */
    String getLdbv5141TpTrch6();

    void setLdbv5141TpTrch6(String ldbv5141TpTrch6);

    /**
     * Host Variable LDBV5141-TP-TRCH-7
     * 
     */
    String getLdbv5141TpTrch7();

    void setLdbv5141TpTrch7(String ldbv5141TpTrch7);

    /**
     * Host Variable LDBV5141-TP-TRCH-8
     * 
     */
    String getLdbv5141TpTrch8();

    void setLdbv5141TpTrch8(String ldbv5141TpTrch8);

    /**
     * Host Variable LDBV5141-TP-TRCH-9
     * 
     */
    String getLdbv5141TpTrch9();

    void setLdbv5141TpTrch9(String ldbv5141TpTrch9);

    /**
     * Host Variable LDBV5141-TP-TRCH-10
     * 
     */
    String getLdbv5141TpTrch10();

    void setLdbv5141TpTrch10(String ldbv5141TpTrch10);

    /**
     * Host Variable LDBV5141-TP-TRCH-11
     * 
     */
    String getLdbv5141TpTrch11();

    void setLdbv5141TpTrch11(String ldbv5141TpTrch11);

    /**
     * Host Variable LDBV5141-TP-TRCH-12
     * 
     */
    String getLdbv5141TpTrch12();

    void setLdbv5141TpTrch12(String ldbv5141TpTrch12);

    /**
     * Host Variable LDBV5141-TP-TRCH-13
     * 
     */
    String getLdbv5141TpTrch13();

    void setLdbv5141TpTrch13(String ldbv5141TpTrch13);

    /**
     * Host Variable LDBV5141-TP-TRCH-14
     * 
     */
    String getLdbv5141TpTrch14();

    void setLdbv5141TpTrch14(String ldbv5141TpTrch14);

    /**
     * Host Variable LDBV5141-TP-TRCH-15
     * 
     */
    String getLdbv5141TpTrch15();

    void setLdbv5141TpTrch15(String ldbv5141TpTrch15);

    /**
     * Host Variable LDBV5141-TP-TRCH-16
     * 
     */
    String getLdbv5141TpTrch16();

    void setLdbv5141TpTrch16(String ldbv5141TpTrch16);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBV7121-CUM-PRE-ATT
     * 
     */
    AfDecimal getLdbv7121CumPreAtt();

    void setLdbv7121CumPreAtt(AfDecimal ldbv7121CumPreAtt);

    /**
     * Nullable property for LDBV7121-CUM-PRE-ATT
     * 
     */
    AfDecimal getLdbv7121CumPreAttObj();

    void setLdbv7121CumPreAttObj(AfDecimal ldbv7121CumPreAttObj);

    /**
     * Host Variable LDBV7121-ID-GAR
     * 
     */
    int getLdbv7121IdGar();

    void setLdbv7121IdGar(int ldbv7121IdGar);

    /**
     * Host Variable LDBV7121-TP-TRCH-1
     * 
     */
    String getLdbv7121TpTrch1();

    void setLdbv7121TpTrch1(String ldbv7121TpTrch1);

    /**
     * Host Variable LDBV7121-TP-TRCH-2
     * 
     */
    String getLdbv7121TpTrch2();

    void setLdbv7121TpTrch2(String ldbv7121TpTrch2);

    /**
     * Host Variable LDBV7121-TP-TRCH-3
     * 
     */
    String getLdbv7121TpTrch3();

    void setLdbv7121TpTrch3(String ldbv7121TpTrch3);

    /**
     * Host Variable LDBV7121-TP-TRCH-4
     * 
     */
    String getLdbv7121TpTrch4();

    void setLdbv7121TpTrch4(String ldbv7121TpTrch4);

    /**
     * Host Variable LDBV7121-TP-TRCH-5
     * 
     */
    String getLdbv7121TpTrch5();

    void setLdbv7121TpTrch5(String ldbv7121TpTrch5);

    /**
     * Host Variable LDBV7121-TP-TRCH-6
     * 
     */
    String getLdbv7121TpTrch6();

    void setLdbv7121TpTrch6(String ldbv7121TpTrch6);

    /**
     * Host Variable LDBV7121-TP-TRCH-7
     * 
     */
    String getLdbv7121TpTrch7();

    void setLdbv7121TpTrch7(String ldbv7121TpTrch7);

    /**
     * Host Variable LDBV7121-TP-TRCH-8
     * 
     */
    String getLdbv7121TpTrch8();

    void setLdbv7121TpTrch8(String ldbv7121TpTrch8);

    /**
     * Host Variable LDBV7121-TP-TRCH-9
     * 
     */
    String getLdbv7121TpTrch9();

    void setLdbv7121TpTrch9(String ldbv7121TpTrch9);

    /**
     * Host Variable LDBV7121-TP-TRCH-10
     * 
     */
    String getLdbv7121TpTrch10();

    void setLdbv7121TpTrch10(String ldbv7121TpTrch10);

    /**
     * Host Variable LDBV7121-TP-TRCH-11
     * 
     */
    String getLdbv7121TpTrch11();

    void setLdbv7121TpTrch11(String ldbv7121TpTrch11);

    /**
     * Host Variable LDBV7121-TP-TRCH-12
     * 
     */
    String getLdbv7121TpTrch12();

    void setLdbv7121TpTrch12(String ldbv7121TpTrch12);

    /**
     * Host Variable LDBV7121-TP-TRCH-13
     * 
     */
    String getLdbv7121TpTrch13();

    void setLdbv7121TpTrch13(String ldbv7121TpTrch13);

    /**
     * Host Variable LDBV7121-TP-TRCH-14
     * 
     */
    String getLdbv7121TpTrch14();

    void setLdbv7121TpTrch14(String ldbv7121TpTrch14);

    /**
     * Host Variable LDBV7121-TP-TRCH-15
     * 
     */
    String getLdbv7121TpTrch15();

    void setLdbv7121TpTrch15(String ldbv7121TpTrch15);

    /**
     * Host Variable LDBV7121-TP-TRCH-16
     * 
     */
    String getLdbv7121TpTrch16();

    void setLdbv7121TpTrch16(String ldbv7121TpTrch16);
};
