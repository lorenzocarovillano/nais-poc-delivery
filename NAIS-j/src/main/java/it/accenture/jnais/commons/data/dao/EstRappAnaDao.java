package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IEstRappAna;

/**
 * Data Access Object(DAO) for table [EST_RAPP_ANA]
 * 
 */
public class EstRappAnaDao extends BaseSqlDao<IEstRappAna> {

    private Cursor cIdUpdEffE15;
    private Cursor cIdpEffE15;
    private Cursor cIdoEffE15;
    private Cursor cIdpCpzE15;
    private Cursor cIdoCpzE15;
    private final IRowMapper<IEstRappAna> selectByE15DsRigaRm = buildNamedRowMapper(IEstRappAna.class, "idEstRappAna", "idRappAna", "idRappAnaCollgObj", "e15IdOgg", "e15TpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codSoggObj", "tpRappAna", "e15DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "idSegmentazCliObj", "flCoincTitEffObj", "flPersEspPolObj", "descPersEspPolVcharObj", "tpLegCntrObj", "descLegCntrVcharObj", "tpLegPercBnficrObj", "dLegPercBnficrVcharObj", "tpCntCorrObj", "tpCoincPicPacObj");

    public EstRappAnaDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IEstRappAna> getToClass() {
        return IEstRappAna.class;
    }

    public IEstRappAna selectByE15DsRiga(long e15DsRiga, IEstRappAna iEstRappAna) {
        return buildQuery("selectByE15DsRiga").bind("e15DsRiga", e15DsRiga).rowMapper(selectByE15DsRigaRm).singleResult(iEstRappAna);
    }

    public DbAccessStatus insertRec(IEstRappAna iEstRappAna) {
        return buildQuery("insertRec").bind(iEstRappAna).executeInsert();
    }

    public DbAccessStatus updateRec(IEstRappAna iEstRappAna) {
        return buildQuery("updateRec").bind(iEstRappAna).executeUpdate();
    }

    public DbAccessStatus deleteByE15DsRiga(long e15DsRiga) {
        return buildQuery("deleteByE15DsRiga").bind("e15DsRiga", e15DsRiga).executeDelete();
    }

    public IEstRappAna selectRec(int e15IdEstRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstRappAna iEstRappAna) {
        return buildQuery("selectRec").bind("e15IdEstRappAna", e15IdEstRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByE15DsRigaRm).singleResult(iEstRappAna);
    }

    public DbAccessStatus updateRec1(IEstRappAna iEstRappAna) {
        return buildQuery("updateRec1").bind(iEstRappAna).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffE15(int e15IdEstRappAna, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffE15 = buildQuery("openCIdUpdEffE15").bind("e15IdEstRappAna", e15IdEstRappAna).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffE15() {
        return closeCursor(cIdUpdEffE15);
    }

    public IEstRappAna fetchCIdUpdEffE15(IEstRappAna iEstRappAna) {
        return fetch(cIdUpdEffE15, iEstRappAna, selectByE15DsRigaRm);
    }

    public IEstRappAna selectRec1(int e15IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstRappAna iEstRappAna) {
        return buildQuery("selectRec1").bind("e15IdRappAna", e15IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByE15DsRigaRm).singleResult(iEstRappAna);
    }

    public DbAccessStatus openCIdpEffE15(int e15IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffE15 = buildQuery("openCIdpEffE15").bind("e15IdRappAna", e15IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffE15() {
        return closeCursor(cIdpEffE15);
    }

    public IEstRappAna fetchCIdpEffE15(IEstRappAna iEstRappAna) {
        return fetch(cIdpEffE15, iEstRappAna, selectByE15DsRigaRm);
    }

    public IEstRappAna selectRec2(int e15IdOgg, String e15TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstRappAna iEstRappAna) {
        return buildQuery("selectRec2").bind("e15IdOgg", e15IdOgg).bind("e15TpOgg", e15TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByE15DsRigaRm).singleResult(iEstRappAna);
    }

    public DbAccessStatus openCIdoEffE15(int e15IdOgg, String e15TpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffE15 = buildQuery("openCIdoEffE15").bind("e15IdOgg", e15IdOgg).bind("e15TpOgg", e15TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffE15() {
        return closeCursor(cIdoEffE15);
    }

    public IEstRappAna fetchCIdoEffE15(IEstRappAna iEstRappAna) {
        return fetch(cIdoEffE15, iEstRappAna, selectByE15DsRigaRm);
    }

    public IEstRappAna selectRec3(int e15IdEstRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstRappAna iEstRappAna) {
        return buildQuery("selectRec3").bind("e15IdEstRappAna", e15IdEstRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByE15DsRigaRm).singleResult(iEstRappAna);
    }

    public IEstRappAna selectRec4(int e15IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstRappAna iEstRappAna) {
        return buildQuery("selectRec4").bind("e15IdRappAna", e15IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByE15DsRigaRm).singleResult(iEstRappAna);
    }

    public DbAccessStatus openCIdpCpzE15(int e15IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzE15 = buildQuery("openCIdpCpzE15").bind("e15IdRappAna", e15IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzE15() {
        return closeCursor(cIdpCpzE15);
    }

    public IEstRappAna fetchCIdpCpzE15(IEstRappAna iEstRappAna) {
        return fetch(cIdpCpzE15, iEstRappAna, selectByE15DsRigaRm);
    }

    public IEstRappAna selectRec5(IEstRappAna iEstRappAna) {
        return buildQuery("selectRec5").bind(iEstRappAna).rowMapper(selectByE15DsRigaRm).singleResult(iEstRappAna);
    }

    public DbAccessStatus openCIdoCpzE15(IEstRappAna iEstRappAna) {
        cIdoCpzE15 = buildQuery("openCIdoCpzE15").bind(iEstRappAna).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzE15() {
        return closeCursor(cIdoCpzE15);
    }

    public IEstRappAna fetchCIdoCpzE15(IEstRappAna iEstRappAna) {
        return fetch(cIdoCpzE15, iEstRappAna, selectByE15DsRigaRm);
    }
}
