package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [MOT_LIQ]
 * 
 */
public interface IMotLiq extends BaseSqlTo {

    /**
     * Host Variable P86-ID-MOT-LIQ
     * 
     */
    int getIdMotLiq();

    void setIdMotLiq(int idMotLiq);

    /**
     * Host Variable P86-ID-LIQ
     * 
     */
    int getIdLiq();

    void setIdLiq(int idLiq);

    /**
     * Host Variable P86-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable P86-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for P86-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable P86-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable P86-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable P86-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P86-TP-LIQ
     * 
     */
    String getTpLiq();

    void setTpLiq(String tpLiq);

    /**
     * Host Variable P86-TP-MOT-LIQ
     * 
     */
    String getTpMotLiq();

    void setTpMotLiq(String tpMotLiq);

    /**
     * Host Variable P86-DS-RIGA
     * 
     */
    long getP86DsRiga();

    void setP86DsRiga(long p86DsRiga);

    /**
     * Host Variable P86-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P86-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P86-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable P86-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable P86-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P86-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable P86-DESC-LIB-MOT-LIQ-VCHAR
     * 
     */
    String getDescLibMotLiqVchar();

    void setDescLibMotLiqVchar(String descLibMotLiqVchar);

    /**
     * Nullable property for P86-DESC-LIB-MOT-LIQ-VCHAR
     * 
     */
    String getDescLibMotLiqVcharObj();

    void setDescLibMotLiqVcharObj(String descLibMotLiqVcharObj);
};
