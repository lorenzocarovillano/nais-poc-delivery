package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PROV_DI_GAR]
 * 
 */
public interface IProvDiGar extends BaseSqlTo {

    /**
     * Host Variable PVT-ID-PROV-DI-GAR
     * 
     */
    int getIdProvDiGar();

    void setIdProvDiGar(int idProvDiGar);

    /**
     * Host Variable PVT-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Nullable property for PVT-ID-GAR
     * 
     */
    Integer getIdGarObj();

    void setIdGarObj(Integer idGarObj);

    /**
     * Host Variable PVT-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable PVT-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for PVT-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable PVT-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable PVT-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable PVT-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable PVT-TP-PROV
     * 
     */
    String getTpProv();

    void setTpProv(String tpProv);

    /**
     * Nullable property for PVT-TP-PROV
     * 
     */
    String getTpProvObj();

    void setTpProvObj(String tpProvObj);

    /**
     * Host Variable PVT-PROV-1AA-ACQ
     * 
     */
    AfDecimal getProv1aaAcq();

    void setProv1aaAcq(AfDecimal prov1aaAcq);

    /**
     * Nullable property for PVT-PROV-1AA-ACQ
     * 
     */
    AfDecimal getProv1aaAcqObj();

    void setProv1aaAcqObj(AfDecimal prov1aaAcqObj);

    /**
     * Host Variable PVT-PROV-2AA-ACQ
     * 
     */
    AfDecimal getProv2aaAcq();

    void setProv2aaAcq(AfDecimal prov2aaAcq);

    /**
     * Nullable property for PVT-PROV-2AA-ACQ
     * 
     */
    AfDecimal getProv2aaAcqObj();

    void setProv2aaAcqObj(AfDecimal prov2aaAcqObj);

    /**
     * Host Variable PVT-PROV-RICOR
     * 
     */
    AfDecimal getProvRicor();

    void setProvRicor(AfDecimal provRicor);

    /**
     * Nullable property for PVT-PROV-RICOR
     * 
     */
    AfDecimal getProvRicorObj();

    void setProvRicorObj(AfDecimal provRicorObj);

    /**
     * Host Variable PVT-PROV-INC
     * 
     */
    AfDecimal getProvInc();

    void setProvInc(AfDecimal provInc);

    /**
     * Nullable property for PVT-PROV-INC
     * 
     */
    AfDecimal getProvIncObj();

    void setProvIncObj(AfDecimal provIncObj);

    /**
     * Host Variable PVT-ALQ-PROV-ACQ
     * 
     */
    AfDecimal getAlqProvAcq();

    void setAlqProvAcq(AfDecimal alqProvAcq);

    /**
     * Nullable property for PVT-ALQ-PROV-ACQ
     * 
     */
    AfDecimal getAlqProvAcqObj();

    void setAlqProvAcqObj(AfDecimal alqProvAcqObj);

    /**
     * Host Variable PVT-ALQ-PROV-INC
     * 
     */
    AfDecimal getAlqProvInc();

    void setAlqProvInc(AfDecimal alqProvInc);

    /**
     * Nullable property for PVT-ALQ-PROV-INC
     * 
     */
    AfDecimal getAlqProvIncObj();

    void setAlqProvIncObj(AfDecimal alqProvIncObj);

    /**
     * Host Variable PVT-ALQ-PROV-RICOR
     * 
     */
    AfDecimal getAlqProvRicor();

    void setAlqProvRicor(AfDecimal alqProvRicor);

    /**
     * Nullable property for PVT-ALQ-PROV-RICOR
     * 
     */
    AfDecimal getAlqProvRicorObj();

    void setAlqProvRicorObj(AfDecimal alqProvRicorObj);

    /**
     * Host Variable PVT-FL-STOR-PROV-ACQ
     * 
     */
    char getFlStorProvAcq();

    void setFlStorProvAcq(char flStorProvAcq);

    /**
     * Nullable property for PVT-FL-STOR-PROV-ACQ
     * 
     */
    Character getFlStorProvAcqObj();

    void setFlStorProvAcqObj(Character flStorProvAcqObj);

    /**
     * Host Variable PVT-FL-REC-PROV-STORN
     * 
     */
    char getFlRecProvStorn();

    void setFlRecProvStorn(char flRecProvStorn);

    /**
     * Nullable property for PVT-FL-REC-PROV-STORN
     * 
     */
    Character getFlRecProvStornObj();

    void setFlRecProvStornObj(Character flRecProvStornObj);

    /**
     * Host Variable PVT-FL-PROV-FORZ
     * 
     */
    char getFlProvForz();

    void setFlProvForz(char flProvForz);

    /**
     * Nullable property for PVT-FL-PROV-FORZ
     * 
     */
    Character getFlProvForzObj();

    void setFlProvForzObj(Character flProvForzObj);

    /**
     * Host Variable PVT-TP-CALC-PROV
     * 
     */
    char getTpCalcProv();

    void setTpCalcProv(char tpCalcProv);

    /**
     * Nullable property for PVT-TP-CALC-PROV
     * 
     */
    Character getTpCalcProvObj();

    void setTpCalcProvObj(Character tpCalcProvObj);

    /**
     * Host Variable PVT-DS-RIGA
     * 
     */
    long getPvtDsRiga();

    void setPvtDsRiga(long pvtDsRiga);

    /**
     * Host Variable PVT-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable PVT-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable PVT-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable PVT-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable PVT-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable PVT-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable PVT-REMUN-ASS
     * 
     */
    AfDecimal getRemunAss();

    void setRemunAss(AfDecimal remunAss);

    /**
     * Nullable property for PVT-REMUN-ASS
     * 
     */
    AfDecimal getRemunAssObj();

    void setRemunAssObj(AfDecimal remunAssObj);

    /**
     * Host Variable PVT-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInter();

    void setCommisInter(AfDecimal commisInter);

    /**
     * Nullable property for PVT-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInterObj();

    void setCommisInterObj(AfDecimal commisInterObj);

    /**
     * Host Variable PVT-ALQ-REMUN-ASS
     * 
     */
    AfDecimal getAlqRemunAss();

    void setAlqRemunAss(AfDecimal alqRemunAss);

    /**
     * Nullable property for PVT-ALQ-REMUN-ASS
     * 
     */
    AfDecimal getAlqRemunAssObj();

    void setAlqRemunAssObj(AfDecimal alqRemunAssObj);

    /**
     * Host Variable PVT-ALQ-COMMIS-INTER
     * 
     */
    AfDecimal getAlqCommisInter();

    void setAlqCommisInter(AfDecimal alqCommisInter);

    /**
     * Nullable property for PVT-ALQ-COMMIS-INTER
     * 
     */
    AfDecimal getAlqCommisInterObj();

    void setAlqCommisInterObj(AfDecimal alqCommisInterObj);
};
