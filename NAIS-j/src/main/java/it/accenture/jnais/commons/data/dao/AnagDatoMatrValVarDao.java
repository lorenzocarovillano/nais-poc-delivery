package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAnagDatoMatrValVar;

/**
 * Data Access Object(DAO) for tables [ANAG_DATO, MATR_VAL_VAR]
 * 
 */
public class AnagDatoMatrValVarDao extends BaseSqlDao<IAnagDatoMatrValVar> {

    private Cursor curServizi;
    private final IRowMapper<IAnagDatoMatrValVar> selectRecRm = buildNamedRowMapper(IAnagDatoMatrValVar.class, "adaTipoDatoObj", "adaLunghezzaDatoObj", "adaPrecisioneDatoObj", "adaFormattazioneDatoObj", "mvvIdMatrValVar", "mvvIdpMatrValVarObj", "mvvTipoMovimentoObj", "mvvCodDatoExtObj", "mvvObbligatorietaObj", "mvvValoreDefaultObj", "mvvCodStrDatoPtfObj", "mvvCodDatoPtfObj", "mvvCodParametroObj", "mvvOperazioneObj", "mvvLivelloOperazioneObj", "mvvTipoOggettoObj", "mvvWhereConditionVcharObj", "mvvServizioLetturaObj", "mvvModuloCalcoloObj", "mvvCodDatoInternoObj");

    public AnagDatoMatrValVarDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAnagDatoMatrValVar> getToClass() {
        return IAnagDatoMatrValVar.class;
    }

    public IAnagDatoMatrValVar selectRec(int adaCodCompagniaAnia, int mvvCodCompagniaAnia, String v1391CodActu, IAnagDatoMatrValVar iAnagDatoMatrValVar) {
        return buildQuery("selectRec").bind("adaCodCompagniaAnia", adaCodCompagniaAnia).bind("mvvCodCompagniaAnia", mvvCodCompagniaAnia).bind("v1391CodActu", v1391CodActu).rowMapper(selectRecRm).singleResult(iAnagDatoMatrValVar);
    }

    public DbAccessStatus openCurServizi(int adaCodCompagniaAnia, int mvvCodCompagniaAnia, String v1391CodActu) {
        curServizi = buildQuery("openCurServizi").bind("adaCodCompagniaAnia", adaCodCompagniaAnia).bind("mvvCodCompagniaAnia", mvvCodCompagniaAnia).bind("v1391CodActu", v1391CodActu).open();
        return dbStatus;
    }

    public IAnagDatoMatrValVar fetchCurServizi(IAnagDatoMatrValVar iAnagDatoMatrValVar) {
        return fetch(curServizi, iAnagDatoMatrValVar, selectRecRm);
    }

    public DbAccessStatus closeCurServizi() {
        return closeCursor(curServizi);
    }
}
