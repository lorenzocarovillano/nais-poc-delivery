package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [MOVI, STAT_OGG_WF]
 * 
 */
public interface IMoviStatOggWf extends BaseSqlTo {

    /**
     * Host Variable MOV-ID-OGG
     * 
     */
    int getMovIdOgg();

    void setMovIdOgg(int movIdOgg);

    /**
     * Host Variable MOV-TP-OGG
     * 
     */
    String getMovTpOgg();

    void setMovTpOgg(String movTpOgg);

    /**
     * Host Variable MOV-DT-EFF-DB
     * 
     */
    String getMovDtEffDb();

    void setMovDtEffDb(String movDtEffDb);

    /**
     * Host Variable WS-DT-INFINITO-1
     * 
     */
    String getWsDtInfinito1();

    void setWsDtInfinito1(String wsDtInfinito1);

    /**
     * Host Variable WS-TS-INFINITO-1
     * 
     */
    long getWsTsInfinito1();

    void setWsTsInfinito1(long wsTsInfinito1);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable MOV-ID-MOVI
     * 
     */
    int getIdMovi();

    void setIdMovi(int idMovi);

    /**
     * Host Variable MOV-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Nullable property for MOV-ID-OGG
     * 
     */
    Integer getMovIdOggObj();

    void setMovIdOggObj(Integer movIdOggObj);

    /**
     * Host Variable MOV-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for MOV-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable MOV-IB-MOVI
     * 
     */
    String getIbMovi();

    void setIbMovi(String ibMovi);

    /**
     * Nullable property for MOV-IB-MOVI
     * 
     */
    String getIbMoviObj();

    void setIbMoviObj(String ibMoviObj);

    /**
     * Nullable property for MOV-TP-OGG
     * 
     */
    String getMovTpOggObj();

    void setMovTpOggObj(String movTpOggObj);

    /**
     * Host Variable MOV-ID-RICH
     * 
     */
    int getIdRich();

    void setIdRich(int idRich);

    /**
     * Nullable property for MOV-ID-RICH
     * 
     */
    Integer getIdRichObj();

    void setIdRichObj(Integer idRichObj);

    /**
     * Host Variable MOV-TP-MOVI
     * 
     */
    int getTpMovi();

    void setTpMovi(int tpMovi);

    /**
     * Nullable property for MOV-TP-MOVI
     * 
     */
    Integer getTpMoviObj();

    void setTpMoviObj(Integer tpMoviObj);

    /**
     * Host Variable MOV-ID-MOVI-ANN
     * 
     */
    int getIdMoviAnn();

    void setIdMoviAnn(int idMoviAnn);

    /**
     * Nullable property for MOV-ID-MOVI-ANN
     * 
     */
    Integer getIdMoviAnnObj();

    void setIdMoviAnnObj(Integer idMoviAnnObj);

    /**
     * Host Variable MOV-ID-MOVI-COLLG
     * 
     */
    int getIdMoviCollg();

    void setIdMoviCollg(int idMoviCollg);

    /**
     * Nullable property for MOV-ID-MOVI-COLLG
     * 
     */
    Integer getIdMoviCollgObj();

    void setIdMoviCollgObj(Integer idMoviCollgObj);

    /**
     * Host Variable MOV-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable MOV-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable MOV-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable MOV-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable MOV-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
