package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IVarFunzDiCalc;

/**
 * Data Access Object(DAO) for table [VAR_FUNZ_DI_CALC]
 * 
 */
public class VarFunzDiCalcDao extends BaseSqlDao<IVarFunzDiCalc> {

    private final IRowMapper<IVarFunzDiCalc> selectRecRm = buildNamedRowMapper(IVarFunzDiCalc.class, "idcomp", "codprodVchar", "codtariVchar", "diniz", "nomefunzVchar", "dend", "isprecalcObj", "nomeprogrVcharObj", "modcalcVcharObj", "glovarlistVcharObj", "stepElabVchar");

    public VarFunzDiCalcDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IVarFunzDiCalc> getToClass() {
        return IVarFunzDiCalc.class;
    }

    public IVarFunzDiCalc selectRec(IVarFunzDiCalc iVarFunzDiCalc) {
        return buildQuery("selectRec").bind(iVarFunzDiCalc).rowMapper(selectRecRm).singleResult(iVarFunzDiCalc);
    }

    public DbAccessStatus insertRec(IVarFunzDiCalc iVarFunzDiCalc) {
        return buildQuery("insertRec").bind(iVarFunzDiCalc).executeInsert();
    }

    public DbAccessStatus updateRec(IVarFunzDiCalc iVarFunzDiCalc) {
        return buildQuery("updateRec").bind(iVarFunzDiCalc).executeUpdate();
    }

    public DbAccessStatus deleteRec(IVarFunzDiCalc iVarFunzDiCalc) {
        return buildQuery("deleteRec").bind(iVarFunzDiCalc).executeDelete();
    }

    public IVarFunzDiCalc selectRec1(IVarFunzDiCalc iVarFunzDiCalc) {
        return buildQuery("selectRec1").bind(iVarFunzDiCalc).rowMapper(selectRecRm).singleResult(iVarFunzDiCalc);
    }
}
