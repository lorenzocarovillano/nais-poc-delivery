package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBtcJobExecution;

/**
 * Data Access Object(DAO) for table [BTC_JOB_EXECUTION]
 * 
 */
public class BtcJobExecutionDao extends BaseSqlDao<IBtcJobExecution> {

    private final IRowMapper<IBtcJobExecution> selectRecRm = buildNamedRowMapper(IBtcJobExecution.class, "idBatch", "idJob", "idExecution", "codElabState", "dataVcharObj", "flagWarningsObj", "dtStartDb", "dtEndDbObj", "userStart");

    public BtcJobExecutionDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBtcJobExecution> getToClass() {
        return IBtcJobExecution.class;
    }

    public IBtcJobExecution selectRec(int idBatch, int idJob, int idExecution, IBtcJobExecution iBtcJobExecution) {
        return buildQuery("selectRec").bind("idBatch", idBatch).bind("idJob", idJob).bind("idExecution", idExecution).rowMapper(selectRecRm).singleResult(iBtcJobExecution);
    }

    public DbAccessStatus insertRec(IBtcJobExecution iBtcJobExecution) {
        return buildQuery("insertRec").bind(iBtcJobExecution).executeInsert();
    }

    public DbAccessStatus updateRec(IBtcJobExecution iBtcJobExecution) {
        return buildQuery("updateRec").bind(iBtcJobExecution).executeUpdate();
    }

    public DbAccessStatus deleteRec(int batch, int job, int execution) {
        return buildQuery("deleteRec").bind("batch", batch).bind("job", job).bind("execution", execution).executeDelete();
    }
}
