package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [GAR, STAT_OGG_BUS]
 * 
 */
public interface IGarStatOggBus extends BaseSqlTo {

    /**
     * Host Variable GRZ-ID-POLI
     * 
     */
    int getGrzIdPoli();

    void setGrzIdPoli(int grzIdPoli);

    /**
     * Host Variable WK-ID-ADES-DA
     * 
     */
    int getWkIdAdesDa();

    void setWkIdAdesDa(int wkIdAdesDa);

    /**
     * Host Variable WK-ID-ADES-A
     * 
     */
    int getWkIdAdesA();

    void setWkIdAdesA(int wkIdAdesA);

    /**
     * Host Variable WK-ID-GAR-DA
     * 
     */
    int getWkIdGarDa();

    void setWkIdGarDa(int wkIdGarDa);

    /**
     * Host Variable WK-ID-GAR-A
     * 
     */
    int getWkIdGarA();

    void setWkIdGarA(int wkIdGarA);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable LDBV1351-TP-STAT-BUS-01
     * 
     */
    String getLdbv1351TpStatBus01();

    void setLdbv1351TpStatBus01(String ldbv1351TpStatBus01);

    /**
     * Host Variable LDBV1351-TP-STAT-BUS-02
     * 
     */
    String getLdbv1351TpStatBus02();

    void setLdbv1351TpStatBus02(String ldbv1351TpStatBus02);

    /**
     * Host Variable LDBV1351-TP-STAT-BUS-03
     * 
     */
    String getLdbv1351TpStatBus03();

    void setLdbv1351TpStatBus03(String ldbv1351TpStatBus03);

    /**
     * Host Variable LDBV1351-TP-STAT-BUS-04
     * 
     */
    String getLdbv1351TpStatBus04();

    void setLdbv1351TpStatBus04(String ldbv1351TpStatBus04);

    /**
     * Host Variable LDBV1351-TP-STAT-BUS-05
     * 
     */
    String getLdbv1351TpStatBus05();

    void setLdbv1351TpStatBus05(String ldbv1351TpStatBus05);

    /**
     * Host Variable LDBV1351-TP-STAT-BUS-06
     * 
     */
    String getLdbv1351TpStatBus06();

    void setLdbv1351TpStatBus06(String ldbv1351TpStatBus06);

    /**
     * Host Variable LDBV1351-TP-STAT-BUS-07
     * 
     */
    String getLdbv1351TpStatBus07();

    void setLdbv1351TpStatBus07(String ldbv1351TpStatBus07);

    /**
     * Host Variable LDBV1351-TP-CAUS-01
     * 
     */
    String getLdbv1351TpCaus01();

    void setLdbv1351TpCaus01(String ldbv1351TpCaus01);

    /**
     * Host Variable LDBV1351-TP-CAUS-02
     * 
     */
    String getLdbv1351TpCaus02();

    void setLdbv1351TpCaus02(String ldbv1351TpCaus02);

    /**
     * Host Variable LDBV1351-TP-CAUS-03
     * 
     */
    String getLdbv1351TpCaus03();

    void setLdbv1351TpCaus03(String ldbv1351TpCaus03);

    /**
     * Host Variable LDBV1351-TP-CAUS-04
     * 
     */
    String getLdbv1351TpCaus04();

    void setLdbv1351TpCaus04(String ldbv1351TpCaus04);

    /**
     * Host Variable LDBV1351-TP-CAUS-05
     * 
     */
    String getLdbv1351TpCaus05();

    void setLdbv1351TpCaus05(String ldbv1351TpCaus05);

    /**
     * Host Variable LDBV1351-TP-CAUS-06
     * 
     */
    String getLdbv1351TpCaus06();

    void setLdbv1351TpCaus06(String ldbv1351TpCaus06);

    /**
     * Host Variable LDBV1351-TP-CAUS-07
     * 
     */
    String getLdbv1351TpCaus07();

    void setLdbv1351TpCaus07(String ldbv1351TpCaus07);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBV1421-ID-POLI
     * 
     */
    int getLdbv1421IdPoli();

    void setLdbv1421IdPoli(int ldbv1421IdPoli);

    /**
     * Host Variable LDBV1421-RAMO-BILA-1
     * 
     */
    String getLdbv1421RamoBila1();

    void setLdbv1421RamoBila1(String ldbv1421RamoBila1);

    /**
     * Host Variable LDBV1421-RAMO-BILA-2
     * 
     */
    String getLdbv1421RamoBila2();

    void setLdbv1421RamoBila2(String ldbv1421RamoBila2);

    /**
     * Host Variable LDBV1421-RAMO-BILA-3
     * 
     */
    String getLdbv1421RamoBila3();

    void setLdbv1421RamoBila3(String ldbv1421RamoBila3);

    /**
     * Host Variable LDBV1421-RAMO-BILA-4
     * 
     */
    String getLdbv1421RamoBila4();

    void setLdbv1421RamoBila4(String ldbv1421RamoBila4);

    /**
     * Host Variable LDBV1421-RAMO-BILA-5
     * 
     */
    String getLdbv1421RamoBila5();

    void setLdbv1421RamoBila5(String ldbv1421RamoBila5);

    /**
     * Host Variable LDBV3401-ID-POLI
     * 
     */
    int getLdbv3401IdPoli();

    void setLdbv3401IdPoli(int ldbv3401IdPoli);

    /**
     * Host Variable LDBV3401-ID-ADES
     * 
     */
    int getLdbv3401IdAdes();

    void setLdbv3401IdAdes(int ldbv3401IdAdes);

    /**
     * Host Variable LDBV3401-TP-OGG
     * 
     */
    String getLdbv3401TpOgg();

    void setLdbv3401TpOgg(String ldbv3401TpOgg);

    /**
     * Host Variable LDBV3401-TP-STAT-BUS-1
     * 
     */
    String getLdbv3401TpStatBus1();

    void setLdbv3401TpStatBus1(String ldbv3401TpStatBus1);

    /**
     * Host Variable LDBV3401-TP-STAT-BUS-2
     * 
     */
    String getLdbv3401TpStatBus2();

    void setLdbv3401TpStatBus2(String ldbv3401TpStatBus2);

    /**
     * Host Variable LDBV3401-RAMO1
     * 
     */
    String getLdbv3401Ramo1();

    void setLdbv3401Ramo1(String ldbv3401Ramo1);

    /**
     * Host Variable LDBV3401-RAMO2
     * 
     */
    String getLdbv3401Ramo2();

    void setLdbv3401Ramo2(String ldbv3401Ramo2);

    /**
     * Host Variable LDBV3401-RAMO3
     * 
     */
    String getLdbv3401Ramo3();

    void setLdbv3401Ramo3(String ldbv3401Ramo3);

    /**
     * Host Variable LDBV3401-RAMO4
     * 
     */
    String getLdbv3401Ramo4();

    void setLdbv3401Ramo4(String ldbv3401Ramo4);

    /**
     * Host Variable LDBV3401-RAMO5
     * 
     */
    String getLdbv3401Ramo5();

    void setLdbv3401Ramo5(String ldbv3401Ramo5);

    /**
     * Host Variable LDBV3401-RAMO6
     * 
     */
    String getLdbv3401Ramo6();

    void setLdbv3401Ramo6(String ldbv3401Ramo6);

    /**
     * Host Variable LDBV3411-ID-POLI
     * 
     */
    int getLdbv3411IdPoli();

    void setLdbv3411IdPoli(int ldbv3411IdPoli);

    /**
     * Host Variable LDBV3411-ID-ADES
     * 
     */
    int getLdbv3411IdAdes();

    void setLdbv3411IdAdes(int ldbv3411IdAdes);

    /**
     * Host Variable LDBV3411-TP-OGG
     * 
     */
    String getLdbv3411TpOgg();

    void setLdbv3411TpOgg(String ldbv3411TpOgg);

    /**
     * Host Variable LDBV3411-TP-STAT-BUS-1
     * 
     */
    String getLdbv3411TpStatBus1();

    void setLdbv3411TpStatBus1(String ldbv3411TpStatBus1);

    /**
     * Host Variable LDBV3411-TP-STAT-BUS-2
     * 
     */
    String getLdbv3411TpStatBus2();

    void setLdbv3411TpStatBus2(String ldbv3411TpStatBus2);

    /**
     * Host Variable LDBV9091-ID-POLI
     * 
     */
    int getLdbv9091IdPoli();

    void setLdbv9091IdPoli(int ldbv9091IdPoli);

    /**
     * Host Variable LDBV9091-ID-ADES
     * 
     */
    int getLdbv9091IdAdes();

    void setLdbv9091IdAdes(int ldbv9091IdAdes);

    /**
     * Host Variable LDBV9091-TP-OGG
     * 
     */
    String getLdbv9091TpOgg();

    void setLdbv9091TpOgg(String ldbv9091TpOgg);

    /**
     * Host Variable LDBV9091-TP-STAT-BUS-1
     * 
     */
    String getLdbv9091TpStatBus1();

    void setLdbv9091TpStatBus1(String ldbv9091TpStatBus1);

    /**
     * Host Variable LDBV9091-TP-STAT-BUS-2
     * 
     */
    String getLdbv9091TpStatBus2();

    void setLdbv9091TpStatBus2(String ldbv9091TpStatBus2);

    /**
     * Host Variable LDBV9091-RAMO1
     * 
     */
    String getLdbv9091Ramo1();

    void setLdbv9091Ramo1(String ldbv9091Ramo1);

    /**
     * Host Variable LDBV9091-RAMO2
     * 
     */
    String getLdbv9091Ramo2();

    void setLdbv9091Ramo2(String ldbv9091Ramo2);

    /**
     * Host Variable LDBV9091-RAMO3
     * 
     */
    String getLdbv9091Ramo3();

    void setLdbv9091Ramo3(String ldbv9091Ramo3);

    /**
     * Host Variable LDBV9091-RAMO4
     * 
     */
    String getLdbv9091Ramo4();

    void setLdbv9091Ramo4(String ldbv9091Ramo4);

    /**
     * Host Variable LDBV9091-RAMO5
     * 
     */
    String getLdbv9091Ramo5();

    void setLdbv9091Ramo5(String ldbv9091Ramo5);

    /**
     * Host Variable LDBV9091-RAMO6
     * 
     */
    String getLdbv9091Ramo6();

    void setLdbv9091Ramo6(String ldbv9091Ramo6);

    /**
     * Host Variable LDBV9091-TP-INVST1
     * 
     */
    short getLdbv9091TpInvst1();

    void setLdbv9091TpInvst1(short ldbv9091TpInvst1);

    /**
     * Host Variable LDBV9091-TP-INVST2
     * 
     */
    short getLdbv9091TpInvst2();

    void setLdbv9091TpInvst2(short ldbv9091TpInvst2);

    /**
     * Host Variable LDBV9091-TP-INVST3
     * 
     */
    short getLdbv9091TpInvst3();

    void setLdbv9091TpInvst3(short ldbv9091TpInvst3);

    /**
     * Host Variable LDBV9091-TP-INVST4
     * 
     */
    short getLdbv9091TpInvst4();

    void setLdbv9091TpInvst4(short ldbv9091TpInvst4);

    /**
     * Host Variable LDBV9091-TP-INVST5
     * 
     */
    short getLdbv9091TpInvst5();

    void setLdbv9091TpInvst5(short ldbv9091TpInvst5);

    /**
     * Host Variable LDBV9091-TP-INVST6
     * 
     */
    short getLdbv9091TpInvst6();

    void setLdbv9091TpInvst6(short ldbv9091TpInvst6);

    /**
     * Host Variable GRZ-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Host Variable GRZ-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Nullable property for GRZ-ID-ADES
     * 
     */
    Integer getIdAdesObj();

    void setIdAdesObj(Integer idAdesObj);

    /**
     * Host Variable GRZ-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable GRZ-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for GRZ-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable GRZ-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable GRZ-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable GRZ-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable GRZ-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for GRZ-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable GRZ-DT-DECOR-DB
     * 
     */
    String getDtDecorDb();

    void setDtDecorDb(String dtDecorDb);

    /**
     * Nullable property for GRZ-DT-DECOR-DB
     * 
     */
    String getDtDecorDbObj();

    void setDtDecorDbObj(String dtDecorDbObj);

    /**
     * Host Variable GRZ-DT-SCAD-DB
     * 
     */
    String getDtScadDb();

    void setDtScadDb(String dtScadDb);

    /**
     * Nullable property for GRZ-DT-SCAD-DB
     * 
     */
    String getDtScadDbObj();

    void setDtScadDbObj(String dtScadDbObj);

    /**
     * Host Variable GRZ-COD-SEZ
     * 
     */
    String getCodSez();

    void setCodSez(String codSez);

    /**
     * Nullable property for GRZ-COD-SEZ
     * 
     */
    String getCodSezObj();

    void setCodSezObj(String codSezObj);

    /**
     * Host Variable GRZ-COD-TARI
     * 
     */
    String getGrzCodTari();

    void setGrzCodTari(String grzCodTari);

    /**
     * Host Variable GRZ-RAMO-BILA
     * 
     */
    String getRamoBila();

    void setRamoBila(String ramoBila);

    /**
     * Nullable property for GRZ-RAMO-BILA
     * 
     */
    String getRamoBilaObj();

    void setRamoBilaObj(String ramoBilaObj);

    /**
     * Host Variable GRZ-DT-INI-VAL-TAR-DB
     * 
     */
    String getDtIniValTarDb();

    void setDtIniValTarDb(String dtIniValTarDb);

    /**
     * Nullable property for GRZ-DT-INI-VAL-TAR-DB
     * 
     */
    String getDtIniValTarDbObj();

    void setDtIniValTarDbObj(String dtIniValTarDbObj);

    /**
     * Host Variable GRZ-ID-1O-ASSTO
     * 
     */
    int getId1oAssto();

    void setId1oAssto(int id1oAssto);

    /**
     * Nullable property for GRZ-ID-1O-ASSTO
     * 
     */
    Integer getId1oAsstoObj();

    void setId1oAsstoObj(Integer id1oAsstoObj);

    /**
     * Host Variable GRZ-ID-2O-ASSTO
     * 
     */
    int getId2oAssto();

    void setId2oAssto(int id2oAssto);

    /**
     * Nullable property for GRZ-ID-2O-ASSTO
     * 
     */
    Integer getId2oAsstoObj();

    void setId2oAsstoObj(Integer id2oAsstoObj);

    /**
     * Host Variable GRZ-ID-3O-ASSTO
     * 
     */
    int getId3oAssto();

    void setId3oAssto(int id3oAssto);

    /**
     * Nullable property for GRZ-ID-3O-ASSTO
     * 
     */
    Integer getId3oAsstoObj();

    void setId3oAsstoObj(Integer id3oAsstoObj);

    /**
     * Host Variable GRZ-TP-GAR
     * 
     */
    short getTpGar();

    void setTpGar(short tpGar);

    /**
     * Nullable property for GRZ-TP-GAR
     * 
     */
    Short getTpGarObj();

    void setTpGarObj(Short tpGarObj);

    /**
     * Host Variable GRZ-TP-RSH
     * 
     */
    String getTpRsh();

    void setTpRsh(String tpRsh);

    /**
     * Nullable property for GRZ-TP-RSH
     * 
     */
    String getTpRshObj();

    void setTpRshObj(String tpRshObj);

    /**
     * Host Variable GRZ-TP-INVST
     * 
     */
    short getTpInvst();

    void setTpInvst(short tpInvst);

    /**
     * Nullable property for GRZ-TP-INVST
     * 
     */
    Short getTpInvstObj();

    void setTpInvstObj(Short tpInvstObj);

    /**
     * Host Variable GRZ-MOD-PAG-GARCOL
     * 
     */
    String getModPagGarcol();

    void setModPagGarcol(String modPagGarcol);

    /**
     * Nullable property for GRZ-MOD-PAG-GARCOL
     * 
     */
    String getModPagGarcolObj();

    void setModPagGarcolObj(String modPagGarcolObj);

    /**
     * Host Variable GRZ-TP-PER-PRE
     * 
     */
    String getTpPerPre();

    void setTpPerPre(String tpPerPre);

    /**
     * Nullable property for GRZ-TP-PER-PRE
     * 
     */
    String getTpPerPreObj();

    void setTpPerPreObj(String tpPerPreObj);

    /**
     * Host Variable GRZ-ETA-AA-1O-ASSTO
     * 
     */
    short getEtaAa1oAssto();

    void setEtaAa1oAssto(short etaAa1oAssto);

    /**
     * Nullable property for GRZ-ETA-AA-1O-ASSTO
     * 
     */
    Short getEtaAa1oAsstoObj();

    void setEtaAa1oAsstoObj(Short etaAa1oAsstoObj);

    /**
     * Host Variable GRZ-ETA-MM-1O-ASSTO
     * 
     */
    short getEtaMm1oAssto();

    void setEtaMm1oAssto(short etaMm1oAssto);

    /**
     * Nullable property for GRZ-ETA-MM-1O-ASSTO
     * 
     */
    Short getEtaMm1oAsstoObj();

    void setEtaMm1oAsstoObj(Short etaMm1oAsstoObj);

    /**
     * Host Variable GRZ-ETA-AA-2O-ASSTO
     * 
     */
    short getEtaAa2oAssto();

    void setEtaAa2oAssto(short etaAa2oAssto);

    /**
     * Nullable property for GRZ-ETA-AA-2O-ASSTO
     * 
     */
    Short getEtaAa2oAsstoObj();

    void setEtaAa2oAsstoObj(Short etaAa2oAsstoObj);

    /**
     * Host Variable GRZ-ETA-MM-2O-ASSTO
     * 
     */
    short getEtaMm2oAssto();

    void setEtaMm2oAssto(short etaMm2oAssto);

    /**
     * Nullable property for GRZ-ETA-MM-2O-ASSTO
     * 
     */
    Short getEtaMm2oAsstoObj();

    void setEtaMm2oAsstoObj(Short etaMm2oAsstoObj);

    /**
     * Host Variable GRZ-ETA-AA-3O-ASSTO
     * 
     */
    short getEtaAa3oAssto();

    void setEtaAa3oAssto(short etaAa3oAssto);

    /**
     * Nullable property for GRZ-ETA-AA-3O-ASSTO
     * 
     */
    Short getEtaAa3oAsstoObj();

    void setEtaAa3oAsstoObj(Short etaAa3oAsstoObj);

    /**
     * Host Variable GRZ-ETA-MM-3O-ASSTO
     * 
     */
    short getEtaMm3oAssto();

    void setEtaMm3oAssto(short etaMm3oAssto);

    /**
     * Nullable property for GRZ-ETA-MM-3O-ASSTO
     * 
     */
    Short getEtaMm3oAsstoObj();

    void setEtaMm3oAsstoObj(Short etaMm3oAsstoObj);

    /**
     * Host Variable GRZ-TP-EMIS-PUR
     * 
     */
    char getTpEmisPur();

    void setTpEmisPur(char tpEmisPur);

    /**
     * Nullable property for GRZ-TP-EMIS-PUR
     * 
     */
    Character getTpEmisPurObj();

    void setTpEmisPurObj(Character tpEmisPurObj);

    /**
     * Host Variable GRZ-ETA-A-SCAD
     * 
     */
    int getEtaAScad();

    void setEtaAScad(int etaAScad);

    /**
     * Nullable property for GRZ-ETA-A-SCAD
     * 
     */
    Integer getEtaAScadObj();

    void setEtaAScadObj(Integer etaAScadObj);

    /**
     * Host Variable GRZ-TP-CALC-PRE-PRSTZ
     * 
     */
    String getTpCalcPrePrstz();

    void setTpCalcPrePrstz(String tpCalcPrePrstz);

    /**
     * Nullable property for GRZ-TP-CALC-PRE-PRSTZ
     * 
     */
    String getTpCalcPrePrstzObj();

    void setTpCalcPrePrstzObj(String tpCalcPrePrstzObj);

    /**
     * Host Variable GRZ-TP-PRE
     * 
     */
    char getTpPre();

    void setTpPre(char tpPre);

    /**
     * Nullable property for GRZ-TP-PRE
     * 
     */
    Character getTpPreObj();

    void setTpPreObj(Character tpPreObj);

    /**
     * Host Variable GRZ-TP-DUR
     * 
     */
    String getTpDur();

    void setTpDur(String tpDur);

    /**
     * Nullable property for GRZ-TP-DUR
     * 
     */
    String getTpDurObj();

    void setTpDurObj(String tpDurObj);

    /**
     * Host Variable GRZ-DUR-AA
     * 
     */
    int getDurAa();

    void setDurAa(int durAa);

    /**
     * Nullable property for GRZ-DUR-AA
     * 
     */
    Integer getDurAaObj();

    void setDurAaObj(Integer durAaObj);

    /**
     * Host Variable GRZ-DUR-MM
     * 
     */
    int getDurMm();

    void setDurMm(int durMm);

    /**
     * Nullable property for GRZ-DUR-MM
     * 
     */
    Integer getDurMmObj();

    void setDurMmObj(Integer durMmObj);

    /**
     * Host Variable GRZ-DUR-GG
     * 
     */
    int getDurGg();

    void setDurGg(int durGg);

    /**
     * Nullable property for GRZ-DUR-GG
     * 
     */
    Integer getDurGgObj();

    void setDurGgObj(Integer durGgObj);

    /**
     * Host Variable GRZ-NUM-AA-PAG-PRE
     * 
     */
    int getNumAaPagPre();

    void setNumAaPagPre(int numAaPagPre);

    /**
     * Nullable property for GRZ-NUM-AA-PAG-PRE
     * 
     */
    Integer getNumAaPagPreObj();

    void setNumAaPagPreObj(Integer numAaPagPreObj);

    /**
     * Host Variable GRZ-AA-PAG-PRE-UNI
     * 
     */
    int getAaPagPreUni();

    void setAaPagPreUni(int aaPagPreUni);

    /**
     * Nullable property for GRZ-AA-PAG-PRE-UNI
     * 
     */
    Integer getAaPagPreUniObj();

    void setAaPagPreUniObj(Integer aaPagPreUniObj);

    /**
     * Host Variable GRZ-MM-PAG-PRE-UNI
     * 
     */
    int getMmPagPreUni();

    void setMmPagPreUni(int mmPagPreUni);

    /**
     * Nullable property for GRZ-MM-PAG-PRE-UNI
     * 
     */
    Integer getMmPagPreUniObj();

    void setMmPagPreUniObj(Integer mmPagPreUniObj);

    /**
     * Host Variable GRZ-FRAZ-INI-EROG-REN
     * 
     */
    int getFrazIniErogRen();

    void setFrazIniErogRen(int frazIniErogRen);

    /**
     * Nullable property for GRZ-FRAZ-INI-EROG-REN
     * 
     */
    Integer getFrazIniErogRenObj();

    void setFrazIniErogRenObj(Integer frazIniErogRenObj);

    /**
     * Host Variable GRZ-MM-1O-RAT
     * 
     */
    short getMm1oRat();

    void setMm1oRat(short mm1oRat);

    /**
     * Nullable property for GRZ-MM-1O-RAT
     * 
     */
    Short getMm1oRatObj();

    void setMm1oRatObj(Short mm1oRatObj);

    /**
     * Host Variable GRZ-PC-1O-RAT
     * 
     */
    AfDecimal getPc1oRat();

    void setPc1oRat(AfDecimal pc1oRat);

    /**
     * Nullable property for GRZ-PC-1O-RAT
     * 
     */
    AfDecimal getPc1oRatObj();

    void setPc1oRatObj(AfDecimal pc1oRatObj);

    /**
     * Host Variable GRZ-TP-PRSTZ-ASSTA
     * 
     */
    String getTpPrstzAssta();

    void setTpPrstzAssta(String tpPrstzAssta);

    /**
     * Nullable property for GRZ-TP-PRSTZ-ASSTA
     * 
     */
    String getTpPrstzAsstaObj();

    void setTpPrstzAsstaObj(String tpPrstzAsstaObj);

    /**
     * Host Variable GRZ-DT-END-CARZ-DB
     * 
     */
    String getDtEndCarzDb();

    void setDtEndCarzDb(String dtEndCarzDb);

    /**
     * Nullable property for GRZ-DT-END-CARZ-DB
     * 
     */
    String getDtEndCarzDbObj();

    void setDtEndCarzDbObj(String dtEndCarzDbObj);

    /**
     * Host Variable GRZ-PC-RIP-PRE
     * 
     */
    AfDecimal getPcRipPre();

    void setPcRipPre(AfDecimal pcRipPre);

    /**
     * Nullable property for GRZ-PC-RIP-PRE
     * 
     */
    AfDecimal getPcRipPreObj();

    void setPcRipPreObj(AfDecimal pcRipPreObj);

    /**
     * Host Variable GRZ-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Nullable property for GRZ-COD-FND
     * 
     */
    String getCodFndObj();

    void setCodFndObj(String codFndObj);

    /**
     * Host Variable GRZ-AA-REN-CER
     * 
     */
    String getAaRenCer();

    void setAaRenCer(String aaRenCer);

    /**
     * Nullable property for GRZ-AA-REN-CER
     * 
     */
    String getAaRenCerObj();

    void setAaRenCerObj(String aaRenCerObj);

    /**
     * Host Variable GRZ-PC-REVRSB
     * 
     */
    AfDecimal getPcRevrsb();

    void setPcRevrsb(AfDecimal pcRevrsb);

    /**
     * Nullable property for GRZ-PC-REVRSB
     * 
     */
    AfDecimal getPcRevrsbObj();

    void setPcRevrsbObj(AfDecimal pcRevrsbObj);

    /**
     * Host Variable GRZ-TP-PC-RIP
     * 
     */
    String getTpPcRip();

    void setTpPcRip(String tpPcRip);

    /**
     * Nullable property for GRZ-TP-PC-RIP
     * 
     */
    String getTpPcRipObj();

    void setTpPcRipObj(String tpPcRipObj);

    /**
     * Host Variable GRZ-PC-OPZ
     * 
     */
    AfDecimal getPcOpz();

    void setPcOpz(AfDecimal pcOpz);

    /**
     * Nullable property for GRZ-PC-OPZ
     * 
     */
    AfDecimal getPcOpzObj();

    void setPcOpzObj(AfDecimal pcOpzObj);

    /**
     * Host Variable GRZ-TP-IAS
     * 
     */
    String getTpIas();

    void setTpIas(String tpIas);

    /**
     * Nullable property for GRZ-TP-IAS
     * 
     */
    String getTpIasObj();

    void setTpIasObj(String tpIasObj);

    /**
     * Host Variable GRZ-TP-STAB
     * 
     */
    String getTpStab();

    void setTpStab(String tpStab);

    /**
     * Nullable property for GRZ-TP-STAB
     * 
     */
    String getTpStabObj();

    void setTpStabObj(String tpStabObj);

    /**
     * Host Variable GRZ-TP-ADEG-PRE
     * 
     */
    char getTpAdegPre();

    void setTpAdegPre(char tpAdegPre);

    /**
     * Nullable property for GRZ-TP-ADEG-PRE
     * 
     */
    Character getTpAdegPreObj();

    void setTpAdegPreObj(Character tpAdegPreObj);

    /**
     * Host Variable GRZ-DT-VARZ-TP-IAS-DB
     * 
     */
    String getDtVarzTpIasDb();

    void setDtVarzTpIasDb(String dtVarzTpIasDb);

    /**
     * Nullable property for GRZ-DT-VARZ-TP-IAS-DB
     * 
     */
    String getDtVarzTpIasDbObj();

    void setDtVarzTpIasDbObj(String dtVarzTpIasDbObj);

    /**
     * Host Variable GRZ-FRAZ-DECR-CPT
     * 
     */
    int getFrazDecrCpt();

    void setFrazDecrCpt(int frazDecrCpt);

    /**
     * Nullable property for GRZ-FRAZ-DECR-CPT
     * 
     */
    Integer getFrazDecrCptObj();

    void setFrazDecrCptObj(Integer frazDecrCptObj);

    /**
     * Host Variable GRZ-COD-TRAT-RIASS
     * 
     */
    String getCodTratRiass();

    void setCodTratRiass(String codTratRiass);

    /**
     * Nullable property for GRZ-COD-TRAT-RIASS
     * 
     */
    String getCodTratRiassObj();

    void setCodTratRiassObj(String codTratRiassObj);

    /**
     * Host Variable GRZ-TP-DT-EMIS-RIASS
     * 
     */
    String getTpDtEmisRiass();

    void setTpDtEmisRiass(String tpDtEmisRiass);

    /**
     * Nullable property for GRZ-TP-DT-EMIS-RIASS
     * 
     */
    String getTpDtEmisRiassObj();

    void setTpDtEmisRiassObj(String tpDtEmisRiassObj);

    /**
     * Host Variable GRZ-TP-CESS-RIASS
     * 
     */
    String getTpCessRiass();

    void setTpCessRiass(String tpCessRiass);

    /**
     * Nullable property for GRZ-TP-CESS-RIASS
     * 
     */
    String getTpCessRiassObj();

    void setTpCessRiassObj(String tpCessRiassObj);

    /**
     * Host Variable GRZ-DS-RIGA
     * 
     */
    long getDsRiga();

    void setDsRiga(long dsRiga);

    /**
     * Host Variable GRZ-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable GRZ-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable GRZ-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable GRZ-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable GRZ-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable GRZ-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable GRZ-AA-STAB
     * 
     */
    int getAaStab();

    void setAaStab(int aaStab);

    /**
     * Nullable property for GRZ-AA-STAB
     * 
     */
    Integer getAaStabObj();

    void setAaStabObj(Integer aaStabObj);

    /**
     * Host Variable GRZ-TS-STAB-LIMITATA
     * 
     */
    AfDecimal getTsStabLimitata();

    void setTsStabLimitata(AfDecimal tsStabLimitata);

    /**
     * Nullable property for GRZ-TS-STAB-LIMITATA
     * 
     */
    AfDecimal getTsStabLimitataObj();

    void setTsStabLimitataObj(AfDecimal tsStabLimitataObj);

    /**
     * Host Variable GRZ-DT-PRESC-DB
     * 
     */
    String getDtPrescDb();

    void setDtPrescDb(String dtPrescDb);

    /**
     * Nullable property for GRZ-DT-PRESC-DB
     * 
     */
    String getDtPrescDbObj();

    void setDtPrescDbObj(String dtPrescDbObj);

    /**
     * Host Variable GRZ-RSH-INVST
     * 
     */
    char getRshInvst();

    void setRshInvst(char rshInvst);

    /**
     * Nullable property for GRZ-RSH-INVST
     * 
     */
    Character getRshInvstObj();

    void setRshInvstObj(Character rshInvstObj);

    /**
     * Host Variable GRZ-TP-RAMO-BILA
     * 
     */
    String getTpRamoBila();

    void setTpRamoBila(String tpRamoBila);

    /**
     * Host Variable STB-ID-STAT-OGG-BUS
     * 
     */
    int getStbIdStatOggBus();

    void setStbIdStatOggBus(int stbIdStatOggBus);

    /**
     * Host Variable STB-ID-OGG
     * 
     */
    int getStbIdOgg();

    void setStbIdOgg(int stbIdOgg);

    /**
     * Host Variable STB-TP-OGG
     * 
     */
    String getStbTpOgg();

    void setStbTpOgg(String stbTpOgg);

    /**
     * Host Variable STB-ID-MOVI-CRZ
     * 
     */
    int getStbIdMoviCrz();

    void setStbIdMoviCrz(int stbIdMoviCrz);

    /**
     * Host Variable STB-ID-MOVI-CHIU
     * 
     */
    int getStbIdMoviChiu();

    void setStbIdMoviChiu(int stbIdMoviChiu);

    /**
     * Nullable property for STB-ID-MOVI-CHIU
     * 
     */
    Integer getStbIdMoviChiuObj();

    void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj);

    /**
     * Host Variable STB-DT-INI-EFF-DB
     * 
     */
    String getStbDtIniEffDb();

    void setStbDtIniEffDb(String stbDtIniEffDb);

    /**
     * Host Variable STB-DT-END-EFF-DB
     * 
     */
    String getStbDtEndEffDb();

    void setStbDtEndEffDb(String stbDtEndEffDb);

    /**
     * Host Variable STB-COD-COMP-ANIA
     * 
     */
    int getStbCodCompAnia();

    void setStbCodCompAnia(int stbCodCompAnia);

    /**
     * Host Variable STB-TP-STAT-BUS
     * 
     */
    String getStbTpStatBus();

    void setStbTpStatBus(String stbTpStatBus);

    /**
     * Host Variable STB-TP-CAUS
     * 
     */
    String getStbTpCaus();

    void setStbTpCaus(String stbTpCaus);

    /**
     * Host Variable STB-DS-RIGA
     * 
     */
    long getStbDsRiga();

    void setStbDsRiga(long stbDsRiga);

    /**
     * Host Variable STB-DS-OPER-SQL
     * 
     */
    char getStbDsOperSql();

    void setStbDsOperSql(char stbDsOperSql);

    /**
     * Host Variable STB-DS-VER
     * 
     */
    int getStbDsVer();

    void setStbDsVer(int stbDsVer);

    /**
     * Host Variable STB-DS-TS-INI-CPTZ
     * 
     */
    long getStbDsTsIniCptz();

    void setStbDsTsIniCptz(long stbDsTsIniCptz);

    /**
     * Host Variable STB-DS-TS-END-CPTZ
     * 
     */
    long getStbDsTsEndCptz();

    void setStbDsTsEndCptz(long stbDsTsEndCptz);

    /**
     * Host Variable STB-DS-UTENTE
     * 
     */
    String getStbDsUtente();

    void setStbDsUtente(String stbDsUtente);

    /**
     * Host Variable STB-DS-STATO-ELAB
     * 
     */
    char getStbDsStatoElab();

    void setStbDsStatoElab(char stbDsStatoElab);
};
