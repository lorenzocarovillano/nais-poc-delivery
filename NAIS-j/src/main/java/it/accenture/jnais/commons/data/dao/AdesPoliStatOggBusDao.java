package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAdesPoliStatOggBus;

/**
 * Data Access Object(DAO) for tables [ADES, POLI, STAT_OGG_BUS]
 * 
 */
public class AdesPoliStatOggBusDao extends BaseSqlDao<IAdesPoliStatOggBus> {

    private final IRowMapper<IAdesPoliStatOggBus> selectRecRm = buildNamedRowMapper(IAdesPoliStatOggBus.class, "adeIdAdes", "adeIdPoli", "adeIdMoviCrz", "adeIdMoviChiuObj", "adeDtIniEffDb", "adeDtEndEffDb", "adeIbPrevObj", "adeIbOggObj", "adeCodCompAnia", "adeDtDecorDbObj", "adeDtScadDbObj", "adeEtaAScadObj", "adeDurAaObj", "adeDurMmObj", "adeDurGgObj", "adeTpRgmFisc", "adeTpRiatObj", "adeTpModPagTit", "adeTpIasObj", "adeDtVarzTpIasDbObj", "adePreNetIndObj", "adePreLrdIndObj", "adeRatLrdIndObj", "adePrstzIniIndObj", "adeFlCoincAsstoObj", "adeIbDfltObj", "adeModCalcObj", "adeTpFntCnbtvaObj", "adeImpAzObj", "adeImpAderObj", "adeImpTfrObj", "adeImpVoloObj", "adePcAzObj", "adePcAderObj", "adePcTfrObj", "adePcVoloObj", "adeDtNovaRgmFiscDbObj", "adeFlAttivObj", "adeImpRecRitVisObj", "adeImpRecRitAccObj", "adeFlVarzStatTbgcObj", "adeFlProvzaMigrazObj", "adeImpbVisDaRecObj", "adeDtDecorPrestBanDbObj", "adeDtEffVarzStatTDbObj", "adeDsRiga", "adeDsOperSql", "adeDsVer", "adeDsTsIniCptz", "adeDsTsEndCptz", "adeDsUtente", "adeDsStatoElab", "adeCumCnbtCapObj", "adeImpGarCnbtObj", "adeDtUltConsCnbtDbObj", "adeIdenIscFndObj", "adeNumRatPianObj", "adeDtPrescDbObj", "adeConcsPrestObj", "polIdPoli", "polIdMoviCrz", "polIdMoviChiuObj", "polIbOggObj", "polIbProp", "polDtPropDbObj", "polDtIniEffDb", "polDtEndEffDb", "polCodCompAnia", "polDtDecorDb", "polDtEmisDb", "polTpPoli", "polDurAaObj", "polDurMmObj", "polDtScadDbObj", "polCodProd", "polDtIniVldtProdDb", "polCodConvObj", "polCodRamoObj", "polDtIniVldtConvDbObj", "polDtApplzConvDbObj", "polTpFrmAssva", "polTpRgmFiscObj", "polFlEstasObj", "polFlRshComunObj", "polFlRshComunCondObj", "polTpLivGenzTit", "polFlCopFinanzObj", "polTpApplzDirObj", "polSpeMedObj", "polDirEmisObj", "polDir1oVersObj", "polDirVersAggObj", "polCodDvsObj", "polFlFntAzObj", "polFlFntAderObj", "polFlFntTfrObj", "polFlFntVoloObj", "polTpOpzAScadObj", "polAaDiffProrDfltObj", "polFlVerProdObj", "polDurGgObj", "polDirQuietObj", "polTpPtfEstnoObj", "polFlCumPreCntrObj", "polFlAmmbMoviObj", "polConvGecoObj", "polDsRiga", "polDsOperSql", "polDsVer", "polDsTsIniCptz", "polDsTsEndCptz", "polDsUtente", "polDsStatoElab", "polFlScudoFiscObj", "polFlTrasfeObj", "polFlTfrStrcObj", "polDtPrescDbObj", "polCodConvAggObj", "polSubcatProdObj", "polFlQuestAdegzAssObj", "polCodTpaObj", "polIdAccCommObj", "polFlPoliCpiPrObj", "polFlPoliBundlingObj", "polIndPoliPrinCollObj", "polFlVndBundleObj", "polIbBsObj", "polFlPoliIfpObj", "stbIdStatOggBus", "stbIdOgg", "stbTpOgg", "stbIdMoviCrz", "stbIdMoviChiuObj", "stbDtIniEffDb", "stbDtEndEffDb", "stbCodCompAnia", "stbTpStatBus", "stbTpCaus", "stbDsRiga", "stbDsOperSql", "stbDsVer", "stbDsTsIniCptz", "stbDsTsEndCptz", "stbDsUtente", "stbDsStatoElab");

    public AdesPoliStatOggBusDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAdesPoliStatOggBus> getToClass() {
        return IAdesPoliStatOggBus.class;
    }

    public IAdesPoliStatOggBus selectRec(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
        return buildQuery("selectRec").bind(iAdesPoliStatOggBus).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBus);
    }

    public IAdesPoliStatOggBus selectRec1(IAdesPoliStatOggBus iAdesPoliStatOggBus) {
        return buildQuery("selectRec1").bind(iAdesPoliStatOggBus).rowMapper(selectRecRm).singleResult(iAdesPoliStatOggBus);
    }
}
