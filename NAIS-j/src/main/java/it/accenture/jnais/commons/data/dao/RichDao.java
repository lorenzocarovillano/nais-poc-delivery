package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IRich;

/**
 * Data Access Object(DAO) for table [RICH]
 * 
 */
public class RichDao extends BaseSqlDao<IRich> {

    private Cursor curRicIdrStd;
    private Cursor curRicIdrMf;
    private Cursor curRicIdrTm;
    private Cursor curRicIdrMfTm;
    private Cursor cIdoNstRic;
    private Cursor cNst11;
    private final IRowMapper<IRich> selectRecRm = buildNamedRowMapper(IRich.class, "ricIdRich", "codCompAnia", "tpRich", "ricCodMacrofunct", "ricTpMovi", "ibRichObj", "dtEffDb", "dtRgstrzRichDb", "dtPervRichDb", "dtEsecRichDb", "tsEffEsecRichObj", "idOggObj", "tpOggObj", "ibPoliObj", "ibAdesObj", "ibGarObj", "ibTrchDiGarObj", "ricIdBatchObj", "idJobObj", "flSimulazioneObj", "keyOrdinamentoVcharObj", "idRichCollgObj", "tpRamoBilaObj", "tpFrmAssvaObj", "tpCalcRisObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "ramoBilaObj");

    public RichDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IRich> getToClass() {
        return IRich.class;
    }

    public DbAccessStatus openCurRicIdrStd(IRich iRich) {
        curRicIdrStd = buildQuery("openCurRicIdrStd").bind(iRich).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurRicIdrMf(IRich iRich) {
        curRicIdrMf = buildQuery("openCurRicIdrMf").bind(iRich).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurRicIdrTm(IRich iRich) {
        curRicIdrTm = buildQuery("openCurRicIdrTm").bind(iRich).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurRicIdrMfTm(IRich iRich) {
        curRicIdrMfTm = buildQuery("openCurRicIdrMfTm").bind(iRich).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public IRich selectRec(IRich iRich) {
        return buildQuery("selectRec").bind(iRich).rowMapper(selectRecRm).singleResult(iRich);
    }

    public IRich selectRec1(IRich iRich) {
        return buildQuery("selectRec1").bind(iRich).rowMapper(selectRecRm).singleResult(iRich);
    }

    public IRich selectRec2(IRich iRich) {
        return buildQuery("selectRec2").bind(iRich).rowMapper(selectRecRm).singleResult(iRich);
    }

    public IRich selectRec3(IRich iRich) {
        return buildQuery("selectRec3").bind(iRich).rowMapper(selectRecRm).singleResult(iRich);
    }

    public IRich selectByRicIdRich(int ricIdRich, IRich iRich) {
        return buildQuery("selectByRicIdRich").bind("ricIdRich", ricIdRich).rowMapper(selectRecRm).singleResult(iRich);
    }

    public DbAccessStatus updateRec(IRich iRich) {
        return buildQuery("updateRec").bind(iRich).executeUpdate();
    }

    public DbAccessStatus updateRec1(long tsEffEsecRich, int idRich) {
        return buildQuery("updateRec1").bind("tsEffEsecRich", tsEffEsecRich).bind("idRich", idRich).executeUpdate();
    }

    public DbAccessStatus updateRec2(char iabv0002StateCurrent, int ricIdRich) {
        return buildQuery("updateRec2").bind("iabv0002StateCurrent", String.valueOf(iabv0002StateCurrent)).bind("ricIdRich", ricIdRich).executeUpdate();
    }

    public DbAccessStatus updateRec3(IRich iRich) {
        return buildQuery("updateRec3").bind(iRich).executeUpdate();
    }

    public DbAccessStatus closeCurRicIdrStd() {
        return closeCursor(curRicIdrStd);
    }

    public DbAccessStatus closeCurRicIdrMf() {
        return closeCursor(curRicIdrMf);
    }

    public DbAccessStatus closeCurRicIdrTm() {
        return closeCursor(curRicIdrTm);
    }

    public DbAccessStatus closeCurRicIdrMfTm() {
        return closeCursor(curRicIdrMfTm);
    }

    public IRich fetchCurRicIdrStd(IRich iRich) {
        return fetch(curRicIdrStd, iRich, selectRecRm);
    }

    public IRich fetchCurRicIdrMf(IRich iRich) {
        return fetch(curRicIdrMf, iRich, selectRecRm);
    }

    public IRich fetchCurRicIdrTm(IRich iRich) {
        return fetch(curRicIdrTm, iRich, selectRecRm);
    }

    public IRich fetchCurRicIdrMfTm(IRich iRich) {
        return fetch(curRicIdrMfTm, iRich, selectRecRm);
    }

    public IRich selectByRicIdRich1(int ricIdRich, IRich iRich) {
        return buildQuery("selectByRicIdRich1").bind("ricIdRich", ricIdRich).rowMapper(selectRecRm).singleResult(iRich);
    }

    public DbAccessStatus updateRec4(IRich iRich) {
        return buildQuery("updateRec4").bind(iRich).executeUpdate();
    }

    public DbAccessStatus deleteByRicIdRich(int ricIdRich) {
        return buildQuery("deleteByRicIdRich").bind("ricIdRich", ricIdRich).executeDelete();
    }

    public IRich selectRec4(int ricIdOgg, String ricTpOgg, int idsv0003CodiceCompagniaAnia, IRich iRich) {
        return buildQuery("selectRec4").bind("ricIdOgg", ricIdOgg).bind("ricTpOgg", ricTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectRecRm).singleResult(iRich);
    }

    public DbAccessStatus openCIdoNstRic(int ricIdOgg, String ricTpOgg, int idsv0003CodiceCompagniaAnia) {
        cIdoNstRic = buildQuery("openCIdoNstRic").bind("ricIdOgg", ricIdOgg).bind("ricTpOgg", ricTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoNstRic() {
        return closeCursor(cIdoNstRic);
    }

    public IRich fetchCIdoNstRic(IRich iRich) {
        return fetch(cIdoNstRic, iRich, selectRecRm);
    }

    public DbAccessStatus openCNst11(IRich iRich) {
        cNst11 = buildQuery("openCNst11").bind(iRich).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst11() {
        return closeCursor(cNst11);
    }

    public IRich fetchCNst11(IRich iRich) {
        return fetch(cNst11, iRich, selectRecRm);
    }

    public DbAccessStatus insertRec(IRich iRich) {
        return buildQuery("insertRec").bind(iRich).executeInsert();
    }

    public DbAccessStatus updateRec5(IRich iRich) {
        return buildQuery("updateRec5").bind(iRich).executeUpdate();
    }

    public DbAccessStatus updateRec6(IRich iRich) {
        return buildQuery("updateRec6").bind(iRich).executeUpdate();
    }

    public DbAccessStatus updateRec7(IRich iRich) {
        return buildQuery("updateRec7").bind(iRich).executeUpdate();
    }

    public DbAccessStatus updateRec8(IRich iRich) {
        return buildQuery("updateRec8").bind(iRich).executeUpdate();
    }
}
