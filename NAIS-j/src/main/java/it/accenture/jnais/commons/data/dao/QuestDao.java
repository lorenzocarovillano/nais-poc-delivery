package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IQuest;

/**
 * Data Access Object(DAO) for table [QUEST]
 * 
 */
public class QuestDao extends BaseSqlDao<IQuest> {

    private Cursor cIdUpdEffQue;
    private Cursor cIdpEffQue;
    private Cursor cIdpCpzQue;
    private final IRowMapper<IQuest> selectByQueDsRigaRm = buildNamedRowMapper(IQuest.class, "idQuest", "idRappAna", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codQuest", "tpQuest", "flVstMedObj", "flStatBuonSalObj", "queDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "tpAdegzObj");

    public QuestDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IQuest> getToClass() {
        return IQuest.class;
    }

    public IQuest selectByQueDsRiga(long queDsRiga, IQuest iQuest) {
        return buildQuery("selectByQueDsRiga").bind("queDsRiga", queDsRiga).rowMapper(selectByQueDsRigaRm).singleResult(iQuest);
    }

    public DbAccessStatus insertRec(IQuest iQuest) {
        return buildQuery("insertRec").bind(iQuest).executeInsert();
    }

    public DbAccessStatus updateRec(IQuest iQuest) {
        return buildQuery("updateRec").bind(iQuest).executeUpdate();
    }

    public DbAccessStatus deleteByQueDsRiga(long queDsRiga) {
        return buildQuery("deleteByQueDsRiga").bind("queDsRiga", queDsRiga).executeDelete();
    }

    public IQuest selectRec(int queIdQuest, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IQuest iQuest) {
        return buildQuery("selectRec").bind("queIdQuest", queIdQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByQueDsRigaRm).singleResult(iQuest);
    }

    public DbAccessStatus updateRec1(IQuest iQuest) {
        return buildQuery("updateRec1").bind(iQuest).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffQue(int queIdQuest, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffQue = buildQuery("openCIdUpdEffQue").bind("queIdQuest", queIdQuest).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffQue() {
        return closeCursor(cIdUpdEffQue);
    }

    public IQuest fetchCIdUpdEffQue(IQuest iQuest) {
        return fetch(cIdUpdEffQue, iQuest, selectByQueDsRigaRm);
    }

    public IQuest selectRec1(int queIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IQuest iQuest) {
        return buildQuery("selectRec1").bind("queIdRappAna", queIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByQueDsRigaRm).singleResult(iQuest);
    }

    public DbAccessStatus openCIdpEffQue(int queIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffQue = buildQuery("openCIdpEffQue").bind("queIdRappAna", queIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffQue() {
        return closeCursor(cIdpEffQue);
    }

    public IQuest fetchCIdpEffQue(IQuest iQuest) {
        return fetch(cIdpEffQue, iQuest, selectByQueDsRigaRm);
    }

    public IQuest selectRec2(int queIdQuest, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IQuest iQuest) {
        return buildQuery("selectRec2").bind("queIdQuest", queIdQuest).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByQueDsRigaRm).singleResult(iQuest);
    }

    public IQuest selectRec3(int queIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IQuest iQuest) {
        return buildQuery("selectRec3").bind("queIdRappAna", queIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByQueDsRigaRm).singleResult(iQuest);
    }

    public DbAccessStatus openCIdpCpzQue(int queIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzQue = buildQuery("openCIdpCpzQue").bind("queIdRappAna", queIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzQue() {
        return closeCursor(cIdpCpzQue);
    }

    public IQuest fetchCIdpCpzQue(IQuest iQuest) {
        return fetch(cIdpCpzQue, iQuest, selectByQueDsRigaRm);
    }
}
