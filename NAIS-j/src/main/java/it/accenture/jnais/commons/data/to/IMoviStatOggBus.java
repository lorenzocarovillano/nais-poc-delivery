package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [MOVI, STAT_OGG_BUS]
 * 
 */
public interface IMoviStatOggBus extends BaseSqlTo {

    /**
     * Host Variable LDBV2631-ID-OGG
     * 
     */
    int getLdbv2631IdOgg();

    void setLdbv2631IdOgg(int ldbv2631IdOgg);

    /**
     * Host Variable LDBV2631-TP-OGG
     * 
     */
    String getLdbv2631TpOgg();

    void setLdbv2631TpOgg(String ldbv2631TpOgg);

    /**
     * Host Variable LDBV2631-TP-MOV1
     * 
     */
    int getLdbv2631TpMov1();

    void setLdbv2631TpMov1(int ldbv2631TpMov1);

    /**
     * Host Variable LDBV2631-TP-MOV2
     * 
     */
    int getLdbv2631TpMov2();

    void setLdbv2631TpMov2(int ldbv2631TpMov2);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable STB-ID-STAT-OGG-BUS
     * 
     */
    int getIdStatOggBus();

    void setIdStatOggBus(int idStatOggBus);

    /**
     * Host Variable STB-ID-OGG
     * 
     */
    int getIdOgg();

    void setIdOgg(int idOgg);

    /**
     * Host Variable STB-TP-OGG
     * 
     */
    String getTpOgg();

    void setTpOgg(String tpOgg);

    /**
     * Host Variable STB-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable STB-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for STB-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable STB-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable STB-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable STB-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable STB-TP-STAT-BUS
     * 
     */
    String getTpStatBus();

    void setTpStatBus(String tpStatBus);

    /**
     * Host Variable STB-TP-CAUS
     * 
     */
    String getTpCaus();

    void setTpCaus(String tpCaus);

    /**
     * Host Variable STB-DS-RIGA
     * 
     */
    long getDsRiga();

    void setDsRiga(long dsRiga);

    /**
     * Host Variable STB-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable STB-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable STB-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable STB-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable STB-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable STB-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
