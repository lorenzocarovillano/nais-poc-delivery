package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IImpstBollo;

/**
 * Data Access Object(DAO) for table [IMPST_BOLLO]
 * 
 */
public class ImpstBolloDao extends BaseSqlDao<IImpstBollo> {

    private Cursor cIdUpdEffP58;
    private Cursor cIdpEffP58;
    private Cursor cIbsEffP580;
    private Cursor cIdpCpzP58;
    private Cursor cIbsCpzP580;
    private Cursor cEff45;
    private Cursor cCpz45;
    private final IRowMapper<IImpstBollo> selectByP58DsRigaRm = buildNamedRowMapper(IImpstBollo.class, "idImpstBollo", "codCompAnia", "idPoli", "ibPoli", "codFiscObj", "codPartIvaObj", "idRappAna", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "dtIniCalcDb", "dtEndCalcDb", "tpCausBollo", "impstBolloDettC", "impstBolloDettVObj", "impstBolloTotVObj", "impstBolloTotR", "p58DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");
    private final IRowMapper<IImpstBollo> selectRec6Rm = buildNamedRowMapper(IImpstBollo.class, "dettC", "dettVObj", "totVObj", "totR");

    public ImpstBolloDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IImpstBollo> getToClass() {
        return IImpstBollo.class;
    }

    public IImpstBollo selectByP58DsRiga(long p58DsRiga, IImpstBollo iImpstBollo) {
        return buildQuery("selectByP58DsRiga").bind("p58DsRiga", p58DsRiga).rowMapper(selectByP58DsRigaRm).singleResult(iImpstBollo);
    }

    public DbAccessStatus insertRec(IImpstBollo iImpstBollo) {
        return buildQuery("insertRec").bind(iImpstBollo).executeInsert();
    }

    public DbAccessStatus updateRec(IImpstBollo iImpstBollo) {
        return buildQuery("updateRec").bind(iImpstBollo).executeUpdate();
    }

    public DbAccessStatus deleteByP58DsRiga(long p58DsRiga) {
        return buildQuery("deleteByP58DsRiga").bind("p58DsRiga", p58DsRiga).executeDelete();
    }

    public IImpstBollo selectRec(int p58IdImpstBollo, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IImpstBollo iImpstBollo) {
        return buildQuery("selectRec").bind("p58IdImpstBollo", p58IdImpstBollo).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP58DsRigaRm).singleResult(iImpstBollo);
    }

    public DbAccessStatus updateRec1(IImpstBollo iImpstBollo) {
        return buildQuery("updateRec1").bind(iImpstBollo).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffP58(int p58IdImpstBollo, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffP58 = buildQuery("openCIdUpdEffP58").bind("p58IdImpstBollo", p58IdImpstBollo).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffP58() {
        return closeCursor(cIdUpdEffP58);
    }

    public IImpstBollo fetchCIdUpdEffP58(IImpstBollo iImpstBollo) {
        return fetch(cIdUpdEffP58, iImpstBollo, selectByP58DsRigaRm);
    }

    public IImpstBollo selectRec1(int p58IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IImpstBollo iImpstBollo) {
        return buildQuery("selectRec1").bind("p58IdRappAna", p58IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP58DsRigaRm).singleResult(iImpstBollo);
    }

    public DbAccessStatus openCIdpEffP58(int p58IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffP58 = buildQuery("openCIdpEffP58").bind("p58IdRappAna", p58IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffP58() {
        return closeCursor(cIdpEffP58);
    }

    public IImpstBollo fetchCIdpEffP58(IImpstBollo iImpstBollo) {
        return fetch(cIdpEffP58, iImpstBollo, selectByP58DsRigaRm);
    }

    public IImpstBollo selectRec2(String p58IbPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IImpstBollo iImpstBollo) {
        return buildQuery("selectRec2").bind("p58IbPoli", p58IbPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP58DsRigaRm).singleResult(iImpstBollo);
    }

    public DbAccessStatus openCIbsEffP580(String p58IbPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffP580 = buildQuery("openCIbsEffP580").bind("p58IbPoli", p58IbPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsEffP580() {
        return closeCursor(cIbsEffP580);
    }

    public IImpstBollo fetchCIbsEffP580(IImpstBollo iImpstBollo) {
        return fetch(cIbsEffP580, iImpstBollo, selectByP58DsRigaRm);
    }

    public IImpstBollo selectRec3(int p58IdImpstBollo, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IImpstBollo iImpstBollo) {
        return buildQuery("selectRec3").bind("p58IdImpstBollo", p58IdImpstBollo).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP58DsRigaRm).singleResult(iImpstBollo);
    }

    public IImpstBollo selectRec4(int p58IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IImpstBollo iImpstBollo) {
        return buildQuery("selectRec4").bind("p58IdRappAna", p58IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP58DsRigaRm).singleResult(iImpstBollo);
    }

    public DbAccessStatus openCIdpCpzP58(int p58IdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzP58 = buildQuery("openCIdpCpzP58").bind("p58IdRappAna", p58IdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzP58() {
        return closeCursor(cIdpCpzP58);
    }

    public IImpstBollo fetchCIdpCpzP58(IImpstBollo iImpstBollo) {
        return fetch(cIdpCpzP58, iImpstBollo, selectByP58DsRigaRm);
    }

    public IImpstBollo selectRec5(String p58IbPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IImpstBollo iImpstBollo) {
        return buildQuery("selectRec5").bind("p58IbPoli", p58IbPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP58DsRigaRm).singleResult(iImpstBollo);
    }

    public DbAccessStatus openCIbsCpzP580(String p58IbPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzP580 = buildQuery("openCIbsCpzP580").bind("p58IbPoli", p58IbPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsCpzP580() {
        return closeCursor(cIbsCpzP580);
    }

    public IImpstBollo fetchCIbsCpzP580(IImpstBollo iImpstBollo) {
        return fetch(cIbsCpzP580, iImpstBollo, selectByP58DsRigaRm);
    }

    public IImpstBollo selectRec6(IImpstBollo iImpstBollo) {
        return buildQuery("selectRec6").bind(iImpstBollo).rowMapper(selectRec6Rm).singleResult(iImpstBollo);
    }

    public IImpstBollo selectRec7(IImpstBollo iImpstBollo) {
        return buildQuery("selectRec7").bind(iImpstBollo).rowMapper(selectRec6Rm).singleResult(iImpstBollo);
    }

    public IImpstBollo selectRec8(IImpstBollo iImpstBollo) {
        return buildQuery("selectRec8").bind(iImpstBollo).rowMapper(selectRec6Rm).singleResult(iImpstBollo);
    }

    public IImpstBollo selectRec9(IImpstBollo iImpstBollo) {
        return buildQuery("selectRec9").bind(iImpstBollo).rowMapper(selectRec6Rm).singleResult(iImpstBollo);
    }

    public IImpstBollo selectRec10(int p58IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IImpstBollo iImpstBollo) {
        return buildQuery("selectRec10").bind("p58IdPoli", p58IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP58DsRigaRm).singleResult(iImpstBollo);
    }

    public DbAccessStatus openCEff45(int p58IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff45 = buildQuery("openCEff45").bind("p58IdPoli", p58IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff45() {
        return closeCursor(cEff45);
    }

    public IImpstBollo fetchCEff45(IImpstBollo iImpstBollo) {
        return fetch(cEff45, iImpstBollo, selectByP58DsRigaRm);
    }

    public IImpstBollo selectRec11(int p58IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IImpstBollo iImpstBollo) {
        return buildQuery("selectRec11").bind("p58IdPoli", p58IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP58DsRigaRm).singleResult(iImpstBollo);
    }

    public DbAccessStatus openCCpz45(int p58IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cCpz45 = buildQuery("openCCpz45").bind("p58IdPoli", p58IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz45() {
        return closeCursor(cCpz45);
    }

    public IImpstBollo fetchCCpz45(IImpstBollo iImpstBollo) {
        return fetch(cCpz45, iImpstBollo, selectByP58DsRigaRm);
    }
}
