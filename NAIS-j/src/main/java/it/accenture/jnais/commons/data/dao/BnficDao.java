package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBnfic;

/**
 * Data Access Object(DAO) for table [BNFIC]
 * 
 */
public class BnficDao extends BaseSqlDao<IBnfic> {

    private Cursor cIdUpdEffBep;
    private Cursor cIdpEffBep;
    private Cursor cIdpCpzBep;
    private final IRowMapper<IBnfic> selectByBepDsRigaRm = buildNamedRowMapper(IBnfic.class, "idBnfic", "idRappAna", "idBnficrObj", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codBnficObj", "tpIndBnficr", "codBnficrObj", "descBnficrVcharObj", "pcDelBnficrObj", "flEseObj", "flIrrevObj", "flDfltObj", "esrcnAttvtImprsObj", "flBnficrColl", "bepDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "tpNormalBnficObj");

    public BnficDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBnfic> getToClass() {
        return IBnfic.class;
    }

    public IBnfic selectByBepDsRiga(long bepDsRiga, IBnfic iBnfic) {
        return buildQuery("selectByBepDsRiga").bind("bepDsRiga", bepDsRiga).rowMapper(selectByBepDsRigaRm).singleResult(iBnfic);
    }

    public DbAccessStatus insertRec(IBnfic iBnfic) {
        return buildQuery("insertRec").bind(iBnfic).executeInsert();
    }

    public DbAccessStatus updateRec(IBnfic iBnfic) {
        return buildQuery("updateRec").bind(iBnfic).executeUpdate();
    }

    public DbAccessStatus deleteByBepDsRiga(long bepDsRiga) {
        return buildQuery("deleteByBepDsRiga").bind("bepDsRiga", bepDsRiga).executeDelete();
    }

    public IBnfic selectRec(int bepIdBnfic, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IBnfic iBnfic) {
        return buildQuery("selectRec").bind("bepIdBnfic", bepIdBnfic).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByBepDsRigaRm).singleResult(iBnfic);
    }

    public DbAccessStatus updateRec1(IBnfic iBnfic) {
        return buildQuery("updateRec1").bind(iBnfic).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffBep(int bepIdBnfic, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffBep = buildQuery("openCIdUpdEffBep").bind("bepIdBnfic", bepIdBnfic).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffBep() {
        return closeCursor(cIdUpdEffBep);
    }

    public IBnfic fetchCIdUpdEffBep(IBnfic iBnfic) {
        return fetch(cIdUpdEffBep, iBnfic, selectByBepDsRigaRm);
    }

    public IBnfic selectRec1(int bepIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IBnfic iBnfic) {
        return buildQuery("selectRec1").bind("bepIdRappAna", bepIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByBepDsRigaRm).singleResult(iBnfic);
    }

    public DbAccessStatus openCIdpEffBep(int bepIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffBep = buildQuery("openCIdpEffBep").bind("bepIdRappAna", bepIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffBep() {
        return closeCursor(cIdpEffBep);
    }

    public IBnfic fetchCIdpEffBep(IBnfic iBnfic) {
        return fetch(cIdpEffBep, iBnfic, selectByBepDsRigaRm);
    }

    public IBnfic selectRec2(int bepIdBnfic, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IBnfic iBnfic) {
        return buildQuery("selectRec2").bind("bepIdBnfic", bepIdBnfic).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByBepDsRigaRm).singleResult(iBnfic);
    }

    public IBnfic selectRec3(int bepIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IBnfic iBnfic) {
        return buildQuery("selectRec3").bind("bepIdRappAna", bepIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByBepDsRigaRm).singleResult(iBnfic);
    }

    public DbAccessStatus openCIdpCpzBep(int bepIdRappAna, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzBep = buildQuery("openCIdpCpzBep").bind("bepIdRappAna", bepIdRappAna).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzBep() {
        return closeCursor(cIdpCpzBep);
    }

    public IBnfic fetchCIdpCpzBep(IBnfic iBnfic) {
        return fetch(cIdpCpzBep, iBnfic, selectByBepDsRigaRm);
    }
}
