package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [NUM_ADE_GAR_TRA]
 * 
 */
public interface INumAdeGarTra extends BaseSqlTo {

    /**
     * Host Variable NUM-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable NUM-ID-OGG
     * 
     */
    int getIdOgg();

    void setIdOgg(int idOgg);

    /**
     * Host Variable NUM-TP-OGG
     * 
     */
    String getTpOgg();

    void setTpOgg(String tpOgg);

    /**
     * Host Variable NUM-ULT-NUM-LIN
     * 
     */
    int getUltNumLin();

    void setUltNumLin(int ultNumLin);

    /**
     * Nullable property for NUM-ULT-NUM-LIN
     * 
     */
    Integer getUltNumLinObj();

    void setUltNumLinObj(Integer ultNumLinObj);

    /**
     * Host Variable NUM-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable NUM-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable NUM-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable NUM-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable NUM-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
