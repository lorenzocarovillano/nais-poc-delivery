package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [LIQ]
 * 
 */
public interface ILiq extends BaseSqlTo {

    /**
     * Host Variable LQU-ID-OGG
     * 
     */
    int getLquIdOgg();

    void setLquIdOgg(int lquIdOgg);

    /**
     * Host Variable LQU-TP-OGG
     * 
     */
    String getLquTpOgg();

    void setLquTpOgg(String lquTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LQU-ID-MOVI-CRZ
     * 
     */
    int getLquIdMoviCrz();

    void setLquIdMoviCrz(int lquIdMoviCrz);

    /**
     * Host Variable LQU-ID-LIQ
     * 
     */
    int getIdLiq();

    void setIdLiq(int idLiq);

    /**
     * Host Variable LQU-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for LQU-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable LQU-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable LQU-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable LQU-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable LQU-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for LQU-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable LQU-TP-LIQ
     * 
     */
    String getTpLiq();

    void setTpLiq(String tpLiq);

    /**
     * Host Variable LQU-DESC-CAU-EVE-SIN-VCHAR
     * 
     */
    String getDescCauEveSinVchar();

    void setDescCauEveSinVchar(String descCauEveSinVchar);

    /**
     * Nullable property for LQU-DESC-CAU-EVE-SIN-VCHAR
     * 
     */
    String getDescCauEveSinVcharObj();

    void setDescCauEveSinVcharObj(String descCauEveSinVcharObj);

    /**
     * Host Variable LQU-COD-CAU-SIN
     * 
     */
    String getCodCauSin();

    void setCodCauSin(String codCauSin);

    /**
     * Nullable property for LQU-COD-CAU-SIN
     * 
     */
    String getCodCauSinObj();

    void setCodCauSinObj(String codCauSinObj);

    /**
     * Host Variable LQU-COD-SIN-CATSTRF
     * 
     */
    String getCodSinCatstrf();

    void setCodSinCatstrf(String codSinCatstrf);

    /**
     * Nullable property for LQU-COD-SIN-CATSTRF
     * 
     */
    String getCodSinCatstrfObj();

    void setCodSinCatstrfObj(String codSinCatstrfObj);

    /**
     * Host Variable LQU-DT-MOR-DB
     * 
     */
    String getDtMorDb();

    void setDtMorDb(String dtMorDb);

    /**
     * Nullable property for LQU-DT-MOR-DB
     * 
     */
    String getDtMorDbObj();

    void setDtMorDbObj(String dtMorDbObj);

    /**
     * Host Variable LQU-DT-DEN-DB
     * 
     */
    String getDtDenDb();

    void setDtDenDb(String dtDenDb);

    /**
     * Nullable property for LQU-DT-DEN-DB
     * 
     */
    String getDtDenDbObj();

    void setDtDenDbObj(String dtDenDbObj);

    /**
     * Host Variable LQU-DT-PERV-DEN-DB
     * 
     */
    String getDtPervDenDb();

    void setDtPervDenDb(String dtPervDenDb);

    /**
     * Nullable property for LQU-DT-PERV-DEN-DB
     * 
     */
    String getDtPervDenDbObj();

    void setDtPervDenDbObj(String dtPervDenDbObj);

    /**
     * Host Variable LQU-DT-RICH-DB
     * 
     */
    String getDtRichDb();

    void setDtRichDb(String dtRichDb);

    /**
     * Nullable property for LQU-DT-RICH-DB
     * 
     */
    String getDtRichDbObj();

    void setDtRichDbObj(String dtRichDbObj);

    /**
     * Host Variable LQU-TP-SIN
     * 
     */
    String getTpSin();

    void setTpSin(String tpSin);

    /**
     * Nullable property for LQU-TP-SIN
     * 
     */
    String getTpSinObj();

    void setTpSinObj(String tpSinObj);

    /**
     * Host Variable LQU-TP-RISC
     * 
     */
    String getTpRisc();

    void setTpRisc(String tpRisc);

    /**
     * Nullable property for LQU-TP-RISC
     * 
     */
    String getTpRiscObj();

    void setTpRiscObj(String tpRiscObj);

    /**
     * Host Variable LQU-TP-MET-RISC
     * 
     */
    short getTpMetRisc();

    void setTpMetRisc(short tpMetRisc);

    /**
     * Nullable property for LQU-TP-MET-RISC
     * 
     */
    Short getTpMetRiscObj();

    void setTpMetRiscObj(Short tpMetRiscObj);

    /**
     * Host Variable LQU-DT-LIQ-DB
     * 
     */
    String getDtLiqDb();

    void setDtLiqDb(String dtLiqDb);

    /**
     * Nullable property for LQU-DT-LIQ-DB
     * 
     */
    String getDtLiqDbObj();

    void setDtLiqDbObj(String dtLiqDbObj);

    /**
     * Host Variable LQU-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for LQU-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable LQU-TOT-IMP-LRD-LIQTO
     * 
     */
    AfDecimal getTotImpLrdLiqto();

    void setTotImpLrdLiqto(AfDecimal totImpLrdLiqto);

    /**
     * Nullable property for LQU-TOT-IMP-LRD-LIQTO
     * 
     */
    AfDecimal getTotImpLrdLiqtoObj();

    void setTotImpLrdLiqtoObj(AfDecimal totImpLrdLiqtoObj);

    /**
     * Host Variable LQU-TOT-IMP-PREST
     * 
     */
    AfDecimal getTotImpPrest();

    void setTotImpPrest(AfDecimal totImpPrest);

    /**
     * Nullable property for LQU-TOT-IMP-PREST
     * 
     */
    AfDecimal getTotImpPrestObj();

    void setTotImpPrestObj(AfDecimal totImpPrestObj);

    /**
     * Host Variable LQU-TOT-IMP-INTR-PREST
     * 
     */
    AfDecimal getTotImpIntrPrest();

    void setTotImpIntrPrest(AfDecimal totImpIntrPrest);

    /**
     * Nullable property for LQU-TOT-IMP-INTR-PREST
     * 
     */
    AfDecimal getTotImpIntrPrestObj();

    void setTotImpIntrPrestObj(AfDecimal totImpIntrPrestObj);

    /**
     * Host Variable LQU-TOT-IMP-UTI
     * 
     */
    AfDecimal getTotImpUti();

    void setTotImpUti(AfDecimal totImpUti);

    /**
     * Nullable property for LQU-TOT-IMP-UTI
     * 
     */
    AfDecimal getTotImpUtiObj();

    void setTotImpUtiObj(AfDecimal totImpUtiObj);

    /**
     * Host Variable LQU-TOT-IMP-RIT-TFR
     * 
     */
    AfDecimal getTotImpRitTfr();

    void setTotImpRitTfr(AfDecimal totImpRitTfr);

    /**
     * Nullable property for LQU-TOT-IMP-RIT-TFR
     * 
     */
    AfDecimal getTotImpRitTfrObj();

    void setTotImpRitTfrObj(AfDecimal totImpRitTfrObj);

    /**
     * Host Variable LQU-TOT-IMP-RIT-ACC
     * 
     */
    AfDecimal getTotImpRitAcc();

    void setTotImpRitAcc(AfDecimal totImpRitAcc);

    /**
     * Nullable property for LQU-TOT-IMP-RIT-ACC
     * 
     */
    AfDecimal getTotImpRitAccObj();

    void setTotImpRitAccObj(AfDecimal totImpRitAccObj);

    /**
     * Host Variable LQU-TOT-IMP-RIT-VIS
     * 
     */
    AfDecimal getTotImpRitVis();

    void setTotImpRitVis(AfDecimal totImpRitVis);

    /**
     * Nullable property for LQU-TOT-IMP-RIT-VIS
     * 
     */
    AfDecimal getTotImpRitVisObj();

    void setTotImpRitVisObj(AfDecimal totImpRitVisObj);

    /**
     * Host Variable LQU-TOT-IMPB-TFR
     * 
     */
    AfDecimal getTotImpbTfr();

    void setTotImpbTfr(AfDecimal totImpbTfr);

    /**
     * Nullable property for LQU-TOT-IMPB-TFR
     * 
     */
    AfDecimal getTotImpbTfrObj();

    void setTotImpbTfrObj(AfDecimal totImpbTfrObj);

    /**
     * Host Variable LQU-TOT-IMPB-ACC
     * 
     */
    AfDecimal getTotImpbAcc();

    void setTotImpbAcc(AfDecimal totImpbAcc);

    /**
     * Nullable property for LQU-TOT-IMPB-ACC
     * 
     */
    AfDecimal getTotImpbAccObj();

    void setTotImpbAccObj(AfDecimal totImpbAccObj);

    /**
     * Host Variable LQU-TOT-IMPB-VIS
     * 
     */
    AfDecimal getTotImpbVis();

    void setTotImpbVis(AfDecimal totImpbVis);

    /**
     * Nullable property for LQU-TOT-IMPB-VIS
     * 
     */
    AfDecimal getTotImpbVisObj();

    void setTotImpbVisObj(AfDecimal totImpbVisObj);

    /**
     * Host Variable LQU-TOT-IMP-RIMB
     * 
     */
    AfDecimal getTotImpRimb();

    void setTotImpRimb(AfDecimal totImpRimb);

    /**
     * Nullable property for LQU-TOT-IMP-RIMB
     * 
     */
    AfDecimal getTotImpRimbObj();

    void setTotImpRimbObj(AfDecimal totImpRimbObj);

    /**
     * Host Variable LQU-IMPB-IMPST-PRVR
     * 
     */
    AfDecimal getImpbImpstPrvr();

    void setImpbImpstPrvr(AfDecimal impbImpstPrvr);

    /**
     * Nullable property for LQU-IMPB-IMPST-PRVR
     * 
     */
    AfDecimal getImpbImpstPrvrObj();

    void setImpbImpstPrvrObj(AfDecimal impbImpstPrvrObj);

    /**
     * Host Variable LQU-IMPST-PRVR
     * 
     */
    AfDecimal getImpstPrvr();

    void setImpstPrvr(AfDecimal impstPrvr);

    /**
     * Nullable property for LQU-IMPST-PRVR
     * 
     */
    AfDecimal getImpstPrvrObj();

    void setImpstPrvrObj(AfDecimal impstPrvrObj);

    /**
     * Host Variable LQU-IMPB-IMPST-252
     * 
     */
    AfDecimal getImpbImpst252();

    void setImpbImpst252(AfDecimal impbImpst252);

    /**
     * Nullable property for LQU-IMPB-IMPST-252
     * 
     */
    AfDecimal getImpbImpst252Obj();

    void setImpbImpst252Obj(AfDecimal impbImpst252Obj);

    /**
     * Host Variable LQU-IMPST-252
     * 
     */
    AfDecimal getImpst252();

    void setImpst252(AfDecimal impst252);

    /**
     * Nullable property for LQU-IMPST-252
     * 
     */
    AfDecimal getImpst252Obj();

    void setImpst252Obj(AfDecimal impst252Obj);

    /**
     * Host Variable LQU-TOT-IMP-IS
     * 
     */
    AfDecimal getTotImpIs();

    void setTotImpIs(AfDecimal totImpIs);

    /**
     * Nullable property for LQU-TOT-IMP-IS
     * 
     */
    AfDecimal getTotImpIsObj();

    void setTotImpIsObj(AfDecimal totImpIsObj);

    /**
     * Host Variable LQU-IMP-DIR-LIQ
     * 
     */
    AfDecimal getImpDirLiq();

    void setImpDirLiq(AfDecimal impDirLiq);

    /**
     * Nullable property for LQU-IMP-DIR-LIQ
     * 
     */
    AfDecimal getImpDirLiqObj();

    void setImpDirLiqObj(AfDecimal impDirLiqObj);

    /**
     * Host Variable LQU-TOT-IMP-NET-LIQTO
     * 
     */
    AfDecimal getTotImpNetLiqto();

    void setTotImpNetLiqto(AfDecimal totImpNetLiqto);

    /**
     * Nullable property for LQU-TOT-IMP-NET-LIQTO
     * 
     */
    AfDecimal getTotImpNetLiqtoObj();

    void setTotImpNetLiqtoObj(AfDecimal totImpNetLiqtoObj);

    /**
     * Host Variable LQU-MONT-END2000
     * 
     */
    AfDecimal getMontEnd2000();

    void setMontEnd2000(AfDecimal montEnd2000);

    /**
     * Nullable property for LQU-MONT-END2000
     * 
     */
    AfDecimal getMontEnd2000Obj();

    void setMontEnd2000Obj(AfDecimal montEnd2000Obj);

    /**
     * Host Variable LQU-MONT-END2006
     * 
     */
    AfDecimal getMontEnd2006();

    void setMontEnd2006(AfDecimal montEnd2006);

    /**
     * Nullable property for LQU-MONT-END2006
     * 
     */
    AfDecimal getMontEnd2006Obj();

    void setMontEnd2006Obj(AfDecimal montEnd2006Obj);

    /**
     * Host Variable LQU-PC-REN
     * 
     */
    AfDecimal getPcRen();

    void setPcRen(AfDecimal pcRen);

    /**
     * Nullable property for LQU-PC-REN
     * 
     */
    AfDecimal getPcRenObj();

    void setPcRenObj(AfDecimal pcRenObj);

    /**
     * Host Variable LQU-IMP-PNL
     * 
     */
    AfDecimal getImpPnl();

    void setImpPnl(AfDecimal impPnl);

    /**
     * Nullable property for LQU-IMP-PNL
     * 
     */
    AfDecimal getImpPnlObj();

    void setImpPnlObj(AfDecimal impPnlObj);

    /**
     * Host Variable LQU-IMPB-IRPEF
     * 
     */
    AfDecimal getImpbIrpef();

    void setImpbIrpef(AfDecimal impbIrpef);

    /**
     * Nullable property for LQU-IMPB-IRPEF
     * 
     */
    AfDecimal getImpbIrpefObj();

    void setImpbIrpefObj(AfDecimal impbIrpefObj);

    /**
     * Host Variable LQU-IMPST-IRPEF
     * 
     */
    AfDecimal getImpstIrpef();

    void setImpstIrpef(AfDecimal impstIrpef);

    /**
     * Nullable property for LQU-IMPST-IRPEF
     * 
     */
    AfDecimal getImpstIrpefObj();

    void setImpstIrpefObj(AfDecimal impstIrpefObj);

    /**
     * Host Variable LQU-DT-VLT-DB
     * 
     */
    String getDtVltDb();

    void setDtVltDb(String dtVltDb);

    /**
     * Nullable property for LQU-DT-VLT-DB
     * 
     */
    String getDtVltDbObj();

    void setDtVltDbObj(String dtVltDbObj);

    /**
     * Host Variable LQU-DT-END-ISTR-DB
     * 
     */
    String getDtEndIstrDb();

    void setDtEndIstrDb(String dtEndIstrDb);

    /**
     * Nullable property for LQU-DT-END-ISTR-DB
     * 
     */
    String getDtEndIstrDbObj();

    void setDtEndIstrDbObj(String dtEndIstrDbObj);

    /**
     * Host Variable LQU-TP-RIMB
     * 
     */
    String getTpRimb();

    void setTpRimb(String tpRimb);

    /**
     * Host Variable LQU-SPE-RCS
     * 
     */
    AfDecimal getSpeRcs();

    void setSpeRcs(AfDecimal speRcs);

    /**
     * Nullable property for LQU-SPE-RCS
     * 
     */
    AfDecimal getSpeRcsObj();

    void setSpeRcsObj(AfDecimal speRcsObj);

    /**
     * Host Variable LQU-IB-LIQ
     * 
     */
    String getIbLiq();

    void setIbLiq(String ibLiq);

    /**
     * Nullable property for LQU-IB-LIQ
     * 
     */
    String getIbLiqObj();

    void setIbLiqObj(String ibLiqObj);

    /**
     * Host Variable LQU-TOT-IAS-ONER-PRVNT
     * 
     */
    AfDecimal getTotIasOnerPrvnt();

    void setTotIasOnerPrvnt(AfDecimal totIasOnerPrvnt);

    /**
     * Nullable property for LQU-TOT-IAS-ONER-PRVNT
     * 
     */
    AfDecimal getTotIasOnerPrvntObj();

    void setTotIasOnerPrvntObj(AfDecimal totIasOnerPrvntObj);

    /**
     * Host Variable LQU-TOT-IAS-MGG-SIN
     * 
     */
    AfDecimal getTotIasMggSin();

    void setTotIasMggSin(AfDecimal totIasMggSin);

    /**
     * Nullable property for LQU-TOT-IAS-MGG-SIN
     * 
     */
    AfDecimal getTotIasMggSinObj();

    void setTotIasMggSinObj(AfDecimal totIasMggSinObj);

    /**
     * Host Variable LQU-TOT-IAS-RST-DPST
     * 
     */
    AfDecimal getTotIasRstDpst();

    void setTotIasRstDpst(AfDecimal totIasRstDpst);

    /**
     * Nullable property for LQU-TOT-IAS-RST-DPST
     * 
     */
    AfDecimal getTotIasRstDpstObj();

    void setTotIasRstDpstObj(AfDecimal totIasRstDpstObj);

    /**
     * Host Variable LQU-IMP-ONER-LIQ
     * 
     */
    AfDecimal getImpOnerLiq();

    void setImpOnerLiq(AfDecimal impOnerLiq);

    /**
     * Nullable property for LQU-IMP-ONER-LIQ
     * 
     */
    AfDecimal getImpOnerLiqObj();

    void setImpOnerLiqObj(AfDecimal impOnerLiqObj);

    /**
     * Host Variable LQU-COMPON-TAX-RIMB
     * 
     */
    AfDecimal getComponTaxRimb();

    void setComponTaxRimb(AfDecimal componTaxRimb);

    /**
     * Nullable property for LQU-COMPON-TAX-RIMB
     * 
     */
    AfDecimal getComponTaxRimbObj();

    void setComponTaxRimbObj(AfDecimal componTaxRimbObj);

    /**
     * Host Variable LQU-TP-MEZ-PAG
     * 
     */
    String getTpMezPag();

    void setTpMezPag(String tpMezPag);

    /**
     * Nullable property for LQU-TP-MEZ-PAG
     * 
     */
    String getTpMezPagObj();

    void setTpMezPagObj(String tpMezPagObj);

    /**
     * Host Variable LQU-IMP-EXCONTR
     * 
     */
    AfDecimal getImpExcontr();

    void setImpExcontr(AfDecimal impExcontr);

    /**
     * Nullable property for LQU-IMP-EXCONTR
     * 
     */
    AfDecimal getImpExcontrObj();

    void setImpExcontrObj(AfDecimal impExcontrObj);

    /**
     * Host Variable LQU-IMP-INTR-RIT-PAG
     * 
     */
    AfDecimal getImpIntrRitPag();

    void setImpIntrRitPag(AfDecimal impIntrRitPag);

    /**
     * Nullable property for LQU-IMP-INTR-RIT-PAG
     * 
     */
    AfDecimal getImpIntrRitPagObj();

    void setImpIntrRitPagObj(AfDecimal impIntrRitPagObj);

    /**
     * Host Variable LQU-BNS-NON-GODUTO
     * 
     */
    AfDecimal getBnsNonGoduto();

    void setBnsNonGoduto(AfDecimal bnsNonGoduto);

    /**
     * Nullable property for LQU-BNS-NON-GODUTO
     * 
     */
    AfDecimal getBnsNonGodutoObj();

    void setBnsNonGodutoObj(AfDecimal bnsNonGodutoObj);

    /**
     * Host Variable LQU-CNBT-INPSTFM
     * 
     */
    AfDecimal getCnbtInpstfm();

    void setCnbtInpstfm(AfDecimal cnbtInpstfm);

    /**
     * Nullable property for LQU-CNBT-INPSTFM
     * 
     */
    AfDecimal getCnbtInpstfmObj();

    void setCnbtInpstfmObj(AfDecimal cnbtInpstfmObj);

    /**
     * Host Variable LQU-IMPST-DA-RIMB
     * 
     */
    AfDecimal getImpstDaRimb();

    void setImpstDaRimb(AfDecimal impstDaRimb);

    /**
     * Nullable property for LQU-IMPST-DA-RIMB
     * 
     */
    AfDecimal getImpstDaRimbObj();

    void setImpstDaRimbObj(AfDecimal impstDaRimbObj);

    /**
     * Host Variable LQU-IMPB-IS
     * 
     */
    AfDecimal getImpbIs();

    void setImpbIs(AfDecimal impbIs);

    /**
     * Nullable property for LQU-IMPB-IS
     * 
     */
    AfDecimal getImpbIsObj();

    void setImpbIsObj(AfDecimal impbIsObj);

    /**
     * Host Variable LQU-TAX-SEP
     * 
     */
    AfDecimal getTaxSep();

    void setTaxSep(AfDecimal taxSep);

    /**
     * Nullable property for LQU-TAX-SEP
     * 
     */
    AfDecimal getTaxSepObj();

    void setTaxSepObj(AfDecimal taxSepObj);

    /**
     * Host Variable LQU-IMPB-TAX-SEP
     * 
     */
    AfDecimal getImpbTaxSep();

    void setImpbTaxSep(AfDecimal impbTaxSep);

    /**
     * Nullable property for LQU-IMPB-TAX-SEP
     * 
     */
    AfDecimal getImpbTaxSepObj();

    void setImpbTaxSepObj(AfDecimal impbTaxSepObj);

    /**
     * Host Variable LQU-IMPB-INTR-SU-PREST
     * 
     */
    AfDecimal getImpbIntrSuPrest();

    void setImpbIntrSuPrest(AfDecimal impbIntrSuPrest);

    /**
     * Nullable property for LQU-IMPB-INTR-SU-PREST
     * 
     */
    AfDecimal getImpbIntrSuPrestObj();

    void setImpbIntrSuPrestObj(AfDecimal impbIntrSuPrestObj);

    /**
     * Host Variable LQU-ADDIZ-COMUN
     * 
     */
    AfDecimal getAddizComun();

    void setAddizComun(AfDecimal addizComun);

    /**
     * Nullable property for LQU-ADDIZ-COMUN
     * 
     */
    AfDecimal getAddizComunObj();

    void setAddizComunObj(AfDecimal addizComunObj);

    /**
     * Host Variable LQU-IMPB-ADDIZ-COMUN
     * 
     */
    AfDecimal getImpbAddizComun();

    void setImpbAddizComun(AfDecimal impbAddizComun);

    /**
     * Nullable property for LQU-IMPB-ADDIZ-COMUN
     * 
     */
    AfDecimal getImpbAddizComunObj();

    void setImpbAddizComunObj(AfDecimal impbAddizComunObj);

    /**
     * Host Variable LQU-ADDIZ-REGION
     * 
     */
    AfDecimal getAddizRegion();

    void setAddizRegion(AfDecimal addizRegion);

    /**
     * Nullable property for LQU-ADDIZ-REGION
     * 
     */
    AfDecimal getAddizRegionObj();

    void setAddizRegionObj(AfDecimal addizRegionObj);

    /**
     * Host Variable LQU-IMPB-ADDIZ-REGION
     * 
     */
    AfDecimal getImpbAddizRegion();

    void setImpbAddizRegion(AfDecimal impbAddizRegion);

    /**
     * Nullable property for LQU-IMPB-ADDIZ-REGION
     * 
     */
    AfDecimal getImpbAddizRegionObj();

    void setImpbAddizRegionObj(AfDecimal impbAddizRegionObj);

    /**
     * Host Variable LQU-MONT-DAL2007
     * 
     */
    AfDecimal getMontDal2007();

    void setMontDal2007(AfDecimal montDal2007);

    /**
     * Nullable property for LQU-MONT-DAL2007
     * 
     */
    AfDecimal getMontDal2007Obj();

    void setMontDal2007Obj(AfDecimal montDal2007Obj);

    /**
     * Host Variable LQU-IMPB-CNBT-INPSTFM
     * 
     */
    AfDecimal getImpbCnbtInpstfm();

    void setImpbCnbtInpstfm(AfDecimal impbCnbtInpstfm);

    /**
     * Nullable property for LQU-IMPB-CNBT-INPSTFM
     * 
     */
    AfDecimal getImpbCnbtInpstfmObj();

    void setImpbCnbtInpstfmObj(AfDecimal impbCnbtInpstfmObj);

    /**
     * Host Variable LQU-IMP-LRD-DA-RIMB
     * 
     */
    AfDecimal getImpLrdDaRimb();

    void setImpLrdDaRimb(AfDecimal impLrdDaRimb);

    /**
     * Nullable property for LQU-IMP-LRD-DA-RIMB
     * 
     */
    AfDecimal getImpLrdDaRimbObj();

    void setImpLrdDaRimbObj(AfDecimal impLrdDaRimbObj);

    /**
     * Host Variable LQU-IMP-DIR-DA-RIMB
     * 
     */
    AfDecimal getImpDirDaRimb();

    void setImpDirDaRimb(AfDecimal impDirDaRimb);

    /**
     * Nullable property for LQU-IMP-DIR-DA-RIMB
     * 
     */
    AfDecimal getImpDirDaRimbObj();

    void setImpDirDaRimbObj(AfDecimal impDirDaRimbObj);

    /**
     * Host Variable LQU-RIS-MAT
     * 
     */
    AfDecimal getRisMat();

    void setRisMat(AfDecimal risMat);

    /**
     * Nullable property for LQU-RIS-MAT
     * 
     */
    AfDecimal getRisMatObj();

    void setRisMatObj(AfDecimal risMatObj);

    /**
     * Host Variable LQU-RIS-SPE
     * 
     */
    AfDecimal getRisSpe();

    void setRisSpe(AfDecimal risSpe);

    /**
     * Nullable property for LQU-RIS-SPE
     * 
     */
    AfDecimal getRisSpeObj();

    void setRisSpeObj(AfDecimal risSpeObj);

    /**
     * Host Variable LQU-DS-RIGA
     * 
     */
    long getLquDsRiga();

    void setLquDsRiga(long lquDsRiga);

    /**
     * Host Variable LQU-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable LQU-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable LQU-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable LQU-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable LQU-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable LQU-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable LQU-TOT-IAS-PNL
     * 
     */
    AfDecimal getTotIasPnl();

    void setTotIasPnl(AfDecimal totIasPnl);

    /**
     * Nullable property for LQU-TOT-IAS-PNL
     * 
     */
    AfDecimal getTotIasPnlObj();

    void setTotIasPnlObj(AfDecimal totIasPnlObj);

    /**
     * Host Variable LQU-FL-EVE-GARTO
     * 
     */
    char getFlEveGarto();

    void setFlEveGarto(char flEveGarto);

    /**
     * Nullable property for LQU-FL-EVE-GARTO
     * 
     */
    Character getFlEveGartoObj();

    void setFlEveGartoObj(Character flEveGartoObj);

    /**
     * Host Variable LQU-IMP-REN-K1
     * 
     */
    AfDecimal getImpRenK1();

    void setImpRenK1(AfDecimal impRenK1);

    /**
     * Nullable property for LQU-IMP-REN-K1
     * 
     */
    AfDecimal getImpRenK1Obj();

    void setImpRenK1Obj(AfDecimal impRenK1Obj);

    /**
     * Host Variable LQU-IMP-REN-K2
     * 
     */
    AfDecimal getImpRenK2();

    void setImpRenK2(AfDecimal impRenK2);

    /**
     * Nullable property for LQU-IMP-REN-K2
     * 
     */
    AfDecimal getImpRenK2Obj();

    void setImpRenK2Obj(AfDecimal impRenK2Obj);

    /**
     * Host Variable LQU-IMP-REN-K3
     * 
     */
    AfDecimal getImpRenK3();

    void setImpRenK3(AfDecimal impRenK3);

    /**
     * Nullable property for LQU-IMP-REN-K3
     * 
     */
    AfDecimal getImpRenK3Obj();

    void setImpRenK3Obj(AfDecimal impRenK3Obj);

    /**
     * Host Variable LQU-PC-REN-K1
     * 
     */
    AfDecimal getPcRenK1();

    void setPcRenK1(AfDecimal pcRenK1);

    /**
     * Nullable property for LQU-PC-REN-K1
     * 
     */
    AfDecimal getPcRenK1Obj();

    void setPcRenK1Obj(AfDecimal pcRenK1Obj);

    /**
     * Host Variable LQU-PC-REN-K2
     * 
     */
    AfDecimal getPcRenK2();

    void setPcRenK2(AfDecimal pcRenK2);

    /**
     * Nullable property for LQU-PC-REN-K2
     * 
     */
    AfDecimal getPcRenK2Obj();

    void setPcRenK2Obj(AfDecimal pcRenK2Obj);

    /**
     * Host Variable LQU-PC-REN-K3
     * 
     */
    AfDecimal getPcRenK3();

    void setPcRenK3(AfDecimal pcRenK3);

    /**
     * Nullable property for LQU-PC-REN-K3
     * 
     */
    AfDecimal getPcRenK3Obj();

    void setPcRenK3Obj(AfDecimal pcRenK3Obj);

    /**
     * Host Variable LQU-TP-CAUS-ANTIC
     * 
     */
    String getTpCausAntic();

    void setTpCausAntic(String tpCausAntic);

    /**
     * Nullable property for LQU-TP-CAUS-ANTIC
     * 
     */
    String getTpCausAnticObj();

    void setTpCausAnticObj(String tpCausAnticObj);

    /**
     * Host Variable LQU-IMP-LRD-LIQTO-RILT
     * 
     */
    AfDecimal getImpLrdLiqtoRilt();

    void setImpLrdLiqtoRilt(AfDecimal impLrdLiqtoRilt);

    /**
     * Nullable property for LQU-IMP-LRD-LIQTO-RILT
     * 
     */
    AfDecimal getImpLrdLiqtoRiltObj();

    void setImpLrdLiqtoRiltObj(AfDecimal impLrdLiqtoRiltObj);

    /**
     * Host Variable LQU-IMPST-APPL-RILT
     * 
     */
    AfDecimal getImpstApplRilt();

    void setImpstApplRilt(AfDecimal impstApplRilt);

    /**
     * Nullable property for LQU-IMPST-APPL-RILT
     * 
     */
    AfDecimal getImpstApplRiltObj();

    void setImpstApplRiltObj(AfDecimal impstApplRiltObj);

    /**
     * Host Variable LQU-PC-RISC-PARZ
     * 
     */
    AfDecimal getPcRiscParz();

    void setPcRiscParz(AfDecimal pcRiscParz);

    /**
     * Nullable property for LQU-PC-RISC-PARZ
     * 
     */
    AfDecimal getPcRiscParzObj();

    void setPcRiscParzObj(AfDecimal pcRiscParzObj);

    /**
     * Host Variable LQU-IMPST-BOLLO-TOT-V
     * 
     */
    AfDecimal getImpstBolloTotV();

    void setImpstBolloTotV(AfDecimal impstBolloTotV);

    /**
     * Nullable property for LQU-IMPST-BOLLO-TOT-V
     * 
     */
    AfDecimal getImpstBolloTotVObj();

    void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj);

    /**
     * Host Variable LQU-IMPST-BOLLO-DETT-C
     * 
     */
    AfDecimal getImpstBolloDettC();

    void setImpstBolloDettC(AfDecimal impstBolloDettC);

    /**
     * Nullable property for LQU-IMPST-BOLLO-DETT-C
     * 
     */
    AfDecimal getImpstBolloDettCObj();

    void setImpstBolloDettCObj(AfDecimal impstBolloDettCObj);

    /**
     * Host Variable LQU-IMPST-BOLLO-TOT-SW
     * 
     */
    AfDecimal getImpstBolloTotSw();

    void setImpstBolloTotSw(AfDecimal impstBolloTotSw);

    /**
     * Nullable property for LQU-IMPST-BOLLO-TOT-SW
     * 
     */
    AfDecimal getImpstBolloTotSwObj();

    void setImpstBolloTotSwObj(AfDecimal impstBolloTotSwObj);

    /**
     * Host Variable LQU-IMPST-BOLLO-TOT-AA
     * 
     */
    AfDecimal getImpstBolloTotAa();

    void setImpstBolloTotAa(AfDecimal impstBolloTotAa);

    /**
     * Nullable property for LQU-IMPST-BOLLO-TOT-AA
     * 
     */
    AfDecimal getImpstBolloTotAaObj();

    void setImpstBolloTotAaObj(AfDecimal impstBolloTotAaObj);

    /**
     * Host Variable LQU-IMPB-VIS-1382011
     * 
     */
    AfDecimal getImpbVis1382011();

    void setImpbVis1382011(AfDecimal impbVis1382011);

    /**
     * Nullable property for LQU-IMPB-VIS-1382011
     * 
     */
    AfDecimal getImpbVis1382011Obj();

    void setImpbVis1382011Obj(AfDecimal impbVis1382011Obj);

    /**
     * Host Variable LQU-IMPST-VIS-1382011
     * 
     */
    AfDecimal getImpstVis1382011();

    void setImpstVis1382011(AfDecimal impstVis1382011);

    /**
     * Nullable property for LQU-IMPST-VIS-1382011
     * 
     */
    AfDecimal getImpstVis1382011Obj();

    void setImpstVis1382011Obj(AfDecimal impstVis1382011Obj);

    /**
     * Host Variable LQU-IMPB-IS-1382011
     * 
     */
    AfDecimal getImpbIs1382011();

    void setImpbIs1382011(AfDecimal impbIs1382011);

    /**
     * Nullable property for LQU-IMPB-IS-1382011
     * 
     */
    AfDecimal getImpbIs1382011Obj();

    void setImpbIs1382011Obj(AfDecimal impbIs1382011Obj);

    /**
     * Host Variable LQU-IMPST-SOST-1382011
     * 
     */
    AfDecimal getImpstSost1382011();

    void setImpstSost1382011(AfDecimal impstSost1382011);

    /**
     * Nullable property for LQU-IMPST-SOST-1382011
     * 
     */
    AfDecimal getImpstSost1382011Obj();

    void setImpstSost1382011Obj(AfDecimal impstSost1382011Obj);

    /**
     * Host Variable LQU-PC-ABB-TIT-STAT
     * 
     */
    AfDecimal getPcAbbTitStat();

    void setPcAbbTitStat(AfDecimal pcAbbTitStat);

    /**
     * Nullable property for LQU-PC-ABB-TIT-STAT
     * 
     */
    AfDecimal getPcAbbTitStatObj();

    void setPcAbbTitStatObj(AfDecimal pcAbbTitStatObj);

    /**
     * Host Variable LQU-IMPB-BOLLO-DETT-C
     * 
     */
    AfDecimal getImpbBolloDettC();

    void setImpbBolloDettC(AfDecimal impbBolloDettC);

    /**
     * Nullable property for LQU-IMPB-BOLLO-DETT-C
     * 
     */
    AfDecimal getImpbBolloDettCObj();

    void setImpbBolloDettCObj(AfDecimal impbBolloDettCObj);

    /**
     * Host Variable LQU-FL-PRE-COMP
     * 
     */
    char getFlPreComp();

    void setFlPreComp(char flPreComp);

    /**
     * Nullable property for LQU-FL-PRE-COMP
     * 
     */
    Character getFlPreCompObj();

    void setFlPreCompObj(Character flPreCompObj);

    /**
     * Host Variable LQU-IMPB-VIS-662014
     * 
     */
    AfDecimal getImpbVis662014();

    void setImpbVis662014(AfDecimal impbVis662014);

    /**
     * Nullable property for LQU-IMPB-VIS-662014
     * 
     */
    AfDecimal getImpbVis662014Obj();

    void setImpbVis662014Obj(AfDecimal impbVis662014Obj);

    /**
     * Host Variable LQU-IMPST-VIS-662014
     * 
     */
    AfDecimal getImpstVis662014();

    void setImpstVis662014(AfDecimal impstVis662014);

    /**
     * Nullable property for LQU-IMPST-VIS-662014
     * 
     */
    AfDecimal getImpstVis662014Obj();

    void setImpstVis662014Obj(AfDecimal impstVis662014Obj);

    /**
     * Host Variable LQU-IMPB-IS-662014
     * 
     */
    AfDecimal getImpbIs662014();

    void setImpbIs662014(AfDecimal impbIs662014);

    /**
     * Nullable property for LQU-IMPB-IS-662014
     * 
     */
    AfDecimal getImpbIs662014Obj();

    void setImpbIs662014Obj(AfDecimal impbIs662014Obj);

    /**
     * Host Variable LQU-IMPST-SOST-662014
     * 
     */
    AfDecimal getImpstSost662014();

    void setImpstSost662014(AfDecimal impstSost662014);

    /**
     * Nullable property for LQU-IMPST-SOST-662014
     * 
     */
    AfDecimal getImpstSost662014Obj();

    void setImpstSost662014Obj(AfDecimal impstSost662014Obj);

    /**
     * Host Variable LQU-PC-ABB-TS-662014
     * 
     */
    AfDecimal getPcAbbTs662014();

    void setPcAbbTs662014(AfDecimal pcAbbTs662014);

    /**
     * Nullable property for LQU-PC-ABB-TS-662014
     * 
     */
    AfDecimal getPcAbbTs662014Obj();

    void setPcAbbTs662014Obj(AfDecimal pcAbbTs662014Obj);

    /**
     * Host Variable LQU-IMP-LRD-CALC-CP
     * 
     */
    AfDecimal getImpLrdCalcCp();

    void setImpLrdCalcCp(AfDecimal impLrdCalcCp);

    /**
     * Nullable property for LQU-IMP-LRD-CALC-CP
     * 
     */
    AfDecimal getImpLrdCalcCpObj();

    void setImpLrdCalcCpObj(AfDecimal impLrdCalcCpObj);

    /**
     * Host Variable LQU-COS-TUNNEL-USCITA
     * 
     */
    AfDecimal getCosTunnelUscita();

    void setCosTunnelUscita(AfDecimal cosTunnelUscita);

    /**
     * Nullable property for LQU-COS-TUNNEL-USCITA
     * 
     */
    AfDecimal getCosTunnelUscitaObj();

    void setCosTunnelUscitaObj(AfDecimal cosTunnelUscitaObj);

    /**
     * Host Variable LDBV2271-IMP-LRD-LIQTO
     * 
     */
    AfDecimal getLdbv2271ImpLrdLiqto();

    void setLdbv2271ImpLrdLiqto(AfDecimal ldbv2271ImpLrdLiqto);

    /**
     * Nullable property for LDBV2271-IMP-LRD-LIQTO
     * 
     */
    AfDecimal getLdbv2271ImpLrdLiqtoObj();

    void setLdbv2271ImpLrdLiqtoObj(AfDecimal ldbv2271ImpLrdLiqtoObj);

    /**
     * Host Variable LDBV2271-ID-OGG
     * 
     */
    int getLdbv2271IdOgg();

    void setLdbv2271IdOgg(int ldbv2271IdOgg);

    /**
     * Host Variable LDBV2271-TP-OGG
     * 
     */
    String getLdbv2271TpOgg();

    void setLdbv2271TpOgg(String ldbv2271TpOgg);

    /**
     * Host Variable LDBV2271-TP-LIQ
     * 
     */
    String getLdbv2271TpLiq();

    void setLdbv2271TpLiq(String ldbv2271TpLiq);
};
