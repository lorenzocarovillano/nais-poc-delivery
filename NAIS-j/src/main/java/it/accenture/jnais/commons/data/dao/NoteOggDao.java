package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.INoteOgg;

/**
 * Data Access Object(DAO) for table [NOTE_OGG]
 * 
 */
public class NoteOggDao extends BaseSqlDao<INoteOgg> {

    private Cursor cIdoNstNot;
    private final IRowMapper<INoteOgg> selectByNotIdNoteOggRm = buildNamedRowMapper(INoteOgg.class, "notIdNoteOgg", "codCompAnia", "idOgg", "tpOgg", "notaOggVcharObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public NoteOggDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<INoteOgg> getToClass() {
        return INoteOgg.class;
    }

    public INoteOgg selectByNotIdNoteOgg(int notIdNoteOgg, INoteOgg iNoteOgg) {
        return buildQuery("selectByNotIdNoteOgg").bind("notIdNoteOgg", notIdNoteOgg).rowMapper(selectByNotIdNoteOggRm).singleResult(iNoteOgg);
    }

    public DbAccessStatus insertRec(INoteOgg iNoteOgg) {
        return buildQuery("insertRec").bind(iNoteOgg).executeInsert();
    }

    public DbAccessStatus updateRec(INoteOgg iNoteOgg) {
        return buildQuery("updateRec").bind(iNoteOgg).executeUpdate();
    }

    public DbAccessStatus deleteByNotIdNoteOgg(int notIdNoteOgg) {
        return buildQuery("deleteByNotIdNoteOgg").bind("notIdNoteOgg", notIdNoteOgg).executeDelete();
    }

    public INoteOgg selectRec(int notIdOgg, String notTpOgg, int idsv0003CodiceCompagniaAnia, INoteOgg iNoteOgg) {
        return buildQuery("selectRec").bind("notIdOgg", notIdOgg).bind("notTpOgg", notTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByNotIdNoteOggRm).singleResult(iNoteOgg);
    }

    public DbAccessStatus openCIdoNstNot(int notIdOgg, String notTpOgg, int idsv0003CodiceCompagniaAnia) {
        cIdoNstNot = buildQuery("openCIdoNstNot").bind("notIdOgg", notIdOgg).bind("notTpOgg", notTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoNstNot() {
        return closeCursor(cIdoNstNot);
    }

    public INoteOgg fetchCIdoNstNot(INoteOgg iNoteOgg) {
        return fetch(cIdoNstNot, iNoteOgg, selectByNotIdNoteOggRm);
    }
}
