package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ITitCont;

/**
 * Data Access Object(DAO) for table [TIT_CONT]
 * 
 */
public class TitContDao extends BaseSqlDao<ITitCont> {

    private Cursor cIdUpdEffTit;
    private Cursor cIbsEffTit0;
    private Cursor cIdoEffTit;
    private Cursor cIbsCpzTit0;
    private Cursor cIdoCpzTit;
    private Cursor cEff38;
    private Cursor cCpz38;
    private Cursor cEff50;
    private Cursor cEff53;
    private final IRowMapper<ITitCont> selectByTitDsRigaRm = buildNamedRowMapper(ITitCont.class, "idTitCont", "titIdOgg", "titTpOgg", "ibRichObj", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "titTpTit", "progTitObj", "titTpPreTit", "titTpStatTit", "titDtIniCopDbObj", "dtEndCopDbObj", "impPagObj", "flSollObj", "frazObj", "dtApplzMoraDbObj", "flMoraObj", "idRappReteObj", "idRappAnaObj", "codDvsObj", "dtEmisTitDbObj", "dtEsiTitDbObj", "totPreNetObj", "totIntrFrazObj", "totIntrMoraObj", "totIntrPrestObj", "totIntrRetdtObj", "totIntrRiatObj", "totDirObj", "totSpeMedObj", "totTaxObj", "totSoprSanObj", "totSoprTecObj", "totSoprSpoObj", "totSoprProfObj", "totSoprAltObj", "totPreTotObj", "totPrePpIasObj", "totCarAcqObj", "totCarGestObj", "totCarIncObj", "totPreSoloRshObj", "totProvAcq1aaObj", "totProvAcq2aaObj", "totProvRicorObj", "totProvIncObj", "totProvDaRecObj", "impAzObj", "impAderObj", "impTfrObj", "impVoloObj", "totManfeeAnticObj", "totManfeeRicorObj", "totManfeeRecObj", "tpMezPagAddObj", "estrCntCorrAddObj", "dtVltDbObj", "flForzDtVltObj", "dtCambioVltDbObj", "totSpeAgeObj", "totCarIasObj", "numRatAccorpateObj", "titDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "flTitDaReinvstObj", "dtRichAddRidDbObj", "tpEsiRidObj", "codIbanObj", "impTrasfeObj", "impTfrStrcObj", "dtCertFiscDbObj", "tpCausStorObj", "tpCausDispStorObj", "tpTitMigrazObj", "totAcqExpObj", "totRemunAssObj", "totCommisInterObj", "tpCausRimbObj", "totCnbtAntiracObj", "flIncAutogenObj");
    private final IRowMapper<ITitCont> selectRec9Rm = buildNamedRowMapper(ITitCont.class, "titDtIniCopDbObj", "dtEndCopDbObj");
    private final IRowMapper<ITitCont> selectRec11Rm = buildNamedRowMapper(ITitCont.class, "ldbv6151DtMaxDbObj");
    private final IRowMapper<ITitCont> selectRec15Rm = buildNamedRowMapper(ITitCont.class, "ldbvb441DtMaxDbObj");
    private final IRowMapper<ITitCont> selectRec17Rm = buildNamedRowMapper(ITitCont.class, "totIntrPrestObj", "dtIniEffDb");

    public TitContDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ITitCont> getToClass() {
        return ITitCont.class;
    }

    public ITitCont selectByTitDsRiga(long titDsRiga, ITitCont iTitCont) {
        return buildQuery("selectByTitDsRiga").bind("titDsRiga", titDsRiga).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public DbAccessStatus insertRec(ITitCont iTitCont) {
        return buildQuery("insertRec").bind(iTitCont).executeInsert();
    }

    public DbAccessStatus updateRec(ITitCont iTitCont) {
        return buildQuery("updateRec").bind(iTitCont).executeUpdate();
    }

    public DbAccessStatus deleteByTitDsRiga(long titDsRiga) {
        return buildQuery("deleteByTitDsRiga").bind("titDsRiga", titDsRiga).executeDelete();
    }

    public ITitCont selectRec(int titIdTitCont, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITitCont iTitCont) {
        return buildQuery("selectRec").bind("titIdTitCont", titIdTitCont).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public DbAccessStatus updateRec1(ITitCont iTitCont) {
        return buildQuery("updateRec1").bind(iTitCont).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffTit(int titIdTitCont, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffTit = buildQuery("openCIdUpdEffTit").bind("titIdTitCont", titIdTitCont).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffTit() {
        return closeCursor(cIdUpdEffTit);
    }

    public ITitCont fetchCIdUpdEffTit(ITitCont iTitCont) {
        return fetch(cIdUpdEffTit, iTitCont, selectByTitDsRigaRm);
    }

    public ITitCont selectRec1(String titIbRich, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITitCont iTitCont) {
        return buildQuery("selectRec1").bind("titIbRich", titIbRich).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public DbAccessStatus openCIbsEffTit0(String titIbRich, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffTit0 = buildQuery("openCIbsEffTit0").bind("titIbRich", titIbRich).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsEffTit0() {
        return closeCursor(cIbsEffTit0);
    }

    public ITitCont fetchCIbsEffTit0(ITitCont iTitCont) {
        return fetch(cIbsEffTit0, iTitCont, selectByTitDsRigaRm);
    }

    public ITitCont selectRec2(int titIdOgg, String titTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ITitCont iTitCont) {
        return buildQuery("selectRec2").bind("titIdOgg", titIdOgg).bind("titTpOgg", titTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public DbAccessStatus openCIdoEffTit(int titIdOgg, String titTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffTit = buildQuery("openCIdoEffTit").bind("titIdOgg", titIdOgg).bind("titTpOgg", titTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffTit() {
        return closeCursor(cIdoEffTit);
    }

    public ITitCont fetchCIdoEffTit(ITitCont iTitCont) {
        return fetch(cIdoEffTit, iTitCont, selectByTitDsRigaRm);
    }

    public ITitCont selectRec3(int titIdTitCont, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITitCont iTitCont) {
        return buildQuery("selectRec3").bind("titIdTitCont", titIdTitCont).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public ITitCont selectRec4(String titIbRich, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ITitCont iTitCont) {
        return buildQuery("selectRec4").bind("titIbRich", titIbRich).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public DbAccessStatus openCIbsCpzTit0(String titIbRich, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzTit0 = buildQuery("openCIbsCpzTit0").bind("titIbRich", titIbRich).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsCpzTit0() {
        return closeCursor(cIbsCpzTit0);
    }

    public ITitCont fetchCIbsCpzTit0(ITitCont iTitCont) {
        return fetch(cIbsCpzTit0, iTitCont, selectByTitDsRigaRm);
    }

    public ITitCont selectRec5(ITitCont iTitCont) {
        return buildQuery("selectRec5").bind(iTitCont).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public DbAccessStatus openCIdoCpzTit(ITitCont iTitCont) {
        cIdoCpzTit = buildQuery("openCIdoCpzTit").bind(iTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzTit() {
        return closeCursor(cIdoCpzTit);
    }

    public ITitCont fetchCIdoCpzTit(ITitCont iTitCont) {
        return fetch(cIdoCpzTit, iTitCont, selectByTitDsRigaRm);
    }

    public AfDecimal selectRec6(ITitCont iTitCont, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec6").bind(iTitCont).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec7(ITitCont iTitCont, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec7").bind(iTitCont).scalarResultDecimal(dft.copy()), 18, 3);
    }

    public AfDecimal selectRec8(ITitCont iTitCont, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec8").bind(iTitCont).scalarResultDecimal(dft.copy()), 18, 3);
    }

    public ITitCont selectRec9(int ldbv4011IdTitCont, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia, ITitCont iTitCont) {
        return buildQuery("selectRec9").bind("ldbv4011IdTitCont", ldbv4011IdTitCont).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectRec9Rm).singleResult(iTitCont);
    }

    public ITitCont selectRec10(int ldbv4011IdTitCont, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia, long wsTsCompetenza, ITitCont iTitCont) {
        return buildQuery("selectRec10").bind("ldbv4011IdTitCont", ldbv4011IdTitCont).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectRec9Rm).singleResult(iTitCont);
    }

    public ITitCont selectRec11(ITitCont iTitCont) {
        return buildQuery("selectRec11").bind(iTitCont).rowMapper(selectRec11Rm).singleResult(iTitCont);
    }

    public ITitCont selectRec12(ITitCont iTitCont) {
        return buildQuery("selectRec12").bind(iTitCont).rowMapper(selectRec11Rm).singleResult(iTitCont);
    }

    public ITitCont selectRec13(ITitCont iTitCont) {
        return buildQuery("selectRec13").bind(iTitCont).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public DbAccessStatus openCEff38(ITitCont iTitCont) {
        cEff38 = buildQuery("openCEff38").bind(iTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff38() {
        return closeCursor(cEff38);
    }

    public ITitCont fetchCEff38(ITitCont iTitCont) {
        return fetch(cEff38, iTitCont, selectByTitDsRigaRm);
    }

    public ITitCont selectRec14(ITitCont iTitCont) {
        return buildQuery("selectRec14").bind(iTitCont).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public DbAccessStatus openCCpz38(ITitCont iTitCont) {
        cCpz38 = buildQuery("openCCpz38").bind(iTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz38() {
        return closeCursor(cCpz38);
    }

    public ITitCont fetchCCpz38(ITitCont iTitCont) {
        return fetch(cCpz38, iTitCont, selectByTitDsRigaRm);
    }

    public ITitCont selectRec15(ITitCont iTitCont) {
        return buildQuery("selectRec15").bind(iTitCont).rowMapper(selectRec15Rm).singleResult(iTitCont);
    }

    public ITitCont selectRec16(ITitCont iTitCont) {
        return buildQuery("selectRec16").bind(iTitCont).rowMapper(selectRec15Rm).singleResult(iTitCont);
    }

    public ITitCont selectRec17(ITitCont iTitCont) {
        return buildQuery("selectRec17").bind(iTitCont).rowMapper(selectRec17Rm).singleResult(iTitCont);
    }

    public ITitCont selectRec18(ITitCont iTitCont) {
        return buildQuery("selectRec18").bind(iTitCont).rowMapper(selectRec17Rm).singleResult(iTitCont);
    }

    public AfDecimal selectRec19(ITitCont iTitCont, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec19").bind(iTitCont).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec20(ITitCont iTitCont, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec20").bind(iTitCont).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public DbAccessStatus openCEff50(ITitCont iTitCont) {
        cEff50 = buildQuery("openCEff50").bind(iTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff50() {
        return closeCursor(cEff50);
    }

    public ITitCont fetchCEff50(ITitCont iTitCont) {
        return fetch(cEff50, iTitCont, selectRec9Rm);
    }

    public ITitCont selectRec21(ITitCont iTitCont) {
        return buildQuery("selectRec21").bind(iTitCont).rowMapper(selectByTitDsRigaRm).singleResult(iTitCont);
    }

    public DbAccessStatus openCEff53(ITitCont iTitCont) {
        cEff53 = buildQuery("openCEff53").bind(iTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff53() {
        return closeCursor(cEff53);
    }

    public ITitCont fetchCEff53(ITitCont iTitCont) {
        return fetch(cEff53, iTitCont, selectByTitDsRigaRm);
    }
}
