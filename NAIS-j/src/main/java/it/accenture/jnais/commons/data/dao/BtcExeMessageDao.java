package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBtcExeMessage;

/**
 * Data Access Object(DAO) for table [BTC_EXE_MESSAGE]
 * 
 */
public class BtcExeMessageDao extends BaseSqlDao<IBtcExeMessage> {

    private final IRowMapper<IBtcExeMessage> selectRecRm = buildNamedRowMapper(IBtcExeMessage.class, "idBatch", "idJob", "idExecution", "idMessage", "codMessage", "messageVchar");

    public BtcExeMessageDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBtcExeMessage> getToClass() {
        return IBtcExeMessage.class;
    }

    public IBtcExeMessage selectRec(int idBatch, int idJob, int idExecution, int idMessage, IBtcExeMessage iBtcExeMessage) {
        return buildQuery("selectRec").bind("idBatch", idBatch).bind("idJob", idJob).bind("idExecution", idExecution).bind("idMessage", idMessage).rowMapper(selectRecRm).singleResult(iBtcExeMessage);
    }

    public DbAccessStatus insertRec(IBtcExeMessage iBtcExeMessage) {
        return buildQuery("insertRec").bind(iBtcExeMessage).executeInsert();
    }

    public DbAccessStatus updateRec(IBtcExeMessage iBtcExeMessage) {
        return buildQuery("updateRec").bind(iBtcExeMessage).executeUpdate();
    }

    public DbAccessStatus deleteRec(int batch, int job, int execution, int message) {
        return buildQuery("deleteRec").bind("batch", batch).bind("job", job).bind("execution", execution).bind("message", message).executeDelete();
    }
}
