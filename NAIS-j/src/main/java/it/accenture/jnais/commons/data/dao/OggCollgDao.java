package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IOggCollg;

/**
 * Data Access Object(DAO) for table [OGG_COLLG]
 * 
 */
public class OggCollgDao extends BaseSqlDao<IOggCollg> {

    private Cursor cIdUpdEffOco;
    private Cursor cIbsEffOco0;
    private Cursor cIbsEffOco1;
    private Cursor cIbsCpzOco0;
    private Cursor cIbsCpzOco1;
    private Cursor cEff3;
    private Cursor cCpz3;
    private final IRowMapper<IOggCollg> selectByOcoDsRigaRm = buildNamedRowMapper(IOggCollg.class, "idOggCollg", "idOggCoinv", "tpOggCoinv", "idOggDer", "tpOggDer", "ibRiftoEstnoObj", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codProd", "dtScadDbObj", "tpCollgm", "tpTrasfObj", "recProvObj", "dtDecorDbObj", "dtUltPrePagDbObj", "totPreObj", "risMatObj", "risZilObj", "impTrasfObj", "impReinvstObj", "pcReinvstRilieviObj", "ib2oRiftoEstnoObj", "indLiqAggManObj", "ocoDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "impCollgObj", "prePerTrasfObj", "carAcqObj", "pre1aAnnualitaObj", "impTrasferitoObj", "tpModAbbinamentoObj", "pcPreTrasferitoObj");
    private final IRowMapper<IOggCollg> selectRec8Rm = buildNamedRowMapper(IOggCollg.class, "ldbv5571TotImpCollObj");

    public OggCollgDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IOggCollg> getToClass() {
        return IOggCollg.class;
    }

    public IOggCollg selectByOcoDsRiga(long ocoDsRiga, IOggCollg iOggCollg) {
        return buildQuery("selectByOcoDsRiga").bind("ocoDsRiga", ocoDsRiga).rowMapper(selectByOcoDsRigaRm).singleResult(iOggCollg);
    }

    public DbAccessStatus insertRec(IOggCollg iOggCollg) {
        return buildQuery("insertRec").bind(iOggCollg).executeInsert();
    }

    public DbAccessStatus updateRec(IOggCollg iOggCollg) {
        return buildQuery("updateRec").bind(iOggCollg).executeUpdate();
    }

    public DbAccessStatus deleteByOcoDsRiga(long ocoDsRiga) {
        return buildQuery("deleteByOcoDsRiga").bind("ocoDsRiga", ocoDsRiga).executeDelete();
    }

    public IOggCollg selectRec(int ocoIdOggCollg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggCollg iOggCollg) {
        return buildQuery("selectRec").bind("ocoIdOggCollg", ocoIdOggCollg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByOcoDsRigaRm).singleResult(iOggCollg);
    }

    public DbAccessStatus updateRec1(IOggCollg iOggCollg) {
        return buildQuery("updateRec1").bind(iOggCollg).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffOco(int ocoIdOggCollg, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffOco = buildQuery("openCIdUpdEffOco").bind("ocoIdOggCollg", ocoIdOggCollg).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffOco() {
        return closeCursor(cIdUpdEffOco);
    }

    public IOggCollg fetchCIdUpdEffOco(IOggCollg iOggCollg) {
        return fetch(cIdUpdEffOco, iOggCollg, selectByOcoDsRigaRm);
    }

    public IOggCollg selectRec1(String ocoIbRiftoEstno, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggCollg iOggCollg) {
        return buildQuery("selectRec1").bind("ocoIbRiftoEstno", ocoIbRiftoEstno).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByOcoDsRigaRm).singleResult(iOggCollg);
    }

    public IOggCollg selectRec2(String ocoIb2oRiftoEstno, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggCollg iOggCollg) {
        return buildQuery("selectRec2").bind("ocoIb2oRiftoEstno", ocoIb2oRiftoEstno).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByOcoDsRigaRm).singleResult(iOggCollg);
    }

    public DbAccessStatus openCIbsEffOco0(String ocoIbRiftoEstno, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffOco0 = buildQuery("openCIbsEffOco0").bind("ocoIbRiftoEstno", ocoIbRiftoEstno).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus openCIbsEffOco1(String ocoIb2oRiftoEstno, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffOco1 = buildQuery("openCIbsEffOco1").bind("ocoIb2oRiftoEstno", ocoIb2oRiftoEstno).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsEffOco0() {
        return closeCursor(cIbsEffOco0);
    }

    public DbAccessStatus closeCIbsEffOco1() {
        return closeCursor(cIbsEffOco1);
    }

    public IOggCollg fetchCIbsEffOco0(IOggCollg iOggCollg) {
        return fetch(cIbsEffOco0, iOggCollg, selectByOcoDsRigaRm);
    }

    public IOggCollg fetchCIbsEffOco1(IOggCollg iOggCollg) {
        return fetch(cIbsEffOco1, iOggCollg, selectByOcoDsRigaRm);
    }

    public IOggCollg selectRec3(int ocoIdOggCollg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IOggCollg iOggCollg) {
        return buildQuery("selectRec3").bind("ocoIdOggCollg", ocoIdOggCollg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByOcoDsRigaRm).singleResult(iOggCollg);
    }

    public IOggCollg selectRec4(String ocoIbRiftoEstno, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IOggCollg iOggCollg) {
        return buildQuery("selectRec4").bind("ocoIbRiftoEstno", ocoIbRiftoEstno).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByOcoDsRigaRm).singleResult(iOggCollg);
    }

    public IOggCollg selectRec5(String ocoIb2oRiftoEstno, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IOggCollg iOggCollg) {
        return buildQuery("selectRec5").bind("ocoIb2oRiftoEstno", ocoIb2oRiftoEstno).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByOcoDsRigaRm).singleResult(iOggCollg);
    }

    public DbAccessStatus openCIbsCpzOco0(String ocoIbRiftoEstno, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzOco0 = buildQuery("openCIbsCpzOco0").bind("ocoIbRiftoEstno", ocoIbRiftoEstno).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus openCIbsCpzOco1(String ocoIb2oRiftoEstno, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzOco1 = buildQuery("openCIbsCpzOco1").bind("ocoIb2oRiftoEstno", ocoIb2oRiftoEstno).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsCpzOco0() {
        return closeCursor(cIbsCpzOco0);
    }

    public DbAccessStatus closeCIbsCpzOco1() {
        return closeCursor(cIbsCpzOco1);
    }

    public IOggCollg fetchCIbsCpzOco0(IOggCollg iOggCollg) {
        return fetch(cIbsCpzOco0, iOggCollg, selectByOcoDsRigaRm);
    }

    public IOggCollg fetchCIbsCpzOco1(IOggCollg iOggCollg) {
        return fetch(cIbsCpzOco1, iOggCollg, selectByOcoDsRigaRm);
    }

    public IOggCollg selectRec6(int ldbv0721IdOggCollg, String ldbv0721TpOggCollg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IOggCollg iOggCollg) {
        return buildQuery("selectRec6").bind("ldbv0721IdOggCollg", ldbv0721IdOggCollg).bind("ldbv0721TpOggCollg", ldbv0721TpOggCollg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByOcoDsRigaRm).singleResult(iOggCollg);
    }

    public DbAccessStatus openCEff3(int ldbv0721IdOggCollg, String ldbv0721TpOggCollg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff3 = buildQuery("openCEff3").bind("ldbv0721IdOggCollg", ldbv0721IdOggCollg).bind("ldbv0721TpOggCollg", ldbv0721TpOggCollg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff3() {
        return closeCursor(cEff3);
    }

    public IOggCollg fetchCEff3(IOggCollg iOggCollg) {
        return fetch(cEff3, iOggCollg, selectByOcoDsRigaRm);
    }

    public IOggCollg selectRec7(IOggCollg iOggCollg) {
        return buildQuery("selectRec7").bind(iOggCollg).rowMapper(selectByOcoDsRigaRm).singleResult(iOggCollg);
    }

    public DbAccessStatus openCCpz3(IOggCollg iOggCollg) {
        cCpz3 = buildQuery("openCCpz3").bind(iOggCollg).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz3() {
        return closeCursor(cCpz3);
    }

    public IOggCollg fetchCCpz3(IOggCollg iOggCollg) {
        return fetch(cCpz3, iOggCollg, selectByOcoDsRigaRm);
    }

    public IOggCollg selectRec8(IOggCollg iOggCollg) {
        return buildQuery("selectRec8").bind(iOggCollg).rowMapper(selectRec8Rm).singleResult(iOggCollg);
    }

    public IOggCollg selectRec9(IOggCollg iOggCollg) {
        return buildQuery("selectRec9").bind(iOggCollg).rowMapper(selectRec8Rm).singleResult(iOggCollg);
    }
}
