package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [RAPP_ANA]
 * 
 */
public interface IRappAna extends BaseSqlTo {

    /**
     * Host Variable RAN-ID-OGG
     * 
     */
    int getRanIdOgg();

    void setRanIdOgg(int ranIdOgg);

    /**
     * Host Variable RAN-TP-OGG
     * 
     */
    String getRanTpOgg();

    void setRanTpOgg(String ranTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBV1241-ID-OGG
     * 
     */
    int getLdbv1241IdOgg();

    void setLdbv1241IdOgg(int ldbv1241IdOgg);

    /**
     * Host Variable LDBV1241-TP-OGG
     * 
     */
    String getLdbv1241TpOgg();

    void setLdbv1241TpOgg(String ldbv1241TpOgg);

    /**
     * Host Variable LDBV1241-TP-RAPP-ANA
     * 
     */
    String getLdbv1241TpRappAna();

    void setLdbv1241TpRappAna(String ldbv1241TpRappAna);

    /**
     * Host Variable LDBV1291-ID-OGG
     * 
     */
    int getLdbv1291IdOgg();

    void setLdbv1291IdOgg(int ldbv1291IdOgg);

    /**
     * Host Variable LDBV1291-TP-OGG
     * 
     */
    String getLdbv1291TpOgg();

    void setLdbv1291TpOgg(String ldbv1291TpOgg);

    /**
     * Host Variable LDBV1291-TP-1
     * 
     */
    String getLdbv1291Tp1();

    void setLdbv1291Tp1(String ldbv1291Tp1);

    /**
     * Host Variable LDBV1291-TP-2
     * 
     */
    String getLdbv1291Tp2();

    void setLdbv1291Tp2(String ldbv1291Tp2);

    /**
     * Host Variable LDBV1291-TP-3
     * 
     */
    String getLdbv1291Tp3();

    void setLdbv1291Tp3(String ldbv1291Tp3);

    /**
     * Host Variable LDBV1291-TP-4
     * 
     */
    String getLdbv1291Tp4();

    void setLdbv1291Tp4(String ldbv1291Tp4);

    /**
     * Host Variable LDBV1291-TP-5
     * 
     */
    String getLdbv1291Tp5();

    void setLdbv1291Tp5(String ldbv1291Tp5);

    /**
     * Host Variable LDBV1291-TP-6
     * 
     */
    String getLdbv1291Tp6();

    void setLdbv1291Tp6(String ldbv1291Tp6);

    /**
     * Host Variable LDBV1291-TP-7
     * 
     */
    String getLdbv1291Tp7();

    void setLdbv1291Tp7(String ldbv1291Tp7);

    /**
     * Host Variable LDBV1291-TP-8
     * 
     */
    String getLdbv1291Tp8();

    void setLdbv1291Tp8(String ldbv1291Tp8);

    /**
     * Host Variable LDBV1291-TP-9
     * 
     */
    String getLdbv1291Tp9();

    void setLdbv1291Tp9(String ldbv1291Tp9);

    /**
     * Host Variable LDBV1291-TP-10
     * 
     */
    String getLdbv1291Tp10();

    void setLdbv1291Tp10(String ldbv1291Tp10);

    /**
     * Host Variable LDBV1291-TP-11
     * 
     */
    String getLdbv1291Tp11();

    void setLdbv1291Tp11(String ldbv1291Tp11);

    /**
     * Host Variable RAN-COD-SOGG
     * 
     */
    String getRanCodSogg();

    void setRanCodSogg(String ranCodSogg);

    /**
     * Host Variable RAN-TP-RAPP-ANA
     * 
     */
    String getRanTpRappAna();

    void setRanTpRappAna(String ranTpRappAna);

    /**
     * Host Variable RAN-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Host Variable RAN-ID-RAPP-ANA-COLLG
     * 
     */
    int getIdRappAnaCollg();

    void setIdRappAnaCollg(int idRappAnaCollg);

    /**
     * Nullable property for RAN-ID-RAPP-ANA-COLLG
     * 
     */
    Integer getIdRappAnaCollgObj();

    void setIdRappAnaCollgObj(Integer idRappAnaCollgObj);

    /**
     * Host Variable RAN-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable RAN-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for RAN-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable RAN-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable RAN-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable RAN-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Nullable property for RAN-COD-SOGG
     * 
     */
    String getRanCodSoggObj();

    void setRanCodSoggObj(String ranCodSoggObj);

    /**
     * Host Variable RAN-TP-PERS
     * 
     */
    char getTpPers();

    void setTpPers(char tpPers);

    /**
     * Nullable property for RAN-TP-PERS
     * 
     */
    Character getTpPersObj();

    void setTpPersObj(Character tpPersObj);

    /**
     * Host Variable RAN-SEX
     * 
     */
    char getSex();

    void setSex(char sex);

    /**
     * Nullable property for RAN-SEX
     * 
     */
    Character getSexObj();

    void setSexObj(Character sexObj);

    /**
     * Host Variable RAN-DT-NASC-DB
     * 
     */
    String getDtNascDb();

    void setDtNascDb(String dtNascDb);

    /**
     * Nullable property for RAN-DT-NASC-DB
     * 
     */
    String getDtNascDbObj();

    void setDtNascDbObj(String dtNascDbObj);

    /**
     * Host Variable RAN-FL-ESTAS
     * 
     */
    char getFlEstas();

    void setFlEstas(char flEstas);

    /**
     * Nullable property for RAN-FL-ESTAS
     * 
     */
    Character getFlEstasObj();

    void setFlEstasObj(Character flEstasObj);

    /**
     * Host Variable RAN-INDIR-1
     * 
     */
    String getIndir1();

    void setIndir1(String indir1);

    /**
     * Nullable property for RAN-INDIR-1
     * 
     */
    String getIndir1Obj();

    void setIndir1Obj(String indir1Obj);

    /**
     * Host Variable RAN-INDIR-2
     * 
     */
    String getIndir2();

    void setIndir2(String indir2);

    /**
     * Nullable property for RAN-INDIR-2
     * 
     */
    String getIndir2Obj();

    void setIndir2Obj(String indir2Obj);

    /**
     * Host Variable RAN-INDIR-3
     * 
     */
    String getIndir3();

    void setIndir3(String indir3);

    /**
     * Nullable property for RAN-INDIR-3
     * 
     */
    String getIndir3Obj();

    void setIndir3Obj(String indir3Obj);

    /**
     * Host Variable RAN-TP-UTLZ-INDIR-1
     * 
     */
    String getTpUtlzIndir1();

    void setTpUtlzIndir1(String tpUtlzIndir1);

    /**
     * Nullable property for RAN-TP-UTLZ-INDIR-1
     * 
     */
    String getTpUtlzIndir1Obj();

    void setTpUtlzIndir1Obj(String tpUtlzIndir1Obj);

    /**
     * Host Variable RAN-TP-UTLZ-INDIR-2
     * 
     */
    String getTpUtlzIndir2();

    void setTpUtlzIndir2(String tpUtlzIndir2);

    /**
     * Nullable property for RAN-TP-UTLZ-INDIR-2
     * 
     */
    String getTpUtlzIndir2Obj();

    void setTpUtlzIndir2Obj(String tpUtlzIndir2Obj);

    /**
     * Host Variable RAN-TP-UTLZ-INDIR-3
     * 
     */
    String getTpUtlzIndir3();

    void setTpUtlzIndir3(String tpUtlzIndir3);

    /**
     * Nullable property for RAN-TP-UTLZ-INDIR-3
     * 
     */
    String getTpUtlzIndir3Obj();

    void setTpUtlzIndir3Obj(String tpUtlzIndir3Obj);

    /**
     * Host Variable RAN-ESTR-CNT-CORR-ACCR
     * 
     */
    String getEstrCntCorrAccr();

    void setEstrCntCorrAccr(String estrCntCorrAccr);

    /**
     * Nullable property for RAN-ESTR-CNT-CORR-ACCR
     * 
     */
    String getEstrCntCorrAccrObj();

    void setEstrCntCorrAccrObj(String estrCntCorrAccrObj);

    /**
     * Host Variable RAN-ESTR-CNT-CORR-ADD
     * 
     */
    String getEstrCntCorrAdd();

    void setEstrCntCorrAdd(String estrCntCorrAdd);

    /**
     * Nullable property for RAN-ESTR-CNT-CORR-ADD
     * 
     */
    String getEstrCntCorrAddObj();

    void setEstrCntCorrAddObj(String estrCntCorrAddObj);

    /**
     * Host Variable RAN-ESTR-DOCTO
     * 
     */
    String getEstrDocto();

    void setEstrDocto(String estrDocto);

    /**
     * Nullable property for RAN-ESTR-DOCTO
     * 
     */
    String getEstrDoctoObj();

    void setEstrDoctoObj(String estrDoctoObj);

    /**
     * Host Variable RAN-PC-NEL-RAPP
     * 
     */
    AfDecimal getPcNelRapp();

    void setPcNelRapp(AfDecimal pcNelRapp);

    /**
     * Nullable property for RAN-PC-NEL-RAPP
     * 
     */
    AfDecimal getPcNelRappObj();

    void setPcNelRappObj(AfDecimal pcNelRappObj);

    /**
     * Host Variable RAN-TP-MEZ-PAG-ADD
     * 
     */
    String getTpMezPagAdd();

    void setTpMezPagAdd(String tpMezPagAdd);

    /**
     * Nullable property for RAN-TP-MEZ-PAG-ADD
     * 
     */
    String getTpMezPagAddObj();

    void setTpMezPagAddObj(String tpMezPagAddObj);

    /**
     * Host Variable RAN-TP-MEZ-PAG-ACCR
     * 
     */
    String getTpMezPagAccr();

    void setTpMezPagAccr(String tpMezPagAccr);

    /**
     * Nullable property for RAN-TP-MEZ-PAG-ACCR
     * 
     */
    String getTpMezPagAccrObj();

    void setTpMezPagAccrObj(String tpMezPagAccrObj);

    /**
     * Host Variable RAN-COD-MATR
     * 
     */
    String getCodMatr();

    void setCodMatr(String codMatr);

    /**
     * Nullable property for RAN-COD-MATR
     * 
     */
    String getCodMatrObj();

    void setCodMatrObj(String codMatrObj);

    /**
     * Host Variable RAN-TP-ADEGZ
     * 
     */
    String getTpAdegz();

    void setTpAdegz(String tpAdegz);

    /**
     * Nullable property for RAN-TP-ADEGZ
     * 
     */
    String getTpAdegzObj();

    void setTpAdegzObj(String tpAdegzObj);

    /**
     * Host Variable RAN-FL-TST-RSH
     * 
     */
    char getFlTstRsh();

    void setFlTstRsh(char flTstRsh);

    /**
     * Nullable property for RAN-FL-TST-RSH
     * 
     */
    Character getFlTstRshObj();

    void setFlTstRshObj(Character flTstRshObj);

    /**
     * Host Variable RAN-COD-AZ
     * 
     */
    String getCodAz();

    void setCodAz(String codAz);

    /**
     * Nullable property for RAN-COD-AZ
     * 
     */
    String getCodAzObj();

    void setCodAzObj(String codAzObj);

    /**
     * Host Variable RAN-IND-PRINC
     * 
     */
    String getIndPrinc();

    void setIndPrinc(String indPrinc);

    /**
     * Nullable property for RAN-IND-PRINC
     * 
     */
    String getIndPrincObj();

    void setIndPrincObj(String indPrincObj);

    /**
     * Host Variable RAN-DT-DELIBERA-CDA-DB
     * 
     */
    String getDtDeliberaCdaDb();

    void setDtDeliberaCdaDb(String dtDeliberaCdaDb);

    /**
     * Nullable property for RAN-DT-DELIBERA-CDA-DB
     * 
     */
    String getDtDeliberaCdaDbObj();

    void setDtDeliberaCdaDbObj(String dtDeliberaCdaDbObj);

    /**
     * Host Variable RAN-DLG-AL-RISC
     * 
     */
    char getDlgAlRisc();

    void setDlgAlRisc(char dlgAlRisc);

    /**
     * Nullable property for RAN-DLG-AL-RISC
     * 
     */
    Character getDlgAlRiscObj();

    void setDlgAlRiscObj(Character dlgAlRiscObj);

    /**
     * Host Variable RAN-LEGALE-RAPPR-PRINC
     * 
     */
    char getLegaleRapprPrinc();

    void setLegaleRapprPrinc(char legaleRapprPrinc);

    /**
     * Nullable property for RAN-LEGALE-RAPPR-PRINC
     * 
     */
    Character getLegaleRapprPrincObj();

    void setLegaleRapprPrincObj(Character legaleRapprPrincObj);

    /**
     * Host Variable RAN-TP-LEGALE-RAPPR
     * 
     */
    String getTpLegaleRappr();

    void setTpLegaleRappr(String tpLegaleRappr);

    /**
     * Nullable property for RAN-TP-LEGALE-RAPPR
     * 
     */
    String getTpLegaleRapprObj();

    void setTpLegaleRapprObj(String tpLegaleRapprObj);

    /**
     * Host Variable RAN-TP-IND-PRINC
     * 
     */
    String getTpIndPrinc();

    void setTpIndPrinc(String tpIndPrinc);

    /**
     * Nullable property for RAN-TP-IND-PRINC
     * 
     */
    String getTpIndPrincObj();

    void setTpIndPrincObj(String tpIndPrincObj);

    /**
     * Host Variable RAN-TP-STAT-RID
     * 
     */
    String getTpStatRid();

    void setTpStatRid(String tpStatRid);

    /**
     * Nullable property for RAN-TP-STAT-RID
     * 
     */
    String getTpStatRidObj();

    void setTpStatRidObj(String tpStatRidObj);

    /**
     * Host Variable RAN-NOME-INT-RID-VCHAR
     * 
     */
    String getNomeIntRidVchar();

    void setNomeIntRidVchar(String nomeIntRidVchar);

    /**
     * Nullable property for RAN-NOME-INT-RID-VCHAR
     * 
     */
    String getNomeIntRidVcharObj();

    void setNomeIntRidVcharObj(String nomeIntRidVcharObj);

    /**
     * Host Variable RAN-COGN-INT-RID-VCHAR
     * 
     */
    String getCognIntRidVchar();

    void setCognIntRidVchar(String cognIntRidVchar);

    /**
     * Nullable property for RAN-COGN-INT-RID-VCHAR
     * 
     */
    String getCognIntRidVcharObj();

    void setCognIntRidVcharObj(String cognIntRidVcharObj);

    /**
     * Host Variable RAN-COGN-INT-TRATT-VCHAR
     * 
     */
    String getCognIntTrattVchar();

    void setCognIntTrattVchar(String cognIntTrattVchar);

    /**
     * Nullable property for RAN-COGN-INT-TRATT-VCHAR
     * 
     */
    String getCognIntTrattVcharObj();

    void setCognIntTrattVcharObj(String cognIntTrattVcharObj);

    /**
     * Host Variable RAN-NOME-INT-TRATT-VCHAR
     * 
     */
    String getNomeIntTrattVchar();

    void setNomeIntTrattVchar(String nomeIntTrattVchar);

    /**
     * Nullable property for RAN-NOME-INT-TRATT-VCHAR
     * 
     */
    String getNomeIntTrattVcharObj();

    void setNomeIntTrattVcharObj(String nomeIntTrattVcharObj);

    /**
     * Host Variable RAN-CF-INT-RID
     * 
     */
    String getCfIntRid();

    void setCfIntRid(String cfIntRid);

    /**
     * Nullable property for RAN-CF-INT-RID
     * 
     */
    String getCfIntRidObj();

    void setCfIntRidObj(String cfIntRidObj);

    /**
     * Host Variable RAN-FL-COINC-DIP-CNTR
     * 
     */
    char getFlCoincDipCntr();

    void setFlCoincDipCntr(char flCoincDipCntr);

    /**
     * Nullable property for RAN-FL-COINC-DIP-CNTR
     * 
     */
    Character getFlCoincDipCntrObj();

    void setFlCoincDipCntrObj(Character flCoincDipCntrObj);

    /**
     * Host Variable RAN-FL-COINC-INT-CNTR
     * 
     */
    char getFlCoincIntCntr();

    void setFlCoincIntCntr(char flCoincIntCntr);

    /**
     * Nullable property for RAN-FL-COINC-INT-CNTR
     * 
     */
    Character getFlCoincIntCntrObj();

    void setFlCoincIntCntrObj(Character flCoincIntCntrObj);

    /**
     * Host Variable RAN-DT-DECES-DB
     * 
     */
    String getDtDecesDb();

    void setDtDecesDb(String dtDecesDb);

    /**
     * Nullable property for RAN-DT-DECES-DB
     * 
     */
    String getDtDecesDbObj();

    void setDtDecesDbObj(String dtDecesDbObj);

    /**
     * Host Variable RAN-FL-FUMATORE
     * 
     */
    char getFlFumatore();

    void setFlFumatore(char flFumatore);

    /**
     * Nullable property for RAN-FL-FUMATORE
     * 
     */
    Character getFlFumatoreObj();

    void setFlFumatoreObj(Character flFumatoreObj);

    /**
     * Host Variable RAN-DS-RIGA
     * 
     */
    long getRanDsRiga();

    void setRanDsRiga(long ranDsRiga);

    /**
     * Host Variable RAN-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable RAN-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable RAN-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable RAN-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable RAN-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable RAN-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable RAN-FL-LAV-DIP
     * 
     */
    char getFlLavDip();

    void setFlLavDip(char flLavDip);

    /**
     * Nullable property for RAN-FL-LAV-DIP
     * 
     */
    Character getFlLavDipObj();

    void setFlLavDipObj(Character flLavDipObj);

    /**
     * Host Variable RAN-TP-VARZ-PAGAT
     * 
     */
    String getTpVarzPagat();

    void setTpVarzPagat(String tpVarzPagat);

    /**
     * Nullable property for RAN-TP-VARZ-PAGAT
     * 
     */
    String getTpVarzPagatObj();

    void setTpVarzPagatObj(String tpVarzPagatObj);

    /**
     * Host Variable RAN-COD-RID
     * 
     */
    String getCodRid();

    void setCodRid(String codRid);

    /**
     * Nullable property for RAN-COD-RID
     * 
     */
    String getCodRidObj();

    void setCodRidObj(String codRidObj);

    /**
     * Host Variable RAN-TP-CAUS-RID
     * 
     */
    String getTpCausRid();

    void setTpCausRid(String tpCausRid);

    /**
     * Nullable property for RAN-TP-CAUS-RID
     * 
     */
    String getTpCausRidObj();

    void setTpCausRidObj(String tpCausRidObj);

    /**
     * Host Variable RAN-IND-MASSA-CORP
     * 
     */
    String getIndMassaCorp();

    void setIndMassaCorp(String indMassaCorp);

    /**
     * Nullable property for RAN-IND-MASSA-CORP
     * 
     */
    String getIndMassaCorpObj();

    void setIndMassaCorpObj(String indMassaCorpObj);

    /**
     * Host Variable RAN-CAT-RSH-PROF
     * 
     */
    String getCatRshProf();

    void setCatRshProf(String catRshProf);

    /**
     * Nullable property for RAN-CAT-RSH-PROF
     * 
     */
    String getCatRshProfObj();

    void setCatRshProfObj(String catRshProfObj);
};
