package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [TRCH_LIQ]
 * 
 */
public interface ITrchLiq extends BaseSqlTo {

    /**
     * Host Variable TLI-ID-TRCH-LIQ
     * 
     */
    int getIdTrchLiq();

    void setIdTrchLiq(int idTrchLiq);

    /**
     * Host Variable TLI-ID-GAR-LIQ
     * 
     */
    int getIdGarLiq();

    void setIdGarLiq(int idGarLiq);

    /**
     * Nullable property for TLI-ID-GAR-LIQ
     * 
     */
    Integer getIdGarLiqObj();

    void setIdGarLiqObj(Integer idGarLiqObj);

    /**
     * Host Variable TLI-ID-LIQ
     * 
     */
    int getIdLiq();

    void setIdLiq(int idLiq);

    /**
     * Host Variable TLI-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Host Variable TLI-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable TLI-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for TLI-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable TLI-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable TLI-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable TLI-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable TLI-IMP-LRD-CALC
     * 
     */
    AfDecimal getImpLrdCalc();

    void setImpLrdCalc(AfDecimal impLrdCalc);

    /**
     * Host Variable TLI-IMP-LRD-DFZ
     * 
     */
    AfDecimal getImpLrdDfz();

    void setImpLrdDfz(AfDecimal impLrdDfz);

    /**
     * Nullable property for TLI-IMP-LRD-DFZ
     * 
     */
    AfDecimal getImpLrdDfzObj();

    void setImpLrdDfzObj(AfDecimal impLrdDfzObj);

    /**
     * Host Variable TLI-IMP-LRD-EFFLQ
     * 
     */
    AfDecimal getImpLrdEfflq();

    void setImpLrdEfflq(AfDecimal impLrdEfflq);

    /**
     * Host Variable TLI-IMP-NET-CALC
     * 
     */
    AfDecimal getImpNetCalc();

    void setImpNetCalc(AfDecimal impNetCalc);

    /**
     * Host Variable TLI-IMP-NET-DFZ
     * 
     */
    AfDecimal getImpNetDfz();

    void setImpNetDfz(AfDecimal impNetDfz);

    /**
     * Nullable property for TLI-IMP-NET-DFZ
     * 
     */
    AfDecimal getImpNetDfzObj();

    void setImpNetDfzObj(AfDecimal impNetDfzObj);

    /**
     * Host Variable TLI-IMP-NET-EFFLQ
     * 
     */
    AfDecimal getImpNetEfflq();

    void setImpNetEfflq(AfDecimal impNetEfflq);

    /**
     * Host Variable TLI-IMP-UTI
     * 
     */
    AfDecimal getImpUti();

    void setImpUti(AfDecimal impUti);

    /**
     * Nullable property for TLI-IMP-UTI
     * 
     */
    AfDecimal getImpUtiObj();

    void setImpUtiObj(AfDecimal impUtiObj);

    /**
     * Host Variable TLI-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Nullable property for TLI-COD-TARI
     * 
     */
    String getCodTariObj();

    void setCodTariObj(String codTariObj);

    /**
     * Host Variable TLI-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for TLI-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable TLI-IAS-ONER-PRVNT-FIN
     * 
     */
    AfDecimal getIasOnerPrvntFin();

    void setIasOnerPrvntFin(AfDecimal iasOnerPrvntFin);

    /**
     * Nullable property for TLI-IAS-ONER-PRVNT-FIN
     * 
     */
    AfDecimal getIasOnerPrvntFinObj();

    void setIasOnerPrvntFinObj(AfDecimal iasOnerPrvntFinObj);

    /**
     * Host Variable TLI-IAS-MGG-SIN
     * 
     */
    AfDecimal getIasMggSin();

    void setIasMggSin(AfDecimal iasMggSin);

    /**
     * Nullable property for TLI-IAS-MGG-SIN
     * 
     */
    AfDecimal getIasMggSinObj();

    void setIasMggSinObj(AfDecimal iasMggSinObj);

    /**
     * Host Variable TLI-IAS-RST-DPST
     * 
     */
    AfDecimal getIasRstDpst();

    void setIasRstDpst(AfDecimal iasRstDpst);

    /**
     * Nullable property for TLI-IAS-RST-DPST
     * 
     */
    AfDecimal getIasRstDpstObj();

    void setIasRstDpstObj(AfDecimal iasRstDpstObj);

    /**
     * Host Variable TLI-IMP-RIMB
     * 
     */
    AfDecimal getImpRimb();

    void setImpRimb(AfDecimal impRimb);

    /**
     * Nullable property for TLI-IMP-RIMB
     * 
     */
    AfDecimal getImpRimbObj();

    void setImpRimbObj(AfDecimal impRimbObj);

    /**
     * Host Variable TLI-COMPON-TAX-RIMB
     * 
     */
    AfDecimal getComponTaxRimb();

    void setComponTaxRimb(AfDecimal componTaxRimb);

    /**
     * Nullable property for TLI-COMPON-TAX-RIMB
     * 
     */
    AfDecimal getComponTaxRimbObj();

    void setComponTaxRimbObj(AfDecimal componTaxRimbObj);

    /**
     * Host Variable TLI-RIS-MAT
     * 
     */
    AfDecimal getRisMat();

    void setRisMat(AfDecimal risMat);

    /**
     * Nullable property for TLI-RIS-MAT
     * 
     */
    AfDecimal getRisMatObj();

    void setRisMatObj(AfDecimal risMatObj);

    /**
     * Host Variable TLI-RIS-SPE
     * 
     */
    AfDecimal getRisSpe();

    void setRisSpe(AfDecimal risSpe);

    /**
     * Nullable property for TLI-RIS-SPE
     * 
     */
    AfDecimal getRisSpeObj();

    void setRisSpeObj(AfDecimal risSpeObj);

    /**
     * Host Variable TLI-DS-RIGA
     * 
     */
    long getTliDsRiga();

    void setTliDsRiga(long tliDsRiga);

    /**
     * Host Variable TLI-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable TLI-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable TLI-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable TLI-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable TLI-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable TLI-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable TLI-IAS-PNL
     * 
     */
    AfDecimal getIasPnl();

    void setIasPnl(AfDecimal iasPnl);

    /**
     * Nullable property for TLI-IAS-PNL
     * 
     */
    AfDecimal getIasPnlObj();

    void setIasPnlObj(AfDecimal iasPnlObj);
};
