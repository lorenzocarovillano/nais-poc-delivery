package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [QUOTZ_AGG_FND]
 * 
 */
public interface IQuotzAggFnd extends BaseSqlTo {

    /**
     * Host Variable L41-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L41-TP-QTZ
     * 
     */
    String getTpQtz();

    void setTpQtz(String tpQtz);

    /**
     * Host Variable L41-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Host Variable L41-DT-QTZ-DB
     * 
     */
    String getDtQtzDb();

    void setDtQtzDb(String dtQtzDb);

    /**
     * Host Variable L41-TP-FND
     * 
     */
    char getTpFnd();

    void setTpFnd(char tpFnd);

    /**
     * Host Variable L41-VAL-QUO
     * 
     */
    AfDecimal getValQuo();

    void setValQuo(AfDecimal valQuo);

    /**
     * Host Variable L41-DESC-AGG-VCHAR
     * 
     */
    String getDescAggVchar();

    void setDescAggVchar(String descAggVchar);

    /**
     * Nullable property for L41-DESC-AGG-VCHAR
     * 
     */
    String getDescAggVcharObj();

    void setDescAggVcharObj(String descAggVcharObj);

    /**
     * Host Variable L41-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L41-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L41-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable L41-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable L41-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
