package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [ACC_COMM]
 * 
 */
public interface IAccComm extends BaseSqlTo {

    /**
     * Host Variable P63-ID-ACC-COMM
     * 
     */
    int getP63IdAccComm();

    void setP63IdAccComm(int p63IdAccComm);

    /**
     * Host Variable P63-TP-ACC-COMM
     * 
     */
    String getTpAccComm();

    void setTpAccComm(String tpAccComm);

    /**
     * Host Variable P63-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P63-COD-PARTNER
     * 
     */
    long getCodPartner();

    void setCodPartner(long codPartner);

    /**
     * Host Variable P63-IB-ACC-COMM
     * 
     */
    String getIbAccComm();

    void setIbAccComm(String ibAccComm);

    /**
     * Host Variable P63-IB-ACC-COMM-MASTER
     * 
     */
    String getIbAccCommMaster();

    void setIbAccCommMaster(String ibAccCommMaster);

    /**
     * Host Variable P63-DESC-ACC-COMM-VCHAR
     * 
     */
    String getDescAccCommVchar();

    void setDescAccCommVchar(String descAccCommVchar);

    /**
     * Host Variable P63-DT-FIRMA-DB
     * 
     */
    String getDtFirmaDb();

    void setDtFirmaDb(String dtFirmaDb);

    /**
     * Host Variable P63-DT-INI-VLDT-DB
     * 
     */
    String getDtIniVldtDb();

    void setDtIniVldtDb(String dtIniVldtDb);

    /**
     * Host Variable P63-DT-END-VLDT-DB
     * 
     */
    String getDtEndVldtDb();

    void setDtEndVldtDb(String dtEndVldtDb);

    /**
     * Host Variable P63-FL-INVIO-CONFERME
     * 
     */
    char getFlInvioConferme();

    void setFlInvioConferme(char flInvioConferme);

    /**
     * Host Variable P63-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P63-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P63-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable P63-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P63-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable P63-DESC-ACC-COMM-MAST-VCHAR
     * 
     */
    String getDescAccCommMastVchar();

    void setDescAccCommMastVchar(String descAccCommMastVchar);

    /**
     * Nullable property for P63-DESC-ACC-COMM-MAST-VCHAR
     * 
     */
    String getDescAccCommMastVcharObj();

    void setDescAccCommMastVcharObj(String descAccCommMastVcharObj);
};
