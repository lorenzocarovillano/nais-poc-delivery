package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDettTitDiRat;

/**
 * Data Access Object(DAO) for table [DETT_TIT_DI_RAT]
 * 
 */
public class DettTitDiRatDao extends BaseSqlDao<IDettTitDiRat> {

    private Cursor cIdUpdEffDtr;
    private Cursor cIdpEffDtr;
    private Cursor cIdoEffDtr;
    private Cursor cIdpCpzDtr;
    private Cursor cIdoCpzDtr;
    private Cursor cEff21;
    private Cursor cCpz21;
    private final IRowMapper<IDettTitDiRat> selectByDtrDsRigaRm = buildNamedRowMapper(IDettTitDiRat.class, "idDettTitDiRat", "dtrIdTitRat", "dtrIdOgg", "dtrTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtIniCopDbObj", "dtEndCopDbObj", "preNetObj", "intrFrazObj", "intrMoraObj", "intrRetdtObj", "intrRiatObj", "dirObj", "speMedObj", "speAgeObj", "taxObj", "soprSanObj", "soprSpoObj", "soprTecObj", "soprProfObj", "soprAltObj", "preTotObj", "prePpIasObj", "preSoloRshObj", "carIasObj", "provAcq1aaObj", "provAcq2aaObj", "provRicorObj", "provIncObj", "provDaRecObj", "codDvsObj", "frqMoviObj", "tpRgmFisc", "codTariObj", "tpStatTit", "impAzObj", "impAderObj", "impTfrObj", "impVoloObj", "dtrFlVldtTitObj", "carAcqObj", "carGestObj", "carIncObj", "manfeeAnticObj", "manfeeRicorObj", "manfeeRecObj", "totIntrPrestObj", "dtrDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "impTrasfeObj", "impTfrStrcObj", "acqExpObj", "remunAssObj", "commisInterObj", "cnbtAntiracObj");

    public DettTitDiRatDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDettTitDiRat> getToClass() {
        return IDettTitDiRat.class;
    }

    public IDettTitDiRat selectByDtrDsRiga(long dtrDsRiga, IDettTitDiRat iDettTitDiRat) {
        return buildQuery("selectByDtrDsRiga").bind("dtrDsRiga", dtrDsRiga).rowMapper(selectByDtrDsRigaRm).singleResult(iDettTitDiRat);
    }

    public DbAccessStatus insertRec(IDettTitDiRat iDettTitDiRat) {
        return buildQuery("insertRec").bind(iDettTitDiRat).executeInsert();
    }

    public DbAccessStatus updateRec(IDettTitDiRat iDettTitDiRat) {
        return buildQuery("updateRec").bind(iDettTitDiRat).executeUpdate();
    }

    public DbAccessStatus deleteByDtrDsRiga(long dtrDsRiga) {
        return buildQuery("deleteByDtrDsRiga").bind("dtrDsRiga", dtrDsRiga).executeDelete();
    }

    public IDettTitDiRat selectRec(int dtrIdDettTitDiRat, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDettTitDiRat iDettTitDiRat) {
        return buildQuery("selectRec").bind("dtrIdDettTitDiRat", dtrIdDettTitDiRat).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDtrDsRigaRm).singleResult(iDettTitDiRat);
    }

    public DbAccessStatus updateRec1(IDettTitDiRat iDettTitDiRat) {
        return buildQuery("updateRec1").bind(iDettTitDiRat).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffDtr(int dtrIdDettTitDiRat, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffDtr = buildQuery("openCIdUpdEffDtr").bind("dtrIdDettTitDiRat", dtrIdDettTitDiRat).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffDtr() {
        return closeCursor(cIdUpdEffDtr);
    }

    public IDettTitDiRat fetchCIdUpdEffDtr(IDettTitDiRat iDettTitDiRat) {
        return fetch(cIdUpdEffDtr, iDettTitDiRat, selectByDtrDsRigaRm);
    }

    public IDettTitDiRat selectRec1(int dtrIdTitRat, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDettTitDiRat iDettTitDiRat) {
        return buildQuery("selectRec1").bind("dtrIdTitRat", dtrIdTitRat).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDtrDsRigaRm).singleResult(iDettTitDiRat);
    }

    public DbAccessStatus openCIdpEffDtr(int dtrIdTitRat, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffDtr = buildQuery("openCIdpEffDtr").bind("dtrIdTitRat", dtrIdTitRat).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffDtr() {
        return closeCursor(cIdpEffDtr);
    }

    public IDettTitDiRat fetchCIdpEffDtr(IDettTitDiRat iDettTitDiRat) {
        return fetch(cIdpEffDtr, iDettTitDiRat, selectByDtrDsRigaRm);
    }

    public IDettTitDiRat selectRec2(int dtrIdOgg, String dtrTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDettTitDiRat iDettTitDiRat) {
        return buildQuery("selectRec2").bind("dtrIdOgg", dtrIdOgg).bind("dtrTpOgg", dtrTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDtrDsRigaRm).singleResult(iDettTitDiRat);
    }

    public DbAccessStatus openCIdoEffDtr(int dtrIdOgg, String dtrTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffDtr = buildQuery("openCIdoEffDtr").bind("dtrIdOgg", dtrIdOgg).bind("dtrTpOgg", dtrTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffDtr() {
        return closeCursor(cIdoEffDtr);
    }

    public IDettTitDiRat fetchCIdoEffDtr(IDettTitDiRat iDettTitDiRat) {
        return fetch(cIdoEffDtr, iDettTitDiRat, selectByDtrDsRigaRm);
    }

    public IDettTitDiRat selectRec3(int dtrIdDettTitDiRat, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDettTitDiRat iDettTitDiRat) {
        return buildQuery("selectRec3").bind("dtrIdDettTitDiRat", dtrIdDettTitDiRat).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDtrDsRigaRm).singleResult(iDettTitDiRat);
    }

    public IDettTitDiRat selectRec4(int dtrIdTitRat, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDettTitDiRat iDettTitDiRat) {
        return buildQuery("selectRec4").bind("dtrIdTitRat", dtrIdTitRat).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDtrDsRigaRm).singleResult(iDettTitDiRat);
    }

    public DbAccessStatus openCIdpCpzDtr(int dtrIdTitRat, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzDtr = buildQuery("openCIdpCpzDtr").bind("dtrIdTitRat", dtrIdTitRat).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzDtr() {
        return closeCursor(cIdpCpzDtr);
    }

    public IDettTitDiRat fetchCIdpCpzDtr(IDettTitDiRat iDettTitDiRat) {
        return fetch(cIdpCpzDtr, iDettTitDiRat, selectByDtrDsRigaRm);
    }

    public IDettTitDiRat selectRec5(IDettTitDiRat iDettTitDiRat) {
        return buildQuery("selectRec5").bind(iDettTitDiRat).rowMapper(selectByDtrDsRigaRm).singleResult(iDettTitDiRat);
    }

    public DbAccessStatus openCIdoCpzDtr(IDettTitDiRat iDettTitDiRat) {
        cIdoCpzDtr = buildQuery("openCIdoCpzDtr").bind(iDettTitDiRat).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzDtr() {
        return closeCursor(cIdoCpzDtr);
    }

    public IDettTitDiRat fetchCIdoCpzDtr(IDettTitDiRat iDettTitDiRat) {
        return fetch(cIdoCpzDtr, iDettTitDiRat, selectByDtrDsRigaRm);
    }

    public DbAccessStatus openCEff21(int dtrIdTitRat, char dtrFlVldtTit, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff21 = buildQuery("openCEff21").bind("dtrIdTitRat", dtrIdTitRat).bind("dtrFlVldtTit", String.valueOf(dtrFlVldtTit)).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff21() {
        return closeCursor(cEff21);
    }

    public IDettTitDiRat fetchCEff21(IDettTitDiRat iDettTitDiRat) {
        return fetch(cEff21, iDettTitDiRat, selectByDtrDsRigaRm);
    }

    public DbAccessStatus openCCpz21(IDettTitDiRat iDettTitDiRat) {
        cCpz21 = buildQuery("openCCpz21").bind(iDettTitDiRat).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz21() {
        return closeCursor(cCpz21);
    }

    public IDettTitDiRat fetchCCpz21(IDettTitDiRat iDettTitDiRat) {
        return fetch(cCpz21, iDettTitDiRat, selectByDtrDsRigaRm);
    }
}
