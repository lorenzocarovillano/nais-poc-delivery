package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBilaVarCalcP;

/**
 * Data Access Object(DAO) for table [BILA_VAR_CALC_P]
 * 
 */
public class BilaVarCalcPDao extends BaseSqlDao<IBilaVarCalcP> {

    private final IRowMapper<IBilaVarCalcP> selectByB04IdBilaVarCalcPRm = buildNamedRowMapper(IBilaVarCalcP.class, "b04IdBilaVarCalcP", "codCompAnia", "idRichEstrazMas", "idRichEstrazAggObj", "dtRisDb", "idPoli", "idAdes", "progSchedaValor", "tpRgmFisc", "dtIniVldtProdDb", "codVar", "tpD", "valImpObj", "valPcObj", "valStringaObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "areaDValorVarVcharObj");

    public BilaVarCalcPDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBilaVarCalcP> getToClass() {
        return IBilaVarCalcP.class;
    }

    public IBilaVarCalcP selectByB04IdBilaVarCalcP(int b04IdBilaVarCalcP, IBilaVarCalcP iBilaVarCalcP) {
        return buildQuery("selectByB04IdBilaVarCalcP").bind("b04IdBilaVarCalcP", b04IdBilaVarCalcP).rowMapper(selectByB04IdBilaVarCalcPRm).singleResult(iBilaVarCalcP);
    }

    public DbAccessStatus insertRec(IBilaVarCalcP iBilaVarCalcP) {
        return buildQuery("insertRec").bind(iBilaVarCalcP).executeInsert();
    }

    public DbAccessStatus updateRec(IBilaVarCalcP iBilaVarCalcP) {
        return buildQuery("updateRec").bind(iBilaVarCalcP).executeUpdate();
    }

    public DbAccessStatus deleteByB04IdBilaVarCalcP(int b04IdBilaVarCalcP) {
        return buildQuery("deleteByB04IdBilaVarCalcP").bind("b04IdBilaVarCalcP", b04IdBilaVarCalcP).executeDelete();
    }

    public DbAccessStatus deleteRec(int richEstrazMas, int poli, int ades) {
        return buildQuery("deleteRec").bind("richEstrazMas", richEstrazMas).bind("poli", poli).bind("ades", ades).executeDelete();
    }
}
