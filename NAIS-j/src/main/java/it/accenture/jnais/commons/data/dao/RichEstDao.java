package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IRichEst;

/**
 * Data Access Object(DAO) for table [RICH_EST]
 * 
 */
public class RichEstDao extends BaseSqlDao<IRichEst> {

    private Cursor cIboNstP01;
    private Cursor cIbsNstP010;
    private Cursor cIbsNstP011;
    private Cursor cIdoNstP01;
    private final IRowMapper<IRichEst> selectByP01IdRichEstRm = buildNamedRowMapper(IRichEst.class, "p01IdRichEst", "idRichEstCollgObj", "idLiqObj", "codCompAnia", "ibRichEstObj", "tpMovi", "dtFormRichDb", "dtInvioRichDb", "dtPervRichDb", "dtRgstrzRichDb", "dtEffDbObj", "dtSinDbObj", "tpOgg", "idOgg", "ibOgg", "flModExec", "idRichiedenteObj", "codProd", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "codCan", "ibOggOrigObj", "dtEstFinanzDbObj", "dtManCopDbObj", "flGestProtezioneObj");

    public RichEstDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IRichEst> getToClass() {
        return IRichEst.class;
    }

    public IRichEst selectByP01IdRichEst(int p01IdRichEst, IRichEst iRichEst) {
        return buildQuery("selectByP01IdRichEst").bind("p01IdRichEst", p01IdRichEst).rowMapper(selectByP01IdRichEstRm).singleResult(iRichEst);
    }

    public DbAccessStatus insertRec(IRichEst iRichEst) {
        return buildQuery("insertRec").bind(iRichEst).executeInsert();
    }

    public DbAccessStatus updateRec(IRichEst iRichEst) {
        return buildQuery("updateRec").bind(iRichEst).executeUpdate();
    }

    public DbAccessStatus deleteByP01IdRichEst(int p01IdRichEst) {
        return buildQuery("deleteByP01IdRichEst").bind("p01IdRichEst", p01IdRichEst).executeDelete();
    }

    public IRichEst selectRec(String p01IbOgg, int idsv0003CodiceCompagniaAnia, IRichEst iRichEst) {
        return buildQuery("selectRec").bind("p01IbOgg", p01IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByP01IdRichEstRm).singleResult(iRichEst);
    }

    public DbAccessStatus openCIboNstP01(String p01IbOgg, int idsv0003CodiceCompagniaAnia) {
        cIboNstP01 = buildQuery("openCIboNstP01").bind("p01IbOgg", p01IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboNstP01() {
        return closeCursor(cIboNstP01);
    }

    public IRichEst fetchCIboNstP01(IRichEst iRichEst) {
        return fetch(cIboNstP01, iRichEst, selectByP01IdRichEstRm);
    }

    public IRichEst selectRec1(String p01IbRichEst, int idsv0003CodiceCompagniaAnia, IRichEst iRichEst) {
        return buildQuery("selectRec1").bind("p01IbRichEst", p01IbRichEst).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByP01IdRichEstRm).singleResult(iRichEst);
    }

    public IRichEst selectRec2(String p01IbOggOrig, int idsv0003CodiceCompagniaAnia, IRichEst iRichEst) {
        return buildQuery("selectRec2").bind("p01IbOggOrig", p01IbOggOrig).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByP01IdRichEstRm).singleResult(iRichEst);
    }

    public DbAccessStatus openCIbsNstP010(String p01IbRichEst, int idsv0003CodiceCompagniaAnia) {
        cIbsNstP010 = buildQuery("openCIbsNstP010").bind("p01IbRichEst", p01IbRichEst).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus openCIbsNstP011(String p01IbOggOrig, int idsv0003CodiceCompagniaAnia) {
        cIbsNstP011 = buildQuery("openCIbsNstP011").bind("p01IbOggOrig", p01IbOggOrig).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsNstP010() {
        return closeCursor(cIbsNstP010);
    }

    public DbAccessStatus closeCIbsNstP011() {
        return closeCursor(cIbsNstP011);
    }

    public IRichEst fetchCIbsNstP010(IRichEst iRichEst) {
        return fetch(cIbsNstP010, iRichEst, selectByP01IdRichEstRm);
    }

    public IRichEst fetchCIbsNstP011(IRichEst iRichEst) {
        return fetch(cIbsNstP011, iRichEst, selectByP01IdRichEstRm);
    }

    public IRichEst selectRec3(int p01IdOgg, String p01TpOgg, int idsv0003CodiceCompagniaAnia, IRichEst iRichEst) {
        return buildQuery("selectRec3").bind("p01IdOgg", p01IdOgg).bind("p01TpOgg", p01TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByP01IdRichEstRm).singleResult(iRichEst);
    }

    public DbAccessStatus openCIdoNstP01(int p01IdOgg, String p01TpOgg, int idsv0003CodiceCompagniaAnia) {
        cIdoNstP01 = buildQuery("openCIdoNstP01").bind("p01IdOgg", p01IdOgg).bind("p01TpOgg", p01TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoNstP01() {
        return closeCursor(cIdoNstP01);
    }

    public IRichEst fetchCIdoNstP01(IRichEst iRichEst) {
        return fetch(cIdoNstP01, iRichEst, selectByP01IdRichEstRm);
    }
}
