package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [MATR_MOVIMENTO]
 * 
 */
public interface IMatrMovimento extends BaseSqlTo {

    /**
     * Host Variable MMO-ID-MATR-MOVIMENTO
     * 
     */
    int getIdMatrMovimento();

    void setIdMatrMovimento(int idMatrMovimento);

    /**
     * Host Variable MMO-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable MMO-TP-MOVI-PTF
     * 
     */
    int getTpMoviPtf();

    void setTpMoviPtf(int tpMoviPtf);

    /**
     * Host Variable MMO-TP-OGG
     * 
     */
    String getTpOgg();

    void setTpOgg(String tpOgg);

    /**
     * Host Variable MMO-TP-FRM-ASSVA
     * 
     */
    String getTpFrmAssva();

    void setTpFrmAssva(String tpFrmAssva);

    /**
     * Nullable property for MMO-TP-FRM-ASSVA
     * 
     */
    String getTpFrmAssvaObj();

    void setTpFrmAssvaObj(String tpFrmAssvaObj);

    /**
     * Host Variable MMO-TP-MOVI-ACT
     * 
     */
    String getTpMoviAct();

    void setTpMoviAct(String tpMoviAct);

    /**
     * Host Variable MMO-AMMISSIBILITA-MOVI
     * 
     */
    char getAmmissibilitaMovi();

    void setAmmissibilitaMovi(char ammissibilitaMovi);

    /**
     * Nullable property for MMO-AMMISSIBILITA-MOVI
     * 
     */
    Character getAmmissibilitaMoviObj();

    void setAmmissibilitaMoviObj(Character ammissibilitaMoviObj);

    /**
     * Host Variable MMO-SERVIZIO-CONTROLLO
     * 
     */
    String getServizioControllo();

    void setServizioControllo(String servizioControllo);

    /**
     * Nullable property for MMO-SERVIZIO-CONTROLLO
     * 
     */
    String getServizioControlloObj();

    void setServizioControlloObj(String servizioControlloObj);

    /**
     * Host Variable MMO-COD-PROCESSO-WF
     * 
     */
    String getCodProcessoWf();

    void setCodProcessoWf(String codProcessoWf);

    /**
     * Nullable property for MMO-COD-PROCESSO-WF
     * 
     */
    String getCodProcessoWfObj();

    void setCodProcessoWfObj(String codProcessoWfObj);
};
