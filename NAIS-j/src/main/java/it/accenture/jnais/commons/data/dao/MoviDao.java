package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IMovi;

/**
 * Data Access Object(DAO) for table [MOVI]
 * 
 */
public class MoviDao extends BaseSqlDao<IMovi> {

    private Cursor cIdoNstMov;
    private Cursor cNst;
    private Cursor cNst1;
    private Cursor cNst3;
    private Cursor cNst8;
    private Cursor cNst10;
    private Cursor cNst17;
    private Cursor cNst18;
    private Cursor cNst21;
    private Cursor cNst22;
    private final IRowMapper<IMovi> selectByMovIdMoviRm = buildNamedRowMapper(IMovi.class, "movIdMovi", "codCompAnia", "movIdOggObj", "ibOggObj", "ibMoviObj", "movTpOggObj", "idRichObj", "tpMoviObj", "movDtEffDb", "idMoviAnnObj", "idMoviCollgObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");
    private final IRowMapper<IMovi> selectRec10Rm = buildNamedRowMapper(IMovi.class, "movIdMovi", "movDtEffDb", "dsTsCptz");
    private final IRowMapper<IMovi> selectRec11Rm = buildNamedRowMapper(IMovi.class, "movDtEffDb", "dsTsCptz");

    public MoviDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IMovi> getToClass() {
        return IMovi.class;
    }

    public IMovi selectByMovIdMovi(int movIdMovi, IMovi iMovi) {
        return buildQuery("selectByMovIdMovi").bind("movIdMovi", movIdMovi).rowMapper(selectByMovIdMoviRm).singleResult(iMovi);
    }

    public DbAccessStatus insertRec(IMovi iMovi) {
        return buildQuery("insertRec").bind(iMovi).executeInsert();
    }

    public DbAccessStatus updateRec(IMovi iMovi) {
        return buildQuery("updateRec").bind(iMovi).executeUpdate();
    }

    public DbAccessStatus deleteByMovIdMovi(int movIdMovi) {
        return buildQuery("deleteByMovIdMovi").bind("movIdMovi", movIdMovi).executeDelete();
    }

    public IMovi selectRec(int movIdOgg, String movTpOgg, int idsv0003CodiceCompagniaAnia, IMovi iMovi) {
        return buildQuery("selectRec").bind("movIdOgg", movIdOgg).bind("movTpOgg", movTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByMovIdMoviRm).singleResult(iMovi);
    }

    public DbAccessStatus openCIdoNstMov(int movIdOgg, String movTpOgg, int idsv0003CodiceCompagniaAnia) {
        cIdoNstMov = buildQuery("openCIdoNstMov").bind("movIdOgg", movIdOgg).bind("movTpOgg", movTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoNstMov() {
        return closeCursor(cIdoNstMov);
    }

    public IMovi fetchCIdoNstMov(IMovi iMovi) {
        return fetch(cIdoNstMov, iMovi, selectByMovIdMoviRm);
    }

    public IMovi selectRec1(int ldbv0471IdRich, int idsv0003CodiceCompagniaAnia, IMovi iMovi) {
        return buildQuery("selectRec1").bind("ldbv0471IdRich", ldbv0471IdRich).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByMovIdMoviRm).singleResult(iMovi);
    }

    public DbAccessStatus openCNst(int ldbv0471IdRich, int idsv0003CodiceCompagniaAnia) {
        cNst = buildQuery("openCNst").bind("ldbv0471IdRich", ldbv0471IdRich).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst() {
        return closeCursor(cNst);
    }

    public IMovi fetchCNst(IMovi iMovi) {
        return fetch(cNst, iMovi, selectByMovIdMoviRm);
    }

    public IMovi selectRec2(int movIdOgg, String movTpOgg, int movTpMovi, int idsv0003CodiceCompagniaAnia, IMovi iMovi) {
        return buildQuery("selectRec2").bind("movIdOgg", movIdOgg).bind("movTpOgg", movTpOgg).bind("movTpMovi", movTpMovi).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByMovIdMoviRm).singleResult(iMovi);
    }

    public DbAccessStatus openCNst1(int movIdOgg, String movTpOgg, int movTpMovi, int idsv0003CodiceCompagniaAnia) {
        cNst1 = buildQuery("openCNst1").bind("movIdOgg", movIdOgg).bind("movTpOgg", movTpOgg).bind("movTpMovi", movTpMovi).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst1() {
        return closeCursor(cNst1);
    }

    public IMovi fetchCNst1(IMovi iMovi) {
        return fetch(cNst1, iMovi, selectByMovIdMoviRm);
    }

    public IMovi selectRec3(int movIdOgg, String movTpOgg, int movTpMovi, int idsv0003CodiceCompagniaAnia, IMovi iMovi) {
        return buildQuery("selectRec3").bind("movIdOgg", movIdOgg).bind("movTpOgg", movTpOgg).bind("movTpMovi", movTpMovi).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByMovIdMoviRm).singleResult(iMovi);
    }

    public DbAccessStatus openCNst3(int movIdOgg, String movTpOgg, int movTpMovi, int idsv0003CodiceCompagniaAnia) {
        cNst3 = buildQuery("openCNst3").bind("movIdOgg", movIdOgg).bind("movTpOgg", movTpOgg).bind("movTpMovi", movTpMovi).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst3() {
        return closeCursor(cNst3);
    }

    public IMovi fetchCNst3(IMovi iMovi) {
        return fetch(cNst3, iMovi, selectByMovIdMoviRm);
    }

    public IMovi selectRec4(IMovi iMovi) {
        return buildQuery("selectRec4").bind(iMovi).rowMapper(selectByMovIdMoviRm).singleResult(iMovi);
    }

    public DbAccessStatus openCNst8(IMovi iMovi) {
        cNst8 = buildQuery("openCNst8").bind(iMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst8() {
        return closeCursor(cNst8);
    }

    public IMovi fetchCNst8(IMovi iMovi) {
        return fetch(cNst8, iMovi, selectByMovIdMoviRm);
    }

    public String selectRec5(IMovi iMovi, String dft) {
        return buildQuery("selectRec5").bind(iMovi).scalarResultString(dft);
    }

    public String selectRec6(IMovi iMovi, String dft) {
        return buildQuery("selectRec6").bind(iMovi).scalarResultString(dft);
    }

    public IMovi selectRec7(IMovi iMovi) {
        return buildQuery("selectRec7").bind(iMovi).rowMapper(selectByMovIdMoviRm).singleResult(iMovi);
    }

    public DbAccessStatus openCNst10(IMovi iMovi) {
        cNst10 = buildQuery("openCNst10").bind(iMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst10() {
        return closeCursor(cNst10);
    }

    public IMovi fetchCNst10(IMovi iMovi) {
        return fetch(cNst10, iMovi, selectByMovIdMoviRm);
    }

    public IMovi selectRec8(IMovi iMovi) {
        return buildQuery("selectRec8").bind(iMovi).rowMapper(selectByMovIdMoviRm).singleResult(iMovi);
    }

    public DbAccessStatus openCNst17(IMovi iMovi) {
        cNst17 = buildQuery("openCNst17").bind(iMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst17() {
        return closeCursor(cNst17);
    }

    public IMovi fetchCNst17(IMovi iMovi) {
        return fetch(cNst17, iMovi, selectByMovIdMoviRm);
    }

    public IMovi selectRec9(String movTpOgg, int movIdOgg, int idsv0003CodiceCompagniaAnia, IMovi iMovi) {
        return buildQuery("selectRec9").bind("movTpOgg", movTpOgg).bind("movIdOgg", movIdOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByMovIdMoviRm).singleResult(iMovi);
    }

    public DbAccessStatus openCNst18(String movTpOgg, int movIdOgg, int idsv0003CodiceCompagniaAnia) {
        cNst18 = buildQuery("openCNst18").bind("movTpOgg", movTpOgg).bind("movIdOgg", movIdOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst18() {
        return closeCursor(cNst18);
    }

    public IMovi fetchCNst18(IMovi iMovi) {
        return fetch(cNst18, iMovi, selectByMovIdMoviRm);
    }

    public IMovi selectRec10(IMovi iMovi) {
        return buildQuery("selectRec10").bind(iMovi).rowMapper(selectRec10Rm).singleResult(iMovi);
    }

    public DbAccessStatus openCNst21(IMovi iMovi) {
        cNst21 = buildQuery("openCNst21").bind(iMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst21() {
        return closeCursor(cNst21);
    }

    public IMovi fetchCNst21(IMovi iMovi) {
        return fetch(cNst21, iMovi, selectRec10Rm);
    }

    public IMovi selectRec11(IMovi iMovi) {
        return buildQuery("selectRec11").bind(iMovi).rowMapper(selectRec11Rm).singleResult(iMovi);
    }

    public DbAccessStatus openCNst22(IMovi iMovi) {
        cNst22 = buildQuery("openCNst22").bind(iMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst22() {
        return closeCursor(cNst22);
    }

    public IMovi fetchCNst22(IMovi iMovi) {
        return fetch(cNst22, iMovi, selectRec11Rm);
    }
}
