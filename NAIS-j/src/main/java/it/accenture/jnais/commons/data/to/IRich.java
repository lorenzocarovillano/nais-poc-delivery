package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [RICH]
 * 
 */
public interface IRich extends BaseSqlTo {

    /**
     * Host Variable PRENOTAZIONE
     * 
     */
    String getPrenotazione();

    void setPrenotazione(String prenotazione);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable RIC-ID-BATCH
     * 
     */
    int getRicIdBatch();

    void setRicIdBatch(int ricIdBatch);

    /**
     * Host Variable IABV0002-STATE-01
     * 
     */
    char getIabv0002State01();

    void setIabv0002State01(char iabv0002State01);

    /**
     * Host Variable IABV0002-STATE-02
     * 
     */
    char getIabv0002State02();

    void setIabv0002State02(char iabv0002State02);

    /**
     * Host Variable IABV0002-STATE-03
     * 
     */
    char getIabv0002State03();

    void setIabv0002State03(char iabv0002State03);

    /**
     * Host Variable IABV0002-STATE-04
     * 
     */
    char getIabv0002State04();

    void setIabv0002State04(char iabv0002State04);

    /**
     * Host Variable IABV0002-STATE-05
     * 
     */
    char getIabv0002State05();

    void setIabv0002State05(char iabv0002State05);

    /**
     * Host Variable IABV0002-STATE-06
     * 
     */
    char getIabv0002State06();

    void setIabv0002State06(char iabv0002State06);

    /**
     * Host Variable IABV0002-STATE-07
     * 
     */
    char getIabv0002State07();

    void setIabv0002State07(char iabv0002State07);

    /**
     * Host Variable IABV0002-STATE-08
     * 
     */
    char getIabv0002State08();

    void setIabv0002State08(char iabv0002State08);

    /**
     * Host Variable IABV0002-STATE-09
     * 
     */
    char getIabv0002State09();

    void setIabv0002State09(char iabv0002State09);

    /**
     * Host Variable IABV0002-STATE-10
     * 
     */
    char getIabv0002State10();

    void setIabv0002State10(char iabv0002State10);

    /**
     * Host Variable RIC-COD-MACROFUNCT
     * 
     */
    String getRicCodMacrofunct();

    void setRicCodMacrofunct(String ricCodMacrofunct);

    /**
     * Host Variable RIC-TP-MOVI
     * 
     */
    int getRicTpMovi();

    void setRicTpMovi(int ricTpMovi);

    /**
     * Host Variable RIC-ID-RICH
     * 
     */
    int getRicIdRich();

    void setRicIdRich(int ricIdRich);

    /**
     * Host Variable LDBV4511-TP-RICH-00
     * 
     */
    String getLdbv4511TpRich00();

    void setLdbv4511TpRich00(String ldbv4511TpRich00);

    /**
     * Host Variable LDBV4511-TP-RICH-01
     * 
     */
    String getLdbv4511TpRich01();

    void setLdbv4511TpRich01(String ldbv4511TpRich01);

    /**
     * Host Variable LDBV4511-TP-RICH-02
     * 
     */
    String getLdbv4511TpRich02();

    void setLdbv4511TpRich02(String ldbv4511TpRich02);

    /**
     * Host Variable LDBV4511-TP-RICH-03
     * 
     */
    String getLdbv4511TpRich03();

    void setLdbv4511TpRich03(String ldbv4511TpRich03);

    /**
     * Host Variable LDBV4511-TP-RICH-04
     * 
     */
    String getLdbv4511TpRich04();

    void setLdbv4511TpRich04(String ldbv4511TpRich04);

    /**
     * Host Variable LDBV4511-TP-RICH-05
     * 
     */
    String getLdbv4511TpRich05();

    void setLdbv4511TpRich05(String ldbv4511TpRich05);

    /**
     * Host Variable LDBV4511-TP-RICH-06
     * 
     */
    String getLdbv4511TpRich06();

    void setLdbv4511TpRich06(String ldbv4511TpRich06);

    /**
     * Host Variable LDBV4511-TP-RICH-07
     * 
     */
    String getLdbv4511TpRich07();

    void setLdbv4511TpRich07(String ldbv4511TpRich07);

    /**
     * Host Variable LDBV4511-TP-RICH-08
     * 
     */
    String getLdbv4511TpRich08();

    void setLdbv4511TpRich08(String ldbv4511TpRich08);

    /**
     * Host Variable LDBV4511-TP-RICH-09
     * 
     */
    String getLdbv4511TpRich09();

    void setLdbv4511TpRich09(String ldbv4511TpRich09);

    /**
     * Host Variable LDBV4511-STATO-ELAB-00
     * 
     */
    char getLdbv4511StatoElab00();

    void setLdbv4511StatoElab00(char ldbv4511StatoElab00);

    /**
     * Host Variable LDBV4511-STATO-ELAB-01
     * 
     */
    char getLdbv4511StatoElab01();

    void setLdbv4511StatoElab01(char ldbv4511StatoElab01);

    /**
     * Host Variable LDBV4511-STATO-ELAB-02
     * 
     */
    char getLdbv4511StatoElab02();

    void setLdbv4511StatoElab02(char ldbv4511StatoElab02);

    /**
     * Host Variable LDBV4511-STATO-ELAB-03
     * 
     */
    char getLdbv4511StatoElab03();

    void setLdbv4511StatoElab03(char ldbv4511StatoElab03);

    /**
     * Host Variable LDBV4511-STATO-ELAB-04
     * 
     */
    char getLdbv4511StatoElab04();

    void setLdbv4511StatoElab04(char ldbv4511StatoElab04);

    /**
     * Host Variable LDBV4511-STATO-ELAB-05
     * 
     */
    char getLdbv4511StatoElab05();

    void setLdbv4511StatoElab05(char ldbv4511StatoElab05);

    /**
     * Host Variable LDBV4511-STATO-ELAB-06
     * 
     */
    char getLdbv4511StatoElab06();

    void setLdbv4511StatoElab06(char ldbv4511StatoElab06);

    /**
     * Host Variable LDBV4511-STATO-ELAB-07
     * 
     */
    char getLdbv4511StatoElab07();

    void setLdbv4511StatoElab07(char ldbv4511StatoElab07);

    /**
     * Host Variable LDBV4511-STATO-ELAB-08
     * 
     */
    char getLdbv4511StatoElab08();

    void setLdbv4511StatoElab08(char ldbv4511StatoElab08);

    /**
     * Host Variable LDBV4511-STATO-ELAB-09
     * 
     */
    char getLdbv4511StatoElab09();

    void setLdbv4511StatoElab09(char ldbv4511StatoElab09);

    /**
     * Host Variable RIC-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable RIC-TP-RICH
     * 
     */
    String getTpRich();

    void setTpRich(String tpRich);

    /**
     * Host Variable RIC-IB-RICH
     * 
     */
    String getIbRich();

    void setIbRich(String ibRich);

    /**
     * Nullable property for RIC-IB-RICH
     * 
     */
    String getIbRichObj();

    void setIbRichObj(String ibRichObj);

    /**
     * Host Variable RIC-DT-EFF-DB
     * 
     */
    String getDtEffDb();

    void setDtEffDb(String dtEffDb);

    /**
     * Host Variable RIC-DT-RGSTRZ-RICH-DB
     * 
     */
    String getDtRgstrzRichDb();

    void setDtRgstrzRichDb(String dtRgstrzRichDb);

    /**
     * Host Variable RIC-DT-PERV-RICH-DB
     * 
     */
    String getDtPervRichDb();

    void setDtPervRichDb(String dtPervRichDb);

    /**
     * Host Variable RIC-DT-ESEC-RICH-DB
     * 
     */
    String getDtEsecRichDb();

    void setDtEsecRichDb(String dtEsecRichDb);

    /**
     * Host Variable RIC-TS-EFF-ESEC-RICH
     * 
     */
    long getTsEffEsecRich();

    void setTsEffEsecRich(long tsEffEsecRich);

    /**
     * Nullable property for RIC-TS-EFF-ESEC-RICH
     * 
     */
    Long getTsEffEsecRichObj();

    void setTsEffEsecRichObj(Long tsEffEsecRichObj);

    /**
     * Host Variable RIC-ID-OGG
     * 
     */
    int getIdOgg();

    void setIdOgg(int idOgg);

    /**
     * Nullable property for RIC-ID-OGG
     * 
     */
    Integer getIdOggObj();

    void setIdOggObj(Integer idOggObj);

    /**
     * Host Variable RIC-TP-OGG
     * 
     */
    String getTpOgg();

    void setTpOgg(String tpOgg);

    /**
     * Nullable property for RIC-TP-OGG
     * 
     */
    String getTpOggObj();

    void setTpOggObj(String tpOggObj);

    /**
     * Host Variable RIC-IB-POLI
     * 
     */
    String getIbPoli();

    void setIbPoli(String ibPoli);

    /**
     * Nullable property for RIC-IB-POLI
     * 
     */
    String getIbPoliObj();

    void setIbPoliObj(String ibPoliObj);

    /**
     * Host Variable RIC-IB-ADES
     * 
     */
    String getIbAdes();

    void setIbAdes(String ibAdes);

    /**
     * Nullable property for RIC-IB-ADES
     * 
     */
    String getIbAdesObj();

    void setIbAdesObj(String ibAdesObj);

    /**
     * Host Variable RIC-IB-GAR
     * 
     */
    String getIbGar();

    void setIbGar(String ibGar);

    /**
     * Nullable property for RIC-IB-GAR
     * 
     */
    String getIbGarObj();

    void setIbGarObj(String ibGarObj);

    /**
     * Host Variable RIC-IB-TRCH-DI-GAR
     * 
     */
    String getIbTrchDiGar();

    void setIbTrchDiGar(String ibTrchDiGar);

    /**
     * Nullable property for RIC-IB-TRCH-DI-GAR
     * 
     */
    String getIbTrchDiGarObj();

    void setIbTrchDiGarObj(String ibTrchDiGarObj);

    /**
     * Nullable property for RIC-ID-BATCH
     * 
     */
    Integer getRicIdBatchObj();

    void setRicIdBatchObj(Integer ricIdBatchObj);

    /**
     * Host Variable RIC-ID-JOB
     * 
     */
    int getIdJob();

    void setIdJob(int idJob);

    /**
     * Nullable property for RIC-ID-JOB
     * 
     */
    Integer getIdJobObj();

    void setIdJobObj(Integer idJobObj);

    /**
     * Host Variable RIC-FL-SIMULAZIONE
     * 
     */
    char getFlSimulazione();

    void setFlSimulazione(char flSimulazione);

    /**
     * Nullable property for RIC-FL-SIMULAZIONE
     * 
     */
    Character getFlSimulazioneObj();

    void setFlSimulazioneObj(Character flSimulazioneObj);

    /**
     * Host Variable RIC-KEY-ORDINAMENTO-VCHAR
     * 
     */
    String getKeyOrdinamentoVchar();

    void setKeyOrdinamentoVchar(String keyOrdinamentoVchar);

    /**
     * Nullable property for RIC-KEY-ORDINAMENTO-VCHAR
     * 
     */
    String getKeyOrdinamentoVcharObj();

    void setKeyOrdinamentoVcharObj(String keyOrdinamentoVcharObj);

    /**
     * Host Variable RIC-ID-RICH-COLLG
     * 
     */
    int getIdRichCollg();

    void setIdRichCollg(int idRichCollg);

    /**
     * Nullable property for RIC-ID-RICH-COLLG
     * 
     */
    Integer getIdRichCollgObj();

    void setIdRichCollgObj(Integer idRichCollgObj);

    /**
     * Host Variable RIC-TP-RAMO-BILA
     * 
     */
    String getTpRamoBila();

    void setTpRamoBila(String tpRamoBila);

    /**
     * Nullable property for RIC-TP-RAMO-BILA
     * 
     */
    String getTpRamoBilaObj();

    void setTpRamoBilaObj(String tpRamoBilaObj);

    /**
     * Host Variable RIC-TP-FRM-ASSVA
     * 
     */
    String getTpFrmAssva();

    void setTpFrmAssva(String tpFrmAssva);

    /**
     * Nullable property for RIC-TP-FRM-ASSVA
     * 
     */
    String getTpFrmAssvaObj();

    void setTpFrmAssvaObj(String tpFrmAssvaObj);

    /**
     * Host Variable RIC-TP-CALC-RIS
     * 
     */
    String getTpCalcRis();

    void setTpCalcRis(String tpCalcRis);

    /**
     * Nullable property for RIC-TP-CALC-RIS
     * 
     */
    String getTpCalcRisObj();

    void setTpCalcRisObj(String tpCalcRisObj);

    /**
     * Host Variable RIC-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable RIC-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable RIC-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable RIC-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable RIC-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable RIC-RAMO-BILA
     * 
     */
    String getRamoBila();

    void setRamoBila(String ramoBila);

    /**
     * Nullable property for RIC-RAMO-BILA
     * 
     */
    String getRamoBilaObj();

    void setRamoBilaObj(String ramoBilaObj);

    /**
     * Host Variable IABV0002-STATE-CURRENT
     * 
     */
    char getIabv0002StateCurrent();

    void setIabv0002StateCurrent(char iabv0002StateCurrent);
};
