package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [DETT_TIT_CONT, TRCH_DI_GAR]
 * 
 */
public interface IDettTitContTrchDiGar extends BaseSqlTo {

    /**
     * Host Variable LDBVF981-ID-GAR
     * 
     */
    int getLdbvf981IdGar();

    void setLdbvf981IdGar(int ldbvf981IdGar);

    /**
     * Host Variable LDBVF981-TP-STAT-TIT-1
     * 
     */
    String getLdbvf981TpStatTit1();

    void setLdbvf981TpStatTit1(String ldbvf981TpStatTit1);

    /**
     * Host Variable LDBVF981-TP-STAT-TIT-2
     * 
     */
    String getLdbvf981TpStatTit2();

    void setLdbvf981TpStatTit2(String ldbvf981TpStatTit2);

    /**
     * Host Variable LDBVF981-TP-STAT-TIT-3
     * 
     */
    String getLdbvf981TpStatTit3();

    void setLdbvf981TpStatTit3(String ldbvf981TpStatTit3);

    /**
     * Host Variable LDBVF981-TP-STAT-TIT-4
     * 
     */
    String getLdbvf981TpStatTit4();

    void setLdbvf981TpStatTit4(String ldbvf981TpStatTit4);

    /**
     * Host Variable LDBVF981-TP-STAT-TIT-5
     * 
     */
    String getLdbvf981TpStatTit5();

    void setLdbvf981TpStatTit5(String ldbvf981TpStatTit5);

    /**
     * Host Variable DTC-DT-INI-COP-DB
     * 
     */
    String getDtcDtIniCopDb();

    void setDtcDtIniCopDb(String dtcDtIniCopDb);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBV4021-PRE-LRD
     * 
     */
    AfDecimal getLdbv4021PreLrd();

    void setLdbv4021PreLrd(AfDecimal ldbv4021PreLrd);

    /**
     * Host Variable LDBV4021-ID-TGA
     * 
     */
    int getLdbv4021IdTga();

    void setLdbv4021IdTga(int ldbv4021IdTga);

    /**
     * Host Variable LDBV4021-DATA-INIZIO-DB
     * 
     */
    String getLdbv4021DataInizioDb();

    void setLdbv4021DataInizioDb(String ldbv4021DataInizioDb);

    /**
     * Host Variable LDBV4021-DATA-FINE-DB
     * 
     */
    String getLdbv4021DataFineDb();

    void setLdbv4021DataFineDb(String ldbv4021DataFineDb);

    /**
     * Host Variable LDBVE091-PRE-LRD
     * 
     */
    AfDecimal getLdbve091PreLrd();

    void setLdbve091PreLrd(AfDecimal ldbve091PreLrd);

    /**
     * Nullable property for LDBVE091-PRE-LRD
     * 
     */
    AfDecimal getLdbve091PreLrdObj();

    void setLdbve091PreLrdObj(AfDecimal ldbve091PreLrdObj);

    /**
     * Host Variable LDBVE091-ID-TGA
     * 
     */
    int getLdbve091IdTga();

    void setLdbve091IdTga(int ldbve091IdTga);

    /**
     * Host Variable LDBVE091-DATA-INIZIO-DB
     * 
     */
    String getLdbve091DataInizioDb();

    void setLdbve091DataInizioDb(String ldbve091DataInizioDb);

    /**
     * Host Variable LDBVE091-DATA-FINE-DB
     * 
     */
    String getLdbve091DataFineDb();

    void setLdbve091DataFineDb(String ldbve091DataFineDb);

    /**
     * Host Variable DTC-ID-DETT-TIT-CONT
     * 
     */
    int getIdDettTitCont();

    void setIdDettTitCont(int idDettTitCont);

    /**
     * Host Variable DTC-ID-TIT-CONT
     * 
     */
    int getIdTitCont();

    void setIdTitCont(int idTitCont);

    /**
     * Host Variable DTC-ID-OGG
     * 
     */
    int getIdOgg();

    void setIdOgg(int idOgg);

    /**
     * Host Variable DTC-TP-OGG
     * 
     */
    String getTpOgg();

    void setTpOgg(String tpOgg);

    /**
     * Host Variable DTC-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable DTC-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for DTC-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable DTC-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable DTC-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable DTC-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Nullable property for DTC-DT-INI-COP-DB
     * 
     */
    String getDtcDtIniCopDbObj();

    void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj);

    /**
     * Host Variable DTC-DT-END-COP-DB
     * 
     */
    String getDtEndCopDb();

    void setDtEndCopDb(String dtEndCopDb);

    /**
     * Nullable property for DTC-DT-END-COP-DB
     * 
     */
    String getDtEndCopDbObj();

    void setDtEndCopDbObj(String dtEndCopDbObj);

    /**
     * Host Variable DTC-PRE-NET
     * 
     */
    AfDecimal getPreNet();

    void setPreNet(AfDecimal preNet);

    /**
     * Nullable property for DTC-PRE-NET
     * 
     */
    AfDecimal getPreNetObj();

    void setPreNetObj(AfDecimal preNetObj);

    /**
     * Host Variable DTC-INTR-FRAZ
     * 
     */
    AfDecimal getIntrFraz();

    void setIntrFraz(AfDecimal intrFraz);

    /**
     * Nullable property for DTC-INTR-FRAZ
     * 
     */
    AfDecimal getIntrFrazObj();

    void setIntrFrazObj(AfDecimal intrFrazObj);

    /**
     * Host Variable DTC-INTR-MORA
     * 
     */
    AfDecimal getIntrMora();

    void setIntrMora(AfDecimal intrMora);

    /**
     * Nullable property for DTC-INTR-MORA
     * 
     */
    AfDecimal getIntrMoraObj();

    void setIntrMoraObj(AfDecimal intrMoraObj);

    /**
     * Host Variable DTC-INTR-RETDT
     * 
     */
    AfDecimal getIntrRetdt();

    void setIntrRetdt(AfDecimal intrRetdt);

    /**
     * Nullable property for DTC-INTR-RETDT
     * 
     */
    AfDecimal getIntrRetdtObj();

    void setIntrRetdtObj(AfDecimal intrRetdtObj);

    /**
     * Host Variable DTC-INTR-RIAT
     * 
     */
    AfDecimal getIntrRiat();

    void setIntrRiat(AfDecimal intrRiat);

    /**
     * Nullable property for DTC-INTR-RIAT
     * 
     */
    AfDecimal getIntrRiatObj();

    void setIntrRiatObj(AfDecimal intrRiatObj);

    /**
     * Host Variable DTC-DIR
     * 
     */
    AfDecimal getDir();

    void setDir(AfDecimal dir);

    /**
     * Nullable property for DTC-DIR
     * 
     */
    AfDecimal getDirObj();

    void setDirObj(AfDecimal dirObj);

    /**
     * Host Variable DTC-SPE-MED
     * 
     */
    AfDecimal getSpeMed();

    void setSpeMed(AfDecimal speMed);

    /**
     * Nullable property for DTC-SPE-MED
     * 
     */
    AfDecimal getSpeMedObj();

    void setSpeMedObj(AfDecimal speMedObj);

    /**
     * Host Variable DTC-TAX
     * 
     */
    AfDecimal getTax();

    void setTax(AfDecimal tax);

    /**
     * Nullable property for DTC-TAX
     * 
     */
    AfDecimal getTaxObj();

    void setTaxObj(AfDecimal taxObj);

    /**
     * Host Variable DTC-SOPR-SAN
     * 
     */
    AfDecimal getSoprSan();

    void setSoprSan(AfDecimal soprSan);

    /**
     * Nullable property for DTC-SOPR-SAN
     * 
     */
    AfDecimal getSoprSanObj();

    void setSoprSanObj(AfDecimal soprSanObj);

    /**
     * Host Variable DTC-SOPR-SPO
     * 
     */
    AfDecimal getSoprSpo();

    void setSoprSpo(AfDecimal soprSpo);

    /**
     * Nullable property for DTC-SOPR-SPO
     * 
     */
    AfDecimal getSoprSpoObj();

    void setSoprSpoObj(AfDecimal soprSpoObj);

    /**
     * Host Variable DTC-SOPR-TEC
     * 
     */
    AfDecimal getSoprTec();

    void setSoprTec(AfDecimal soprTec);

    /**
     * Nullable property for DTC-SOPR-TEC
     * 
     */
    AfDecimal getSoprTecObj();

    void setSoprTecObj(AfDecimal soprTecObj);

    /**
     * Host Variable DTC-SOPR-PROF
     * 
     */
    AfDecimal getSoprProf();

    void setSoprProf(AfDecimal soprProf);

    /**
     * Nullable property for DTC-SOPR-PROF
     * 
     */
    AfDecimal getSoprProfObj();

    void setSoprProfObj(AfDecimal soprProfObj);

    /**
     * Host Variable DTC-SOPR-ALT
     * 
     */
    AfDecimal getSoprAlt();

    void setSoprAlt(AfDecimal soprAlt);

    /**
     * Nullable property for DTC-SOPR-ALT
     * 
     */
    AfDecimal getSoprAltObj();

    void setSoprAltObj(AfDecimal soprAltObj);

    /**
     * Host Variable DTC-PRE-TOT
     * 
     */
    AfDecimal getPreTot();

    void setPreTot(AfDecimal preTot);

    /**
     * Nullable property for DTC-PRE-TOT
     * 
     */
    AfDecimal getPreTotObj();

    void setPreTotObj(AfDecimal preTotObj);

    /**
     * Host Variable DTC-PRE-PP-IAS
     * 
     */
    AfDecimal getPrePpIas();

    void setPrePpIas(AfDecimal prePpIas);

    /**
     * Nullable property for DTC-PRE-PP-IAS
     * 
     */
    AfDecimal getPrePpIasObj();

    void setPrePpIasObj(AfDecimal prePpIasObj);

    /**
     * Host Variable DTC-PRE-SOLO-RSH
     * 
     */
    AfDecimal getPreSoloRsh();

    void setPreSoloRsh(AfDecimal preSoloRsh);

    /**
     * Nullable property for DTC-PRE-SOLO-RSH
     * 
     */
    AfDecimal getPreSoloRshObj();

    void setPreSoloRshObj(AfDecimal preSoloRshObj);

    /**
     * Host Variable DTC-CAR-ACQ
     * 
     */
    AfDecimal getCarAcq();

    void setCarAcq(AfDecimal carAcq);

    /**
     * Nullable property for DTC-CAR-ACQ
     * 
     */
    AfDecimal getCarAcqObj();

    void setCarAcqObj(AfDecimal carAcqObj);

    /**
     * Host Variable DTC-CAR-GEST
     * 
     */
    AfDecimal getCarGest();

    void setCarGest(AfDecimal carGest);

    /**
     * Nullable property for DTC-CAR-GEST
     * 
     */
    AfDecimal getCarGestObj();

    void setCarGestObj(AfDecimal carGestObj);

    /**
     * Host Variable DTC-CAR-INC
     * 
     */
    AfDecimal getCarInc();

    void setCarInc(AfDecimal carInc);

    /**
     * Nullable property for DTC-CAR-INC
     * 
     */
    AfDecimal getCarIncObj();

    void setCarIncObj(AfDecimal carIncObj);

    /**
     * Host Variable DTC-PROV-ACQ-1AA
     * 
     */
    AfDecimal getProvAcq1aa();

    void setProvAcq1aa(AfDecimal provAcq1aa);

    /**
     * Nullable property for DTC-PROV-ACQ-1AA
     * 
     */
    AfDecimal getProvAcq1aaObj();

    void setProvAcq1aaObj(AfDecimal provAcq1aaObj);

    /**
     * Host Variable DTC-PROV-ACQ-2AA
     * 
     */
    AfDecimal getProvAcq2aa();

    void setProvAcq2aa(AfDecimal provAcq2aa);

    /**
     * Nullable property for DTC-PROV-ACQ-2AA
     * 
     */
    AfDecimal getProvAcq2aaObj();

    void setProvAcq2aaObj(AfDecimal provAcq2aaObj);

    /**
     * Host Variable DTC-PROV-RICOR
     * 
     */
    AfDecimal getProvRicor();

    void setProvRicor(AfDecimal provRicor);

    /**
     * Nullable property for DTC-PROV-RICOR
     * 
     */
    AfDecimal getProvRicorObj();

    void setProvRicorObj(AfDecimal provRicorObj);

    /**
     * Host Variable DTC-PROV-INC
     * 
     */
    AfDecimal getProvInc();

    void setProvInc(AfDecimal provInc);

    /**
     * Nullable property for DTC-PROV-INC
     * 
     */
    AfDecimal getProvIncObj();

    void setProvIncObj(AfDecimal provIncObj);

    /**
     * Host Variable DTC-PROV-DA-REC
     * 
     */
    AfDecimal getProvDaRec();

    void setProvDaRec(AfDecimal provDaRec);

    /**
     * Nullable property for DTC-PROV-DA-REC
     * 
     */
    AfDecimal getProvDaRecObj();

    void setProvDaRecObj(AfDecimal provDaRecObj);

    /**
     * Host Variable DTC-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for DTC-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable DTC-FRQ-MOVI
     * 
     */
    int getFrqMovi();

    void setFrqMovi(int frqMovi);

    /**
     * Nullable property for DTC-FRQ-MOVI
     * 
     */
    Integer getFrqMoviObj();

    void setFrqMoviObj(Integer frqMoviObj);

    /**
     * Host Variable DTC-TP-RGM-FISC
     * 
     */
    String getTpRgmFisc();

    void setTpRgmFisc(String tpRgmFisc);

    /**
     * Host Variable DTC-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Nullable property for DTC-COD-TARI
     * 
     */
    String getCodTariObj();

    void setCodTariObj(String codTariObj);

    /**
     * Host Variable DTC-TP-STAT-TIT
     * 
     */
    String getTpStatTit();

    void setTpStatTit(String tpStatTit);

    /**
     * Host Variable DTC-IMP-AZ
     * 
     */
    AfDecimal getImpAz();

    void setImpAz(AfDecimal impAz);

    /**
     * Nullable property for DTC-IMP-AZ
     * 
     */
    AfDecimal getImpAzObj();

    void setImpAzObj(AfDecimal impAzObj);

    /**
     * Host Variable DTC-IMP-ADER
     * 
     */
    AfDecimal getImpAder();

    void setImpAder(AfDecimal impAder);

    /**
     * Nullable property for DTC-IMP-ADER
     * 
     */
    AfDecimal getImpAderObj();

    void setImpAderObj(AfDecimal impAderObj);

    /**
     * Host Variable DTC-IMP-TFR
     * 
     */
    AfDecimal getImpTfr();

    void setImpTfr(AfDecimal impTfr);

    /**
     * Nullable property for DTC-IMP-TFR
     * 
     */
    AfDecimal getImpTfrObj();

    void setImpTfrObj(AfDecimal impTfrObj);

    /**
     * Host Variable DTC-IMP-VOLO
     * 
     */
    AfDecimal getImpVolo();

    void setImpVolo(AfDecimal impVolo);

    /**
     * Nullable property for DTC-IMP-VOLO
     * 
     */
    AfDecimal getImpVoloObj();

    void setImpVoloObj(AfDecimal impVoloObj);

    /**
     * Host Variable DTC-MANFEE-ANTIC
     * 
     */
    AfDecimal getManfeeAntic();

    void setManfeeAntic(AfDecimal manfeeAntic);

    /**
     * Nullable property for DTC-MANFEE-ANTIC
     * 
     */
    AfDecimal getManfeeAnticObj();

    void setManfeeAnticObj(AfDecimal manfeeAnticObj);

    /**
     * Host Variable DTC-MANFEE-RICOR
     * 
     */
    AfDecimal getManfeeRicor();

    void setManfeeRicor(AfDecimal manfeeRicor);

    /**
     * Nullable property for DTC-MANFEE-RICOR
     * 
     */
    AfDecimal getManfeeRicorObj();

    void setManfeeRicorObj(AfDecimal manfeeRicorObj);

    /**
     * Host Variable DTC-MANFEE-REC
     * 
     */
    AfDecimal getManfeeRec();

    void setManfeeRec(AfDecimal manfeeRec);

    /**
     * Nullable property for DTC-MANFEE-REC
     * 
     */
    AfDecimal getManfeeRecObj();

    void setManfeeRecObj(AfDecimal manfeeRecObj);

    /**
     * Host Variable DTC-DT-ESI-TIT-DB
     * 
     */
    String getDtEsiTitDb();

    void setDtEsiTitDb(String dtEsiTitDb);

    /**
     * Nullable property for DTC-DT-ESI-TIT-DB
     * 
     */
    String getDtEsiTitDbObj();

    void setDtEsiTitDbObj(String dtEsiTitDbObj);

    /**
     * Host Variable DTC-SPE-AGE
     * 
     */
    AfDecimal getSpeAge();

    void setSpeAge(AfDecimal speAge);

    /**
     * Nullable property for DTC-SPE-AGE
     * 
     */
    AfDecimal getSpeAgeObj();

    void setSpeAgeObj(AfDecimal speAgeObj);

    /**
     * Host Variable DTC-CAR-IAS
     * 
     */
    AfDecimal getCarIas();

    void setCarIas(AfDecimal carIas);

    /**
     * Nullable property for DTC-CAR-IAS
     * 
     */
    AfDecimal getCarIasObj();

    void setCarIasObj(AfDecimal carIasObj);

    /**
     * Host Variable DTC-TOT-INTR-PREST
     * 
     */
    AfDecimal getTotIntrPrest();

    void setTotIntrPrest(AfDecimal totIntrPrest);

    /**
     * Nullable property for DTC-TOT-INTR-PREST
     * 
     */
    AfDecimal getTotIntrPrestObj();

    void setTotIntrPrestObj(AfDecimal totIntrPrestObj);

    /**
     * Host Variable DTC-DS-RIGA
     * 
     */
    long getDsRiga();

    void setDsRiga(long dsRiga);

    /**
     * Host Variable DTC-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable DTC-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable DTC-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable DTC-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable DTC-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable DTC-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable DTC-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfe();

    void setImpTrasfe(AfDecimal impTrasfe);

    /**
     * Nullable property for DTC-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfeObj();

    void setImpTrasfeObj(AfDecimal impTrasfeObj);

    /**
     * Host Variable DTC-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrc();

    void setImpTfrStrc(AfDecimal impTfrStrc);

    /**
     * Nullable property for DTC-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrcObj();

    void setImpTfrStrcObj(AfDecimal impTfrStrcObj);

    /**
     * Host Variable DTC-NUM-GG-RITARDO-PAG
     * 
     */
    int getNumGgRitardoPag();

    void setNumGgRitardoPag(int numGgRitardoPag);

    /**
     * Nullable property for DTC-NUM-GG-RITARDO-PAG
     * 
     */
    Integer getNumGgRitardoPagObj();

    void setNumGgRitardoPagObj(Integer numGgRitardoPagObj);

    /**
     * Host Variable DTC-NUM-GG-RIVAL
     * 
     */
    int getNumGgRival();

    void setNumGgRival(int numGgRival);

    /**
     * Nullable property for DTC-NUM-GG-RIVAL
     * 
     */
    Integer getNumGgRivalObj();

    void setNumGgRivalObj(Integer numGgRivalObj);

    /**
     * Host Variable DTC-ACQ-EXP
     * 
     */
    AfDecimal getAcqExp();

    void setAcqExp(AfDecimal acqExp);

    /**
     * Nullable property for DTC-ACQ-EXP
     * 
     */
    AfDecimal getAcqExpObj();

    void setAcqExpObj(AfDecimal acqExpObj);

    /**
     * Host Variable DTC-REMUN-ASS
     * 
     */
    AfDecimal getRemunAss();

    void setRemunAss(AfDecimal remunAss);

    /**
     * Nullable property for DTC-REMUN-ASS
     * 
     */
    AfDecimal getRemunAssObj();

    void setRemunAssObj(AfDecimal remunAssObj);

    /**
     * Host Variable DTC-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInter();

    void setCommisInter(AfDecimal commisInter);

    /**
     * Nullable property for DTC-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInterObj();

    void setCommisInterObj(AfDecimal commisInterObj);

    /**
     * Host Variable DTC-CNBT-ANTIRAC
     * 
     */
    AfDecimal getCnbtAntirac();

    void setCnbtAntirac(AfDecimal cnbtAntirac);

    /**
     * Nullable property for DTC-CNBT-ANTIRAC
     * 
     */
    AfDecimal getCnbtAntiracObj();

    void setCnbtAntiracObj(AfDecimal cnbtAntiracObj);
};
