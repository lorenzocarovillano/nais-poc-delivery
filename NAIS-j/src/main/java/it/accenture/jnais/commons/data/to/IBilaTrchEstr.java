package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BILA_TRCH_ESTR]
 * 
 */
public interface IBilaTrchEstr extends BaseSqlTo {

    /**
     * Host Variable B03-ID-RICH-ESTRAZ-MAS
     * 
     */
    int getB03IdRichEstrazMas();

    void setB03IdRichEstrazMas(int b03IdRichEstrazMas);

    /**
     * Host Variable B03-ID-POLI
     * 
     */
    int getB03IdPoli();

    void setB03IdPoli(int b03IdPoli);

    /**
     * Host Variable B03-ID-ADES
     * 
     */
    int getB03IdAdes();

    void setB03IdAdes(int b03IdAdes);

    /**
     * Host Variable B03-ID-TRCH-DI-GAR
     * 
     */
    int getB03IdTrchDiGar();

    void setB03IdTrchDiGar(int b03IdTrchDiGar);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable B03-ID-BILA-TRCH-ESTR
     * 
     */
    int getB03IdBilaTrchEstr();

    void setB03IdBilaTrchEstr(int b03IdBilaTrchEstr);

    /**
     * Host Variable B03-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable B03-ID-RICH-ESTRAZ-AGG
     * 
     */
    int getIdRichEstrazAgg();

    void setIdRichEstrazAgg(int idRichEstrazAgg);

    /**
     * Nullable property for B03-ID-RICH-ESTRAZ-AGG
     * 
     */
    Integer getIdRichEstrazAggObj();

    void setIdRichEstrazAggObj(Integer idRichEstrazAggObj);

    /**
     * Host Variable B03-FL-SIMULAZIONE
     * 
     */
    char getFlSimulazione();

    void setFlSimulazione(char flSimulazione);

    /**
     * Nullable property for B03-FL-SIMULAZIONE
     * 
     */
    Character getFlSimulazioneObj();

    void setFlSimulazioneObj(Character flSimulazioneObj);

    /**
     * Host Variable B03-DT-RIS-DB
     * 
     */
    String getDtRisDb();

    void setDtRisDb(String dtRisDb);

    /**
     * Host Variable B03-DT-PRODUZIONE-DB
     * 
     */
    String getDtProduzioneDb();

    void setDtProduzioneDb(String dtProduzioneDb);

    /**
     * Host Variable B03-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Host Variable B03-TP-FRM-ASSVA
     * 
     */
    String getTpFrmAssva();

    void setTpFrmAssva(String tpFrmAssva);

    /**
     * Host Variable B03-TP-RAMO-BILA
     * 
     */
    String getTpRamoBila();

    void setTpRamoBila(String tpRamoBila);

    /**
     * Host Variable B03-TP-CALC-RIS
     * 
     */
    String getTpCalcRis();

    void setTpCalcRis(String tpCalcRis);

    /**
     * Host Variable B03-COD-RAMO
     * 
     */
    String getCodRamo();

    void setCodRamo(String codRamo);

    /**
     * Host Variable B03-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Host Variable B03-DT-INI-VAL-TAR-DB
     * 
     */
    String getDtIniValTarDb();

    void setDtIniValTarDb(String dtIniValTarDb);

    /**
     * Nullable property for B03-DT-INI-VAL-TAR-DB
     * 
     */
    String getDtIniValTarDbObj();

    void setDtIniValTarDbObj(String dtIniValTarDbObj);

    /**
     * Host Variable B03-COD-PROD
     * 
     */
    String getCodProd();

    void setCodProd(String codProd);

    /**
     * Nullable property for B03-COD-PROD
     * 
     */
    String getCodProdObj();

    void setCodProdObj(String codProdObj);

    /**
     * Host Variable B03-DT-INI-VLDT-PROD-DB
     * 
     */
    String getDtIniVldtProdDb();

    void setDtIniVldtProdDb(String dtIniVldtProdDb);

    /**
     * Host Variable B03-COD-TARI-ORGN
     * 
     */
    String getCodTariOrgn();

    void setCodTariOrgn(String codTariOrgn);

    /**
     * Nullable property for B03-COD-TARI-ORGN
     * 
     */
    String getCodTariOrgnObj();

    void setCodTariOrgnObj(String codTariOrgnObj);

    /**
     * Host Variable B03-MIN-GARTO-T
     * 
     */
    AfDecimal getMinGartoT();

    void setMinGartoT(AfDecimal minGartoT);

    /**
     * Nullable property for B03-MIN-GARTO-T
     * 
     */
    AfDecimal getMinGartoTObj();

    void setMinGartoTObj(AfDecimal minGartoTObj);

    /**
     * Host Variable B03-TP-TARI
     * 
     */
    String getTpTari();

    void setTpTari(String tpTari);

    /**
     * Nullable property for B03-TP-TARI
     * 
     */
    String getTpTariObj();

    void setTpTariObj(String tpTariObj);

    /**
     * Host Variable B03-TP-PRE
     * 
     */
    char getTpPre();

    void setTpPre(char tpPre);

    /**
     * Nullable property for B03-TP-PRE
     * 
     */
    Character getTpPreObj();

    void setTpPreObj(Character tpPreObj);

    /**
     * Host Variable B03-TP-ADEG-PRE
     * 
     */
    char getTpAdegPre();

    void setTpAdegPre(char tpAdegPre);

    /**
     * Nullable property for B03-TP-ADEG-PRE
     * 
     */
    Character getTpAdegPreObj();

    void setTpAdegPreObj(Character tpAdegPreObj);

    /**
     * Host Variable B03-TP-RIVAL
     * 
     */
    String getTpRival();

    void setTpRival(String tpRival);

    /**
     * Nullable property for B03-TP-RIVAL
     * 
     */
    String getTpRivalObj();

    void setTpRivalObj(String tpRivalObj);

    /**
     * Host Variable B03-FL-DA-TRASF
     * 
     */
    char getFlDaTrasf();

    void setFlDaTrasf(char flDaTrasf);

    /**
     * Nullable property for B03-FL-DA-TRASF
     * 
     */
    Character getFlDaTrasfObj();

    void setFlDaTrasfObj(Character flDaTrasfObj);

    /**
     * Host Variable B03-FL-CAR-CONT
     * 
     */
    char getFlCarCont();

    void setFlCarCont(char flCarCont);

    /**
     * Nullable property for B03-FL-CAR-CONT
     * 
     */
    Character getFlCarContObj();

    void setFlCarContObj(Character flCarContObj);

    /**
     * Host Variable B03-FL-PRE-DA-RIS
     * 
     */
    char getFlPreDaRis();

    void setFlPreDaRis(char flPreDaRis);

    /**
     * Nullable property for B03-FL-PRE-DA-RIS
     * 
     */
    Character getFlPreDaRisObj();

    void setFlPreDaRisObj(Character flPreDaRisObj);

    /**
     * Host Variable B03-FL-PRE-AGG
     * 
     */
    char getFlPreAgg();

    void setFlPreAgg(char flPreAgg);

    /**
     * Nullable property for B03-FL-PRE-AGG
     * 
     */
    Character getFlPreAggObj();

    void setFlPreAggObj(Character flPreAggObj);

    /**
     * Host Variable B03-TP-TRCH
     * 
     */
    String getTpTrch();

    void setTpTrch(String tpTrch);

    /**
     * Nullable property for B03-TP-TRCH
     * 
     */
    String getTpTrchObj();

    void setTpTrchObj(String tpTrchObj);

    /**
     * Host Variable B03-TP-TST
     * 
     */
    String getTpTst();

    void setTpTst(String tpTst);

    /**
     * Nullable property for B03-TP-TST
     * 
     */
    String getTpTstObj();

    void setTpTstObj(String tpTstObj);

    /**
     * Host Variable B03-COD-CONV
     * 
     */
    String getCodConv();

    void setCodConv(String codConv);

    /**
     * Nullable property for B03-COD-CONV
     * 
     */
    String getCodConvObj();

    void setCodConvObj(String codConvObj);

    /**
     * Host Variable B03-DT-DECOR-POLI-DB
     * 
     */
    String getDtDecorPoliDb();

    void setDtDecorPoliDb(String dtDecorPoliDb);

    /**
     * Host Variable B03-DT-DECOR-ADES-DB
     * 
     */
    String getDtDecorAdesDb();

    void setDtDecorAdesDb(String dtDecorAdesDb);

    /**
     * Nullable property for B03-DT-DECOR-ADES-DB
     * 
     */
    String getDtDecorAdesDbObj();

    void setDtDecorAdesDbObj(String dtDecorAdesDbObj);

    /**
     * Host Variable B03-DT-DECOR-TRCH-DB
     * 
     */
    String getDtDecorTrchDb();

    void setDtDecorTrchDb(String dtDecorTrchDb);

    /**
     * Host Variable B03-DT-EMIS-POLI-DB
     * 
     */
    String getDtEmisPoliDb();

    void setDtEmisPoliDb(String dtEmisPoliDb);

    /**
     * Host Variable B03-DT-EMIS-TRCH-DB
     * 
     */
    String getDtEmisTrchDb();

    void setDtEmisTrchDb(String dtEmisTrchDb);

    /**
     * Nullable property for B03-DT-EMIS-TRCH-DB
     * 
     */
    String getDtEmisTrchDbObj();

    void setDtEmisTrchDbObj(String dtEmisTrchDbObj);

    /**
     * Host Variable B03-DT-SCAD-TRCH-DB
     * 
     */
    String getDtScadTrchDb();

    void setDtScadTrchDb(String dtScadTrchDb);

    /**
     * Nullable property for B03-DT-SCAD-TRCH-DB
     * 
     */
    String getDtScadTrchDbObj();

    void setDtScadTrchDbObj(String dtScadTrchDbObj);

    /**
     * Host Variable B03-DT-SCAD-INTMD-DB
     * 
     */
    String getDtScadIntmdDb();

    void setDtScadIntmdDb(String dtScadIntmdDb);

    /**
     * Nullable property for B03-DT-SCAD-INTMD-DB
     * 
     */
    String getDtScadIntmdDbObj();

    void setDtScadIntmdDbObj(String dtScadIntmdDbObj);

    /**
     * Host Variable B03-DT-SCAD-PAG-PRE-DB
     * 
     */
    String getDtScadPagPreDb();

    void setDtScadPagPreDb(String dtScadPagPreDb);

    /**
     * Nullable property for B03-DT-SCAD-PAG-PRE-DB
     * 
     */
    String getDtScadPagPreDbObj();

    void setDtScadPagPreDbObj(String dtScadPagPreDbObj);

    /**
     * Host Variable B03-DT-ULT-PRE-PAG-DB
     * 
     */
    String getDtUltPrePagDb();

    void setDtUltPrePagDb(String dtUltPrePagDb);

    /**
     * Nullable property for B03-DT-ULT-PRE-PAG-DB
     * 
     */
    String getDtUltPrePagDbObj();

    void setDtUltPrePagDbObj(String dtUltPrePagDbObj);

    /**
     * Host Variable B03-DT-NASC-1O-ASSTO-DB
     * 
     */
    String getDtNasc1oAsstoDb();

    void setDtNasc1oAsstoDb(String dtNasc1oAsstoDb);

    /**
     * Nullable property for B03-DT-NASC-1O-ASSTO-DB
     * 
     */
    String getDtNasc1oAsstoDbObj();

    void setDtNasc1oAsstoDbObj(String dtNasc1oAsstoDbObj);

    /**
     * Host Variable B03-SEX-1O-ASSTO
     * 
     */
    char getSex1oAssto();

    void setSex1oAssto(char sex1oAssto);

    /**
     * Nullable property for B03-SEX-1O-ASSTO
     * 
     */
    Character getSex1oAsstoObj();

    void setSex1oAsstoObj(Character sex1oAsstoObj);

    /**
     * Host Variable B03-ETA-AA-1O-ASSTO
     * 
     */
    int getEtaAa1oAssto();

    void setEtaAa1oAssto(int etaAa1oAssto);

    /**
     * Nullable property for B03-ETA-AA-1O-ASSTO
     * 
     */
    Integer getEtaAa1oAsstoObj();

    void setEtaAa1oAsstoObj(Integer etaAa1oAsstoObj);

    /**
     * Host Variable B03-ETA-MM-1O-ASSTO
     * 
     */
    int getEtaMm1oAssto();

    void setEtaMm1oAssto(int etaMm1oAssto);

    /**
     * Nullable property for B03-ETA-MM-1O-ASSTO
     * 
     */
    Integer getEtaMm1oAsstoObj();

    void setEtaMm1oAsstoObj(Integer etaMm1oAsstoObj);

    /**
     * Host Variable B03-ETA-RAGGN-DT-CALC
     * 
     */
    AfDecimal getEtaRaggnDtCalc();

    void setEtaRaggnDtCalc(AfDecimal etaRaggnDtCalc);

    /**
     * Nullable property for B03-ETA-RAGGN-DT-CALC
     * 
     */
    AfDecimal getEtaRaggnDtCalcObj();

    void setEtaRaggnDtCalcObj(AfDecimal etaRaggnDtCalcObj);

    /**
     * Host Variable B03-DUR-AA
     * 
     */
    int getDurAa();

    void setDurAa(int durAa);

    /**
     * Nullable property for B03-DUR-AA
     * 
     */
    Integer getDurAaObj();

    void setDurAaObj(Integer durAaObj);

    /**
     * Host Variable B03-DUR-MM
     * 
     */
    int getDurMm();

    void setDurMm(int durMm);

    /**
     * Nullable property for B03-DUR-MM
     * 
     */
    Integer getDurMmObj();

    void setDurMmObj(Integer durMmObj);

    /**
     * Host Variable B03-DUR-GG
     * 
     */
    int getDurGg();

    void setDurGg(int durGg);

    /**
     * Nullable property for B03-DUR-GG
     * 
     */
    Integer getDurGgObj();

    void setDurGgObj(Integer durGgObj);

    /**
     * Host Variable B03-DUR-1O-PER-AA
     * 
     */
    int getDur1oPerAa();

    void setDur1oPerAa(int dur1oPerAa);

    /**
     * Nullable property for B03-DUR-1O-PER-AA
     * 
     */
    Integer getDur1oPerAaObj();

    void setDur1oPerAaObj(Integer dur1oPerAaObj);

    /**
     * Host Variable B03-DUR-1O-PER-MM
     * 
     */
    int getDur1oPerMm();

    void setDur1oPerMm(int dur1oPerMm);

    /**
     * Nullable property for B03-DUR-1O-PER-MM
     * 
     */
    Integer getDur1oPerMmObj();

    void setDur1oPerMmObj(Integer dur1oPerMmObj);

    /**
     * Host Variable B03-DUR-1O-PER-GG
     * 
     */
    int getDur1oPerGg();

    void setDur1oPerGg(int dur1oPerGg);

    /**
     * Nullable property for B03-DUR-1O-PER-GG
     * 
     */
    Integer getDur1oPerGgObj();

    void setDur1oPerGgObj(Integer dur1oPerGgObj);

    /**
     * Host Variable B03-ANTIDUR-RICOR-PREC
     * 
     */
    int getAntidurRicorPrec();

    void setAntidurRicorPrec(int antidurRicorPrec);

    /**
     * Nullable property for B03-ANTIDUR-RICOR-PREC
     * 
     */
    Integer getAntidurRicorPrecObj();

    void setAntidurRicorPrecObj(Integer antidurRicorPrecObj);

    /**
     * Host Variable B03-ANTIDUR-DT-CALC
     * 
     */
    AfDecimal getAntidurDtCalc();

    void setAntidurDtCalc(AfDecimal antidurDtCalc);

    /**
     * Nullable property for B03-ANTIDUR-DT-CALC
     * 
     */
    AfDecimal getAntidurDtCalcObj();

    void setAntidurDtCalcObj(AfDecimal antidurDtCalcObj);

    /**
     * Host Variable B03-DUR-RES-DT-CALC
     * 
     */
    AfDecimal getDurResDtCalc();

    void setDurResDtCalc(AfDecimal durResDtCalc);

    /**
     * Nullable property for B03-DUR-RES-DT-CALC
     * 
     */
    AfDecimal getDurResDtCalcObj();

    void setDurResDtCalcObj(AfDecimal durResDtCalcObj);

    /**
     * Host Variable B03-TP-STAT-BUS-POLI
     * 
     */
    String getTpStatBusPoli();

    void setTpStatBusPoli(String tpStatBusPoli);

    /**
     * Host Variable B03-TP-CAUS-POLI
     * 
     */
    String getTpCausPoli();

    void setTpCausPoli(String tpCausPoli);

    /**
     * Host Variable B03-TP-STAT-BUS-ADES
     * 
     */
    String getTpStatBusAdes();

    void setTpStatBusAdes(String tpStatBusAdes);

    /**
     * Host Variable B03-TP-CAUS-ADES
     * 
     */
    String getTpCausAdes();

    void setTpCausAdes(String tpCausAdes);

    /**
     * Host Variable B03-TP-STAT-BUS-TRCH
     * 
     */
    String getTpStatBusTrch();

    void setTpStatBusTrch(String tpStatBusTrch);

    /**
     * Host Variable B03-TP-CAUS-TRCH
     * 
     */
    String getTpCausTrch();

    void setTpCausTrch(String tpCausTrch);

    /**
     * Host Variable B03-DT-EFF-CAMB-STAT-DB
     * 
     */
    String getDtEffCambStatDb();

    void setDtEffCambStatDb(String dtEffCambStatDb);

    /**
     * Nullable property for B03-DT-EFF-CAMB-STAT-DB
     * 
     */
    String getDtEffCambStatDbObj();

    void setDtEffCambStatDbObj(String dtEffCambStatDbObj);

    /**
     * Host Variable B03-DT-EMIS-CAMB-STAT-DB
     * 
     */
    String getDtEmisCambStatDb();

    void setDtEmisCambStatDb(String dtEmisCambStatDb);

    /**
     * Nullable property for B03-DT-EMIS-CAMB-STAT-DB
     * 
     */
    String getDtEmisCambStatDbObj();

    void setDtEmisCambStatDbObj(String dtEmisCambStatDbObj);

    /**
     * Host Variable B03-DT-EFF-STAB-DB
     * 
     */
    String getDtEffStabDb();

    void setDtEffStabDb(String dtEffStabDb);

    /**
     * Nullable property for B03-DT-EFF-STAB-DB
     * 
     */
    String getDtEffStabDbObj();

    void setDtEffStabDbObj(String dtEffStabDbObj);

    /**
     * Host Variable B03-CPT-DT-STAB
     * 
     */
    AfDecimal getCptDtStab();

    void setCptDtStab(AfDecimal cptDtStab);

    /**
     * Nullable property for B03-CPT-DT-STAB
     * 
     */
    AfDecimal getCptDtStabObj();

    void setCptDtStabObj(AfDecimal cptDtStabObj);

    /**
     * Host Variable B03-DT-EFF-RIDZ-DB
     * 
     */
    String getDtEffRidzDb();

    void setDtEffRidzDb(String dtEffRidzDb);

    /**
     * Nullable property for B03-DT-EFF-RIDZ-DB
     * 
     */
    String getDtEffRidzDbObj();

    void setDtEffRidzDbObj(String dtEffRidzDbObj);

    /**
     * Host Variable B03-DT-EMIS-RIDZ-DB
     * 
     */
    String getDtEmisRidzDb();

    void setDtEmisRidzDb(String dtEmisRidzDb);

    /**
     * Nullable property for B03-DT-EMIS-RIDZ-DB
     * 
     */
    String getDtEmisRidzDbObj();

    void setDtEmisRidzDbObj(String dtEmisRidzDbObj);

    /**
     * Host Variable B03-CPT-DT-RIDZ
     * 
     */
    AfDecimal getCptDtRidz();

    void setCptDtRidz(AfDecimal cptDtRidz);

    /**
     * Nullable property for B03-CPT-DT-RIDZ
     * 
     */
    AfDecimal getCptDtRidzObj();

    void setCptDtRidzObj(AfDecimal cptDtRidzObj);

    /**
     * Host Variable B03-FRAZ
     * 
     */
    int getFraz();

    void setFraz(int fraz);

    /**
     * Nullable property for B03-FRAZ
     * 
     */
    Integer getFrazObj();

    void setFrazObj(Integer frazObj);

    /**
     * Host Variable B03-DUR-PAG-PRE
     * 
     */
    int getDurPagPre();

    void setDurPagPre(int durPagPre);

    /**
     * Nullable property for B03-DUR-PAG-PRE
     * 
     */
    Integer getDurPagPreObj();

    void setDurPagPreObj(Integer durPagPreObj);

    /**
     * Host Variable B03-NUM-PRE-PATT
     * 
     */
    int getNumPrePatt();

    void setNumPrePatt(int numPrePatt);

    /**
     * Nullable property for B03-NUM-PRE-PATT
     * 
     */
    Integer getNumPrePattObj();

    void setNumPrePattObj(Integer numPrePattObj);

    /**
     * Host Variable B03-FRAZ-INI-EROG-REN
     * 
     */
    int getFrazIniErogRen();

    void setFrazIniErogRen(int frazIniErogRen);

    /**
     * Nullable property for B03-FRAZ-INI-EROG-REN
     * 
     */
    Integer getFrazIniErogRenObj();

    void setFrazIniErogRenObj(Integer frazIniErogRenObj);

    /**
     * Host Variable B03-AA-REN-CER
     * 
     */
    int getAaRenCer();

    void setAaRenCer(int aaRenCer);

    /**
     * Nullable property for B03-AA-REN-CER
     * 
     */
    Integer getAaRenCerObj();

    void setAaRenCerObj(Integer aaRenCerObj);

    /**
     * Host Variable B03-RAT-REN
     * 
     */
    AfDecimal getRatRen();

    void setRatRen(AfDecimal ratRen);

    /**
     * Nullable property for B03-RAT-REN
     * 
     */
    AfDecimal getRatRenObj();

    void setRatRenObj(AfDecimal ratRenObj);

    /**
     * Host Variable B03-COD-DIV
     * 
     */
    String getCodDiv();

    void setCodDiv(String codDiv);

    /**
     * Nullable property for B03-COD-DIV
     * 
     */
    String getCodDivObj();

    void setCodDivObj(String codDivObj);

    /**
     * Host Variable B03-RISCPAR
     * 
     */
    AfDecimal getRiscpar();

    void setRiscpar(AfDecimal riscpar);

    /**
     * Nullable property for B03-RISCPAR
     * 
     */
    AfDecimal getRiscparObj();

    void setRiscparObj(AfDecimal riscparObj);

    /**
     * Host Variable B03-CUM-RISCPAR
     * 
     */
    AfDecimal getCumRiscpar();

    void setCumRiscpar(AfDecimal cumRiscpar);

    /**
     * Nullable property for B03-CUM-RISCPAR
     * 
     */
    AfDecimal getCumRiscparObj();

    void setCumRiscparObj(AfDecimal cumRiscparObj);

    /**
     * Host Variable B03-ULT-RM
     * 
     */
    AfDecimal getUltRm();

    void setUltRm(AfDecimal ultRm);

    /**
     * Nullable property for B03-ULT-RM
     * 
     */
    AfDecimal getUltRmObj();

    void setUltRmObj(AfDecimal ultRmObj);

    /**
     * Host Variable B03-TS-RENDTO-T
     * 
     */
    AfDecimal getTsRendtoT();

    void setTsRendtoT(AfDecimal tsRendtoT);

    /**
     * Nullable property for B03-TS-RENDTO-T
     * 
     */
    AfDecimal getTsRendtoTObj();

    void setTsRendtoTObj(AfDecimal tsRendtoTObj);

    /**
     * Host Variable B03-ALQ-RETR-T
     * 
     */
    AfDecimal getAlqRetrT();

    void setAlqRetrT(AfDecimal alqRetrT);

    /**
     * Nullable property for B03-ALQ-RETR-T
     * 
     */
    AfDecimal getAlqRetrTObj();

    void setAlqRetrTObj(AfDecimal alqRetrTObj);

    /**
     * Host Variable B03-MIN-TRNUT-T
     * 
     */
    AfDecimal getMinTrnutT();

    void setMinTrnutT(AfDecimal minTrnutT);

    /**
     * Nullable property for B03-MIN-TRNUT-T
     * 
     */
    AfDecimal getMinTrnutTObj();

    void setMinTrnutTObj(AfDecimal minTrnutTObj);

    /**
     * Host Variable B03-TS-NET-T
     * 
     */
    AfDecimal getTsNetT();

    void setTsNetT(AfDecimal tsNetT);

    /**
     * Nullable property for B03-TS-NET-T
     * 
     */
    AfDecimal getTsNetTObj();

    void setTsNetTObj(AfDecimal tsNetTObj);

    /**
     * Host Variable B03-DT-ULT-RIVAL-DB
     * 
     */
    String getDtUltRivalDb();

    void setDtUltRivalDb(String dtUltRivalDb);

    /**
     * Nullable property for B03-DT-ULT-RIVAL-DB
     * 
     */
    String getDtUltRivalDbObj();

    void setDtUltRivalDbObj(String dtUltRivalDbObj);

    /**
     * Host Variable B03-PRSTZ-INI
     * 
     */
    AfDecimal getPrstzIni();

    void setPrstzIni(AfDecimal prstzIni);

    /**
     * Nullable property for B03-PRSTZ-INI
     * 
     */
    AfDecimal getPrstzIniObj();

    void setPrstzIniObj(AfDecimal prstzIniObj);

    /**
     * Host Variable B03-PRSTZ-AGG-INI
     * 
     */
    AfDecimal getPrstzAggIni();

    void setPrstzAggIni(AfDecimal prstzAggIni);

    /**
     * Nullable property for B03-PRSTZ-AGG-INI
     * 
     */
    AfDecimal getPrstzAggIniObj();

    void setPrstzAggIniObj(AfDecimal prstzAggIniObj);

    /**
     * Host Variable B03-PRSTZ-AGG-ULT
     * 
     */
    AfDecimal getPrstzAggUlt();

    void setPrstzAggUlt(AfDecimal prstzAggUlt);

    /**
     * Nullable property for B03-PRSTZ-AGG-ULT
     * 
     */
    AfDecimal getPrstzAggUltObj();

    void setPrstzAggUltObj(AfDecimal prstzAggUltObj);

    /**
     * Host Variable B03-RAPPEL
     * 
     */
    AfDecimal getRappel();

    void setRappel(AfDecimal rappel);

    /**
     * Nullable property for B03-RAPPEL
     * 
     */
    AfDecimal getRappelObj();

    void setRappelObj(AfDecimal rappelObj);

    /**
     * Host Variable B03-PRE-PATTUITO-INI
     * 
     */
    AfDecimal getPrePattuitoIni();

    void setPrePattuitoIni(AfDecimal prePattuitoIni);

    /**
     * Nullable property for B03-PRE-PATTUITO-INI
     * 
     */
    AfDecimal getPrePattuitoIniObj();

    void setPrePattuitoIniObj(AfDecimal prePattuitoIniObj);

    /**
     * Host Variable B03-PRE-DOV-INI
     * 
     */
    AfDecimal getPreDovIni();

    void setPreDovIni(AfDecimal preDovIni);

    /**
     * Nullable property for B03-PRE-DOV-INI
     * 
     */
    AfDecimal getPreDovIniObj();

    void setPreDovIniObj(AfDecimal preDovIniObj);

    /**
     * Host Variable B03-PRE-DOV-RIVTO-T
     * 
     */
    AfDecimal getPreDovRivtoT();

    void setPreDovRivtoT(AfDecimal preDovRivtoT);

    /**
     * Nullable property for B03-PRE-DOV-RIVTO-T
     * 
     */
    AfDecimal getPreDovRivtoTObj();

    void setPreDovRivtoTObj(AfDecimal preDovRivtoTObj);

    /**
     * Host Variable B03-PRE-ANNUALIZ-RICOR
     * 
     */
    AfDecimal getPreAnnualizRicor();

    void setPreAnnualizRicor(AfDecimal preAnnualizRicor);

    /**
     * Nullable property for B03-PRE-ANNUALIZ-RICOR
     * 
     */
    AfDecimal getPreAnnualizRicorObj();

    void setPreAnnualizRicorObj(AfDecimal preAnnualizRicorObj);

    /**
     * Host Variable B03-PRE-CONT
     * 
     */
    AfDecimal getPreCont();

    void setPreCont(AfDecimal preCont);

    /**
     * Nullable property for B03-PRE-CONT
     * 
     */
    AfDecimal getPreContObj();

    void setPreContObj(AfDecimal preContObj);

    /**
     * Host Variable B03-PRE-PP-INI
     * 
     */
    AfDecimal getPrePpIni();

    void setPrePpIni(AfDecimal prePpIni);

    /**
     * Nullable property for B03-PRE-PP-INI
     * 
     */
    AfDecimal getPrePpIniObj();

    void setPrePpIniObj(AfDecimal prePpIniObj);

    /**
     * Host Variable B03-RIS-PURA-T
     * 
     */
    AfDecimal getRisPuraT();

    void setRisPuraT(AfDecimal risPuraT);

    /**
     * Nullable property for B03-RIS-PURA-T
     * 
     */
    AfDecimal getRisPuraTObj();

    void setRisPuraTObj(AfDecimal risPuraTObj);

    /**
     * Host Variable B03-PROV-ACQ
     * 
     */
    AfDecimal getProvAcq();

    void setProvAcq(AfDecimal provAcq);

    /**
     * Nullable property for B03-PROV-ACQ
     * 
     */
    AfDecimal getProvAcqObj();

    void setProvAcqObj(AfDecimal provAcqObj);

    /**
     * Host Variable B03-PROV-ACQ-RICOR
     * 
     */
    AfDecimal getProvAcqRicor();

    void setProvAcqRicor(AfDecimal provAcqRicor);

    /**
     * Nullable property for B03-PROV-ACQ-RICOR
     * 
     */
    AfDecimal getProvAcqRicorObj();

    void setProvAcqRicorObj(AfDecimal provAcqRicorObj);

    /**
     * Host Variable B03-PROV-INC
     * 
     */
    AfDecimal getProvInc();

    void setProvInc(AfDecimal provInc);

    /**
     * Nullable property for B03-PROV-INC
     * 
     */
    AfDecimal getProvIncObj();

    void setProvIncObj(AfDecimal provIncObj);

    /**
     * Host Variable B03-CAR-ACQ-NON-SCON
     * 
     */
    AfDecimal getCarAcqNonScon();

    void setCarAcqNonScon(AfDecimal carAcqNonScon);

    /**
     * Nullable property for B03-CAR-ACQ-NON-SCON
     * 
     */
    AfDecimal getCarAcqNonSconObj();

    void setCarAcqNonSconObj(AfDecimal carAcqNonSconObj);

    /**
     * Host Variable B03-OVER-COMM
     * 
     */
    AfDecimal getOverComm();

    void setOverComm(AfDecimal overComm);

    /**
     * Nullable property for B03-OVER-COMM
     * 
     */
    AfDecimal getOverCommObj();

    void setOverCommObj(AfDecimal overCommObj);

    /**
     * Host Variable B03-CAR-ACQ-PRECONTATO
     * 
     */
    AfDecimal getCarAcqPrecontato();

    void setCarAcqPrecontato(AfDecimal carAcqPrecontato);

    /**
     * Nullable property for B03-CAR-ACQ-PRECONTATO
     * 
     */
    AfDecimal getCarAcqPrecontatoObj();

    void setCarAcqPrecontatoObj(AfDecimal carAcqPrecontatoObj);

    /**
     * Host Variable B03-RIS-ACQ-T
     * 
     */
    AfDecimal getRisAcqT();

    void setRisAcqT(AfDecimal risAcqT);

    /**
     * Nullable property for B03-RIS-ACQ-T
     * 
     */
    AfDecimal getRisAcqTObj();

    void setRisAcqTObj(AfDecimal risAcqTObj);

    /**
     * Host Variable B03-RIS-ZIL-T
     * 
     */
    AfDecimal getRisZilT();

    void setRisZilT(AfDecimal risZilT);

    /**
     * Nullable property for B03-RIS-ZIL-T
     * 
     */
    AfDecimal getRisZilTObj();

    void setRisZilTObj(AfDecimal risZilTObj);

    /**
     * Host Variable B03-CAR-GEST-NON-SCON
     * 
     */
    AfDecimal getCarGestNonScon();

    void setCarGestNonScon(AfDecimal carGestNonScon);

    /**
     * Nullable property for B03-CAR-GEST-NON-SCON
     * 
     */
    AfDecimal getCarGestNonSconObj();

    void setCarGestNonSconObj(AfDecimal carGestNonSconObj);

    /**
     * Host Variable B03-CAR-GEST
     * 
     */
    AfDecimal getCarGest();

    void setCarGest(AfDecimal carGest);

    /**
     * Nullable property for B03-CAR-GEST
     * 
     */
    AfDecimal getCarGestObj();

    void setCarGestObj(AfDecimal carGestObj);

    /**
     * Host Variable B03-RIS-SPE-T
     * 
     */
    AfDecimal getRisSpeT();

    void setRisSpeT(AfDecimal risSpeT);

    /**
     * Nullable property for B03-RIS-SPE-T
     * 
     */
    AfDecimal getRisSpeTObj();

    void setRisSpeTObj(AfDecimal risSpeTObj);

    /**
     * Host Variable B03-CAR-INC-NON-SCON
     * 
     */
    AfDecimal getCarIncNonScon();

    void setCarIncNonScon(AfDecimal carIncNonScon);

    /**
     * Nullable property for B03-CAR-INC-NON-SCON
     * 
     */
    AfDecimal getCarIncNonSconObj();

    void setCarIncNonSconObj(AfDecimal carIncNonSconObj);

    /**
     * Host Variable B03-CAR-INC
     * 
     */
    AfDecimal getCarInc();

    void setCarInc(AfDecimal carInc);

    /**
     * Nullable property for B03-CAR-INC
     * 
     */
    AfDecimal getCarIncObj();

    void setCarIncObj(AfDecimal carIncObj);

    /**
     * Host Variable B03-RIS-RISTORNI-CAP
     * 
     */
    AfDecimal getRisRistorniCap();

    void setRisRistorniCap(AfDecimal risRistorniCap);

    /**
     * Nullable property for B03-RIS-RISTORNI-CAP
     * 
     */
    AfDecimal getRisRistorniCapObj();

    void setRisRistorniCapObj(AfDecimal risRistorniCapObj);

    /**
     * Host Variable B03-INTR-TECN
     * 
     */
    AfDecimal getIntrTecn();

    void setIntrTecn(AfDecimal intrTecn);

    /**
     * Nullable property for B03-INTR-TECN
     * 
     */
    AfDecimal getIntrTecnObj();

    void setIntrTecnObj(AfDecimal intrTecnObj);

    /**
     * Host Variable B03-CPT-RSH-MOR
     * 
     */
    AfDecimal getCptRshMor();

    void setCptRshMor(AfDecimal cptRshMor);

    /**
     * Nullable property for B03-CPT-RSH-MOR
     * 
     */
    AfDecimal getCptRshMorObj();

    void setCptRshMorObj(AfDecimal cptRshMorObj);

    /**
     * Host Variable B03-C-SUBRSH-T
     * 
     */
    AfDecimal getcSubrshT();

    void setcSubrshT(AfDecimal cSubrshT);

    /**
     * Nullable property for B03-C-SUBRSH-T
     * 
     */
    AfDecimal getcSubrshTObj();

    void setcSubrshTObj(AfDecimal cSubrshTObj);

    /**
     * Host Variable B03-PRE-RSH-T
     * 
     */
    AfDecimal getPreRshT();

    void setPreRshT(AfDecimal preRshT);

    /**
     * Nullable property for B03-PRE-RSH-T
     * 
     */
    AfDecimal getPreRshTObj();

    void setPreRshTObj(AfDecimal preRshTObj);

    /**
     * Host Variable B03-ALQ-MARG-RIS
     * 
     */
    AfDecimal getAlqMargRis();

    void setAlqMargRis(AfDecimal alqMargRis);

    /**
     * Nullable property for B03-ALQ-MARG-RIS
     * 
     */
    AfDecimal getAlqMargRisObj();

    void setAlqMargRisObj(AfDecimal alqMargRisObj);

    /**
     * Host Variable B03-ALQ-MARG-C-SUBRSH
     * 
     */
    AfDecimal getAlqMargCSubrsh();

    void setAlqMargCSubrsh(AfDecimal alqMargCSubrsh);

    /**
     * Nullable property for B03-ALQ-MARG-C-SUBRSH
     * 
     */
    AfDecimal getAlqMargCSubrshObj();

    void setAlqMargCSubrshObj(AfDecimal alqMargCSubrshObj);

    /**
     * Host Variable B03-TS-RENDTO-SPPR
     * 
     */
    AfDecimal getTsRendtoSppr();

    void setTsRendtoSppr(AfDecimal tsRendtoSppr);

    /**
     * Nullable property for B03-TS-RENDTO-SPPR
     * 
     */
    AfDecimal getTsRendtoSpprObj();

    void setTsRendtoSpprObj(AfDecimal tsRendtoSpprObj);

    /**
     * Host Variable B03-TP-IAS
     * 
     */
    String getTpIas();

    void setTpIas(String tpIas);

    /**
     * Nullable property for B03-TP-IAS
     * 
     */
    String getTpIasObj();

    void setTpIasObj(String tpIasObj);

    /**
     * Host Variable B03-NS-QUO
     * 
     */
    AfDecimal getNsQuo();

    void setNsQuo(AfDecimal nsQuo);

    /**
     * Nullable property for B03-NS-QUO
     * 
     */
    AfDecimal getNsQuoObj();

    void setNsQuoObj(AfDecimal nsQuoObj);

    /**
     * Host Variable B03-TS-MEDIO
     * 
     */
    AfDecimal getTsMedio();

    void setTsMedio(AfDecimal tsMedio);

    /**
     * Nullable property for B03-TS-MEDIO
     * 
     */
    AfDecimal getTsMedioObj();

    void setTsMedioObj(AfDecimal tsMedioObj);

    /**
     * Host Variable B03-CPT-RIASTO
     * 
     */
    AfDecimal getCptRiasto();

    void setCptRiasto(AfDecimal cptRiasto);

    /**
     * Nullable property for B03-CPT-RIASTO
     * 
     */
    AfDecimal getCptRiastoObj();

    void setCptRiastoObj(AfDecimal cptRiastoObj);

    /**
     * Host Variable B03-PRE-RIASTO
     * 
     */
    AfDecimal getPreRiasto();

    void setPreRiasto(AfDecimal preRiasto);

    /**
     * Nullable property for B03-PRE-RIASTO
     * 
     */
    AfDecimal getPreRiastoObj();

    void setPreRiastoObj(AfDecimal preRiastoObj);

    /**
     * Host Variable B03-RIS-RIASTA
     * 
     */
    AfDecimal getRisRiasta();

    void setRisRiasta(AfDecimal risRiasta);

    /**
     * Nullable property for B03-RIS-RIASTA
     * 
     */
    AfDecimal getRisRiastaObj();

    void setRisRiastaObj(AfDecimal risRiastaObj);

    /**
     * Host Variable B03-CPT-RIASTO-ECC
     * 
     */
    AfDecimal getCptRiastoEcc();

    void setCptRiastoEcc(AfDecimal cptRiastoEcc);

    /**
     * Nullable property for B03-CPT-RIASTO-ECC
     * 
     */
    AfDecimal getCptRiastoEccObj();

    void setCptRiastoEccObj(AfDecimal cptRiastoEccObj);

    /**
     * Host Variable B03-PRE-RIASTO-ECC
     * 
     */
    AfDecimal getPreRiastoEcc();

    void setPreRiastoEcc(AfDecimal preRiastoEcc);

    /**
     * Nullable property for B03-PRE-RIASTO-ECC
     * 
     */
    AfDecimal getPreRiastoEccObj();

    void setPreRiastoEccObj(AfDecimal preRiastoEccObj);

    /**
     * Host Variable B03-RIS-RIASTA-ECC
     * 
     */
    AfDecimal getRisRiastaEcc();

    void setRisRiastaEcc(AfDecimal risRiastaEcc);

    /**
     * Nullable property for B03-RIS-RIASTA-ECC
     * 
     */
    AfDecimal getRisRiastaEccObj();

    void setRisRiastaEccObj(AfDecimal risRiastaEccObj);

    /**
     * Host Variable B03-COD-AGE
     * 
     */
    int getCodAge();

    void setCodAge(int codAge);

    /**
     * Nullable property for B03-COD-AGE
     * 
     */
    Integer getCodAgeObj();

    void setCodAgeObj(Integer codAgeObj);

    /**
     * Host Variable B03-COD-SUBAGE
     * 
     */
    int getCodSubage();

    void setCodSubage(int codSubage);

    /**
     * Nullable property for B03-COD-SUBAGE
     * 
     */
    Integer getCodSubageObj();

    void setCodSubageObj(Integer codSubageObj);

    /**
     * Host Variable B03-COD-CAN
     * 
     */
    int getCodCan();

    void setCodCan(int codCan);

    /**
     * Nullable property for B03-COD-CAN
     * 
     */
    Integer getCodCanObj();

    void setCodCanObj(Integer codCanObj);

    /**
     * Host Variable B03-IB-POLI
     * 
     */
    String getIbPoli();

    void setIbPoli(String ibPoli);

    /**
     * Nullable property for B03-IB-POLI
     * 
     */
    String getIbPoliObj();

    void setIbPoliObj(String ibPoliObj);

    /**
     * Host Variable B03-IB-ADES
     * 
     */
    String getIbAdes();

    void setIbAdes(String ibAdes);

    /**
     * Nullable property for B03-IB-ADES
     * 
     */
    String getIbAdesObj();

    void setIbAdesObj(String ibAdesObj);

    /**
     * Host Variable B03-IB-TRCH-DI-GAR
     * 
     */
    String getIbTrchDiGar();

    void setIbTrchDiGar(String ibTrchDiGar);

    /**
     * Nullable property for B03-IB-TRCH-DI-GAR
     * 
     */
    String getIbTrchDiGarObj();

    void setIbTrchDiGarObj(String ibTrchDiGarObj);

    /**
     * Host Variable B03-TP-PRSTZ
     * 
     */
    String getTpPrstz();

    void setTpPrstz(String tpPrstz);

    /**
     * Nullable property for B03-TP-PRSTZ
     * 
     */
    String getTpPrstzObj();

    void setTpPrstzObj(String tpPrstzObj);

    /**
     * Host Variable B03-TP-TRASF
     * 
     */
    String getTpTrasf();

    void setTpTrasf(String tpTrasf);

    /**
     * Nullable property for B03-TP-TRASF
     * 
     */
    String getTpTrasfObj();

    void setTpTrasfObj(String tpTrasfObj);

    /**
     * Host Variable B03-PP-INVRIO-TARI
     * 
     */
    char getPpInvrioTari();

    void setPpInvrioTari(char ppInvrioTari);

    /**
     * Nullable property for B03-PP-INVRIO-TARI
     * 
     */
    Character getPpInvrioTariObj();

    void setPpInvrioTariObj(Character ppInvrioTariObj);

    /**
     * Host Variable B03-COEFF-OPZ-REN
     * 
     */
    AfDecimal getCoeffOpzRen();

    void setCoeffOpzRen(AfDecimal coeffOpzRen);

    /**
     * Nullable property for B03-COEFF-OPZ-REN
     * 
     */
    AfDecimal getCoeffOpzRenObj();

    void setCoeffOpzRenObj(AfDecimal coeffOpzRenObj);

    /**
     * Host Variable B03-COEFF-OPZ-CPT
     * 
     */
    AfDecimal getCoeffOpzCpt();

    void setCoeffOpzCpt(AfDecimal coeffOpzCpt);

    /**
     * Nullable property for B03-COEFF-OPZ-CPT
     * 
     */
    AfDecimal getCoeffOpzCptObj();

    void setCoeffOpzCptObj(AfDecimal coeffOpzCptObj);

    /**
     * Host Variable B03-DUR-PAG-REN
     * 
     */
    int getDurPagRen();

    void setDurPagRen(int durPagRen);

    /**
     * Nullable property for B03-DUR-PAG-REN
     * 
     */
    Integer getDurPagRenObj();

    void setDurPagRenObj(Integer durPagRenObj);

    /**
     * Host Variable B03-VLT
     * 
     */
    String getVlt();

    void setVlt(String vlt);

    /**
     * Nullable property for B03-VLT
     * 
     */
    String getVltObj();

    void setVltObj(String vltObj);

    /**
     * Host Variable B03-RIS-MAT-CHIU-PREC
     * 
     */
    AfDecimal getRisMatChiuPrec();

    void setRisMatChiuPrec(AfDecimal risMatChiuPrec);

    /**
     * Nullable property for B03-RIS-MAT-CHIU-PREC
     * 
     */
    AfDecimal getRisMatChiuPrecObj();

    void setRisMatChiuPrecObj(AfDecimal risMatChiuPrecObj);

    /**
     * Host Variable B03-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Nullable property for B03-COD-FND
     * 
     */
    String getCodFndObj();

    void setCodFndObj(String codFndObj);

    /**
     * Host Variable B03-PRSTZ-T
     * 
     */
    AfDecimal getPrstzT();

    void setPrstzT(AfDecimal prstzT);

    /**
     * Nullable property for B03-PRSTZ-T
     * 
     */
    AfDecimal getPrstzTObj();

    void setPrstzTObj(AfDecimal prstzTObj);

    /**
     * Host Variable B03-TS-TARI-DOV
     * 
     */
    AfDecimal getTsTariDov();

    void setTsTariDov(AfDecimal tsTariDov);

    /**
     * Nullable property for B03-TS-TARI-DOV
     * 
     */
    AfDecimal getTsTariDovObj();

    void setTsTariDovObj(AfDecimal tsTariDovObj);

    /**
     * Host Variable B03-TS-TARI-SCON
     * 
     */
    AfDecimal getTsTariScon();

    void setTsTariScon(AfDecimal tsTariScon);

    /**
     * Nullable property for B03-TS-TARI-SCON
     * 
     */
    AfDecimal getTsTariSconObj();

    void setTsTariSconObj(AfDecimal tsTariSconObj);

    /**
     * Host Variable B03-TS-PP
     * 
     */
    AfDecimal getTsPp();

    void setTsPp(AfDecimal tsPp);

    /**
     * Nullable property for B03-TS-PP
     * 
     */
    AfDecimal getTsPpObj();

    void setTsPpObj(AfDecimal tsPpObj);

    /**
     * Host Variable B03-COEFF-RIS-1-T
     * 
     */
    AfDecimal getCoeffRis1T();

    void setCoeffRis1T(AfDecimal coeffRis1T);

    /**
     * Nullable property for B03-COEFF-RIS-1-T
     * 
     */
    AfDecimal getCoeffRis1TObj();

    void setCoeffRis1TObj(AfDecimal coeffRis1TObj);

    /**
     * Host Variable B03-COEFF-RIS-2-T
     * 
     */
    AfDecimal getCoeffRis2T();

    void setCoeffRis2T(AfDecimal coeffRis2T);

    /**
     * Nullable property for B03-COEFF-RIS-2-T
     * 
     */
    AfDecimal getCoeffRis2TObj();

    void setCoeffRis2TObj(AfDecimal coeffRis2TObj);

    /**
     * Host Variable B03-ABB
     * 
     */
    AfDecimal getAbb();

    void setAbb(AfDecimal abb);

    /**
     * Nullable property for B03-ABB
     * 
     */
    AfDecimal getAbbObj();

    void setAbbObj(AfDecimal abbObj);

    /**
     * Host Variable B03-TP-COASS
     * 
     */
    String getTpCoass();

    void setTpCoass(String tpCoass);

    /**
     * Nullable property for B03-TP-COASS
     * 
     */
    String getTpCoassObj();

    void setTpCoassObj(String tpCoassObj);

    /**
     * Host Variable B03-TRAT-RIASS
     * 
     */
    String getTratRiass();

    void setTratRiass(String tratRiass);

    /**
     * Nullable property for B03-TRAT-RIASS
     * 
     */
    String getTratRiassObj();

    void setTratRiassObj(String tratRiassObj);

    /**
     * Host Variable B03-TRAT-RIASS-ECC
     * 
     */
    String getTratRiassEcc();

    void setTratRiassEcc(String tratRiassEcc);

    /**
     * Nullable property for B03-TRAT-RIASS-ECC
     * 
     */
    String getTratRiassEccObj();

    void setTratRiassEccObj(String tratRiassEccObj);

    /**
     * Host Variable B03-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable B03-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable B03-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable B03-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable B03-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable B03-TP-RGM-FISC
     * 
     */
    String getTpRgmFisc();

    void setTpRgmFisc(String tpRgmFisc);

    /**
     * Nullable property for B03-TP-RGM-FISC
     * 
     */
    String getTpRgmFiscObj();

    void setTpRgmFiscObj(String tpRgmFiscObj);

    /**
     * Host Variable B03-DUR-GAR-AA
     * 
     */
    int getDurGarAa();

    void setDurGarAa(int durGarAa);

    /**
     * Nullable property for B03-DUR-GAR-AA
     * 
     */
    Integer getDurGarAaObj();

    void setDurGarAaObj(Integer durGarAaObj);

    /**
     * Host Variable B03-DUR-GAR-MM
     * 
     */
    int getDurGarMm();

    void setDurGarMm(int durGarMm);

    /**
     * Nullable property for B03-DUR-GAR-MM
     * 
     */
    Integer getDurGarMmObj();

    void setDurGarMmObj(Integer durGarMmObj);

    /**
     * Host Variable B03-DUR-GAR-GG
     * 
     */
    int getDurGarGg();

    void setDurGarGg(int durGarGg);

    /**
     * Nullable property for B03-DUR-GAR-GG
     * 
     */
    Integer getDurGarGgObj();

    void setDurGarGgObj(Integer durGarGgObj);

    /**
     * Host Variable B03-ANTIDUR-CALC-365
     * 
     */
    AfDecimal getAntidurCalc365();

    void setAntidurCalc365(AfDecimal antidurCalc365);

    /**
     * Nullable property for B03-ANTIDUR-CALC-365
     * 
     */
    AfDecimal getAntidurCalc365Obj();

    void setAntidurCalc365Obj(AfDecimal antidurCalc365Obj);

    /**
     * Host Variable B03-COD-FISC-CNTR
     * 
     */
    String getCodFiscCntr();

    void setCodFiscCntr(String codFiscCntr);

    /**
     * Nullable property for B03-COD-FISC-CNTR
     * 
     */
    String getCodFiscCntrObj();

    void setCodFiscCntrObj(String codFiscCntrObj);

    /**
     * Host Variable B03-COD-FISC-ASSTO1
     * 
     */
    String getCodFiscAssto1();

    void setCodFiscAssto1(String codFiscAssto1);

    /**
     * Nullable property for B03-COD-FISC-ASSTO1
     * 
     */
    String getCodFiscAssto1Obj();

    void setCodFiscAssto1Obj(String codFiscAssto1Obj);

    /**
     * Host Variable B03-COD-FISC-ASSTO2
     * 
     */
    String getCodFiscAssto2();

    void setCodFiscAssto2(String codFiscAssto2);

    /**
     * Nullable property for B03-COD-FISC-ASSTO2
     * 
     */
    String getCodFiscAssto2Obj();

    void setCodFiscAssto2Obj(String codFiscAssto2Obj);

    /**
     * Host Variable B03-COD-FISC-ASSTO3
     * 
     */
    String getCodFiscAssto3();

    void setCodFiscAssto3(String codFiscAssto3);

    /**
     * Nullable property for B03-COD-FISC-ASSTO3
     * 
     */
    String getCodFiscAssto3Obj();

    void setCodFiscAssto3Obj(String codFiscAssto3Obj);

    /**
     * Host Variable B03-CAUS-SCON-VCHAR
     * 
     */
    String getCausSconVchar();

    void setCausSconVchar(String causSconVchar);

    /**
     * Nullable property for B03-CAUS-SCON-VCHAR
     * 
     */
    String getCausSconVcharObj();

    void setCausSconVcharObj(String causSconVcharObj);

    /**
     * Host Variable B03-EMIT-TIT-OPZ-VCHAR
     * 
     */
    String getEmitTitOpzVchar();

    void setEmitTitOpzVchar(String emitTitOpzVchar);

    /**
     * Nullable property for B03-EMIT-TIT-OPZ-VCHAR
     * 
     */
    String getEmitTitOpzVcharObj();

    void setEmitTitOpzVcharObj(String emitTitOpzVcharObj);

    /**
     * Host Variable B03-QTZ-SP-Z-COUP-EMIS
     * 
     */
    AfDecimal getQtzSpZCoupEmis();

    void setQtzSpZCoupEmis(AfDecimal qtzSpZCoupEmis);

    /**
     * Nullable property for B03-QTZ-SP-Z-COUP-EMIS
     * 
     */
    AfDecimal getQtzSpZCoupEmisObj();

    void setQtzSpZCoupEmisObj(AfDecimal qtzSpZCoupEmisObj);

    /**
     * Host Variable B03-QTZ-SP-Z-OPZ-EMIS
     * 
     */
    AfDecimal getQtzSpZOpzEmis();

    void setQtzSpZOpzEmis(AfDecimal qtzSpZOpzEmis);

    /**
     * Nullable property for B03-QTZ-SP-Z-OPZ-EMIS
     * 
     */
    AfDecimal getQtzSpZOpzEmisObj();

    void setQtzSpZOpzEmisObj(AfDecimal qtzSpZOpzEmisObj);

    /**
     * Host Variable B03-QTZ-SP-Z-COUP-DT-C
     * 
     */
    AfDecimal getQtzSpZCoupDtC();

    void setQtzSpZCoupDtC(AfDecimal qtzSpZCoupDtC);

    /**
     * Nullable property for B03-QTZ-SP-Z-COUP-DT-C
     * 
     */
    AfDecimal getQtzSpZCoupDtCObj();

    void setQtzSpZCoupDtCObj(AfDecimal qtzSpZCoupDtCObj);

    /**
     * Host Variable B03-QTZ-SP-Z-OPZ-DT-CA
     * 
     */
    AfDecimal getQtzSpZOpzDtCa();

    void setQtzSpZOpzDtCa(AfDecimal qtzSpZOpzDtCa);

    /**
     * Nullable property for B03-QTZ-SP-Z-OPZ-DT-CA
     * 
     */
    AfDecimal getQtzSpZOpzDtCaObj();

    void setQtzSpZOpzDtCaObj(AfDecimal qtzSpZOpzDtCaObj);

    /**
     * Host Variable B03-QTZ-TOT-EMIS
     * 
     */
    AfDecimal getQtzTotEmis();

    void setQtzTotEmis(AfDecimal qtzTotEmis);

    /**
     * Nullable property for B03-QTZ-TOT-EMIS
     * 
     */
    AfDecimal getQtzTotEmisObj();

    void setQtzTotEmisObj(AfDecimal qtzTotEmisObj);

    /**
     * Host Variable B03-QTZ-TOT-DT-CALC
     * 
     */
    AfDecimal getQtzTotDtCalc();

    void setQtzTotDtCalc(AfDecimal qtzTotDtCalc);

    /**
     * Nullable property for B03-QTZ-TOT-DT-CALC
     * 
     */
    AfDecimal getQtzTotDtCalcObj();

    void setQtzTotDtCalcObj(AfDecimal qtzTotDtCalcObj);

    /**
     * Host Variable B03-QTZ-TOT-DT-ULT-BIL
     * 
     */
    AfDecimal getQtzTotDtUltBil();

    void setQtzTotDtUltBil(AfDecimal qtzTotDtUltBil);

    /**
     * Nullable property for B03-QTZ-TOT-DT-ULT-BIL
     * 
     */
    AfDecimal getQtzTotDtUltBilObj();

    void setQtzTotDtUltBilObj(AfDecimal qtzTotDtUltBilObj);

    /**
     * Host Variable B03-DT-QTZ-EMIS-DB
     * 
     */
    String getDtQtzEmisDb();

    void setDtQtzEmisDb(String dtQtzEmisDb);

    /**
     * Nullable property for B03-DT-QTZ-EMIS-DB
     * 
     */
    String getDtQtzEmisDbObj();

    void setDtQtzEmisDbObj(String dtQtzEmisDbObj);

    /**
     * Host Variable B03-PC-CAR-GEST
     * 
     */
    AfDecimal getPcCarGest();

    void setPcCarGest(AfDecimal pcCarGest);

    /**
     * Nullable property for B03-PC-CAR-GEST
     * 
     */
    AfDecimal getPcCarGestObj();

    void setPcCarGestObj(AfDecimal pcCarGestObj);

    /**
     * Host Variable B03-PC-CAR-ACQ
     * 
     */
    AfDecimal getPcCarAcq();

    void setPcCarAcq(AfDecimal pcCarAcq);

    /**
     * Nullable property for B03-PC-CAR-ACQ
     * 
     */
    AfDecimal getPcCarAcqObj();

    void setPcCarAcqObj(AfDecimal pcCarAcqObj);

    /**
     * Host Variable B03-IMP-CAR-CASO-MOR
     * 
     */
    AfDecimal getImpCarCasoMor();

    void setImpCarCasoMor(AfDecimal impCarCasoMor);

    /**
     * Nullable property for B03-IMP-CAR-CASO-MOR
     * 
     */
    AfDecimal getImpCarCasoMorObj();

    void setImpCarCasoMorObj(AfDecimal impCarCasoMorObj);

    /**
     * Host Variable B03-PC-CAR-MOR
     * 
     */
    AfDecimal getPcCarMor();

    void setPcCarMor(AfDecimal pcCarMor);

    /**
     * Nullable property for B03-PC-CAR-MOR
     * 
     */
    AfDecimal getPcCarMorObj();

    void setPcCarMorObj(AfDecimal pcCarMorObj);

    /**
     * Host Variable B03-TP-VERS
     * 
     */
    char getTpVers();

    void setTpVers(char tpVers);

    /**
     * Nullable property for B03-TP-VERS
     * 
     */
    Character getTpVersObj();

    void setTpVersObj(Character tpVersObj);

    /**
     * Host Variable B03-FL-SWITCH
     * 
     */
    char getFlSwitch();

    void setFlSwitch(char flSwitch);

    /**
     * Nullable property for B03-FL-SWITCH
     * 
     */
    Character getFlSwitchObj();

    void setFlSwitchObj(Character flSwitchObj);

    /**
     * Host Variable B03-FL-IAS
     * 
     */
    char getFlIas();

    void setFlIas(char flIas);

    /**
     * Nullable property for B03-FL-IAS
     * 
     */
    Character getFlIasObj();

    void setFlIasObj(Character flIasObj);

    /**
     * Host Variable B03-DIR
     * 
     */
    AfDecimal getDir();

    void setDir(AfDecimal dir);

    /**
     * Nullable property for B03-DIR
     * 
     */
    AfDecimal getDirObj();

    void setDirObj(AfDecimal dirObj);

    /**
     * Host Variable B03-TP-COP-CASO-MOR
     * 
     */
    String getTpCopCasoMor();

    void setTpCopCasoMor(String tpCopCasoMor);

    /**
     * Nullable property for B03-TP-COP-CASO-MOR
     * 
     */
    String getTpCopCasoMorObj();

    void setTpCopCasoMorObj(String tpCopCasoMorObj);

    /**
     * Host Variable B03-MET-RISC-SPCL
     * 
     */
    short getMetRiscSpcl();

    void setMetRiscSpcl(short metRiscSpcl);

    /**
     * Nullable property for B03-MET-RISC-SPCL
     * 
     */
    Short getMetRiscSpclObj();

    void setMetRiscSpclObj(Short metRiscSpclObj);

    /**
     * Host Variable B03-TP-STAT-INVST
     * 
     */
    String getTpStatInvst();

    void setTpStatInvst(String tpStatInvst);

    /**
     * Nullable property for B03-TP-STAT-INVST
     * 
     */
    String getTpStatInvstObj();

    void setTpStatInvstObj(String tpStatInvstObj);

    /**
     * Host Variable B03-COD-PRDT
     * 
     */
    int getCodPrdt();

    void setCodPrdt(int codPrdt);

    /**
     * Nullable property for B03-COD-PRDT
     * 
     */
    Integer getCodPrdtObj();

    void setCodPrdtObj(Integer codPrdtObj);

    /**
     * Host Variable B03-STAT-ASSTO-1
     * 
     */
    char getStatAssto1();

    void setStatAssto1(char statAssto1);

    /**
     * Nullable property for B03-STAT-ASSTO-1
     * 
     */
    Character getStatAssto1Obj();

    void setStatAssto1Obj(Character statAssto1Obj);

    /**
     * Host Variable B03-STAT-ASSTO-2
     * 
     */
    char getStatAssto2();

    void setStatAssto2(char statAssto2);

    /**
     * Nullable property for B03-STAT-ASSTO-2
     * 
     */
    Character getStatAssto2Obj();

    void setStatAssto2Obj(Character statAssto2Obj);

    /**
     * Host Variable B03-STAT-ASSTO-3
     * 
     */
    char getStatAssto3();

    void setStatAssto3(char statAssto3);

    /**
     * Nullable property for B03-STAT-ASSTO-3
     * 
     */
    Character getStatAssto3Obj();

    void setStatAssto3Obj(Character statAssto3Obj);

    /**
     * Host Variable B03-CPT-ASSTO-INI-MOR
     * 
     */
    AfDecimal getCptAsstoIniMor();

    void setCptAsstoIniMor(AfDecimal cptAsstoIniMor);

    /**
     * Nullable property for B03-CPT-ASSTO-INI-MOR
     * 
     */
    AfDecimal getCptAsstoIniMorObj();

    void setCptAsstoIniMorObj(AfDecimal cptAsstoIniMorObj);

    /**
     * Host Variable B03-TS-STAB-PRE
     * 
     */
    AfDecimal getTsStabPre();

    void setTsStabPre(AfDecimal tsStabPre);

    /**
     * Nullable property for B03-TS-STAB-PRE
     * 
     */
    AfDecimal getTsStabPreObj();

    void setTsStabPreObj(AfDecimal tsStabPreObj);

    /**
     * Host Variable B03-DIR-EMIS
     * 
     */
    AfDecimal getDirEmis();

    void setDirEmis(AfDecimal dirEmis);

    /**
     * Nullable property for B03-DIR-EMIS
     * 
     */
    AfDecimal getDirEmisObj();

    void setDirEmisObj(AfDecimal dirEmisObj);

    /**
     * Host Variable B03-DT-INC-ULT-PRE-DB
     * 
     */
    String getDtIncUltPreDb();

    void setDtIncUltPreDb(String dtIncUltPreDb);

    /**
     * Nullable property for B03-DT-INC-ULT-PRE-DB
     * 
     */
    String getDtIncUltPreDbObj();

    void setDtIncUltPreDbObj(String dtIncUltPreDbObj);

    /**
     * Host Variable B03-STAT-TBGC-ASSTO-1
     * 
     */
    char getStatTbgcAssto1();

    void setStatTbgcAssto1(char statTbgcAssto1);

    /**
     * Nullable property for B03-STAT-TBGC-ASSTO-1
     * 
     */
    Character getStatTbgcAssto1Obj();

    void setStatTbgcAssto1Obj(Character statTbgcAssto1Obj);

    /**
     * Host Variable B03-STAT-TBGC-ASSTO-2
     * 
     */
    char getStatTbgcAssto2();

    void setStatTbgcAssto2(char statTbgcAssto2);

    /**
     * Nullable property for B03-STAT-TBGC-ASSTO-2
     * 
     */
    Character getStatTbgcAssto2Obj();

    void setStatTbgcAssto2Obj(Character statTbgcAssto2Obj);

    /**
     * Host Variable B03-STAT-TBGC-ASSTO-3
     * 
     */
    char getStatTbgcAssto3();

    void setStatTbgcAssto3(char statTbgcAssto3);

    /**
     * Nullable property for B03-STAT-TBGC-ASSTO-3
     * 
     */
    Character getStatTbgcAssto3Obj();

    void setStatTbgcAssto3Obj(Character statTbgcAssto3Obj);

    /**
     * Host Variable B03-FRAZ-DECR-CPT
     * 
     */
    int getFrazDecrCpt();

    void setFrazDecrCpt(int frazDecrCpt);

    /**
     * Nullable property for B03-FRAZ-DECR-CPT
     * 
     */
    Integer getFrazDecrCptObj();

    void setFrazDecrCptObj(Integer frazDecrCptObj);

    /**
     * Host Variable B03-PRE-PP-ULT
     * 
     */
    AfDecimal getPrePpUlt();

    void setPrePpUlt(AfDecimal prePpUlt);

    /**
     * Nullable property for B03-PRE-PP-ULT
     * 
     */
    AfDecimal getPrePpUltObj();

    void setPrePpUltObj(AfDecimal prePpUltObj);

    /**
     * Host Variable B03-ACQ-EXP
     * 
     */
    AfDecimal getAcqExp();

    void setAcqExp(AfDecimal acqExp);

    /**
     * Nullable property for B03-ACQ-EXP
     * 
     */
    AfDecimal getAcqExpObj();

    void setAcqExpObj(AfDecimal acqExpObj);

    /**
     * Host Variable B03-REMUN-ASS
     * 
     */
    AfDecimal getRemunAss();

    void setRemunAss(AfDecimal remunAss);

    /**
     * Nullable property for B03-REMUN-ASS
     * 
     */
    AfDecimal getRemunAssObj();

    void setRemunAssObj(AfDecimal remunAssObj);

    /**
     * Host Variable B03-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInter();

    void setCommisInter(AfDecimal commisInter);

    /**
     * Nullable property for B03-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInterObj();

    void setCommisInterObj(AfDecimal commisInterObj);

    /**
     * Host Variable B03-NUM-FINANZ
     * 
     */
    String getNumFinanz();

    void setNumFinanz(String numFinanz);

    /**
     * Nullable property for B03-NUM-FINANZ
     * 
     */
    String getNumFinanzObj();

    void setNumFinanzObj(String numFinanzObj);

    /**
     * Host Variable B03-TP-ACC-COMM
     * 
     */
    String getTpAccComm();

    void setTpAccComm(String tpAccComm);

    /**
     * Nullable property for B03-TP-ACC-COMM
     * 
     */
    String getTpAccCommObj();

    void setTpAccCommObj(String tpAccCommObj);

    /**
     * Host Variable B03-IB-ACC-COMM
     * 
     */
    String getIbAccComm();

    void setIbAccComm(String ibAccComm);

    /**
     * Nullable property for B03-IB-ACC-COMM
     * 
     */
    String getIbAccCommObj();

    void setIbAccCommObj(String ibAccCommObj);

    /**
     * Host Variable B03-RAMO-BILA
     * 
     */
    String getRamoBila();

    void setRamoBila(String ramoBila);

    /**
     * Host Variable B03-CARZ
     * 
     */
    int getCarz();

    void setCarz(int carz);

    /**
     * Nullable property for B03-CARZ
     * 
     */
    Integer getCarzObj();

    void setCarzObj(Integer carzObj);
};
