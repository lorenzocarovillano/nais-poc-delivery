package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [STAT_OGG_BUS, TRCH_DI_GAR]
 * 
 */
public interface IStatOggBusTrchDiGar extends BaseSqlTo {

    /**
     * Host Variable TGA-ID-POLI
     * 
     */
    int getTgaIdPoli();

    void setTgaIdPoli(int tgaIdPoli);

    /**
     * Host Variable WK-ID-ADES-DA
     * 
     */
    int getWkIdAdesDa();

    void setWkIdAdesDa(int wkIdAdesDa);

    /**
     * Host Variable WK-ID-ADES-A
     * 
     */
    int getWkIdAdesA();

    void setWkIdAdesA(int wkIdAdesA);

    /**
     * Host Variable WK-ID-GAR-DA
     * 
     */
    int getWkIdGarDa();

    void setWkIdGarDa(int wkIdGarDa);

    /**
     * Host Variable WK-ID-GAR-A
     * 
     */
    int getWkIdGarA();

    void setWkIdGarA(int wkIdGarA);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable LDBV1361-TP-STAT-BUS-01
     * 
     */
    String getLdbv1361TpStatBus01();

    void setLdbv1361TpStatBus01(String ldbv1361TpStatBus01);

    /**
     * Host Variable LDBV1361-TP-STAT-BUS-02
     * 
     */
    String getLdbv1361TpStatBus02();

    void setLdbv1361TpStatBus02(String ldbv1361TpStatBus02);

    /**
     * Host Variable LDBV1361-TP-STAT-BUS-03
     * 
     */
    String getLdbv1361TpStatBus03();

    void setLdbv1361TpStatBus03(String ldbv1361TpStatBus03);

    /**
     * Host Variable LDBV1361-TP-STAT-BUS-04
     * 
     */
    String getLdbv1361TpStatBus04();

    void setLdbv1361TpStatBus04(String ldbv1361TpStatBus04);

    /**
     * Host Variable LDBV1361-TP-STAT-BUS-05
     * 
     */
    String getLdbv1361TpStatBus05();

    void setLdbv1361TpStatBus05(String ldbv1361TpStatBus05);

    /**
     * Host Variable LDBV1361-TP-STAT-BUS-06
     * 
     */
    String getLdbv1361TpStatBus06();

    void setLdbv1361TpStatBus06(String ldbv1361TpStatBus06);

    /**
     * Host Variable LDBV1361-TP-STAT-BUS-07
     * 
     */
    String getLdbv1361TpStatBus07();

    void setLdbv1361TpStatBus07(String ldbv1361TpStatBus07);

    /**
     * Host Variable LDBV1361-TP-CAUS-01
     * 
     */
    String getLdbv1361TpCaus01();

    void setLdbv1361TpCaus01(String ldbv1361TpCaus01);

    /**
     * Host Variable LDBV1361-TP-CAUS-02
     * 
     */
    String getLdbv1361TpCaus02();

    void setLdbv1361TpCaus02(String ldbv1361TpCaus02);

    /**
     * Host Variable LDBV1361-TP-CAUS-03
     * 
     */
    String getLdbv1361TpCaus03();

    void setLdbv1361TpCaus03(String ldbv1361TpCaus03);

    /**
     * Host Variable LDBV1361-TP-CAUS-04
     * 
     */
    String getLdbv1361TpCaus04();

    void setLdbv1361TpCaus04(String ldbv1361TpCaus04);

    /**
     * Host Variable LDBV1361-TP-CAUS-05
     * 
     */
    String getLdbv1361TpCaus05();

    void setLdbv1361TpCaus05(String ldbv1361TpCaus05);

    /**
     * Host Variable LDBV1361-TP-CAUS-06
     * 
     */
    String getLdbv1361TpCaus06();

    void setLdbv1361TpCaus06(String ldbv1361TpCaus06);

    /**
     * Host Variable LDBV1361-TP-CAUS-07
     * 
     */
    String getLdbv1361TpCaus07();

    void setLdbv1361TpCaus07(String ldbv1361TpCaus07);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBV3021-ID-OGG
     * 
     */
    int getLdbv3021IdOgg();

    void setLdbv3021IdOgg(int ldbv3021IdOgg);

    /**
     * Host Variable LDBV3021-TP-OGG
     * 
     */
    String getLdbv3021TpOgg();

    void setLdbv3021TpOgg(String ldbv3021TpOgg);

    /**
     * Host Variable LDBV3021-TP-STAT-BUS
     * 
     */
    String getLdbv3021TpStatBus();

    void setLdbv3021TpStatBus(String ldbv3021TpStatBus);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS1
     * 
     */
    String getLdbv3021TpCausBus1();

    void setLdbv3021TpCausBus1(String ldbv3021TpCausBus1);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS2
     * 
     */
    String getLdbv3021TpCausBus2();

    void setLdbv3021TpCausBus2(String ldbv3021TpCausBus2);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS3
     * 
     */
    String getLdbv3021TpCausBus3();

    void setLdbv3021TpCausBus3(String ldbv3021TpCausBus3);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS4
     * 
     */
    String getLdbv3021TpCausBus4();

    void setLdbv3021TpCausBus4(String ldbv3021TpCausBus4);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS5
     * 
     */
    String getLdbv3021TpCausBus5();

    void setLdbv3021TpCausBus5(String ldbv3021TpCausBus5);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS6
     * 
     */
    String getLdbv3021TpCausBus6();

    void setLdbv3021TpCausBus6(String ldbv3021TpCausBus6);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS7
     * 
     */
    String getLdbv3021TpCausBus7();

    void setLdbv3021TpCausBus7(String ldbv3021TpCausBus7);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS8
     * 
     */
    String getLdbv3021TpCausBus8();

    void setLdbv3021TpCausBus8(String ldbv3021TpCausBus8);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS9
     * 
     */
    String getLdbv3021TpCausBus9();

    void setLdbv3021TpCausBus9(String ldbv3021TpCausBus9);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS10
     * 
     */
    String getLdbv3021TpCausBus10();

    void setLdbv3021TpCausBus10(String ldbv3021TpCausBus10);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS11
     * 
     */
    String getLdbv3021TpCausBus11();

    void setLdbv3021TpCausBus11(String ldbv3021TpCausBus11);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS12
     * 
     */
    String getLdbv3021TpCausBus12();

    void setLdbv3021TpCausBus12(String ldbv3021TpCausBus12);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS13
     * 
     */
    String getLdbv3021TpCausBus13();

    void setLdbv3021TpCausBus13(String ldbv3021TpCausBus13);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS14
     * 
     */
    String getLdbv3021TpCausBus14();

    void setLdbv3021TpCausBus14(String ldbv3021TpCausBus14);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS15
     * 
     */
    String getLdbv3021TpCausBus15();

    void setLdbv3021TpCausBus15(String ldbv3021TpCausBus15);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS16
     * 
     */
    String getLdbv3021TpCausBus16();

    void setLdbv3021TpCausBus16(String ldbv3021TpCausBus16);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS17
     * 
     */
    String getLdbv3021TpCausBus17();

    void setLdbv3021TpCausBus17(String ldbv3021TpCausBus17);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS18
     * 
     */
    String getLdbv3021TpCausBus18();

    void setLdbv3021TpCausBus18(String ldbv3021TpCausBus18);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS19
     * 
     */
    String getLdbv3021TpCausBus19();

    void setLdbv3021TpCausBus19(String ldbv3021TpCausBus19);

    /**
     * Host Variable LDBV3021-TP-CAUS-BUS20
     * 
     */
    String getLdbv3021TpCausBus20();

    void setLdbv3021TpCausBus20(String ldbv3021TpCausBus20);

    /**
     * Host Variable LDBV3421-ID-POLI
     * 
     */
    int getLdbv3421IdPoli();

    void setLdbv3421IdPoli(int ldbv3421IdPoli);

    /**
     * Host Variable LDBV3421-ID-ADES
     * 
     */
    int getLdbv3421IdAdes();

    void setLdbv3421IdAdes(int ldbv3421IdAdes);

    /**
     * Host Variable LDBV3421-ID-GAR
     * 
     */
    int getLdbv3421IdGar();

    void setLdbv3421IdGar(int ldbv3421IdGar);

    /**
     * Host Variable LDBV3421-TP-OGG
     * 
     */
    String getLdbv3421TpOgg();

    void setLdbv3421TpOgg(String ldbv3421TpOgg);

    /**
     * Host Variable LDBV3421-TP-STAT-BUS-1
     * 
     */
    String getLdbv3421TpStatBus1();

    void setLdbv3421TpStatBus1(String ldbv3421TpStatBus1);

    /**
     * Host Variable LDBV3421-TP-STAT-BUS-2
     * 
     */
    String getLdbv3421TpStatBus2();

    void setLdbv3421TpStatBus2(String ldbv3421TpStatBus2);

    /**
     * Host Variable LDBVD511-ID-POLI
     * 
     */
    int getLdbvd511IdPoli();

    void setLdbvd511IdPoli(int ldbvd511IdPoli);

    /**
     * Host Variable LDBVD511-ID-ADES
     * 
     */
    int getLdbvd511IdAdes();

    void setLdbvd511IdAdes(int ldbvd511IdAdes);

    /**
     * Host Variable LDBVD511-TP-OGG
     * 
     */
    String getLdbvd511TpOgg();

    void setLdbvd511TpOgg(String ldbvd511TpOgg);

    /**
     * Host Variable LDBVD511-TP-STAT-BUS
     * 
     */
    String getLdbvd511TpStatBus();

    void setLdbvd511TpStatBus(String ldbvd511TpStatBus);

    /**
     * Host Variable LDBVD511-TP-CAUS
     * 
     */
    String getLdbvd511TpCaus();

    void setLdbvd511TpCaus(String ldbvd511TpCaus);

    /**
     * Host Variable LDBI0731-ID-POLI
     * 
     */
    int getLdbi0731IdPoli();

    void setLdbi0731IdPoli(int ldbi0731IdPoli);

    /**
     * Host Variable LDBI0731-ID-ADES
     * 
     */
    int getLdbi0731IdAdes();

    void setLdbi0731IdAdes(int ldbi0731IdAdes);

    /**
     * Host Variable LDBI0731-TP-STAT-BUS
     * 
     */
    String getLdbi0731TpStatBus();

    void setLdbi0731TpStatBus(String ldbi0731TpStatBus);

    /**
     * Host Variable TGA-ID-TRCH-DI-GAR
     * 
     */
    int getTgaIdTrchDiGar();

    void setTgaIdTrchDiGar(int tgaIdTrchDiGar);

    /**
     * Host Variable TGA-ID-GAR
     * 
     */
    int getTgaIdGar();

    void setTgaIdGar(int tgaIdGar);

    /**
     * Host Variable TGA-ID-ADES
     * 
     */
    int getTgaIdAdes();

    void setTgaIdAdes(int tgaIdAdes);

    /**
     * Host Variable TGA-ID-MOVI-CRZ
     * 
     */
    int getTgaIdMoviCrz();

    void setTgaIdMoviCrz(int tgaIdMoviCrz);

    /**
     * Host Variable TGA-ID-MOVI-CHIU
     * 
     */
    int getTgaIdMoviChiu();

    void setTgaIdMoviChiu(int tgaIdMoviChiu);

    /**
     * Nullable property for TGA-ID-MOVI-CHIU
     * 
     */
    Integer getTgaIdMoviChiuObj();

    void setTgaIdMoviChiuObj(Integer tgaIdMoviChiuObj);

    /**
     * Host Variable TGA-DT-INI-EFF-DB
     * 
     */
    String getTgaDtIniEffDb();

    void setTgaDtIniEffDb(String tgaDtIniEffDb);

    /**
     * Host Variable TGA-DT-END-EFF-DB
     * 
     */
    String getTgaDtEndEffDb();

    void setTgaDtEndEffDb(String tgaDtEndEffDb);

    /**
     * Host Variable TGA-COD-COMP-ANIA
     * 
     */
    int getTgaCodCompAnia();

    void setTgaCodCompAnia(int tgaCodCompAnia);

    /**
     * Host Variable TGA-DT-DECOR-DB
     * 
     */
    String getTgaDtDecorDb();

    void setTgaDtDecorDb(String tgaDtDecorDb);

    /**
     * Host Variable TGA-DT-SCAD-DB
     * 
     */
    String getTgaDtScadDb();

    void setTgaDtScadDb(String tgaDtScadDb);

    /**
     * Nullable property for TGA-DT-SCAD-DB
     * 
     */
    String getTgaDtScadDbObj();

    void setTgaDtScadDbObj(String tgaDtScadDbObj);

    /**
     * Host Variable TGA-IB-OGG
     * 
     */
    String getTgaIbOgg();

    void setTgaIbOgg(String tgaIbOgg);

    /**
     * Nullable property for TGA-IB-OGG
     * 
     */
    String getTgaIbOggObj();

    void setTgaIbOggObj(String tgaIbOggObj);

    /**
     * Host Variable TGA-TP-RGM-FISC
     * 
     */
    String getTgaTpRgmFisc();

    void setTgaTpRgmFisc(String tgaTpRgmFisc);

    /**
     * Host Variable TGA-DT-EMIS-DB
     * 
     */
    String getTgaDtEmisDb();

    void setTgaDtEmisDb(String tgaDtEmisDb);

    /**
     * Nullable property for TGA-DT-EMIS-DB
     * 
     */
    String getTgaDtEmisDbObj();

    void setTgaDtEmisDbObj(String tgaDtEmisDbObj);

    /**
     * Host Variable TGA-TP-TRCH
     * 
     */
    String getTgaTpTrch();

    void setTgaTpTrch(String tgaTpTrch);

    /**
     * Host Variable TGA-DUR-AA
     * 
     */
    int getTgaDurAa();

    void setTgaDurAa(int tgaDurAa);

    /**
     * Nullable property for TGA-DUR-AA
     * 
     */
    Integer getTgaDurAaObj();

    void setTgaDurAaObj(Integer tgaDurAaObj);

    /**
     * Host Variable TGA-DUR-MM
     * 
     */
    int getTgaDurMm();

    void setTgaDurMm(int tgaDurMm);

    /**
     * Nullable property for TGA-DUR-MM
     * 
     */
    Integer getTgaDurMmObj();

    void setTgaDurMmObj(Integer tgaDurMmObj);

    /**
     * Host Variable TGA-DUR-GG
     * 
     */
    int getTgaDurGg();

    void setTgaDurGg(int tgaDurGg);

    /**
     * Nullable property for TGA-DUR-GG
     * 
     */
    Integer getTgaDurGgObj();

    void setTgaDurGgObj(Integer tgaDurGgObj);

    /**
     * Host Variable TGA-PRE-CASO-MOR
     * 
     */
    AfDecimal getTgaPreCasoMor();

    void setTgaPreCasoMor(AfDecimal tgaPreCasoMor);

    /**
     * Nullable property for TGA-PRE-CASO-MOR
     * 
     */
    AfDecimal getTgaPreCasoMorObj();

    void setTgaPreCasoMorObj(AfDecimal tgaPreCasoMorObj);

    /**
     * Host Variable TGA-PC-INTR-RIAT
     * 
     */
    AfDecimal getTgaPcIntrRiat();

    void setTgaPcIntrRiat(AfDecimal tgaPcIntrRiat);

    /**
     * Nullable property for TGA-PC-INTR-RIAT
     * 
     */
    AfDecimal getTgaPcIntrRiatObj();

    void setTgaPcIntrRiatObj(AfDecimal tgaPcIntrRiatObj);

    /**
     * Host Variable TGA-IMP-BNS-ANTIC
     * 
     */
    AfDecimal getTgaImpBnsAntic();

    void setTgaImpBnsAntic(AfDecimal tgaImpBnsAntic);

    /**
     * Nullable property for TGA-IMP-BNS-ANTIC
     * 
     */
    AfDecimal getTgaImpBnsAnticObj();

    void setTgaImpBnsAnticObj(AfDecimal tgaImpBnsAnticObj);

    /**
     * Host Variable TGA-PRE-INI-NET
     * 
     */
    AfDecimal getTgaPreIniNet();

    void setTgaPreIniNet(AfDecimal tgaPreIniNet);

    /**
     * Nullable property for TGA-PRE-INI-NET
     * 
     */
    AfDecimal getTgaPreIniNetObj();

    void setTgaPreIniNetObj(AfDecimal tgaPreIniNetObj);

    /**
     * Host Variable TGA-PRE-PP-INI
     * 
     */
    AfDecimal getTgaPrePpIni();

    void setTgaPrePpIni(AfDecimal tgaPrePpIni);

    /**
     * Nullable property for TGA-PRE-PP-INI
     * 
     */
    AfDecimal getTgaPrePpIniObj();

    void setTgaPrePpIniObj(AfDecimal tgaPrePpIniObj);

    /**
     * Host Variable TGA-PRE-PP-ULT
     * 
     */
    AfDecimal getTgaPrePpUlt();

    void setTgaPrePpUlt(AfDecimal tgaPrePpUlt);

    /**
     * Nullable property for TGA-PRE-PP-ULT
     * 
     */
    AfDecimal getTgaPrePpUltObj();

    void setTgaPrePpUltObj(AfDecimal tgaPrePpUltObj);

    /**
     * Host Variable TGA-PRE-TARI-INI
     * 
     */
    AfDecimal getTgaPreTariIni();

    void setTgaPreTariIni(AfDecimal tgaPreTariIni);

    /**
     * Nullable property for TGA-PRE-TARI-INI
     * 
     */
    AfDecimal getTgaPreTariIniObj();

    void setTgaPreTariIniObj(AfDecimal tgaPreTariIniObj);

    /**
     * Host Variable TGA-PRE-TARI-ULT
     * 
     */
    AfDecimal getTgaPreTariUlt();

    void setTgaPreTariUlt(AfDecimal tgaPreTariUlt);

    /**
     * Nullable property for TGA-PRE-TARI-ULT
     * 
     */
    AfDecimal getTgaPreTariUltObj();

    void setTgaPreTariUltObj(AfDecimal tgaPreTariUltObj);

    /**
     * Host Variable TGA-PRE-INVRIO-INI
     * 
     */
    AfDecimal getTgaPreInvrioIni();

    void setTgaPreInvrioIni(AfDecimal tgaPreInvrioIni);

    /**
     * Nullable property for TGA-PRE-INVRIO-INI
     * 
     */
    AfDecimal getTgaPreInvrioIniObj();

    void setTgaPreInvrioIniObj(AfDecimal tgaPreInvrioIniObj);

    /**
     * Host Variable TGA-PRE-INVRIO-ULT
     * 
     */
    AfDecimal getTgaPreInvrioUlt();

    void setTgaPreInvrioUlt(AfDecimal tgaPreInvrioUlt);

    /**
     * Nullable property for TGA-PRE-INVRIO-ULT
     * 
     */
    AfDecimal getTgaPreInvrioUltObj();

    void setTgaPreInvrioUltObj(AfDecimal tgaPreInvrioUltObj);

    /**
     * Host Variable TGA-PRE-RIVTO
     * 
     */
    AfDecimal getTgaPreRivto();

    void setTgaPreRivto(AfDecimal tgaPreRivto);

    /**
     * Nullable property for TGA-PRE-RIVTO
     * 
     */
    AfDecimal getTgaPreRivtoObj();

    void setTgaPreRivtoObj(AfDecimal tgaPreRivtoObj);

    /**
     * Host Variable TGA-IMP-SOPR-PROF
     * 
     */
    AfDecimal getTgaImpSoprProf();

    void setTgaImpSoprProf(AfDecimal tgaImpSoprProf);

    /**
     * Nullable property for TGA-IMP-SOPR-PROF
     * 
     */
    AfDecimal getTgaImpSoprProfObj();

    void setTgaImpSoprProfObj(AfDecimal tgaImpSoprProfObj);

    /**
     * Host Variable TGA-IMP-SOPR-SAN
     * 
     */
    AfDecimal getTgaImpSoprSan();

    void setTgaImpSoprSan(AfDecimal tgaImpSoprSan);

    /**
     * Nullable property for TGA-IMP-SOPR-SAN
     * 
     */
    AfDecimal getTgaImpSoprSanObj();

    void setTgaImpSoprSanObj(AfDecimal tgaImpSoprSanObj);

    /**
     * Host Variable TGA-IMP-SOPR-SPO
     * 
     */
    AfDecimal getTgaImpSoprSpo();

    void setTgaImpSoprSpo(AfDecimal tgaImpSoprSpo);

    /**
     * Nullable property for TGA-IMP-SOPR-SPO
     * 
     */
    AfDecimal getTgaImpSoprSpoObj();

    void setTgaImpSoprSpoObj(AfDecimal tgaImpSoprSpoObj);

    /**
     * Host Variable TGA-IMP-SOPR-TEC
     * 
     */
    AfDecimal getTgaImpSoprTec();

    void setTgaImpSoprTec(AfDecimal tgaImpSoprTec);

    /**
     * Nullable property for TGA-IMP-SOPR-TEC
     * 
     */
    AfDecimal getTgaImpSoprTecObj();

    void setTgaImpSoprTecObj(AfDecimal tgaImpSoprTecObj);

    /**
     * Host Variable TGA-IMP-ALT-SOPR
     * 
     */
    AfDecimal getTgaImpAltSopr();

    void setTgaImpAltSopr(AfDecimal tgaImpAltSopr);

    /**
     * Nullable property for TGA-IMP-ALT-SOPR
     * 
     */
    AfDecimal getTgaImpAltSoprObj();

    void setTgaImpAltSoprObj(AfDecimal tgaImpAltSoprObj);

    /**
     * Host Variable TGA-PRE-STAB
     * 
     */
    AfDecimal getTgaPreStab();

    void setTgaPreStab(AfDecimal tgaPreStab);

    /**
     * Nullable property for TGA-PRE-STAB
     * 
     */
    AfDecimal getTgaPreStabObj();

    void setTgaPreStabObj(AfDecimal tgaPreStabObj);

    /**
     * Host Variable TGA-DT-EFF-STAB-DB
     * 
     */
    String getTgaDtEffStabDb();

    void setTgaDtEffStabDb(String tgaDtEffStabDb);

    /**
     * Nullable property for TGA-DT-EFF-STAB-DB
     * 
     */
    String getTgaDtEffStabDbObj();

    void setTgaDtEffStabDbObj(String tgaDtEffStabDbObj);

    /**
     * Host Variable TGA-TS-RIVAL-FIS
     * 
     */
    AfDecimal getTgaTsRivalFis();

    void setTgaTsRivalFis(AfDecimal tgaTsRivalFis);

    /**
     * Nullable property for TGA-TS-RIVAL-FIS
     * 
     */
    AfDecimal getTgaTsRivalFisObj();

    void setTgaTsRivalFisObj(AfDecimal tgaTsRivalFisObj);

    /**
     * Host Variable TGA-TS-RIVAL-INDICIZ
     * 
     */
    AfDecimal getTgaTsRivalIndiciz();

    void setTgaTsRivalIndiciz(AfDecimal tgaTsRivalIndiciz);

    /**
     * Nullable property for TGA-TS-RIVAL-INDICIZ
     * 
     */
    AfDecimal getTgaTsRivalIndicizObj();

    void setTgaTsRivalIndicizObj(AfDecimal tgaTsRivalIndicizObj);

    /**
     * Host Variable TGA-OLD-TS-TEC
     * 
     */
    AfDecimal getTgaOldTsTec();

    void setTgaOldTsTec(AfDecimal tgaOldTsTec);

    /**
     * Nullable property for TGA-OLD-TS-TEC
     * 
     */
    AfDecimal getTgaOldTsTecObj();

    void setTgaOldTsTecObj(AfDecimal tgaOldTsTecObj);

    /**
     * Host Variable TGA-RAT-LRD
     * 
     */
    AfDecimal getTgaRatLrd();

    void setTgaRatLrd(AfDecimal tgaRatLrd);

    /**
     * Nullable property for TGA-RAT-LRD
     * 
     */
    AfDecimal getTgaRatLrdObj();

    void setTgaRatLrdObj(AfDecimal tgaRatLrdObj);

    /**
     * Host Variable TGA-PRE-LRD
     * 
     */
    AfDecimal getTgaPreLrd();

    void setTgaPreLrd(AfDecimal tgaPreLrd);

    /**
     * Nullable property for TGA-PRE-LRD
     * 
     */
    AfDecimal getTgaPreLrdObj();

    void setTgaPreLrdObj(AfDecimal tgaPreLrdObj);

    /**
     * Host Variable TGA-PRSTZ-INI
     * 
     */
    AfDecimal getTgaPrstzIni();

    void setTgaPrstzIni(AfDecimal tgaPrstzIni);

    /**
     * Nullable property for TGA-PRSTZ-INI
     * 
     */
    AfDecimal getTgaPrstzIniObj();

    void setTgaPrstzIniObj(AfDecimal tgaPrstzIniObj);

    /**
     * Host Variable TGA-PRSTZ-ULT
     * 
     */
    AfDecimal getTgaPrstzUlt();

    void setTgaPrstzUlt(AfDecimal tgaPrstzUlt);

    /**
     * Nullable property for TGA-PRSTZ-ULT
     * 
     */
    AfDecimal getTgaPrstzUltObj();

    void setTgaPrstzUltObj(AfDecimal tgaPrstzUltObj);

    /**
     * Host Variable TGA-CPT-IN-OPZ-RIVTO
     * 
     */
    AfDecimal getTgaCptInOpzRivto();

    void setTgaCptInOpzRivto(AfDecimal tgaCptInOpzRivto);

    /**
     * Nullable property for TGA-CPT-IN-OPZ-RIVTO
     * 
     */
    AfDecimal getTgaCptInOpzRivtoObj();

    void setTgaCptInOpzRivtoObj(AfDecimal tgaCptInOpzRivtoObj);

    /**
     * Host Variable TGA-PRSTZ-INI-STAB
     * 
     */
    AfDecimal getTgaPrstzIniStab();

    void setTgaPrstzIniStab(AfDecimal tgaPrstzIniStab);

    /**
     * Nullable property for TGA-PRSTZ-INI-STAB
     * 
     */
    AfDecimal getTgaPrstzIniStabObj();

    void setTgaPrstzIniStabObj(AfDecimal tgaPrstzIniStabObj);

    /**
     * Host Variable TGA-CPT-RSH-MOR
     * 
     */
    AfDecimal getTgaCptRshMor();

    void setTgaCptRshMor(AfDecimal tgaCptRshMor);

    /**
     * Nullable property for TGA-CPT-RSH-MOR
     * 
     */
    AfDecimal getTgaCptRshMorObj();

    void setTgaCptRshMorObj(AfDecimal tgaCptRshMorObj);

    /**
     * Host Variable TGA-PRSTZ-RID-INI
     * 
     */
    AfDecimal getTgaPrstzRidIni();

    void setTgaPrstzRidIni(AfDecimal tgaPrstzRidIni);

    /**
     * Nullable property for TGA-PRSTZ-RID-INI
     * 
     */
    AfDecimal getTgaPrstzRidIniObj();

    void setTgaPrstzRidIniObj(AfDecimal tgaPrstzRidIniObj);

    /**
     * Host Variable TGA-FL-CAR-CONT
     * 
     */
    char getTgaFlCarCont();

    void setTgaFlCarCont(char tgaFlCarCont);

    /**
     * Nullable property for TGA-FL-CAR-CONT
     * 
     */
    Character getTgaFlCarContObj();

    void setTgaFlCarContObj(Character tgaFlCarContObj);

    /**
     * Host Variable TGA-BNS-GIA-LIQTO
     * 
     */
    AfDecimal getTgaBnsGiaLiqto();

    void setTgaBnsGiaLiqto(AfDecimal tgaBnsGiaLiqto);

    /**
     * Nullable property for TGA-BNS-GIA-LIQTO
     * 
     */
    AfDecimal getTgaBnsGiaLiqtoObj();

    void setTgaBnsGiaLiqtoObj(AfDecimal tgaBnsGiaLiqtoObj);

    /**
     * Host Variable TGA-IMP-BNS
     * 
     */
    AfDecimal getTgaImpBns();

    void setTgaImpBns(AfDecimal tgaImpBns);

    /**
     * Nullable property for TGA-IMP-BNS
     * 
     */
    AfDecimal getTgaImpBnsObj();

    void setTgaImpBnsObj(AfDecimal tgaImpBnsObj);

    /**
     * Host Variable TGA-COD-DVS
     * 
     */
    String getTgaCodDvs();

    void setTgaCodDvs(String tgaCodDvs);

    /**
     * Host Variable TGA-PRSTZ-INI-NEWFIS
     * 
     */
    AfDecimal getTgaPrstzIniNewfis();

    void setTgaPrstzIniNewfis(AfDecimal tgaPrstzIniNewfis);

    /**
     * Nullable property for TGA-PRSTZ-INI-NEWFIS
     * 
     */
    AfDecimal getTgaPrstzIniNewfisObj();

    void setTgaPrstzIniNewfisObj(AfDecimal tgaPrstzIniNewfisObj);

    /**
     * Host Variable TGA-IMP-SCON
     * 
     */
    AfDecimal getTgaImpScon();

    void setTgaImpScon(AfDecimal tgaImpScon);

    /**
     * Nullable property for TGA-IMP-SCON
     * 
     */
    AfDecimal getTgaImpSconObj();

    void setTgaImpSconObj(AfDecimal tgaImpSconObj);

    /**
     * Host Variable TGA-ALQ-SCON
     * 
     */
    AfDecimal getTgaAlqScon();

    void setTgaAlqScon(AfDecimal tgaAlqScon);

    /**
     * Nullable property for TGA-ALQ-SCON
     * 
     */
    AfDecimal getTgaAlqSconObj();

    void setTgaAlqSconObj(AfDecimal tgaAlqSconObj);

    /**
     * Host Variable TGA-IMP-CAR-ACQ
     * 
     */
    AfDecimal getTgaImpCarAcq();

    void setTgaImpCarAcq(AfDecimal tgaImpCarAcq);

    /**
     * Nullable property for TGA-IMP-CAR-ACQ
     * 
     */
    AfDecimal getTgaImpCarAcqObj();

    void setTgaImpCarAcqObj(AfDecimal tgaImpCarAcqObj);

    /**
     * Host Variable TGA-IMP-CAR-INC
     * 
     */
    AfDecimal getTgaImpCarInc();

    void setTgaImpCarInc(AfDecimal tgaImpCarInc);

    /**
     * Nullable property for TGA-IMP-CAR-INC
     * 
     */
    AfDecimal getTgaImpCarIncObj();

    void setTgaImpCarIncObj(AfDecimal tgaImpCarIncObj);

    /**
     * Host Variable TGA-IMP-CAR-GEST
     * 
     */
    AfDecimal getTgaImpCarGest();

    void setTgaImpCarGest(AfDecimal tgaImpCarGest);

    /**
     * Nullable property for TGA-IMP-CAR-GEST
     * 
     */
    AfDecimal getTgaImpCarGestObj();

    void setTgaImpCarGestObj(AfDecimal tgaImpCarGestObj);

    /**
     * Host Variable TGA-ETA-AA-1O-ASSTO
     * 
     */
    short getTgaEtaAa1oAssto();

    void setTgaEtaAa1oAssto(short tgaEtaAa1oAssto);

    /**
     * Nullable property for TGA-ETA-AA-1O-ASSTO
     * 
     */
    Short getTgaEtaAa1oAsstoObj();

    void setTgaEtaAa1oAsstoObj(Short tgaEtaAa1oAsstoObj);

    /**
     * Host Variable TGA-ETA-MM-1O-ASSTO
     * 
     */
    short getTgaEtaMm1oAssto();

    void setTgaEtaMm1oAssto(short tgaEtaMm1oAssto);

    /**
     * Nullable property for TGA-ETA-MM-1O-ASSTO
     * 
     */
    Short getTgaEtaMm1oAsstoObj();

    void setTgaEtaMm1oAsstoObj(Short tgaEtaMm1oAsstoObj);

    /**
     * Host Variable TGA-ETA-AA-2O-ASSTO
     * 
     */
    short getTgaEtaAa2oAssto();

    void setTgaEtaAa2oAssto(short tgaEtaAa2oAssto);

    /**
     * Nullable property for TGA-ETA-AA-2O-ASSTO
     * 
     */
    Short getTgaEtaAa2oAsstoObj();

    void setTgaEtaAa2oAsstoObj(Short tgaEtaAa2oAsstoObj);

    /**
     * Host Variable TGA-ETA-MM-2O-ASSTO
     * 
     */
    short getTgaEtaMm2oAssto();

    void setTgaEtaMm2oAssto(short tgaEtaMm2oAssto);

    /**
     * Nullable property for TGA-ETA-MM-2O-ASSTO
     * 
     */
    Short getTgaEtaMm2oAsstoObj();

    void setTgaEtaMm2oAsstoObj(Short tgaEtaMm2oAsstoObj);

    /**
     * Host Variable TGA-ETA-AA-3O-ASSTO
     * 
     */
    short getTgaEtaAa3oAssto();

    void setTgaEtaAa3oAssto(short tgaEtaAa3oAssto);

    /**
     * Nullable property for TGA-ETA-AA-3O-ASSTO
     * 
     */
    Short getTgaEtaAa3oAsstoObj();

    void setTgaEtaAa3oAsstoObj(Short tgaEtaAa3oAsstoObj);

    /**
     * Host Variable TGA-ETA-MM-3O-ASSTO
     * 
     */
    short getTgaEtaMm3oAssto();

    void setTgaEtaMm3oAssto(short tgaEtaMm3oAssto);

    /**
     * Nullable property for TGA-ETA-MM-3O-ASSTO
     * 
     */
    Short getTgaEtaMm3oAsstoObj();

    void setTgaEtaMm3oAsstoObj(Short tgaEtaMm3oAsstoObj);

    /**
     * Host Variable TGA-RENDTO-LRD
     * 
     */
    AfDecimal getTgaRendtoLrd();

    void setTgaRendtoLrd(AfDecimal tgaRendtoLrd);

    /**
     * Nullable property for TGA-RENDTO-LRD
     * 
     */
    AfDecimal getTgaRendtoLrdObj();

    void setTgaRendtoLrdObj(AfDecimal tgaRendtoLrdObj);

    /**
     * Host Variable TGA-PC-RETR
     * 
     */
    AfDecimal getTgaPcRetr();

    void setTgaPcRetr(AfDecimal tgaPcRetr);

    /**
     * Nullable property for TGA-PC-RETR
     * 
     */
    AfDecimal getTgaPcRetrObj();

    void setTgaPcRetrObj(AfDecimal tgaPcRetrObj);

    /**
     * Host Variable TGA-RENDTO-RETR
     * 
     */
    AfDecimal getTgaRendtoRetr();

    void setTgaRendtoRetr(AfDecimal tgaRendtoRetr);

    /**
     * Nullable property for TGA-RENDTO-RETR
     * 
     */
    AfDecimal getTgaRendtoRetrObj();

    void setTgaRendtoRetrObj(AfDecimal tgaRendtoRetrObj);

    /**
     * Host Variable TGA-MIN-GARTO
     * 
     */
    AfDecimal getTgaMinGarto();

    void setTgaMinGarto(AfDecimal tgaMinGarto);

    /**
     * Nullable property for TGA-MIN-GARTO
     * 
     */
    AfDecimal getTgaMinGartoObj();

    void setTgaMinGartoObj(AfDecimal tgaMinGartoObj);

    /**
     * Host Variable TGA-MIN-TRNUT
     * 
     */
    AfDecimal getTgaMinTrnut();

    void setTgaMinTrnut(AfDecimal tgaMinTrnut);

    /**
     * Nullable property for TGA-MIN-TRNUT
     * 
     */
    AfDecimal getTgaMinTrnutObj();

    void setTgaMinTrnutObj(AfDecimal tgaMinTrnutObj);

    /**
     * Host Variable TGA-PRE-ATT-DI-TRCH
     * 
     */
    AfDecimal getTgaPreAttDiTrch();

    void setTgaPreAttDiTrch(AfDecimal tgaPreAttDiTrch);

    /**
     * Nullable property for TGA-PRE-ATT-DI-TRCH
     * 
     */
    AfDecimal getTgaPreAttDiTrchObj();

    void setTgaPreAttDiTrchObj(AfDecimal tgaPreAttDiTrchObj);

    /**
     * Host Variable TGA-MATU-END2000
     * 
     */
    AfDecimal getTgaMatuEnd2000();

    void setTgaMatuEnd2000(AfDecimal tgaMatuEnd2000);

    /**
     * Nullable property for TGA-MATU-END2000
     * 
     */
    AfDecimal getTgaMatuEnd2000Obj();

    void setTgaMatuEnd2000Obj(AfDecimal tgaMatuEnd2000Obj);

    /**
     * Host Variable TGA-ABB-TOT-INI
     * 
     */
    AfDecimal getTgaAbbTotIni();

    void setTgaAbbTotIni(AfDecimal tgaAbbTotIni);

    /**
     * Nullable property for TGA-ABB-TOT-INI
     * 
     */
    AfDecimal getTgaAbbTotIniObj();

    void setTgaAbbTotIniObj(AfDecimal tgaAbbTotIniObj);

    /**
     * Host Variable TGA-ABB-TOT-ULT
     * 
     */
    AfDecimal getTgaAbbTotUlt();

    void setTgaAbbTotUlt(AfDecimal tgaAbbTotUlt);

    /**
     * Nullable property for TGA-ABB-TOT-ULT
     * 
     */
    AfDecimal getTgaAbbTotUltObj();

    void setTgaAbbTotUltObj(AfDecimal tgaAbbTotUltObj);

    /**
     * Host Variable TGA-ABB-ANNU-ULT
     * 
     */
    AfDecimal getTgaAbbAnnuUlt();

    void setTgaAbbAnnuUlt(AfDecimal tgaAbbAnnuUlt);

    /**
     * Nullable property for TGA-ABB-ANNU-ULT
     * 
     */
    AfDecimal getTgaAbbAnnuUltObj();

    void setTgaAbbAnnuUltObj(AfDecimal tgaAbbAnnuUltObj);

    /**
     * Host Variable TGA-DUR-ABB
     * 
     */
    int getTgaDurAbb();

    void setTgaDurAbb(int tgaDurAbb);

    /**
     * Nullable property for TGA-DUR-ABB
     * 
     */
    Integer getTgaDurAbbObj();

    void setTgaDurAbbObj(Integer tgaDurAbbObj);

    /**
     * Host Variable TGA-TP-ADEG-ABB
     * 
     */
    char getTgaTpAdegAbb();

    void setTgaTpAdegAbb(char tgaTpAdegAbb);

    /**
     * Nullable property for TGA-TP-ADEG-ABB
     * 
     */
    Character getTgaTpAdegAbbObj();

    void setTgaTpAdegAbbObj(Character tgaTpAdegAbbObj);

    /**
     * Host Variable TGA-MOD-CALC
     * 
     */
    String getTgaModCalc();

    void setTgaModCalc(String tgaModCalc);

    /**
     * Nullable property for TGA-MOD-CALC
     * 
     */
    String getTgaModCalcObj();

    void setTgaModCalcObj(String tgaModCalcObj);

    /**
     * Host Variable TGA-IMP-AZ
     * 
     */
    AfDecimal getTgaImpAz();

    void setTgaImpAz(AfDecimal tgaImpAz);

    /**
     * Nullable property for TGA-IMP-AZ
     * 
     */
    AfDecimal getTgaImpAzObj();

    void setTgaImpAzObj(AfDecimal tgaImpAzObj);

    /**
     * Host Variable TGA-IMP-ADER
     * 
     */
    AfDecimal getTgaImpAder();

    void setTgaImpAder(AfDecimal tgaImpAder);

    /**
     * Nullable property for TGA-IMP-ADER
     * 
     */
    AfDecimal getTgaImpAderObj();

    void setTgaImpAderObj(AfDecimal tgaImpAderObj);

    /**
     * Host Variable TGA-IMP-TFR
     * 
     */
    AfDecimal getTgaImpTfr();

    void setTgaImpTfr(AfDecimal tgaImpTfr);

    /**
     * Nullable property for TGA-IMP-TFR
     * 
     */
    AfDecimal getTgaImpTfrObj();

    void setTgaImpTfrObj(AfDecimal tgaImpTfrObj);

    /**
     * Host Variable TGA-IMP-VOLO
     * 
     */
    AfDecimal getTgaImpVolo();

    void setTgaImpVolo(AfDecimal tgaImpVolo);

    /**
     * Nullable property for TGA-IMP-VOLO
     * 
     */
    AfDecimal getTgaImpVoloObj();

    void setTgaImpVoloObj(AfDecimal tgaImpVoloObj);

    /**
     * Host Variable TGA-VIS-END2000
     * 
     */
    AfDecimal getTgaVisEnd2000();

    void setTgaVisEnd2000(AfDecimal tgaVisEnd2000);

    /**
     * Nullable property for TGA-VIS-END2000
     * 
     */
    AfDecimal getTgaVisEnd2000Obj();

    void setTgaVisEnd2000Obj(AfDecimal tgaVisEnd2000Obj);

    /**
     * Host Variable TGA-DT-VLDT-PROD-DB
     * 
     */
    String getTgaDtVldtProdDb();

    void setTgaDtVldtProdDb(String tgaDtVldtProdDb);

    /**
     * Nullable property for TGA-DT-VLDT-PROD-DB
     * 
     */
    String getTgaDtVldtProdDbObj();

    void setTgaDtVldtProdDbObj(String tgaDtVldtProdDbObj);

    /**
     * Host Variable TGA-DT-INI-VAL-TAR-DB
     * 
     */
    String getTgaDtIniValTarDb();

    void setTgaDtIniValTarDb(String tgaDtIniValTarDb);

    /**
     * Nullable property for TGA-DT-INI-VAL-TAR-DB
     * 
     */
    String getTgaDtIniValTarDbObj();

    void setTgaDtIniValTarDbObj(String tgaDtIniValTarDbObj);

    /**
     * Host Variable TGA-IMPB-VIS-END2000
     * 
     */
    AfDecimal getTgaImpbVisEnd2000();

    void setTgaImpbVisEnd2000(AfDecimal tgaImpbVisEnd2000);

    /**
     * Nullable property for TGA-IMPB-VIS-END2000
     * 
     */
    AfDecimal getTgaImpbVisEnd2000Obj();

    void setTgaImpbVisEnd2000Obj(AfDecimal tgaImpbVisEnd2000Obj);

    /**
     * Host Variable TGA-REN-INI-TS-TEC-0
     * 
     */
    AfDecimal getTgaRenIniTsTec0();

    void setTgaRenIniTsTec0(AfDecimal tgaRenIniTsTec0);

    /**
     * Nullable property for TGA-REN-INI-TS-TEC-0
     * 
     */
    AfDecimal getTgaRenIniTsTec0Obj();

    void setTgaRenIniTsTec0Obj(AfDecimal tgaRenIniTsTec0Obj);

    /**
     * Host Variable TGA-PC-RIP-PRE
     * 
     */
    AfDecimal getTgaPcRipPre();

    void setTgaPcRipPre(AfDecimal tgaPcRipPre);

    /**
     * Nullable property for TGA-PC-RIP-PRE
     * 
     */
    AfDecimal getTgaPcRipPreObj();

    void setTgaPcRipPreObj(AfDecimal tgaPcRipPreObj);

    /**
     * Host Variable TGA-FL-IMPORTI-FORZ
     * 
     */
    char getTgaFlImportiForz();

    void setTgaFlImportiForz(char tgaFlImportiForz);

    /**
     * Nullable property for TGA-FL-IMPORTI-FORZ
     * 
     */
    Character getTgaFlImportiForzObj();

    void setTgaFlImportiForzObj(Character tgaFlImportiForzObj);

    /**
     * Host Variable TGA-PRSTZ-INI-NFORZ
     * 
     */
    AfDecimal getTgaPrstzIniNforz();

    void setTgaPrstzIniNforz(AfDecimal tgaPrstzIniNforz);

    /**
     * Nullable property for TGA-PRSTZ-INI-NFORZ
     * 
     */
    AfDecimal getTgaPrstzIniNforzObj();

    void setTgaPrstzIniNforzObj(AfDecimal tgaPrstzIniNforzObj);

    /**
     * Host Variable TGA-VIS-END2000-NFORZ
     * 
     */
    AfDecimal getTgaVisEnd2000Nforz();

    void setTgaVisEnd2000Nforz(AfDecimal tgaVisEnd2000Nforz);

    /**
     * Nullable property for TGA-VIS-END2000-NFORZ
     * 
     */
    AfDecimal getTgaVisEnd2000NforzObj();

    void setTgaVisEnd2000NforzObj(AfDecimal tgaVisEnd2000NforzObj);

    /**
     * Host Variable TGA-INTR-MORA
     * 
     */
    AfDecimal getTgaIntrMora();

    void setTgaIntrMora(AfDecimal tgaIntrMora);

    /**
     * Nullable property for TGA-INTR-MORA
     * 
     */
    AfDecimal getTgaIntrMoraObj();

    void setTgaIntrMoraObj(AfDecimal tgaIntrMoraObj);

    /**
     * Host Variable TGA-MANFEE-ANTIC
     * 
     */
    AfDecimal getTgaManfeeAntic();

    void setTgaManfeeAntic(AfDecimal tgaManfeeAntic);

    /**
     * Nullable property for TGA-MANFEE-ANTIC
     * 
     */
    AfDecimal getTgaManfeeAnticObj();

    void setTgaManfeeAnticObj(AfDecimal tgaManfeeAnticObj);

    /**
     * Host Variable TGA-MANFEE-RICOR
     * 
     */
    AfDecimal getTgaManfeeRicor();

    void setTgaManfeeRicor(AfDecimal tgaManfeeRicor);

    /**
     * Nullable property for TGA-MANFEE-RICOR
     * 
     */
    AfDecimal getTgaManfeeRicorObj();

    void setTgaManfeeRicorObj(AfDecimal tgaManfeeRicorObj);

    /**
     * Host Variable TGA-PRE-UNI-RIVTO
     * 
     */
    AfDecimal getTgaPreUniRivto();

    void setTgaPreUniRivto(AfDecimal tgaPreUniRivto);

    /**
     * Nullable property for TGA-PRE-UNI-RIVTO
     * 
     */
    AfDecimal getTgaPreUniRivtoObj();

    void setTgaPreUniRivtoObj(AfDecimal tgaPreUniRivtoObj);

    /**
     * Host Variable TGA-PROV-1AA-ACQ
     * 
     */
    AfDecimal getTgaProv1aaAcq();

    void setTgaProv1aaAcq(AfDecimal tgaProv1aaAcq);

    /**
     * Nullable property for TGA-PROV-1AA-ACQ
     * 
     */
    AfDecimal getTgaProv1aaAcqObj();

    void setTgaProv1aaAcqObj(AfDecimal tgaProv1aaAcqObj);

    /**
     * Host Variable TGA-PROV-2AA-ACQ
     * 
     */
    AfDecimal getTgaProv2aaAcq();

    void setTgaProv2aaAcq(AfDecimal tgaProv2aaAcq);

    /**
     * Nullable property for TGA-PROV-2AA-ACQ
     * 
     */
    AfDecimal getTgaProv2aaAcqObj();

    void setTgaProv2aaAcqObj(AfDecimal tgaProv2aaAcqObj);

    /**
     * Host Variable TGA-PROV-RICOR
     * 
     */
    AfDecimal getTgaProvRicor();

    void setTgaProvRicor(AfDecimal tgaProvRicor);

    /**
     * Nullable property for TGA-PROV-RICOR
     * 
     */
    AfDecimal getTgaProvRicorObj();

    void setTgaProvRicorObj(AfDecimal tgaProvRicorObj);

    /**
     * Host Variable TGA-PROV-INC
     * 
     */
    AfDecimal getTgaProvInc();

    void setTgaProvInc(AfDecimal tgaProvInc);

    /**
     * Nullable property for TGA-PROV-INC
     * 
     */
    AfDecimal getTgaProvIncObj();

    void setTgaProvIncObj(AfDecimal tgaProvIncObj);

    /**
     * Host Variable TGA-ALQ-PROV-ACQ
     * 
     */
    AfDecimal getTgaAlqProvAcq();

    void setTgaAlqProvAcq(AfDecimal tgaAlqProvAcq);

    /**
     * Nullable property for TGA-ALQ-PROV-ACQ
     * 
     */
    AfDecimal getTgaAlqProvAcqObj();

    void setTgaAlqProvAcqObj(AfDecimal tgaAlqProvAcqObj);

    /**
     * Host Variable TGA-ALQ-PROV-INC
     * 
     */
    AfDecimal getTgaAlqProvInc();

    void setTgaAlqProvInc(AfDecimal tgaAlqProvInc);

    /**
     * Nullable property for TGA-ALQ-PROV-INC
     * 
     */
    AfDecimal getTgaAlqProvIncObj();

    void setTgaAlqProvIncObj(AfDecimal tgaAlqProvIncObj);

    /**
     * Host Variable TGA-ALQ-PROV-RICOR
     * 
     */
    AfDecimal getTgaAlqProvRicor();

    void setTgaAlqProvRicor(AfDecimal tgaAlqProvRicor);

    /**
     * Nullable property for TGA-ALQ-PROV-RICOR
     * 
     */
    AfDecimal getTgaAlqProvRicorObj();

    void setTgaAlqProvRicorObj(AfDecimal tgaAlqProvRicorObj);

    /**
     * Host Variable TGA-IMPB-PROV-ACQ
     * 
     */
    AfDecimal getTgaImpbProvAcq();

    void setTgaImpbProvAcq(AfDecimal tgaImpbProvAcq);

    /**
     * Nullable property for TGA-IMPB-PROV-ACQ
     * 
     */
    AfDecimal getTgaImpbProvAcqObj();

    void setTgaImpbProvAcqObj(AfDecimal tgaImpbProvAcqObj);

    /**
     * Host Variable TGA-IMPB-PROV-INC
     * 
     */
    AfDecimal getTgaImpbProvInc();

    void setTgaImpbProvInc(AfDecimal tgaImpbProvInc);

    /**
     * Nullable property for TGA-IMPB-PROV-INC
     * 
     */
    AfDecimal getTgaImpbProvIncObj();

    void setTgaImpbProvIncObj(AfDecimal tgaImpbProvIncObj);

    /**
     * Host Variable TGA-IMPB-PROV-RICOR
     * 
     */
    AfDecimal getTgaImpbProvRicor();

    void setTgaImpbProvRicor(AfDecimal tgaImpbProvRicor);

    /**
     * Nullable property for TGA-IMPB-PROV-RICOR
     * 
     */
    AfDecimal getTgaImpbProvRicorObj();

    void setTgaImpbProvRicorObj(AfDecimal tgaImpbProvRicorObj);

    /**
     * Host Variable TGA-FL-PROV-FORZ
     * 
     */
    char getTgaFlProvForz();

    void setTgaFlProvForz(char tgaFlProvForz);

    /**
     * Nullable property for TGA-FL-PROV-FORZ
     * 
     */
    Character getTgaFlProvForzObj();

    void setTgaFlProvForzObj(Character tgaFlProvForzObj);

    /**
     * Host Variable TGA-PRSTZ-AGG-INI
     * 
     */
    AfDecimal getTgaPrstzAggIni();

    void setTgaPrstzAggIni(AfDecimal tgaPrstzAggIni);

    /**
     * Nullable property for TGA-PRSTZ-AGG-INI
     * 
     */
    AfDecimal getTgaPrstzAggIniObj();

    void setTgaPrstzAggIniObj(AfDecimal tgaPrstzAggIniObj);

    /**
     * Host Variable TGA-INCR-PRE
     * 
     */
    AfDecimal getTgaIncrPre();

    void setTgaIncrPre(AfDecimal tgaIncrPre);

    /**
     * Nullable property for TGA-INCR-PRE
     * 
     */
    AfDecimal getTgaIncrPreObj();

    void setTgaIncrPreObj(AfDecimal tgaIncrPreObj);

    /**
     * Host Variable TGA-INCR-PRSTZ
     * 
     */
    AfDecimal getTgaIncrPrstz();

    void setTgaIncrPrstz(AfDecimal tgaIncrPrstz);

    /**
     * Nullable property for TGA-INCR-PRSTZ
     * 
     */
    AfDecimal getTgaIncrPrstzObj();

    void setTgaIncrPrstzObj(AfDecimal tgaIncrPrstzObj);

    /**
     * Host Variable TGA-DT-ULT-ADEG-PRE-PR-DB
     * 
     */
    String getTgaDtUltAdegPrePrDb();

    void setTgaDtUltAdegPrePrDb(String tgaDtUltAdegPrePrDb);

    /**
     * Nullable property for TGA-DT-ULT-ADEG-PRE-PR-DB
     * 
     */
    String getTgaDtUltAdegPrePrDbObj();

    void setTgaDtUltAdegPrePrDbObj(String tgaDtUltAdegPrePrDbObj);

    /**
     * Host Variable TGA-PRSTZ-AGG-ULT
     * 
     */
    AfDecimal getTgaPrstzAggUlt();

    void setTgaPrstzAggUlt(AfDecimal tgaPrstzAggUlt);

    /**
     * Nullable property for TGA-PRSTZ-AGG-ULT
     * 
     */
    AfDecimal getTgaPrstzAggUltObj();

    void setTgaPrstzAggUltObj(AfDecimal tgaPrstzAggUltObj);

    /**
     * Host Variable TGA-TS-RIVAL-NET
     * 
     */
    AfDecimal getTgaTsRivalNet();

    void setTgaTsRivalNet(AfDecimal tgaTsRivalNet);

    /**
     * Nullable property for TGA-TS-RIVAL-NET
     * 
     */
    AfDecimal getTgaTsRivalNetObj();

    void setTgaTsRivalNetObj(AfDecimal tgaTsRivalNetObj);

    /**
     * Host Variable TGA-PRE-PATTUITO
     * 
     */
    AfDecimal getTgaPrePattuito();

    void setTgaPrePattuito(AfDecimal tgaPrePattuito);

    /**
     * Nullable property for TGA-PRE-PATTUITO
     * 
     */
    AfDecimal getTgaPrePattuitoObj();

    void setTgaPrePattuitoObj(AfDecimal tgaPrePattuitoObj);

    /**
     * Host Variable TGA-TP-RIVAL
     * 
     */
    String getTgaTpRival();

    void setTgaTpRival(String tgaTpRival);

    /**
     * Nullable property for TGA-TP-RIVAL
     * 
     */
    String getTgaTpRivalObj();

    void setTgaTpRivalObj(String tgaTpRivalObj);

    /**
     * Host Variable TGA-RIS-MAT
     * 
     */
    AfDecimal getTgaRisMat();

    void setTgaRisMat(AfDecimal tgaRisMat);

    /**
     * Nullable property for TGA-RIS-MAT
     * 
     */
    AfDecimal getTgaRisMatObj();

    void setTgaRisMatObj(AfDecimal tgaRisMatObj);

    /**
     * Host Variable TGA-CPT-MIN-SCAD
     * 
     */
    AfDecimal getTgaCptMinScad();

    void setTgaCptMinScad(AfDecimal tgaCptMinScad);

    /**
     * Nullable property for TGA-CPT-MIN-SCAD
     * 
     */
    AfDecimal getTgaCptMinScadObj();

    void setTgaCptMinScadObj(AfDecimal tgaCptMinScadObj);

    /**
     * Host Variable TGA-COMMIS-GEST
     * 
     */
    AfDecimal getTgaCommisGest();

    void setTgaCommisGest(AfDecimal tgaCommisGest);

    /**
     * Nullable property for TGA-COMMIS-GEST
     * 
     */
    AfDecimal getTgaCommisGestObj();

    void setTgaCommisGestObj(AfDecimal tgaCommisGestObj);

    /**
     * Host Variable TGA-TP-MANFEE-APPL
     * 
     */
    String getTgaTpManfeeAppl();

    void setTgaTpManfeeAppl(String tgaTpManfeeAppl);

    /**
     * Nullable property for TGA-TP-MANFEE-APPL
     * 
     */
    String getTgaTpManfeeApplObj();

    void setTgaTpManfeeApplObj(String tgaTpManfeeApplObj);

    /**
     * Host Variable TGA-DS-RIGA
     * 
     */
    long getTgaDsRiga();

    void setTgaDsRiga(long tgaDsRiga);

    /**
     * Host Variable TGA-DS-OPER-SQL
     * 
     */
    char getTgaDsOperSql();

    void setTgaDsOperSql(char tgaDsOperSql);

    /**
     * Host Variable TGA-DS-VER
     * 
     */
    int getTgaDsVer();

    void setTgaDsVer(int tgaDsVer);

    /**
     * Host Variable TGA-DS-TS-INI-CPTZ
     * 
     */
    long getTgaDsTsIniCptz();

    void setTgaDsTsIniCptz(long tgaDsTsIniCptz);

    /**
     * Host Variable TGA-DS-TS-END-CPTZ
     * 
     */
    long getTgaDsTsEndCptz();

    void setTgaDsTsEndCptz(long tgaDsTsEndCptz);

    /**
     * Host Variable TGA-DS-UTENTE
     * 
     */
    String getTgaDsUtente();

    void setTgaDsUtente(String tgaDsUtente);

    /**
     * Host Variable TGA-DS-STATO-ELAB
     * 
     */
    char getTgaDsStatoElab();

    void setTgaDsStatoElab(char tgaDsStatoElab);

    /**
     * Host Variable TGA-PC-COMMIS-GEST
     * 
     */
    AfDecimal getTgaPcCommisGest();

    void setTgaPcCommisGest(AfDecimal tgaPcCommisGest);

    /**
     * Nullable property for TGA-PC-COMMIS-GEST
     * 
     */
    AfDecimal getTgaPcCommisGestObj();

    void setTgaPcCommisGestObj(AfDecimal tgaPcCommisGestObj);

    /**
     * Host Variable TGA-NUM-GG-RIVAL
     * 
     */
    int getTgaNumGgRival();

    void setTgaNumGgRival(int tgaNumGgRival);

    /**
     * Nullable property for TGA-NUM-GG-RIVAL
     * 
     */
    Integer getTgaNumGgRivalObj();

    void setTgaNumGgRivalObj(Integer tgaNumGgRivalObj);

    /**
     * Host Variable TGA-IMP-TRASFE
     * 
     */
    AfDecimal getTgaImpTrasfe();

    void setTgaImpTrasfe(AfDecimal tgaImpTrasfe);

    /**
     * Nullable property for TGA-IMP-TRASFE
     * 
     */
    AfDecimal getTgaImpTrasfeObj();

    void setTgaImpTrasfeObj(AfDecimal tgaImpTrasfeObj);

    /**
     * Host Variable TGA-IMP-TFR-STRC
     * 
     */
    AfDecimal getTgaImpTfrStrc();

    void setTgaImpTfrStrc(AfDecimal tgaImpTfrStrc);

    /**
     * Nullable property for TGA-IMP-TFR-STRC
     * 
     */
    AfDecimal getTgaImpTfrStrcObj();

    void setTgaImpTfrStrcObj(AfDecimal tgaImpTfrStrcObj);

    /**
     * Host Variable TGA-ACQ-EXP
     * 
     */
    AfDecimal getTgaAcqExp();

    void setTgaAcqExp(AfDecimal tgaAcqExp);

    /**
     * Nullable property for TGA-ACQ-EXP
     * 
     */
    AfDecimal getTgaAcqExpObj();

    void setTgaAcqExpObj(AfDecimal tgaAcqExpObj);

    /**
     * Host Variable TGA-REMUN-ASS
     * 
     */
    AfDecimal getTgaRemunAss();

    void setTgaRemunAss(AfDecimal tgaRemunAss);

    /**
     * Nullable property for TGA-REMUN-ASS
     * 
     */
    AfDecimal getTgaRemunAssObj();

    void setTgaRemunAssObj(AfDecimal tgaRemunAssObj);

    /**
     * Host Variable TGA-COMMIS-INTER
     * 
     */
    AfDecimal getTgaCommisInter();

    void setTgaCommisInter(AfDecimal tgaCommisInter);

    /**
     * Nullable property for TGA-COMMIS-INTER
     * 
     */
    AfDecimal getTgaCommisInterObj();

    void setTgaCommisInterObj(AfDecimal tgaCommisInterObj);

    /**
     * Host Variable TGA-ALQ-REMUN-ASS
     * 
     */
    AfDecimal getTgaAlqRemunAss();

    void setTgaAlqRemunAss(AfDecimal tgaAlqRemunAss);

    /**
     * Nullable property for TGA-ALQ-REMUN-ASS
     * 
     */
    AfDecimal getTgaAlqRemunAssObj();

    void setTgaAlqRemunAssObj(AfDecimal tgaAlqRemunAssObj);

    /**
     * Host Variable TGA-ALQ-COMMIS-INTER
     * 
     */
    AfDecimal getTgaAlqCommisInter();

    void setTgaAlqCommisInter(AfDecimal tgaAlqCommisInter);

    /**
     * Nullable property for TGA-ALQ-COMMIS-INTER
     * 
     */
    AfDecimal getTgaAlqCommisInterObj();

    void setTgaAlqCommisInterObj(AfDecimal tgaAlqCommisInterObj);

    /**
     * Host Variable TGA-IMPB-REMUN-ASS
     * 
     */
    AfDecimal getTgaImpbRemunAss();

    void setTgaImpbRemunAss(AfDecimal tgaImpbRemunAss);

    /**
     * Nullable property for TGA-IMPB-REMUN-ASS
     * 
     */
    AfDecimal getTgaImpbRemunAssObj();

    void setTgaImpbRemunAssObj(AfDecimal tgaImpbRemunAssObj);

    /**
     * Host Variable TGA-IMPB-COMMIS-INTER
     * 
     */
    AfDecimal getTgaImpbCommisInter();

    void setTgaImpbCommisInter(AfDecimal tgaImpbCommisInter);

    /**
     * Nullable property for TGA-IMPB-COMMIS-INTER
     * 
     */
    AfDecimal getTgaImpbCommisInterObj();

    void setTgaImpbCommisInterObj(AfDecimal tgaImpbCommisInterObj);

    /**
     * Host Variable TGA-COS-RUN-ASSVA
     * 
     */
    AfDecimal getTgaCosRunAssva();

    void setTgaCosRunAssva(AfDecimal tgaCosRunAssva);

    /**
     * Nullable property for TGA-COS-RUN-ASSVA
     * 
     */
    AfDecimal getTgaCosRunAssvaObj();

    void setTgaCosRunAssvaObj(AfDecimal tgaCosRunAssvaObj);

    /**
     * Host Variable TGA-COS-RUN-ASSVA-IDC
     * 
     */
    AfDecimal getTgaCosRunAssvaIdc();

    void setTgaCosRunAssvaIdc(AfDecimal tgaCosRunAssvaIdc);

    /**
     * Nullable property for TGA-COS-RUN-ASSVA-IDC
     * 
     */
    AfDecimal getTgaCosRunAssvaIdcObj();

    void setTgaCosRunAssvaIdcObj(AfDecimal tgaCosRunAssvaIdcObj);

    /**
     * Host Variable STB-ID-STAT-OGG-BUS
     * 
     */
    int getStbIdStatOggBus();

    void setStbIdStatOggBus(int stbIdStatOggBus);

    /**
     * Host Variable STB-ID-OGG
     * 
     */
    int getStbIdOgg();

    void setStbIdOgg(int stbIdOgg);

    /**
     * Host Variable STB-TP-OGG
     * 
     */
    String getStbTpOgg();

    void setStbTpOgg(String stbTpOgg);

    /**
     * Host Variable STB-ID-MOVI-CRZ
     * 
     */
    int getStbIdMoviCrz();

    void setStbIdMoviCrz(int stbIdMoviCrz);

    /**
     * Host Variable STB-ID-MOVI-CHIU
     * 
     */
    int getStbIdMoviChiu();

    void setStbIdMoviChiu(int stbIdMoviChiu);

    /**
     * Nullable property for STB-ID-MOVI-CHIU
     * 
     */
    Integer getStbIdMoviChiuObj();

    void setStbIdMoviChiuObj(Integer stbIdMoviChiuObj);

    /**
     * Host Variable STB-DT-INI-EFF-DB
     * 
     */
    String getStbDtIniEffDb();

    void setStbDtIniEffDb(String stbDtIniEffDb);

    /**
     * Host Variable STB-DT-END-EFF-DB
     * 
     */
    String getStbDtEndEffDb();

    void setStbDtEndEffDb(String stbDtEndEffDb);

    /**
     * Host Variable STB-COD-COMP-ANIA
     * 
     */
    int getStbCodCompAnia();

    void setStbCodCompAnia(int stbCodCompAnia);

    /**
     * Host Variable STB-TP-STAT-BUS
     * 
     */
    String getStbTpStatBus();

    void setStbTpStatBus(String stbTpStatBus);

    /**
     * Host Variable STB-TP-CAUS
     * 
     */
    String getStbTpCaus();

    void setStbTpCaus(String stbTpCaus);

    /**
     * Host Variable STB-DS-RIGA
     * 
     */
    long getStbDsRiga();

    void setStbDsRiga(long stbDsRiga);

    /**
     * Host Variable STB-DS-OPER-SQL
     * 
     */
    char getStbDsOperSql();

    void setStbDsOperSql(char stbDsOperSql);

    /**
     * Host Variable STB-DS-VER
     * 
     */
    int getStbDsVer();

    void setStbDsVer(int stbDsVer);

    /**
     * Host Variable STB-DS-TS-INI-CPTZ
     * 
     */
    long getStbDsTsIniCptz();

    void setStbDsTsIniCptz(long stbDsTsIniCptz);

    /**
     * Host Variable STB-DS-TS-END-CPTZ
     * 
     */
    long getStbDsTsEndCptz();

    void setStbDsTsEndCptz(long stbDsTsEndCptz);

    /**
     * Host Variable STB-DS-UTENTE
     * 
     */
    String getStbDsUtente();

    void setStbDsUtente(String stbDsUtente);

    /**
     * Host Variable STB-DS-STATO-ELAB
     * 
     */
    char getStbDsStatoElab();

    void setStbDsStatoElab(char stbDsStatoElab);

    /**
     * Host Variable LDBI0731-ID-TRCH
     * 
     */
    int getLdbi0731IdTrch();

    void setLdbi0731IdTrch(int ldbi0731IdTrch);

    /**
     * Host Variable LDBV2911-PRSTZ-INI-NEWFIS
     * 
     */
    AfDecimal getLdbv2911PrstzIniNewfis();

    void setLdbv2911PrstzIniNewfis(AfDecimal ldbv2911PrstzIniNewfis);

    /**
     * Host Variable LDBV2911-ID-POLI
     * 
     */
    int getLdbv2911IdPoli();

    void setLdbv2911IdPoli(int ldbv2911IdPoli);

    /**
     * Host Variable LDBV2911-ID-ADES
     * 
     */
    int getLdbv2911IdAdes();

    void setLdbv2911IdAdes(int ldbv2911IdAdes);

    /**
     * Host Variable LC601-ID-TRCH-DI-GAR
     * 
     */
    int getLc601IdTrchDiGar();

    void setLc601IdTrchDiGar(int lc601IdTrchDiGar);

    /**
     * Host Variable LC601-ID-MOVI-CRZ
     * 
     */
    int getLc601IdMoviCrz();

    void setLc601IdMoviCrz(int lc601IdMoviCrz);

    /**
     * Host Variable WS-DT-INFINITO-1
     * 
     */
    String getWsDtInfinito1();

    void setWsDtInfinito1(String wsDtInfinito1);

    /**
     * Host Variable LDBVE251-IMPB-VIS-END2000
     * 
     */
    AfDecimal getLdbve251ImpbVisEnd2000();

    void setLdbve251ImpbVisEnd2000(AfDecimal ldbve251ImpbVisEnd2000);

    /**
     * Host Variable LDBVE251-ID-MOVI
     * 
     */
    int getLdbve251IdMovi();

    void setLdbve251IdMovi(int ldbve251IdMovi);

    /**
     * Host Variable LDBVE251-ID-ADES
     * 
     */
    int getLdbve251IdAdes();

    void setLdbve251IdAdes(int ldbve251IdAdes);

    /**
     * Host Variable LDBVE251-TP-TRCH-1
     * 
     */
    String getLdbve251TpTrch1();

    void setLdbve251TpTrch1(String ldbve251TpTrch1);

    /**
     * Host Variable LDBVE251-TP-TRCH-2
     * 
     */
    String getLdbve251TpTrch2();

    void setLdbve251TpTrch2(String ldbve251TpTrch2);

    /**
     * Host Variable LDBVE251-TP-TRCH-3
     * 
     */
    String getLdbve251TpTrch3();

    void setLdbve251TpTrch3(String ldbve251TpTrch3);

    /**
     * Host Variable LDBVE251-TP-TRCH-4
     * 
     */
    String getLdbve251TpTrch4();

    void setLdbve251TpTrch4(String ldbve251TpTrch4);

    /**
     * Host Variable LDBVE251-TP-TRCH-5
     * 
     */
    String getLdbve251TpTrch5();

    void setLdbve251TpTrch5(String ldbve251TpTrch5);

    /**
     * Host Variable LDBVE251-TP-TRCH-6
     * 
     */
    String getLdbve251TpTrch6();

    void setLdbve251TpTrch6(String ldbve251TpTrch6);

    /**
     * Host Variable LDBVE251-TP-TRCH-7
     * 
     */
    String getLdbve251TpTrch7();

    void setLdbve251TpTrch7(String ldbve251TpTrch7);

    /**
     * Host Variable LDBVE251-TP-TRCH-8
     * 
     */
    String getLdbve251TpTrch8();

    void setLdbve251TpTrch8(String ldbve251TpTrch8);

    /**
     * Host Variable LDBVE251-TP-TRCH-9
     * 
     */
    String getLdbve251TpTrch9();

    void setLdbve251TpTrch9(String ldbve251TpTrch9);

    /**
     * Host Variable LDBVE251-TP-TRCH-10
     * 
     */
    String getLdbve251TpTrch10();

    void setLdbve251TpTrch10(String ldbve251TpTrch10);

    /**
     * Host Variable LDBVE251-TP-TRCH-11
     * 
     */
    String getLdbve251TpTrch11();

    void setLdbve251TpTrch11(String ldbve251TpTrch11);

    /**
     * Host Variable LDBVE251-TP-TRCH-12
     * 
     */
    String getLdbve251TpTrch12();

    void setLdbve251TpTrch12(String ldbve251TpTrch12);

    /**
     * Host Variable LDBVE251-TP-TRCH-13
     * 
     */
    String getLdbve251TpTrch13();

    void setLdbve251TpTrch13(String ldbve251TpTrch13);

    /**
     * Host Variable LDBVE251-TP-TRCH-14
     * 
     */
    String getLdbve251TpTrch14();

    void setLdbve251TpTrch14(String ldbve251TpTrch14);

    /**
     * Host Variable LDBVE251-TP-TRCH-15
     * 
     */
    String getLdbve251TpTrch15();

    void setLdbve251TpTrch15(String ldbve251TpTrch15);

    /**
     * Host Variable LDBVE251-TP-TRCH-16
     * 
     */
    String getLdbve251TpTrch16();

    void setLdbve251TpTrch16(String ldbve251TpTrch16);

    /**
     * Host Variable LDBVE261-IMPB-VIS-END2000
     * 
     */
    AfDecimal getLdbve261ImpbVisEnd2000();

    void setLdbve261ImpbVisEnd2000(AfDecimal ldbve261ImpbVisEnd2000);

    /**
     * Host Variable LDBVE261-ID-ADES
     * 
     */
    int getLdbve261IdAdes();

    void setLdbve261IdAdes(int ldbve261IdAdes);

    /**
     * Host Variable LDBVE261-TP-TRCH-1
     * 
     */
    String getLdbve261TpTrch1();

    void setLdbve261TpTrch1(String ldbve261TpTrch1);

    /**
     * Host Variable LDBVE261-TP-TRCH-2
     * 
     */
    String getLdbve261TpTrch2();

    void setLdbve261TpTrch2(String ldbve261TpTrch2);

    /**
     * Host Variable LDBVE261-TP-TRCH-3
     * 
     */
    String getLdbve261TpTrch3();

    void setLdbve261TpTrch3(String ldbve261TpTrch3);

    /**
     * Host Variable LDBVE261-TP-TRCH-4
     * 
     */
    String getLdbve261TpTrch4();

    void setLdbve261TpTrch4(String ldbve261TpTrch4);

    /**
     * Host Variable LDBVE261-TP-TRCH-5
     * 
     */
    String getLdbve261TpTrch5();

    void setLdbve261TpTrch5(String ldbve261TpTrch5);

    /**
     * Host Variable LDBVE261-TP-TRCH-6
     * 
     */
    String getLdbve261TpTrch6();

    void setLdbve261TpTrch6(String ldbve261TpTrch6);

    /**
     * Host Variable LDBVE261-TP-TRCH-7
     * 
     */
    String getLdbve261TpTrch7();

    void setLdbve261TpTrch7(String ldbve261TpTrch7);

    /**
     * Host Variable LDBVE261-TP-TRCH-8
     * 
     */
    String getLdbve261TpTrch8();

    void setLdbve261TpTrch8(String ldbve261TpTrch8);

    /**
     * Host Variable LDBVE261-TP-TRCH-9
     * 
     */
    String getLdbve261TpTrch9();

    void setLdbve261TpTrch9(String ldbve261TpTrch9);

    /**
     * Host Variable LDBVE261-TP-TRCH-10
     * 
     */
    String getLdbve261TpTrch10();

    void setLdbve261TpTrch10(String ldbve261TpTrch10);

    /**
     * Host Variable LDBVE261-TP-TRCH-11
     * 
     */
    String getLdbve261TpTrch11();

    void setLdbve261TpTrch11(String ldbve261TpTrch11);

    /**
     * Host Variable LDBVE261-TP-TRCH-12
     * 
     */
    String getLdbve261TpTrch12();

    void setLdbve261TpTrch12(String ldbve261TpTrch12);

    /**
     * Host Variable LDBVE261-TP-TRCH-13
     * 
     */
    String getLdbve261TpTrch13();

    void setLdbve261TpTrch13(String ldbve261TpTrch13);

    /**
     * Host Variable LDBVE261-TP-TRCH-14
     * 
     */
    String getLdbve261TpTrch14();

    void setLdbve261TpTrch14(String ldbve261TpTrch14);

    /**
     * Host Variable LDBVE261-TP-TRCH-15
     * 
     */
    String getLdbve261TpTrch15();

    void setLdbve261TpTrch15(String ldbve261TpTrch15);

    /**
     * Host Variable LDBVE261-TP-TRCH-16
     * 
     */
    String getLdbve261TpTrch16();

    void setLdbve261TpTrch16(String ldbve261TpTrch16);
};
