package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IEstPoli;

/**
 * Data Access Object(DAO) for table [EST_POLI]
 * 
 */
public class EstPoliDao extends BaseSqlDao<IEstPoli> {

    private Cursor cIdUpdEffE06;
    private Cursor cIdpEffE06;
    private Cursor cIboEffE06;
    private Cursor cIbsEffE060;
    private Cursor cIdpCpzE06;
    private Cursor cIboCpzE06;
    private Cursor cIbsCpzE060;
    private final IRowMapper<IEstPoli> selectByE06DsRigaRm = buildNamedRowMapper(IEstPoli.class, "idEstPoli", "idPoli", "ibOggObj", "ibProp", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "e06DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "dtUltPerdDbObj", "pcUltPerdObj", "flEsclSwitchMaxObj", "esiAdegzIsvapObj", "numInaObj", "tpModProvObj", "cptProtettoObj", "cumPreAttTakePObj", "dtEmisPartnerDbObj");

    public EstPoliDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IEstPoli> getToClass() {
        return IEstPoli.class;
    }

    public IEstPoli selectByE06DsRiga(long e06DsRiga, IEstPoli iEstPoli) {
        return buildQuery("selectByE06DsRiga").bind("e06DsRiga", e06DsRiga).rowMapper(selectByE06DsRigaRm).singleResult(iEstPoli);
    }

    public DbAccessStatus insertRec(IEstPoli iEstPoli) {
        return buildQuery("insertRec").bind(iEstPoli).executeInsert();
    }

    public DbAccessStatus updateRec(IEstPoli iEstPoli) {
        return buildQuery("updateRec").bind(iEstPoli).executeUpdate();
    }

    public DbAccessStatus deleteByE06DsRiga(long e06DsRiga) {
        return buildQuery("deleteByE06DsRiga").bind("e06DsRiga", e06DsRiga).executeDelete();
    }

    public IEstPoli selectRec(String e06IdEstPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstPoli iEstPoli) {
        return buildQuery("selectRec").bind("e06IdEstPoli", e06IdEstPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByE06DsRigaRm).singleResult(iEstPoli);
    }

    public DbAccessStatus updateRec1(IEstPoli iEstPoli) {
        return buildQuery("updateRec1").bind(iEstPoli).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffE06(String e06IdEstPoli, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffE06 = buildQuery("openCIdUpdEffE06").bind("e06IdEstPoli", e06IdEstPoli).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffE06() {
        return closeCursor(cIdUpdEffE06);
    }

    public IEstPoli fetchCIdUpdEffE06(IEstPoli iEstPoli) {
        return fetch(cIdUpdEffE06, iEstPoli, selectByE06DsRigaRm);
    }

    public IEstPoli selectRec1(int e06IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstPoli iEstPoli) {
        return buildQuery("selectRec1").bind("e06IdPoli", e06IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByE06DsRigaRm).singleResult(iEstPoli);
    }

    public DbAccessStatus openCIdpEffE06(int e06IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffE06 = buildQuery("openCIdpEffE06").bind("e06IdPoli", e06IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffE06() {
        return closeCursor(cIdpEffE06);
    }

    public IEstPoli fetchCIdpEffE06(IEstPoli iEstPoli) {
        return fetch(cIdpEffE06, iEstPoli, selectByE06DsRigaRm);
    }

    public IEstPoli selectRec2(String e06IbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstPoli iEstPoli) {
        return buildQuery("selectRec2").bind("e06IbOgg", e06IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByE06DsRigaRm).singleResult(iEstPoli);
    }

    public DbAccessStatus openCIboEffE06(String e06IbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIboEffE06 = buildQuery("openCIboEffE06").bind("e06IbOgg", e06IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboEffE06() {
        return closeCursor(cIboEffE06);
    }

    public IEstPoli fetchCIboEffE06(IEstPoli iEstPoli) {
        return fetch(cIboEffE06, iEstPoli, selectByE06DsRigaRm);
    }

    public IEstPoli selectRec3(String e06IbProp, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IEstPoli iEstPoli) {
        return buildQuery("selectRec3").bind("e06IbProp", e06IbProp).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByE06DsRigaRm).singleResult(iEstPoli);
    }

    public DbAccessStatus openCIbsEffE060(String e06IbProp, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIbsEffE060 = buildQuery("openCIbsEffE060").bind("e06IbProp", e06IbProp).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsEffE060() {
        return closeCursor(cIbsEffE060);
    }

    public IEstPoli fetchCIbsEffE060(IEstPoli iEstPoli) {
        return fetch(cIbsEffE060, iEstPoli, selectByE06DsRigaRm);
    }

    public IEstPoli selectRec4(String e06IdEstPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstPoli iEstPoli) {
        return buildQuery("selectRec4").bind("e06IdEstPoli", e06IdEstPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByE06DsRigaRm).singleResult(iEstPoli);
    }

    public IEstPoli selectRec5(int e06IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstPoli iEstPoli) {
        return buildQuery("selectRec5").bind("e06IdPoli", e06IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByE06DsRigaRm).singleResult(iEstPoli);
    }

    public DbAccessStatus openCIdpCpzE06(int e06IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzE06 = buildQuery("openCIdpCpzE06").bind("e06IdPoli", e06IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzE06() {
        return closeCursor(cIdpCpzE06);
    }

    public IEstPoli fetchCIdpCpzE06(IEstPoli iEstPoli) {
        return fetch(cIdpCpzE06, iEstPoli, selectByE06DsRigaRm);
    }

    public IEstPoli selectRec6(String e06IbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstPoli iEstPoli) {
        return buildQuery("selectRec6").bind("e06IbOgg", e06IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByE06DsRigaRm).singleResult(iEstPoli);
    }

    public DbAccessStatus openCIboCpzE06(String e06IbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIboCpzE06 = buildQuery("openCIboCpzE06").bind("e06IbOgg", e06IbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboCpzE06() {
        return closeCursor(cIboCpzE06);
    }

    public IEstPoli fetchCIboCpzE06(IEstPoli iEstPoli) {
        return fetch(cIboCpzE06, iEstPoli, selectByE06DsRigaRm);
    }

    public IEstPoli selectRec7(String e06IbProp, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IEstPoli iEstPoli) {
        return buildQuery("selectRec7").bind("e06IbProp", e06IbProp).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByE06DsRigaRm).singleResult(iEstPoli);
    }

    public DbAccessStatus openCIbsCpzE060(String e06IbProp, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIbsCpzE060 = buildQuery("openCIbsCpzE060").bind("e06IbProp", e06IbProp).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsCpzE060() {
        return closeCursor(cIbsCpzE060);
    }

    public IEstPoli fetchCIbsCpzE060(IEstPoli iEstPoli) {
        return fetch(cIbsCpzE060, iEstPoli, selectByE06DsRigaRm);
    }
}
