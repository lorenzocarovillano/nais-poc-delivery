package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBilaFndEstr;

/**
 * Data Access Object(DAO) for table [BILA_FND_ESTR]
 * 
 */
public class BilaFndEstrDao extends BaseSqlDao<IBilaFndEstr> {

    private final IRowMapper<IBilaFndEstr> selectByB01IdBilaFndEstrRm = buildNamedRowMapper(IBilaFndEstr.class, "b01IdBilaFndEstr", "idBilaTrchEstr", "codCompAnia", "idRichEstrazMas", "idRichEstrazAggObj", "dtRisDb", "idPoli", "idAdes", "idGar", "idTrchDiGar", "codFnd", "dtQtzIniDbObj", "numQuoIniObj", "numQuoDtCalcObj", "valQuoIniObj", "dtValzzQuoDtCaDbObj", "valQuoDtCalcObj", "valQuoTObj", "pcInvstObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public BilaFndEstrDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBilaFndEstr> getToClass() {
        return IBilaFndEstr.class;
    }

    public IBilaFndEstr selectByB01IdBilaFndEstr(int b01IdBilaFndEstr, IBilaFndEstr iBilaFndEstr) {
        return buildQuery("selectByB01IdBilaFndEstr").bind("b01IdBilaFndEstr", b01IdBilaFndEstr).rowMapper(selectByB01IdBilaFndEstrRm).singleResult(iBilaFndEstr);
    }

    public DbAccessStatus insertRec(IBilaFndEstr iBilaFndEstr) {
        return buildQuery("insertRec").bind(iBilaFndEstr).executeInsert();
    }

    public DbAccessStatus updateRec(IBilaFndEstr iBilaFndEstr) {
        return buildQuery("updateRec").bind(iBilaFndEstr).executeUpdate();
    }

    public DbAccessStatus deleteByB01IdBilaFndEstr(int b01IdBilaFndEstr) {
        return buildQuery("deleteByB01IdBilaFndEstr").bind("b01IdBilaFndEstr", b01IdBilaFndEstr).executeDelete();
    }
}
