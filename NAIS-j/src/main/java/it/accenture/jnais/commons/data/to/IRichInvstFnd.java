package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [RICH_INVST_FND]
 * 
 */
public interface IRichInvstFnd extends BaseSqlTo {

    /**
     * Host Variable RIF-ID-RICH-INVST-FND
     * 
     */
    int getIdRichInvstFnd();

    void setIdRichInvstFnd(int idRichInvstFnd);

    /**
     * Host Variable RIF-ID-MOVI-FINRIO
     * 
     */
    int getIdMoviFinrio();

    void setIdMoviFinrio(int idMoviFinrio);

    /**
     * Host Variable RIF-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable RIF-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for RIF-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable RIF-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable RIF-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable RIF-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable RIF-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Nullable property for RIF-COD-FND
     * 
     */
    String getCodFndObj();

    void setCodFndObj(String codFndObj);

    /**
     * Host Variable RIF-NUM-QUO
     * 
     */
    AfDecimal getNumQuo();

    void setNumQuo(AfDecimal numQuo);

    /**
     * Nullable property for RIF-NUM-QUO
     * 
     */
    AfDecimal getNumQuoObj();

    void setNumQuoObj(AfDecimal numQuoObj);

    /**
     * Host Variable RIF-PC
     * 
     */
    AfDecimal getPc();

    void setPc(AfDecimal pc);

    /**
     * Nullable property for RIF-PC
     * 
     */
    AfDecimal getPcObj();

    void setPcObj(AfDecimal pcObj);

    /**
     * Host Variable RIF-IMP-MOVTO
     * 
     */
    AfDecimal getImpMovto();

    void setImpMovto(AfDecimal impMovto);

    /**
     * Nullable property for RIF-IMP-MOVTO
     * 
     */
    AfDecimal getImpMovtoObj();

    void setImpMovtoObj(AfDecimal impMovtoObj);

    /**
     * Host Variable RIF-DT-INVST-DB
     * 
     */
    String getDtInvstDb();

    void setDtInvstDb(String dtInvstDb);

    /**
     * Nullable property for RIF-DT-INVST-DB
     * 
     */
    String getDtInvstDbObj();

    void setDtInvstDbObj(String dtInvstDbObj);

    /**
     * Host Variable RIF-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Nullable property for RIF-COD-TARI
     * 
     */
    String getCodTariObj();

    void setCodTariObj(String codTariObj);

    /**
     * Host Variable RIF-TP-STAT
     * 
     */
    String getTpStat();

    void setTpStat(String tpStat);

    /**
     * Nullable property for RIF-TP-STAT
     * 
     */
    String getTpStatObj();

    void setTpStatObj(String tpStatObj);

    /**
     * Host Variable RIF-TP-MOD-INVST
     * 
     */
    String getTpModInvst();

    void setTpModInvst(String tpModInvst);

    /**
     * Nullable property for RIF-TP-MOD-INVST
     * 
     */
    String getTpModInvstObj();

    void setTpModInvstObj(String tpModInvstObj);

    /**
     * Host Variable RIF-COD-DIV
     * 
     */
    String getCodDiv();

    void setCodDiv(String codDiv);

    /**
     * Host Variable RIF-DT-CAMBIO-VLT-DB
     * 
     */
    String getDtCambioVltDb();

    void setDtCambioVltDb(String dtCambioVltDb);

    /**
     * Nullable property for RIF-DT-CAMBIO-VLT-DB
     * 
     */
    String getDtCambioVltDbObj();

    void setDtCambioVltDbObj(String dtCambioVltDbObj);

    /**
     * Host Variable RIF-TP-FND
     * 
     */
    char getTpFnd();

    void setTpFnd(char tpFnd);

    /**
     * Host Variable RIF-DS-RIGA
     * 
     */
    long getRifDsRiga();

    void setRifDsRiga(long rifDsRiga);

    /**
     * Host Variable RIF-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable RIF-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable RIF-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable RIF-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable RIF-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable RIF-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable RIF-DT-INVST-CALC-DB
     * 
     */
    String getDtInvstCalcDb();

    void setDtInvstCalcDb(String dtInvstCalcDb);

    /**
     * Nullable property for RIF-DT-INVST-CALC-DB
     * 
     */
    String getDtInvstCalcDbObj();

    void setDtInvstCalcDbObj(String dtInvstCalcDbObj);

    /**
     * Host Variable RIF-FL-CALC-INVTO
     * 
     */
    char getFlCalcInvto();

    void setFlCalcInvto(char flCalcInvto);

    /**
     * Nullable property for RIF-FL-CALC-INVTO
     * 
     */
    Character getFlCalcInvtoObj();

    void setFlCalcInvtoObj(Character flCalcInvtoObj);

    /**
     * Host Variable RIF-IMP-GAP-EVENT
     * 
     */
    AfDecimal getImpGapEvent();

    void setImpGapEvent(AfDecimal impGapEvent);

    /**
     * Nullable property for RIF-IMP-GAP-EVENT
     * 
     */
    AfDecimal getImpGapEventObj();

    void setImpGapEventObj(AfDecimal impGapEventObj);

    /**
     * Host Variable RIF-FL-SWM-BP2S
     * 
     */
    char getFlSwmBp2s();

    void setFlSwmBp2s(char flSwmBp2s);

    /**
     * Nullable property for RIF-FL-SWM-BP2S
     * 
     */
    Character getFlSwmBp2sObj();

    void setFlSwmBp2sObj(Character flSwmBp2sObj);
};
