package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [DETT_TIT_DI_RAT]
 * 
 */
public interface IDettTitDiRat extends BaseSqlTo {

    /**
     * Host Variable DTR-ID-OGG
     * 
     */
    int getDtrIdOgg();

    void setDtrIdOgg(int dtrIdOgg);

    /**
     * Host Variable DTR-TP-OGG
     * 
     */
    String getDtrTpOgg();

    void setDtrTpOgg(String dtrTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable DTR-ID-TIT-RAT
     * 
     */
    int getDtrIdTitRat();

    void setDtrIdTitRat(int dtrIdTitRat);

    /**
     * Host Variable DTR-FL-VLDT-TIT
     * 
     */
    char getDtrFlVldtTit();

    void setDtrFlVldtTit(char dtrFlVldtTit);

    /**
     * Host Variable DTR-ID-DETT-TIT-DI-RAT
     * 
     */
    int getIdDettTitDiRat();

    void setIdDettTitDiRat(int idDettTitDiRat);

    /**
     * Host Variable DTR-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable DTR-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for DTR-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable DTR-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable DTR-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable DTR-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable DTR-DT-INI-COP-DB
     * 
     */
    String getDtIniCopDb();

    void setDtIniCopDb(String dtIniCopDb);

    /**
     * Nullable property for DTR-DT-INI-COP-DB
     * 
     */
    String getDtIniCopDbObj();

    void setDtIniCopDbObj(String dtIniCopDbObj);

    /**
     * Host Variable DTR-DT-END-COP-DB
     * 
     */
    String getDtEndCopDb();

    void setDtEndCopDb(String dtEndCopDb);

    /**
     * Nullable property for DTR-DT-END-COP-DB
     * 
     */
    String getDtEndCopDbObj();

    void setDtEndCopDbObj(String dtEndCopDbObj);

    /**
     * Host Variable DTR-PRE-NET
     * 
     */
    AfDecimal getPreNet();

    void setPreNet(AfDecimal preNet);

    /**
     * Nullable property for DTR-PRE-NET
     * 
     */
    AfDecimal getPreNetObj();

    void setPreNetObj(AfDecimal preNetObj);

    /**
     * Host Variable DTR-INTR-FRAZ
     * 
     */
    AfDecimal getIntrFraz();

    void setIntrFraz(AfDecimal intrFraz);

    /**
     * Nullable property for DTR-INTR-FRAZ
     * 
     */
    AfDecimal getIntrFrazObj();

    void setIntrFrazObj(AfDecimal intrFrazObj);

    /**
     * Host Variable DTR-INTR-MORA
     * 
     */
    AfDecimal getIntrMora();

    void setIntrMora(AfDecimal intrMora);

    /**
     * Nullable property for DTR-INTR-MORA
     * 
     */
    AfDecimal getIntrMoraObj();

    void setIntrMoraObj(AfDecimal intrMoraObj);

    /**
     * Host Variable DTR-INTR-RETDT
     * 
     */
    AfDecimal getIntrRetdt();

    void setIntrRetdt(AfDecimal intrRetdt);

    /**
     * Nullable property for DTR-INTR-RETDT
     * 
     */
    AfDecimal getIntrRetdtObj();

    void setIntrRetdtObj(AfDecimal intrRetdtObj);

    /**
     * Host Variable DTR-INTR-RIAT
     * 
     */
    AfDecimal getIntrRiat();

    void setIntrRiat(AfDecimal intrRiat);

    /**
     * Nullable property for DTR-INTR-RIAT
     * 
     */
    AfDecimal getIntrRiatObj();

    void setIntrRiatObj(AfDecimal intrRiatObj);

    /**
     * Host Variable DTR-DIR
     * 
     */
    AfDecimal getDir();

    void setDir(AfDecimal dir);

    /**
     * Nullable property for DTR-DIR
     * 
     */
    AfDecimal getDirObj();

    void setDirObj(AfDecimal dirObj);

    /**
     * Host Variable DTR-SPE-MED
     * 
     */
    AfDecimal getSpeMed();

    void setSpeMed(AfDecimal speMed);

    /**
     * Nullable property for DTR-SPE-MED
     * 
     */
    AfDecimal getSpeMedObj();

    void setSpeMedObj(AfDecimal speMedObj);

    /**
     * Host Variable DTR-SPE-AGE
     * 
     */
    AfDecimal getSpeAge();

    void setSpeAge(AfDecimal speAge);

    /**
     * Nullable property for DTR-SPE-AGE
     * 
     */
    AfDecimal getSpeAgeObj();

    void setSpeAgeObj(AfDecimal speAgeObj);

    /**
     * Host Variable DTR-TAX
     * 
     */
    AfDecimal getTax();

    void setTax(AfDecimal tax);

    /**
     * Nullable property for DTR-TAX
     * 
     */
    AfDecimal getTaxObj();

    void setTaxObj(AfDecimal taxObj);

    /**
     * Host Variable DTR-SOPR-SAN
     * 
     */
    AfDecimal getSoprSan();

    void setSoprSan(AfDecimal soprSan);

    /**
     * Nullable property for DTR-SOPR-SAN
     * 
     */
    AfDecimal getSoprSanObj();

    void setSoprSanObj(AfDecimal soprSanObj);

    /**
     * Host Variable DTR-SOPR-SPO
     * 
     */
    AfDecimal getSoprSpo();

    void setSoprSpo(AfDecimal soprSpo);

    /**
     * Nullable property for DTR-SOPR-SPO
     * 
     */
    AfDecimal getSoprSpoObj();

    void setSoprSpoObj(AfDecimal soprSpoObj);

    /**
     * Host Variable DTR-SOPR-TEC
     * 
     */
    AfDecimal getSoprTec();

    void setSoprTec(AfDecimal soprTec);

    /**
     * Nullable property for DTR-SOPR-TEC
     * 
     */
    AfDecimal getSoprTecObj();

    void setSoprTecObj(AfDecimal soprTecObj);

    /**
     * Host Variable DTR-SOPR-PROF
     * 
     */
    AfDecimal getSoprProf();

    void setSoprProf(AfDecimal soprProf);

    /**
     * Nullable property for DTR-SOPR-PROF
     * 
     */
    AfDecimal getSoprProfObj();

    void setSoprProfObj(AfDecimal soprProfObj);

    /**
     * Host Variable DTR-SOPR-ALT
     * 
     */
    AfDecimal getSoprAlt();

    void setSoprAlt(AfDecimal soprAlt);

    /**
     * Nullable property for DTR-SOPR-ALT
     * 
     */
    AfDecimal getSoprAltObj();

    void setSoprAltObj(AfDecimal soprAltObj);

    /**
     * Host Variable DTR-PRE-TOT
     * 
     */
    AfDecimal getPreTot();

    void setPreTot(AfDecimal preTot);

    /**
     * Nullable property for DTR-PRE-TOT
     * 
     */
    AfDecimal getPreTotObj();

    void setPreTotObj(AfDecimal preTotObj);

    /**
     * Host Variable DTR-PRE-PP-IAS
     * 
     */
    AfDecimal getPrePpIas();

    void setPrePpIas(AfDecimal prePpIas);

    /**
     * Nullable property for DTR-PRE-PP-IAS
     * 
     */
    AfDecimal getPrePpIasObj();

    void setPrePpIasObj(AfDecimal prePpIasObj);

    /**
     * Host Variable DTR-PRE-SOLO-RSH
     * 
     */
    AfDecimal getPreSoloRsh();

    void setPreSoloRsh(AfDecimal preSoloRsh);

    /**
     * Nullable property for DTR-PRE-SOLO-RSH
     * 
     */
    AfDecimal getPreSoloRshObj();

    void setPreSoloRshObj(AfDecimal preSoloRshObj);

    /**
     * Host Variable DTR-CAR-IAS
     * 
     */
    AfDecimal getCarIas();

    void setCarIas(AfDecimal carIas);

    /**
     * Nullable property for DTR-CAR-IAS
     * 
     */
    AfDecimal getCarIasObj();

    void setCarIasObj(AfDecimal carIasObj);

    /**
     * Host Variable DTR-PROV-ACQ-1AA
     * 
     */
    AfDecimal getProvAcq1aa();

    void setProvAcq1aa(AfDecimal provAcq1aa);

    /**
     * Nullable property for DTR-PROV-ACQ-1AA
     * 
     */
    AfDecimal getProvAcq1aaObj();

    void setProvAcq1aaObj(AfDecimal provAcq1aaObj);

    /**
     * Host Variable DTR-PROV-ACQ-2AA
     * 
     */
    AfDecimal getProvAcq2aa();

    void setProvAcq2aa(AfDecimal provAcq2aa);

    /**
     * Nullable property for DTR-PROV-ACQ-2AA
     * 
     */
    AfDecimal getProvAcq2aaObj();

    void setProvAcq2aaObj(AfDecimal provAcq2aaObj);

    /**
     * Host Variable DTR-PROV-RICOR
     * 
     */
    AfDecimal getProvRicor();

    void setProvRicor(AfDecimal provRicor);

    /**
     * Nullable property for DTR-PROV-RICOR
     * 
     */
    AfDecimal getProvRicorObj();

    void setProvRicorObj(AfDecimal provRicorObj);

    /**
     * Host Variable DTR-PROV-INC
     * 
     */
    AfDecimal getProvInc();

    void setProvInc(AfDecimal provInc);

    /**
     * Nullable property for DTR-PROV-INC
     * 
     */
    AfDecimal getProvIncObj();

    void setProvIncObj(AfDecimal provIncObj);

    /**
     * Host Variable DTR-PROV-DA-REC
     * 
     */
    AfDecimal getProvDaRec();

    void setProvDaRec(AfDecimal provDaRec);

    /**
     * Nullable property for DTR-PROV-DA-REC
     * 
     */
    AfDecimal getProvDaRecObj();

    void setProvDaRecObj(AfDecimal provDaRecObj);

    /**
     * Host Variable DTR-COD-DVS
     * 
     */
    String getCodDvs();

    void setCodDvs(String codDvs);

    /**
     * Nullable property for DTR-COD-DVS
     * 
     */
    String getCodDvsObj();

    void setCodDvsObj(String codDvsObj);

    /**
     * Host Variable DTR-FRQ-MOVI
     * 
     */
    int getFrqMovi();

    void setFrqMovi(int frqMovi);

    /**
     * Nullable property for DTR-FRQ-MOVI
     * 
     */
    Integer getFrqMoviObj();

    void setFrqMoviObj(Integer frqMoviObj);

    /**
     * Host Variable DTR-TP-RGM-FISC
     * 
     */
    String getTpRgmFisc();

    void setTpRgmFisc(String tpRgmFisc);

    /**
     * Host Variable DTR-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Nullable property for DTR-COD-TARI
     * 
     */
    String getCodTariObj();

    void setCodTariObj(String codTariObj);

    /**
     * Host Variable DTR-TP-STAT-TIT
     * 
     */
    String getTpStatTit();

    void setTpStatTit(String tpStatTit);

    /**
     * Host Variable DTR-IMP-AZ
     * 
     */
    AfDecimal getImpAz();

    void setImpAz(AfDecimal impAz);

    /**
     * Nullable property for DTR-IMP-AZ
     * 
     */
    AfDecimal getImpAzObj();

    void setImpAzObj(AfDecimal impAzObj);

    /**
     * Host Variable DTR-IMP-ADER
     * 
     */
    AfDecimal getImpAder();

    void setImpAder(AfDecimal impAder);

    /**
     * Nullable property for DTR-IMP-ADER
     * 
     */
    AfDecimal getImpAderObj();

    void setImpAderObj(AfDecimal impAderObj);

    /**
     * Host Variable DTR-IMP-TFR
     * 
     */
    AfDecimal getImpTfr();

    void setImpTfr(AfDecimal impTfr);

    /**
     * Nullable property for DTR-IMP-TFR
     * 
     */
    AfDecimal getImpTfrObj();

    void setImpTfrObj(AfDecimal impTfrObj);

    /**
     * Host Variable DTR-IMP-VOLO
     * 
     */
    AfDecimal getImpVolo();

    void setImpVolo(AfDecimal impVolo);

    /**
     * Nullable property for DTR-IMP-VOLO
     * 
     */
    AfDecimal getImpVoloObj();

    void setImpVoloObj(AfDecimal impVoloObj);

    /**
     * Nullable property for DTR-FL-VLDT-TIT
     * 
     */
    Character getDtrFlVldtTitObj();

    void setDtrFlVldtTitObj(Character dtrFlVldtTitObj);

    /**
     * Host Variable DTR-CAR-ACQ
     * 
     */
    AfDecimal getCarAcq();

    void setCarAcq(AfDecimal carAcq);

    /**
     * Nullable property for DTR-CAR-ACQ
     * 
     */
    AfDecimal getCarAcqObj();

    void setCarAcqObj(AfDecimal carAcqObj);

    /**
     * Host Variable DTR-CAR-GEST
     * 
     */
    AfDecimal getCarGest();

    void setCarGest(AfDecimal carGest);

    /**
     * Nullable property for DTR-CAR-GEST
     * 
     */
    AfDecimal getCarGestObj();

    void setCarGestObj(AfDecimal carGestObj);

    /**
     * Host Variable DTR-CAR-INC
     * 
     */
    AfDecimal getCarInc();

    void setCarInc(AfDecimal carInc);

    /**
     * Nullable property for DTR-CAR-INC
     * 
     */
    AfDecimal getCarIncObj();

    void setCarIncObj(AfDecimal carIncObj);

    /**
     * Host Variable DTR-MANFEE-ANTIC
     * 
     */
    AfDecimal getManfeeAntic();

    void setManfeeAntic(AfDecimal manfeeAntic);

    /**
     * Nullable property for DTR-MANFEE-ANTIC
     * 
     */
    AfDecimal getManfeeAnticObj();

    void setManfeeAnticObj(AfDecimal manfeeAnticObj);

    /**
     * Host Variable DTR-MANFEE-RICOR
     * 
     */
    AfDecimal getManfeeRicor();

    void setManfeeRicor(AfDecimal manfeeRicor);

    /**
     * Nullable property for DTR-MANFEE-RICOR
     * 
     */
    AfDecimal getManfeeRicorObj();

    void setManfeeRicorObj(AfDecimal manfeeRicorObj);

    /**
     * Host Variable DTR-MANFEE-REC
     * 
     */
    String getManfeeRec();

    void setManfeeRec(String manfeeRec);

    /**
     * Nullable property for DTR-MANFEE-REC
     * 
     */
    String getManfeeRecObj();

    void setManfeeRecObj(String manfeeRecObj);

    /**
     * Host Variable DTR-TOT-INTR-PREST
     * 
     */
    AfDecimal getTotIntrPrest();

    void setTotIntrPrest(AfDecimal totIntrPrest);

    /**
     * Nullable property for DTR-TOT-INTR-PREST
     * 
     */
    AfDecimal getTotIntrPrestObj();

    void setTotIntrPrestObj(AfDecimal totIntrPrestObj);

    /**
     * Host Variable DTR-DS-RIGA
     * 
     */
    long getDtrDsRiga();

    void setDtrDsRiga(long dtrDsRiga);

    /**
     * Host Variable DTR-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable DTR-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable DTR-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable DTR-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable DTR-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable DTR-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable DTR-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfe();

    void setImpTrasfe(AfDecimal impTrasfe);

    /**
     * Nullable property for DTR-IMP-TRASFE
     * 
     */
    AfDecimal getImpTrasfeObj();

    void setImpTrasfeObj(AfDecimal impTrasfeObj);

    /**
     * Host Variable DTR-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrc();

    void setImpTfrStrc(AfDecimal impTfrStrc);

    /**
     * Nullable property for DTR-IMP-TFR-STRC
     * 
     */
    AfDecimal getImpTfrStrcObj();

    void setImpTfrStrcObj(AfDecimal impTfrStrcObj);

    /**
     * Host Variable DTR-ACQ-EXP
     * 
     */
    AfDecimal getAcqExp();

    void setAcqExp(AfDecimal acqExp);

    /**
     * Nullable property for DTR-ACQ-EXP
     * 
     */
    AfDecimal getAcqExpObj();

    void setAcqExpObj(AfDecimal acqExpObj);

    /**
     * Host Variable DTR-REMUN-ASS
     * 
     */
    AfDecimal getRemunAss();

    void setRemunAss(AfDecimal remunAss);

    /**
     * Nullable property for DTR-REMUN-ASS
     * 
     */
    AfDecimal getRemunAssObj();

    void setRemunAssObj(AfDecimal remunAssObj);

    /**
     * Host Variable DTR-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInter();

    void setCommisInter(AfDecimal commisInter);

    /**
     * Nullable property for DTR-COMMIS-INTER
     * 
     */
    AfDecimal getCommisInterObj();

    void setCommisInterObj(AfDecimal commisInterObj);

    /**
     * Host Variable DTR-CNBT-ANTIRAC
     * 
     */
    AfDecimal getCnbtAntirac();

    void setCnbtAntirac(AfDecimal cnbtAntirac);

    /**
     * Nullable property for DTR-CNBT-ANTIRAC
     * 
     */
    AfDecimal getCnbtAntiracObj();

    void setCnbtAntiracObj(AfDecimal cnbtAntiracObj);
};
