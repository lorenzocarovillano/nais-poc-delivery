package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IMotDeroga;

/**
 * Data Access Object(DAO) for table [MOT_DEROGA]
 * 
 */
public class MotDerogaDao extends BaseSqlDao<IMotDeroga> {

    private Cursor cIdUpdEffMde;
    private Cursor cIdpEffMde;
    private Cursor cIdpCpzMde;
    private final IRowMapper<IMotDeroga> selectByMdeDsRigaRm = buildNamedRowMapper(IMotDeroga.class, "idMotDeroga", "idOggDeroga", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpMotDeroga", "codLivAut", "codErr", "tpErr", "descErrBreveVcharObj", "descErrEstVcharObj", "mdeDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public MotDerogaDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IMotDeroga> getToClass() {
        return IMotDeroga.class;
    }

    public IMotDeroga selectByMdeDsRiga(long mdeDsRiga, IMotDeroga iMotDeroga) {
        return buildQuery("selectByMdeDsRiga").bind("mdeDsRiga", mdeDsRiga).rowMapper(selectByMdeDsRigaRm).singleResult(iMotDeroga);
    }

    public DbAccessStatus insertRec(IMotDeroga iMotDeroga) {
        return buildQuery("insertRec").bind(iMotDeroga).executeInsert();
    }

    public DbAccessStatus updateRec(IMotDeroga iMotDeroga) {
        return buildQuery("updateRec").bind(iMotDeroga).executeUpdate();
    }

    public DbAccessStatus deleteByMdeDsRiga(long mdeDsRiga) {
        return buildQuery("deleteByMdeDsRiga").bind("mdeDsRiga", mdeDsRiga).executeDelete();
    }

    public IMotDeroga selectRec(int mdeIdMotDeroga, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IMotDeroga iMotDeroga) {
        return buildQuery("selectRec").bind("mdeIdMotDeroga", mdeIdMotDeroga).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByMdeDsRigaRm).singleResult(iMotDeroga);
    }

    public DbAccessStatus updateRec1(IMotDeroga iMotDeroga) {
        return buildQuery("updateRec1").bind(iMotDeroga).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffMde(int mdeIdMotDeroga, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffMde = buildQuery("openCIdUpdEffMde").bind("mdeIdMotDeroga", mdeIdMotDeroga).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffMde() {
        return closeCursor(cIdUpdEffMde);
    }

    public IMotDeroga fetchCIdUpdEffMde(IMotDeroga iMotDeroga) {
        return fetch(cIdUpdEffMde, iMotDeroga, selectByMdeDsRigaRm);
    }

    public IMotDeroga selectRec1(int mdeIdOggDeroga, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IMotDeroga iMotDeroga) {
        return buildQuery("selectRec1").bind("mdeIdOggDeroga", mdeIdOggDeroga).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByMdeDsRigaRm).singleResult(iMotDeroga);
    }

    public DbAccessStatus openCIdpEffMde(int mdeIdOggDeroga, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffMde = buildQuery("openCIdpEffMde").bind("mdeIdOggDeroga", mdeIdOggDeroga).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffMde() {
        return closeCursor(cIdpEffMde);
    }

    public IMotDeroga fetchCIdpEffMde(IMotDeroga iMotDeroga) {
        return fetch(cIdpEffMde, iMotDeroga, selectByMdeDsRigaRm);
    }

    public IMotDeroga selectRec2(int mdeIdMotDeroga, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IMotDeroga iMotDeroga) {
        return buildQuery("selectRec2").bind("mdeIdMotDeroga", mdeIdMotDeroga).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByMdeDsRigaRm).singleResult(iMotDeroga);
    }

    public IMotDeroga selectRec3(int mdeIdOggDeroga, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IMotDeroga iMotDeroga) {
        return buildQuery("selectRec3").bind("mdeIdOggDeroga", mdeIdOggDeroga).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByMdeDsRigaRm).singleResult(iMotDeroga);
    }

    public DbAccessStatus openCIdpCpzMde(int mdeIdOggDeroga, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzMde = buildQuery("openCIdpCpzMde").bind("mdeIdOggDeroga", mdeIdOggDeroga).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzMde() {
        return closeCursor(cIdpCpzMde);
    }

    public IMotDeroga fetchCIdpCpzMde(IMotDeroga iMotDeroga) {
        return fetch(cIdpCpzMde, iMotDeroga, selectByMdeDsRigaRm);
    }
}
