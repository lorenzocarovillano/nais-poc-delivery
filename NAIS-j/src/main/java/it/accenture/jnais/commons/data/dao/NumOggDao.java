package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.INumOgg;

/**
 * Data Access Object(DAO) for table [NUM_OGG]
 * 
 */
public class NumOggDao extends BaseSqlDao<INumOgg> {

    private Cursor cNst13;
    private final IRowMapper<INumOgg> selectRecRm = buildNamedRowMapper(INumOgg.class, "codCompagniaAnia", "formaAssicurativa", "codOggetto", "tipoOggetto", "ultProgrObj", "descOggettoVchar");
    private final IRowMapper<INumOgg> fetchCNst13Rm = buildNamedRowMapper(INumOgg.class, "ultProgrObj");

    public NumOggDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<INumOgg> getToClass() {
        return INumOgg.class;
    }

    public INumOgg selectRec(int codCompagniaAnia, String formaAssicurativa, String codOggetto, INumOgg iNumOgg) {
        return buildQuery("selectRec").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).rowMapper(selectRecRm).singleResult(iNumOgg);
    }

    public DbAccessStatus insertRec(INumOgg iNumOgg) {
        return buildQuery("insertRec").bind(iNumOgg).executeInsert();
    }

    public DbAccessStatus updateRec(INumOgg iNumOgg) {
        return buildQuery("updateRec").bind(iNumOgg).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompagniaAnia, String formaAssicurativa, String codOggetto) {
        return buildQuery("deleteRec").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).executeDelete();
    }

    public INumOgg selectRec1(int codCompagniaAnia, String formaAssicurativa, String codOggetto, String tipoOggetto, INumOgg iNumOgg) {
        return buildQuery("selectRec1").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).bind("tipoOggetto", tipoOggetto).rowMapper(selectRecRm).singleResult(iNumOgg);
    }

    public DbAccessStatus openCNst13(int codCompagniaAnia, String formaAssicurativa, String codOggetto, String tipoOggetto) {
        cNst13 = buildQuery("openCNst13").bind("codCompagniaAnia", codCompagniaAnia).bind("formaAssicurativa", formaAssicurativa).bind("codOggetto", codOggetto).bind("tipoOggetto", tipoOggetto).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst13() {
        return closeCursor(cNst13);
    }

    public INumOgg fetchCNst13(INumOgg iNumOgg) {
        return fetch(cNst13, iNumOgg, fetchCNst13Rm);
    }

    public DbAccessStatus updateRec1(INumOgg iNumOgg) {
        return buildQuery("updateRec1").bind(iNumOgg).executeUpdate();
    }
}
