package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [VAL_AST]
 * 
 */
public interface IValAst extends BaseSqlTo {

    /**
     * Host Variable LDBV4911-ID-TRCH-DI-GAR
     * 
     */
    int getLdbv4911IdTrchDiGar();

    void setLdbv4911IdTrchDiGar(int ldbv4911IdTrchDiGar);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-1
     * 
     */
    String getLdbv4911TpValAst1();

    void setLdbv4911TpValAst1(String ldbv4911TpValAst1);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-2
     * 
     */
    String getLdbv4911TpValAst2();

    void setLdbv4911TpValAst2(String ldbv4911TpValAst2);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-3
     * 
     */
    String getLdbv4911TpValAst3();

    void setLdbv4911TpValAst3(String ldbv4911TpValAst3);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-4
     * 
     */
    String getLdbv4911TpValAst4();

    void setLdbv4911TpValAst4(String ldbv4911TpValAst4);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-5
     * 
     */
    String getLdbv4911TpValAst5();

    void setLdbv4911TpValAst5(String ldbv4911TpValAst5);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-6
     * 
     */
    String getLdbv4911TpValAst6();

    void setLdbv4911TpValAst6(String ldbv4911TpValAst6);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-7
     * 
     */
    String getLdbv4911TpValAst7();

    void setLdbv4911TpValAst7(String ldbv4911TpValAst7);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-8
     * 
     */
    String getLdbv4911TpValAst8();

    void setLdbv4911TpValAst8(String ldbv4911TpValAst8);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-9
     * 
     */
    String getLdbv4911TpValAst9();

    void setLdbv4911TpValAst9(String ldbv4911TpValAst9);

    /**
     * Host Variable LDBV4911-TP-VAL-AST-10
     * 
     */
    String getLdbv4911TpValAst10();

    void setLdbv4911TpValAst10(String ldbv4911TpValAst10);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBV6001-ID-TRCH-DI-GAR
     * 
     */
    int getLdbv6001IdTrchDiGar();

    void setLdbv6001IdTrchDiGar(int ldbv6001IdTrchDiGar);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-1
     * 
     */
    String getLdbv6001TpValAst1();

    void setLdbv6001TpValAst1(String ldbv6001TpValAst1);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-2
     * 
     */
    String getLdbv6001TpValAst2();

    void setLdbv6001TpValAst2(String ldbv6001TpValAst2);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-3
     * 
     */
    String getLdbv6001TpValAst3();

    void setLdbv6001TpValAst3(String ldbv6001TpValAst3);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-4
     * 
     */
    String getLdbv6001TpValAst4();

    void setLdbv6001TpValAst4(String ldbv6001TpValAst4);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-5
     * 
     */
    String getLdbv6001TpValAst5();

    void setLdbv6001TpValAst5(String ldbv6001TpValAst5);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-6
     * 
     */
    String getLdbv6001TpValAst6();

    void setLdbv6001TpValAst6(String ldbv6001TpValAst6);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-7
     * 
     */
    String getLdbv6001TpValAst7();

    void setLdbv6001TpValAst7(String ldbv6001TpValAst7);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-8
     * 
     */
    String getLdbv6001TpValAst8();

    void setLdbv6001TpValAst8(String ldbv6001TpValAst8);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-9
     * 
     */
    String getLdbv6001TpValAst9();

    void setLdbv6001TpValAst9(String ldbv6001TpValAst9);

    /**
     * Host Variable LDBV6001-TP-VAL-AST-10
     * 
     */
    String getLdbv6001TpValAst10();

    void setLdbv6001TpValAst10(String ldbv6001TpValAst10);

    /**
     * Host Variable LDBV6001-ANN-DS-TS-CPTZ
     * 
     */
    long getLdbv6001AnnDsTsCptz();

    void setLdbv6001AnnDsTsCptz(long ldbv6001AnnDsTsCptz);

    /**
     * Host Variable VAS-ID-VAL-AST
     * 
     */
    int getIdValAst();

    void setIdValAst(int idValAst);

    /**
     * Host Variable VAS-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Nullable property for VAS-ID-TRCH-DI-GAR
     * 
     */
    Integer getIdTrchDiGarObj();

    void setIdTrchDiGarObj(Integer idTrchDiGarObj);

    /**
     * Host Variable VAS-ID-MOVI-FINRIO
     * 
     */
    int getIdMoviFinrio();

    void setIdMoviFinrio(int idMoviFinrio);

    /**
     * Host Variable VAS-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable VAS-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for VAS-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable VAS-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable VAS-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable VAS-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable VAS-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Nullable property for VAS-COD-FND
     * 
     */
    String getCodFndObj();

    void setCodFndObj(String codFndObj);

    /**
     * Host Variable VAS-NUM-QUO
     * 
     */
    AfDecimal getNumQuo();

    void setNumQuo(AfDecimal numQuo);

    /**
     * Nullable property for VAS-NUM-QUO
     * 
     */
    AfDecimal getNumQuoObj();

    void setNumQuoObj(AfDecimal numQuoObj);

    /**
     * Host Variable VAS-VAL-QUO
     * 
     */
    AfDecimal getValQuo();

    void setValQuo(AfDecimal valQuo);

    /**
     * Nullable property for VAS-VAL-QUO
     * 
     */
    AfDecimal getValQuoObj();

    void setValQuoObj(AfDecimal valQuoObj);

    /**
     * Host Variable VAS-DT-VALZZ-DB
     * 
     */
    String getDtValzzDb();

    void setDtValzzDb(String dtValzzDb);

    /**
     * Nullable property for VAS-DT-VALZZ-DB
     * 
     */
    String getDtValzzDbObj();

    void setDtValzzDbObj(String dtValzzDbObj);

    /**
     * Host Variable VAS-TP-VAL-AST
     * 
     */
    String getTpValAst();

    void setTpValAst(String tpValAst);

    /**
     * Host Variable VAS-ID-RICH-INVST-FND
     * 
     */
    int getIdRichInvstFnd();

    void setIdRichInvstFnd(int idRichInvstFnd);

    /**
     * Nullable property for VAS-ID-RICH-INVST-FND
     * 
     */
    Integer getIdRichInvstFndObj();

    void setIdRichInvstFndObj(Integer idRichInvstFndObj);

    /**
     * Host Variable VAS-ID-RICH-DIS-FND
     * 
     */
    int getIdRichDisFnd();

    void setIdRichDisFnd(int idRichDisFnd);

    /**
     * Nullable property for VAS-ID-RICH-DIS-FND
     * 
     */
    Integer getIdRichDisFndObj();

    void setIdRichDisFndObj(Integer idRichDisFndObj);

    /**
     * Host Variable VAS-DS-RIGA
     * 
     */
    long getDsRiga();

    void setDsRiga(long dsRiga);

    /**
     * Host Variable VAS-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable VAS-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable VAS-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable VAS-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable VAS-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable VAS-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable VAS-PRE-MOVTO
     * 
     */
    AfDecimal getPreMovto();

    void setPreMovto(AfDecimal preMovto);

    /**
     * Nullable property for VAS-PRE-MOVTO
     * 
     */
    AfDecimal getPreMovtoObj();

    void setPreMovtoObj(AfDecimal preMovtoObj);

    /**
     * Host Variable VAS-IMP-MOVTO
     * 
     */
    AfDecimal getImpMovto();

    void setImpMovto(AfDecimal impMovto);

    /**
     * Nullable property for VAS-IMP-MOVTO
     * 
     */
    AfDecimal getImpMovtoObj();

    void setImpMovtoObj(AfDecimal impMovtoObj);

    /**
     * Host Variable VAS-PC-INV-DIS
     * 
     */
    AfDecimal getPcInvDis();

    void setPcInvDis(AfDecimal pcInvDis);

    /**
     * Nullable property for VAS-PC-INV-DIS
     * 
     */
    AfDecimal getPcInvDisObj();

    void setPcInvDisObj(AfDecimal pcInvDisObj);

    /**
     * Host Variable VAS-NUM-QUO-LORDE
     * 
     */
    AfDecimal getNumQuoLorde();

    void setNumQuoLorde(AfDecimal numQuoLorde);

    /**
     * Nullable property for VAS-NUM-QUO-LORDE
     * 
     */
    AfDecimal getNumQuoLordeObj();

    void setNumQuoLordeObj(AfDecimal numQuoLordeObj);

    /**
     * Host Variable VAS-DT-VALZZ-CALC-DB
     * 
     */
    String getDtValzzCalcDb();

    void setDtValzzCalcDb(String dtValzzCalcDb);

    /**
     * Nullable property for VAS-DT-VALZZ-CALC-DB
     * 
     */
    String getDtValzzCalcDbObj();

    void setDtValzzCalcDbObj(String dtValzzCalcDbObj);

    /**
     * Host Variable VAS-MINUS-VALENZA
     * 
     */
    AfDecimal getMinusValenza();

    void setMinusValenza(AfDecimal minusValenza);

    /**
     * Nullable property for VAS-MINUS-VALENZA
     * 
     */
    AfDecimal getMinusValenzaObj();

    void setMinusValenzaObj(AfDecimal minusValenzaObj);

    /**
     * Host Variable VAS-PLUS-VALENZA
     * 
     */
    AfDecimal getPlusValenza();

    void setPlusValenza(AfDecimal plusValenza);

    /**
     * Nullable property for VAS-PLUS-VALENZA
     * 
     */
    AfDecimal getPlusValenzaObj();

    void setPlusValenzaObj(AfDecimal plusValenzaObj);
};
