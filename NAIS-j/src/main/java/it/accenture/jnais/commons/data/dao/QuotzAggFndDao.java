package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IQuotzAggFnd;

/**
 * Data Access Object(DAO) for table [QUOTZ_AGG_FND]
 * 
 */
public class QuotzAggFndDao extends BaseSqlDao<IQuotzAggFnd> {

    private final IRowMapper<IQuotzAggFnd> selectRecRm = buildNamedRowMapper(IQuotzAggFnd.class, "codCompAnia", "tpQtz", "codFnd", "dtQtzDb", "tpFnd", "valQuo", "descAggVcharObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public QuotzAggFndDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IQuotzAggFnd> getToClass() {
        return IQuotzAggFnd.class;
    }

    public IQuotzAggFnd selectRec(int codCompAnia, String tpQtz, String codFnd, String dtQtzDb, IQuotzAggFnd iQuotzAggFnd) {
        return buildQuery("selectRec").bind("codCompAnia", codCompAnia).bind("tpQtz", tpQtz).bind("codFnd", codFnd).bind("dtQtzDb", dtQtzDb).rowMapper(selectRecRm).singleResult(iQuotzAggFnd);
    }

    public DbAccessStatus insertRec(IQuotzAggFnd iQuotzAggFnd) {
        return buildQuery("insertRec").bind(iQuotzAggFnd).executeInsert();
    }

    public DbAccessStatus updateRec(IQuotzAggFnd iQuotzAggFnd) {
        return buildQuery("updateRec").bind(iQuotzAggFnd).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompAnia, String tpQtz, String codFnd, String dtQtzDb) {
        return buildQuery("deleteRec").bind("codCompAnia", codCompAnia).bind("tpQtz", tpQtz).bind("codFnd", codFnd).bind("dtQtzDb", dtQtzDb).executeDelete();
    }
}
