package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [DETT_TIT_CONT, TIT_CONT]
 * 
 */
public interface IDettTitCont1 extends BaseSqlTo {

    /**
     * Host Variable LDBV0371-ID-OGG
     * 
     */
    int getLdbv0371IdOgg();

    void setLdbv0371IdOgg(int ldbv0371IdOgg);

    /**
     * Host Variable LDBV0371-TP-OGG
     * 
     */
    String getLdbv0371TpOgg();

    void setLdbv0371TpOgg(String ldbv0371TpOgg);

    /**
     * Host Variable LDBV0371-TP-PRE-TIT
     * 
     */
    String getLdbv0371TpPreTit();

    void setLdbv0371TpPreTit(String ldbv0371TpPreTit);

    /**
     * Host Variable LDBV0371-TP-STAT-TIT1
     * 
     */
    String getLdbv0371TpStatTit1();

    void setLdbv0371TpStatTit1(String ldbv0371TpStatTit1);

    /**
     * Host Variable LDBV0371-TP-STAT-TIT2
     * 
     */
    String getLdbv0371TpStatTit2();

    void setLdbv0371TpStatTit2(String ldbv0371TpStatTit2);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable LDBV3101-TP-OGG
     * 
     */
    String getLdbv3101TpOgg();

    void setLdbv3101TpOgg(String ldbv3101TpOgg);

    /**
     * Host Variable LDBV3101-ID-OGG
     * 
     */
    int getLdbv3101IdOgg();

    void setLdbv3101IdOgg(int ldbv3101IdOgg);

    /**
     * Host Variable LDBV3101-TP-STA-TIT
     * 
     */
    String getLdbv3101TpStaTit();

    void setLdbv3101TpStaTit(String ldbv3101TpStaTit);

    /**
     * Host Variable LDBV3101-TP-TIT
     * 
     */
    String getLdbv3101TpTit();

    void setLdbv3101TpTit(String ldbv3101TpTit);

    /**
     * Host Variable LDBV3101-DT-A-DB
     * 
     */
    String getLdbv3101DtADb();

    void setLdbv3101DtADb(String ldbv3101DtADb);

    /**
     * Host Variable LDBV3101-DT-DA-DB
     * 
     */
    String getLdbv3101DtDaDb();

    void setLdbv3101DtDaDb(String ldbv3101DtDaDb);

    /**
     * Host Variable LDBV7141-TP-OGG
     * 
     */
    String getLdbv7141TpOgg();

    void setLdbv7141TpOgg(String ldbv7141TpOgg);

    /**
     * Host Variable LDBV7141-ID-OGG
     * 
     */
    int getLdbv7141IdOgg();

    void setLdbv7141IdOgg(int ldbv7141IdOgg);

    /**
     * Host Variable LDBV7141-TP-STA-TIT1
     * 
     */
    String getLdbv7141TpStaTit1();

    void setLdbv7141TpStaTit1(String ldbv7141TpStaTit1);

    /**
     * Host Variable LDBV7141-TP-STA-TIT2
     * 
     */
    String getLdbv7141TpStaTit2();

    void setLdbv7141TpStaTit2(String ldbv7141TpStaTit2);

    /**
     * Host Variable LDBV7141-TP-TIT
     * 
     */
    String getLdbv7141TpTit();

    void setLdbv7141TpTit(String ldbv7141TpTit);

    /**
     * Host Variable LDBV7141-DT-A-DB
     * 
     */
    String getLdbv7141DtADb();

    void setLdbv7141DtADb(String ldbv7141DtADb);

    /**
     * Host Variable LDBV7141-DT-DA-DB
     * 
     */
    String getLdbv7141DtDaDb();

    void setLdbv7141DtDaDb(String ldbv7141DtDaDb);

    /**
     * Host Variable LDBVD601-TP-OGG
     * 
     */
    String getLdbvd601TpOgg();

    void setLdbvd601TpOgg(String ldbvd601TpOgg);

    /**
     * Host Variable LDBVD601-ID-OGG
     * 
     */
    int getLdbvd601IdOgg();

    void setLdbvd601IdOgg(int ldbvd601IdOgg);

    /**
     * Host Variable LDBVD601-TP-STA-TIT
     * 
     */
    String getLdbvd601TpStaTit();

    void setLdbvd601TpStaTit(String ldbvd601TpStaTit);

    /**
     * Host Variable LDBVD601-TP-TIT
     * 
     */
    String getLdbvd601TpTit();

    void setLdbvd601TpTit(String ldbvd601TpTit);

    /**
     * Host Variable DTC-ID-DETT-TIT-CONT
     * 
     */
    int getDtcIdDettTitCont();

    void setDtcIdDettTitCont(int dtcIdDettTitCont);

    /**
     * Host Variable DTC-ID-TIT-CONT
     * 
     */
    int getDtcIdTitCont();

    void setDtcIdTitCont(int dtcIdTitCont);

    /**
     * Host Variable DTC-ID-OGG
     * 
     */
    int getDtcIdOgg();

    void setDtcIdOgg(int dtcIdOgg);

    /**
     * Host Variable DTC-TP-OGG
     * 
     */
    String getDtcTpOgg();

    void setDtcTpOgg(String dtcTpOgg);

    /**
     * Host Variable DTC-ID-MOVI-CRZ
     * 
     */
    int getDtcIdMoviCrz();

    void setDtcIdMoviCrz(int dtcIdMoviCrz);

    /**
     * Host Variable DTC-ID-MOVI-CHIU
     * 
     */
    int getDtcIdMoviChiu();

    void setDtcIdMoviChiu(int dtcIdMoviChiu);

    /**
     * Nullable property for DTC-ID-MOVI-CHIU
     * 
     */
    Integer getDtcIdMoviChiuObj();

    void setDtcIdMoviChiuObj(Integer dtcIdMoviChiuObj);

    /**
     * Host Variable DTC-DT-INI-EFF-DB
     * 
     */
    String getDtcDtIniEffDb();

    void setDtcDtIniEffDb(String dtcDtIniEffDb);

    /**
     * Host Variable DTC-DT-END-EFF-DB
     * 
     */
    String getDtcDtEndEffDb();

    void setDtcDtEndEffDb(String dtcDtEndEffDb);

    /**
     * Host Variable DTC-COD-COMP-ANIA
     * 
     */
    int getDtcCodCompAnia();

    void setDtcCodCompAnia(int dtcCodCompAnia);

    /**
     * Host Variable DTC-DT-INI-COP-DB
     * 
     */
    String getDtcDtIniCopDb();

    void setDtcDtIniCopDb(String dtcDtIniCopDb);

    /**
     * Nullable property for DTC-DT-INI-COP-DB
     * 
     */
    String getDtcDtIniCopDbObj();

    void setDtcDtIniCopDbObj(String dtcDtIniCopDbObj);

    /**
     * Host Variable DTC-DT-END-COP-DB
     * 
     */
    String getDtcDtEndCopDb();

    void setDtcDtEndCopDb(String dtcDtEndCopDb);

    /**
     * Nullable property for DTC-DT-END-COP-DB
     * 
     */
    String getDtcDtEndCopDbObj();

    void setDtcDtEndCopDbObj(String dtcDtEndCopDbObj);

    /**
     * Host Variable DTC-PRE-NET
     * 
     */
    AfDecimal getDtcPreNet();

    void setDtcPreNet(AfDecimal dtcPreNet);

    /**
     * Nullable property for DTC-PRE-NET
     * 
     */
    AfDecimal getDtcPreNetObj();

    void setDtcPreNetObj(AfDecimal dtcPreNetObj);

    /**
     * Host Variable DTC-INTR-FRAZ
     * 
     */
    AfDecimal getDtcIntrFraz();

    void setDtcIntrFraz(AfDecimal dtcIntrFraz);

    /**
     * Nullable property for DTC-INTR-FRAZ
     * 
     */
    AfDecimal getDtcIntrFrazObj();

    void setDtcIntrFrazObj(AfDecimal dtcIntrFrazObj);

    /**
     * Host Variable DTC-INTR-MORA
     * 
     */
    AfDecimal getDtcIntrMora();

    void setDtcIntrMora(AfDecimal dtcIntrMora);

    /**
     * Nullable property for DTC-INTR-MORA
     * 
     */
    AfDecimal getDtcIntrMoraObj();

    void setDtcIntrMoraObj(AfDecimal dtcIntrMoraObj);

    /**
     * Host Variable DTC-INTR-RETDT
     * 
     */
    AfDecimal getDtcIntrRetdt();

    void setDtcIntrRetdt(AfDecimal dtcIntrRetdt);

    /**
     * Nullable property for DTC-INTR-RETDT
     * 
     */
    AfDecimal getDtcIntrRetdtObj();

    void setDtcIntrRetdtObj(AfDecimal dtcIntrRetdtObj);

    /**
     * Host Variable DTC-INTR-RIAT
     * 
     */
    AfDecimal getDtcIntrRiat();

    void setDtcIntrRiat(AfDecimal dtcIntrRiat);

    /**
     * Nullable property for DTC-INTR-RIAT
     * 
     */
    AfDecimal getDtcIntrRiatObj();

    void setDtcIntrRiatObj(AfDecimal dtcIntrRiatObj);

    /**
     * Host Variable DTC-DIR
     * 
     */
    AfDecimal getDtcDir();

    void setDtcDir(AfDecimal dtcDir);

    /**
     * Nullable property for DTC-DIR
     * 
     */
    AfDecimal getDtcDirObj();

    void setDtcDirObj(AfDecimal dtcDirObj);

    /**
     * Host Variable DTC-SPE-MED
     * 
     */
    AfDecimal getDtcSpeMed();

    void setDtcSpeMed(AfDecimal dtcSpeMed);

    /**
     * Nullable property for DTC-SPE-MED
     * 
     */
    AfDecimal getDtcSpeMedObj();

    void setDtcSpeMedObj(AfDecimal dtcSpeMedObj);

    /**
     * Host Variable DTC-TAX
     * 
     */
    AfDecimal getDtcTax();

    void setDtcTax(AfDecimal dtcTax);

    /**
     * Nullable property for DTC-TAX
     * 
     */
    AfDecimal getDtcTaxObj();

    void setDtcTaxObj(AfDecimal dtcTaxObj);

    /**
     * Host Variable DTC-SOPR-SAN
     * 
     */
    AfDecimal getDtcSoprSan();

    void setDtcSoprSan(AfDecimal dtcSoprSan);

    /**
     * Nullable property for DTC-SOPR-SAN
     * 
     */
    AfDecimal getDtcSoprSanObj();

    void setDtcSoprSanObj(AfDecimal dtcSoprSanObj);

    /**
     * Host Variable DTC-SOPR-SPO
     * 
     */
    AfDecimal getDtcSoprSpo();

    void setDtcSoprSpo(AfDecimal dtcSoprSpo);

    /**
     * Nullable property for DTC-SOPR-SPO
     * 
     */
    AfDecimal getDtcSoprSpoObj();

    void setDtcSoprSpoObj(AfDecimal dtcSoprSpoObj);

    /**
     * Host Variable DTC-SOPR-TEC
     * 
     */
    AfDecimal getDtcSoprTec();

    void setDtcSoprTec(AfDecimal dtcSoprTec);

    /**
     * Nullable property for DTC-SOPR-TEC
     * 
     */
    AfDecimal getDtcSoprTecObj();

    void setDtcSoprTecObj(AfDecimal dtcSoprTecObj);

    /**
     * Host Variable DTC-SOPR-PROF
     * 
     */
    AfDecimal getDtcSoprProf();

    void setDtcSoprProf(AfDecimal dtcSoprProf);

    /**
     * Nullable property for DTC-SOPR-PROF
     * 
     */
    AfDecimal getDtcSoprProfObj();

    void setDtcSoprProfObj(AfDecimal dtcSoprProfObj);

    /**
     * Host Variable DTC-SOPR-ALT
     * 
     */
    AfDecimal getDtcSoprAlt();

    void setDtcSoprAlt(AfDecimal dtcSoprAlt);

    /**
     * Nullable property for DTC-SOPR-ALT
     * 
     */
    AfDecimal getDtcSoprAltObj();

    void setDtcSoprAltObj(AfDecimal dtcSoprAltObj);

    /**
     * Host Variable DTC-PRE-TOT
     * 
     */
    AfDecimal getDtcPreTot();

    void setDtcPreTot(AfDecimal dtcPreTot);

    /**
     * Nullable property for DTC-PRE-TOT
     * 
     */
    AfDecimal getDtcPreTotObj();

    void setDtcPreTotObj(AfDecimal dtcPreTotObj);

    /**
     * Host Variable DTC-PRE-PP-IAS
     * 
     */
    AfDecimal getDtcPrePpIas();

    void setDtcPrePpIas(AfDecimal dtcPrePpIas);

    /**
     * Nullable property for DTC-PRE-PP-IAS
     * 
     */
    AfDecimal getDtcPrePpIasObj();

    void setDtcPrePpIasObj(AfDecimal dtcPrePpIasObj);

    /**
     * Host Variable DTC-PRE-SOLO-RSH
     * 
     */
    AfDecimal getDtcPreSoloRsh();

    void setDtcPreSoloRsh(AfDecimal dtcPreSoloRsh);

    /**
     * Nullable property for DTC-PRE-SOLO-RSH
     * 
     */
    AfDecimal getDtcPreSoloRshObj();

    void setDtcPreSoloRshObj(AfDecimal dtcPreSoloRshObj);

    /**
     * Host Variable DTC-CAR-ACQ
     * 
     */
    AfDecimal getDtcCarAcq();

    void setDtcCarAcq(AfDecimal dtcCarAcq);

    /**
     * Nullable property for DTC-CAR-ACQ
     * 
     */
    AfDecimal getDtcCarAcqObj();

    void setDtcCarAcqObj(AfDecimal dtcCarAcqObj);

    /**
     * Host Variable DTC-CAR-GEST
     * 
     */
    AfDecimal getDtcCarGest();

    void setDtcCarGest(AfDecimal dtcCarGest);

    /**
     * Nullable property for DTC-CAR-GEST
     * 
     */
    AfDecimal getDtcCarGestObj();

    void setDtcCarGestObj(AfDecimal dtcCarGestObj);

    /**
     * Host Variable DTC-CAR-INC
     * 
     */
    AfDecimal getDtcCarInc();

    void setDtcCarInc(AfDecimal dtcCarInc);

    /**
     * Nullable property for DTC-CAR-INC
     * 
     */
    AfDecimal getDtcCarIncObj();

    void setDtcCarIncObj(AfDecimal dtcCarIncObj);

    /**
     * Host Variable DTC-PROV-ACQ-1AA
     * 
     */
    AfDecimal getDtcProvAcq1aa();

    void setDtcProvAcq1aa(AfDecimal dtcProvAcq1aa);

    /**
     * Nullable property for DTC-PROV-ACQ-1AA
     * 
     */
    AfDecimal getDtcProvAcq1aaObj();

    void setDtcProvAcq1aaObj(AfDecimal dtcProvAcq1aaObj);

    /**
     * Host Variable DTC-PROV-ACQ-2AA
     * 
     */
    AfDecimal getDtcProvAcq2aa();

    void setDtcProvAcq2aa(AfDecimal dtcProvAcq2aa);

    /**
     * Nullable property for DTC-PROV-ACQ-2AA
     * 
     */
    AfDecimal getDtcProvAcq2aaObj();

    void setDtcProvAcq2aaObj(AfDecimal dtcProvAcq2aaObj);

    /**
     * Host Variable DTC-PROV-RICOR
     * 
     */
    AfDecimal getDtcProvRicor();

    void setDtcProvRicor(AfDecimal dtcProvRicor);

    /**
     * Nullable property for DTC-PROV-RICOR
     * 
     */
    AfDecimal getDtcProvRicorObj();

    void setDtcProvRicorObj(AfDecimal dtcProvRicorObj);

    /**
     * Host Variable DTC-PROV-INC
     * 
     */
    AfDecimal getDtcProvInc();

    void setDtcProvInc(AfDecimal dtcProvInc);

    /**
     * Nullable property for DTC-PROV-INC
     * 
     */
    AfDecimal getDtcProvIncObj();

    void setDtcProvIncObj(AfDecimal dtcProvIncObj);

    /**
     * Host Variable DTC-PROV-DA-REC
     * 
     */
    AfDecimal getDtcProvDaRec();

    void setDtcProvDaRec(AfDecimal dtcProvDaRec);

    /**
     * Nullable property for DTC-PROV-DA-REC
     * 
     */
    AfDecimal getDtcProvDaRecObj();

    void setDtcProvDaRecObj(AfDecimal dtcProvDaRecObj);

    /**
     * Host Variable DTC-COD-DVS
     * 
     */
    String getDtcCodDvs();

    void setDtcCodDvs(String dtcCodDvs);

    /**
     * Nullable property for DTC-COD-DVS
     * 
     */
    String getDtcCodDvsObj();

    void setDtcCodDvsObj(String dtcCodDvsObj);

    /**
     * Host Variable DTC-FRQ-MOVI
     * 
     */
    int getDtcFrqMovi();

    void setDtcFrqMovi(int dtcFrqMovi);

    /**
     * Nullable property for DTC-FRQ-MOVI
     * 
     */
    Integer getDtcFrqMoviObj();

    void setDtcFrqMoviObj(Integer dtcFrqMoviObj);

    /**
     * Host Variable DTC-TP-RGM-FISC
     * 
     */
    String getDtcTpRgmFisc();

    void setDtcTpRgmFisc(String dtcTpRgmFisc);

    /**
     * Host Variable DTC-COD-TARI
     * 
     */
    String getDtcCodTari();

    void setDtcCodTari(String dtcCodTari);

    /**
     * Nullable property for DTC-COD-TARI
     * 
     */
    String getDtcCodTariObj();

    void setDtcCodTariObj(String dtcCodTariObj);

    /**
     * Host Variable DTC-TP-STAT-TIT
     * 
     */
    String getDtcTpStatTit();

    void setDtcTpStatTit(String dtcTpStatTit);

    /**
     * Host Variable DTC-IMP-AZ
     * 
     */
    AfDecimal getDtcImpAz();

    void setDtcImpAz(AfDecimal dtcImpAz);

    /**
     * Nullable property for DTC-IMP-AZ
     * 
     */
    AfDecimal getDtcImpAzObj();

    void setDtcImpAzObj(AfDecimal dtcImpAzObj);

    /**
     * Host Variable DTC-IMP-ADER
     * 
     */
    AfDecimal getDtcImpAder();

    void setDtcImpAder(AfDecimal dtcImpAder);

    /**
     * Nullable property for DTC-IMP-ADER
     * 
     */
    AfDecimal getDtcImpAderObj();

    void setDtcImpAderObj(AfDecimal dtcImpAderObj);

    /**
     * Host Variable DTC-IMP-TFR
     * 
     */
    AfDecimal getDtcImpTfr();

    void setDtcImpTfr(AfDecimal dtcImpTfr);

    /**
     * Nullable property for DTC-IMP-TFR
     * 
     */
    AfDecimal getDtcImpTfrObj();

    void setDtcImpTfrObj(AfDecimal dtcImpTfrObj);

    /**
     * Host Variable DTC-IMP-VOLO
     * 
     */
    AfDecimal getDtcImpVolo();

    void setDtcImpVolo(AfDecimal dtcImpVolo);

    /**
     * Nullable property for DTC-IMP-VOLO
     * 
     */
    AfDecimal getDtcImpVoloObj();

    void setDtcImpVoloObj(AfDecimal dtcImpVoloObj);

    /**
     * Host Variable DTC-MANFEE-ANTIC
     * 
     */
    AfDecimal getDtcManfeeAntic();

    void setDtcManfeeAntic(AfDecimal dtcManfeeAntic);

    /**
     * Nullable property for DTC-MANFEE-ANTIC
     * 
     */
    AfDecimal getDtcManfeeAnticObj();

    void setDtcManfeeAnticObj(AfDecimal dtcManfeeAnticObj);

    /**
     * Host Variable DTC-MANFEE-RICOR
     * 
     */
    AfDecimal getDtcManfeeRicor();

    void setDtcManfeeRicor(AfDecimal dtcManfeeRicor);

    /**
     * Nullable property for DTC-MANFEE-RICOR
     * 
     */
    AfDecimal getDtcManfeeRicorObj();

    void setDtcManfeeRicorObj(AfDecimal dtcManfeeRicorObj);

    /**
     * Host Variable DTC-MANFEE-REC
     * 
     */
    AfDecimal getDtcManfeeRec();

    void setDtcManfeeRec(AfDecimal dtcManfeeRec);

    /**
     * Nullable property for DTC-MANFEE-REC
     * 
     */
    AfDecimal getDtcManfeeRecObj();

    void setDtcManfeeRecObj(AfDecimal dtcManfeeRecObj);

    /**
     * Host Variable DTC-DT-ESI-TIT-DB
     * 
     */
    String getDtcDtEsiTitDb();

    void setDtcDtEsiTitDb(String dtcDtEsiTitDb);

    /**
     * Nullable property for DTC-DT-ESI-TIT-DB
     * 
     */
    String getDtcDtEsiTitDbObj();

    void setDtcDtEsiTitDbObj(String dtcDtEsiTitDbObj);

    /**
     * Host Variable DTC-SPE-AGE
     * 
     */
    AfDecimal getDtcSpeAge();

    void setDtcSpeAge(AfDecimal dtcSpeAge);

    /**
     * Nullable property for DTC-SPE-AGE
     * 
     */
    AfDecimal getDtcSpeAgeObj();

    void setDtcSpeAgeObj(AfDecimal dtcSpeAgeObj);

    /**
     * Host Variable DTC-CAR-IAS
     * 
     */
    AfDecimal getDtcCarIas();

    void setDtcCarIas(AfDecimal dtcCarIas);

    /**
     * Nullable property for DTC-CAR-IAS
     * 
     */
    AfDecimal getDtcCarIasObj();

    void setDtcCarIasObj(AfDecimal dtcCarIasObj);

    /**
     * Host Variable DTC-TOT-INTR-PREST
     * 
     */
    AfDecimal getDtcTotIntrPrest();

    void setDtcTotIntrPrest(AfDecimal dtcTotIntrPrest);

    /**
     * Nullable property for DTC-TOT-INTR-PREST
     * 
     */
    AfDecimal getDtcTotIntrPrestObj();

    void setDtcTotIntrPrestObj(AfDecimal dtcTotIntrPrestObj);

    /**
     * Host Variable DTC-DS-RIGA
     * 
     */
    long getDtcDsRiga();

    void setDtcDsRiga(long dtcDsRiga);

    /**
     * Host Variable DTC-DS-OPER-SQL
     * 
     */
    char getDtcDsOperSql();

    void setDtcDsOperSql(char dtcDsOperSql);

    /**
     * Host Variable DTC-DS-VER
     * 
     */
    int getDtcDsVer();

    void setDtcDsVer(int dtcDsVer);

    /**
     * Host Variable DTC-DS-TS-INI-CPTZ
     * 
     */
    long getDtcDsTsIniCptz();

    void setDtcDsTsIniCptz(long dtcDsTsIniCptz);

    /**
     * Host Variable DTC-DS-TS-END-CPTZ
     * 
     */
    long getDtcDsTsEndCptz();

    void setDtcDsTsEndCptz(long dtcDsTsEndCptz);

    /**
     * Host Variable DTC-DS-UTENTE
     * 
     */
    String getDtcDsUtente();

    void setDtcDsUtente(String dtcDsUtente);

    /**
     * Host Variable DTC-DS-STATO-ELAB
     * 
     */
    char getDtcDsStatoElab();

    void setDtcDsStatoElab(char dtcDsStatoElab);

    /**
     * Host Variable DTC-IMP-TRASFE
     * 
     */
    AfDecimal getDtcImpTrasfe();

    void setDtcImpTrasfe(AfDecimal dtcImpTrasfe);

    /**
     * Nullable property for DTC-IMP-TRASFE
     * 
     */
    AfDecimal getDtcImpTrasfeObj();

    void setDtcImpTrasfeObj(AfDecimal dtcImpTrasfeObj);

    /**
     * Host Variable DTC-IMP-TFR-STRC
     * 
     */
    AfDecimal getDtcImpTfrStrc();

    void setDtcImpTfrStrc(AfDecimal dtcImpTfrStrc);

    /**
     * Nullable property for DTC-IMP-TFR-STRC
     * 
     */
    AfDecimal getDtcImpTfrStrcObj();

    void setDtcImpTfrStrcObj(AfDecimal dtcImpTfrStrcObj);

    /**
     * Host Variable DTC-NUM-GG-RITARDO-PAG
     * 
     */
    int getDtcNumGgRitardoPag();

    void setDtcNumGgRitardoPag(int dtcNumGgRitardoPag);

    /**
     * Nullable property for DTC-NUM-GG-RITARDO-PAG
     * 
     */
    Integer getDtcNumGgRitardoPagObj();

    void setDtcNumGgRitardoPagObj(Integer dtcNumGgRitardoPagObj);

    /**
     * Host Variable DTC-NUM-GG-RIVAL
     * 
     */
    int getDtcNumGgRival();

    void setDtcNumGgRival(int dtcNumGgRival);

    /**
     * Nullable property for DTC-NUM-GG-RIVAL
     * 
     */
    Integer getDtcNumGgRivalObj();

    void setDtcNumGgRivalObj(Integer dtcNumGgRivalObj);

    /**
     * Host Variable DTC-ACQ-EXP
     * 
     */
    AfDecimal getDtcAcqExp();

    void setDtcAcqExp(AfDecimal dtcAcqExp);

    /**
     * Nullable property for DTC-ACQ-EXP
     * 
     */
    AfDecimal getDtcAcqExpObj();

    void setDtcAcqExpObj(AfDecimal dtcAcqExpObj);

    /**
     * Host Variable DTC-REMUN-ASS
     * 
     */
    AfDecimal getDtcRemunAss();

    void setDtcRemunAss(AfDecimal dtcRemunAss);

    /**
     * Nullable property for DTC-REMUN-ASS
     * 
     */
    AfDecimal getDtcRemunAssObj();

    void setDtcRemunAssObj(AfDecimal dtcRemunAssObj);

    /**
     * Host Variable DTC-COMMIS-INTER
     * 
     */
    AfDecimal getDtcCommisInter();

    void setDtcCommisInter(AfDecimal dtcCommisInter);

    /**
     * Nullable property for DTC-COMMIS-INTER
     * 
     */
    AfDecimal getDtcCommisInterObj();

    void setDtcCommisInterObj(AfDecimal dtcCommisInterObj);

    /**
     * Host Variable DTC-CNBT-ANTIRAC
     * 
     */
    AfDecimal getDtcCnbtAntirac();

    void setDtcCnbtAntirac(AfDecimal dtcCnbtAntirac);

    /**
     * Nullable property for DTC-CNBT-ANTIRAC
     * 
     */
    AfDecimal getDtcCnbtAntiracObj();

    void setDtcCnbtAntiracObj(AfDecimal dtcCnbtAntiracObj);

    /**
     * Host Variable TIT-ID-TIT-CONT
     * 
     */
    int getTitIdTitCont();

    void setTitIdTitCont(int titIdTitCont);

    /**
     * Host Variable TIT-ID-OGG
     * 
     */
    int getTitIdOgg();

    void setTitIdOgg(int titIdOgg);

    /**
     * Host Variable TIT-TP-OGG
     * 
     */
    String getTitTpOgg();

    void setTitTpOgg(String titTpOgg);

    /**
     * Host Variable TIT-IB-RICH
     * 
     */
    String getTitIbRich();

    void setTitIbRich(String titIbRich);

    /**
     * Nullable property for TIT-IB-RICH
     * 
     */
    String getTitIbRichObj();

    void setTitIbRichObj(String titIbRichObj);

    /**
     * Host Variable TIT-ID-MOVI-CRZ
     * 
     */
    int getTitIdMoviCrz();

    void setTitIdMoviCrz(int titIdMoviCrz);

    /**
     * Host Variable TIT-ID-MOVI-CHIU
     * 
     */
    int getTitIdMoviChiu();

    void setTitIdMoviChiu(int titIdMoviChiu);

    /**
     * Nullable property for TIT-ID-MOVI-CHIU
     * 
     */
    Integer getTitIdMoviChiuObj();

    void setTitIdMoviChiuObj(Integer titIdMoviChiuObj);

    /**
     * Host Variable TIT-DT-INI-EFF-DB
     * 
     */
    String getTitDtIniEffDb();

    void setTitDtIniEffDb(String titDtIniEffDb);

    /**
     * Host Variable TIT-DT-END-EFF-DB
     * 
     */
    String getTitDtEndEffDb();

    void setTitDtEndEffDb(String titDtEndEffDb);

    /**
     * Host Variable TIT-COD-COMP-ANIA
     * 
     */
    int getTitCodCompAnia();

    void setTitCodCompAnia(int titCodCompAnia);

    /**
     * Host Variable TIT-TP-TIT
     * 
     */
    String getTitTpTit();

    void setTitTpTit(String titTpTit);

    /**
     * Host Variable TIT-PROG-TIT
     * 
     */
    int getTitProgTit();

    void setTitProgTit(int titProgTit);

    /**
     * Nullable property for TIT-PROG-TIT
     * 
     */
    Integer getTitProgTitObj();

    void setTitProgTitObj(Integer titProgTitObj);

    /**
     * Host Variable TIT-TP-PRE-TIT
     * 
     */
    String getTitTpPreTit();

    void setTitTpPreTit(String titTpPreTit);

    /**
     * Host Variable TIT-TP-STAT-TIT
     * 
     */
    String getTitTpStatTit();

    void setTitTpStatTit(String titTpStatTit);

    /**
     * Host Variable TIT-DT-INI-COP-DB
     * 
     */
    String getTitDtIniCopDb();

    void setTitDtIniCopDb(String titDtIniCopDb);

    /**
     * Nullable property for TIT-DT-INI-COP-DB
     * 
     */
    String getTitDtIniCopDbObj();

    void setTitDtIniCopDbObj(String titDtIniCopDbObj);

    /**
     * Host Variable TIT-DT-END-COP-DB
     * 
     */
    String getTitDtEndCopDb();

    void setTitDtEndCopDb(String titDtEndCopDb);

    /**
     * Nullable property for TIT-DT-END-COP-DB
     * 
     */
    String getTitDtEndCopDbObj();

    void setTitDtEndCopDbObj(String titDtEndCopDbObj);

    /**
     * Host Variable TIT-IMP-PAG
     * 
     */
    AfDecimal getTitImpPag();

    void setTitImpPag(AfDecimal titImpPag);

    /**
     * Nullable property for TIT-IMP-PAG
     * 
     */
    AfDecimal getTitImpPagObj();

    void setTitImpPagObj(AfDecimal titImpPagObj);

    /**
     * Host Variable TIT-FL-SOLL
     * 
     */
    char getTitFlSoll();

    void setTitFlSoll(char titFlSoll);

    /**
     * Nullable property for TIT-FL-SOLL
     * 
     */
    Character getTitFlSollObj();

    void setTitFlSollObj(Character titFlSollObj);

    /**
     * Host Variable TIT-FRAZ
     * 
     */
    int getTitFraz();

    void setTitFraz(int titFraz);

    /**
     * Nullable property for TIT-FRAZ
     * 
     */
    Integer getTitFrazObj();

    void setTitFrazObj(Integer titFrazObj);

    /**
     * Host Variable TIT-DT-APPLZ-MORA-DB
     * 
     */
    String getTitDtApplzMoraDb();

    void setTitDtApplzMoraDb(String titDtApplzMoraDb);

    /**
     * Nullable property for TIT-DT-APPLZ-MORA-DB
     * 
     */
    String getTitDtApplzMoraDbObj();

    void setTitDtApplzMoraDbObj(String titDtApplzMoraDbObj);

    /**
     * Host Variable TIT-FL-MORA
     * 
     */
    char getTitFlMora();

    void setTitFlMora(char titFlMora);

    /**
     * Nullable property for TIT-FL-MORA
     * 
     */
    Character getTitFlMoraObj();

    void setTitFlMoraObj(Character titFlMoraObj);

    /**
     * Host Variable TIT-ID-RAPP-RETE
     * 
     */
    int getTitIdRappRete();

    void setTitIdRappRete(int titIdRappRete);

    /**
     * Nullable property for TIT-ID-RAPP-RETE
     * 
     */
    Integer getTitIdRappReteObj();

    void setTitIdRappReteObj(Integer titIdRappReteObj);

    /**
     * Host Variable TIT-ID-RAPP-ANA
     * 
     */
    int getTitIdRappAna();

    void setTitIdRappAna(int titIdRappAna);

    /**
     * Nullable property for TIT-ID-RAPP-ANA
     * 
     */
    Integer getTitIdRappAnaObj();

    void setTitIdRappAnaObj(Integer titIdRappAnaObj);

    /**
     * Host Variable TIT-COD-DVS
     * 
     */
    String getTitCodDvs();

    void setTitCodDvs(String titCodDvs);

    /**
     * Nullable property for TIT-COD-DVS
     * 
     */
    String getTitCodDvsObj();

    void setTitCodDvsObj(String titCodDvsObj);

    /**
     * Host Variable TIT-DT-EMIS-TIT-DB
     * 
     */
    String getTitDtEmisTitDb();

    void setTitDtEmisTitDb(String titDtEmisTitDb);

    /**
     * Nullable property for TIT-DT-EMIS-TIT-DB
     * 
     */
    String getTitDtEmisTitDbObj();

    void setTitDtEmisTitDbObj(String titDtEmisTitDbObj);

    /**
     * Host Variable TIT-DT-ESI-TIT-DB
     * 
     */
    String getTitDtEsiTitDb();

    void setTitDtEsiTitDb(String titDtEsiTitDb);

    /**
     * Nullable property for TIT-DT-ESI-TIT-DB
     * 
     */
    String getTitDtEsiTitDbObj();

    void setTitDtEsiTitDbObj(String titDtEsiTitDbObj);

    /**
     * Host Variable TIT-TOT-PRE-NET
     * 
     */
    AfDecimal getTitTotPreNet();

    void setTitTotPreNet(AfDecimal titTotPreNet);

    /**
     * Nullable property for TIT-TOT-PRE-NET
     * 
     */
    AfDecimal getTitTotPreNetObj();

    void setTitTotPreNetObj(AfDecimal titTotPreNetObj);

    /**
     * Host Variable TIT-TOT-INTR-FRAZ
     * 
     */
    AfDecimal getTitTotIntrFraz();

    void setTitTotIntrFraz(AfDecimal titTotIntrFraz);

    /**
     * Nullable property for TIT-TOT-INTR-FRAZ
     * 
     */
    AfDecimal getTitTotIntrFrazObj();

    void setTitTotIntrFrazObj(AfDecimal titTotIntrFrazObj);

    /**
     * Host Variable TIT-TOT-INTR-MORA
     * 
     */
    AfDecimal getTitTotIntrMora();

    void setTitTotIntrMora(AfDecimal titTotIntrMora);

    /**
     * Nullable property for TIT-TOT-INTR-MORA
     * 
     */
    AfDecimal getTitTotIntrMoraObj();

    void setTitTotIntrMoraObj(AfDecimal titTotIntrMoraObj);

    /**
     * Host Variable TIT-TOT-INTR-PREST
     * 
     */
    AfDecimal getTitTotIntrPrest();

    void setTitTotIntrPrest(AfDecimal titTotIntrPrest);

    /**
     * Nullable property for TIT-TOT-INTR-PREST
     * 
     */
    AfDecimal getTitTotIntrPrestObj();

    void setTitTotIntrPrestObj(AfDecimal titTotIntrPrestObj);

    /**
     * Host Variable TIT-TOT-INTR-RETDT
     * 
     */
    AfDecimal getTitTotIntrRetdt();

    void setTitTotIntrRetdt(AfDecimal titTotIntrRetdt);

    /**
     * Nullable property for TIT-TOT-INTR-RETDT
     * 
     */
    AfDecimal getTitTotIntrRetdtObj();

    void setTitTotIntrRetdtObj(AfDecimal titTotIntrRetdtObj);

    /**
     * Host Variable TIT-TOT-INTR-RIAT
     * 
     */
    AfDecimal getTitTotIntrRiat();

    void setTitTotIntrRiat(AfDecimal titTotIntrRiat);

    /**
     * Nullable property for TIT-TOT-INTR-RIAT
     * 
     */
    AfDecimal getTitTotIntrRiatObj();

    void setTitTotIntrRiatObj(AfDecimal titTotIntrRiatObj);

    /**
     * Host Variable TIT-TOT-DIR
     * 
     */
    AfDecimal getTitTotDir();

    void setTitTotDir(AfDecimal titTotDir);

    /**
     * Nullable property for TIT-TOT-DIR
     * 
     */
    AfDecimal getTitTotDirObj();

    void setTitTotDirObj(AfDecimal titTotDirObj);

    /**
     * Host Variable TIT-TOT-SPE-MED
     * 
     */
    AfDecimal getTitTotSpeMed();

    void setTitTotSpeMed(AfDecimal titTotSpeMed);

    /**
     * Nullable property for TIT-TOT-SPE-MED
     * 
     */
    AfDecimal getTitTotSpeMedObj();

    void setTitTotSpeMedObj(AfDecimal titTotSpeMedObj);

    /**
     * Host Variable TIT-TOT-TAX
     * 
     */
    AfDecimal getTitTotTax();

    void setTitTotTax(AfDecimal titTotTax);

    /**
     * Nullable property for TIT-TOT-TAX
     * 
     */
    AfDecimal getTitTotTaxObj();

    void setTitTotTaxObj(AfDecimal titTotTaxObj);

    /**
     * Host Variable TIT-TOT-SOPR-SAN
     * 
     */
    AfDecimal getTitTotSoprSan();

    void setTitTotSoprSan(AfDecimal titTotSoprSan);

    /**
     * Nullable property for TIT-TOT-SOPR-SAN
     * 
     */
    AfDecimal getTitTotSoprSanObj();

    void setTitTotSoprSanObj(AfDecimal titTotSoprSanObj);

    /**
     * Host Variable TIT-TOT-SOPR-TEC
     * 
     */
    AfDecimal getTitTotSoprTec();

    void setTitTotSoprTec(AfDecimal titTotSoprTec);

    /**
     * Nullable property for TIT-TOT-SOPR-TEC
     * 
     */
    AfDecimal getTitTotSoprTecObj();

    void setTitTotSoprTecObj(AfDecimal titTotSoprTecObj);

    /**
     * Host Variable TIT-TOT-SOPR-SPO
     * 
     */
    AfDecimal getTitTotSoprSpo();

    void setTitTotSoprSpo(AfDecimal titTotSoprSpo);

    /**
     * Nullable property for TIT-TOT-SOPR-SPO
     * 
     */
    AfDecimal getTitTotSoprSpoObj();

    void setTitTotSoprSpoObj(AfDecimal titTotSoprSpoObj);

    /**
     * Host Variable TIT-TOT-SOPR-PROF
     * 
     */
    AfDecimal getTitTotSoprProf();

    void setTitTotSoprProf(AfDecimal titTotSoprProf);

    /**
     * Nullable property for TIT-TOT-SOPR-PROF
     * 
     */
    AfDecimal getTitTotSoprProfObj();

    void setTitTotSoprProfObj(AfDecimal titTotSoprProfObj);

    /**
     * Host Variable TIT-TOT-SOPR-ALT
     * 
     */
    AfDecimal getTitTotSoprAlt();

    void setTitTotSoprAlt(AfDecimal titTotSoprAlt);

    /**
     * Nullable property for TIT-TOT-SOPR-ALT
     * 
     */
    AfDecimal getTitTotSoprAltObj();

    void setTitTotSoprAltObj(AfDecimal titTotSoprAltObj);

    /**
     * Host Variable TIT-TOT-PRE-TOT
     * 
     */
    AfDecimal getTitTotPreTot();

    void setTitTotPreTot(AfDecimal titTotPreTot);

    /**
     * Nullable property for TIT-TOT-PRE-TOT
     * 
     */
    AfDecimal getTitTotPreTotObj();

    void setTitTotPreTotObj(AfDecimal titTotPreTotObj);

    /**
     * Host Variable TIT-TOT-PRE-PP-IAS
     * 
     */
    AfDecimal getTitTotPrePpIas();

    void setTitTotPrePpIas(AfDecimal titTotPrePpIas);

    /**
     * Nullable property for TIT-TOT-PRE-PP-IAS
     * 
     */
    AfDecimal getTitTotPrePpIasObj();

    void setTitTotPrePpIasObj(AfDecimal titTotPrePpIasObj);

    /**
     * Host Variable TIT-TOT-CAR-ACQ
     * 
     */
    AfDecimal getTitTotCarAcq();

    void setTitTotCarAcq(AfDecimal titTotCarAcq);

    /**
     * Nullable property for TIT-TOT-CAR-ACQ
     * 
     */
    AfDecimal getTitTotCarAcqObj();

    void setTitTotCarAcqObj(AfDecimal titTotCarAcqObj);

    /**
     * Host Variable TIT-TOT-CAR-GEST
     * 
     */
    AfDecimal getTitTotCarGest();

    void setTitTotCarGest(AfDecimal titTotCarGest);

    /**
     * Nullable property for TIT-TOT-CAR-GEST
     * 
     */
    AfDecimal getTitTotCarGestObj();

    void setTitTotCarGestObj(AfDecimal titTotCarGestObj);

    /**
     * Host Variable TIT-TOT-CAR-INC
     * 
     */
    AfDecimal getTitTotCarInc();

    void setTitTotCarInc(AfDecimal titTotCarInc);

    /**
     * Nullable property for TIT-TOT-CAR-INC
     * 
     */
    AfDecimal getTitTotCarIncObj();

    void setTitTotCarIncObj(AfDecimal titTotCarIncObj);

    /**
     * Host Variable TIT-TOT-PRE-SOLO-RSH
     * 
     */
    AfDecimal getTitTotPreSoloRsh();

    void setTitTotPreSoloRsh(AfDecimal titTotPreSoloRsh);

    /**
     * Nullable property for TIT-TOT-PRE-SOLO-RSH
     * 
     */
    AfDecimal getTitTotPreSoloRshObj();

    void setTitTotPreSoloRshObj(AfDecimal titTotPreSoloRshObj);

    /**
     * Host Variable TIT-TOT-PROV-ACQ-1AA
     * 
     */
    AfDecimal getTitTotProvAcq1aa();

    void setTitTotProvAcq1aa(AfDecimal titTotProvAcq1aa);

    /**
     * Nullable property for TIT-TOT-PROV-ACQ-1AA
     * 
     */
    AfDecimal getTitTotProvAcq1aaObj();

    void setTitTotProvAcq1aaObj(AfDecimal titTotProvAcq1aaObj);

    /**
     * Host Variable TIT-TOT-PROV-ACQ-2AA
     * 
     */
    AfDecimal getTitTotProvAcq2aa();

    void setTitTotProvAcq2aa(AfDecimal titTotProvAcq2aa);

    /**
     * Nullable property for TIT-TOT-PROV-ACQ-2AA
     * 
     */
    AfDecimal getTitTotProvAcq2aaObj();

    void setTitTotProvAcq2aaObj(AfDecimal titTotProvAcq2aaObj);

    /**
     * Host Variable TIT-TOT-PROV-RICOR
     * 
     */
    AfDecimal getTitTotProvRicor();

    void setTitTotProvRicor(AfDecimal titTotProvRicor);

    /**
     * Nullable property for TIT-TOT-PROV-RICOR
     * 
     */
    AfDecimal getTitTotProvRicorObj();

    void setTitTotProvRicorObj(AfDecimal titTotProvRicorObj);

    /**
     * Host Variable TIT-TOT-PROV-INC
     * 
     */
    AfDecimal getTitTotProvInc();

    void setTitTotProvInc(AfDecimal titTotProvInc);

    /**
     * Nullable property for TIT-TOT-PROV-INC
     * 
     */
    AfDecimal getTitTotProvIncObj();

    void setTitTotProvIncObj(AfDecimal titTotProvIncObj);

    /**
     * Host Variable TIT-TOT-PROV-DA-REC
     * 
     */
    AfDecimal getTitTotProvDaRec();

    void setTitTotProvDaRec(AfDecimal titTotProvDaRec);

    /**
     * Nullable property for TIT-TOT-PROV-DA-REC
     * 
     */
    AfDecimal getTitTotProvDaRecObj();

    void setTitTotProvDaRecObj(AfDecimal titTotProvDaRecObj);

    /**
     * Host Variable TIT-IMP-AZ
     * 
     */
    AfDecimal getTitImpAz();

    void setTitImpAz(AfDecimal titImpAz);

    /**
     * Nullable property for TIT-IMP-AZ
     * 
     */
    AfDecimal getTitImpAzObj();

    void setTitImpAzObj(AfDecimal titImpAzObj);

    /**
     * Host Variable TIT-IMP-ADER
     * 
     */
    AfDecimal getTitImpAder();

    void setTitImpAder(AfDecimal titImpAder);

    /**
     * Nullable property for TIT-IMP-ADER
     * 
     */
    AfDecimal getTitImpAderObj();

    void setTitImpAderObj(AfDecimal titImpAderObj);

    /**
     * Host Variable TIT-IMP-TFR
     * 
     */
    AfDecimal getTitImpTfr();

    void setTitImpTfr(AfDecimal titImpTfr);

    /**
     * Nullable property for TIT-IMP-TFR
     * 
     */
    AfDecimal getTitImpTfrObj();

    void setTitImpTfrObj(AfDecimal titImpTfrObj);

    /**
     * Host Variable TIT-IMP-VOLO
     * 
     */
    AfDecimal getTitImpVolo();

    void setTitImpVolo(AfDecimal titImpVolo);

    /**
     * Nullable property for TIT-IMP-VOLO
     * 
     */
    AfDecimal getTitImpVoloObj();

    void setTitImpVoloObj(AfDecimal titImpVoloObj);

    /**
     * Host Variable TIT-TOT-MANFEE-ANTIC
     * 
     */
    AfDecimal getTitTotManfeeAntic();

    void setTitTotManfeeAntic(AfDecimal titTotManfeeAntic);

    /**
     * Nullable property for TIT-TOT-MANFEE-ANTIC
     * 
     */
    AfDecimal getTitTotManfeeAnticObj();

    void setTitTotManfeeAnticObj(AfDecimal titTotManfeeAnticObj);

    /**
     * Host Variable TIT-TOT-MANFEE-RICOR
     * 
     */
    AfDecimal getTitTotManfeeRicor();

    void setTitTotManfeeRicor(AfDecimal titTotManfeeRicor);

    /**
     * Nullable property for TIT-TOT-MANFEE-RICOR
     * 
     */
    AfDecimal getTitTotManfeeRicorObj();

    void setTitTotManfeeRicorObj(AfDecimal titTotManfeeRicorObj);

    /**
     * Host Variable TIT-TOT-MANFEE-REC
     * 
     */
    AfDecimal getTitTotManfeeRec();

    void setTitTotManfeeRec(AfDecimal titTotManfeeRec);

    /**
     * Nullable property for TIT-TOT-MANFEE-REC
     * 
     */
    AfDecimal getTitTotManfeeRecObj();

    void setTitTotManfeeRecObj(AfDecimal titTotManfeeRecObj);

    /**
     * Host Variable TIT-TP-MEZ-PAG-ADD
     * 
     */
    String getTitTpMezPagAdd();

    void setTitTpMezPagAdd(String titTpMezPagAdd);

    /**
     * Nullable property for TIT-TP-MEZ-PAG-ADD
     * 
     */
    String getTitTpMezPagAddObj();

    void setTitTpMezPagAddObj(String titTpMezPagAddObj);

    /**
     * Host Variable TIT-ESTR-CNT-CORR-ADD
     * 
     */
    String getTitEstrCntCorrAdd();

    void setTitEstrCntCorrAdd(String titEstrCntCorrAdd);

    /**
     * Nullable property for TIT-ESTR-CNT-CORR-ADD
     * 
     */
    String getTitEstrCntCorrAddObj();

    void setTitEstrCntCorrAddObj(String titEstrCntCorrAddObj);

    /**
     * Host Variable TIT-DT-VLT-DB
     * 
     */
    String getTitDtVltDb();

    void setTitDtVltDb(String titDtVltDb);

    /**
     * Nullable property for TIT-DT-VLT-DB
     * 
     */
    String getTitDtVltDbObj();

    void setTitDtVltDbObj(String titDtVltDbObj);

    /**
     * Host Variable TIT-FL-FORZ-DT-VLT
     * 
     */
    char getTitFlForzDtVlt();

    void setTitFlForzDtVlt(char titFlForzDtVlt);

    /**
     * Nullable property for TIT-FL-FORZ-DT-VLT
     * 
     */
    Character getTitFlForzDtVltObj();

    void setTitFlForzDtVltObj(Character titFlForzDtVltObj);

    /**
     * Host Variable TIT-DT-CAMBIO-VLT-DB
     * 
     */
    String getTitDtCambioVltDb();

    void setTitDtCambioVltDb(String titDtCambioVltDb);

    /**
     * Nullable property for TIT-DT-CAMBIO-VLT-DB
     * 
     */
    String getTitDtCambioVltDbObj();

    void setTitDtCambioVltDbObj(String titDtCambioVltDbObj);

    /**
     * Host Variable TIT-TOT-SPE-AGE
     * 
     */
    AfDecimal getTitTotSpeAge();

    void setTitTotSpeAge(AfDecimal titTotSpeAge);

    /**
     * Nullable property for TIT-TOT-SPE-AGE
     * 
     */
    AfDecimal getTitTotSpeAgeObj();

    void setTitTotSpeAgeObj(AfDecimal titTotSpeAgeObj);

    /**
     * Host Variable TIT-TOT-CAR-IAS
     * 
     */
    AfDecimal getTitTotCarIas();

    void setTitTotCarIas(AfDecimal titTotCarIas);

    /**
     * Nullable property for TIT-TOT-CAR-IAS
     * 
     */
    AfDecimal getTitTotCarIasObj();

    void setTitTotCarIasObj(AfDecimal titTotCarIasObj);

    /**
     * Host Variable TIT-NUM-RAT-ACCORPATE
     * 
     */
    int getTitNumRatAccorpate();

    void setTitNumRatAccorpate(int titNumRatAccorpate);

    /**
     * Nullable property for TIT-NUM-RAT-ACCORPATE
     * 
     */
    Integer getTitNumRatAccorpateObj();

    void setTitNumRatAccorpateObj(Integer titNumRatAccorpateObj);

    /**
     * Host Variable TIT-DS-RIGA
     * 
     */
    long getTitDsRiga();

    void setTitDsRiga(long titDsRiga);

    /**
     * Host Variable TIT-DS-OPER-SQL
     * 
     */
    char getTitDsOperSql();

    void setTitDsOperSql(char titDsOperSql);

    /**
     * Host Variable TIT-DS-VER
     * 
     */
    int getTitDsVer();

    void setTitDsVer(int titDsVer);

    /**
     * Host Variable TIT-DS-TS-INI-CPTZ
     * 
     */
    long getTitDsTsIniCptz();

    void setTitDsTsIniCptz(long titDsTsIniCptz);

    /**
     * Host Variable TIT-DS-TS-END-CPTZ
     * 
     */
    long getTitDsTsEndCptz();

    void setTitDsTsEndCptz(long titDsTsEndCptz);

    /**
     * Host Variable TIT-DS-UTENTE
     * 
     */
    String getTitDsUtente();

    void setTitDsUtente(String titDsUtente);

    /**
     * Host Variable TIT-DS-STATO-ELAB
     * 
     */
    char getTitDsStatoElab();

    void setTitDsStatoElab(char titDsStatoElab);

    /**
     * Host Variable TIT-FL-TIT-DA-REINVST
     * 
     */
    char getTitFlTitDaReinvst();

    void setTitFlTitDaReinvst(char titFlTitDaReinvst);

    /**
     * Nullable property for TIT-FL-TIT-DA-REINVST
     * 
     */
    Character getTitFlTitDaReinvstObj();

    void setTitFlTitDaReinvstObj(Character titFlTitDaReinvstObj);

    /**
     * Host Variable TIT-DT-RICH-ADD-RID-DB
     * 
     */
    String getTitDtRichAddRidDb();

    void setTitDtRichAddRidDb(String titDtRichAddRidDb);

    /**
     * Nullable property for TIT-DT-RICH-ADD-RID-DB
     * 
     */
    String getTitDtRichAddRidDbObj();

    void setTitDtRichAddRidDbObj(String titDtRichAddRidDbObj);

    /**
     * Host Variable TIT-TP-ESI-RID
     * 
     */
    String getTitTpEsiRid();

    void setTitTpEsiRid(String titTpEsiRid);

    /**
     * Nullable property for TIT-TP-ESI-RID
     * 
     */
    String getTitTpEsiRidObj();

    void setTitTpEsiRidObj(String titTpEsiRidObj);

    /**
     * Host Variable TIT-COD-IBAN
     * 
     */
    String getTitCodIban();

    void setTitCodIban(String titCodIban);

    /**
     * Nullable property for TIT-COD-IBAN
     * 
     */
    String getTitCodIbanObj();

    void setTitCodIbanObj(String titCodIbanObj);

    /**
     * Host Variable TIT-IMP-TRASFE
     * 
     */
    AfDecimal getTitImpTrasfe();

    void setTitImpTrasfe(AfDecimal titImpTrasfe);

    /**
     * Nullable property for TIT-IMP-TRASFE
     * 
     */
    AfDecimal getTitImpTrasfeObj();

    void setTitImpTrasfeObj(AfDecimal titImpTrasfeObj);

    /**
     * Host Variable TIT-IMP-TFR-STRC
     * 
     */
    AfDecimal getTitImpTfrStrc();

    void setTitImpTfrStrc(AfDecimal titImpTfrStrc);

    /**
     * Nullable property for TIT-IMP-TFR-STRC
     * 
     */
    AfDecimal getTitImpTfrStrcObj();

    void setTitImpTfrStrcObj(AfDecimal titImpTfrStrcObj);

    /**
     * Host Variable TIT-DT-CERT-FISC-DB
     * 
     */
    String getTitDtCertFiscDb();

    void setTitDtCertFiscDb(String titDtCertFiscDb);

    /**
     * Nullable property for TIT-DT-CERT-FISC-DB
     * 
     */
    String getTitDtCertFiscDbObj();

    void setTitDtCertFiscDbObj(String titDtCertFiscDbObj);

    /**
     * Host Variable TIT-TP-CAUS-STOR
     * 
     */
    int getTitTpCausStor();

    void setTitTpCausStor(int titTpCausStor);

    /**
     * Nullable property for TIT-TP-CAUS-STOR
     * 
     */
    Integer getTitTpCausStorObj();

    void setTitTpCausStorObj(Integer titTpCausStorObj);

    /**
     * Host Variable TIT-TP-CAUS-DISP-STOR
     * 
     */
    String getTitTpCausDispStor();

    void setTitTpCausDispStor(String titTpCausDispStor);

    /**
     * Nullable property for TIT-TP-CAUS-DISP-STOR
     * 
     */
    String getTitTpCausDispStorObj();

    void setTitTpCausDispStorObj(String titTpCausDispStorObj);

    /**
     * Host Variable TIT-TP-TIT-MIGRAZ
     * 
     */
    String getTitTpTitMigraz();

    void setTitTpTitMigraz(String titTpTitMigraz);

    /**
     * Nullable property for TIT-TP-TIT-MIGRAZ
     * 
     */
    String getTitTpTitMigrazObj();

    void setTitTpTitMigrazObj(String titTpTitMigrazObj);

    /**
     * Host Variable TIT-TOT-ACQ-EXP
     * 
     */
    AfDecimal getTitTotAcqExp();

    void setTitTotAcqExp(AfDecimal titTotAcqExp);

    /**
     * Nullable property for TIT-TOT-ACQ-EXP
     * 
     */
    AfDecimal getTitTotAcqExpObj();

    void setTitTotAcqExpObj(AfDecimal titTotAcqExpObj);

    /**
     * Host Variable TIT-TOT-REMUN-ASS
     * 
     */
    AfDecimal getTitTotRemunAss();

    void setTitTotRemunAss(AfDecimal titTotRemunAss);

    /**
     * Nullable property for TIT-TOT-REMUN-ASS
     * 
     */
    AfDecimal getTitTotRemunAssObj();

    void setTitTotRemunAssObj(AfDecimal titTotRemunAssObj);

    /**
     * Host Variable TIT-TOT-COMMIS-INTER
     * 
     */
    AfDecimal getTitTotCommisInter();

    void setTitTotCommisInter(AfDecimal titTotCommisInter);

    /**
     * Nullable property for TIT-TOT-COMMIS-INTER
     * 
     */
    AfDecimal getTitTotCommisInterObj();

    void setTitTotCommisInterObj(AfDecimal titTotCommisInterObj);

    /**
     * Host Variable TIT-TP-CAUS-RIMB
     * 
     */
    String getTitTpCausRimb();

    void setTitTpCausRimb(String titTpCausRimb);

    /**
     * Nullable property for TIT-TP-CAUS-RIMB
     * 
     */
    String getTitTpCausRimbObj();

    void setTitTpCausRimbObj(String titTpCausRimbObj);

    /**
     * Host Variable TIT-TOT-CNBT-ANTIRAC
     * 
     */
    AfDecimal getTitTotCnbtAntirac();

    void setTitTotCnbtAntirac(AfDecimal titTotCnbtAntirac);

    /**
     * Nullable property for TIT-TOT-CNBT-ANTIRAC
     * 
     */
    AfDecimal getTitTotCnbtAntiracObj();

    void setTitTotCnbtAntiracObj(AfDecimal titTotCnbtAntiracObj);

    /**
     * Host Variable TIT-FL-INC-AUTOGEN
     * 
     */
    char getTitFlIncAutogen();

    void setTitFlIncAutogen(char titFlIncAutogen);

    /**
     * Nullable property for TIT-FL-INC-AUTOGEN
     * 
     */
    Character getTitFlIncAutogenObj();

    void setTitFlIncAutogenObj(Character titFlIncAutogenObj);

    /**
     * Host Variable LDBV2971-ID-OGG
     * 
     */
    int getLdbv2971IdOgg();

    void setLdbv2971IdOgg(int ldbv2971IdOgg);

    /**
     * Host Variable LDBV2971-TP-OGG
     * 
     */
    String getLdbv2971TpOgg();

    void setLdbv2971TpOgg(String ldbv2971TpOgg);
};
