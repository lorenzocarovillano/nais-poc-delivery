package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ISoprDiGar;

/**
 * Data Access Object(DAO) for table [SOPR_DI_GAR]
 * 
 */
public class SoprDiGarDao extends BaseSqlDao<ISoprDiGar> {

    private Cursor cIdUpdEffSpg;
    private Cursor cIdpEffSpg;
    private Cursor cIdpCpzSpg;
    private Cursor cEff7;
    private Cursor cCpz7;
    private final IRowMapper<ISoprDiGar> selectBySpgDsRigaRm = buildNamedRowMapper(ISoprDiGar.class, "idSoprDiGar", "spgIdGarObj", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "spgCodSoprObj", "tpDObj", "valPcObj", "valImpObj", "pcSopramObj", "flEsclSoprObj", "descEsclVcharObj", "spgDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public SoprDiGarDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ISoprDiGar> getToClass() {
        return ISoprDiGar.class;
    }

    public ISoprDiGar selectBySpgDsRiga(long spgDsRiga, ISoprDiGar iSoprDiGar) {
        return buildQuery("selectBySpgDsRiga").bind("spgDsRiga", spgDsRiga).rowMapper(selectBySpgDsRigaRm).singleResult(iSoprDiGar);
    }

    public DbAccessStatus insertRec(ISoprDiGar iSoprDiGar) {
        return buildQuery("insertRec").bind(iSoprDiGar).executeInsert();
    }

    public DbAccessStatus updateRec(ISoprDiGar iSoprDiGar) {
        return buildQuery("updateRec").bind(iSoprDiGar).executeUpdate();
    }

    public DbAccessStatus deleteBySpgDsRiga(long spgDsRiga) {
        return buildQuery("deleteBySpgDsRiga").bind("spgDsRiga", spgDsRiga).executeDelete();
    }

    public ISoprDiGar selectRec(int spgIdSoprDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ISoprDiGar iSoprDiGar) {
        return buildQuery("selectRec").bind("spgIdSoprDiGar", spgIdSoprDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectBySpgDsRigaRm).singleResult(iSoprDiGar);
    }

    public DbAccessStatus updateRec1(ISoprDiGar iSoprDiGar) {
        return buildQuery("updateRec1").bind(iSoprDiGar).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffSpg(int spgIdSoprDiGar, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffSpg = buildQuery("openCIdUpdEffSpg").bind("spgIdSoprDiGar", spgIdSoprDiGar).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffSpg() {
        return closeCursor(cIdUpdEffSpg);
    }

    public ISoprDiGar fetchCIdUpdEffSpg(ISoprDiGar iSoprDiGar) {
        return fetch(cIdUpdEffSpg, iSoprDiGar, selectBySpgDsRigaRm);
    }

    public ISoprDiGar selectRec1(int spgIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ISoprDiGar iSoprDiGar) {
        return buildQuery("selectRec1").bind("spgIdGar", spgIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectBySpgDsRigaRm).singleResult(iSoprDiGar);
    }

    public DbAccessStatus openCIdpEffSpg(int spgIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffSpg = buildQuery("openCIdpEffSpg").bind("spgIdGar", spgIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffSpg() {
        return closeCursor(cIdpEffSpg);
    }

    public ISoprDiGar fetchCIdpEffSpg(ISoprDiGar iSoprDiGar) {
        return fetch(cIdpEffSpg, iSoprDiGar, selectBySpgDsRigaRm);
    }

    public ISoprDiGar selectRec2(int spgIdSoprDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ISoprDiGar iSoprDiGar) {
        return buildQuery("selectRec2").bind("spgIdSoprDiGar", spgIdSoprDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectBySpgDsRigaRm).singleResult(iSoprDiGar);
    }

    public ISoprDiGar selectRec3(int spgIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, ISoprDiGar iSoprDiGar) {
        return buildQuery("selectRec3").bind("spgIdGar", spgIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectBySpgDsRigaRm).singleResult(iSoprDiGar);
    }

    public DbAccessStatus openCIdpCpzSpg(int spgIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzSpg = buildQuery("openCIdpCpzSpg").bind("spgIdGar", spgIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzSpg() {
        return closeCursor(cIdpCpzSpg);
    }

    public ISoprDiGar fetchCIdpCpzSpg(ISoprDiGar iSoprDiGar) {
        return fetch(cIdpCpzSpg, iSoprDiGar, selectBySpgDsRigaRm);
    }

    public ISoprDiGar selectRec4(int spgIdGar, String spgCodSopr, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, ISoprDiGar iSoprDiGar) {
        return buildQuery("selectRec4").bind("spgIdGar", spgIdGar).bind("spgCodSopr", spgCodSopr).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectBySpgDsRigaRm).singleResult(iSoprDiGar);
    }

    public DbAccessStatus openCEff7(int spgIdGar, String spgCodSopr, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff7 = buildQuery("openCEff7").bind("spgIdGar", spgIdGar).bind("spgCodSopr", spgCodSopr).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff7() {
        return closeCursor(cEff7);
    }

    public ISoprDiGar fetchCEff7(ISoprDiGar iSoprDiGar) {
        return fetch(cEff7, iSoprDiGar, selectBySpgDsRigaRm);
    }

    public ISoprDiGar selectRec5(ISoprDiGar iSoprDiGar) {
        return buildQuery("selectRec5").bind(iSoprDiGar).rowMapper(selectBySpgDsRigaRm).singleResult(iSoprDiGar);
    }

    public DbAccessStatus openCCpz7(ISoprDiGar iSoprDiGar) {
        cCpz7 = buildQuery("openCCpz7").bind(iSoprDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz7() {
        return closeCursor(cCpz7);
    }

    public ISoprDiGar fetchCCpz7(ISoprDiGar iSoprDiGar) {
        return fetch(cCpz7, iSoprDiGar, selectBySpgDsRigaRm);
    }
}
