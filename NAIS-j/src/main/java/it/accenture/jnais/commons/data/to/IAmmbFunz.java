package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [AMMB_FUNZ_FUNZ]
 * 
 */
public interface IAmmbFunz extends BaseSqlTo {

    /**
     * Host Variable L05-TP-MOVI-ESEC
     * 
     */
    int getL05TpMoviEsec();

    void setL05TpMoviEsec(int l05TpMoviEsec);

    /**
     * Host Variable L05-TP-MOVI-RIFTO
     * 
     */
    int getL05TpMoviRifto();

    void setL05TpMoviRifto(int l05TpMoviRifto);

    /**
     * Host Variable LDBV2441-GRAV-FUNZ-FUNZ-1
     * 
     */
    String getLdbv2441GravFunzFunz1();

    void setLdbv2441GravFunzFunz1(String ldbv2441GravFunzFunz1);

    /**
     * Host Variable LDBV2441-GRAV-FUNZ-FUNZ-2
     * 
     */
    String getLdbv2441GravFunzFunz2();

    void setLdbv2441GravFunzFunz2(String ldbv2441GravFunzFunz2);

    /**
     * Host Variable LDBV2441-GRAV-FUNZ-FUNZ-3
     * 
     */
    String getLdbv2441GravFunzFunz3();

    void setLdbv2441GravFunzFunz3(String ldbv2441GravFunzFunz3);

    /**
     * Host Variable LDBV2441-GRAV-FUNZ-FUNZ-4
     * 
     */
    String getLdbv2441GravFunzFunz4();

    void setLdbv2441GravFunzFunz4(String ldbv2441GravFunzFunz4);

    /**
     * Host Variable LDBV2441-GRAV-FUNZ-FUNZ-5
     * 
     */
    String getLdbv2441GravFunzFunz5();

    void setLdbv2441GravFunzFunz5(String ldbv2441GravFunzFunz5);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable LDBVG781-GRAV-FUNZ-FUNZ-1
     * 
     */
    String getLdbvg781GravFunzFunz1();

    void setLdbvg781GravFunzFunz1(String ldbvg781GravFunzFunz1);

    /**
     * Host Variable LDBVG781-GRAV-FUNZ-FUNZ-2
     * 
     */
    String getLdbvg781GravFunzFunz2();

    void setLdbvg781GravFunzFunz2(String ldbvg781GravFunzFunz2);

    /**
     * Host Variable LDBVG781-GRAV-FUNZ-FUNZ-3
     * 
     */
    String getLdbvg781GravFunzFunz3();

    void setLdbvg781GravFunzFunz3(String ldbvg781GravFunzFunz3);

    /**
     * Host Variable LDBVG781-GRAV-FUNZ-FUNZ-4
     * 
     */
    String getLdbvg781GravFunzFunz4();

    void setLdbvg781GravFunzFunz4(String ldbvg781GravFunzFunz4);

    /**
     * Host Variable LDBVG781-GRAV-FUNZ-FUNZ-5
     * 
     */
    String getLdbvg781GravFunzFunz5();

    void setLdbvg781GravFunzFunz5(String ldbvg781GravFunzFunz5);

    /**
     * Host Variable LDBVG781-FL-POLI-IFP
     * 
     */
    char getLdbvg781FlPoliIfp();

    void setLdbvg781FlPoliIfp(char ldbvg781FlPoliIfp);

    /**
     * Host Variable L05-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L05-GRAV-FUNZ-FUNZ
     * 
     */
    String getGravFunzFunz();

    void setGravFunzFunz(String gravFunzFunz);

    /**
     * Host Variable L05-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L05-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L05-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable L05-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable L05-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable L05-COD-BLOCCO
     * 
     */
    String getCodBlocco();

    void setCodBlocco(String codBlocco);

    /**
     * Nullable property for L05-COD-BLOCCO
     * 
     */
    String getCodBloccoObj();

    void setCodBloccoObj(String codBloccoObj);

    /**
     * Host Variable L05-SRVZ-VER-ANN
     * 
     */
    String getSrvzVerAnn();

    void setSrvzVerAnn(String srvzVerAnn);

    /**
     * Nullable property for L05-SRVZ-VER-ANN
     * 
     */
    String getSrvzVerAnnObj();

    void setSrvzVerAnnObj(String srvzVerAnnObj);

    /**
     * Host Variable L05-WHERE-CONDITION-VCHAR
     * 
     */
    String getWhereConditionVchar();

    void setWhereConditionVchar(String whereConditionVchar);

    /**
     * Nullable property for L05-WHERE-CONDITION-VCHAR
     * 
     */
    String getWhereConditionVcharObj();

    void setWhereConditionVcharObj(String whereConditionVcharObj);

    /**
     * Host Variable L05-FL-POLI-IFP
     * 
     */
    char getFlPoliIfp();

    void setFlPoliIfp(char flPoliIfp);

    /**
     * Nullable property for L05-FL-POLI-IFP
     * 
     */
    Character getFlPoliIfpObj();

    void setFlPoliIfpObj(Character flPoliIfpObj);
};
