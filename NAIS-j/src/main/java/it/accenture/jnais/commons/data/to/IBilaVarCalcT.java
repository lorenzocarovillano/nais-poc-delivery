package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BILA_VAR_CALC_T]
 * 
 */
public interface IBilaVarCalcT extends BaseSqlTo {

    /**
     * Host Variable B05-ID-BILA-VAR-CALC-T
     * 
     */
    int getB05IdBilaVarCalcT();

    void setB05IdBilaVarCalcT(int b05IdBilaVarCalcT);

    /**
     * Host Variable B05-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable B05-ID-BILA-TRCH-ESTR
     * 
     */
    int getIdBilaTrchEstr();

    void setIdBilaTrchEstr(int idBilaTrchEstr);

    /**
     * Host Variable B05-ID-RICH-ESTRAZ-MAS
     * 
     */
    int getIdRichEstrazMas();

    void setIdRichEstrazMas(int idRichEstrazMas);

    /**
     * Host Variable B05-ID-RICH-ESTRAZ-AGG
     * 
     */
    int getIdRichEstrazAgg();

    void setIdRichEstrazAgg(int idRichEstrazAgg);

    /**
     * Nullable property for B05-ID-RICH-ESTRAZ-AGG
     * 
     */
    Integer getIdRichEstrazAggObj();

    void setIdRichEstrazAggObj(Integer idRichEstrazAggObj);

    /**
     * Host Variable B05-DT-RIS-DB
     * 
     */
    String getDtRisDb();

    void setDtRisDb(String dtRisDb);

    /**
     * Nullable property for B05-DT-RIS-DB
     * 
     */
    String getDtRisDbObj();

    void setDtRisDbObj(String dtRisDbObj);

    /**
     * Host Variable B05-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable B05-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Host Variable B05-ID-TRCH-DI-GAR
     * 
     */
    int getIdTrchDiGar();

    void setIdTrchDiGar(int idTrchDiGar);

    /**
     * Host Variable B05-PROG-SCHEDA-VALOR
     * 
     */
    int getProgSchedaValor();

    void setProgSchedaValor(int progSchedaValor);

    /**
     * Nullable property for B05-PROG-SCHEDA-VALOR
     * 
     */
    Integer getProgSchedaValorObj();

    void setProgSchedaValorObj(Integer progSchedaValorObj);

    /**
     * Host Variable B05-DT-INI-VLDT-TARI-DB
     * 
     */
    String getDtIniVldtTariDb();

    void setDtIniVldtTariDb(String dtIniVldtTariDb);

    /**
     * Host Variable B05-TP-RGM-FISC
     * 
     */
    String getTpRgmFisc();

    void setTpRgmFisc(String tpRgmFisc);

    /**
     * Host Variable B05-DT-INI-VLDT-PROD-DB
     * 
     */
    String getDtIniVldtProdDb();

    void setDtIniVldtProdDb(String dtIniVldtProdDb);

    /**
     * Host Variable B05-DT-DECOR-TRCH-DB
     * 
     */
    String getDtDecorTrchDb();

    void setDtDecorTrchDb(String dtDecorTrchDb);

    /**
     * Host Variable B05-COD-VAR
     * 
     */
    String getCodVar();

    void setCodVar(String codVar);

    /**
     * Host Variable B05-TP-D
     * 
     */
    char getTpD();

    void setTpD(char tpD);

    /**
     * Host Variable B05-VAL-IMP
     * 
     */
    AfDecimal getValImp();

    void setValImp(AfDecimal valImp);

    /**
     * Nullable property for B05-VAL-IMP
     * 
     */
    AfDecimal getValImpObj();

    void setValImpObj(AfDecimal valImpObj);

    /**
     * Host Variable B05-VAL-PC
     * 
     */
    AfDecimal getValPc();

    void setValPc(AfDecimal valPc);

    /**
     * Nullable property for B05-VAL-PC
     * 
     */
    AfDecimal getValPcObj();

    void setValPcObj(AfDecimal valPcObj);

    /**
     * Host Variable B05-VAL-STRINGA
     * 
     */
    String getValStringa();

    void setValStringa(String valStringa);

    /**
     * Nullable property for B05-VAL-STRINGA
     * 
     */
    String getValStringaObj();

    void setValStringaObj(String valStringaObj);

    /**
     * Host Variable B05-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable B05-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable B05-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable B05-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable B05-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable B05-AREA-D-VALOR-VAR-VCHAR
     * 
     */
    String getAreaDValorVarVchar();

    void setAreaDValorVarVchar(String areaDValorVarVchar);

    /**
     * Nullable property for B05-AREA-D-VALOR-VAR-VCHAR
     * 
     */
    String getAreaDValorVarVcharObj();

    void setAreaDValorVarVcharObj(String areaDValorVarVcharObj);
};
