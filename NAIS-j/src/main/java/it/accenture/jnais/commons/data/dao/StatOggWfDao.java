package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IStatOggWf;

/**
 * Data Access Object(DAO) for table [STAT_OGG_WF]
 * 
 */
public class StatOggWfDao extends BaseSqlDao<IStatOggWf> {

    private Cursor cIdUpdEffStw;
    private Cursor cIdoEffStw;
    private Cursor cIdoCpzStw;
    private final IRowMapper<IStatOggWf> selectByStwDsRigaRm = buildNamedRowMapper(IStatOggWf.class, "idStatOggWf", "stwIdOgg", "stwTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "codPrcs", "codAttvt", "statOggWf", "flStatEndObj", "stwDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab");

    public StatOggWfDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IStatOggWf> getToClass() {
        return IStatOggWf.class;
    }

    public IStatOggWf selectByStwDsRiga(long stwDsRiga, IStatOggWf iStatOggWf) {
        return buildQuery("selectByStwDsRiga").bind("stwDsRiga", stwDsRiga).rowMapper(selectByStwDsRigaRm).singleResult(iStatOggWf);
    }

    public DbAccessStatus insertRec(IStatOggWf iStatOggWf) {
        return buildQuery("insertRec").bind(iStatOggWf).executeInsert();
    }

    public DbAccessStatus updateRec(IStatOggWf iStatOggWf) {
        return buildQuery("updateRec").bind(iStatOggWf).executeUpdate();
    }

    public DbAccessStatus deleteByStwDsRiga(long stwDsRiga) {
        return buildQuery("deleteByStwDsRiga").bind("stwDsRiga", stwDsRiga).executeDelete();
    }

    public IStatOggWf selectRec(int stwIdStatOggWf, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IStatOggWf iStatOggWf) {
        return buildQuery("selectRec").bind("stwIdStatOggWf", stwIdStatOggWf).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByStwDsRigaRm).singleResult(iStatOggWf);
    }

    public DbAccessStatus updateRec1(IStatOggWf iStatOggWf) {
        return buildQuery("updateRec1").bind(iStatOggWf).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffStw(int stwIdStatOggWf, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffStw = buildQuery("openCIdUpdEffStw").bind("stwIdStatOggWf", stwIdStatOggWf).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffStw() {
        return closeCursor(cIdUpdEffStw);
    }

    public IStatOggWf fetchCIdUpdEffStw(IStatOggWf iStatOggWf) {
        return fetch(cIdUpdEffStw, iStatOggWf, selectByStwDsRigaRm);
    }

    public IStatOggWf selectRec1(int stwIdOgg, String stwTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IStatOggWf iStatOggWf) {
        return buildQuery("selectRec1").bind("stwIdOgg", stwIdOgg).bind("stwTpOgg", stwTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByStwDsRigaRm).singleResult(iStatOggWf);
    }

    public DbAccessStatus openCIdoEffStw(int stwIdOgg, String stwTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffStw = buildQuery("openCIdoEffStw").bind("stwIdOgg", stwIdOgg).bind("stwTpOgg", stwTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffStw() {
        return closeCursor(cIdoEffStw);
    }

    public IStatOggWf fetchCIdoEffStw(IStatOggWf iStatOggWf) {
        return fetch(cIdoEffStw, iStatOggWf, selectByStwDsRigaRm);
    }

    public IStatOggWf selectRec2(int stwIdStatOggWf, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IStatOggWf iStatOggWf) {
        return buildQuery("selectRec2").bind("stwIdStatOggWf", stwIdStatOggWf).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByStwDsRigaRm).singleResult(iStatOggWf);
    }

    public IStatOggWf selectRec3(IStatOggWf iStatOggWf) {
        return buildQuery("selectRec3").bind(iStatOggWf).rowMapper(selectByStwDsRigaRm).singleResult(iStatOggWf);
    }

    public DbAccessStatus openCIdoCpzStw(IStatOggWf iStatOggWf) {
        cIdoCpzStw = buildQuery("openCIdoCpzStw").bind(iStatOggWf).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzStw() {
        return closeCursor(cIdoCpzStw);
    }

    public IStatOggWf fetchCIdoCpzStw(IStatOggWf iStatOggWf) {
        return fetch(cIdoCpzStw, iStatOggWf, selectByStwDsRigaRm);
    }
}
