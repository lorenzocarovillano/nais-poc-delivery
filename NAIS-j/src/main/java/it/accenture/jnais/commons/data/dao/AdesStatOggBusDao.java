package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAdesStatOggBus;

/**
 * Data Access Object(DAO) for tables [ADES, STAT_OGG_BUS]
 * 
 */
public class AdesStatOggBusDao extends BaseSqlDao<IAdesStatOggBus> {

    private Cursor curWc1Stb;
    private final IRowMapper<IAdesStatOggBus> selectRecRm = buildNamedRowMapper(IAdesStatOggBus.class, "adeIdPoli", "stbIdStatOggBus", "stbIdOgg", "stbTpOgg", "stbIdMoviCrz", "stbIdMoviChiuObj", "stbDtIniEffDb", "stbDtEndEffDb", "stbCodCompAnia", "stbTpStatBus", "stbTpCaus", "stbDsRiga", "stbDsOperSql", "stbDsVer", "stbDsTsIniCptz", "stbDsTsEndCptz", "stbDsUtente", "stbDsStatoElab");

    public AdesStatOggBusDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAdesStatOggBus> getToClass() {
        return IAdesStatOggBus.class;
    }

    public IAdesStatOggBus selectRec(IAdesStatOggBus iAdesStatOggBus) {
        return buildQuery("selectRec").bind(iAdesStatOggBus).rowMapper(selectRecRm).singleResult(iAdesStatOggBus);
    }

    public DbAccessStatus openCurWc1Stb(IAdesStatOggBus iAdesStatOggBus) {
        curWc1Stb = buildQuery("openCurWc1Stb").bind(iAdesStatOggBus).open();
        return dbStatus;
    }

    public DbAccessStatus closeCurWc1Stb() {
        return closeCursor(curWc1Stb);
    }

    public IAdesStatOggBus fetchCurWc1Stb(IAdesStatOggBus iAdesStatOggBus) {
        return fetch(curWc1Stb, iAdesStatOggBus, selectRecRm);
    }
}
