package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAmmbFunzBlocco;

/**
 * Data Access Object(DAO) for table [AMMB_FUNZ_BLOCCO]
 * 
 */
public class AmmbFunzBloccoDao extends BaseSqlDao<IAmmbFunzBlocco> {

    private Cursor cNst5;
    private Cursor cNst23;
    private Cursor cNst27;
    private final IRowMapper<IAmmbFunzBlocco> selectRecRm = buildNamedRowMapper(IAmmbFunzBlocco.class, "codCompAnia", "l16CodCan", "l16CodBlocco", "l16TpMovi", "gravFunzBlocco", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "l16TpMoviRiftoObj");

    public AmmbFunzBloccoDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAmmbFunzBlocco> getToClass() {
        return IAmmbFunzBlocco.class;
    }

    public IAmmbFunzBlocco selectRec(int codCompAnia, int codCan, String codBlocco, int tpMovi, IAmmbFunzBlocco iAmmbFunzBlocco) {
        return buildQuery("selectRec").bind("codCompAnia", codCompAnia).bind("codCan", codCan).bind("codBlocco", codBlocco).bind("tpMovi", tpMovi).rowMapper(selectRecRm).singleResult(iAmmbFunzBlocco);
    }

    public DbAccessStatus insertRec(IAmmbFunzBlocco iAmmbFunzBlocco) {
        return buildQuery("insertRec").bind(iAmmbFunzBlocco).executeInsert();
    }

    public DbAccessStatus updateRec(IAmmbFunzBlocco iAmmbFunzBlocco) {
        return buildQuery("updateRec").bind(iAmmbFunzBlocco).executeUpdate();
    }

    public DbAccessStatus deleteRec(int codCompAnia, int codCan, String codBlocco, int tpMovi) {
        return buildQuery("deleteRec").bind("codCompAnia", codCompAnia).bind("codCan", codCan).bind("codBlocco", codBlocco).bind("tpMovi", tpMovi).executeDelete();
    }

    public IAmmbFunzBlocco selectRec1(String l16CodBlocco, int l16TpMovi, int idsv0003CodiceCompagniaAnia, IAmmbFunzBlocco iAmmbFunzBlocco) {
        return buildQuery("selectRec1").bind("l16CodBlocco", l16CodBlocco).bind("l16TpMovi", l16TpMovi).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectRecRm).singleResult(iAmmbFunzBlocco);
    }

    public DbAccessStatus openCNst5(String l16CodBlocco, int l16TpMovi, int idsv0003CodiceCompagniaAnia) {
        cNst5 = buildQuery("openCNst5").bind("l16CodBlocco", l16CodBlocco).bind("l16TpMovi", l16TpMovi).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst5() {
        return closeCursor(cNst5);
    }

    public IAmmbFunzBlocco fetchCNst5(IAmmbFunzBlocco iAmmbFunzBlocco) {
        return fetch(cNst5, iAmmbFunzBlocco, selectRecRm);
    }

    public IAmmbFunzBlocco selectRec2(String l16CodBlocco, int l16TpMovi, int l16CodCan, int idsv0003CodiceCompagniaAnia, IAmmbFunzBlocco iAmmbFunzBlocco) {
        return buildQuery("selectRec2").bind("l16CodBlocco", l16CodBlocco).bind("l16TpMovi", l16TpMovi).bind("l16CodCan", l16CodCan).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectRecRm).singleResult(iAmmbFunzBlocco);
    }

    public DbAccessStatus openCNst23(String l16CodBlocco, int l16TpMovi, int l16CodCan, int idsv0003CodiceCompagniaAnia) {
        cNst23 = buildQuery("openCNst23").bind("l16CodBlocco", l16CodBlocco).bind("l16TpMovi", l16TpMovi).bind("l16CodCan", l16CodCan).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst23() {
        return closeCursor(cNst23);
    }

    public IAmmbFunzBlocco fetchCNst23(IAmmbFunzBlocco iAmmbFunzBlocco) {
        return fetch(cNst23, iAmmbFunzBlocco, selectRecRm);
    }

    public IAmmbFunzBlocco selectRec3(IAmmbFunzBlocco iAmmbFunzBlocco) {
        return buildQuery("selectRec3").bind(iAmmbFunzBlocco).rowMapper(selectRecRm).singleResult(iAmmbFunzBlocco);
    }

    public DbAccessStatus openCNst27(IAmmbFunzBlocco iAmmbFunzBlocco) {
        cNst27 = buildQuery("openCNst27").bind(iAmmbFunzBlocco).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst27() {
        return closeCursor(cNst27);
    }

    public IAmmbFunzBlocco fetchCNst27(IAmmbFunzBlocco iAmmbFunzBlocco) {
        return fetch(cNst27, iAmmbFunzBlocco, selectRecRm);
    }
}
