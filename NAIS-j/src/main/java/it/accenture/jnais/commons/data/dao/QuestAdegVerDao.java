package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IQuestAdegVer;

/**
 * Data Access Object(DAO) for table [QUEST_ADEG_VER]
 * 
 */
public class QuestAdegVerDao extends BaseSqlDao<IQuestAdegVer> {

    private final IRowMapper<IQuestAdegVer> selectByP56IdQuestAdegVerRm = buildNamedRowMapper(IQuestAdegVer.class, "p56IdQuestAdegVer", "codCompAnia", "idMoviCrz", "idRappAnaObj", "idPoli", "naturaOprzObj", "orgnFndObj", "codQlfcProfObj", "codNazQlfcProfObj", "codPrvQlfcProfObj", "fntReddObj", "reddFattAnnuObj", "codAtecoObj", "valutCollObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "luogoCostituzioneVcharObj", "tpMoviObj", "flRagRappObj", "flPrszTitEffObj", "tpMotRiscObj", "tpPntVndObj", "tpAdegVerObj", "tpRelaEsecObj", "tpScoFinRappObj", "flPrsz3oPagatObj", "areaGeoProvFndObj", "tpDestFndObj", "flPaeseResidAutObj", "flPaeseCitAutObj", "flPaeseNazAutObj", "codProfPrecObj", "flAutPepObj", "flImpCarPubObj", "flLisTerrSorvObj", "tpSitFinPatObj", "tpSitFinPatConObj", "impTotAffUtilObj", "impTotFinUtilObj", "impTotAffAccObj", "impTotFinAccObj", "tpFrmGiurSavObj", "regCollPoliObj", "numTelObj", "numDipObj", "tpSitFamConvObj", "codProfConObj", "flEsProcPenObj", "tpCondClienteObj", "codSaeObj", "tpOperEsteroObj", "statOperEsteroObj", "codPrvSvolAttObj", "codStatSvolAttObj", "tpSocObj", "flIndSocQuotObj", "tpSitGiurObj", "pcQuoDetTitEffObj", "tpPrflRshPepObj", "tpPepObj", "flNotPregObj", "dtIniFntReddDbObj", "fntRedd2Obj", "dtIniFntRedd2DbObj", "fntRedd3Obj", "dtIniFntRedd3DbObj", "motAssTitEffVcharObj", "finCostituzioneVcharObj", "descImpCarPubVcharObj", "descScoFinRappVcharObj", "descProcPnlVcharObj", "descNotPregVcharObj", "idAssicuratiObj", "reddConObj", "descLibMotRiscVcharObj", "tpMotAssTitEffObj", "tpRagRappObj", "codCanObj", "tpFinCostObj", "nazDestFndObj", "flAuFatcaAeoiObj", "tpCarFinGiurObj", "tpCarFinGiurAtObj", "tpCarFinGiurPaObj", "flIstituzFinObj", "tpOriFndTitEffObj", "pcEspAgPaMscObj", "flPrTrUsaObj", "flPrTrNoUsaObj", "pcRipPatAsVitaObj", "pcRipPatImObj", "pcRipPatSetImObj", "tpStatusAeoiObj", "tpStatusFatcaObj", "flRappPaMscObj", "codComunSvolAttObj", "tpDt1oConCliObj", "tpModEnRelaIntObj", "tpReddAnnuLrdObj", "tpReddConObj", "tpOperSocFidObj", "codPaEspMsc1Obj", "impPaEspMsc1Obj", "codPaEspMsc2Obj", "impPaEspMsc2Obj", "codPaEspMsc3Obj", "impPaEspMsc3Obj", "codPaEspMsc4Obj", "impPaEspMsc4Obj", "codPaEspMsc5Obj", "impPaEspMsc5Obj", "descOrgnFndVcharObj", "codAutDueDilObj", "flPrQuestFatcaObj", "flPrQuestAeoiObj", "flPrQuestOfacObj", "flPrQuestKycObj", "flPrQuestMscqObj", "tpNotPregObj", "tpProcPnlObj", "codImpCarPubObj", "oprzSospetteObj", "ultFattAnnuObj", "descPepVcharObj", "numTel2Obj", "impAfiObj", "flNewProObj", "tpMotCambioCntrObj", "descMotCambioCnVcharObj", "codSoggObj");

    public QuestAdegVerDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IQuestAdegVer> getToClass() {
        return IQuestAdegVer.class;
    }

    public IQuestAdegVer selectByP56IdQuestAdegVer(int p56IdQuestAdegVer, IQuestAdegVer iQuestAdegVer) {
        return buildQuery("selectByP56IdQuestAdegVer").bind("p56IdQuestAdegVer", p56IdQuestAdegVer).rowMapper(selectByP56IdQuestAdegVerRm).singleResult(iQuestAdegVer);
    }

    public DbAccessStatus insertRec(IQuestAdegVer iQuestAdegVer) {
        return buildQuery("insertRec").bind(iQuestAdegVer).executeInsert();
    }

    public DbAccessStatus updateRec(IQuestAdegVer iQuestAdegVer) {
        return buildQuery("updateRec").bind(iQuestAdegVer).executeUpdate();
    }

    public DbAccessStatus deleteByP56IdQuestAdegVer(int p56IdQuestAdegVer) {
        return buildQuery("deleteByP56IdQuestAdegVer").bind("p56IdQuestAdegVer", p56IdQuestAdegVer).executeDelete();
    }
}
