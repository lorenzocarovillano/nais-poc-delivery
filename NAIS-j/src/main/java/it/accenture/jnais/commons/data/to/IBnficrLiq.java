package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BNFICR_LIQ]
 * 
 */
public interface IBnficrLiq extends BaseSqlTo {

    /**
     * Host Variable BEL-ID-BNFICR-LIQ
     * 
     */
    int getIdBnficrLiq();

    void setIdBnficrLiq(int idBnficrLiq);

    /**
     * Host Variable BEL-ID-LIQ
     * 
     */
    int getIdLiq();

    void setIdLiq(int idLiq);

    /**
     * Host Variable BEL-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable BEL-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for BEL-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable BEL-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable BEL-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable BEL-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable BEL-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Nullable property for BEL-ID-RAPP-ANA
     * 
     */
    Integer getIdRappAnaObj();

    void setIdRappAnaObj(Integer idRappAnaObj);

    /**
     * Host Variable BEL-COD-BNFICR
     * 
     */
    String getCodBnficr();

    void setCodBnficr(String codBnficr);

    /**
     * Nullable property for BEL-COD-BNFICR
     * 
     */
    String getCodBnficrObj();

    void setCodBnficrObj(String codBnficrObj);

    /**
     * Host Variable BEL-DESC-BNFICR-VCHAR
     * 
     */
    String getDescBnficrVchar();

    void setDescBnficrVchar(String descBnficrVchar);

    /**
     * Nullable property for BEL-DESC-BNFICR-VCHAR
     * 
     */
    String getDescBnficrVcharObj();

    void setDescBnficrVcharObj(String descBnficrVcharObj);

    /**
     * Host Variable BEL-PC-LIQ
     * 
     */
    AfDecimal getPcLiq();

    void setPcLiq(AfDecimal pcLiq);

    /**
     * Nullable property for BEL-PC-LIQ
     * 
     */
    AfDecimal getPcLiqObj();

    void setPcLiqObj(AfDecimal pcLiqObj);

    /**
     * Host Variable BEL-ESRCN-ATTVT-IMPRS
     * 
     */
    char getEsrcnAttvtImprs();

    void setEsrcnAttvtImprs(char esrcnAttvtImprs);

    /**
     * Nullable property for BEL-ESRCN-ATTVT-IMPRS
     * 
     */
    Character getEsrcnAttvtImprsObj();

    void setEsrcnAttvtImprsObj(Character esrcnAttvtImprsObj);

    /**
     * Host Variable BEL-TP-IND-BNFICR
     * 
     */
    String getTpIndBnficr();

    void setTpIndBnficr(String tpIndBnficr);

    /**
     * Nullable property for BEL-TP-IND-BNFICR
     * 
     */
    String getTpIndBnficrObj();

    void setTpIndBnficrObj(String tpIndBnficrObj);

    /**
     * Host Variable BEL-FL-ESE
     * 
     */
    char getFlEse();

    void setFlEse(char flEse);

    /**
     * Nullable property for BEL-FL-ESE
     * 
     */
    Character getFlEseObj();

    void setFlEseObj(Character flEseObj);

    /**
     * Host Variable BEL-FL-IRREV
     * 
     */
    char getFlIrrev();

    void setFlIrrev(char flIrrev);

    /**
     * Nullable property for BEL-FL-IRREV
     * 
     */
    Character getFlIrrevObj();

    void setFlIrrevObj(Character flIrrevObj);

    /**
     * Host Variable BEL-IMP-LRD-LIQTO
     * 
     */
    AfDecimal getImpLrdLiqto();

    void setImpLrdLiqto(AfDecimal impLrdLiqto);

    /**
     * Nullable property for BEL-IMP-LRD-LIQTO
     * 
     */
    AfDecimal getImpLrdLiqtoObj();

    void setImpLrdLiqtoObj(AfDecimal impLrdLiqtoObj);

    /**
     * Host Variable BEL-IMPST-IPT
     * 
     */
    AfDecimal getImpstIpt();

    void setImpstIpt(AfDecimal impstIpt);

    /**
     * Nullable property for BEL-IMPST-IPT
     * 
     */
    AfDecimal getImpstIptObj();

    void setImpstIptObj(AfDecimal impstIptObj);

    /**
     * Host Variable BEL-IMP-NET-LIQTO
     * 
     */
    AfDecimal getImpNetLiqto();

    void setImpNetLiqto(AfDecimal impNetLiqto);

    /**
     * Nullable property for BEL-IMP-NET-LIQTO
     * 
     */
    AfDecimal getImpNetLiqtoObj();

    void setImpNetLiqtoObj(AfDecimal impNetLiqtoObj);

    /**
     * Host Variable BEL-RIT-ACC-IPT
     * 
     */
    AfDecimal getRitAccIpt();

    void setRitAccIpt(AfDecimal ritAccIpt);

    /**
     * Nullable property for BEL-RIT-ACC-IPT
     * 
     */
    AfDecimal getRitAccIptObj();

    void setRitAccIptObj(AfDecimal ritAccIptObj);

    /**
     * Host Variable BEL-RIT-VIS-IPT
     * 
     */
    AfDecimal getRitVisIpt();

    void setRitVisIpt(AfDecimal ritVisIpt);

    /**
     * Nullable property for BEL-RIT-VIS-IPT
     * 
     */
    AfDecimal getRitVisIptObj();

    void setRitVisIptObj(AfDecimal ritVisIptObj);

    /**
     * Host Variable BEL-RIT-TFR-IPT
     * 
     */
    AfDecimal getRitTfrIpt();

    void setRitTfrIpt(AfDecimal ritTfrIpt);

    /**
     * Nullable property for BEL-RIT-TFR-IPT
     * 
     */
    AfDecimal getRitTfrIptObj();

    void setRitTfrIptObj(AfDecimal ritTfrIptObj);

    /**
     * Host Variable BEL-IMPST-IRPEF-IPT
     * 
     */
    AfDecimal getImpstIrpefIpt();

    void setImpstIrpefIpt(AfDecimal impstIrpefIpt);

    /**
     * Nullable property for BEL-IMPST-IRPEF-IPT
     * 
     */
    AfDecimal getImpstIrpefIptObj();

    void setImpstIrpefIptObj(AfDecimal impstIrpefIptObj);

    /**
     * Host Variable BEL-IMPST-SOST-IPT
     * 
     */
    AfDecimal getImpstSostIpt();

    void setImpstSostIpt(AfDecimal impstSostIpt);

    /**
     * Nullable property for BEL-IMPST-SOST-IPT
     * 
     */
    AfDecimal getImpstSostIptObj();

    void setImpstSostIptObj(AfDecimal impstSostIptObj);

    /**
     * Host Variable BEL-IMPST-PRVR-IPT
     * 
     */
    AfDecimal getImpstPrvrIpt();

    void setImpstPrvrIpt(AfDecimal impstPrvrIpt);

    /**
     * Nullable property for BEL-IMPST-PRVR-IPT
     * 
     */
    AfDecimal getImpstPrvrIptObj();

    void setImpstPrvrIptObj(AfDecimal impstPrvrIptObj);

    /**
     * Host Variable BEL-IMPST-252-IPT
     * 
     */
    AfDecimal getImpst252Ipt();

    void setImpst252Ipt(AfDecimal impst252Ipt);

    /**
     * Nullable property for BEL-IMPST-252-IPT
     * 
     */
    AfDecimal getImpst252IptObj();

    void setImpst252IptObj(AfDecimal impst252IptObj);

    /**
     * Host Variable BEL-ID-ASSTO
     * 
     */
    int getIdAssto();

    void setIdAssto(int idAssto);

    /**
     * Nullable property for BEL-ID-ASSTO
     * 
     */
    Integer getIdAsstoObj();

    void setIdAsstoObj(Integer idAsstoObj);

    /**
     * Host Variable BEL-TAX-SEP
     * 
     */
    AfDecimal getTaxSep();

    void setTaxSep(AfDecimal taxSep);

    /**
     * Nullable property for BEL-TAX-SEP
     * 
     */
    AfDecimal getTaxSepObj();

    void setTaxSepObj(AfDecimal taxSepObj);

    /**
     * Host Variable BEL-DT-RISERVE-SOM-P-DB
     * 
     */
    String getDtRiserveSomPDb();

    void setDtRiserveSomPDb(String dtRiserveSomPDb);

    /**
     * Nullable property for BEL-DT-RISERVE-SOM-P-DB
     * 
     */
    String getDtRiserveSomPDbObj();

    void setDtRiserveSomPDbObj(String dtRiserveSomPDbObj);

    /**
     * Host Variable BEL-DT-VLT-DB
     * 
     */
    String getDtVltDb();

    void setDtVltDb(String dtVltDb);

    /**
     * Nullable property for BEL-DT-VLT-DB
     * 
     */
    String getDtVltDbObj();

    void setDtVltDbObj(String dtVltDbObj);

    /**
     * Host Variable BEL-TP-STAT-LIQ-BNFICR
     * 
     */
    String getTpStatLiqBnficr();

    void setTpStatLiqBnficr(String tpStatLiqBnficr);

    /**
     * Nullable property for BEL-TP-STAT-LIQ-BNFICR
     * 
     */
    String getTpStatLiqBnficrObj();

    void setTpStatLiqBnficrObj(String tpStatLiqBnficrObj);

    /**
     * Host Variable BEL-DS-RIGA
     * 
     */
    long getBelDsRiga();

    void setBelDsRiga(long belDsRiga);

    /**
     * Host Variable BEL-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable BEL-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable BEL-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable BEL-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable BEL-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable BEL-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable BEL-RICH-CALC-CNBT-INP
     * 
     */
    char getRichCalcCnbtInp();

    void setRichCalcCnbtInp(char richCalcCnbtInp);

    /**
     * Nullable property for BEL-RICH-CALC-CNBT-INP
     * 
     */
    Character getRichCalcCnbtInpObj();

    void setRichCalcCnbtInpObj(Character richCalcCnbtInpObj);

    /**
     * Host Variable BEL-IMP-INTR-RIT-PAG
     * 
     */
    AfDecimal getImpIntrRitPag();

    void setImpIntrRitPag(AfDecimal impIntrRitPag);

    /**
     * Nullable property for BEL-IMP-INTR-RIT-PAG
     * 
     */
    AfDecimal getImpIntrRitPagObj();

    void setImpIntrRitPagObj(AfDecimal impIntrRitPagObj);

    /**
     * Host Variable BEL-DT-ULT-DOCTO-DB
     * 
     */
    String getDtUltDoctoDb();

    void setDtUltDoctoDb(String dtUltDoctoDb);

    /**
     * Nullable property for BEL-DT-ULT-DOCTO-DB
     * 
     */
    String getDtUltDoctoDbObj();

    void setDtUltDoctoDbObj(String dtUltDoctoDbObj);

    /**
     * Host Variable BEL-DT-DORMIENZA-DB
     * 
     */
    String getDtDormienzaDb();

    void setDtDormienzaDb(String dtDormienzaDb);

    /**
     * Nullable property for BEL-DT-DORMIENZA-DB
     * 
     */
    String getDtDormienzaDbObj();

    void setDtDormienzaDbObj(String dtDormienzaDbObj);

    /**
     * Host Variable BEL-IMPST-BOLLO-TOT-V
     * 
     */
    AfDecimal getImpstBolloTotV();

    void setImpstBolloTotV(AfDecimal impstBolloTotV);

    /**
     * Nullable property for BEL-IMPST-BOLLO-TOT-V
     * 
     */
    AfDecimal getImpstBolloTotVObj();

    void setImpstBolloTotVObj(AfDecimal impstBolloTotVObj);

    /**
     * Host Variable BEL-IMPST-VIS-1382011
     * 
     */
    AfDecimal getImpstVis1382011();

    void setImpstVis1382011(AfDecimal impstVis1382011);

    /**
     * Nullable property for BEL-IMPST-VIS-1382011
     * 
     */
    AfDecimal getImpstVis1382011Obj();

    void setImpstVis1382011Obj(AfDecimal impstVis1382011Obj);

    /**
     * Host Variable BEL-IMPST-SOST-1382011
     * 
     */
    AfDecimal getImpstSost1382011();

    void setImpstSost1382011(AfDecimal impstSost1382011);

    /**
     * Nullable property for BEL-IMPST-SOST-1382011
     * 
     */
    AfDecimal getImpstSost1382011Obj();

    void setImpstSost1382011Obj(AfDecimal impstSost1382011Obj);

    /**
     * Host Variable BEL-IMPST-VIS-662014
     * 
     */
    AfDecimal getImpstVis662014();

    void setImpstVis662014(AfDecimal impstVis662014);

    /**
     * Nullable property for BEL-IMPST-VIS-662014
     * 
     */
    AfDecimal getImpstVis662014Obj();

    void setImpstVis662014Obj(AfDecimal impstVis662014Obj);

    /**
     * Host Variable BEL-IMPST-SOST-662014
     * 
     */
    AfDecimal getImpstSost662014();

    void setImpstSost662014(AfDecimal impstSost662014);

    /**
     * Nullable property for BEL-IMPST-SOST-662014
     * 
     */
    AfDecimal getImpstSost662014Obj();

    void setImpstSost662014Obj(AfDecimal impstSost662014Obj);
};
