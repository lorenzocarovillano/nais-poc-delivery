package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IRisDiTrch;

/**
 * Data Access Object(DAO) for table [RIS_DI_TRCH]
 * 
 */
public class RisDiTrchDao extends BaseSqlDao<IRisDiTrch> {

    private Cursor cIdUpdEffRst;
    private Cursor cIdpEffRst;
    private Cursor cIdpCpzRst;
    private final IRowMapper<IRisDiTrch> selectByRstDsRigaRm = buildNamedRowMapper(IRisDiTrch.class, "idRisDiTrch", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpCalcRisObj", "ultRmObj", "dtCalcDbObj", "dtElabDbObj", "risBilaObj", "risMatObj", "incrXRivalObj", "rptoPreObj", "frazPrePpObj", "risTotObj", "risSpeObj", "risAbbObj", "risBnsfdtObj", "risSoprObj", "risIntegBasTecObj", "risIntegDecrTsObj", "risGarCasoMorObj", "risZilObj", "risFaivlObj", "risCosAmmtzObj", "risSpeFaivlObj", "risPrestFaivlObj", "risComponAssvaObj", "ultCoeffRisObj", "ultCoeffAggRisObj", "risAcqObj", "risUtiObj", "risMatEffObj", "risRistorniCapObj", "risTrmBnsObj", "risBnsricObj", "rstDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "idTrchDiGar", "codFndObj", "idPoli", "idAdes", "idGar", "risMinGartoObj", "risRshDfltObj", "risMoviNonInvesObj");

    public RisDiTrchDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IRisDiTrch> getToClass() {
        return IRisDiTrch.class;
    }

    public IRisDiTrch selectByRstDsRiga(long rstDsRiga, IRisDiTrch iRisDiTrch) {
        return buildQuery("selectByRstDsRiga").bind("rstDsRiga", rstDsRiga).rowMapper(selectByRstDsRigaRm).singleResult(iRisDiTrch);
    }

    public DbAccessStatus insertRec(IRisDiTrch iRisDiTrch) {
        return buildQuery("insertRec").bind(iRisDiTrch).executeInsert();
    }

    public DbAccessStatus updateRec(IRisDiTrch iRisDiTrch) {
        return buildQuery("updateRec").bind(iRisDiTrch).executeUpdate();
    }

    public DbAccessStatus deleteByRstDsRiga(long rstDsRiga) {
        return buildQuery("deleteByRstDsRiga").bind("rstDsRiga", rstDsRiga).executeDelete();
    }

    public IRisDiTrch selectRec(int rstIdRisDiTrch, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRisDiTrch iRisDiTrch) {
        return buildQuery("selectRec").bind("rstIdRisDiTrch", rstIdRisDiTrch).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRstDsRigaRm).singleResult(iRisDiTrch);
    }

    public DbAccessStatus updateRec1(IRisDiTrch iRisDiTrch) {
        return buildQuery("updateRec1").bind(iRisDiTrch).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffRst(int rstIdRisDiTrch, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffRst = buildQuery("openCIdUpdEffRst").bind("rstIdRisDiTrch", rstIdRisDiTrch).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffRst() {
        return closeCursor(cIdUpdEffRst);
    }

    public IRisDiTrch fetchCIdUpdEffRst(IRisDiTrch iRisDiTrch) {
        return fetch(cIdUpdEffRst, iRisDiTrch, selectByRstDsRigaRm);
    }

    public IRisDiTrch selectRec1(int rstIdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IRisDiTrch iRisDiTrch) {
        return buildQuery("selectRec1").bind("rstIdTrchDiGar", rstIdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByRstDsRigaRm).singleResult(iRisDiTrch);
    }

    public DbAccessStatus openCIdpEffRst(int rstIdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffRst = buildQuery("openCIdpEffRst").bind("rstIdTrchDiGar", rstIdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffRst() {
        return closeCursor(cIdpEffRst);
    }

    public IRisDiTrch fetchCIdpEffRst(IRisDiTrch iRisDiTrch) {
        return fetch(cIdpEffRst, iRisDiTrch, selectByRstDsRigaRm);
    }

    public IRisDiTrch selectRec2(int rstIdRisDiTrch, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IRisDiTrch iRisDiTrch) {
        return buildQuery("selectRec2").bind("rstIdRisDiTrch", rstIdRisDiTrch).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByRstDsRigaRm).singleResult(iRisDiTrch);
    }

    public IRisDiTrch selectRec3(int rstIdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IRisDiTrch iRisDiTrch) {
        return buildQuery("selectRec3").bind("rstIdTrchDiGar", rstIdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByRstDsRigaRm).singleResult(iRisDiTrch);
    }

    public DbAccessStatus openCIdpCpzRst(int rstIdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzRst = buildQuery("openCIdpCpzRst").bind("rstIdTrchDiGar", rstIdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzRst() {
        return closeCursor(cIdpCpzRst);
    }

    public IRisDiTrch fetchCIdpCpzRst(IRisDiTrch iRisDiTrch) {
        return fetch(cIdpCpzRst, iRisDiTrch, selectByRstDsRigaRm);
    }
}
