package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PERS]
 * 
 */
public interface IPers extends BaseSqlTo {

    /**
     * Host Variable A25-ID-PERS
     * 
     */
    int getIdPers();

    void setIdPers(int idPers);

    /**
     * Host Variable A25-TSTAM-INI-VLDT
     * 
     */
    long getTstamIniVldt();

    void setTstamIniVldt(long tstamIniVldt);

    /**
     * Host Variable A25-TSTAM-END-VLDT
     * 
     */
    long getTstamEndVldt();

    void setTstamEndVldt(long tstamEndVldt);

    /**
     * Nullable property for A25-TSTAM-END-VLDT
     * 
     */
    Long getTstamEndVldtObj();

    void setTstamEndVldtObj(Long tstamEndVldtObj);

    /**
     * Host Variable A25-COD-PERS
     * 
     */
    long getCodPers();

    void setCodPers(long codPers);

    /**
     * Host Variable A25-RIFTO-RETE
     * 
     */
    String getRiftoRete();

    void setRiftoRete(String riftoRete);

    /**
     * Nullable property for A25-RIFTO-RETE
     * 
     */
    String getRiftoReteObj();

    void setRiftoReteObj(String riftoReteObj);

    /**
     * Host Variable A25-COD-PRT-IVA
     * 
     */
    String getCodPrtIva();

    void setCodPrtIva(String codPrtIva);

    /**
     * Nullable property for A25-COD-PRT-IVA
     * 
     */
    String getCodPrtIvaObj();

    void setCodPrtIvaObj(String codPrtIvaObj);

    /**
     * Host Variable A25-IND-PVCY-PRSNL
     * 
     */
    char getIndPvcyPrsnl();

    void setIndPvcyPrsnl(char indPvcyPrsnl);

    /**
     * Nullable property for A25-IND-PVCY-PRSNL
     * 
     */
    Character getIndPvcyPrsnlObj();

    void setIndPvcyPrsnlObj(Character indPvcyPrsnlObj);

    /**
     * Host Variable A25-IND-PVCY-CMMRC
     * 
     */
    char getIndPvcyCmmrc();

    void setIndPvcyCmmrc(char indPvcyCmmrc);

    /**
     * Nullable property for A25-IND-PVCY-CMMRC
     * 
     */
    Character getIndPvcyCmmrcObj();

    void setIndPvcyCmmrcObj(Character indPvcyCmmrcObj);

    /**
     * Host Variable A25-IND-PVCY-INDST
     * 
     */
    char getIndPvcyIndst();

    void setIndPvcyIndst(char indPvcyIndst);

    /**
     * Nullable property for A25-IND-PVCY-INDST
     * 
     */
    Character getIndPvcyIndstObj();

    void setIndPvcyIndstObj(Character indPvcyIndstObj);

    /**
     * Host Variable A25-DT-NASC-CLI-DB
     * 
     */
    String getDtNascCliDb();

    void setDtNascCliDb(String dtNascCliDb);

    /**
     * Nullable property for A25-DT-NASC-CLI-DB
     * 
     */
    String getDtNascCliDbObj();

    void setDtNascCliDbObj(String dtNascCliDbObj);

    /**
     * Host Variable A25-DT-ACQS-PERS-DB
     * 
     */
    String getDtAcqsPersDb();

    void setDtAcqsPersDb(String dtAcqsPersDb);

    /**
     * Nullable property for A25-DT-ACQS-PERS-DB
     * 
     */
    String getDtAcqsPersDbObj();

    void setDtAcqsPersDbObj(String dtAcqsPersDbObj);

    /**
     * Host Variable A25-IND-CLI
     * 
     */
    char getIndCli();

    void setIndCli(char indCli);

    /**
     * Nullable property for A25-IND-CLI
     * 
     */
    Character getIndCliObj();

    void setIndCliObj(Character indCliObj);

    /**
     * Host Variable A25-COD-CMN
     * 
     */
    String getCodCmn();

    void setCodCmn(String codCmn);

    /**
     * Nullable property for A25-COD-CMN
     * 
     */
    String getCodCmnObj();

    void setCodCmnObj(String codCmnObj);

    /**
     * Host Variable A25-COD-FRM-GIURD
     * 
     */
    String getCodFrmGiurd();

    void setCodFrmGiurd(String codFrmGiurd);

    /**
     * Nullable property for A25-COD-FRM-GIURD
     * 
     */
    String getCodFrmGiurdObj();

    void setCodFrmGiurdObj(String codFrmGiurdObj);

    /**
     * Host Variable A25-COD-ENTE-PUBB
     * 
     */
    String getCodEntePubb();

    void setCodEntePubb(String codEntePubb);

    /**
     * Nullable property for A25-COD-ENTE-PUBB
     * 
     */
    String getCodEntePubbObj();

    void setCodEntePubbObj(String codEntePubbObj);

    /**
     * Host Variable A25-DEN-RGN-SOC-VCHAR
     * 
     */
    String getDenRgnSocVchar();

    void setDenRgnSocVchar(String denRgnSocVchar);

    /**
     * Nullable property for A25-DEN-RGN-SOC-VCHAR
     * 
     */
    String getDenRgnSocVcharObj();

    void setDenRgnSocVcharObj(String denRgnSocVcharObj);

    /**
     * Host Variable A25-DEN-SIG-RGN-SOC-VCHAR
     * 
     */
    String getDenSigRgnSocVchar();

    void setDenSigRgnSocVchar(String denSigRgnSocVchar);

    /**
     * Nullable property for A25-DEN-SIG-RGN-SOC-VCHAR
     * 
     */
    String getDenSigRgnSocVcharObj();

    void setDenSigRgnSocVcharObj(String denSigRgnSocVcharObj);

    /**
     * Host Variable A25-IND-ESE-FISC
     * 
     */
    char getIndEseFisc();

    void setIndEseFisc(char indEseFisc);

    /**
     * Nullable property for A25-IND-ESE-FISC
     * 
     */
    Character getIndEseFiscObj();

    void setIndEseFiscObj(Character indEseFiscObj);

    /**
     * Host Variable A25-COD-STAT-CVL
     * 
     */
    String getCodStatCvl();

    void setCodStatCvl(String codStatCvl);

    /**
     * Nullable property for A25-COD-STAT-CVL
     * 
     */
    String getCodStatCvlObj();

    void setCodStatCvlObj(String codStatCvlObj);

    /**
     * Host Variable A25-DEN-NOME-VCHAR
     * 
     */
    String getDenNomeVchar();

    void setDenNomeVchar(String denNomeVchar);

    /**
     * Nullable property for A25-DEN-NOME-VCHAR
     * 
     */
    String getDenNomeVcharObj();

    void setDenNomeVcharObj(String denNomeVcharObj);

    /**
     * Host Variable A25-DEN-COGN-VCHAR
     * 
     */
    String getDenCognVchar();

    void setDenCognVchar(String denCognVchar);

    /**
     * Nullable property for A25-DEN-COGN-VCHAR
     * 
     */
    String getDenCognVcharObj();

    void setDenCognVcharObj(String denCognVcharObj);

    /**
     * Host Variable A25-COD-FISC
     * 
     */
    String getCodFisc();

    void setCodFisc(String codFisc);

    /**
     * Nullable property for A25-COD-FISC
     * 
     */
    String getCodFiscObj();

    void setCodFiscObj(String codFiscObj);

    /**
     * Host Variable A25-IND-SEX
     * 
     */
    char getIndSex();

    void setIndSex(char indSex);

    /**
     * Nullable property for A25-IND-SEX
     * 
     */
    Character getIndSexObj();

    void setIndSexObj(Character indSexObj);

    /**
     * Host Variable A25-IND-CPCT-GIURD
     * 
     */
    char getIndCpctGiurd();

    void setIndCpctGiurd(char indCpctGiurd);

    /**
     * Nullable property for A25-IND-CPCT-GIURD
     * 
     */
    Character getIndCpctGiurdObj();

    void setIndCpctGiurdObj(Character indCpctGiurdObj);

    /**
     * Host Variable A25-IND-PORT-HDCP
     * 
     */
    char getIndPortHdcp();

    void setIndPortHdcp(char indPortHdcp);

    /**
     * Nullable property for A25-IND-PORT-HDCP
     * 
     */
    Character getIndPortHdcpObj();

    void setIndPortHdcpObj(Character indPortHdcpObj);

    /**
     * Host Variable A25-COD-USER-INS
     * 
     */
    String getCodUserIns();

    void setCodUserIns(String codUserIns);

    /**
     * Host Variable A25-TSTAM-INS-RIGA
     * 
     */
    long getTstamInsRiga();

    void setTstamInsRiga(long tstamInsRiga);

    /**
     * Host Variable A25-COD-USER-AGGM
     * 
     */
    String getCodUserAggm();

    void setCodUserAggm(String codUserAggm);

    /**
     * Nullable property for A25-COD-USER-AGGM
     * 
     */
    String getCodUserAggmObj();

    void setCodUserAggmObj(String codUserAggmObj);

    /**
     * Host Variable A25-TSTAM-AGGM-RIGA
     * 
     */
    long getTstamAggmRiga();

    void setTstamAggmRiga(long tstamAggmRiga);

    /**
     * Nullable property for A25-TSTAM-AGGM-RIGA
     * 
     */
    Long getTstamAggmRigaObj();

    void setTstamAggmRigaObj(Long tstamAggmRigaObj);

    /**
     * Host Variable A25-DEN-CMN-NASC-STRN-VCHAR
     * 
     */
    String getDenCmnNascStrnVchar();

    void setDenCmnNascStrnVchar(String denCmnNascStrnVchar);

    /**
     * Nullable property for A25-DEN-CMN-NASC-STRN-VCHAR
     * 
     */
    String getDenCmnNascStrnVcharObj();

    void setDenCmnNascStrnVcharObj(String denCmnNascStrnVcharObj);

    /**
     * Host Variable A25-COD-RAMO-STGR
     * 
     */
    String getCodRamoStgr();

    void setCodRamoStgr(String codRamoStgr);

    /**
     * Nullable property for A25-COD-RAMO-STGR
     * 
     */
    String getCodRamoStgrObj();

    void setCodRamoStgrObj(String codRamoStgrObj);

    /**
     * Host Variable A25-COD-STGR-ATVT-UIC
     * 
     */
    String getCodStgrAtvtUic();

    void setCodStgrAtvtUic(String codStgrAtvtUic);

    /**
     * Nullable property for A25-COD-STGR-ATVT-UIC
     * 
     */
    String getCodStgrAtvtUicObj();

    void setCodStgrAtvtUicObj(String codStgrAtvtUicObj);

    /**
     * Host Variable A25-COD-RAMO-ATVT-UIC
     * 
     */
    String getCodRamoAtvtUic();

    void setCodRamoAtvtUic(String codRamoAtvtUic);

    /**
     * Nullable property for A25-COD-RAMO-ATVT-UIC
     * 
     */
    String getCodRamoAtvtUicObj();

    void setCodRamoAtvtUicObj(String codRamoAtvtUicObj);

    /**
     * Host Variable A25-DT-END-VLDT-PERS-DB
     * 
     */
    String getDtEndVldtPersDb();

    void setDtEndVldtPersDb(String dtEndVldtPersDb);

    /**
     * Nullable property for A25-DT-END-VLDT-PERS-DB
     * 
     */
    String getDtEndVldtPersDbObj();

    void setDtEndVldtPersDbObj(String dtEndVldtPersDbObj);

    /**
     * Host Variable A25-DT-DEAD-PERS-DB
     * 
     */
    String getDtDeadPersDb();

    void setDtDeadPersDb(String dtDeadPersDb);

    /**
     * Nullable property for A25-DT-DEAD-PERS-DB
     * 
     */
    String getDtDeadPersDbObj();

    void setDtDeadPersDbObj(String dtDeadPersDbObj);

    /**
     * Host Variable A25-TP-STAT-CLI
     * 
     */
    String getTpStatCli();

    void setTpStatCli(String tpStatCli);

    /**
     * Nullable property for A25-TP-STAT-CLI
     * 
     */
    String getTpStatCliObj();

    void setTpStatCliObj(String tpStatCliObj);

    /**
     * Host Variable A25-DT-BLOC-CLI-DB
     * 
     */
    String getDtBlocCliDb();

    void setDtBlocCliDb(String dtBlocCliDb);

    /**
     * Nullable property for A25-DT-BLOC-CLI-DB
     * 
     */
    String getDtBlocCliDbObj();

    void setDtBlocCliDbObj(String dtBlocCliDbObj);

    /**
     * Host Variable A25-COD-PERS-SECOND
     * 
     */
    long getCodPersSecond();

    void setCodPersSecond(long codPersSecond);

    /**
     * Nullable property for A25-COD-PERS-SECOND
     * 
     */
    Long getCodPersSecondObj();

    void setCodPersSecondObj(Long codPersSecondObj);

    /**
     * Host Variable A25-ID-SEGMENTAZ-CLI
     * 
     */
    int getIdSegmentazCli();

    void setIdSegmentazCli(int idSegmentazCli);

    /**
     * Nullable property for A25-ID-SEGMENTAZ-CLI
     * 
     */
    Integer getIdSegmentazCliObj();

    void setIdSegmentazCliObj(Integer idSegmentazCliObj);

    /**
     * Host Variable A25-DT-1A-ATVT-DB
     * 
     */
    String getDt1aAtvtDb();

    void setDt1aAtvtDb(String dt1aAtvtDb);

    /**
     * Nullable property for A25-DT-1A-ATVT-DB
     * 
     */
    String getDt1aAtvtDbObj();

    void setDt1aAtvtDbObj(String dt1aAtvtDbObj);

    /**
     * Host Variable A25-DT-SEGNAL-PARTNER-DB
     * 
     */
    String getDtSegnalPartnerDb();

    void setDtSegnalPartnerDb(String dtSegnalPartnerDb);

    /**
     * Nullable property for A25-DT-SEGNAL-PARTNER-DB
     * 
     */
    String getDtSegnalPartnerDbObj();

    void setDtSegnalPartnerDbObj(String dtSegnalPartnerDbObj);
};
