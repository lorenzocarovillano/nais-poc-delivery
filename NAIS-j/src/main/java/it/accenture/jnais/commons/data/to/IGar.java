package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [GAR]
 * 
 */
public interface IGar extends BaseSqlTo {

    /**
     * Host Variable GRZ-ID-POLI
     * 
     */
    int getGrzIdPoli();

    void setGrzIdPoli(int grzIdPoli);

    /**
     * Host Variable GRZ-ID-ADES
     * 
     */
    int getGrzIdAdes();

    void setGrzIdAdes(int grzIdAdes);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable GRZ-ID-GAR
     * 
     */
    int getIdGar();

    void setIdGar(int idGar);

    /**
     * Nullable property for GRZ-ID-ADES
     * 
     */
    Integer getGrzIdAdesObj();

    void setGrzIdAdesObj(Integer grzIdAdesObj);

    /**
     * Host Variable GRZ-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable GRZ-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for GRZ-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable GRZ-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable GRZ-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable GRZ-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable GRZ-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for GRZ-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable GRZ-DT-DECOR-DB
     * 
     */
    String getDtDecorDb();

    void setDtDecorDb(String dtDecorDb);

    /**
     * Nullable property for GRZ-DT-DECOR-DB
     * 
     */
    String getDtDecorDbObj();

    void setDtDecorDbObj(String dtDecorDbObj);

    /**
     * Host Variable GRZ-DT-SCAD-DB
     * 
     */
    String getDtScadDb();

    void setDtScadDb(String dtScadDb);

    /**
     * Nullable property for GRZ-DT-SCAD-DB
     * 
     */
    String getDtScadDbObj();

    void setDtScadDbObj(String dtScadDbObj);

    /**
     * Host Variable GRZ-COD-SEZ
     * 
     */
    String getCodSez();

    void setCodSez(String codSez);

    /**
     * Nullable property for GRZ-COD-SEZ
     * 
     */
    String getCodSezObj();

    void setCodSezObj(String codSezObj);

    /**
     * Host Variable GRZ-COD-TARI
     * 
     */
    String getCodTari();

    void setCodTari(String codTari);

    /**
     * Host Variable GRZ-RAMO-BILA
     * 
     */
    String getRamoBila();

    void setRamoBila(String ramoBila);

    /**
     * Nullable property for GRZ-RAMO-BILA
     * 
     */
    String getRamoBilaObj();

    void setRamoBilaObj(String ramoBilaObj);

    /**
     * Host Variable GRZ-DT-INI-VAL-TAR-DB
     * 
     */
    String getDtIniValTarDb();

    void setDtIniValTarDb(String dtIniValTarDb);

    /**
     * Nullable property for GRZ-DT-INI-VAL-TAR-DB
     * 
     */
    String getDtIniValTarDbObj();

    void setDtIniValTarDbObj(String dtIniValTarDbObj);

    /**
     * Host Variable GRZ-ID-1O-ASSTO
     * 
     */
    int getId1oAssto();

    void setId1oAssto(int id1oAssto);

    /**
     * Nullable property for GRZ-ID-1O-ASSTO
     * 
     */
    Integer getId1oAsstoObj();

    void setId1oAsstoObj(Integer id1oAsstoObj);

    /**
     * Host Variable GRZ-ID-2O-ASSTO
     * 
     */
    int getId2oAssto();

    void setId2oAssto(int id2oAssto);

    /**
     * Nullable property for GRZ-ID-2O-ASSTO
     * 
     */
    Integer getId2oAsstoObj();

    void setId2oAsstoObj(Integer id2oAsstoObj);

    /**
     * Host Variable GRZ-ID-3O-ASSTO
     * 
     */
    int getId3oAssto();

    void setId3oAssto(int id3oAssto);

    /**
     * Nullable property for GRZ-ID-3O-ASSTO
     * 
     */
    Integer getId3oAsstoObj();

    void setId3oAsstoObj(Integer id3oAsstoObj);

    /**
     * Host Variable GRZ-TP-GAR
     * 
     */
    short getTpGar();

    void setTpGar(short tpGar);

    /**
     * Nullable property for GRZ-TP-GAR
     * 
     */
    Short getTpGarObj();

    void setTpGarObj(Short tpGarObj);

    /**
     * Host Variable GRZ-TP-RSH
     * 
     */
    String getTpRsh();

    void setTpRsh(String tpRsh);

    /**
     * Nullable property for GRZ-TP-RSH
     * 
     */
    String getTpRshObj();

    void setTpRshObj(String tpRshObj);

    /**
     * Host Variable GRZ-TP-INVST
     * 
     */
    short getTpInvst();

    void setTpInvst(short tpInvst);

    /**
     * Nullable property for GRZ-TP-INVST
     * 
     */
    Short getTpInvstObj();

    void setTpInvstObj(Short tpInvstObj);

    /**
     * Host Variable GRZ-MOD-PAG-GARCOL
     * 
     */
    String getModPagGarcol();

    void setModPagGarcol(String modPagGarcol);

    /**
     * Nullable property for GRZ-MOD-PAG-GARCOL
     * 
     */
    String getModPagGarcolObj();

    void setModPagGarcolObj(String modPagGarcolObj);

    /**
     * Host Variable GRZ-TP-PER-PRE
     * 
     */
    String getTpPerPre();

    void setTpPerPre(String tpPerPre);

    /**
     * Nullable property for GRZ-TP-PER-PRE
     * 
     */
    String getTpPerPreObj();

    void setTpPerPreObj(String tpPerPreObj);

    /**
     * Host Variable GRZ-ETA-AA-1O-ASSTO
     * 
     */
    short getEtaAa1oAssto();

    void setEtaAa1oAssto(short etaAa1oAssto);

    /**
     * Nullable property for GRZ-ETA-AA-1O-ASSTO
     * 
     */
    Short getEtaAa1oAsstoObj();

    void setEtaAa1oAsstoObj(Short etaAa1oAsstoObj);

    /**
     * Host Variable GRZ-ETA-MM-1O-ASSTO
     * 
     */
    short getEtaMm1oAssto();

    void setEtaMm1oAssto(short etaMm1oAssto);

    /**
     * Nullable property for GRZ-ETA-MM-1O-ASSTO
     * 
     */
    Short getEtaMm1oAsstoObj();

    void setEtaMm1oAsstoObj(Short etaMm1oAsstoObj);

    /**
     * Host Variable GRZ-ETA-AA-2O-ASSTO
     * 
     */
    short getEtaAa2oAssto();

    void setEtaAa2oAssto(short etaAa2oAssto);

    /**
     * Nullable property for GRZ-ETA-AA-2O-ASSTO
     * 
     */
    Short getEtaAa2oAsstoObj();

    void setEtaAa2oAsstoObj(Short etaAa2oAsstoObj);

    /**
     * Host Variable GRZ-ETA-MM-2O-ASSTO
     * 
     */
    short getEtaMm2oAssto();

    void setEtaMm2oAssto(short etaMm2oAssto);

    /**
     * Nullable property for GRZ-ETA-MM-2O-ASSTO
     * 
     */
    Short getEtaMm2oAsstoObj();

    void setEtaMm2oAsstoObj(Short etaMm2oAsstoObj);

    /**
     * Host Variable GRZ-ETA-AA-3O-ASSTO
     * 
     */
    short getEtaAa3oAssto();

    void setEtaAa3oAssto(short etaAa3oAssto);

    /**
     * Nullable property for GRZ-ETA-AA-3O-ASSTO
     * 
     */
    Short getEtaAa3oAsstoObj();

    void setEtaAa3oAsstoObj(Short etaAa3oAsstoObj);

    /**
     * Host Variable GRZ-ETA-MM-3O-ASSTO
     * 
     */
    short getEtaMm3oAssto();

    void setEtaMm3oAssto(short etaMm3oAssto);

    /**
     * Nullable property for GRZ-ETA-MM-3O-ASSTO
     * 
     */
    Short getEtaMm3oAsstoObj();

    void setEtaMm3oAsstoObj(Short etaMm3oAsstoObj);

    /**
     * Host Variable GRZ-TP-EMIS-PUR
     * 
     */
    char getTpEmisPur();

    void setTpEmisPur(char tpEmisPur);

    /**
     * Nullable property for GRZ-TP-EMIS-PUR
     * 
     */
    Character getTpEmisPurObj();

    void setTpEmisPurObj(Character tpEmisPurObj);

    /**
     * Host Variable GRZ-ETA-A-SCAD
     * 
     */
    int getEtaAScad();

    void setEtaAScad(int etaAScad);

    /**
     * Nullable property for GRZ-ETA-A-SCAD
     * 
     */
    Integer getEtaAScadObj();

    void setEtaAScadObj(Integer etaAScadObj);

    /**
     * Host Variable GRZ-TP-CALC-PRE-PRSTZ
     * 
     */
    String getTpCalcPrePrstz();

    void setTpCalcPrePrstz(String tpCalcPrePrstz);

    /**
     * Nullable property for GRZ-TP-CALC-PRE-PRSTZ
     * 
     */
    String getTpCalcPrePrstzObj();

    void setTpCalcPrePrstzObj(String tpCalcPrePrstzObj);

    /**
     * Host Variable GRZ-TP-PRE
     * 
     */
    char getTpPre();

    void setTpPre(char tpPre);

    /**
     * Nullable property for GRZ-TP-PRE
     * 
     */
    Character getTpPreObj();

    void setTpPreObj(Character tpPreObj);

    /**
     * Host Variable GRZ-TP-DUR
     * 
     */
    String getTpDur();

    void setTpDur(String tpDur);

    /**
     * Nullable property for GRZ-TP-DUR
     * 
     */
    String getTpDurObj();

    void setTpDurObj(String tpDurObj);

    /**
     * Host Variable GRZ-DUR-AA
     * 
     */
    int getDurAa();

    void setDurAa(int durAa);

    /**
     * Nullable property for GRZ-DUR-AA
     * 
     */
    Integer getDurAaObj();

    void setDurAaObj(Integer durAaObj);

    /**
     * Host Variable GRZ-DUR-MM
     * 
     */
    int getDurMm();

    void setDurMm(int durMm);

    /**
     * Nullable property for GRZ-DUR-MM
     * 
     */
    Integer getDurMmObj();

    void setDurMmObj(Integer durMmObj);

    /**
     * Host Variable GRZ-DUR-GG
     * 
     */
    int getDurGg();

    void setDurGg(int durGg);

    /**
     * Nullable property for GRZ-DUR-GG
     * 
     */
    Integer getDurGgObj();

    void setDurGgObj(Integer durGgObj);

    /**
     * Host Variable GRZ-NUM-AA-PAG-PRE
     * 
     */
    int getNumAaPagPre();

    void setNumAaPagPre(int numAaPagPre);

    /**
     * Nullable property for GRZ-NUM-AA-PAG-PRE
     * 
     */
    Integer getNumAaPagPreObj();

    void setNumAaPagPreObj(Integer numAaPagPreObj);

    /**
     * Host Variable GRZ-AA-PAG-PRE-UNI
     * 
     */
    int getAaPagPreUni();

    void setAaPagPreUni(int aaPagPreUni);

    /**
     * Nullable property for GRZ-AA-PAG-PRE-UNI
     * 
     */
    Integer getAaPagPreUniObj();

    void setAaPagPreUniObj(Integer aaPagPreUniObj);

    /**
     * Host Variable GRZ-MM-PAG-PRE-UNI
     * 
     */
    int getMmPagPreUni();

    void setMmPagPreUni(int mmPagPreUni);

    /**
     * Nullable property for GRZ-MM-PAG-PRE-UNI
     * 
     */
    Integer getMmPagPreUniObj();

    void setMmPagPreUniObj(Integer mmPagPreUniObj);

    /**
     * Host Variable GRZ-FRAZ-INI-EROG-REN
     * 
     */
    int getFrazIniErogRen();

    void setFrazIniErogRen(int frazIniErogRen);

    /**
     * Nullable property for GRZ-FRAZ-INI-EROG-REN
     * 
     */
    Integer getFrazIniErogRenObj();

    void setFrazIniErogRenObj(Integer frazIniErogRenObj);

    /**
     * Host Variable GRZ-MM-1O-RAT
     * 
     */
    short getMm1oRat();

    void setMm1oRat(short mm1oRat);

    /**
     * Nullable property for GRZ-MM-1O-RAT
     * 
     */
    Short getMm1oRatObj();

    void setMm1oRatObj(Short mm1oRatObj);

    /**
     * Host Variable GRZ-PC-1O-RAT
     * 
     */
    AfDecimal getPc1oRat();

    void setPc1oRat(AfDecimal pc1oRat);

    /**
     * Nullable property for GRZ-PC-1O-RAT
     * 
     */
    AfDecimal getPc1oRatObj();

    void setPc1oRatObj(AfDecimal pc1oRatObj);

    /**
     * Host Variable GRZ-TP-PRSTZ-ASSTA
     * 
     */
    String getTpPrstzAssta();

    void setTpPrstzAssta(String tpPrstzAssta);

    /**
     * Nullable property for GRZ-TP-PRSTZ-ASSTA
     * 
     */
    String getTpPrstzAsstaObj();

    void setTpPrstzAsstaObj(String tpPrstzAsstaObj);

    /**
     * Host Variable GRZ-DT-END-CARZ-DB
     * 
     */
    String getDtEndCarzDb();

    void setDtEndCarzDb(String dtEndCarzDb);

    /**
     * Nullable property for GRZ-DT-END-CARZ-DB
     * 
     */
    String getDtEndCarzDbObj();

    void setDtEndCarzDbObj(String dtEndCarzDbObj);

    /**
     * Host Variable GRZ-PC-RIP-PRE
     * 
     */
    AfDecimal getPcRipPre();

    void setPcRipPre(AfDecimal pcRipPre);

    /**
     * Nullable property for GRZ-PC-RIP-PRE
     * 
     */
    AfDecimal getPcRipPreObj();

    void setPcRipPreObj(AfDecimal pcRipPreObj);

    /**
     * Host Variable GRZ-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Nullable property for GRZ-COD-FND
     * 
     */
    String getCodFndObj();

    void setCodFndObj(String codFndObj);

    /**
     * Host Variable GRZ-AA-REN-CER
     * 
     */
    String getAaRenCer();

    void setAaRenCer(String aaRenCer);

    /**
     * Nullable property for GRZ-AA-REN-CER
     * 
     */
    String getAaRenCerObj();

    void setAaRenCerObj(String aaRenCerObj);

    /**
     * Host Variable GRZ-PC-REVRSB
     * 
     */
    AfDecimal getPcRevrsb();

    void setPcRevrsb(AfDecimal pcRevrsb);

    /**
     * Nullable property for GRZ-PC-REVRSB
     * 
     */
    AfDecimal getPcRevrsbObj();

    void setPcRevrsbObj(AfDecimal pcRevrsbObj);

    /**
     * Host Variable GRZ-TP-PC-RIP
     * 
     */
    String getTpPcRip();

    void setTpPcRip(String tpPcRip);

    /**
     * Nullable property for GRZ-TP-PC-RIP
     * 
     */
    String getTpPcRipObj();

    void setTpPcRipObj(String tpPcRipObj);

    /**
     * Host Variable GRZ-PC-OPZ
     * 
     */
    AfDecimal getPcOpz();

    void setPcOpz(AfDecimal pcOpz);

    /**
     * Nullable property for GRZ-PC-OPZ
     * 
     */
    AfDecimal getPcOpzObj();

    void setPcOpzObj(AfDecimal pcOpzObj);

    /**
     * Host Variable GRZ-TP-IAS
     * 
     */
    String getTpIas();

    void setTpIas(String tpIas);

    /**
     * Nullable property for GRZ-TP-IAS
     * 
     */
    String getTpIasObj();

    void setTpIasObj(String tpIasObj);

    /**
     * Host Variable GRZ-TP-STAB
     * 
     */
    String getTpStab();

    void setTpStab(String tpStab);

    /**
     * Nullable property for GRZ-TP-STAB
     * 
     */
    String getTpStabObj();

    void setTpStabObj(String tpStabObj);

    /**
     * Host Variable GRZ-TP-ADEG-PRE
     * 
     */
    char getTpAdegPre();

    void setTpAdegPre(char tpAdegPre);

    /**
     * Nullable property for GRZ-TP-ADEG-PRE
     * 
     */
    Character getTpAdegPreObj();

    void setTpAdegPreObj(Character tpAdegPreObj);

    /**
     * Host Variable GRZ-DT-VARZ-TP-IAS-DB
     * 
     */
    String getDtVarzTpIasDb();

    void setDtVarzTpIasDb(String dtVarzTpIasDb);

    /**
     * Nullable property for GRZ-DT-VARZ-TP-IAS-DB
     * 
     */
    String getDtVarzTpIasDbObj();

    void setDtVarzTpIasDbObj(String dtVarzTpIasDbObj);

    /**
     * Host Variable GRZ-FRAZ-DECR-CPT
     * 
     */
    int getFrazDecrCpt();

    void setFrazDecrCpt(int frazDecrCpt);

    /**
     * Nullable property for GRZ-FRAZ-DECR-CPT
     * 
     */
    Integer getFrazDecrCptObj();

    void setFrazDecrCptObj(Integer frazDecrCptObj);

    /**
     * Host Variable GRZ-COD-TRAT-RIASS
     * 
     */
    String getCodTratRiass();

    void setCodTratRiass(String codTratRiass);

    /**
     * Nullable property for GRZ-COD-TRAT-RIASS
     * 
     */
    String getCodTratRiassObj();

    void setCodTratRiassObj(String codTratRiassObj);

    /**
     * Host Variable GRZ-TP-DT-EMIS-RIASS
     * 
     */
    String getTpDtEmisRiass();

    void setTpDtEmisRiass(String tpDtEmisRiass);

    /**
     * Nullable property for GRZ-TP-DT-EMIS-RIASS
     * 
     */
    String getTpDtEmisRiassObj();

    void setTpDtEmisRiassObj(String tpDtEmisRiassObj);

    /**
     * Host Variable GRZ-TP-CESS-RIASS
     * 
     */
    String getTpCessRiass();

    void setTpCessRiass(String tpCessRiass);

    /**
     * Nullable property for GRZ-TP-CESS-RIASS
     * 
     */
    String getTpCessRiassObj();

    void setTpCessRiassObj(String tpCessRiassObj);

    /**
     * Host Variable GRZ-DS-RIGA
     * 
     */
    long getGrzDsRiga();

    void setGrzDsRiga(long grzDsRiga);

    /**
     * Host Variable GRZ-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable GRZ-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable GRZ-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable GRZ-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable GRZ-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable GRZ-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable GRZ-AA-STAB
     * 
     */
    int getAaStab();

    void setAaStab(int aaStab);

    /**
     * Nullable property for GRZ-AA-STAB
     * 
     */
    Integer getAaStabObj();

    void setAaStabObj(Integer aaStabObj);

    /**
     * Host Variable GRZ-TS-STAB-LIMITATA
     * 
     */
    AfDecimal getTsStabLimitata();

    void setTsStabLimitata(AfDecimal tsStabLimitata);

    /**
     * Nullable property for GRZ-TS-STAB-LIMITATA
     * 
     */
    AfDecimal getTsStabLimitataObj();

    void setTsStabLimitataObj(AfDecimal tsStabLimitataObj);

    /**
     * Host Variable GRZ-DT-PRESC-DB
     * 
     */
    String getDtPrescDb();

    void setDtPrescDb(String dtPrescDb);

    /**
     * Nullable property for GRZ-DT-PRESC-DB
     * 
     */
    String getDtPrescDbObj();

    void setDtPrescDbObj(String dtPrescDbObj);

    /**
     * Host Variable GRZ-RSH-INVST
     * 
     */
    char getRshInvst();

    void setRshInvst(char rshInvst);

    /**
     * Nullable property for GRZ-RSH-INVST
     * 
     */
    Character getRshInvstObj();

    void setRshInvstObj(Character rshInvstObj);

    /**
     * Host Variable GRZ-TP-RAMO-BILA
     * 
     */
    String getTpRamoBila();

    void setTpRamoBila(String tpRamoBila);
};
