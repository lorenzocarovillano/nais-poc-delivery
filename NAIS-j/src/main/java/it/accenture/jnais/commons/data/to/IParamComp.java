package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PARAM_COMP]
 * 
 */
public interface IParamComp extends BaseSqlTo {

    /**
     * Host Variable PCO-COD-COMP-ANIA
     * 
     */
    int getPcoCodCompAnia();

    void setPcoCodCompAnia(int pcoCodCompAnia);

    /**
     * Host Variable PCO-COD-TRAT-CIRT
     * 
     */
    String getCodTratCirt();

    void setCodTratCirt(String codTratCirt);

    /**
     * Nullable property for PCO-COD-TRAT-CIRT
     * 
     */
    String getCodTratCirtObj();

    void setCodTratCirtObj(String codTratCirtObj);

    /**
     * Host Variable PCO-LIM-VLTR
     * 
     */
    AfDecimal getLimVltr();

    void setLimVltr(AfDecimal limVltr);

    /**
     * Nullable property for PCO-LIM-VLTR
     * 
     */
    AfDecimal getLimVltrObj();

    void setLimVltrObj(AfDecimal limVltrObj);

    /**
     * Host Variable PCO-TP-RAT-PERF
     * 
     */
    char getTpRatPerf();

    void setTpRatPerf(char tpRatPerf);

    /**
     * Nullable property for PCO-TP-RAT-PERF
     * 
     */
    Character getTpRatPerfObj();

    void setTpRatPerfObj(Character tpRatPerfObj);

    /**
     * Host Variable PCO-TP-LIV-GENZ-TIT
     * 
     */
    String getTpLivGenzTit();

    void setTpLivGenzTit(String tpLivGenzTit);

    /**
     * Host Variable PCO-ARROT-PRE
     * 
     */
    AfDecimal getArrotPre();

    void setArrotPre(AfDecimal arrotPre);

    /**
     * Nullable property for PCO-ARROT-PRE
     * 
     */
    AfDecimal getArrotPreObj();

    void setArrotPreObj(AfDecimal arrotPreObj);

    /**
     * Host Variable PCO-DT-CONT-DB
     * 
     */
    String getDtContDb();

    void setDtContDb(String dtContDb);

    /**
     * Nullable property for PCO-DT-CONT-DB
     * 
     */
    String getDtContDbObj();

    void setDtContDbObj(String dtContDbObj);

    /**
     * Host Variable PCO-DT-ULT-RIVAL-IN-DB
     * 
     */
    String getDtUltRivalInDb();

    void setDtUltRivalInDb(String dtUltRivalInDb);

    /**
     * Nullable property for PCO-DT-ULT-RIVAL-IN-DB
     * 
     */
    String getDtUltRivalInDbObj();

    void setDtUltRivalInDbObj(String dtUltRivalInDbObj);

    /**
     * Host Variable PCO-DT-ULT-QTZO-IN-DB
     * 
     */
    String getDtUltQtzoInDb();

    void setDtUltQtzoInDb(String dtUltQtzoInDb);

    /**
     * Nullable property for PCO-DT-ULT-QTZO-IN-DB
     * 
     */
    String getDtUltQtzoInDbObj();

    void setDtUltQtzoInDbObj(String dtUltQtzoInDbObj);

    /**
     * Host Variable PCO-DT-ULT-RICL-RIASS-DB
     * 
     */
    String getDtUltRiclRiassDb();

    void setDtUltRiclRiassDb(String dtUltRiclRiassDb);

    /**
     * Nullable property for PCO-DT-ULT-RICL-RIASS-DB
     * 
     */
    String getDtUltRiclRiassDbObj();

    void setDtUltRiclRiassDbObj(String dtUltRiclRiassDbObj);

    /**
     * Host Variable PCO-DT-ULT-TABUL-RIASS-DB
     * 
     */
    String getDtUltTabulRiassDb();

    void setDtUltTabulRiassDb(String dtUltTabulRiassDb);

    /**
     * Nullable property for PCO-DT-ULT-TABUL-RIASS-DB
     * 
     */
    String getDtUltTabulRiassDbObj();

    void setDtUltTabulRiassDbObj(String dtUltTabulRiassDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-EMES-DB
     * 
     */
    String getDtUltBollEmesDb();

    void setDtUltBollEmesDb(String dtUltBollEmesDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-EMES-DB
     * 
     */
    String getDtUltBollEmesDbObj();

    void setDtUltBollEmesDbObj(String dtUltBollEmesDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-STOR-DB
     * 
     */
    String getDtUltBollStorDb();

    void setDtUltBollStorDb(String dtUltBollStorDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-STOR-DB
     * 
     */
    String getDtUltBollStorDbObj();

    void setDtUltBollStorDbObj(String dtUltBollStorDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-LIQ-DB
     * 
     */
    String getDtUltBollLiqDb();

    void setDtUltBollLiqDb(String dtUltBollLiqDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-LIQ-DB
     * 
     */
    String getDtUltBollLiqDbObj();

    void setDtUltBollLiqDbObj(String dtUltBollLiqDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-RIAT-DB
     * 
     */
    String getDtUltBollRiatDb();

    void setDtUltBollRiatDb(String dtUltBollRiatDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-RIAT-DB
     * 
     */
    String getDtUltBollRiatDbObj();

    void setDtUltBollRiatDbObj(String dtUltBollRiatDbObj);

    /**
     * Host Variable PCO-DT-ULTELRISCPAR-PR-DB
     * 
     */
    String getDtUltelriscparPrDb();

    void setDtUltelriscparPrDb(String dtUltelriscparPrDb);

    /**
     * Nullable property for PCO-DT-ULTELRISCPAR-PR-DB
     * 
     */
    String getDtUltelriscparPrDbObj();

    void setDtUltelriscparPrDbObj(String dtUltelriscparPrDbObj);

    /**
     * Host Variable PCO-DT-ULTC-IS-IN-DB
     * 
     */
    String getDtUltcIsInDb();

    void setDtUltcIsInDb(String dtUltcIsInDb);

    /**
     * Nullable property for PCO-DT-ULTC-IS-IN-DB
     * 
     */
    String getDtUltcIsInDbObj();

    void setDtUltcIsInDbObj(String dtUltcIsInDbObj);

    /**
     * Host Variable PCO-DT-ULT-RICL-PRE-DB
     * 
     */
    String getDtUltRiclPreDb();

    void setDtUltRiclPreDb(String dtUltRiclPreDb);

    /**
     * Nullable property for PCO-DT-ULT-RICL-PRE-DB
     * 
     */
    String getDtUltRiclPreDbObj();

    void setDtUltRiclPreDbObj(String dtUltRiclPreDbObj);

    /**
     * Host Variable PCO-DT-ULTC-MARSOL-DB
     * 
     */
    String getDtUltcMarsolDb();

    void setDtUltcMarsolDb(String dtUltcMarsolDb);

    /**
     * Nullable property for PCO-DT-ULTC-MARSOL-DB
     * 
     */
    String getDtUltcMarsolDbObj();

    void setDtUltcMarsolDbObj(String dtUltcMarsolDbObj);

    /**
     * Host Variable PCO-DT-ULTC-RB-IN-DB
     * 
     */
    String getDtUltcRbInDb();

    void setDtUltcRbInDb(String dtUltcRbInDb);

    /**
     * Nullable property for PCO-DT-ULTC-RB-IN-DB
     * 
     */
    String getDtUltcRbInDbObj();

    void setDtUltcRbInDbObj(String dtUltcRbInDbObj);

    /**
     * Host Variable PCO-PC-PROV-1AA-ACQ
     * 
     */
    AfDecimal getPcProv1aaAcq();

    void setPcProv1aaAcq(AfDecimal pcProv1aaAcq);

    /**
     * Nullable property for PCO-PC-PROV-1AA-ACQ
     * 
     */
    AfDecimal getPcProv1aaAcqObj();

    void setPcProv1aaAcqObj(AfDecimal pcProv1aaAcqObj);

    /**
     * Host Variable PCO-MOD-INTR-PREST
     * 
     */
    String getModIntrPrest();

    void setModIntrPrest(String modIntrPrest);

    /**
     * Nullable property for PCO-MOD-INTR-PREST
     * 
     */
    String getModIntrPrestObj();

    void setModIntrPrestObj(String modIntrPrestObj);

    /**
     * Host Variable PCO-GG-MAX-REC-PROV
     * 
     */
    short getGgMaxRecProv();

    void setGgMaxRecProv(short ggMaxRecProv);

    /**
     * Nullable property for PCO-GG-MAX-REC-PROV
     * 
     */
    Short getGgMaxRecProvObj();

    void setGgMaxRecProvObj(Short ggMaxRecProvObj);

    /**
     * Host Variable PCO-DT-ULTGZ-TRCH-E-IN-DB
     * 
     */
    String getDtUltgzTrchEInDb();

    void setDtUltgzTrchEInDb(String dtUltgzTrchEInDb);

    /**
     * Nullable property for PCO-DT-ULTGZ-TRCH-E-IN-DB
     * 
     */
    String getDtUltgzTrchEInDbObj();

    void setDtUltgzTrchEInDbObj(String dtUltgzTrchEInDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-SNDEN-DB
     * 
     */
    String getDtUltBollSndenDb();

    void setDtUltBollSndenDb(String dtUltBollSndenDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-SNDEN-DB
     * 
     */
    String getDtUltBollSndenDbObj();

    void setDtUltBollSndenDbObj(String dtUltBollSndenDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-SNDNLQ-DB
     * 
     */
    String getDtUltBollSndnlqDb();

    void setDtUltBollSndnlqDb(String dtUltBollSndnlqDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-SNDNLQ-DB
     * 
     */
    String getDtUltBollSndnlqDbObj();

    void setDtUltBollSndnlqDbObj(String dtUltBollSndnlqDbObj);

    /**
     * Host Variable PCO-DT-ULTSC-ELAB-IN-DB
     * 
     */
    String getDtUltscElabInDb();

    void setDtUltscElabInDb(String dtUltscElabInDb);

    /**
     * Nullable property for PCO-DT-ULTSC-ELAB-IN-DB
     * 
     */
    String getDtUltscElabInDbObj();

    void setDtUltscElabInDbObj(String dtUltscElabInDbObj);

    /**
     * Host Variable PCO-DT-ULTSC-OPZ-IN-DB
     * 
     */
    String getDtUltscOpzInDb();

    void setDtUltscOpzInDb(String dtUltscOpzInDb);

    /**
     * Nullable property for PCO-DT-ULTSC-OPZ-IN-DB
     * 
     */
    String getDtUltscOpzInDbObj();

    void setDtUltscOpzInDbObj(String dtUltscOpzInDbObj);

    /**
     * Host Variable PCO-DT-ULTC-BNSRIC-IN-DB
     * 
     */
    String getDtUltcBnsricInDb();

    void setDtUltcBnsricInDb(String dtUltcBnsricInDb);

    /**
     * Nullable property for PCO-DT-ULTC-BNSRIC-IN-DB
     * 
     */
    String getDtUltcBnsricInDbObj();

    void setDtUltcBnsricInDbObj(String dtUltcBnsricInDbObj);

    /**
     * Host Variable PCO-DT-ULTC-BNSFDT-IN-DB
     * 
     */
    String getDtUltcBnsfdtInDb();

    void setDtUltcBnsfdtInDb(String dtUltcBnsfdtInDb);

    /**
     * Nullable property for PCO-DT-ULTC-BNSFDT-IN-DB
     * 
     */
    String getDtUltcBnsfdtInDbObj();

    void setDtUltcBnsfdtInDbObj(String dtUltcBnsfdtInDbObj);

    /**
     * Host Variable PCO-DT-ULT-RINN-GARAC-DB
     * 
     */
    String getDtUltRinnGaracDb();

    void setDtUltRinnGaracDb(String dtUltRinnGaracDb);

    /**
     * Nullable property for PCO-DT-ULT-RINN-GARAC-DB
     * 
     */
    String getDtUltRinnGaracDbObj();

    void setDtUltRinnGaracDbObj(String dtUltRinnGaracDbObj);

    /**
     * Host Variable PCO-DT-ULTGZ-CED-DB
     * 
     */
    String getDtUltgzCedDb();

    void setDtUltgzCedDb(String dtUltgzCedDb);

    /**
     * Nullable property for PCO-DT-ULTGZ-CED-DB
     * 
     */
    String getDtUltgzCedDbObj();

    void setDtUltgzCedDbObj(String dtUltgzCedDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-PRLCOS-DB
     * 
     */
    String getDtUltElabPrlcosDb();

    void setDtUltElabPrlcosDb(String dtUltElabPrlcosDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-PRLCOS-DB
     * 
     */
    String getDtUltElabPrlcosDbObj();

    void setDtUltElabPrlcosDbObj(String dtUltElabPrlcosDbObj);

    /**
     * Host Variable PCO-DT-ULT-RINN-COLL-DB
     * 
     */
    String getDtUltRinnCollDb();

    void setDtUltRinnCollDb(String dtUltRinnCollDb);

    /**
     * Nullable property for PCO-DT-ULT-RINN-COLL-DB
     * 
     */
    String getDtUltRinnCollDbObj();

    void setDtUltRinnCollDbObj(String dtUltRinnCollDbObj);

    /**
     * Host Variable PCO-FL-RVC-PERF
     * 
     */
    char getFlRvcPerf();

    void setFlRvcPerf(char flRvcPerf);

    /**
     * Nullable property for PCO-FL-RVC-PERF
     * 
     */
    Character getFlRvcPerfObj();

    void setFlRvcPerfObj(Character flRvcPerfObj);

    /**
     * Host Variable PCO-FL-RCS-POLI-NOPERF
     * 
     */
    char getFlRcsPoliNoperf();

    void setFlRcsPoliNoperf(char flRcsPoliNoperf);

    /**
     * Nullable property for PCO-FL-RCS-POLI-NOPERF
     * 
     */
    Character getFlRcsPoliNoperfObj();

    void setFlRcsPoliNoperfObj(Character flRcsPoliNoperfObj);

    /**
     * Host Variable PCO-FL-GEST-PLUSV
     * 
     */
    char getFlGestPlusv();

    void setFlGestPlusv(char flGestPlusv);

    /**
     * Nullable property for PCO-FL-GEST-PLUSV
     * 
     */
    Character getFlGestPlusvObj();

    void setFlGestPlusvObj(Character flGestPlusvObj);

    /**
     * Host Variable PCO-DT-ULT-RIVAL-CL-DB
     * 
     */
    String getDtUltRivalClDb();

    void setDtUltRivalClDb(String dtUltRivalClDb);

    /**
     * Nullable property for PCO-DT-ULT-RIVAL-CL-DB
     * 
     */
    String getDtUltRivalClDbObj();

    void setDtUltRivalClDbObj(String dtUltRivalClDbObj);

    /**
     * Host Variable PCO-DT-ULT-QTZO-CL-DB
     * 
     */
    String getDtUltQtzoClDb();

    void setDtUltQtzoClDb(String dtUltQtzoClDb);

    /**
     * Nullable property for PCO-DT-ULT-QTZO-CL-DB
     * 
     */
    String getDtUltQtzoClDbObj();

    void setDtUltQtzoClDbObj(String dtUltQtzoClDbObj);

    /**
     * Host Variable PCO-DT-ULTC-BNSRIC-CL-DB
     * 
     */
    String getDtUltcBnsricClDb();

    void setDtUltcBnsricClDb(String dtUltcBnsricClDb);

    /**
     * Nullable property for PCO-DT-ULTC-BNSRIC-CL-DB
     * 
     */
    String getDtUltcBnsricClDbObj();

    void setDtUltcBnsricClDbObj(String dtUltcBnsricClDbObj);

    /**
     * Host Variable PCO-DT-ULTC-BNSFDT-CL-DB
     * 
     */
    String getDtUltcBnsfdtClDb();

    void setDtUltcBnsfdtClDb(String dtUltcBnsfdtClDb);

    /**
     * Nullable property for PCO-DT-ULTC-BNSFDT-CL-DB
     * 
     */
    String getDtUltcBnsfdtClDbObj();

    void setDtUltcBnsfdtClDbObj(String dtUltcBnsfdtClDbObj);

    /**
     * Host Variable PCO-DT-ULTC-IS-CL-DB
     * 
     */
    String getDtUltcIsClDb();

    void setDtUltcIsClDb(String dtUltcIsClDb);

    /**
     * Nullable property for PCO-DT-ULTC-IS-CL-DB
     * 
     */
    String getDtUltcIsClDbObj();

    void setDtUltcIsClDbObj(String dtUltcIsClDbObj);

    /**
     * Host Variable PCO-DT-ULTC-RB-CL-DB
     * 
     */
    String getDtUltcRbClDb();

    void setDtUltcRbClDb(String dtUltcRbClDb);

    /**
     * Nullable property for PCO-DT-ULTC-RB-CL-DB
     * 
     */
    String getDtUltcRbClDbObj();

    void setDtUltcRbClDbObj(String dtUltcRbClDbObj);

    /**
     * Host Variable PCO-DT-ULTGZ-TRCH-E-CL-DB
     * 
     */
    String getDtUltgzTrchEClDb();

    void setDtUltgzTrchEClDb(String dtUltgzTrchEClDb);

    /**
     * Nullable property for PCO-DT-ULTGZ-TRCH-E-CL-DB
     * 
     */
    String getDtUltgzTrchEClDbObj();

    void setDtUltgzTrchEClDbObj(String dtUltgzTrchEClDbObj);

    /**
     * Host Variable PCO-DT-ULTSC-ELAB-CL-DB
     * 
     */
    String getDtUltscElabClDb();

    void setDtUltscElabClDb(String dtUltscElabClDb);

    /**
     * Nullable property for PCO-DT-ULTSC-ELAB-CL-DB
     * 
     */
    String getDtUltscElabClDbObj();

    void setDtUltscElabClDbObj(String dtUltscElabClDbObj);

    /**
     * Host Variable PCO-DT-ULTSC-OPZ-CL-DB
     * 
     */
    String getDtUltscOpzClDb();

    void setDtUltscOpzClDb(String dtUltscOpzClDb);

    /**
     * Nullable property for PCO-DT-ULTSC-OPZ-CL-DB
     * 
     */
    String getDtUltscOpzClDbObj();

    void setDtUltscOpzClDbObj(String dtUltscOpzClDbObj);

    /**
     * Host Variable PCO-STST-X-REGIONE-DB
     * 
     */
    String getStstXRegioneDb();

    void setStstXRegioneDb(String ststXRegioneDb);

    /**
     * Nullable property for PCO-STST-X-REGIONE-DB
     * 
     */
    String getStstXRegioneDbObj();

    void setStstXRegioneDbObj(String ststXRegioneDbObj);

    /**
     * Host Variable PCO-DT-ULTGZ-CED-COLL-DB
     * 
     */
    String getDtUltgzCedCollDb();

    void setDtUltgzCedCollDb(String dtUltgzCedCollDb);

    /**
     * Nullable property for PCO-DT-ULTGZ-CED-COLL-DB
     * 
     */
    String getDtUltgzCedCollDbObj();

    void setDtUltgzCedCollDbObj(String dtUltgzCedCollDbObj);

    /**
     * Host Variable PCO-TP-MOD-RIVAL
     * 
     */
    String getTpModRival();

    void setTpModRival(String tpModRival);

    /**
     * Host Variable PCO-NUM-MM-CALC-MORA
     * 
     */
    int getNumMmCalcMora();

    void setNumMmCalcMora(int numMmCalcMora);

    /**
     * Nullable property for PCO-NUM-MM-CALC-MORA
     * 
     */
    Integer getNumMmCalcMoraObj();

    void setNumMmCalcMoraObj(Integer numMmCalcMoraObj);

    /**
     * Host Variable PCO-DT-ULT-EC-RIV-COLL-DB
     * 
     */
    String getDtUltEcRivCollDb();

    void setDtUltEcRivCollDb(String dtUltEcRivCollDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-RIV-COLL-DB
     * 
     */
    String getDtUltEcRivCollDbObj();

    void setDtUltEcRivCollDbObj(String dtUltEcRivCollDbObj);

    /**
     * Host Variable PCO-DT-ULT-EC-RIV-IND-DB
     * 
     */
    String getDtUltEcRivIndDb();

    void setDtUltEcRivIndDb(String dtUltEcRivIndDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-RIV-IND-DB
     * 
     */
    String getDtUltEcRivIndDbObj();

    void setDtUltEcRivIndDbObj(String dtUltEcRivIndDbObj);

    /**
     * Host Variable PCO-DT-ULT-EC-IL-COLL-DB
     * 
     */
    String getDtUltEcIlCollDb();

    void setDtUltEcIlCollDb(String dtUltEcIlCollDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-IL-COLL-DB
     * 
     */
    String getDtUltEcIlCollDbObj();

    void setDtUltEcIlCollDbObj(String dtUltEcIlCollDbObj);

    /**
     * Host Variable PCO-DT-ULT-EC-IL-IND-DB
     * 
     */
    String getDtUltEcIlIndDb();

    void setDtUltEcIlIndDb(String dtUltEcIlIndDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-IL-IND-DB
     * 
     */
    String getDtUltEcIlIndDbObj();

    void setDtUltEcIlIndDbObj(String dtUltEcIlIndDbObj);

    /**
     * Host Variable PCO-DT-ULT-EC-UL-COLL-DB
     * 
     */
    String getDtUltEcUlCollDb();

    void setDtUltEcUlCollDb(String dtUltEcUlCollDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-UL-COLL-DB
     * 
     */
    String getDtUltEcUlCollDbObj();

    void setDtUltEcUlCollDbObj(String dtUltEcUlCollDbObj);

    /**
     * Host Variable PCO-DT-ULT-EC-UL-IND-DB
     * 
     */
    String getDtUltEcUlIndDb();

    void setDtUltEcUlIndDb(String dtUltEcUlIndDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-UL-IND-DB
     * 
     */
    String getDtUltEcUlIndDbObj();

    void setDtUltEcUlIndDbObj(String dtUltEcUlIndDbObj);

    /**
     * Host Variable PCO-AA-UTI
     * 
     */
    short getAaUti();

    void setAaUti(short aaUti);

    /**
     * Nullable property for PCO-AA-UTI
     * 
     */
    Short getAaUtiObj();

    void setAaUtiObj(Short aaUtiObj);

    /**
     * Host Variable PCO-CALC-RSH-COMUN
     * 
     */
    char getCalcRshComun();

    void setCalcRshComun(char calcRshComun);

    /**
     * Host Variable PCO-FL-LIV-DEBUG
     * 
     */
    short getFlLivDebug();

    void setFlLivDebug(short flLivDebug);

    /**
     * Host Variable PCO-DT-ULT-BOLL-PERF-C-DB
     * 
     */
    String getDtUltBollPerfCDb();

    void setDtUltBollPerfCDb(String dtUltBollPerfCDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-PERF-C-DB
     * 
     */
    String getDtUltBollPerfCDbObj();

    void setDtUltBollPerfCDbObj(String dtUltBollPerfCDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-RSP-IN-DB
     * 
     */
    String getDtUltBollRspInDb();

    void setDtUltBollRspInDb(String dtUltBollRspInDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-RSP-IN-DB
     * 
     */
    String getDtUltBollRspInDbObj();

    void setDtUltBollRspInDbObj(String dtUltBollRspInDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-RSP-CL-DB
     * 
     */
    String getDtUltBollRspClDb();

    void setDtUltBollRspClDb(String dtUltBollRspClDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-RSP-CL-DB
     * 
     */
    String getDtUltBollRspClDbObj();

    void setDtUltBollRspClDbObj(String dtUltBollRspClDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-EMES-I-DB
     * 
     */
    String getDtUltBollEmesIDb();

    void setDtUltBollEmesIDb(String dtUltBollEmesIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-EMES-I-DB
     * 
     */
    String getDtUltBollEmesIDbObj();

    void setDtUltBollEmesIDbObj(String dtUltBollEmesIDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-STOR-I-DB
     * 
     */
    String getDtUltBollStorIDb();

    void setDtUltBollStorIDb(String dtUltBollStorIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-STOR-I-DB
     * 
     */
    String getDtUltBollStorIDbObj();

    void setDtUltBollStorIDbObj(String dtUltBollStorIDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-RIAT-I-DB
     * 
     */
    String getDtUltBollRiatIDb();

    void setDtUltBollRiatIDb(String dtUltBollRiatIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-RIAT-I-DB
     * 
     */
    String getDtUltBollRiatIDbObj();

    void setDtUltBollRiatIDbObj(String dtUltBollRiatIDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-SD-I-DB
     * 
     */
    String getDtUltBollSdIDb();

    void setDtUltBollSdIDb(String dtUltBollSdIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-SD-I-DB
     * 
     */
    String getDtUltBollSdIDbObj();

    void setDtUltBollSdIDbObj(String dtUltBollSdIDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-SDNL-I-DB
     * 
     */
    String getDtUltBollSdnlIDb();

    void setDtUltBollSdnlIDb(String dtUltBollSdnlIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-SDNL-I-DB
     * 
     */
    String getDtUltBollSdnlIDbObj();

    void setDtUltBollSdnlIDbObj(String dtUltBollSdnlIDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-PERF-I-DB
     * 
     */
    String getDtUltBollPerfIDb();

    void setDtUltBollPerfIDb(String dtUltBollPerfIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-PERF-I-DB
     * 
     */
    String getDtUltBollPerfIDbObj();

    void setDtUltBollPerfIDbObj(String dtUltBollPerfIDbObj);

    /**
     * Host Variable PCO-DT-RICL-RIRIAS-COM-DB
     * 
     */
    String getDtRiclRiriasComDb();

    void setDtRiclRiriasComDb(String dtRiclRiriasComDb);

    /**
     * Nullable property for PCO-DT-RICL-RIRIAS-COM-DB
     * 
     */
    String getDtRiclRiriasComDbObj();

    void setDtRiclRiriasComDbObj(String dtRiclRiriasComDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-AT92-C-DB
     * 
     */
    String getDtUltElabAt92CDb();

    void setDtUltElabAt92CDb(String dtUltElabAt92CDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-AT92-C-DB
     * 
     */
    String getDtUltElabAt92CDbObj();

    void setDtUltElabAt92CDbObj(String dtUltElabAt92CDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-AT92-I-DB
     * 
     */
    String getDtUltElabAt92IDb();

    void setDtUltElabAt92IDb(String dtUltElabAt92IDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-AT92-I-DB
     * 
     */
    String getDtUltElabAt92IDbObj();

    void setDtUltElabAt92IDbObj(String dtUltElabAt92IDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-AT93-C-DB
     * 
     */
    String getDtUltElabAt93CDb();

    void setDtUltElabAt93CDb(String dtUltElabAt93CDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-AT93-C-DB
     * 
     */
    String getDtUltElabAt93CDbObj();

    void setDtUltElabAt93CDbObj(String dtUltElabAt93CDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-AT93-I-DB
     * 
     */
    String getDtUltElabAt93IDb();

    void setDtUltElabAt93IDb(String dtUltElabAt93IDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-AT93-I-DB
     * 
     */
    String getDtUltElabAt93IDbObj();

    void setDtUltElabAt93IDbObj(String dtUltElabAt93IDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-SPE-IN-DB
     * 
     */
    String getDtUltElabSpeInDb();

    void setDtUltElabSpeInDb(String dtUltElabSpeInDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-SPE-IN-DB
     * 
     */
    String getDtUltElabSpeInDbObj();

    void setDtUltElabSpeInDbObj(String dtUltElabSpeInDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-PR-CON-DB
     * 
     */
    String getDtUltElabPrConDb();

    void setDtUltElabPrConDb(String dtUltElabPrConDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-PR-CON-DB
     * 
     */
    String getDtUltElabPrConDbObj();

    void setDtUltElabPrConDbObj(String dtUltElabPrConDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-RP-CL-DB
     * 
     */
    String getDtUltBollRpClDb();

    void setDtUltBollRpClDb(String dtUltBollRpClDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-RP-CL-DB
     * 
     */
    String getDtUltBollRpClDbObj();

    void setDtUltBollRpClDbObj(String dtUltBollRpClDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-RP-IN-DB
     * 
     */
    String getDtUltBollRpInDb();

    void setDtUltBollRpInDb(String dtUltBollRpInDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-RP-IN-DB
     * 
     */
    String getDtUltBollRpInDbObj();

    void setDtUltBollRpInDbObj(String dtUltBollRpInDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-PRE-I-DB
     * 
     */
    String getDtUltBollPreIDb();

    void setDtUltBollPreIDb(String dtUltBollPreIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-PRE-I-DB
     * 
     */
    String getDtUltBollPreIDbObj();

    void setDtUltBollPreIDbObj(String dtUltBollPreIDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-PRE-C-DB
     * 
     */
    String getDtUltBollPreCDb();

    void setDtUltBollPreCDb(String dtUltBollPreCDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-PRE-C-DB
     * 
     */
    String getDtUltBollPreCDbObj();

    void setDtUltBollPreCDbObj(String dtUltBollPreCDbObj);

    /**
     * Host Variable PCO-DT-ULTC-PILDI-MM-C-DB
     * 
     */
    String getDtUltcPildiMmCDb();

    void setDtUltcPildiMmCDb(String dtUltcPildiMmCDb);

    /**
     * Nullable property for PCO-DT-ULTC-PILDI-MM-C-DB
     * 
     */
    String getDtUltcPildiMmCDbObj();

    void setDtUltcPildiMmCDbObj(String dtUltcPildiMmCDbObj);

    /**
     * Host Variable PCO-DT-ULTC-PILDI-AA-C-DB
     * 
     */
    String getDtUltcPildiAaCDb();

    void setDtUltcPildiAaCDb(String dtUltcPildiAaCDb);

    /**
     * Nullable property for PCO-DT-ULTC-PILDI-AA-C-DB
     * 
     */
    String getDtUltcPildiAaCDbObj();

    void setDtUltcPildiAaCDbObj(String dtUltcPildiAaCDbObj);

    /**
     * Host Variable PCO-DT-ULTC-PILDI-MM-I-DB
     * 
     */
    String getDtUltcPildiMmIDb();

    void setDtUltcPildiMmIDb(String dtUltcPildiMmIDb);

    /**
     * Nullable property for PCO-DT-ULTC-PILDI-MM-I-DB
     * 
     */
    String getDtUltcPildiMmIDbObj();

    void setDtUltcPildiMmIDbObj(String dtUltcPildiMmIDbObj);

    /**
     * Host Variable PCO-DT-ULTC-PILDI-TR-I-DB
     * 
     */
    String getDtUltcPildiTrIDb();

    void setDtUltcPildiTrIDb(String dtUltcPildiTrIDb);

    /**
     * Nullable property for PCO-DT-ULTC-PILDI-TR-I-DB
     * 
     */
    String getDtUltcPildiTrIDbObj();

    void setDtUltcPildiTrIDbObj(String dtUltcPildiTrIDbObj);

    /**
     * Host Variable PCO-DT-ULTC-PILDI-AA-I-DB
     * 
     */
    String getDtUltcPildiAaIDb();

    void setDtUltcPildiAaIDb(String dtUltcPildiAaIDb);

    /**
     * Nullable property for PCO-DT-ULTC-PILDI-AA-I-DB
     * 
     */
    String getDtUltcPildiAaIDbObj();

    void setDtUltcPildiAaIDbObj(String dtUltcPildiAaIDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-QUIE-C-DB
     * 
     */
    String getDtUltBollQuieCDb();

    void setDtUltBollQuieCDb(String dtUltBollQuieCDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-QUIE-C-DB
     * 
     */
    String getDtUltBollQuieCDbObj();

    void setDtUltBollQuieCDbObj(String dtUltBollQuieCDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-QUIE-I-DB
     * 
     */
    String getDtUltBollQuieIDb();

    void setDtUltBollQuieIDb(String dtUltBollQuieIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-QUIE-I-DB
     * 
     */
    String getDtUltBollQuieIDbObj();

    void setDtUltBollQuieIDbObj(String dtUltBollQuieIDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-COTR-I-DB
     * 
     */
    String getDtUltBollCotrIDb();

    void setDtUltBollCotrIDb(String dtUltBollCotrIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-COTR-I-DB
     * 
     */
    String getDtUltBollCotrIDbObj();

    void setDtUltBollCotrIDbObj(String dtUltBollCotrIDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-COTR-C-DB
     * 
     */
    String getDtUltBollCotrCDb();

    void setDtUltBollCotrCDb(String dtUltBollCotrCDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-COTR-C-DB
     * 
     */
    String getDtUltBollCotrCDbObj();

    void setDtUltBollCotrCDbObj(String dtUltBollCotrCDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-CORI-C-DB
     * 
     */
    String getDtUltBollCoriCDb();

    void setDtUltBollCoriCDb(String dtUltBollCoriCDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-CORI-C-DB
     * 
     */
    String getDtUltBollCoriCDbObj();

    void setDtUltBollCoriCDbObj(String dtUltBollCoriCDbObj);

    /**
     * Host Variable PCO-DT-ULT-BOLL-CORI-I-DB
     * 
     */
    String getDtUltBollCoriIDb();

    void setDtUltBollCoriIDb(String dtUltBollCoriIDb);

    /**
     * Nullable property for PCO-DT-ULT-BOLL-CORI-I-DB
     * 
     */
    String getDtUltBollCoriIDbObj();

    void setDtUltBollCoriIDbObj(String dtUltBollCoriIDbObj);

    /**
     * Host Variable PCO-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable PCO-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable PCO-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable PCO-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable PCO-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable PCO-TP-VALZZ-DT-VLT
     * 
     */
    String getTpValzzDtVlt();

    void setTpValzzDtVlt(String tpValzzDtVlt);

    /**
     * Nullable property for PCO-TP-VALZZ-DT-VLT
     * 
     */
    String getTpValzzDtVltObj();

    void setTpValzzDtVltObj(String tpValzzDtVltObj);

    /**
     * Host Variable PCO-FL-FRAZ-PROV-ACQ
     * 
     */
    char getFlFrazProvAcq();

    void setFlFrazProvAcq(char flFrazProvAcq);

    /**
     * Nullable property for PCO-FL-FRAZ-PROV-ACQ
     * 
     */
    Character getFlFrazProvAcqObj();

    void setFlFrazProvAcqObj(Character flFrazProvAcqObj);

    /**
     * Host Variable PCO-DT-ULT-AGG-EROG-RE-DB
     * 
     */
    String getDtUltAggErogReDb();

    void setDtUltAggErogReDb(String dtUltAggErogReDb);

    /**
     * Nullable property for PCO-DT-ULT-AGG-EROG-RE-DB
     * 
     */
    String getDtUltAggErogReDbObj();

    void setDtUltAggErogReDbObj(String dtUltAggErogReDbObj);

    /**
     * Host Variable PCO-PC-RM-MARSOL
     * 
     */
    AfDecimal getPcRmMarsol();

    void setPcRmMarsol(AfDecimal pcRmMarsol);

    /**
     * Nullable property for PCO-PC-RM-MARSOL
     * 
     */
    AfDecimal getPcRmMarsolObj();

    void setPcRmMarsolObj(AfDecimal pcRmMarsolObj);

    /**
     * Host Variable PCO-PC-C-SUBRSH-MARSOL
     * 
     */
    AfDecimal getPcCSubrshMarsol();

    void setPcCSubrshMarsol(AfDecimal pcCSubrshMarsol);

    /**
     * Nullable property for PCO-PC-C-SUBRSH-MARSOL
     * 
     */
    AfDecimal getPcCSubrshMarsolObj();

    void setPcCSubrshMarsolObj(AfDecimal pcCSubrshMarsolObj);

    /**
     * Host Variable PCO-COD-COMP-ISVAP
     * 
     */
    String getCodCompIsvap();

    void setCodCompIsvap(String codCompIsvap);

    /**
     * Nullable property for PCO-COD-COMP-ISVAP
     * 
     */
    String getCodCompIsvapObj();

    void setCodCompIsvapObj(String codCompIsvapObj);

    /**
     * Host Variable PCO-LM-RIS-CON-INT
     * 
     */
    AfDecimal getLmRisConInt();

    void setLmRisConInt(AfDecimal lmRisConInt);

    /**
     * Nullable property for PCO-LM-RIS-CON-INT
     * 
     */
    AfDecimal getLmRisConIntObj();

    void setLmRisConIntObj(AfDecimal lmRisConIntObj);

    /**
     * Host Variable PCO-LM-C-SUBRSH-CON-IN
     * 
     */
    AfDecimal getLmCSubrshConIn();

    void setLmCSubrshConIn(AfDecimal lmCSubrshConIn);

    /**
     * Nullable property for PCO-LM-C-SUBRSH-CON-IN
     * 
     */
    AfDecimal getLmCSubrshConInObj();

    void setLmCSubrshConInObj(AfDecimal lmCSubrshConInObj);

    /**
     * Host Variable PCO-PC-GAR-NORISK-MARS
     * 
     */
    AfDecimal getPcGarNoriskMars();

    void setPcGarNoriskMars(AfDecimal pcGarNoriskMars);

    /**
     * Nullable property for PCO-PC-GAR-NORISK-MARS
     * 
     */
    AfDecimal getPcGarNoriskMarsObj();

    void setPcGarNoriskMarsObj(AfDecimal pcGarNoriskMarsObj);

    /**
     * Host Variable PCO-CRZ-1A-RAT-INTR-PR
     * 
     */
    char getCrz1aRatIntrPr();

    void setCrz1aRatIntrPr(char crz1aRatIntrPr);

    /**
     * Nullable property for PCO-CRZ-1A-RAT-INTR-PR
     * 
     */
    Character getCrz1aRatIntrPrObj();

    void setCrz1aRatIntrPrObj(Character crz1aRatIntrPrObj);

    /**
     * Host Variable PCO-NUM-GG-ARR-INTR-PR
     * 
     */
    int getNumGgArrIntrPr();

    void setNumGgArrIntrPr(int numGgArrIntrPr);

    /**
     * Nullable property for PCO-NUM-GG-ARR-INTR-PR
     * 
     */
    Integer getNumGgArrIntrPrObj();

    void setNumGgArrIntrPrObj(Integer numGgArrIntrPrObj);

    /**
     * Host Variable PCO-FL-VISUAL-VINPG
     * 
     */
    char getFlVisualVinpg();

    void setFlVisualVinpg(char flVisualVinpg);

    /**
     * Nullable property for PCO-FL-VISUAL-VINPG
     * 
     */
    Character getFlVisualVinpgObj();

    void setFlVisualVinpgObj(Character flVisualVinpgObj);

    /**
     * Host Variable PCO-DT-ULT-ESTRAZ-FUG-DB
     * 
     */
    String getDtUltEstrazFugDb();

    void setDtUltEstrazFugDb(String dtUltEstrazFugDb);

    /**
     * Nullable property for PCO-DT-ULT-ESTRAZ-FUG-DB
     * 
     */
    String getDtUltEstrazFugDbObj();

    void setDtUltEstrazFugDbObj(String dtUltEstrazFugDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-PR-AUT-DB
     * 
     */
    String getDtUltElabPrAutDb();

    void setDtUltElabPrAutDb(String dtUltElabPrAutDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-PR-AUT-DB
     * 
     */
    String getDtUltElabPrAutDbObj();

    void setDtUltElabPrAutDbObj(String dtUltElabPrAutDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-COMMEF-DB
     * 
     */
    String getDtUltElabCommefDb();

    void setDtUltElabCommefDb(String dtUltElabCommefDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-COMMEF-DB
     * 
     */
    String getDtUltElabCommefDbObj();

    void setDtUltElabCommefDbObj(String dtUltElabCommefDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-LIQMEF-DB
     * 
     */
    String getDtUltElabLiqmefDb();

    void setDtUltElabLiqmefDb(String dtUltElabLiqmefDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-LIQMEF-DB
     * 
     */
    String getDtUltElabLiqmefDbObj();

    void setDtUltElabLiqmefDbObj(String dtUltElabLiqmefDbObj);

    /**
     * Host Variable PCO-COD-FISC-MEF
     * 
     */
    String getCodFiscMef();

    void setCodFiscMef(String codFiscMef);

    /**
     * Nullable property for PCO-COD-FISC-MEF
     * 
     */
    String getCodFiscMefObj();

    void setCodFiscMefObj(String codFiscMefObj);

    /**
     * Host Variable PCO-IMP-ASS-SOCIALE
     * 
     */
    AfDecimal getImpAssSociale();

    void setImpAssSociale(AfDecimal impAssSociale);

    /**
     * Nullable property for PCO-IMP-ASS-SOCIALE
     * 
     */
    AfDecimal getImpAssSocialeObj();

    void setImpAssSocialeObj(AfDecimal impAssSocialeObj);

    /**
     * Host Variable PCO-MOD-COMNZ-INVST-SW
     * 
     */
    char getModComnzInvstSw();

    void setModComnzInvstSw(char modComnzInvstSw);

    /**
     * Nullable property for PCO-MOD-COMNZ-INVST-SW
     * 
     */
    Character getModComnzInvstSwObj();

    void setModComnzInvstSwObj(Character modComnzInvstSwObj);

    /**
     * Host Variable PCO-DT-RIAT-RIASS-RSH-DB
     * 
     */
    String getDtRiatRiassRshDb();

    void setDtRiatRiassRshDb(String dtRiatRiassRshDb);

    /**
     * Nullable property for PCO-DT-RIAT-RIASS-RSH-DB
     * 
     */
    String getDtRiatRiassRshDbObj();

    void setDtRiatRiassRshDbObj(String dtRiatRiassRshDbObj);

    /**
     * Host Variable PCO-DT-RIAT-RIASS-COMM-DB
     * 
     */
    String getDtRiatRiassCommDb();

    void setDtRiatRiassCommDb(String dtRiatRiassCommDb);

    /**
     * Nullable property for PCO-DT-RIAT-RIASS-COMM-DB
     * 
     */
    String getDtRiatRiassCommDbObj();

    void setDtRiatRiassCommDbObj(String dtRiatRiassCommDbObj);

    /**
     * Host Variable PCO-GG-INTR-RIT-PAG
     * 
     */
    int getGgIntrRitPag();

    void setGgIntrRitPag(int ggIntrRitPag);

    /**
     * Nullable property for PCO-GG-INTR-RIT-PAG
     * 
     */
    Integer getGgIntrRitPagObj();

    void setGgIntrRitPagObj(Integer ggIntrRitPagObj);

    /**
     * Host Variable PCO-DT-ULT-RINN-TAC-DB
     * 
     */
    String getDtUltRinnTacDb();

    void setDtUltRinnTacDb(String dtUltRinnTacDb);

    /**
     * Nullable property for PCO-DT-ULT-RINN-TAC-DB
     * 
     */
    String getDtUltRinnTacDbObj();

    void setDtUltRinnTacDbObj(String dtUltRinnTacDbObj);

    /**
     * Host Variable PCO-DESC-COMP-VCHAR
     * 
     */
    String getDescCompVchar();

    void setDescCompVchar(String descCompVchar);

    /**
     * Nullable property for PCO-DESC-COMP-VCHAR
     * 
     */
    String getDescCompVcharObj();

    void setDescCompVcharObj(String descCompVcharObj);

    /**
     * Host Variable PCO-DT-ULT-EC-TCM-IND-DB
     * 
     */
    String getDtUltEcTcmIndDb();

    void setDtUltEcTcmIndDb(String dtUltEcTcmIndDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-TCM-IND-DB
     * 
     */
    String getDtUltEcTcmIndDbObj();

    void setDtUltEcTcmIndDbObj(String dtUltEcTcmIndDbObj);

    /**
     * Host Variable PCO-DT-ULT-EC-TCM-COLL-DB
     * 
     */
    String getDtUltEcTcmCollDb();

    void setDtUltEcTcmCollDb(String dtUltEcTcmCollDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-TCM-COLL-DB
     * 
     */
    String getDtUltEcTcmCollDbObj();

    void setDtUltEcTcmCollDbObj(String dtUltEcTcmCollDbObj);

    /**
     * Host Variable PCO-DT-ULT-EC-MRM-IND-DB
     * 
     */
    String getDtUltEcMrmIndDb();

    void setDtUltEcMrmIndDb(String dtUltEcMrmIndDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-MRM-IND-DB
     * 
     */
    String getDtUltEcMrmIndDbObj();

    void setDtUltEcMrmIndDbObj(String dtUltEcMrmIndDbObj);

    /**
     * Host Variable PCO-DT-ULT-EC-MRM-COLL-DB
     * 
     */
    String getDtUltEcMrmCollDb();

    void setDtUltEcMrmCollDb(String dtUltEcMrmCollDb);

    /**
     * Nullable property for PCO-DT-ULT-EC-MRM-COLL-DB
     * 
     */
    String getDtUltEcMrmCollDbObj();

    void setDtUltEcMrmCollDbObj(String dtUltEcMrmCollDbObj);

    /**
     * Host Variable PCO-COD-COMP-LDAP
     * 
     */
    String getCodCompLdap();

    void setCodCompLdap(String codCompLdap);

    /**
     * Nullable property for PCO-COD-COMP-LDAP
     * 
     */
    String getCodCompLdapObj();

    void setCodCompLdapObj(String codCompLdapObj);

    /**
     * Host Variable PCO-PC-RID-IMP-1382011
     * 
     */
    AfDecimal getPcRidImp1382011();

    void setPcRidImp1382011(AfDecimal pcRidImp1382011);

    /**
     * Nullable property for PCO-PC-RID-IMP-1382011
     * 
     */
    AfDecimal getPcRidImp1382011Obj();

    void setPcRidImp1382011Obj(AfDecimal pcRidImp1382011Obj);

    /**
     * Host Variable PCO-PC-RID-IMP-662014
     * 
     */
    AfDecimal getPcRidImp662014();

    void setPcRidImp662014(AfDecimal pcRidImp662014);

    /**
     * Nullable property for PCO-PC-RID-IMP-662014
     * 
     */
    AfDecimal getPcRidImp662014Obj();

    void setPcRidImp662014Obj(AfDecimal pcRidImp662014Obj);

    /**
     * Host Variable PCO-SOGL-AML-PRE-UNI
     * 
     */
    AfDecimal getSoglAmlPreUni();

    void setSoglAmlPreUni(AfDecimal soglAmlPreUni);

    /**
     * Nullable property for PCO-SOGL-AML-PRE-UNI
     * 
     */
    AfDecimal getSoglAmlPreUniObj();

    void setSoglAmlPreUniObj(AfDecimal soglAmlPreUniObj);

    /**
     * Host Variable PCO-SOGL-AML-PRE-PER
     * 
     */
    AfDecimal getSoglAmlPrePer();

    void setSoglAmlPrePer(AfDecimal soglAmlPrePer);

    /**
     * Nullable property for PCO-SOGL-AML-PRE-PER
     * 
     */
    AfDecimal getSoglAmlPrePerObj();

    void setSoglAmlPrePerObj(AfDecimal soglAmlPrePerObj);

    /**
     * Host Variable PCO-COD-SOGG-FTZ-ASSTO
     * 
     */
    String getCodSoggFtzAssto();

    void setCodSoggFtzAssto(String codSoggFtzAssto);

    /**
     * Nullable property for PCO-COD-SOGG-FTZ-ASSTO
     * 
     */
    String getCodSoggFtzAsstoObj();

    void setCodSoggFtzAsstoObj(String codSoggFtzAsstoObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-REDPRO-DB
     * 
     */
    String getDtUltElabRedproDb();

    void setDtUltElabRedproDb(String dtUltElabRedproDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-REDPRO-DB
     * 
     */
    String getDtUltElabRedproDbObj();

    void setDtUltElabRedproDbObj(String dtUltElabRedproDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-TAKE-P-DB
     * 
     */
    String getDtUltElabTakePDb();

    void setDtUltElabTakePDb(String dtUltElabTakePDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-TAKE-P-DB
     * 
     */
    String getDtUltElabTakePDbObj();

    void setDtUltElabTakePDbObj(String dtUltElabTakePDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-PASPAS-DB
     * 
     */
    String getDtUltElabPaspasDb();

    void setDtUltElabPaspasDb(String dtUltElabPaspasDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-PASPAS-DB
     * 
     */
    String getDtUltElabPaspasDbObj();

    void setDtUltElabPaspasDbObj(String dtUltElabPaspasDbObj);

    /**
     * Host Variable PCO-SOGL-AML-PRE-SAV-R
     * 
     */
    AfDecimal getSoglAmlPreSavR();

    void setSoglAmlPreSavR(AfDecimal soglAmlPreSavR);

    /**
     * Nullable property for PCO-SOGL-AML-PRE-SAV-R
     * 
     */
    AfDecimal getSoglAmlPreSavRObj();

    void setSoglAmlPreSavRObj(AfDecimal soglAmlPreSavRObj);

    /**
     * Host Variable PCO-DT-ULT-ESTR-DEC-CO-DB
     * 
     */
    String getDtUltEstrDecCoDb();

    void setDtUltEstrDecCoDb(String dtUltEstrDecCoDb);

    /**
     * Nullable property for PCO-DT-ULT-ESTR-DEC-CO-DB
     * 
     */
    String getDtUltEstrDecCoDbObj();

    void setDtUltEstrDecCoDbObj(String dtUltEstrDecCoDbObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-COS-AT-DB
     * 
     */
    String getDtUltElabCosAtDb();

    void setDtUltElabCosAtDb(String dtUltElabCosAtDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-COS-AT-DB
     * 
     */
    String getDtUltElabCosAtDbObj();

    void setDtUltElabCosAtDbObj(String dtUltElabCosAtDbObj);

    /**
     * Host Variable PCO-FRQ-COSTI-ATT
     * 
     */
    int getFrqCostiAtt();

    void setFrqCostiAtt(int frqCostiAtt);

    /**
     * Nullable property for PCO-FRQ-COSTI-ATT
     * 
     */
    Integer getFrqCostiAttObj();

    void setFrqCostiAttObj(Integer frqCostiAttObj);

    /**
     * Host Variable PCO-DT-ULT-ELAB-COS-ST-DB
     * 
     */
    String getDtUltElabCosStDb();

    void setDtUltElabCosStDb(String dtUltElabCosStDb);

    /**
     * Nullable property for PCO-DT-ULT-ELAB-COS-ST-DB
     * 
     */
    String getDtUltElabCosStDbObj();

    void setDtUltElabCosStDbObj(String dtUltElabCosStDbObj);

    /**
     * Host Variable PCO-FRQ-COSTI-STORNATI
     * 
     */
    int getFrqCostiStornati();

    void setFrqCostiStornati(int frqCostiStornati);

    /**
     * Nullable property for PCO-FRQ-COSTI-STORNATI
     * 
     */
    Integer getFrqCostiStornatiObj();

    void setFrqCostiStornatiObj(Integer frqCostiStornatiObj);

    /**
     * Host Variable PCO-DT-ESTR-ASS-MIN70A-DB
     * 
     */
    String getDtEstrAssMin70aDb();

    void setDtEstrAssMin70aDb(String dtEstrAssMin70aDb);

    /**
     * Nullable property for PCO-DT-ESTR-ASS-MIN70A-DB
     * 
     */
    String getDtEstrAssMin70aDbObj();

    void setDtEstrAssMin70aDbObj(String dtEstrAssMin70aDbObj);

    /**
     * Host Variable PCO-DT-ESTR-ASS-MAG70A-DB
     * 
     */
    String getDtEstrAssMag70aDb();

    void setDtEstrAssMag70aDb(String dtEstrAssMag70aDb);

    /**
     * Nullable property for PCO-DT-ESTR-ASS-MAG70A-DB
     * 
     */
    String getDtEstrAssMag70aDbObj();

    void setDtEstrAssMag70aDbObj(String dtEstrAssMag70aDbObj);
};
