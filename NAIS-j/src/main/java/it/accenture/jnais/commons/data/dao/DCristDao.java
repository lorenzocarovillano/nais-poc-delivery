package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDCrist;

/**
 * Data Access Object(DAO) for table [D_CRIST]
 * 
 */
public class DCristDao extends BaseSqlDao<IDCrist> {

    private Cursor cIdUpdEffP61;
    private Cursor cIdpEffP61;
    private Cursor cIdpCpzP61;
    private Cursor cEff47;
    private Cursor cCpz47;
    private Cursor cEff49;
    private Cursor cCpz49;
    private final IRowMapper<IDCrist> selectByP61DsRigaRm = buildNamedRowMapper(IDCrist.class, "idDCrist", "idPoli", "codCompAnia", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codProd", "dtDecorDb", "p61DsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "risMat31122011Obj", "preV31122011Obj", "preRshV31122011Obj", "cptRivto31122011Obj", "impbVis31122011Obj", "impbIs31122011Obj", "impbVisRpP2011Obj", "impbIsRpP2011Obj", "preV30062014Obj", "preRshV30062014Obj", "cptIni30062014Obj", "impbVis30062014Obj", "impbIs30062014Obj", "impbVisRpP62014Obj", "impbIsRpP62014Obj", "risMat30062014Obj", "idAdesObj", "montLrdEnd2000Obj", "preLrdEnd2000Obj", "rendtoLrdEnd2000Obj", "montLrdEnd2006Obj", "preLrdEnd2006Obj", "rendtoLrdEnd2006Obj", "montLrdDal2007Obj", "preLrdDal2007Obj", "rendtoLrdDal2007Obj", "idTrchDiGarObj");

    public DCristDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDCrist> getToClass() {
        return IDCrist.class;
    }

    public IDCrist selectByP61DsRiga(long p61DsRiga, IDCrist iDCrist) {
        return buildQuery("selectByP61DsRiga").bind("p61DsRiga", p61DsRiga).rowMapper(selectByP61DsRigaRm).singleResult(iDCrist);
    }

    public DbAccessStatus insertRec(IDCrist iDCrist) {
        return buildQuery("insertRec").bind(iDCrist).executeInsert();
    }

    public DbAccessStatus updateRec(IDCrist iDCrist) {
        return buildQuery("updateRec").bind(iDCrist).executeUpdate();
    }

    public DbAccessStatus deleteByP61DsRiga(long p61DsRiga) {
        return buildQuery("deleteByP61DsRiga").bind("p61DsRiga", p61DsRiga).executeDelete();
    }

    public IDCrist selectRec(int p61IdDCrist, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDCrist iDCrist) {
        return buildQuery("selectRec").bind("p61IdDCrist", p61IdDCrist).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP61DsRigaRm).singleResult(iDCrist);
    }

    public DbAccessStatus updateRec1(IDCrist iDCrist) {
        return buildQuery("updateRec1").bind(iDCrist).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffP61(int p61IdDCrist, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffP61 = buildQuery("openCIdUpdEffP61").bind("p61IdDCrist", p61IdDCrist).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffP61() {
        return closeCursor(cIdUpdEffP61);
    }

    public IDCrist fetchCIdUpdEffP61(IDCrist iDCrist) {
        return fetch(cIdUpdEffP61, iDCrist, selectByP61DsRigaRm);
    }

    public IDCrist selectRec1(int p61IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDCrist iDCrist) {
        return buildQuery("selectRec1").bind("p61IdPoli", p61IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP61DsRigaRm).singleResult(iDCrist);
    }

    public DbAccessStatus openCIdpEffP61(int p61IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffP61 = buildQuery("openCIdpEffP61").bind("p61IdPoli", p61IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffP61() {
        return closeCursor(cIdpEffP61);
    }

    public IDCrist fetchCIdpEffP61(IDCrist iDCrist) {
        return fetch(cIdpEffP61, iDCrist, selectByP61DsRigaRm);
    }

    public IDCrist selectRec2(int p61IdDCrist, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDCrist iDCrist) {
        return buildQuery("selectRec2").bind("p61IdDCrist", p61IdDCrist).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP61DsRigaRm).singleResult(iDCrist);
    }

    public IDCrist selectRec3(int p61IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDCrist iDCrist) {
        return buildQuery("selectRec3").bind("p61IdPoli", p61IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP61DsRigaRm).singleResult(iDCrist);
    }

    public DbAccessStatus openCIdpCpzP61(int p61IdPoli, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzP61 = buildQuery("openCIdpCpzP61").bind("p61IdPoli", p61IdPoli).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzP61() {
        return closeCursor(cIdpCpzP61);
    }

    public IDCrist fetchCIdpCpzP61(IDCrist iDCrist) {
        return fetch(cIdpCpzP61, iDCrist, selectByP61DsRigaRm);
    }

    public IDCrist selectRec4(int p61IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDCrist iDCrist) {
        return buildQuery("selectRec4").bind("p61IdTrchDiGar", p61IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP61DsRigaRm).singleResult(iDCrist);
    }

    public DbAccessStatus openCEff47(int p61IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff47 = buildQuery("openCEff47").bind("p61IdTrchDiGar", p61IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff47() {
        return closeCursor(cEff47);
    }

    public IDCrist fetchCEff47(IDCrist iDCrist) {
        return fetch(cEff47, iDCrist, selectByP61DsRigaRm);
    }

    public IDCrist selectRec5(int p61IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDCrist iDCrist) {
        return buildQuery("selectRec5").bind("p61IdTrchDiGar", p61IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP61DsRigaRm).singleResult(iDCrist);
    }

    public DbAccessStatus openCCpz47(int p61IdTrchDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cCpz47 = buildQuery("openCCpz47").bind("p61IdTrchDiGar", p61IdTrchDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz47() {
        return closeCursor(cCpz47);
    }

    public IDCrist fetchCCpz47(IDCrist iDCrist) {
        return fetch(cCpz47, iDCrist, selectByP61DsRigaRm);
    }

    public IDCrist selectRec6(int p61IdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDCrist iDCrist) {
        return buildQuery("selectRec6").bind("p61IdAdes", p61IdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByP61DsRigaRm).singleResult(iDCrist);
    }

    public DbAccessStatus openCEff49(int p61IdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cEff49 = buildQuery("openCEff49").bind("p61IdAdes", p61IdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff49() {
        return closeCursor(cEff49);
    }

    public IDCrist fetchCEff49(IDCrist iDCrist) {
        return fetch(cEff49, iDCrist, selectByP61DsRigaRm);
    }

    public IDCrist selectRec7(int p61IdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDCrist iDCrist) {
        return buildQuery("selectRec7").bind("p61IdAdes", p61IdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByP61DsRigaRm).singleResult(iDCrist);
    }

    public DbAccessStatus openCCpz49(int p61IdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cCpz49 = buildQuery("openCCpz49").bind("p61IdAdes", p61IdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz49() {
        return closeCursor(cCpz49);
    }

    public IDCrist fetchCCpz49(IDCrist iDCrist) {
        return fetch(cCpz49, iDCrist, selectByP61DsRigaRm);
    }
}
