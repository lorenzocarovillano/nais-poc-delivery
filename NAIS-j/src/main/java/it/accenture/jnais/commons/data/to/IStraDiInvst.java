package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [STRA_DI_INVST]
 * 
 */
public interface IStraDiInvst extends BaseSqlTo {

    /**
     * Host Variable SDI-ID-OGG
     * 
     */
    int getSdiIdOgg();

    void setSdiIdOgg(int sdiIdOgg);

    /**
     * Host Variable SDI-TP-OGG
     * 
     */
    String getSdiTpOgg();

    void setSdiTpOgg(String sdiTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable SDI-ID-STRA-DI-INVST
     * 
     */
    int getIdStraDiInvst();

    void setIdStraDiInvst(int idStraDiInvst);

    /**
     * Host Variable SDI-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable SDI-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for SDI-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable SDI-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable SDI-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable SDI-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable SDI-COD-STRA
     * 
     */
    String getCodStra();

    void setCodStra(String codStra);

    /**
     * Nullable property for SDI-COD-STRA
     * 
     */
    String getCodStraObj();

    void setCodStraObj(String codStraObj);

    /**
     * Host Variable SDI-MOD-GEST
     * 
     */
    short getModGest();

    void setModGest(short modGest);

    /**
     * Nullable property for SDI-MOD-GEST
     * 
     */
    Short getModGestObj();

    void setModGestObj(Short modGestObj);

    /**
     * Host Variable SDI-VAR-RIFTO
     * 
     */
    String getVarRifto();

    void setVarRifto(String varRifto);

    /**
     * Nullable property for SDI-VAR-RIFTO
     * 
     */
    String getVarRiftoObj();

    void setVarRiftoObj(String varRiftoObj);

    /**
     * Host Variable SDI-VAL-VAR-RIFTO
     * 
     */
    String getValVarRifto();

    void setValVarRifto(String valVarRifto);

    /**
     * Nullable property for SDI-VAL-VAR-RIFTO
     * 
     */
    String getValVarRiftoObj();

    void setValVarRiftoObj(String valVarRiftoObj);

    /**
     * Host Variable SDI-DT-FIS
     * 
     */
    short getDtFis();

    void setDtFis(short dtFis);

    /**
     * Nullable property for SDI-DT-FIS
     * 
     */
    Short getDtFisObj();

    void setDtFisObj(Short dtFisObj);

    /**
     * Host Variable SDI-FRQ-VALUT
     * 
     */
    int getFrqValut();

    void setFrqValut(int frqValut);

    /**
     * Nullable property for SDI-FRQ-VALUT
     * 
     */
    Integer getFrqValutObj();

    void setFrqValutObj(Integer frqValutObj);

    /**
     * Host Variable SDI-FL-INI-END-PER
     * 
     */
    char getFlIniEndPer();

    void setFlIniEndPer(char flIniEndPer);

    /**
     * Nullable property for SDI-FL-INI-END-PER
     * 
     */
    Character getFlIniEndPerObj();

    void setFlIniEndPerObj(Character flIniEndPerObj);

    /**
     * Host Variable SDI-DS-RIGA
     * 
     */
    long getSdiDsRiga();

    void setSdiDsRiga(long sdiDsRiga);

    /**
     * Host Variable SDI-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable SDI-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable SDI-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable SDI-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable SDI-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable SDI-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable SDI-TP-STRA
     * 
     */
    String getTpStra();

    void setTpStra(String tpStra);

    /**
     * Nullable property for SDI-TP-STRA
     * 
     */
    String getTpStraObj();

    void setTpStraObj(String tpStraObj);

    /**
     * Host Variable SDI-TP-PROVZA-STRA
     * 
     */
    String getTpProvzaStra();

    void setTpProvzaStra(String tpProvzaStra);

    /**
     * Nullable property for SDI-TP-PROVZA-STRA
     * 
     */
    String getTpProvzaStraObj();

    void setTpProvzaStraObj(String tpProvzaStraObj);

    /**
     * Host Variable SDI-PC-PROTEZIONE
     * 
     */
    AfDecimal getPcProtezione();

    void setPcProtezione(AfDecimal pcProtezione);

    /**
     * Nullable property for SDI-PC-PROTEZIONE
     * 
     */
    AfDecimal getPcProtezioneObj();

    void setPcProtezioneObj(AfDecimal pcProtezioneObj);
};
