package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IDettTitCont;

/**
 * Data Access Object(DAO) for table [DETT_TIT_CONT]
 * 
 */
public class DettTitContDao extends BaseSqlDao<IDettTitCont> {

    private Cursor cIdUpdEffDtc;
    private Cursor cIdpEffDtc;
    private Cursor cIdoEffDtc;
    private Cursor cIdpCpzDtc;
    private Cursor cIdoCpzDtc;
    private Cursor cEff10;
    private Cursor cCpz10;
    private Cursor cEff11;
    private Cursor cCpz11;
    private Cursor cEff13;
    private Cursor cCpz13;
    private Cursor cEff30;
    private Cursor cCpz30;
    private Cursor cEff56;
    private final IRowMapper<IDettTitCont> selectByDtcDsRigaRm = buildNamedRowMapper(IDettTitCont.class, "idDettTitCont", "idTitCont", "dtcIdOgg", "dtcTpOgg", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "dtcDtIniCopDbObj", "dtEndCopDbObj", "preNetObj", "intrFrazObj", "intrMoraObj", "intrRetdtObj", "intrRiatObj", "dirObj", "speMedObj", "taxObj", "soprSanObj", "soprSpoObj", "soprTecObj", "soprProfObj", "soprAltObj", "preTotObj", "prePpIasObj", "preSoloRshObj", "carAcqObj", "carGestObj", "carIncObj", "provAcq1aaObj", "provAcq2aaObj", "provRicorObj", "provIncObj", "provDaRecObj", "codDvsObj", "frqMoviObj", "tpRgmFisc", "codTariObj", "dtcTpStatTit", "impAzObj", "impAderObj", "impTfrObj", "impVoloObj", "manfeeAnticObj", "manfeeRicorObj", "manfeeRecObj", "dtEsiTitDbObj", "speAgeObj", "carIasObj", "totIntrPrestObj", "dtcDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "impTrasfeObj", "impTfrStrcObj", "numGgRitardoPagObj", "numGgRivalObj", "acqExpObj", "remunAssObj", "commisInterObj", "cnbtAntiracObj");
    private final IRowMapper<IDettTitCont> fetchCEff10Rm = buildNamedRowMapper(IDettTitCont.class, "ldbv2131TotPremiObj");
    private final IRowMapper<IDettTitCont> selectRec14Rm = buildNamedRowMapper(IDettTitCont.class, "dtEsiTitDbObj");

    public DettTitContDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IDettTitCont> getToClass() {
        return IDettTitCont.class;
    }

    public IDettTitCont selectByDtcDsRiga(long dtcDsRiga, IDettTitCont iDettTitCont) {
        return buildQuery("selectByDtcDsRiga").bind("dtcDsRiga", dtcDsRiga).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus insertRec(IDettTitCont iDettTitCont) {
        return buildQuery("insertRec").bind(iDettTitCont).executeInsert();
    }

    public DbAccessStatus updateRec(IDettTitCont iDettTitCont) {
        return buildQuery("updateRec").bind(iDettTitCont).executeUpdate();
    }

    public DbAccessStatus deleteByDtcDsRiga(long dtcDsRiga) {
        return buildQuery("deleteByDtcDsRiga").bind("dtcDsRiga", dtcDsRiga).executeDelete();
    }

    public IDettTitCont selectRec(int dtcIdDettTitCont, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDettTitCont iDettTitCont) {
        return buildQuery("selectRec").bind("dtcIdDettTitCont", dtcIdDettTitCont).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus updateRec1(IDettTitCont iDettTitCont) {
        return buildQuery("updateRec1").bind(iDettTitCont).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffDtc(int dtcIdDettTitCont, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffDtc = buildQuery("openCIdUpdEffDtc").bind("dtcIdDettTitCont", dtcIdDettTitCont).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffDtc() {
        return closeCursor(cIdUpdEffDtc);
    }

    public IDettTitCont fetchCIdUpdEffDtc(IDettTitCont iDettTitCont) {
        return fetch(cIdUpdEffDtc, iDettTitCont, selectByDtcDsRigaRm);
    }

    public IDettTitCont selectRec1(int dtcIdTitCont, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDettTitCont iDettTitCont) {
        return buildQuery("selectRec1").bind("dtcIdTitCont", dtcIdTitCont).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCIdpEffDtc(int dtcIdTitCont, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffDtc = buildQuery("openCIdpEffDtc").bind("dtcIdTitCont", dtcIdTitCont).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffDtc() {
        return closeCursor(cIdpEffDtc);
    }

    public IDettTitCont fetchCIdpEffDtc(IDettTitCont iDettTitCont) {
        return fetch(cIdpEffDtc, iDettTitCont, selectByDtcDsRigaRm);
    }

    public IDettTitCont selectRec2(int dtcIdOgg, String dtcTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IDettTitCont iDettTitCont) {
        return buildQuery("selectRec2").bind("dtcIdOgg", dtcIdOgg).bind("dtcTpOgg", dtcTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCIdoEffDtc(int dtcIdOgg, String dtcTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffDtc = buildQuery("openCIdoEffDtc").bind("dtcIdOgg", dtcIdOgg).bind("dtcTpOgg", dtcTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffDtc() {
        return closeCursor(cIdoEffDtc);
    }

    public IDettTitCont fetchCIdoEffDtc(IDettTitCont iDettTitCont) {
        return fetch(cIdoEffDtc, iDettTitCont, selectByDtcDsRigaRm);
    }

    public IDettTitCont selectRec3(int dtcIdDettTitCont, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDettTitCont iDettTitCont) {
        return buildQuery("selectRec3").bind("dtcIdDettTitCont", dtcIdDettTitCont).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public IDettTitCont selectRec4(int dtcIdTitCont, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IDettTitCont iDettTitCont) {
        return buildQuery("selectRec4").bind("dtcIdTitCont", dtcIdTitCont).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCIdpCpzDtc(int dtcIdTitCont, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzDtc = buildQuery("openCIdpCpzDtc").bind("dtcIdTitCont", dtcIdTitCont).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzDtc() {
        return closeCursor(cIdpCpzDtc);
    }

    public IDettTitCont fetchCIdpCpzDtc(IDettTitCont iDettTitCont) {
        return fetch(cIdpCpzDtc, iDettTitCont, selectByDtcDsRigaRm);
    }

    public IDettTitCont selectRec5(IDettTitCont iDettTitCont) {
        return buildQuery("selectRec5").bind(iDettTitCont).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCIdoCpzDtc(IDettTitCont iDettTitCont) {
        cIdoCpzDtc = buildQuery("openCIdoCpzDtc").bind(iDettTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzDtc() {
        return closeCursor(cIdoCpzDtc);
    }

    public IDettTitCont fetchCIdoCpzDtc(IDettTitCont iDettTitCont) {
        return fetch(cIdoCpzDtc, iDettTitCont, selectByDtcDsRigaRm);
    }

    public DbAccessStatus openCEff10(IDettTitCont iDettTitCont) {
        cEff10 = buildQuery("openCEff10").bind(iDettTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff10() {
        return closeCursor(cEff10);
    }

    public IDettTitCont fetchCEff10(IDettTitCont iDettTitCont) {
        return fetch(cEff10, iDettTitCont, fetchCEff10Rm);
    }

    public DbAccessStatus openCCpz10(IDettTitCont iDettTitCont) {
        cCpz10 = buildQuery("openCCpz10").bind(iDettTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz10() {
        return closeCursor(cCpz10);
    }

    public IDettTitCont fetchCCpz10(IDettTitCont iDettTitCont) {
        return fetch(cCpz10, iDettTitCont, fetchCEff10Rm);
    }

    public IDettTitCont selectRec6(IDettTitCont iDettTitCont) {
        return buildQuery("selectRec6").bind(iDettTitCont).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCEff11(IDettTitCont iDettTitCont) {
        cEff11 = buildQuery("openCEff11").bind(iDettTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff11() {
        return closeCursor(cEff11);
    }

    public IDettTitCont fetchCEff11(IDettTitCont iDettTitCont) {
        return fetch(cEff11, iDettTitCont, selectByDtcDsRigaRm);
    }

    public IDettTitCont selectRec7(IDettTitCont iDettTitCont) {
        return buildQuery("selectRec7").bind(iDettTitCont).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCCpz11(IDettTitCont iDettTitCont) {
        cCpz11 = buildQuery("openCCpz11").bind(iDettTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz11() {
        return closeCursor(cCpz11);
    }

    public IDettTitCont fetchCCpz11(IDettTitCont iDettTitCont) {
        return fetch(cCpz11, iDettTitCont, selectByDtcDsRigaRm);
    }

    public IDettTitCont selectRec8(IDettTitCont iDettTitCont) {
        return buildQuery("selectRec8").bind(iDettTitCont).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCEff13(IDettTitCont iDettTitCont) {
        cEff13 = buildQuery("openCEff13").bind(iDettTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff13() {
        return closeCursor(cEff13);
    }

    public IDettTitCont fetchCEff13(IDettTitCont iDettTitCont) {
        return fetch(cEff13, iDettTitCont, selectByDtcDsRigaRm);
    }

    public IDettTitCont selectRec9(IDettTitCont iDettTitCont) {
        return buildQuery("selectRec9").bind(iDettTitCont).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCCpz13(IDettTitCont iDettTitCont) {
        cCpz13 = buildQuery("openCCpz13").bind(iDettTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz13() {
        return closeCursor(cCpz13);
    }

    public IDettTitCont fetchCCpz13(IDettTitCont iDettTitCont) {
        return fetch(cCpz13, iDettTitCont, selectByDtcDsRigaRm);
    }

    public AfDecimal selectRec10(IDettTitCont iDettTitCont, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec10").bind(iDettTitCont).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec11(IDettTitCont iDettTitCont, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec11").bind(iDettTitCont).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public IDettTitCont selectRec12(IDettTitCont iDettTitCont) {
        return buildQuery("selectRec12").bind(iDettTitCont).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCEff30(IDettTitCont iDettTitCont) {
        cEff30 = buildQuery("openCEff30").bind(iDettTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff30() {
        return closeCursor(cEff30);
    }

    public IDettTitCont fetchCEff30(IDettTitCont iDettTitCont) {
        return fetch(cEff30, iDettTitCont, selectByDtcDsRigaRm);
    }

    public IDettTitCont selectRec13(IDettTitCont iDettTitCont) {
        return buildQuery("selectRec13").bind(iDettTitCont).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCCpz30(IDettTitCont iDettTitCont) {
        cCpz30 = buildQuery("openCCpz30").bind(iDettTitCont).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz30() {
        return closeCursor(cCpz30);
    }

    public IDettTitCont fetchCCpz30(IDettTitCont iDettTitCont) {
        return fetch(cCpz30, iDettTitCont, selectByDtcDsRigaRm);
    }

    public IDettTitCont selectRec14(IDettTitCont iDettTitCont) {
        return buildQuery("selectRec14").bind(iDettTitCont).rowMapper(selectRec14Rm).singleResult(iDettTitCont);
    }

    public IDettTitCont selectRec15(IDettTitCont iDettTitCont) {
        return buildQuery("selectRec15").bind(iDettTitCont).rowMapper(selectRec14Rm).singleResult(iDettTitCont);
    }

    public IDettTitCont selectRec16(String dtcTpOgg, int dtcIdOgg, int idsv0003CodiceCompagniaAnia, IDettTitCont iDettTitCont) {
        return buildQuery("selectRec16").bind("dtcTpOgg", dtcTpOgg).bind("dtcIdOgg", dtcIdOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByDtcDsRigaRm).singleResult(iDettTitCont);
    }

    public DbAccessStatus openCEff56(String dtcTpOgg, int dtcIdOgg, int idsv0003CodiceCompagniaAnia) {
        cEff56 = buildQuery("openCEff56").bind("dtcTpOgg", dtcTpOgg).bind("dtcIdOgg", dtcIdOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff56() {
        return closeCursor(cEff56);
    }

    public IDettTitCont fetchCEff56(IDettTitCont iDettTitCont) {
        return fetch(cEff56, iDettTitCont, selectByDtcDsRigaRm);
    }
}
