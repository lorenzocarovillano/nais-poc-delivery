package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PARAM_OGG]
 * 
 */
public interface IParamOgg extends BaseSqlTo {

    /**
     * Host Variable POG-ID-OGG
     * 
     */
    int getPogIdOgg();

    void setPogIdOgg(int pogIdOgg);

    /**
     * Host Variable POG-TP-OGG
     * 
     */
    String getPogTpOgg();

    void setPogTpOgg(String pogTpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable POG-ID-PARAM-OGG
     * 
     */
    int getIdParamOgg();

    void setIdParamOgg(int idParamOgg);

    /**
     * Host Variable POG-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable POG-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for POG-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable POG-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable POG-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable POG-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable POG-COD-PARAM
     * 
     */
    String getCodParam();

    void setCodParam(String codParam);

    /**
     * Nullable property for POG-COD-PARAM
     * 
     */
    String getCodParamObj();

    void setCodParamObj(String codParamObj);

    /**
     * Host Variable POG-TP-PARAM
     * 
     */
    char getTpParam();

    void setTpParam(char tpParam);

    /**
     * Nullable property for POG-TP-PARAM
     * 
     */
    Character getTpParamObj();

    void setTpParamObj(Character tpParamObj);

    /**
     * Host Variable POG-TP-D
     * 
     */
    String getTpD();

    void setTpD(String tpD);

    /**
     * Nullable property for POG-TP-D
     * 
     */
    String getTpDObj();

    void setTpDObj(String tpDObj);

    /**
     * Host Variable POG-VAL-IMP
     * 
     */
    AfDecimal getValImp();

    void setValImp(AfDecimal valImp);

    /**
     * Nullable property for POG-VAL-IMP
     * 
     */
    AfDecimal getValImpObj();

    void setValImpObj(AfDecimal valImpObj);

    /**
     * Host Variable POG-VAL-DT-DB
     * 
     */
    String getValDtDb();

    void setValDtDb(String valDtDb);

    /**
     * Nullable property for POG-VAL-DT-DB
     * 
     */
    String getValDtDbObj();

    void setValDtDbObj(String valDtDbObj);

    /**
     * Host Variable POG-VAL-TS
     * 
     */
    AfDecimal getValTs();

    void setValTs(AfDecimal valTs);

    /**
     * Nullable property for POG-VAL-TS
     * 
     */
    AfDecimal getValTsObj();

    void setValTsObj(AfDecimal valTsObj);

    /**
     * Host Variable POG-VAL-TXT-VCHAR
     * 
     */
    String getValTxtVchar();

    void setValTxtVchar(String valTxtVchar);

    /**
     * Nullable property for POG-VAL-TXT-VCHAR
     * 
     */
    String getValTxtVcharObj();

    void setValTxtVcharObj(String valTxtVcharObj);

    /**
     * Host Variable POG-VAL-FL
     * 
     */
    char getValFl();

    void setValFl(char valFl);

    /**
     * Nullable property for POG-VAL-FL
     * 
     */
    Character getValFlObj();

    void setValFlObj(Character valFlObj);

    /**
     * Host Variable POG-VAL-NUM
     * 
     */
    long getValNum();

    void setValNum(long valNum);

    /**
     * Nullable property for POG-VAL-NUM
     * 
     */
    Long getValNumObj();

    void setValNumObj(Long valNumObj);

    /**
     * Host Variable POG-VAL-PC
     * 
     */
    AfDecimal getValPc();

    void setValPc(AfDecimal valPc);

    /**
     * Nullable property for POG-VAL-PC
     * 
     */
    AfDecimal getValPcObj();

    void setValPcObj(AfDecimal valPcObj);

    /**
     * Host Variable POG-DS-RIGA
     * 
     */
    long getPogDsRiga();

    void setPogDsRiga(long pogDsRiga);

    /**
     * Host Variable POG-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable POG-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable POG-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable POG-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable POG-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable POG-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable LDBV1131-ID-OGG
     * 
     */
    int getLdbv1131IdOgg();

    void setLdbv1131IdOgg(int ldbv1131IdOgg);

    /**
     * Host Variable LDBV1131-TP-OGG
     * 
     */
    String getLdbv1131TpOgg();

    void setLdbv1131TpOgg(String ldbv1131TpOgg);

    /**
     * Host Variable LDBV1131-COD-PARAM
     * 
     */
    String getLdbv1131CodParam();

    void setLdbv1131CodParam(String ldbv1131CodParam);
};
