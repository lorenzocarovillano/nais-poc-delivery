package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [GRU_ARZ]
 * 
 */
public interface IGruArz extends BaseSqlTo {

    /**
     * Host Variable GRU-COD-GRU-ARZ
     * 
     */
    long getGruCodGruArz();

    void setGruCodGruArz(long gruCodGruArz);

    /**
     * Host Variable GRU-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable GRU-COD-LIV-OGZ
     * 
     */
    int getCodLivOgz();

    void setCodLivOgz(int codLivOgz);

    /**
     * Host Variable GRU-DSC-GRU-ARZ-VCHAR
     * 
     */
    String getDscGruArzVchar();

    void setDscGruArzVchar(String dscGruArzVchar);

    /**
     * Host Variable GRU-DAT-INI-GRU-ARZ-DB
     * 
     */
    String getDatIniGruArzDb();

    void setDatIniGruArzDb(String datIniGruArzDb);

    /**
     * Host Variable GRU-DAT-FINE-GRU-ARZ-DB
     * 
     */
    String getDatFineGruArzDb();

    void setDatFineGruArzDb(String datFineGruArzDb);

    /**
     * Host Variable GRU-DEN-RESP-GRU-ARZ-VCHAR
     * 
     */
    String getDenRespGruArzVchar();

    void setDenRespGruArzVchar(String denRespGruArzVchar);

    /**
     * Host Variable GRU-IND-TLM-RESP-VCHAR
     * 
     */
    String getIndTlmRespVchar();

    void setIndTlmRespVchar(String indTlmRespVchar);

    /**
     * Host Variable GRU-COD-UTE-INS
     * 
     */
    String getCodUteIns();

    void setCodUteIns(String codUteIns);

    /**
     * Host Variable GRU-TMST-INS-RIG-DB
     * 
     */
    String getTmstInsRigDb();

    void setTmstInsRigDb(String tmstInsRigDb);

    /**
     * Host Variable GRU-COD-UTE-AGR
     * 
     */
    String getCodUteAgr();

    void setCodUteAgr(String codUteAgr);

    /**
     * Host Variable GRU-TMST-AGR-RIG-DB
     * 
     */
    String getTmstAgrRigDb();

    void setTmstAgrRigDb(String tmstAgrRigDb);
};
