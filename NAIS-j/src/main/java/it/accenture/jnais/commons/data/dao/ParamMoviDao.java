package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IParamMovi;

/**
 * Data Access Object(DAO) for table [PARAM_MOVI]
 * 
 */
public class ParamMoviDao extends BaseSqlDao<IParamMovi> {

    private Cursor cIdUpdEffPmo;
    private Cursor cIboEffPmo;
    private Cursor cIdoEffPmo;
    private Cursor cIboCpzPmo;
    private Cursor cIdoCpzPmo;
    private Cursor curPmoCcasRange;
    private Cursor curPmoCcas;
    private Cursor curPmoRange;
    private Cursor curPmoRamoRange;
    private Cursor curPmoPolRange;
    private Cursor curPmoPol;
    private Cursor curPmoRangeC;
    private Cursor curPmoRamoRangeC;
    private Cursor curPmoPolC;
    private Cursor cEff2;
    private Cursor cCpz2;
    private Cursor cEff8;
    private Cursor cCpz8;
    private Cursor curPmo;
    private Cursor cEff26;
    private Cursor cCpz26;
    private Cursor cEff29;
    private Cursor cCpz29;
    private Cursor cEff36;
    private Cursor cCpz36;
    private Cursor cEff43;
    private Cursor cCpz43;
    private Cursor cEff48;
    private Cursor cCpz48;
    private Cursor curPmo1;
    private Cursor curPmoRamo;
    private Cursor cEff52;
    private Cursor cEff58;
    private Cursor cEff59;
    private final IRowMapper<IParamMovi> selectByPmoDsRigaRm = buildNamedRowMapper(IParamMovi.class, "idParamMovi", "pmoIdOgg", "pmoTpOgg", "idMoviCrz", "idMoviChiuObj", "pmoDtIniEffDb", "pmoDtEndEffDb", "pmoCodCompAnia", "pmoTpMoviObj", "frqMoviObj", "durAaObj", "durMmObj", "durGgObj", "dtRicorPrecDbObj", "pmoDtRicorSuccDbObj", "pcIntrFrazObj", "impBnsDaScoTotObj", "impBnsDaScoObj", "pcAnticBnsObj", "tpRinnCollObj", "tpRivalPreObj", "tpRivalPrstzObj", "flEvidRivalObj", "ultPcPerdObj", "totAaGiaProrObj", "tpOpzObj", "aaRenCerObj", "pcRevrsbObj", "impRiscParzPrgtObj", "impLrdDiRatObj", "ibOggObj", "cosOnerObj", "spePcObj", "flAttivGarObj", "cambioVerProdObj", "mmDiffObj", "impRatManfeeObj", "dtUltErogManfeeDbObj", "tpOggRivalObj", "somAsstaGaracObj", "pcApplzOpzObj", "pmoIdAdesObj", "pmoIdPoli", "pmoTpFrmAssva", "pmoDsRiga", "dsOperSql", "dsVer", "pmoDsTsIniCptz", "pmoDsTsEndCptz", "dsUtente", "dsStatoElab", "tpEstrCntObj", "codRamoObj", "genDaSinObj", "codTariObj", "numRatPagPreObj", "pcServValObj", "etaAaSoglBnficrObj");

    public ParamMoviDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IParamMovi> getToClass() {
        return IParamMovi.class;
    }

    public IParamMovi selectByPmoDsRiga(long pmoDsRiga, IParamMovi iParamMovi) {
        return buildQuery("selectByPmoDsRiga").bind("pmoDsRiga", pmoDsRiga).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus insertRec(IParamMovi iParamMovi) {
        return buildQuery("insertRec").bind(iParamMovi).executeInsert();
    }

    public DbAccessStatus updateRec(IParamMovi iParamMovi) {
        return buildQuery("updateRec").bind(iParamMovi).executeUpdate();
    }

    public DbAccessStatus deleteByPmoDsRiga(long pmoDsRiga) {
        return buildQuery("deleteByPmoDsRiga").bind("pmoDsRiga", pmoDsRiga).executeDelete();
    }

    public IParamMovi selectRec(int pmoIdParamMovi, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IParamMovi iParamMovi) {
        return buildQuery("selectRec").bind("pmoIdParamMovi", pmoIdParamMovi).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus updateRec1(IParamMovi iParamMovi) {
        return buildQuery("updateRec1").bind(iParamMovi).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffPmo(int pmoIdParamMovi, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffPmo = buildQuery("openCIdUpdEffPmo").bind("pmoIdParamMovi", pmoIdParamMovi).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffPmo() {
        return closeCursor(cIdUpdEffPmo);
    }

    public IParamMovi fetchCIdUpdEffPmo(IParamMovi iParamMovi) {
        return fetch(cIdUpdEffPmo, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec1(String pmoIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IParamMovi iParamMovi) {
        return buildQuery("selectRec1").bind("pmoIbOgg", pmoIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCIboEffPmo(String pmoIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIboEffPmo = buildQuery("openCIboEffPmo").bind("pmoIbOgg", pmoIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboEffPmo() {
        return closeCursor(cIboEffPmo);
    }

    public IParamMovi fetchCIboEffPmo(IParamMovi iParamMovi) {
        return fetch(cIboEffPmo, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec2(int pmoIdOgg, String pmoTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IParamMovi iParamMovi) {
        return buildQuery("selectRec2").bind("pmoIdOgg", pmoIdOgg).bind("pmoTpOgg", pmoTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCIdoEffPmo(int pmoIdOgg, String pmoTpOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdoEffPmo = buildQuery("openCIdoEffPmo").bind("pmoIdOgg", pmoIdOgg).bind("pmoTpOgg", pmoTpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoEffPmo() {
        return closeCursor(cIdoEffPmo);
    }

    public IParamMovi fetchCIdoEffPmo(IParamMovi iParamMovi) {
        return fetch(cIdoEffPmo, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec3(int pmoIdParamMovi, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IParamMovi iParamMovi) {
        return buildQuery("selectRec3").bind("pmoIdParamMovi", pmoIdParamMovi).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec4(String pmoIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IParamMovi iParamMovi) {
        return buildQuery("selectRec4").bind("pmoIbOgg", pmoIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCIboCpzPmo(String pmoIbOgg, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIboCpzPmo = buildQuery("openCIboCpzPmo").bind("pmoIbOgg", pmoIbOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIboCpzPmo() {
        return closeCursor(cIboCpzPmo);
    }

    public IParamMovi fetchCIboCpzPmo(IParamMovi iParamMovi) {
        return fetch(cIboCpzPmo, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec5(IParamMovi iParamMovi) {
        return buildQuery("selectRec5").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCIdoCpzPmo(IParamMovi iParamMovi) {
        cIdoCpzPmo = buildQuery("openCIdoCpzPmo").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoCpzPmo() {
        return closeCursor(cIdoCpzPmo);
    }

    public IParamMovi fetchCIdoCpzPmo(IParamMovi iParamMovi) {
        return fetch(cIdoCpzPmo, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec6(IParamMovi iParamMovi) {
        return buildQuery("selectRec6").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec7(IParamMovi iParamMovi) {
        return buildQuery("selectRec7").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec8(IParamMovi iParamMovi) {
        return buildQuery("selectRec8").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec9(IParamMovi iParamMovi) {
        return buildQuery("selectRec9").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec10(IParamMovi iParamMovi) {
        return buildQuery("selectRec10").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec11(IParamMovi iParamMovi) {
        return buildQuery("selectRec11").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec12(IParamMovi iParamMovi) {
        return buildQuery("selectRec12").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec13(IParamMovi iParamMovi) {
        return buildQuery("selectRec13").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec14(IParamMovi iParamMovi) {
        return buildQuery("selectRec14").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCurPmoCcasRange(IParamMovi iParamMovi) {
        curPmoCcasRange = buildQuery("openCurPmoCcasRange").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurPmoCcas(IParamMovi iParamMovi) {
        curPmoCcas = buildQuery("openCurPmoCcas").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurPmoRamoRange(IParamMovi iParamMovi) {
        curPmoRamoRange = buildQuery("openCurPmoRamoRange").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurPmoRangeC(IParamMovi iParamMovi) {
        curPmoRangeC = buildQuery("openCurPmoRangeC").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurPmoRamoRangeC(IParamMovi iParamMovi) {
        curPmoRamoRangeC = buildQuery("openCurPmoRamoRangeC").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurPmoPolC(IParamMovi iParamMovi) {
        curPmoPolC = buildQuery("openCurPmoPolC").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurPmoCcasRange() {
        return closeCursor(curPmoCcasRange);
    }

    public DbAccessStatus closeCurPmoCcas() {
        return closeCursor(curPmoCcas);
    }

    public DbAccessStatus closeCurPmoRange() {
        return closeCursor(curPmoRange);
    }

    public DbAccessStatus closeCurPmoRamoRange() {
        return closeCursor(curPmoRamoRange);
    }

    public DbAccessStatus closeCurPmoPolRange() {
        return closeCursor(curPmoPolRange);
    }

    public DbAccessStatus closeCurPmoPol() {
        return closeCursor(curPmoPol);
    }

    public DbAccessStatus closeCurPmoRangeC() {
        return closeCursor(curPmoRangeC);
    }

    public DbAccessStatus closeCurPmoRamoRangeC() {
        return closeCursor(curPmoRamoRangeC);
    }

    public DbAccessStatus closeCurPmoPolC() {
        return closeCursor(curPmoPolC);
    }

    public IParamMovi fetchCurPmoCcasRange(IParamMovi iParamMovi) {
        return fetch(curPmoCcasRange, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi fetchCurPmoCcas(IParamMovi iParamMovi) {
        return fetch(curPmoCcas, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi fetchCurPmoRange(IParamMovi iParamMovi) {
        return fetch(curPmoRange, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi fetchCurPmoRamoRange(IParamMovi iParamMovi) {
        return fetch(curPmoRamoRange, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi fetchCurPmoPolRange(IParamMovi iParamMovi) {
        return fetch(curPmoPolRange, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi fetchCurPmoPol(IParamMovi iParamMovi) {
        return fetch(curPmoPol, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi fetchCurPmoRangeC(IParamMovi iParamMovi) {
        return fetch(curPmoRangeC, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi fetchCurPmoRamoRangeC(IParamMovi iParamMovi) {
        return fetch(curPmoRamoRangeC, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi fetchCurPmoPolC(IParamMovi iParamMovi) {
        return fetch(curPmoPolC, iParamMovi, selectByPmoDsRigaRm);
    }

    public DbAccessStatus openCEff2(IParamMovi iParamMovi) {
        cEff2 = buildQuery("openCEff2").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff2() {
        return closeCursor(cEff2);
    }

    public IParamMovi fetchCEff2(IParamMovi iParamMovi) {
        return fetch(cEff2, iParamMovi, selectByPmoDsRigaRm);
    }

    public DbAccessStatus openCCpz2(IParamMovi iParamMovi) {
        cCpz2 = buildQuery("openCCpz2").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz2() {
        return closeCursor(cCpz2);
    }

    public IParamMovi fetchCCpz2(IParamMovi iParamMovi) {
        return fetch(cCpz2, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec15(IParamMovi iParamMovi) {
        return buildQuery("selectRec15").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCEff8(IParamMovi iParamMovi) {
        cEff8 = buildQuery("openCEff8").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff8() {
        return closeCursor(cEff8);
    }

    public IParamMovi fetchCEff8(IParamMovi iParamMovi) {
        return fetch(cEff8, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec16(IParamMovi iParamMovi) {
        return buildQuery("selectRec16").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCCpz8(IParamMovi iParamMovi) {
        cCpz8 = buildQuery("openCCpz8").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz8() {
        return closeCursor(cCpz8);
    }

    public IParamMovi fetchCCpz8(IParamMovi iParamMovi) {
        return fetch(cCpz8, iParamMovi, selectByPmoDsRigaRm);
    }

    public DbAccessStatus updateRec2(String idsv0003UserName, char iabv0002StateCurrent, long pmoDsRiga) {
        return buildQuery("updateRec2").bind("idsv0003UserName", idsv0003UserName).bind("iabv0002StateCurrent", String.valueOf(iabv0002StateCurrent)).bind("pmoDsRiga", pmoDsRiga).executeUpdate();
    }

    public DbAccessStatus openCurPmo(IParamMovi iParamMovi) {
        curPmo = buildQuery("openCurPmo").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurPmo() {
        return closeCursor(curPmo);
    }

    public IParamMovi fetchCurPmo(IParamMovi iParamMovi) {
        return fetch(curPmo, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec17(IParamMovi iParamMovi) {
        return buildQuery("selectRec17").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCEff26(IParamMovi iParamMovi) {
        cEff26 = buildQuery("openCEff26").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff26() {
        return closeCursor(cEff26);
    }

    public IParamMovi fetchCEff26(IParamMovi iParamMovi) {
        return fetch(cEff26, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec18(IParamMovi iParamMovi) {
        return buildQuery("selectRec18").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCCpz26(IParamMovi iParamMovi) {
        cCpz26 = buildQuery("openCCpz26").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz26() {
        return closeCursor(cCpz26);
    }

    public IParamMovi fetchCCpz26(IParamMovi iParamMovi) {
        return fetch(cCpz26, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec19(IParamMovi iParamMovi) {
        return buildQuery("selectRec19").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCEff29(IParamMovi iParamMovi) {
        cEff29 = buildQuery("openCEff29").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff29() {
        return closeCursor(cEff29);
    }

    public IParamMovi fetchCEff29(IParamMovi iParamMovi) {
        return fetch(cEff29, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec20(IParamMovi iParamMovi) {
        return buildQuery("selectRec20").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCCpz29(IParamMovi iParamMovi) {
        cCpz29 = buildQuery("openCCpz29").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz29() {
        return closeCursor(cCpz29);
    }

    public IParamMovi fetchCCpz29(IParamMovi iParamMovi) {
        return fetch(cCpz29, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec21(IParamMovi iParamMovi) {
        return buildQuery("selectRec21").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCEff36(IParamMovi iParamMovi) {
        cEff36 = buildQuery("openCEff36").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff36() {
        return closeCursor(cEff36);
    }

    public IParamMovi fetchCEff36(IParamMovi iParamMovi) {
        return fetch(cEff36, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec22(IParamMovi iParamMovi) {
        return buildQuery("selectRec22").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCCpz36(IParamMovi iParamMovi) {
        cCpz36 = buildQuery("openCCpz36").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz36() {
        return closeCursor(cCpz36);
    }

    public IParamMovi fetchCCpz36(IParamMovi iParamMovi) {
        return fetch(cCpz36, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec23(IParamMovi iParamMovi) {
        return buildQuery("selectRec23").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCEff43(IParamMovi iParamMovi) {
        cEff43 = buildQuery("openCEff43").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff43() {
        return closeCursor(cEff43);
    }

    public IParamMovi fetchCEff43(IParamMovi iParamMovi) {
        return fetch(cEff43, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec24(IParamMovi iParamMovi) {
        return buildQuery("selectRec24").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCCpz43(IParamMovi iParamMovi) {
        cCpz43 = buildQuery("openCCpz43").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz43() {
        return closeCursor(cCpz43);
    }

    public IParamMovi fetchCCpz43(IParamMovi iParamMovi) {
        return fetch(cCpz43, iParamMovi, selectByPmoDsRigaRm);
    }

    public DbAccessStatus openCEff48(IParamMovi iParamMovi) {
        cEff48 = buildQuery("openCEff48").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff48() {
        return closeCursor(cEff48);
    }

    public IParamMovi fetchCEff48(IParamMovi iParamMovi) {
        return fetch(cEff48, iParamMovi, selectByPmoDsRigaRm);
    }

    public DbAccessStatus openCCpz48(IParamMovi iParamMovi) {
        cCpz48 = buildQuery("openCCpz48").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz48() {
        return closeCursor(cCpz48);
    }

    public IParamMovi fetchCCpz48(IParamMovi iParamMovi) {
        return fetch(cCpz48, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec25(IParamMovi iParamMovi) {
        return buildQuery("selectRec25").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public IParamMovi selectRec26(IParamMovi iParamMovi) {
        return buildQuery("selectRec26").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus updateRec3(char pmoDsOperSql, String pmoDsUtente, char iabv0002StateCurrent, long pmoDsRiga) {
        return buildQuery("updateRec3").bind("pmoDsOperSql", String.valueOf(pmoDsOperSql)).bind("pmoDsUtente", pmoDsUtente).bind("iabv0002StateCurrent", String.valueOf(iabv0002StateCurrent)).bind("pmoDsRiga", pmoDsRiga).executeUpdate();
    }

    public DbAccessStatus openCurPmoRange(IParamMovi iParamMovi) {
        curPmoRange = buildQuery("openCurPmoRange").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurPmo1(IParamMovi iParamMovi) {
        curPmo1 = buildQuery("openCurPmo1").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus openCurPmoRamo(IParamMovi iParamMovi) {
        curPmoRamo = buildQuery("openCurPmoRamo").bind(iParamMovi).withHoldCursorsOverCommit().open();
        return dbStatus;
    }

    public DbAccessStatus closeCurPmo1() {
        return closeCursor(curPmo1);
    }

    public DbAccessStatus closeCurPmoRamo() {
        return closeCursor(curPmoRamo);
    }

    public IParamMovi fetchCurPmo1(IParamMovi iParamMovi) {
        return fetch(curPmo1, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi fetchCurPmoRamo(IParamMovi iParamMovi) {
        return fetch(curPmoRamo, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec27(IParamMovi iParamMovi) {
        return buildQuery("selectRec27").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCEff52(IParamMovi iParamMovi) {
        cEff52 = buildQuery("openCEff52").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff52() {
        return closeCursor(cEff52);
    }

    public IParamMovi fetchCEff52(IParamMovi iParamMovi) {
        return fetch(cEff52, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec28(IParamMovi iParamMovi) {
        return buildQuery("selectRec28").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCEff58(IParamMovi iParamMovi) {
        cEff58 = buildQuery("openCEff58").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff58() {
        return closeCursor(cEff58);
    }

    public IParamMovi fetchCEff58(IParamMovi iParamMovi) {
        return fetch(cEff58, iParamMovi, selectByPmoDsRigaRm);
    }

    public IParamMovi selectRec29(IParamMovi iParamMovi) {
        return buildQuery("selectRec29").bind(iParamMovi).rowMapper(selectByPmoDsRigaRm).singleResult(iParamMovi);
    }

    public DbAccessStatus openCEff59(IParamMovi iParamMovi) {
        cEff59 = buildQuery("openCEff59").bind(iParamMovi).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff59() {
        return closeCursor(cEff59);
    }

    public IParamMovi fetchCEff59(IParamMovi iParamMovi) {
        return fetch(cEff59, iParamMovi, selectByPmoDsRigaRm);
    }
}
