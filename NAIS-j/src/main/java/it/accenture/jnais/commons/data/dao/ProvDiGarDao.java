package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IProvDiGar;

/**
 * Data Access Object(DAO) for table [PROV_DI_GAR]
 * 
 */
public class ProvDiGarDao extends BaseSqlDao<IProvDiGar> {

    private Cursor cIdUpdEffPvt;
    private Cursor cIdpEffPvt;
    private Cursor cIdpCpzPvt;
    private final IRowMapper<IProvDiGar> selectByPvtDsRigaRm = buildNamedRowMapper(IProvDiGar.class, "idProvDiGar", "idGarObj", "idMoviCrz", "idMoviChiuObj", "dtIniEffDb", "dtEndEffDb", "codCompAnia", "tpProvObj", "prov1aaAcqObj", "prov2aaAcqObj", "provRicorObj", "provIncObj", "alqProvAcqObj", "alqProvIncObj", "alqProvRicorObj", "flStorProvAcqObj", "flRecProvStornObj", "flProvForzObj", "tpCalcProvObj", "pvtDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "remunAssObj", "commisInterObj", "alqRemunAssObj", "alqCommisInterObj");

    public ProvDiGarDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IProvDiGar> getToClass() {
        return IProvDiGar.class;
    }

    public IProvDiGar selectByPvtDsRiga(long pvtDsRiga, IProvDiGar iProvDiGar) {
        return buildQuery("selectByPvtDsRiga").bind("pvtDsRiga", pvtDsRiga).rowMapper(selectByPvtDsRigaRm).singleResult(iProvDiGar);
    }

    public DbAccessStatus insertRec(IProvDiGar iProvDiGar) {
        return buildQuery("insertRec").bind(iProvDiGar).executeInsert();
    }

    public DbAccessStatus updateRec(IProvDiGar iProvDiGar) {
        return buildQuery("updateRec").bind(iProvDiGar).executeUpdate();
    }

    public DbAccessStatus deleteByPvtDsRiga(long pvtDsRiga) {
        return buildQuery("deleteByPvtDsRiga").bind("pvtDsRiga", pvtDsRiga).executeDelete();
    }

    public IProvDiGar selectRec(int pvtIdProvDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IProvDiGar iProvDiGar) {
        return buildQuery("selectRec").bind("pvtIdProvDiGar", pvtIdProvDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPvtDsRigaRm).singleResult(iProvDiGar);
    }

    public DbAccessStatus updateRec1(IProvDiGar iProvDiGar) {
        return buildQuery("updateRec1").bind(iProvDiGar).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffPvt(int pvtIdProvDiGar, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffPvt = buildQuery("openCIdUpdEffPvt").bind("pvtIdProvDiGar", pvtIdProvDiGar).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffPvt() {
        return closeCursor(cIdUpdEffPvt);
    }

    public IProvDiGar fetchCIdUpdEffPvt(IProvDiGar iProvDiGar) {
        return fetch(cIdUpdEffPvt, iProvDiGar, selectByPvtDsRigaRm);
    }

    public IProvDiGar selectRec1(int pvtIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IProvDiGar iProvDiGar) {
        return buildQuery("selectRec1").bind("pvtIdGar", pvtIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByPvtDsRigaRm).singleResult(iProvDiGar);
    }

    public DbAccessStatus openCIdpEffPvt(int pvtIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffPvt = buildQuery("openCIdpEffPvt").bind("pvtIdGar", pvtIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffPvt() {
        return closeCursor(cIdpEffPvt);
    }

    public IProvDiGar fetchCIdpEffPvt(IProvDiGar iProvDiGar) {
        return fetch(cIdpEffPvt, iProvDiGar, selectByPvtDsRigaRm);
    }

    public IProvDiGar selectRec2(int pvtIdProvDiGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IProvDiGar iProvDiGar) {
        return buildQuery("selectRec2").bind("pvtIdProvDiGar", pvtIdProvDiGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPvtDsRigaRm).singleResult(iProvDiGar);
    }

    public IProvDiGar selectRec3(int pvtIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IProvDiGar iProvDiGar) {
        return buildQuery("selectRec3").bind("pvtIdGar", pvtIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByPvtDsRigaRm).singleResult(iProvDiGar);
    }

    public DbAccessStatus openCIdpCpzPvt(int pvtIdGar, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzPvt = buildQuery("openCIdpCpzPvt").bind("pvtIdGar", pvtIdGar).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzPvt() {
        return closeCursor(cIdpCpzPvt);
    }

    public IProvDiGar fetchCIdpCpzPvt(IProvDiGar iProvDiGar) {
        return fetch(cIdpCpzPvt, iProvDiGar, selectByPvtDsRigaRm);
    }
}
