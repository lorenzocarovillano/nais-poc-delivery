package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [AMMB_FUNZ_BLOCCO]
 * 
 */
public interface IAmmbFunzBlocco extends BaseSqlTo {

    /**
     * Host Variable L16-COD-BLOCCO
     * 
     */
    String getL16CodBlocco();

    void setL16CodBlocco(String l16CodBlocco);

    /**
     * Host Variable L16-TP-MOVI
     * 
     */
    int getL16TpMovi();

    void setL16TpMovi(int l16TpMovi);

    /**
     * Host Variable L16-COD-CAN
     * 
     */
    int getL16CodCan();

    void setL16CodCan(int l16CodCan);

    /**
     * Host Variable L16-TP-MOVI-RIFTO
     * 
     */
    int getL16TpMoviRifto();

    void setL16TpMoviRifto(int l16TpMoviRifto);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable L16-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L16-GRAV-FUNZ-BLOCCO
     * 
     */
    char getGravFunzBlocco();

    void setGravFunzBlocco(char gravFunzBlocco);

    /**
     * Host Variable L16-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L16-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L16-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable L16-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable L16-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Nullable property for L16-TP-MOVI-RIFTO
     * 
     */
    Integer getL16TpMoviRiftoObj();

    void setL16TpMoviRiftoObj(Integer l16TpMoviRiftoObj);
};
