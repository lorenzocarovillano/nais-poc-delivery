package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BILA_VAR_CALC_P]
 * 
 */
public interface IBilaVarCalcP extends BaseSqlTo {

    /**
     * Host Variable B04-ID-BILA-VAR-CALC-P
     * 
     */
    int getB04IdBilaVarCalcP();

    void setB04IdBilaVarCalcP(int b04IdBilaVarCalcP);

    /**
     * Host Variable B04-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable B04-ID-RICH-ESTRAZ-MAS
     * 
     */
    int getIdRichEstrazMas();

    void setIdRichEstrazMas(int idRichEstrazMas);

    /**
     * Host Variable B04-ID-RICH-ESTRAZ-AGG
     * 
     */
    int getIdRichEstrazAgg();

    void setIdRichEstrazAgg(int idRichEstrazAgg);

    /**
     * Nullable property for B04-ID-RICH-ESTRAZ-AGG
     * 
     */
    Integer getIdRichEstrazAggObj();

    void setIdRichEstrazAggObj(Integer idRichEstrazAggObj);

    /**
     * Host Variable B04-DT-RIS-DB
     * 
     */
    String getDtRisDb();

    void setDtRisDb(String dtRisDb);

    /**
     * Host Variable B04-ID-POLI
     * 
     */
    int getIdPoli();

    void setIdPoli(int idPoli);

    /**
     * Host Variable B04-ID-ADES
     * 
     */
    int getIdAdes();

    void setIdAdes(int idAdes);

    /**
     * Host Variable B04-PROG-SCHEDA-VALOR
     * 
     */
    int getProgSchedaValor();

    void setProgSchedaValor(int progSchedaValor);

    /**
     * Host Variable B04-TP-RGM-FISC
     * 
     */
    String getTpRgmFisc();

    void setTpRgmFisc(String tpRgmFisc);

    /**
     * Host Variable B04-DT-INI-VLDT-PROD-DB
     * 
     */
    String getDtIniVldtProdDb();

    void setDtIniVldtProdDb(String dtIniVldtProdDb);

    /**
     * Host Variable B04-COD-VAR
     * 
     */
    String getCodVar();

    void setCodVar(String codVar);

    /**
     * Host Variable B04-TP-D
     * 
     */
    char getTpD();

    void setTpD(char tpD);

    /**
     * Host Variable B04-VAL-IMP
     * 
     */
    AfDecimal getValImp();

    void setValImp(AfDecimal valImp);

    /**
     * Nullable property for B04-VAL-IMP
     * 
     */
    AfDecimal getValImpObj();

    void setValImpObj(AfDecimal valImpObj);

    /**
     * Host Variable B04-VAL-PC
     * 
     */
    AfDecimal getValPc();

    void setValPc(AfDecimal valPc);

    /**
     * Nullable property for B04-VAL-PC
     * 
     */
    AfDecimal getValPcObj();

    void setValPcObj(AfDecimal valPcObj);

    /**
     * Host Variable B04-VAL-STRINGA
     * 
     */
    String getValStringa();

    void setValStringa(String valStringa);

    /**
     * Nullable property for B04-VAL-STRINGA
     * 
     */
    String getValStringaObj();

    void setValStringaObj(String valStringaObj);

    /**
     * Host Variable B04-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable B04-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable B04-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable B04-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable B04-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable B04-AREA-D-VALOR-VAR-VCHAR
     * 
     */
    String getAreaDValorVarVchar();

    void setAreaDValorVarVchar(String areaDValorVarVchar);

    /**
     * Nullable property for B04-AREA-D-VALOR-VAR-VCHAR
     * 
     */
    String getAreaDValorVarVcharObj();

    void setAreaDValorVarVcharObj(String areaDValorVarVcharObj);
};
