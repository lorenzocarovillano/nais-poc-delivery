package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [MOT_DEROGA]
 * 
 */
public interface IMotDeroga extends BaseSqlTo {

    /**
     * Host Variable MDE-ID-MOT-DEROGA
     * 
     */
    int getIdMotDeroga();

    void setIdMotDeroga(int idMotDeroga);

    /**
     * Host Variable MDE-ID-OGG-DEROGA
     * 
     */
    int getIdOggDeroga();

    void setIdOggDeroga(int idOggDeroga);

    /**
     * Host Variable MDE-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable MDE-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for MDE-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable MDE-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable MDE-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable MDE-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable MDE-TP-MOT-DEROGA
     * 
     */
    String getTpMotDeroga();

    void setTpMotDeroga(String tpMotDeroga);

    /**
     * Host Variable MDE-COD-LIV-AUT
     * 
     */
    int getCodLivAut();

    void setCodLivAut(int codLivAut);

    /**
     * Host Variable MDE-COD-ERR
     * 
     */
    String getCodErr();

    void setCodErr(String codErr);

    /**
     * Host Variable MDE-TP-ERR
     * 
     */
    String getTpErr();

    void setTpErr(String tpErr);

    /**
     * Host Variable MDE-DESC-ERR-BREVE-VCHAR
     * 
     */
    String getDescErrBreveVchar();

    void setDescErrBreveVchar(String descErrBreveVchar);

    /**
     * Nullable property for MDE-DESC-ERR-BREVE-VCHAR
     * 
     */
    String getDescErrBreveVcharObj();

    void setDescErrBreveVcharObj(String descErrBreveVcharObj);

    /**
     * Host Variable MDE-DESC-ERR-EST-VCHAR
     * 
     */
    String getDescErrEstVchar();

    void setDescErrEstVchar(String descErrEstVchar);

    /**
     * Nullable property for MDE-DESC-ERR-EST-VCHAR
     * 
     */
    String getDescErrEstVcharObj();

    void setDescErrEstVcharObj(String descErrEstVcharObj);

    /**
     * Host Variable MDE-DS-RIGA
     * 
     */
    long getMdeDsRiga();

    void setMdeDsRiga(long mdeDsRiga);

    /**
     * Host Variable MDE-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable MDE-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable MDE-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable MDE-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable MDE-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable MDE-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
