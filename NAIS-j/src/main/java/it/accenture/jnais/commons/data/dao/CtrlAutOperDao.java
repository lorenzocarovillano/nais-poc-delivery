package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.ICtrlAutOper;

/**
 * Data Access Object(DAO) for table [CTRL_AUT_OPER]
 * 
 */
public class CtrlAutOperDao extends BaseSqlDao<ICtrlAutOper> {

    private Cursor cao1;
    private Cursor cao2;
    private Cursor cao3;
    private Cursor cao4;
    private Cursor cao5;
    private Cursor cao6;
    private final IRowMapper<ICtrlAutOper> fetchCao1Rm = buildNamedRowMapper(ICtrlAutOper.class, "idCtrlAutOper", "codCompagniaAnia", "tpMoviObj", "codErrore", "keyAutOper1Obj", "keyAutOper2Obj", "keyAutOper3Obj", "keyAutOper4Obj", "keyAutOper5Obj", "codLivAut", "tpMotDeroga", "moduloVerificaObj", "codCondObj", "progCondObj", "risultCondObj", "paramAutOperVcharObj", "statoAttivazioneObj");

    public CtrlAutOperDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<ICtrlAutOper> getToClass() {
        return ICtrlAutOper.class;
    }

    public DbAccessStatus openCao1(ICtrlAutOper iCtrlAutOper) {
        cao1 = buildQuery("openCao1").bind(iCtrlAutOper).open();
        return dbStatus;
    }

    public DbAccessStatus openCao2(ICtrlAutOper iCtrlAutOper) {
        cao2 = buildQuery("openCao2").bind(iCtrlAutOper).open();
        return dbStatus;
    }

    public DbAccessStatus openCao3(ICtrlAutOper iCtrlAutOper) {
        cao3 = buildQuery("openCao3").bind(iCtrlAutOper).open();
        return dbStatus;
    }

    public DbAccessStatus openCao4(int codCompagniaAnia, int tpMovi, String keyAutOper1, String keyAutOper2) {
        cao4 = buildQuery("openCao4").bind("codCompagniaAnia", codCompagniaAnia).bind("tpMovi", tpMovi).bind("keyAutOper1", keyAutOper1).bind("keyAutOper2", keyAutOper2).open();
        return dbStatus;
    }

    public DbAccessStatus openCao5(int codCompagniaAnia, int tpMovi, String keyAutOper1) {
        cao5 = buildQuery("openCao5").bind("codCompagniaAnia", codCompagniaAnia).bind("tpMovi", tpMovi).bind("keyAutOper1", keyAutOper1).open();
        return dbStatus;
    }

    public DbAccessStatus openCao6(int codCompagniaAnia, int tpMovi) {
        cao6 = buildQuery("openCao6").bind("codCompagniaAnia", codCompagniaAnia).bind("tpMovi", tpMovi).open();
        return dbStatus;
    }

    public ICtrlAutOper fetchCao1(ICtrlAutOper iCtrlAutOper) {
        return fetch(cao1, iCtrlAutOper, fetchCao1Rm);
    }

    public ICtrlAutOper fetchCao2(ICtrlAutOper iCtrlAutOper) {
        return fetch(cao2, iCtrlAutOper, fetchCao1Rm);
    }

    public ICtrlAutOper fetchCao3(ICtrlAutOper iCtrlAutOper) {
        return fetch(cao3, iCtrlAutOper, fetchCao1Rm);
    }

    public ICtrlAutOper fetchCao4(ICtrlAutOper iCtrlAutOper) {
        return fetch(cao4, iCtrlAutOper, fetchCao1Rm);
    }

    public ICtrlAutOper fetchCao5(ICtrlAutOper iCtrlAutOper) {
        return fetch(cao5, iCtrlAutOper, fetchCao1Rm);
    }

    public ICtrlAutOper fetchCao6(ICtrlAutOper iCtrlAutOper) {
        return fetch(cao6, iCtrlAutOper, fetchCao1Rm);
    }

    public DbAccessStatus closeCao1() {
        return closeCursor(cao1);
    }

    public DbAccessStatus closeCao2() {
        return closeCursor(cao2);
    }

    public DbAccessStatus closeCao3() {
        return closeCursor(cao3);
    }

    public DbAccessStatus closeCao4() {
        return closeCursor(cao4);
    }

    public DbAccessStatus closeCao5() {
        return closeCursor(cao5);
    }

    public DbAccessStatus closeCao6() {
        return closeCursor(cao6);
    }
}
