package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBnficrLiq;

/**
 * Data Access Object(DAO) for table [BNFICR_LIQ]
 * 
 */
public class BnficrLiqDao extends BaseSqlDao<IBnficrLiq> {

    private Cursor cIdUpdEffBel;
    private Cursor cIdpEffBel;
    private Cursor cIdpCpzBel;
    private final IRowMapper<IBnficrLiq> selectByBelDsRigaRm = buildNamedRowMapper(IBnficrLiq.class, "idBnficrLiq", "idLiq", "idMoviCrz", "idMoviChiuObj", "codCompAnia", "dtIniEffDb", "dtEndEffDb", "idRappAnaObj", "codBnficrObj", "descBnficrVcharObj", "pcLiqObj", "esrcnAttvtImprsObj", "tpIndBnficrObj", "flEseObj", "flIrrevObj", "impLrdLiqtoObj", "impstIptObj", "impNetLiqtoObj", "ritAccIptObj", "ritVisIptObj", "ritTfrIptObj", "impstIrpefIptObj", "impstSostIptObj", "impstPrvrIptObj", "impst252IptObj", "idAsstoObj", "taxSepObj", "dtRiserveSomPDbObj", "dtVltDbObj", "tpStatLiqBnficrObj", "belDsRiga", "dsOperSql", "dsVer", "dsTsIniCptz", "dsTsEndCptz", "dsUtente", "dsStatoElab", "richCalcCnbtInpObj", "impIntrRitPagObj", "dtUltDoctoDbObj", "dtDormienzaDbObj", "impstBolloTotVObj", "impstVis1382011Obj", "impstSost1382011Obj", "impstVis662014Obj", "impstSost662014Obj");

    public BnficrLiqDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBnficrLiq> getToClass() {
        return IBnficrLiq.class;
    }

    public IBnficrLiq selectByBelDsRiga(long belDsRiga, IBnficrLiq iBnficrLiq) {
        return buildQuery("selectByBelDsRiga").bind("belDsRiga", belDsRiga).rowMapper(selectByBelDsRigaRm).singleResult(iBnficrLiq);
    }

    public DbAccessStatus insertRec(IBnficrLiq iBnficrLiq) {
        return buildQuery("insertRec").bind(iBnficrLiq).executeInsert();
    }

    public DbAccessStatus updateRec(IBnficrLiq iBnficrLiq) {
        return buildQuery("updateRec").bind(iBnficrLiq).executeUpdate();
    }

    public DbAccessStatus deleteByBelDsRiga(long belDsRiga) {
        return buildQuery("deleteByBelDsRiga").bind("belDsRiga", belDsRiga).executeDelete();
    }

    public IBnficrLiq selectRec(int belIdBnficrLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IBnficrLiq iBnficrLiq) {
        return buildQuery("selectRec").bind("belIdBnficrLiq", belIdBnficrLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByBelDsRigaRm).singleResult(iBnficrLiq);
    }

    public DbAccessStatus updateRec1(IBnficrLiq iBnficrLiq) {
        return buildQuery("updateRec1").bind(iBnficrLiq).executeUpdate();
    }

    public DbAccessStatus openCIdUpdEffBel(int belIdBnficrLiq, long wsTsInfinito, String wsDataInizioEffettoDb, int idsv0003CodiceCompagniaAnia) {
        cIdUpdEffBel = buildQuery("openCIdUpdEffBel").bind("belIdBnficrLiq", belIdBnficrLiq).bind("wsTsInfinito", wsTsInfinito).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdUpdEffBel() {
        return closeCursor(cIdUpdEffBel);
    }

    public IBnficrLiq fetchCIdUpdEffBel(IBnficrLiq iBnficrLiq) {
        return fetch(cIdUpdEffBel, iBnficrLiq, selectByBelDsRigaRm);
    }

    public IBnficrLiq selectRec1(int belIdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, IBnficrLiq iBnficrLiq) {
        return buildQuery("selectRec1").bind("belIdLiq", belIdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).rowMapper(selectByBelDsRigaRm).singleResult(iBnficrLiq);
    }

    public DbAccessStatus openCIdpEffBel(int belIdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb) {
        cIdpEffBel = buildQuery("openCIdpEffBel").bind("belIdLiq", belIdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpEffBel() {
        return closeCursor(cIdpEffBel);
    }

    public IBnficrLiq fetchCIdpEffBel(IBnficrLiq iBnficrLiq) {
        return fetch(cIdpEffBel, iBnficrLiq, selectByBelDsRigaRm);
    }

    public IBnficrLiq selectRec2(int belIdBnficrLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IBnficrLiq iBnficrLiq) {
        return buildQuery("selectRec2").bind("belIdBnficrLiq", belIdBnficrLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByBelDsRigaRm).singleResult(iBnficrLiq);
    }

    public IBnficrLiq selectRec3(int belIdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, IBnficrLiq iBnficrLiq) {
        return buildQuery("selectRec3").bind("belIdLiq", belIdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).rowMapper(selectByBelDsRigaRm).singleResult(iBnficrLiq);
    }

    public DbAccessStatus openCIdpCpzBel(int belIdLiq, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza) {
        cIdpCpzBel = buildQuery("openCIdpCpzBel").bind("belIdLiq", belIdLiq).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdpCpzBel() {
        return closeCursor(cIdpCpzBel);
    }

    public IBnficrLiq fetchCIdpCpzBel(IBnficrLiq iBnficrLiq) {
        return fetch(cIdpCpzBel, iBnficrLiq, selectByBelDsRigaRm);
    }
}
