package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IOggBlocco;

/**
 * Data Access Object(DAO) for table [OGG_BLOCCO]
 * 
 */
public class OggBloccoDao extends BaseSqlDao<IOggBlocco> {

    private Cursor cIdoNstL11;
    private Cursor cNst4;
    private Cursor cNst7;
    private final IRowMapper<IOggBlocco> selectByL11IdOggBloccoRm = buildNamedRowMapper(IOggBlocco.class, "l11IdOggBlocco", "idOgg1rioObj", "tpOgg1rioObj", "codCompAnia", "tpMovi", "l11TpOgg", "l11IdOgg", "dtEffDb", "l11TpStatBlocco", "l11CodBlocco", "idRichObj", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab");

    public OggBloccoDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IOggBlocco> getToClass() {
        return IOggBlocco.class;
    }

    public IOggBlocco selectByL11IdOggBlocco(int l11IdOggBlocco, IOggBlocco iOggBlocco) {
        return buildQuery("selectByL11IdOggBlocco").bind("l11IdOggBlocco", l11IdOggBlocco).rowMapper(selectByL11IdOggBloccoRm).singleResult(iOggBlocco);
    }

    public DbAccessStatus insertRec(IOggBlocco iOggBlocco) {
        return buildQuery("insertRec").bind(iOggBlocco).executeInsert();
    }

    public DbAccessStatus updateRec(IOggBlocco iOggBlocco) {
        return buildQuery("updateRec").bind(iOggBlocco).executeUpdate();
    }

    public DbAccessStatus deleteByL11IdOggBlocco(int l11IdOggBlocco) {
        return buildQuery("deleteByL11IdOggBlocco").bind("l11IdOggBlocco", l11IdOggBlocco).executeDelete();
    }

    public IOggBlocco selectRec(int l11IdOgg, String l11TpOgg, int idsv0003CodiceCompagniaAnia, IOggBlocco iOggBlocco) {
        return buildQuery("selectRec").bind("l11IdOgg", l11IdOgg).bind("l11TpOgg", l11TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByL11IdOggBloccoRm).singleResult(iOggBlocco);
    }

    public DbAccessStatus openCIdoNstL11(int l11IdOgg, String l11TpOgg, int idsv0003CodiceCompagniaAnia) {
        cIdoNstL11 = buildQuery("openCIdoNstL11").bind("l11IdOgg", l11IdOgg).bind("l11TpOgg", l11TpOgg).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIdoNstL11() {
        return closeCursor(cIdoNstL11);
    }

    public IOggBlocco fetchCIdoNstL11(IOggBlocco iOggBlocco) {
        return fetch(cIdoNstL11, iOggBlocco, selectByL11IdOggBloccoRm);
    }

    public IOggBlocco selectRec1(int l11IdOgg, String l11TpOgg, String l11TpStatBlocco, int idsv0003CodiceCompagniaAnia, IOggBlocco iOggBlocco) {
        return buildQuery("selectRec1").bind("l11IdOgg", l11IdOgg).bind("l11TpOgg", l11TpOgg).bind("l11TpStatBlocco", l11TpStatBlocco).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByL11IdOggBloccoRm).singleResult(iOggBlocco);
    }

    public DbAccessStatus openCNst4(int l11IdOgg, String l11TpOgg, String l11TpStatBlocco, int idsv0003CodiceCompagniaAnia) {
        cNst4 = buildQuery("openCNst4").bind("l11IdOgg", l11IdOgg).bind("l11TpOgg", l11TpOgg).bind("l11TpStatBlocco", l11TpStatBlocco).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst4() {
        return closeCursor(cNst4);
    }

    public IOggBlocco fetchCNst4(IOggBlocco iOggBlocco) {
        return fetch(cNst4, iOggBlocco, selectByL11IdOggBloccoRm);
    }

    public IOggBlocco selectRec2(IOggBlocco iOggBlocco) {
        return buildQuery("selectRec2").bind(iOggBlocco).rowMapper(selectByL11IdOggBloccoRm).singleResult(iOggBlocco);
    }

    public DbAccessStatus openCNst7(IOggBlocco iOggBlocco) {
        cNst7 = buildQuery("openCNst7").bind(iOggBlocco).open();
        return dbStatus;
    }

    public DbAccessStatus closeCNst7() {
        return closeCursor(cNst7);
    }

    public IOggBlocco fetchCNst7(IOggBlocco iOggBlocco) {
        return fetch(cNst7, iOggBlocco, selectByL11IdOggBloccoRm);
    }
}
