package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [BTC_JOB_SCHEDULE]
 * 
 */
public interface IBtcJobSchedule extends BaseSqlTo {

    /**
     * Host Variable BJS-ID-BATCH
     * 
     */
    int getBjsIdBatch();

    void setBjsIdBatch(int bjsIdBatch);

    /**
     * Host Variable IABV0002-STATE-01
     * 
     */
    char getIabv0002State01();

    void setIabv0002State01(char iabv0002State01);

    /**
     * Host Variable IABV0002-STATE-02
     * 
     */
    char getIabv0002State02();

    void setIabv0002State02(char iabv0002State02);

    /**
     * Host Variable IABV0002-STATE-03
     * 
     */
    char getIabv0002State03();

    void setIabv0002State03(char iabv0002State03);

    /**
     * Host Variable IABV0002-STATE-04
     * 
     */
    char getIabv0002State04();

    void setIabv0002State04(char iabv0002State04);

    /**
     * Host Variable IABV0002-STATE-05
     * 
     */
    char getIabv0002State05();

    void setIabv0002State05(char iabv0002State05);

    /**
     * Host Variable IABV0002-STATE-06
     * 
     */
    char getIabv0002State06();

    void setIabv0002State06(char iabv0002State06);

    /**
     * Host Variable IABV0002-STATE-07
     * 
     */
    char getIabv0002State07();

    void setIabv0002State07(char iabv0002State07);

    /**
     * Host Variable IABV0002-STATE-08
     * 
     */
    char getIabv0002State08();

    void setIabv0002State08(char iabv0002State08);

    /**
     * Host Variable IABV0002-STATE-09
     * 
     */
    char getIabv0002State09();

    void setIabv0002State09(char iabv0002State09);

    /**
     * Host Variable IABV0002-STATE-10
     * 
     */
    char getIabv0002State10();

    void setIabv0002State10(char iabv0002State10);

    /**
     * Host Variable BJS-COD-MACROFUNCT
     * 
     */
    String getBjsCodMacrofunct();

    void setBjsCodMacrofunct(String bjsCodMacrofunct);

    /**
     * Host Variable BJS-TP-MOVI
     * 
     */
    int getBjsTpMovi();

    void setBjsTpMovi(int bjsTpMovi);

    /**
     * Host Variable IABV0009-ID-OGG-DA
     * 
     */
    int getIabv0009IdOggDa();

    void setIabv0009IdOggDa(int iabv0009IdOggDa);

    /**
     * Host Variable IABV0009-ID-OGG-A
     * 
     */
    int getIabv0009IdOggA();

    void setIabv0009IdOggA(int iabv0009IdOggA);

    /**
     * Host Variable BJS-ID-JOB
     * 
     */
    int getIdJob();

    void setIdJob(int idJob);

    /**
     * Host Variable BJS-COD-ELAB-STATE
     * 
     */
    char getCodElabState();

    void setCodElabState(char codElabState);

    /**
     * Host Variable BJS-FLAG-WARNINGS
     * 
     */
    char getFlagWarnings();

    void setFlagWarnings(char flagWarnings);

    /**
     * Nullable property for BJS-FLAG-WARNINGS
     * 
     */
    Character getFlagWarningsObj();

    void setFlagWarningsObj(Character flagWarningsObj);

    /**
     * Host Variable BJS-DT-START-DB
     * 
     */
    String getDtStartDb();

    void setDtStartDb(String dtStartDb);

    /**
     * Nullable property for BJS-DT-START-DB
     * 
     */
    String getDtStartDbObj();

    void setDtStartDbObj(String dtStartDbObj);

    /**
     * Host Variable BJS-DT-END-DB
     * 
     */
    String getDtEndDb();

    void setDtEndDb(String dtEndDb);

    /**
     * Nullable property for BJS-DT-END-DB
     * 
     */
    String getDtEndDbObj();

    void setDtEndDbObj(String dtEndDbObj);

    /**
     * Host Variable BJS-USER-START
     * 
     */
    String getUserStart();

    void setUserStart(String userStart);

    /**
     * Nullable property for BJS-USER-START
     * 
     */
    String getUserStartObj();

    void setUserStartObj(String userStartObj);

    /**
     * Host Variable BJS-FLAG-ALWAYS-EXE
     * 
     */
    char getFlagAlwaysExe();

    void setFlagAlwaysExe(char flagAlwaysExe);

    /**
     * Nullable property for BJS-FLAG-ALWAYS-EXE
     * 
     */
    Character getFlagAlwaysExeObj();

    void setFlagAlwaysExeObj(Character flagAlwaysExeObj);

    /**
     * Host Variable BJS-EXECUTIONS-COUNT
     * 
     */
    int getExecutionsCount();

    void setExecutionsCount(int executionsCount);

    /**
     * Nullable property for BJS-EXECUTIONS-COUNT
     * 
     */
    Integer getExecutionsCountObj();

    void setExecutionsCountObj(Integer executionsCountObj);

    /**
     * Host Variable BJS-DATA-CONTENT-TYPE
     * 
     */
    String getDataContentType();

    void setDataContentType(String dataContentType);

    /**
     * Nullable property for BJS-DATA-CONTENT-TYPE
     * 
     */
    String getDataContentTypeObj();

    void setDataContentTypeObj(String dataContentTypeObj);

    /**
     * Nullable property for BJS-COD-MACROFUNCT
     * 
     */
    String getBjsCodMacrofunctObj();

    void setBjsCodMacrofunctObj(String bjsCodMacrofunctObj);

    /**
     * Nullable property for BJS-TP-MOVI
     * 
     */
    Integer getBjsTpMoviObj();

    void setBjsTpMoviObj(Integer bjsTpMoviObj);

    /**
     * Host Variable BJS-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Nullable property for BJS-IB-OGG
     * 
     */
    String getIbOggObj();

    void setIbOggObj(String ibOggObj);

    /**
     * Host Variable BJS-DATA-VCHAR
     * 
     */
    String getDataVchar();

    void setDataVchar(String dataVchar);

    /**
     * Nullable property for BJS-DATA-VCHAR
     * 
     */
    String getDataVcharObj();

    void setDataVcharObj(String dataVcharObj);

    /**
     * Host Variable IABV0002-STATE-CURRENT
     * 
     */
    char getIabv0002StateCurrent();

    void setIabv0002StateCurrent(char iabv0002StateCurrent);
};
