package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [COMP_QUEST]
 * 
 */
public interface ICompQuest extends BaseSqlTo {

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable Q04-COD-CAN
     * 
     */
    int getQ04CodCan();

    void setQ04CodCan(int q04CodCan);

    /**
     * Nullable property for Q04-COD-CAN
     * 
     */
    Integer getQ04CodCanObj();

    void setQ04CodCanObj(Integer q04CodCanObj);

    /**
     * Host Variable Q04-COD-QUEST
     * 
     */
    String getQ04CodQuest();

    void setQ04CodQuest(String q04CodQuest);

    /**
     * Host Variable Q04-COD-DOMANDA
     * 
     */
    String getQ04CodDomanda();

    void setQ04CodDomanda(String q04CodDomanda);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable Q04-ID-COMP-QUEST
     * 
     */
    int getIdCompQuest();

    void setIdCompQuest(int idCompQuest);

    /**
     * Host Variable Q04-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable Q04-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable Q04-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable Q04-ORDINE-DOMANDA
     * 
     */
    int getOrdineDomanda();

    void setOrdineDomanda(int ordineDomanda);

    /**
     * Host Variable Q04-COD-RISP
     * 
     */
    String getCodRisp();

    void setCodRisp(String codRisp);

    /**
     * Host Variable Q04-OBBL-RISP
     * 
     */
    char getObblRisp();

    void setObblRisp(char obblRisp);

    /**
     * Nullable property for Q04-OBBL-RISP
     * 
     */
    Character getObblRispObj();

    void setObblRispObj(Character obblRispObj);
};
