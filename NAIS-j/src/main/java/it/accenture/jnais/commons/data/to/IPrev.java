package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PREV]
 * 
 */
public interface IPrev extends BaseSqlTo {

    /**
     * Host Variable L27-ID-PREV
     * 
     */
    int getL27IdPrev();

    void setL27IdPrev(int l27IdPrev);

    /**
     * Host Variable L27-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L27-STAT-PREV
     * 
     */
    String getStatPrev();

    void setStatPrev(String statPrev);

    /**
     * Host Variable L27-ID-OGG
     * 
     */
    int getIdOgg();

    void setIdOgg(int idOgg);

    /**
     * Nullable property for L27-ID-OGG
     * 
     */
    Integer getIdOggObj();

    void setIdOggObj(Integer idOggObj);

    /**
     * Host Variable L27-TP-OGG
     * 
     */
    String getTpOgg();

    void setTpOgg(String tpOgg);

    /**
     * Nullable property for L27-TP-OGG
     * 
     */
    String getTpOggObj();

    void setTpOggObj(String tpOggObj);

    /**
     * Host Variable L27-IB-OGG
     * 
     */
    String getIbOgg();

    void setIbOgg(String ibOgg);

    /**
     * Host Variable L27-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L27-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L27-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable L27-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable L27-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
