package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IStatOggBusTrchDiGar;

/**
 * Data Access Object(DAO) for tables [STAT_OGG_BUS, TRCH_DI_GAR]
 * 
 */
public class StatOggBusTrchDiGarDao extends BaseSqlDao<IStatOggBusTrchDiGar> {

    private Cursor cEffTga;
    private Cursor cEffTgaDi;
    private Cursor cCpzTga;
    private Cursor cCpzTgaDi;
    private Cursor cEffTgaNs;
    private Cursor cEffTgaDiNs;
    private Cursor cCpzTgaNs;
    private Cursor cCpzTgaDiNs;
    private Cursor cEffTgaNc;
    private Cursor cEffTgaDiNc;
    private Cursor cCpzTgaNc;
    private Cursor cCpzTgaDiNc;
    private Cursor cEffTgaNsNc;
    private Cursor cEffTgaDiNsNc;
    private Cursor cCpzTgaNsNc;
    private Cursor cCpzTgaDiNsNc;
    private Cursor cEff15;
    private Cursor cCpz15;
    private Cursor cEff20;
    private Cursor cCpz20;
    private Cursor cEff41;
    private Cursor cCpz41;
    private Cursor curWc2Stb;
    private final IRowMapper<IStatOggBusTrchDiGar> fetchCurWc2StbRm = buildNamedRowMapper(IStatOggBusTrchDiGar.class, "tgaIdTrchDiGar", "tgaIdGar", "tgaIdAdes", "tgaIdPoli", "tgaIdMoviCrz", "tgaIdMoviChiuObj", "tgaDtIniEffDb", "tgaDtEndEffDb", "tgaCodCompAnia", "tgaDtDecorDb", "tgaDtScadDbObj", "tgaIbOggObj", "tgaTpRgmFisc", "tgaDtEmisDbObj", "tgaTpTrch", "tgaDurAaObj", "tgaDurMmObj", "tgaDurGgObj", "tgaPreCasoMorObj", "tgaPcIntrRiatObj", "tgaImpBnsAnticObj", "tgaPreIniNetObj", "tgaPrePpIniObj", "tgaPrePpUltObj", "tgaPreTariIniObj", "tgaPreTariUltObj", "tgaPreInvrioIniObj", "tgaPreInvrioUltObj", "tgaPreRivtoObj", "tgaImpSoprProfObj", "tgaImpSoprSanObj", "tgaImpSoprSpoObj", "tgaImpSoprTecObj", "tgaImpAltSoprObj", "tgaPreStabObj", "tgaDtEffStabDbObj", "tgaTsRivalFisObj", "tgaTsRivalIndicizObj", "tgaOldTsTecObj", "tgaRatLrdObj", "tgaPreLrdObj", "tgaPrstzIniObj", "tgaPrstzUltObj", "tgaCptInOpzRivtoObj", "tgaPrstzIniStabObj", "tgaCptRshMorObj", "tgaPrstzRidIniObj", "tgaFlCarContObj", "tgaBnsGiaLiqtoObj", "tgaImpBnsObj", "tgaCodDvs", "tgaPrstzIniNewfisObj", "tgaImpSconObj", "tgaAlqSconObj", "tgaImpCarAcqObj", "tgaImpCarIncObj", "tgaImpCarGestObj", "tgaEtaAa1oAsstoObj", "tgaEtaMm1oAsstoObj", "tgaEtaAa2oAsstoObj", "tgaEtaMm2oAsstoObj", "tgaEtaAa3oAsstoObj", "tgaEtaMm3oAsstoObj", "tgaRendtoLrdObj", "tgaPcRetrObj", "tgaRendtoRetrObj", "tgaMinGartoObj", "tgaMinTrnutObj", "tgaPreAttDiTrchObj", "tgaMatuEnd2000Obj", "tgaAbbTotIniObj", "tgaAbbTotUltObj", "tgaAbbAnnuUltObj", "tgaDurAbbObj", "tgaTpAdegAbbObj", "tgaModCalcObj", "tgaImpAzObj", "tgaImpAderObj", "tgaImpTfrObj", "tgaImpVoloObj", "tgaVisEnd2000Obj", "tgaDtVldtProdDbObj", "tgaDtIniValTarDbObj", "tgaImpbVisEnd2000Obj", "tgaRenIniTsTec0Obj", "tgaPcRipPreObj", "tgaFlImportiForzObj", "tgaPrstzIniNforzObj", "tgaVisEnd2000NforzObj", "tgaIntrMoraObj", "tgaManfeeAnticObj", "tgaManfeeRicorObj", "tgaPreUniRivtoObj", "tgaProv1aaAcqObj", "tgaProv2aaAcqObj", "tgaProvRicorObj", "tgaProvIncObj", "tgaAlqProvAcqObj", "tgaAlqProvIncObj", "tgaAlqProvRicorObj", "tgaImpbProvAcqObj", "tgaImpbProvIncObj", "tgaImpbProvRicorObj", "tgaFlProvForzObj", "tgaPrstzAggIniObj", "tgaIncrPreObj", "tgaIncrPrstzObj", "tgaDtUltAdegPrePrDbObj", "tgaPrstzAggUltObj", "tgaTsRivalNetObj", "tgaPrePattuitoObj", "tgaTpRivalObj", "tgaRisMatObj", "tgaCptMinScadObj", "tgaCommisGestObj", "tgaTpManfeeApplObj", "tgaDsRiga", "tgaDsOperSql", "tgaDsVer", "tgaDsTsIniCptz", "tgaDsTsEndCptz", "tgaDsUtente", "tgaDsStatoElab", "tgaPcCommisGestObj", "tgaNumGgRivalObj", "tgaImpTrasfeObj", "tgaImpTfrStrcObj", "tgaAcqExpObj", "tgaRemunAssObj", "tgaCommisInterObj", "tgaAlqRemunAssObj", "tgaAlqCommisInterObj", "tgaImpbRemunAssObj", "tgaImpbCommisInterObj", "tgaCosRunAssvaObj", "tgaCosRunAssvaIdcObj", "stbIdStatOggBus", "stbIdOgg", "stbTpOgg", "stbIdMoviCrz", "stbIdMoviChiuObj", "stbDtIniEffDb", "stbDtEndEffDb", "stbCodCompAnia", "stbTpStatBus", "stbTpCaus", "stbDsRiga", "stbDsOperSql", "stbDsVer", "stbDsTsIniCptz", "stbDsTsEndCptz", "stbDsUtente", "stbDsStatoElab");
    private final IRowMapper<IStatOggBusTrchDiGar> fetchCurWc2Stb1Rm = buildNamedRowMapper(IStatOggBusTrchDiGar.class, "tgaIdTrchDiGar", "tgaIdGar", "tgaIdAdes", "tgaIdPoli", "tgaIdMoviCrz", "tgaIdMoviChiuObj", "tgaDtIniEffDb", "tgaDtEndEffDb", "tgaCodCompAnia", "tgaDtDecorDb", "tgaDtScadDbObj", "tgaIbOggObj", "tgaTpRgmFisc", "tgaDtEmisDbObj", "tgaTpTrch", "tgaDurAaObj", "tgaDurMmObj", "tgaDurGgObj", "tgaPreCasoMorObj", "tgaPcIntrRiatObj", "tgaImpBnsAnticObj", "tgaPreIniNetObj", "tgaPrePpIniObj", "tgaPrePpUltObj", "tgaPreTariIniObj", "tgaPreTariUltObj", "tgaPreInvrioIniObj", "tgaPreInvrioUltObj", "tgaPreRivtoObj", "tgaImpSoprProfObj", "tgaImpSoprSanObj", "tgaImpSoprSpoObj", "tgaImpSoprTecObj", "tgaImpAltSoprObj", "tgaPreStabObj", "tgaDtEffStabDbObj", "tgaTsRivalFisObj", "tgaTsRivalIndicizObj", "tgaOldTsTecObj", "tgaRatLrdObj", "tgaPreLrdObj", "tgaPrstzIniObj", "tgaPrstzUltObj", "tgaCptInOpzRivtoObj", "tgaPrstzIniStabObj", "tgaCptRshMorObj", "tgaPrstzRidIniObj", "tgaFlCarContObj", "tgaBnsGiaLiqtoObj", "tgaImpBnsObj", "tgaCodDvs", "tgaPrstzIniNewfisObj", "tgaImpSconObj", "tgaAlqSconObj", "tgaImpCarAcqObj", "tgaImpCarIncObj", "tgaImpCarGestObj", "tgaEtaAa1oAsstoObj", "tgaEtaMm1oAsstoObj", "tgaEtaAa2oAsstoObj", "tgaEtaMm2oAsstoObj", "tgaEtaAa3oAsstoObj", "tgaEtaMm3oAsstoObj", "tgaRendtoLrdObj", "tgaPcRetrObj", "tgaRendtoRetrObj", "tgaMinGartoObj", "tgaMinTrnutObj", "tgaPreAttDiTrchObj", "tgaMatuEnd2000Obj", "tgaAbbTotIniObj", "tgaAbbTotUltObj", "tgaAbbAnnuUltObj", "tgaDurAbbObj", "tgaTpAdegAbbObj", "tgaModCalcObj", "tgaImpAzObj", "tgaImpAderObj", "tgaImpTfrObj", "tgaImpVoloObj", "tgaVisEnd2000Obj", "tgaDtVldtProdDbObj", "tgaDtIniValTarDbObj", "tgaImpbVisEnd2000Obj", "tgaRenIniTsTec0Obj", "tgaPcRipPreObj", "tgaFlImportiForzObj", "tgaPrstzIniNforzObj", "tgaVisEnd2000NforzObj", "tgaIntrMoraObj", "tgaManfeeAnticObj", "tgaManfeeRicorObj", "tgaPreUniRivtoObj", "tgaProv1aaAcqObj", "tgaProv2aaAcqObj", "tgaProvRicorObj", "tgaProvIncObj", "tgaAlqProvAcqObj", "tgaAlqProvIncObj", "tgaAlqProvRicorObj", "tgaImpbProvAcqObj", "tgaImpbProvIncObj", "tgaImpbProvRicorObj", "tgaFlProvForzObj", "tgaPrstzAggIniObj", "tgaIncrPreObj", "tgaIncrPrstzObj", "tgaDtUltAdegPrePrDbObj", "tgaPrstzAggUltObj", "tgaTsRivalNetObj", "tgaPrePattuitoObj", "tgaTpRivalObj", "tgaDsRiga", "tgaDsOperSql", "tgaDsVer", "tgaDsTsIniCptz", "tgaDsTsEndCptz", "tgaDsUtente", "tgaDsStatoElab", "tgaPcCommisGestObj", "tgaNumGgRivalObj", "tgaImpTrasfeObj", "tgaImpTfrStrcObj", "tgaAcqExpObj", "tgaRemunAssObj", "tgaCommisInterObj", "tgaAlqRemunAssObj", "tgaAlqCommisInterObj", "tgaImpbRemunAssObj", "tgaImpbCommisInterObj", "tgaCosRunAssvaObj", "tgaCosRunAssvaIdcObj", "stbIdStatOggBus", "stbIdOgg", "stbTpOgg", "stbIdMoviCrz", "stbIdMoviChiuObj", "stbDtIniEffDb", "stbDtEndEffDb", "stbCodCompAnia", "stbTpStatBus", "stbTpCaus", "stbDsRiga", "stbDsOperSql", "stbDsVer", "stbDsTsIniCptz", "stbDsTsEndCptz", "stbDsUtente", "stbDsStatoElab");
    private final IRowMapper<IStatOggBusTrchDiGar> fetchCEffTgaRm = buildNamedRowMapper(IStatOggBusTrchDiGar.class, "tgaIdTrchDiGar", "tgaIdGar", "tgaIdAdes", "tgaIdPoli", "tgaIdMoviCrz", "tgaIdMoviChiuObj", "tgaDtIniEffDb", "tgaDtEndEffDb", "tgaCodCompAnia", "tgaDtDecorDb", "tgaDtScadDbObj", "tgaIbOggObj", "tgaTpRgmFisc", "tgaDtEmisDbObj", "tgaTpTrch", "tgaDurAaObj", "tgaDurMmObj", "tgaDurGgObj", "tgaPreCasoMorObj", "tgaPcIntrRiatObj", "tgaImpBnsAnticObj", "tgaPreIniNetObj", "tgaPrePpIniObj", "tgaPrePpUltObj", "tgaPreTariIniObj", "tgaPreTariUltObj", "tgaPreInvrioIniObj", "tgaPreInvrioUltObj", "tgaPreRivtoObj", "tgaImpSoprProfObj", "tgaImpSoprSanObj", "tgaImpSoprSpoObj", "tgaImpSoprTecObj", "tgaImpAltSoprObj", "tgaPreStabObj", "tgaDtEffStabDbObj", "tgaTsRivalFisObj", "tgaTsRivalIndicizObj", "tgaOldTsTecObj", "tgaRatLrdObj", "tgaPreLrdObj", "tgaPrstzIniObj", "tgaPrstzUltObj", "tgaCptInOpzRivtoObj", "tgaPrstzIniStabObj", "tgaCptRshMorObj", "tgaPrstzRidIniObj", "tgaFlCarContObj", "tgaBnsGiaLiqtoObj", "tgaImpBnsObj", "tgaCodDvs", "tgaPrstzIniNewfisObj", "tgaImpSconObj", "tgaAlqSconObj", "tgaImpCarAcqObj", "tgaImpCarIncObj", "tgaImpCarGestObj", "tgaEtaAa1oAsstoObj", "tgaEtaMm1oAsstoObj", "tgaEtaAa2oAsstoObj", "tgaEtaMm2oAsstoObj", "tgaEtaAa3oAsstoObj", "tgaEtaMm3oAsstoObj", "tgaRendtoLrdObj", "tgaPcRetrObj", "tgaRendtoRetrObj", "tgaMinGartoObj", "tgaMinTrnutObj", "tgaPreAttDiTrchObj", "tgaMatuEnd2000Obj", "tgaAbbTotIniObj", "tgaAbbTotUltObj", "tgaAbbAnnuUltObj", "tgaDurAbbObj", "tgaTpAdegAbbObj", "tgaModCalcObj", "tgaImpAzObj", "tgaImpAderObj", "tgaImpTfrObj", "tgaImpVoloObj", "tgaVisEnd2000Obj", "tgaDtVldtProdDbObj", "tgaDtIniValTarDbObj", "tgaImpbVisEnd2000Obj", "tgaRenIniTsTec0Obj", "tgaPcRipPreObj", "tgaFlImportiForzObj", "tgaPrstzIniNforzObj", "tgaVisEnd2000NforzObj", "tgaIntrMoraObj", "tgaManfeeAnticObj", "tgaManfeeRicorObj", "tgaPreUniRivtoObj", "tgaProv1aaAcqObj", "tgaProv2aaAcqObj", "tgaProvRicorObj", "tgaProvIncObj", "tgaAlqProvAcqObj", "tgaAlqProvIncObj", "tgaAlqProvRicorObj", "tgaImpbProvAcqObj", "tgaImpbProvIncObj", "tgaImpbProvRicorObj", "tgaFlProvForzObj", "tgaPrstzAggIniObj", "tgaIncrPreObj", "tgaIncrPrstzObj", "tgaDtUltAdegPrePrDbObj", "tgaPrstzAggUltObj", "tgaTsRivalNetObj", "tgaPrePattuitoObj", "tgaTpRivalObj", "tgaRisMatObj", "tgaCptMinScadObj", "tgaCommisGestObj", "tgaTpManfeeApplObj", "tgaDsRiga", "tgaDsOperSql", "tgaDsVer", "tgaDsTsIniCptz", "tgaDsTsEndCptz", "tgaDsUtente", "tgaDsStatoElab", "tgaPcCommisGestObj", "tgaNumGgRivalObj", "tgaImpTrasfeObj", "tgaImpTfrStrcObj", "tgaAcqExpObj", "tgaRemunAssObj", "tgaCommisInterObj", "tgaAlqRemunAssObj", "tgaAlqCommisInterObj", "tgaImpbRemunAssObj", "tgaImpbCommisInterObj", "tgaCosRunAssvaObj", "tgaCosRunAssvaIdcObj");
    private final IRowMapper<IStatOggBusTrchDiGar> fetchCEffTgaDiRm = buildNamedRowMapper(IStatOggBusTrchDiGar.class, "tgaDtVldtProdDbObj");
    private final IRowMapper<IStatOggBusTrchDiGar> selectRec10Rm = buildNamedRowMapper(IStatOggBusTrchDiGar.class, "tgaIdTrchDiGar", "tgaIdGar", "tgaIdAdes", "tgaIdPoli", "tgaIdMoviCrz", "tgaIdMoviChiuObj", "tgaDtIniEffDb", "tgaDtEndEffDb", "tgaCodCompAnia", "tgaDtDecorDb", "tgaDtScadDbObj", "tgaIbOggObj", "tgaTpRgmFisc", "tgaDtEmisDbObj", "tgaTpTrch", "tgaDurAaObj", "tgaDurMmObj", "tgaDurGgObj", "tgaPreCasoMorObj", "tgaPcIntrRiatObj", "tgaImpBnsAnticObj", "tgaPreIniNetObj", "tgaPrePpIniObj", "tgaPrePpUltObj", "tgaPreTariIniObj", "tgaPreTariUltObj", "tgaPreInvrioIniObj", "tgaPreInvrioUltObj", "tgaPreRivtoObj", "tgaImpSoprProfObj", "tgaImpSoprSanObj", "tgaImpSoprSpoObj", "tgaImpSoprTecObj", "tgaImpAltSoprObj", "tgaPreStabObj", "tgaDtEffStabDbObj", "tgaTsRivalFisObj", "tgaTsRivalIndicizObj", "tgaOldTsTecObj", "tgaRatLrdObj", "tgaPreLrdObj", "tgaPrstzIniObj", "tgaPrstzUltObj", "tgaCptInOpzRivtoObj", "tgaPrstzIniStabObj", "tgaCptRshMorObj", "tgaPrstzRidIniObj", "tgaFlCarContObj", "tgaBnsGiaLiqtoObj", "tgaImpBnsObj", "tgaCodDvs", "tgaPrstzIniNewfisObj", "tgaImpSconObj", "tgaAlqSconObj", "tgaImpCarAcqObj", "tgaImpCarIncObj", "tgaImpCarGestObj", "tgaEtaAa1oAsstoObj", "tgaEtaMm1oAsstoObj", "tgaEtaAa2oAsstoObj", "tgaEtaMm2oAsstoObj", "tgaEtaAa3oAsstoObj", "tgaEtaMm3oAsstoObj", "tgaRendtoLrdObj", "tgaPcRetrObj", "tgaRendtoRetrObj", "tgaMinGartoObj", "tgaMinTrnutObj", "tgaPreAttDiTrchObj", "tgaMatuEnd2000Obj", "tgaAbbTotIniObj", "tgaAbbTotUltObj", "tgaAbbAnnuUltObj", "tgaDurAbbObj", "tgaTpAdegAbbObj", "tgaModCalcObj", "tgaImpAzObj", "tgaImpAderObj", "tgaImpTfrObj", "tgaImpVoloObj", "tgaVisEnd2000Obj", "tgaDtVldtProdDbObj", "tgaDtIniValTarDbObj", "tgaImpbVisEnd2000Obj", "tgaRenIniTsTec0Obj", "tgaPcRipPreObj", "tgaFlImportiForzObj", "tgaPrstzIniNforzObj", "tgaVisEnd2000NforzObj", "tgaIntrMoraObj", "tgaManfeeAnticObj", "tgaManfeeRicorObj", "tgaPreUniRivtoObj", "tgaProv1aaAcqObj", "tgaProv2aaAcqObj", "tgaProvRicorObj", "tgaProvIncObj", "tgaAlqProvAcqObj", "tgaAlqProvIncObj", "tgaAlqProvRicorObj", "tgaImpbProvAcqObj", "tgaImpbProvIncObj", "tgaImpbProvRicorObj", "tgaFlProvForzObj", "tgaPrstzAggIniObj", "tgaIncrPreObj", "tgaIncrPrstzObj", "tgaDtUltAdegPrePrDbObj", "tgaPrstzAggUltObj", "tgaTsRivalNetObj", "tgaPrePattuitoObj", "tgaTpRivalObj", "tgaRisMatObj", "tgaCptMinScadObj", "tgaCommisGestObj", "tgaTpManfeeApplObj", "tgaDsRiga", "tgaDsOperSql", "tgaDsVer", "tgaDsTsIniCptz", "tgaDsTsEndCptz", "tgaDsUtente", "tgaDsStatoElab", "tgaPcCommisGestObj", "tgaNumGgRivalObj", "tgaImpTrasfeObj", "tgaImpTfrStrcObj", "tgaAcqExpObj", "tgaRemunAssObj", "tgaCommisInterObj", "tgaAlqRemunAssObj", "tgaAlqCommisInterObj", "tgaImpbRemunAssObj", "tgaImpbCommisInterObj", "stbIdStatOggBus", "stbIdOgg", "stbTpOgg", "stbIdMoviCrz", "stbIdMoviChiuObj", "stbDtIniEffDb", "stbDtEndEffDb", "stbCodCompAnia", "stbTpStatBus", "stbTpCaus", "stbDsRiga", "stbDsOperSql", "stbDsVer", "stbDsTsIniCptz", "stbDsTsEndCptz", "stbDsUtente", "stbDsStatoElab");

    public StatOggBusTrchDiGarDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IStatOggBusTrchDiGar> getToClass() {
        return IStatOggBusTrchDiGar.class;
    }

    public IStatOggBusTrchDiGar fetchCurWc2Stb(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(curWc2Stb, iStatOggBusTrchDiGar, fetchCurWc2StbRm);
    }

    public IStatOggBusTrchDiGar selectRec(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return buildQuery("selectRec").bind(iStatOggBusTrchDiGar).rowMapper(fetchCurWc2StbRm).singleResult(iStatOggBusTrchDiGar);
    }

    public IStatOggBusTrchDiGar fetchCurWc2Stb1(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(curWc2Stb, iStatOggBusTrchDiGar, fetchCurWc2Stb1Rm);
    }

    public DbAccessStatus openCEffTga(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEffTga = buildQuery("openCEffTga").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffTga() {
        return closeCursor(cEffTga);
    }

    public IStatOggBusTrchDiGar fetchCEffTga(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEffTga, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public DbAccessStatus openCEffTgaDi(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEffTgaDi = buildQuery("openCEffTgaDi").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffTgaDi() {
        return closeCursor(cEffTgaDi);
    }

    public IStatOggBusTrchDiGar fetchCEffTgaDi(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEffTgaDi, iStatOggBusTrchDiGar, fetchCEffTgaDiRm);
    }

    public DbAccessStatus openCCpzTga(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpzTga = buildQuery("openCCpzTga").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzTga() {
        return closeCursor(cCpzTga);
    }

    public IStatOggBusTrchDiGar fetchCCpzTga(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpzTga, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public DbAccessStatus openCCpzTgaDi(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpzTgaDi = buildQuery("openCCpzTgaDi").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzTgaDi() {
        return closeCursor(cCpzTgaDi);
    }

    public IStatOggBusTrchDiGar fetchCCpzTgaDi(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpzTgaDi, iStatOggBusTrchDiGar, fetchCEffTgaDiRm);
    }

    public DbAccessStatus openCEffTgaNs(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEffTgaNs = buildQuery("openCEffTgaNs").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffTgaNs() {
        return closeCursor(cEffTgaNs);
    }

    public IStatOggBusTrchDiGar fetchCEffTgaNs(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEffTgaNs, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public DbAccessStatus openCEffTgaDiNs(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEffTgaDiNs = buildQuery("openCEffTgaDiNs").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffTgaDiNs() {
        return closeCursor(cEffTgaDiNs);
    }

    public IStatOggBusTrchDiGar fetchCEffTgaDiNs(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEffTgaDiNs, iStatOggBusTrchDiGar, fetchCEffTgaDiRm);
    }

    public DbAccessStatus openCCpzTgaNs(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpzTgaNs = buildQuery("openCCpzTgaNs").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzTgaNs() {
        return closeCursor(cCpzTgaNs);
    }

    public IStatOggBusTrchDiGar fetchCCpzTgaNs(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpzTgaNs, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public DbAccessStatus openCCpzTgaDiNs(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpzTgaDiNs = buildQuery("openCCpzTgaDiNs").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzTgaDiNs() {
        return closeCursor(cCpzTgaDiNs);
    }

    public IStatOggBusTrchDiGar fetchCCpzTgaDiNs(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpzTgaDiNs, iStatOggBusTrchDiGar, fetchCEffTgaDiRm);
    }

    public DbAccessStatus openCEffTgaNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEffTgaNc = buildQuery("openCEffTgaNc").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffTgaNc() {
        return closeCursor(cEffTgaNc);
    }

    public IStatOggBusTrchDiGar fetchCEffTgaNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEffTgaNc, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public DbAccessStatus openCEffTgaDiNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEffTgaDiNc = buildQuery("openCEffTgaDiNc").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffTgaDiNc() {
        return closeCursor(cEffTgaDiNc);
    }

    public IStatOggBusTrchDiGar fetchCEffTgaDiNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEffTgaDiNc, iStatOggBusTrchDiGar, fetchCEffTgaDiRm);
    }

    public DbAccessStatus openCCpzTgaNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpzTgaNc = buildQuery("openCCpzTgaNc").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzTgaNc() {
        return closeCursor(cCpzTgaNc);
    }

    public IStatOggBusTrchDiGar fetchCCpzTgaNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpzTgaNc, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public DbAccessStatus openCCpzTgaDiNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpzTgaDiNc = buildQuery("openCCpzTgaDiNc").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzTgaDiNc() {
        return closeCursor(cCpzTgaDiNc);
    }

    public IStatOggBusTrchDiGar fetchCCpzTgaDiNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpzTgaDiNc, iStatOggBusTrchDiGar, fetchCEffTgaDiRm);
    }

    public DbAccessStatus openCEffTgaNsNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEffTgaNsNc = buildQuery("openCEffTgaNsNc").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffTgaNsNc() {
        return closeCursor(cEffTgaNsNc);
    }

    public IStatOggBusTrchDiGar fetchCEffTgaNsNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEffTgaNsNc, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public DbAccessStatus openCEffTgaDiNsNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEffTgaDiNsNc = buildQuery("openCEffTgaDiNsNc").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEffTgaDiNsNc() {
        return closeCursor(cEffTgaDiNsNc);
    }

    public IStatOggBusTrchDiGar fetchCEffTgaDiNsNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEffTgaDiNsNc, iStatOggBusTrchDiGar, fetchCEffTgaDiRm);
    }

    public DbAccessStatus openCCpzTgaNsNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpzTgaNsNc = buildQuery("openCCpzTgaNsNc").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzTgaNsNc() {
        return closeCursor(cCpzTgaNsNc);
    }

    public IStatOggBusTrchDiGar fetchCCpzTgaNsNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpzTgaNsNc, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public DbAccessStatus openCCpzTgaDiNsNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpzTgaDiNsNc = buildQuery("openCCpzTgaDiNsNc").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpzTgaDiNsNc() {
        return closeCursor(cCpzTgaDiNsNc);
    }

    public IStatOggBusTrchDiGar fetchCCpzTgaDiNsNc(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpzTgaDiNsNc, iStatOggBusTrchDiGar, fetchCEffTgaDiRm);
    }

    public AfDecimal selectRec1(int ldbv2891IdPol, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec1").bind("ldbv2891IdPol", ldbv2891IdPol).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec2(int ldbv2891IdPol, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, long wsTsCompetenza, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec2").bind("ldbv2891IdPol", ldbv2891IdPol).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).bind("wsTsCompetenza", wsTsCompetenza).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec3(int ldbv2911IdPoli, int ldbv2911IdAdes, int idsv0003CodiceCompagniaAnia, String wsDataInizioEffettoDb, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec3").bind("ldbv2911IdPoli", ldbv2911IdPoli).bind("ldbv2911IdAdes", ldbv2911IdAdes).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).bind("wsDataInizioEffettoDb", wsDataInizioEffettoDb).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec4(IStatOggBusTrchDiGar iStatOggBusTrchDiGar, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec4").bind(iStatOggBusTrchDiGar).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public IStatOggBusTrchDiGar selectRec5(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return buildQuery("selectRec5").bind(iStatOggBusTrchDiGar).rowMapper(fetchCEffTgaRm).singleResult(iStatOggBusTrchDiGar);
    }

    public DbAccessStatus openCEff15(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEff15 = buildQuery("openCEff15").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff15() {
        return closeCursor(cEff15);
    }

    public IStatOggBusTrchDiGar fetchCEff15(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEff15, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public IStatOggBusTrchDiGar selectRec6(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return buildQuery("selectRec6").bind(iStatOggBusTrchDiGar).rowMapper(fetchCEffTgaRm).singleResult(iStatOggBusTrchDiGar);
    }

    public DbAccessStatus openCCpz15(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpz15 = buildQuery("openCCpz15").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz15() {
        return closeCursor(cCpz15);
    }

    public IStatOggBusTrchDiGar fetchCCpz15(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpz15, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public IStatOggBusTrchDiGar selectRec7(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return buildQuery("selectRec7").bind(iStatOggBusTrchDiGar).rowMapper(fetchCurWc2StbRm).singleResult(iStatOggBusTrchDiGar);
    }

    public DbAccessStatus openCEff20(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEff20 = buildQuery("openCEff20").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff20() {
        return closeCursor(cEff20);
    }

    public IStatOggBusTrchDiGar fetchCEff20(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEff20, iStatOggBusTrchDiGar, fetchCurWc2StbRm);
    }

    public IStatOggBusTrchDiGar selectRec8(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return buildQuery("selectRec8").bind(iStatOggBusTrchDiGar).rowMapper(fetchCurWc2StbRm).singleResult(iStatOggBusTrchDiGar);
    }

    public DbAccessStatus openCCpz20(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpz20 = buildQuery("openCCpz20").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz20() {
        return closeCursor(cCpz20);
    }

    public IStatOggBusTrchDiGar fetchCCpz20(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpz20, iStatOggBusTrchDiGar, fetchCurWc2StbRm);
    }

    public IStatOggBusTrchDiGar selectRec9(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return buildQuery("selectRec9").bind(iStatOggBusTrchDiGar).rowMapper(fetchCurWc2StbRm).singleResult(iStatOggBusTrchDiGar);
    }

    public IStatOggBusTrchDiGar selectRec10(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return buildQuery("selectRec10").bind(iStatOggBusTrchDiGar).rowMapper(selectRec10Rm).singleResult(iStatOggBusTrchDiGar);
    }

    public DbAccessStatus openCEff41(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cEff41 = buildQuery("openCEff41").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCEff41() {
        return closeCursor(cEff41);
    }

    public IStatOggBusTrchDiGar fetchCEff41(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cEff41, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public DbAccessStatus openCCpz41(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        cCpz41 = buildQuery("openCCpz41").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCCpz41() {
        return closeCursor(cCpz41);
    }

    public IStatOggBusTrchDiGar fetchCCpz41(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        return fetch(cCpz41, iStatOggBusTrchDiGar, fetchCEffTgaRm);
    }

    public AfDecimal selectRec11(IStatOggBusTrchDiGar iStatOggBusTrchDiGar, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec11").bind(iStatOggBusTrchDiGar).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec12(IStatOggBusTrchDiGar iStatOggBusTrchDiGar, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec12").bind(iStatOggBusTrchDiGar).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec13(IStatOggBusTrchDiGar iStatOggBusTrchDiGar, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec13").bind(iStatOggBusTrchDiGar).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public AfDecimal selectRec14(IStatOggBusTrchDiGar iStatOggBusTrchDiGar, AfDecimal dft) {
        return new AfDecimal(buildQuery("selectRec14").bind(iStatOggBusTrchDiGar).scalarResultDecimal(dft.copy()), 15, 3);
    }

    public DbAccessStatus openCurWc2Stb(IStatOggBusTrchDiGar iStatOggBusTrchDiGar) {
        curWc2Stb = buildQuery("openCurWc2Stb").bind(iStatOggBusTrchDiGar).open();
        return dbStatus;
    }

    public DbAccessStatus closeCurWc2Stb() {
        return closeCursor(curWc2Stb);
    }
}
