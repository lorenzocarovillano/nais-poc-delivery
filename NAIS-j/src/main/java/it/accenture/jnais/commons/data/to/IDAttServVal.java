package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [D_ATT_SERV_VAL]
 * 
 */
public interface IDAttServVal extends BaseSqlTo {

    /**
     * Host Variable P89-ID-OGG
     * 
     */
    int getP89IdOgg();

    void setP89IdOgg(int p89IdOgg);

    /**
     * Host Variable P89-TP-OGG
     * 
     */
    String getP89TpOgg();

    void setP89TpOgg(String p89TpOgg);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);

    /**
     * Host Variable P89-ID-D-ATT-SERV-VAL
     * 
     */
    int getIdDAttServVal();

    void setIdDAttServVal(int idDAttServVal);

    /**
     * Host Variable P89-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable P89-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable P89-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for P89-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable P89-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable P89-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable P89-ID-ATT-SERV-VAL
     * 
     */
    int getIdAttServVal();

    void setIdAttServVal(int idAttServVal);

    /**
     * Host Variable P89-TP-SERV-VAL
     * 
     */
    String getTpServVal();

    void setTpServVal(String tpServVal);

    /**
     * Host Variable P89-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Host Variable P89-DT-INI-CNTRL-FND-DB
     * 
     */
    String getDtIniCntrlFndDb();

    void setDtIniCntrlFndDb(String dtIniCntrlFndDb);

    /**
     * Nullable property for P89-DT-INI-CNTRL-FND-DB
     * 
     */
    String getDtIniCntrlFndDbObj();

    void setDtIniCntrlFndDbObj(String dtIniCntrlFndDbObj);

    /**
     * Host Variable P89-DS-RIGA
     * 
     */
    long getP89DsRiga();

    void setP89DsRiga(long p89DsRiga);

    /**
     * Host Variable P89-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable P89-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable P89-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable P89-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable P89-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable P89-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);
};
