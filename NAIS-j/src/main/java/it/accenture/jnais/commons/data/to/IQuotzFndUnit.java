package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [QUOTZ_FND_UNIT]
 * 
 */
public interface IQuotzFndUnit extends BaseSqlTo {

    /**
     * Host Variable L19-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable L19-COD-FND
     * 
     */
    String getCodFnd();

    void setCodFnd(String codFnd);

    /**
     * Host Variable L19-DT-QTZ-DB
     * 
     */
    String getDtQtzDb();

    void setDtQtzDb(String dtQtzDb);

    /**
     * Host Variable L19-VAL-QUO
     * 
     */
    AfDecimal getValQuo();

    void setValQuo(AfDecimal valQuo);

    /**
     * Nullable property for L19-VAL-QUO
     * 
     */
    AfDecimal getValQuoObj();

    void setValQuoObj(AfDecimal valQuoObj);

    /**
     * Host Variable L19-VAL-QUO-MANFEE
     * 
     */
    AfDecimal getValQuoManfee();

    void setValQuoManfee(AfDecimal valQuoManfee);

    /**
     * Nullable property for L19-VAL-QUO-MANFEE
     * 
     */
    AfDecimal getValQuoManfeeObj();

    void setValQuoManfeeObj(AfDecimal valQuoManfeeObj);

    /**
     * Host Variable L19-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable L19-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable L19-DS-TS-CPTZ
     * 
     */
    long getDsTsCptz();

    void setDsTsCptz(long dsTsCptz);

    /**
     * Host Variable L19-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable L19-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable L19-TP-FND
     * 
     */
    char getTpFnd();

    void setTpFnd(char tpFnd);

    /**
     * Host Variable L19-DT-RILEVAZIONE-NAV-DB
     * 
     */
    String getDtRilevazioneNavDb();

    void setDtRilevazioneNavDb(String dtRilevazioneNavDb);

    /**
     * Nullable property for L19-DT-RILEVAZIONE-NAV-DB
     * 
     */
    String getDtRilevazioneNavDbObj();

    void setDtRilevazioneNavDbObj(String dtRilevazioneNavDbObj);

    /**
     * Host Variable L19-VAL-QUO-ACQ
     * 
     */
    AfDecimal getValQuoAcq();

    void setValQuoAcq(AfDecimal valQuoAcq);

    /**
     * Nullable property for L19-VAL-QUO-ACQ
     * 
     */
    AfDecimal getValQuoAcqObj();

    void setValQuoAcqObj(AfDecimal valQuoAcqObj);

    /**
     * Host Variable L19-FL-NO-NAV
     * 
     */
    char getFlNoNav();

    void setFlNoNav(char flNoNav);

    /**
     * Nullable property for L19-FL-NO-NAV
     * 
     */
    Character getFlNoNavObj();

    void setFlNoNavObj(Character flNoNavObj);
};
