package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IBtcBatchType;

/**
 * Data Access Object(DAO) for table [BTC_BATCH_TYPE]
 * 
 */
public class BtcBatchTypeDao extends BaseSqlDao<IBtcBatchType> {

    private final IRowMapper<IBtcBatchType> selectByBbtCodBatchTypeRm = buildNamedRowMapper(IBtcBatchType.class, "bbtCodBatchType", "desVchar", "defContentTypeObj", "commitFrequencyObj", "serviceNameObj", "serviceGuideNameObj", "serviceAlpoNameObj", "codMacrofunctObj", "tpMoviObj");

    public BtcBatchTypeDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IBtcBatchType> getToClass() {
        return IBtcBatchType.class;
    }

    public IBtcBatchType selectByBbtCodBatchType(int bbtCodBatchType, IBtcBatchType iBtcBatchType) {
        return buildQuery("selectByBbtCodBatchType").bind("bbtCodBatchType", bbtCodBatchType).rowMapper(selectByBbtCodBatchTypeRm).singleResult(iBtcBatchType);
    }

    public IBtcBatchType selectRec(String codMacrofunct, Integer tpMovi, String defContentType, IBtcBatchType iBtcBatchType) {
        return buildQuery("selectRec").bind("codMacrofunct", codMacrofunct).bind("tpMovi", tpMovi).bind("defContentType", defContentType).rowMapper(selectByBbtCodBatchTypeRm).singleResult(iBtcBatchType);
    }
}
