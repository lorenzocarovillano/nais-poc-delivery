package it.accenture.jnais.commons.data.dao;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.modernsystems.jdbc.BaseSqlDao;
import com.modernsystems.jdbc.Cursor;
import com.modernsystems.jdbc.mapper.IRowMapper;
import it.accenture.jnais.commons.data.to.IAccComm;

/**
 * Data Access Object(DAO) for table [ACC_COMM]
 * 
 */
public class AccCommDao extends BaseSqlDao<IAccComm> {

    private Cursor cIbsNstP630;
    private Cursor cIbsNstP631;
    private final IRowMapper<IAccComm> selectByP63IdAccCommRm = buildNamedRowMapper(IAccComm.class, "p63IdAccComm", "tpAccComm", "codCompAnia", "codPartner", "ibAccComm", "ibAccCommMaster", "descAccCommVchar", "dtFirmaDb", "dtIniVldtDb", "dtEndVldtDb", "flInvioConferme", "dsOperSql", "dsVer", "dsTsCptz", "dsUtente", "dsStatoElab", "descAccCommMastVcharObj");

    public AccCommDao(DbAccessStatus dbAccessStatus) {
        super(dbAccessStatus);
    }

    @Override
    public Class<IAccComm> getToClass() {
        return IAccComm.class;
    }

    public IAccComm selectByP63IdAccComm(int p63IdAccComm, IAccComm iAccComm) {
        return buildQuery("selectByP63IdAccComm").bind("p63IdAccComm", p63IdAccComm).rowMapper(selectByP63IdAccCommRm).singleResult(iAccComm);
    }

    public DbAccessStatus insertRec(IAccComm iAccComm) {
        return buildQuery("insertRec").bind(iAccComm).executeInsert();
    }

    public DbAccessStatus updateRec(IAccComm iAccComm) {
        return buildQuery("updateRec").bind(iAccComm).executeUpdate();
    }

    public DbAccessStatus deleteByP63IdAccComm(int p63IdAccComm) {
        return buildQuery("deleteByP63IdAccComm").bind("p63IdAccComm", p63IdAccComm).executeDelete();
    }

    public IAccComm selectRec(String p63IbAccComm, int idsv0003CodiceCompagniaAnia, IAccComm iAccComm) {
        return buildQuery("selectRec").bind("p63IbAccComm", p63IbAccComm).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByP63IdAccCommRm).singleResult(iAccComm);
    }

    public IAccComm selectRec1(String p63IbAccCommMaster, int idsv0003CodiceCompagniaAnia, IAccComm iAccComm) {
        return buildQuery("selectRec1").bind("p63IbAccCommMaster", p63IbAccCommMaster).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).rowMapper(selectByP63IdAccCommRm).singleResult(iAccComm);
    }

    public DbAccessStatus openCIbsNstP630(String p63IbAccComm, int idsv0003CodiceCompagniaAnia) {
        cIbsNstP630 = buildQuery("openCIbsNstP630").bind("p63IbAccComm", p63IbAccComm).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus openCIbsNstP631(String p63IbAccCommMaster, int idsv0003CodiceCompagniaAnia) {
        cIbsNstP631 = buildQuery("openCIbsNstP631").bind("p63IbAccCommMaster", p63IbAccCommMaster).bind("idsv0003CodiceCompagniaAnia", idsv0003CodiceCompagniaAnia).open();
        return dbStatus;
    }

    public DbAccessStatus closeCIbsNstP630() {
        return closeCursor(cIbsNstP630);
    }

    public DbAccessStatus closeCIbsNstP631() {
        return closeCursor(cIbsNstP631);
    }

    public IAccComm fetchCIbsNstP630(IAccComm iAccComm) {
        return fetch(cIbsNstP630, iAccComm, selectByP63IdAccCommRm);
    }

    public IAccComm fetchCIbsNstP631(IAccComm iAccComm) {
        return fetch(cIbsNstP631, iAccComm, selectByP63IdAccCommRm);
    }
}
