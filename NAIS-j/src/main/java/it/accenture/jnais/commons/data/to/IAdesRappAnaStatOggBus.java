package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for tables [ADES, RAPP_ANA, STAT_OGG_BUS]
 * 
 */
public interface IAdesRappAnaStatOggBus extends BaseSqlTo {

    /**
     * Host Variable LDBV1691-IMP-TOT
     * 
     */
    int getLdbv1691ImpTot();

    void setLdbv1691ImpTot(int ldbv1691ImpTot);

    /**
     * Host Variable LDBV1691-ID-POLIZZA
     * 
     */
    int getLdbv1691IdPolizza();

    void setLdbv1691IdPolizza(int ldbv1691IdPolizza);

    /**
     * Host Variable LDBV1691-TP-STAT-BUS
     * 
     */
    String getLdbv1691TpStatBus();

    void setLdbv1691TpStatBus(String ldbv1691TpStatBus);

    /**
     * Host Variable IDSV0003-CODICE-COMPAGNIA-ANIA
     * 
     */
    int getIdsv0003CodiceCompagniaAnia();

    void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia);

    /**
     * Host Variable WS-DATA-INIZIO-EFFETTO-DB
     * 
     */
    String getWsDataInizioEffettoDb();

    void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb);

    /**
     * Host Variable WS-TS-COMPETENZA
     * 
     */
    long getWsTsCompetenza();

    void setWsTsCompetenza(long wsTsCompetenza);
};
