package it.accenture.jnais.commons.data.to;

import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PARAM_INFR_APPL]
 * 
 */
public interface IParamInfrAppl extends BaseSqlTo {

    /**
     * Host Variable D09-COD-COMP-ANIA
     * 
     */
    int getD09CodCompAnia();

    void setD09CodCompAnia(int d09CodCompAnia);

    /**
     * Host Variable D09-AMBIENTE
     * 
     */
    String getAmbiente();

    void setAmbiente(String ambiente);

    /**
     * Host Variable D09-PIATTAFORMA
     * 
     */
    String getPiattaforma();

    void setPiattaforma(String piattaforma);

    /**
     * Host Variable D09-TP-COM-COBOL-JAVA
     * 
     */
    String getTpComCobolJava();

    void setTpComCobolJava(String tpComCobolJava);

    /**
     * Host Variable D09-MQ-TP-UTILIZZO-API
     * 
     */
    String getMqTpUtilizzoApi();

    void setMqTpUtilizzoApi(String mqTpUtilizzoApi);

    /**
     * Nullable property for D09-MQ-TP-UTILIZZO-API
     * 
     */
    String getMqTpUtilizzoApiObj();

    void setMqTpUtilizzoApiObj(String mqTpUtilizzoApiObj);

    /**
     * Host Variable D09-MQ-QUEUE-MANAGER
     * 
     */
    String getMqQueueManager();

    void setMqQueueManager(String mqQueueManager);

    /**
     * Nullable property for D09-MQ-QUEUE-MANAGER
     * 
     */
    String getMqQueueManagerObj();

    void setMqQueueManagerObj(String mqQueueManagerObj);

    /**
     * Host Variable D09-MQ-CODA-PUT
     * 
     */
    String getMqCodaPut();

    void setMqCodaPut(String mqCodaPut);

    /**
     * Nullable property for D09-MQ-CODA-PUT
     * 
     */
    String getMqCodaPutObj();

    void setMqCodaPutObj(String mqCodaPutObj);

    /**
     * Host Variable D09-MQ-CODA-GET
     * 
     */
    String getMqCodaGet();

    void setMqCodaGet(String mqCodaGet);

    /**
     * Nullable property for D09-MQ-CODA-GET
     * 
     */
    String getMqCodaGetObj();

    void setMqCodaGetObj(String mqCodaGetObj);

    /**
     * Host Variable D09-MQ-OPZ-PERSISTENZA
     * 
     */
    char getMqOpzPersistenza();

    void setMqOpzPersistenza(char mqOpzPersistenza);

    /**
     * Nullable property for D09-MQ-OPZ-PERSISTENZA
     * 
     */
    Character getMqOpzPersistenzaObj();

    void setMqOpzPersistenzaObj(Character mqOpzPersistenzaObj);

    /**
     * Host Variable D09-MQ-OPZ-WAIT
     * 
     */
    char getMqOpzWait();

    void setMqOpzWait(char mqOpzWait);

    /**
     * Nullable property for D09-MQ-OPZ-WAIT
     * 
     */
    Character getMqOpzWaitObj();

    void setMqOpzWaitObj(Character mqOpzWaitObj);

    /**
     * Host Variable D09-MQ-OPZ-SYNCPOINT
     * 
     */
    char getMqOpzSyncpoint();

    void setMqOpzSyncpoint(char mqOpzSyncpoint);

    /**
     * Nullable property for D09-MQ-OPZ-SYNCPOINT
     * 
     */
    Character getMqOpzSyncpointObj();

    void setMqOpzSyncpointObj(Character mqOpzSyncpointObj);

    /**
     * Host Variable D09-MQ-ATTESA-RISPOSTA
     * 
     */
    char getMqAttesaRisposta();

    void setMqAttesaRisposta(char mqAttesaRisposta);

    /**
     * Nullable property for D09-MQ-ATTESA-RISPOSTA
     * 
     */
    Character getMqAttesaRispostaObj();

    void setMqAttesaRispostaObj(Character mqAttesaRispostaObj);

    /**
     * Host Variable D09-MQ-TEMPO-ATTESA-1
     * 
     */
    long getMqTempoAttesa1();

    void setMqTempoAttesa1(long mqTempoAttesa1);

    /**
     * Nullable property for D09-MQ-TEMPO-ATTESA-1
     * 
     */
    Long getMqTempoAttesa1Obj();

    void setMqTempoAttesa1Obj(Long mqTempoAttesa1Obj);

    /**
     * Host Variable D09-MQ-TEMPO-ATTESA-2
     * 
     */
    long getMqTempoAttesa2();

    void setMqTempoAttesa2(long mqTempoAttesa2);

    /**
     * Nullable property for D09-MQ-TEMPO-ATTESA-2
     * 
     */
    Long getMqTempoAttesa2Obj();

    void setMqTempoAttesa2Obj(Long mqTempoAttesa2Obj);

    /**
     * Host Variable D09-MQ-TEMPO-EXPIRY
     * 
     */
    long getMqTempoExpiry();

    void setMqTempoExpiry(long mqTempoExpiry);

    /**
     * Nullable property for D09-MQ-TEMPO-EXPIRY
     * 
     */
    Long getMqTempoExpiryObj();

    void setMqTempoExpiryObj(Long mqTempoExpiryObj);

    /**
     * Host Variable D09-CSOCKET-IP-ADDRESS
     * 
     */
    String getCsocketIpAddress();

    void setCsocketIpAddress(String csocketIpAddress);

    /**
     * Nullable property for D09-CSOCKET-IP-ADDRESS
     * 
     */
    String getCsocketIpAddressObj();

    void setCsocketIpAddressObj(String csocketIpAddressObj);

    /**
     * Host Variable D09-CSOCKET-PORT-NUM
     * 
     */
    int getCsocketPortNum();

    void setCsocketPortNum(int csocketPortNum);

    /**
     * Nullable property for D09-CSOCKET-PORT-NUM
     * 
     */
    Integer getCsocketPortNumObj();

    void setCsocketPortNumObj(Integer csocketPortNumObj);

    /**
     * Host Variable D09-FL-COMPRESSORE-C
     * 
     */
    char getFlCompressoreC();

    void setFlCompressoreC(char flCompressoreC);

    /**
     * Nullable property for D09-FL-COMPRESSORE-C
     * 
     */
    Character getFlCompressoreCObj();

    void setFlCompressoreCObj(Character flCompressoreCObj);
};
