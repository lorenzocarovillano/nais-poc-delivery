package it.accenture.jnais.commons.data.to;

import com.bphx.ctu.af.lang.types.AfDecimal;
import com.modernsystems.jdbc.BaseSqlTo;

/**
 * Interface Transfer Object(TO) for table [PERC_LIQ]
 * 
 */
public interface IPercLiq extends BaseSqlTo {

    /**
     * Host Variable PLI-ID-PERC-LIQ
     * 
     */
    int getIdPercLiq();

    void setIdPercLiq(int idPercLiq);

    /**
     * Host Variable PLI-ID-BNFICR-LIQ
     * 
     */
    int getIdBnficrLiq();

    void setIdBnficrLiq(int idBnficrLiq);

    /**
     * Host Variable PLI-ID-MOVI-CRZ
     * 
     */
    int getIdMoviCrz();

    void setIdMoviCrz(int idMoviCrz);

    /**
     * Host Variable PLI-ID-MOVI-CHIU
     * 
     */
    int getIdMoviChiu();

    void setIdMoviChiu(int idMoviChiu);

    /**
     * Nullable property for PLI-ID-MOVI-CHIU
     * 
     */
    Integer getIdMoviChiuObj();

    void setIdMoviChiuObj(Integer idMoviChiuObj);

    /**
     * Host Variable PLI-ID-RAPP-ANA
     * 
     */
    int getIdRappAna();

    void setIdRappAna(int idRappAna);

    /**
     * Nullable property for PLI-ID-RAPP-ANA
     * 
     */
    Integer getIdRappAnaObj();

    void setIdRappAnaObj(Integer idRappAnaObj);

    /**
     * Host Variable PLI-DT-INI-EFF-DB
     * 
     */
    String getDtIniEffDb();

    void setDtIniEffDb(String dtIniEffDb);

    /**
     * Host Variable PLI-DT-END-EFF-DB
     * 
     */
    String getDtEndEffDb();

    void setDtEndEffDb(String dtEndEffDb);

    /**
     * Host Variable PLI-COD-COMP-ANIA
     * 
     */
    int getCodCompAnia();

    void setCodCompAnia(int codCompAnia);

    /**
     * Host Variable PLI-PC-LIQ
     * 
     */
    AfDecimal getPcLiq();

    void setPcLiq(AfDecimal pcLiq);

    /**
     * Nullable property for PLI-PC-LIQ
     * 
     */
    AfDecimal getPcLiqObj();

    void setPcLiqObj(AfDecimal pcLiqObj);

    /**
     * Host Variable PLI-IMP-LIQ
     * 
     */
    AfDecimal getImpLiq();

    void setImpLiq(AfDecimal impLiq);

    /**
     * Nullable property for PLI-IMP-LIQ
     * 
     */
    AfDecimal getImpLiqObj();

    void setImpLiqObj(AfDecimal impLiqObj);

    /**
     * Host Variable PLI-TP-MEZ-PAG
     * 
     */
    String getTpMezPag();

    void setTpMezPag(String tpMezPag);

    /**
     * Nullable property for PLI-TP-MEZ-PAG
     * 
     */
    String getTpMezPagObj();

    void setTpMezPagObj(String tpMezPagObj);

    /**
     * Host Variable PLI-ITER-PAG-AVV
     * 
     */
    char getIterPagAvv();

    void setIterPagAvv(char iterPagAvv);

    /**
     * Nullable property for PLI-ITER-PAG-AVV
     * 
     */
    Character getIterPagAvvObj();

    void setIterPagAvvObj(Character iterPagAvvObj);

    /**
     * Host Variable PLI-DS-RIGA
     * 
     */
    long getPliDsRiga();

    void setPliDsRiga(long pliDsRiga);

    /**
     * Host Variable PLI-DS-OPER-SQL
     * 
     */
    char getDsOperSql();

    void setDsOperSql(char dsOperSql);

    /**
     * Host Variable PLI-DS-VER
     * 
     */
    int getDsVer();

    void setDsVer(int dsVer);

    /**
     * Host Variable PLI-DS-TS-INI-CPTZ
     * 
     */
    long getDsTsIniCptz();

    void setDsTsIniCptz(long dsTsIniCptz);

    /**
     * Host Variable PLI-DS-TS-END-CPTZ
     * 
     */
    long getDsTsEndCptz();

    void setDsTsEndCptz(long dsTsEndCptz);

    /**
     * Host Variable PLI-DS-UTENTE
     * 
     */
    String getDsUtente();

    void setDsUtente(String dsUtente);

    /**
     * Host Variable PLI-DS-STATO-ELAB
     * 
     */
    char getDsStatoElab();

    void setDsStatoElab(char dsStatoElab);

    /**
     * Host Variable PLI-DT-VLT-DB
     * 
     */
    String getDtVltDb();

    void setDtVltDb(String dtVltDb);

    /**
     * Nullable property for PLI-DT-VLT-DB
     * 
     */
    String getDtVltDbObj();

    void setDtVltDbObj(String dtVltDbObj);

    /**
     * Host Variable PLI-INT-CNT-CORR-ACCR-VCHAR
     * 
     */
    String getIntCntCorrAccrVchar();

    void setIntCntCorrAccrVchar(String intCntCorrAccrVchar);

    /**
     * Nullable property for PLI-INT-CNT-CORR-ACCR-VCHAR
     * 
     */
    String getIntCntCorrAccrVcharObj();

    void setIntCntCorrAccrVcharObj(String intCntCorrAccrVcharObj);

    /**
     * Host Variable PLI-COD-IBAN-RIT-CON
     * 
     */
    String getCodIbanRitCon();

    void setCodIbanRitCon(String codIbanRitCon);

    /**
     * Nullable property for PLI-COD-IBAN-RIT-CON
     * 
     */
    String getCodIbanRitConObj();

    void setCodIbanRitConObj(String codIbanRitConObj);
};
