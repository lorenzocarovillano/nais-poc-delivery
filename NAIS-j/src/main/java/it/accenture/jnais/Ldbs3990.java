package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.jdbc.FieldNotMappedException;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.ImpstSostDao;
import it.accenture.jnais.commons.data.to.IImpstSost;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.ImpstSostLdbs3990;
import it.accenture.jnais.ws.Ldbs3990Data;
import it.accenture.jnais.ws.redefines.IsoAlqIs;
import it.accenture.jnais.ws.redefines.IsoCumPreVers;
import it.accenture.jnais.ws.redefines.IsoDtEndPer;
import it.accenture.jnais.ws.redefines.IsoDtIniPer;
import it.accenture.jnais.ws.redefines.IsoIdMoviChiu;
import it.accenture.jnais.ws.redefines.IsoIdOgg;
import it.accenture.jnais.ws.redefines.IsoImpbIs;
import it.accenture.jnais.ws.redefines.IsoImpstSost;
import it.accenture.jnais.ws.redefines.IsoPrstzLrdAnteIs;
import it.accenture.jnais.ws.redefines.IsoPrstzNet;
import it.accenture.jnais.ws.redefines.IsoPrstzPrec;
import it.accenture.jnais.ws.redefines.IsoRisMatAnteTax;
import it.accenture.jnais.ws.redefines.IsoRisMatNetPrec;
import it.accenture.jnais.ws.redefines.IsoRisMatPostTax;

/**Original name: LDBS3990<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  07 MAR 2011.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO AD HOC PER ACCESSO RISORSE DB        *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Ldbs3990 extends Program implements IImpstSost {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private ImpstSostDao impstSostDao = new ImpstSostDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Ldbs3990Data ws = new Ldbs3990Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: IMPST-SOST
    private ImpstSostLdbs3990 impstSost;

    //==== METHODS ====
    /**Original name: PROGRAM_LDBS3990_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, ImpstSostLdbs3990 impstSost) {
        this.idsv0003 = idsv0003;
        this.impstSost = impstSost;
        // COB_CODE: PERFORM A000-INIZIO              THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM A200-ELABORA-WC-EFF         THRU A200-EX
                        a200ElaboraWcEff();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM B200-ELABORA-WC-CPZ       THRU B200-EX
                        b200ElaboraWcCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //               SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-WHERE-CONDITION
                //                 PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.WHERE_CONDITION:// COB_CODE: PERFORM C200-ELABORA-WC-NST     THRU C200-EX
                        c200ElaboraWcNst();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Ldbs3990 getInstance() {
        return ((Ldbs3990)Programs.getInstance(Ldbs3990.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'LDBS3990'              TO   IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("LDBS3990");
        // COB_CODE: MOVE 'IMPST-SOST' TO   IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("IMPST-SOST");
        // COB_CODE: MOVE '00'                      TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                    TO   IDSV0003-SQLCODE
        //                                               IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                    TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                               IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-WC-EFF<br>*/
    private void a200ElaboraWcEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-WC-EFF          THRU A210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-WC-EFF          THRU A210-EX
            a210SelectWcEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF     THRU A260-EX
            a260OpenCursorWcEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A270-CLOSE-CURSOR-WC-EFF    THRU A270-EX
            a270CloseCursorWcEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A280-FETCH-FIRST-WC-EFF     THRU A280-EX
            a280FetchFirstWcEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF      THRU A290-EX
            a290FetchNextWcEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B200-ELABORA-WC-CPZ<br>*/
    private void b200ElaboraWcCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B210-SELECT-WC-CPZ          THRU B210-EX
            b210SelectWcCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ     THRU B260-EX
            b260OpenCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B270-CLOSE-CURSOR-WC-CPZ    THRU B270-EX
            b270CloseCursorWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B280-FETCH-FIRST-WC-CPZ     THRU B280-EX
            b280FetchFirstWcCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ      THRU B290-EX
            b290FetchNextWcCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: C200-ELABORA-WC-NST<br>*/
    private void c200ElaboraWcNst() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM C210-SELECT-WC-NST          THRU C210-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM C210-SELECT-WC-NST          THRU C210-EX
            c210SelectWcNst();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST     THRU C260-EX
            c260OpenCursorWcNst();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM C270-CLOSE-CURSOR-WC-NST    THRU C270-EX
            c270CloseCursorWcNst();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM C280-FETCH-FIRST-WC-NST     THRU C280-EX
            c280FetchFirstWcNst();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST      THRU C290-EX
            c290FetchNextWcNst();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A205-DECLARE-CURSOR-WC-EFF<br>
	 * <pre>----
	 * ----  gestione WC Effetto
	 * ----</pre>*/
    private void a205DeclareCursorWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A210-SELECT-WC-EFF<br>*/
    private void a210SelectWcEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_IMPST_SOST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,DT_INI_PER
        //                    ,DT_END_PER
        //                    ,COD_COMP_ANIA
        //                    ,IMPST_SOST
        //                    ,IMPB_IS
        //                    ,ALQ_IS
        //                    ,COD_TRB
        //                    ,PRSTZ_LRD_ANTE_IS
        //                    ,RIS_MAT_NET_PREC
        //                    ,RIS_MAT_ANTE_TAX
        //                    ,RIS_MAT_POST_TAX
        //                    ,PRSTZ_NET
        //                    ,PRSTZ_PREC
        //                    ,CUM_PRE_VERS
        //                    ,TP_CALC_IMPST
        //                    ,IMP_GIA_TASSATO
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //             INTO
        //                :ISO-ID-IMPST-SOST
        //               ,:ISO-ID-OGG
        //                :IND-ISO-ID-OGG
        //               ,:ISO-TP-OGG
        //               ,:ISO-ID-MOVI-CRZ
        //               ,:ISO-ID-MOVI-CHIU
        //                :IND-ISO-ID-MOVI-CHIU
        //               ,:ISO-DT-INI-EFF-DB
        //               ,:ISO-DT-END-EFF-DB
        //               ,:ISO-DT-INI-PER-DB
        //                :IND-ISO-DT-INI-PER
        //               ,:ISO-DT-END-PER-DB
        //                :IND-ISO-DT-END-PER
        //               ,:ISO-COD-COMP-ANIA
        //               ,:ISO-IMPST-SOST
        //                :IND-ISO-IMPST-SOST
        //               ,:ISO-IMPB-IS
        //                :IND-ISO-IMPB-IS
        //               ,:ISO-ALQ-IS
        //                :IND-ISO-ALQ-IS
        //               ,:ISO-COD-TRB
        //                :IND-ISO-COD-TRB
        //               ,:ISO-PRSTZ-LRD-ANTE-IS
        //                :IND-ISO-PRSTZ-LRD-ANTE-IS
        //               ,:ISO-RIS-MAT-NET-PREC
        //                :IND-ISO-RIS-MAT-NET-PREC
        //               ,:ISO-RIS-MAT-ANTE-TAX
        //                :IND-ISO-RIS-MAT-ANTE-TAX
        //               ,:ISO-RIS-MAT-POST-TAX
        //                :IND-ISO-RIS-MAT-POST-TAX
        //               ,:ISO-PRSTZ-NET
        //                :IND-ISO-PRSTZ-NET
        //               ,:ISO-PRSTZ-PREC
        //                :IND-ISO-PRSTZ-PREC
        //               ,:ISO-CUM-PRE-VERS
        //                :IND-ISO-CUM-PRE-VERS
        //               ,:ISO-TP-CALC-IMPST
        //               ,:ISO-IMP-GIA-TASSATO
        //               ,:ISO-DS-RIGA
        //               ,:ISO-DS-OPER-SQL
        //               ,:ISO-DS-VER
        //               ,:ISO-DS-TS-INI-CPTZ
        //               ,:ISO-DS-TS-END-CPTZ
        //               ,:ISO-DS-UTENTE
        //               ,:ISO-DS-STATO-ELAB
        //             FROM IMPST_SOST
        //             WHERE  ID_OGG     =  :ISO-ID-OGG
        //                    AND TP_OGG =  :ISO-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY DT_END_PER DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        impstSostDao.selectRec6(impstSost.getIsoIdOgg().getIsoIdOgg(), impstSost.getIsoTpOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: A260-OPEN-CURSOR-WC-EFF<br>*/
    private void a260OpenCursorWcEff() {
        // COB_CODE: PERFORM A205-DECLARE-CURSOR-WC-EFF THRU A205-EX.
        a205DeclareCursorWcEff();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A270-CLOSE-CURSOR-WC-EFF<br>*/
    private void a270CloseCursorWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A280-FETCH-FIRST-WC-EFF<br>*/
    private void a280FetchFirstWcEff() {
        // COB_CODE: PERFORM A260-OPEN-CURSOR-WC-EFF    THRU A260-EX.
        a260OpenCursorWcEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A290-FETCH-NEXT-WC-EFF THRU A290-EX
            a290FetchNextWcEff();
        }
    }

    /**Original name: A290-FETCH-NEXT-WC-EFF<br>*/
    private void a290FetchNextWcEff() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B205-DECLARE-CURSOR-WC-CPZ<br>
	 * <pre>----
	 * ----  gestione WC Competenza
	 * ----</pre>*/
    private void b205DeclareCursorWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B210-SELECT-WC-CPZ<br>*/
    private void b210SelectWcCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                     ID_IMPST_SOST
        //                    ,ID_OGG
        //                    ,TP_OGG
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,DT_INI_PER
        //                    ,DT_END_PER
        //                    ,COD_COMP_ANIA
        //                    ,IMPST_SOST
        //                    ,IMPB_IS
        //                    ,ALQ_IS
        //                    ,COD_TRB
        //                    ,PRSTZ_LRD_ANTE_IS
        //                    ,RIS_MAT_NET_PREC
        //                    ,RIS_MAT_ANTE_TAX
        //                    ,RIS_MAT_POST_TAX
        //                    ,PRSTZ_NET
        //                    ,PRSTZ_PREC
        //                    ,CUM_PRE_VERS
        //                    ,TP_CALC_IMPST
        //                    ,IMP_GIA_TASSATO
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //             INTO
        //                :ISO-ID-IMPST-SOST
        //               ,:ISO-ID-OGG
        //                :IND-ISO-ID-OGG
        //               ,:ISO-TP-OGG
        //               ,:ISO-ID-MOVI-CRZ
        //               ,:ISO-ID-MOVI-CHIU
        //                :IND-ISO-ID-MOVI-CHIU
        //               ,:ISO-DT-INI-EFF-DB
        //               ,:ISO-DT-END-EFF-DB
        //               ,:ISO-DT-INI-PER-DB
        //                :IND-ISO-DT-INI-PER
        //               ,:ISO-DT-END-PER-DB
        //                :IND-ISO-DT-END-PER
        //               ,:ISO-COD-COMP-ANIA
        //               ,:ISO-IMPST-SOST
        //                :IND-ISO-IMPST-SOST
        //               ,:ISO-IMPB-IS
        //                :IND-ISO-IMPB-IS
        //               ,:ISO-ALQ-IS
        //                :IND-ISO-ALQ-IS
        //               ,:ISO-COD-TRB
        //                :IND-ISO-COD-TRB
        //               ,:ISO-PRSTZ-LRD-ANTE-IS
        //                :IND-ISO-PRSTZ-LRD-ANTE-IS
        //               ,:ISO-RIS-MAT-NET-PREC
        //                :IND-ISO-RIS-MAT-NET-PREC
        //               ,:ISO-RIS-MAT-ANTE-TAX
        //                :IND-ISO-RIS-MAT-ANTE-TAX
        //               ,:ISO-RIS-MAT-POST-TAX
        //                :IND-ISO-RIS-MAT-POST-TAX
        //               ,:ISO-PRSTZ-NET
        //                :IND-ISO-PRSTZ-NET
        //               ,:ISO-PRSTZ-PREC
        //                :IND-ISO-PRSTZ-PREC
        //               ,:ISO-CUM-PRE-VERS
        //                :IND-ISO-CUM-PRE-VERS
        //               ,:ISO-TP-CALC-IMPST
        //               ,:ISO-IMP-GIA-TASSATO
        //               ,:ISO-DS-RIGA
        //               ,:ISO-DS-OPER-SQL
        //               ,:ISO-DS-VER
        //               ,:ISO-DS-TS-INI-CPTZ
        //               ,:ISO-DS-TS-END-CPTZ
        //               ,:ISO-DS-UTENTE
        //               ,:ISO-DS-STATO-ELAB
        //             FROM IMPST_SOST
        //             WHERE  ID_OGG     =  :ISO-ID-OGG
        //                    AND TP_OGG =  :ISO-TP-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY DT_END_PER DESC
        //              FETCH FIRST ROW ONLY
        //           END-EXEC.
        impstSostDao.selectRec7(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
            // COB_CODE: PERFORM Z980-CODICE-ADHOC-POST THRU Z980-EX
            z980CodiceAdhocPost();
        }
    }

    /**Original name: B260-OPEN-CURSOR-WC-CPZ<br>*/
    private void b260OpenCursorWcCpz() {
        // COB_CODE: PERFORM B205-DECLARE-CURSOR-WC-CPZ THRU B205-EX.
        b205DeclareCursorWcCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B270-CLOSE-CURSOR-WC-CPZ<br>*/
    private void b270CloseCursorWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B280-FETCH-FIRST-WC-CPZ<br>*/
    private void b280FetchFirstWcCpz() {
        // COB_CODE: PERFORM B260-OPEN-CURSOR-WC-CPZ    THRU B260-EX.
        b260OpenCursorWcCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B290-FETCH-NEXT-WC-CPZ THRU B290-EX
            b290FetchNextWcCpz();
        }
    }

    /**Original name: B290-FETCH-NEXT-WC-CPZ<br>*/
    private void b290FetchNextWcCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C205-DECLARE-CURSOR-WC-NST<br>
	 * <pre>----
	 * ----  gestione WC Senza Storicità
	 * ----</pre>*/
    private void c205DeclareCursorWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C210-SELECT-WC-NST<br>*/
    private void c210SelectWcNst() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: PERFORM Z970-CODICE-ADHOC-PRE    THRU Z970-EX.
        z970CodiceAdhocPre();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C260-OPEN-CURSOR-WC-NST<br>*/
    private void c260OpenCursorWcNst() {
        // COB_CODE: PERFORM C205-DECLARE-CURSOR-WC-NST THRU C205-EX.
        c205DeclareCursorWcNst();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C270-CLOSE-CURSOR-WC-NST<br>*/
    private void c270CloseCursorWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: C280-FETCH-FIRST-WC-NST<br>*/
    private void c280FetchFirstWcNst() {
        // COB_CODE: PERFORM C260-OPEN-CURSOR-WC-NST    THRU C260-EX.
        c260OpenCursorWcNst();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM C290-FETCH-NEXT-WC-NST THRU C290-EX
            c290FetchNextWcNst();
        }
    }

    /**Original name: C290-FETCH-NEXT-WC-NST<br>*/
    private void c290FetchNextWcNst() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>
	 * <pre>----
	 * ----  utilità comuni a tutti i livelli operazione
	 * ----</pre>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-ISO-ID-OGG = -1
        //              MOVE HIGH-VALUES TO ISO-ID-OGG-NULL
        //           END-IF
        if (ws.getIndImpstSost().getIdOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-ID-OGG-NULL
            impstSost.getIsoIdOgg().setIsoIdOggNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoIdOgg.Len.ISO_ID_OGG_NULL));
        }
        // COB_CODE: IF IND-ISO-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO ISO-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndImpstSost().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-ID-MOVI-CHIU-NULL
            impstSost.getIsoIdMoviChiu().setIsoIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoIdMoviChiu.Len.ISO_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-ISO-DT-INI-PER = -1
        //              MOVE HIGH-VALUES TO ISO-DT-INI-PER-NULL
        //           END-IF
        if (ws.getIndImpstSost().getDtIniPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-DT-INI-PER-NULL
            impstSost.getIsoDtIniPer().setIsoDtIniPerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoDtIniPer.Len.ISO_DT_INI_PER_NULL));
        }
        // COB_CODE: IF IND-ISO-DT-END-PER = -1
        //              MOVE HIGH-VALUES TO ISO-DT-END-PER-NULL
        //           END-IF
        if (ws.getIndImpstSost().getDtEndPer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-DT-END-PER-NULL
            impstSost.getIsoDtEndPer().setIsoDtEndPerNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoDtEndPer.Len.ISO_DT_END_PER_NULL));
        }
        // COB_CODE: IF IND-ISO-IMPST-SOST = -1
        //              MOVE HIGH-VALUES TO ISO-IMPST-SOST-NULL
        //           END-IF
        if (ws.getIndImpstSost().getImpstSost() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-IMPST-SOST-NULL
            impstSost.getIsoImpstSost().setIsoImpstSostNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoImpstSost.Len.ISO_IMPST_SOST_NULL));
        }
        // COB_CODE: IF IND-ISO-IMPB-IS = -1
        //              MOVE HIGH-VALUES TO ISO-IMPB-IS-NULL
        //           END-IF
        if (ws.getIndImpstSost().getImpbIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-IMPB-IS-NULL
            impstSost.getIsoImpbIs().setIsoImpbIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoImpbIs.Len.ISO_IMPB_IS_NULL));
        }
        // COB_CODE: IF IND-ISO-ALQ-IS = -1
        //              MOVE HIGH-VALUES TO ISO-ALQ-IS-NULL
        //           END-IF
        if (ws.getIndImpstSost().getAlqIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-ALQ-IS-NULL
            impstSost.getIsoAlqIs().setIsoAlqIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoAlqIs.Len.ISO_ALQ_IS_NULL));
        }
        // COB_CODE: IF IND-ISO-COD-TRB = -1
        //              MOVE HIGH-VALUES TO ISO-COD-TRB-NULL
        //           END-IF
        if (ws.getIndImpstSost().getCodTrb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-COD-TRB-NULL
            impstSost.setIsoCodTrb(LiteralGenerator.create(Types.HIGH_CHAR_VAL, ImpstSostLdbs3990.Len.ISO_COD_TRB));
        }
        // COB_CODE: IF IND-ISO-PRSTZ-LRD-ANTE-IS = -1
        //              MOVE HIGH-VALUES TO ISO-PRSTZ-LRD-ANTE-IS-NULL
        //           END-IF
        if (ws.getIndImpstSost().getPrstzLrdAnteIs() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-PRSTZ-LRD-ANTE-IS-NULL
            impstSost.getIsoPrstzLrdAnteIs().setIsoPrstzLrdAnteIsNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoPrstzLrdAnteIs.Len.ISO_PRSTZ_LRD_ANTE_IS_NULL));
        }
        // COB_CODE: IF IND-ISO-RIS-MAT-NET-PREC = -1
        //              MOVE HIGH-VALUES TO ISO-RIS-MAT-NET-PREC-NULL
        //           END-IF
        if (ws.getIndImpstSost().getRisMatNetPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-RIS-MAT-NET-PREC-NULL
            impstSost.getIsoRisMatNetPrec().setIsoRisMatNetPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoRisMatNetPrec.Len.ISO_RIS_MAT_NET_PREC_NULL));
        }
        // COB_CODE: IF IND-ISO-RIS-MAT-ANTE-TAX = -1
        //              MOVE HIGH-VALUES TO ISO-RIS-MAT-ANTE-TAX-NULL
        //           END-IF
        if (ws.getIndImpstSost().getRisMatAnteTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-RIS-MAT-ANTE-TAX-NULL
            impstSost.getIsoRisMatAnteTax().setIsoRisMatAnteTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoRisMatAnteTax.Len.ISO_RIS_MAT_ANTE_TAX_NULL));
        }
        // COB_CODE: IF IND-ISO-RIS-MAT-POST-TAX = -1
        //              MOVE HIGH-VALUES TO ISO-RIS-MAT-POST-TAX-NULL
        //           END-IF
        if (ws.getIndImpstSost().getRisMatPostTax() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-RIS-MAT-POST-TAX-NULL
            impstSost.getIsoRisMatPostTax().setIsoRisMatPostTaxNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoRisMatPostTax.Len.ISO_RIS_MAT_POST_TAX_NULL));
        }
        // COB_CODE: IF IND-ISO-PRSTZ-NET = -1
        //              MOVE HIGH-VALUES TO ISO-PRSTZ-NET-NULL
        //           END-IF
        if (ws.getIndImpstSost().getPrstzNet() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-PRSTZ-NET-NULL
            impstSost.getIsoPrstzNet().setIsoPrstzNetNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoPrstzNet.Len.ISO_PRSTZ_NET_NULL));
        }
        // COB_CODE: IF IND-ISO-PRSTZ-PREC = -1
        //              MOVE HIGH-VALUES TO ISO-PRSTZ-PREC-NULL
        //           END-IF
        if (ws.getIndImpstSost().getPrstzPrec() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-PRSTZ-PREC-NULL
            impstSost.getIsoPrstzPrec().setIsoPrstzPrecNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoPrstzPrec.Len.ISO_PRSTZ_PREC_NULL));
        }
        // COB_CODE: IF IND-ISO-CUM-PRE-VERS = -1
        //              MOVE HIGH-VALUES TO ISO-CUM-PRE-VERS-NULL
        //           END-IF.
        if (ws.getIndImpstSost().getCumPreVers() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO ISO-CUM-PRE-VERS-NULL
            impstSost.getIsoCumPreVers().setIsoCumPreVersNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, IsoCumPreVers.Len.ISO_CUM_PRE_VERS_NULL));
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE ISO-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstSostDb().getEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ISO-DT-INI-EFF
        impstSost.setIsoDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE ISO-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getImpstSostDb().getRgstrzRichDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO ISO-DT-END-EFF
        impstSost.setIsoDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-ISO-DT-INI-PER = 0
        //               MOVE WS-DATE-N      TO ISO-DT-INI-PER
        //           END-IF
        if (ws.getIndImpstSost().getDtIniPer() == 0) {
            // COB_CODE: MOVE ISO-DT-INI-PER-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getImpstSostDb().getPervRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ISO-DT-INI-PER
            impstSost.getIsoDtIniPer().setIsoDtIniPer(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-ISO-DT-END-PER = 0
        //               MOVE WS-DATE-N      TO ISO-DT-END-PER
        //           END-IF.
        if (ws.getIndImpstSost().getDtEndPer() == 0) {
            // COB_CODE: MOVE ISO-DT-END-PER-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getImpstSostDb().getEsecRichDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO ISO-DT-END-PER
            impstSost.getIsoDtEndPer().setIsoDtEndPer(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z970-CODICE-ADHOC-PRE<br>
	 * <pre>----
	 * ----  prevede statements AD HOC PRE Query
	 * ----</pre>*/
    private void z970CodiceAdhocPre() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: Z980-CODICE-ADHOC-POST<br>
	 * <pre>----
	 * ----  prevede statements AD HOC POST Query
	 * ----</pre>*/
    private void z980CodiceAdhocPost() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public AfDecimal getAlqIs() {
        return impstSost.getIsoAlqIs().getIsoAlqIs();
    }

    @Override
    public void setAlqIs(AfDecimal alqIs) {
        this.impstSost.getIsoAlqIs().setIsoAlqIs(alqIs.copy());
    }

    @Override
    public AfDecimal getAlqIsObj() {
        if (ws.getIndImpstSost().getAlqIs() >= 0) {
            return getAlqIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAlqIsObj(AfDecimal alqIsObj) {
        if (alqIsObj != null) {
            setAlqIs(new AfDecimal(alqIsObj, 6, 3));
            ws.getIndImpstSost().setAlqIs(((short)0));
        }
        else {
            ws.getIndImpstSost().setAlqIs(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return impstSost.getIsoCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.impstSost.setIsoCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodTrb() {
        return impstSost.getIsoCodTrb();
    }

    @Override
    public void setCodTrb(String codTrb) {
        this.impstSost.setIsoCodTrb(codTrb);
    }

    @Override
    public String getCodTrbObj() {
        if (ws.getIndImpstSost().getCodTrb() >= 0) {
            return getCodTrb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTrbObj(String codTrbObj) {
        if (codTrbObj != null) {
            setCodTrb(codTrbObj);
            ws.getIndImpstSost().setCodTrb(((short)0));
        }
        else {
            ws.getIndImpstSost().setCodTrb(((short)-1));
        }
    }

    @Override
    public AfDecimal getCumPreVers() {
        return impstSost.getIsoCumPreVers().getIsoCumPreVers();
    }

    @Override
    public void setCumPreVers(AfDecimal cumPreVers) {
        this.impstSost.getIsoCumPreVers().setIsoCumPreVers(cumPreVers.copy());
    }

    @Override
    public AfDecimal getCumPreVersObj() {
        if (ws.getIndImpstSost().getCumPreVers() >= 0) {
            return getCumPreVers();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCumPreVersObj(AfDecimal cumPreVersObj) {
        if (cumPreVersObj != null) {
            setCumPreVers(new AfDecimal(cumPreVersObj, 15, 3));
            ws.getIndImpstSost().setCumPreVers(((short)0));
        }
        else {
            ws.getIndImpstSost().setCumPreVers(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return impstSost.getIsoDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.impstSost.setIsoDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return impstSost.getIsoDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.impstSost.setIsoDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return impstSost.getIsoDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.impstSost.setIsoDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return impstSost.getIsoDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.impstSost.setIsoDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return impstSost.getIsoDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.impstSost.setIsoDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return impstSost.getIsoDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.impstSost.setIsoDsVer(dsVer);
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getImpstSostDb().getRgstrzRichDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getImpstSostDb().setRgstrzRichDb(dtEndEffDb);
    }

    @Override
    public String getDtEndPerDb() {
        return ws.getImpstSostDb().getEsecRichDb();
    }

    @Override
    public void setDtEndPerDb(String dtEndPerDb) {
        this.ws.getImpstSostDb().setEsecRichDb(dtEndPerDb);
    }

    @Override
    public String getDtEndPerDbObj() {
        if (ws.getIndImpstSost().getDtEndPer() >= 0) {
            return getDtEndPerDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndPerDbObj(String dtEndPerDbObj) {
        if (dtEndPerDbObj != null) {
            setDtEndPerDb(dtEndPerDbObj);
            ws.getIndImpstSost().setDtEndPer(((short)0));
        }
        else {
            ws.getIndImpstSost().setDtEndPer(((short)-1));
        }
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getImpstSostDb().getEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getImpstSostDb().setEffDb(dtIniEffDb);
    }

    @Override
    public String getDtIniPerDb() {
        return ws.getImpstSostDb().getPervRichDb();
    }

    @Override
    public void setDtIniPerDb(String dtIniPerDb) {
        this.ws.getImpstSostDb().setPervRichDb(dtIniPerDb);
    }

    @Override
    public String getDtIniPerDbObj() {
        if (ws.getIndImpstSost().getDtIniPer() >= 0) {
            return getDtIniPerDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniPerDbObj(String dtIniPerDbObj) {
        if (dtIniPerDbObj != null) {
            setDtIniPerDb(dtIniPerDbObj);
            ws.getIndImpstSost().setDtIniPer(((short)0));
        }
        else {
            ws.getIndImpstSost().setDtIniPer(((short)-1));
        }
    }

    @Override
    public int getIdImpstSost() {
        return impstSost.getIsoIdImpstSost();
    }

    @Override
    public void setIdImpstSost(int idImpstSost) {
        this.impstSost.setIsoIdImpstSost(idImpstSost);
    }

    @Override
    public int getIdMoviChiu() {
        return impstSost.getIsoIdMoviChiu().getIsoIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.impstSost.getIsoIdMoviChiu().setIsoIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndImpstSost().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndImpstSost().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndImpstSost().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return impstSost.getIsoIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.impstSost.setIsoIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public AfDecimal getImpGiaTassato() {
        return impstSost.getIsoImpGiaTassato();
    }

    @Override
    public void setImpGiaTassato(AfDecimal impGiaTassato) {
        this.impstSost.setIsoImpGiaTassato(impGiaTassato.copy());
    }

    @Override
    public AfDecimal getImpbIs() {
        return impstSost.getIsoImpbIs().getIsoImpbIs();
    }

    @Override
    public void setImpbIs(AfDecimal impbIs) {
        this.impstSost.getIsoImpbIs().setIsoImpbIs(impbIs.copy());
    }

    @Override
    public AfDecimal getImpbIsObj() {
        if (ws.getIndImpstSost().getImpbIs() >= 0) {
            return getImpbIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpbIsObj(AfDecimal impbIsObj) {
        if (impbIsObj != null) {
            setImpbIs(new AfDecimal(impbIsObj, 15, 3));
            ws.getIndImpstSost().setImpbIs(((short)0));
        }
        else {
            ws.getIndImpstSost().setImpbIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getImpstSost() {
        return impstSost.getIsoImpstSost().getIsoImpstSost();
    }

    @Override
    public void setImpstSost(AfDecimal impstSost) {
        this.impstSost.getIsoImpstSost().setIsoImpstSost(impstSost.copy());
    }

    @Override
    public AfDecimal getImpstSostObj() {
        if (ws.getIndImpstSost().getImpstSost() >= 0) {
            return getImpstSost();
        }
        else {
            return null;
        }
    }

    @Override
    public void setImpstSostObj(AfDecimal impstSostObj) {
        if (impstSostObj != null) {
            setImpstSost(new AfDecimal(impstSostObj, 15, 3));
            ws.getIndImpstSost().setImpstSost(((short)0));
        }
        else {
            ws.getIndImpstSost().setImpstSost(((short)-1));
        }
    }

    @Override
    public long getIsoDsRiga() {
        return impstSost.getIsoDsRiga();
    }

    @Override
    public void setIsoDsRiga(long isoDsRiga) {
        this.impstSost.setIsoDsRiga(isoDsRiga);
    }

    @Override
    public int getIsoIdOgg() {
        return impstSost.getIsoIdOgg().getIsoIdOgg();
    }

    @Override
    public void setIsoIdOgg(int isoIdOgg) {
        this.impstSost.getIsoIdOgg().setIsoIdOgg(isoIdOgg);
    }

    @Override
    public Integer getIsoIdOggObj() {
        if (ws.getIndImpstSost().getIdOgg() >= 0) {
            return ((Integer)getIsoIdOgg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIsoIdOggObj(Integer isoIdOggObj) {
        if (isoIdOggObj != null) {
            setIsoIdOgg(((int)isoIdOggObj));
            ws.getIndImpstSost().setIdOgg(((short)0));
        }
        else {
            ws.getIndImpstSost().setIdOgg(((short)-1));
        }
    }

    @Override
    public String getIsoTpOgg() {
        return impstSost.getIsoTpOgg();
    }

    @Override
    public void setIsoTpOgg(String isoTpOgg) {
        this.impstSost.setIsoTpOgg(isoTpOgg);
    }

    @Override
    public int getLdbv1591IdAdes() {
        throw new FieldNotMappedException("ldbv1591IdAdes");
    }

    @Override
    public void setLdbv1591IdAdes(int ldbv1591IdAdes) {
        throw new FieldNotMappedException("ldbv1591IdAdes");
    }

    @Override
    public int getLdbv2901IdAdes() {
        throw new FieldNotMappedException("ldbv2901IdAdes");
    }

    @Override
    public void setLdbv2901IdAdes(int ldbv2901IdAdes) {
        throw new FieldNotMappedException("ldbv2901IdAdes");
    }

    @Override
    public String getLdbv6191DtInfDb() {
        throw new FieldNotMappedException("ldbv6191DtInfDb");
    }

    @Override
    public void setLdbv6191DtInfDb(String ldbv6191DtInfDb) {
        throw new FieldNotMappedException("ldbv6191DtInfDb");
    }

    @Override
    public String getLdbv6191DtSupDb() {
        throw new FieldNotMappedException("ldbv6191DtSupDb");
    }

    @Override
    public void setLdbv6191DtSupDb(String ldbv6191DtSupDb) {
        throw new FieldNotMappedException("ldbv6191DtSupDb");
    }

    @Override
    public AfDecimal getMpbIs() {
        throw new FieldNotMappedException("mpbIs");
    }

    @Override
    public void setMpbIs(AfDecimal mpbIs) {
        throw new FieldNotMappedException("mpbIs");
    }

    @Override
    public AfDecimal getPrstzLrdAnteIs() {
        return impstSost.getIsoPrstzLrdAnteIs().getIsoPrstzLrdAnteIs();
    }

    @Override
    public void setPrstzLrdAnteIs(AfDecimal prstzLrdAnteIs) {
        this.impstSost.getIsoPrstzLrdAnteIs().setIsoPrstzLrdAnteIs(prstzLrdAnteIs.copy());
    }

    @Override
    public AfDecimal getPrstzLrdAnteIsObj() {
        if (ws.getIndImpstSost().getPrstzLrdAnteIs() >= 0) {
            return getPrstzLrdAnteIs();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzLrdAnteIsObj(AfDecimal prstzLrdAnteIsObj) {
        if (prstzLrdAnteIsObj != null) {
            setPrstzLrdAnteIs(new AfDecimal(prstzLrdAnteIsObj, 15, 3));
            ws.getIndImpstSost().setPrstzLrdAnteIs(((short)0));
        }
        else {
            ws.getIndImpstSost().setPrstzLrdAnteIs(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzNet() {
        return impstSost.getIsoPrstzNet().getIsoPrstzNet();
    }

    @Override
    public void setPrstzNet(AfDecimal prstzNet) {
        this.impstSost.getIsoPrstzNet().setIsoPrstzNet(prstzNet.copy());
    }

    @Override
    public AfDecimal getPrstzNetObj() {
        if (ws.getIndImpstSost().getPrstzNet() >= 0) {
            return getPrstzNet();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzNetObj(AfDecimal prstzNetObj) {
        if (prstzNetObj != null) {
            setPrstzNet(new AfDecimal(prstzNetObj, 15, 3));
            ws.getIndImpstSost().setPrstzNet(((short)0));
        }
        else {
            ws.getIndImpstSost().setPrstzNet(((short)-1));
        }
    }

    @Override
    public AfDecimal getPrstzPrec() {
        return impstSost.getIsoPrstzPrec().getIsoPrstzPrec();
    }

    @Override
    public void setPrstzPrec(AfDecimal prstzPrec) {
        this.impstSost.getIsoPrstzPrec().setIsoPrstzPrec(prstzPrec.copy());
    }

    @Override
    public AfDecimal getPrstzPrecObj() {
        if (ws.getIndImpstSost().getPrstzPrec() >= 0) {
            return getPrstzPrec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPrstzPrecObj(AfDecimal prstzPrecObj) {
        if (prstzPrecObj != null) {
            setPrstzPrec(new AfDecimal(prstzPrecObj, 15, 3));
            ws.getIndImpstSost().setPrstzPrec(((short)0));
        }
        else {
            ws.getIndImpstSost().setPrstzPrec(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMatAnteTax() {
        return impstSost.getIsoRisMatAnteTax().getIsoRisMatAnteTax();
    }

    @Override
    public void setRisMatAnteTax(AfDecimal risMatAnteTax) {
        this.impstSost.getIsoRisMatAnteTax().setIsoRisMatAnteTax(risMatAnteTax.copy());
    }

    @Override
    public AfDecimal getRisMatAnteTaxObj() {
        if (ws.getIndImpstSost().getRisMatAnteTax() >= 0) {
            return getRisMatAnteTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatAnteTaxObj(AfDecimal risMatAnteTaxObj) {
        if (risMatAnteTaxObj != null) {
            setRisMatAnteTax(new AfDecimal(risMatAnteTaxObj, 15, 3));
            ws.getIndImpstSost().setRisMatAnteTax(((short)0));
        }
        else {
            ws.getIndImpstSost().setRisMatAnteTax(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMatNetPrec() {
        return impstSost.getIsoRisMatNetPrec().getIsoRisMatNetPrec();
    }

    @Override
    public void setRisMatNetPrec(AfDecimal risMatNetPrec) {
        this.impstSost.getIsoRisMatNetPrec().setIsoRisMatNetPrec(risMatNetPrec.copy());
    }

    @Override
    public AfDecimal getRisMatNetPrecObj() {
        if (ws.getIndImpstSost().getRisMatNetPrec() >= 0) {
            return getRisMatNetPrec();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatNetPrecObj(AfDecimal risMatNetPrecObj) {
        if (risMatNetPrecObj != null) {
            setRisMatNetPrec(new AfDecimal(risMatNetPrecObj, 15, 3));
            ws.getIndImpstSost().setRisMatNetPrec(((short)0));
        }
        else {
            ws.getIndImpstSost().setRisMatNetPrec(((short)-1));
        }
    }

    @Override
    public AfDecimal getRisMatPostTax() {
        return impstSost.getIsoRisMatPostTax().getIsoRisMatPostTax();
    }

    @Override
    public void setRisMatPostTax(AfDecimal risMatPostTax) {
        this.impstSost.getIsoRisMatPostTax().setIsoRisMatPostTax(risMatPostTax.copy());
    }

    @Override
    public AfDecimal getRisMatPostTaxObj() {
        if (ws.getIndImpstSost().getRisMatPostTax() >= 0) {
            return getRisMatPostTax();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRisMatPostTaxObj(AfDecimal risMatPostTaxObj) {
        if (risMatPostTaxObj != null) {
            setRisMatPostTax(new AfDecimal(risMatPostTaxObj, 15, 3));
            ws.getIndImpstSost().setRisMatPostTax(((short)0));
        }
        else {
            ws.getIndImpstSost().setRisMatPostTax(((short)-1));
        }
    }

    @Override
    public String getTpCalcImpst() {
        return impstSost.getIsoTpCalcImpst();
    }

    @Override
    public void setTpCalcImpst(String tpCalcImpst) {
        this.impstSost.setIsoTpCalcImpst(tpCalcImpst);
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public String getWsDtInfinito1() {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public void setWsDtInfinito1(String wsDtInfinito1) {
        throw new FieldNotMappedException("wsDtInfinito1");
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }

    @Override
    public long getWsTsInfinito1() {
        throw new FieldNotMappedException("wsTsInfinito1");
    }

    @Override
    public void setWsTsInfinito1(long wsTsInfinito1) {
        throw new FieldNotMappedException("wsTsInfinito1");
    }
}
