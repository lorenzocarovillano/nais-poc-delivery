package it.accenture.jnais;

import com.bphx.ctu.af.core.DbAccessStatus;
import com.bphx.ctu.af.core.LiteralGenerator;
import com.bphx.ctu.af.core.Types;
import com.bphx.ctu.af.lang.types.AfDecimal;
import com.bphx.ctu.af.util.Conditions;
import com.bphx.ctu.af.util.Functions;
import com.bphx.ctu.af.util.TruncAbs;
import com.modernsystems.ctu.core.impl.Program;
import com.modernsystems.ctu.utils.Characters;
import com.modernsystems.programs.Programs;
import it.accenture.jnais.commons.data.dao.GarDao;
import it.accenture.jnais.commons.data.to.IGar;
import it.accenture.jnais.copy.Sqlca;
import it.accenture.jnais.ws.enums.Idsv0003LivelloOperazione;
import it.accenture.jnais.ws.Gar;
import it.accenture.jnais.ws.Idbsgrz0Data;
import it.accenture.jnais.ws.Idsv0003;
import it.accenture.jnais.ws.redefines.GrzAaPagPreUni;
import it.accenture.jnais.ws.redefines.GrzAaStab;
import it.accenture.jnais.ws.redefines.GrzDtDecor;
import it.accenture.jnais.ws.redefines.GrzDtEndCarz;
import it.accenture.jnais.ws.redefines.GrzDtIniValTar;
import it.accenture.jnais.ws.redefines.GrzDtPresc;
import it.accenture.jnais.ws.redefines.GrzDtScad;
import it.accenture.jnais.ws.redefines.GrzDtVarzTpIas;
import it.accenture.jnais.ws.redefines.GrzDurAa;
import it.accenture.jnais.ws.redefines.GrzDurGg;
import it.accenture.jnais.ws.redefines.GrzDurMm;
import it.accenture.jnais.ws.redefines.GrzEtaAa1oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaAa2oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaAa3oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaAScad;
import it.accenture.jnais.ws.redefines.GrzEtaMm1oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaMm2oAssto;
import it.accenture.jnais.ws.redefines.GrzEtaMm3oAssto;
import it.accenture.jnais.ws.redefines.GrzFrazDecrCpt;
import it.accenture.jnais.ws.redefines.GrzFrazIniErogRen;
import it.accenture.jnais.ws.redefines.GrzId1oAssto;
import it.accenture.jnais.ws.redefines.GrzId2oAssto;
import it.accenture.jnais.ws.redefines.GrzId3oAssto;
import it.accenture.jnais.ws.redefines.GrzIdAdes;
import it.accenture.jnais.ws.redefines.GrzIdMoviChiu;
import it.accenture.jnais.ws.redefines.GrzMm1oRat;
import it.accenture.jnais.ws.redefines.GrzMmPagPreUni;
import it.accenture.jnais.ws.redefines.GrzNumAaPagPre;
import it.accenture.jnais.ws.redefines.GrzPc1oRat;
import it.accenture.jnais.ws.redefines.GrzPcOpz;
import it.accenture.jnais.ws.redefines.GrzPcRevrsb;
import it.accenture.jnais.ws.redefines.GrzPcRipPre;
import it.accenture.jnais.ws.redefines.GrzTpGar;
import it.accenture.jnais.ws.redefines.GrzTpInvst;
import it.accenture.jnais.ws.redefines.GrzTsStabLimitata;

/**Original name: IDBSGRZ0<br>
 * <pre>AUTHOR.        AISS.
 * DATE-WRITTEN.  31 OTT 2013.
 * DATE-COMPILED.
 * ---------------------------------------------------------------*
 *  P R O G E T T O : NEWLIFE                                     *
 *  F A S E         : MODULO STANDARD PER ACCESSO RISORSE DB      *
 * ---------------------------------------------------------------*
 *                                                                *
 *                                                                *
 * ---------------------------------------------------------------*</pre>*/
public class Idbsgrz0 extends Program implements IGar {

    //==== PROPERTIES ====
    //Original name: SQLCA
    private Sqlca sqlca = new Sqlca();
    private DbAccessStatus dbAccessStatus = new DbAccessStatus(sqlca);
    private GarDao garDao = new GarDao(dbAccessStatus);
    //Original name: WORKING-STORAGE
    private Idbsgrz0Data ws = new Idbsgrz0Data();
    //Original name: IDSV0003
    private Idsv0003 idsv0003;
    //Original name: GAR
    private Gar gar;

    //==== METHODS ====
    /**Original name: PROGRAM_IDBSGRZ0_FIRST_SENTENCES<br>*/
    public long execute(Idsv0003 idsv0003, Gar gar) {
        this.idsv0003 = idsv0003;
        this.gar = gar;
        // COB_CODE: PERFORM A000-INIZIO                    THRU A000-EX.
        a000Inizio();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (this.idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-TRATT-X-EFFETTO
            //              END-EVALUATE
            //           ELSE
            //              END-IF
            //           END-IF
            if (this.idsv0003.getTrattamentoStoricita().isTrattXEffetto()) {
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO          THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS          THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO          THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM A300-ELABORA-ID-EFF       THRU A300-EX
                        a300ElaboraIdEff();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM A400-ELABORA-IDP-EFF      THRU A400-EX
                        a400ElaboraIdpEff();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO          THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS          THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO          THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattXCompetenza()) {
                // COB_CODE: IF IDSV0003-TRATT-X-COMPETENZA
                //              END-EVALUATE
                //           ELSE
                //              END-IF
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-ID
                //                 PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                //              WHEN IDSV0003-ID-PADRE
                //                 PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.ID:// COB_CODE: PERFORM B300-ELABORA-ID-CPZ       THRU B300-EX
                        b300ElaboraIdCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_PADRE:// COB_CODE: PERFORM B400-ELABORA-IDP-CPZ      THRU B400-EX
                        b400ElaboraIdpCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM B500-ELABORA-IBO-CPZ      THRU B500-EX
                        b500ElaboraIboCpz();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM B600-ELABORA-IBS-CPZ      THRU B600-EX
                        b600ElaboraIbsCpz();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM B700-ELABORA-IDO-CPZ      THRU B700-EX
                        b700ElaboraIdoCpz();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else if (this.idsv0003.getTrattamentoStoricita().isTrattSenzaStor()) {
                // COB_CODE: IF IDSV0003-TRATT-SENZA-STOR
                //              END-EVALUATE
                //           ELSE
                //              SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //            END-IF
                // COB_CODE: EVALUATE TRUE
                //              WHEN IDSV0003-PRIMARY-KEY
                //                 PERFORM A200-ELABORA-PK          THRU A200-EX
                //              WHEN IDSV0003-IB-OGGETTO
                //                 PERFORM A500-ELABORA-IBO         THRU A500-EX
                //              WHEN IDSV0003-IB-SECONDARIO
                //                 PERFORM A600-ELABORA-IBS         THRU A600-EX
                //              WHEN IDSV0003-ID-OGGETTO
                //                 PERFORM A700-ELABORA-IDO         THRU A700-EX
                //              WHEN OTHER
                //                 SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                //           END-EVALUATE
                switch (this.idsv0003.getLivelloOperazione().getLivelloOperazione()) {

                    case Idsv0003LivelloOperazione.PRIMARY_KEY:// COB_CODE: PERFORM A200-ELABORA-PK          THRU A200-EX
                        a200ElaboraPk();
                        break;

                    case Idsv0003LivelloOperazione.IB_OGGETTO:// COB_CODE: PERFORM A500-ELABORA-IBO         THRU A500-EX
                        a500ElaboraIbo();
                        break;

                    case Idsv0003LivelloOperazione.IB_SECONDARIO:// COB_CODE: PERFORM A600-ELABORA-IBS         THRU A600-EX
                        a600ElaboraIbs();
                        break;

                    case Idsv0003LivelloOperazione.ID_OGGETTO:// COB_CODE: PERFORM A700-ELABORA-IDO         THRU A700-EX
                        a700ElaboraIdo();
                        break;

                    default:// COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                        this.idsv0003.getReturnCode().setInvalidLevelOper();
                        break;
                }
            }
            else {
                // COB_CODE: SET IDSV0003-INVALID-LEVEL-OPER TO TRUE
                this.idsv0003.getReturnCode().setInvalidLevelOper();
            }
        }
        // COB_CODE: GOBACK.
        //last return statement was skipped
        return 0;
    }

    public static Idbsgrz0 getInstance() {
        return ((Idbsgrz0)Programs.getInstance(Idbsgrz0.class));
    }

    /**Original name: A000-INIZIO<br>*/
    private void a000Inizio() {
        // COB_CODE: MOVE 'IDBSGRZ0'   TO IDSV0003-COD-SERVIZIO-BE.
        idsv0003.getCampiEsito().setCodServizioBe("IDBSGRZ0");
        // COB_CODE: MOVE 'GAR' TO IDSV0003-NOME-TABELLA.
        idsv0003.getCampiEsito().setNomeTabella("GAR");
        // COB_CODE: MOVE '00'                     TO   IDSV0003-RETURN-CODE.
        idsv0003.getReturnCode().setReturnCode("00");
        // COB_CODE: MOVE ZEROES                   TO   IDSV0003-SQLCODE
        //                                              IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getSqlcode().setSqlcode(0);
        idsv0003.getCampiEsito().setNumRigheLette(((short)0));
        // COB_CODE: MOVE SPACES                   TO   IDSV0003-DESCRIZ-ERR-DB2
        //                                              IDSV0003-KEY-TABELLA.
        idsv0003.getCampiEsito().setDescrizErrDb2("");
        idsv0003.getCampiEsito().setKeyTabella("");
        // COB_CODE: PERFORM A001-TRATTA-DATE-TIMESTAMP THRU A001-EX.
        a001TrattaDateTimestamp();
    }

    /**Original name: A100-CHECK-RETURN-CODE<br>*/
    private void a100CheckReturnCode() {
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-EVALUATE
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: MOVE SQLCODE               TO   IDSV0003-SQLCODE
            idsv0003.getSqlcode().setSqlcode(sqlca.getSqlcode());
            // COB_CODE: MOVE DESCRIZ-ERR-DB2       TO   IDSV0003-DESCRIZ-ERR-DB2
            idsv0003.getCampiEsito().setDescrizErrDb2(ws.getDescrizErrDb2());
            // COB_CODE: EVALUATE IDSV0003-SQLCODE
            //               WHEN ZERO
            //                             CONTINUE
            //               WHEN +100
            //                  END-IF
            //               WHEN OTHER
            //                             SET IDSV0003-SQL-ERROR TO TRUE
            //           END-EVALUATE
            if (idsv0003.getSqlcode().getSqlcode() == 0) {
            // COB_CODE: CONTINUE
            //continue
            }
            else if (idsv0003.getSqlcode().getSqlcode() == 100) {
                // COB_CODE: IF IDSV0003-AGGIORNAMENTO-STORICO OR
                //              IDSV0003-AGG-STORICO-SOLO-INS  OR
                //              IDSV0003-DELETE-LOGICA         OR
                //              IDSV0003-SELECT                OR
                //              IDSV0003-FETCH-FIRST           OR
                //              IDSV0003-FETCH-NEXT            OR
                //              IDSV0003-FETCH-FIRST-MULTIPLE  OR
                //              IDSV0003-FETCH-NEXT-MULTIPLE
                //                      CONTINUE
                //           ELSE
                //                      SET IDSV0003-SQL-ERROR TO TRUE
                //           END-IF
                if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isAggStoricoSoloIns() || idsv0003.getOperazione().isDeleteLogica() || idsv0003.getOperazione().isSelect() || idsv0003.getOperazione().isFetchFirst() || idsv0003.getOperazione().isFetchNext() || idsv0003.getOperazione().isFetchFirstMultiple() || idsv0003.getOperazione().isFetchNextMultiple()) {
                // COB_CODE: CONTINUE
                //continue
                }
                else {
                    // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                    idsv0003.getReturnCode().setSqlError();
                }
            }
            else {
                // COB_CODE: SET IDSV0003-SQL-ERROR TO TRUE
                idsv0003.getReturnCode().setSqlError();
            }
        }
    }

    /**Original name: A200-ELABORA-PK<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a200ElaboraPk() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A210-SELECT-PK          THRU A210-EX
        //              WHEN IDSV0003-INSERT
        //                 PERFORM A220-INSERT-PK          THRU A220-EX
        //              WHEN IDSV0003-UPDATE
        //                 PERFORM A230-UPDATE-PK          THRU A230-EX
        //              WHEN IDSV0003-DELETE
        //                 PERFORM A240-DELETE-PK          THRU A240-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A210-SELECT-PK          THRU A210-EX
            a210SelectPk();
        }
        else if (idsv0003.getOperazione().isInsert()) {
            // COB_CODE: PERFORM A220-INSERT-PK          THRU A220-EX
            a220InsertPk();
        }
        else if (idsv0003.getOperazione().isUpdate()) {
            // COB_CODE: PERFORM A230-UPDATE-PK          THRU A230-EX
            a230UpdatePk();
        }
        else if (idsv0003.getOperazione().isDelete()) {
            // COB_CODE: PERFORM A240-DELETE-PK          THRU A240-EX
            a240DeletePk();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A300-ELABORA-ID-EFF<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void a300ElaboraIdEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A310-SELECT-ID-EFF          THRU A310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A310-SELECT-ID-EFF          THRU A310-EX
            a310SelectIdEff();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A400-ELABORA-IDP-EFF<br>*/
    private void a400ElaboraIdpEff() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A410-SELECT-IDP-EFF       THRU A410-EX
            a410SelectIdpEff();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF  THRU A460-EX
            a460OpenCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A480-FETCH-FIRST-IDP-EFF  THRU A480-EX
            a480FetchFirstIdpEff();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF   THRU A490-EX
            a490FetchNextIdpEff();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A500-ELABORA-IBO<br>
	 * <pre>----
	 * ----  Gestione prevista per tabelle Storiche e non
	 * ----</pre>*/
    private void a500ElaboraIbo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A510-SELECT-IBO             THRU A510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A510-SELECT-IBO             THRU A510-EX
            a510SelectIbo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX
            a560OpenCursorIbo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO       THRU A570-EX
            a570CloseCursorIbo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A580-FETCH-FIRST-IBO        THRU A580-EX
            a580FetchFirstIbo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO         THRU A590-EX
            a590FetchNextIbo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A600-ELABORA-IBS<br>*/
    private void a600ElaboraIbs() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A610-SELECT-IBS             THRU A610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A610-SELECT-IBS             THRU A610-EX
            a610SelectIbs();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX
            a660OpenCursorIbs();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A670-CLOSE-CURSOR-IBS       THRU A670-EX
            a670CloseCursorIbs();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A680-FETCH-FIRST-IBS        THRU A680-EX
            a680FetchFirstIbs();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS         THRU A690-EX
            a690FetchNextIbs();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A700-ELABORA-IDO<br>*/
    private void a700ElaboraIdo() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM A710-SELECT-IDO                 THRU A710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM A710-SELECT-IDO                 THRU A710-EX
            a710SelectIdo();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO            THRU A760-EX
            a760OpenCursorIdo();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM A770-CLOSE-CURSOR-IDO           THRU A770-EX
            a770CloseCursorIdo();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM A780-FETCH-FIRST-IDO            THRU A780-EX
            a780FetchFirstIdo();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO             THRU A790-EX
            a790FetchNextIdo();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B300-ELABORA-ID-CPZ<br>
	 * <pre>----
	 * ----  Gestione prevista solo per tabelle Storiche
	 * ----</pre>*/
    private void b300ElaboraIdCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
        //              WHEN IDSV0003-AGGIORNAMENTO-STORICO
        //                   OR IDSV0003-DELETE-LOGICA
        //                 PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
        //              WHEN IDSV0003-AGG-STORICO-SOLO-INS
        //                 PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B310-SELECT-ID-CPZ          THRU B310-EX
            b310SelectIdCpz();
        }
        else if (idsv0003.getOperazione().isAggiornamentoStorico() || idsv0003.getOperazione().isDeleteLogica()) {
            // COB_CODE: PERFORM Z500-AGGIORNAMENTO-STORICO  THRU Z500-EX
            z500AggiornamentoStorico();
        }
        else if (idsv0003.getOperazione().isAggStoricoSoloIns()) {
            // COB_CODE: PERFORM Z550-AGG-STORICO-SOLO-INS   THRU Z550-EX
            z550AggStoricoSoloIns();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B400-ELABORA-IDP-CPZ<br>*/
    private void b400ElaboraIdpCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B410-SELECT-IDP-CPZ       THRU B410-EX
            b410SelectIdpCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ  THRU B460-EX
            b460OpenCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B480-FETCH-FIRST-IDP-CPZ  THRU B480-EX
            b480FetchFirstIdpCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ   THRU B490-EX
            b490FetchNextIdpCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B500-ELABORA-IBO-CPZ<br>*/
    private void b500ElaboraIboCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B510-SELECT-IBO-CPZ         THRU B510-EX
            b510SelectIboCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ    THRU B560-EX
            b560OpenCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ   THRU B570-EX
            b570CloseCursorIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B580-FETCH-FIRST-IBO-CPZ    THRU B580-EX
            b580FetchFirstIboCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B600-ELABORA-IBS-CPZ<br>*/
    private void b600ElaboraIbsCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B610-SELECT-IBS-CPZ         THRU B610-EX
            b610SelectIbsCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ    THRU B660-EX
            b660OpenCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B670-CLOSE-CURSOR-IBS-CPZ   THRU B670-EX
            b670CloseCursorIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B680-FETCH-FIRST-IBS-CPZ    THRU B680-EX
            b680FetchFirstIbsCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: B700-ELABORA-IDO-CPZ<br>*/
    private void b700ElaboraIdoCpz() {
        // COB_CODE: EVALUATE TRUE
        //              WHEN IDSV0003-SELECT
        //                 PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
        //              WHEN IDSV0003-OPEN-CURSOR
        //                 PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
        //              WHEN IDSV0003-CLOSE-CURSOR
        //                 PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
        //              WHEN IDSV0003-FETCH-FIRST
        //                 PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
        //              WHEN IDSV0003-FETCH-NEXT
        //                 PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //              WHEN OTHER
        //                 SET IDSV0003-INVALID-OPER TO TRUE
        //           END-EVALUATE.
        if (idsv0003.getOperazione().isSelect()) {
            // COB_CODE: PERFORM B710-SELECT-IDO-CPZ         THRU B710-EX
            b710SelectIdoCpz();
        }
        else if (idsv0003.getOperazione().isOpenCursor()) {
            // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ    THRU B760-EX
            b760OpenCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isCloseCursor()) {
            // COB_CODE: PERFORM B770-CLOSE-CURSOR-IDO-CPZ   THRU B770-EX
            b770CloseCursorIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchFirst()) {
            // COB_CODE: PERFORM B780-FETCH-FIRST-IDO-CPZ    THRU B780-EX
            b780FetchFirstIdoCpz();
        }
        else if (idsv0003.getOperazione().isFetchNext()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
        else {
            // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE
            idsv0003.getReturnCode().setInvalidOper();
        }
    }

    /**Original name: A210-SELECT-PK<br>
	 * <pre>----
	 * ----  gestione PK
	 * ----</pre>*/
    private void a210SelectPk() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_GAR
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,COD_SEZ
        //                ,COD_TARI
        //                ,RAMO_BILA
        //                ,DT_INI_VAL_TAR
        //                ,ID_1O_ASSTO
        //                ,ID_2O_ASSTO
        //                ,ID_3O_ASSTO
        //                ,TP_GAR
        //                ,TP_RSH
        //                ,TP_INVST
        //                ,MOD_PAG_GARCOL
        //                ,TP_PER_PRE
        //                ,ETA_AA_1O_ASSTO
        //                ,ETA_MM_1O_ASSTO
        //                ,ETA_AA_2O_ASSTO
        //                ,ETA_MM_2O_ASSTO
        //                ,ETA_AA_3O_ASSTO
        //                ,ETA_MM_3O_ASSTO
        //                ,TP_EMIS_PUR
        //                ,ETA_A_SCAD
        //                ,TP_CALC_PRE_PRSTZ
        //                ,TP_PRE
        //                ,TP_DUR
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,NUM_AA_PAG_PRE
        //                ,AA_PAG_PRE_UNI
        //                ,MM_PAG_PRE_UNI
        //                ,FRAZ_INI_EROG_REN
        //                ,MM_1O_RAT
        //                ,PC_1O_RAT
        //                ,TP_PRSTZ_ASSTA
        //                ,DT_END_CARZ
        //                ,PC_RIP_PRE
        //                ,COD_FND
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,TP_PC_RIP
        //                ,PC_OPZ
        //                ,TP_IAS
        //                ,TP_STAB
        //                ,TP_ADEG_PRE
        //                ,DT_VARZ_TP_IAS
        //                ,FRAZ_DECR_CPT
        //                ,COD_TRAT_RIASS
        //                ,TP_DT_EMIS_RIASS
        //                ,TP_CESS_RIASS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,AA_STAB
        //                ,TS_STAB_LIMITATA
        //                ,DT_PRESC
        //                ,RSH_INVST
        //                ,TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //             FROM GAR
        //             WHERE     DS_RIGA = :GRZ-DS-RIGA
        //           END-EXEC.
        garDao.selectByGrzDsRiga(gar.getGrzDsRiga(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE   THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A220-INSERT-PK<br>*/
    private void a220InsertPk() {
        // COB_CODE: PERFORM Z400-SEQ-RIGA                     THRU Z400-EX.
        z400SeqRiga();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z150-VALORIZZA-DATA-SERVICES-I THRU Z150-EX
            z150ValorizzaDataServicesI();
            // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX
            z200SetIndicatoriNull();
            // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX
            z900ConvertiNToX();
            // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX
            z960LengthVchar();
            // COB_CODE: EXEC SQL
            //              INSERT
            //              INTO GAR
            //                  (
            //                     ID_GAR
            //                    ,ID_ADES
            //                    ,ID_POLI
            //                    ,ID_MOVI_CRZ
            //                    ,ID_MOVI_CHIU
            //                    ,DT_INI_EFF
            //                    ,DT_END_EFF
            //                    ,COD_COMP_ANIA
            //                    ,IB_OGG
            //                    ,DT_DECOR
            //                    ,DT_SCAD
            //                    ,COD_SEZ
            //                    ,COD_TARI
            //                    ,RAMO_BILA
            //                    ,DT_INI_VAL_TAR
            //                    ,ID_1O_ASSTO
            //                    ,ID_2O_ASSTO
            //                    ,ID_3O_ASSTO
            //                    ,TP_GAR
            //                    ,TP_RSH
            //                    ,TP_INVST
            //                    ,MOD_PAG_GARCOL
            //                    ,TP_PER_PRE
            //                    ,ETA_AA_1O_ASSTO
            //                    ,ETA_MM_1O_ASSTO
            //                    ,ETA_AA_2O_ASSTO
            //                    ,ETA_MM_2O_ASSTO
            //                    ,ETA_AA_3O_ASSTO
            //                    ,ETA_MM_3O_ASSTO
            //                    ,TP_EMIS_PUR
            //                    ,ETA_A_SCAD
            //                    ,TP_CALC_PRE_PRSTZ
            //                    ,TP_PRE
            //                    ,TP_DUR
            //                    ,DUR_AA
            //                    ,DUR_MM
            //                    ,DUR_GG
            //                    ,NUM_AA_PAG_PRE
            //                    ,AA_PAG_PRE_UNI
            //                    ,MM_PAG_PRE_UNI
            //                    ,FRAZ_INI_EROG_REN
            //                    ,MM_1O_RAT
            //                    ,PC_1O_RAT
            //                    ,TP_PRSTZ_ASSTA
            //                    ,DT_END_CARZ
            //                    ,PC_RIP_PRE
            //                    ,COD_FND
            //                    ,AA_REN_CER
            //                    ,PC_REVRSB
            //                    ,TP_PC_RIP
            //                    ,PC_OPZ
            //                    ,TP_IAS
            //                    ,TP_STAB
            //                    ,TP_ADEG_PRE
            //                    ,DT_VARZ_TP_IAS
            //                    ,FRAZ_DECR_CPT
            //                    ,COD_TRAT_RIASS
            //                    ,TP_DT_EMIS_RIASS
            //                    ,TP_CESS_RIASS
            //                    ,DS_RIGA
            //                    ,DS_OPER_SQL
            //                    ,DS_VER
            //                    ,DS_TS_INI_CPTZ
            //                    ,DS_TS_END_CPTZ
            //                    ,DS_UTENTE
            //                    ,DS_STATO_ELAB
            //                    ,AA_STAB
            //                    ,TS_STAB_LIMITATA
            //                    ,DT_PRESC
            //                    ,RSH_INVST
            //                    ,TP_RAMO_BILA
            //                  )
            //              VALUES
            //                  (
            //                    :GRZ-ID-GAR
            //                    ,:GRZ-ID-ADES
            //                     :IND-GRZ-ID-ADES
            //                    ,:GRZ-ID-POLI
            //                    ,:GRZ-ID-MOVI-CRZ
            //                    ,:GRZ-ID-MOVI-CHIU
            //                     :IND-GRZ-ID-MOVI-CHIU
            //                    ,:GRZ-DT-INI-EFF-DB
            //                    ,:GRZ-DT-END-EFF-DB
            //                    ,:GRZ-COD-COMP-ANIA
            //                    ,:GRZ-IB-OGG
            //                     :IND-GRZ-IB-OGG
            //                    ,:GRZ-DT-DECOR-DB
            //                     :IND-GRZ-DT-DECOR
            //                    ,:GRZ-DT-SCAD-DB
            //                     :IND-GRZ-DT-SCAD
            //                    ,:GRZ-COD-SEZ
            //                     :IND-GRZ-COD-SEZ
            //                    ,:GRZ-COD-TARI
            //                    ,:GRZ-RAMO-BILA
            //                     :IND-GRZ-RAMO-BILA
            //                    ,:GRZ-DT-INI-VAL-TAR-DB
            //                     :IND-GRZ-DT-INI-VAL-TAR
            //                    ,:GRZ-ID-1O-ASSTO
            //                     :IND-GRZ-ID-1O-ASSTO
            //                    ,:GRZ-ID-2O-ASSTO
            //                     :IND-GRZ-ID-2O-ASSTO
            //                    ,:GRZ-ID-3O-ASSTO
            //                     :IND-GRZ-ID-3O-ASSTO
            //                    ,:GRZ-TP-GAR
            //                     :IND-GRZ-TP-GAR
            //                    ,:GRZ-TP-RSH
            //                     :IND-GRZ-TP-RSH
            //                    ,:GRZ-TP-INVST
            //                     :IND-GRZ-TP-INVST
            //                    ,:GRZ-MOD-PAG-GARCOL
            //                     :IND-GRZ-MOD-PAG-GARCOL
            //                    ,:GRZ-TP-PER-PRE
            //                     :IND-GRZ-TP-PER-PRE
            //                    ,:GRZ-ETA-AA-1O-ASSTO
            //                     :IND-GRZ-ETA-AA-1O-ASSTO
            //                    ,:GRZ-ETA-MM-1O-ASSTO
            //                     :IND-GRZ-ETA-MM-1O-ASSTO
            //                    ,:GRZ-ETA-AA-2O-ASSTO
            //                     :IND-GRZ-ETA-AA-2O-ASSTO
            //                    ,:GRZ-ETA-MM-2O-ASSTO
            //                     :IND-GRZ-ETA-MM-2O-ASSTO
            //                    ,:GRZ-ETA-AA-3O-ASSTO
            //                     :IND-GRZ-ETA-AA-3O-ASSTO
            //                    ,:GRZ-ETA-MM-3O-ASSTO
            //                     :IND-GRZ-ETA-MM-3O-ASSTO
            //                    ,:GRZ-TP-EMIS-PUR
            //                     :IND-GRZ-TP-EMIS-PUR
            //                    ,:GRZ-ETA-A-SCAD
            //                     :IND-GRZ-ETA-A-SCAD
            //                    ,:GRZ-TP-CALC-PRE-PRSTZ
            //                     :IND-GRZ-TP-CALC-PRE-PRSTZ
            //                    ,:GRZ-TP-PRE
            //                     :IND-GRZ-TP-PRE
            //                    ,:GRZ-TP-DUR
            //                     :IND-GRZ-TP-DUR
            //                    ,:GRZ-DUR-AA
            //                     :IND-GRZ-DUR-AA
            //                    ,:GRZ-DUR-MM
            //                     :IND-GRZ-DUR-MM
            //                    ,:GRZ-DUR-GG
            //                     :IND-GRZ-DUR-GG
            //                    ,:GRZ-NUM-AA-PAG-PRE
            //                     :IND-GRZ-NUM-AA-PAG-PRE
            //                    ,:GRZ-AA-PAG-PRE-UNI
            //                     :IND-GRZ-AA-PAG-PRE-UNI
            //                    ,:GRZ-MM-PAG-PRE-UNI
            //                     :IND-GRZ-MM-PAG-PRE-UNI
            //                    ,:GRZ-FRAZ-INI-EROG-REN
            //                     :IND-GRZ-FRAZ-INI-EROG-REN
            //                    ,:GRZ-MM-1O-RAT
            //                     :IND-GRZ-MM-1O-RAT
            //                    ,:GRZ-PC-1O-RAT
            //                     :IND-GRZ-PC-1O-RAT
            //                    ,:GRZ-TP-PRSTZ-ASSTA
            //                     :IND-GRZ-TP-PRSTZ-ASSTA
            //                    ,:GRZ-DT-END-CARZ-DB
            //                     :IND-GRZ-DT-END-CARZ
            //                    ,:GRZ-PC-RIP-PRE
            //                     :IND-GRZ-PC-RIP-PRE
            //                    ,:GRZ-COD-FND
            //                     :IND-GRZ-COD-FND
            //                    ,:GRZ-AA-REN-CER
            //                     :IND-GRZ-AA-REN-CER
            //                    ,:GRZ-PC-REVRSB
            //                     :IND-GRZ-PC-REVRSB
            //                    ,:GRZ-TP-PC-RIP
            //                     :IND-GRZ-TP-PC-RIP
            //                    ,:GRZ-PC-OPZ
            //                     :IND-GRZ-PC-OPZ
            //                    ,:GRZ-TP-IAS
            //                     :IND-GRZ-TP-IAS
            //                    ,:GRZ-TP-STAB
            //                     :IND-GRZ-TP-STAB
            //                    ,:GRZ-TP-ADEG-PRE
            //                     :IND-GRZ-TP-ADEG-PRE
            //                    ,:GRZ-DT-VARZ-TP-IAS-DB
            //                     :IND-GRZ-DT-VARZ-TP-IAS
            //                    ,:GRZ-FRAZ-DECR-CPT
            //                     :IND-GRZ-FRAZ-DECR-CPT
            //                    ,:GRZ-COD-TRAT-RIASS
            //                     :IND-GRZ-COD-TRAT-RIASS
            //                    ,:GRZ-TP-DT-EMIS-RIASS
            //                     :IND-GRZ-TP-DT-EMIS-RIASS
            //                    ,:GRZ-TP-CESS-RIASS
            //                     :IND-GRZ-TP-CESS-RIASS
            //                    ,:GRZ-DS-RIGA
            //                    ,:GRZ-DS-OPER-SQL
            //                    ,:GRZ-DS-VER
            //                    ,:GRZ-DS-TS-INI-CPTZ
            //                    ,:GRZ-DS-TS-END-CPTZ
            //                    ,:GRZ-DS-UTENTE
            //                    ,:GRZ-DS-STATO-ELAB
            //                    ,:GRZ-AA-STAB
            //                     :IND-GRZ-AA-STAB
            //                    ,:GRZ-TS-STAB-LIMITATA
            //                     :IND-GRZ-TS-STAB-LIMITATA
            //                    ,:GRZ-DT-PRESC-DB
            //                     :IND-GRZ-DT-PRESC
            //                    ,:GRZ-RSH-INVST
            //                     :IND-GRZ-RSH-INVST
            //                    ,:GRZ-TP-RAMO-BILA
            //                  )
            //           END-EXEC
            garDao.insertRec(this);
            // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX
            a100CheckReturnCode();
        }
    }

    /**Original name: A230-UPDATE-PK<br>*/
    private void a230UpdatePk() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL       THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X           THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR              THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE GAR SET
        //                   ID_GAR                 =
        //                :GRZ-ID-GAR
        //                  ,ID_ADES                =
        //                :GRZ-ID-ADES
        //                                       :IND-GRZ-ID-ADES
        //                  ,ID_POLI                =
        //                :GRZ-ID-POLI
        //                  ,ID_MOVI_CRZ            =
        //                :GRZ-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :GRZ-ID-MOVI-CHIU
        //                                       :IND-GRZ-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :GRZ-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :GRZ-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :GRZ-COD-COMP-ANIA
        //                  ,IB_OGG                 =
        //                :GRZ-IB-OGG
        //                                       :IND-GRZ-IB-OGG
        //                  ,DT_DECOR               =
        //           :GRZ-DT-DECOR-DB
        //                                       :IND-GRZ-DT-DECOR
        //                  ,DT_SCAD                =
        //           :GRZ-DT-SCAD-DB
        //                                       :IND-GRZ-DT-SCAD
        //                  ,COD_SEZ                =
        //                :GRZ-COD-SEZ
        //                                       :IND-GRZ-COD-SEZ
        //                  ,COD_TARI               =
        //                :GRZ-COD-TARI
        //                  ,RAMO_BILA              =
        //                :GRZ-RAMO-BILA
        //                                       :IND-GRZ-RAMO-BILA
        //                  ,DT_INI_VAL_TAR         =
        //           :GRZ-DT-INI-VAL-TAR-DB
        //                                       :IND-GRZ-DT-INI-VAL-TAR
        //                  ,ID_1O_ASSTO            =
        //                :GRZ-ID-1O-ASSTO
        //                                       :IND-GRZ-ID-1O-ASSTO
        //                  ,ID_2O_ASSTO            =
        //                :GRZ-ID-2O-ASSTO
        //                                       :IND-GRZ-ID-2O-ASSTO
        //                  ,ID_3O_ASSTO            =
        //                :GRZ-ID-3O-ASSTO
        //                                       :IND-GRZ-ID-3O-ASSTO
        //                  ,TP_GAR                 =
        //                :GRZ-TP-GAR
        //                                       :IND-GRZ-TP-GAR
        //                  ,TP_RSH                 =
        //                :GRZ-TP-RSH
        //                                       :IND-GRZ-TP-RSH
        //                  ,TP_INVST               =
        //                :GRZ-TP-INVST
        //                                       :IND-GRZ-TP-INVST
        //                  ,MOD_PAG_GARCOL         =
        //                :GRZ-MOD-PAG-GARCOL
        //                                       :IND-GRZ-MOD-PAG-GARCOL
        //                  ,TP_PER_PRE             =
        //                :GRZ-TP-PER-PRE
        //                                       :IND-GRZ-TP-PER-PRE
        //                  ,ETA_AA_1O_ASSTO        =
        //                :GRZ-ETA-AA-1O-ASSTO
        //                                       :IND-GRZ-ETA-AA-1O-ASSTO
        //                  ,ETA_MM_1O_ASSTO        =
        //                :GRZ-ETA-MM-1O-ASSTO
        //                                       :IND-GRZ-ETA-MM-1O-ASSTO
        //                  ,ETA_AA_2O_ASSTO        =
        //                :GRZ-ETA-AA-2O-ASSTO
        //                                       :IND-GRZ-ETA-AA-2O-ASSTO
        //                  ,ETA_MM_2O_ASSTO        =
        //                :GRZ-ETA-MM-2O-ASSTO
        //                                       :IND-GRZ-ETA-MM-2O-ASSTO
        //                  ,ETA_AA_3O_ASSTO        =
        //                :GRZ-ETA-AA-3O-ASSTO
        //                                       :IND-GRZ-ETA-AA-3O-ASSTO
        //                  ,ETA_MM_3O_ASSTO        =
        //                :GRZ-ETA-MM-3O-ASSTO
        //                                       :IND-GRZ-ETA-MM-3O-ASSTO
        //                  ,TP_EMIS_PUR            =
        //                :GRZ-TP-EMIS-PUR
        //                                       :IND-GRZ-TP-EMIS-PUR
        //                  ,ETA_A_SCAD             =
        //                :GRZ-ETA-A-SCAD
        //                                       :IND-GRZ-ETA-A-SCAD
        //                  ,TP_CALC_PRE_PRSTZ      =
        //                :GRZ-TP-CALC-PRE-PRSTZ
        //                                       :IND-GRZ-TP-CALC-PRE-PRSTZ
        //                  ,TP_PRE                 =
        //                :GRZ-TP-PRE
        //                                       :IND-GRZ-TP-PRE
        //                  ,TP_DUR                 =
        //                :GRZ-TP-DUR
        //                                       :IND-GRZ-TP-DUR
        //                  ,DUR_AA                 =
        //                :GRZ-DUR-AA
        //                                       :IND-GRZ-DUR-AA
        //                  ,DUR_MM                 =
        //                :GRZ-DUR-MM
        //                                       :IND-GRZ-DUR-MM
        //                  ,DUR_GG                 =
        //                :GRZ-DUR-GG
        //                                       :IND-GRZ-DUR-GG
        //                  ,NUM_AA_PAG_PRE         =
        //                :GRZ-NUM-AA-PAG-PRE
        //                                       :IND-GRZ-NUM-AA-PAG-PRE
        //                  ,AA_PAG_PRE_UNI         =
        //                :GRZ-AA-PAG-PRE-UNI
        //                                       :IND-GRZ-AA-PAG-PRE-UNI
        //                  ,MM_PAG_PRE_UNI         =
        //                :GRZ-MM-PAG-PRE-UNI
        //                                       :IND-GRZ-MM-PAG-PRE-UNI
        //                  ,FRAZ_INI_EROG_REN      =
        //                :GRZ-FRAZ-INI-EROG-REN
        //                                       :IND-GRZ-FRAZ-INI-EROG-REN
        //                  ,MM_1O_RAT              =
        //                :GRZ-MM-1O-RAT
        //                                       :IND-GRZ-MM-1O-RAT
        //                  ,PC_1O_RAT              =
        //                :GRZ-PC-1O-RAT
        //                                       :IND-GRZ-PC-1O-RAT
        //                  ,TP_PRSTZ_ASSTA         =
        //                :GRZ-TP-PRSTZ-ASSTA
        //                                       :IND-GRZ-TP-PRSTZ-ASSTA
        //                  ,DT_END_CARZ            =
        //           :GRZ-DT-END-CARZ-DB
        //                                       :IND-GRZ-DT-END-CARZ
        //                  ,PC_RIP_PRE             =
        //                :GRZ-PC-RIP-PRE
        //                                       :IND-GRZ-PC-RIP-PRE
        //                  ,COD_FND                =
        //                :GRZ-COD-FND
        //                                       :IND-GRZ-COD-FND
        //                  ,AA_REN_CER             =
        //                :GRZ-AA-REN-CER
        //                                       :IND-GRZ-AA-REN-CER
        //                  ,PC_REVRSB              =
        //                :GRZ-PC-REVRSB
        //                                       :IND-GRZ-PC-REVRSB
        //                  ,TP_PC_RIP              =
        //                :GRZ-TP-PC-RIP
        //                                       :IND-GRZ-TP-PC-RIP
        //                  ,PC_OPZ                 =
        //                :GRZ-PC-OPZ
        //                                       :IND-GRZ-PC-OPZ
        //                  ,TP_IAS                 =
        //                :GRZ-TP-IAS
        //                                       :IND-GRZ-TP-IAS
        //                  ,TP_STAB                =
        //                :GRZ-TP-STAB
        //                                       :IND-GRZ-TP-STAB
        //                  ,TP_ADEG_PRE            =
        //                :GRZ-TP-ADEG-PRE
        //                                       :IND-GRZ-TP-ADEG-PRE
        //                  ,DT_VARZ_TP_IAS         =
        //           :GRZ-DT-VARZ-TP-IAS-DB
        //                                       :IND-GRZ-DT-VARZ-TP-IAS
        //                  ,FRAZ_DECR_CPT          =
        //                :GRZ-FRAZ-DECR-CPT
        //                                       :IND-GRZ-FRAZ-DECR-CPT
        //                  ,COD_TRAT_RIASS         =
        //                :GRZ-COD-TRAT-RIASS
        //                                       :IND-GRZ-COD-TRAT-RIASS
        //                  ,TP_DT_EMIS_RIASS       =
        //                :GRZ-TP-DT-EMIS-RIASS
        //                                       :IND-GRZ-TP-DT-EMIS-RIASS
        //                  ,TP_CESS_RIASS          =
        //                :GRZ-TP-CESS-RIASS
        //                                       :IND-GRZ-TP-CESS-RIASS
        //                  ,DS_RIGA                =
        //                :GRZ-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :GRZ-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :GRZ-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :GRZ-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :GRZ-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :GRZ-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :GRZ-DS-STATO-ELAB
        //                  ,AA_STAB                =
        //                :GRZ-AA-STAB
        //                                       :IND-GRZ-AA-STAB
        //                  ,TS_STAB_LIMITATA       =
        //                :GRZ-TS-STAB-LIMITATA
        //                                       :IND-GRZ-TS-STAB-LIMITATA
        //                  ,DT_PRESC               =
        //           :GRZ-DT-PRESC-DB
        //                                       :IND-GRZ-DT-PRESC
        //                  ,RSH_INVST              =
        //                :GRZ-RSH-INVST
        //                                       :IND-GRZ-RSH-INVST
        //                  ,TP_RAMO_BILA           =
        //                :GRZ-TP-RAMO-BILA
        //                WHERE     DS_RIGA = :GRZ-DS-RIGA
        //           END-EXEC.
        garDao.updateRec(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A240-DELETE-PK<br>*/
    private void a240DeletePk() {
        // COB_CODE: EXEC SQL
        //                DELETE
        //                FROM GAR
        //                WHERE     DS_RIGA = :GRZ-DS-RIGA
        //           END-EXEC.
        garDao.deleteByGrzDsRiga(gar.getGrzDsRiga());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A305-DECLARE-CURSOR-ID-EFF<br>
	 * <pre>----
	 * ----  gestione ID Effetto
	 * ----</pre>*/
    private void a305DeclareCursorIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-ID-UPD-EFF-GRZ CURSOR FOR
        //              SELECT
        //                     ID_GAR
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,COD_SEZ
        //                    ,COD_TARI
        //                    ,RAMO_BILA
        //                    ,DT_INI_VAL_TAR
        //                    ,ID_1O_ASSTO
        //                    ,ID_2O_ASSTO
        //                    ,ID_3O_ASSTO
        //                    ,TP_GAR
        //                    ,TP_RSH
        //                    ,TP_INVST
        //                    ,MOD_PAG_GARCOL
        //                    ,TP_PER_PRE
        //                    ,ETA_AA_1O_ASSTO
        //                    ,ETA_MM_1O_ASSTO
        //                    ,ETA_AA_2O_ASSTO
        //                    ,ETA_MM_2O_ASSTO
        //                    ,ETA_AA_3O_ASSTO
        //                    ,ETA_MM_3O_ASSTO
        //                    ,TP_EMIS_PUR
        //                    ,ETA_A_SCAD
        //                    ,TP_CALC_PRE_PRSTZ
        //                    ,TP_PRE
        //                    ,TP_DUR
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,NUM_AA_PAG_PRE
        //                    ,AA_PAG_PRE_UNI
        //                    ,MM_PAG_PRE_UNI
        //                    ,FRAZ_INI_EROG_REN
        //                    ,MM_1O_RAT
        //                    ,PC_1O_RAT
        //                    ,TP_PRSTZ_ASSTA
        //                    ,DT_END_CARZ
        //                    ,PC_RIP_PRE
        //                    ,COD_FND
        //                    ,AA_REN_CER
        //                    ,PC_REVRSB
        //                    ,TP_PC_RIP
        //                    ,PC_OPZ
        //                    ,TP_IAS
        //                    ,TP_STAB
        //                    ,TP_ADEG_PRE
        //                    ,DT_VARZ_TP_IAS
        //                    ,FRAZ_DECR_CPT
        //                    ,COD_TRAT_RIASS
        //                    ,TP_DT_EMIS_RIASS
        //                    ,TP_CESS_RIASS
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,AA_STAB
        //                    ,TS_STAB_LIMITATA
        //                    ,DT_PRESC
        //                    ,RSH_INVST
        //                    ,TP_RAMO_BILA
        //              FROM GAR
        //              WHERE     ID_GAR = :GRZ-ID-GAR
        //                    AND DS_TS_END_CPTZ = :WS-TS-INFINITO
        //                    AND DT_END_EFF > :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //              ORDER BY DT_INI_EFF ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A310-SELECT-ID-EFF<br>*/
    private void a310SelectIdEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_GAR
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,COD_SEZ
        //                ,COD_TARI
        //                ,RAMO_BILA
        //                ,DT_INI_VAL_TAR
        //                ,ID_1O_ASSTO
        //                ,ID_2O_ASSTO
        //                ,ID_3O_ASSTO
        //                ,TP_GAR
        //                ,TP_RSH
        //                ,TP_INVST
        //                ,MOD_PAG_GARCOL
        //                ,TP_PER_PRE
        //                ,ETA_AA_1O_ASSTO
        //                ,ETA_MM_1O_ASSTO
        //                ,ETA_AA_2O_ASSTO
        //                ,ETA_MM_2O_ASSTO
        //                ,ETA_AA_3O_ASSTO
        //                ,ETA_MM_3O_ASSTO
        //                ,TP_EMIS_PUR
        //                ,ETA_A_SCAD
        //                ,TP_CALC_PRE_PRSTZ
        //                ,TP_PRE
        //                ,TP_DUR
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,NUM_AA_PAG_PRE
        //                ,AA_PAG_PRE_UNI
        //                ,MM_PAG_PRE_UNI
        //                ,FRAZ_INI_EROG_REN
        //                ,MM_1O_RAT
        //                ,PC_1O_RAT
        //                ,TP_PRSTZ_ASSTA
        //                ,DT_END_CARZ
        //                ,PC_RIP_PRE
        //                ,COD_FND
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,TP_PC_RIP
        //                ,PC_OPZ
        //                ,TP_IAS
        //                ,TP_STAB
        //                ,TP_ADEG_PRE
        //                ,DT_VARZ_TP_IAS
        //                ,FRAZ_DECR_CPT
        //                ,COD_TRAT_RIASS
        //                ,TP_DT_EMIS_RIASS
        //                ,TP_CESS_RIASS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,AA_STAB
        //                ,TS_STAB_LIMITATA
        //                ,DT_PRESC
        //                ,RSH_INVST
        //                ,TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //             FROM GAR
        //             WHERE     ID_GAR = :GRZ-ID-GAR
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        garDao.selectRec(gar.getGrzIdGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A330-UPDATE-ID-EFF<br>*/
    private void a330UpdateIdEff() {
        // COB_CODE: PERFORM Z160-VALORIZZA-DATA-SERVICES-U THRU Z160-EX.
        z160ValorizzaDataServicesU();
        // COB_CODE: PERFORM Z200-SET-INDICATORI-NULL     THRU Z200-EX.
        z200SetIndicatoriNull();
        // COB_CODE: PERFORM Z900-CONVERTI-N-TO-X         THRU Z900-EX.
        z900ConvertiNToX();
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR            THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                UPDATE GAR SET
        //                   ID_GAR                 =
        //                :GRZ-ID-GAR
        //                  ,ID_ADES                =
        //                :GRZ-ID-ADES
        //                                       :IND-GRZ-ID-ADES
        //                  ,ID_POLI                =
        //                :GRZ-ID-POLI
        //                  ,ID_MOVI_CRZ            =
        //                :GRZ-ID-MOVI-CRZ
        //                  ,ID_MOVI_CHIU           =
        //                :GRZ-ID-MOVI-CHIU
        //                                       :IND-GRZ-ID-MOVI-CHIU
        //                  ,DT_INI_EFF             =
        //           :GRZ-DT-INI-EFF-DB
        //                  ,DT_END_EFF             =
        //           :GRZ-DT-END-EFF-DB
        //                  ,COD_COMP_ANIA          =
        //                :GRZ-COD-COMP-ANIA
        //                  ,IB_OGG                 =
        //                :GRZ-IB-OGG
        //                                       :IND-GRZ-IB-OGG
        //                  ,DT_DECOR               =
        //           :GRZ-DT-DECOR-DB
        //                                       :IND-GRZ-DT-DECOR
        //                  ,DT_SCAD                =
        //           :GRZ-DT-SCAD-DB
        //                                       :IND-GRZ-DT-SCAD
        //                  ,COD_SEZ                =
        //                :GRZ-COD-SEZ
        //                                       :IND-GRZ-COD-SEZ
        //                  ,COD_TARI               =
        //                :GRZ-COD-TARI
        //                  ,RAMO_BILA              =
        //                :GRZ-RAMO-BILA
        //                                       :IND-GRZ-RAMO-BILA
        //                  ,DT_INI_VAL_TAR         =
        //           :GRZ-DT-INI-VAL-TAR-DB
        //                                       :IND-GRZ-DT-INI-VAL-TAR
        //                  ,ID_1O_ASSTO            =
        //                :GRZ-ID-1O-ASSTO
        //                                       :IND-GRZ-ID-1O-ASSTO
        //                  ,ID_2O_ASSTO            =
        //                :GRZ-ID-2O-ASSTO
        //                                       :IND-GRZ-ID-2O-ASSTO
        //                  ,ID_3O_ASSTO            =
        //                :GRZ-ID-3O-ASSTO
        //                                       :IND-GRZ-ID-3O-ASSTO
        //                  ,TP_GAR                 =
        //                :GRZ-TP-GAR
        //                                       :IND-GRZ-TP-GAR
        //                  ,TP_RSH                 =
        //                :GRZ-TP-RSH
        //                                       :IND-GRZ-TP-RSH
        //                  ,TP_INVST               =
        //                :GRZ-TP-INVST
        //                                       :IND-GRZ-TP-INVST
        //                  ,MOD_PAG_GARCOL         =
        //                :GRZ-MOD-PAG-GARCOL
        //                                       :IND-GRZ-MOD-PAG-GARCOL
        //                  ,TP_PER_PRE             =
        //                :GRZ-TP-PER-PRE
        //                                       :IND-GRZ-TP-PER-PRE
        //                  ,ETA_AA_1O_ASSTO        =
        //                :GRZ-ETA-AA-1O-ASSTO
        //                                       :IND-GRZ-ETA-AA-1O-ASSTO
        //                  ,ETA_MM_1O_ASSTO        =
        //                :GRZ-ETA-MM-1O-ASSTO
        //                                       :IND-GRZ-ETA-MM-1O-ASSTO
        //                  ,ETA_AA_2O_ASSTO        =
        //                :GRZ-ETA-AA-2O-ASSTO
        //                                       :IND-GRZ-ETA-AA-2O-ASSTO
        //                  ,ETA_MM_2O_ASSTO        =
        //                :GRZ-ETA-MM-2O-ASSTO
        //                                       :IND-GRZ-ETA-MM-2O-ASSTO
        //                  ,ETA_AA_3O_ASSTO        =
        //                :GRZ-ETA-AA-3O-ASSTO
        //                                       :IND-GRZ-ETA-AA-3O-ASSTO
        //                  ,ETA_MM_3O_ASSTO        =
        //                :GRZ-ETA-MM-3O-ASSTO
        //                                       :IND-GRZ-ETA-MM-3O-ASSTO
        //                  ,TP_EMIS_PUR            =
        //                :GRZ-TP-EMIS-PUR
        //                                       :IND-GRZ-TP-EMIS-PUR
        //                  ,ETA_A_SCAD             =
        //                :GRZ-ETA-A-SCAD
        //                                       :IND-GRZ-ETA-A-SCAD
        //                  ,TP_CALC_PRE_PRSTZ      =
        //                :GRZ-TP-CALC-PRE-PRSTZ
        //                                       :IND-GRZ-TP-CALC-PRE-PRSTZ
        //                  ,TP_PRE                 =
        //                :GRZ-TP-PRE
        //                                       :IND-GRZ-TP-PRE
        //                  ,TP_DUR                 =
        //                :GRZ-TP-DUR
        //                                       :IND-GRZ-TP-DUR
        //                  ,DUR_AA                 =
        //                :GRZ-DUR-AA
        //                                       :IND-GRZ-DUR-AA
        //                  ,DUR_MM                 =
        //                :GRZ-DUR-MM
        //                                       :IND-GRZ-DUR-MM
        //                  ,DUR_GG                 =
        //                :GRZ-DUR-GG
        //                                       :IND-GRZ-DUR-GG
        //                  ,NUM_AA_PAG_PRE         =
        //                :GRZ-NUM-AA-PAG-PRE
        //                                       :IND-GRZ-NUM-AA-PAG-PRE
        //                  ,AA_PAG_PRE_UNI         =
        //                :GRZ-AA-PAG-PRE-UNI
        //                                       :IND-GRZ-AA-PAG-PRE-UNI
        //                  ,MM_PAG_PRE_UNI         =
        //                :GRZ-MM-PAG-PRE-UNI
        //                                       :IND-GRZ-MM-PAG-PRE-UNI
        //                  ,FRAZ_INI_EROG_REN      =
        //                :GRZ-FRAZ-INI-EROG-REN
        //                                       :IND-GRZ-FRAZ-INI-EROG-REN
        //                  ,MM_1O_RAT              =
        //                :GRZ-MM-1O-RAT
        //                                       :IND-GRZ-MM-1O-RAT
        //                  ,PC_1O_RAT              =
        //                :GRZ-PC-1O-RAT
        //                                       :IND-GRZ-PC-1O-RAT
        //                  ,TP_PRSTZ_ASSTA         =
        //                :GRZ-TP-PRSTZ-ASSTA
        //                                       :IND-GRZ-TP-PRSTZ-ASSTA
        //                  ,DT_END_CARZ            =
        //           :GRZ-DT-END-CARZ-DB
        //                                       :IND-GRZ-DT-END-CARZ
        //                  ,PC_RIP_PRE             =
        //                :GRZ-PC-RIP-PRE
        //                                       :IND-GRZ-PC-RIP-PRE
        //                  ,COD_FND                =
        //                :GRZ-COD-FND
        //                                       :IND-GRZ-COD-FND
        //                  ,AA_REN_CER             =
        //                :GRZ-AA-REN-CER
        //                                       :IND-GRZ-AA-REN-CER
        //                  ,PC_REVRSB              =
        //                :GRZ-PC-REVRSB
        //                                       :IND-GRZ-PC-REVRSB
        //                  ,TP_PC_RIP              =
        //                :GRZ-TP-PC-RIP
        //                                       :IND-GRZ-TP-PC-RIP
        //                  ,PC_OPZ                 =
        //                :GRZ-PC-OPZ
        //                                       :IND-GRZ-PC-OPZ
        //                  ,TP_IAS                 =
        //                :GRZ-TP-IAS
        //                                       :IND-GRZ-TP-IAS
        //                  ,TP_STAB                =
        //                :GRZ-TP-STAB
        //                                       :IND-GRZ-TP-STAB
        //                  ,TP_ADEG_PRE            =
        //                :GRZ-TP-ADEG-PRE
        //                                       :IND-GRZ-TP-ADEG-PRE
        //                  ,DT_VARZ_TP_IAS         =
        //           :GRZ-DT-VARZ-TP-IAS-DB
        //                                       :IND-GRZ-DT-VARZ-TP-IAS
        //                  ,FRAZ_DECR_CPT          =
        //                :GRZ-FRAZ-DECR-CPT
        //                                       :IND-GRZ-FRAZ-DECR-CPT
        //                  ,COD_TRAT_RIASS         =
        //                :GRZ-COD-TRAT-RIASS
        //                                       :IND-GRZ-COD-TRAT-RIASS
        //                  ,TP_DT_EMIS_RIASS       =
        //                :GRZ-TP-DT-EMIS-RIASS
        //                                       :IND-GRZ-TP-DT-EMIS-RIASS
        //                  ,TP_CESS_RIASS          =
        //                :GRZ-TP-CESS-RIASS
        //                                       :IND-GRZ-TP-CESS-RIASS
        //                  ,DS_RIGA                =
        //                :GRZ-DS-RIGA
        //                  ,DS_OPER_SQL            =
        //                :GRZ-DS-OPER-SQL
        //                  ,DS_VER                 =
        //                :GRZ-DS-VER
        //                  ,DS_TS_INI_CPTZ         =
        //                :GRZ-DS-TS-INI-CPTZ
        //                  ,DS_TS_END_CPTZ         =
        //                :GRZ-DS-TS-END-CPTZ
        //                  ,DS_UTENTE              =
        //                :GRZ-DS-UTENTE
        //                  ,DS_STATO_ELAB          =
        //                :GRZ-DS-STATO-ELAB
        //                  ,AA_STAB                =
        //                :GRZ-AA-STAB
        //                                       :IND-GRZ-AA-STAB
        //                  ,TS_STAB_LIMITATA       =
        //                :GRZ-TS-STAB-LIMITATA
        //                                       :IND-GRZ-TS-STAB-LIMITATA
        //                  ,DT_PRESC               =
        //           :GRZ-DT-PRESC-DB
        //                                       :IND-GRZ-DT-PRESC
        //                  ,RSH_INVST              =
        //                :GRZ-RSH-INVST
        //                                       :IND-GRZ-RSH-INVST
        //                  ,TP_RAMO_BILA           =
        //                :GRZ-TP-RAMO-BILA
        //                WHERE     DS_RIGA = :GRZ-DS-RIGA
        //                   AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        garDao.updateRec1(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A360-OPEN-CURSOR-ID-EFF<br>*/
    private void a360OpenCursorIdEff() {
        // COB_CODE: PERFORM A305-DECLARE-CURSOR-ID-EFF THRU A305-EX.
        a305DeclareCursorIdEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-ID-UPD-EFF-GRZ
        //           END-EXEC.
        garDao.openCIdUpdEffGrz(gar.getGrzIdGar(), ws.getIdsv0010().getWsTsInfinito(), ws.getIdsv0010().getWsDataInizioEffettoDb(), idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A370-CLOSE-CURSOR-ID-EFF<br>*/
    private void a370CloseCursorIdEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-ID-UPD-EFF-GRZ
        //           END-EXEC.
        garDao.closeCIdUpdEffGrz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A390-FETCH-NEXT-ID-EFF<br>*/
    private void a390FetchNextIdEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-ID-UPD-EFF-GRZ
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garDao.fetchCIdUpdEffGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A370-CLOSE-CURSOR-ID-EFF THRU A370-EX
            a370CloseCursorIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A405-DECLARE-CURSOR-IDP-EFF<br>
	 * <pre>----
	 * ----  gestione IDP Effetto
	 * ----</pre>*/
    private void a405DeclareCursorIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-EFF-GRZ CURSOR FOR
        //              SELECT
        //                     ID_GAR
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,COD_SEZ
        //                    ,COD_TARI
        //                    ,RAMO_BILA
        //                    ,DT_INI_VAL_TAR
        //                    ,ID_1O_ASSTO
        //                    ,ID_2O_ASSTO
        //                    ,ID_3O_ASSTO
        //                    ,TP_GAR
        //                    ,TP_RSH
        //                    ,TP_INVST
        //                    ,MOD_PAG_GARCOL
        //                    ,TP_PER_PRE
        //                    ,ETA_AA_1O_ASSTO
        //                    ,ETA_MM_1O_ASSTO
        //                    ,ETA_AA_2O_ASSTO
        //                    ,ETA_MM_2O_ASSTO
        //                    ,ETA_AA_3O_ASSTO
        //                    ,ETA_MM_3O_ASSTO
        //                    ,TP_EMIS_PUR
        //                    ,ETA_A_SCAD
        //                    ,TP_CALC_PRE_PRSTZ
        //                    ,TP_PRE
        //                    ,TP_DUR
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,NUM_AA_PAG_PRE
        //                    ,AA_PAG_PRE_UNI
        //                    ,MM_PAG_PRE_UNI
        //                    ,FRAZ_INI_EROG_REN
        //                    ,MM_1O_RAT
        //                    ,PC_1O_RAT
        //                    ,TP_PRSTZ_ASSTA
        //                    ,DT_END_CARZ
        //                    ,PC_RIP_PRE
        //                    ,COD_FND
        //                    ,AA_REN_CER
        //                    ,PC_REVRSB
        //                    ,TP_PC_RIP
        //                    ,PC_OPZ
        //                    ,TP_IAS
        //                    ,TP_STAB
        //                    ,TP_ADEG_PRE
        //                    ,DT_VARZ_TP_IAS
        //                    ,FRAZ_DECR_CPT
        //                    ,COD_TRAT_RIASS
        //                    ,TP_DT_EMIS_RIASS
        //                    ,TP_CESS_RIASS
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,AA_STAB
        //                    ,TS_STAB_LIMITATA
        //                    ,DT_PRESC
        //                    ,RSH_INVST
        //                    ,TP_RAMO_BILA
        //              FROM GAR
        //              WHERE     ID_POLI = :GRZ-ID-POLI
        //                    AND ID_ADES = :GRZ-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A410-SELECT-IDP-EFF<br>*/
    private void a410SelectIdpEff() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_GAR
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,COD_SEZ
        //                ,COD_TARI
        //                ,RAMO_BILA
        //                ,DT_INI_VAL_TAR
        //                ,ID_1O_ASSTO
        //                ,ID_2O_ASSTO
        //                ,ID_3O_ASSTO
        //                ,TP_GAR
        //                ,TP_RSH
        //                ,TP_INVST
        //                ,MOD_PAG_GARCOL
        //                ,TP_PER_PRE
        //                ,ETA_AA_1O_ASSTO
        //                ,ETA_MM_1O_ASSTO
        //                ,ETA_AA_2O_ASSTO
        //                ,ETA_MM_2O_ASSTO
        //                ,ETA_AA_3O_ASSTO
        //                ,ETA_MM_3O_ASSTO
        //                ,TP_EMIS_PUR
        //                ,ETA_A_SCAD
        //                ,TP_CALC_PRE_PRSTZ
        //                ,TP_PRE
        //                ,TP_DUR
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,NUM_AA_PAG_PRE
        //                ,AA_PAG_PRE_UNI
        //                ,MM_PAG_PRE_UNI
        //                ,FRAZ_INI_EROG_REN
        //                ,MM_1O_RAT
        //                ,PC_1O_RAT
        //                ,TP_PRSTZ_ASSTA
        //                ,DT_END_CARZ
        //                ,PC_RIP_PRE
        //                ,COD_FND
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,TP_PC_RIP
        //                ,PC_OPZ
        //                ,TP_IAS
        //                ,TP_STAB
        //                ,TP_ADEG_PRE
        //                ,DT_VARZ_TP_IAS
        //                ,FRAZ_DECR_CPT
        //                ,COD_TRAT_RIASS
        //                ,TP_DT_EMIS_RIASS
        //                ,TP_CESS_RIASS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,AA_STAB
        //                ,TS_STAB_LIMITATA
        //                ,DT_PRESC
        //                ,RSH_INVST
        //                ,TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //             FROM GAR
        //             WHERE     ID_POLI = :GRZ-ID-POLI
        //                    AND ID_ADES = :GRZ-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        garDao.selectRec1(gar.getGrzIdPoli(), gar.getGrzIdAdes().getGrzIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A460-OPEN-CURSOR-IDP-EFF<br>*/
    private void a460OpenCursorIdpEff() {
        // COB_CODE: PERFORM A405-DECLARE-CURSOR-IDP-EFF THRU A405-EX.
        a405DeclareCursorIdpEff();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-EFF-GRZ
        //           END-EXEC.
        garDao.openCIdpEffGrz(gar.getGrzIdPoli(), gar.getGrzIdAdes().getGrzIdAdes(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A470-CLOSE-CURSOR-IDP-EFF<br>*/
    private void a470CloseCursorIdpEff() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-EFF-GRZ
        //           END-EXEC.
        garDao.closeCIdpEffGrz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A480-FETCH-FIRST-IDP-EFF<br>*/
    private void a480FetchFirstIdpEff() {
        // COB_CODE: PERFORM A460-OPEN-CURSOR-IDP-EFF    THRU A460-EX.
        a460OpenCursorIdpEff();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A490-FETCH-NEXT-IDP-EFF THRU A490-EX
            a490FetchNextIdpEff();
        }
    }

    /**Original name: A490-FETCH-NEXT-IDP-EFF<br>*/
    private void a490FetchNextIdpEff() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-EFF-GRZ
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garDao.fetchCIdpEffGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A470-CLOSE-CURSOR-IDP-EFF THRU A470-EX
            a470CloseCursorIdpEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A505-DECLARE-CURSOR-IBO<br>
	 * <pre>----
	 * ----  gestione IBO Effetto e non
	 * ----</pre>*/
    private void a505DeclareCursorIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-EFF-GRZ CURSOR FOR
        //              SELECT
        //                     ID_GAR
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,COD_SEZ
        //                    ,COD_TARI
        //                    ,RAMO_BILA
        //                    ,DT_INI_VAL_TAR
        //                    ,ID_1O_ASSTO
        //                    ,ID_2O_ASSTO
        //                    ,ID_3O_ASSTO
        //                    ,TP_GAR
        //                    ,TP_RSH
        //                    ,TP_INVST
        //                    ,MOD_PAG_GARCOL
        //                    ,TP_PER_PRE
        //                    ,ETA_AA_1O_ASSTO
        //                    ,ETA_MM_1O_ASSTO
        //                    ,ETA_AA_2O_ASSTO
        //                    ,ETA_MM_2O_ASSTO
        //                    ,ETA_AA_3O_ASSTO
        //                    ,ETA_MM_3O_ASSTO
        //                    ,TP_EMIS_PUR
        //                    ,ETA_A_SCAD
        //                    ,TP_CALC_PRE_PRSTZ
        //                    ,TP_PRE
        //                    ,TP_DUR
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,NUM_AA_PAG_PRE
        //                    ,AA_PAG_PRE_UNI
        //                    ,MM_PAG_PRE_UNI
        //                    ,FRAZ_INI_EROG_REN
        //                    ,MM_1O_RAT
        //                    ,PC_1O_RAT
        //                    ,TP_PRSTZ_ASSTA
        //                    ,DT_END_CARZ
        //                    ,PC_RIP_PRE
        //                    ,COD_FND
        //                    ,AA_REN_CER
        //                    ,PC_REVRSB
        //                    ,TP_PC_RIP
        //                    ,PC_OPZ
        //                    ,TP_IAS
        //                    ,TP_STAB
        //                    ,TP_ADEG_PRE
        //                    ,DT_VARZ_TP_IAS
        //                    ,FRAZ_DECR_CPT
        //                    ,COD_TRAT_RIASS
        //                    ,TP_DT_EMIS_RIASS
        //                    ,TP_CESS_RIASS
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,AA_STAB
        //                    ,TS_STAB_LIMITATA
        //                    ,DT_PRESC
        //                    ,RSH_INVST
        //                    ,TP_RAMO_BILA
        //              FROM GAR
        //              WHERE     IB_OGG = :GRZ-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //              ORDER BY ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: A510-SELECT-IBO<br>*/
    private void a510SelectIbo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_GAR
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,COD_SEZ
        //                ,COD_TARI
        //                ,RAMO_BILA
        //                ,DT_INI_VAL_TAR
        //                ,ID_1O_ASSTO
        //                ,ID_2O_ASSTO
        //                ,ID_3O_ASSTO
        //                ,TP_GAR
        //                ,TP_RSH
        //                ,TP_INVST
        //                ,MOD_PAG_GARCOL
        //                ,TP_PER_PRE
        //                ,ETA_AA_1O_ASSTO
        //                ,ETA_MM_1O_ASSTO
        //                ,ETA_AA_2O_ASSTO
        //                ,ETA_MM_2O_ASSTO
        //                ,ETA_AA_3O_ASSTO
        //                ,ETA_MM_3O_ASSTO
        //                ,TP_EMIS_PUR
        //                ,ETA_A_SCAD
        //                ,TP_CALC_PRE_PRSTZ
        //                ,TP_PRE
        //                ,TP_DUR
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,NUM_AA_PAG_PRE
        //                ,AA_PAG_PRE_UNI
        //                ,MM_PAG_PRE_UNI
        //                ,FRAZ_INI_EROG_REN
        //                ,MM_1O_RAT
        //                ,PC_1O_RAT
        //                ,TP_PRSTZ_ASSTA
        //                ,DT_END_CARZ
        //                ,PC_RIP_PRE
        //                ,COD_FND
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,TP_PC_RIP
        //                ,PC_OPZ
        //                ,TP_IAS
        //                ,TP_STAB
        //                ,TP_ADEG_PRE
        //                ,DT_VARZ_TP_IAS
        //                ,FRAZ_DECR_CPT
        //                ,COD_TRAT_RIASS
        //                ,TP_DT_EMIS_RIASS
        //                ,TP_CESS_RIASS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,AA_STAB
        //                ,TS_STAB_LIMITATA
        //                ,DT_PRESC
        //                ,RSH_INVST
        //                ,TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //             FROM GAR
        //             WHERE     IB_OGG = :GRZ-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND ID_MOVI_CHIU IS NULL
        //           END-EXEC.
        garDao.selectRec2(gar.getGrzIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: A560-OPEN-CURSOR-IBO<br>*/
    private void a560OpenCursorIbo() {
        // COB_CODE: PERFORM A505-DECLARE-CURSOR-IBO     THRU A505-EX.
        a505DeclareCursorIbo();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-EFF-GRZ
        //           END-EXEC.
        garDao.openCIboEffGrz(gar.getGrzIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A570-CLOSE-CURSOR-IBO<br>*/
    private void a570CloseCursorIbo() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-EFF-GRZ
        //           END-EXEC.
        garDao.closeCIboEffGrz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: A580-FETCH-FIRST-IBO<br>*/
    private void a580FetchFirstIbo() {
        // COB_CODE: PERFORM A560-OPEN-CURSOR-IBO        THRU A560-EX.
        a560OpenCursorIbo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A590-FETCH-NEXT-IBO     THRU A590-EX
            a590FetchNextIbo();
        }
    }

    /**Original name: A590-FETCH-NEXT-IBO<br>*/
    private void a590FetchNextIbo() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-EFF-GRZ
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garDao.fetchCIboEffGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM A570-CLOSE-CURSOR-IBO     THRU A570-EX
            a570CloseCursorIbo();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: A605-DECLARE-CURSOR-IBS<br>
	 * <pre>----
	 * ----  gestione IBS Effetto e non
	 * ----</pre>*/
    private void a605DeclareCursorIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A610-SELECT-IBS<br>*/
    private void a610SelectIbs() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A660-OPEN-CURSOR-IBS<br>*/
    private void a660OpenCursorIbs() {
        // COB_CODE: PERFORM A605-DECLARE-CURSOR-IBS     THRU A605-EX.
        a605DeclareCursorIbs();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A670-CLOSE-CURSOR-IBS<br>*/
    private void a670CloseCursorIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A680-FETCH-FIRST-IBS<br>*/
    private void a680FetchFirstIbs() {
        // COB_CODE: PERFORM A660-OPEN-CURSOR-IBS        THRU A660-EX.
        a660OpenCursorIbs();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A690-FETCH-NEXT-IBS     THRU A690-EX
            a690FetchNextIbs();
        }
    }

    /**Original name: A690-FETCH-NEXT-IBS<br>*/
    private void a690FetchNextIbs() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A705-DECLARE-CURSOR-IDO<br>
	 * <pre>----
	 * ----  gestione IDO Effetto e non
	 * ----</pre>*/
    private void a705DeclareCursorIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A710-SELECT-IDO<br>*/
    private void a710SelectIdo() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A760-OPEN-CURSOR-IDO<br>*/
    private void a760OpenCursorIdo() {
        // COB_CODE: PERFORM A705-DECLARE-CURSOR-IDO     THRU A705-EX.
        a705DeclareCursorIdo();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A770-CLOSE-CURSOR-IDO<br>*/
    private void a770CloseCursorIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: A780-FETCH-FIRST-IDO<br>*/
    private void a780FetchFirstIdo() {
        // COB_CODE: PERFORM A760-OPEN-CURSOR-IDO        THRU A760-EX.
        a760OpenCursorIdo();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A790-FETCH-NEXT-IDO     THRU A790-EX
            a790FetchNextIdo();
        }
    }

    /**Original name: A790-FETCH-NEXT-IDO<br>*/
    private void a790FetchNextIdo() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B310-SELECT-ID-CPZ<br>
	 * <pre>----
	 * ----  gestione ID Competenza
	 * ----</pre>*/
    private void b310SelectIdCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_GAR
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,COD_SEZ
        //                ,COD_TARI
        //                ,RAMO_BILA
        //                ,DT_INI_VAL_TAR
        //                ,ID_1O_ASSTO
        //                ,ID_2O_ASSTO
        //                ,ID_3O_ASSTO
        //                ,TP_GAR
        //                ,TP_RSH
        //                ,TP_INVST
        //                ,MOD_PAG_GARCOL
        //                ,TP_PER_PRE
        //                ,ETA_AA_1O_ASSTO
        //                ,ETA_MM_1O_ASSTO
        //                ,ETA_AA_2O_ASSTO
        //                ,ETA_MM_2O_ASSTO
        //                ,ETA_AA_3O_ASSTO
        //                ,ETA_MM_3O_ASSTO
        //                ,TP_EMIS_PUR
        //                ,ETA_A_SCAD
        //                ,TP_CALC_PRE_PRSTZ
        //                ,TP_PRE
        //                ,TP_DUR
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,NUM_AA_PAG_PRE
        //                ,AA_PAG_PRE_UNI
        //                ,MM_PAG_PRE_UNI
        //                ,FRAZ_INI_EROG_REN
        //                ,MM_1O_RAT
        //                ,PC_1O_RAT
        //                ,TP_PRSTZ_ASSTA
        //                ,DT_END_CARZ
        //                ,PC_RIP_PRE
        //                ,COD_FND
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,TP_PC_RIP
        //                ,PC_OPZ
        //                ,TP_IAS
        //                ,TP_STAB
        //                ,TP_ADEG_PRE
        //                ,DT_VARZ_TP_IAS
        //                ,FRAZ_DECR_CPT
        //                ,COD_TRAT_RIASS
        //                ,TP_DT_EMIS_RIASS
        //                ,TP_CESS_RIASS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,AA_STAB
        //                ,TS_STAB_LIMITATA
        //                ,DT_PRESC
        //                ,RSH_INVST
        //                ,TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //             FROM GAR
        //             WHERE     ID_GAR = :GRZ-ID-GAR
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        garDao.selectRec3(gar.getGrzIdGar(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B405-DECLARE-CURSOR-IDP-CPZ<br>
	 * <pre>----
	 * ----  gestione IDP Competenza
	 * ----</pre>*/
    private void b405DeclareCursorIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IDP-CPZ-GRZ CURSOR FOR
        //              SELECT
        //                     ID_GAR
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,COD_SEZ
        //                    ,COD_TARI
        //                    ,RAMO_BILA
        //                    ,DT_INI_VAL_TAR
        //                    ,ID_1O_ASSTO
        //                    ,ID_2O_ASSTO
        //                    ,ID_3O_ASSTO
        //                    ,TP_GAR
        //                    ,TP_RSH
        //                    ,TP_INVST
        //                    ,MOD_PAG_GARCOL
        //                    ,TP_PER_PRE
        //                    ,ETA_AA_1O_ASSTO
        //                    ,ETA_MM_1O_ASSTO
        //                    ,ETA_AA_2O_ASSTO
        //                    ,ETA_MM_2O_ASSTO
        //                    ,ETA_AA_3O_ASSTO
        //                    ,ETA_MM_3O_ASSTO
        //                    ,TP_EMIS_PUR
        //                    ,ETA_A_SCAD
        //                    ,TP_CALC_PRE_PRSTZ
        //                    ,TP_PRE
        //                    ,TP_DUR
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,NUM_AA_PAG_PRE
        //                    ,AA_PAG_PRE_UNI
        //                    ,MM_PAG_PRE_UNI
        //                    ,FRAZ_INI_EROG_REN
        //                    ,MM_1O_RAT
        //                    ,PC_1O_RAT
        //                    ,TP_PRSTZ_ASSTA
        //                    ,DT_END_CARZ
        //                    ,PC_RIP_PRE
        //                    ,COD_FND
        //                    ,AA_REN_CER
        //                    ,PC_REVRSB
        //                    ,TP_PC_RIP
        //                    ,PC_OPZ
        //                    ,TP_IAS
        //                    ,TP_STAB
        //                    ,TP_ADEG_PRE
        //                    ,DT_VARZ_TP_IAS
        //                    ,FRAZ_DECR_CPT
        //                    ,COD_TRAT_RIASS
        //                    ,TP_DT_EMIS_RIASS
        //                    ,TP_CESS_RIASS
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,AA_STAB
        //                    ,TS_STAB_LIMITATA
        //                    ,DT_PRESC
        //                    ,RSH_INVST
        //                    ,TP_RAMO_BILA
        //              FROM GAR
        //              WHERE     ID_POLI = :GRZ-ID-POLI
        //           AND ID_ADES = :GRZ-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B410-SELECT-IDP-CPZ<br>*/
    private void b410SelectIdpCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_GAR
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,COD_SEZ
        //                ,COD_TARI
        //                ,RAMO_BILA
        //                ,DT_INI_VAL_TAR
        //                ,ID_1O_ASSTO
        //                ,ID_2O_ASSTO
        //                ,ID_3O_ASSTO
        //                ,TP_GAR
        //                ,TP_RSH
        //                ,TP_INVST
        //                ,MOD_PAG_GARCOL
        //                ,TP_PER_PRE
        //                ,ETA_AA_1O_ASSTO
        //                ,ETA_MM_1O_ASSTO
        //                ,ETA_AA_2O_ASSTO
        //                ,ETA_MM_2O_ASSTO
        //                ,ETA_AA_3O_ASSTO
        //                ,ETA_MM_3O_ASSTO
        //                ,TP_EMIS_PUR
        //                ,ETA_A_SCAD
        //                ,TP_CALC_PRE_PRSTZ
        //                ,TP_PRE
        //                ,TP_DUR
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,NUM_AA_PAG_PRE
        //                ,AA_PAG_PRE_UNI
        //                ,MM_PAG_PRE_UNI
        //                ,FRAZ_INI_EROG_REN
        //                ,MM_1O_RAT
        //                ,PC_1O_RAT
        //                ,TP_PRSTZ_ASSTA
        //                ,DT_END_CARZ
        //                ,PC_RIP_PRE
        //                ,COD_FND
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,TP_PC_RIP
        //                ,PC_OPZ
        //                ,TP_IAS
        //                ,TP_STAB
        //                ,TP_ADEG_PRE
        //                ,DT_VARZ_TP_IAS
        //                ,FRAZ_DECR_CPT
        //                ,COD_TRAT_RIASS
        //                ,TP_DT_EMIS_RIASS
        //                ,TP_CESS_RIASS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,AA_STAB
        //                ,TS_STAB_LIMITATA
        //                ,DT_PRESC
        //                ,RSH_INVST
        //                ,TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //             FROM GAR
        //             WHERE     ID_POLI = :GRZ-ID-POLI
        //                    AND ID_ADES = :GRZ-ID-ADES
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        garDao.selectRec4(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B460-OPEN-CURSOR-IDP-CPZ<br>*/
    private void b460OpenCursorIdpCpz() {
        // COB_CODE: PERFORM B405-DECLARE-CURSOR-IDP-CPZ THRU B405-EX.
        b405DeclareCursorIdpCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IDP-CPZ-GRZ
        //           END-EXEC.
        garDao.openCIdpCpzGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B470-CLOSE-CURSOR-IDP-CPZ<br>*/
    private void b470CloseCursorIdpCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IDP-CPZ-GRZ
        //           END-EXEC.
        garDao.closeCIdpCpzGrz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B480-FETCH-FIRST-IDP-CPZ<br>*/
    private void b480FetchFirstIdpCpz() {
        // COB_CODE: PERFORM B460-OPEN-CURSOR-IDP-CPZ    THRU B460-EX.
        b460OpenCursorIdpCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B490-FETCH-NEXT-IDP-CPZ THRU B490-EX
            b490FetchNextIdpCpz();
        }
    }

    /**Original name: B490-FETCH-NEXT-IDP-CPZ<br>*/
    private void b490FetchNextIdpCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IDP-CPZ-GRZ
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garDao.fetchCIdpCpzGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B470-CLOSE-CURSOR-IDP-CPZ THRU B470-EX
            b470CloseCursorIdpCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B505-DECLARE-CURSOR-IBO-CPZ<br>
	 * <pre>----
	 * ----  gestione IBO Competenza
	 * ----</pre>*/
    private void b505DeclareCursorIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //                DECLARE C-IBO-CPZ-GRZ CURSOR FOR
        //              SELECT
        //                     ID_GAR
        //                    ,ID_ADES
        //                    ,ID_POLI
        //                    ,ID_MOVI_CRZ
        //                    ,ID_MOVI_CHIU
        //                    ,DT_INI_EFF
        //                    ,DT_END_EFF
        //                    ,COD_COMP_ANIA
        //                    ,IB_OGG
        //                    ,DT_DECOR
        //                    ,DT_SCAD
        //                    ,COD_SEZ
        //                    ,COD_TARI
        //                    ,RAMO_BILA
        //                    ,DT_INI_VAL_TAR
        //                    ,ID_1O_ASSTO
        //                    ,ID_2O_ASSTO
        //                    ,ID_3O_ASSTO
        //                    ,TP_GAR
        //                    ,TP_RSH
        //                    ,TP_INVST
        //                    ,MOD_PAG_GARCOL
        //                    ,TP_PER_PRE
        //                    ,ETA_AA_1O_ASSTO
        //                    ,ETA_MM_1O_ASSTO
        //                    ,ETA_AA_2O_ASSTO
        //                    ,ETA_MM_2O_ASSTO
        //                    ,ETA_AA_3O_ASSTO
        //                    ,ETA_MM_3O_ASSTO
        //                    ,TP_EMIS_PUR
        //                    ,ETA_A_SCAD
        //                    ,TP_CALC_PRE_PRSTZ
        //                    ,TP_PRE
        //                    ,TP_DUR
        //                    ,DUR_AA
        //                    ,DUR_MM
        //                    ,DUR_GG
        //                    ,NUM_AA_PAG_PRE
        //                    ,AA_PAG_PRE_UNI
        //                    ,MM_PAG_PRE_UNI
        //                    ,FRAZ_INI_EROG_REN
        //                    ,MM_1O_RAT
        //                    ,PC_1O_RAT
        //                    ,TP_PRSTZ_ASSTA
        //                    ,DT_END_CARZ
        //                    ,PC_RIP_PRE
        //                    ,COD_FND
        //                    ,AA_REN_CER
        //                    ,PC_REVRSB
        //                    ,TP_PC_RIP
        //                    ,PC_OPZ
        //                    ,TP_IAS
        //                    ,TP_STAB
        //                    ,TP_ADEG_PRE
        //                    ,DT_VARZ_TP_IAS
        //                    ,FRAZ_DECR_CPT
        //                    ,COD_TRAT_RIASS
        //                    ,TP_DT_EMIS_RIASS
        //                    ,TP_CESS_RIASS
        //                    ,DS_RIGA
        //                    ,DS_OPER_SQL
        //                    ,DS_VER
        //                    ,DS_TS_INI_CPTZ
        //                    ,DS_TS_END_CPTZ
        //                    ,DS_UTENTE
        //                    ,DS_STATO_ELAB
        //                    ,AA_STAB
        //                    ,TS_STAB_LIMITATA
        //                    ,DT_PRESC
        //                    ,RSH_INVST
        //                    ,TP_RAMO_BILA
        //              FROM GAR
        //              WHERE     IB_OGG = :GRZ-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //              ORDER BY ID_GAR ASC
        //           END-EXEC.
        // DECLARE CURSOR doesn't need a translation;
    }

    /**Original name: B510-SELECT-IBO-CPZ<br>*/
    private void b510SelectIboCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: EXEC SQL
        //             SELECT
        //                ID_GAR
        //                ,ID_ADES
        //                ,ID_POLI
        //                ,ID_MOVI_CRZ
        //                ,ID_MOVI_CHIU
        //                ,DT_INI_EFF
        //                ,DT_END_EFF
        //                ,COD_COMP_ANIA
        //                ,IB_OGG
        //                ,DT_DECOR
        //                ,DT_SCAD
        //                ,COD_SEZ
        //                ,COD_TARI
        //                ,RAMO_BILA
        //                ,DT_INI_VAL_TAR
        //                ,ID_1O_ASSTO
        //                ,ID_2O_ASSTO
        //                ,ID_3O_ASSTO
        //                ,TP_GAR
        //                ,TP_RSH
        //                ,TP_INVST
        //                ,MOD_PAG_GARCOL
        //                ,TP_PER_PRE
        //                ,ETA_AA_1O_ASSTO
        //                ,ETA_MM_1O_ASSTO
        //                ,ETA_AA_2O_ASSTO
        //                ,ETA_MM_2O_ASSTO
        //                ,ETA_AA_3O_ASSTO
        //                ,ETA_MM_3O_ASSTO
        //                ,TP_EMIS_PUR
        //                ,ETA_A_SCAD
        //                ,TP_CALC_PRE_PRSTZ
        //                ,TP_PRE
        //                ,TP_DUR
        //                ,DUR_AA
        //                ,DUR_MM
        //                ,DUR_GG
        //                ,NUM_AA_PAG_PRE
        //                ,AA_PAG_PRE_UNI
        //                ,MM_PAG_PRE_UNI
        //                ,FRAZ_INI_EROG_REN
        //                ,MM_1O_RAT
        //                ,PC_1O_RAT
        //                ,TP_PRSTZ_ASSTA
        //                ,DT_END_CARZ
        //                ,PC_RIP_PRE
        //                ,COD_FND
        //                ,AA_REN_CER
        //                ,PC_REVRSB
        //                ,TP_PC_RIP
        //                ,PC_OPZ
        //                ,TP_IAS
        //                ,TP_STAB
        //                ,TP_ADEG_PRE
        //                ,DT_VARZ_TP_IAS
        //                ,FRAZ_DECR_CPT
        //                ,COD_TRAT_RIASS
        //                ,TP_DT_EMIS_RIASS
        //                ,TP_CESS_RIASS
        //                ,DS_RIGA
        //                ,DS_OPER_SQL
        //                ,DS_VER
        //                ,DS_TS_INI_CPTZ
        //                ,DS_TS_END_CPTZ
        //                ,DS_UTENTE
        //                ,DS_STATO_ELAB
        //                ,AA_STAB
        //                ,TS_STAB_LIMITATA
        //                ,DT_PRESC
        //                ,RSH_INVST
        //                ,TP_RAMO_BILA
        //             INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //             FROM GAR
        //             WHERE     IB_OGG = :GRZ-IB-OGG
        //                    AND COD_COMP_ANIA =
        //                        :IDSV0003-CODICE-COMPAGNIA-ANIA
        //                    AND DT_INI_EFF <=
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DT_END_EFF >
        //                        :WS-DATA-INIZIO-EFFETTO-DB
        //                    AND DS_TS_INI_CPTZ <=
        //                         :WS-TS-COMPETENZA
        //                    AND DS_TS_END_CPTZ >
        //                         :WS-TS-COMPETENZA
        //           END-EXEC.
        garDao.selectRec5(gar.getGrzIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza(), this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
    }

    /**Original name: B560-OPEN-CURSOR-IBO-CPZ<br>*/
    private void b560OpenCursorIboCpz() {
        // COB_CODE: PERFORM B505-DECLARE-CURSOR-IBO-CPZ     THRU B505-EX.
        b505DeclareCursorIboCpz();
        // COB_CODE: EXEC SQL
        //                OPEN C-IBO-CPZ-GRZ
        //           END-EXEC.
        garDao.openCIboCpzGrz(gar.getGrzIbOgg(), idsv0003.getCodiceCompagniaAnia(), ws.getIdsv0010().getWsDataInizioEffettoDb(), ws.getIdsv0010().getWsTsCompetenza());
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B570-CLOSE-CURSOR-IBO-CPZ<br>*/
    private void b570CloseCursorIboCpz() {
        // COB_CODE: EXEC SQL
        //                CLOSE C-IBO-CPZ-GRZ
        //           END-EXEC.
        garDao.closeCIboCpzGrz();
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: B580-FETCH-FIRST-IBO-CPZ<br>*/
    private void b580FetchFirstIboCpz() {
        // COB_CODE: PERFORM B560-OPEN-CURSOR-IBO-CPZ        THRU B560-EX.
        b560OpenCursorIboCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B590-FETCH-NEXT-IBO-CPZ     THRU B590-EX
            b590FetchNextIboCpz();
        }
    }

    /**Original name: B590-FETCH-NEXT-IBO-CPZ<br>*/
    private void b590FetchNextIboCpz() {
        // COB_CODE: EXEC SQL
        //                FETCH C-IBO-CPZ-GRZ
        //           INTO
        //                :GRZ-ID-GAR
        //               ,:GRZ-ID-ADES
        //                :IND-GRZ-ID-ADES
        //               ,:GRZ-ID-POLI
        //               ,:GRZ-ID-MOVI-CRZ
        //               ,:GRZ-ID-MOVI-CHIU
        //                :IND-GRZ-ID-MOVI-CHIU
        //               ,:GRZ-DT-INI-EFF-DB
        //               ,:GRZ-DT-END-EFF-DB
        //               ,:GRZ-COD-COMP-ANIA
        //               ,:GRZ-IB-OGG
        //                :IND-GRZ-IB-OGG
        //               ,:GRZ-DT-DECOR-DB
        //                :IND-GRZ-DT-DECOR
        //               ,:GRZ-DT-SCAD-DB
        //                :IND-GRZ-DT-SCAD
        //               ,:GRZ-COD-SEZ
        //                :IND-GRZ-COD-SEZ
        //               ,:GRZ-COD-TARI
        //               ,:GRZ-RAMO-BILA
        //                :IND-GRZ-RAMO-BILA
        //               ,:GRZ-DT-INI-VAL-TAR-DB
        //                :IND-GRZ-DT-INI-VAL-TAR
        //               ,:GRZ-ID-1O-ASSTO
        //                :IND-GRZ-ID-1O-ASSTO
        //               ,:GRZ-ID-2O-ASSTO
        //                :IND-GRZ-ID-2O-ASSTO
        //               ,:GRZ-ID-3O-ASSTO
        //                :IND-GRZ-ID-3O-ASSTO
        //               ,:GRZ-TP-GAR
        //                :IND-GRZ-TP-GAR
        //               ,:GRZ-TP-RSH
        //                :IND-GRZ-TP-RSH
        //               ,:GRZ-TP-INVST
        //                :IND-GRZ-TP-INVST
        //               ,:GRZ-MOD-PAG-GARCOL
        //                :IND-GRZ-MOD-PAG-GARCOL
        //               ,:GRZ-TP-PER-PRE
        //                :IND-GRZ-TP-PER-PRE
        //               ,:GRZ-ETA-AA-1O-ASSTO
        //                :IND-GRZ-ETA-AA-1O-ASSTO
        //               ,:GRZ-ETA-MM-1O-ASSTO
        //                :IND-GRZ-ETA-MM-1O-ASSTO
        //               ,:GRZ-ETA-AA-2O-ASSTO
        //                :IND-GRZ-ETA-AA-2O-ASSTO
        //               ,:GRZ-ETA-MM-2O-ASSTO
        //                :IND-GRZ-ETA-MM-2O-ASSTO
        //               ,:GRZ-ETA-AA-3O-ASSTO
        //                :IND-GRZ-ETA-AA-3O-ASSTO
        //               ,:GRZ-ETA-MM-3O-ASSTO
        //                :IND-GRZ-ETA-MM-3O-ASSTO
        //               ,:GRZ-TP-EMIS-PUR
        //                :IND-GRZ-TP-EMIS-PUR
        //               ,:GRZ-ETA-A-SCAD
        //                :IND-GRZ-ETA-A-SCAD
        //               ,:GRZ-TP-CALC-PRE-PRSTZ
        //                :IND-GRZ-TP-CALC-PRE-PRSTZ
        //               ,:GRZ-TP-PRE
        //                :IND-GRZ-TP-PRE
        //               ,:GRZ-TP-DUR
        //                :IND-GRZ-TP-DUR
        //               ,:GRZ-DUR-AA
        //                :IND-GRZ-DUR-AA
        //               ,:GRZ-DUR-MM
        //                :IND-GRZ-DUR-MM
        //               ,:GRZ-DUR-GG
        //                :IND-GRZ-DUR-GG
        //               ,:GRZ-NUM-AA-PAG-PRE
        //                :IND-GRZ-NUM-AA-PAG-PRE
        //               ,:GRZ-AA-PAG-PRE-UNI
        //                :IND-GRZ-AA-PAG-PRE-UNI
        //               ,:GRZ-MM-PAG-PRE-UNI
        //                :IND-GRZ-MM-PAG-PRE-UNI
        //               ,:GRZ-FRAZ-INI-EROG-REN
        //                :IND-GRZ-FRAZ-INI-EROG-REN
        //               ,:GRZ-MM-1O-RAT
        //                :IND-GRZ-MM-1O-RAT
        //               ,:GRZ-PC-1O-RAT
        //                :IND-GRZ-PC-1O-RAT
        //               ,:GRZ-TP-PRSTZ-ASSTA
        //                :IND-GRZ-TP-PRSTZ-ASSTA
        //               ,:GRZ-DT-END-CARZ-DB
        //                :IND-GRZ-DT-END-CARZ
        //               ,:GRZ-PC-RIP-PRE
        //                :IND-GRZ-PC-RIP-PRE
        //               ,:GRZ-COD-FND
        //                :IND-GRZ-COD-FND
        //               ,:GRZ-AA-REN-CER
        //                :IND-GRZ-AA-REN-CER
        //               ,:GRZ-PC-REVRSB
        //                :IND-GRZ-PC-REVRSB
        //               ,:GRZ-TP-PC-RIP
        //                :IND-GRZ-TP-PC-RIP
        //               ,:GRZ-PC-OPZ
        //                :IND-GRZ-PC-OPZ
        //               ,:GRZ-TP-IAS
        //                :IND-GRZ-TP-IAS
        //               ,:GRZ-TP-STAB
        //                :IND-GRZ-TP-STAB
        //               ,:GRZ-TP-ADEG-PRE
        //                :IND-GRZ-TP-ADEG-PRE
        //               ,:GRZ-DT-VARZ-TP-IAS-DB
        //                :IND-GRZ-DT-VARZ-TP-IAS
        //               ,:GRZ-FRAZ-DECR-CPT
        //                :IND-GRZ-FRAZ-DECR-CPT
        //               ,:GRZ-COD-TRAT-RIASS
        //                :IND-GRZ-COD-TRAT-RIASS
        //               ,:GRZ-TP-DT-EMIS-RIASS
        //                :IND-GRZ-TP-DT-EMIS-RIASS
        //               ,:GRZ-TP-CESS-RIASS
        //                :IND-GRZ-TP-CESS-RIASS
        //               ,:GRZ-DS-RIGA
        //               ,:GRZ-DS-OPER-SQL
        //               ,:GRZ-DS-VER
        //               ,:GRZ-DS-TS-INI-CPTZ
        //               ,:GRZ-DS-TS-END-CPTZ
        //               ,:GRZ-DS-UTENTE
        //               ,:GRZ-DS-STATO-ELAB
        //               ,:GRZ-AA-STAB
        //                :IND-GRZ-AA-STAB
        //               ,:GRZ-TS-STAB-LIMITATA
        //                :IND-GRZ-TS-STAB-LIMITATA
        //               ,:GRZ-DT-PRESC-DB
        //                :IND-GRZ-DT-PRESC
        //               ,:GRZ-RSH-INVST
        //                :IND-GRZ-RSH-INVST
        //               ,:GRZ-TP-RAMO-BILA
        //           END-EXEC.
        garDao.fetchCIboCpzGrz(this);
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
        //           ELSE
        //             END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM Z100-SET-COLONNE-NULL THRU Z100-EX
            z100SetColonneNull();
            // COB_CODE: PERFORM Z950-CONVERTI-X-TO-N  THRU Z950-EX
            z950ConvertiXToN();
        }
        else if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF IDSV0003-NOT-FOUND
            //              END-IF
            //           END-IF
            // COB_CODE: PERFORM B570-CLOSE-CURSOR-IBO-CPZ     THRU B570-EX
            b570CloseCursorIboCpz();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              SET IDSV0003-NOT-FOUND TO TRUE
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: SET IDSV0003-NOT-FOUND TO TRUE
                idsv0003.getSqlcode().setNotFound();
            }
        }
    }

    /**Original name: B605-DECLARE-CURSOR-IBS-CPZ<br>
	 * <pre>----
	 * ----  gestione IBS Competenza
	 * ----</pre>*/
    private void b605DeclareCursorIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B610-SELECT-IBS-CPZ<br>*/
    private void b610SelectIbsCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B660-OPEN-CURSOR-IBS-CPZ<br>*/
    private void b660OpenCursorIbsCpz() {
        // COB_CODE: PERFORM B605-DECLARE-CURSOR-IBS-CPZ     THRU B605-EX.
        b605DeclareCursorIbsCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B670-CLOSE-CURSOR-IBS-CPZ<br>*/
    private void b670CloseCursorIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B680-FETCH-FIRST-IBS-CPZ<br>*/
    private void b680FetchFirstIbsCpz() {
        // COB_CODE: PERFORM B660-OPEN-CURSOR-IBS-CPZ        THRU B660-EX.
        b660OpenCursorIbsCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B690-FETCH-NEXT-IBS-CPZ     THRU B690-EX
            b690FetchNextIbsCpz();
        }
    }

    /**Original name: B690-FETCH-NEXT-IBS-CPZ<br>*/
    private void b690FetchNextIbsCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B705-DECLARE-CURSOR-IDO-CPZ<br>
	 * <pre>----
	 * ----  gestione IDO Competenza
	 * ----</pre>*/
    private void b705DeclareCursorIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B710-SELECT-IDO-CPZ<br>*/
    private void b710SelectIdoCpz() {
        // COB_CODE: PERFORM Z960-LENGTH-VCHAR        THRU Z960-EX.
        z960LengthVchar();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B760-OPEN-CURSOR-IDO-CPZ<br>*/
    private void b760OpenCursorIdoCpz() {
        // COB_CODE: PERFORM B705-DECLARE-CURSOR-IDO-CPZ     THRU B705-EX.
        b705DeclareCursorIdoCpz();
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B770-CLOSE-CURSOR-IDO-CPZ<br>*/
    private void b770CloseCursorIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: B780-FETCH-FIRST-IDO-CPZ<br>*/
    private void b780FetchFirstIdoCpz() {
        // COB_CODE: PERFORM B760-OPEN-CURSOR-IDO-CPZ        THRU B760-EX.
        b760OpenCursorIdoCpz();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
        //              PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
        //           END-IF.
        if (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM B790-FETCH-NEXT-IDO-CPZ     THRU B790-EX
            b790FetchNextIdoCpz();
        }
    }

    /**Original name: B790-FETCH-NEXT-IDO-CPZ<br>*/
    private void b790FetchNextIdoCpz() {
        // COB_CODE: SET IDSV0003-INVALID-OPER TO TRUE.
        idsv0003.getReturnCode().setInvalidOper();
    }

    /**Original name: Z100-SET-COLONNE-NULL<br>*/
    private void z100SetColonneNull() {
        // COB_CODE: MOVE 1 TO IDSV0003-NUM-RIGHE-LETTE.
        idsv0003.getCampiEsito().setNumRigheLette(((short)1));
        // COB_CODE: IF IND-GRZ-ID-ADES = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-ADES-NULL
        //           END-IF
        if (ws.getIndGar().getIdAdes() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-ADES-NULL
            gar.getGrzIdAdes().setGrzIdAdesNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzIdAdes.Len.GRZ_ID_ADES_NULL));
        }
        // COB_CODE: IF IND-GRZ-ID-MOVI-CHIU = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-MOVI-CHIU-NULL
        //           END-IF
        if (ws.getIndGar().getIdMoviChiu() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-MOVI-CHIU-NULL
            gar.getGrzIdMoviChiu().setGrzIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzIdMoviChiu.Len.GRZ_ID_MOVI_CHIU_NULL));
        }
        // COB_CODE: IF IND-GRZ-IB-OGG = -1
        //              MOVE HIGH-VALUES TO GRZ-IB-OGG-NULL
        //           END-IF
        if (ws.getIndGar().getIbOgg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-IB-OGG-NULL
            gar.setGrzIbOgg(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_IB_OGG));
        }
        // COB_CODE: IF IND-GRZ-DT-DECOR = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-DECOR-NULL
        //           END-IF
        if (ws.getIndGar().getDtDecor() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-DECOR-NULL
            gar.getGrzDtDecor().setGrzDtDecorNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtDecor.Len.GRZ_DT_DECOR_NULL));
        }
        // COB_CODE: IF IND-GRZ-DT-SCAD = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-SCAD-NULL
        //           END-IF
        if (ws.getIndGar().getDtScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-SCAD-NULL
            gar.getGrzDtScad().setGrzDtScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtScad.Len.GRZ_DT_SCAD_NULL));
        }
        // COB_CODE: IF IND-GRZ-COD-SEZ = -1
        //              MOVE HIGH-VALUES TO GRZ-COD-SEZ-NULL
        //           END-IF
        if (ws.getIndGar().getCodSez() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-COD-SEZ-NULL
            gar.setGrzCodSez(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_COD_SEZ));
        }
        // COB_CODE: IF IND-GRZ-RAMO-BILA = -1
        //              MOVE HIGH-VALUES TO GRZ-RAMO-BILA-NULL
        //           END-IF
        if (ws.getIndGar().getRamoBila() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-RAMO-BILA-NULL
            gar.setGrzRamoBila(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_RAMO_BILA));
        }
        // COB_CODE: IF IND-GRZ-DT-INI-VAL-TAR = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-INI-VAL-TAR-NULL
        //           END-IF
        if (ws.getIndGar().getDtIniValTar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-INI-VAL-TAR-NULL
            gar.getGrzDtIniValTar().setGrzDtIniValTarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtIniValTar.Len.GRZ_DT_INI_VAL_TAR_NULL));
        }
        // COB_CODE: IF IND-GRZ-ID-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getId1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-1O-ASSTO-NULL
            gar.getGrzId1oAssto().setGrzId1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzId1oAssto.Len.GRZ_ID1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ID-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getId2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-2O-ASSTO-NULL
            gar.getGrzId2oAssto().setGrzId2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzId2oAssto.Len.GRZ_ID2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ID-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ID-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getId3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ID-3O-ASSTO-NULL
            gar.getGrzId3oAssto().setGrzId3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzId3oAssto.Len.GRZ_ID3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-GAR = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-GAR-NULL
        //           END-IF
        if (ws.getIndGar().getTpGar() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-GAR-NULL
            gar.getGrzTpGar().setGrzTpGarNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzTpGar.Len.GRZ_TP_GAR_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-RSH = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-RSH-NULL
        //           END-IF
        if (ws.getIndGar().getTpRsh() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-RSH-NULL
            gar.setGrzTpRsh(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_RSH));
        }
        // COB_CODE: IF IND-GRZ-TP-INVST = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-INVST-NULL
        //           END-IF
        if (ws.getIndGar().getTpInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-INVST-NULL
            gar.getGrzTpInvst().setGrzTpInvstNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzTpInvst.Len.GRZ_TP_INVST_NULL));
        }
        // COB_CODE: IF IND-GRZ-MOD-PAG-GARCOL = -1
        //              MOVE HIGH-VALUES TO GRZ-MOD-PAG-GARCOL-NULL
        //           END-IF
        if (ws.getIndGar().getModPagGarcol() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-MOD-PAG-GARCOL-NULL
            gar.setGrzModPagGarcol(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_MOD_PAG_GARCOL));
        }
        // COB_CODE: IF IND-GRZ-TP-PER-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-PER-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getTpPerPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-PER-PRE-NULL
            gar.setGrzTpPerPre(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_PER_PRE));
        }
        // COB_CODE: IF IND-GRZ-ETA-AA-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-AA-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaAa1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-AA-1O-ASSTO-NULL
            gar.getGrzEtaAa1oAssto().setGrzEtaAa1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaAa1oAssto.Len.GRZ_ETA_AA1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-MM-1O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-MM-1O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaMm1oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-MM-1O-ASSTO-NULL
            gar.getGrzEtaMm1oAssto().setGrzEtaMm1oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaMm1oAssto.Len.GRZ_ETA_MM1O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-AA-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-AA-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaAa2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-AA-2O-ASSTO-NULL
            gar.getGrzEtaAa2oAssto().setGrzEtaAa2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaAa2oAssto.Len.GRZ_ETA_AA2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-MM-2O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-MM-2O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaMm2oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-MM-2O-ASSTO-NULL
            gar.getGrzEtaMm2oAssto().setGrzEtaMm2oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaMm2oAssto.Len.GRZ_ETA_MM2O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-AA-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-AA-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaAa3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-AA-3O-ASSTO-NULL
            gar.getGrzEtaAa3oAssto().setGrzEtaAa3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaAa3oAssto.Len.GRZ_ETA_AA3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-ETA-MM-3O-ASSTO = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-MM-3O-ASSTO-NULL
        //           END-IF
        if (ws.getIndGar().getEtaMm3oAssto() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-MM-3O-ASSTO-NULL
            gar.getGrzEtaMm3oAssto().setGrzEtaMm3oAsstoNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaMm3oAssto.Len.GRZ_ETA_MM3O_ASSTO_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-EMIS-PUR = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-EMIS-PUR-NULL
        //           END-IF
        if (ws.getIndGar().getTpEmisPur() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-EMIS-PUR-NULL
            gar.setGrzTpEmisPur(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-GRZ-ETA-A-SCAD = -1
        //              MOVE HIGH-VALUES TO GRZ-ETA-A-SCAD-NULL
        //           END-IF
        if (ws.getIndGar().getEtaAScad() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-ETA-A-SCAD-NULL
            gar.getGrzEtaAScad().setGrzEtaAScadNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzEtaAScad.Len.GRZ_ETA_A_SCAD_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-CALC-PRE-PRSTZ = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-CALC-PRE-PRSTZ-NULL
        //           END-IF
        if (ws.getIndGar().getTpCalcPrePrstz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-CALC-PRE-PRSTZ-NULL
            gar.setGrzTpCalcPrePrstz(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_CALC_PRE_PRSTZ));
        }
        // COB_CODE: IF IND-GRZ-TP-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getTpPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-PRE-NULL
            gar.setGrzTpPre(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-GRZ-TP-DUR = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-DUR-NULL
        //           END-IF
        if (ws.getIndGar().getTpDur() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-DUR-NULL
            gar.setGrzTpDur(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_DUR));
        }
        // COB_CODE: IF IND-GRZ-DUR-AA = -1
        //              MOVE HIGH-VALUES TO GRZ-DUR-AA-NULL
        //           END-IF
        if (ws.getIndGar().getDurAa() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DUR-AA-NULL
            gar.getGrzDurAa().setGrzDurAaNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDurAa.Len.GRZ_DUR_AA_NULL));
        }
        // COB_CODE: IF IND-GRZ-DUR-MM = -1
        //              MOVE HIGH-VALUES TO GRZ-DUR-MM-NULL
        //           END-IF
        if (ws.getIndGar().getDurMm() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DUR-MM-NULL
            gar.getGrzDurMm().setGrzDurMmNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDurMm.Len.GRZ_DUR_MM_NULL));
        }
        // COB_CODE: IF IND-GRZ-DUR-GG = -1
        //              MOVE HIGH-VALUES TO GRZ-DUR-GG-NULL
        //           END-IF
        if (ws.getIndGar().getDurGg() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DUR-GG-NULL
            gar.getGrzDurGg().setGrzDurGgNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDurGg.Len.GRZ_DUR_GG_NULL));
        }
        // COB_CODE: IF IND-GRZ-NUM-AA-PAG-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-NUM-AA-PAG-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getNumAaPagPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-NUM-AA-PAG-PRE-NULL
            gar.getGrzNumAaPagPre().setGrzNumAaPagPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzNumAaPagPre.Len.GRZ_NUM_AA_PAG_PRE_NULL));
        }
        // COB_CODE: IF IND-GRZ-AA-PAG-PRE-UNI = -1
        //              MOVE HIGH-VALUES TO GRZ-AA-PAG-PRE-UNI-NULL
        //           END-IF
        if (ws.getIndGar().getAaPagPreUni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-AA-PAG-PRE-UNI-NULL
            gar.getGrzAaPagPreUni().setGrzAaPagPreUniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzAaPagPreUni.Len.GRZ_AA_PAG_PRE_UNI_NULL));
        }
        // COB_CODE: IF IND-GRZ-MM-PAG-PRE-UNI = -1
        //              MOVE HIGH-VALUES TO GRZ-MM-PAG-PRE-UNI-NULL
        //           END-IF
        if (ws.getIndGar().getMmPagPreUni() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-MM-PAG-PRE-UNI-NULL
            gar.getGrzMmPagPreUni().setGrzMmPagPreUniNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzMmPagPreUni.Len.GRZ_MM_PAG_PRE_UNI_NULL));
        }
        // COB_CODE: IF IND-GRZ-FRAZ-INI-EROG-REN = -1
        //              MOVE HIGH-VALUES TO GRZ-FRAZ-INI-EROG-REN-NULL
        //           END-IF
        if (ws.getIndGar().getFrazIniErogRen() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-FRAZ-INI-EROG-REN-NULL
            gar.getGrzFrazIniErogRen().setGrzFrazIniErogRenNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzFrazIniErogRen.Len.GRZ_FRAZ_INI_EROG_REN_NULL));
        }
        // COB_CODE: IF IND-GRZ-MM-1O-RAT = -1
        //              MOVE HIGH-VALUES TO GRZ-MM-1O-RAT-NULL
        //           END-IF
        if (ws.getIndGar().getMm1oRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-MM-1O-RAT-NULL
            gar.getGrzMm1oRat().setGrzMm1oRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzMm1oRat.Len.GRZ_MM1O_RAT_NULL));
        }
        // COB_CODE: IF IND-GRZ-PC-1O-RAT = -1
        //              MOVE HIGH-VALUES TO GRZ-PC-1O-RAT-NULL
        //           END-IF
        if (ws.getIndGar().getPc1oRat() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-PC-1O-RAT-NULL
            gar.getGrzPc1oRat().setGrzPc1oRatNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzPc1oRat.Len.GRZ_PC1O_RAT_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-PRSTZ-ASSTA = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-PRSTZ-ASSTA-NULL
        //           END-IF
        if (ws.getIndGar().getTpPrstzAssta() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-PRSTZ-ASSTA-NULL
            gar.setGrzTpPrstzAssta(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_PRSTZ_ASSTA));
        }
        // COB_CODE: IF IND-GRZ-DT-END-CARZ = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-END-CARZ-NULL
        //           END-IF
        if (ws.getIndGar().getDtEndCarz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-END-CARZ-NULL
            gar.getGrzDtEndCarz().setGrzDtEndCarzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtEndCarz.Len.GRZ_DT_END_CARZ_NULL));
        }
        // COB_CODE: IF IND-GRZ-PC-RIP-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-PC-RIP-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getPcRipPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-PC-RIP-PRE-NULL
            gar.getGrzPcRipPre().setGrzPcRipPreNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzPcRipPre.Len.GRZ_PC_RIP_PRE_NULL));
        }
        // COB_CODE: IF IND-GRZ-COD-FND = -1
        //              MOVE HIGH-VALUES TO GRZ-COD-FND-NULL
        //           END-IF
        if (ws.getIndGar().getCodFnd() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-COD-FND-NULL
            gar.setGrzCodFnd(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_COD_FND));
        }
        // COB_CODE: IF IND-GRZ-AA-REN-CER = -1
        //              MOVE HIGH-VALUES TO GRZ-AA-REN-CER-NULL
        //           END-IF
        if (ws.getIndGar().getAaRenCer() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-AA-REN-CER-NULL
            gar.setGrzAaRenCer(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_AA_REN_CER));
        }
        // COB_CODE: IF IND-GRZ-PC-REVRSB = -1
        //              MOVE HIGH-VALUES TO GRZ-PC-REVRSB-NULL
        //           END-IF
        if (ws.getIndGar().getPcRevrsb() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-PC-REVRSB-NULL
            gar.getGrzPcRevrsb().setGrzPcRevrsbNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzPcRevrsb.Len.GRZ_PC_REVRSB_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-PC-RIP = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-PC-RIP-NULL
        //           END-IF
        if (ws.getIndGar().getTpPcRip() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-PC-RIP-NULL
            gar.setGrzTpPcRip(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_PC_RIP));
        }
        // COB_CODE: IF IND-GRZ-PC-OPZ = -1
        //              MOVE HIGH-VALUES TO GRZ-PC-OPZ-NULL
        //           END-IF
        if (ws.getIndGar().getPcOpz() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-PC-OPZ-NULL
            gar.getGrzPcOpz().setGrzPcOpzNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzPcOpz.Len.GRZ_PC_OPZ_NULL));
        }
        // COB_CODE: IF IND-GRZ-TP-IAS = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-IAS-NULL
        //           END-IF
        if (ws.getIndGar().getTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-IAS-NULL
            gar.setGrzTpIas(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_IAS));
        }
        // COB_CODE: IF IND-GRZ-TP-STAB = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-STAB-NULL
        //           END-IF
        if (ws.getIndGar().getTpStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-STAB-NULL
            gar.setGrzTpStab(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_STAB));
        }
        // COB_CODE: IF IND-GRZ-TP-ADEG-PRE = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-ADEG-PRE-NULL
        //           END-IF
        if (ws.getIndGar().getTpAdegPre() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-ADEG-PRE-NULL
            gar.setGrzTpAdegPre(Types.HIGH_CHAR_VAL);
        }
        // COB_CODE: IF IND-GRZ-DT-VARZ-TP-IAS = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-VARZ-TP-IAS-NULL
        //           END-IF
        if (ws.getIndGar().getDtVarzTpIas() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-VARZ-TP-IAS-NULL
            gar.getGrzDtVarzTpIas().setGrzDtVarzTpIasNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtVarzTpIas.Len.GRZ_DT_VARZ_TP_IAS_NULL));
        }
        // COB_CODE: IF IND-GRZ-FRAZ-DECR-CPT = -1
        //              MOVE HIGH-VALUES TO GRZ-FRAZ-DECR-CPT-NULL
        //           END-IF
        if (ws.getIndGar().getFrazDecrCpt() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-FRAZ-DECR-CPT-NULL
            gar.getGrzFrazDecrCpt().setGrzFrazDecrCptNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzFrazDecrCpt.Len.GRZ_FRAZ_DECR_CPT_NULL));
        }
        // COB_CODE: IF IND-GRZ-COD-TRAT-RIASS = -1
        //              MOVE HIGH-VALUES TO GRZ-COD-TRAT-RIASS-NULL
        //           END-IF
        if (ws.getIndGar().getCodTratRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-COD-TRAT-RIASS-NULL
            gar.setGrzCodTratRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_COD_TRAT_RIASS));
        }
        // COB_CODE: IF IND-GRZ-TP-DT-EMIS-RIASS = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-DT-EMIS-RIASS-NULL
        //           END-IF
        if (ws.getIndGar().getTpDtEmisRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-DT-EMIS-RIASS-NULL
            gar.setGrzTpDtEmisRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_DT_EMIS_RIASS));
        }
        // COB_CODE: IF IND-GRZ-TP-CESS-RIASS = -1
        //              MOVE HIGH-VALUES TO GRZ-TP-CESS-RIASS-NULL
        //           END-IF
        if (ws.getIndGar().getTpCessRiass() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TP-CESS-RIASS-NULL
            gar.setGrzTpCessRiass(LiteralGenerator.create(Types.HIGH_CHAR_VAL, Gar.Len.GRZ_TP_CESS_RIASS));
        }
        // COB_CODE: IF IND-GRZ-AA-STAB = -1
        //              MOVE HIGH-VALUES TO GRZ-AA-STAB-NULL
        //           END-IF
        if (ws.getIndGar().getAaStab() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-AA-STAB-NULL
            gar.getGrzAaStab().setGrzAaStabNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzAaStab.Len.GRZ_AA_STAB_NULL));
        }
        // COB_CODE: IF IND-GRZ-TS-STAB-LIMITATA = -1
        //              MOVE HIGH-VALUES TO GRZ-TS-STAB-LIMITATA-NULL
        //           END-IF
        if (ws.getIndGar().getTsStabLimitata() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-TS-STAB-LIMITATA-NULL
            gar.getGrzTsStabLimitata().setGrzTsStabLimitataNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzTsStabLimitata.Len.GRZ_TS_STAB_LIMITATA_NULL));
        }
        // COB_CODE: IF IND-GRZ-DT-PRESC = -1
        //              MOVE HIGH-VALUES TO GRZ-DT-PRESC-NULL
        //           END-IF
        if (ws.getIndGar().getDtPresc() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-DT-PRESC-NULL
            gar.getGrzDtPresc().setGrzDtPrescNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzDtPresc.Len.GRZ_DT_PRESC_NULL));
        }
        // COB_CODE: IF IND-GRZ-RSH-INVST = -1
        //              MOVE HIGH-VALUES TO GRZ-RSH-INVST-NULL
        //           END-IF.
        if (ws.getIndGar().getRshInvst() == -1) {
            // COB_CODE: MOVE HIGH-VALUES TO GRZ-RSH-INVST-NULL
            gar.setGrzRshInvst(Types.HIGH_CHAR_VAL);
        }
    }

    /**Original name: Z150-VALORIZZA-DATA-SERVICES-I<br>*/
    private void z150ValorizzaDataServicesI() {
        // COB_CODE: MOVE 'I' TO GRZ-DS-OPER-SQL
        gar.setGrzDsOperSqlFormatted("I");
        // COB_CODE: MOVE 0                   TO GRZ-DS-VER
        gar.setGrzDsVer(0);
        // COB_CODE: MOVE IDSV0003-USER-NAME TO GRZ-DS-UTENTE
        gar.setGrzDsUtente(idsv0003.getUserName());
        // COB_CODE: MOVE '1'                   TO GRZ-DS-STATO-ELAB.
        gar.setGrzDsStatoElabFormatted("1");
    }

    /**Original name: Z160-VALORIZZA-DATA-SERVICES-U<br>*/
    private void z160ValorizzaDataServicesU() {
        // COB_CODE: MOVE 'U' TO GRZ-DS-OPER-SQL
        gar.setGrzDsOperSqlFormatted("U");
        // COB_CODE: MOVE IDSV0003-USER-NAME TO GRZ-DS-UTENTE.
        gar.setGrzDsUtente(idsv0003.getUserName());
    }

    /**Original name: Z200-SET-INDICATORI-NULL<br>*/
    private void z200SetIndicatoriNull() {
        // COB_CODE: IF GRZ-ID-ADES-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ID-ADES
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ID-ADES
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzIdAdes().getGrzIdAdesNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ID-ADES
            ws.getIndGar().setIdAdes(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ID-ADES
            ws.getIndGar().setIdAdes(((short)0));
        }
        // COB_CODE: IF GRZ-ID-MOVI-CHIU-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ID-MOVI-CHIU
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ID-MOVI-CHIU
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzIdMoviChiu().getGrzIdMoviChiuNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ID-MOVI-CHIU
            ws.getIndGar().setIdMoviChiu(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ID-MOVI-CHIU
            ws.getIndGar().setIdMoviChiu(((short)0));
        }
        // COB_CODE: IF GRZ-IB-OGG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-IB-OGG
        //           ELSE
        //              MOVE 0 TO IND-GRZ-IB-OGG
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzIbOgg(), Gar.Len.GRZ_IB_OGG)) {
            // COB_CODE: MOVE -1 TO IND-GRZ-IB-OGG
            ws.getIndGar().setIbOgg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-IB-OGG
            ws.getIndGar().setIbOgg(((short)0));
        }
        // COB_CODE: IF GRZ-DT-DECOR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-DT-DECOR
        //           ELSE
        //              MOVE 0 TO IND-GRZ-DT-DECOR
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzDtDecor().getGrzDtDecorNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-DT-DECOR
            ws.getIndGar().setDtDecor(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-DT-DECOR
            ws.getIndGar().setDtDecor(((short)0));
        }
        // COB_CODE: IF GRZ-DT-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-DT-SCAD
        //           ELSE
        //              MOVE 0 TO IND-GRZ-DT-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzDtScad().getGrzDtScadNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-DT-SCAD
            ws.getIndGar().setDtScad(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-DT-SCAD
            ws.getIndGar().setDtScad(((short)0));
        }
        // COB_CODE: IF GRZ-COD-SEZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-COD-SEZ
        //           ELSE
        //              MOVE 0 TO IND-GRZ-COD-SEZ
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzCodSezFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-COD-SEZ
            ws.getIndGar().setCodSez(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-COD-SEZ
            ws.getIndGar().setCodSez(((short)0));
        }
        // COB_CODE: IF GRZ-RAMO-BILA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-RAMO-BILA
        //           ELSE
        //              MOVE 0 TO IND-GRZ-RAMO-BILA
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzRamoBilaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-RAMO-BILA
            ws.getIndGar().setRamoBila(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-RAMO-BILA
            ws.getIndGar().setRamoBila(((short)0));
        }
        // COB_CODE: IF GRZ-DT-INI-VAL-TAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-DT-INI-VAL-TAR
        //           ELSE
        //              MOVE 0 TO IND-GRZ-DT-INI-VAL-TAR
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzDtIniValTar().getGrzDtIniValTarNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-DT-INI-VAL-TAR
            ws.getIndGar().setDtIniValTar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-DT-INI-VAL-TAR
            ws.getIndGar().setDtIniValTar(((short)0));
        }
        // COB_CODE: IF GRZ-ID-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ID-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ID-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzId1oAssto().getGrzId1oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ID-1O-ASSTO
            ws.getIndGar().setId1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ID-1O-ASSTO
            ws.getIndGar().setId1oAssto(((short)0));
        }
        // COB_CODE: IF GRZ-ID-2O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ID-2O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ID-2O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzId2oAssto().getGrzId2oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ID-2O-ASSTO
            ws.getIndGar().setId2oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ID-2O-ASSTO
            ws.getIndGar().setId2oAssto(((short)0));
        }
        // COB_CODE: IF GRZ-ID-3O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ID-3O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ID-3O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzId3oAssto().getGrzId3oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ID-3O-ASSTO
            ws.getIndGar().setId3oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ID-3O-ASSTO
            ws.getIndGar().setId3oAssto(((short)0));
        }
        // COB_CODE: IF GRZ-TP-GAR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-GAR
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-GAR
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpGar().getGrzTpGarNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-GAR
            ws.getIndGar().setTpGar(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-GAR
            ws.getIndGar().setTpGar(((short)0));
        }
        // COB_CODE: IF GRZ-TP-RSH-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-RSH
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-RSH
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpRshFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-RSH
            ws.getIndGar().setTpRsh(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-RSH
            ws.getIndGar().setTpRsh(((short)0));
        }
        // COB_CODE: IF GRZ-TP-INVST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-INVST
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-INVST
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpInvst().getGrzTpInvstNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-INVST
            ws.getIndGar().setTpInvst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-INVST
            ws.getIndGar().setTpInvst(((short)0));
        }
        // COB_CODE: IF GRZ-MOD-PAG-GARCOL-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-MOD-PAG-GARCOL
        //           ELSE
        //              MOVE 0 TO IND-GRZ-MOD-PAG-GARCOL
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzModPagGarcolFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-MOD-PAG-GARCOL
            ws.getIndGar().setModPagGarcol(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-MOD-PAG-GARCOL
            ws.getIndGar().setModPagGarcol(((short)0));
        }
        // COB_CODE: IF GRZ-TP-PER-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-PER-PRE
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-PER-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpPerPreFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-PER-PRE
            ws.getIndGar().setTpPerPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-PER-PRE
            ws.getIndGar().setTpPerPre(((short)0));
        }
        // COB_CODE: IF GRZ-ETA-AA-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ETA-AA-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ETA-AA-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzEtaAa1oAssto().getGrzEtaAa1oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ETA-AA-1O-ASSTO
            ws.getIndGar().setEtaAa1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ETA-AA-1O-ASSTO
            ws.getIndGar().setEtaAa1oAssto(((short)0));
        }
        // COB_CODE: IF GRZ-ETA-MM-1O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ETA-MM-1O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ETA-MM-1O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzEtaMm1oAssto().getGrzEtaMm1oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ETA-MM-1O-ASSTO
            ws.getIndGar().setEtaMm1oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ETA-MM-1O-ASSTO
            ws.getIndGar().setEtaMm1oAssto(((short)0));
        }
        // COB_CODE: IF GRZ-ETA-AA-2O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ETA-AA-2O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ETA-AA-2O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzEtaAa2oAssto().getGrzEtaAa2oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ETA-AA-2O-ASSTO
            ws.getIndGar().setEtaAa2oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ETA-AA-2O-ASSTO
            ws.getIndGar().setEtaAa2oAssto(((short)0));
        }
        // COB_CODE: IF GRZ-ETA-MM-2O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ETA-MM-2O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ETA-MM-2O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzEtaMm2oAssto().getGrzEtaMm2oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ETA-MM-2O-ASSTO
            ws.getIndGar().setEtaMm2oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ETA-MM-2O-ASSTO
            ws.getIndGar().setEtaMm2oAssto(((short)0));
        }
        // COB_CODE: IF GRZ-ETA-AA-3O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ETA-AA-3O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ETA-AA-3O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzEtaAa3oAssto().getGrzEtaAa3oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ETA-AA-3O-ASSTO
            ws.getIndGar().setEtaAa3oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ETA-AA-3O-ASSTO
            ws.getIndGar().setEtaAa3oAssto(((short)0));
        }
        // COB_CODE: IF GRZ-ETA-MM-3O-ASSTO-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ETA-MM-3O-ASSTO
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ETA-MM-3O-ASSTO
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzEtaMm3oAssto().getGrzEtaMm3oAsstoNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ETA-MM-3O-ASSTO
            ws.getIndGar().setEtaMm3oAssto(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ETA-MM-3O-ASSTO
            ws.getIndGar().setEtaMm3oAssto(((short)0));
        }
        // COB_CODE: IF GRZ-TP-EMIS-PUR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-EMIS-PUR
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-EMIS-PUR
        //           END-IF
        if (Conditions.eq(gar.getGrzTpEmisPur(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-EMIS-PUR
            ws.getIndGar().setTpEmisPur(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-EMIS-PUR
            ws.getIndGar().setTpEmisPur(((short)0));
        }
        // COB_CODE: IF GRZ-ETA-A-SCAD-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-ETA-A-SCAD
        //           ELSE
        //              MOVE 0 TO IND-GRZ-ETA-A-SCAD
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzEtaAScad().getGrzEtaAScadNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-ETA-A-SCAD
            ws.getIndGar().setEtaAScad(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-ETA-A-SCAD
            ws.getIndGar().setEtaAScad(((short)0));
        }
        // COB_CODE: IF GRZ-TP-CALC-PRE-PRSTZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-CALC-PRE-PRSTZ
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-CALC-PRE-PRSTZ
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpCalcPrePrstzFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-CALC-PRE-PRSTZ
            ws.getIndGar().setTpCalcPrePrstz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-CALC-PRE-PRSTZ
            ws.getIndGar().setTpCalcPrePrstz(((short)0));
        }
        // COB_CODE: IF GRZ-TP-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-PRE
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-PRE
        //           END-IF
        if (Conditions.eq(gar.getGrzTpPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-PRE
            ws.getIndGar().setTpPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-PRE
            ws.getIndGar().setTpPre(((short)0));
        }
        // COB_CODE: IF GRZ-TP-DUR-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-DUR
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-DUR
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpDurFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-DUR
            ws.getIndGar().setTpDur(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-DUR
            ws.getIndGar().setTpDur(((short)0));
        }
        // COB_CODE: IF GRZ-DUR-AA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-DUR-AA
        //           ELSE
        //              MOVE 0 TO IND-GRZ-DUR-AA
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzDurAa().getGrzDurAaNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-DUR-AA
            ws.getIndGar().setDurAa(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-DUR-AA
            ws.getIndGar().setDurAa(((short)0));
        }
        // COB_CODE: IF GRZ-DUR-MM-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-DUR-MM
        //           ELSE
        //              MOVE 0 TO IND-GRZ-DUR-MM
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzDurMm().getGrzDurMmNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-DUR-MM
            ws.getIndGar().setDurMm(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-DUR-MM
            ws.getIndGar().setDurMm(((short)0));
        }
        // COB_CODE: IF GRZ-DUR-GG-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-DUR-GG
        //           ELSE
        //              MOVE 0 TO IND-GRZ-DUR-GG
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzDurGg().getGrzDurGgNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-DUR-GG
            ws.getIndGar().setDurGg(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-DUR-GG
            ws.getIndGar().setDurGg(((short)0));
        }
        // COB_CODE: IF GRZ-NUM-AA-PAG-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-NUM-AA-PAG-PRE
        //           ELSE
        //              MOVE 0 TO IND-GRZ-NUM-AA-PAG-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzNumAaPagPre().getGrzNumAaPagPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-NUM-AA-PAG-PRE
            ws.getIndGar().setNumAaPagPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-NUM-AA-PAG-PRE
            ws.getIndGar().setNumAaPagPre(((short)0));
        }
        // COB_CODE: IF GRZ-AA-PAG-PRE-UNI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-AA-PAG-PRE-UNI
        //           ELSE
        //              MOVE 0 TO IND-GRZ-AA-PAG-PRE-UNI
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzAaPagPreUni().getGrzAaPagPreUniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-AA-PAG-PRE-UNI
            ws.getIndGar().setAaPagPreUni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-AA-PAG-PRE-UNI
            ws.getIndGar().setAaPagPreUni(((short)0));
        }
        // COB_CODE: IF GRZ-MM-PAG-PRE-UNI-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-MM-PAG-PRE-UNI
        //           ELSE
        //              MOVE 0 TO IND-GRZ-MM-PAG-PRE-UNI
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzMmPagPreUni().getGrzMmPagPreUniNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-MM-PAG-PRE-UNI
            ws.getIndGar().setMmPagPreUni(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-MM-PAG-PRE-UNI
            ws.getIndGar().setMmPagPreUni(((short)0));
        }
        // COB_CODE: IF GRZ-FRAZ-INI-EROG-REN-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-FRAZ-INI-EROG-REN
        //           ELSE
        //              MOVE 0 TO IND-GRZ-FRAZ-INI-EROG-REN
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzFrazIniErogRen().getGrzFrazIniErogRenNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-FRAZ-INI-EROG-REN
            ws.getIndGar().setFrazIniErogRen(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-FRAZ-INI-EROG-REN
            ws.getIndGar().setFrazIniErogRen(((short)0));
        }
        // COB_CODE: IF GRZ-MM-1O-RAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-MM-1O-RAT
        //           ELSE
        //              MOVE 0 TO IND-GRZ-MM-1O-RAT
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzMm1oRat().getGrzMm1oRatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-MM-1O-RAT
            ws.getIndGar().setMm1oRat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-MM-1O-RAT
            ws.getIndGar().setMm1oRat(((short)0));
        }
        // COB_CODE: IF GRZ-PC-1O-RAT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-PC-1O-RAT
        //           ELSE
        //              MOVE 0 TO IND-GRZ-PC-1O-RAT
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzPc1oRat().getGrzPc1oRatNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-PC-1O-RAT
            ws.getIndGar().setPc1oRat(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-PC-1O-RAT
            ws.getIndGar().setPc1oRat(((short)0));
        }
        // COB_CODE: IF GRZ-TP-PRSTZ-ASSTA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-PRSTZ-ASSTA
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-PRSTZ-ASSTA
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpPrstzAsstaFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-PRSTZ-ASSTA
            ws.getIndGar().setTpPrstzAssta(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-PRSTZ-ASSTA
            ws.getIndGar().setTpPrstzAssta(((short)0));
        }
        // COB_CODE: IF GRZ-DT-END-CARZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-DT-END-CARZ
        //           ELSE
        //              MOVE 0 TO IND-GRZ-DT-END-CARZ
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzDtEndCarz().getGrzDtEndCarzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-DT-END-CARZ
            ws.getIndGar().setDtEndCarz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-DT-END-CARZ
            ws.getIndGar().setDtEndCarz(((short)0));
        }
        // COB_CODE: IF GRZ-PC-RIP-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-PC-RIP-PRE
        //           ELSE
        //              MOVE 0 TO IND-GRZ-PC-RIP-PRE
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzPcRipPre().getGrzPcRipPreNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-PC-RIP-PRE
            ws.getIndGar().setPcRipPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-PC-RIP-PRE
            ws.getIndGar().setPcRipPre(((short)0));
        }
        // COB_CODE: IF GRZ-COD-FND-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-COD-FND
        //           ELSE
        //              MOVE 0 TO IND-GRZ-COD-FND
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzCodFndFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-COD-FND
            ws.getIndGar().setCodFnd(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-COD-FND
            ws.getIndGar().setCodFnd(((short)0));
        }
        // COB_CODE: IF GRZ-AA-REN-CER-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-AA-REN-CER
        //           ELSE
        //              MOVE 0 TO IND-GRZ-AA-REN-CER
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzAaRenCerFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-AA-REN-CER
            ws.getIndGar().setAaRenCer(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-AA-REN-CER
            ws.getIndGar().setAaRenCer(((short)0));
        }
        // COB_CODE: IF GRZ-PC-REVRSB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-PC-REVRSB
        //           ELSE
        //              MOVE 0 TO IND-GRZ-PC-REVRSB
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzPcRevrsb().getGrzPcRevrsbNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-PC-REVRSB
            ws.getIndGar().setPcRevrsb(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-PC-REVRSB
            ws.getIndGar().setPcRevrsb(((short)0));
        }
        // COB_CODE: IF GRZ-TP-PC-RIP-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-PC-RIP
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-PC-RIP
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpPcRipFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-PC-RIP
            ws.getIndGar().setTpPcRip(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-PC-RIP
            ws.getIndGar().setTpPcRip(((short)0));
        }
        // COB_CODE: IF GRZ-PC-OPZ-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-PC-OPZ
        //           ELSE
        //              MOVE 0 TO IND-GRZ-PC-OPZ
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzPcOpz().getGrzPcOpzNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-PC-OPZ
            ws.getIndGar().setPcOpz(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-PC-OPZ
            ws.getIndGar().setPcOpz(((short)0));
        }
        // COB_CODE: IF GRZ-TP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-IAS
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpIasFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-IAS
            ws.getIndGar().setTpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-IAS
            ws.getIndGar().setTpIas(((short)0));
        }
        // COB_CODE: IF GRZ-TP-STAB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-STAB
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-STAB
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpStabFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-STAB
            ws.getIndGar().setTpStab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-STAB
            ws.getIndGar().setTpStab(((short)0));
        }
        // COB_CODE: IF GRZ-TP-ADEG-PRE-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-ADEG-PRE
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-ADEG-PRE
        //           END-IF
        if (Conditions.eq(gar.getGrzTpAdegPre(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-ADEG-PRE
            ws.getIndGar().setTpAdegPre(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-ADEG-PRE
            ws.getIndGar().setTpAdegPre(((short)0));
        }
        // COB_CODE: IF GRZ-DT-VARZ-TP-IAS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-DT-VARZ-TP-IAS
        //           ELSE
        //              MOVE 0 TO IND-GRZ-DT-VARZ-TP-IAS
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzDtVarzTpIas().getGrzDtVarzTpIasNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-DT-VARZ-TP-IAS
            ws.getIndGar().setDtVarzTpIas(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-DT-VARZ-TP-IAS
            ws.getIndGar().setDtVarzTpIas(((short)0));
        }
        // COB_CODE: IF GRZ-FRAZ-DECR-CPT-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-FRAZ-DECR-CPT
        //           ELSE
        //              MOVE 0 TO IND-GRZ-FRAZ-DECR-CPT
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzFrazDecrCpt().getGrzFrazDecrCptNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-FRAZ-DECR-CPT
            ws.getIndGar().setFrazDecrCpt(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-FRAZ-DECR-CPT
            ws.getIndGar().setFrazDecrCpt(((short)0));
        }
        // COB_CODE: IF GRZ-COD-TRAT-RIASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-COD-TRAT-RIASS
        //           ELSE
        //              MOVE 0 TO IND-GRZ-COD-TRAT-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzCodTratRiassFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-COD-TRAT-RIASS
            ws.getIndGar().setCodTratRiass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-COD-TRAT-RIASS
            ws.getIndGar().setCodTratRiass(((short)0));
        }
        // COB_CODE: IF GRZ-TP-DT-EMIS-RIASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-DT-EMIS-RIASS
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-DT-EMIS-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpDtEmisRiassFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-DT-EMIS-RIASS
            ws.getIndGar().setTpDtEmisRiass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-DT-EMIS-RIASS
            ws.getIndGar().setTpDtEmisRiass(((short)0));
        }
        // COB_CODE: IF GRZ-TP-CESS-RIASS-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TP-CESS-RIASS
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TP-CESS-RIASS
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTpCessRiassFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TP-CESS-RIASS
            ws.getIndGar().setTpCessRiass(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TP-CESS-RIASS
            ws.getIndGar().setTpCessRiass(((short)0));
        }
        // COB_CODE: IF GRZ-AA-STAB-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-AA-STAB
        //           ELSE
        //              MOVE 0 TO IND-GRZ-AA-STAB
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzAaStab().getGrzAaStabNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-AA-STAB
            ws.getIndGar().setAaStab(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-AA-STAB
            ws.getIndGar().setAaStab(((short)0));
        }
        // COB_CODE: IF GRZ-TS-STAB-LIMITATA-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-TS-STAB-LIMITATA
        //           ELSE
        //              MOVE 0 TO IND-GRZ-TS-STAB-LIMITATA
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzTsStabLimitata().getGrzTsStabLimitataNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-TS-STAB-LIMITATA
            ws.getIndGar().setTsStabLimitata(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-TS-STAB-LIMITATA
            ws.getIndGar().setTsStabLimitata(((short)0));
        }
        // COB_CODE: IF GRZ-DT-PRESC-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-DT-PRESC
        //           ELSE
        //              MOVE 0 TO IND-GRZ-DT-PRESC
        //           END-IF
        if (Characters.EQ_HIGH.test(gar.getGrzDtPresc().getGrzDtPrescNullFormatted())) {
            // COB_CODE: MOVE -1 TO IND-GRZ-DT-PRESC
            ws.getIndGar().setDtPresc(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-DT-PRESC
            ws.getIndGar().setDtPresc(((short)0));
        }
        // COB_CODE: IF GRZ-RSH-INVST-NULL = HIGH-VALUES
        //              MOVE -1 TO IND-GRZ-RSH-INVST
        //           ELSE
        //              MOVE 0 TO IND-GRZ-RSH-INVST
        //           END-IF.
        if (Conditions.eq(gar.getGrzRshInvst(), Types.HIGH_CHAR_VAL)) {
            // COB_CODE: MOVE -1 TO IND-GRZ-RSH-INVST
            ws.getIndGar().setRshInvst(((short)-1));
        }
        else {
            // COB_CODE: MOVE 0 TO IND-GRZ-RSH-INVST
            ws.getIndGar().setRshInvst(((short)0));
        }
    }

    /**Original name: Z400-SEQ-RIGA<br>*/
    private void z400SeqRiga() {
        // COB_CODE: EXEC SQL
        //              VALUES NEXTVAL FOR SEQ_RIGA
        //              INTO : GRZ-DS-RIGA
        //           END-EXEC.
        //TODO: Implement: valuesStmt;
        // COB_CODE: PERFORM A100-CHECK-RETURN-CODE THRU A100-EX.
        a100CheckReturnCode();
    }

    /**Original name: Z500-AGGIORNAMENTO-STORICO<br>*/
    private void z500AggiornamentoStorico() {
        // COB_CODE: MOVE GAR TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(gar.getGarFormatted());
        // COB_CODE: MOVE GRZ-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(gar.getGrzIdMoviCrz(), 9));
        // COB_CODE: PERFORM A360-OPEN-CURSOR-ID-EFF THRU A360-EX.
        a360OpenCursorIdEff();
        // COB_CODE: PERFORM UNTIL NOT IDSV0003-SUCCESSFUL-SQL
        //              END-IF
        //           END-PERFORM.
        while (idsv0003.getSqlcode().isSuccessfulSql()) {
            // COB_CODE: PERFORM A390-FETCH-NEXT-ID-EFF THRU A390-EX
            a390FetchNextIdEff();
            // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
            //              END-IF
            //           END-IF
            if (idsv0003.getSqlcode().isSuccessfulSql()) {
                // COB_CODE: MOVE WS-ID-MOVI-CRZ   TO GRZ-ID-MOVI-CHIU
                gar.getGrzIdMoviChiu().setGrzIdMoviChiu(ws.getWsIdMoviCrz());
                // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                //                                 TO GRZ-DS-TS-END-CPTZ
                gar.setGrzDsTsEndCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                // COB_CODE: PERFORM A330-UPDATE-ID-EFF THRU A330-EX
                a330UpdateIdEff();
                // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                //                 END-IF
                //           END-IF
                if (idsv0003.getSqlcode().isSuccessfulSql()) {
                    // COB_CODE: MOVE WS-ID-MOVI-CRZ TO GRZ-ID-MOVI-CRZ
                    gar.setGrzIdMoviCrz(ws.getWsIdMoviCrz());
                    // COB_CODE: MOVE HIGH-VALUES    TO GRZ-ID-MOVI-CHIU-NULL
                    gar.getGrzIdMoviChiu().setGrzIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzIdMoviChiu.Len.GRZ_ID_MOVI_CHIU_NULL));
                    // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
                    //                               TO GRZ-DT-END-EFF
                    gar.setGrzDtEndEff(idsv0003.getDataInizioEffetto());
                    // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
                    //                               TO GRZ-DS-TS-INI-CPTZ
                    gar.setGrzDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
                    // COB_CODE: MOVE WS-TS-INFINITO
                    //                               TO GRZ-DS-TS-END-CPTZ
                    gar.setGrzDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
                    // COB_CODE: IF IDSV0003-SUCCESSFUL-SQL
                    //              PERFORM A220-INSERT-PK THRU A220-EX
                    //           END-IF
                    if (idsv0003.getSqlcode().isSuccessfulSql()) {
                        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX
                        a220InsertPk();
                    }
                }
            }
        }
        // COB_CODE: IF IDSV0003-NOT-FOUND
        //              END-IF
        //           END-IF.
        if (idsv0003.getSqlcode().isNotFound()) {
            // COB_CODE: IF NOT IDSV0003-DELETE-LOGICA
            //              PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
            //           ELSE
            //              SET IDSV0003-SUCCESSFUL-SQL TO TRUE
            //           END-IF
            if (!idsv0003.getOperazione().isDeleteLogica()) {
                // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX
                z600InsertNuovaRigaStorica();
            }
            else {
                // COB_CODE: SET IDSV0003-SUCCESSFUL-SQL TO TRUE
                idsv0003.getSqlcode().setSuccessfulSql();
            }
        }
    }

    /**Original name: Z550-AGG-STORICO-SOLO-INS<br>*/
    private void z550AggStoricoSoloIns() {
        // COB_CODE: MOVE GAR TO WS-BUFFER-TABLE.
        ws.setWsBufferTable(gar.getGarFormatted());
        // COB_CODE: MOVE GRZ-ID-MOVI-CRZ TO WS-ID-MOVI-CRZ.
        ws.setWsIdMoviCrz(TruncAbs.toInt(gar.getGrzIdMoviCrz(), 9));
        // COB_CODE: PERFORM Z600-INSERT-NUOVA-RIGA-STORICA THRU Z600-EX.
        z600InsertNuovaRigaStorica();
    }

    /**Original name: Z600-INSERT-NUOVA-RIGA-STORICA<br>*/
    private void z600InsertNuovaRigaStorica() {
        // COB_CODE: MOVE WS-BUFFER-TABLE TO GAR.
        gar.setGarFormatted(ws.getWsBufferTableFormatted());
        // COB_CODE: MOVE WS-ID-MOVI-CRZ  TO GRZ-ID-MOVI-CRZ.
        gar.setGrzIdMoviCrz(ws.getWsIdMoviCrz());
        // COB_CODE: MOVE HIGH-VALUES     TO GRZ-ID-MOVI-CHIU-NULL.
        gar.getGrzIdMoviChiu().setGrzIdMoviChiuNull(LiteralGenerator.create(Types.HIGH_CHAR_VAL, GrzIdMoviChiu.Len.GRZ_ID_MOVI_CHIU_NULL));
        // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO
        //                                TO GRZ-DT-INI-EFF.
        gar.setGrzDtIniEff(idsv0003.getDataInizioEffetto());
        // COB_CODE: MOVE WS-DT-INFINITO
        //                                TO GRZ-DT-END-EFF.
        gar.setGrzDtEndEff(ws.getIdsv0010().getWsDtInfinito());
        // COB_CODE: MOVE WS-TS-COMPETENZA-AGG-STOR
        //                                TO GRZ-DS-TS-INI-CPTZ.
        gar.setGrzDsTsIniCptz(ws.getIdsv0010().getWsTsCompetenzaAggStor());
        // COB_CODE: MOVE WS-TS-INFINITO
        //                                TO GRZ-DS-TS-END-CPTZ.
        gar.setGrzDsTsEndCptz(ws.getIdsv0010().getWsTsInfinito());
        // COB_CODE: MOVE IDSV0003-CODICE-COMPAGNIA-ANIA
        //                                TO GRZ-COD-COMP-ANIA.
        gar.setGrzCodCompAnia(idsv0003.getCodiceCompagniaAnia());
        // COB_CODE: PERFORM A220-INSERT-PK THRU A220-EX.
        a220InsertPk();
    }

    /**Original name: Z900-CONVERTI-N-TO-X<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da 9(8) comp-3 a date
	 * ----</pre>*/
    private void z900ConvertiNToX() {
        // COB_CODE: MOVE GRZ-DT-INI-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(gar.getGrzDtIniEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO GRZ-DT-INI-EFF-DB
        ws.getGarDb().setIniEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: MOVE GRZ-DT-END-EFF TO WS-DATE-N
        ws.getIdsv0010().setWsDateN(TruncAbs.toInt(gar.getGrzDtEndEff(), 8));
        // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
        z700DtNToX();
        // COB_CODE: MOVE WS-DATE-X      TO GRZ-DT-END-EFF-DB
        ws.getGarDb().setEndEffDb(ws.getIdsv0010().getWsDateX());
        // COB_CODE: IF IND-GRZ-DT-DECOR = 0
        //               MOVE WS-DATE-X      TO GRZ-DT-DECOR-DB
        //           END-IF
        if (ws.getIndGar().getDtDecor() == 0) {
            // COB_CODE: MOVE GRZ-DT-DECOR TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(gar.getGrzDtDecor().getGrzDtDecor(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO GRZ-DT-DECOR-DB
            ws.getGarDb().setDecorDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-GRZ-DT-SCAD = 0
        //               MOVE WS-DATE-X      TO GRZ-DT-SCAD-DB
        //           END-IF
        if (ws.getIndGar().getDtScad() == 0) {
            // COB_CODE: MOVE GRZ-DT-SCAD TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(gar.getGrzDtScad().getGrzDtScad(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO GRZ-DT-SCAD-DB
            ws.getGarDb().setScadDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-GRZ-DT-INI-VAL-TAR = 0
        //               MOVE WS-DATE-X      TO GRZ-DT-INI-VAL-TAR-DB
        //           END-IF
        if (ws.getIndGar().getDtIniValTar() == 0) {
            // COB_CODE: MOVE GRZ-DT-INI-VAL-TAR TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(gar.getGrzDtIniValTar().getGrzDtIniValTar(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO GRZ-DT-INI-VAL-TAR-DB
            ws.getGarDb().setIniValTarDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-GRZ-DT-END-CARZ = 0
        //               MOVE WS-DATE-X      TO GRZ-DT-END-CARZ-DB
        //           END-IF
        if (ws.getIndGar().getDtEndCarz() == 0) {
            // COB_CODE: MOVE GRZ-DT-END-CARZ TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(gar.getGrzDtEndCarz().getGrzDtEndCarz(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO GRZ-DT-END-CARZ-DB
            ws.getGarDb().setEndCarzDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-GRZ-DT-VARZ-TP-IAS = 0
        //               MOVE WS-DATE-X      TO GRZ-DT-VARZ-TP-IAS-DB
        //           END-IF
        if (ws.getIndGar().getDtVarzTpIas() == 0) {
            // COB_CODE: MOVE GRZ-DT-VARZ-TP-IAS TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(gar.getGrzDtVarzTpIas().getGrzDtVarzTpIas(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO GRZ-DT-VARZ-TP-IAS-DB
            ws.getGarDb().setVarzTpIasDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IND-GRZ-DT-PRESC = 0
        //               MOVE WS-DATE-X      TO GRZ-DT-PRESC-DB
        //           END-IF.
        if (ws.getIndGar().getDtPresc() == 0) {
            // COB_CODE: MOVE GRZ-DT-PRESC TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(gar.getGrzDtPresc().getGrzDtPresc(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X      TO GRZ-DT-PRESC-DB
            ws.getGarDb().setPrescDb(ws.getIdsv0010().getWsDateX());
        }
    }

    /**Original name: Z950-CONVERTI-X-TO-N<br>
	 * <pre>----
	 * ----  Conversione Data e Timestamp da date a 9(8) comp-3
	 * ----</pre>*/
    private void z950ConvertiXToN() {
        // COB_CODE: MOVE GRZ-DT-INI-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getGarDb().getIniEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-INI-EFF
        gar.setGrzDtIniEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: MOVE GRZ-DT-END-EFF-DB TO WS-DATE-X
        ws.getIdsv0010().setWsDateX(ws.getGarDb().getEndEffDb());
        // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
        z800DtXToN();
        // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-END-EFF
        gar.setGrzDtEndEff(ws.getIdsv0010().getWsDateN());
        // COB_CODE: IF IND-GRZ-DT-DECOR = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-DECOR
        //           END-IF
        if (ws.getIndGar().getDtDecor() == 0) {
            // COB_CODE: MOVE GRZ-DT-DECOR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getDecorDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-DECOR
            gar.getGrzDtDecor().setGrzDtDecor(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-SCAD = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-SCAD
        //           END-IF
        if (ws.getIndGar().getDtScad() == 0) {
            // COB_CODE: MOVE GRZ-DT-SCAD-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getScadDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-SCAD
            gar.getGrzDtScad().setGrzDtScad(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-INI-VAL-TAR = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-INI-VAL-TAR
        //           END-IF
        if (ws.getIndGar().getDtIniValTar() == 0) {
            // COB_CODE: MOVE GRZ-DT-INI-VAL-TAR-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getIniValTarDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-INI-VAL-TAR
            gar.getGrzDtIniValTar().setGrzDtIniValTar(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-END-CARZ = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-END-CARZ
        //           END-IF
        if (ws.getIndGar().getDtEndCarz() == 0) {
            // COB_CODE: MOVE GRZ-DT-END-CARZ-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getEndCarzDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-END-CARZ
            gar.getGrzDtEndCarz().setGrzDtEndCarz(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-VARZ-TP-IAS = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-VARZ-TP-IAS
        //           END-IF
        if (ws.getIndGar().getDtVarzTpIas() == 0) {
            // COB_CODE: MOVE GRZ-DT-VARZ-TP-IAS-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getVarzTpIasDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-VARZ-TP-IAS
            gar.getGrzDtVarzTpIas().setGrzDtVarzTpIas(ws.getIdsv0010().getWsDateN());
        }
        // COB_CODE: IF IND-GRZ-DT-PRESC = 0
        //               MOVE WS-DATE-N      TO GRZ-DT-PRESC
        //           END-IF.
        if (ws.getIndGar().getDtPresc() == 0) {
            // COB_CODE: MOVE GRZ-DT-PRESC-DB TO WS-DATE-X
            ws.getIdsv0010().setWsDateX(ws.getGarDb().getPrescDb());
            // COB_CODE: PERFORM Z800-DT-X-TO-N   THRU Z800-EX
            z800DtXToN();
            // COB_CODE: MOVE WS-DATE-N      TO GRZ-DT-PRESC
            gar.getGrzDtPresc().setGrzDtPresc(ws.getIdsv0010().getWsDateN());
        }
    }

    /**Original name: Z960-LENGTH-VCHAR<br>
	 * <pre>----
	 * ----  Calcola la lunghezza di tutti i campi VARCHAR
	 * ----</pre>*/
    private void z960LengthVchar() {
    // COB_CODE: CONTINUE.
    //continue
    }

    /**Original name: A001-TRATTA-DATE-TIMESTAMP<br>
	 * <pre>----------------------------------------------------------------*
	 *     ROUTINES DI :
	 *         -     CONVERSIONE DATE AND TIMESTAMP
	 *         -     GESTIONE COMPETENZA
	 * ----------------------------------------------------------------*</pre>*/
    private void a001TrattaDateTimestamp() {
        // COB_CODE: PERFORM A020-CONVERTI-DT-EFFETTO THRU A020-EX.
        a020ConvertiDtEffetto();
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: PERFORM A050-VALORIZZA-CPTZ   THRU A050-EX
            a050ValorizzaCptz();
        }
    }

    /**Original name: A020-CONVERTI-DT-EFFETTO<br>*/
    private void a020ConvertiDtEffetto() {
        // COB_CODE:      IF IDSV0003-DATA-INIZIO-EFFETTO  NOT NUMERIC OR
        //                   IDSV0003-DATA-INIZIO-EFFETTO  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
        //                END-IF
        if (!Functions.isNumber(idsv0003.getDataInizioEffetto()) || idsv0003.getDataInizioEffetto() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA INIZIO EFFETTO DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-INIZIO-EFFETTO TO WS-DATE-N
            ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataInizioEffetto(), 8));
            // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
            z700DtNToX();
            // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-INIZIO-EFFETTO-DB
            ws.getIdsv0010().setWsDataInizioEffettoDb(ws.getIdsv0010().getWsDateX());
        }
        // COB_CODE: IF IDSV0003-SUCCESSFUL-RC
        //              END-IF
        //           END-IF.
        if (idsv0003.getReturnCode().isSuccessfulRc()) {
            // COB_CODE: IF IDSV0003-DATA-FINE-EFFETTO  NUMERIC AND
            //              IDSV0003-DATA-FINE-EFFETTO  NOT = 0
            //              MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
            //           END-IF
            if (Functions.isNumber(idsv0003.getDataFineEffetto()) && idsv0003.getDataFineEffetto() != 0) {
                // COB_CODE: MOVE IDSV0003-DATA-FINE-EFFETTO TO WS-DATE-N
                ws.getIdsv0010().setWsDateN(TruncAbs.toInt(idsv0003.getDataFineEffetto(), 8));
                // COB_CODE: PERFORM Z700-DT-N-TO-X THRU Z700-EX
                z700DtNToX();
                // COB_CODE: MOVE WS-DATE-X         TO WS-DATA-FINE-EFFETTO-DB
                ws.getIdsv0010().setWsDataFineEffettoDb(ws.getIdsv0010().getWsDateX());
            }
        }
    }

    /**Original name: A050-VALORIZZA-CPTZ<br>*/
    private void a050ValorizzaCptz() {
        // COB_CODE:      IF IDSV0003-DATA-COMPETENZA  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMPETENZA  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                   MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompetenza()) || idsv0003.getDataCompetenza() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMPETENZA TO WS-TS-COMPETENZA
            ws.getIdsv0010().setWsTsCompetenza(idsv0003.getDataCompetenza());
        }
        // COB_CODE:      IF IDSV0003-DATA-COMP-AGG-STOR  NOT NUMERIC OR
        //                   IDSV0003-DATA-COMP-AGG-STOR  = 0
        //           *       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //           *       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //           *                                TO IDSV0003-DESCRIZ-ERR-DB2
        //                   CONTINUE
        //                ELSE
        //                                       TO WS-TS-COMPETENZA-AGG-STOR
        //                END-IF.
        if (!Functions.isNumber(idsv0003.getDataCompAggStor()) || idsv0003.getDataCompAggStor() == 0) {
        //       SET IDSV0003-FIELD-NOT-VALUED TO TRUE
        //       MOVE 'DATA COMPETENZA DI CONTESTO NON VALORIZZATA'
        //                                TO IDSV0003-DESCRIZ-ERR-DB2
        // COB_CODE: CONTINUE
        //continue
        }
        else {
            // COB_CODE: MOVE IDSV0003-DATA-COMP-AGG-STOR
            //                               TO WS-TS-COMPETENZA-AGG-STOR
            ws.getIdsv0010().setWsTsCompetenzaAggStor(idsv0003.getDataCompAggStor());
        }
    }

    /**Original name: Z700-DT-N-TO-X<br>
	 * <pre>---
	 * --- ROUTINE PER LA CONVERSIONE DI DATE E TIMESTAMP
	 * ---</pre>*/
    private void z700DtNToX() {
        // COB_CODE: MOVE WS-STR-DATE-N(1:4)
        //                TO WS-DATE-X(1:4)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-STR-DATE-N(5:2)
        //                TO WS-DATE-X(6:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((5) - 1, 6), 6, 2));
        // COB_CODE: MOVE WS-STR-DATE-N(7:2)
        //                TO WS-DATE-X(9:2)
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), ws.getIdsv0010().getWsStrDateNFormatted().substring((7) - 1, 8), 9, 2));
        // COB_CODE: MOVE '-'
        //                TO WS-DATE-X(5:1)
        //                   WS-DATE-X(8:1).
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 5, 1));
        ws.getIdsv0010().setWsDateX(Functions.setSubstring(ws.getIdsv0010().getWsDateX(), "-", 8, 1));
    }

    /**Original name: Z800-DT-X-TO-N<br>*/
    private void z800DtXToN() {
        // COB_CODE: IF IDSV0003-DB-ISO
        //              PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
        //           ELSE
        //              PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
        //           END-IF.
        if (idsv0003.getFormatoDataDb().isIso()) {
            // COB_CODE: PERFORM Z810-DT-X-TO-N-ISO THRU Z810-EX
            z810DtXToNIso();
        }
        else {
            // COB_CODE: PERFORM Z820-DT-X-TO-N-EUR THRU Z820-EX
            z820DtXToNEur();
        }
    }

    /**Original name: Z810-DT-X-TO-N-ISO<br>*/
    private void z810DtXToNIso() {
        // COB_CODE: MOVE WS-DATE-X(1:4)
        //                   TO WS-STR-DATE-N(1:4)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 4), 1, 4));
        // COB_CODE: MOVE WS-DATE-X(6:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((6) - 1, 7), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(9:2)
        //                   TO WS-STR-DATE-N(7:2).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((9) - 1, 10), 7, 2));
    }

    /**Original name: Z820-DT-X-TO-N-EUR<br>*/
    private void z820DtXToNEur() {
        // COB_CODE: MOVE WS-DATE-X(1:2)
        //                   TO WS-STR-DATE-N(7:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((1) - 1, 2), 7, 2));
        // COB_CODE: MOVE WS-DATE-X(4:2)
        //                   TO WS-STR-DATE-N(5:2)
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((4) - 1, 5), 5, 2));
        // COB_CODE: MOVE WS-DATE-X(7:4)
        //                   TO WS-STR-DATE-N(1:4).
        ws.getIdsv0010().setWsStrDateNFormatted(Functions.setSubstring(ws.getIdsv0010().getWsStrDateNFormatted(), ws.getIdsv0010().getWsDateXFormatted().substring((7) - 1, 10), 1, 4));
    }

    @Override
    public int getAaPagPreUni() {
        return gar.getGrzAaPagPreUni().getGrzAaPagPreUni();
    }

    @Override
    public void setAaPagPreUni(int aaPagPreUni) {
        this.gar.getGrzAaPagPreUni().setGrzAaPagPreUni(aaPagPreUni);
    }

    @Override
    public Integer getAaPagPreUniObj() {
        if (ws.getIndGar().getAaPagPreUni() >= 0) {
            return ((Integer)getAaPagPreUni());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaPagPreUniObj(Integer aaPagPreUniObj) {
        if (aaPagPreUniObj != null) {
            setAaPagPreUni(((int)aaPagPreUniObj));
            ws.getIndGar().setAaPagPreUni(((short)0));
        }
        else {
            ws.getIndGar().setAaPagPreUni(((short)-1));
        }
    }

    @Override
    public String getAaRenCer() {
        return gar.getGrzAaRenCer();
    }

    @Override
    public void setAaRenCer(String aaRenCer) {
        this.gar.setGrzAaRenCer(aaRenCer);
    }

    @Override
    public String getAaRenCerObj() {
        if (ws.getIndGar().getAaRenCer() >= 0) {
            return getAaRenCer();
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaRenCerObj(String aaRenCerObj) {
        if (aaRenCerObj != null) {
            setAaRenCer(aaRenCerObj);
            ws.getIndGar().setAaRenCer(((short)0));
        }
        else {
            ws.getIndGar().setAaRenCer(((short)-1));
        }
    }

    @Override
    public int getAaStab() {
        return gar.getGrzAaStab().getGrzAaStab();
    }

    @Override
    public void setAaStab(int aaStab) {
        this.gar.getGrzAaStab().setGrzAaStab(aaStab);
    }

    @Override
    public Integer getAaStabObj() {
        if (ws.getIndGar().getAaStab() >= 0) {
            return ((Integer)getAaStab());
        }
        else {
            return null;
        }
    }

    @Override
    public void setAaStabObj(Integer aaStabObj) {
        if (aaStabObj != null) {
            setAaStab(((int)aaStabObj));
            ws.getIndGar().setAaStab(((short)0));
        }
        else {
            ws.getIndGar().setAaStab(((short)-1));
        }
    }

    @Override
    public int getCodCompAnia() {
        return gar.getGrzCodCompAnia();
    }

    @Override
    public void setCodCompAnia(int codCompAnia) {
        this.gar.setGrzCodCompAnia(codCompAnia);
    }

    @Override
    public String getCodFnd() {
        return gar.getGrzCodFnd();
    }

    @Override
    public void setCodFnd(String codFnd) {
        this.gar.setGrzCodFnd(codFnd);
    }

    @Override
    public String getCodFndObj() {
        if (ws.getIndGar().getCodFnd() >= 0) {
            return getCodFnd();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodFndObj(String codFndObj) {
        if (codFndObj != null) {
            setCodFnd(codFndObj);
            ws.getIndGar().setCodFnd(((short)0));
        }
        else {
            ws.getIndGar().setCodFnd(((short)-1));
        }
    }

    @Override
    public String getCodSez() {
        return gar.getGrzCodSez();
    }

    @Override
    public void setCodSez(String codSez) {
        this.gar.setGrzCodSez(codSez);
    }

    @Override
    public String getCodSezObj() {
        if (ws.getIndGar().getCodSez() >= 0) {
            return getCodSez();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodSezObj(String codSezObj) {
        if (codSezObj != null) {
            setCodSez(codSezObj);
            ws.getIndGar().setCodSez(((short)0));
        }
        else {
            ws.getIndGar().setCodSez(((short)-1));
        }
    }

    @Override
    public String getCodTari() {
        return gar.getGrzCodTari();
    }

    @Override
    public void setCodTari(String codTari) {
        this.gar.setGrzCodTari(codTari);
    }

    @Override
    public String getCodTratRiass() {
        return gar.getGrzCodTratRiass();
    }

    @Override
    public void setCodTratRiass(String codTratRiass) {
        this.gar.setGrzCodTratRiass(codTratRiass);
    }

    @Override
    public String getCodTratRiassObj() {
        if (ws.getIndGar().getCodTratRiass() >= 0) {
            return getCodTratRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setCodTratRiassObj(String codTratRiassObj) {
        if (codTratRiassObj != null) {
            setCodTratRiass(codTratRiassObj);
            ws.getIndGar().setCodTratRiass(((short)0));
        }
        else {
            ws.getIndGar().setCodTratRiass(((short)-1));
        }
    }

    @Override
    public char getDsOperSql() {
        return gar.getGrzDsOperSql();
    }

    @Override
    public void setDsOperSql(char dsOperSql) {
        this.gar.setGrzDsOperSql(dsOperSql);
    }

    @Override
    public char getDsStatoElab() {
        return gar.getGrzDsStatoElab();
    }

    @Override
    public void setDsStatoElab(char dsStatoElab) {
        this.gar.setGrzDsStatoElab(dsStatoElab);
    }

    @Override
    public long getDsTsEndCptz() {
        return gar.getGrzDsTsEndCptz();
    }

    @Override
    public void setDsTsEndCptz(long dsTsEndCptz) {
        this.gar.setGrzDsTsEndCptz(dsTsEndCptz);
    }

    @Override
    public long getDsTsIniCptz() {
        return gar.getGrzDsTsIniCptz();
    }

    @Override
    public void setDsTsIniCptz(long dsTsIniCptz) {
        this.gar.setGrzDsTsIniCptz(dsTsIniCptz);
    }

    @Override
    public String getDsUtente() {
        return gar.getGrzDsUtente();
    }

    @Override
    public void setDsUtente(String dsUtente) {
        this.gar.setGrzDsUtente(dsUtente);
    }

    @Override
    public int getDsVer() {
        return gar.getGrzDsVer();
    }

    @Override
    public void setDsVer(int dsVer) {
        this.gar.setGrzDsVer(dsVer);
    }

    @Override
    public String getDtDecorDb() {
        return ws.getGarDb().getDecorDb();
    }

    @Override
    public void setDtDecorDb(String dtDecorDb) {
        this.ws.getGarDb().setDecorDb(dtDecorDb);
    }

    @Override
    public String getDtDecorDbObj() {
        if (ws.getIndGar().getDtDecor() >= 0) {
            return getDtDecorDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtDecorDbObj(String dtDecorDbObj) {
        if (dtDecorDbObj != null) {
            setDtDecorDb(dtDecorDbObj);
            ws.getIndGar().setDtDecor(((short)0));
        }
        else {
            ws.getIndGar().setDtDecor(((short)-1));
        }
    }

    @Override
    public String getDtEndCarzDb() {
        return ws.getGarDb().getEndCarzDb();
    }

    @Override
    public void setDtEndCarzDb(String dtEndCarzDb) {
        this.ws.getGarDb().setEndCarzDb(dtEndCarzDb);
    }

    @Override
    public String getDtEndCarzDbObj() {
        if (ws.getIndGar().getDtEndCarz() >= 0) {
            return getDtEndCarzDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtEndCarzDbObj(String dtEndCarzDbObj) {
        if (dtEndCarzDbObj != null) {
            setDtEndCarzDb(dtEndCarzDbObj);
            ws.getIndGar().setDtEndCarz(((short)0));
        }
        else {
            ws.getIndGar().setDtEndCarz(((short)-1));
        }
    }

    @Override
    public String getDtEndEffDb() {
        return ws.getGarDb().getEndEffDb();
    }

    @Override
    public void setDtEndEffDb(String dtEndEffDb) {
        this.ws.getGarDb().setEndEffDb(dtEndEffDb);
    }

    @Override
    public String getDtIniEffDb() {
        return ws.getGarDb().getIniEffDb();
    }

    @Override
    public void setDtIniEffDb(String dtIniEffDb) {
        this.ws.getGarDb().setIniEffDb(dtIniEffDb);
    }

    @Override
    public String getDtIniValTarDb() {
        return ws.getGarDb().getIniValTarDb();
    }

    @Override
    public void setDtIniValTarDb(String dtIniValTarDb) {
        this.ws.getGarDb().setIniValTarDb(dtIniValTarDb);
    }

    @Override
    public String getDtIniValTarDbObj() {
        if (ws.getIndGar().getDtIniValTar() >= 0) {
            return getDtIniValTarDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtIniValTarDbObj(String dtIniValTarDbObj) {
        if (dtIniValTarDbObj != null) {
            setDtIniValTarDb(dtIniValTarDbObj);
            ws.getIndGar().setDtIniValTar(((short)0));
        }
        else {
            ws.getIndGar().setDtIniValTar(((short)-1));
        }
    }

    @Override
    public String getDtPrescDb() {
        return ws.getGarDb().getPrescDb();
    }

    @Override
    public void setDtPrescDb(String dtPrescDb) {
        this.ws.getGarDb().setPrescDb(dtPrescDb);
    }

    @Override
    public String getDtPrescDbObj() {
        if (ws.getIndGar().getDtPresc() >= 0) {
            return getDtPrescDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtPrescDbObj(String dtPrescDbObj) {
        if (dtPrescDbObj != null) {
            setDtPrescDb(dtPrescDbObj);
            ws.getIndGar().setDtPresc(((short)0));
        }
        else {
            ws.getIndGar().setDtPresc(((short)-1));
        }
    }

    @Override
    public String getDtScadDb() {
        return ws.getGarDb().getScadDb();
    }

    @Override
    public void setDtScadDb(String dtScadDb) {
        this.ws.getGarDb().setScadDb(dtScadDb);
    }

    @Override
    public String getDtScadDbObj() {
        if (ws.getIndGar().getDtScad() >= 0) {
            return getDtScadDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtScadDbObj(String dtScadDbObj) {
        if (dtScadDbObj != null) {
            setDtScadDb(dtScadDbObj);
            ws.getIndGar().setDtScad(((short)0));
        }
        else {
            ws.getIndGar().setDtScad(((short)-1));
        }
    }

    @Override
    public String getDtVarzTpIasDb() {
        return ws.getGarDb().getVarzTpIasDb();
    }

    @Override
    public void setDtVarzTpIasDb(String dtVarzTpIasDb) {
        this.ws.getGarDb().setVarzTpIasDb(dtVarzTpIasDb);
    }

    @Override
    public String getDtVarzTpIasDbObj() {
        if (ws.getIndGar().getDtVarzTpIas() >= 0) {
            return getDtVarzTpIasDb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setDtVarzTpIasDbObj(String dtVarzTpIasDbObj) {
        if (dtVarzTpIasDbObj != null) {
            setDtVarzTpIasDb(dtVarzTpIasDbObj);
            ws.getIndGar().setDtVarzTpIas(((short)0));
        }
        else {
            ws.getIndGar().setDtVarzTpIas(((short)-1));
        }
    }

    @Override
    public int getDurAa() {
        return gar.getGrzDurAa().getGrzDurAa();
    }

    @Override
    public void setDurAa(int durAa) {
        this.gar.getGrzDurAa().setGrzDurAa(durAa);
    }

    @Override
    public Integer getDurAaObj() {
        if (ws.getIndGar().getDurAa() >= 0) {
            return ((Integer)getDurAa());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurAaObj(Integer durAaObj) {
        if (durAaObj != null) {
            setDurAa(((int)durAaObj));
            ws.getIndGar().setDurAa(((short)0));
        }
        else {
            ws.getIndGar().setDurAa(((short)-1));
        }
    }

    @Override
    public int getDurGg() {
        return gar.getGrzDurGg().getGrzDurGg();
    }

    @Override
    public void setDurGg(int durGg) {
        this.gar.getGrzDurGg().setGrzDurGg(durGg);
    }

    @Override
    public Integer getDurGgObj() {
        if (ws.getIndGar().getDurGg() >= 0) {
            return ((Integer)getDurGg());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurGgObj(Integer durGgObj) {
        if (durGgObj != null) {
            setDurGg(((int)durGgObj));
            ws.getIndGar().setDurGg(((short)0));
        }
        else {
            ws.getIndGar().setDurGg(((short)-1));
        }
    }

    @Override
    public int getDurMm() {
        return gar.getGrzDurMm().getGrzDurMm();
    }

    @Override
    public void setDurMm(int durMm) {
        this.gar.getGrzDurMm().setGrzDurMm(durMm);
    }

    @Override
    public Integer getDurMmObj() {
        if (ws.getIndGar().getDurMm() >= 0) {
            return ((Integer)getDurMm());
        }
        else {
            return null;
        }
    }

    @Override
    public void setDurMmObj(Integer durMmObj) {
        if (durMmObj != null) {
            setDurMm(((int)durMmObj));
            ws.getIndGar().setDurMm(((short)0));
        }
        else {
            ws.getIndGar().setDurMm(((short)-1));
        }
    }

    @Override
    public int getEtaAScad() {
        return gar.getGrzEtaAScad().getGrzEtaAScad();
    }

    @Override
    public void setEtaAScad(int etaAScad) {
        this.gar.getGrzEtaAScad().setGrzEtaAScad(etaAScad);
    }

    @Override
    public Integer getEtaAScadObj() {
        if (ws.getIndGar().getEtaAScad() >= 0) {
            return ((Integer)getEtaAScad());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAScadObj(Integer etaAScadObj) {
        if (etaAScadObj != null) {
            setEtaAScad(((int)etaAScadObj));
            ws.getIndGar().setEtaAScad(((short)0));
        }
        else {
            ws.getIndGar().setEtaAScad(((short)-1));
        }
    }

    @Override
    public short getEtaAa1oAssto() {
        return gar.getGrzEtaAa1oAssto().getGrzEtaAa1oAssto();
    }

    @Override
    public void setEtaAa1oAssto(short etaAa1oAssto) {
        this.gar.getGrzEtaAa1oAssto().setGrzEtaAa1oAssto(etaAa1oAssto);
    }

    @Override
    public Short getEtaAa1oAsstoObj() {
        if (ws.getIndGar().getEtaAa1oAssto() >= 0) {
            return ((Short)getEtaAa1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa1oAsstoObj(Short etaAa1oAsstoObj) {
        if (etaAa1oAsstoObj != null) {
            setEtaAa1oAssto(((short)etaAa1oAsstoObj));
            ws.getIndGar().setEtaAa1oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaAa1oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaAa2oAssto() {
        return gar.getGrzEtaAa2oAssto().getGrzEtaAa2oAssto();
    }

    @Override
    public void setEtaAa2oAssto(short etaAa2oAssto) {
        this.gar.getGrzEtaAa2oAssto().setGrzEtaAa2oAssto(etaAa2oAssto);
    }

    @Override
    public Short getEtaAa2oAsstoObj() {
        if (ws.getIndGar().getEtaAa2oAssto() >= 0) {
            return ((Short)getEtaAa2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa2oAsstoObj(Short etaAa2oAsstoObj) {
        if (etaAa2oAsstoObj != null) {
            setEtaAa2oAssto(((short)etaAa2oAsstoObj));
            ws.getIndGar().setEtaAa2oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaAa2oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaAa3oAssto() {
        return gar.getGrzEtaAa3oAssto().getGrzEtaAa3oAssto();
    }

    @Override
    public void setEtaAa3oAssto(short etaAa3oAssto) {
        this.gar.getGrzEtaAa3oAssto().setGrzEtaAa3oAssto(etaAa3oAssto);
    }

    @Override
    public Short getEtaAa3oAsstoObj() {
        if (ws.getIndGar().getEtaAa3oAssto() >= 0) {
            return ((Short)getEtaAa3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaAa3oAsstoObj(Short etaAa3oAsstoObj) {
        if (etaAa3oAsstoObj != null) {
            setEtaAa3oAssto(((short)etaAa3oAsstoObj));
            ws.getIndGar().setEtaAa3oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaAa3oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm1oAssto() {
        return gar.getGrzEtaMm1oAssto().getGrzEtaMm1oAssto();
    }

    @Override
    public void setEtaMm1oAssto(short etaMm1oAssto) {
        this.gar.getGrzEtaMm1oAssto().setGrzEtaMm1oAssto(etaMm1oAssto);
    }

    @Override
    public Short getEtaMm1oAsstoObj() {
        if (ws.getIndGar().getEtaMm1oAssto() >= 0) {
            return ((Short)getEtaMm1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm1oAsstoObj(Short etaMm1oAsstoObj) {
        if (etaMm1oAsstoObj != null) {
            setEtaMm1oAssto(((short)etaMm1oAsstoObj));
            ws.getIndGar().setEtaMm1oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaMm1oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm2oAssto() {
        return gar.getGrzEtaMm2oAssto().getGrzEtaMm2oAssto();
    }

    @Override
    public void setEtaMm2oAssto(short etaMm2oAssto) {
        this.gar.getGrzEtaMm2oAssto().setGrzEtaMm2oAssto(etaMm2oAssto);
    }

    @Override
    public Short getEtaMm2oAsstoObj() {
        if (ws.getIndGar().getEtaMm2oAssto() >= 0) {
            return ((Short)getEtaMm2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm2oAsstoObj(Short etaMm2oAsstoObj) {
        if (etaMm2oAsstoObj != null) {
            setEtaMm2oAssto(((short)etaMm2oAsstoObj));
            ws.getIndGar().setEtaMm2oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaMm2oAssto(((short)-1));
        }
    }

    @Override
    public short getEtaMm3oAssto() {
        return gar.getGrzEtaMm3oAssto().getGrzEtaMm3oAssto();
    }

    @Override
    public void setEtaMm3oAssto(short etaMm3oAssto) {
        this.gar.getGrzEtaMm3oAssto().setGrzEtaMm3oAssto(etaMm3oAssto);
    }

    @Override
    public Short getEtaMm3oAsstoObj() {
        if (ws.getIndGar().getEtaMm3oAssto() >= 0) {
            return ((Short)getEtaMm3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setEtaMm3oAsstoObj(Short etaMm3oAsstoObj) {
        if (etaMm3oAsstoObj != null) {
            setEtaMm3oAssto(((short)etaMm3oAsstoObj));
            ws.getIndGar().setEtaMm3oAssto(((short)0));
        }
        else {
            ws.getIndGar().setEtaMm3oAssto(((short)-1));
        }
    }

    @Override
    public int getFrazDecrCpt() {
        return gar.getGrzFrazDecrCpt().getGrzFrazDecrCpt();
    }

    @Override
    public void setFrazDecrCpt(int frazDecrCpt) {
        this.gar.getGrzFrazDecrCpt().setGrzFrazDecrCpt(frazDecrCpt);
    }

    @Override
    public Integer getFrazDecrCptObj() {
        if (ws.getIndGar().getFrazDecrCpt() >= 0) {
            return ((Integer)getFrazDecrCpt());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazDecrCptObj(Integer frazDecrCptObj) {
        if (frazDecrCptObj != null) {
            setFrazDecrCpt(((int)frazDecrCptObj));
            ws.getIndGar().setFrazDecrCpt(((short)0));
        }
        else {
            ws.getIndGar().setFrazDecrCpt(((short)-1));
        }
    }

    @Override
    public int getFrazIniErogRen() {
        return gar.getGrzFrazIniErogRen().getGrzFrazIniErogRen();
    }

    @Override
    public void setFrazIniErogRen(int frazIniErogRen) {
        this.gar.getGrzFrazIniErogRen().setGrzFrazIniErogRen(frazIniErogRen);
    }

    @Override
    public Integer getFrazIniErogRenObj() {
        if (ws.getIndGar().getFrazIniErogRen() >= 0) {
            return ((Integer)getFrazIniErogRen());
        }
        else {
            return null;
        }
    }

    @Override
    public void setFrazIniErogRenObj(Integer frazIniErogRenObj) {
        if (frazIniErogRenObj != null) {
            setFrazIniErogRen(((int)frazIniErogRenObj));
            ws.getIndGar().setFrazIniErogRen(((short)0));
        }
        else {
            ws.getIndGar().setFrazIniErogRen(((short)-1));
        }
    }

    @Override
    public long getGrzDsRiga() {
        return gar.getGrzDsRiga();
    }

    @Override
    public void setGrzDsRiga(long grzDsRiga) {
        this.gar.setGrzDsRiga(grzDsRiga);
    }

    @Override
    public int getGrzIdAdes() {
        return gar.getGrzIdAdes().getGrzIdAdes();
    }

    @Override
    public void setGrzIdAdes(int grzIdAdes) {
        this.gar.getGrzIdAdes().setGrzIdAdes(grzIdAdes);
    }

    @Override
    public Integer getGrzIdAdesObj() {
        if (ws.getIndGar().getIdAdes() >= 0) {
            return ((Integer)getGrzIdAdes());
        }
        else {
            return null;
        }
    }

    @Override
    public void setGrzIdAdesObj(Integer grzIdAdesObj) {
        if (grzIdAdesObj != null) {
            setGrzIdAdes(((int)grzIdAdesObj));
            ws.getIndGar().setIdAdes(((short)0));
        }
        else {
            ws.getIndGar().setIdAdes(((short)-1));
        }
    }

    @Override
    public int getGrzIdPoli() {
        return gar.getGrzIdPoli();
    }

    @Override
    public void setGrzIdPoli(int grzIdPoli) {
        this.gar.setGrzIdPoli(grzIdPoli);
    }

    @Override
    public String getIbOgg() {
        return gar.getGrzIbOgg();
    }

    @Override
    public void setIbOgg(String ibOgg) {
        this.gar.setGrzIbOgg(ibOgg);
    }

    @Override
    public String getIbOggObj() {
        if (ws.getIndGar().getIbOgg() >= 0) {
            return getIbOgg();
        }
        else {
            return null;
        }
    }

    @Override
    public void setIbOggObj(String ibOggObj) {
        if (ibOggObj != null) {
            setIbOgg(ibOggObj);
            ws.getIndGar().setIbOgg(((short)0));
        }
        else {
            ws.getIndGar().setIbOgg(((short)-1));
        }
    }

    @Override
    public int getId1oAssto() {
        return gar.getGrzId1oAssto().getGrzId1oAssto();
    }

    @Override
    public void setId1oAssto(int id1oAssto) {
        this.gar.getGrzId1oAssto().setGrzId1oAssto(id1oAssto);
    }

    @Override
    public Integer getId1oAsstoObj() {
        if (ws.getIndGar().getId1oAssto() >= 0) {
            return ((Integer)getId1oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setId1oAsstoObj(Integer id1oAsstoObj) {
        if (id1oAsstoObj != null) {
            setId1oAssto(((int)id1oAsstoObj));
            ws.getIndGar().setId1oAssto(((short)0));
        }
        else {
            ws.getIndGar().setId1oAssto(((short)-1));
        }
    }

    @Override
    public int getId2oAssto() {
        return gar.getGrzId2oAssto().getGrzId2oAssto();
    }

    @Override
    public void setId2oAssto(int id2oAssto) {
        this.gar.getGrzId2oAssto().setGrzId2oAssto(id2oAssto);
    }

    @Override
    public Integer getId2oAsstoObj() {
        if (ws.getIndGar().getId2oAssto() >= 0) {
            return ((Integer)getId2oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setId2oAsstoObj(Integer id2oAsstoObj) {
        if (id2oAsstoObj != null) {
            setId2oAssto(((int)id2oAsstoObj));
            ws.getIndGar().setId2oAssto(((short)0));
        }
        else {
            ws.getIndGar().setId2oAssto(((short)-1));
        }
    }

    @Override
    public int getId3oAssto() {
        return gar.getGrzId3oAssto().getGrzId3oAssto();
    }

    @Override
    public void setId3oAssto(int id3oAssto) {
        this.gar.getGrzId3oAssto().setGrzId3oAssto(id3oAssto);
    }

    @Override
    public Integer getId3oAsstoObj() {
        if (ws.getIndGar().getId3oAssto() >= 0) {
            return ((Integer)getId3oAssto());
        }
        else {
            return null;
        }
    }

    @Override
    public void setId3oAsstoObj(Integer id3oAsstoObj) {
        if (id3oAsstoObj != null) {
            setId3oAssto(((int)id3oAsstoObj));
            ws.getIndGar().setId3oAssto(((short)0));
        }
        else {
            ws.getIndGar().setId3oAssto(((short)-1));
        }
    }

    @Override
    public int getIdGar() {
        return gar.getGrzIdGar();
    }

    @Override
    public void setIdGar(int idGar) {
        this.gar.setGrzIdGar(idGar);
    }

    @Override
    public int getIdMoviChiu() {
        return gar.getGrzIdMoviChiu().getGrzIdMoviChiu();
    }

    @Override
    public void setIdMoviChiu(int idMoviChiu) {
        this.gar.getGrzIdMoviChiu().setGrzIdMoviChiu(idMoviChiu);
    }

    @Override
    public Integer getIdMoviChiuObj() {
        if (ws.getIndGar().getIdMoviChiu() >= 0) {
            return ((Integer)getIdMoviChiu());
        }
        else {
            return null;
        }
    }

    @Override
    public void setIdMoviChiuObj(Integer idMoviChiuObj) {
        if (idMoviChiuObj != null) {
            setIdMoviChiu(((int)idMoviChiuObj));
            ws.getIndGar().setIdMoviChiu(((short)0));
        }
        else {
            ws.getIndGar().setIdMoviChiu(((short)-1));
        }
    }

    @Override
    public int getIdMoviCrz() {
        return gar.getGrzIdMoviCrz();
    }

    @Override
    public void setIdMoviCrz(int idMoviCrz) {
        this.gar.setGrzIdMoviCrz(idMoviCrz);
    }

    @Override
    public int getIdsv0003CodiceCompagniaAnia() {
        return idsv0003.getCodiceCompagniaAnia();
    }

    @Override
    public void setIdsv0003CodiceCompagniaAnia(int idsv0003CodiceCompagniaAnia) {
        this.idsv0003.setCodiceCompagniaAnia(idsv0003CodiceCompagniaAnia);
    }

    @Override
    public short getMm1oRat() {
        return gar.getGrzMm1oRat().getGrzMm1oRat();
    }

    @Override
    public void setMm1oRat(short mm1oRat) {
        this.gar.getGrzMm1oRat().setGrzMm1oRat(mm1oRat);
    }

    @Override
    public Short getMm1oRatObj() {
        if (ws.getIndGar().getMm1oRat() >= 0) {
            return ((Short)getMm1oRat());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMm1oRatObj(Short mm1oRatObj) {
        if (mm1oRatObj != null) {
            setMm1oRat(((short)mm1oRatObj));
            ws.getIndGar().setMm1oRat(((short)0));
        }
        else {
            ws.getIndGar().setMm1oRat(((short)-1));
        }
    }

    @Override
    public int getMmPagPreUni() {
        return gar.getGrzMmPagPreUni().getGrzMmPagPreUni();
    }

    @Override
    public void setMmPagPreUni(int mmPagPreUni) {
        this.gar.getGrzMmPagPreUni().setGrzMmPagPreUni(mmPagPreUni);
    }

    @Override
    public Integer getMmPagPreUniObj() {
        if (ws.getIndGar().getMmPagPreUni() >= 0) {
            return ((Integer)getMmPagPreUni());
        }
        else {
            return null;
        }
    }

    @Override
    public void setMmPagPreUniObj(Integer mmPagPreUniObj) {
        if (mmPagPreUniObj != null) {
            setMmPagPreUni(((int)mmPagPreUniObj));
            ws.getIndGar().setMmPagPreUni(((short)0));
        }
        else {
            ws.getIndGar().setMmPagPreUni(((short)-1));
        }
    }

    @Override
    public String getModPagGarcol() {
        return gar.getGrzModPagGarcol();
    }

    @Override
    public void setModPagGarcol(String modPagGarcol) {
        this.gar.setGrzModPagGarcol(modPagGarcol);
    }

    @Override
    public String getModPagGarcolObj() {
        if (ws.getIndGar().getModPagGarcol() >= 0) {
            return getModPagGarcol();
        }
        else {
            return null;
        }
    }

    @Override
    public void setModPagGarcolObj(String modPagGarcolObj) {
        if (modPagGarcolObj != null) {
            setModPagGarcol(modPagGarcolObj);
            ws.getIndGar().setModPagGarcol(((short)0));
        }
        else {
            ws.getIndGar().setModPagGarcol(((short)-1));
        }
    }

    @Override
    public int getNumAaPagPre() {
        return gar.getGrzNumAaPagPre().getGrzNumAaPagPre();
    }

    @Override
    public void setNumAaPagPre(int numAaPagPre) {
        this.gar.getGrzNumAaPagPre().setGrzNumAaPagPre(numAaPagPre);
    }

    @Override
    public Integer getNumAaPagPreObj() {
        if (ws.getIndGar().getNumAaPagPre() >= 0) {
            return ((Integer)getNumAaPagPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setNumAaPagPreObj(Integer numAaPagPreObj) {
        if (numAaPagPreObj != null) {
            setNumAaPagPre(((int)numAaPagPreObj));
            ws.getIndGar().setNumAaPagPre(((short)0));
        }
        else {
            ws.getIndGar().setNumAaPagPre(((short)-1));
        }
    }

    @Override
    public AfDecimal getPc1oRat() {
        return gar.getGrzPc1oRat().getGrzPc1oRat();
    }

    @Override
    public void setPc1oRat(AfDecimal pc1oRat) {
        this.gar.getGrzPc1oRat().setGrzPc1oRat(pc1oRat.copy());
    }

    @Override
    public AfDecimal getPc1oRatObj() {
        if (ws.getIndGar().getPc1oRat() >= 0) {
            return getPc1oRat();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPc1oRatObj(AfDecimal pc1oRatObj) {
        if (pc1oRatObj != null) {
            setPc1oRat(new AfDecimal(pc1oRatObj, 6, 3));
            ws.getIndGar().setPc1oRat(((short)0));
        }
        else {
            ws.getIndGar().setPc1oRat(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcOpz() {
        return gar.getGrzPcOpz().getGrzPcOpz();
    }

    @Override
    public void setPcOpz(AfDecimal pcOpz) {
        this.gar.getGrzPcOpz().setGrzPcOpz(pcOpz.copy());
    }

    @Override
    public AfDecimal getPcOpzObj() {
        if (ws.getIndGar().getPcOpz() >= 0) {
            return getPcOpz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcOpzObj(AfDecimal pcOpzObj) {
        if (pcOpzObj != null) {
            setPcOpz(new AfDecimal(pcOpzObj, 6, 3));
            ws.getIndGar().setPcOpz(((short)0));
        }
        else {
            ws.getIndGar().setPcOpz(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRevrsb() {
        return gar.getGrzPcRevrsb().getGrzPcRevrsb();
    }

    @Override
    public void setPcRevrsb(AfDecimal pcRevrsb) {
        this.gar.getGrzPcRevrsb().setGrzPcRevrsb(pcRevrsb.copy());
    }

    @Override
    public AfDecimal getPcRevrsbObj() {
        if (ws.getIndGar().getPcRevrsb() >= 0) {
            return getPcRevrsb();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRevrsbObj(AfDecimal pcRevrsbObj) {
        if (pcRevrsbObj != null) {
            setPcRevrsb(new AfDecimal(pcRevrsbObj, 6, 3));
            ws.getIndGar().setPcRevrsb(((short)0));
        }
        else {
            ws.getIndGar().setPcRevrsb(((short)-1));
        }
    }

    @Override
    public AfDecimal getPcRipPre() {
        return gar.getGrzPcRipPre().getGrzPcRipPre();
    }

    @Override
    public void setPcRipPre(AfDecimal pcRipPre) {
        this.gar.getGrzPcRipPre().setGrzPcRipPre(pcRipPre.copy());
    }

    @Override
    public AfDecimal getPcRipPreObj() {
        if (ws.getIndGar().getPcRipPre() >= 0) {
            return getPcRipPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setPcRipPreObj(AfDecimal pcRipPreObj) {
        if (pcRipPreObj != null) {
            setPcRipPre(new AfDecimal(pcRipPreObj, 6, 3));
            ws.getIndGar().setPcRipPre(((short)0));
        }
        else {
            ws.getIndGar().setPcRipPre(((short)-1));
        }
    }

    @Override
    public String getRamoBila() {
        return gar.getGrzRamoBila();
    }

    @Override
    public void setRamoBila(String ramoBila) {
        this.gar.setGrzRamoBila(ramoBila);
    }

    @Override
    public String getRamoBilaObj() {
        if (ws.getIndGar().getRamoBila() >= 0) {
            return getRamoBila();
        }
        else {
            return null;
        }
    }

    @Override
    public void setRamoBilaObj(String ramoBilaObj) {
        if (ramoBilaObj != null) {
            setRamoBila(ramoBilaObj);
            ws.getIndGar().setRamoBila(((short)0));
        }
        else {
            ws.getIndGar().setRamoBila(((short)-1));
        }
    }

    @Override
    public char getRshInvst() {
        return gar.getGrzRshInvst();
    }

    @Override
    public void setRshInvst(char rshInvst) {
        this.gar.setGrzRshInvst(rshInvst);
    }

    @Override
    public Character getRshInvstObj() {
        if (ws.getIndGar().getRshInvst() >= 0) {
            return ((Character)getRshInvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setRshInvstObj(Character rshInvstObj) {
        if (rshInvstObj != null) {
            setRshInvst(((char)rshInvstObj));
            ws.getIndGar().setRshInvst(((short)0));
        }
        else {
            ws.getIndGar().setRshInvst(((short)-1));
        }
    }

    @Override
    public char getTpAdegPre() {
        return gar.getGrzTpAdegPre();
    }

    @Override
    public void setTpAdegPre(char tpAdegPre) {
        this.gar.setGrzTpAdegPre(tpAdegPre);
    }

    @Override
    public Character getTpAdegPreObj() {
        if (ws.getIndGar().getTpAdegPre() >= 0) {
            return ((Character)getTpAdegPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpAdegPreObj(Character tpAdegPreObj) {
        if (tpAdegPreObj != null) {
            setTpAdegPre(((char)tpAdegPreObj));
            ws.getIndGar().setTpAdegPre(((short)0));
        }
        else {
            ws.getIndGar().setTpAdegPre(((short)-1));
        }
    }

    @Override
    public String getTpCalcPrePrstz() {
        return gar.getGrzTpCalcPrePrstz();
    }

    @Override
    public void setTpCalcPrePrstz(String tpCalcPrePrstz) {
        this.gar.setGrzTpCalcPrePrstz(tpCalcPrePrstz);
    }

    @Override
    public String getTpCalcPrePrstzObj() {
        if (ws.getIndGar().getTpCalcPrePrstz() >= 0) {
            return getTpCalcPrePrstz();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCalcPrePrstzObj(String tpCalcPrePrstzObj) {
        if (tpCalcPrePrstzObj != null) {
            setTpCalcPrePrstz(tpCalcPrePrstzObj);
            ws.getIndGar().setTpCalcPrePrstz(((short)0));
        }
        else {
            ws.getIndGar().setTpCalcPrePrstz(((short)-1));
        }
    }

    @Override
    public String getTpCessRiass() {
        return gar.getGrzTpCessRiass();
    }

    @Override
    public void setTpCessRiass(String tpCessRiass) {
        this.gar.setGrzTpCessRiass(tpCessRiass);
    }

    @Override
    public String getTpCessRiassObj() {
        if (ws.getIndGar().getTpCessRiass() >= 0) {
            return getTpCessRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpCessRiassObj(String tpCessRiassObj) {
        if (tpCessRiassObj != null) {
            setTpCessRiass(tpCessRiassObj);
            ws.getIndGar().setTpCessRiass(((short)0));
        }
        else {
            ws.getIndGar().setTpCessRiass(((short)-1));
        }
    }

    @Override
    public String getTpDtEmisRiass() {
        return gar.getGrzTpDtEmisRiass();
    }

    @Override
    public void setTpDtEmisRiass(String tpDtEmisRiass) {
        this.gar.setGrzTpDtEmisRiass(tpDtEmisRiass);
    }

    @Override
    public String getTpDtEmisRiassObj() {
        if (ws.getIndGar().getTpDtEmisRiass() >= 0) {
            return getTpDtEmisRiass();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDtEmisRiassObj(String tpDtEmisRiassObj) {
        if (tpDtEmisRiassObj != null) {
            setTpDtEmisRiass(tpDtEmisRiassObj);
            ws.getIndGar().setTpDtEmisRiass(((short)0));
        }
        else {
            ws.getIndGar().setTpDtEmisRiass(((short)-1));
        }
    }

    @Override
    public String getTpDur() {
        return gar.getGrzTpDur();
    }

    @Override
    public void setTpDur(String tpDur) {
        this.gar.setGrzTpDur(tpDur);
    }

    @Override
    public String getTpDurObj() {
        if (ws.getIndGar().getTpDur() >= 0) {
            return getTpDur();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpDurObj(String tpDurObj) {
        if (tpDurObj != null) {
            setTpDur(tpDurObj);
            ws.getIndGar().setTpDur(((short)0));
        }
        else {
            ws.getIndGar().setTpDur(((short)-1));
        }
    }

    @Override
    public char getTpEmisPur() {
        return gar.getGrzTpEmisPur();
    }

    @Override
    public void setTpEmisPur(char tpEmisPur) {
        this.gar.setGrzTpEmisPur(tpEmisPur);
    }

    @Override
    public Character getTpEmisPurObj() {
        if (ws.getIndGar().getTpEmisPur() >= 0) {
            return ((Character)getTpEmisPur());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpEmisPurObj(Character tpEmisPurObj) {
        if (tpEmisPurObj != null) {
            setTpEmisPur(((char)tpEmisPurObj));
            ws.getIndGar().setTpEmisPur(((short)0));
        }
        else {
            ws.getIndGar().setTpEmisPur(((short)-1));
        }
    }

    @Override
    public short getTpGar() {
        return gar.getGrzTpGar().getGrzTpGar();
    }

    @Override
    public void setTpGar(short tpGar) {
        this.gar.getGrzTpGar().setGrzTpGar(tpGar);
    }

    @Override
    public Short getTpGarObj() {
        if (ws.getIndGar().getTpGar() >= 0) {
            return ((Short)getTpGar());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpGarObj(Short tpGarObj) {
        if (tpGarObj != null) {
            setTpGar(((short)tpGarObj));
            ws.getIndGar().setTpGar(((short)0));
        }
        else {
            ws.getIndGar().setTpGar(((short)-1));
        }
    }

    @Override
    public String getTpIas() {
        return gar.getGrzTpIas();
    }

    @Override
    public void setTpIas(String tpIas) {
        this.gar.setGrzTpIas(tpIas);
    }

    @Override
    public String getTpIasObj() {
        if (ws.getIndGar().getTpIas() >= 0) {
            return getTpIas();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpIasObj(String tpIasObj) {
        if (tpIasObj != null) {
            setTpIas(tpIasObj);
            ws.getIndGar().setTpIas(((short)0));
        }
        else {
            ws.getIndGar().setTpIas(((short)-1));
        }
    }

    @Override
    public short getTpInvst() {
        return gar.getGrzTpInvst().getGrzTpInvst();
    }

    @Override
    public void setTpInvst(short tpInvst) {
        this.gar.getGrzTpInvst().setGrzTpInvst(tpInvst);
    }

    @Override
    public Short getTpInvstObj() {
        if (ws.getIndGar().getTpInvst() >= 0) {
            return ((Short)getTpInvst());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpInvstObj(Short tpInvstObj) {
        if (tpInvstObj != null) {
            setTpInvst(((short)tpInvstObj));
            ws.getIndGar().setTpInvst(((short)0));
        }
        else {
            ws.getIndGar().setTpInvst(((short)-1));
        }
    }

    @Override
    public String getTpPcRip() {
        return gar.getGrzTpPcRip();
    }

    @Override
    public void setTpPcRip(String tpPcRip) {
        this.gar.setGrzTpPcRip(tpPcRip);
    }

    @Override
    public String getTpPcRipObj() {
        if (ws.getIndGar().getTpPcRip() >= 0) {
            return getTpPcRip();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPcRipObj(String tpPcRipObj) {
        if (tpPcRipObj != null) {
            setTpPcRip(tpPcRipObj);
            ws.getIndGar().setTpPcRip(((short)0));
        }
        else {
            ws.getIndGar().setTpPcRip(((short)-1));
        }
    }

    @Override
    public String getTpPerPre() {
        return gar.getGrzTpPerPre();
    }

    @Override
    public void setTpPerPre(String tpPerPre) {
        this.gar.setGrzTpPerPre(tpPerPre);
    }

    @Override
    public String getTpPerPreObj() {
        if (ws.getIndGar().getTpPerPre() >= 0) {
            return getTpPerPre();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPerPreObj(String tpPerPreObj) {
        if (tpPerPreObj != null) {
            setTpPerPre(tpPerPreObj);
            ws.getIndGar().setTpPerPre(((short)0));
        }
        else {
            ws.getIndGar().setTpPerPre(((short)-1));
        }
    }

    @Override
    public char getTpPre() {
        return gar.getGrzTpPre();
    }

    @Override
    public void setTpPre(char tpPre) {
        this.gar.setGrzTpPre(tpPre);
    }

    @Override
    public Character getTpPreObj() {
        if (ws.getIndGar().getTpPre() >= 0) {
            return ((Character)getTpPre());
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPreObj(Character tpPreObj) {
        if (tpPreObj != null) {
            setTpPre(((char)tpPreObj));
            ws.getIndGar().setTpPre(((short)0));
        }
        else {
            ws.getIndGar().setTpPre(((short)-1));
        }
    }

    @Override
    public String getTpPrstzAssta() {
        return gar.getGrzTpPrstzAssta();
    }

    @Override
    public void setTpPrstzAssta(String tpPrstzAssta) {
        this.gar.setGrzTpPrstzAssta(tpPrstzAssta);
    }

    @Override
    public String getTpPrstzAsstaObj() {
        if (ws.getIndGar().getTpPrstzAssta() >= 0) {
            return getTpPrstzAssta();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpPrstzAsstaObj(String tpPrstzAsstaObj) {
        if (tpPrstzAsstaObj != null) {
            setTpPrstzAssta(tpPrstzAsstaObj);
            ws.getIndGar().setTpPrstzAssta(((short)0));
        }
        else {
            ws.getIndGar().setTpPrstzAssta(((short)-1));
        }
    }

    @Override
    public String getTpRamoBila() {
        return gar.getGrzTpRamoBila();
    }

    @Override
    public void setTpRamoBila(String tpRamoBila) {
        this.gar.setGrzTpRamoBila(tpRamoBila);
    }

    @Override
    public String getTpRsh() {
        return gar.getGrzTpRsh();
    }

    @Override
    public void setTpRsh(String tpRsh) {
        this.gar.setGrzTpRsh(tpRsh);
    }

    @Override
    public String getTpRshObj() {
        if (ws.getIndGar().getTpRsh() >= 0) {
            return getTpRsh();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpRshObj(String tpRshObj) {
        if (tpRshObj != null) {
            setTpRsh(tpRshObj);
            ws.getIndGar().setTpRsh(((short)0));
        }
        else {
            ws.getIndGar().setTpRsh(((short)-1));
        }
    }

    @Override
    public String getTpStab() {
        return gar.getGrzTpStab();
    }

    @Override
    public void setTpStab(String tpStab) {
        this.gar.setGrzTpStab(tpStab);
    }

    @Override
    public String getTpStabObj() {
        if (ws.getIndGar().getTpStab() >= 0) {
            return getTpStab();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTpStabObj(String tpStabObj) {
        if (tpStabObj != null) {
            setTpStab(tpStabObj);
            ws.getIndGar().setTpStab(((short)0));
        }
        else {
            ws.getIndGar().setTpStab(((short)-1));
        }
    }

    @Override
    public AfDecimal getTsStabLimitata() {
        return gar.getGrzTsStabLimitata().getGrzTsStabLimitata();
    }

    @Override
    public void setTsStabLimitata(AfDecimal tsStabLimitata) {
        this.gar.getGrzTsStabLimitata().setGrzTsStabLimitata(tsStabLimitata.copy());
    }

    @Override
    public AfDecimal getTsStabLimitataObj() {
        if (ws.getIndGar().getTsStabLimitata() >= 0) {
            return getTsStabLimitata();
        }
        else {
            return null;
        }
    }

    @Override
    public void setTsStabLimitataObj(AfDecimal tsStabLimitataObj) {
        if (tsStabLimitataObj != null) {
            setTsStabLimitata(new AfDecimal(tsStabLimitataObj, 14, 9));
            ws.getIndGar().setTsStabLimitata(((short)0));
        }
        else {
            ws.getIndGar().setTsStabLimitata(((short)-1));
        }
    }

    @Override
    public String getWsDataInizioEffettoDb() {
        return ws.getIdsv0010().getWsDataInizioEffettoDb();
    }

    @Override
    public void setWsDataInizioEffettoDb(String wsDataInizioEffettoDb) {
        this.ws.getIdsv0010().setWsDataInizioEffettoDb(wsDataInizioEffettoDb);
    }

    @Override
    public long getWsTsCompetenza() {
        return ws.getIdsv0010().getWsTsCompetenza();
    }

    @Override
    public void setWsTsCompetenza(long wsTsCompetenza) {
        this.ws.getIdsv0010().setWsTsCompetenza(wsTsCompetenza);
    }
}
